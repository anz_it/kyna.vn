<?php

namespace console\components;

use yii\db\Migration;

class KynaMigration extends Migration
{
    
    public static $_tableOptions = null;
    
    public function init()
    {
        parent::init();
        
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            self::$_tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
    }
}

