<?php
/**
 * Created by PhpStorm.
 * User: khanhphan
 * Date: 1/20/17
 * Time: 4:27 PM
 */

namespace app\modules\tracking\controllers;


use kyna\tracking\models\Tracking;
use kyna\tracking\models\TrackingSetting;
use kyna\tracking\models\TrackingUserCare;

class AssignController extends  \yii\console\Controller
{
    public function actionIndex()
    {
        if (TrackingSetting::get("auto_assign")) {
            $lastRun = TrackingSetting::get("last_run");
            $schedule = strtotime("now -".TrackingSetting::get("schedule"));
            if ($schedule - $lastRun >= 0) {
                echo "Will be run.\n";
                do {
                    /** @var Tracking $obj */
                    $obj = Tracking::find()->where(['or', 'is_registered = 0', 'is_registered is null'])
                        ->andWhere(['or', 'user_id_care = 0', 'user_id_care is null'])
                        ->one();
                    if ($obj == null) {

                        break;
                    }
                    /** @var TrackingUserCare $user */
                    $user = TrackingUserCare::find()->where(['>','id', TrackingSetting::get('last_assign_id')])
                        ->orderBy(['id' => 'asc'])->one();
                    if ($user == null)
                        $user = TrackingUserCare::find()
                            ->orderBy(['id' => 'asc'])->one();
                    if ($user == null) {
                        echo "No user to add\n";
                        break;
                    }

                    $obj->user_id_care = $user->user_id;
                    $obj->save(false);

                    $user->last_assign = time();
                    $user->save(false);

                    TrackingSetting::set('last_assign_id', $user->id);

                } while ($obj != null);
                TrackingSetting::set("last_run", time());
            }

        } else {
            echo "Not enable\n";
        }
    }

}