<?php

/**
 * Created by PhpStorm.
 * User: khanhphan
 * Date: 1/13/17
 * Time: 5:24 PM
 */
namespace app\modules\tracking\controllers;

use kyna\tracking\models\PiwikLogVisit;
use kyna\tracking\models\Tracking;
use Yii;

class GetMobileController extends \yii\console\Controller
{

    const API_URL = "http://128.199.209.82/api";
    const CACHE_KEY = "get_mobile_token";
    const API_KEY = "90b16e7e6f08df888089af399b4eca50";

    public function getToken()
    {
        $token = Yii::$app->cache->get(self::CACHE_KEY);
        if (empty ($token)) {

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, self::API_URL . "/accounts/auth");
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode([
                "usr" => "tram@kyna.vn",
                "pwd" => "tram@123",
                "orgId" => "KYNA"
            ]));
            curl_setopt($ch, CURLOPT_HTTPHEADER, [
                'Content-Type: application/json',
               // 'Auth-Secret: Bvk7Hg8VBx7bespYY3RLnOQMrSb1hCN0' // Secret provided by Kyna
            ]);
            curl_setopt($ch, CURLOPT_POST, 1);

            $result = json_decode(curl_exec($ch));
            curl_close($ch);
            if ($result != null && isset($result->token)) {
                Yii::$app->cache->set(self::CACHE_KEY, $result->token, 1800);
                return $result->token;
            }
        }
        return $token;
    }

    public function getPiwikId($token, $leadsId, $fromTime, $toTime)
    {
        $url = self::API_URL
            . "/dsvc/leads/filters/087b9f6d9e5e18c5f8288485c4346adf/access-log?fromTime={$fromTime}&toTime={$toTime}&leadId={$leadsId}";
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'TOKEN: '.$token,
            'API_KEY: '.self::API_KEY
        ]);
        $result = json_decode(curl_exec($ch));
        if ($result != null && isset($result->results->content[0])) {
            $object = $result->results->content[0];
            $parts = parse_url($object->url);
            parse_str($parts['query'], $query);
            if (isset($query['piwikId']))
                return explode('-', $query['piwikId']."-"."0");
        }
        return ["",""];
    }

    public function getNumber()
    {
        $fromTime = strtotime("-1 day")."000";
        $toTime = strtotime("now + 1 day")."000";
        $token = $this->getToken();
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, self::API_URL
            . "/dsvc/leads/filters/087b9f6d9e5e18c5f8288485c4346adf?fromTime={$fromTime}&toTime={$toTime}");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'TOKEN: '.$token,
            'API_KEY: '.self::API_KEY
        ]);

        $result = json_decode(curl_exec($ch));

        if ($result != null && is_array($result->results))
        {
            foreach ($result->results as $item) {

                if (empty ($item->contact))
                    continue;

                /** @var  $track Tracking*/
                $track = Tracking::find()->where(['number' => $item->contact])->one();
                $data = $this->getPiwikId($token, $item->leadId, $fromTime, $toTime);
                $piwikId = $data[0];
                $pageId = $data[1];

                if ($track == null) {
                    echo "New track, add to db\n";
                    $track = new Tracking();
                    $track->updated_time = time();
                    $track->piwik_id = $piwikId;
                    $track->number = $item->contact;
                    $track->page_id = $pageId;


                    $piwikVisit = PiwikLogVisit::find()->where(['custom_var_v1' => $piwikId])->one();
                    if ($piwikVisit != null) {
                        $track->piwik_visitor_id = bin2hex($piwikVisit->idvisitor);

                        $registered = PiwikLogVisit::find()->where(['custom_var_v1' => $piwikId])->andWhere(
                            ['or', ['visit_entry_idaction_url' => 27], ['visit_exit_idaction_url' => 27] ]
                        )->count();

                        $track->is_registered = $registered ? 1 : 0;


                    } else {
                        echo "Piwik not found.\n";
                    }

                    $track->number_called = 0;
                    $track->update_piwik_times = 0;
                    $track->last_access = substr($item->lastAccess, 0, 10);
                    $track->save(false);
                } else if ($track->piwik_visitor_id == null) {
                    echo "Track found. piwik null\n";
                    if ($track->update_piwik_times < 5) {
                        $piwikVisit = PiwikLogVisit::find()->where(['custom_var_v1' => $piwikId])->one();

                        if ($piwikVisit != null) {
                            $track->piwik_visitor_id = bin2hex($piwikVisit->idvisitor);
                            $track->updated_time = time();
                        }

                    }
                    $track->update_piwik_times += 1;
                    $track->save(false);

                } else if ( time() - $track->updated_time > 5184000) {
                    echo "Track found, timedout. re-update piwik.\n";
                    $piwikVisit = PiwikLogVisit::find()->where(['custom_var_v1' => $piwikId])->one();

                    if ($piwikVisit != null) {
                        $track->piwik_visitor_id = bin2hex($piwikVisit->idvisitor);
                    }
                    $track->updated_time = time();
                    $track->save(false);
                } else {
                    echo "Do nothing.\n";
                }
            }
        }

    }
    public function actionIndex()
    {
        $this->getNumber();
    }

}