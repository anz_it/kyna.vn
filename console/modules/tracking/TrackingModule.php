<?php

/**
 * Created by PhpStorm.
 * User: khanhphan
 * Date: 1/13/17
 * Time: 5:21 PM
 *
 */
namespace app\modules\tracking;

use \yii\base\Module;

class TrackingModule extends Module
{

    public $controllerNamespace = 'app\modules\tracking\controllers';


}