<?php

/**
 * Created by PhpStorm.
 * User: ngunp
 * Date: 7/10/2017
 * Time: 10:40 AM
 */

namespace app\modules\tag\controllers;

use kyna\course\models\Course;
use kyna\tag\models\CourseTag;
use kyna\tag\models\Tag;
use Yii;
use yii\console\Controller;

class WorkerController extends Controller
{
    public function actionAdd()
    {
        $tag_id = false;
        while (!$tag_id){
            $tag_id = readline("Nhập khóa chính ID Tag khóa học: ");
            $tag = Tag::findOne(['id'=>$tag_id,'status'=>1]);
            if(empty($tag) || is_null($tag)){
                $tag_id = false;
                echo "\033[31m Error : ID Tag này không tồn tại trong hệ thống \033[0m \n";
            }
        }

        $listIdCourses = FALSE;
        while (!$listIdCourses){
            $listIdCourses = readline("Bạn nhập danh sách Id khóa học cách nhau bởi dấu phẩy: ");
        }

        $listIdCourses = explode(',',$listIdCourses);
        foreach ($listIdCourses as $key => $course){
            $courseTag = CourseTag::findOne(['course_id'=>$course,'tag_id'=>$tag_id]);
            if(!$courseTag){
                $courseTag = new CourseTag();
            }
            $courseTag->setCourse($course);
            $courseTag->setTag($tag_id);
            $courseTag->save(false);
        }
        print_r(CourseTag::find()->where(['tag_id'=>$tag_id])->asArray()->all());
        print_r('OK');die;
    }

    public function actionRemove(){
        $tag_id = false;
        while (!$tag_id){
            $tag_id = readline("Nhập khóa chính ID Tag khóa học: ");
            $tag = Tag::findOne(['id'=>$tag_id,'status'=>1]);
            if(empty($tag) || is_null($tag)){
                $tag_id = false;
                echo "\033[31m Error : ID Tag này không tồn tại trong hệ thống \033[0m \n";
            }
        }

        $listIdCourses = false;
        while (!$listIdCourses){
            $listIdCourses = readline("Bạn nhập danh sách Id khóa học cách nhau bởi dấu phẩy: ");
        }

        $listIdCourses = explode(',',$listIdCourses);
        foreach ($listIdCourses as $key => $course){
            $courseTag = CourseTag::findOne(['course_id'=>$course,'tag_id'=>$tag_id]);
            if($courseTag){
                $courseTag->delete();
            }
        }
        print_r('remove OK');
    }
}