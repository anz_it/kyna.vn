<?php

/**
 * Created by PhpStorm.
 * User: ngunp
 * Date: 7/10/2017
 * Time: 10:38 AM
 */
namespace app\modules\tag;

class TagModule extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\tag\controllers';

    public function init()
    {
        parent::init();
        // custom initialization code goes here
    }
}