<?php

namespace app\modules\commission;

class CommissionModule extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\commission\controllers';

    public function init()
    {
        parent::init();
        // custom initialization code goes here
    }
}
