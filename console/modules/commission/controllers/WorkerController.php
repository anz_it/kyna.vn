<?php


namespace app\modules\commission\controllers;

use kyna\commission\Commission;
use kyna\commission\models\CommissionIncome;
use kyna\order\models\Order;
use Yii;
use yii\console\Controller;

class WorkerController extends Controller
{
    public function actionAdd()
    {
        $order_id = false;
        while (!$order_id){
            $order_id = readline("Nhập ID đơn hàng: ");
            $order = Order::findOne(['id'=>$order_id,'status'=>5]);
            if(empty($order) || is_null($order)){
                $order_id = false;
                echo "\033[31m Error : ID đơn hàng này không tồn tại trong hệ thống \033[0m \n";
            }
        }
        $commissions = CommissionIncome::findOne(['order_id'=>$order_id]);
        if($commissions){
            print_r('Commission đã được tính cho đơn hàng này rồi');die;
        }
        $details = $order->details;
        //tính commmission
        foreach ($details as $detail) {
            Commission::calculate(
                $order->id,
                $order->subTotal,
                $order->total,
                $order->totalCourseDiscount,
                $order->totalDiscount,
                $order->paymentFee,
                $detail->course_id,
                $detail->itemTotal,
                $order->affiliate_id,
                $detail->course_combo_id,
                $detail,
                $order->shipping_fee,
                $order->direct_discount_amount,
                $details
            );
        }
        print_r('OK');die;
    }
}