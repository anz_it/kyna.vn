<?php

/**
 * Created by PhpStorm.
 * User: khanhphan
 * Date: 4/18/17
 * Time: 4:27 PM
 */
namespace app\modules\elastic\controllers;

use app\modules\course\models\Category;
use common\helpers\StringHelper;
use Elasticsearch\Client;
use kyna\course\models\Course;
use kyna\course\models\Teacher;
use kyna\user\models\Profile;
use yii\console\Controller;
use yii\helpers\Json;

class IndexController extends Controller
{
    /**
     * Delete Index
     * @param null $index index-name (required)
     */
    public function actionDelete($index=null)
    {
        if (!isset($index)) {
            echo " - ERROR!! Index name missing!!\n";
            echo " - SYNTAX: ./yii elastic/index/delete <index-name>\n";
            return;
        }

        /** @var Client $client */
        $client = \Yii::$app->elasticsearch->client;

        if (!$client->indices()->exists(['index' => $index])) {
            echo " - ERROR!! Index '".$index."' not existed!!\n";
            return;
        }

        $response = $client->indices()->delete([
            'index' => $index
        ]);
        print_r($response);
    }

    /**
     * Create Index
     * @param null $index index-name (required)
     */
    public function actionInit($index=null)
    {
        if (!isset($index)) {
            echo " - ERROR!! Index name missing!!\n";
            echo " - SYNTAX: ./yii elastic/index/init <index-name>\n";
            return;
        }

        $json = \common\elastic\Course::getMappingQuery();
        /** @var Client $client */
        $client = \Yii::$app->elasticsearch->client;
        //$url = $client->transport->getConnection()->getTransportSchema()."://".$client->transport->getConnection()->getHost();
        // $ch = curl_init($url."/".\common\elastic\Course::getIndex());

        if ($client->indices()->exists(['index' => $index])) {
            echo " - ERROR!! Index '".$index."' already existed!!\n";
            return;
        }

        $response = $client->indices()->create([
            'index' => $index,
            'body' => Json::decode($json)
        ]);
        print_r($response);
    }

    /**
     * Index document
     * @param null $index index-name (required)
     */
    public function actionIndex($index=null)
    {
        if (!isset($index)) {
            echo " - ERROR!! Index name missing!!\n";
            echo " - SYNTAX: ./yii elastic/index/index <index-name>\n";
            return;
        }

        if (isset(\Yii::$app->log->targets['file']))
            \Yii::$app->log->targets['file']->enabled = false;
        $time = microtime(true);
        /** @var Client $client */
        $client = \Yii::$app->elasticsearch->client;

        if (!$client->indices()->exists(['index' => $index])) {
            echo " - ERROR!! Index '".$index."' not existed\n";
            return;
        }

        /** @var Course[] $courses */
        $courses = Course::find()->all();
        $count = count($courses);
        $params = ['body' => []];
        echo "Query done, time ".(microtime(true) - $time) . "\n";
        $time = microtime(true);
        for ($i = 0; $i < $count; $i++) {
            $params['body'][] = [
                'index' => [
                    '_index' => $index,
                    '_type' => 'page',
                    '_id' => $courses[$i]->id
                ]
            ];

            $params['body'][] = \common\elastic\Course::getIndexQuery($courses[$i]);

            // Every 1000 documents stop and send the bulk request
            if ($i % 1000 == 0) {
                $responses = $client->bulk($params);

                // erase the old bulk request
                $params = ['body' => []];

                // unset the bulk response when you are done to save memory
                unset($responses);
            }

            unset($courses[$i]);
        }

        // Send the last batch if it exists
        if (!empty($params['body'])) {
            $responses = $client->bulk($params);
        }

        echo "Index done, time ".(microtime(true) - $time)."\n";

    }

    // Doi Index cua alias: Cu phap ./yii elastic/index/changealias <oldIndexName> <newIndexName> [aliasName]
    /**
     * @param null $new new-index-name (required)
     * @param null $old old-index-name
     * @param null $alias alias-name
     */
    public function actionChangeAlias($old=null, $new=null, $alias=null)
    {
        if (!isset($new)) {
            echo " - ERROR!! Index name missing!!\n";
            echo " - SYNTAX: ./yii elastic/index/change-alias <old-index-name> <new-index-name> [alias-name]\n";
            return;
        }

        if (!isset($alias))
            $alias = \common\elastic\Course::getAlias();

        /** @var Client $client */
        $client = \Yii::$app->elasticsearch->client;
        if (!$client->indices()->exists(['index' => $new])) {
            echo " - ERROR!! Index '".$new."' not existed\n";
            return;
        }

        if (!$client->indices()->exists(['index' => $old])) {
            echo " - ERROR!! Index '".$old."' not existed\n";
            return;
        }

        // Add alias to $new index
        $response = $client->indices()->putAlias([
            'index' => $new,
            'name' => $alias
        ]);
        print_r($response);

        // Delete alias from $old index
        $response = $client->indices()->getAlias([
            'index' => $old,
            'name' => $alias
        ]);
        if (sizeof($response) > 0)
        {
            unset($response);
            $response = $client->indices()->deleteAlias([
                'index' => $old,
                'name' => $alias
            ]);
            unset($response);
        }
        unset($response);
        unset($response);

        echo " - SUCCESS!! From now alias '".$alias."' will present for index '".$new."'\n";
    }

    public function actionSetAlias($index=null, $alias=null)
    {
        if (!isset($index)) {
            echo " - ERROR!! Index name missing!!\n";
            echo " - SYNTAX: ./yii elastic/index/set-alias <index-name> [alias-name]\n";
            return;
        }

        if (!isset($alias))
            $alias = \common\elastic\Course::getAlias();

        /** @var Client $client */
        $client = \Yii::$app->elasticsearch->client;
        if (!$client->indices()->exists(['index' => $index])) {
            echo " - ERROR!! Index '".$index."' not existed\n";
            return;
        }

        // Add alias to $new index
        $response = $client->indices()->putAlias([
            'index' => $index,
            'name' => $alias
        ]);
        print_r($response);
        unset($response);
        echo " - SUCCESS!! From now alias '".$alias."' will present for index '".$index."'\n";
    }

    public function actionRemoveAlias($index=null, $alias=null)
    {
        if (!isset($index)) {
            echo " - ERROR!! Index name missing!!\n";
            echo " - SYNTAX: ./yii elastic/index/remove-alias <index-name> [alias-name]\n";
            return;
        }

        if (!isset($alias))
            $alias = \common\elastic\Course::getAlias();

        /** @var Client $client */
        $client = \Yii::$app->elasticsearch->client;
        if (!$client->indices()->exists(['index' => $index])) {
            echo " - ERROR!! Index '".$index."' not existed\n";
            return;
        }

        // Delete alias from $old index
        $response = $client->indices()->getAlias([
            'index' => $index,
            'name' => $alias
        ]);
        if (sizeof($response) > 0)
        {
            unset($response);
            $response = $client->indices()->deleteAlias([
                'index' => $index,
                'name' => $alias
            ]);
            unset($response);
        }

        echo " - SUCCESS!! From now alias '".$alias."' will present for index '".$index."'\n";
    }

    public function actionLogInit($index=null)
    {

        if (!isset($index)) {
            echo " - ERROR!! Index name missing!!\n";
            echo " - SYNTAX: ./yii elastic/index/log-init <index-name>\n";
            return;
        }

        $json = file_get_contents("common/elastic/mapping/log.json");
        /** @var Client $client */
        $client = \Yii::$app->elasticlog->client;
        //$url = $client->transport->getConnection()->getTransportSchema()."://".$client->transport->getConnection()->getHost();
        // $ch = curl_init($url."/".\common\elastic\Course::getIndex());

        if ($client->indices()->exists(['index' => $index])) {
            echo " - ERROR!! Index '".$index."' already existed!!\n";
            return;
        }

        $response = $client->indices()->create([
            'index' => $index,
            'body' => Json::decode($json)
        ]);
        print_r($response);

    }
}