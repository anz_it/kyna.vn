<?php

namespace app\modules\elastic;

class ElasticModule extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\elastic\controllers';

    public function init()
    {
        parent::init();
        // custom initialization code goes here
    }
}
