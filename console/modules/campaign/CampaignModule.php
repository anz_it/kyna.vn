<?php

namespace app\modules\campaign;

class CampaignModule extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\campaign\controllers';

    public function init()
    {
        parent::init();
        // custom initialization code goes here
    }
}
