<?php

namespace app\modules\extra\models;

/**
 * Created by PhpStorm.
 * User: macintosh
 * Date: 5/22/17
 * Time: 4:55 PM
 */
class Order extends \kyna\order\models\Order
{

    public function scenarios()
    {
        $scenarios = parent::scenarios();

        $scenarios[self::SCENARIO_DEFAULT] = [
            'user_id',
            'direct_discount_amount',
            'group_discount_amount',
            'payment_method',
            'operator_id',
            'reference_id',
            'sub_total',
            'total_discount',
            'shipping_fee',
            'total',
            'payment_fee',
            'point_of_sale',
            'is_done_telesale_process',
            'sub_total',
            'total_discount',
            'status',
            'order_number',
            'order_date',
            'promotion_code',
            'activation_code',
            'is_paid',
            'affiliate_id',
            'is_activated',
            'activation_date',
            'sent_payment_request_time',
            'page_id',
        ];

        return $scenarios;
    }
}