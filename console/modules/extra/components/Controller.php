<?php

namespace app\modules\extra\components;

use Yii;
use yii\helpers\Console;
use common\helpers\StringHelper;

class Controller extends \yii\console\Controller
{
    
    protected $table = '';

    public function init()
    {
        $ret = parent::init();
        
        $result = Yii::$app->db->createCommand("SHOW COLUMNS FROM {$this->table} like 'is_sync';")->queryOne();
        if ($result === false) {
            Yii::$app->db->createCommand("ALTER TABLE {$this->table} 
                ADD COLUMN `key` VARCHAR(100) NULL DEFAULT NULL,
                ADD COLUMN `is_sync` BIT(1) NULL DEFAULT 0,
                ADD UNIQUE INDEX `key_UNIQUE` (`key` ASC);
            ")->execute();
        }
        
        return $ret;
    }
    
    public function actionRun()
    {
        $i = 0;
        do {
            list($key, $rets) = $this->setKey();
            
            if (empty($rets)) {
                break;
            }
            echo ++$i;
            
            $dataV2 = $this->getData($key);

            if ($dataV2 !== false) {
                $model = $this->create($dataV2);
                if ($model !== false) {
                    $this->makeSync($key);
                    echo ' -> ' . $dataV2['id'];
                    Yii::info($i . ' -> ' . $dataV2['id'], $this->table);
                } else {
                    echo ' -> error: ' . $key;
                    Yii::error($i . ' -> error: ' . $key, $this->table);
                }
            }
            echo PHP_EOL;
        } while (!empty($rets));
        
        $this->stdout("--End--" . PHP_EOL, Console::FG_GREEN);
        return 0;
    }
    
    /**
     * Generate unique key and update to table row will be migrate
     * @return $key and $results
     */
    protected function setKey()
    {
        $key = StringHelper::random(20, true);

        $rets = Yii::$app->db->createCommand("UPDATE {$this->table} SET `key` = '{$key}' WHERE `is_sync` = 0 AND `key` IS NULL ORDER BY id LIMIT 1")->execute();
        
        return [$key, $rets];
    }
    
    /**
     * Get table row of item need to be migrated by the key
     * @param type $key
     * @return array field and data (table row)
     */
    protected function getData($key)
    {
        return Yii::$app->db->createCommand("SELECT * FROM {$this->table} WHERE `key` = '$key'")->queryOne();
    }
    
    /**
     * When migrate successfully to V3 then make status is_sync = 1 at V2
     * @param type $key
     * @return result
     */
    protected function makeSync($key)
    {
        return Yii::$app->db->createCommand("UPDATE {$this->table} SET `is_sync` = 1 WHERE `is_sync` = 0 AND `key` = '{$key}'")->execute();
    }
    
}
