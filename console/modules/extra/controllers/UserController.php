<?php

namespace app\modules\extra\controllers;

use kyna\user\models\UserMeta;
use kyna\user\models\Profile;

/* 
 * UserController class to help synch User data from V2 to V3.
 */
class UserController extends \app\modules\extra\components\Controller
{
    
    public $table = 'user';
    
    protected function create($data = [])
    {
        $needToUpdate = false;
        $phone = $fid = '';
        
        $userMetas = UserMeta::find()->where(['key' => ['fb_id', 'phone'], 'user_id' => $data['id']])->all();
        
        foreach ($userMetas as $metaModel) {
            if ($metaModel->key == 'fb_id' && !empty($metaModel->value)) {
                $needToUpdate = true;
                $fid = $metaModel->value;
            }
            if ($metaModel->key == 'phone' && !empty($metaModel->value)) {
                $needToUpdate = true;
                $phone = $metaModel->value;
            }
        }
        
        if ($needToUpdate) {
            $profile = Profile::find()->where(['user_id' => $data['id']])->one();
            
            if (!empty($fid)) {
                $profile->facebook_id = $fid;
            }
            
            if (!empty($phone)) {
                $profile->phone_number = substr(trim($phone), 0, 15);
            }
            
            $profile->save(false);
            
            return $profile;
        }
        
        return false;
    }
}