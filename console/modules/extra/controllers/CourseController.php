<?php

namespace app\modules\extra\controllers;

use kyna\course\models\Course;
use kyna\tag\models\CourseTag;
use kyna\tag\models\Tag;
use Yii;
use yii\console\Exception;

class CourseController extends \yii\console\Controller
{
    
    public function actionDeleteTrash()
    {
        do {
            $models = Course::find()->where(['v2_id' => NULL])->limit(10)->all();
            
            foreach ($models as $model) {
                $dbTransaction = Yii::$app->db->beginTransaction();

                try {
                    $id = $model->id;


                    if ($model->delete()) {
                        echo "DELETED: {$id}" . PHP_EOL;

                        $dbTransaction->commit();
                    }
                } catch (\Exception $e) {
                    var_dump($e->getMessage());
                    $dbTransaction->rollBack();
                    die;
                }
            }
        } while (!empty($models));
    }

    /**
     * Just re-filter for Course content
     */
    public function actionRemoveNofollow() {
        $courses = Course::find()->all();
        if (!empty($courses)) {
            foreach ($courses as $course) {
                $course->save();
            }
        }
    }
    public function actionMigrateTags($strKeywords = '')
    {
        echo "---------Pre-Defined Tags--------- \n";

        if (!empty($strKeywords)) {
            $tags = explode(',', $strKeywords);
        } else {
            $tags = [
                // 02.2017
                'Marketing',
                'Bất động sản',
                'Lập trình',
                'Tiếng nhật',
                'Tiếng hoa',
                'Giao tiếp',
                'Monkey junior',
                'Bán hàng',
                'Giáo dục sớm',
                'Tiếng anh',
                // 03.2017
                'Facebook',
                'Guitar',
                'Photoshop',
                'Yoga'
            ];
        }
        foreach ($tags as $tagKey) {
            $tagModel = Tag::getTagModel($tagKey);
            echo "Created/Updated tag: {$tagModel->tag} \n";
        }

        echo "---------Migrate Tags--------- \n";
        foreach ($tags as $tag) {
            $courses = Course::find()
                ->alias('c')
                ->where("(c.keyword LIKE :q)", [':q' => "%{$tag}%"])
                ->all();
            if (!empty($courses)) {
                foreach ($courses as $key => $course) {
                    CourseTag::assignCourseTag($tag, $course->id, false);
                    echo "Assigned tag: course-{$course->id} <=> tag-{$tag} /{$key} \n";
                }
            }
        }

    }

    public function actionCopyUserCourse($fromID, $toID) {
        $fromCourse = Course::findOne($fromID);
        $toCourse = Course::findOne($toID);
        if (empty($fromCourse) || empty($toCourse)) {
            echo 'Invalid Course(s)';
            die;
        }
        $sql = "
            INSERT INTO user_courses (
                user_id, 
                course_id, 
                is_activated,
                activation_date,
                created_time,
                updated_time
            )
            SELECT 
                uc.user_id, 
                {$toCourse->id}, 
                uc.is_activated, 
                UNIX_TIMESTAMP(), 
                UNIX_TIMESTAMP(), 
                UNIX_TIMESTAMP()
            FROM user_courses uc
            WHERE uc.course_id = {$fromCourse->id}
                AND uc.is_activated = 1 
                AND uc.is_deleted = 0
        ";
        $results = Yii::$app->db->createCommand($sql)->execute();
        echo "Inserted {$results} record(s)";
    }

    public function actionMigrateKeywordsToTags() {
        $courses = Course::find()
            ->where(['not',['keyword' => null]])
            ->all();
        foreach ($courses as $key => $course) {
            $keywords = str_replace(';', ',', $course->keyword);
            $keywords = explode(',', $keywords);
            $db = \Yii::$app->db;
            $transaction = $db->beginTransaction();
            try {
                CourseTag::assignCourseTag($keywords, $course->id, false);
                echo "{$key} - Course: {$course->id} \n";
                $transaction->commit();
            } catch (Exception $e) {
                $transaction->rollback();
            }
        }
        echo "--Done--";
    }
}
