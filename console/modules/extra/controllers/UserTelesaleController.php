<?php

namespace app\modules\extra\controllers;

use Yii;
use kyna\user\models\User;
use kyna\user\models\Profile;
use kyna\user\models\UserTelesale;

use kyna\user\models\actions\UserTelesaleAction;
use kyna\user\models\actions\UserTelesaleActionMeta;

class UserTelesaleController extends \yii\console\Controller
{
    
    public function actionDeleteTrash()
    {
        do {
            $models = UserTelesale::find()->where(['v2_id' => NULL])->limit(10)->all();
            
            foreach ($models as $model) {
                $dbTransaction = Yii::$app->db->beginTransaction();

                try {
                    $id = $model->id;
                    $actions = UserTelesaleAction::find()->where(['user_telesale_id' => $id])->all();
                    
                    foreach ($actions as $action) {
                        UserTelesaleActionMeta::deleteAll(['user_telesale_action_id' => $action->id]);
                        $action->delete();
                    }

                    if ($model->delete()) {
                        echo "DELETED: {$id}" . PHP_EOL;

                        $dbTransaction->commit();
                    }
                } catch (\Exception $e) {
                    var_dump($e->getMessage());
                    $dbTransaction->rollBack();
                    die;
                }
            }
        } while (!empty($models));
    }

    /**
     * Sync data when switch Product's environments.
     */
    public function actionSyncData()
    {
        $newUserIds = $this->syncUser();

        echo "...Start sync user telesales..." . PHP_EOL;

        $result = Yii::$app->db_backup->createCommand("select max(id) as lastId from user_telesales;")->queryOne();
        $lastId = $result['lastId'];

        $newRecords = Yii::$app->db_new->createCommand("select * from user_telesales where id > {$lastId}")->queryAll();
        foreach ($newRecords as $newRecord) {
            $userTelesale = new UserTelesale();
            $userTelesale->attributes = $newRecord;

            if (!empty($userTelesale->user_id) && key_exists($userTelesale->user_id, $newUserIds)) {
                $userTelesale->user_id = $newUserIds[$userTelesale->user_id];
            }

            if ($userTelesale->save()) {
                echo  $newRecord['id'] . " ---> " . $userTelesale->id . PHP_EOL;
            } else {
                var_dump($userTelesale->errors);
            }
        }
    }

    public function syncUser()
    {
        echo "...Start sync users..." . PHP_EOL;
        $newUserIds = [];

        $result = Yii::$app->db_backup->createCommand("select max(id) as lastId from user;")->queryOne();
        $lastId = $result['lastId'];

        $newRecords = Yii::$app->db_new->createCommand("select * from user where id > {$lastId}")->queryAll();

        foreach ($newRecords as $newRecord) {
            $user = new User();
            $user->attributes = $newRecord;
            $user->password_hash = $newRecord['password_hash'];
            $user->auth_key = $newRecord['auth_key'];
            $user->confirmed_at = $newRecord['confirmed_at'];
            $user->blocked_at = $newRecord['blocked_at'];
            $user->registration_ip = $newRecord['registration_ip'];
            $user->status = $newRecord['status'];
            $user->created_at = $newRecord['created_at'];
            $user->updated_at = $newRecord['updated_at'];

            if ($user->save()) {
                $profileData = Yii::$app->db_new->createCommand("select * from profile where user_id = {$newRecord['id']}")->queryOne();

                $profile = Profile::findOne(['user_id' => $profileData['user_id']]);
                if ($profile == null) {
                    $profile = new Profile();
                }
                $profile->attributes = $profileData;
                $profile->user_id = $profileData['user_id'];
                if (!$profile->save()) {
                    var_dump($profile->errors);
                }

                $newUserIds[$newRecord['id']] = $user->id;

                echo  $newRecord['id'] . " ---> " . $user->id . PHP_EOL;
            } else {
                var_dump($user->errors, $newRecord);
            }
        }

        return $newUserIds;
    }

}
