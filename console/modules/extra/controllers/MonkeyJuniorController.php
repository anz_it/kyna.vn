<?php

namespace app\modules\extra\controllers;

use Yii;
use yii\helpers\ArrayHelper;
use yii\console\Controller;

use kyna\order\models\Order;
use kyna\order\models\OrderDetails;
use kyna\course\models\Course;
use kyna\user\models\UserCourse;
use kyna\user\models\User;
use kyna\partner\models\Partner;

class MonkeyJuniorController extends Controller
{

    public function actionAddGuideCourse()
    {
        $tblOrderDetail = OrderDetails::tableName();
        $tblUserCourse = UserCourse::tableName();
        $tblUser = User::tableName();

        $idCourseGuide = Yii::$app->params['id_of_monkey_junior_guide_course'];

        $courseTbl = Course::tableName();
        $partnerTbl = Partner::tableName();
        $mkClass = Partner::CLASS_MONKEY_JUNIOR;
        $monkeyCourseIds = ArrayHelper::map(Course::find()
            ->join('INNER JOIN', $partnerTbl, "{$partnerTbl}.id = {$courseTbl}.partner_id")
            ->andWhere(["{$courseTbl}.type" => Course::TYPE_SOFTWARE, "{$partnerTbl}.class" => "{$mkClass}"])
            ->select("`{$courseTbl}`.`id`")->asArray()->all(), "id", "id");

        $i = 0;
        do {
            $orders = Order::find()->alias('o')->joinWith(['details', 'user'])
                ->where([$tblOrderDetail . '.course_id' => $monkeyCourseIds])
                ->andWhere(['is not', $tblUser . '.id', null])
                ->andWhere("user_id NOT IN (SELECT user_id FROM $tblUserCourse WHERE course_id = $idCourseGuide)")
                ->limit(10)->all();

            foreach ($orders as $order) {
                $user = $order->user;

                if ($user != null && UserCourse::active($user->id, $idCourseGuide) !== false) {
                    // send mail
                    $mailer = Yii::$app->mailer;
                    $mailer->htmlLayout = false;
                    $mailer->compose('@common/mail/order/monkey_junior_guide_auto')
                        ->setTo($user->email)
                        ->setSubject('Tặng ba mẹ khóa học "Hướng dẫn học Monkey Junior hiệu quả" tại Kyna.vn')
                        ->send();
                }

                echo $i++ . PHP_EOL;
            }

        } while (!empty($orders));

        echo "__END__" . PHP_EOL;
    }
}