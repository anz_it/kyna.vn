<?php

namespace app\modules\extra\controllers;

use kyna\course\models\Teacher;
use common\helpers\ArrayHelper;
use kyna\course\models\CourseLearnerQna;
use kyna\settings\models\Setting;
use Yii;

class QnaController extends \yii\console\Controller
{

    /**
     * Email to Teacher if question is approved and sent to student
     * @param $id
     */
    public function actionEmailTeacher()
    {
        $data = $this->formatDataQna();

        $template = '@common/mail/qa/tpl_send_teacher';
        if (!is_null($data)) {
            foreach ($data as $teacherId => $questionCourses) {
                $teacher = Teacher::findOne($teacherId);
                $gender = $teacher->getMeta('gender');
                $subject = "[Kyna.vn] Câu hỏi từ học viên";
                // get setting cc email
                $settings = ArrayHelper::map(Setting::find()->all(), 'key', 'value');
                $fromEmail =  !empty($settings['email_teacher_from']) ? $settings['email_teacher_from'] : 'hotro@kyna.vn';
                $ccEmails = !empty($settings['email_teacher_cc']) ? $settings['email_teacher_cc'] : '';
                $ccEmails = explode(',', $ccEmails);
                // send mail
                Yii::$app->mailer->compose()
                    ->setHtmlBody($this->renderPartial($template, [
                        'questionCourses' => $questionCourses,
                        'gender' => 'Thầy/Cô',//is_null($gender) ? 'cô/thầy' : (($gender) ? 'thầy' : 'cô'),
                        'fullName' => $teacher->profile->name
                    ]))
                    ->setFrom($fromEmail)
                    ->setTo($teacher->email)
                    ->setCc($ccEmails)
                    ->setSubject($subject)
                    ->send();
                // update question param after send mail
                foreach ($questionCourses as $questions) {
                    foreach ($questions as $questionId) {
                        $model = CourseLearnerQna::findOne($questionId);
                        $model->is_send_to_teacher = CourseLearnerQna::BOOL_YES;
                        $model->save();
                    }
                }
            }
        }
    }

    protected function formatDataQna()
    {
        $data = [];
        $date = date('l');

        $questions = CourseLearnerQna::find()
            ->alias('q')
            ->select(['max(q.id) as id', 'q.course_id'])
            ->where([
                'is_approved' => CourseLearnerQna::BOOL_YES,
                'is_send_to_teacher' => CourseLearnerQna::BOOL_NO,
            ])
            ->joinWith('teacherReceiveDateSetting st')
            ->andWhere("(st.id IS NULL OR (st.id IS NOT NULL AND st.value = '{$date}'))")
            ->groupBy('course_id')
            ->orderBy('course_id DESC')
            ->all();
        foreach ($questions as $question) {
            $questionCourse = $question->course;

            if (!empty($questionCourse)) {
                /* @var $courseTeacher Teacher */
                $courseTeacher = $questionCourse->teacher;
                $receiveQnaDate = $courseTeacher->getReceiveQnaDate();
                if ($receiveQnaDate != null && $receiveQnaDate != $date) {
                    continue;
                }

                // find all questions by course
                $questionsHasReply = CourseLearnerQna::find()
                    ->select('c.id')
                    ->alias('c')
                    ->innerJoin(['c1' => CourseLearnerQna::tableName()], 'c.id = c1.question_id')
                    ->where([
                        'c.course_id' => $questionCourse->id,
                        'c.is_approved' => CourseLearnerQna::BOOL_YES,
                        'c.is_send_to_teacher' => CourseLearnerQna::BOOL_NO,
                    ])
                    ->orderBy('c.id')
                    ->all();
                $questionsHasReply = ArrayHelper::getColumn($questionsHasReply, 'id');
                $questions = CourseLearnerQna::find()
                    ->where([
                        'course_id' => $questionCourse->id,
                        'is_approved' => CourseLearnerQna::BOOL_YES,
                        'is_send_to_teacher' => CourseLearnerQna::BOOL_NO,
                    ])
                    ->andWhere(['not in', 'id', $questionsHasReply])
                    ->all();
                if (!empty($questions)) {
                    $data[$courseTeacher->id] = isset($data[$courseTeacher->id]) ? $data[$courseTeacher->id] : [];
                    $data[$courseTeacher->id][$questionCourse->id] = ArrayHelper::getColumn($questions, 'id');
                }
            }
        }
        return $data;
    }

    protected function findAllModelToEmail()
    {
        return CourseLearnerQna::find()
            ->where([
                'is_approved' => CourseLearnerQna::BOOL_YES,
                'is_send_to_teacher' => CourseLearnerQna::BOOL_NO,
            ])
            ->all();
    }

}
