<?php

namespace app\modules\extra\controllers;

use kyna\settings\models\Banner;
use yii\console\Controller;
use common\lib\CDNImage;
use kyna\course\models\Course;
use kyna\user\models\User;
use kyna\tag\models\Tag;

/* 
 * ImageController class
 */
class ImageController extends Controller
{

    /**
     * @param $modelClass
     * @param $ids
     * @param bool $forceCreate
     */
    public function actionResize($modelClass, $ids = [], $forceCreate = false)
    {
        switch ($modelClass) {
            case 'course':
                $models = Course::find();
                $fields = ['image_url'];
                break;
            case 'user':
                $models = User::find();
                $fields = ['avatar'];
                break;
            case 'tag':
                $models = Tag::find();
                $fields = ['image_url'];
                $models->andWhere('image_url IS NOT NULL');
                break;
            case 'banner':
                $models = Banner::find();
                $fields = ['image_url', 'mobile_image_url'];
                break;
            default:
                $models = null;
                $fields = [];
                break;
        }
        if (!empty($ids)) {
            $ids = explode(',', $ids);
            $models->andWhere(['id' => $ids]);
        }
        $models = $models->all();
        if ($models) {
            foreach ($models as $model) {
                foreach ($fields as $field) {
                    if (!empty($model->{$field})) {
                        $cdnImage = new CDNImage($model, $field);
                        $result = $cdnImage->resize($model->{$field}, $model->imageSize, false, $forceCreate);
                        if ($result) {
                            echo "Resized: {$model->id} - {$field} \n";
                        }
                        unset($cdnImage);
                    }
                }
            }
        }

        echo "Finished \n";
    }
}