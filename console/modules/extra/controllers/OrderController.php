<?php

namespace app\modules\extra\controllers;

use Yii;

use kyna\order\models\Order;
use kyna\order\models\OrderMeta;
use kyna\order\models\OrderDetails;
use kyna\order\models\OrderShipping;
use kyna\order\models\actions\OrderAction;
use kyna\order\models\actions\OrderActionMeta;
use kyna\payment\models\PaymentTransaction;
use kyna\payment\models\PaymentTransactionMeta;
use kyna\commission\models\CommissionIncome;
use kyna\payment\models\TopupTransaction;
use kyna\commission\Commission;
use kyna\course\models\Course;
use kyna\settings\models\Setting;
use kyna\user\models\User;
use common\components\ExportExcel;
use console\models\Ghn;
use app\modules\extra\models\Order as SyncOrder;

class OrderController extends \yii\console\Controller
{

    /**
     * Delete test order
     * @param $id order id
     */
    public function actionDeleteTrash($id)
    {
        do {
            $orders = Order::find()->where(['id' => $id])->limit(10)->all();
            
            foreach ($orders as $order) {
                $dbTransaction = Yii::$app->db->beginTransaction();

                try {
                    $orderId = $order->id;

                    OrderMeta::deleteAll(['order_id' => $orderId]);
                    OrderDetails::deleteAll(['order_id' => $orderId]);
                    OrderShipping::deleteAll(['order_id' => $orderId]);
                    $actions = OrderAction::find()->where(['order_id' => $orderId])->all();
                    foreach ($actions as $action) {
                        OrderActionMeta::deleteAll(['order_action_id' => $action->id]);
                        $action->delete();
                    }
                    $transactions = PaymentTransaction::find()->where(['order_id' => $orderId])->all();
                    foreach ($transactions as $transaction) {
                        PaymentTransactionMeta::deleteAll(['payment_transaction_id' => $transaction->id]);
                        $transaction->delete();
                    }
                    CommissionIncome::deleteAll(['order_id' => $orderId]);
                    TopupTransaction::deleteAll(['order_id' => $orderId]);

                    if ($order->delete()) {
                        echo "DELETED: {$orderId}" . PHP_EOL;

                        $dbTransaction->commit();
                    }
                } catch (\Exception $e) {
                    var_dump($e->getMessage());
                    $dbTransaction->rollBack();
                    die;
                }
            }
        } while (!empty($orders));
    }

    /**
     * Update affiliate for order which user promotion code is related affiliate (Distribution Partners)
     * @param $id order id
     */
    public function actionUpdateAffiliate($id)
    {
        $order = Order::findOne($id);

        $promotion = $order->promotion;

        if (!empty($promotion->partner_id) && $order->affiliate_id != $promotion->partner_id) {
            $order->affiliate_id = $promotion->partner_id;
            $order->save(false);

            $comIncomes = CommissionIncome::find()->where(['order_id' => $id])->all();

            foreach ($comIncomes as $comIncome) {
                if ($comIncome->total_amount < 0) {
                    $comIncome->total_amount = 0;
                }

                $comIncome->user_id = $promotion->partner_id;
                $comIncome->affiliate_commission_percent = $promotion->affiliateUser->commissionPercent;
                $comIncome->affiliate_commission_amount = floor($comIncome->total_amount * $comIncome->affiliate_commission_percent / 100);

                $comIncome->teacher_commission_amount = floor(($comIncome->total_amount - $comIncome->affiliate_commission_amount) * $comIncome->teacher_commission_percent / 100);
                $comIncome->kyna_commission_amount = floor($comIncome->total_amount - $comIncome->teacher_commission_amount - $comIncome->affiliate_commission_amount);
                $comIncome->save();
            }
        }
    }

    /**
     * Find order is complete, activate but not calculate commission yet
     * @param null $method payment method
     */
    public function actionCalculateCommission($method = null)
    {
        $tblComIncome = CommissionIncome::tableName();
        $tblOrderDetail = OrderDetails::tableName();
        $i = 1;

        try {
            do {
                $query = Order::find()
                    ->alias('o')
                    ->leftJoin($tblComIncome . ' ci', 'ci.order_id = o.id')
                    ->leftJoin($tblOrderDetail . ' od', 'od.order_id = o.id')
                    ->where(['o.status' => Order::ORDER_STATUS_COMPLETE])
                    ->andWhere(['o.is_activated' => Order::BOOL_YES])
                    ->andWhere(['>=', 'o.order_date', 1475859600])// 08/10/2016 00:00:00
                    ->andWhere(['ci.id' => null])
                    ->andWhere(['IS NOT', 'od.id', null]);

                if ($method != null) {
                    $query->andWhere(['o.payment_method' => $method]);
                }
                $order = $query->one();

                if ($order != null) {
                    echo $i++ . PHP_EOL;
                    foreach ($order->details as $detail) {
                        Commission::calculate($order->id, $order->subTotal, $order->total, $order->totalCourseDiscount, $order->totalDiscount, $order->paymentFee, $detail->course_id, $detail->itemTotal, $order->affiliate_id, $detail->course_combo_id);
                    }

                    CommissionIncome::updateAll(['created_time' => $order->activation_date != null ? $order->activation_date : $order->order_date], ['order_id' => $order->id]);
                }
            } while ($order != null);
        } catch (Exception $ex) {
            echo $ex->getMessage() . PHP_EOL;
            die;
        }

        echo "--End--" . PHP_EOL;
    }

    /**
     * Recalculate order commission
     * @param integer $id order id
     */
    public function actionReCalculateCommission($id)
    {
        try {
            $order = Order::findOne($id);

            if ($order != null) {
                foreach ($order->details as $detail) {
                    Commission::calculate($order->id, $order->subTotal, $order->total, $order->totalCourseDiscount, $order->totalDiscount, $order->paymentFee, $detail->course_id, $detail->itemTotal, $order->affiliate_id, $detail->course_combo_id);
                }
            }
        } catch (Exception $ex) {
            echo $ex->getMessage() . PHP_EOL;
            die;
        }

        echo "--End--" . PHP_EOL;
    }

    public function actionFixWrongOrderAmount()
    {
        $sql = "SELECT order_id FROM commission_incomes where total_amount < 0 group by order_id;";

        $results = Yii::$app->db->createCommand($sql)->queryAll();

        foreach ($results as $item) {
            $orderId = $item['order_id'];

            $order = Order::findOne($orderId);
            $details = $order->details;
            $subTotal = 0;
            $totalCourseDiscount = 0;

            foreach ($details as $detail) {
                $subTotal += $detail->subTotal;
                $totalCourseDiscount += $detail->discountAmount;
            }

            if ($order->subTotal != $subTotal || $order->sub_total < $order->total_discount) {
                $order->sub_total = $subTotal;
                $order->total_discount = $order->subTotal - $order->total;
                if ($order->total_discount > $order->sub_total) {
                    $order->total_discount = $order->sub_total;
                }
                if (empty($order->payment_fee)) {
                    $order->payment_fee = $order->calPaymentFee();
                }

                if ($order->sub_total - $order->total_discount == $order->total) {
                    if ($order->save()) {
                        foreach ($order->details as $detail) {
                            Commission::calculate($order->id, $order->subTotal, $order->total, $order->totalCourseDiscount, $order->totalDiscount, $order->paymentFee, $detail->course_id, $detail->itemTotal, $order->affiliate_id, $detail->course_combo_id);
                        }
                    }
                } else {
                    var_dump($order->id, $order->sub_total, $order->total_discount);
                    echo('wrong amount');
                    die();
                }
            }

            echo "-- Done: {$orderId}" . PHP_EOL;
        }

        echo "-- END --" . PHP_EOL;
    }

    public function actionCheckMonkeyCommission()
    {
        $tblComIncome = CommissionIncome::tableName();
        $tblOrderDetail = OrderDetails::tableName();
        $i = 1;

        $notMonkeyIds = Course::find()->where(['!=', 'type', Course::TYPE_SPECIAL_JUNIOR_MONKEY])->select('id')->column();

        try {
            do {
                $query = Order::find()
                    ->alias('o')
                    ->leftJoin($tblComIncome . ' ci', 'ci.order_id = o.id')
                    ->leftJoin($tblOrderDetail . ' od', 'od.order_id = o.id')
                    ->where(['o.status' => Order::ORDER_STATUS_COMPLETE])
                    ->andWhere(['o.is_activated' => Order::BOOL_NO])
                    ->andWhere(['o.payment_method' => 'ghn'])
                    ->andWhere(['>=', 'o.order_date', 1475859600])// 08/10/2016 00:00:00
                    ->andWhere(['ci.id' => null])
                    ->andWhere(['NOT IN', 'od.course_id', $notMonkeyIds])
                    ->andWhere(['IS NOT', 'od.id', null]);

                $order = $query->one();

                if ($order != null) {
                    echo $i++ . "-" . $order->id . PHP_EOL;
                    Order::activate($order->id);
                }
            } while ($order != null);
        } catch (Exception $ex) {
            echo $ex->getMessage() . PHP_EOL;
            die;
        }

        echo "--End--" . PHP_EOL;
    }

    /**
     * Fix wrong shipping: remove shipping from order total amount when calculate commission
     */
    public function actionFixWrongShippingCommission()
    {
        $tblComIncome = CommissionIncome::tableName();
        $tblOrderDetail = OrderDetails::tableName();
        $i = 1;

        $query = Order::find()
            ->alias('o')
            ->leftJoin($tblComIncome . ' ci', 'ci.order_id = o.id')
            ->leftJoin($tblOrderDetail . ' od', 'od.order_id = o.id')
            ->andWhere(['o.payment_method' => 'ghn'])
            ->andWhere(['>', 'o.shipping_fee', 0])
            ->andWhere(['IS NOT', 'ci.id', null]);

        $orders = $query->all();

        foreach ($orders as $order) {
            echo $i++ . ' - ' . $order->id . PHP_EOL;
            
            foreach ($order->details as $detail) {
                Commission::calculate($order->id, $order->subTotal, $order->total, $order->totalCourseDiscount, $order->totalDiscount, $order->paymentFee, $detail->course_id, $detail->itemTotal, $order->affiliate_id, $detail->course_combo_id);
            }
        }

        echo "--End--" . PHP_EOL;
    }

    /**
     * apply payment fee for v3 orders (miss)
     */
    public function actionApplyPaymentFee()
    {
        $i = 1;
        try {
            do {
                $query = Order::find()
                    ->alias('o')
                    ->where(['o.payment_method' => ['epay', 'onepay_atm', 'onepay_cc', 'ghn']])
                    ->andWhere(['v2_id' => null])
                    ->andWhere(['>', 'total', 0])
                    ->andWhere(['>', 'order_date', 1472662800])
                    ->andWhere(['payment_fee' => null]);
                $order = $query->one();
                if ($order != null) {
                    $order->payment_fee = $order->calPaymentFee();
                    if (Order::updateAll(['payment_fee' => $order->payment_fee], ['id' => $order->id])) {
                        if (CommissionIncome::find()->where(['order_id' => $order->id])->exists()) {
                            foreach ($order->details as $detail) {
                                Commission::calculate($order->id, $order->subTotal, $order->total, $order->totalCourseDiscount, $order->totalDiscount, $order->paymentFee, $detail->course_id, $detail->itemTotal, $order->affiliate_id, $detail->course_combo_id);
                            }
                        }
                    }
                    echo $i++ . ' - ' . $order->id . PHP_EOL;
                }
            } while ($order != null);
        } catch (Exception $ex) {
            echo $ex->getMessage() . PHP_EOL;
            die;
        }
        echo "--End--" . PHP_EOL;
    }

    /**
     * Some orders is lost affiliate because telesale process is not in affilite user or category is null => set aff and recal commission
     * @param bool $beforeOct before or after 01/10/2016
     */
    public function actionFixLostTelesaleAffiliate($beforeOct = false)
    {
        $i = 1;
        try {
            do {
                $query = Order::find()
                    ->alias('o')
                    ->where(['o.payment_method' => ['ghn', 'in-store', 'bank-transfer']])
                    ->andWhere('affiliate_id IS NULL OR affiliate_id = 0')
                    ->andWhere(['>', 'operator_id', 0])
                    ->andWhere([$beforeOct ? '<' : '>=', 'order_date', 1475254800]) // >= 01/10/2016
                    ->andWhere(['is_done_telesale_process' => Order::BOOL_YES]);

                $order = $query->one();
                if ($order != null) {
                    $order->affiliate_id = $order->operator_id;

                    if (Order::updateAll(['affiliate_id' => $order->operator_id], ['id' => $order->id])) {
                        if (CommissionIncome::find()->where(['order_id' => $order->id])->exists()) {
                            foreach ($order->details as $detail) {
                                Commission::calculate($order->id, $order->subTotal, $order->total, $order->totalCourseDiscount, $order->totalDiscount, $order->paymentFee, $detail->course_id, $detail->itemTotal, $order->affiliate_id, $detail->course_combo_id);
                            }
                        }
                    }
                    echo $i++ . ' - ' . $order->id . PHP_EOL;
                }
            } while ($order != null);
        } catch (Exception $ex) {
            echo $ex->getMessage() . PHP_EOL;
            die;
        }
        echo "--End--" . PHP_EOL;
    }

    /**
     * Update commission percent for Teacher Incomes
     */
    public function actionUpdateTeacherCommissionPercent()
    {
        // course => percent configs
        $params = Yii::$app->params['update-teacher-commission'];

        foreach ($params as $courseId => $value) {
            $startTime = 100000000000000000;

            if (is_array($value)) {
                $percent = $value['percent'];
                if (isset($value['startTime'])) {
                    $startTime = $value['startTime'];
                }
            } else {
                $percent = $value;
            }

            $query = CommissionIncome::find();
            $query->where(['>=', 'created_time', $startTime]);
            $query->andWhere(['!=', 'teacher_commission_percent', $percent]);
            $query->andWhere(['course_id' => $courseId]);

            $comIncomes = $query->all();
            foreach ($comIncomes as $comIncome) {
                $comIncome->teacher_commission_amount = floor($comIncome->teacher_commission_amount * $percent / $comIncome->teacher_commission_percent);
                $comIncome->teacher_commission_percent = $percent;
                $comIncome->kyna_commission_amount = $comIncome->total_amount - $comIncome->affiliate_commission_amount - $comIncome->teacher_commission_amount;
                if (!$comIncome->save()) {
                    var_dump($comIncome->errors);die;
                }

                echo $comIncome->id . PHP_EOL;
            }
        }

        die('--DONE--' . PHP_EOL);
    }

    /**
     * Update Transaction/Order and export result
     */
    public function actionUpdateGhn($email = 'trang.nguyen@kyna.vn')
    {
        $returnData = [];
        $transactionCode = Ghn::find()->all();
        foreach ($transactionCode as $ghnCode) {
            $transaction = PaymentTransaction::findOne(['transaction_code' => $ghnCode->code]);
            if (!empty($transaction)) {
                $transaction->is_call = PaymentTransaction::BOOL_YES;
                $transaction->status = PaymentTransaction::BOOL_YES;
                $transaction->save(false);
                // update Order completed
                $order = Order::find()->filterWhere([
                    'id' => $transaction->order_id,
                    'status' => [
                        Order::ORDER_STATUS_NEW,
                        Order::ORDER_STATUS_PENDING_PAYMENT,
                        Order::ORDER_STATUS_PENDING_CONTACT,
                        Order::ORDER_STATUS_DELIVERING,
                        Order::ORDER_STATUS_IN_COMPLETE
                    ]
                ])->one();
                if (!empty($order) && Order::complete($transaction->order_id, $transaction->transaction_code, false, Order::SCENARIO_COMPLETE_COD)) {
                    array_push($returnData, [
                        'order_id' => $transaction->order_id,
                        'transaction_code' => $ghnCode->code,
                        'status' => 'Cập nhật thành công'
                    ]);
                } else {
                    array_push($returnData, [
                        'order_id' => $transaction->order_id,
                        'transaction_code' => $ghnCode->code,
                        'status' => 'Order không thể cập nhật'
                    ]);
                }
            } else {
                array_push($returnData, [
                    'order_id' => isset($transaction->order_id) ? $transaction->order_id : null,
                    'transaction_code' => $ghnCode->code,
                    'status' => 'Không tìm thấy transaction'
                ]);
            }
        }

        $this->_exportGhnResult($returnData, $email);
    }

    private function _exportGhnResult($returnData, $email)
    {
        $date = date('c');
        $startTime = time();

        // format data
        $data['header'] = [
            'order_id' => 'Order Kyna',
            'transaction_code' => 'Ghn Code',
            'status' => 'Kết quả'
        ];
        $data['content'] = $returnData;

        // render Excel
        $excelRunner = new ExportExcel();
        $fileName = 'ghn_code_'.date('Y-m-d_H-i', time()) . '.xls';
        $excelRunner->renderData($data, $email, $fileName);

        /*
         * Log executed time
         */
        $endTime = time();
        $executedTime = $endTime - $startTime;
        echo "Date: {$date}"
            ."\n Email: {$email}"
            ."\n Executed Time: {$executedTime}"
        ;

        echo "\n Done \n";
    }

    public function actionFixExpireCommission()
    {
        $sql = "
            select c.id as `course_id`, date(from_unixtime(cc.apply_to_date)) as `expire_date`, cc.commission_percent as `percent` from courses c
            left join course_commission_types cct ON cct.id = c.course_commission_type_id
            left join course_commissions cc ON cc.course_id = c.id
            left join commission_incomes ci ON ci.course_id = c.id
            WHERE cct.is_required_expiration_date = 1 
                    and cc.apply_to_date is not null 
                and cc.apply_to_date > 0 
                and date(from_unixtime(ci.created_time)) = date(from_unixtime(cc.apply_to_date))
            GROUP BY c.id, date(from_unixtime(cc.apply_to_date)), cc.commission_percent
            having sum(ci.teacher_commission_percent) = 0
        ";

        $results = Yii::$app->db->createCommand($sql)->queryAll();

        foreach ($results as $item) {
            $courseId = $item['course_id'];
            $expireDate = $item['expire_date'];

            $percent = $item['percent'];

            $query = CommissionIncome::find();
            $query->where(['=', 'date(from_unixtime(created_time))', $expireDate]);
            $query->andWhere(['!=', 'teacher_commission_percent', $percent]);
            $query->andWhere(['course_id' => $courseId]);

            $comIncomes = $query->all();
            foreach ($comIncomes as $comIncome) {
                if ($comIncome->teacher_commission_percent == 0) {
                    if ($comIncome->commissionCalculation !== null) {
                        $comIncome->teacher_commission_amount = floor($comIncome->total_amount * $comIncome->commissionCalculation->instructor_percent * $percent / 10000);
                    } else {
                        $comIncome->teacher_commission_amount = floor($comIncome->total_amount * (100 - $comIncome->affiliate_commission_percent) * $percent / 10000);
                    }
                } else {
                    $comIncome->teacher_commission_amount = floor($comIncome->teacher_commission_amount * $percent / $comIncome->teacher_commission_percent);
                }
                $comIncome->teacher_commission_percent = $percent;

                $comIncome->kyna_commission_amount = $comIncome->total_amount - $comIncome->affiliate_commission_amount - $comIncome->teacher_commission_amount;
                if (!$comIncome->save()) {
                    var_dump($comIncome->errors);die;
                }

                echo $comIncome->id . PHP_EOL;
            }

            echo "-- Done: {$courseId}" . PHP_EOL;
        }

        echo "-- END --" . PHP_EOL;
    }

    public function actionUpdateSpecialOrder()
    {
        $settings = ArrayHelper::map(Setting::find()->all(), 'key', 'value');
        $specialCourses = !empty($settings['special_courses']) ? $settings['special_courses'] : '';
        $specialCourses = explode(',', $specialCourses);
        $specialCourseTypes = [
            Course::TYPE_SOFTWARE,
        ];

        $results = Order::find()
            ->alias('o')
            ->join('INNER JOIN', OrderDetails::tableName(), OrderDetails::tableName() . '.order_id = o.id')
            ->join('INNER JOIN', Course::tableName(), Course::tableName() . '.id = ' . OrderDetails::tableName() . '.course_id')
            ->where([
                'o.status' => Order::ORDER_STATUS_COMPLETE,
                'o.payment_method' => 'ghn'
            ])
            ->andWhere('o.id NOT IN (SELECT ci.order_id FROM commission_incomes ci)')
            ->andFilterWhere([
                'OR',
                [Course::tableName() . '.type' => $specialCourseTypes],
                [Course::tableName() . '.id' => $specialCourses],
            ])
            ->groupBy(OrderDetails::tableName() . '.order_id')
            ->all();

        foreach ($results as $key => $order) {
            if ($order->isSpecial) {
                Order::activate($order->id);
                // update time at complete order date
                $order = Order::findOne($order->id);
                $actionComplete = $order->actionHistory(Order::EVENT_ORDER_COMPLETED);
                $actionModels = $actionComplete->getModels();
                if (!empty($actionModels)) {
                    $completedTime = $actionModels{0}->action_time;
                    $this->_updateCommissionIncome($order->id, $completedTime);
                } else {
                    $actionShipping = $order->actionHistory(Order::EVENT_ORDER_SENT_TO_SHIPPING);
                    $actionModels = $actionShipping->getModels();
                    if (!empty($actionModels)) {
                        $shippingTime = strtotime('+7 days', $actionModels{0}->action_time);
                        $this->_updateCommissionIncome($order->id, $shippingTime);
                    } else {
                        $orderUpdatedTime = $order->updated_time;
                        $this->_updateCommissionIncome($order->id, $orderUpdatedTime);
                    }
                }
                echo "-- {$key} - Done: {$order->id}" . PHP_EOL;
            }
        }

        echo "-- END --" . PHP_EOL;
    }

    private function _updateCommissionIncome($orderId, $time) {
        $order = Order::findOne($orderId);
        $order->activation_date = $time;
        $order->save(false);
        $comIncomes = CommissionIncome::findAll([
            'order_id' => $orderId
        ]);
        foreach ($comIncomes as $comIncome) {
            $comIncome->created_time = $time;
            $comIncome->save(false);
        }
    }

    public function actionUpdateDuplicateCommission() {
        $query = "
            DELETE c1
            FROM commission_incomes c1, commission_incomes c2
            WHERE (c1.fee < c2.fee OR (c1.fee = c2.fee AND c1.id > c2.id))
                AND c1.order_id = c2.order_id 
                AND c1.course_id = c2.course_id
        ";
        $results = Yii::$app->db->createCommand($query)->execute();
        echo "Removed {$results} record(s)";
    }

    public function actionFixV2WrongOrderSubtotal()
    {
        $i = 1;
        try {
            do {
                $query = Order::find()
                    ->alias('o')
                    ->orderBy('id desc')
                    ->where(['o.payment_method' => 'ghn'])
                    ->andWhere('o.sub_total != (select sum(unit_price) from order_details where order_id = o.id)');

                $order = $query->one();

                if ($order != null) {
                    $details = $order->details;
                    $subTotal = 0;
                    $totalCourseDiscount = 0;

                    foreach ($details as $detail) {
                        $subTotal += $detail->subTotal;
                        $totalCourseDiscount += $detail->discountAmount;
                    }

                    if ($order->subTotal != $subTotal) {
                        $order->sub_total = $subTotal;
                        $order->total_discount = $order->subTotal - $order->total;
                        if ($order->total_discount > $order->sub_total) {
                            $order->total_discount = $order->sub_total;
                        }
                        if (empty($order->payment_fee)) {
                            $order->payment_fee = $order->calPaymentFee();
                        }

                        if ($order->sub_total - $order->total_discount == $order->total) {
                            if ($order->save()) {
                                $hasCommission = CommissionIncome::find()->where(['order_id' => $order->id])->exists();
                                if ($hasCommission) {
                                    foreach ($order->details as $detail) {
                                        Commission::calculate($order->id, $order->subTotal, $order->total, $order->totalCourseDiscount, $order->totalDiscount, $order->paymentFee, $detail->course_id, $detail->itemTotal, $order->affiliate_id, $detail->course_combo_id);
                                    }
                                }
                            }
                        } else {
                            var_dump($order->id, $order->sub_total, $order->total_discount);
                            echo('wrong amount');
                            die();
                        }
                    }

                    echo $i++ . ' - ' . $order->id . PHP_EOL;
                }
            } while ($order != null);
        } catch (Exception $ex) {
            echo $ex->getMessage() . PHP_EOL;
            die;
        }
        echo "--End--" . PHP_EOL;
    }

    public function actionRevertV2Commission()
    {
        $query = "
            DELETE ci FROM commission_incomes ci
            LEFT JOIN orders o ON o.id = ci.order_id
            WHERE o.payment_method = 'ghn'
            AND (o.v2_id IS NOT null OR o.v2_cod_id IS NOT null)
            AND ci.created_time >= UNIX_TIMESTAMP('2017-04-20 00:00:00');
        ";
        $results = Yii::$app->db->createCommand($query)->execute();
        echo "Removed {$results} record(s)";
    }

    public function actionSyncData()
    {
        $result = Yii::$app->db_backup->createCommand("select max(id) as lastId from orders;")->queryOne();
        $lastId = $result['lastId'];

        $newRecords = Yii::$app->db_new->createCommand("select * from orders where id > {$lastId}")->queryAll();
        foreach ($newRecords as $newRecord) {
            $order = new SyncOrder();
            $order->attributes = $newRecord;

            if ($order->user_id > 0) {
                $userData = Yii::$app->db_new->createCommand("select email from user where id = {$order->user_id}")->queryOne();
                $email = $userData['email'];
                $user = User::findOne(['email' => $email]);
                if ($user !== null) {
                    $order->user_id = $user->id;
                }
            }

            if ($order->save()) {
                $details = Yii::$app->db_new->createCommand("select * from order_details where order_id = {$newRecord['id']}")->queryAll();
                foreach ($details as $detail) {
                    $orderDetail = new OrderDetails();
                    $orderDetail->attributes = $detail;
                    $orderDetail->order_id = $order->id;
                    if (!$orderDetail->save()) {
                        var_dump($orderDetail->errors);
                    }
                }

                $actions = Yii::$app->db_new->createCommand("select * from order_actions where order_id = {$newRecord['id']}")->queryAll();
                foreach ($actions as $action) {
                    $orderAction = new OrderAction();
                    $orderAction->attributes = $action;
                    $orderAction->order_id = $order->id;
                    if (!$orderAction->save(false)) {
                        var_dump($orderAction->errors);
                    }
                }

                $incomes = Yii::$app->db_new->createCommand("select * from commission_incomes where order_id = {$newRecord['id']}")->queryAll();
                foreach ($incomes as $income) {
                    $commissionIncome = new CommissionIncome();
                    $commissionIncome->attributes = $income;
                    $commissionIncome->order_id = $order->id;
                    if (!$commissionIncome->save()) {
                        var_dump($commissionIncome->errors);
                    }
                }

                echo  $newRecord['id'] . " ---> " . $order->id . PHP_EOL;
            } else {
                var_dump($order->errors);
            }
        }
    }

    /**
     * Update Transaction Code if exist in Response
     */
    public function actionUpdateTransactionCode() {
        $transactions = PaymentTransaction::find()
            ->where([
                'status' => PaymentTransaction::BOOL_YES,
                'payment_method' => 'cod',
                'transaction_code' => ''
            ])
            ->andWhere('created_time > UNIX_TIMESTAMP("2017-06-16T09:58:00")')
            ->groupBy('order_id')
            ->all();
        echo "Start Update Transaction" . PHP_EOL;
        foreach ($transactions as $transaction) {
            if (isset($transaction->response_string)) {
                $responseString = urldecode($transaction->response_string);
                $response = [];
                parse_str($responseString, $response);
                if (!empty($response['OrderCode'])) {
                    $transaction->transaction_code = $response['OrderCode'];
                    $transaction->save(false);
                    echo "- Transaction: {$transaction->id}" . PHP_EOL;
                }
            }
        }
        echo "Done" . PHP_EOL;
    }
}
