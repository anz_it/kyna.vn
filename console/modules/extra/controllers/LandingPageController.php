<?php

namespace app\modules\extra\controllers;

use Yii;
use yii\console\Controller;
use kyna\page\models\Page;


/* 
 * LandingPageController class
 */
class LandingPageController extends Controller
{

    /**
     * @param $modelClass
     * @param $ids
     * @param bool $forceCreate
     */
    public function actionMigrateContent($ids = null)
    {
        if (!is_null($ids)) {
            $ids = explode(',', $ids);
            $pages = Page::find()->where(['id' => $ids])->all();
        } else {
            $pages = Page::find()->all();
        }
        foreach ($pages as $page) {
            $staticLink = Yii::$app->params['lp_media_link'];
            $fileName = "{$staticLink}/uploads/pages/{$page->slug}/index.php";
            $fileHeaders = @get_headers($fileName);
            if($fileHeaders[0] == 'HTTP/1.1 404 Not Found') {
                continue;
            }
            $content = file_get_contents($fileName);
            $content = str_replace('"/pages', '"<?= Yii::$app->params["lp_media_link"] ?>/uploads/pages', $content);
            $content = str_replace("= '/pages", '= Yii::$app->params["lp_media_link"] . \'/uploads/pages', $content);
            $content = str_replace('($_SERVER[\'DOCUMENT_ROOT\'].\'', ' Yii::getAlias(\'@media', $content);
            $page->content = $content;
            $page->save(false);
            echo "Migrated: {$page->id} - {$page->slug} \n";
        }
        echo "Finished \n";
    }
}