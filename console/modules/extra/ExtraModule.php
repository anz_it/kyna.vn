<?php

namespace app\modules\extra;

class ExtraModule extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\extra\controllers';

    public function init()
    {
        parent::init();
        // custom initialization code goes here
    }
}
