<?php

namespace app\modules\ghn\controllers;

use yii;

use kyna\order\models\Order;
use kyna\payment\lib\ghn\Ghn;
use kyna\payment\models\PaymentTransaction;

use app\modules\ghn\components\Controller;

class GetOrderInforController extends Controller
{

    public function actionRun($limit = null)
    {
        if ($limit == null) {
            $limit = Yii::$app->params['pageSize'];
        }

        $transactions = PaymentTransaction::find()->where([
            'payment_method' => 'cod',
            'is_call' => PaymentTransaction::BOOL_NO
        ])->orWhere([
            'payment_method' => 'cod',
            'is_call' => null,
            'status' => PaymentTransaction::BOOL_YES
        ])->orWhere([
            'payment_method' => 'cod',
            'is_call' => PaymentTransaction::BOOL_YES,
            'status' => -1
        ])->andWhere([
            'NOT', ['transaction_code' => null]
        ])->all();

        if (!empty($transactions)) {
            $ghn_class = new Ghn();
            set_time_limit(0);
            foreach ($transactions as $key => $transaction) {

                $result = $ghn_class->query($transaction->transaction_code);
                try {
                    if ($result && $result['error'] == null) {
                        switch ($result['status']) {
                            case 'WaitingToFinish':
                                if (!empty($result['CoDAmount'])) {
                                    $this->_completeTransaction($transaction->id);

                                    if (!empty($transaction->order_id)) {
                                        Yii::info("Complete order #{$transaction->order_id}", 'cod');
                                        Order::complete($transaction->order_id, $transaction->transaction_code);
                                    }
                                } else {
                                    Yii::info("Waiting To Finish order #{$transaction->order_id}", 'cod');
                                }
                                break;

                            case 'Return':
                                $this->_completeTransaction($transaction->id);

                                if (!empty($transaction->order_id)) {
                                    Yii::info("Waiting return order #{$transaction->order_id}", 'cod');
                                    Order::makeCodWaitingForReturn($transaction->order_id);
                                }

                                break;

                            case 'Cancel':
                                $this->_completeTransaction($transaction->id);

                                if (!empty($transaction->order_id)) {
                                    Yii::info("Payment failed order #{$transaction->order_id}", 'cod');
                                    Order::makeFailedPayment($transaction->order_id);
                                }

                                break;

                            default:
                                break;
                        }
                    } else {
                        Yii::info("Transaction #{$transaction->id} has error: '{$result['error']}'", 'cod');
                    }
                } catch (yii\base\Exception $ex) {
                    Yii::info("Transaction #{$transaction->id} has exception: '{$ex->getMessage()}'", 'cod');
                }
            }
        } else {
            Yii::info('Transaction cannot found', 'cod');
        }
    }

    private function _completeTransaction($id)
    {
        $trans = PaymentTransaction::findOne($id);
        $trans->is_call = PaymentTransaction::BOOL_YES;
        $trans->status = PaymentTransaction::BOOL_YES;

        return $trans->save(false);
    }
}

