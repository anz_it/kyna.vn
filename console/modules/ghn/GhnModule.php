<?php

namespace app\modules\ghn;

class GhnModule extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\ghn\controllers';

    public function init()
    {
        parent::init();
        // custom initialization code goes here
    }
}
