<?php


namespace app\modules\promotion\controllers;

use kyna\promotion\models\Promotion;
use kyna\promotion\models\PromotionCourse;
use Yii;
use yii\console\Controller;

class WorkerController extends Controller
{
    public function actionAdd()
    {
        $promotion_id = false;
        while (!$promotion_id){
            $promotion_id = readline("Nhập khóa chính ID promotion voucher: ");
            $promotion = Promotion::findOne(['id'=>$promotion_id,'status'=>1]);
            if(empty($promotion) || is_null($promotion)){
                $promotion_id = false;
                echo "\033[31m Error : ID Promotion voucher này không tồn tại trong hệ thống \033[0m \n";
            }
        }

        $listIdCourses = FALSE;
        while (!$listIdCourses){
            $listIdCourses = readline("Bạn nhập danh sách Id khóa học cách nhau bởi dấu phẩy: ");
        }

        $listIdCourses = explode(',',$listIdCourses);
        foreach ($listIdCourses as $key => $course){
            $promotionCourse = PromotionCourse::findOne(['course_id'=>$course,'promotion_id'=>$promotion_id]);
            if(!$promotionCourse){
                $promotionCourse = new PromotionCourse();
            }
            $promotionCourse->setCourseId($course);
            $promotionCourse->setPromotionId($promotion_id);
            $promotionCourse->save(false);
        }
        print_r(PromotionCourse::find()->where(['promotion_id'=>$promotion_id])->asArray()->all());
        print_r('OK');die;
    }

    public function actionRemove(){
        $promotion_id = false;
        while (!$promotion_id){
            $promotion_id = readline("Nhập khóa chính ID promotion voucher: ");
            $promotion = Promotion::findOne(['id'=>$promotion_id,'status'=>1]);
            if(empty($promotion) || is_null($promotion)){
                $promotion_id = false;
                echo "\033[31m Error : ID Promotion voucher này không tồn tại trong hệ thống \033[0m \n";
            }
        }

        $listIdCourses = false;
        while (!$listIdCourses){
            $listIdCourses = readline("Bạn nhập danh sách Id khóa học cách nhau bởi dấu phẩy: ");
        }

        $listIdCourses = explode(',',$listIdCourses);
        foreach ($listIdCourses as $key => $course){
            $promotionCourse = PromotionCourse::findOne(['course_id'=>$course,'promotion_id'=>$promotion_id]);
            if($promotionCourse){
                $promotionCourse->delete();
            }
        }
        print_r('remove OK');
    }
}