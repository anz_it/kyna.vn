<?php

namespace app\modules\promotion;

class PromotionModule extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\promotion\controllers';

    public function init()
    {
        parent::init();
        // custom initialization code goes here
    }
}
