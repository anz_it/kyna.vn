<?php

namespace app\modules\seo\controllers;

use Yii;
use yii\console\Controller;
use yii\helpers\Url;
use app\modules\course\models\Category;
use app\modules\course\models\Course;
use kyna\tag\models\Tag;
use kyna\page\models\Page;
use kyna\seo\helpers\SitemapXmlHelper;
use kyna\course\models\Teacher;
use kyna\user\models\UserMeta;

class SitemapXmlController extends Controller {
    public $models;
    public $index = false;

    public function options($actionId) {
        return ['models', 'index'];
    }

    public function optionAliases() {
        return [
            'm' => 'models',
            'i' => 'index',
        ];
    }

    public function init() {
        parent::init();
        Yii::setAlias('@web', 'https://kyna.vn');
        Yii::setAlias('@webroot', '@root/frontend/web');
    }

    public function actionIndex() {
        $mArr = explode(',', $this->models);
        $options = SitemapXmlHelper::getSiteMapSettings();

        // Auto mod, get model list from db,
        if (in_array('auto', $mArr)) {
            $mArr = [];
            foreach ($options as $key => $value) {
                $mArr[] = $key;
            }
        }

        if (in_array('categories', $mArr) || in_array('all', $mArr)) {
            $categories = Category::find()->where([
                'status' => Category::STATUS_ACTIVE,
                'is_deleted' => false,
            ])->orderBy([
                'parent_id' => SORT_ASC,
                'order' => SORT_ASC,
                'name' => SORT_ASC,
            ])->all();
            foreach($categories as $category) {
                echo $category->url.PHP_EOL;
            }
//            echo 'categories: '.SitemapXmlHelper::generateFromModels($categories, date('c'), 'daily', 0.8).PHP_EOL;
            echo 'categories: '.SitemapXmlHelper::generateFromModels(
                    $categories,
                    date('c'),
                    isset($options['categories']['changefreg'])? SitemapXmlHelper::CHANGE_FREQS[$options['categories']['changefreg']] : 'daily',
                    isset($options['categories']['priority'])? $options['categories']['priority'] : 0.8
                ).PHP_EOL;

        }
        if (in_array('courses', $mArr) || in_array('all', $mArr)) {
            $courses = Course::find()->where([
                'status' => Course::STATUS_ACTIVE,
                'is_deleted' => false,
                'type' => Course::TYPE_VIDEO,
            ])->orderBy(['created_time' => 'desc'])->all();
            echo 'courses: '.SitemapXmlHelper::generateFromModels(
                $courses,
                date('c'),
                isset($options['courses']['changefreg'])? SitemapXmlHelper::CHANGE_FREQS[$options['courses']['changefreg']] : 'daily',
                isset($options['courses']['priority'])? $options['courses']['priority'] : 1
                ).PHP_EOL;
        }
        if (in_array('tags', $mArr) || in_array('all', $mArr)) {
            $tags = Tag::find()->where([
                'status' => Tag::STATUS_ACTIVE,
                'is_deleted' => false,
                'seo_sitemap' => true,
                'seo_robot_index' => Tag::SEO_ROBOT_INDEX,
                'seo_robot_follow' => Tag::SEO_ROBOT_FOLLOW,

            ])->all();
//            echo 'tags: '.SitemapXmlHelper::generateFromModels($tags, date('c'), 'daily', 1).PHP_EOL;
            echo 'tags: '.SitemapXmlHelper::generateFromModels(
                    $tags,
                    date('c'),
                    isset($options['tags']['changefreg'])? SitemapXmlHelper::CHANGE_FREQS[$options['tags']['changefreg']] : 'daily',
                    isset($options['tags']['priority'])? $options['tags']['priority'] : 1
                ).PHP_EOL;
        }
        if (in_array('landing_pages', $mArr) || in_array('all', $mArr)) {
            $landingPages = Page::find()->where([
                'status' => Page::STATUS_ACTIVE,
                'seo_is_sitemap' => true,
            ])->all();
//            echo 'landing-page: '.SitemapXmlHelper::generateFromModels($landingPages, date('c'), 'daily', 1).PHP_EOL;
            echo 'landing_pages: '.SitemapXmlHelper::generateFromModels(
                    $landingPages,
                    date('c'),
                    isset($options['landing_pages']['changefreg'])? SitemapXmlHelper::CHANGE_FREQS[$options['landing_pages']['changefreg']] : 'daily',
                    isset($options['landing_pages']['priority'])? $options['landing_pages']['priority'] : 1
                ).PHP_EOL;
        }
        if (in_array('teacher', $mArr) || in_array('all', $mArr)) {
            $userMetaTblName = UserMeta::tableName();
            $teacherTblName = Teacher::tableName();
            $teachers = Teacher::find()
                ->joinWith('authAssignments')
                ->join('LEFT JOIN', $userMetaTblName, "{$userMetaTblName}.user_id = {$teacherTblName}.id AND {$userMetaTblName}.key = 'seo_sitemap'")
                ->join('INNER JOIN', $userMetaTblName . ' m1', "m1.user_id = {$teacherTblName}.id AND m1.key = 'slug'")
                ->andWhere(['auth_assignment.item_name' => 'Teacher'])
                ->andWhere([
                    "$teacherTblName.status" => Teacher::STATUS_ACTIVE,
                    "$teacherTblName.is_deleted" => UserMeta::BOOL_NO
                ])
                ->andWhere("{$userMetaTblName}.id IS NULL OR {$userMetaTblName}.value = 'always'")
                ->all();
            echo 'teacher: '.SitemapXmlHelper::generateFromModels(
                    $teachers,
                    date('c'),
                    isset($options['teacher']['changefreg'])? SitemapXmlHelper::CHANGE_FREQS[$options['teacher']['changefreg']] : 'monthly',
                    isset($options['teacher']['priority'])? $options['teacher']['priority'] : 0.5
                ).PHP_EOL;
        }

        if ($this->index == true) {
            echo 'index: '.SitemapXmlHelper::generateIndex($mArr).PHP_EOL;
        }
    }
}
