<?php

namespace app\modules\seo;

class SeoModule extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\seo\controllers';

    public function init()
    {
        parent::init();
        // custom initialization code goes here
    }
}
