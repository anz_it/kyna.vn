<?php

/**
 * Created by PhpStorm.
 * User: ngunp
 * Date: 7/10/2017
 * Time: 10:40 AM
 */

namespace app\modules\recommendation\controllers;

use Yii;
use yii\console\Controller;

use common\helpers\NotificationHelper;
use common\recommendation\Recommendation;
use yii\helpers\Json;

class WorkerController extends Controller
{
    public function actionCoursesHotScore()
    {
        var_dump(Recommendation::updateCourseHotScore());

    }

    public function actionCoursesSellScore()
    {
        var_dump(Recommendation::updateCourseSellScore());
    }

    public function actionCategoriesSellScore()
    {
        var_dump(Recommendation::updateCategorySellScore());
    }
    public function actionCombo()
    {
        var_dump(Recommendation::updateRecommendationsCombo());
    }
}