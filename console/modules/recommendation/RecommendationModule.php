<?php

/**
 * Created by PhpStorm.
 * User: ngunp
 * Date: 7/10/2017
 * Time: 10:38 AM
 */
namespace app\modules\recommendation;

class RecommendationModule extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\recommendation\controllers';

    public function init()
    {
        parent::init();
        // custom initialization code goes here
    }
}