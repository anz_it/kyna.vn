<?php

namespace app\modules\taamkru\controllers;

use kyna\taamkru\lib\TaamkruCode;
use kyna\taamkru\models\Transaction;
use yii;

class CodeController extends yii\console\Controller
{

    public function actionUpdateStatus()
    {
        $transactions = Transaction::find()->orderBy('id DESC')->all();
        $date = date('c');
        echo "Date: {$date}\n";
        foreach ($transactions as $transaction) {
            // call API update code status
            $caller = new TaamkruCode();
            $result = $caller->update($transaction->code);
            if (isset($result['used']) && is_integer($result['used'])) {
                $transaction->num_used = intval($result['used']);
                $transaction->save();
            } else {
                echo "- Code Fail: {$transaction->code}\n";
            }
        }
    }
}

