<?php

namespace app\modules\import\controllers;

use common\components\ExportExcel;
use kyna\partner\models\Code;
use kyna\partner\models\Retailer;
use yii;
use yii\console\Controller;

class PartnerCodeController extends Controller
{

    /**
     * @param $filePath
     * @param $email
     * Get from $dataProvider->query->createCommand()->getRawSql()
     */
    public function actionImport($filePath, $email)
    {
        $date = date('c');
        $startTime = time();
        // Get result from query
        $results = $this->_process($filePath);

        // format data
        $data['header'] = $this->_getGridColumns();
        $data['content'] = $this->_formatContentRows($results);

        // render Excel
        $excelRunner = new ExportExcel();
        $fileName = 'partner_code_'.date('Y-m-d_H-i', time()) . '.xls';
        $excelRunner->renderData($data, $email, $fileName);

        // remove temple file
        $this->_removeFile($filePath);

        /*
         * Log executed time
         */
        $endTime = time();
        $executedTime = $endTime - $startTime;
        echo "Date: {$date}"
            ."\n SQL: {$filePath}"
            ."\n Email: {$email}"
            ."\n Executed Time: {$executedTime}"
        ;

        echo "\n Done \n";
    }

    /**
     * Format Content Row
     * @param $results
     * @return array
     */
    private function _formatContentRows($results)
    {
        $returnData = [];
        if (count($results) > 0) {
            $row = 2;
            foreach ($results as $item) {
                $rowData = null;
                foreach ($this->_getGridColumns() as $column => $label) {
                    $value = null;
                    switch ($column) {
                        case 'serial';
                            $value = $item['serial'];
                            break;
                        case 'retailer_id':
                            $value = $item['retailer_id'];
                            break;
                        case 'result';
                            $value = $item['result'];
                            break;
                        default:
                            if (isset($item[$column])) {
                                $value = $item[$column];
                            }
                            break;
                    }
                    $rowData[$column] = $value;
                }
                array_push($returnData, $rowData);
                $row ++;
            }
            unset($rowData);
        }
        return $returnData;
    }

    /**
     * Get result from query
     * @param $filePath
     * @return array
     */
    private function _process ($filePath) {
        try {
            $inputFileType = \PHPExcel_IOFactory::identify($filePath);
            $objReader = \PHPExcel_IOFactory::createReader($inputFileType);
            $objExcel = $objReader->load($filePath);
        } catch (yii\console\Exception $ex) {
            return false;
        }

        $sheet = $objExcel->getSheet(0);
        $highestRow = $sheet->getHighestRow();
        $highestColumn = $sheet->getHighestColumn();

        $returnData = [];
        for ($row = 2; $row <= $highestRow; $row++) {
            $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, NULL);
            if ($result = $this->saveExcelRow($rowData)) {
                array_push($returnData, $result);
            }
        }
        return $returnData;
    }

    private function saveExcelRow($rowData)
    {
        $serial = trim($rowData[0][0]);
        $retailerID = trim($rowData[0][1]);
        if (empty($serial) && empty($retailerID)) {
            return false;
        }
        $code = Code::findOne(['serial' => $serial]);
        $result = [
            'serial' => $serial,
            'retailer_id' => $retailerID,
            'result' => ''
        ];
        if (!empty($code)) {
            $retailer = Retailer::findOne([
                'id' => $retailerID,
                'partner_id' => $code->partner_id,
                'status' => Retailer::STATUS_ACTIVE
            ]);
            switch ($code->status) {
                case Code::CODE_STATUS_IN_STORE:
                    if (intval($retailerID) != 0 && empty($retailer)) {
                        $result['result'] = 'Retailer không tồn tại';
                    } else {
                        if (intval($retailerID) == 0) {
                            $code->retailer_id = null;
                            $code->status = Code::CODE_STATUS_IN_STORE;
                        } else {
                            $code->retailer_id = intval($retailerID);
                            $code->status = Code::CODE_STATUS_IN_ACTIVE;
                        }
                        $result['result'] = 'Đã cập nhật';
                    }
                    break;
                case Code::CODE_STATUS_IN_ACTIVE:
                    if (!is_null($code->retailer_id)) {
                        if (intval($retailerID) != 0 && empty($retailer)) {
                            $result['result'] = 'Retailer không tồn tại';
                        } else {
                            if (intval($retailerID) == 0) {
                                $code->retailer_id = null;
                                $code->status = Code::CODE_STATUS_IN_STORE;
                            } else {
                                $code->retailer_id = intval($retailerID);
                                $code->status = Code::CODE_STATUS_IN_ACTIVE;
                            }
                            $result['result'] = 'Đã cập nhật';
                        }
                    } else {
                        $result['result'] = 'Không thể cập nhật khi thẻ đang giao COD';
                    }
                    break;
                default:
                    $result['result'] = 'Không thể cập nhật thẻ ở trạng thái ' . $code->getStatusText();
                    break;
            }
            $code->save();
        } else {
            $result['result'] = "Serial {$serial} không tồn tại";
        }
        return $result;
    }

    /**
     * Define columns
     * @return array
     */
    private function _getGridColumns()
    {
        $gridColumns = [
            'serial' => 'Serial Code',
            'retailer_id' => 'Retailer ID',
            'result' => 'Kết quả'
        ];
        return $gridColumns;
    }

    private function _removeFile($filePath)
    {
        unlink($filePath);
    }
}