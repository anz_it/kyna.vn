<?php

namespace app\modules\course\models;

use Yii;
use yii\helpers\Url;
/*
 * This is override active record class of table `categories` for Frontend usage
 */
class Category extends \kyna\course\models\Category
{
    private $_categoryUrlSegments = [
        'module' => 'course',
        'controller' => 'default',
        'action' => 'index',
    ];

    public function getUrl($withId = false, $scheme = false)
    {
        $url = '/';
        if ($this->_categoryUrlSegments['module']) {
            $url .= $this->_categoryUrlSegments['module'].'/';
        }

        $url .= ($this->_categoryUrlSegments['controller']) ? $this->_categoryUrlSegments['controller'].'/' : 'default/';
        $url .= ($this->_categoryUrlSegments['action']) ? $this->_categoryUrlSegments['action'].'/' : 'index';

        $params[] = $url;

        if ($this->slug) {
            $params['slug'] = $this->slug;
        }
        else {
            // force id if slug not found
            $withId = true;
            $params['slug'] = 'danh-muc';
        }

        if ($withId) {
            $params['catId'] = $this->id;
        }

        if ($this->parent_id > 0 and $parent = $this->parent->slug) {
            $params['parent'] = $parent;
        }

        return Url::toRoute($params, $scheme);
        //return \yii\helpers\Url::toRoute(['/course/default/index', 'catId' => $this->id, 'slug' => $this->slug]);
    }
}
