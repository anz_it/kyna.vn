<?php

namespace app\modules\course\models;

use Yii;
use yii\helpers\Url;
/*
 * This is override active record class of table `categories` for Frontend usage
 */
class Course extends \kyna\course\models\Course
{
    private $_courseUrlSegments = [
        'module' => 'course',
        'controller' => 'default',
        'action' => 'view',
    ];

    public function getUrl($withId = false, $scheme = false)
    {
        $url = '/';
        if ($this->_courseUrlSegments['module']) {
            $url .= $this->_courseUrlSegments['module'].'/';
        }

        $url .= ($this->_courseUrlSegments['controller']) ? $this->_courseUrlSegments['controller'].'/' : 'default/';
        $url .= ($this->_courseUrlSegments['action']) ? $this->_courseUrlSegments['action'].'/' : 'index';

        $params[] = $url;

        if ($this->slug) {
            $params['slug'] = $this->slug;
        }
        else {
            // force id if slug not found
            $withId = true;
            $params['slug'] = 'khoa-hoc';
        }

        if ($withId) {
            $params['id'] = $this->id;
        }

        return Url::toRoute($params, $scheme);
    }
}
