<?php

namespace app\modules\course\controllers;

use Yii;
use yii\console\Controller;
use kyna\course\models\Course;

class ImageCrawlerController extends Controller {
    public $savedDir = '@upload/courses';
    public $baseUrl = '/uploads/courses';

    public function allowedMimeTypes() {
        return [
            'image/png' => '.png',
            'image/jpeg' => '.jpg',
        ];
    }

    public function init() {
        parent::init();
    }

    public function actionIndex() {
        $existedDirs = $this->_getExisted();
        $courses = Course::find()
            ->where(['not in','id',$existedDirs])
            ->all();

        foreach ($courses as $course) {
            echo 'Start download course #'.$course->id.PHP_EOL;

            if ($image_url = $this->_saveFile($course, 'image_url')) {
                $course->image_url = $image_url;
            }
            if ($video_cover_image_url = $this->_saveFile($course, 'video_cover_image_url')) {
                $course->video_cover_image_url = $video_cover_image_url;
            }

            if (($video_cover_image_url or $video_cover_image_url) and $course->save()) {
                echo 'Course #'.$course->id.' was saved with new data'.PHP_EOL.PHP_EOL;
            }
            else {

                echo 'Course #'.$course->id.' was not saved'.PHP_EOL.PHP_EOL;
            }
        }
    }

    private function _getExisted() {
        $dirPath = Yii::getAlias($this->savedDir);
        $dir = scandir($dirPath);
        $dir = array_diff($dir, ['.', '..']);
        $dir = array_filter($dir, function($app) use ($dirPath){
           $path = $dirPath.'/'.$app;
           return is_dir($path);
       });
       return array_values($dir);
    }

    // without file extension
    private function _getFileName($model, $attribute) {
        $dir = Yii::getAlias($this->savedDir.'/'.$model->id.'/img');
        if (!file_exists($dir) or !is_dir($dir)) {
            if (!mkdir($dir, 755, true)) {
                throw new Exception('Can not create image directory');
            }
        }

        return $dir.'/'.$attribute;
    }

    private function _saveFile($model, $attribute) {
        $url = $model->$attribute;

        if (empty($url)) {
            return false;
        }

        $fileName = $this->_getFileName($model, $attribute);

        echo 'Saving `'.$attribute.'` from: '.$model->$attribute.'...';
        $startTime = microtime(true);

        $ch = curl_init($url);

        $fp = fopen($fileName, 'wb');
        curl_setopt($ch, CURLOPT_FILE, $fp);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_exec($ch);
        $contentType = curl_getinfo($ch, CURLINFO_CONTENT_TYPE);
        curl_close($ch);
        fclose($fp);

        $parts = explode(';', $contentType);
        $mimeType = $parts[0];

        $allowedTypes = $this->allowedMimeTypes();
        $fileExt = array_key_exists($mimeType, $allowedTypes) ? $allowedTypes[$mimeType] : false;

        if ($fileExt) {
            $newFileName = $fileName.$fileExt;
            rename($fileName, $newFileName);

            $endTime = microtime(true);
            $duration = $endTime - $startTime;
            echo ' to: '.$newFileName.' --> SUCCESS in '.$duration.' seconds'.PHP_EOL;

            $savedDir = Yii::getAlias($this->savedDir);
            return str_replace($savedDir, $this->baseUrl, $newFileName);
        }
        else {
            unlink($fileName);
        }

        $endTime = microtime(true);
        $duration = $endTime - $startTime;
        echo ' --> FAILED in '.$duration.' seconds'.PHP_EOL;

        return false;
    }
}
