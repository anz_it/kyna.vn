<?php

namespace app\modules\course\controllers;

use Yii;
use yii\console\Controller;
use kyna\course\models\CourseLesson;

class VideoCrawlerController extends Controller {
    public function init() {
        parent::init();
    }

    public function actionIndex() {
        $lessons = CourseLesson::find()
            ->where('`video_link` like "rtmp%"')
            // ->andWhere(['type' => 'video'])
            //->limit(2)
            ->all();

        foreach ($lessons as $lesson) {
            $app = 'vod';
            $filenameWithPrefix = basename($lesson->video_link);
            $fileParts = explode(':', $filenameWithPrefix);
            $filename = $fileParts[1];

            $link = '/'.$app.'/'.$filename;

            echo 'Updating video link of lession #'.$lesson->id.PHP_EOL;
            echo 'from: '.$lesson->video_link.' to: '.$link.PHP_EOL;

            $lesson->video_link = $link;
            if ($lesson->type !== 'video') {
                $lesson->type = 'video';
            }

            if ($lesson->save()) {
                echo ' --> SUCCESS'.PHP_EOL.PHP_EOL;
            }
            else {
                echo ' --> FAILED'.PHP_EOL.PHP_EOL;
            }
        }
    }
}
