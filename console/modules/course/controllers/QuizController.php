<?php
/**
 */
namespace app\modules\course\controllers;

use yii;
use yii\console\Controller;
use kyna\course\models\Quiz;

class QuizController extends Controller {

    public function actionRun()
    {
        $quizes = Quiz::find()->where(['type' => null, 'is_updated' => [Quiz::BOOL_YES, Quiz::BOOL_NO]])->limit(1000)->all();
        foreach($quizes as $quiz) {
            $flag = false;
            if(!empty($quiz->details)){
                foreach($quiz->details as $detail){
                    $answer = $detail->quizQuestion->answers;
                    if(empty($answer)){
                        $flag = true;
                        break;
                    }
                }
                if($flag == true){
                    $quiz->type = Quiz::TYPE_WRITING;
                }else{
                    $quiz->type = Quiz::TYPE_ONE_CHOICE;
                }
                $quiz->is_updated = Quiz::BOOL_YES;
                $quiz->save(false);
            }else{
                $quiz->type = Quiz::TYPE_ONE_CHOICE;
                $quiz->is_updated = Quiz::BOOL_YES;
                $quiz->save(false);
                echo "Test \n";
            }
        }
        echo "Done \n";
    }
}