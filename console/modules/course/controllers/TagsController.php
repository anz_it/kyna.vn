<?php

namespace app\modules\course\controllers;

use kyna\course_combo\models\CourseCombo;
use yii;
use yii\console\Controller;
use kyna\tag\models\CourseTag;
use kyna\course\models\Course;

class TagsController extends Controller {

    /**
     * Remove all tags for combo
     */
    public function actionRemoveComboTags()
    {
        $comboType = Course::TYPE_COMBO;
        $query = "
            DELETE ct FROM course_tags ct
            INNER JOIN courses c ON c.id = ct.course_id
            WHERE c.type = {$comboType}
        ";
        $results = Yii::$app->db->createCommand($query)->execute();
        echo "Removed {$results} record(s)";
    }

    /**
     * Update tags for all combo
     */
    public function actionUpdateComboTags()
    {
        $comboCourses = CourseCombo::find()->all();
        foreach ($comboCourses as $comboCourse) {
            CourseCombo::updateTags($comboCourse->id, 'update');
            echo "Update tags for course: {$comboCourse->id} - {$comboCourse->name} \n";
        }
    }
}