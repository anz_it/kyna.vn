<?php

namespace app\modules\course\controllers;

use kyna\course\models\CourseOpinions;
use kyna\course\models\CourseScreenshot;
use kyna\course_combo\models\CourseCombo;
use yii;
use yii\console\Controller;
use kyna\tag\models\CourseTag;
use kyna\course\models\Course;

class ContentController extends Controller {

    /**
     * Update content
     */
    public function actionUpdate()
    {
        $courses = Yii::$app->dbcontent->createCommand("SELECT * FROM courses WHERE id < 928")->queryAll();
        echo "Start Update Course \n";
        foreach ($courses as $course) {
            $courseId = trim($course['id']);
            echo "{$courseId} - ";
            $kynaCourse = Course::findOne($courseId);
            if (!empty($kynaCourse)) {
                $kynaCourse->purchase_type = $course['purchase_type'];
                $kynaCourse->mac_app_link = $course['mac_app_link'];
                $kynaCourse->window_app_link = $course['window_app_link'];
                $kynaCourse->ios_app_link = $course['ios_app_link'];
                $kynaCourse->android_app_link = $course['android_app_link'];
                $kynaCourse->what_you_learn = $course['what_you_learn'];
                $kynaCourse->max_device = $course['max_device'];
                $kynaCourse->tags_list = $kynaCourse->tags;
                $kynaCourse->save(false);
                unset($kynaCourse);
                echo "Success \n";
            }
        }

        echo "Start Update Screenshot \n";
        $courseScreenshots = Yii::$app->dbcontent->createCommand("SELECT * FROM course_screenshots WHERE course_id < 928")->queryAll();
        foreach ($courseScreenshots as $screenshot) {
            echo "{$screenshot['id']} - ";
            $kynaScreenshot = new CourseScreenshot();
            $kynaScreenshot->course_id = $screenshot['course_id'];
            $kynaScreenshot->status = $screenshot['status'];
            $kynaScreenshot->description = $screenshot['description'];
            $kynaScreenshot->image_url = $screenshot['image_url'];
            $kynaScreenshot->save(false);
            echo "Success \n";
        }

        echo "Start Update Opinion \n";
        $courseOpinions = Yii::$app->dbcontent->createCommand("SELECT * FROM course_opinions WHERE course_id < 928")->queryAll();
        foreach ($courseOpinions as $opinion) {
            echo "{$opinion['id']} - ";
            $courseOpinions = new CourseOpinions();
            $courseOpinions->course_id = $opinion['course_id'];
            $courseOpinions->status = $opinion['status'];
            $courseOpinions->description = $opinion['description'];
            $courseOpinions->user_name = $opinion['user_name'];
            $courseOpinions->avatar_url = $opinion['avatar_url'];
            $courseOpinions->save(false);
            echo "Success \n";
        }
        echo "Done \n";
    }

    /**
     * Update content
     */
    public function actionUpdateNew()
    {
        $courses = Yii::$app->dbcontent->createCommand("SELECT * FROM courses WHERE id > 927")->queryAll();
        echo "Start Update Course > 927 \n";
        foreach ($courses as $course) {
            $courseSlug = $course['slug'];
            echo "{$courseSlug} - ";
            $kynaCourse = Course::findOne(['slug' => $courseSlug]);
            if (!empty($kynaCourse)) {
                $kynaCourse->purchase_type = $course['purchase_type'];
                $kynaCourse->mac_app_link = $course['mac_app_link'];
                $kynaCourse->window_app_link = $course['window_app_link'];
                $kynaCourse->ios_app_link = $course['ios_app_link'];
                $kynaCourse->android_app_link = $course['android_app_link'];
                $kynaCourse->what_you_learn = $course['what_you_learn'];
                $kynaCourse->max_device = $course['max_device'];
                $kynaCourse->tags_list = $kynaCourse->tags;
                $kynaCourse->save(false);
                unset($kynaCourse);
                echo "Success \n";
            }
        }
    }
}