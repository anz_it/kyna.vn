<?php
/**
 */
namespace app\modules\course\controllers;

use app\modules\sync\models\CourseLesson;
use common\helpers\ArrayHelper;
use kyna\user\models\search\UserCourseSearch;
use yii;
use yii\console\Controller;
use kyna\user\models\UserCourse;

class CourseController extends Controller {

    private $objPHPExcel;
    private $letters = [];
    private $email = '';

    /**
     * @param $sql
     * Get from $dataProvider->query->createCommand()->getRawSql()
     */
    public function actionExportStudent($id, $emailCondition, $email)
    {
        // Get result from query
        $searchModel = new UserCourseSearch();
        $searchModel->course_id = $id;
        $searchModel->userInfo = $emailCondition;
        $dataProvider = $searchModel->search(array());
        $results = $this->_queryResults($dataProvider->query->createCommand()->getRawSql());

        // Create new PHPExcel object
        $this->objPHPExcel = new \PHPExcel();

        // Init email to attach report
        $this->email = $email;

        // Set properties
        $this->objPHPExcel->getProperties()->setCreator("Kyna.vn");
        $this->objPHPExcel->getActiveSheet()->setTitle('Students');
        $this->objPHPExcel->setActiveSheetIndex(0);
        $objWriter = \PHPExcel_IOFactory::createWriter($this->objPHPExcel, 'Excel2007');

        // Set Excel Header
        $this->_setHeader();

        // Set Excel content
        $this->_setContentRows($results);

        // Save file temporary
        ob_start();
        $objWriter->save('php://output');
        $data = ob_get_clean();

        $fileName = $id . '_students_status' . time() . '.xls';
        $contentType = 'application/vnd.ms-excel; charset=utf-8';

        // Send mail with report attached
        $this->sendEmail($data, $fileName, $contentType);

        echo "Done \n";
    }

    /**
     * Sends an email to the specified email address using the information collected by this model.
     *
     * @param  string  $email the target email address
     * @return boolean whether the email was sent
     */
    public function sendEmail($data, $fileName, $contentType)
    {
        try {
            Yii::$app->mailer->compose()
                ->setTo($this->email)
                ->setFrom([\Yii::$app->params['supportEmail'] => \Yii::$app->name])
                ->setSubject('Order Report')
                ->setTextBody('Please view attached file')
                ->attachContent($data, ['fileName' => $fileName, 'contentType' => $contentType])
                ->send();
        }
        catch (\Swift_TransportException $e) {
            Yii::error($e->getMessage(), 'app');
        }
    }

    /**
     * Get result from query
     * @param $sql
     * @return array
     */
    private function _queryResults ($sql) {
        $connection = Yii::$app->getDb();
        $command = $connection->createCommand($sql);
        $results = $command->queryAll();
        return $results;
    }

    /**
     * Get Content Row
     * @param $results
     */
    private function _setContentRows($results)
    {
        if (count($results) > 0) {
            $row = 2;
            foreach ($results as $item) {
                $model = UserCourse::findOne($item['id']);
                $countColumn = 0;
                foreach ($this->_getGridColumns() as $column) {
                    $cell_name = $this->letters[$countColumn] . $row;
                    $countColumn++;
                    $value = null;
                    switch ($column['attribute']) {
                        case 'inc':
                            $value = $row - 1;
                            break;
                        case 'email':
                            $value = $model->student->email;
                            break;
                        case 'status':
                            // = current_section/total section
                            $currentSection = $model->current_section;
                            $sections = CourseLesson::find()->select('section_id')->where(['course_id' => $model->course_id])->asArray()->all();
                            $currentSecIndex = 0;
                            foreach ($sections as $key => $section) {
                                if (intval($section['section_id']) == intval($currentSection)) {
                                    $currentSecIndex = $key;
                                }
                            }
                            $value = round($currentSecIndex / count($sections) * 100);
                            break;
                        default:
                            if (isset($item[$column['attribute']])) {
                                $value = $item[$column['attribute']];
                            }
                            break;
                    }
                    $this->objPHPExcel->getActiveSheet()->SetCellValue($cell_name, $value);
                }
                $row++;
            }
        }
    }

    /**
     * set Header for excel file
     */
    private function _setHeader()
    {
        $this->letters = range('A', 'Z');
        $count = 0;
        $cell_name = "";
        $row = 1;
        foreach ($this->_getGridColumns() as $item) {
            $cell_name = $this->letters[$count] . $row;
            $count++;
            $value = $item['header'];
            $this->objPHPExcel->getActiveSheet()->SetCellValue($cell_name, $value);
            // Make bold cells
            $this->objPHPExcel->getActiveSheet()->getStyle($cell_name)->getFont()->setBold(true);
            // Set auto size
            $this->objPHPExcel->getActiveSheet()->getColumnDimension($this->letters[$count])->setAutoSize( true );
        }
    }

    /**
     * Define columns
     * @return array
     */
    private function _getGridColumns()
    {
        $gridColumns = [
            [
                'header' => '#',
                'attribute' => 'inc',
            ],
            [
                'header' => 'Email',
                'attribute' => 'email',
            ],
            [
                'header' => 'Study Status (%)',
                'attribute' => 'status',

            ],
        ];
        return $gridColumns;
    }
    public function actionAddKynaEnglish(){
        $query = new yii\db\Query();
        $query->select(['count(user_id) as total_course', 'user_id'])
            ->from('user_courses')
            ->andWhere(['course_id' => [1282, 1283, 1284, 1166, 1188, 1189]])
            ->groupBy(['user_id'])
            ->having('count(user_id) = 6')
            ->andWhere(['is_activated' => 1]);

        $results = $query->all();
        $userIds = ArrayHelper::getColumn($results, 'user_id');
        echo "Find Total ".count($userIds). " user\n";
        foreach ($userIds as $userId) {
           echo "User ID: ".$userId ."\n";
        }
        echo "Do you want to add english course to " .count($userIds). " user ? y/n?\n";
        $add = trim(fgets(STDIN));
        $total = 0;
        $users = [];
        if ($add == 'y') {
            foreach ($userIds as $userId) {
                $insert = UserCourse::addUserCourse($userId, 1323);
                if(!empty($insert)){
                    echo "User: ".$userId . " course added!\n";
                    $total++;
                    $users[] = $userId;
                }
            }
        echo "Total ".$total. " user  added english course!\n";
            $logFileDir = \Yii::getAlias('@console/runtime/logs/add_kyna_english_course_'.time().'.csv');
            $logFile = fopen($logFileDir, "w") or die("Unable to open log file!");
            fwrite($logFile, implode(",", $users));
            fclose($logFile);
            echo "Done!";
        }


    }
}