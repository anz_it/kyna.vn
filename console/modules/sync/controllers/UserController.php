<?php

namespace app\modules\sync\controllers;

use Yii;
use app\modules\sync\models\User;
use common\lib\CI_Encrypt;

/* 
 * UserController class to help synch User data from V2 to V3.
 */
class UserController extends \app\modules\sync\components\Controller
{
    
    public $table = 'users';
    
    protected function create($dataV2 = [])
    {
        $user = User::find()->andWhere(['v2_id' => $dataV2['id']])->one();
        $authManager = Yii::$app->authManager;
        
        if (is_null($user)) {
            $user = new User();

            $user->v2_id = $dataV2['id'];
            $user->username = $dataV2['username'];
            $user->email = $dataV2['email'];
            $user->password_hash = Yii::$app->security->generatePasswordHash($this->getPasswordFromHash($dataV2['password']));
            $user->created_at = date_create_from_format("Y-m-d H:i:s", trim($dataV2['registered_date']))->getTimestamp();

            if (!$user->save()) {
                if ($user->hasErrors('username') && !$user->hasErrors('email') && $dataV2['username'] != $dataV2['email']) {
                    $dataV2['username'] = $dataV2['email'];

                    return $this->create($dataV2);
                }
                // log errors
                var_dump($user->errors);
                Yii::error($user->errors, $this->table);
                return false;
            }

            // save user profile
            $profile = $user->profile;
            $profile->name = $dataV2['fullname'];
            $profile->public_email = $dataV2['email'];

            if (!$profile->save(false) || !$this->saveUserMeta($user, $dataV2)) {
                // log errors
                Yii::error($profile->errors, $this->table);
                return false;
            }
        } else {
            $user->email = $dataV2['email'];
            
            if (!empty($dataV2['registered_date']) && $dataV2['registered_date'] != '0000-00-00 00:00:00') {
                $user->created_at = date_create_from_format("Y-m-d H:i:s", trim($dataV2['registered_date']))->getTimestamp();
            }
            $user->save(false);
            
            $this->saveUserMeta($user, $dataV2);
            
            $authManager->revokeAll($user->id);
        }
        
        $roles = explode(',', $dataV2['roles']);
        foreach ($roles as $v2Role) {
            $v3Role = $this->mapRole($v2Role);
            if (is_null($v3Role)) {
                continue;
            }
            
            $roles = $authManager->getRolesByUser($user->id);
            if (!array_key_exists($v3Role, $roles)) {
                // assign role
                $authManager->assign($authManager->getRole($v3Role), $user->id);
            }
        }
        
        return $user;
    }
    
    public function mapRole($roleV2)
    {
        // map v2 roles to v3
        $mapRoles = [
            'U' => 'User',
            'T' => 'Teacher',
            'A' => 'Admin',
            'Tel' => 'Telesale',
            'TelLead' => 'TelesaleLeader',
            'An' => 'Analyzer',
            'C' => 'CustomerService',
            'V' => 'Video',
            'Mar' => 'Marketing',
            'R' => 'Relation',
            'HN_C' => 'CustomerServiceHaNoi',
            'Bis_Dev' => 'BusinessDevelopment',
            'Content_Dev' => 'ContentDevelopment',
            'XN' => 'Cod',
            'M' => 'Manager'
        ];
        
        return isset($mapRoles[$roleV2]) ? $mapRoles[$roleV2] : null;
    }
    
    private function getPasswordFromHash($v2PasswordHash)
    {
        $encypt = new CI_Encrypt();
        
        return $encypt->decode($v2PasswordHash);
    }
    
    private function saveUserMeta($user, $dataV2)
    {
        $user->fb_id = $dataV2['facebook_account'];
        $user->gender = intval($dataV2['gender']);
        $user->phone = $dataV2['phone_number'];
        $user->address = $dataV2['street_address'];
        $user->birthday = $dataV2['birthday'];
        if (!empty($dataV2['district'])) {
            $user->location_id = $this->getLocationByDistrictText($dataV2['district']);
        }
        $user->is_receive_email_newsletter = $dataV2['news_email'];
        $user->is_receive_email_new_course_created = $dataV2['study_email'];
        $user->is_finished_tutorial_beginner = $dataV2['finished_tutorial_beginner'];
        if (!empty($dataV2['manager_id'])) {
            $manaUser = User::find()->andWhere(['v2_id' => $dataV2['manager_id']])->one();
            if (!is_null($manaUser)) {
                $user->manager_id = $manaUser->id;
            }
        }
        if (!empty($dataV2['large_avatar'])) {
            $user->avatar = $dataV2['large_avatar'];
        } else {
            $user->avatar = $dataV2['avatar'];
        }

        return $user->save();
    }
    
}
