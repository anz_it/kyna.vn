<?php
/**
 * Created by PhpStorm.
 * User: Hien Nguyen
 * Date: 10/6/2016
 * Time: 6:41 PM
 */

namespace app\modules\sync\controllers;


use kyna\mana\models\Teacher;
use Yii;

class ManaTeacherController extends \app\modules\sync\components\Controller
{
    public $table = 'mana_teachers';

    public function create($data)
    {
        $teacher = Teacher::find()->where(['v2_id' => $data['id']])->one();
        if (empty($teacher)) {
            $teacher = new Teacher();
            $teacher->name = $data['name'];
            $teacher->image_url = $data['avatar'];
            $teacher->description = $data['introduction'];
            $teacher->title = $data['title'];
            $teacher->status = $data['is_deleted'];
            $teacher->v2_id = $data['id'];
            if (!$teacher->save(false)) {
                Yii::error($teacher->errors, $this->table);
                return false;
            }
        }

        return $teacher;
    }
}