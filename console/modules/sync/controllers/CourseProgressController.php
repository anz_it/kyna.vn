<?php

namespace app\modules\sync\controllers;

use Yii;
use kyna\user\models\UserCourseMission;
use app\modules\sync\models\User;
use app\modules\sync\models\Course;
use app\modules\sync\models\CourseMission;
use app\modules\sync\components\Controller;

class CourseProgressController extends Controller
{

    public $table = 'user_course_mission';

    public function init()
    {
        $ret = parent::init();

        $v3Table = UserCourseMission::tableName();

        $result = Yii::$app->db->createCommand("SHOW COLUMNS FROM {$v3Table} like 'v2_id';")->queryOne();
        if ($result === false) {
            Yii::$app->db->createCommand("ALTER TABLE {$v3Table}
                ADD COLUMN `v2_id` INT(11)
            ")->execute();
        }

        return $ret;
    }

    public function create($data)
    {
        $model = UserCourseMission::find()->where(['v2_id' => $data['ID']])->one();

        if (is_null($model)) {
            $model = new UserCourseMission();

            $user = User::find()->where(['v2_id' => $data['UserID']])->one();
            if (!is_null($user)) {
                $model->user_id = $user->id;
            }

            $mission = CourseMission::find()->where(['v2_id' => $data['CourseMissionID']])->one();
            if (!is_null($mission)) {
                $model->course_mission_id = $mission->id;
            }

            $model->v2_id = $data['ID'];

            $model->user_name = $data['Username'];

            $model->current_day = $data['CurrentDay'];

            $course = Course::find()->where(['type' => array_keys(Course::listTypes()), 'v2_id' => $data['CourseID']])->one();
            if (!is_null($course)) {
                $model->course_id = $course->id;
            }

            $model->is_passed = $data['IsPassed'];

            $created_date = date_create_from_format("Y-m-d H:i:s", trim($data['CreatedDate']));
            if ($created_date !== false) {
                $model->created_date = $created_date->getTimestamp();
            }

            $finished_date = date_create_from_format("Y-m-d H:i:s", trim($data['FinishedDate']));
            if ($finished_date !== false) {
                $model->finished_date = $finished_date->getTimestamp();
            }

            $deadline_date = date_create_from_format("Y-m-d H:i:s", trim($data['DeadlineDate']));
            if ($deadline_date !== false) {
                $model->deadline_date = $deadline_date->getTimestamp();
            }
        }

        if (!$model->save()) {
            var_dump($model->errors);
            Yii::error($model->errors, $this->table);
            return false;
        }

        return $model;
    }

}
