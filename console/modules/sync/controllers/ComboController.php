<?php

namespace app\modules\sync\controllers;

use Yii;
use app\modules\sync\models\Course;
use app\modules\sync\models\CourseCombo;
use kyna\course_combo\models\CourseComboItem;
use kyna\course\models\Teacher;

class ComboController extends \app\modules\sync\components\Controller
{
    
    public $table = 'group_promotion';
    
    public function create($data)
    {
        $model = CourseCombo::find()->andWhere(['v2_group_promotion_id' => $data['id']])->one();
        if (empty($model)) {
            $model = CourseCombo::find()->andWhere(['name' => $data['name']])->one();
        }
        
        if (empty($model)) {
            $model = new CourseCombo();
            $model->loadDefaultValues();

            $model->type = CourseCombo::TYPE_COMBO;
            $model->name = $data['name'];
            $model->short_name = $data['short_name'];
            $model->image_url = $data['img_url'];
            $model->object = $data['object'];
            $model->benefit = $data['benefit'];
            $model->is_deleted = $data['is_deleted'];
        }
        $createdDate = date_create_from_format("Y-m-d H:i:s", trim($data['created_date']));
        if ($createdDate !== false) {
            $model->created_time = $createdDate->getTimestamp();
        }
        $model->v2_group_promotion_id = $data['id'];
        
        if (!empty($data['teacher_id'])) {
            $model->teacher_id = $this->migrateTeacher($data['teacher_id']);
        }
        
        $model->save(false);
        
        $this->migrateItems($model->id, $data['id']);
        
        return $model;
    }
    
    private function migrateItems($combo_id, $group_promotion_id)
    {
        $items = Yii::$app->dbv2->createCommand("SELECT * FROM group_course_promotion WHERE group_promotion_id = {$group_promotion_id}")->queryAll();
        
        foreach ($items as $data) {
            $course = Course::find()->where(['type' => array_keys(Course::listTypes()), 'v2_id' => $data['course_id']])->one();
            if (is_null($course)) {
                continue;
            }
            
            $model = CourseComboItem::find()->where([
                'course_combo_id' => $combo_id,
                'course_id' => $course->id,
            ])->one();
            if (empty($model)) {
                $model = new CourseComboItem();

                $model->course_combo_id = $combo_id;
                $model->course_id = $course->id;
                $model->price = $data['promotion_price'];
                $model->reg_total = $data['reg_total'];
            }
            
            $createdDate = date_create_from_format("Y-m-d H:i:s", trim($data['created_date']));
            if ($createdDate !== false) {
                $model->created_time = $createdDate->getTimestamp();
            }

            if (!$model->save()) {
                Yii::error($model->errors, $this->table);
            }
        }
    }
    
    private function migrateTeacher($v2Id)
    {
        if (empty($v2Id)) {
            return 0;
        }
        
        $v2Data = Yii::$app->dbv2->createCommand("SELECT * FROM teacher WHERE id = {$v2Id}")->queryOne();
        if ($v2Data === false) {
            return 0;
        }
        $teacher = Teacher::find()->where(['v2_id' => $v2Data['user_id']])->one();
        if (is_null($teacher)) {
            return 0;
        }
        
        $roles = Yii::$app->authManager->getRolesByUser($teacher->id);
        if (!array_key_exists('Teacher', $roles)) {
            $authManager = Yii::$app->authManager;
            // assign role Teacher
            $authManager->assign($authManager->getRole('Teacher'), $teacher->id);
        }
        
        
        $teacher->title = $v2Data['Title'];
        $teacher->introduction = $v2Data['Introduction'];
        $teacher->avatar = $v2Data['Avatar'];
        $teacher->status = $v2Data['is_hidden'] ? Teacher::STATUS_DEACTIVE : Teacher::STATUS_ACTIVE;
        $teacher->save();

        $profile = $teacher->profile;
        $profile->name = $v2Data['Name'];
        $profile->save();
       
        return $teacher->id;
    }
}
