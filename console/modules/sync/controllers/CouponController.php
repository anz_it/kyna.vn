<?php

namespace app\modules\sync\controllers;

use Yii;
use yii\helpers\ArrayHelper;
use app\modules\sync\models\Promotion;
use kyna\course\models\Course;
use kyna\promo\models\CouponCourse;
use app\modules\sync\models\User;

class CouponController extends \app\modules\sync\components\Controller
{
    
    public $table = 'coupon';
    
    public function create($data)
    {
        $model = Promotion::find()->andWhere([
            'kind' => Promotion::KIND_COUPON,
            'v2_id' => $data['id'],
        ])->one();
        if (is_null($model)) {
            $model = Promotion::find()->andWhere([
                'kind' => Promotion::KIND_COUPON,
                'code' => $data['code'],
            ])->one();
        }
        if (is_null($model)) {
            $model = new Promotion();

            $model->kind = Promotion::KIND_COUPON;
            $model->number_usage = $data['reg_num'];
            $model->code = $data['code'];
            if (!empty($data['percent'])) {
                $model->type = Promotion::TYPE_PERCENTAGE;
                $model->value = $data['percent'];
            } else {
                $model->type = Promotion::TYPE_FEE;
                $model->value = $data['voucher_value'];
            }
            
            $startDate = date_create_from_format("Y-m-d H:i:s", trim($data['issued_date']));
            if ($startDate !== false) {
                $model->start_date = $startDate->getTimestamp();
            }

            $expiredDate = date_create_from_format("Y-m-d H:i:s", trim($data['deadline_date']));
            if ($expiredDate !== false) {
                $model->expiration_date = $expiredDate->getTimestamp();
            }
            
            if (!empty($data['user_id'])) {
                $createdUser = User::find()->where(['v2_id' => $data['user_id']])->one();
                $model->created_user_id = !is_null($createdUser) ? $createdUser->id : 0;
            }
            
            $createdDate = date_create_from_format("Y-m-d H:i:s", trim($data['created_date']));
            if ($createdDate !== false) {
                $model->created_time = $createdDate->getTimestamp();
            }
            
            $model->v2_id = $data['id'];

            if (!empty($data['issued_person'])) {
                $v2Data = Yii::$app->dbv2->createCommand("SELECT * FROM teacher WHERE id = {$data['issued_person']}")->queryOne();
                if ($v2Data !== false) {
                    $teacher = User::find()->where(['v2_id' => $v2Data['user_id']])->one();
                    if (!is_null($teacher)) {
                        $model->issued_person = $teacher->id;
                    }
                }
            }
        }
        
        if (!empty($data['affiliate_id'])) {
            $affUser = User::find()->where(['v2_id' => $data['affiliate_id']])->one();
            if ($affUser != null) {
                $model->partner_id = !is_null($affUser) ? $affUser->id : 0;
            }
        }
        
        if (!$model->save()) {
            var_dump($model->errors);
            Yii::error($model->errors, $this->table);
        }
        
        if (!is_null($data['course_list'])) {
            //$this->saveCourses($model->id, $data['course_list']);
        }
        
        return $model;
    }
    
    private function saveCourses($promotion_id, $course_list)
    {
        $listCourses = [];
        if ($course_list === 'all') {
            $listCourses = ArrayHelper::map(Course::find()->where(['type' => array_keys(Course::listTypes()), 'status' => Course::STATUS_ACTIVE])->all(), 'id', 'id');
        } else {
            $v2CourseList = explode(',', $course_list);
            foreach ($v2CourseList as $v2CourseId) {
                $course = Course::find()->where(['type' => Course::TYPE_VIDEO, 'v2_id' => $v2CourseId])->select('id')->one();
                if (!is_null($course) && !in_array($course->id, $listCourses)) {
                    $listCourses[] = $course->id;
                }
            }
        }
        
        foreach ($listCourses as $course_id) {
            $model = CouponCourse::find()->where(['promotion_id' => $promotion_id, 'course_id' => $course_id])->one();
            if (!is_null($model)) {
                continue;
            }
            $model = new CouponCourse();

            $model->course_id = $course_id;
            $model->promotion_id = $promotion_id;

            if (!$model->save()) {
                var_dump($course_list, $model->errors);
                Yii::error($model->errors, $this->table);
            }
        }
    }
}
