<?php

namespace app\modules\sync\controllers;

use Yii;
use app\modules\sync\models\Order;
use app\modules\sync\models\User;
use app\modules\sync\models\Course;
use kyna\course_combo\models\CourseCombo;
use kyna\order\models\OrderDetails;
use kyna\commission\models\CommissionIncome;

class OrderController extends \app\modules\sync\components\Controller
{
    
    public $table = 'account_registration';
    
    public function create($data)
    {
        $model = Order::find()->where(['v2_id' => $data['id']])->one();
        if (is_null($model)) {
            $model = new Order();
            
            $user = User::find()->where(['v2_id' => $data['user_id']])->one();

            $model->user_id = !is_null($user) ? $user->id : 0;
            $model->activation_code = $data['activate_code'];
            $model->is_paid = $data['is_actived'];
            $model->promotion_code = $data['coupon_code'];
            $model->payment_method = $data['payment_registered'];
            
            if (!empty($data['affiliate_id'])) {
                $affiliater = User::find()->where(['v2_id' => $data['affiliate_id']])->one();
                $model->affiliate_id = !is_null($affiliater) ? $affiliater->id : 0;
            }
            
            if (!empty($data['tel_id'])) {
                $teler = User::find()->where(['v2_id' => $data['tel_id']])->one();
                $model->reference_id = !is_null($teler) ? $teler->id : 0;
            }

            $registeredDate = date_create_from_format("Y-m-d H:i:s", trim($data['registered_date']));
            if ($registeredDate !== false) {
                $model->order_date = $model->created_time = $registeredDate->getTimestamp();
            }

            $model->v2_id = $data['id'];
            $model->save(false);
        }

        if ($data['is_actived']) {
            $model->status = Order::ORDER_STATUS_COMPLETE;
            $model->is_done_telesale_process = Order::BOOL_YES;
        } else {
            $model->status = Order::ORDER_STATUS_PENDING_PAYMENT;
        }
        
        $model->sub_total = ($data['payment'] > $data['cost']) ? $data['payment'] : $data['cost'];
        $model->total = $data['real_payment'];
        
        $model->save(false);
        $this->saveDetails($model->id, $data);
        
        $this->saveCommission($model, $data);

        return $model;
    }
    
    private function saveDetails($order_id, $data)
    {
        $course = Course::find()->where(['type' => array_keys(Course::listTypes()), 'v2_id' => $data['course_id']])->one();
        $courseId = !is_null($course) ? $course->id : 0;
        
        $model = OrderDetails::find()->where([
            'order_id' => $order_id,
            'course_id' => $courseId,
        ])->one();
        
        if (is_null($model)) {
            $model = new OrderDetails();

            $model->order_id = $order_id;
            $model->course_id = $courseId;
            $model->unit_price = $data['cost'];
        }
        
        if ($data['payment'] > 0) {
            $model->discount_amount =  $data['cost'] >= $data['payment'] ? ($data['cost'] - $data['payment']) : ($data['payment'] - $data['cost']);
        }
        
        if (!empty($data['group_promotion_id'])) {
            $combo = CourseCombo::find()->where(['v2_group_promotion_id' => $data['group_promotion_id']])->one();
            if (!is_null($combo)) {
                $model->course_combo_id = $combo->id;
            }
        }
        
        if (!$model->save()) {
            var_dump($model->errors);
            Yii::error($model->errors, $this->table);
        }
    }
    
    private function saveCommission($order, $data)
    {
        if (empty($data['real_payment']) && empty($data['payment_date'])) {
            return false;
        }
        
        $affiliater = User::find()->where(['v2_id' => $data['affiliate_id']])->one();
        $course = Course::find()->where(['type' => array_keys(Course::listTypes()), 'v2_id' => $data['course_id']])->one();

        $comIncome = CommissionIncome::find()->where(['order_id' => $order->id])->one();
        if (is_null($comIncome)) {
            $comIncome = new CommissionIncome();

            $comIncome->order_id = $order->id;
            $comIncome->user_id = !is_null($affiliater) ? $affiliater->id : 0;
        }
        $comIncome->total_amount = $data['real_payment'];
        $comIncome->course_id = !is_null($course) ? $course->id : 0;

        $comIncome->affiliate_commission_percent = 0;
        if (!empty($data['affiliate_id'])) {
            $comIncome->affiliate_commission_percent = 50;
            
            $paymentDate = date_create_from_format("Y-m-d H:i:s", '2016-05-11 00:00:00');
            if (!empty($order->order_date) && $order->order_date > $paymentDate->getTimestamp()) {
                // check if affiliate is teacher, if true then check course is owner of teacher?
                $teacher = Yii::$app->dbv2->createCommand("SELECT * FROM teacher WHERE user_id = {$data['affiliate_id']}")->queryOne();
                if ($teacher !== false) {
                    if (!empty($data['group_promotion_id'])) {
                        // if combo then check if combo is owner of teacher, if not just 30% commission
                        $v2GroupPromotion = Yii::$app->dbv2->createCommand("SELECT * FROM group_promotion WHERE id = {$data['group_promotion_id']}")->queryOne();
                        if ($v2GroupPromotion['teacher_id'] !== $teacher['ID']) {
                            $comIncome->affiliate_commission_percent = 30;
                        }
                    } elseif ($data['course_id']) {
                        // if single course, then check course is owner of teacher, if not just 30% commission
                        $v2Course = Yii::$app->dbv2->createCommand("SELECT * FROM course WHERE id = {$data['course_id']}")->queryOne();
                        if ($v2Course !== false && $v2Course['TeacherID'] !== $teacher['ID']) {
                            $comIncome->affiliate_commission_percent = 30;
                        }
                    }
                }
            }
        }
        $comIncome->affiliate_commission_amount = !empty($data['affiliate_id']) ? ($comIncome->total_amount / 2) : 0;
        $comIncome->teacher_id = !empty($comIncome->course) ? $comIncome->course->teacher_id : 0;
        $comIncome->teacher_commission_percent = $data['teacher_commission'];
        $realIncome = !empty($data['affiliate_id']) ? ($comIncome->total_amount / 2) : $comIncome->total_amount;
        $comIncome->teacher_commission_amount = $comIncome->teacher_commission_percent *  $realIncome / 100;
        $comIncome->kyna_commission_amount = $data['real_payment'] - $comIncome->affiliate_commission_amount - $comIncome->teacher_commission_amount;

        if (!empty($data['payment_date'])) {
            $paymentDate = date_create_from_format("Y-m-d H:i:s", trim($data['payment_date']));
            if ($paymentDate !== false) {
                $comIncome->created_time = $paymentDate->getTimestamp();
            }
        } else {
            $comIncome->created_time = $order->order_date;
        }

        $comIncome->save(false);
    }
}
