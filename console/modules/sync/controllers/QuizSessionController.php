<?php

namespace app\modules\sync\controllers;

use Yii;
use kyna\course\models\QuizSession;
use kyna\course\models\QuizSessionAnswer;
use app\modules\sync\models\QuizDetail;
use app\modules\sync\models\QuizQuestion;
use app\modules\sync\models\User;
use app\modules\sync\models\Quiz;

class QuizSessionController extends \app\modules\sync\components\Controller
{
    
    public $table = 'result_essay';
    
    public function init()
    {
        $ret = parent::init();
        
        $v3Table = QuizSession::tableName();
        
        $result = Yii::$app->db->createCommand("SHOW COLUMNS FROM {$v3Table} like 'v2_id';")->queryOne();
        if ($result === false) {
            Yii::$app->db->createCommand("ALTER TABLE {$v3Table} 
                ADD COLUMN `v2_id` INT(11)
            ")->execute();
        }
        
        return $ret;
    }
    
    protected function create($data)
    {
        $startTime = 0;
        
        $startDate = date_create_from_format("Y-m-d H:i:s", trim($data['created_date']));
        if ($startDate !== false) {
            $startTime = $startDate->getTimestamp();
        }
        
        $user = User::find()->where(['v2_id' => $data['user_id']])->one();
        if (is_null($user)) {
            return false;
        }
        
        $quiz = Quiz::find()->where(['v2_id' => $data['quiz_id']])->one();
        if (is_null($quiz)) {
            return false;
        }
        
        $model = QuizSession::find()->where([
            'user_id' => $user->id,
            'quiz_id' => $quiz->id,
            'start_time' => $startTime,
        ])->one();
        
        if (empty($model)) {
            $model = new QuizSession();

            $model->v2_id = $data['id'];
            $model->user_id = $user->id;
            $model->quiz_id = $quiz->id;
            $model->start_time = $startTime;
            
            $model->save(false);
        } elseif ($model->v2_id !== $data['id']) {
            $model->v2_id = $data['id'];
            $model->save(false);
        }
        
        $question = QuizQuestion::find()->where(['v2_id' => $data['question_id']])->one();
        if (is_null($question)) {
            return false;
        }
        $quizDetail = QuizDetail::find()->where([
            'quiz_id' => $quiz->id,
            'quiz_question_id' => $question->id,
        ])->one();
        
        if (empty($quizDetail)) {
            $quizDetail = new QuizDetail();
            $quizDetail->quiz_id = $quiz->id;
            $quizDetail->quiz_question_id = $question->id;
            $quizDetail->save();
        }
        
        $answer = new QuizSessionAnswer();
        $answer->quiz_session_id = $model->id;
        $answer->quiz_detail_id = $quizDetail->id;
        $answer->answer_text = $data['content'];
        $answer->is_correct = $data['is_passed'];
        $answer->save();
        
        return $model;
    }
    
}
