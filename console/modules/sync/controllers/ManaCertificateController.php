<?php
/**
 * Created by PhpStorm.
 * User: Hien Nguyen
 * Date: 10/6/2016
 * Time: 6:41 PM
 */

namespace app\modules\sync\controllers;


use kyna\mana\models\Certificate;
use Yii;

class ManaCertificateController extends \app\modules\sync\components\Controller
{
    public $table = 'mana_certificates';

    public function create($data)
    {
        $certificate = Certificate::find()->where(['v2_id' => $data['id']])->one();
        if (empty($certificate)) {
            $certificate = new Certificate();
            $certificate->name = $data['name'];
            $certificate->slug = $data['slug'];
            $certificate->description = $data['description'];
            $certificate->image_url = $data['image'];
            $certificate->order = $data['order'];
            $certificate->status = $data['is_deleted'];
            $certificate->v2_id = $data['id'];
            if (!$certificate->save(false)) {
                Yii::error($certificate->errors, $this->table);
                return false;
            }
        }

        return $certificate;
    }
}