<?php
/**
 * Created by PhpStorm.
 * User: Hien Nguyen
 * Date: 10/6/2016
 * Time: 6:41 PM
 */

namespace app\modules\sync\controllers;


use kyna\mana\models\Certificate;
use kyna\mana\models\Contact;
use kyna\mana\models\Education;
use kyna\mana\models\Subject;
use Yii;

class ManaContactController extends \app\modules\sync\components\Controller
{
    public $table = 'mana_contacts';

    public function create($data)
    {
        $contact = Contact::find()->where(['v2_id' => $data['id']])->one();
        if (empty($contact)) {
            $contact = new Contact();
            $contact->name = $data['name'];
            $contact->phone = $data['phone'];
            $contact->email = $data['email'];

            $subject = Subject::findOne(['v2_id' => $data['subject_id']]);
            if ($subject) {
                $contact->subject_id = $subject->id;
            }

            $certificate = Certificate::findOne(['v2_id' => $data['certificate_id']]);
            if ($subject) {
                $contact->certificate_id = $certificate->id;
            }

            $education = Education::findOne(['v2_id' => $data['education_id']]);
            if ($subject) {
                $contact->education_id = $education->id;
            }
            $contact->status = $data['is_completed'];
            $contact->v2_id = $data['id'];
            if (!$contact->save(false)) {
                Yii::error($contact->errors, $this->table);
                return false;
            }
        }

        return $contact;
    }
}