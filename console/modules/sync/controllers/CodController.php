<?php

namespace app\modules\sync\controllers;

use Yii;

use common\helpers\StringHelper;

use kyna\course_combo\models\CourseCombo;
use kyna\order\models\OrderDetails;
use kyna\course_combo\models\CourseComboItem;
use kyna\order\models\OrderShipping;
use kyna\base\models\Location;

use app\modules\sync\models\User;
use app\modules\sync\models\UserCourse;
use app\modules\sync\models\Course;
use app\modules\sync\models\Order;

class CodController extends \app\modules\sync\components\Controller
{
    
    public $table = 'cod_orders';
    
    protected function setKey()
    {
        $key = StringHelper::random(20, true);

        $rets = Yii::$app->dbv2->createCommand("UPDATE {$this->table} SET `key` = '{$key}' WHERE `is_sync` = 0 AND `key` IS NULL AND is_actived = 0 ORDER BY id DESC LIMIT 1")->execute();
        
        return [$key, $rets];
    }
    
    public function create($data)
    {
        echo "COD controller - create /n";
        $model = Order::find()->where(['v2_cod_id' => $data['id']])->one();
        $user = User::find()->where(['v2_id' => $data['user_id']])->one();
        
        if (is_null($model)) {
            $model = new Order();

            $model->user_id = !is_null($user) ? $user->id : 0;
            $model->activation_code = $data['activate_code'];
            $model->is_paid = $data['is_pay'];
            $model->promotion_code = !empty($data['voucher_code']) ? $data['voucher_code'] : $data['coupon'];
            $model->payment_method = 'cod';
            
            if (!empty($data['affiliate_id'])) {
                $affiliater = User::find()->where(['v2_id' => $data['affiliate_id']])->one();
                $model->affiliate_id = !is_null($affiliater) ? $affiliater->id : 0;
            }
            
            $model->is_done_telesale_process = Order::BOOL_YES;
            
            switch ($data['status']) {
                case 'da-giao':
                    $model->status = Order::ORDER_STATUS_DELIVERING;
                    break;
                
                case 'tu-choi':
                    $model->status = Order::ORDER_STATUS_CANCELLED;
                    break;
                
                case 'tra-lai':
                    $model->status = Order::ORDER_STATUS_RETURN;
                    break;
                
                default: 
                    $model->status = Order::ORDER_STATUS_PENDING_CONTACT;
                    break;
            }
            
            if (!empty($data['tel_id'])) {
                $teler = User::find()->where(['v2_id' => $data['tel_id']])->one();
                $model->reference_id = !is_null($teler) ? $teler->id : 0;
            }
            
            if (!empty($data['xn_id'])) {
                $operator = User::find()->where(['v2_id' => $data['xn_id']])->one();
                $model->operator_id = !is_null($operator) ? $operator->id : 0;
            }

            $registeredDate = date_create_from_format("Y-m-d H:i:s", trim($data['created_date']));
            if ($registeredDate !== false) {
                $model->order_date = $model->created_time = $registeredDate->getTimestamp();
            }

            $model->v2_cod_id = $data['id'];
        }
        
        $model->sub_total = $data['cost'];
        
        $comboId = 0;
        if (!empty($data['group_promotion_id'])) {
            $combo = CourseCombo::find()->where(['v2_group_promotion_id' => $data['group_promotion_id']])->one();
            if (!is_null($combo)) {
                $comboId = $combo->id;
                $model->sub_total = $combo->price;
                $model->total_discount = $model->sub_total - $data['cost'];
            }
        }
        
        $model->shipping_fee = $data['shipping'];
        $model->total = $data['cost'] + $data['shipping'];
        
        $model->save(false);
        $this->_saveShippingAddress($model->id, $data);
        $courseIds = $this->_saveDetails($model, $data, $comboId);
        if (!empty($courseIds) && !is_null($user)) {
            UserCourse::register($user->id, $courseIds);
        }
        
        return $model;
    }
    
    public function _saveShippingAddress($order_id, $data)
    {
        $model = OrderShipping::find()->where(['order_id' => $order_id])->one();
        
        if (is_null($model)) {
            $model = new OrderShipping();
            
            $model->order_id = $order_id;
            $model->street_address = $data['street'];
            $model->fee = $data['shipping'];
            
            if (!empty($data['province_id'])) {
                $v2District = Yii::$app->dbv2->createCommand("SELECT * FROM district WHERE value = {$data['province_id']}")->queryOne();
                if (!empty($v2District['name'])) {
                    $location = Location::getByDistrictText($v2District['name']);
                    if (!is_null($location)) {
                        $model->location_id = $location->id;
                    }
                }
            }

            $model->save(false);
        }
    }
    
    private function _saveDetails($order, $data, $combo_id = 0)
    {
        $order_id = $order->id;
        $courseIds = [];
        
        if (!empty($data['course_id'])) {
            $course = Course::find()->where(['type' => array_keys(Course::listTypes()), 'v2_id' => $data['course_id']])->one();
            if (!is_null($course)) {
                $meta = json_decode(OrderDetails::extractCourseMeta($course), true);
                $oldPrice = $course->oldPrice;
                $discount = $oldPrice - $data['cost'];
                
                $this->_createDetail($order_id, $course->id, $oldPrice, $discount, 0, $meta);
                
                if ($oldPrice != $order->sub_total) {
                    $order->sub_total = $oldPrice;
                    $order->total_discount = $discount;
                    $order->save(false);
                }
                
                $courseIds[] = $course->id;
            }
        } elseif (!empty($data['course_list_id'])) {
            $courseV2Ids = explode(',', $data['course_list_id']);
            $combo = Course::find()->where(['id' => $combo_id])->one();
            
            foreach ($courseV2Ids as $cV2Id) {
                $course = Course::find()->where(['type' => array_keys(Course::listTypes()), 'v2_id' => $cV2Id])->one();
                if (!is_null($course)) {
                    $meta = json_decode(OrderDetails::extractCourseMeta($course), true);
                    
                    if (!is_null($combo)) {
                        $courseComboItem = CourseComboItem::find()->andWhere(['course_combo_id' => $combo_id, 'course_id' => $course->id])->one();
                        if (empty($courseComboItem)) {
                            continue;
                        }

                        $meta['combo_name'] = $combo->name;
                        $discount = $course->oldPrice - $courseComboItem->price;
                    } else {
                        $discount = 0;
                    }
                    $this->_createDetail($order_id, $course->id, $course->oldPrice, $discount, $combo_id, $meta);
                    $courseIds[] = $course->id;
                }
            }
        }
        
        return $courseIds;
    }
    
    private function _createDetail($order_id, $course_id, $unit_price, $discount_amount = 0, $combo_id = 0, $meta = null)
    {
        $model = OrderDetails::find()->where([
            'order_id' => $order_id,
            'course_id' => $course_id,
        ])->one();

        if (is_null($model)) {
            $model = new OrderDetails();

            $model->order_id = $order_id;
            $model->course_id = $course_id;
            $model->unit_price = $unit_price;
            $model->discount_amount = $discount_amount;
            $model->course_combo_id = $combo_id;
            if (!is_null($meta)) {
                $model->course_meta = json_encode($meta);
            }
        }

        if (!$model->save()) {
            var_dump($model->errors);
            Yii::error($model->errors, $this->table);
        }
    }
}