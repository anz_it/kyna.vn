<?php
/**
 * Created by PhpStorm.
 * User: Hien Nguyen
 * Date: 10/6/2016
 * Time: 6:41 PM
 */

namespace app\modules\sync\controllers;


use kyna\mana\models\Organization;
use Yii;

class ManaOrganizationController extends \app\modules\sync\components\Controller
{
    public $table = 'mana_organizations';

    public function create($data)
    {
        $organization = Organization::find()->where(['v2_id' => $data['id']])->one();
        if (empty($organization)) {
            $organization = new Organization();
            $organization->name = $data['name'];
            $organization->slug = $data['slug'];
            $organization->description = $data['description'];
            $organization->image_url = $data['image'];
            $organization->order = $data['order'];
            $organization->status = $data['is_deleted'];
            $organization->v2_id = $data['id'];
            if (!$organization->save(false)) {
                Yii::error($organization->errors, $this->table);
                return false;
            }
        }

        return $organization;
    }
}