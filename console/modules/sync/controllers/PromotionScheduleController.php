<?php

namespace app\modules\sync\controllers;

use app\modules\sync\models\Course;
use app\modules\sync\models\PromotionSchedule;

class PromotionScheduleController extends \app\modules\sync\components\Controller
{
    
    public $table = 'promotion';
    
    public function create($data)
    {
        if (empty($data['course_id'])) {
            return false;
        }
        
        $course = Course::find()->where(['type' => array_keys(Course::listTypes()), 'v2_id' => $data['course_id']])->one();
        if (is_null($course)) {
            return false;
        }
        
        $model = PromotionSchedule::find()->where(['v2_id' => $data['id']])->one();
        if (is_null($model)) {
            $model = new PromotionSchedule();
            $model->setScenario('migrate');

            $model->course_id = $course->id;
        }
        
        $model->price = $data['promotion_price'];
        $model->reg_count = $data['reg_count'];
        $model->note = $data['note_event'];
        $model->image_url = $data['link_image'];

        $unitPrice = !is_null($model->course) ? $model->course->price : 0;
        if (!empty($unitPrice) && !empty($model->price)) {
            $model->discount_amount = $unitPrice - $model->price;
            $model->discount_percent = $model->discount_amount * 100 / $unitPrice;
        }

        $createdDate = date_create_from_format("Y-m-d H:i:s", trim($data['created_date']));
        if ($createdDate !== false) {
            $model->created_time = $createdDate->getTimestamp();
        }
        
        $startDate = date_create_from_format("Y-m-d H:i:s", trim($data['promotion_start_time']));
        if ($startDate !== false) {
            $model->start_time = $startDate->getTimestamp();
        }
        
        $endDate = date_create_from_format("Y-m-d H:i:s", trim($data['promotion_end_time']));
        if ($endDate !== false) {
            $model->end_time = $endDate->getTimestamp();
        }
        
        $model->v2_id = $data['id'];
        $model->save(false);
        
        return $model;
    }
    
}
