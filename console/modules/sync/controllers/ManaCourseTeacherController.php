<?php
/**
 * Created by PhpStorm.
 * User: Hien Nguyen
 * Date: 10/6/2016
 * Time: 6:41 PM
 */

namespace app\modules\sync\controllers;

use kyna\mana\models\Course;
use kyna\mana\models\CourseTeacher;
use kyna\mana\models\Teacher;
use Yii;

class ManaCourseTeacherController extends \app\modules\sync\components\Controller
{
    public $table = 'mana_course_teachers';

    public function create($data)
    {
        $teacherCourse = CourseTeacher::find()->where(['v2_id' => $data['id']])->one();
        if (empty($teacherCourse)) {
            $teacherCourse = new CourseTeacher();
            $kynaCourse = Course::findOne(['v2_id' => $data['mana_course_id']]);
            if ($kynaCourse) {
                $teacherCourse->course_id = $kynaCourse->id;
            } else {
                $teacherCourse->course_id = 0;
            }

            $teacher = Teacher::findOne(['v2_id' => $data['mana_teacher_id']]);
            if ($teacher) {
                $teacherCourse->teacher_id = $teacher->id;
            } else {
                $teacherCourse->teacher_id = 0;
            }
            $teacherCourse->v2_id = $data['id'];
            if (!$teacherCourse->save(false)) {
                Yii::error($teacherCourse->errors, $this->table);
                return false;
            }
        }

        return $teacherCourse;
    }
}