<?php

namespace app\modules\sync\controllers;

use kyna\course\models\CourseLearnerQna;
use app\modules\sync\models\User;
use app\modules\sync\models\Course;

class QnaController extends \app\modules\sync\components\Controller
{
    
    public $table = 'q_a';
    
    protected function create($data)
    {
        $model = CourseLearnerQna::find()->andWhere(['v2_id' => $data['id']])->one();
        
        if (is_null($model)) {
            $model = new CourseLearnerQna();
            
            $model->v2_id = $data['id'];
            $model->content = $data['question_content'];
            
            $learner = User::find()->where(['v2_id' => $data['user_id']])->one();
            if (!is_null($learner)) {
                $model->user_id = $learner->id;
            }
            
            $createdDate = date_create_from_format("Y-m-d H:i:s", trim($data['created_date']));
            if ($createdDate !== false) {
                $model->posted_time = $createdDate->getTimestamp();
            }

            $course = Course::find()->where([
                'v2_id' => $data['object_id'],
                'type' => array_keys(Course::listTypes())
            ])->one();
            
            if (!is_null($course)) {
                $model->course_id = $course->id;
            }
            
            $model->is_approved = $data['is_approved'];
            $model->is_send_approved = $data['is_send_approved'];
            $model->is_send_unapproved = $data['is_send_unapproved'];
            $model->is_send_to_teacher = $data['is_send_teacher'];
            $model->is_send_moved = $data['is_send_move'];
            $model->is_send_deleted = $data['is_send_deleted'];
            $model->is_send_answered = $data['is_send_answer'];
        }

        if ($model->save()) {
            $this->_migrateAnswer($model, $data);
        } else {
            var_dump($model->getErrors());die;
            Yii::error($model->errors, $this->table);
        }
        
        return $model;
    }
    
    private function _migrateAnswer($question, $data)
    {
        if (!empty($data['answer_content'])) {
            $model = CourseLearnerQna::find()->where(['question_id' => $question->id])->one();
            
            if (is_null($model)) {
                $model = new CourseLearnerQna();
                $model->question_id = $question->id;
                $model->course_id = $question->course_id;
                $model->content = $data['answer_content'];
            }
            
            if (!$model->save()) {
                var_dump($model->errors);die;
            }
        }
    }
}
