<?php
/**
 * Created by PhpStorm.
 * User: Hien Nguyen
 * Date: 10/6/2016
 * Time: 6:41 PM
 */

namespace app\modules\sync\controllers;


use kyna\mana\models\Certificate;
use kyna\mana\models\Course;
use kyna\mana\models\CourseSubject;
use kyna\mana\models\Education;
use kyna\mana\models\Organization;
use kyna\course\models\Course as KynaCourse;
use Yii;

class ManaCourseController extends \app\modules\sync\components\Controller
{
    public $table = 'mana_courses';

    public function create($data)
    {
        $course = Course::find()->where(['v2_id' => $data['id']])->one();
        if (empty($course) && $data['course_lang_type'] == 'vietnamese') {
            $course = new Course();

            $kynaCourse = KynaCourse::findOne(['v2_id' =>$data['course_id']]);
            if ($kynaCourse) {
                $course->course_id = $kynaCourse->id;
            } else {
                $course->course_id = 0;
            }

            $kynaCourseRef = KynaCourse::findOne(['v2_id' =>$data['course_ref_id']]);
            if ($kynaCourseRef) {
                $course->course_ref_id = $kynaCourseRef->id;
            } else {
                $course->course_ref_id = 0;
            }

            $education = Education::findOne(['v2_id' => $data['education_id']]);
            if ($education) {
                $course->education_id = $education->id;
            }

            $certificate = Certificate::findOne(['v2_id' => $data['certificate_id']]);
            if ($certificate) {
                $course->certificate_id = $certificate->id;
            }

            $organization = Organization::findOne(['v2_id' => $data['organization_id']]);
            if ($organization) {
                $course->organization_id = $organization->id;
            }

            $course->description_for = $data['description_for'];
            $course->condition = $data['condition'];
            $course->learning_time = $data['learning_time'];
            $course->learning_method = $data['learning_method'];
            $course->learning_start_date = $data['start_learning_date'];
            $course->order = 0;

            $course->status = $data['is_deleted'];
            $course->v2_id = $data['id'];
            if (!$course->save(false)) {
                Yii::error($course->errors, $this->table);
                return false;
            }

            $courseSubject = new CourseSubject();
            $courseSubject->course_id = $course->id;
            $courseSubject->subject_id = $data['subject_id'];
            if (!$courseSubject->save(false)) {
                return false;
            }
        } else {
            $course = Course::find()->one();
        }

        return $course;
    }
}