<?php

namespace app\modules\sync\controllers;

use Yii;
use app\modules\sync\models\User;
use app\modules\sync\models\AffiliateUser;

class AffiliateController extends \app\modules\sync\components\Controller
{
    
    public $table = 'affiliate';
    
    public function create($data)
    {
        $user = User::find()->andWhere(['v2_id' => $data['user_id']])->one();
        if (is_null($user)) {
            return false;
        }
        
        $model = AffiliateUser::find()->where([
            'user_id' => $user->id
        ])->one();
        if (empty($model)) {
            $model = new AffiliateUser();
        }
        $model->user_id = $user->id;
        
        $createdDate = date_create_from_format("Y-m-d H:i:s", trim($data['created_date']));
        if ($createdDate !== false && empty($model->created_time)) {
            $model->created_time = $createdDate->getTimestamp();
        }
        if (empty($model->affiliate_category_id)) {
            $model->affiliate_category_id = $data['cate_id'];
        }
        
        if (!$model->save()) {
            var_dump($model->errors);
            Yii::error($model->errors, $this->table);
        }
        
        return $model;
    }
}
