<?php
/**
 * Created by PhpStorm.
 * User: Hien Nguyen
 * Date: 10/6/2016
 * Time: 6:41 PM
 */

namespace app\modules\sync\controllers;


use kyna\mana\models\Education;
use Yii;

class ManaEducationController extends \app\modules\sync\components\Controller
{
    public $table = 'mana_educations';

    public function create($data)
    {
        $education = Education::find()->where(['v2_id' => $data['id']])->one();
        if (empty($education)) {
            $education = new Education();
            $education->name = $data['name'];
            $education->slug = $data['slug'];
            $education->description = $data['description'];
            $education->image_url = $data['image'];
            $education->order = $data['order'];
            $education->status = $data['is_deleted'];
            $education->v2_id = $data['id'];
            if (!$education->save(false)) {
                Yii::error($education->errors, $this->table);
                return false;
            }
        }

        return $education;
    }
}