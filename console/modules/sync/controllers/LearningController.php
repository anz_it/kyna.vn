<?php

namespace app\modules\sync\controllers;

use Yii;
use app\modules\sync\models\User;
use app\modules\sync\models\Course;
use kyna\user\models\UserCourse;
use kyna\user\models\actions\UserCourseAction;
use common\helpers\StringHelper;

class LearningController extends \app\modules\sync\components\Controller
{
    
    public $table = 'learner';
    
    protected function create($data)
    {
        $user = User::find()->where(['v2_id' => $data['user_id']])->one();

        if (is_null($user)) {
            return false;
        }
        
        $course = Course::find()->where(['type' => array_keys(Course::listTypes()), 'v2_id' => $data['course_id']])->one();

        if (is_null($course)) {
            die(PHP_EOL . $data['course_id'] . ' - course' . PHP_EOL);
            return false;
        }
        
        $isNewRecord = false;
        
        $model = UserCourse::find()->andWhere([
            'user_id' => $user->id,
            'course_id' => $course->id
        ])->one();
        
        if (empty($model)) {
            $model = new UserCourse();

            $model->user_id = $user->id;
            $model->course_id = $course->id;
            $model->is_activated = UserCourse::BOOL_YES;
            $isNewRecord = true;
            
            $model->is_started = $data['is_started'];
            $model->is_graduated = !empty($data['congratulation_date']) ? UserCourse::BOOL_YES : UserCourse::BOOL_NO;
            $model->method = $data['method'];
            $model->process = $data['process'];

            $activeDate = date_create_from_format("Y-m-d H:i:s", trim($data['activate_date']));
            if ($activeDate !== false) {
                $model->activation_date = $activeDate->getTimestamp();
            }
            
            $expireDate = date_create_from_format("Y-m-d H:i:s", trim($data['ngay_het_han']));
            if ($expireDate !== false) {
                $model->expiration_date = $expireDate->getTimestamp();
            }
        }
        
        $startedDate = date_create_from_format("Y-m-d H:i:s", trim($data['started_date']));
        if ($startedDate !== false) {
            $model->started_date = $startedDate->getTimestamp();
        }

        if ($model->save()) {
            if ($isNewRecord) {
                $this->makeTrigger($model->id, UserCourse::EVENT_ACTIVE, $model->activation_date);

                if ($model->is_graduated == UserCourse::BOOL_YES) {
                    $gradutedTime = time();
                    $gradutedDate = date_create_from_format("Y-m-d H:i:s", trim($data['congratulation_date']));
                    if ($gradutedDate !== false) {
                        $gradutedTime = $gradutedDate->getTimestamp();
                    }
                    $this->makeTrigger($model->id, UserCourse::EVENT_GRADUATE, $gradutedTime);

                    if (!empty($data['congratulation_code'])) {
                        $this->makeTrigger($model->id, UserCourse::EVENT_CERTIFICATE, $gradutedTime, ['certificate_number' => $data['congratulation_code']]);
                    }
                }
            }
        } else {
            var_dump($model->getErrors());die;
            Yii::error($model->errors, $this->table);
        }
        
        return $model;
    }
    
    protected function setKey()
    {
        $key = StringHelper::random(20, true);

        $rets = Yii::$app->dbv2->createCommand("UPDATE {$this->table} SET `key` = '{$key}' WHERE `is_sync` = 0 AND `key` IS NULL ORDER BY user_id LIMIT 1")->execute();
        
        return [$key, $rets];
    }
    
    private function makeTrigger($id, $action, $time, $meta = [])
    {
        $ucAction = new UserCourseAction();
        $ucAction->user_id = 0;
        $ucAction->user_course_id = $id;
        $ucAction->name = $action;
        $ucAction->action_time = $time;
        $ucAction->save(false);
        
        if (!empty($meta)) {
            foreach ($meta as $key => $value) {
                $ucAction->$key = $value;
            }
            
            $ucAction->save(false);
        }
    }
}
