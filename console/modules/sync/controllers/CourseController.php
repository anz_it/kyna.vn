<?php

namespace app\modules\sync\controllers;

use Yii;
use app\modules\sync\models\Course;
use app\modules\sync\models\CourseCommission;
use app\modules\sync\models\Teacher;
use app\modules\sync\models\CourseSection;
use app\modules\sync\models\CourseLesson;
use app\modules\sync\models\CourseMission;
use app\modules\sync\models\Quiz;
use app\modules\sync\models\QuizQuestion;
use app\modules\sync\models\QuizDetail;
use app\modules\sync\models\QuizAnswer;

class CourseController extends \app\modules\sync\components\Controller
{

    public $table = 'course';

    protected function create($data)
    {
        $course = Course::find()->where(['type' => array_keys(Course::listTypes()), 'v2_id' => $data['ID']])->one();
        if (empty($course)) {
            $course = new Course();

            $course->v2_id = $data['ID'];
            $course->name = $data['Name'];
            $course->short_name = $data['short_name'];
            $course->level = $data['Level'];
            $course->price = $data['Price'];
            $course->price_discount = $data['Price'] - $data['promotion_price'];
            $course->slug = $data['slug'];
            $course->image_url = $data['ImageUrl'];
            $course->course_commission_type_id = $this->migrateCommissionType($data['type_commission']);
            $course->video_url = $data['UrlVideo'];
            $course->total_time = $data['totaltime'] * 3600; // v3 save as second
            $course->category_id = $data['cat_id'];
            $course->video_cover_image_url = $data['video_cover_image'];
            $course->teacher_id = $this->migrateTeacher($data['TeacherID']);
            $course->status = intval($data['hidden']) ? Course::STATUS_DEACTIVE : Course::STATUS_ACTIVE;
            $course->is_deleted = $data['is_deleted'];
            
            $createdDate = date_create_from_format("Y-m-d H:i:s", trim($data['created_date']));
            if ($createdDate !== false) {
                $course->created_time = $createdDate->getTimestamp();
            }
        } else {
            $course->price_discount = $data['Price'] - $data['promotion_price'];
        }

        if (!$course->save(false)) {
            var_dump($course->errors);
            Yii::error($course->errors, $this->table);

            return false;
        }

        $this->saveCourseMeta($course, $data);
        $this->saveCommission($course->id, $data);

        $this->migrateSection($course->id, $course->v2_id);
        $this->migrateQuiz($course->id, $course->v2_id);
        $this->migrateMission($course->id, $course->v2_id);

        return $course;
    }

    private function migrateTeacher($v2Id)
    {
        if (empty($v2Id)) {
            return 0;
        }

        $v2Data = Yii::$app->dbv2->createCommand("SELECT * FROM teacher WHERE id = {$v2Id}")->queryOne();
        if ($v2Data === false) {
            return 0;
        }
        $teacher = Teacher::find()->where(['v2_id' => $v2Data['user_id']])->one();
        if (is_null($teacher)) {
            return 0;
        }

        $roles = Yii::$app->authManager->getRolesByUser($teacher->id);
        if (!array_key_exists('Teacher', $roles)) {
            $authManager = Yii::$app->authManager;
            // assign role Teacher
            $authManager->assign($authManager->getRole('Teacher'), $teacher->id);
        }


        $teacher->title = $v2Data['Title'];
        $teacher->introduction = $v2Data['Introduction'];
        $teacher->avatar = $v2Data['Avatar'];
        $teacher->status = $v2Data['is_hidden'] ? Teacher::STATUS_DEACTIVE : Teacher::STATUS_ACTIVE;
        $teacher->save();

        $profile = $teacher->profile;
        $profile->name = $v2Data['Name'];
        $profile->save();

        return $teacher->id;
    }

    private function migrateCommissionType($v2Type)
    {
        $mapData = [
            'premium' => 4,
            'recruitment' => 1,
            'special' => 5,
            'standard' => 3,
            'static_standard' => 2,
        ];

        return $mapData[$v2Type];
    }

    private function saveCommission($course_id, $v2Data)
    {
        $courseCommission = CourseCommission::find()->andWhere([
            'course_id' => $course_id,
            'is_default' => CourseCommission::BOOL_YES
        ])->one();
        if (is_null($courseCommission)) {
            $courseCommission = new CourseCommission();
            $courseCommission->course_id = $course_id;
            $courseCommission->commission_percent = $v2Data['commission'];
            $courseCommission->is_default = CourseCommission::BOOL_YES;
            $commissionEndDate = date_create_from_format("Y-m-d H:i:s", trim($v2Data['commission_end_date']));
            if ($commissionEndDate !== false) {
                $courseCommission->apply_to_date = date(str_replace('php:', '', Yii::$app->formatter->dateFormat), $commissionEndDate->getTimestamp());
            }

            return $courseCommission->save();
        }

        return true;
    }

    private function saveCourseMeta($course, $v2Data)
    {
        $course->facebook_thumbnail = $v2Data['facebook_thumbnail'];
        $course->keyword = $v2Data['meta_key'];
        $course->overview = $v2Data['marketing_content'];
        $course->description = $v2Data['Description'];
        $course->content = $v2Data['course_content'];
        $course->average_rating = $v2Data['average_rating'];
        $course->view_count = $v2Data['View'];
        $course->price_landing_page = $v2Data['landing_price'];

        return $course->save();
    }

    private function migrateSection($course_id, $course_v2_id)
    {
        $sections = Yii::$app->dbv2->createCommand("SELECT * FROM section WHERE CourseID = {$course_v2_id}")->queryAll();

        foreach ($sections as $data) {
            $model = CourseSection::find()->andWhere(['v2_id' => $data['ID']])->one();
            if (empty($model)) {
                $model = new CourseSection();

                $model->course_id = $course_id;
                $model->name = $data['Name'];
                $model->active = $data['IsDeleted'] ? 0 : 1;
                $model->makeRoot();

                $model->v2_id = $data['ID'];

                if (!$model->save(false)) {
                    var_dump($model->errors);
                    Yii::error($model->errors, $this->table);
                }
            }
            
            $this->migrateLesson($model);
        }
    }

    private function migrateLesson($section)
    {
        $lessons = Yii::$app->dbv2->createCommand("SELECT * FROM lesson WHERE SectionID = {$section->v2_id} ORDER BY PrevID")->queryAll();

        foreach ($lessons as $data) {
            $model = CourseSection::find()->andWhere([
                'course_id' => $section->course_id,
                'name' => $data['Name'],
            ])->one();
            
            if (is_null($model)) {
                $model = new CourseSection();

                $model->course_id = $section->course_id;
                $model->name = $data['Name'];
                $model->appendTo($section);

                if (!$model->save()) {
                    var_dump($model->errors);
                    Yii::error($model->errors, $this->table);
                    return false;
                }
            }
                
            $lesson = CourseLesson::find()->andWhere(['v2_id' => $data['ID']])->one();
            if (is_null($lesson)) {
                $lesson = new CourseLesson();

                $lesson->v2_id = $data['ID'];
                $lesson->course_id = $section->course_id;
                $lesson->section_id = $model->id;
                $lesson->name = $data['Name'];
                $lesson->day_can_learn = $data['OrderDay'];
                $lesson->video_link = $data['VideoLink'];
                $lesson->note = $data['Notes'];
                $lesson->content = $data['Content'];
            }
            $lesson->type = $data['Type'] == 'VIDEO' ? CourseLesson::TYPE_VIDEO : (!empty($data['QuizID']) ? CourseLesson::TYPE_QUIZ : (!empty($data['VideoLink']) ? CourseLesson::TYPE_VIDEO : CourseLesson::TYPE_CONTENT));
            
            if (!empty($data['QuizID'])) {
                $quiz = Quiz::find()->andWhere(['v2_id' => $data['QuizID']])->one();
                if (is_null($quiz)) {
                    $quizData = Yii::$app->dbv2->createCommand("SELECT * FROM quizs WHERE ID = {$data['QuizID']}")->queryOne();
                    if ($quizData !== false) {
                        $quiz = $this->saveQuiz($section->course_id, $quizData);
                    }
                } elseif ($quiz->course_id == 491) {
                    $quiz->course_id = $section->course_id;
                    $quiz->save(false);
                }

                if (!is_null($quiz) && $quiz !== false) {
                    $lesson->quiz_id = $quiz->id;
                }
            }
            
            $lesson->save(false);
        }
    }

    public function migrateQuiz($course_id, $course_v2_id)
    {
        $v2Quizs = Yii::$app->dbv2->createCommand("SELECT * FROM quizs WHERE CourseID = {$course_v2_id}")->queryAll();

        foreach ($v2Quizs as $data) {
            $quiz = $this->saveQuiz($course_id, $data);
        }
    }
    
    public function saveQuiz($course_id, $data)
    {
        $model = Quiz::find()->andWhere(['v2_id' => $data['ID']])->one();
        if (empty($model)) {
            $model = new Quiz();

            $model->v2_id = $data['ID'];
            $model->name = $data['Name'];
            $model->image_url = $data['ImageUrl'];
            $model->course_id = $course_id;
            $model->description = $data['Description'];
            $model->is_deleted = $data['IsDeleted'];
        }

        $createdDate = date_create_from_format("Y-m-d H:i:s", trim($data['CreatedDate']));
        if ($createdDate !== false) {
            $model->created_time = $createdDate->getTimestamp();
        }

        if (!$model->save()) {
            var_dump($model->errors);
            Yii::error($model->errors, $this->table);
            
            return false;
        } 

        $this->migrateQuizQuestion($course_id, $model->id, $model->v2_id);
        
        return $model;
    }

    public function migrateQuizQuestion($course_id, $quiz_id, $quiz_v2_id)
    {
        $v2Questions = Yii::$app->dbv2->createCommand("SELECT * FROM questions WHERE QuizID = {$quiz_v2_id}")->queryAll();

        foreach ($v2Questions as $data) {
            $model = QuizQuestion::find()->andWhere(['v2_id' => $data['ID']])->one();
            if (empty($model)) {
                $model = new QuizQuestion();

                $model->v2_id = $data['ID'];
                $model->course_id = $course_id;
                $model->content = $data['Content'];
                $model->image_url = $data['ImageUrl'];

                $v2Answers = Yii::$app->dbv2->createCommand("SELECT * FROM answers WHERE QuestionID = {$model->v2_id}")->queryAll();
                if (empty($v2Answers)) {
                    $model->type = QuizQuestion::TYPE_WRITING;
                } else {
                    $model->type = QuizQuestion::TYPE_ONE_CHOICE;
                }

                if (!$model->save()) {
                    var_dump($model->errors);
                    Yii::error($model->errors, $this->table);
                }

                $quizDetail = new QuizDetail();

                $quizDetail->quiz_id = $quiz_id;
                $quizDetail->quiz_question_id = $model->id;
                $quizDetail->save(false);


                foreach ($v2Answers as $data) {
                    $answer = QuizAnswer::find()->andWhere(['v2_id' => $data['ID']])->one();
                    if (is_null($answer)) {
                        $answer = new QuizAnswer();

                        $answer->v2_id = $data['ID'];
                        $answer->question_id = $model->id;
                        $answer->content = $data['Content'];
                        $answer->order = $data['Order'];
                        $answer->is_correct = $data['IsExactly'];

                        if (!$answer->save()) {
                            var_dump($answer->errors);
                            Yii::error($answer->errors, $this->table);
                        }
                    }
                }
            } else {
                $v2Answers = Yii::$app->dbv2->createCommand("SELECT * FROM answers WHERE QuestionID = {$model->v2_id}")->queryAll();
                if (empty($v2Answers)) {
                    $model->type = QuizQuestion::TYPE_WRITING;
                } else {
                    $model->type = QuizQuestion::TYPE_ONE_CHOICE;
                }
                $model->save(false);
            }
        }
    }

    public function migrateMission($course_id, $course_v2_id)
    {
        $v2Missions = Yii::$app->dbv2->createCommand("SELECT * FROM coursemission WHERE CourseID = {$course_v2_id}")->queryAll();

        foreach ($v2Missions as $data) {
            $model = CourseMission::find()->andWhere(['v2_id' => $data['ID']])->one();
            if (!is_null($model)) {
                continue;
            }
            $model = new CourseMission();

            $model->v2_id = $data['ID'];
            $model->course_id = $course_id;
            $model->name = $data['Name'];
            $model->type = $data['TypeMission'];
            $model->day_can_do = $data['CurrentDay'];
            $model->max_executing_day = $data['ExecutedDay'];
            $model->lesson_id = $data['LessonID'];
            $model->description = $data['Description'];
            $model->bonus_delta_xu = $data['BonusDeltaXu'];
            $model->is_deleted = $data['is_deleted'];

            if (!$model->save()) {
                var_dump($model->errors);
                Yii::error($model->errors, $this->table);
            }
        }
    }
}
