<?php

namespace app\modules\sync\controllers;

use Yii;
use kyna\course\models\CourseDiscussion;
use common\helpers\StringHelper;
use app\modules\sync\models\Course;
use app\modules\sync\models\User;

class DiscussController extends \app\modules\sync\components\Controller
{
    
    public $table = 'posts';
    
    protected function setKey()
    {
        $key = StringHelper::random(20, true);

        $rets = Yii::$app->dbv2->createCommand("UPDATE {$this->table} SET `key` = '{$key}' WHERE `is_sync` = 0 AND `key` IS NULL AND `Object_Type` = 'lesson_comment' ORDER BY ID ASC LIMIT 1")->execute();
        
        return [$key, $rets];
    }
    
    protected function create($data)
    {
        $model = CourseDiscussion::find()->andWhere(['v2_id' => $data['ID']])->one();
        
        if (is_null($model)) {
            $model = new CourseDiscussion();
            
            $model->v2_id = $data['ID'];
            
            $course = Course::find()->where([
                'v2_id' => $data['ObjectID'],
                'type' => array_keys(Course::listTypes())
            ])->one();
            if (!is_null($course)) {
                $model->course_id = $course->id;
            }
            
            $learner = User::find()->where(['v2_id' => $data['UserID']])->one();
            if (!is_null($learner)) {
                $model->user_id = $learner->id;
            }
            
            if (!empty($data['CommentTo'])) {
                $toLearner = User::find()->where(['v2_id' => $data['CommentTo']])->one();
                if (!is_null($toLearner)) {
                    $model->comment_to_user_id = $toLearner->id;
                }
            }
            
            $model->comment = trim($data['Content']);
            
            if (!empty($data['ParentID'])) {
                $parent = CourseDiscussion::find()->where(['v2_id' => $data['ParentID']])->one();
                if (!is_null($parent)) {
                    $model->parent_id = $parent->id;
                } else {
                    return false;
                }
            }
            
            $createdDate = date_create_from_format("Y-m-d H:i:s", trim($data['CreatedDate']));
            if ($createdDate !== false) {
                $model->created_time = $createdDate->getTimestamp();
            }
            
            if ($data['IsSpam']) {
                $model->status = CourseDiscussion::STATUS_SPAM;
            }
        }

        if (!$model->save()) {
            var_dump($model->getErrors());
            Yii::error($model->errors, $this->table);
        }
        
        return $model;
    }
    
}
