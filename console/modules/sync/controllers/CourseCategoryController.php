<?php

namespace app\modules\sync\controllers;

use Yii;
use app\modules\sync\models\Category;

class CourseCategoryController extends \app\modules\sync\components\Controller
{
    
    public $table = 'categories';
    
    protected function create($data)
    {
        $model = Category::find()->andWhere(['v2_id' => $data['id']])->one();
        
        if (!is_null($model)) {
            return $model;
        }
        $model = new Category();
        
        $model->v2_id = $data['id'];
        $model->name = $data['name'];
        $model->slug = $data['slug'];
        $model->description = $data['description'];
        $model->order = $data['order'];
        $model->is_deleted = $data['is_deleted'];
        
        if (!$model->save()) {
            var_dump($model->errors);
            Yii::error($model->errors, $this->table);
            
            return false;
        }
        
        return $model;
    }
    
}
