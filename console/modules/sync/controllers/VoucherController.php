<?php

namespace app\modules\sync\controllers;

use Yii;
use app\modules\sync\models\Promotion;
use app\modules\sync\models\User;

class VoucherController extends \app\modules\sync\components\Controller
{
    
    public $table = 'voucher';
    
    public function create($data)
    {
        $model = Promotion::find()->andWhere([
            'kind' => Promotion::KIND_VOUCHER,
            'v2_id' => $data['id'],
        ])->one();
        if (is_null($model)) {
            $model = Promotion::find()->andWhere([
                'kind' => Promotion::KIND_VOUCHER,
                'code' => $data['code'],
            ])->one();
        }
        
        if (is_null($model)) {
            $model = new Promotion();

            $model->kind = Promotion::KIND_VOUCHER;
            $model->type = Promotion::TYPE_FEE;
            $model->code = $data['code'];
            $model->value = $data['value'];
            $model->note = $data['note'];
            
            if (!empty($data['seller_id'])) {
                $seller = User::find()->where(['v2_id' => $data['seller_id']])->one();
                $model->seller_id = !is_null($seller) ? $seller->id : 0;
            }
            
            if (!empty($data['user_id'])) {
                $user = User::find()->where(['v2_id' => $data['user_id']])->one();
                $model->user_id = !is_null($user) ? $user->id : 0;
            }

            if (!empty($data['creator_id'])) {
                $createdUser = User::find()->where(['v2_id' => $data['creator_id']])->one();
                $model->created_user_id = !is_null($createdUser) ? $createdUser->id : 0;
            }

            $model->is_deleted = $data['is_deleted'];
            
            $expiredDate = date_create_from_format("Y-m-d H:i:s", trim($data['deadline_date']));
            if ($expiredDate !== false) {
                $model->expiration_date = $expiredDate->getTimestamp();
            }

            $usedDate = date_create_from_format("Y-m-d H:i:s", trim($data['activated_date']));
            if ($usedDate !== false) {
                $model->used_date = $usedDate->getTimestamp();
                $model->is_used = Promotion::BOOL_YES;
            }
        }
        
        $createdDate = date_create_from_format("Y-m-d H:i:s", trim($data['created_date']));
        if ($createdDate !== false) {
            $model->created_time = $createdDate->getTimestamp();
        }
        
        $model->v2_id = $data['id'];
        
        if (!$model->save()) {
            var_dump($model->errors);
            Yii::error($model->errors, $this->table);
        }
        
        return $model;
    }
}
