<?php

namespace app\modules\sync\controllers;

use Yii;
use app\modules\sync\models\UserTelesale;
use kyna\user\models\actions\UserTelesaleAction;
use app\modules\sync\models\User;
use app\modules\sync\models\Course;

class UserTelesaleController extends \app\modules\sync\components\Controller
{
    
    public $table = 'user_telesales';
    
    public function init()
    {
        $ret = parent::init();
        
        $v3Table = UserTelesale::tableName();
        
        $result = Yii::$app->db->createCommand("SHOW COLUMNS FROM {$v3Table} like 'v2_id';")->queryOne();
        if ($result === false) {
            Yii::$app->db->createCommand("ALTER TABLE {$v3Table} 
                ADD COLUMN `v2_id` INT(11)
            ")->execute();
        }
        
        return $ret;
    }
    
    public function create($data)
    {
        $model = UserTelesale::find()->where(['v2_id' => $data['id']])->one();
        
        if (is_null($model)) {
            $model = new UserTelesale();
            
            $model->v2_id = $data['id'];
            $model->email = $data['email'];
            $model->phone_number = $data['phone_number'];
            $model->full_name = $data['fullname'];
            $model->type = $data['user_type'];
            $model->note = $data['notes'];
            $model->form_name = $data['form_name'];
            
            $existModel = UserTelesale::find()->andWhere(['email' => $model->email, 'type' => $model->type, 'form_name' => $model->form_name])->one();
            if (!is_null($existModel)) {
                if (is_null($existModel->v2_id)) {
                    $existModel->v2_id = $data['id'];
                    $existModel->save(FALSE);
                    
                    return $existModel;
                } else {
                    echo " --> Học viên đã đăng ký khóa học";
                    return false;
                }
            }
            
            $createdDate = date_create_from_format("Y-m-d H:i:s", trim($data['created_date']));
            if ($createdDate !== false) {
                $model->created_time = $createdDate->getTimestamp();
            }
            
            if (!empty($data['district_id'])) {
                $v2District = Yii::$app->dbv2->createCommand("SELECT * FROM district WHERE id = {$data['district_id']}")->queryOne();
                if (!empty($v2District['name'])) {
                    $model->location_id = $this->getLocationByDistrictText($v2District['name']);
                }
            }
            
            $v3User = User::find()->andWhere(['v2_id' => $data['user_id']])->one();
            if (!is_null($v3User)) {
                $model->user_id = $v3User->id;
            }
            
            $v2AssignUser = User::find()->andWhere(['v2_id' => $data['assign_id']])->one();
            if (!is_null($v2AssignUser)) {
                $model->tel_id = $v2AssignUser->id;
            }
            
            $callDate = date_create_from_format("Y-m-d H:i:s", trim($data['call_date']));
            if ($callDate !== false) {
                $model->last_call_date = $callDate->getTimestamp();
            }
            
            $model->bonus = $data['bonus'];
            $model->street_address = $data['street_address'];
            
            $recallDate = date_create_from_format("Y-m-d H:i:s", trim($data['recall_date']));
            if ($recallDate !== false) {
                $model->recall_date = date('d/m/Y H:i', $recallDate->getTimestamp());
            }
            
            $model->follow_num = $data['follow_num'];
            
            $affUser = User::find()->where(['v2_id' => $data['affiliate_id']])->one();
            if (!is_null($affUser)) {
                $model->affiliate_id = $affUser->id;
            }
            
            $course = Course::find()->where(['type' => array_keys(Course::listTypes()), 'v2_id' => $data['course_id']])->one();
            if (!is_null($course)) {
                $model->course_id = $course->id;
            }
            
            $oldAffUser = User::find()->where(['v2_id' => $data['old_affiliate_id']])->one();
            if (!is_null($oldAffUser)) {
                $model->old_affiliate_id = $oldAffUser->id;
            }
            
            $model->amount = $data['cod_money'];
            $model->is_success = $data['is_success'];
            $model->is_deleted = $data['is_deleted'];
            
            if ($model->save()) {
                if (!empty($data['notes'])) {
                    $this->saveNotes($model->id, $data);
                }
            }
        }
        
        if (!$model->save()) {
            var_dump($model->errors);
            Yii::error($model->errors, $this->table);
            return false;
        }
        
        return $model;
    }
    
    private function saveNotes($id, $data)
    {
        
        $noteRecs = explode('<br/>', $data['notes']);
        foreach ($noteRecs as $note) {
            if (empty($note)) {
                continue;
            }
            
            preg_match_all('/(\[?\d{4}\-\d{2}\-\d{2}\s\d{2}:\d{2}:?\d*\]?):([a-zA-Z0-9_]+):(.*)/', $note, $tmpData);

            if (count($tmpData) > 3) {
                if (!isset($tmpData[1][0]) || !isset($tmpData[2][0]) || !isset($tmpData[3][0])) {
                    continue;
                }
                $noteDate = str_replace(['[', ']'], '', $tmpData[1][0]);
                $noteUsername = trim($tmpData[2][0]);
                $noteDetail = $tmpData[3][0];

                $v2User = Yii::$app->dbv2->createCommand("SELECT id FROM users where username = '{$noteUsername}'")->queryOne();
                if (!empty($v2User)) {
                    $v3User = User::find()->andWhere(['v2_id' => $v2User['id']])->one();
                    if (!is_null($v3User)) {
                        $userTelesaleAction = new UserTelesaleAction();
                        $userTelesaleAction->user_telesale_id = $id;
                        $userTelesaleAction->user_id = $v3User->id;
                        $userTelesaleAction->name = UserTelesale::EVENT_NOTED;
                        $userTelesaleAction->note = $noteDetail;

                        $actionDate = date_create_from_format("Y-m-d H:i", trim($noteDate));
                        if ($actionDate !== false) {
                            $userTelesaleAction->action_time = $actionDate->getTimestamp();
                        }

                        $userTelesaleAction->save(false);
                    }
                }
            } else {
                var_dump($noteRecs, $tmpData);die;
            }
        }
    }
}

