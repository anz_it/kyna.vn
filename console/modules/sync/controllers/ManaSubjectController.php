<?php
/**
 * Created by PhpStorm.
 * User: Hien Nguyen
 * Date: 10/6/2016
 * Time: 6:41 PM
 */

namespace app\modules\sync\controllers;


use kyna\mana\models\Subject;
use Yii;

class ManaSubjectController extends \app\modules\sync\components\Controller
{
    public $table = 'mana_subjects';

    public function create($data)
    {
        $subject = Subject::find()->where(['v2_id' => $data['id']])->one();
        if (empty($subject)) {
            $subject = new Subject();
            $subject->name = $data['name'];
            $subject->slug = $data['slug'];
            $subject->description = $data['description'];
            $subject->image_url = $data['image'];
            $subject->order = $data['order'];
            $subject->status = $data['is_deleted'];
            $subject->v2_id = $data['id'];

            $subjectParent = Subject::find()->where(['v2_id' => $data['parent']])->one();
            if ($subjectParent) {
                $subject->parent_id = $subjectParent->id;
            }
            if (!$subject->save(false)) {
                Yii::error($subject->errors, $this->table);
                return false;
            }
        }

        return $subject;
    }
}