<?php

namespace app\modules\sync\models;

class Category extends \kyna\course\models\Category
{

    protected static function softDelete()
    {
        return FALSE;
    }
}
