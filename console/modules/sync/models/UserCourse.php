<?php
/**
 * @author: Hong Ta
 * @desc: User course model
 */

namespace app\modules\sync\models;

class UserCourse extends \kyna\user\models\UserCourse
{

    public static function softDelete()
    {
        return FALSE;
    }

}