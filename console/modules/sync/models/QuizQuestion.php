<?php

namespace app\modules\sync\models;

class QuizQuestion extends \kyna\course\models\QuizQuestion
{
    
    protected static function softDelete()
    {
        return FALSE;
    }
}
