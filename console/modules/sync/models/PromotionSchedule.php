<?php

namespace app\modules\sync\models;

class PromotionSchedule extends \kyna\promo\models\PromotionSchedule
{

    protected static function softDelete()
    {
        return FALSE;
    }
}
