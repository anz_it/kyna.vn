<?php

namespace app\modules\sync\models;

class QuizAnswer extends \kyna\course\models\QuizAnswer
{

    protected static function softDelete()
    {
        return FALSE;
    }

}
