<?php

namespace app\modules\sync\models;

class AffiliateUser extends \kyna\commission\models\AffiliateUser
{

    protected static function softDelete()
    {
        return FALSE;
    }
}
