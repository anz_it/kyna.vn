<?php

namespace app\modules\sync\models;

class User extends \kyna\user\models\User
{

    protected static function softDelete()
    {
        return FALSE;
    }
    
    public function rules()
    {
        $rules = parent::rules();
        
        unset($rules['emailPattern']);
        
        return $rules;
    }
}
