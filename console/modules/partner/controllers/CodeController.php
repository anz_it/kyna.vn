<?php

namespace app\modules\partner\controllers;

use kyna\order\models\Order;
use kyna\partner\models\Category;
use kyna\partner\models\Code;
use kyna\partner\models\Partner;
use kyna\partner\models\Transaction;
use yii;

class CodeController extends yii\console\Controller
{

    public function actionUpdateStatus()
    {
        $transactions = Transaction::find()->orderBy('id DESC')->all();
        $date = date('c');
        echo "Date: {$date}\n";
        foreach ($transactions as $transaction) {
            // call API update code status
            $partnerCode = Code::findOne(['code' => $transaction->code]);
            $client = $partnerCode->partner->client;
            if ($client) {
                $result = $client->query($transaction->id);
                if (isset($result['used']) && is_integer($result['used'])) {
                    $transaction->num_used = intval($result['used']);
                    $transaction->save();
                } else {
                    echo "- Code Fail: {$transaction->code}\n";
                }
            }
        }
    }

    public function actionMigrateCode($partnerID) {
        $partner = Partner::findOne($partnerID);
        if (!empty($partner)) {
            Category::updateAll(['partner_id' => $partnerID]);
            Code::updateAll(['partner_id' => $partnerID]);
            Transaction::updateAll(['partner_id' => $partnerID]);
            echo "Done";
        } else {
            echo "Partner does not exists";
        }
    }

    public function actionMoveCode()
    {
        $orders = Order::find()
            ->alias('o')
            ->innerJoin(Code::tableName(), Code::tableName() . ".serial = o.activation_code")
            ->all();
        if ($orders) {
            foreach ($orders as $order) {
                $details = $order->details;
                foreach ($details as $detail) {
                    $course = $detail->course;
                    if ($course && $course->isPartner && empty($detail->activation_code)) {
                        $detail->activation_code = $order->activation_code;
                        $detail->save(false);
                        echo "Order #{$order->id} - {$course->id}: {$course->name}\n";
                    }
                }
            }
        }
        echo "Done\n";
    }
}

