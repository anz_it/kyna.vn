<?php

namespace app\modules\order\controllers;

use kyna\commission\Commission;
use kyna\commission\models\CommissionIncome;
use kyna\order\models\Order;
use Yii;
use yii\console\Controller;
use yii\helpers\ArrayHelper;

class CommissionController extends Controller
{

    public function actionUpdate($fromDate = '2018-04-04')
    {
        echo "Starting Update Commission for order missing...\n";
        $commissionIncomeTbl = CommissionIncome::tableName();
        $orders = Order::find()
            ->alias('o')
            ->leftJoin($commissionIncomeTbl, "{$commissionIncomeTbl}.order_id = o.id")
            ->where("{$commissionIncomeTbl}.id IS NULL")
            ->andWhere(['o.status' => Order::ORDER_STATUS_COMPLETE])
            ->andWhere("o.activation_date > UNIX_TIMESTAMP('{$fromDate}T00:00:00')")
            ->all();
        if (empty($orders)) {
            echo "No order found!";
            return;
        }
        foreach ($orders as $order) {
            echo "Order {$order->id} - ";
            $details = $order->details;
            foreach ($details as $detail) {
                Commission::calculate($order->id, $order->subTotal, $order->total, $order->totalCourseDiscount, $order->totalDiscount, $order->paymentFee, $detail->course_id, $detail->itemTotal, $order->affiliate_id, $detail->course_combo_id);
            }
            \Yii::info("#{$order->id}", 'commission');
            echo "Success\n";
        }
        echo "Finish";
    }

    public function actionFixOrderZero()
    {
        //
        echo "Update commission.....\n";

        $orders = Order::find()->where(['>=', 'order_date', strtotime('2018-03-01')])->andWhere(['total'=>0])->asArray()->all();
        $oder_ids = ArrayHelper::getColumn($orders, 'id');
        $updateRows = CommissionIncome::find()->andWhere(['>', 'total_amount', 0])->andWhere(['>', 'affiliate_commission_amount', 0])->andWhere(['in', 'order_id', $oder_ids])->all();
        echo "Rows need to update: ".count($updateRows);
        echo "\n";
        echo "Press 1 to update, pesss 2 to only export,  3 to cancel\n";
        $answer = trim(fgets(STDIN));
        if($answer == '1' || $answer == '2') {
            $count = 0;
            $commissions = [];
            if (!empty($updateRows)) {
                foreach ($updateRows as $record) {
                    $comm = [];
                    $comm['id'] = $record->id;
                    $comm['order_id'] = $record->order_id;
                    $comm['old_total_amount'] = $record->total_amount;
                    $comm['old_aff_commission_amount'] = $record->affiliate_commission_amount;
                    $comm['new_total_amount'] = 0;
                    $comm['new_aff_commission_amount'] = 0;
                    if($answer == '1')
                    {
                        $record->total_amount = 0;
                        $record->affiliate_commission_amount = 0;
                        if ($record->save(false)) {
                            $commissions[] = $comm;
                            $count++;
                        }
                    } else {
                        $commissions[] = $comm;
                        $count++;
                    }
                }
                $this->export($commissions);
            }

            echo "Updated: " . $count . " rows";
        }
        echo "\nQuit";
    }


    private function export($commissions)
    {
        ini_set('max_execution_time', 0);


        $output = fopen(\Yii::getAlias('@runtime').'/logs/commission_'.time().'.csv', 'w');
        fputs($output, "\xEF\xBB\xBF");

        fputcsv($output, [
            'ID',
            'Order ID',
            'Old Total',
            'Old Affiliate Commission Amount',
        ]);

        foreach ($commissions as $commission) {
            fputcsv($output, [$commission['id'], $commission['order_id'], $commission['old_total_amount'], $commission['old_aff_commission_amount']]);
        }

        fclose($output);
    }
}