<?php

namespace app\modules\order\controllers;

use kyna\commission\Commission;
use kyna\commission\models\CommissionIncome;
use kyna\order\models\Order;
use Yii;
use yii\console\Controller;

use common\helpers\ArrayHelper;

class AffiliateController extends Controller
{

    public function actionUpdateManager()
    {
        echo "Start update manager commission\n";
        $commisionIncoms = CommissionIncome::find()
            ->where('affiliate_manager_id > 0')
            ->all();
        if (!empty($commisionIncoms)) {
            /* @var CommissionIncome $item */
            foreach ($commisionIncoms as $item) {
                echo "{$item->id} - Manager ID: {$item->affiliate_manager_id} \n";
                $item->affiliate_manager_amount = floor($item->total_amount * 2 / 100);
                $item->save(false);
            }
        }
        echo "Finish update manager commission\n";
    }


    public function actionCalculateCommissionOrder($orderId)
    {
        $order = Order::findOne(['id'=>$orderId]);
        if(!empty($order) && !CommissionIncome::find()->where(['order_id'=>$orderId])->exists())
        {
            $details = $order->details;
            foreach ($details as $detail) {
                Commission::calculate($order->id, $order->subTotal, $order->total, $order->totalCourseDiscount, $order->totalDiscount, $order->paymentFee, $detail->course_id, $detail->itemTotal, $order->affiliate_id, $detail->course_combo_id);
            }
        } else {
            echo 'can find order \n';
        }
    }
}