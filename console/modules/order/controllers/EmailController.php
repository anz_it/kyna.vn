<?php

namespace app\modules\order\controllers;

use Yii;
use yii\console\Controller;

use common\helpers\ArrayHelper;

use kyna\settings\models\Setting;
use kyna\order\models\Order;

class EmailController extends Controller
{

    public function actionRequestPayment($dayToReSend = 3, $limit = null, $ignoreAfterMonths = 1)
    {
        if ($limit == null) {
            $limit = Yii::$app->params['pageSize'];
        }

        $orders = Order::find()
            ->select(['id', 'user_id'])
            ->where([
                'status' => [
                    Order::ORDER_STATUS_PAYMENT_FAILED,
                    Order::ORDER_STATUS_REQUESTING_PAYMENT
                ]
            ])
            ->andWhere(['or', ['<', 'sent_payment_request_time', strtotime("-$dayToReSend days")], ['sent_payment_request_time' => null]])
            ->andWhere(['>=', 'order_date', strtotime("-$ignoreAfterMonths month" . ($ignoreAfterMonths > 1 ? 's' : ''))])
            ->limit($limit)->all();

        if (count($orders) > 0) {
            foreach ($orders as $order) {
                Order::updateAll(['sent_payment_request_time' => time()], ['id' => $order->id]);

                $mailer = Yii::$app->mailer;
                $mailer->htmlLayout = '@common/mail/layouts/main';
                $mailer->compose('@common/mail/order/request_payment', [
                    'order' => $order,
                    'settings' => ArrayHelper::map(Setting::find()->all(), 'key', 'value')
                ])
                    ->setTo($order->user->email)
                    ->setSubject('Yêu cầu thanh toán đơn hàng #' . $order->id . ' tại Kyna.vn')
                    ->send();

                Yii::info("Sent payment request order #{$order->id}", 'order');
            }
        } else {
            Yii::info("No payment request", 'order');
        }
    }
}