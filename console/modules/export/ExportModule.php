<?php

namespace app\modules\export;

class ExportModule extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\export\controllers';

    public function init()
    {
        parent::init();
    }
}
