<?php

namespace app\modules\export\controllers;

use common\components\ExportExcel;
use kyna\course\models\Course;
use yii;
use yii\console\Controller;
use kyna\order\models\Order;
use kyna\user\models\UserCourse;

class OrderReportController extends Controller
{
    private $email = '';

    /**
     * @param $sql
     * Get from $dataProvider->query->createCommand()->getRawSql()
     */
    public function actionExport($sql, $email)
    {
        $date = date('c');
        $startTime = time();
        // Get result from query
        $results = $this->_queryResults($sql);

        // format data
        $data['header'] = $this->_getGridColumns();
        $data['content'] = $this->_formatContentRows($results);

        // render Excel
        $excelRunner = new ExportExcel();
        $fileName = 'report_export_'.date('Y-m-d_H-i', time()) . '.xls';
        $excelRunner->renderData($data, $email, $fileName);

        /*
         * Log executed time
         */
        $endTime = time();
        $executedTime = $endTime - $startTime;
        echo "Date: {$date}"
            ."\n SQL: {$sql}"
            ."\n Email: {$email}"
            ."\n Executed Time: {$executedTime}"
        ;

        echo "\n Done \n";
    }

    /**
     * Format Content Row
     * @param $results
     * @return array
     */
    private function _formatContentRows($results)
    {
        $returnData = [];
        if (count($results) > 0) {
            $row = 2;
            foreach ($results as $item) {
                $rowData = null;
                $model = Order::findOne($item['id']);
                foreach ($this->_getGridColumns() as $column => $label) {
                    $value = null;
                    switch ($column) {
                        case 'inc':
                            $value = $row - 1;
                            break;
                        case 'order_date';
                            $value = Yii::$app->formatter->asDatetime(!empty($item['created_time']) ? $item['created_time'] : null);
                            break;
                        case 'activation_code':
                            $detail = $model->getDetails()->one();
                            if ($detail) {
                                $course = $detail->course;
                                if ($course->isMonkeyJunior) {
                                    $value = $model->activation_code;
                                }
                            }
                            break;
                        case 'activation_date':
                            $detail = $model->getDetails()->one();
                            if ($detail) {
                                $userCourseModel = UserCourse::find()->where([
                                    'user_id' => $model->user_id,
                                    'course_id' => $detail->course_id,
                                    'is_activated' => UserCourse::BOOL_YES,
                                ])->one();
                                if ($userCourseModel) {
                                    $value = Yii::$app->formatter->asDatetime(!empty($userCourseModel->activation_date) ? $userCourseModel->activation_date : null);
                                }
                            }
                            break;
                        case 'user';
                            $user = $model->user;
                            if ($user && $user->profile) {
                                $value = $user->profile->name;
                            }
                            break;
                        case 'phone_number':
                            $user = $model->user;
                            if (!empty($user)) {
                                if ($user->profile && !is_null($user->profile->phone_number)) {
                                    $value = $user->profile->phone_number;
                                } else {
                                    if ($user->userAddress) {
                                        $value = $user->userAddress->phone_number;
                                    }
                                }
                            }
                            break;
                        case 'email':
                            $user = $model->user;
                            if (!empty($user)) {
                                $value = $user->email;
                            }
                            break;
                        case 'location_city':
                            $shippingAddress = $model->shippingAddress;
                            if ($shippingAddress && $shippingAddress->location && $shippingAddress->location->parent) {
                                $value = $shippingAddress->location->parent->name;
                            }
                            break;
                        case 'location_district':
                            $shippingAddress = $model->shippingAddress;
                            if ($shippingAddress && $shippingAddress->location) {
                                $value = $shippingAddress->location->name;
                            }
                            break;
                        case 'address':
                            $shippingAddress = $model->shippingAddress;
                            if ($shippingAddress) {
                                $value = $shippingAddress->street_address;
                            }
                            break;
                        case 'fee':
                            if ($model->isCod) {
                                if (!empty($model->shippingAddress->fee)) {
                                    $value = $model->shippingAddress->fee;
                                }
                            }
                            break;
                        case 'operator_id':
                            $affiliate = $model->affiliateLastUser;
                            if ($affiliate && $affiliate->user && $affiliate->user->profile) {
                                $value = $affiliate->user->profile->name;
                            }
                            break;
                        case 'affiliate_id':
                            $affiliate = $model->originalAffiliateUser;
                            if ($affiliate && $affiliate->user) {
                                $value = $affiliate->user->email;
                            }
                            break;
                        case 'category':
                            $affiliate = $model->originalAffiliateUser;
                            if ($affiliate && $affiliate->affiliateCategory) {
                                $value = $affiliate->affiliateCategory->name;
                            }
                            break;
                        case 'status':
                            $html = "";
                            $html .=  $model->statusLabel;
                            $value = $html;
                            break;
                        case 'operator':
                            $user = $model->operator;
                            if ($user && $user->profile) {
                                $value = $user->profile->name;
                            }
                            break;
                        case 'shipping_code':
                            /*$data = $model->shippingAddress;
                            if ($data) {
                                $value = $data->shipping_code;
                            }*/
                            $payment_transaction = \kyna\payment\models\PaymentTransaction::find()->where(['order_id' => $model->id])->orderBy(['id' => SORT_DESC])->one();
                            $value = !empty($payment_transaction->transaction_code) ? $payment_transaction->transaction_code : null;
                            break;
                        case 'details':
                            $value = strip_tags($model->getDetailsText());
                            break;
                        default:
                            if (isset($item[$column])) {
                                $value = $item[$column];
                            }
                            break;
                    }
                    $rowData[$column] = $value;
                }
                array_push($returnData, $rowData);
                $row++;
            }
            unset($rowData);
        }
        return $returnData;
    }

    /**
     * Get result from query
     * @param $sql
     * @return array
     */
    private function _queryResults ($sql) {
        $connection = Yii::$app->getDb();
        $command = $connection->createCommand($sql);
        $results = $command->queryAll();
        return $results;
    }

    /**
     * Define columns
     * @return array
     */
    private function _getGridColumns()
    {
        $gridColumns = [
            'inc' => '#',
            'id' => 'Order ID',
            'order_date' => 'Ngày đăng ký',
            'activation_code' => 'Mã kích hoạt Monkey Junior',
            'activation_date' => 'Ngày kích hoạt',
            'user' => 'Họ tên',
            'phone_number' => 'Số điện thoại',
            'email' => 'Email',
            'location_city' => 'Tỉnh thành',
            'location_district' => 'Quyện huyện',
            'address' => 'Địa chỉ',
            'details' => 'Sản phẩm',
            'total' => 'Giá trị đơn hàng thực tế (sau khuyến mãi)',
            'fee' => 'Chi phí giao nhận (học viên phải chịu)',
            'payment_fee' => 'Chi phí giao nhận (Kyna trả GHN, đối tác)',
            'realInCome' => 'Giá trị đơn hàng thuần',
            'operator_id' => 'Affiliate cuối',
            'affiliate_id' => 'Original affiliate',
            'category' => 'Affiliate category',
            'status' => 'Status',
            'operator' => 'Telesales',
            'payment_method' => 'Đơn vị giao nhận',
            'shipping_code' => 'Mã đơn hàng giao nhận',
        ];
        return $gridColumns;
    }
}