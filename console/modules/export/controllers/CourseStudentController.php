<?php

namespace app\modules\export\controllers;

use common\components\ExportExcel;
use kyna\user\models\UserCourse;
use yii;
use yii\console\Controller;

class CourseStudentController extends Controller
{
    /**
     * @param $sql
     * Get from $dataProvider->query->createCommand()->getRawSql()
     */
    public function actionExport($sql, $email)
    {
        $date = date('c');
        $startTime = time();
        // Get result from query
        $results = $this->_queryResults($sql);

        // format data
        $data['header'] = $this->_getGridColumns();
        $data['content'] = $this->_formatContentRows($results);

        // render Excel
        $excelRunner = new ExportExcel();
        $fileName = 'course_student_export_' . date('Y-m-d_H-i', time()) . '.xls';
        $excelRunner->renderData($data, $email, $fileName);

        /*
         * Log executed time
         */
        $endTime = time();
        $executedTime = $endTime - $startTime;
        echo "Date: {$date}"
            . "\n SQL: {$sql}"
            . "\n Email: {$email}"
            . "\n Executed Time: {$executedTime}";

        echo "\n Done \n";
    }

    /**
     * Format Content Row
     * @param $results
     * @return array
     */
    private function _formatContentRows($results)
    {
        $returnData = [];
        if (count($results) > 0) {
            $row = 2;
            foreach ($results as $item) {
                $rowData = null;
                $model = UserCourse::findOne($item['id']);
                foreach ($this->_getGridColumns() as $column => $label) {
                    $value = null;
                    switch ($column) {
                        case 'inc':
                            $value = $row - 1;
                            break;
                        case 'user':
                            if ($model->student && $model->student->profile) {
                                $value = !is_null($model->student->profile->name) ? $model->student->profile->name : $model->student->username;
                            }
                            break;
                        case 'email':
                            if ($model->student) {
                                $value = $model->student->email;
                            }
                            break;
                        case 'phone_number':
                            if ($model->student && $model->student->profile) {
                                $value = !is_null($model->student->profile->phone_number) ? $model->student->profile->phone_number : $model->student->phone;
                            }
                            break;
                        case 'created_time':
                            $value = Yii::$app->formatter->asDatetime($model->created_time);
                            break;
                        case 'is_started':
                            if ($model->is_started == UserCourse::BOOL_NO) {
                                $value = UserCourse::BOOL_NO_TEXT;
                            } else {
                                $value = UserCourse::BOOL_YES_TEXT;
                            }
                            break;
                        case 'is_graduated':
                            if ($model->is_graduated == UserCourse::BOOL_NO) {
                                $value = UserCourse::BOOL_NO_TEXT;
                            } else {
                                $value = UserCourse::BOOL_YES_TEXT;
                            }
                            break;
                        case 'is_activated':
                            if ($model->is_activated == UserCourse::BOOL_NO) {
                                $value = UserCourse::BOOL_NO_TEXT;
                            } else {
                                $value = UserCourse::BOOL_YES_TEXT;
                            }
                            break;
                        case 'is_quick':
                            if ($model->is_quick == UserCourse::BOOL_NO) {
                                $value = UserCourse::BOOL_NO_TEXT;
                            } else {
                                $value = UserCourse::BOOL_YES_TEXT;
                            }
                            break;
                        case 'activation_date':
                            $value = Yii::$app->formatter->asDatetime($model->activation_date);
                            break;
                        default:
                            break;
                    }
                    $rowData[$column] = $value;
                }
                array_push($returnData, $rowData);
                $row++;
            }
            unset($rowData);
        }
        return $returnData;
    }

    /**
     * Get result from query
     * @param $sql
     * @return array
     */
    private function _queryResults($sql)
    {
        $connection = Yii::$app->getDb();
        $command = $connection->createCommand($sql);
        $results = $command->queryAll();
        return $results;
    }

    /**
     * Define columns
     * @return array
     */
    private function _getGridColumns()
    {
        $gridColumns = [
            'inc' => '#',
            'user' => 'Tên học viên',
            'email' => 'Email',
            'phone_number' => 'SĐT đăng ký',
            'created_time' => 'Ngày đăng ki',
            'is_started' => 'Đã bắt đầu',
            'is_graduated' => 'Đã tốt nghiệp',
            'is_activated' => 'Đã activated',
            'is_quick' => 'Học cấp tốc',
            'activation_date' => 'Ngày kích hoạt'
        ];
        return $gridColumns;
    }
}