<?php

namespace app\modules\export\controllers;

use common\components\ExportExcel;
use kyna\partner\models\Code;
use yii;
use yii\console\Controller;

class PartnerCodeRetailerController extends Controller
{

    /**
     * @param $sql
     * @param $email
     * Get from $dataProvider->query->createCommand()->getRawSql()
     */
    public function actionExport($sql, $email)
    {
        $date = date('c');
        $startTime = time();
        // Get result from query
        $results = $this->_queryResults($sql);

        // format data
        $data['header'] = $this->_getGridColumns();
        $data['content'] = $this->_formatContentRows($results);

        // render Excel
        $excelRunner = new ExportExcel();
        $fileName = 'partner_code_'.date('Y-m-d_H-i', time()) . '.xls';
        $excelRunner->renderData($data, $email, $fileName);

        /*
         * Log executed time
         */
        $endTime = time();
        $executedTime = $endTime - $startTime;
        echo "Date: {$date}"
            ."\n SQL: {$sql}"
            ."\n Email: {$email}"
            ."\n Executed Time: {$executedTime}"
        ;

        echo "\n Done \n";
    }

    /**
     * Format Content Row
     * @param $results
     * @return array
     */
    private function _formatContentRows($results)
    {
        $returnData = [];
        if (count($results) > 0) {
            $row = 2;
            foreach ($results as $item) {
                $rowData = null;
                $model = Code::findOne($item['id']);
                foreach ($this->_getGridColumns() as $column => $label) {
                    $value = null;
                    switch ($column) {
                        case 'id':
                            $value = $item['id'];
                            break;
                        case 'serial';
                            $value = $item['serial'];
                            break;
                        case 'created_time':
                            $value = date('Y-m-d H:i', $item['created_time']);
                            break;
                        case 'partner':
                            $value = !empty($model->partner) ? $model->partner->name : null;
                            break;
                        case 'category_id';
                            $value = (!empty($model->category)) ? $model->category->title : null;
                            break;
                        case 'status';
                            $value = $model->statusText;
                            break;
                        case 'payment_method';
                            $value = $model->paymentMethodText;
                            break;
                        default:
                            if (isset($item[$column])) {
                                $value = $item[$column];
                            }
                            break;
                    }
                    $rowData[$column] = $value;
                }
                array_push($returnData, $rowData);
                $row ++;
            }
            unset($rowData);
        }
        return $returnData;
    }

    /**
     * Get result from query
     * @param $sql
     * @return array
     */
    private function _queryResults ($sql) {
        $connection = Yii::$app->getDb();
        $command = $connection->createCommand($sql);
        $results = $command->queryAll();
        return $results;
    }

    /**
     * Define columns
     * @return array
     */
    private function _getGridColumns()
    {
        $gridColumns = [
            'id' => 'ID',
            'serial' => 'Serial Code',
            'created_time' => 'Ngày tạo',
            'partner' => 'Partner',
            'category_id' => 'Sản phẩm',
            'status' => 'Trạng thái',
            'payment_method' => 'Hình thức thanh toán'
        ];
        return $gridColumns;
    }
}