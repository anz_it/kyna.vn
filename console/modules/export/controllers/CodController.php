<?php

namespace app\modules\export\controllers;

use common\components\ExportExcel;
use kyna\course\models\Course;
use kyna\payment\models\PaymentMethod;
use yii;
use yii\console\Controller;
use kyna\order\models\Order;
use kyna\user\models\UserCourse;
use kyna\payment\models\PaymentTransaction;
use common\helpers\ArrayHelper;

class CodController extends Controller
{
    private $email = '';

    /**
     * @param $sql
     * Get from $dataProvider->query->createCommand()->getRawSql()
     */
    public function actionExport($sql, $email, $location = null)
    {
        $date = date('c');
        $startTime = time();
        // Get result from query
        $results = $this->_queryResults($sql);
        $results = ArrayHelper::index($results, 'id');
        if (!empty($location)) {
            $codMethod = PaymentMethod::find()->where(['class' => 'cod'])->one();
            $codSetting = $codMethod->clientSettings;
            $results = $this->_filterData($results, $codSetting, $location);
        }

        // format data
        $data['header'] = $this->_getGridColumns();
        $data['content'] = $this->_formatContentRows($results);

        // render Excel
        $excelRunner = new ExportExcel();
        $fileName = 'cod_export_'.date('Y-m-d_H-i', time()) . '.xls';
        $excelRunner->renderData($data, $email, $fileName);

        /*
         * Log executed time
         */
        $endTime = time();
        $executedTime = $endTime - $startTime;
        echo "Date: {$date}"
            ."\n SQL: {$sql}"
            ."\n Email: {$email}"
            ."\n Executed Time: {$executedTime}"
        ;

        echo "\n Done \n";
    }

    /**
     * @param $data
     * @param $settings
     * @param $location_id
     * @return array|null
     */
    private function _filterData($data, $settings, $location_id)
    {
        if (!empty($data) && !empty($location_id)) {
            $all_data = null;
            $filter_data = null;
            // Get all data at warehouse
            foreach ($data as $key => $item) {
                $item = Order::findOne($item['id']);
                switch ($item->shipping_method) {
                    case 'ghn':
                        // lay pickupid by ghn setting (ghn.ini)
                        if (!empty($item->shippingAddress->location->parent_id)) {
                            $ghn = new \kyna\payment\lib\ghn\Ghn();
                            $provId = ($item->shippingAddress->location->parent_id);
                            $pickupId = $ghn->settings['Locations'][$provId]['pickup'];
                            // convert to Cod pickup id
                            $_pickupId = $settings['Pickups'][Yii::$app->params['cod']['GhnAlias']['pickupId'][$pickupId]];
                            if (isset($pickupId))
                                $all_data[$key][$_pickupId] = $item;
                        }
                        break;
                    case 'proship':
                        // Get pick up id from payment_transaction
                        $transactions = PaymentTransaction::findOne([
                            'status' => PaymentTransaction::STATUS_ACTIVE,
                            'order_id' => $item->id
                        ]);
                        if (isset($transactions)) {
                            $pickupId = $transactions['pick_up_location_id'];
                            // convert to Cod pickup id
                            $_pickupId = $settings['Pickups'][Yii::$app->params['cod']['ProshipAlias']['pickupId'][$pickupId]];
                            if (isset($_pickupId))
                                $all_data[$key][$_pickupId] = $item;
                        }
                        break;
                    case 'ghtk':
                        // Get pick up id from payment_transaction
                        $transactions = PaymentTransaction::findOne([
                            'status' => PaymentTransaction::STATUS_ACTIVE,
                            'order_id' => $item->id
                        ]);
                        if (isset($transactions)) {
                            $pickupId = $transactions['pick_up_location_id'];
                            // convert to Cod pickup id
                            $_pickupId = $settings['Pickups'][Yii::$app->params['cod']['GhtkAlias']['pickupId'][$pickupId]];
                            if (isset($_pickupId))
                                $all_data[$key][$_pickupId] = $item;
                        }
                        break;
                    case 'default':
                        break;
                }
            }
            // Filter by warehouse ID
            foreach ($all_data as $item) {
                if (empty($item[$location_id]))
                    continue;
                $filter_data[] = $item[$location_id];
            }
            return $filter_data;
        }
        return $data;
    }

    /**
     * Format Content Row
     * @param $results
     * @return array
     */
    private function _formatContentRows($results)
    {
        $returnData = [];
        if (count($results) > 0) {
            $row = 2;
            foreach ($results as $item) {
                $rowData = null;
                $model = Order::findOne($item['id']);
                $paymentTransaction = \kyna\payment\models\PaymentTransaction::find()->where(['order_id' => $model->id])->orderBy(['id' => SORT_DESC])->one();
                foreach ($this->_getGridColumns() as $column => $label) {
                    $value = null;
                    switch ($column) {
                        case 'inc':
                            $value = $row - 1;
                            break;
                        case 'transaction_code':
                            $value = !empty($paymentTransaction->transaction_code) ? $paymentTransaction->transaction_code : '';
                            break;
                        case 'bill_code':
                            $value = !empty($paymentTransaction->bill_code) ? $paymentTransaction->bill_code : '';
                            break;
                        case 'activation_code':
                            $value = $model->activation_code;
                            break;
                        case 'status':
                            $value = $model->statusLabel;
                            break;
                        case 'contact_name';
                            $value = !empty($model->user->userAddress->contact_name) ? $model->user->userAddress->contact_name : '';
                            break;
                        case 'phone_number':
                            $value = !empty($model->user->userAddress->phone_number) ? $model->user->userAddress->phone_number : '';
                            break;
                        case 'shipping_method':
                            $value = !empty($model->shipping_method) ? $model->shipping_method : '';
                            break;
                        default:
                            if (isset($item[$column])) {
                                $value = $item[$column];
                            }
                            break;
                    }
                    $rowData[$column] = $value;
                }
                array_push($returnData, $rowData);
                $row++;
            }
            unset($rowData);
        }
        return $returnData;
    }

    /**
     * Get result from query
     * @param $sql
     * @return array
     */
    private function _queryResults ($sql) {
        $connection = Yii::$app->getDb();
        $command = $connection->createCommand($sql);
        $results = $command->queryAll();
        return $results;
    }

    /**
     * Define columns
     * @return array
     */
    private function _getGridColumns()
    {
        $gridColumns = [
            'inc' => '#',
            'id' => 'Mã đơn hàng - Hệ thống Kyna',
            'transaction_code' => 'Mã đơn hàng - Giao vận',
            'bill_code' => 'Bill Code - Proship',
            'activation_code' => 'Code hoặc số Series [Dùng Tham Chiếu]',
            'status' => 'Trạng thái',
            'contact_name' => 'Họ và tên',
            'phone_number' => 'Số điện thoại',
            'shipping_method' => 'Đơn vị giao vận',
        ];
        return $gridColumns;
    }
}