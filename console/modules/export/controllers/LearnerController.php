<?php

namespace app\modules\export\controllers;

use common\components\ExportExcel;
use yii;
use yii\console\Controller;
use kyna\order\models\Order;

class LearnerController extends Controller
{
    /**
     * @param $sql
     * Get from $dataProvider->query->createCommand()->getRawSql()
     */
    public function actionExport($sql, $email)
    {
        $date = date('c');
        $startTime = time();
        // Get result from query
        $results = $this->_queryResults($sql);

        // format data
        $data['header'] = $this->_getGridColumns();
        $data['content'] = $this->_formatContentRows($results);

        // render Excel
        $excelRunner = new ExportExcel();
        $fileName = 'learner_export_'.date('Y-m-d_H-i', time()) . '.xls';
        $excelRunner->renderData($data, $email, $fileName);

        /*
         * Log executed time
         */
        $endTime = time();
        $executedTime = $endTime - $startTime;
        echo "Date: {$date}"
            . "\n SQL: {$sql}"
            . "\n Email: {$email}"
            . "\n Executed Time: {$executedTime}";

        echo "\n Done \n";
    }

    /**
     * Format Content Row
     * @param $results
     * @return array
     */
    private function _formatContentRows($results)
    {
        $returnData = [];
        if (count($results) > 0) {
            $row = 2;
            foreach ($results as $item) {
                $rowData = null;
                $model = Order::findOne($item['id']);
                foreach ($this->_getGridColumns() as $column => $label) {
                    $value = null;
                    switch ($column) {
                        case 'inc':
                            $value = $row - 1;
                            break;
                        case 'id':
                            $value = $model->id;
                            break;
                        case 'created_time';
                            $value = Yii::$app->formatter->asDatetime($model->created_time);
                            break;
                        case 'statusLabel':
                            if ($model->statusLabel) {
                                $value = $model->statusLabel;
                            }
                            break;
                        case 'detailsText':
                            if ($model->exportDetailsText) {
                                $value = $model->exportDetailsText;
                            }
                            break;
                        case 'user':
                            if ($model->user && $model->user->profile) {
                                $value = !is_null($model->user->profile->name)?$model->user->profile->name:$model->user->username;
                            }
                            break;
                        case 'email':
                            if ($model->user) {
                                $value = $model->user->email;
                            }
                            break;
                        case 'phone_number':
                            if ($model->user && $model->user->profile) {
                                $value = !is_null($model->user->profile->phone_number)?$model->user->profile->phone_number:$model->user->phone;
                            }
                            break;
                        case 'shipping_phone_number':
                            if ($model->isCod) {
                                $value = $model->shippingAddress->phone_number;
                            }
                            break;
                        case 'original_affiliate':
                            if ($model->originalAffiliateUser) {
                                $value = $model->originalAffiliateUser->user->email;
                            }
                            break;
                        case 'address':
                            if ($model->isCod) {
                                $value = $model->shippingAddress->location->parent->name;
                            }
                            break;
                        default:
                            break;
                    }
                    $rowData[$column] = $value;
                }
                array_push($returnData, $rowData);
                $row++;
            }
            unset($rowData);
        }
        return $returnData;
    }

    /**
     * Get result from query
     * @param $sql
     * @return array
     */
    private function _queryResults($sql)
    {
        $connection = Yii::$app->getDb();
        $command = $connection->createCommand($sql);
        $results = $command->queryAll();
        return $results;
    }

    /**
     * Define columns
     * @return array
     */
    private function _getGridColumns()
    {
        $gridColumns = [
            'inc' => '#',
            'id' => 'Đơn hàng số',
            'created_time' => 'Ngày tạo',
            'statusLabel' => 'Tình trạng',
            'detailsText' => 'Khóa học',
            'user' => 'Học viên',
            'email' => 'Email',
            'phone_number' => 'SĐT đăng ký',
            'shipping_phone_number' => 'SĐT liên hệ',
            'original_affiliate' => 'Original Affiliate',
            'address' => 'Tỉnh thành'
        ];
        return $gridColumns;
    }
}