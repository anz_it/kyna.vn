<?php

namespace app\modules\export\controllers;

use common\components\ExportExcel;
use yii;
use yii\console\Controller;
use kyna\user\models\UserTelesale;

class UserCareController extends Controller
{
    /**
     * @param $sql
     * Get from $dataProvider->query->createCommand()->getRawSql()
     */
    public function actionExport($sql, $email)
    {
        $date = date('c');
        $startTime = time();
        // Get result from query
        $results = $this->_queryResults($sql);

        // format data
        $data['header'] = $this->_getGridColumns();
        $data['content'] = $this->_formatContentRows($results);

        // render Excel
        $excelRunner = new ExportExcel();
        $fileName = 'user_telesales_export_'.date('Y-m-d_H-i', time()) . '.xls';
        $excelRunner->renderData($data, $email, $fileName);

        /*
         * Log executed time
         */
        $endTime = time();
        $executedTime = $endTime - $startTime;
        echo "Date: {$date}"
            . "\n SQL: {$sql}"
            . "\n Email: {$email}"
            . "\n Executed Time: {$executedTime}";

        echo "\n Done \n";
    }

    /**
     * Format Content Row
     * @param $results
     * @return array
     */
    private function _formatContentRows($results)
    {
        $returnData = [];
        if (count($results) > 0) {
            $row = 2;
            foreach ($results as $item) {
                $rowData = null;
                $model = UserTelesale::findOne($item['id']);
                foreach ($this->_getGridColumns() as $column => $label) {
                    $value = null;
                    switch ($column) {
                        case 'inc':
                            $value = $row - 1;
                            break;
                        case 'created_time';
                            $value = Yii::$app->formatter->asDatetime(!empty($item['created_time']) ? $item['created_time'] : null);
                            break;
                        case 'affiliate':
                            if ($model->affiliate) {
                                $value = $model->affiliate->username;
                            }
                            break;
                        case 'original_affiliate':
                            if ($model->oldAffiliate) {
                                $value = $model->oldAffiliate->username;
                            }
                            break;
                        case 'affiliate_cat':
                            if ($model->affiliateUser && $model->affiliateUser->affiliateCategory) {
                                if ($model->oldAffiliateUser && $model->oldAffiliateUser->affiliateCategory) {
                                    $value = $model->oldAffiliateUser->affiliateCategory->name;
                                } else {
                                    $value = $model->affiliateUser->affiliateCategory->name;
                                }
                            }
                            break;
                        case 'tel_id':
                            if ($model->teler) {
                                $value = $model->teler->username;
                            }
                            break;
                        case 'note':
                            $historyDataProvider = $model->actionHistory();
                            $logs = $historyDataProvider->getModels();
                            $ret = "";
                            foreach ($logs as $log) {
                                $date = Yii::$app->formatter->asDatetime($log->action_time);
                                $userInfo = '';
                                if (!empty($log->user)) {
                                    $userInfo = (empty($log->user->profile) ? $log->user->email : $log->user->profile->name) . ' - ';
                                }
                                $logInfo = $log->status ? $log->status . ': ' . $log->note : $log->note;
                                $ret .= (!empty($ret) ? "\n" : '') . "[$date] - {$userInfo}{$logInfo}";
                            }
                            if (!empty($model->recall_date)) {
                                $ret .= " \n\n Gọi lại vào: " . Yii::$app->formatter->asDatetime($model->recall_date);
                            }
                            $value = $ret;
                            break;
                        case 'status':
                            if ($model->user && !empty($model->user->orders)) {
                                $value = 'Thành công (Đã tạo đơn hàng)';
                            } else {
                                $value = 'Từ chối';
                            }
                            break;
                        case 'price':
                            if ($model->user && !empty($model->user->orders)) {
                                $value = $model->user->totalPriceOrders;
                            } else {
                                $value = 0;
                            }
                            break;
                        default:
                            if (isset($item[$column])) {
                                $value = $item[$column];
                            }
                            break;
                    }
                    $rowData[$column] = $value;
                }
                array_push($returnData, $rowData);
                $row++;
            }
            unset($rowData);
        }
        return $returnData;
    }

    /**
     * Get result from query
     * @param $sql
     * @return array
     */
    private function _queryResults($sql)
    {
        $connection = Yii::$app->getDb();
        $command = $connection->createCommand($sql);
        $results = $command->queryAll();
        return $results;
    }

    /**
     * Define columns
     * @return array
     */
    private function _getGridColumns()
    {
        $gridColumns = [
            'inc' => '#',
            'id' => 'ID',
            'created_time' => 'Created Time',
            'email' => 'Email',
            'full_name' => 'Full Name',
            'phone_number' => 'Phone Number',
            'affiliate' => 'Affiliate',
            'original_affiliate' => 'Original Affiliate',
            'affiliate_cat' => 'Affiliate Category',
            'tel_id' => 'Telesale',
            'form_name' => 'Form Name',
            'note' => 'Ghi chú',
            'status' => 'Trạng thái',
            'price' => 'Giá trị đơn hàng thực tế',
        ];
        return $gridColumns;
    }
}