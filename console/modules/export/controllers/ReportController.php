<?php
/**
 * Created by PhpStorm.
 * User: truongn
 * Date: 8/7/18
 * Time: 2:52 PM
 */

namespace app\modules\export\controllers;


use common\components\ExportExcel;
use yii\console\Controller;

class ReportController extends Controller
{

    public function actionExport($sql, $email,   $report_name ,$columns ,$current_user_email)
    {


        $date = date('c');
        $startTime = time();
        $results = $this->_queryResults($sql);

        // format data
     //   $data['header'] = $this->_getGridColumns();
      //  $data['content'] = $this->_formatContentRows($results);
        $data['header'] =    $tmp = split(',', $columns);
        ;
        $data['content'] = $results;
       //var_dump($results); die();
        // render Excel
        $excelRunner = new ExportExcel();
        $fileName = $report_name . ' - ' . date('Y-m-d_H-i', time()) . '.xls';
        $excelRunner->renderData($data, $email, $fileName);

        /*
         * Log executed time
         */
        $endTime = time();
        $executedTime = $endTime - $startTime;
        echo  "\n-----------------------------------------------------"
            . "\n Date: {$date}"
            . "\n SQL: {$sql}"
            . "\n Email: {$email}"
            . "\n Current User run: {$current_user_email}"
            . "\n Executed Time: {$executedTime}";

        echo "\n Done \n";
    }


    /**
     * Format Content Row
     * @param $results
     * @return array
     */

    /**
     * Get result from query
     * @param $sql
     * @return array
     */
    private function _queryResults($sql)
    {
        $connection = \Yii::$app->getDb();
        $command = $connection->createCommand($sql);
        $results = $command->queryAll();
        return $results;
    }

    /**
     * Define columns
     * @return array
     */

}