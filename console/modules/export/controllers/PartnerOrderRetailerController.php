<?php

namespace app\modules\export\controllers;

use common\components\ExportExcel;
use kyna\course\models\Course;
use yii;
use yii\console\Controller;
use kyna\order\models\Order;
use kyna\user\models\UserCourse;
use kyna\partner\models\Code;
use kyna\order\models\OrderShipping;

class TaamkruOrderRetailerController extends Controller
{
    private $email = '';

    /**
     * @param $sql
     * Get from $dataProvider->query->createCommand()->getRawSql()
     */
    public function actionExport($sql, $email)
    {
        $date = date('c');
        $startTime = time();
        // Get result from query
        $results = $this->_queryResults($sql);

        // format data
        $data['header'] = $this->_getGridColumns();
        $data['content'] = $this->_formatContentRows($results);

        // render Excel
        $excelRunner = new ExportExcel();
        $fileName = 'partner_order_'.date('Y-m-d_H-i', time()) . '.xls';
        $excelRunner->renderData($data, $email, $fileName);

        /*
         * Log executed time
         */
        $endTime = time();
        $executedTime = $endTime - $startTime;
        echo "Date: {$date}"
            ."\n SQL: {$sql}"
            ."\n Email: {$email}"
            ."\n Executed Time: {$executedTime}"
        ;

        echo "\n Done \n";
    }

    /**
     * Format Content Row
     * @param $results
     * @return array
     */
    private function _formatContentRows($results)
    {
        $returnData = [];
        if (count($results) > 0) {
            $row = 2;
            foreach ($results as $item) {
                $rowData = null;
                $model = Order::findOne($item['id']);
                $orderShipping = OrderShipping::find()->where(['order_id' => $model->id])->one();
                foreach ($this->_getGridColumns() as $column => $label) {
                    $value = null;
                    switch ($column) {
                        case 'inc':
                            $value = $row - 1;
                            break;
                        case 'order_date';
                            $value = Yii::$app->formatter->asDatetime(!empty($item['created_time']) ? $item['created_time'] : null);
                            break;
                        case 'user';
                            $user = $model->user;
                            if ($user && $user->profile) {
                                $value = $user->profile->name;
                            }
                            break;
                        case 'phone_number':
                            $user = $model->user;
                            if (!empty($user)) {
                                if ($user->profile && !is_null($user->profile->phone_number)) {
                                    $value = $user->profile->phone_number;
                                } else {
                                    if ($user->userAddress) {
                                        $value = $user->userAddress->phone_number;
                                    }
                                }
                            }
                            break;
                        case 'email':
                            $user = $model->user;
                            if (!empty($user)) {
                                $value = $user->email;
                            }
                            break;
                        case 'location_city':
                            if ($orderShipping) {
                                $value = $orderShipping->location->parent->name;
                            }
                            break;
                        case 'location_district':
                            if ($orderShipping) {
                                $value = $orderShipping->location->name;
                            }
                            break;
                        case 'address':
                            if ($orderShipping) {
                                $value = $orderShipping->street_address;
                            }
                            break;
                        case 'details':
                            $value = strip_tags($model->getDetailsText());
                            break;
                        case 'payment_method':
                            $value = $model->isCod ? 'COD' : 'Online';
                            break;
                        default:
                            if (isset($item[$column])) {
                                $value = $item[$column];
                            }
                            break;
                    }
                    $rowData[$column] = $value;
                }
                array_push($returnData, $rowData);
                $row ++;
            }
            unset($rowData);
        }
        return $returnData;
    }

    /**
     * Get result from query
     * @param $sql
     * @return array
     */
    private function _queryResults ($sql) {
        $connection = Yii::$app->getDb();
        $command = $connection->createCommand($sql);
        $results = $command->queryAll();
        return $results;
    }

    /**
     * Define columns
     * @return array
     */
    private function _getGridColumns()
    {
        $gridColumns = [
            'inc' => '#',
            'id' => 'ID đơn hàng',
            'order_date' => 'Thời gian tạo',
            'user' => 'Họ tên',
            'email' => 'Email',
            'phone_number' => 'Số điện thoại',
            'location_city' => 'Tỉnh thành',
            'location_district' => 'Quận huyện',
            'address' => 'Địa chỉ',
            'details' => 'Sản phẩm',
            'total' => 'Giá bán',
            'payment_method' => 'Hình thức thanh toán',
        ];
        return $gridColumns;
    }
}