<?php

namespace app\modules\export\controllers;

use yii;
use yii\console\Controller;

use app\modules\export\models\Order;
use common\components\ExportExcel;

class RegistrationController extends Controller
{

    /**
     * @param $sql
     * Get from $dataProvider->query->createCommand()->getRawSql()
     */
    public function actionExport($sql, $email)
    {
        $date = date('c');
        $startTime = time();
        // Get result from query
        $results = $this->_queryResults($sql);

        // format data
        $data['header'] = $this->_getGridColumns();
        $data['content'] = $this->_formatContentRows($results);

        // render Excel
        $excelRunner = new ExportExcel();
        $fileName = 'registration_export_' . date('Y-m-d_H-i', time()) . '.xls';
        $excelRunner->renderData($data, $email, $fileName);

        /*
         * Log executed time
         */
        $endTime = time();
        $executedTime = $endTime - $startTime;
        echo "Date: {$date}"
            . "\n SQL: {$sql}"
            . "\n Email: {$email}"
            . "\n Executed Time: {$executedTime}";

        echo "\n Done \n";
    }

    /**
     * Format Content Row
     * @param $results
     * @return array
     */
    private function _formatContentRows($results)
    {
        $returnData = [];
        if (count($results) > 0) {
            $statusLabels = Order::getStatusLabels();
            $row = 2;

            foreach ($results as $model) {
                $rowData = null;
                foreach ($this->_getGridColumns() as $column => $label) {
                    $value = null;
                    switch ($column) {
                        case 'inc':
                            $value = $row - 1;
                            break;

                        case 'order_date';
                            $value = Yii::$app->formatter->asDatetime($model->order_date);
                            break;

                        case 'full_name':
                            $value = $model->full_name;
                            break;

                        case 'email':
                            $value = $model->email;
                            break;

                        case 'phone_number':
                            $value = $model->phone_number;
                            break;

                        case 'is_paid':
                            $value = $model->is_paid ? 1 : 0;
                            break;

                        case 'course_id':
                            $value = $model->course != null ? $model->course->name : $model->form_name;
                            break;

                        case 'status':
                            $value = $statusLabels[$model->status];
                            break;

                        default:
                            break;
                    }
                    $rowData[$column] = $value;
                }
                array_push($returnData, $rowData);
                $row++;
            }
            unset($rowData);
        }
        return $returnData;
    }

    /**
     * Get result from query
     * @param $sql
     * @return array
     */
    private function _queryResults($sql)
    {
        return Order::findBySql($sql)->all();
    }

    /**
     * Define columns
     * @return array
     */
    private function _getGridColumns()
    {
        return [
            'inc' => '#',
            'order_date' => 'Ngày đăng ký',
            'full_name' => 'Tên học viên',
            'email' => 'Email',
            'phone_number' => 'SĐT đăng ký',
            'is_paid' => 'Đã thanh toán?',
            'course_id' => 'Khóa học',
            'status' => 'Trạng thái'
        ];
    }
}