<?php

namespace app\modules\export\controllers;

use common\components\ExportExcel;
use kyna\commission\models\CommissionIncome;
use yii;
use yii\console\Controller;

class TeacherIncomeController extends Controller
{
    /**
     * @param $sql
     * Get from $dataProvider->query->createCommand()->getRawSql()
     */
    public function actionExport($sql, $email)
    {
        $date = date('c');
        $startTime = time();
        // Get result from query
        $results = $this->_queryResults($sql);

        // format data
        $data['header'] = $this->_getGridColumns();
        $data['content'] = $this->_formatContentRows($results);

        // render Excel
        $excelRunner = new ExportExcel();
        $fileName = 'teacher_student_export_'.date('Y-m-d_H-i', time()) . '.xls';
        $excelRunner->renderData($data, $email, $fileName);

        /*
         * Log executed time
         */
        $endTime = time();
        $executedTime = $endTime - $startTime;
        echo "Date: {$date}"
            . "\n SQL: {$sql}"
            . "\n Email: {$email}"
            . "\n Executed Time: {$executedTime}";

        echo "\n Done \n";
    }

    /**
     * Format Content Row
     * @param $results
     * @return array
     */
    private function _formatContentRows($results)
    {
        $returnData = [];
        if (count($results) > 0) {
            $row = 2;
            foreach ($results as $item) {
                $rowData = null;
                $model = CommissionIncome::findOne($item['id']);
                foreach ($this->_getGridColumns() as $column => $label) {
                    $value = null;
                    switch ($column) {
                        case 'inc':
                            $value = $row - 1;
                            break;
                        case 'id':
                            $value = $model->id;
                            break;
                        case 'created_time';
                            $value = Yii::$app->formatter->asDatetime($model->created_time);
                            break;
                        case 'course_id':
                            $value = $model->course->name;
                            break;
                        case 'user':
                            $user = $model->order->user;
                            $value = !empty($user) ? $user->email : null;
                            break;
                        case 'amount':
                            Yii::$app->formatter->thousandSeparator = '';
                            Yii::$app->formatter->decimalSeparator = '';
                            $value = Yii::$app->formatter->asDecimal($model->total_amount + $model->fee, 0);
                            break;
                        case 'real_amount':
                            Yii::$app->formatter->thousandSeparator = '';
                            Yii::$app->formatter->decimalSeparator = '';
                            $value = Yii::$app->formatter->asDecimal($model->realIncome, 0);
                            break;
                        case 'commission_percent':
                            $value = $model->teacher_commission_percent;
                            break;
                        case 'commission_amount':
                            Yii::$app->formatter->thousandSeparator = '';
                            Yii::$app->formatter->decimalSeparator = '';
                            $value = Yii::$app->formatter->asDecimal($model->teacher_commission_amount, 0);
                            break;
                        default:
                            break;
                    }
                    $rowData[$column] = $value;
                }
                array_push($returnData, $rowData);
                $row++;
            }
            unset($rowData);
        }
        return $returnData;
    }

    /**
     * Get result from query
     * @param $sql
     * @return array
     */
    private function _queryResults($sql)
    {
        $connection = Yii::$app->getDb();
        $command = $connection->createCommand($sql);
        $results = $command->queryAll();
        return $results;
    }

    /**
     * Define columns
     * @return array
     */
    private function _getGridColumns()
    {
        $gridColumns = [
            'inc' => '#',
            'created_time' => 'Ngày thu nhập',
            'course_id' => 'Khóa học',
            'user' => 'Học viên',
            'amount' => 'Học phí thanh toán',
            'real_amount' => 'Thực nhận học phí',
            'commission_percent' => 'Hoa hồng (%)',
            'commission_amount' => 'Hoa hồng giảng dạy',
        ];
        return $gridColumns;
    }
}