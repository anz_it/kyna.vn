<?php
/**
 * Created by PhpStorm.
 * User: macintosh
 * Date: 5/4/17
 * Time: 9:19 AM
 */

namespace app\modules\export\controllers;

use Yii;
use yii\console\Controller;
use kyna\course\models\Course;
use kyna\user\models\UserCourse;
use kyna\course\models\Quiz;
use kyna\course\models\QuizSession;
use kyna\course\models\QuizSessionTrash;
use kyna\user\models\User;

class CourseController extends Controller
{

    /**
     * Report complete ratio for all single courses.
     * Following this document: https://docs.google.com/spreadsheets/d/1PRSTILsmwjS6DPilPv9IxEiERWsCnYA11TJkKinC7XY/edit#gid=0
     */
    public function actionReportComplete()
    {
        $courses = Course::find()->andWhere(['type' => Course::TYPE_VIDEO])->all();
        $tblUserCourseName = UserCourse::tableName();

        $excelObject = $this->getExcelObject();

        $row = 2;
        foreach ($courses as $key => $course) {
            /* @var $course Course */
            $data = Yii::$app->db
                ->createCommand("select count(*) as totalUser, sum(process) as totalProcess from {$tblUserCourseName} where course_id = {$course->id} and is_deleted = 0")
                ->queryOne();

            $totalUser = $data['totalUser'];
            $totalProcess = $data['totalProcess'];

            $completeRatio = $totalUser > 0 ? round($totalProcess / $totalUser) : 0;
            $totalUserTime = $course->total_time * $totalUser * $completeRatio / 100 / 60;

            // v2
            $v2Id = $course->v2_id != null ? $course->v2_id : 0;
            $v2Data = Yii::$app->dbv2
                ->createCommand("select count(*) as totalUser, sum(process) as totalProcess from learner where course_id = {$v2Id}")
                ->queryOne();

            $excelObject->setActiveSheetIndex(0)
                ->setCellValue("A{$row}", $course->id)
                ->setCellValue("B{$row}", $course->name)
                ->setCellValue("C{$row}", $course->total_time / 60)
                ->setCellValue("D{$row}", $totalUser)
                ->setCellValue("E{$row}", $totalUserTime)
                ->setCellValue("F{$row}", $completeRatio)
                ->setCellValue("G{$row}", $v2Data['totalUser'] > 0 ? round($v2Data['totalProcess'] / $v2Data['totalUser']) : 0)
                ->setCellValue("H{$row}", $v2Data['totalUser']);

            $row++;
        }

        $fileName = date('Y-m-d')  . "_Báo cáo tỉ lệ hoàn thành khóa học";

        $objWriter = \PHPExcel_IOFactory::createWriter($excelObject, 'Excel5');
        $objWriter->save(Yii::getAlias('@console/runtime/') . $fileName . '.xls');

        exit;
    }

    public function actionReportNewComplete($date = '2017-01-21')
    {
        $courses = Course::find()->andWhere(['type' => Course::TYPE_VIDEO])->all();
        $tblUserCourseName = UserCourse::tableName();

        $excelObject = $this->getExcelObjectForNewReport($date);

        $row = 2;
        foreach ($courses as $key => $course) {
            /* @var $course Course */
            $data = Yii::$app->db
                ->createCommand("select count(*) as totalUser, sum(process) as totalProcess from {$tblUserCourseName} where course_id = {$course->id} and is_deleted = 0 and started_date >= unix_timestamp('{$date}')")
                ->queryOne();

            $totalUser = $data['totalUser'];
            $totalProcess = $data['totalProcess'];

            $completeRatio = $totalUser > 0 ? round($totalProcess / $totalUser) : 0;

            $excelObject->setActiveSheetIndex(0)
                ->setCellValue("A{$row}", $course->id)
                ->setCellValue("B{$row}", $course->name)
                ->setCellValue("C{$row}", $totalUser)
                ->setCellValue("D{$row}", $completeRatio);

            $row++;
        }

        $fileName = date('Y-m-d')  . "_Báo cáo tỉ lệ hoàn thành khóa học từ {$date}";

        $objWriter = \PHPExcel_IOFactory::createWriter($excelObject, 'Excel5');
        $objWriter->save(Yii::getAlias('@console/runtime/') . $fileName . '.xls');

        exit;
    }

    public function getExcelObject()
    {
        $objPHPExcel = new \PHPExcel();

        // Set document properties
        $objPHPExcel->getProperties()->setCreator("Kyna")
            ->setLastModifiedBy("Kyna")
            ->setDescription("Báo cáo tỉ lệ hoàn thành khóa học")
            ->setCategory("Báo cáo");

        // Add some data
        $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A1', "ID khóa học")
            ->setCellValue('B1', 'Tên khóa học')
            ->setCellValue('C1', 'Thời lượng (phút)')
            ->setCellValue('D1', 'Số học viên theo số liệu v3 hiện tại (vào lúc xuất report này hoặc trước đó vài ngày cũng được)')
            ->setCellValue('E1', 'Tổng thời gian học viên học (phút)')
            ->setCellValue('F1', 'Tỉ lệ hoàn thành khóa học theo cách tính của v3')
            ->setCellValue('G1', 'Tỉ lệ hoàn thành khóa học theo cách tính của v2 (lấy % mision hoàn thành)')
            ->setCellValue('H1', 'Số học viên theo số liệu v2 vào ngày 08/10/2016 (hoặc tầm tầm ngày đó cũng được)');

        $objPHPExcel->getActiveSheet()->getStyle('A1:Z1')->applyFromArray([
            'font'  => [
                'bold'  => true,
                'size'  => 11,
            ]
        ]);

        foreach (range('A', $objPHPExcel->getActiveSheet()->getHighestDataColumn()) as $col) {
            $objPHPExcel->getActiveSheet()
                ->getColumnDimension($col)
                ->setAutoSize(true);
        }

        // Rename worksheet
        $objPHPExcel->getActiveSheet()->setTitle('Chi tiết');
        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $objPHPExcel->setActiveSheetIndex(0);

        return $objPHPExcel;
    }

    public function getExcelObjectForNewReport($date)
    {
        $objPHPExcel = new \PHPExcel();

        // Set document properties
        $objPHPExcel->getProperties()->setCreator("Kyna")
            ->setLastModifiedBy("Kyna")
            ->setDescription("Báo cáo tỉ lệ hoàn thành khóa học")
            ->setCategory("Báo cáo");

        // Add some data
        $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A1', "ID khóa học")
            ->setCellValue('B1', 'Tên khóa học')
            ->setCellValue('C1', 'Số học viên theo từ ' . $date)
            ->setCellValue('D1', 'Tỉ lệ hoàn thành khóa học từ ' . $date);

        $objPHPExcel->getActiveSheet()->getStyle('A1:D1')->applyFromArray([
            'font'  => [
                'bold'  => true,
                'size'  => 11,
            ]
        ]);

        foreach (range('A', $objPHPExcel->getActiveSheet()->getHighestDataColumn()) as $col) {
            $objPHPExcel->getActiveSheet()
                ->getColumnDimension($col)
                ->setAutoSize(true);
        }

        // Rename worksheet
        $objPHPExcel->getActiveSheet()->setTitle('Chi tiết');
        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $objPHPExcel->setActiveSheetIndex(0);

        return $objPHPExcel;
    }

    public function actionReportCompleteRanger($from, $to = null)
    {
        $courses = Course::find()->andWhere(['type' => Course::TYPE_VIDEO])->all();
        $tblUserCourseName = UserCourse::tableName();

        if ($to == null) {
            $to = date('Y-m-d');
        }

        $excelObject = $this->getExcelObjectForRangerReport($from, $to);

        $row = 2;
        foreach ($courses as $key => $course) {
            $sql = "select count(*) as totalUser, SUM(if(process > 50, 1, 0)) AS total50, SUM(if(process > 75, 1, 0)) AS total75, SUM(if(process >= 100, 1, 0)) AS total100 from {$tblUserCourseName} where course_id = {$course->id} and is_deleted = 0 and started_date >= unix_timestamp('{$from}') and started_date <= unix_timestamp('{$to}')";
            /* @var $course Course */
            $data = Yii::$app->db
                ->createCommand($sql)
                ->queryOne();

            $totalUser = $data['totalUser'];

            $excelObject->setActiveSheetIndex(0)
                ->setCellValue("A{$row}", $course->id)
                ->setCellValue("B{$row}", $course->name)
                ->setCellValue("C{$row}", $totalUser)
                ->setCellValue("D{$row}", $data['total50'])
                ->setCellValue("E{$row}", $data['total75'])
                ->setCellValue("F{$row}", $data['total100']);

            $row++;
        }

        $fileName = date('Y-m-d')  . "_Báo cáo tỉ lệ hoàn thành khóa học từ {$from} đến {$to}";

        $objWriter = \PHPExcel_IOFactory::createWriter($excelObject, 'Excel5');
        $objWriter->save(Yii::getAlias('@console/runtime/') . $fileName . '.xls');

        exit;
    }

    public function getExcelObjectForRangerReport($from, $to)
    {
        $objPHPExcel = new \PHPExcel();

        // Set document properties
        $objPHPExcel->getProperties()->setCreator("Kyna")
            ->setLastModifiedBy("Kyna")
            ->setDescription("Báo cáo tỉ lệ hoàn thành khóa học")
            ->setCategory("Báo cáo");

        // Add some data
        $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A1', "ID khóa học")
            ->setCellValue('B1', 'Tên khóa học')
            ->setCellValue('C1', "Số học viên theo học từ {$from} đến {$to}")
            ->setCellValue('D1', 'Số học viên hoàn thành hơn 50%')
            ->setCellValue('E1', 'Số học viên hoàn thành hơn 75%')
            ->setCellValue('F1', 'Số học viên hoàn thành 100%');

        $objPHPExcel->getActiveSheet()->getStyle('A1:F1')->applyFromArray([
            'font'  => [
                'bold'  => true,
                'size'  => 11,
            ]
        ]);

        foreach (range('A', $objPHPExcel->getActiveSheet()->getHighestDataColumn()) as $col) {
            $objPHPExcel->getActiveSheet()
                ->getColumnDimension($col)
                ->setAutoSize(true);
        }

        // Rename worksheet
        $objPHPExcel->getActiveSheet()->setTitle('Chi tiết');
        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $objPHPExcel->setActiveSheetIndex(0);

        return $objPHPExcel;
    }

    public function getUsersAndCoursesToReport()
    {
        $reportParams = Yii::$app->params['report-completion'];
        $userEmails = $reportParams['userEmails'];
        $courseIds = $reportParams['courseIds'];

        return [
            $userEmails,
            $courseIds
        ];
    }

    public function actionReportCompleteByUsers()
    {
        list($userEmails, $courseIds) = $this->getUsersAndCoursesToReport();

        $excelObject = $this->getExcelObjectForReportCompleteByUsers($courseIds);

        $users = User::find()->with('profile')->where(['email' => $userEmails])->all();

        $row = 3;
        foreach ($users as $key => $user) {
            $name = $user->profile->name;

            $excelObject->setActiveSheetIndex(0)
                ->setCellValue("A{$row}", ++$key)
                ->setCellValue("B{$row}", $name)
                ->setCellValue("C{$row}", $user->email);

            $colChar = 'D';
            $colNumber = \PHPExcel_Cell::columnIndexFromString($colChar) - 1;
            foreach ($courseIds as $courseId) {
                $userCourse = UserCourse::findOne([
                    'user_id' => $user->id,
                    'course_id' => $courseId
                ]);

                $nextChar = \PHPExcel_Cell::stringFromColumnIndex($colNumber + 1);
                $endChar = \PHPExcel_Cell::stringFromColumnIndex($colNumber + 2);

                $tblQuizSession = QuizSession::tableName();
                $tblQuizSessionTrash = QuizSessionTrash::tableName();
                $tblQuiz = Quiz::tableName();

                $data = Yii::$app->db
                    ->createCommand("select count(*) as total, sum(total_score / total_quiz_score) as totalProcess from {$tblQuizSession} qs left join {$tblQuiz} q ON q.id = qs.quiz_id where q.course_id = {$courseId} and qs.user_id = {$user->id}")
                    ->queryOne();

                $data2 = Yii::$app->db
                    ->createCommand("select count(*) as total from {$tblQuizSessionTrash} qs left join {$tblQuiz} q ON q.id = qs.quiz_id where q.course_id = {$courseId} and qs.user_id = {$user->id}")
                    ->queryOne();

                $excelObject->setActiveSheetIndex(0)
                    ->setCellValue("{$colChar}{$row}", $userCourse != null ? $userCourse->process : 0)
                    ->setCellValue("{$nextChar}{$row}", $data['total'] + $data2['total'])
                    ->setCellValue("{$endChar}{$row}", $data['total'] > 0 ? ($data['totalProcess'] * 100 / $data['total']) : 0);

                $colNumber = $colNumber + 3;
                $colChar = \PHPExcel_Cell::stringFromColumnIndex($colNumber);
            }

            $row++;
        }

        $fileName = date('Y-m-d')  . "_Báo cáo tỉ lệ hoàn thành khóa học.";

        $objWriter = \PHPExcel_IOFactory::createWriter($excelObject, 'Excel5');
        $objWriter->save(Yii::getAlias('@console/runtime/') . $fileName . '.xls');

        exit;
    }

    public function getExcelObjectForReportCompleteByUsers($courseIds)
    {
        $courses = Course::find()->where(['id' => $courseIds])->all();

        $objPHPExcel = new \PHPExcel();

        // Set document properties
        $objPHPExcel->getProperties()->setCreator("Kyna")
            ->setLastModifiedBy("Kyna")
            ->setDescription("Báo cáo tỉ lệ hoàn thành khóa học")
            ->setCategory("Báo cáo");

        // Add some data
        $objPHPExcel->setActiveSheetIndex(0)
            ->mergeCells('A1:A2')
            ->mergeCells('B1:B2')
            ->mergeCells('C1:C2')
            ->setCellValue('A1', "STT")
            ->setCellValue('B1', "Họ và tên")
            ->setCellValue('C1', "Email");

        $colChar = 'D';
        $colNumber = \PHPExcel_Cell::columnIndexFromString($colChar) - 1;

        foreach ($courses as $course) {
            $nextChar = \PHPExcel_Cell::stringFromColumnIndex($colNumber + 1);
            $endChar = \PHPExcel_Cell::stringFromColumnIndex($colNumber + 2);

            $nextRanger = "{$colChar}1:{$endChar}1";

            $objPHPExcel->setActiveSheetIndex(0)
                ->mergeCells($nextRanger)
                ->setCellValue("{$colChar}1", $course->name)
                ->setCellValue("{$colChar}2", "Tỉ lệ hoàn thành khóa học")
                ->setCellValue("{$nextChar}2", "Số lần làm bài tập")
                ->setCellValue("{$endChar}2", "Điểm bài tập")
            ;

            $colNumber = $colNumber + 3;
            $colChar = \PHPExcel_Cell::stringFromColumnIndex($colNumber);
        }

        $objPHPExcel->getActiveSheet()->getStyle('A1:Z2')->applyFromArray([
            'font'  => [
                'bold'  => true,
                'size'  => 11,
            ]
        ]);

        foreach (range('A', $objPHPExcel->getActiveSheet()->getHighestDataColumn()) as $col) {
            $objPHPExcel->getActiveSheet()
                ->getColumnDimension($col)
                ->setAutoSize(true);
        }

        // Rename worksheet
        $objPHPExcel->getActiveSheet()->setTitle('Chi tiết');
        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $objPHPExcel->setActiveSheetIndex(0);

        return $objPHPExcel;
    }

    public function actionReportOnlyCompleteByUsersAndCourses($param_name = 'report-completion')
    {
        list($userEmails, $courseIds) = $this->getUsersAndCoursesToList($param_name);

        $excelObject = $this->getExcelObjectForReportOnlyCompleteByUsersAndCourses($courseIds);

        $users = User::find()->with('profile')->where(['email' => $userEmails])->all();

        $row = 3;
        foreach ($users as $key => $user) {
            $name = $user->profile->name;

            $excelObject->setActiveSheetIndex(0)
                ->setCellValue("A{$row}", ++$key)
                ->setCellValue("B{$row}", $name)
                ->setCellValue("C{$row}", $user->email)
                ->setCellValue("D{$row}", $user->username)
                ->setCellValue("E{$row}", "123456")
                ->setCellValue("F{$row}", $user->profile->phone_number);

            $colChar = 'G';
            $colNumber = \PHPExcel_Cell::columnIndexFromString($colChar) - 1;
            foreach ($courseIds as $courseId) {
                $userCourse = UserCourse::find()->where([
                    'user_id' => $user->id,
                    'course_id' => $courseId
                ])->select(['process', 'started_date'])->one();

                $nextChar = \PHPExcel_Cell::stringFromColumnIndex($colNumber + 1);

                $excelObject->setActiveSheetIndex(0)
                    ->setCellValue("{$colChar}{$row}", !empty($userCourse->started_date) ? date('d-m-Y', $userCourse->started_date) : null)
                    ->setCellValue("{$nextChar}{$row}", $userCourse != null ? $userCourse->process : 0);

                $colNumber += 2;
                $colChar = \PHPExcel_Cell::stringFromColumnIndex($colNumber);
            }

            $row++;
        }

        $fileName = date('Y-m-d')  . $param_name;

        $objWriter = \PHPExcel_IOFactory::createWriter($excelObject, 'Excel5');
        $objWriter->save(Yii::getAlias('@console/runtime/') . $fileName . '.xls');

        exit;
    }

    public function getExcelObjectForReportOnlyCompleteByUsersAndCourses($courseIds)
    {
        $objPHPExcel = new \PHPExcel();

        // Set document properties
        $objPHPExcel->getProperties()->setCreator("Kyna")
            ->setLastModifiedBy("Kyna")
            ->setDescription("Báo cáo tỉ lệ hoàn thành khóa học")
            ->setCategory("Báo cáo");

        // Add some data
        $objPHPExcel->setActiveSheetIndex(0)
            ->mergeCells('A1:A2')
            ->mergeCells('B1:B2')
            ->mergeCells('C1:C2')
            ->mergeCells('D1:D2')
            ->mergeCells('E1:E2')
            ->mergeCells('F1:F2')
            ->setCellValue('A1', "STT")
            ->setCellValue('B1', "Họ và tên")
            ->setCellValue('C1', "Email")
            ->setCellValue('D1', "Username")
            ->setCellValue('E1', "Password")
            ->setCellValue('F1', "Số điện thoại");

        $colChar = 'G';
        $colNumber = \PHPExcel_Cell::columnIndexFromString($colChar) - 1;

        foreach ($courseIds as $courseId) {
            $course = Course::findOne($courseId);

            $nextChar = \PHPExcel_Cell::stringFromColumnIndex($colNumber + 1);

            $nextRanger = "{$colChar}1:{$nextChar}1";

            $objPHPExcel->setActiveSheetIndex(0)
                ->mergeCells($nextRanger)
                ->setCellValue("{$colChar}1", $course->name)
                ->setCellValue("{$colChar}2", "Ngày bắt đầu")
                ->setCellValue("{$nextChar}2", "Tiến độ");

            $colNumber += 2;
            $colChar = \PHPExcel_Cell::stringFromColumnIndex($colNumber);
        }

        $endColChar = \PHPExcel_Cell::stringFromColumnIndex($colNumber - 1);

        $objPHPExcel->getActiveSheet()->getStyle("A1:{$endColChar}2")->applyFromArray([
            'font'  => [
                'bold'  => true,
                'size'  => 11,
            ]
        ]);

        foreach (range('A', $objPHPExcel->getActiveSheet()->getHighestDataColumn()) as $col) {
            $objPHPExcel->getActiveSheet()
                ->getColumnDimension($col)
                ->setAutoSize(true);
        }

        // Rename worksheet
        $objPHPExcel->getActiveSheet()->setTitle('Chi tiết');
        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $objPHPExcel->setActiveSheetIndex(0);

        return $objPHPExcel;
    }

    public function getUsersAndCoursesToList($param_name)
    {
        $reportParams = Yii::$app->params[$param_name];
        $userEmails = $reportParams['userEmails'];
        $courseIds = $reportParams['courseIds'];

        return [
            $userEmails,
            $courseIds
        ];
    }
}