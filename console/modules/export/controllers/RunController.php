<?php

namespace app\modules\export\controllers;

use yii;
use yii\console\Controller;

class RunController extends Controller
{
    public static $moduleName = 'export';
    public static $bootstrapName = 'run';
    public static $runAction = 'export';

    /**
     * params = [
     *      0: module (command name, default='export')
     *      1: method/controller
     *      2: $sql
     *      3: $email (attach export file to email)
 *          4: option params
     * ]
     */
    public function actionIndex()
    {
        $params = \Yii::$app->request->params;
        if (isset($params[0]) && ($params[0] == self::$moduleName . '/' . self::$bootstrapName)) {
            if (isset($params[1]) && isset($params[2])) {
                return $this->run($params[1] . '/' . self::$runAction, [$params[2], isset($params[3]) ? $params[3] : null, isset($params[4]) ? $params[4] : null]);
            }
        }
        echo "Invalid Command";
        \Yii::$app->end();
    }

}