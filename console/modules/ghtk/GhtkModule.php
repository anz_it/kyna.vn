<?php

namespace app\modules\ghtk;

class GhtkModule extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\ghtk\controllers';

    public function init()
    {
        parent::init();
        // custom initialization code goes here
    }
}
