<?php
/**
 * Created by PhpStorm.
 * User: vothienhoa
 * Date: 24/09/2018
 * Time: 17:43
 */

namespace app\modules\user\models;

use kyna\user\models\Address as AddressBase;
class Address extends AddressBase
{
    public function setPhoneNumber($phone_number){
        $this->phone_number = $phone_number;
    }

    public function getPhoneNumber(){
        return $this->phone_number;
    }

    public function setOldPhoneNumber($oldPhoneNumber){
        $this->old_phone_number = $oldPhoneNumber;
    }

    public function getOldPhoneNumber(){
        return $this->old_phone_number;
    }

    public function rules(){
        $rules = parent::rules();
        $rules += [
            'old_phone_number'=>'safe'
        ];
        return $rules;
    }

    public function updateNewPhoneNumber($oldPhoneNumber,$newPhoneNumber){
        $this->setOldPhoneNumber($oldPhoneNumber);
        $this->setPhoneNumber($newPhoneNumber);
        $this->save(false);
    }
}