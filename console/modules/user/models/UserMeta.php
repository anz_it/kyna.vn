<?php
/**
 * Created by PhpStorm.
 * User: vothienhoa
 * Date: 24/09/2018
 * Time: 17:54
 */

namespace app\modules\user\models;

use kyna\user\models\UserMeta as UserMetaBase;
class UserMeta extends UserMetaBase
{
    /**
     * @return int
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * @param int $user_id
     */
    public function setUserId($user_id)
    {
        $this->user_id = $user_id;
    }

    /**
     * @return int
     */
    public function getMetaFieldId()
    {
        return $this->meta_field_id;
    }

    /**
     * @param int $meta_field_id
     */
    public function setMetaFieldId($meta_field_id)
    {
        $this->meta_field_id = $meta_field_id;
    }

    /**
     * @return string
     */
    public function getKey()
    {
        return $this->key;
    }

    /**
     * @param string $key
     */
    public function setKey($key)
    {
        $this->key = $key;
    }

    /**
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param string $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }

    public function addNewRowOldPhoneNumber($oldPhoneNumber,$newPhoneNumber){
        $isHasOldPhoneNumber = false;
        $userMetaFind = UserMeta::findAll(['user_id'=>$this->getUserId()]);
        foreach ($userMetaFind as $userMeta){
            if($userMeta->getKey() == 'phone'){
                /*save new phone number*/
                $userMeta->setValue($newPhoneNumber);
                $userMeta->save(false);
            }
            if($userMeta->getKey() == 'old_phone_number'){
                /*new old phone number*/
                $userMeta->setValue($newPhoneNumber);
                $userMeta->save(false);
            }
        }
        if($isHasOldPhoneNumber == false){
            /*new old phone number*/
            $userMeta = new UserMeta();
            $userMeta->setUserId($this->getUserId());
            $userMeta->setValue($oldPhoneNumber);
            $userMeta->setKey('old_phone_number');
            $userMeta->save(false);
        }
    }


}