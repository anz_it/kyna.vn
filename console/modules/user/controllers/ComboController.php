<?php

namespace app\modules\user\controllers;

use kyna\course_combo\models\CourseComboItem;
use kyna\user\models\UserCourse;
use kyna\course\models\Course;
use kyna\user\models\User;
use Yii;
use yii\console\Controller;

class ComboController extends Controller
{
    public function actionAdd()
    {
        $combo_id = false;
        while (!$combo_id){
            $combo_id = readline("Nhập Combo ID: ");
            $combo = Course::findOne(['id'=>$combo_id,'status'=>1,'type'=>Course::TYPE_COMBO,'is_deleted'=>0]);
            if(empty($combo) || is_null($combo)){
                $combo_id = false;
                echo "\033[31m Error : Combo ID này không tồn tại trong hệ thống \033[0m \n";
            }
        }

        $listIdUser = FALSE;
        while (!$listIdUser){
            $listIdUser = readline("Bạn nhập danh sách User Id cách nhau bởi dấu phẩy: ");
        }

        $listIdUser = explode(',',$listIdUser);
        foreach ($listIdUser as $key => $user_id){
            $user = User::findOne(['id'=>$user_id]);
            if($user){
                $coursesCombo = CourseComboItem::find()->where(['course_combo_id'=>$combo_id,'status'=>1])->all();
                foreach ($coursesCombo as $item){
                    $userCourse = UserCourse::findOne(['user_id'=>$user_id,'course_id'=>$item->course_id]);
                    if(empty($userCourse)){
                        $userCourse = new UserCourse();
                        $userCourse->user_id = $user_id;
                        $userCourse->course_id = $item->course_id;
                        $userCourse->is_activated = UserCourse::BOOL_YES;
                        $userCourse->activation_date = time();
                        $userCourse->save(false);
                        echo 'ComboID: '.$combo_id.'---'.'Course:'.$item->course_id. '--- User Id: '.$user_id ."==>OK \n";
                    }
                }
            }
        }
        print_r('Done');die;
    }

    public function actionRemove(){
        $combo_id = false;
        while (!$combo_id){
            $combo_id = readline("Nhập Combo ID: ");
            $combo = Course::findOne(['id'=>$combo_id,'status'=>1,'type'=>Course::TYPE_COMBO]);
            if(empty($combo) || is_null($combo)){
                $combo_id = false;
                echo "\033[31m Error : Combo ID này không tồn tại trong hệ thống \033[0m \n";
            }
        }

        $listIdUser = FALSE;
        while (!$listIdUser){
            $listIdUser = readline("Bạn nhập danh sách User Id cách nhau bởi dấu phẩy: ");
        }
        $listIdUser = explode(',',$listIdUser);
        foreach ($listIdUser as $key => $user_id){
            $coursesCombo = CourseComboItem::findAll(['course_combo_id'=>$combo_id,'status'=>1]);
            foreach ($coursesCombo as $item){
                $userCourse = UserCourse::findOne(['user_id'=>$user_id,'course_id'=>$item->course_id]);
                if($userCourse){
                    $userCourse->delete();
                }
            }
        }
        print_r('remove OK');
    }
}