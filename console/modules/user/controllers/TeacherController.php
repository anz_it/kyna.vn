<?php

namespace app\modules\user\controllers;

use yii\console\Controller;
use common\helpers\StringHelper;
use kyna\course\models\Teacher;
use kyna\user\models\UserMeta;

class TeacherController extends Controller
{

    public function actionUpdateSlug()
    {
        $teachers = Teacher::find()
            ->joinWith('authAssignments')
            ->andFilterWhere(['auth_assignment.item_name' => 'Teacher'])
            ->all();
        if ($teachers) {
            foreach ($teachers as $key => $teacher) {
                if (empty($teacher->slug) && $teacher->profile->name) {
                    $slug = StringHelper::slugify($teacher->profile->name);
                    $metaValueModel = UserMeta::find()
                        ->where([
                            'value' => $slug,
                            'key' => 'slug',
                        ])
                        ->exists();
                    if ($metaValueModel) {
                        $slug = StringHelper::slugify($teacher->profile->name) . '-' . $teacher->id;
                    }
                    $teacher->slug = $slug;
                    if ($teacher->save()) {
                        echo "{$key} - Update Slug: {$teacher->id} - {$teacher->profile->name} \n";
                    }
                }
            }
        }
        echo "Done!!!";
    }
}