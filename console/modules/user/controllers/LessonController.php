<?php
/**
 * Created by PhpStorm.
 * User: khanhphan
 * Date: 10/13/16
 * Time: 11:36 AM
 */

namespace app\modules\user\controllers;


use app\modules\sync\models\CourseLesson;
use kyna\user\models\actions\UserCourseAction;
use kyna\user\models\actions\UserCourseActionMeta;
use kyna\user\models\UserCourse;
use yii\console\Controller;
use yii\db\Query;

class LessonController extends Controller
{


    public function actionCalculate()
    {

        $totalUserCourse = (new Query())
            ->from(UserCourse::tableName()) //->where(['course_id' => 17, 'user_id' => 3])
            ->count();
        $limit = 1000;

        $totalPage = ceil($totalUserCourse / $limit);

        // Cache for course lesson count, save query performance
        $courseLessonCount = [];
        echo "Total page is $totalPage\n";
        $time = time();
        for ($page = 0; $page <= $totalPage; $page++) { // foreach 1000 record to process



            echo "Processing  page $page\n";

            $userCourses = UserCourse::find() //->where(['course_id' => 17, 'user_id' => 3])
                ->limit($limit)
                ->offset($page*$limit)
                ->all();

            /** @var  $userCourse UserCourse */
            foreach ($userCourses as $userCourse) {

                // Get lesson count of this course;
                if (!isset($courseLessonCount[$userCourse->course_id])) {
                    $courseLessonCount[$userCourse->course_id] = CourseLesson::find()->where(['course_id' => $userCourse->course_id])->count();
                }

                // calculate lesson number which user finished
                $number = floor($userCourse->process * $courseLessonCount[$userCourse->course_id] / 100);

                if ($number == 0)
                    continue;

                $currentLesson = CourseLesson::find()
                    ->where(['course_id' => $userCourse->course_id])
                    ->orderBy(['order' => SORT_ASC])
                    ->offset($number)
                    ->one();

                // find last lesson user learned

               if ($currentLesson != null ) {
                   $userCourse->current_section = $currentLesson->section_id;
                   $userCourse->save(false);

               }
            }
            $sec = time()-$time;
            echo "Done page $page, time = $sec\n";
            $time = time();
        }
    }
}