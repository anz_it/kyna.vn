<?php

namespace app\modules\user\controllers;

use Yii;
use yii\console\Controller;
use common\helpers\StringHelper;
use kyna\course\models\Teacher;
use kyna\user\models\UserMeta;
use yii\db\Query;
use common\helpers\ArrayHelper;
use kyna\settings\models\Setting;
use kyna\course\models\Course;

class TeleSalesController extends Controller
{
    public $page_slug_dong_gia = 'campaign/dong-gia';

    public function actionSendEmailDonggia()
    {
        $connection = \Yii::$app->db;
        $sql = "
            SELECT tl.`list_course_ids`,tl.`email`,tl.`full_name`,pages.`title` FROM user_telesales tl
            LEFT JOIN pages ON pages.`slug` = tl.page_slug
                    WHERE tl.id NOT IN (
                        SELECT od.user_telesale_id FROM orders od
                        WHERE od.user_telesale_id IS NOT NULL
                    )
                    AND tl.page_slug = '$this->page_slug_dong_gia'
        ";
        $model = $connection->createCommand($sql);
        $sales = $model->queryAll();

        if(count($sales)>0){
            foreach($sales as $sale){
                $list_course_id = explode(',',$sale['list_course_ids']);
                $full_name = $sale['full_name'];
                $email = $sale['email'];
                $page_title = $sale['title'];

                $courses = Course::find()
                    ->where(['IN', 'id', $list_course_id])
                    ->andWhere('status = :status', [':status' => 1])
                    ->andWhere('is_deleted = :is_deleted', [':is_deleted' => 0])
                    ->all();

                if($courses>0){
                    $list_course_id = array();
                    foreach($courses as $course){
                        array_push($list_course_id,$course->id);
                    }
                    $list_course_id = implode(',',$list_course_id);
                    $mailer = Yii::$app->mailer;
                    $mailer->htmlLayout = '@common/mail/layouts/telesaler';
                    $flag = $mailer->compose('@common/mail/user/landingpage-donggia', [
                        'courses' => $courses,
                        'page_title' => $page_title,
                        'full_name' => $full_name,
                        'link_page' => Yii::$app->getUrlManager()->getBaseUrl().'/p/'.$this->page_slug_dong_gia,
                        'domain'=> Yii::$app->getUrlManager()->getBaseUrl(),
                        'products'=>$list_course_id
                    ])
                        ->setTo($email)
                        ->setSubject("$full_name ơi, hình như bạn đã quên thanh toán")
                        ->send();

                    if($flag == true)
                        $msg_log = "$email ".'gui thanh cong - '.date('d-m-Y H:i:s');
                    else
                        $msg_log = " $email ".'gui that bai - '.date('d-m-Y H:i:s');

                    $this->wh_log($msg_log);

                    Yii::info("Sent payment request user", 'courser');
                }

            }
        }else{
            Yii::info("No payment request", 'order');
        }


    }
    public function actionListEmail(){
        $connection = \Yii::$app->db;
        $sql = "
            SELECT tl.`list_course_ids`,tl.`email`,tl.`full_name`,pages.`title` FROM user_telesales tl
            LEFT JOIN pages ON pages.`slug` = tl.page_slug
                    WHERE tl.id NOT IN (
                        SELECT od.user_telesale_id FROM orders od
                        WHERE od.user_telesale_id IS NOT NULL
                    )
                    AND tl.page_slug = '$this->page_slug_dong_gia'
        ";
        $model = $connection->createCommand($sql);
        $sales = $model->queryAll();
        $show = [];
        if(count($sales)>0) {
            $i = 0;
            foreach ($sales as $sale) {
                $email = $sale['email'];
                $list_course_id = explode(',',$sale['list_course_ids']);
                $courses = Course::find()
                    ->where(['IN', 'id', $list_course_id])
                    ->andWhere('status = :status', [':status' => 1])
                    ->andWhere('is_deleted = :is_deleted', [':is_deleted' => 0])
                    ->all();

                $show[$i]['email'] = $email;
                foreach($courses as $j=>$course){
                    $temp = [];
                    $temp['id_khoa_hoc'] = $course->id;
                    $temp['ten_khoa_hoc'] = $course->name;

                    $show[$i]['data'] = $temp;
                }
                $i++;
            }
            print_r($show);
            Yii::info("show email", 'courser');
        }

    }
    function wh_log($log_msg)
    {
        $log_filename = realpath(__DIR__ . '/../../../../') . '/log/';
        if (!file_exists($log_filename))
        {
            // create directory/folder uploads.
            mkdir($log_filename, 0777, true);
        }
        $log_file_data = $log_filename.'/log_' . date('d-M-Y') . '.log';
        file_put_contents($log_file_data, $log_msg . "\n", FILE_APPEND);
    }

}