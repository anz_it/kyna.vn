<?php
/**
 * Created by PhpStorm.
 * User: vothienhoa
 * Date: 24/09/2018
 * Time: 09:41
 */

namespace app\modules\user\controllers;
use app\modules\user\models\Address;
use app\modules\user\models\User;
use app\modules\user\models\UserMeta;
use app\modules\user\models\UserTelesale;
use app\modules\user\models\OrderShipping;
use kyna\user\models\Profile;
use yii\console\Controller;
use Yii;
use yii\data\Pagination;
use yii\log\Logger;

class PhoneController extends Controller
{
    /*VIETTEL*/
    public function actionViettel(){
        $carriersNumber = array(
            '0162' => '032',
            '0163' => '033',
            '0164' => '034',
            '0165' => '035',
            '0166' => '036',
            '0167' => '037',
            '0168' => '038',
            '0169' => '039',
        );

        $carriersNumberDayChange = array(
            '0162' => '07/10/2018',
            '0163' => '05/10/2018',
            '0164' => '03/10/2018',
            '0165' => '27/9/2018',
            '0166' => '25/9/2018',
            '0167' => '23/9/2018',
            '0168' => '19/9/2018',
            '0169' => '17/9/2018'
        );

        $this->runCovert(Profile::tableName(),$carriersNumber,$carriersNumberDayChange);
        $this->runCovert(UserTelesale::tableName(),$carriersNumber,$carriersNumberDayChange);
        $this->runCovert(OrderShipping::tableName(),$carriersNumber,$carriersNumberDayChange);
        $this->runCovert(Address::tableName(),$carriersNumber,$carriersNumberDayChange);
    }

    public function actionRevertViettel(){
        $carriersNumber = array(
            '032' => '0162',
            '033' => '0163',
            '034' => '0164',
            '035' => '0165',
            '036' => '0166',
            '037' => '0167',
            '038' => '0168',
            '039' => '0169',
        );
        $carriersNumberDayChange = array(
            '032' => '07/10/2018',
            '033' => '05/10/2018',
            '034' => '03/10/2018',
            '035' => '27/9/2018',
            '036' => '25/9/2018',
            '037' => '23/9/2018',
            '038' => '19/9/2018',
            '039' => '17/9/2018',
        );
        $this->runRevert(Profile::tableName(),$carriersNumber,$carriersNumberDayChange);
        $this->runRevert(UserTelesale::tableName(),$carriersNumber,$carriersNumberDayChange);
        $this->runRevert(OrderShipping::tableName(),$carriersNumber,$carriersNumberDayChange);
        $this->runRevert(Address::tableName(),$carriersNumber,$carriersNumberDayChange);
    }

    /*MOBIFONE*/
    public function actionMobifone(){
        $carriersNumber = array(
            '0120' => '070',
            '0121' => '079',
            '0122' => '077',
            '0126' => '076',
            '0128' => '078',
        );

        $carriersNumberDayChange = array(
            '0120' => '15/9/2018',
            '0121' => '21/9/2018',
            '0122' => '25/9/2018',
            '0126' => '28/9/2018',
            '0128' => '2/10/2018',
        );
        $this->runCovert(Profile::tableName(),$carriersNumber,$carriersNumberDayChange);
        $this->runCovert(UserTelesale::tableName(),$carriersNumber,$carriersNumberDayChange);
        $this->runCovert(OrderShipping::tableName(),$carriersNumber,$carriersNumberDayChange);
        $this->runCovert(Address::tableName(),$carriersNumber,$carriersNumberDayChange);
    }

    public function actionRevertMobifone(){
        $carriersNumber = array(
            '070' => '0120',
            '079' => '0121',
            '077' => '0122',
            '076' => '0126',
            '078' => '0128',
        );

        $carriersNumberDayChange = array(
            '070' => '15/9/2018',
            '079' => '21/9/2018',
            '077' => '25/9/2018',
            '076' => '28/9/2018',
            '078' => '2/10/2018',
        );
        $this->runRevert(Profile::tableName(),$carriersNumber,$carriersNumberDayChange);
        $this->runRevert(UserTelesale::tableName(),$carriersNumber,$carriersNumberDayChange);
        $this->runRevert(OrderShipping::tableName(),$carriersNumber,$carriersNumberDayChange);
        $this->runRevert(Address::tableName(),$carriersNumber,$carriersNumberDayChange);
    }

    /*VINAPHONE*/
    public function actionVinaphone(){
        $carriersNumber = array(
            '0123' => '083',
            '0124' => '084',
            '0125' => '085',
            '0127' => '081',
            '0129' => '082',
        );

        $carriersNumberDayChange = array(
            '0123' => '24/9/2018',
            '0124' => '15/9/2018',
            '0125' => '27/9/2018',
            '0127' => '18/9/2018',
            '0129' => '21/9/2018',
        );
        $this->runCovert(Profile::tableName(),$carriersNumber,$carriersNumberDayChange);
        $this->runCovert(UserTelesale::tableName(),$carriersNumber,$carriersNumberDayChange);
        $this->runCovert(OrderShipping::tableName(),$carriersNumber,$carriersNumberDayChange);
        $this->runCovert(Address::tableName(),$carriersNumber,$carriersNumberDayChange);
    }

    public function actionRevertVinaphone(){
        $carriersNumber = array(
            '083' => '0123',
            '084' => '0124',
            '085' => '0125',
            '081' => '0127',
            '082' => '0129',
        );

        $carriersNumberDayChange = array(
            '083' => '24/9/2018',
            '084' => '15/9/2018',
            '085' => '27/9/2018',
            '081' => '18/9/2018',
            '082' => '21/9/2018',
        );
        $this->runRevert(Profile::tableName(),$carriersNumber,$carriersNumberDayChange);
        $this->runRevert(UserTelesale::tableName(),$carriersNumber,$carriersNumberDayChange);
        $this->runRevert(OrderShipping::tableName(),$carriersNumber,$carriersNumberDayChange);
        $this->runRevert(Address::tableName(),$carriersNumber,$carriersNumberDayChange);
    }

    /*GMOBILE*/
    public function actionGmobile(){
        $carriersNumber = array(
            '01992' => '0592',
            '01993' => '0593',
            '01998' => '0598',
            '01999' => '0599',
        );
        $carriersNumberDayChange = array(
            '01992' => '15/9/2018',
            '01993' => '17/9/2018',
            '01998' => '19/9/2018',
            '01999' => '21/9/2018',
        );

        $this->runCovert(Profile::tableName(),$carriersNumber,$carriersNumberDayChange);
        $this->runCovert(UserTelesale::tableName(),$carriersNumber,$carriersNumberDayChange);
        $this->runCovert(OrderShipping::tableName(),$carriersNumber,$carriersNumberDayChange);
        $this->runCovert(Address::tableName(),$carriersNumber,$carriersNumberDayChange);
    }

    public function actionRevertGmobile(){
        $carriersNumber = array(
            '0592' => '01992',
            '0593' => '01993',
            '0598' => '01998',
            '0599' => '01999',
        );
        $carriersNumberDayChange = array(
            '0592' => '15/9/2018',
            '0593' => '17/9/2018',
            '0598' => '19/9/2018',
            '0599' => '21/9/2018',
        );
        $this->runRevert(Profile::tableName(),$carriersNumber,$carriersNumberDayChange);
        $this->runRevert(UserTelesale::tableName(),$carriersNumber,$carriersNumberDayChange);
        $this->runRevert(OrderShipping::tableName(),$carriersNumber,$carriersNumberDayChange);
        $this->runRevert(Address::tableName(),$carriersNumber,$carriersNumberDayChange);
    }

    /*VIETNAM MOBILE*/
    public function actionVietnammobile(){
        $carriersNumber = array(
            '01882' => '0582',
            '01883' => '0583',
            '01884' => '0584',
            '01885' => '0585',
            '01886' => '0586',
            '01887' => '0587',
            '01888' => '0588',
            '01889' => '0589',

            '01863' => '0563',
            '01864' => '0564',
            '01865' => '0565',
            '01866' => '0566',
            '01867' => '0567',
            '01868' => '0568',
            '01869' => '0569',
        );

        $carriersNumberDayChange = array(
            '01882' => '23/9/2018',
            '01883' => '21/9/2018',
            '01884' => '19/9/2018',
            '01885' => '19/9/2018',
            '01886' => '19/9/2018',
            '01887' => '19/9/2018',
            '01888' => '19/9/2018',
            '01889' => '19/9/2018',

            '01863' => '17/9/2018',
            '01864' => '15/9/2018',
            '01865' => '23/9/2018',
            '01866' => '23/9/2018',
            '01867' => '23/9/2018',
            '01868' => '23/9/2018',
            '01869' => '23/9/2018',
        );
        $this->runCovert(Profile::tableName(),$carriersNumber,$carriersNumberDayChange);
        $this->runCovert(UserTelesale::tableName(),$carriersNumber,$carriersNumberDayChange);
        $this->runCovert(OrderShipping::tableName(),$carriersNumber,$carriersNumberDayChange);
        $this->runCovert(Address::tableName(),$carriersNumber,$carriersNumberDayChange);
    }

    public function actionRevertVietnammobile(){
        $carriersNumber = array(
            '0582' => '01882',
            '0583' => '01883',
            '0584' => '01884',
            '0585' => '01885',
            '0586' => '01886',
            '0587' => '01887',
            '0588' => '01888',
            '0589' => '01889',

            '0563' => '01863',
            '0564' => '01864',
            '0565' => '01865',
            '0566' => '01866',
            '0567' => '01867',
            '0568' => '01868',
            '0569' => '01869',
        );

        $carriersNumberDayChange = array(
            '0582' => '23/9/2018',
            '0583' => '21/9/2018',
            '0584' => '19/9/2018',
            '0585' => '19/9/2018',
            '0586' => '19/9/2018',
            '0587' => '19/9/2018',
            '0588' => '19/9/2018',
            '0589' => '19/9/2018',

            '0563' => '17/9/2018',
            '0564' => '15/9/2018',
            '0565' => '23/9/2018',
            '0566' => '23/9/2018',
            '0567' => '23/9/2018',
            '0568' => '23/9/2018',
            '0569' => '23/9/2018',
        );
        $this->runRevert(Profile::tableName(),$carriersNumber,$carriersNumberDayChange);
        $this->runRevert(UserTelesale::tableName(),$carriersNumber,$carriersNumberDayChange);
        $this->runRevert(OrderShipping::tableName(),$carriersNumber,$carriersNumberDayChange);
        $this->runRevert(Address::tableName(),$carriersNumber,$carriersNumberDayChange);
    }

    protected function checkDayToConvertPhoneNumber($carriersNumberDayChange,$firstPhoneNumber){
        $day = $carriersNumberDayChange[$firstPhoneNumber];

        $dateChangePhone = \DateTime::createFromFormat("d/m/Y", $day);
        $dateChangePhone->modify("+1 days");
        $dateNow = new \DateTime('now');
        if ($dateChangePhone <= $dateNow)
            return true;
        else{
            return false;
        }
    }

    public function runRevert($tableName,$carriersNumber,$carriersNumberDayChange){
        echo "============= TABLE $tableName ==============\n";
        foreach ($carriersNumber as $firsOldPhoneNumber=>$firstNewPhoneNumber){
            if($this->checkDayToConvertPhoneNumber($carriersNumberDayChange,$firsOldPhoneNumber) === true){
                echo "Dau so $firsOldPhoneNumber => $firstNewPhoneNumber \n";
                echo "Ngay bat dau :  $carriersNumberDayChange[$firsOldPhoneNumber]\n";
                $this->replaceRevertPhoneNumber($tableName,$firsOldPhoneNumber,$firstNewPhoneNumber);
            }
        }
        echo "============= END TABLE $tableName============\n";
    }

    public function runCovert($tableName,$carriersNumber,$carriersNumberDayChange){
        echo "============= TABLE $tableName ==============\n";
        foreach ($carriersNumber as $firsOldPhoneNumber=>$firstNewPhoneNumber){
            if($this->checkDayToConvertPhoneNumber($carriersNumberDayChange,$firsOldPhoneNumber) === true){
                echo "Dau so $firsOldPhoneNumber => $firstNewPhoneNumber \n";
                echo "Ngay bat dau :  $carriersNumberDayChange[$firsOldPhoneNumber]\n";
                $this->copyPhoneColumnToOldPhoneColumn($tableName,'phone_number','old_phone_number',$firsOldPhoneNumber);
                $this->replacePhoneNumber($tableName,$firsOldPhoneNumber,$firstNewPhoneNumber);
            }
        }
        echo "============= END TABLE $tableName============\n";
    }

    public function copyPhoneColumnToOldPhoneColumn($tableName,$phone_number,$old_phone_number,$firstPhone){
        echo " Copy phone_number to new column old_phone_number \n";
        echo " waiting ... \n";

        $query = "UPDATE $tableName SET $old_phone_number = $phone_number
                  WHERE $old_phone_number IS NULL";
        $connection = Yii::$app->getDb();
        $command = $connection->createCommand($query);
        $result = $command->execute();
        echo "Ok"."\n";
        return $result;
    }

    public function replaceRevertPhoneNumber($tableName,$firstPhone,$newFirstPhone){
        echo "Replace revert phone number \n";
        echo "waiting ... replace phone $firstPhone => $newFirstPhone \n";
        $len = strlen($firstPhone);
        $queryUpdate = "UPDATE $tableName
                  SET phone_number = 
                  CONCAT(LEFT(phone_number, INSTR(phone_number, '$firstPhone')-1),'$newFirstPhone',
                  SUBSTRING(phone_number, INSTR(phone_number, '$firstPhone')+ $len))
                  WHERE phone_number like '$firstPhone%'
                  ";


        $connection = Yii::$app->getDb();
        $command = $connection->createCommand($queryUpdate);
        $result = $command->execute();
        echo "Total $result \n";
        echo "=====OK ====="."\n";
        return $result;
    }

    public function replacePhoneNumber($tableName,$firstPhone,$newFirstPhone){
        echo "Replace phone number \n";
        echo "waiting ... replace phone $firstPhone => $newFirstPhone \n";
        $len = strlen($firstPhone);
        $queryUpdate = "
                  UPDATE $tableName
                  SET phone_number = TRIM(phone_number);
                
                  UPDATE $tableName
                  SET phone_number = 
                  CONCAT(LEFT(phone_number, INSTR(phone_number, '$firstPhone')-1),'$newFirstPhone',
                  SUBSTRING(phone_number, INSTR(phone_number, '$firstPhone')+ $len))
                  WHERE phone_number like '$firstPhone%'
                  and old_phone_number IS NOT NULL
                  ";
        $connection = Yii::$app->getDb();
        $command = $connection->createCommand($queryUpdate);
        $result = $command->execute();
        echo "Total $result \n";
        echo "=====OK ====="."\n";
        return $result;
    }
}