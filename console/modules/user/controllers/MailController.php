<?php

namespace app\modules\user\controllers;


use kyna\user\models\User;
use yii\console\Controller;

class MailController extends Controller
{

    public function actionWelcome($userID)
    {
        echo "======== User: {$userID} ======== \n";
        $user = User::findOne($userID);
        if ($user) {
            $user->mailer->sendWelcomeMessage($user, null, true);
            echo "Done";
        } else {
            echo "User does not exists \n";
        }
    }
}