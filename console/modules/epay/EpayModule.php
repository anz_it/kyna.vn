<?php

namespace app\modules\epay;

class EpayModule extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\epay\controllers';

    public function init()
    {
        parent::init();
        // custom initialization code goes here
    }
}
