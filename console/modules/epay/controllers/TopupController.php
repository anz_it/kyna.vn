<?php
/**
 * @author: Hong Ta
 * @desc: Topup console controller.
 */

namespace app\modules\epay\controllers;

use app\modules\epay\components\Controller;
use kyna\payment\lib\epay\EpayTopUp;
use kyna\payment\models\PaymentTransaction;
use kyna\payment\models\TopupTransaction;
use yii\console\Exception;
use yii;

class TopupController extends Controller{

    public function actionRun()
    {
        $topup_user = TopupTransaction::find()->where([
            'is_topup' => TopupTransaction::BOOL_NO,
            'status' => [
                TopupTransaction::STATUS_CREATED,
                TopupTransaction::STATUS_RETRY
            ]
            ])
            ->andWhere(['is not', 'phone_number', null])
            ->one();

        if (is_null($topup_user)) {
            throw new Exception('Không tìm thấy record nào.');
        }

        $result = $this->createTransaction($topup_user);
        if($result)
            $topup_user->transaction_id = $result->id;

        if($topup_user->save()){
            $topup_Class = new EpayTopUp();

            $result = $topup_Class->topup($topup_user);
            if($result){
                if($result->errorCode == 0){
                    $topup_user->status = TopupTransaction::STATUS_SUCCESS;
                    $topup_user->is_topup = TopupTransaction::BOOL_YES;
                    $topup_user->error_code = $result->errorCode;
                    $topup_user->message = $result->message;
                    $topup_user->save();
                    echo "Success";
                    return true;
                }else{
                    if($topup_user->status == TopupTransaction::STATUS_CREATED){
                        $topup_user->status = TopupTransaction::STATUS_RETRY;
                    }else{
                        $topup_user->status = TopupTransaction::STATUS_FAIL;
                    }
                    $topup_user->is_topup = TopupTransaction::BOOL_NO;
                    $topup_user->error_code = $result->errorCode;
                    $topup_user->message = $result->message;
                    $topup_user->save();
                    echo "Epay system error";
                    return true;
                }
            }
        }
        return false;
    }

    public function actionReset()
    {
        $topup_user = TopupTransaction::find()->where(['is_topup' => TopupTransaction::BOOL_YES])->all();

        foreach($topup_user as $topup){
            $topup->is_topup = TopupTransaction::BOOL_NO;
            $topup->save();
        }
    }

    public function createTransaction($data)
    {
//        if ($this->payment_type == self::PAYMENT_TYPE_NONE) {
//            throw new yii\base\InvalidParamException('Transactions supported by redirect payment only');
//        }

        $transaction = new PaymentTransaction();
        $transaction->attributes = [
            'order_id' => $data->order_id,
            'payment_method' => 'epay',
            'amount' => $data->amount,
            'type' => $transaction::TOP_UP_TYPE,
        ];

        //$transaction->payment_fee = $this->client->calculateFee($this->paymentParams);
        if ($transaction->save()) {
            return $transaction;
        }
        return false;
    }
}