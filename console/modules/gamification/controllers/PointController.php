<?php
/**
 */

namespace app\modules\gamification\controllers;

use kyna\course\models\Category;
use kyna\course\models\Course;
use kyna\course\models\CourseLesson;
use kyna\course\models\CourseRating;
use kyna\course\models\Quiz;
use kyna\gamification\models\UserPoint;
use kyna\gamification\models\UserPointHistory;
use kyna\learning\models\UserCourseLesson;
use kyna\order\models\Order;
use kyna\order\models\OrderDetails;
use kyna\user\models\actions\UserCourseAction;
use kyna\user\models\UserCourse;
use kyna\gamification\models\Mission;
use kyna\gamification\models\MissionCondition;
use yii\console\Controller;

class PointController extends Controller
{

    public function actionApplyUsable()
    {
        $count = 0;

        do {
            $data = UserPointHistory::find()
                ->where([
                    'type' => UserPointHistory::TYPE_MISSION,
                    'is_added' => UserPointHistory::BOOL_NO,
                ])
                ->andWhere("(date(from_unixtime(usable_date)) < date(now())) OR (date(from_unixtime(usable_date)) = date(now()) and hour(from_unixtime(usable_date)) <= hour(now()))")
                ->limit(10)
                ->all();

            foreach ($data as $history) {
                /* @var $history UserPointHistory */
                $userPoint = UserPoint::find()->where(['user_id' => $history->user_id])->one();
                if ($userPoint == null) {
                    $userPoint = new UserPoint();
                    $userPoint->user_id = $history->user_id;
                    $userPoint->k_point = $history->k_point;
                }
                $userPoint->k_point_usable += $history->k_point;
                if ($userPoint->save()) {
                    $history->is_added = UserPointHistory::BOOL_YES;
                    if ($history->save()) {

                        echo $count++ . " - user: #{$userPoint->user_id}, kpoint: {$history->k_point}" . PHP_EOL;
                    } else {
                        var_dump($history->errors);die;
                    }
                } else {
                    var_dump($userPoint->errors);die;
                }
            }

        } while (!empty($data));

        echo "Finished" . PHP_EOL;
        echo "$count done!" . PHP_EOL;
    }

    /**
     * @m_type
     */
    public function actionAddByMission($m_type)
    {
        $missionTbl = Mission::tableName();
        $missionConditionTbl = MissionCondition::tableName();

        $missions = Mission::find()->alias($missionTbl)
            ->joinWith("missionConditions $missionConditionTbl")
            ->where([
                "$missionConditionTbl.type" => $m_type,
                "$missionTbl.status" => Mission::STATUS_ACTIVE
            ])->all();

        foreach ($missions as $mission) {
            switch ($m_type) {
                case MissionCondition::TYPE_COURSE_GRADUATED:
                    $this->runCourseGraduated($mission);
                    break;

                case MissionCondition::TYPE_COURSE_GRADUATED_EXCELLENT:
                    $this->runCourseGraduatedExcellent($mission);
                    break;

                case MissionCondition::TYPE_COURSE_REVIEW:
                    $this->runCourseRating($mission);
                    break;

                case MissionCondition::TYPE_LESSON_QUIZ_PASS_MULTI_CHOICE:
                    $this->runQuizMultiChoice($mission);
                    break;

                case MissionCondition::TYPE_LESSON_QUIZ_PASS_OTHER:
                    $this->runQuizOther($mission);
                    break;

                case MissionCondition::TYPE_LESSON_VIDEO_PROCESS:
                    $this->runLessonVideo($mission);
                    break;

                case MissionCondition::TYPE_LESSON_CONTENT_COMPLETE:
                    $this->runLessonContent($mission);
                    break;

                case MissionCondition::TYPE_ORDER_COMPLETE:
                    $this->runOrderComplete($mission);
                    break;

                default:
                    break;
            }
        }

        echo "Finished" .PHP_EOL ;
    }

    /**
     * @desc function to add point for users with graduated courses which match the mission
     * @param $mission Mission
     */
    private function runCourseGraduated($mission)
    {
        /* @var $condition MissionCondition */
        $condition = $mission->getMissionConditions()->one();
        $minValue = $condition->min_value;

        $userCourseTbl = UserCourse::tableName();
        $userCourseActionTbl = UserCourseAction::tableName();

        $userGraduatedQuery = UserCourse::find()->alias($userCourseTbl)
            ->where(['is_graduated' => UserCourse::BOOL_YES])
            ->leftJoin("$userCourseActionTbl uca", "uca.user_course_id = $userCourseTbl.id")
            ->andWhere("uca.name = 'graduate' and date(from_unixtime(uca.action_time)) >= '2016-10-08'");

        $courseTbl = Course::tableName();
        $categoryTbl = Category::tableName();
        switch ($mission->apply_course_type) {
            case Mission::APPLY_COURSE_TYPE_SOME:
                $courseIds = $mission->getMissionCourses()->select(['course_id'])->column();

                $userGraduatedQuery->andWhere([
                    $userCourseTbl . '.course_id' => $courseIds
                ]);
                break;

            case Mission::APPLY_COURSE_TYPE_CATEGORY:
                $categoryIds = $mission->getMissionCategories()->select(['category_id'])->column();
                $userGraduatedQuery->leftJoin($courseTbl . ' c', 'c.id = ' . $userCourseTbl . '.course_id');
                $userGraduatedQuery->leftJoin($categoryTbl . ' cat', 'cat.id = c.category_id');

                $userGraduatedQuery->andWhere([
                    'OR',
                    ['c.category_id' => $categoryIds],
                    ['cat.parent_id' => $categoryIds],

                ]);
                break;

            default:
                break;
        }

        $totalUserGraduatedQuery = clone $userGraduatedQuery;
        $totalUserGraduatedQuery->select(["$userCourseTbl.user_id"])
            ->groupBy("$userCourseTbl.user_id")
            ->having("count(*) >= $minValue");

        $passedUsers = $totalUserGraduatedQuery->column();

        $count = 0;

        foreach ($passedUsers as $userId) {
            $query = clone $userGraduatedQuery;

            $courseIds = $query->andWhere(["$userCourseTbl.user_id" => $userId])
                ->select(["$userCourseTbl.course_id"])
                ->groupBy("$userCourseTbl.course_id")->column();

            $mIds = [];
            foreach ($courseIds as $courseId) {
                $mIds[] = $courseId;

                if (count($mIds) == $minValue) {
                    $kpoint = $mission->k_point;
                    if ($minValue == 1 && $mission->k_point_for_free_course != null) {
                        $course = Course::findOne($courseId);
                        if ($course != null && $course->sellPrice == 0) {
                            $kpoint = $mission->k_point_for_free_course;
                        }
                    }

                    Mission::addPointByMission($userId, $kpoint, $mission->id, $mIds);

                    echo $count++ . " - Done: user_id.{$userId}, point.$kpoint, mission.#{$mission->id}, ref." . implode('-', $mIds) . PHP_EOL;
                    $mIds = [];
                }
            }

        }
    }

    /**
     * @desc function to add point for users with graduated courses excellent which match the mission
     * @param $mission Mission
     */
    private function runCourseGraduatedExcellent($mission)
    {
        /* @var $condition MissionCondition */
        $condition = $mission->getMissionConditions()->one();
        $minValue = $condition->min_value;

        $courseTbl = Course::tableName();
        $userCourseTbl = UserCourse::tableName();
        $userCourseActionTbl = UserCourseAction::tableName();

        $userGraduatedQuery = UserCourse::find()->alias($userCourseTbl)
            ->where(['is_graduated' => UserCourse::BOOL_YES])
            ->leftJoin("$userCourseActionTbl uca", "uca.user_course_id = $userCourseTbl.id")
            ->andWhere("uca.name = 'graduate' and date(from_unixtime(uca.action_time)) >= '2016-10-08'")
            ->leftJoin("$courseTbl", "$courseTbl.id = $userCourseTbl.course_id")
            ->andWhere("$courseTbl.percent_can_be_excellent is not null and $userCourseTbl.process >= $courseTbl.percent_can_be_excellent");

        $courseTbl = Course::tableName();
        $categoryTbl = Category::tableName();
        switch ($mission->apply_course_type) {
            case Mission::APPLY_COURSE_TYPE_SOME:
                $courseIds = $mission->getMissionCourses()->select(['course_id'])->column();

                $userGraduatedQuery->andWhere([
                    $userCourseTbl . '.course_id' => $courseIds
                ]);
                break;

            case Mission::APPLY_COURSE_TYPE_CATEGORY:
                $categoryIds = $mission->getMissionCategories()->select(['category_id'])->column();
                $userGraduatedQuery->leftJoin($courseTbl . ' c', 'c.id = ' . $userCourseTbl . '.course_id');
                $userGraduatedQuery->leftJoin($categoryTbl . ' cat', 'cat.id = c.category_id');

                $userGraduatedQuery->andWhere([
                    'OR',
                    ['c.category_id' => $categoryIds],
                    ['cat.parent_id' => $categoryIds],

                ]);
                break;

            default:
                break;
        }

        $totalUserGraduatedQuery = clone $userGraduatedQuery;
        $totalUserGraduatedQuery->select(["$userCourseTbl.user_id"])
            ->groupBy("$userCourseTbl.user_id")
            ->having("count(*) >= $minValue");

        $passedUsers = $totalUserGraduatedQuery->column();

        $count = 0;

        foreach ($passedUsers as $userId) {
            $query = clone $userGraduatedQuery;

            $courseIds = $query->andWhere(["$userCourseTbl.user_id" => $userId])
                ->select(["$userCourseTbl.course_id"])
                ->groupBy("$userCourseTbl.course_id")->column();

            $mIds = [];
            foreach ($courseIds as $courseId) {
                $mIds[] = $courseId;

                if (count($mIds) == $minValue) {
                    $kpoint = $mission->k_point;
                    if ($minValue == 1 && $mission->k_point_for_free_course != null) {
                        $course = Course::findOne($courseId);
                        if ($course != null && $course->sellPrice == 0) {
                            $kpoint = $mission->k_point_for_free_course;
                        }
                    }

                    Mission::addPointByMission($userId, $kpoint, $mission->id, $mIds);

                    echo $count++ . " - Done: user_id.{$userId}, point.$kpoint, mission.#{$mission->id}, ref." . implode('-', $mIds) . PHP_EOL;
                    $mIds = [];
                }
            }

        }
    }

    /**
     * @param $mission Mission
     */
    private function runCourseRating($mission)
    {
        /* @var $condition MissionCondition */
        $condition = $mission->getMissionConditions()->one();
        $minValue = $condition->min_value;

        $ratingQuery = CourseRating::find()
            ->alias('r')
            ->andWhere(['status' => CourseRating::STATUS_ACTIVE]);

        $courseTbl = Course::tableName();
        $categoryTbl = Category::tableName();
        switch ($mission->apply_course_type) {
            case Mission::APPLY_COURSE_TYPE_SOME:
                $courseIds = $mission->getMissionCourses()->select(['course_id'])->column();

                $ratingQuery->andWhere([
                    'r.course_id' => $courseIds
                ]);
                break;

            case Mission::APPLY_COURSE_TYPE_CATEGORY:
                $categoryIds = $mission->getMissionCategories()->select(['category_id'])->column();
                $ratingQuery->leftJoin($courseTbl . ' c', 'c.id = r.course_id');
                $ratingQuery->leftJoin($categoryTbl . ' cat', 'cat.id = c.category_id');

                $ratingQuery->andWhere([
                    'OR',
                    ['c.category_id' => $categoryIds],
                    ['cat.parent_id' => $categoryIds],

                ]);
                break;

            default:
                break;
        }

        $totalRatingQuery = clone $ratingQuery;
        $totalRatingQuery->select(["r.user_id"])
            ->groupBy("r.user_id")
            ->having("count(*) >= $minValue");

        $passedUsers = $totalRatingQuery->column();

        $count = 0;
        foreach ($passedUsers as $userId) {

            $query = clone $ratingQuery;

            $courseIds = $query->andWhere(["r.user_id" => $userId])
                ->select(["r.course_id"])
                ->groupBy("r.course_id")->column();

            $mIds = [];
            foreach ($courseIds as $courseId) {
                $mIds[] = $courseId;

                if (count($mIds) == $minValue) {
                    $kpoint = $mission->k_point;
                    if ($minValue == 1 && $mission->k_point_for_free_course != null) {
                        $course = Course::findOne($courseId);
                        if ($course != null && $course->sellPrice == 0) {
                            $kpoint = $mission->k_point_for_free_course;
                        }
                    }

                    Mission::addPointByMission($userId, $kpoint, $mission->id, $mIds);
                    echo $count++ . " - Done: user_id.{$userId}, point.$kpoint, mission.#{$mission->id}, ref." . implode('-', $mIds) . PHP_EOL;
                    $mIds = [];
                }
            }
        }
    }

    /**
     * @param $mission Mission
     */
    private function runQuizMultiChoice($mission)
    {
        /* @var $condition MissionCondition */
        $condition = $mission->getMissionConditions()->one();
        $minValue = $condition->min_value;

        $quizTbl = Quiz::tableName();
        $userCourseTbl = UserCourse::tableName();
        $courseLessonTbl = CourseLesson::tableName();

        $quizQuery = UserCourseLesson::find()
            ->alias('ucl')
            ->leftJoin("$courseLessonTbl cl", "cl.id = ucl.course_lesson_id")
            ->leftJoin("$quizTbl q", "q.id = cl.quiz_id")
            ->leftJoin("$userCourseTbl uc", "uc.id = ucl.user_course_id")
            ->andWhere([
                'ucl.is_passed' => UserCourseLesson::BOOL_YES,
                "cl.type" => CourseLesson::TYPE_QUIZ,
                "q.type" => Quiz::TYPE_ONE_CHOICE,
            ]);

        $courseTbl = Course::tableName();
        $categoryTbl = Category::tableName();
        switch ($mission->apply_course_type) {
            case Mission::APPLY_COURSE_TYPE_SOME:
                $courseIds = $mission->getMissionCourses()->select(['course_id'])->column();

                $quizQuery->andWhere([
                    'uc.course_id' => $courseIds
                ]);
                break;

            case Mission::APPLY_COURSE_TYPE_CATEGORY:
                $categoryIds = $mission->getMissionCategories()->select(['category_id'])->column();
                $quizQuery->leftJoin($courseTbl . ' c', 'c.id = uc.course_id');
                $quizQuery->leftJoin($categoryTbl . ' cat', 'cat.id = c.category_id');

                $quizQuery->andWhere([
                    'OR',
                    ['c.category_id' => $categoryIds],
                    ['cat.parent_id' => $categoryIds],

                ]);
                break;

            default:
                break;
        }

        $totalQuizQuery = clone $quizQuery;
        $totalQuizQuery->select(["uc.user_id as userId", "uc.course_id as courseId"])
            ->groupBy(["uc.user_id", "uc.course_id"])
            ->having("count(*) >= $minValue");

        $passedUsers = $totalQuizQuery->asArray()->all();

        $count = 0;
        foreach ($passedUsers as $key => $cols) {
            /* @var $ucLesson UserCourseLesson */
            $query = clone $quizQuery;

            $quizIds = $query->andWhere(
                [
                    "uc.user_id" => $cols['userId'],
                    "uc.course_id" => $cols['courseId'],
                ])
                ->select(["q.id"])
                ->groupBy("q.id")->column();

            $mIds = [];
            foreach ($quizIds as $quizId) {
                $mIds[] = $quizId;

                if (count($mIds) == $minValue) {
                    $kpoint = $mission->k_point;
                    if ($minValue == 1 && $mission->k_point_for_free_course != null) {
                        $course = Course::findOne($cols['courseId']);
                        if ($course != null && $course->sellPrice == 0) {
                            $kpoint = $mission->k_point_for_free_course;
                        }
                    }

                    Mission::addPointByMission($cols['userId'], $kpoint, $mission->id, $mIds, $cols['courseId']);
                    echo $count++ . " - Done: user_id.{$cols['userId']}, point.$kpoint, mission.#{$mission->id}, ref." . implode('-', $mIds) . PHP_EOL;
                    $mIds = [];
                }
            }
        }
    }

    /**
     * @param $mission Mission
     */
    private function runQuizOther($mission)
    {
        /* @var $condition MissionCondition */
        $condition = $mission->getMissionConditions()->one();
        $minValue = $condition->min_value;

        $quizTbl = Quiz::tableName();
        $userCourseTbl = UserCourse::tableName();
        $courseLessonTbl = CourseLesson::tableName();

        $quizQuery = UserCourseLesson::find()
            ->alias('ucl')
            ->leftJoin("$courseLessonTbl cl", "cl.id = ucl.course_lesson_id")
            ->leftJoin("$quizTbl q", "q.id = cl.quiz_id")
            ->leftJoin("$userCourseTbl uc", "uc.id = ucl.user_course_id")
            ->andWhere([
                'ucl.is_passed' => UserCourseLesson::BOOL_YES,
                "cl.type" => CourseLesson::TYPE_QUIZ,
            ])
            ->andWhere(['!=', "q.type", Quiz::TYPE_ONE_CHOICE]);

        $courseTbl = Course::tableName();
        $categoryTbl = Category::tableName();
        switch ($mission->apply_course_type) {
            case Mission::APPLY_COURSE_TYPE_SOME:
                $courseIds = $mission->getMissionCourses()->select(['course_id'])->column();

                $quizQuery->andWhere([
                    'uc.course_id' => $courseIds
                ]);
                break;

            case Mission::APPLY_COURSE_TYPE_CATEGORY:
                $categoryIds = $mission->getMissionCategories()->select(['category_id'])->column();
                $quizQuery->leftJoin($courseTbl . ' c', 'c.id = uc.course_id');
                $quizQuery->leftJoin($categoryTbl . ' cat', 'cat.id = c.category_id');

                $quizQuery->andWhere([
                    'OR',
                    ['c.category_id' => $categoryIds],
                    ['cat.parent_id' => $categoryIds],

                ]);
                break;

            default:
                break;
        }

        $totalQuizQuery = clone $quizQuery;
        $totalQuizQuery->select(["uc.user_id as userId", "uc.course_id as courseId"])
            ->groupBy(["uc.user_id", "uc.course_id"])
            ->having("count(*) >= $minValue");

        $passedUsers = $totalQuizQuery->asArray()->all();

        $count = 0;
        foreach ($passedUsers as $key => $cols) {
            /* @var $ucLesson UserCourseLesson */
            $query = clone $quizQuery;

            $quizIds = $query->andWhere(
                [
                    "uc.user_id" => $cols['userId'],
                    "uc.course_id" => $cols['courseId'],
                ])
                ->select(["q.id"])
                ->groupBy("q.id")->column();

            $mIds = [];
            foreach ($quizIds as $quizId) {
                $mIds[] = $quizId;

                if (count($mIds) == $minValue) {
                    $kpoint = $mission->k_point;
                    if ($minValue == 1 && $mission->k_point_for_free_course != null) {
                        $course = Course::findOne($cols['courseId']);
                        if ($course != null && $course->sellPrice == 0) {
                            $kpoint = $mission->k_point_for_free_course;
                        }
                    }

                    Mission::addPointByMission($cols['userId'], $kpoint, $mission->id, $mIds, $cols['courseId']);
                    echo $count++ . " - Done: user_id.{$cols['userId']}, point.$kpoint, mission.#{$mission->id}, ref." . implode('-', $mIds) . PHP_EOL;
                    $mIds = [];
                }
            }
        }
    }

    /**
     * @param $mission Mission
     */
    private function runLessonContent($mission)
    {
        /* @var $condition MissionCondition */
        $condition = $mission->getMissionConditions()->one();
        $minValue = $condition->min_value;

        $userCourseTbl = UserCourse::tableName();
        $courseLessonTbl = CourseLesson::tableName();

        $contentQuery = UserCourseLesson::find()
            ->alias('ucl')
            ->leftJoin("$courseLessonTbl cl", "cl.id = ucl.course_lesson_id")
            ->leftJoin("$userCourseTbl uc", "uc.id = ucl.user_course_id")
            ->andWhere([
                'ucl.is_passed' => UserCourseLesson::BOOL_YES,
                "cl.type" => CourseLesson::TYPE_CONTENT,
            ]);

        $courseTbl = Course::tableName();
        $categoryTbl = Category::tableName();
        switch ($mission->apply_course_type) {
            case Mission::APPLY_COURSE_TYPE_SOME:
                $courseIds = $mission->getMissionCourses()->select(['course_id'])->column();

                $contentQuery->andWhere([
                    'uc.course_id' => $courseIds
                ]);
                break;

            case Mission::APPLY_COURSE_TYPE_CATEGORY:
                $categoryIds = $mission->getMissionCategories()->select(['category_id'])->column();
                $contentQuery->leftJoin($courseTbl . ' c', 'c.id = uc.course_id');
                $contentQuery->leftJoin($categoryTbl . ' cat', 'cat.id = c.category_id');

                $contentQuery->andWhere([
                    'OR',
                    ['c.category_id' => $categoryIds],
                    ['cat.parent_id' => $categoryIds],

                ]);
                break;

            default:
                break;
        }

        $totalContentQuery = clone $contentQuery;
        $totalContentQuery->select(["uc.user_id as userId", "uc.course_id as courseId"])
            ->groupBy(["uc.user_id", "uc.course_id"])
            ->having("count(*) >= $minValue");

        $passedUsers = $totalContentQuery->asArray()->all();

        $count = 0;
        foreach ($passedUsers as $key => $cols) {
            /* @var $ucLesson UserCourseLesson */
            $query = clone $contentQuery;

            $contentIds = $query->andWhere(
                [
                    "uc.user_id" => $cols['userId'],
                    "uc.course_id" => $cols['courseId'],
                ])
                ->select(["ucl.course_lesson_id"])
                ->groupBy("ucl.course_lesson_id")->column();

            $mIds = [];
            foreach ($contentIds as $id) {
                $mIds[] = $id;

                if (count($mIds) == $minValue) {
                    $kpoint = $mission->k_point;
                    if ($minValue == 1 && $mission->k_point_for_free_course != null) {
                        $course = Course::findOne($cols['courseId']);
                        if ($course != null && $course->sellPrice == 0) {
                            $kpoint = $mission->k_point_for_free_course;
                        }
                    }

                    Mission::addPointByMission($cols['userId'], $kpoint, $mission->id, $mIds, $cols['courseId']);
                    echo $count++ . " - Done: user_id.{$cols['userId']}, point.$kpoint, mission.#{$mission->id}, ref." . implode('-', $mIds) . PHP_EOL;
                    $mIds = [];
                }
            }
        }
    }

    /**
     * @param $mission Mission
     */
    private function runLessonVideo($mission)
    {
        /* @var $condition MissionCondition */
        $condition = $mission->getMissionConditions()->one();
        $minValue = $condition->min_value;

        $userCourseTbl = UserCourse::tableName();
        $courseLessonTbl = CourseLesson::tableName();

        $contentQuery = UserCourseLesson::find()
            ->alias('ucl')
            ->leftJoin("$courseLessonTbl cl", "cl.id = ucl.course_lesson_id")
            ->leftJoin("$userCourseTbl uc", "uc.id = ucl.user_course_id")
            ->where([
                "cl.type" => CourseLesson::TYPE_VIDEO,
            ])
            ->andWhere([">=", 'ucl.process', $minValue]);

        $courseTbl = Course::tableName();
        $categoryTbl = Category::tableName();
        switch ($mission->apply_course_type) {
            case Mission::APPLY_COURSE_TYPE_SOME:
                $courseIds = $mission->getMissionCourses()->select(['course_id'])->column();

                $contentQuery->andWhere([
                    'uc.course_id' => $courseIds
                ]);
                break;

            case Mission::APPLY_COURSE_TYPE_CATEGORY:
                $categoryIds = $mission->getMissionCategories()->select(['category_id'])->column();
                $contentQuery->leftJoin($courseTbl . ' c', 'c.id = uc.course_id');
                $contentQuery->leftJoin($categoryTbl . ' cat', 'cat.id = c.category_id');

                $contentQuery->andWhere([
                    'OR',
                    ['c.category_id' => $categoryIds],
                    ['cat.parent_id' => $categoryIds],

                ]);
                break;

            default:
                break;
        }

        $totalContentQuery = clone $contentQuery;
        $totalContentQuery->select(["uc.user_id as userId", "uc.course_id as courseId"])
            ->groupBy(["uc.user_id", "uc.course_id"]);

        $passedUsers = $totalContentQuery->asArray()->all();

        $count = 0;
        foreach ($passedUsers as $key => $cols) {
            /* @var $ucLesson UserCourseLesson */
            $query = clone $contentQuery;

            $contentIds = $query->andWhere(
                [
                    "uc.user_id" => $cols['userId'],
                    "uc.course_id" => $cols['courseId'],
                ])
                ->select(["ucl.course_lesson_id"])
                ->groupBy("ucl.course_lesson_id")->column();

            foreach ($contentIds as $id) {
                $kpoint = $mission->k_point;
                if ($mission->k_point_for_free_course != null) {
                    $course = Course::findOne($cols['courseId']);
                    if ($course != null && $course->sellPrice == 0) {
                        $kpoint = $mission->k_point_for_free_course;
                    }
                }

                Mission::addPointByMission($cols['userId'], $kpoint, $mission->id, $id, $cols['courseId']);
                echo $count++ . " - Done: user_id.{$cols['userId']}, point.$kpoint, mission.#{$mission->id}, ref." . $id . PHP_EOL;
            }
        }
    }

    /**
     * @desc function to add point for users when complete orders which match the mission
     * @param $mission Mission
     */
    private function runOrderComplete($mission)
    {
        /* @var $condition MissionCondition */
        $condition = $mission->getMissionConditions()->one();
        $minValue = $condition->min_value;
        $maxValue = $condition->max_value;

        $completedOrderQuery = Order::find()->alias('o')
            ->where(['status' => Order::ORDER_STATUS_COMPLETE])
            ->andWhere(['>=', 'o.total', $minValue])
            ->andWhere(['<=', 'o.total', $maxValue]);

        $totalCompletedOrderQuery = clone $completedOrderQuery;
        $totalCompletedOrderQuery->select(["o.user_id"])
            ->groupBy("o.user_id");

        $passedUsers = $totalCompletedOrderQuery->column();

        $count = 0;
        foreach ($passedUsers as $userId) {
            $query = clone $completedOrderQuery;

            $orders = $query->andWhere(["o.user_id" => $userId])->column();

            foreach ($orders as $oId) {
                $kpoint = $mission->k_point;

                Mission::addPointByMission($userId, $kpoint, $mission->id, $oId);
                echo $count++ . " - Done: user_id.{$userId}, point.$kpoint, mission.#{$mission->id}" . PHP_EOL;
            }

        }
    }

    public function actionOptimizeHistory()
    {
        $count = 0;

        $videoMText = "1, 10, 11";
        do {
            $data = UserPointHistory::find()
                ->alias('h')
                ->joinWith('historyVideos hv')
                ->joinWith('historyReferences hr')
                ->where([
                    'type' => UserPointHistory::TYPE_MISSION,
                ])
                ->andWhere(['is not', 'list_reference_ids', null])
                ->andWhere([
                    'OR',
                    "h.mission_id in ($videoMText) and hv.id is null",
                    "h.mission_id not in ($videoMText) and hr.id is null",
                ])
                ->limit(10)
                ->orderBy('id desc')
                ->all();

            foreach ($data as $history) {
                $history->afterSave(false, []);
                echo $history->id . PHP_EOL;
            }

        } while (!empty($data));

        echo "Finished" . PHP_EOL;
        echo "$count done!" . PHP_EOL;
    }

}