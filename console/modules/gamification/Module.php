<?php

namespace app\modules\gamification;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\gamification\controllers';

    public function init()
    {
        parent::init();
        // custom initialization code goes here
    }
}
