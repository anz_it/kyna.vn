<?php
/**
 * Created by PhpStorm.
 * User: Tan
 * Date: 9/6/2018
 * Time: 5:25 PM
 */

namespace console\controllers;


use kyna\promotion\models\Promotion;
use yii\console\Controller;

class AppPromotionController extends Controller
{
    public function actionUpdateTime()
    {
       $kynaAppVoucher = Promotion::find()->andWhere(['code' => 'APP_KYNA'])->one();
       /* @var Promotion $kynaAppVoucher*/
       if(!empty($kynaAppVoucher)){

           $startTime = \Yii::$app->params['start_promotion_time'];
           $endTime = \Yii::$app->params['end_promotion_time'];
           $kynaAppVoucher->start_date = strtotime($startTime);
           $kynaAppVoucher->end_date = strtotime($endTime);
           $kynaAppVoucher->status = Promotion::STATUS_ACTIVE;
           if($kynaAppVoucher->save(false)){
               echo "Promotion updated!\n";
           }
       }
       echo "Finish!";
    }
}