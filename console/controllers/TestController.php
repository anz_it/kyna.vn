<?php
namespace console\controllers;
/**
 * Created by PhpStorm.
 * User: khanhphan
 * Date: 10/14/16
 * Time: 10:18 AM
 */

use kyna\order\models\Order;
use kyna\user\models\UserCourse;
use kyna\user\models\UserTelesale;
use yii\console\Controller;

class TestController extends Controller
{

    public function actionTest()
    {
        /** @var UserCourse $userCourse */
        $userCourse = UserCourse::findOne(816086);
        $userCourse->calculateProgress();

    }

    public function testC1($closure) {
        echo "TestC1 running\n";
        $closure();
    }

    public function testC2($closure) {
        echo "TestC2 running\n";
        $closure();
    }

    public function actionMax($n)
    {
        $res = 2;
        while ($n <> 1) {
            if ($n % $res == 0)
                $n = $n/$res;
            else
                $res ++;
        }
        echo $res;
        return $res;
    }
    public function actionTest2()
    {

        $course = \app\modules\course\models\Course::find()->one();
        echo $course->name;

    }
    public function actionTest1()
    {

        /**
         * [time, day can learn, can learn]
         */
        $tesCase = [
            ['today -2 days', 1, true],
            ['today -1 day',  1, true],
            ['today -3 days', 4, false],
            ['today -5 days', 4, true],
            ['today',         1, false],
            ['today -5 days',  5, true],
            [1476277169, 2, true]
        ];
        foreach ($tesCase as $key => $test) {
            $userCourse = new \kyna\user\models\UserCourse();
            $userCourse->started_date = strtotime($test[0]);
            $res = $userCourse->compareDate($test[1]);
            echo "Test case {$key} =>";
            echo $res === $test[2] ? "PASS\n" : "FAILED\n";

            echo date('Y-m-d', 1476277169);
        }
    }

    public function actionUpdateOrder()
    {
        $orders = Order::find()->where(['>', 'user_telesale_id' , 0 ]);
        $total = $orders->count();
        echo $total;
        echo "\n";
        $index = 0 ;
        while ($index < $total)
        {
            $session_orders = $orders->limit(10)->offset($index )->all();

            foreach ($session_orders as $order) {
                $user_telesale_id = $order->user_telesale_id;
                $user_tele = UserTelesale::findOne($user_telesale_id);
                $user_tele->order_id = $order->id;
                $user_tele->save(false);
            }
            echo $index . "\n";
            $index = $index + 10;
        }


    }
}