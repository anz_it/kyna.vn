<?php

namespace console\controllers;


use kyna\commission\Commission;
use kyna\commission\models\CommissionIncome;
use kyna\course\models\Course;
use kyna\course_combo\models\CourseComboItem;
use kyna\order\models\Order;
use kyna\order\models\OrderDetails;
use kyna\user\models\Profile;
use kyna\user\models\User;
use kyna\user\models\UserCourse;
use yii\console\Controller;
use yii\helpers\ArrayHelper;

class ExportStudentController extends Controller
{
    public function actionIndex()
    {
        ini_set('memory_limit', '1024M');
        echo "Export students.....\n";
        echo "Press y to export free course\n";
        $free = trim(fgets(STDIN));
        $course_ids = [];
        $isCombo = false;
        if ($free == 'y') {
            $courses_free = Course::find()->andWhere(['price' => 0, 'status' => 1, 'is_deleted' => 0])->all();
            $free_ids = ArrayHelper::getColumn($courses_free, 'id');
            $course_ids = $free_ids;
        } else {
            echo "Enter course ID: ";
            $course_id = trim(fgets(STDIN));
            $course = Course::find()->andWhere(['id' => $course_id])->one();

            if (!empty($course)) {
                if ($course->type == Course::TYPE_COMBO) {
                    $isCombo = true;
                    $course_ids[] = $course_id;
                    /*$courses = CourseComboItem::find()->andWhere(['course_combo_id' => $course->id])->all();
                    foreach ($courses as $c) {
                        $course_ids[] = $c->course_id;
                    }*/
                } else
                    $course_ids[] = $course_id;
            } else {
                echo "Course not found";
                exit;
            }
        }
        echo "Total Course: " . count($course_ids) . "\n";
        echo "Register From Date: ";
        $from_date = trim(fgets(STDIN));

        echo "Register To Date: ";
        $to_date = trim(fgets(STDIN));

        /* $orders = Order::find(['id'])->where(['>=', 'order_date', strtotime($from_date)])
             ->andWhere(['<=', 'order_date', strtotime($to_date)])
             ->andWhere(['or', ['status'=>Order::ORDER_STATUS_COMPLETE], ['is_activated' => 1]])
             ->all();*/

        $query = new \yii\db\Query();
        $query->from(['orders order']);
        $query->leftJoin('order_details details', 'details.order_id = order.id');
        $query->select('order.*');
        $query->andWhere(['>=', 'order.order_date', strtotime($from_date)]);
        $query->andWhere(['<=', 'order.order_date', strtotime($to_date)]);
        $query->andFilterWhere(['or', ['order.status' => Order::ORDER_STATUS_COMPLETE], ['order.is_activated' => 1]]);

        if ($isCombo == false) {
            $query->andWhere(['details.course_id' => $course_ids]);
        } else {
            $query->andWhere(['details.course_combo_id' => $course_ids]);
        }
        $orders = $query->all();
        // var_dump($query->createCommand()->getRawSql());
        echo "\n";
        /*  $orders_filter = [];
          foreach ($orders as $order)
          {
              $order_details = OrderDetails::find()->andWhere(['order_id'=> $order->id])->andWhere(['course_id'=>$course_ids])->exists();
              if($order_details)
              {
                  $orders_filter[] = $order;
              }
          }
          echo "OKKK";
          var_dump(count($orders_filter)); exit;*/
        $users = [];
        if (!empty($orders)) {
            foreach ($orders as $order) {
                $user = User::find()->where(['id' => $order['user_id']])->one();
                //$user = $order->getUser();
                if (!empty($user)) {
                    $profile = Profile::find()->where(['user_id' => $user->id])->one();
                    $u = array();
                    if (!empty($profile->name))
                        $u['name'] = $profile->name;
                    else
                        $u['name'] = $user->username;
                    $u['email'] = $user->email;
                    $u['phone_number'] = $profile->phone_number;
                    $u['order_date'] = date("Y-m-d H:i:s", $order['order_date']);
                    if (!$this->checkUserExist($u['email'], $u['phone_number'], $users))
                        $users[] = $u;
                }
            }
        }
        echo "Founded " . count($users) . " users\n";

        echo "Press y to export, n to cancel\n";
        $answer = trim(fgets(STDIN));
        if ($answer == 'y') {
            if ($free == 'y')
                $this->export($users, 'FREE');
            else
                $this->export($users, $course_id);
        }


        echo "\nQuit";
    }

    private function checkUserExist($email, $phone_number, $users)
    {
        foreach ($users as $user) {
            if ($user['email'] == $email && $user['phone_number'] == $phone_number)
                return true;
        }
        return false;
    }

    private function export($users, $course_id)
    {
        ini_set('max_execution_time', 0);


        $output = fopen(\Yii::getAlias('@runtime') . '/logs/user_' . $course_id . "_" . time() . '.csv', 'w');
        fputs($output, "\xEF\xBB\xBF");

        fputcsv($output, [
            'Tên học viên',
            'Email',
            'Số điện thoại',
            'Ngày đăng kí',
        ]);

        foreach ($users as $user) {
            fputcsv($output, [$user['name'], $user['email'], $user['phone_number'], $user['order_date']]);
        }

        fclose($output);
    }


    //báo cáo số lượng user đã mua 1 khóa học
    public function actionBuyUserOfCourse()
    {

        echo "Enter file name such as output: ";
        $file_name = trim(fgets(STDIN));

        ini_set('max_execution_time', 0);
        $output = fopen(\Yii::getAlias('@runtime') . '/logs/' . $file_name . '.csv', 'w');
        fputs($output, "\xEF\xBB\xBF");


        echo "FROM DATE EX:2018-01-01: ";
        $from_time_str = trim(fgets(STDIN));
        $from_time = strtotime($from_time_str);

        echo "TO DATE EX: 2018-02-01: ";
        $to_time_str = trim(fgets(STDIN));
        $to_time = strtotime($to_time_str);


        echo "LIST COURSES EX: 2,3,4 ";
        $list_course_id = trim(fgets(STDIN));


        $command = "select  od.course_id as 'Mã khóa học', c.name as 'Tên khóa học' , count( distinct o.user_id) as 'Tổng số user'
                    from order_details od , orders o, courses c
                    where c.id in (" . $list_course_id . ")" .
            " and (o.status = 5 or o.is_activated=1) and  o.created_time <  " . $to_time . "
                    and o.created_time >=" . $from_time . "
                    and  od.order_id = o.id and od.course_id = c.id
                    group by course_id, name";

        echo $command;

        $connection = \Yii::$app->getDb();
        $command = $connection->createCommand($command);
        $result = $command->queryAll();

        $c = 0;
        $row = [];
        foreach ($result as $item) {
            for ($i = 0; $i < count($item); $i++) {
                array_push($row, array_keys($item)[$i]);
            };
            fputcsv($output, $row);
            break;
        }

        foreach ($result as $item) {

            $row = [];
            for ($i = 0; $i < count($item); $i++) {
                array_push($row, array_values($item)[$i]);
            };
            $c++;
            fputcsv($output, $row);

        }
        fclose($output);


    }

    public function actionBuyUserOfCombo()
    {

        echo "Enter file name such as output: ";
        $file_name = trim(fgets(STDIN));

        ini_set('max_execution_time', 0);
        $output = fopen(\Yii::getAlias('@runtime') . '/logs/' . $file_name . '.csv', 'w');
        fputs($output, "\xEF\xBB\xBF");


        echo "FROM DATE EX:2018-01-01: ";
        $from_time_str = trim(fgets(STDIN));
        $from_time = strtotime($from_time_str);

        echo "TO DATE EX: 2018-02-01: ";
        $to_time_str = trim(fgets(STDIN));
        $to_time = strtotime($to_time_str);


        echo "LIST COMBO ID EX: 2,3,4 ";
        $list_course_id = trim(fgets(STDIN));


        $command = "select  od.course_combo_id as 'Mã khóa học', c.name as 'Tên khóa học' , count( distinct o.user_id) as 'Tổng số user'
                    from order_details od , orders o, courses c
                    where c.id in (" . $list_course_id . ")" .
            " and (o.status = 5 or o.is_activated=1) and  o.created_time <  " . $to_time . "
                    and o.created_time >=" . $from_time . "   and  od.order_id = o.id and od.course_combo_id = c.id
                    group by course_combo_id, name";


        echo $command;

        $connection = \Yii::$app->getDb();
        $command = $connection->createCommand($command);
        $result = $command->queryAll();

        $c = 0;
        $row = [];
        foreach ($result as $item) {
            for ($i = 0; $i < count($item); $i++) {
                array_push($row, array_keys($item)[$i]);
            };
            fputcsv($output, $row);
            break;
        }

        foreach ($result as $item) {

            $row = [];
            for ($i = 0; $i < count($item); $i++) {
                array_push($row, array_values($item)[$i]);
            };
            $c++;
            fputcsv($output, $row);

        }
        fclose($output);


    }

}