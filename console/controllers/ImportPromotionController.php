<?php
namespace console\controllers;
use kyna\promotion\models\Promotion;
use yii\base\Exception;

/**
 * Created by PhpStorm.
 * User: Tan
 * Date: 6/11/2018
 * Time: 9:31 AM
 */
class ImportPromotionController extends \yii\console\Controller
{
    public function actionIndex()
    {
        ini_set('memory_limit','3G');
        echo "Import promotions.....\n";
        $count = \kyna\promo\models\Promotion::find()->orderBy(['id' => SORT_DESC])->count();
        echo "Found ".$count." promotions to import, press y to import\n";
        $import = trim(fgets(STDIN));
        if($import == "y") {
            $offset = 0;
            do {

                $promos = \kyna\promo\models\Promotion::find()->orderBy(['id' => SORT_DESC])->limit(1000)->offset($offset)->all();
                $arr = [];
                foreach ($promos as $promo) {

                    $voucher = new \kyna\promotion\models\Promotion();
                    $voucher->manual_insert = true;
                    $voucher->id = $promo->id;
                    $voucher->code = $promo->code;
                    $voucher->user_id = $promo->user_id;
                    $voucher->order_id = $promo->order_id;
                    $voucher->partner_id = $promo->partner_id;
                    $voucher->issued_person = $promo->issued_person;
                    $voucher->prefix = $promo->prefix;
                    $voucher->value = $promo->value;
                    $voucher->min_amount = $promo->min_amount;
                    $voucher->type = $promo->kind;
                    $voucher->discount_type = $promo->type;
                    $voucher->used_date = $promo->used_date;
                    $voucher->number_usage = $promo->number_usage;
                    $voucher->current_number_usage = $promo->current_number_usage;
                    $voucher->user_number_usage = 1; //default is 1 time for each user
                    $voucher->is_used = $promo->is_used;
                    $voucher->seller_id = $promo->seller_id;
                    $voucher->start_date = $promo->start_date;
                    $voucher->end_date = $promo->expiration_date;
                    $voucher->apply_all = 0;// default is all single courses
                    $voucher->apply_scope = $promo->apply_scope;
                    $voucher->apply_condition = \kyna\promotion\models\Promotion::ONE_COURSE_CONDITION;

                    if (!empty($promo->apply_for_all)) {

                        $voucher->apply_all_single_course = 1;
                        $voucher->apply_all_single_course_double = 0;
                        $voucher->apply_all_combo = 0;

                    } else {

                        if ($voucher->type == Promotion::KIND_ORDER_APPLY) {
                            $voucher->apply_all_single_course = 1;
                            $voucher->apply_all_single_course_double = 0;
                            $voucher->apply_all_combo = 0;
                        } else {
                            $voucher->apply_all_single_course = 0;
                            $voucher->apply_all_single_course_double = 0;
                            $voucher->apply_all_combo = 0;
                        }
                    }
                    $voucher->status = $promo->status;
                    $voucher->is_deleted = $promo->is_deleted;
                    $voucher->note = $promo->note;
                    $voucher->block_id = $promo->session;
                    $voucher->created_time = $promo->created_time;
                    $voucher->updated_time = $promo->updated_time;
                    $voucher->created_by = $promo->created_user_id;
                    $arr[] = $voucher->attributes;
                }

                try {
                    $voucher2 = new \kyna\promotion\models\Promotion();
                    $columns = array_keys($voucher2->attributes);
                    \Yii::$app->db->createCommand()->batchInsert('promotion', $columns, $arr)->execute();

                } catch (Exception $exception) {

                }
                $offset += 1000;

            } while ($offset <= $count);

            echo "Import completed!";
        }
    }
}