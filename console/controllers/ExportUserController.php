<?php
/**
 * Created by PhpStorm.
 * User: Tan
 * Date: 2/14/2019
 * Time: 1:44 PM
 */

namespace console\controllers;


use kyna\order\models\Order;
use kyna\order\models\OrderDetails;
use kyna\user\models\UserCourse;
use yii\console\Controller;

class ExportUserController extends Controller
{
    public function actionExportSoroban($email)
    {
        $file_name = "soroban_dang_hoc";
        $time = new \DateTime('2019-02-20 00:00:00');
        $time = $time->getTimestamp();
        $query = UserCourse::find()->alias('user_courses')
            ->select(['u.email', 'f.name', 'f.phone_number', 'u.username', 'u.password_hash'])
            ->leftJoin('user u', 'u.id = user_courses.user_id')
            ->leftJoin('profile f', 'f.user_id = u.id')
            ->andWhere(['user_courses.course_id' => [894, 895, 896, 913]])
            ->andWhere(['<=', 'user_courses.created_time', $time])
            ->groupBy(['u.email']);

        $k = 0;
        while ($data = $query->limit(2000)->offset($k)->asArray()->all()) {
            echo "Exporting....\n";
            if (empty($data)) {
                break;
            }
            if ($k == 0) {
                $this->export($data, $file_name, 'w');
            } else {
                $this->export($data, $file_name, 'a');
            }
            $k = $k + 2000;
        }
        if ($k > 0) {
            echo "Sending mail ....\n";
            \Yii::$app->mailer->compose()
                ->setTo($email)
                ->setTextBody('Please view attached file')
                ->attach(\Yii::getAlias('@runtime') . '/logs/order/' . $file_name . '.csv')
                ->send();
        }
        echo "Finish!\n";
    }

    public function actionExportEnglish($email)
    {
        $courseIds = [1282, 1283, 1284, 1323, 1324, 1325, 1326, 1327, 1328, 1329, 1330, 1331, 1189, 1188, 1166];
        $file_name = "english_dang_hoc";
        $time = new \DateTime('2019-02-20 00:00:00');
        $time = $time->getTimestamp();
        $query = UserCourse::find()->alias('user_courses')
            ->select(['u.email', 'f.name', 'f.phone_number', 'u.username', 'u.password_hash'])
            ->leftJoin('user u', 'u.id = user_courses.user_id')
            ->leftJoin('profile f', 'f.user_id = u.id')
            ->andWhere(['user_courses.course_id' => $courseIds])
            ->andWhere(['<=', 'user_courses.created_time', $time])
            ->groupBy(['u.email']);

        $k = 0;
        while ($data = $query->limit(2000)->offset($k)->asArray()->all()) {
            echo "Exporting....\n";
            if (empty($data)) {
                break;
            }
            if ($k == 0) {
                $this->export($data, $file_name, 'w');
            } else {
                $this->export($data, $file_name, 'a');
            }
            $k = $k + 2000;
        }
        if ($k > 0) {
            echo "Sending mail ....\n";
            \Yii::$app->mailer->compose()
                ->setTo($email)
                ->setTextBody('Please view attached file')
                ->attach(\Yii::getAlias('@runtime') . '/logs/order/' . $file_name . '.csv')
                ->send();
        }
        echo "Finish!\n";
    }

    public function actionExportEnglishNotActive($email)
    {
        $time = new \DateTime('2019-02-20 00:00:00');
        $time = $time->getTimestamp();
        $query = OrderDetails::find()->alias('order_details')
            ->select(['u.email', 'f.name', 'f.phone_number', 'u.username', 'u.password_hash'])
            ->leftJoin('orders', 'orders.id = order_details.order_id')
            ->leftJoin('user u', 'u.id = orders.user_id')
            ->leftJoin('profile f', 'f.user_id = u.id')
            ->andWhere(['order_details.course_id' => [1282, 1283, 1284, 1323, 1324, 1325, 1326, 1327, 1328, 1329, 1330, 1331, 1189, 1188, 1166]])
            ->andWhere(['orders.is_activated' => 0, 'orders.payment_method' => 'cod', 'orders.status' => Order::ORDER_STATUS_COMPLETE])
            ->andWhere(['<=', 'orders.created_time', $time])
            ->groupBy(['u.email']);
        $file_name = "english_chua_active";
        $k = 0;
        while ($data = $query->limit(2000)->offset($k)->asArray()->all()) {
            echo "Exporting....\n";
            if (empty($data)) {
                break;
            }
            if ($k == 0) {
                $this->export($data, $file_name, 'w');
            } else {
                $this->export($data, $file_name, 'a');
            }
            $k = $k + 2000;
        }
        if ($k > 0) {
            echo "Sending mail ....\n";
            \Yii::$app->mailer->compose()
                ->setTo($email)
                ->setTextBody('Please view attached file')
                ->attach(\Yii::getAlias('@runtime') . '/logs/order/' . $file_name . '.csv')
                ->send();
        }
        echo "Finish!\n";

    }


    public function actionSorobanNotActive()
    {
        $time = new \DateTime('2019-02-10 00:00:00');
        $time = $time->getTimestamp();
        $query = OrderDetails::find()->alias('order_details')
            ->select(['u.email', 'f.name', 'f.phone_number', 'u.username', 'u.password_hash'])
            ->leftJoin('orders', 'orders.id = order_details.order_id')
            ->leftJoin('user u', 'u.id = orders.user_id')
            ->leftJoin('profile f', 'f.user_id = u.id')
            ->andWhere(['order_details.course_id' => [894, 895, 896, 913]])
            ->andWhere(['orders.is_activated' => 0, 'orders.payment_method' => 'cod'])
            ->andWhere(['<=', 'orders.created_time', $time])
            ->groupBy(['u.email']);
        $file_name = "soroban_chua_active";
        $k = 0;
        while ($data = $query->limit(2000)->offset($k)->asArray()->all()) {
            echo "Exporting....\n";
            if (empty($data)) {
                break;
            }
            if ($k == 0) {
                $this->export($data, $file_name, 'w');
            } else {
                $this->export($data, $file_name, 'a');
            }
            $k = $k + 2000;
        }
        echo "Finish!\n";

    }

    public function actionSorobanNotPay()
    {
        $time = new \DateTime('2019-02-10 00:00:00');
        $time = $time->getTimestamp();

        $query = OrderDetails::find()->alias('order_details')
            ->select(['u.email', 'f.name', 'f.phone_number', 'u.username', 'u.password_hash'])
            ->leftJoin('orders', 'orders.id = order_details.order_id')
            ->leftJoin('user u', 'u.id = orders.user_id')
            ->leftJoin('profile f', 'f.user_id = u.id')
            ->andWhere(['order_details.course_id' => [894, 895, 896, 913]])
            ->andWhere(['!=', 'orders.payment_method', 'cod'])
            ->andWhere(['!=', 'orders.status', 5])
            ->andWhere(['<=', 'orders.created_time', $time])
            ->groupBy(['u.email']);
        $file_name = "soroban_chua_thanhtoan";
        $k = 0;
        while ($data = $query->limit(2000)->offset($k)->asArray()->all()) {
            echo "Exporting....\n";
            if (empty($data)) {
                break;
            }
            if ($k == 0) {
                $this->export($data, $file_name, 'w');
            } else {
                $this->export($data, $file_name, 'a');
            }
            $k = $k + 2000;
        }
        echo "Finish!\n";

    }

    private function export($data, $file_name, $mod = 'a')
    {
        ini_set('max_execution_time', 0);
        $output = fopen(\Yii::getAlias('@runtime') . '/logs/order/' . $file_name . '.csv', $mod);
        if ($mod == 'w') {
            fputs($output, "\xEF\xBB\xBF");
            fputcsv($output, [
                'Email',
                'Tên học viên',
                'Số điện thoại',
                'Username',
                'Password'
            ]);
        }
        foreach ($data as $user) {
            fputcsv($output, [$user['email'], $user['name'], "'" . $user['phone_number'], $user['username'], $user['password_hash']]);
        }

        fclose($output);
    }
}