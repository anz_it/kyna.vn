<?php


namespace console\controllers;


class CsvController extends \yii\console\Controller
{

    public function actionIndex ()
    {
        $query = "select slug as id, name as 'title',
cm.value as 'description', 'Business & Industrial' as 'google_product_category', 
concat('https://kyna.vn/', slug) as 'link',
concat('https://kyna.vn/uploads/courses/', c.id, '/img/image_url.png') as 'image_link', 
'new' as 'condition', 'in stock' as 'availability', price -price_discount as 'price' 
from courses c left join course_meta cm on cm.course_id = c.id and cm.key = 'description'
where type = 1 and status = 1";

        $data = \Yii::$app->db->createCommand($query)->queryAll();
        $file = fopen("export.csv","w");
        foreach ($data as $line) {
            fputcsv($file, $line);
        }
        fclose($file);
    }
}