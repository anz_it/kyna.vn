<?php

use yii\db\Migration;


class m171102_103202_add_facebook_offline_conversion_options extends Migration
{
    public function up()
    {
        $this->insert(
            'meta_fields',
            [
                'key' => 'seo_fbofflineconversion_daily_is_enabled',
                'name' => 'SEO Facebook Offline Conversion Daily Is_Enabled',
                'type' => 4, // check box
                'model' => 'setting',
                'extra_validate' => '',
                'data_set' => '',
                'note' => '',
                'is_unique' => false,
                'status' => 1,
                'created_time' => time(),
                'updated_time' => time(),
            ]
        );

        $this->insert(
            'meta_fields',
            [
                'key' => 'seo_fbofflineconversion_daily_time',
                'name' => 'SEO Facebook Offline Conversion Daily Time (HH:mm AM/PM)',
                'type' => 8, // date time
                'model' => 'setting',
                'extra_validate' => '',
                'data_set' => '',
                'note' => '',
                'is_unique' => false,
                'status' => 1,
                'created_time' => time(),
                'updated_time' => time(),
            ]
        );

        $this->insert(
            'meta_fields',
            [
                'key' => 'seo_fbofflineconversion_is_ignore_uploaded',
                'name' => 'SEO Facebook Offline Conversion Is_Ignore_Uploaded',
                'type' => 4, // check box
                'model' => 'setting',
                'extra_validate' => '',
                'data_set' => '',
                'note' => '',
                'is_unique' => false,
                'status' => 1,
                'created_time' => time(),
                'updated_time' => time(),
            ]
        );

        $this->insert(
            'meta_fields',
            [
                'key' => 'seo_fbofflineconversion_is_upload_on_lead_create',
                'name' => 'SEO Facebook Offline Conversion Is_Upload_On_Lead_Create',
                'type' => 4, // check box
                'model' => 'setting',
                'extra_validate' => '',
                'data_set' => '',
                'note' => '',
                'is_unique' => false,
                'status' => 1,
                'created_time' => time(),
                'updated_time' => time(),
            ]
        );

    }

    public function down()
    {
        $this->delete(
            'setting_meta',
            [
                'key' => [
                    'seo_fbofflineconversion_daily_is_enabled',
                    'seo_fbofflineconversion_daily_time',
                    'seo_fbofflineconversion_is_ignore_uploaded',
                    'seo_fbofflineconversion_is_upload_on_lead_create',
                ]
            ]
        );
        $this->delete(
            'meta_fields',
            [
                'key' => [
                    'seo_fbofflineconversion_daily_is_enabled',
                    'seo_fbofflineconversion_daily_time',
                    'seo_fbofflineconversion_is_ignore_uploaded',
                    'seo_fbofflineconversion_is_upload_on_lead_create',
                ]
            ]
        );

    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
