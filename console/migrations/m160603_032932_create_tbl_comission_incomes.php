<?php

use yii\db\Migration;

/**
 * Handles the creation for table `tbl_comission_incomes`.
 */
class m160603_032932_create_tbl_comission_incomes extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->renameColumn('{{%affiliate_calculations}}', 'instuctor_percent', 'instructor_percent');
        $this->renameTable('{{%affiliate_calculations}}', '{{%commission_calculations}}');
        
        $this->createTable('{{%commission_incomes}}', [
            'id' => $this->primaryKey(),
            'order_id' => $this->integer(11)->notNull(),
            'course_id' => $this->integer(11)->notNull(),
            'user_id' => $this->integer(11)->defaultValue(0),
            'total_amount' => $this->integer(11)->defaultValue(0),
            'affiliate_commission_percent' => $this->float(2)->defaultValue(0),
            'affiliate_commission_amount' => $this->integer(10)->defaultValue(0),
            'teacher_id' => $this->integer(11)->defaultValue(0),
            'teacher_commission_percent' => $this->float(2)->defaultValue(0),
            'teacher_commission_amount' => $this->integer(10)->defaultValue(0),
            'kyna_commission_amount' => $this->integer(10)->defaultValue(0),
            'commission_caculation_id' => $this->integer(11)->defaultValue(0),
            'created_time' => $this->integer(10)->defaultValue(0),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('{{%commission_incomes}}');
        
        $this->renameTable('{{%commission_calculations}}', '{{%affiliate_calculations}}');
        
        $this->renameColumn('{{%affiliate_calculations}}', 'instructor_percent', 'instuctor_percent');
    }
}
