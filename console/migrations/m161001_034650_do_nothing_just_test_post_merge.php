<?php

use yii\db\Migration;

class m161001_034650_do_nothing_just_test_post_merge extends Migration
{
    public function up()
    {
        return true;
    }

    public function down()
    {
        echo "m161001_034650_do_nothing_just_test_post_merge cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
