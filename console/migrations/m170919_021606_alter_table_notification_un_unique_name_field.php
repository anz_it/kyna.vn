<?php

use yii\db\Migration;

class m170919_021606_alter_table_notification_un_unique_name_field extends Migration
{
    public function up()
    {
        $this->alterColumn(
            'notification',
            'name',
            $this->string(256)->notNull()
        );

        $this->dropIndex(
            'name',
            'notification'
        );


    }

    public function down()
    {
        $this->createIndex(
            'name',
            'notification',
            'name',
            true
        );

        //        $this->alterColumn(
//            'notification',
//            'name',
//            $this->string(256)->unique()
//        );

//        echo "m170919_021606_alter_table_notification_un_unique_name_field cannot be reverted.\n";
//
//        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
