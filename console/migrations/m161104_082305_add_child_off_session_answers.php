<?php

use yii\db\Migration;

class m161104_082305_add_child_off_session_answers extends Migration
{
    public function up()
    {
        $this->addColumn(\kyna\course\models\QuizSessionAnswer::tableName(), "parent_id", "int null default null");
        $this->addColumn(\kyna\course\models\QuizSessionAnswerTrash::tableName(), "parent_id", "int null default null");
    }

    public function down()
    {
        echo "m161104_082305_add_child_off_session_answers cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
