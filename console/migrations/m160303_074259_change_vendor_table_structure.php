<?php

use yii\db\Migration;

class m160303_074259_change_vendor_table_structure extends Migration
{
    public function up()
    {
        $this->addColumn('{{%vendors}}', 'alias', 'string');
        $this->dropColumn('{{%vendors}}', 'total_amount');
        $this->alterColumn('{{%vendors}}', 'api_account_id', 'string');
        $this->renameColumn('{{%vendors}}', 'api_account_id', 'api_account');
    }

    public function down()
    {
        echo "m160303_074259_change_vendor_table_structure cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
