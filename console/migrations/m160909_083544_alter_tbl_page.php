<?php

use yii\db\Migration;

class m160909_083544_alter_tbl_page extends Migration
{

    public function safeUp()
    {
        $this->renameTable('{{%page}}', '{{%pages}}');
        
        $this->dropColumn('{{%pages}}', 'is_hidden');
        
        $this->addColumn('{{%pages}}', 'status', $this->smallInteger(3)->defaultValue(1));
        $this->addColumn('{{%pages}}', 'created_time', $this->integer(10));
        $this->addColumn('{{%pages}}', 'updated_time', $this->integer(10));
    }

    public function safeDown()
    {
        $this->dropColumn('{{%pages}}', 'updated_time');
        $this->dropColumn('{{%pages}}', 'created_time');
        $this->dropColumn('{{%pages}}', 'status');
        
        $this->addColumn('{{%pages}}', 'is_hidden', 'BIT(1) default 0');
        $this->renameTable('{{%pages}}', '{{%page}}');
        
        return true;
    }

}
