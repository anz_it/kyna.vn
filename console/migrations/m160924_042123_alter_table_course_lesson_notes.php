<?php

use yii\db\Migration;

class m160924_042123_alter_table_course_lesson_notes extends Migration
{
    public function up()
    {
        $this->alterColumn('{{%course_lesson_notes}}', 'note', $this->text());
    }

    public function down()
    {
        $this->alterColumn('{{%course_lesson_notes}}', 'note', $this->string(50));

        return true;
    }
}
