<?php

use yii\db\Migration;

class m160525_091353_alter_table_coupon_course extends Migration
{
    public function up()
    {
        $this->renameColumn("{{%coupon_courses}}", "coupon_id", "promotion_id");
        $this->addForeignKey('fk_promotion_course', "{{%coupon_courses}}", 'promotion_id', "{{%promotions}}", 'id');
        $this->addForeignKey('fk_course_promotion', "{{%coupon_courses}}", 'course_id', "{{%courses}}", 'id');
    }

    public function down()
    {
        return true;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
