<?php

use yii\db\Migration;

class m160704_032800_alter_tbl_payment_transaction_meta_rename_col_name_to_key extends Migration
{

    public function safeUp()
    {
        $this->renameColumn('{{%payment_transaction_meta}}', 'name', 'key');
    }

    public function safeDown()
    {
        $this->renameColumn('{{%payment_transaction_meta}}', 'key', 'name');
        
        return true;
    }

}
