<?php

use yii\db\Migration;

class m160525_043925_delete_table_coupon_voucher extends Migration
{
    public function up()
    {
        $this->dropForeignKey('fk_coupon_course', "{{%coupon_courses}}");
        $this->dropForeignKey('fk_course_coupon', "{{%coupon_courses}}");
        $this->dropTable("{{%vouchers}}");
        $this->dropTable("{{%coupons}}");
    }

    public function down()
    {
        echo "m160525_043925_merge_table_coupon_voucher cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
