<?php

use yii\db\Migration;

class m160412_044327_alter_tbl_course_sections_add_col_parent_id extends Migration
{

    public function up()
    {
        $this->addColumn("{{%course_sections}}", "parent_id", "INT(11) DEFAULT 0 AFTER `type`");
        
        $this->createIndex("index_parent_id", "{{%course_sections}}", "parent_id");
    }

    public function safeDown()
    {
        $this->dropIndex("index_parent_id", "{{%course_sections}}");
        
        $this->dropColumn("{{%course_sections}}", "parent_id");
        
        echo "m160412_044327_alter_tbl_course_sections_add_col_parent_id has been reverted.\n";

        return true;
    }
}
