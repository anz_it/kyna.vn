<?php

use yii\db\Migration;

class m160429_020726_lession_meta extends \console\components\KynaMigration
{
    public function up()
    {
        $this->dropTable('{{%course_lesson_contents}}');
        $this->createTable('{{%course_lession_meta}}', [
            'id' => $this->primaryKey(),
            'course_lession_id' => $this->integer(),
            'key' => $this->string(50),
            'value' => $this->text(),
        ], self::$_tableOptions);

        $this->createIndex('index_course_id', '{{%course_lession_meta}}', ['course_lession_id']);

        return true;
    }

    public function down()
    {
        echo "m160429_020726_lession_meta cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
