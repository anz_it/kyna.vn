<?php

use yii\db\Migration;

class m170427_092912_create_tbl_teacher_setting extends Migration
{

    public function up()
    {
        $this->createTable('{{%teacher_settings}}', [
            'id' => $this->primaryKey(11),
            'teacher_id' => $this->integer(11)->notNull(),
            'key' => $this->string(32)->notNull(),
            'value' => $this->text(),
            'updated_time' => $this->integer(10)->defaultValue(0),
        ]);

        $this->createIndex('index_uniqe_teacher_key', '{{%teacher_settings}}', ['teacher_id', 'key'], true);
    }

    public function down()
    {
        $this->dropTable('{{%teacher_settings}}');
    }

}
