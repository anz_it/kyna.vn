<?php

use yii\db\Migration;

/**
 * Handles the creation for table `tbl_vouchers`.
 */
class m160524_033720_create_tbl_vouchers extends Migration
{

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('{{%vouchers}}', [
            'id' => $this->primaryKey(),
            'code' => $this->string(20)->notNull()->unique(),
            'value' => $this->integer(7)->notNull(),
            'min_amount' => $this->integer(7)->defaultValue(0),
            'note' => $this->string(100),
            'expiration_date' => $this->integer(10)->defaultValue(0),
            'seller_id' => $this->integer(11)->defaultValue(0),
            'user_id' => $this->integer(11)->defaultValue(0),
            'order_id' => $this->integer(11)->defaultValue(0),
            'is_used' => "bit(1) DEFAULT b'0'",
            'used_date' => $this->integer(10)->defaultValue(0),
            'created_user_id' => $this->integer(11)->notNull(),
            'is_deleted' => "bit(1) DEFAULT b'0'",
            'created_time' => $this->integer(10)->defaultValue(0),
            'updated_time' => $this->integer(10)->defaultValue(0),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('{{%vouchers}}');
        
        return true;
    }

}
