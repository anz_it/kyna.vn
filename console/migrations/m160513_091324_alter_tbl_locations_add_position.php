<?php

class m160513_091324_alter_tbl_locations_add_position extends console\components\KynaMigration
{

    public function safeUp()
    {
        $this->addColumn('{{locations}}', 'position', 'INT(11) DEFAULT 0 AFTER parent_id');
    }

    public function safeDown()
    {
        $this->dropColumn('{{locations}}', 'position');
        
        return true;
    }

}
