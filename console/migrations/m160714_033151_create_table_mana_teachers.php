<?php

use yii\db\Migration;

/**
 * Handles the creation for table `table_mana_teachers`.
 */
class m160714_033151_create_table_mana_teachers extends \console\components\KynaMigration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        try {
            $this->createTable('{{%mana_teachers}}', [
                'id' => $this->primaryKey(),
                'name' => $this->string(255)->notNull(),
                'title' => $this->string(255)->notNull(),
                'description' => $this->text(),
                'image_url' => $this->text(),
                'order' => $this->integer(11)->defaultValue(0),
                'status' => $this->smallInteger(3)->defaultValue(1),
                'created_time' => $this->integer(10)->defaultValue(0),
                'updated_time' => $this->integer(10)->defaultValue(0),
            ], self::$_tableOptions
            );

            $this->createIndex('index_name', '{{%mana_teachers}}', ['name']);
            $this->createIndex('index_title', '{{%mana_teachers}}', ['title']);
        } catch (yii\console\Exception $ex) {
            echo $ex->getMessage();
            return false;
        }

        return true;
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%mana_teachers}}');
        echo "m160714_033151_create_table_mana_teachers has been reverted.\n";
        return true;
    }
}
