<?php

use yii\db\Migration;

class m161010_081826_alter_tbl_courses_add_col_keyword extends Migration
{
    
    public function up()
    {
        $this->addColumn('{{%courses}}', 'keyword', 'TEXT NULL AFTER slug');
    }

    public function down()
    {
        $this->dropColumn('{{%courses}}', 'keyword');
    }
}
