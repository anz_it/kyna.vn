<?php

use yii\db\Migration;

class m170426_044649_alter_table_pages_add_colum_is_payment extends Migration
{
    public function up()
    {
        $this->addColumn('{{%pages}}', 'is_payment', $this->smallInteger(1)->defaultValue(0));
    }

    public function down()
    {
        $this->dropColumn('{{%pages}}', 'is_payment');
    }

}
