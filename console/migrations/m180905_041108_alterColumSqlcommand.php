<?php

use yii\db\Migration;

class m180905_041108_alterColumSqlcommand extends Migration
{
    public function up()
    {
        $this->alterColumn('report','sql_command', $this->string(5024));
    }

    public function down()
    {
        echo "m180905_041108_alterColumSqlcommand cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
