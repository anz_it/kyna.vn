<?php

use yii\db\Migration;

class m180109_022653_create_table_group_discount_courses extends Migration
{
    public function up()
    {
        $this->createTable('group_discount_courses', [
            'id' => $this->primaryKey(11),
            'course_id' => $this->integer(11)->notNull(),
        ]);

        $this->createIndex('index_group_discount_courses_course_id', 'group_discount_courses','course_id', true);

    }

    public function down()
    {
        echo "m180109_022653_create_table_group_discount_courses cannot be reverted.\n";
        $this->dropTable('group_discount_courses');

    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
