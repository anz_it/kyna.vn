<?php

use yii\db\Migration;

class m161008_082154_alter_tbl_orders_add_col_v2_cod_id extends Migration
{

    public function up()
    {
        $this->addColumn('{{%orders}}', 'v2_cod_id', $this->integer(10));
    }

    public function down()
    {
        $this->dropColumn('{{%orders}}', 'v2_cod_id');

        return true;
    }

}
