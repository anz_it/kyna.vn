<?php

use yii\db\Migration;

/**
 * Handles the dropping for table `foreign_keys`.
 */
class m160608_030159_drop_foreign_keys extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->dropForeignKey('auth_assignment_ibfk_1', 'auth_assignment');
        
        $this->dropForeignKey('auth_item_ibfk_1', 'auth_item');
        
        $this->dropForeignKey('auth_item_child_ibfk_1', 'auth_item_child');
        $this->dropForeignKey('auth_item_child_ibfk_2', 'auth_item_child');
        
        $this->dropForeignKey('fk_user_profile', 'profile');
        
        $this->dropForeignKey('fk_user_account', 'social_account');
        
        $this->dropForeignKey('fk_user_token', 'token');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->addForeignKey('fk_user_token', 'token', 'user_id', 'user', 'id', 'CASCADE');
        
        $this->addForeignKey('fk_user_account', 'social_account', 'user_id', 'user', 'id', 'CASCADE');
        
        $this->addForeignKey('fk_user_profile', 'profile', 'user_id', 'user', 'id', 'CASCADE');
        
        $this->addForeignKey('auth_item_child_ibfk_2', 'auth_item_child', 'child', 'auth_item', 'name', 'CASCADE', 'CASCADE');
        $this->addForeignKey('auth_item_child_ibfk_1', 'auth_item_child', 'parent', 'auth_item', 'name', 'CASCADE', 'CASCADE');
        $this->addForeignKey('auth_item_ibfk_1', 'auth_item', 'rule_name', 'auth_rule', 'name', 'SET NULL', 'CASCADE');
        $this->addForeignKey('auth_assignment_ibfk_1', 'auth_assignment', 'item_name', 'auth_item', 'name', 'CASCADE', 'CASCADE');
    }
}
