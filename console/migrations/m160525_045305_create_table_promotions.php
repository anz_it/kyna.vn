<?php

use yii\db\Migration;

/**
 * Handles the creation for table `table_promotions`.
 */
class m160525_045305_create_table_promotions extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('{{%promotions}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(11)->defaultValue(0),
            'order_id' => $this->integer(11)->defaultValue(0),
            'seller_id' => $this->integer(11)->defaultValue(0),
            'partner_id'    => $this->integer(11)->defaultValue(0),
            'status'                => $this->smallInteger(2)->defaultValue(1),
            'number_usage'  => $this->integer(3)->defaultValue(0),
            'code'  => $this->string(32),
            'type'  => $this->smallInteger(2)->defaultValue(0),
            'kind'  => $this->smallInteger(2)->defaultValue(0),
            'value' => $this->integer(10)->defaultValue(0),
            'used_date' => $this->integer(10)->defaultValue(0),
            'start_date'    => $this->integer(10)->defaultValue(0),
            'expiration_date'   => $this->integer(10)->defaultValue(0),
            'is_used' => "bit(1) DEFAULT b'0'",
            'is_deleted' => "bit(1) DEFAULT b'0'",
            'created_user_id' => $this->integer(11)->notNull(),
            'created_time' => $this->integer(10)->defaultValue(0),
            'updated_time' => $this->integer(10)->defaultValue(0),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%promotions}}');
        return true;
    }
}
