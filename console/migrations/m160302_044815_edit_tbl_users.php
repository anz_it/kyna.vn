<?php

use yii\db\Migration;

class m160302_044815_edit_tbl_users extends Migration
{
    public function up()
    {

    }

    public function down()
    {
        $this->dropColumn('{{%users}}', 'full_name');
        $this->dropColumn('{{%users}}', 'role');
        $this->dropColumn('{{%users}}', 'phone_number');
        $this->dropColumn('{{%users}}', 'delta_xu');
        $this->dropColumn('{{%users}}', 'real_delta_xu');
        $this->dropColumn('{{%users}}', 'is_confirmed');

        echo "m160229_030201_milestone1_alter_categories_add_parent_and_level has been reverted.\n";

        return true;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
