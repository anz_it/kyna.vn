<?php

use yii\db\Migration;

class m170831_101901_create_table_course_view_logs extends Migration
{
    public function up()
    {
        $this->createTable(
            'course_view_logs',
            [
                'id' => $this->primaryKey(11),
                'session' => $this->text(),
                'course_id' => $this->integer(11)->notNull(),
                'created_time' => $this->integer(10),
                'updated_time' => $this->integer(10),
            ]
        );

        $this->createIndex(
            'course_view_logs',
            'course_view_logs',
            ['course_id', 'created_time']
        );

    }

    public function down()
    {
        $this->dropIndex(
            'course_view_logs',
            'course_view_logs'
        );

        $this->dropTable(
            'course_view_logs'
        );
//        echo "m170831_101901_create_table_course_view_logs cannot be reverted.\n";

//        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
