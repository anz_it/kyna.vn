<?php

use yii\db\Migration;

class m180619_025335_create_promotion_new_version extends Migration
{
    const PPROMOTION_TABLE = "promotion";
    public function up()
    {
        $this->createTable(self::PPROMOTION_TABLE, [
            'id' => $this->primaryKey(),
            'order_id' => $this->integer(),
            'user_id' => $this->integer(),
            'partner_id' => $this->integer(),
            'issued_person' => $this->integer(),
            'prefix' => $this->string('255'),
            'code' => $this->string('255'),
            'value' => $this->integer(),
            'min_amount' => $this->integer(),
            'discount_type' => $this->integer(),
            'type' => $this->integer(),
            'used_date' => $this->integer(),
            'number_usage' => $this->integer(),
            'user_number_usage' => $this->integer(),
            'current_number_usage' => $this->integer(),
            'is_used' => $this->integer(),
            'seller_id' => $this->integer(),
            'start_date' => $this->integer(),
            'end_date' => $this->integer(),

            'apply_all'=> $this->integer(),
            'apply_scope'=> $this->integer(),
            'apply_condition' => $this->integer(),
            'apply_all_single_course' => $this->integer(),
            'apply_all_single_course_double' => $this->integer(),
            'apply_all_combo' => $this->integer(),

            'status'=> $this->integer(),
            'is_deleted' => $this->integer(),
            'note' => $this->string('255'),
            'block_id' => $this->string('255'),
            'created_time' => $this->integer(),
            'updated_time' => $this->integer(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
        ]);
    }

    public function down()
    {
        echo "m180619_025335_create_promotion_new_version cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
