<?php

use yii\db\Migration;
use kyna\course\models\Course;
use kyna\user\models\UserCourse;

class m170613_040738_alter_tbl_courses_add_percent_can_be_excellent extends Migration
{

    public function up()
    {
        $courseTblName = Course::tableName();
        $userCourseTblName = UserCourse::tableName();

        $this->addColumn($courseTblName, 'percent_can_be_excellent', 'float default 70 after percent_can_pass');

        $this->update($courseTblName, ['percent_can_pass' => 50]);

        $this->update($userCourseTblName, ['is_graduated' => UserCourse::BOOL_YES], ['AND', ['>=', 'process', 50], ['is_graduated' => UserCourse::BOOL_NO]]);
        $this->update($userCourseTblName, ['is_graduated' => UserCourse::BOOL_NO], ['AND', ['<', 'process', 50], ['is_graduated' => UserCourse::BOOL_YES]]);
    }

    public function down()
    {
        $courseTblName = Course::tableName();

        $this->dropColumn($courseTblName, 'percent_can_be_excellent');
    }

}
