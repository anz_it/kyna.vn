<?php

use yii\db\Migration;

class m180913_093456_addOrderIdToTelesales extends Migration
{
    public function up()
    {
        $this->addColumn('user_telesales','order_id',$this->integer());
    }

    public function down()
    {
        $this->dropColumn('user_telesales','order_id');
        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
