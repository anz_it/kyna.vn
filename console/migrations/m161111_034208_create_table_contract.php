<?php

use yii\db\Migration;

class m161111_034208_create_table_contract extends Migration
{
    public function up()
    {
        $this->createTable('{{%teacher_contracts}}', [
            'id' => $this->primaryKey(),
            'title' => $this->string(),
            'description' => $this->string(),
            'code' => $this->string()->notNull()->unique(),
            'user_id' => $this->integer(11)->notNull(),
            'created_time' => $this->integer(11),
            'updated_time' => $this->integer(11)
        ]);
        // create index
        $this->createIndex(
            'idx-code', 'teacher_contracts', 'code'
        );
        $this->createTable('{{%teacher_contract_meta}}', [
            'id' => $this->primaryKey(),
            'teacher_contract_id' => $this->integer(11)->notNull(),
            'key' => $this->string()->notNull()->defaultValue('relation'),
            'value' => $this->text()->notNull()
        ]);
        // create index
        $this->createIndex(
            'idx-value', 'teacher_contract_meta', 'teacher_contract_id'
        );
        // create foreign key
        $this->addForeignKey('fk_contract_meta', 'teacher_contract_meta', 'teacher_contract_id', 'teacher_contracts', 'id', 'CASCADE', 'CASCADE');
    }

    public function down()
    {
        $this->dropTable('{{%teacher_contract_meta}}');
        $this->dropTable('{{%teacher_contracts}}');
        return true;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
