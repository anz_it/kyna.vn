<?php

use yii\db\Migration;

class m180327_072321_add_affiliate_commission_percent_course extends Migration
{
    public function safeUp()
    {
        $this->addColumn('courses','affiliate_commission_percent',$this->float()->defaultValue(50));

    }

    public function safeDown()
    {
        echo "m180327_072321_add_affiliate_commission_percent_course cannot be reverted.\n";
        $this->dropColumn('courses','affiliate_commission_percent');
        return false;
    }


}
