<?php

use yii\db\Migration;
use kyna\base\models\MetaField;
use kyna\settings\models\Setting;

class m171101_034228_add_special_aff_setting extends Migration
{
    public function safeUp()
    {
        $this->insert(MetaField::tableName(), [
            'key' => 'kyna_aff_cat_ids',
            'name' => 'Danh sách loại affiliate của Kyna (bắt đầu bằng chữ Kyna)',
            'type' => 1,
            'model' => MetaField::MODEL_SETTING,
            'status' => MetaField::STATUS_ACTIVE
        ]);
        $this->insert(MetaField::tableName(), [
            'key' => 'id_aff_can_not_overwrite_by_kyna',
            'name' => 'Danh sách affiliate ID không thể bị ghi đè bới affiliate của Kyna',
            'type' => 1,
            'model' => MetaField::MODEL_SETTING,
            'status' => MetaField::STATUS_ACTIVE
        ]);
    }

    public function safeDown()
    {
        $this->delete(MetaField::tableName(), [
            'key' => ['kyna_aff_cat_ids', 'id_aff_can_not_overwrite_by_kyna']
        ]);
    }
}
