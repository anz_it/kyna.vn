<?php

use yii\db\Migration;

class m160318_063039_add_tbl_order_divisions extends \console\components\KynaMigration
{

    public function up()
    {
        $this->createTable('{{%order_divisions}}', [
            'id' => $this->primaryKey(),
            'order_id' => $this->integer(11)->notNull(),
            'user_id' => $this->integer(11)->notNull(),
            'type' => $this->string(20)->notNull(),
            ], self::$_tableOptions
        );
        
        $this->createIndex('index_order_id', '{{%order_divisions}}', 'order_id');
        $this->createIndex('index_user_id', '{{%order_divisions}}', 'user_id');
    }

    public function down()
    {
        $this->dropTable('{{%order_divisions}}');
        echo "m160318_063039_add_tbl_order_divisions has been reverted.\n";

        return true;
    }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
