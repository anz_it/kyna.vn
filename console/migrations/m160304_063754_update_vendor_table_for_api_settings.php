<?php

use yii\db\Migration;

class m160304_063754_update_vendor_table_for_api_settings extends Migration
{
    public function up()
    {
        $this->addColumn('{{%vendors}}', 'settings', 'text');
        $this->dropColumn('{{%vendors}}', 'api_account');
    }

    public function down()
    {
        echo "m160304_063754_update_vendor_table_for_api_settings cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
