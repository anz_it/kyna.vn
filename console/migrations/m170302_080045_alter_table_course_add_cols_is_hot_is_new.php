<?php

use yii\db\Migration;

class m170302_080045_alter_table_course_add_cols_is_hot_is_new extends Migration
{

    public function up()
    {
        $this->addColumn('{{%courses}}', 'is_hot', "BIT(1) DEFAULT 0");
        $this->addColumn('{{%courses}}', 'is_new', "BIT(1) DEFAULT 0");

        $this->createIndex('index_is_hot', '{{%courses}}', 'is_hot');
        $this->createIndex('index_is_new', '{{%courses}}', 'is_new');
    }

    public function down()
    {
        $this->dropIndex('index_is_new', '{{%courses}}');
        $this->dropIndex('index_is_hot', '{{%courses}}');

        $this->dropColumn('{{%courses}}', 'is_new');
        $this->dropColumn('{{%courses}}', 'is_hot');

        return true;
    }
}
