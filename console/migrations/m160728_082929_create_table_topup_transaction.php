<?php

use yii\db\Migration;

/**
 * Handles the creation for table `table_topup_transaction`.
 */
class m160728_082929_create_table_topup_transaction extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('{{%topup_transactions}}', [
            'id' => $this->primaryKey(),
            'order_id' => $this->integer(),
            'transaction_id' => $this->integer(),
            'phone_number' => $this->string(),
            'provider'  => $this->string(),
            'amount'    => $this->integer(),
            'quantity'    => $this->integer(),
            'status'    => $this->smallInteger(2),
            'created_time'  => $this->integer(11),
            'updated_time'  => $this->integer(11)
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%topup_transactions}}');
        return true;
    }
}
