<?php

use yii\db\Migration;

class m170530_023914_alter_tbl_banners_add_image_mobile extends Migration
{
    public function up()
    {
        $this->addColumn('{{%banners}}', 'mobile_image_url', 'text null after image_url');
    }

    public function down()
    {
        $this->dropColumn('{{%banners}}', 'mobile_image_url');
    }

}
