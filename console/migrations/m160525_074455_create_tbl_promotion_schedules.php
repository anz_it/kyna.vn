<?php

use yii\db\Migration;

/**
 * Handles the creation for table `{{%promotion_schedules}}`.
 */
class m160525_074455_create_tbl_promotion_schedules extends Migration
{

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('{{%promotion_schedules}}', [
            'id' => $this->primaryKey(),
            'course_id' => $this->integer(11)->notNull(),
            'start_time' => $this->integer(10)->defaultValue(0),
            'end_time' => $this->integer(10)->defaultValue(0),
            'discount_percent' => $this->decimal(10, 2)->defaultValue(0),
            'discount_amount' => $this->integer(10)->defaultValue(0),
            'price' => $this->integer(10)->defaultValue(0),
            'reg_count' => $this->integer(6)->defaultValue(0),
            'note' => $this->string(255),
            'image_url' => $this->text(),
            'is_deleted' => "bit(1) DEFAULT b'0'",
            'status' => $this->smallInteger(3)->defaultValue(1),
            'created_time' => $this->integer(10)->defaultValue(0),
            'updated_time' => $this->integer(10)->defaultValue(0),
        ]);
        
        $this->createIndex('index_course_id', '{{%promotion_schedules}}', 'course_id');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('{{%promotion_schedules}}');
    }

}
