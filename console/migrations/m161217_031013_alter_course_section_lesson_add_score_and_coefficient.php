<?php

use yii\db\Migration;

class m161217_031013_alter_course_section_lesson_add_score_and_coefficient extends Migration
{
    public function up()
    {
        $this->addColumn('{{%course_sections}}', 'score', $this->float()->defaultValue(1));
        $this->addColumn('{{%course_sections}}', 'coefficient', $this->float()->defaultValue(1));
        $this->addColumn('{{%course_sections}}', 'percent_can_pass', $this->float()->defaultValue(1));

        $this->addColumn('{{%course_lessons}}', 'score', $this->float()->defaultValue(1));
        $this->addColumn('{{%course_lessons}}', 'coefficient', $this->float()->defaultValue(1));
        $this->addColumn('{{%course_lessons}}', 'percent_can_pass', $this->float()->defaultValue(1));
    }

    public function down()
    {
        $this->dropColumn('{{%course_lessons}}', 'percent_can_pass');
        $this->dropColumn('{{%course_lessons}}', 'coefficient');
        $this->dropColumn('{{%course_lessons}}', 'score');

        $this->dropColumn('{{%course_sections}}', 'percent_can_pass');
        $this->dropColumn('{{%course_sections}}', 'coefficient');
        $this->dropColumn('{{%course_sections}}', 'score');

        return true;
    }

}
