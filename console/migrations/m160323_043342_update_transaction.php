<?php

use yii\db\Migration;

class m160323_043342_update_transaction extends Migration
{
    public function up()
    {
        $this->addColumn('{{%payment_transactions}}', 'status', $this->integer());
        return true;
    }

    public function down()
    {
        echo "m160323_043342_update_transaction cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
