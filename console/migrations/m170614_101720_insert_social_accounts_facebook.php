<?php

use yii\db\Migration;

class m170614_101720_insert_social_accounts_facebook extends Migration
{

    public function up()
    {
        $this->execute("update profile set facebook_id = null where facebook_id = 1");

        $this->execute("insert into social_account (user_id, provider, client_id) (select user_id, 'facebook', facebook_id from profile where facebook_id is not null and facebook_id not in (SELECT facebook_id FROM profile where facebook_id is not null group by facebook_id having count(*) > 1))");
    }

    public function down()
    {
        // can not revert
    }
}
