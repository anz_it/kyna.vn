<?php

use yii\db\Migration;

class m161019_045501_alter_promotion_set_default_value_for_col_number_usage extends Migration
{

    public function up()
    {
        $this->alterColumn('{{%promotions}}', 'number_usage', $this->integer(3)->defaultValue(1));
    }

    public function down()
    {
        $this->alterColumn('{{%promotions}}', 'number_usage', $this->integer(3)->defaultValue(0));

        return true;
    }

}
