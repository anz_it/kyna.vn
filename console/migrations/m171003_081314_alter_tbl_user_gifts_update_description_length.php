<?php

use yii\db\Migration;

class m171003_081314_alter_tbl_user_gifts_update_description_length extends Migration
{
    public function up()
    {
        $this->alterColumn('user_point_histories', 'description', $this->text()->notNull());
    }

    public function down()
    {
        $this->alterColumn('user_point_histories', 'description', $this->string(100)->notNull());
    }

}
