<?php

use yii\db\Migration;

class m171030_092344_alter_table_user_telesalse__add_column_is_uploaded_fboc extends Migration
{
    public function up()
    {
        $this->addColumn(
            'user_telesales',
            'is_uploaded_fboc',
            $this->boolean()->defaultValue(false)->comment('is uploaded to Facebook Offline Conversion')
        );
    }

    public function down()
    {
        $this->dropColumn(
            'user_telesales',
            'is_uploaded_fboc'
        );
//        echo "m171030_092344_alter_table_user_telesalse__add_column_is_uploaded_fboc cannot be reverted.\n";

//        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
