<?php

use yii\db\Migration;

class m170508_100605_create_tbl_affiliate_courses extends Migration
{

    public function up()
    {
        $this->createTable('{{%affiliate_category_courses}}', [
            'id' => $this->primaryKey(),
            'course_id' => $this->integer(11)->notNull(),
            'affiliate_category_id' => $this->integer(11)->defaultValue(0),
            'commission_percent' => $this->float()->notNull(),
            'status' => $this->smallInteger(3)->defaultValue(1),
            'created_time' => $this->integer(10)->defaultValue(0),
            'updated_time' => $this->integer(10)->defaultValue(0),
        ]);

        $this->createIndex('index_unique_course_aff_category', '{{%affiliate_category_courses}}', ['course_id', 'affiliate_category_id'], true);
    }

    public function down()
    {
        $this->dropTable('{{%affiliate_category_courses}}');
    }
}
