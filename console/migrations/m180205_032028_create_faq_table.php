<?php

use yii\db\Migration;

/**
 * Handles the creation for table `faq`.
 */
class m180205_032028_create_faq_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('faq', [
            'id' => $this->primaryKey(),
            'title' => $this->string()->notNull(),
            'slug' => $this->string()->notNull(),
            'category_id' => $this->integer(11)->notNull(),
            'content' => 'MEDIUMTEXT not null',
            'order' => $this->integer(11),
            'is_important' => $this->integer(11),
            'is_deleted' => $this->integer(11),
            'status' => $this->integer(11)->defaultValue(0),
            'created_time' => $this->integer(11),
            'updated_time' => $this->integer(11),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('faq');
    }
}
