<?php

use yii\db\Migration;

class m161008_044937_alter_tbl_order_add_index_for_created_time_and_status extends Migration
{
    
    public function up()
    {
        $this->createIndex('index_status_created_time', '{{%orders}}', ['status', 'created_time']);
    }

    public function down()
    {
        $this->dropIndex('index_status_created_time', '{{%orders}}');

        return true;
    }

}
