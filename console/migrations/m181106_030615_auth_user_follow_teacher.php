<?php

use yii\db\Migration;

class m181106_030615_auth_user_follow_teacher extends Migration
{
    const USER_FOLLOW_TEACHER = "user_follow_teacher";
    public function up()
    {
        $this->createTable(self::USER_FOLLOW_TEACHER, [
            'id' => $this->primaryKey(),
            'user_follow' => $this->integer()->notNull(),
            'user_teacher' => $this->integer()->notNull(),
            'created_time' => $this->integer(10)->defaultValue(0),
            'updated_time' => $this->integer(10)->defaultValue(0),
        ]);
    }

    public function down()
    {
        $this->dropTable(self::USER_FOLLOW_TEACHER);
    }
}
