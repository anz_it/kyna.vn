<?php

use yii\db\Migration;

class m161122_082959_coupon_sesson extends Migration
{
    public function up()
    {
        $this->addColumn(\kyna\promo\models\Coupon::tableName(), "session", "varchar(50) null");
        $this->addColumn(\kyna\promo\models\Coupon::tableName(), "apply_for_all", "int(1) default 0");
    }

    public function down()
    {
        echo "m161122_082959_coupon_sesson cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
