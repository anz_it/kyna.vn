<?php

use yii\db\Migration;

class m160401_041104_alter_tbl_orders_add_activation_code extends \console\components\KynaMigration
{

    public function up()
    {
        // add `activation_code` to `orders` table
        $this->addColumn('{{%orders}}', 'activation_code', 'VARCHAR(16) AFTER `coupon_code`');
        $this->createIndex('index_user_activation_code', '{{%orders}}', ['user_id', 'activation_code']);
        
        // drop column `activation_code` from `user_courses` table
        $this->dropColumn('{{%user_courses}}', 'activation_code');
    }

    public function down()
    {
        // add column `activation_code` back to `user_courses` table
        $this->addColumn('{{%user_courses}}', 'activation_code', 'VARCHAR(16) AFTER `course_id`');
        
        // drop `activation_code` from `orders` table
        $this->dropIndex('index_user_activation_code', '{{%orders}}');
        $this->dropColumn('{{%orders}}', 'activation_code');
        
        echo "m160401_041104_alter_tbl_orders_add_activation_code has been reverted.\n";

        return true;
    }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
