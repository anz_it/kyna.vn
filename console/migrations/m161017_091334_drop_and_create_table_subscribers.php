<?php

use yii\db\Migration;

/**
 * Handles the dropping for table `table_subscribers`.
 */
class m161017_091334_drop_and_create_table_subscribers extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->dropTable('{{%subscribers}}');
        
        $this->createTable('{{%subscribers}}', [            
            'email' => $this->string()->notNull(),
            'landing_page' => $this->string(),
            'subscribe_time' => $this->integer(11),
            'opt_out' => $this->smallInteger(2)->defaultValue(0),
            'created_time' => $this->integer(11),
            'updated_time' => $this->integer(11),
            'PRIMARY KEY (`email`,`landing_page`)'
        ]);
        
        
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%subscribers}}');
        return true;
    }
}
