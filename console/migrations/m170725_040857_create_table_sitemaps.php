<?php

use yii\db\Migration;

/**
 * Class m170725_040857_create_table_sitemaps
 * Create table sitemaps
 * Id -> integer
 * Name -> string
 * Parents -> integer
 * Priority -> float [0,1]
 *
 *
 */
//1. Create table sitemap
//2. Create permission Setting.Sitemap
//3. Add Setting.Sitemap to Setting.All

class m170725_040857_create_table_sitemaps extends Migration
{

  public function up()
  {
      // 1.
    $this->createTable('sitemaps', [
        'id' => $this->primaryKey(11),
        'name' => $this->string(20),
        'parent' => $this->integer(11)->defaultValue(0),
        'changefreg' => $this->string(10)->defaultValue('2')->comment('always | hourly | daily | weekly | monthly | yearly | never'),
        'priority' => $this->float(1)->defaultValue(0.5),
        'query_strings' => $this->text()->comment('JSON FORMAT'),
        'options' => $this->text()->comment('JSON FORMAT'),

        'is_enabled' => $this->smallInteger(1)->defaultValue(1),

        'is_deleted' => $this->smallInteger(1)->defaultValue(0),

        'created_by' => $this->integer(11),
        'created_time' => $this->integer(10),
        'updated_by' => $this->integer(11),
        'updated_time' => $this->integer(10),
    ]);
      // 1.5 Insert entries: categories, courses, tags, landing_pages
      $this->insert('sitemaps', [
            'name' => 'courses',
            'priority' => 1,
            'created_time' => time(),
            'updated_time' => time(),
      ]);
      $this->insert('sitemaps', [
          'name' => 'categories',
          'priority' => 0.8,
          'created_time' => time(),
          'updated_time' => time(),
      ]);
      $this->insert('sitemaps', [
          'name' => 'tags',
          'created_time' => time(),
          'updated_time' => time(),
      ]);
      $this->insert('sitemaps', [
          'name' => 'landing_pages',
          'created_time' => time(),
          'updated_time' => time(),
      ]);

      // 2.
    Yii::$app->authManager->add(Yii::$app->authManager->createPermission('Setting.Sitemap'));
      // 3.
    Yii::$app->authManager->addChild(Yii::$app->authManager->getPermission('Setting.All'), Yii::$app->authManager->getPermission('Setting.Sitemap'));

  }

  public function down()
  {
      // 3.
      Yii::$app->authManager->removeChild(Yii::$app->authManager->getPermission('Setting.All'), Yii::$app->authManager->getPermission('Setting.Sitemap'));
      // 2.
      Yii::$app->authManager->remove(Yii::$app->authManager->getPermission('Setting.Sitemap'));
      // 1.
        $this->dropTable('sitemaps');


//        echo "m170725_040857_create_table_sitemaps cannot be reverted.\n";

//        return false;
  }

  /*
  // Use safeUp/safeDown to run migration code within a transaction
  public function safeUp()
  {
  }

  public function safeDown()
  {
  }
  */
}
