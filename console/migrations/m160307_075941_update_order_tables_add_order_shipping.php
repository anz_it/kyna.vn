<?php

use yii\db\Migration;

class m160307_075941_update_order_tables_add_order_shipping extends Migration
{
    public function up()
    {
        $this->createTable('{{%order_shipping}}', [
            'id' => $this->primaryKey(),
            'order_id' => $this->integer(11),
            'method_id' => $this->integer(11),
            'receiver_name' => $this->string(30),
            'phone_number' => $this->string(15),
            'street_address' => $this->string(255),
            'location_id' => $this->integer(11),
            'shipping_code' => $this->string(45),
            'fee' => $this->double(),
            'status' => $this->integer(4),
            'created_time' => $this->integer(10)->defaultValue(0),
            'updated_time' => $this->integer(10)->defaultValue(0),
        ]);

        $this->renameColumn('{{%orders}}', 'shipping_method_id', 'shipping_method');
        $this->alterColumn('{{%orders}}', 'shipping_method', 'varchar(20)');

        $this->dropColumn('{{%orders}}', 'shipping_address');
        $this->dropColumn('{{%orders}}', 'shipping_fee');
        $this->dropColumn('{{%orders}}', 'shipping_code');
        return true;
    }

    public function down()
    {
        echo "m160307_075941_update_order_tables_add_order_shipping cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
