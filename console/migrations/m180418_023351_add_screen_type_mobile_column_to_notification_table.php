<?php

use yii\db\Migration;

/**
 * Handles adding screen_type_mobile to table `notification`.
 */
class m180418_023351_add_screen_type_mobile_column_to_notification_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('notification', 'screen_type_mobile', $this->integer());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('notification', 'screen_type_mobile');
    }
}
