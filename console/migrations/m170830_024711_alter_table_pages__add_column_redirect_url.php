<?php

use yii\db\Migration;

class m170830_024711_alter_table_pages__add_column_redirect_url extends Migration
{
    public function up()
    {
        $this->addColumn(
            'pages',
            'redirect_url',
            $this->text()
        );

    }

    public function down()
    {
        $this->dropColumn(
            'pages',
            'redirect_url'
        );

//        echo "m170830_024711_alter_table_pages__add_column_redirect_url cannot be reverted.\n";
//
//        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
