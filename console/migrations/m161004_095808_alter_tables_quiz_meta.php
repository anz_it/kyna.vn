<?php

use yii\db\Migration;

class m161004_095808_alter_tables_quiz_meta extends Migration
{
    public function up()
    {
        $this->alterColumn('{{%quiz_meta}}', 'course_id', "int(11) NULL");
    }

    public function down()
    {
        $this->alterColumn('{{%quiz_meta}}', 'course_id', "int(11) NOT NULL");

        return TRUE;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
