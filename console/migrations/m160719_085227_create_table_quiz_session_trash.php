<?php

use yii\db\Migration;

/**
 * Handles the creation for table `table_quiz_session_trash`.
 */
class m160719_085227_create_table_quiz_session_trash extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('{{%quiz_session_trash}}', [
            'id' => $this->primaryKey(),
            'user_id'   => $this->integer(11),
            'quiz_id'   => $this->integer(11),
            'start_time'    => $this->integer(11),
            'duration'  => $this->integer(11),
            'is_passed' => $this->boolean(),
            'total_score'   => $this->integer(10),
            'status'    => $this->smallInteger(2),
            'last_interactive' => $this->integer(11),
            'submit_time'   => $this->integer(11),
            'time_remaining'    => $this->integer(11)
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%quiz_session_trash}}');
        return true;
    }
}
