<?php

use yii\db\Migration;

class m160803_033540_alter_tbl_user_telesales_add_col_recall_date_and_col_call_status extends Migration
{

    public function up()
    {
        $this->addColumn('{{%user_telesales}}', 'recall_date', 'INT(10) AFTER last_call_date');
        $this->addColumn('{{%user_telesales}}', 'follow_num', 'INT(10) DEFAULT 0 AFTER recall_date');
    }

    public function down()
    {
        $this->dropColumn('{{%user_telesales}}', 'follow_num');
        $this->dropColumn('{{%user_telesales}}', 'recall_date');

        return true;
    }

}
