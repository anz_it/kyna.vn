<?php

use yii\db\Migration;

class m160623_103714_alter_tbl_orders_add_index_for_affiliate extends Migration
{

    public function safeUp()
    {
        $this->createIndex('index_affiliate_id', '{{%orders}}', 'affiliate_id');
        
        $this->createIndex('index_order_id', '{{%commission_incomes}}', 'order_id');
        $this->createIndex('index_user_id', '{{%commission_incomes}}', 'user_id');
        $this->createIndex('index_teacher_id', '{{%commission_incomes}}', 'teacher_id');
        $this->createIndex('index_course_id', '{{%commission_incomes}}', 'course_id');
        
        
    }

    public function safeDown()
    {
        $this->dropIndex('index_affiliate_id', '{{%orders}}');
        
        $this->dropIndex('index_order_id', '{{%commission_incomes}}');
        $this->dropIndex('index_user_id', '{{%commission_incomes}}');
        $this->dropIndex('index_teacher_id', '{{%commission_incomes}}');
        $this->dropIndex('index_course_id', '{{%commission_incomes}}');
        
        return true;
    }

}
