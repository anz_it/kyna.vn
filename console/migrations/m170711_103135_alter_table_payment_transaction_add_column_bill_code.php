<?php

use yii\db\Migration;

use kyna\payment\models\PaymentTransaction;

class m170711_103135_alter_table_payment_transaction_add_column_bill_code extends Migration
{
    public function up()
    {
        $paymentTransactionTableName = PaymentTransaction::tableName();

        $this->addColumn(
            $paymentTransactionTableName,
            'bill_code',
            $this->string(30)
        );
    }

    public function down()
    {
//        echo "m170711_103135_alter_table_payment_transaction_add_column_bill_code cannot be reverted.\n";

//        return false;
        $paymentTransactionTableName = PaymentTransaction::tableName();

        $this->dropColumn(
            $paymentTransactionTableName,
            'bill_code'
        );

    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
