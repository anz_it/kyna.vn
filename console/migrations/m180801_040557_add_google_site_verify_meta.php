<?php

use yii\db\Migration;

class m180801_040557_add_google_site_verify_meta extends Migration
{
    public function up()
    {
        $this->insert('meta_fields', [
            'key' => 'google_site_verification',
            'name' => 'Google Site Verification Code',
            'type' => 1,
            'model' => \kyna\base\models\MetaField::MODEL_SETTING,
            'is_required' => 0,
            'status' => 1,
            'created_time' => time(),
        ]);
    }

    public function down()
    {
        $this->delete('meta_fields', ['key' => 'google_site_verification']);
    }
}
