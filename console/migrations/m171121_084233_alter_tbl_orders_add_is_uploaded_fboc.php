<?php

use yii\db\Migration;

class m171121_084233_alter_tbl_orders_add_is_uploaded_fboc extends Migration
{
    public function up()
    {
        $this->addColumn(
            'orders',
            'is_uploaded_fboc',
            $this->boolean()->defaultValue(false)->comment('is uploaded to Facebook Offline Conversion')
        );
    }

    public function down()
    {
        $this->dropColumn(
            'orders',
            'is_uploaded_fboc'
        );
    }

}
