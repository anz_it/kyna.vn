<?php

use yii\db\Migration;

class m161006_083235_alter_tbl_auth_assignment_add_index_user_id extends Migration
{
    public function up()
    {
        $this->createIndex('index_user_id', '{{%auth_assignment}}', 'user_id');
    }

    public function down()
    {
        $this->dropIndex('index_user_id', '{{%auth_assignment}}');

        return true;
    }

}
