<?php

use yii\db\Migration;

class m171005_092317_banner_for_mobile_app extends Migration
{
    public function up()
    {
        $this->addColumn('banners', 'tag', $this->text());
        $this->addColumn('banners', 'keyword', $this->text());
    }

    public function down()
    {
        $this->dropColumn('banners', 'keyword');
        $this->dropColumn('banners', 'tag');
    }

}
