<?php

use yii\db\Migration;

class m161004_084627_alter_table_quiz extends Migration
{
    public function up()
    {
        $this->addColumn('{{%quizes}}', 'is_updated', "BIT(1) DEFAULT 0");
    }

    public function down()
    {
        $this->dropColumn('{{%quizes}}', 'is_updated');

        return TRUE;
    }

}
