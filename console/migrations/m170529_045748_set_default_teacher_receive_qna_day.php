<?php

use yii\db\Migration;
use kyna\settings\models\TeacherSetting;
use kyna\user\models\AuthAssignment;

class m170529_045748_set_default_teacher_receive_qna_day extends Migration
{
    public function up()
    {
        $settingTblName = TeacherSetting::tableName();
        $authAssignTblName = AuthAssignment::tableName();
        $receiveDayKey = TeacherSetting::KEY_RECEIVE_MAIL_DATE;

        $sql = "insert into {$settingTblName} (teacher_id, `key`, `value`) 
            select user_id, '{$receiveDayKey}' as `key`, 'Monday' as `value`
            from {$authAssignTblName} where item_name = 'Teacher' and user_id not in (select teacher_id from {$settingTblName})
        ;";

        Yii::$app->db->createCommand($sql)->execute();
    }

    public function down()
    {
        $settingTblName = TeacherSetting::tableName();

        $sql = "delete from {$settingTblName} where updated_time = 0";

        Yii::$app->db->createCommand($sql)->execute();
    }
}
