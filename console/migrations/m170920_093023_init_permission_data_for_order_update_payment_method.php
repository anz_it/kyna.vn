<?php

use yii\db\Migration;

class m170920_093023_init_permission_data_for_order_update_payment_method extends Migration
{
    public function up()
    {
        $authManager = Yii::$app->authManager;

        // Create permissions
        $authManager->add($authManager->createPermission('Order.Action.UpdatePaymentMethod'));

        $authManager->addChild($authManager->getPermission('Order.All'), $authManager->getPermission('Order.Action.UpdatePaymentMethod'));
        $authManager->addChild($authManager->getPermission('Order.ForTelesale'), $authManager->getPermission('Order.Action.UpdatePaymentMethod'));
        $authManager->addChild($authManager->getRole('CustomerService'), $authManager->getPermission('Order.Action.UpdatePaymentMethod'));
    }

    public function down()
    {
        $authManager = Yii::$app->authManager;

        $authManager->removeChild($authManager->getPermission('Order.All'), $authManager->getPermission('Order.Action.UpdatePaymentMethod'));
        $authManager->removeChild($authManager->getPermission('Order.ForTelesale'), $authManager->getPermission('Order.Action.UpdatePaymentMethod'));
        $authManager->removeChild($authManager->getRole('CustomerService'), $authManager->getPermission('Order.Action.UpdatePaymentMethod'));

        // Delete permisstion
        $authManager->remove($authManager->getPermission('Order.Action.UpdatePaymentMethod'));
    }

}
