<?php

use yii\db\Migration;

class m160801_072711_alter_table_topup_transaction extends Migration
{
    public function up()
    {
        $this->addColumn('{{%topup_transactions}}', 'is_topup', 'BIT(1)');
        $this->addColumn('{{%topup_transactions}}', 'error_code', $this->smallInteger(4));
        $this->addColumn('{{%topup_transactions}}', 'message', $this->string());
    }

    public function down()
    {

        $this->dropColumn('{{%topup_transactions}}', 'is_topup');
        $this->dropColumn('{{%topup_transactions}}', 'error_code');
        $this->dropColumn('{{%topup_transactions}}', 'message');
        return true;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
