<?php

use yii\db\Migration;

class m171101_033124_alter_tbl_user_points_add_col_k_point_usable extends Migration
{
    public function up()
    {
        $this->addColumn('user_points', 'k_point_usable', $this->integer(10)->defaultValue(0));
        $this->addColumn('user_point_histories', 'usable_date', $this->integer(10));
        $this->addColumn('user_point_histories', 'is_added', $this->integer(1));

        $this->createIndex('index_type', 'user_point_histories', 'type');
    }

    public function down()
    {
        $this->dropColumn('user_points', 'k_point_usable');
        $this->dropColumn('user_point_histories', 'usable_date');
        $this->dropColumn('user_point_histories', 'is_added');

        $this->dropIndex('index_type', 'user_point_histories');
    }

}
