<?php

use yii\db\Migration;

class m170114_140519_add_extra_column_tracking extends Migration
{
    public function up()
    {
        $this->addColumn("tracking", "last_access", "int null");
        $this->addColumn("tracking", "note", "text null");
        $this->addColumn("tracking", "profile", "text null");
        $this->addColumn("tracking", "page_id", "int null");
        $this->addColumn("tracking", "user_id_care", "int null");
        $this->addColumn("tracking", "is_registered", "int null default 0");
    }

    public function down()
    {

        $this->dropColumn("tracking", "user_id_care");
        $this->dropColumn("tracking", "page_id");
        $this->dropColumn("tracking", "profile");
        $this->dropColumn("tracking", "note");
        $this->dropColumn("tracking", "last_access");
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
