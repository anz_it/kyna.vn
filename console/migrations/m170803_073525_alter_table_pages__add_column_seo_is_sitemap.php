<?php
/**
 * Add column seo_is_sitemap to table pages
 */
use yii\db\Migration;

class m170803_073525_alter_table_pages__add_column_seo_is_sitemap extends Migration
{
    public function up()
    {

        $this->addColumn(
            'pages',
            'seo_is_sitemap',
            $this->smallInteger(1)->defaultValue(true)
        );

    }

    public function down()
    {
        $this->dropColumn('pages', 'seo_is_sitemap');

//        echo "m170803_073525_alter_table_pages__add_column_seo_is_sitemap cannot be reverted.\n";

//        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
