<?php

use yii\db\Migration;

class m160930_063420_alter_tbl_orders_add_is_telesale_processed extends Migration
{

    public function up()
    {
        $this->addColumn('{{%orders}}', 'is_done_telesale_process', "BIT(1) DEFAULT 0");
    }

    public function down()
    {
        $this->dropColumn('{{%orders}}', 'is_done_telesale_process');

        return TRUE;
    }

}
