<?php

use yii\db\Migration;

class m170821_034955_alter_table_categories_add_column_seo_description extends Migration
{
    public function up()
    {
        $this->addColumn('{{%categories}}', 'seo_description', $this->text());
    }

    public function down()
    {
        $this->dropColumn('{{%categories}}', 'seo_description');

        return true;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
