<?php

use yii\db\Migration;

class m170823_043104_gamification_create_tbl_gift extends Migration
{
    public function up()
    {
        $this->createTable('{{%gifts}}', [
            'id' => $this->primaryKey(11),
            'title' => $this->string(64)->notNull(),
            'image_url' => $this->text(),
            'k_point' => $this->integer(10)->notNull()->defaultValue(0),
            'expiration_date' => $this->integer(10),
            'is_deleted' => 'BIT(1) DEFAULT 0',
            'status' => $this->smallInteger(3)->defaultValue(0),
            'created_time' => $this->integer(10)->defaultValue(0),
            'created_user_id' => $this->integer(11),
            'updated_time' => $this->integer(10)->defaultValue(0),
            'updated_user_id' => $this->integer(11)
        ]);

        $this->createTable('{{%gift_contents}}', [
            'id' => $this->primaryKey(11),
            'gift_id' => $this->integer(11)->notNull(),
            'type' => $this->string(64)->notNull(),
            'discount_type' => $this->smallInteger(2)->defaultValue(0),
            'discount_value' => $this->integer(10)->defaultValue(0),
            'min_amount' => $this->integer(7)->defaultValue(0)
        ]);

        $this->createIndex('index_gift_id', '{{%gift_contents}}', 'gift_id');

        $this->createTable('{{%user_gifts}}', [
            'id' => $this->primaryKey(11),
            'user_id' => $this->integer(11)->notNull(),
            'gift_id' => $this->integer(11)->notNull(),
            'code' => $this->string(32),
            'used_date' => $this->integer(10),
            'created_time' => $this->integer(10)->defaultValue(0),
        ]);

        $this->createIndex('index_user', '{{%user_gifts}}', 'user_id');
        $this->createIndex('index_gift', '{{%user_gifts}}', 'gift_id');
    }

    public function down()
    {
        $this->dropTable('{{%user_gifts}}');

        $this->dropTable('{{%gift_contents}}');

        $this->dropTable('{{%gifts}}');
    }
}
