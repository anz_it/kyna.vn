<?php

use yii\db\Migration;

class m170506_074731_add_title_column extends Migration
{
    public function up()
    {

        $this->addColumn(\kyna\course\models\Category::tableName(), 'title', 'text null');
    }

    public function down()
    {
       $this->dropColumn(\kyna\course\models\Category::tableName(), 'title');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
