<?php

use yii\db\Migration;

class m190214_074450_add_column_apply_special_course_table_promotion extends Migration
{
    const PROMOTION = "promotion";
    public function up()
    {
        $this->addColumn(self::PROMOTION,'apply_special_course',
            $this->integer()->after('apply_all_combo')->defaultValue(1));
    }

    public function down()
    {
        $this->dropColumn(self::PROMOTION,'apply_special_course');
    }
}
