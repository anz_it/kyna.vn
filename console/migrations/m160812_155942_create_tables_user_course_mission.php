<?php

use yii\db\Migration;

class m160812_155942_create_tables_user_course_mission extends Migration
{
    public function up()
    {
        $this->createTable('{{%user_course_missions}}',[
            'id'    =>  $this->primaryKey(),
            'course_mission_id' => $this->integer(),
            'user_id'           => $this->integer(),
            'is_passed'         => $this->boolean(),
            'created_date'      => $this->integer(),
            'deadline_date'     => $this->integer(),
            'finished_date'     => $this->integer(),
            'course_id'         => $this->integer(),
            'current_day'       => $this->integer(),
            'user_name'         => $this->string(),
            'v2_id'             => $this->integer()
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%user_course_missions}}');
        return true;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
