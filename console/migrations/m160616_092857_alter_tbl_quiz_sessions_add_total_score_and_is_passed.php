<?php

use yii\db\Migration;

class m160616_092857_alter_tbl_quiz_sessions_add_total_score_and_is_passed extends Migration
{

    public function safeUp()
    {
        $this->addColumn('{{%quiz_sessions}}', 'is_passed', 'BIT(1) DEFAULT 0');
        $this->addColumn('{{%quiz_sessions}}', 'total_score', 'INT(10) DEFAULT 0');
    }

    public function safeDown()
    {
        $this->dropColumn('{{%quiz_sessions}}', 'total_score');
        $this->dropColumn('{{%quiz_sessions}}', 'is_passed');
        return true;
    }

}
