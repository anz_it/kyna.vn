<?php

use yii\db\Migration;

class m161125_065402_alter_tbl_orders_add_is_activated extends Migration
{

    public function up()
    {
        $this->addColumn('{{%orders}}', 'is_activated', 'BIT(1) DEFAULT 0');
        $this->addColumn('{{%orders}}', 'activation_date', $this->integer(10));
    }

    public function down()
    {
        $this->dropColumn('{{%orders}}', 'activation_date');
        $this->dropColumn('{{%orders}}', 'is_activated');

        return true;
    }

}
