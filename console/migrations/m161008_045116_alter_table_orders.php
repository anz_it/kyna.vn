<?php

use yii\db\Migration;

class m161008_045116_alter_table_orders extends Migration
{
    public function up()
    {
        $this->alterColumn('{{%orders}}', 'activation_code', "varchar(50)");
    }

    public function down()
    {
        $this->alterColumn('{{%orders}}', 'activation_code', "varchar(16)");

        return TRUE;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
