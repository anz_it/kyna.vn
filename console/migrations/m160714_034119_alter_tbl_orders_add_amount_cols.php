<?php

use yii\db\Migration;

class m160714_034119_alter_tbl_orders_add_amount_cols extends Migration
{

    public function safeUp()
    {
        $this->addColumn('{{%orders}}', 'sub_total', $this->decimal(10, 2));
        $this->addColumn('{{%orders}}', 'total_discount', $this->decimal(10, 2));
        $this->addColumn('{{%orders}}', 'shipping_fee', $this->decimal(10, 2));
        $this->addColumn('{{%orders}}', 'total', $this->decimal(10, 2));
        $this->addColumn('{{%orders}}', 'payment_fee', $this->decimal(10, 2));
    }

    public function safeDown()
    {
        $this->dropColumn('{{%orders}}', 'payment_fee');
        $this->dropColumn('{{%orders}}', 'total');
        $this->dropColumn('{{%orders}}', 'shipping_fee');
        $this->dropColumn('{{%orders}}', 'total_discount');
        $this->dropColumn('{{%orders}}', 'sub_total');

        return true;
    }

}
