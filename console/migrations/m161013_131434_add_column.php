<?php

use yii\db\Migration;

class m161013_131434_add_column extends Migration
{
    public function up()
    {
        $this->addColumn(\kyna\user\models\UserCourse::tableName(), "current_section", "int null");
    }

    public function down()
    {
        echo "m161013_131434_add_column cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
