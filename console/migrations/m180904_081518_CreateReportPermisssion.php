<?php

use yii\db\Migration;

class m180904_081518_CreateReportPermisssion extends Migration
{
    public function up()
    {
        $auth = Yii::$app->authManager;

        $reportView = $auth->createPermission('Report.View');
        $reportView->description = 'Report View';
        $auth->add($reportView);

        $reportUpdate = $auth->createPermission('Report.Update');
        $reportUpdate->description = 'Report Update';
        $auth->add($reportUpdate);


        $reportCreate = $auth->createPermission('Report.Create');
        $reportCreate->description = 'Report Create';
        $auth->add($reportCreate);

        $reportDelete = $auth->createPermission('Report.Delete');
        $reportDelete->description = 'Report Delete';
        $auth->add($reportDelete);


        $reportRun = $auth->createPermission('Report.Run');
        $reportRun->description = 'Report Run';
        $auth->add($reportRun);


        $reportAll = $auth->createPermission('Report.All');
        $reportAll->description = 'Report All';
        $auth->add($reportAll);

        $auth->addChild($reportAll, $reportCreate);
        $auth->addChild($reportAll, $reportDelete);
        $auth->addChild($reportAll, $reportUpdate);
        $auth->addChild($reportAll, $reportRun);
        $auth->addChild($reportAll, $reportView);
        //=============================================================================
        //add permission for Admin role
        $admin = $auth->getRole('Admin');
        $auth->addChild($admin, $reportAll);



    }

    public function down()
    {
        echo "m180904_081518_CreateReportPermisssion cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
