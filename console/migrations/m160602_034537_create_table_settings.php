<?php

use yii\db\Migration;

/**
 * Handles the creation for table `table_settings`.
 */
class m160602_034537_create_table_settings extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('{{%settings}}', [
            'id'    => $this->primaryKey(),
            'key'   => $this->string(255),
            'value' => 'BLOB',
            'is_deleted' => "bit(1) DEFAULT b'0'",
            'created_time' => $this->integer(10)->defaultValue(0),
            'updated_time' => $this->integer(10)->defaultValue(0),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%settings}}');
        return true;
    }
}
