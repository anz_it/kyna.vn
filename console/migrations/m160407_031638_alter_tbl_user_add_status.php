<?php

class m160407_031638_alter_tbl_user_add_status extends console\components\KynaMigration
{

    public function up()
    {
        $this->addColumn('{{%user}}', 'status', 'smallint(3) DEFAULT 1 after registration_ip');
    }

    public function down()
    {
        $this->dropColumn('{{%user}}', 'status');
        
        echo "m160407_031638_alter_tbl_user_add_status has been reverted.\n";

        return true;
    }

}
