<?php

use yii\db\Migration;

class m161110_101503_add_video_sound_to_question extends Migration
{
    public function up()
    {

        $this->addColumn(\kyna\course\models\QuizQuestion::tableName(), "sound_url", "varchar(255) null");
        $this->addColumn(\kyna\course\models\QuizQuestion::tableName(), "video_embed", "text null");
    }

    public function down()
    {
        echo "m161110_101503_add_video_sound_to_question cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
