<?php

use yii\db\Migration;

class m170629_021406_create_table_user_fcm_token extends Migration
{
    public function up()
    {
        // Create table notification
        $this->createTable("{{%notification}}", [
            'id' => $this->primaryKey(11),
            'name' => $this->string(256)->notNull()->unique(),
            'priority' => $this->integer(2)->notNull()->defaultValue(1)->comment('0: low, 1: medium, 2: high'),    // 0: low, 1: medium, 2: high
            'description' => $this->string(2048),

            // Notification details
            'title' =>  $this->string(512),
            'body' => $this->string(1024),
            'icon' => $this->string(1024)->comment('icon url'),
            'image' => $this->string(1024)->comment('image url'),
            'redirect_url' => $this->string(1024)->comment('onclick redirect url'),
            'actions' => $this->string(4096)->defaultValue('')->comment('JSON ENDCODE, actions params, hien nay chua xai toi'),

            'is_deleted' => $this->boolean()->notNull()->defaultValue(false),

            // Base
            'created_at' => $this->integer(10),
            'created_by' => $this->integer(11),
            'modified_at' => $this->integer(10),
            'modified_by' => $this->integer(11),
        ]);

        // Create table notification_schedule
        $this->createTable("{{%notification_schedule}}", [
            'id' => $this->primaryKey(11),
            'notification_id' => $this->integer(11)->notNull(),
            'is_enabled' => $this->integer(1)->notNull()->defaultValue(false),
            'type' => $this->integer(2)->notNull()->defaultValue(0)->comment('0: onetime, 1: daily, 2: weekly, 3: monthly'),


            'start_time' => $this->integer(10),
            'last_occure' => $this->integer(10),
            'next_occure' => $this->integer(10),

            'setting' => $this->string(4096)->defaultValue('')->comment('JSON ENCODE, setting'),
            'adv_setting' => $this->string(4096)->defaultValue('')->comment('JSON ENCODE, advanced setting'),

            'is_deleted' => $this->boolean()->notNull()->defaultValue(false),

            // Base
            'created_at' => $this->integer(10),
            'created_by' => $this->integer(11),
            'modified_at' => $this->integer(10),
            'modified_by' => $this->integer(11),
        ]);

        // Create table notification_receiver
        $this->createTable("{{%notification_receiver}}", [
            'notification_id' => $this->integer(11),
            'type' => $this->integer(2)->comment('0: group, 1: user, 2: token'),
            'receiver_code' => $this->string(2048),

            'is_deleted' => $this->boolean()->notNull()->defaultValue(false),

            // Base
            'created_at' => $this->integer(10),
            'created_by' => $this->integer(11),
            'modified_at' => $this->integer(10),
            'modified_by' => $this->integer(11),
        ]);

        // Create table user_receiver
        $this->createTable("{{notification_user_token}}", [
            'user_id' => $this->integer(11),
            'token' => $this->string(1024),

            'created_at' => $this->integer(10),
            'modified_at' => $this->integer(10),
        ]);
    }

    public function down()
    {
        $this->dropTable("{{%notification}}");
        $this->dropTable("{{%notification_schedule}}");
        $this->dropTable("{{%notification_receiver}}");
        $this->dropTable("{{%notification_user_token}}");


        return true;
//        echo "m170629_021406_create_table_user_fcm_token cannot be reverted.\n";

//        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
