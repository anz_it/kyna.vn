<?php

use yii\db\Migration;

/**
 * Handles the creation for table `recommendation_combo`.
 */
class m180420_022504_create_recommendation_combo_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('recommendation_combo', [
            'id' => $this->primaryKey(),
            'combo_id' => $this->integer(),
            'courses_id' => $this->text(),
            'total' => $this->integer(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('recommendation_combo');
    }
}
