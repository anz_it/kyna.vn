<?php

use yii\db\Migration;
use kyna\user\models\UserCourse;

class m170704_035151_alter_tbl_user_course_add_col_is_requested_rating extends Migration
{
    public function up()
    {
        $userCourseTblName = UserCourse::tableName();

        $this->addColumn($userCourseTblName, 'is_requested_rating', $this->smallInteger(1)->defaultValue(0));
    }

    public function down()
    {
        $userCourseTblName = UserCourse::tableName();

        $this->dropColumn($userCourseTblName, 'is_requested_rating');
    }

}
