<?php

use yii\db\Migration;

class m160727_023619_alter_tbl_group_discount_change_tbl_name_and_add_max_discount_amount extends Migration
{

    public function up()
    {
        $this->renameTable('{{%group_discount}}', '{{%group_discounts}}');
        
        $this->addColumn('{{%group_discounts}}', 'max_discount_amount', "INT(10) AFTER percent_discount");
    }

    public function down()
    {
        $this->dropColumn('{{%group_discounts}}', 'max_discount_amount');
        
        $this->renameTable('{{%group_discounts}}', '{{%group_discount}}');

        return true;
    }

}
