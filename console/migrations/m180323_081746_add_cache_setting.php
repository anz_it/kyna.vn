<?php

use yii\db\Migration;

class m180323_081746_add_cache_setting extends Migration
{
    public function up()
    {
        $this->insert('meta_fields', [
            'key' => 'cache_version',
            'name' => 'Cache version (using timestamp from https://www.unixtimestamp.com/)',
            'type' => 1,
            'model' => \kyna\base\models\MetaField::MODEL_SETTING,
            'is_required' => 1,
            'status' => 1,
            'created_time' => time(),
        ]);
    }

    public function down()
    {
        $this->delete('meta_fields', ['key' => 'cache_version']);
    }
}
