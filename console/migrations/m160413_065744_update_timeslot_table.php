<?php

use yii\db\Migration;

class m160413_065744_update_timeslot_table extends Migration
{
    public function up()
    {
        $this->alterColumn('{{%time_slot}}', 'start_time', $this->string(5));
        $this->alterColumn('{{%time_slot}}', 'end_time', $this->string(5));
    }

    public function down()
    {
        echo "m160413_065744_update_timeslot_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
