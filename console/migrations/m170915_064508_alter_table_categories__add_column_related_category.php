<?php

use yii\db\Migration;

class m170915_064508_alter_table_categories__add_column_related_category extends Migration
{
    public function up()
    {
        $this->addColumn('categories', 'related_categories_id', $this->text());
    }

    public function down()
    {
        $this->dropColumn('categories', 'related_categories_id');

//        echo "m170915_064508_alter_table_categories__add_column_related_category cannot be reverted.\n";

//        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
