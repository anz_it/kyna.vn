<?php

class m160407_083652_is_deleted extends console\components\KynaMigration
{
    
    private $_solfDeleteTables = [
        '{{%categories}}',
        '{{%courses}}',
        '{{%course_lessons}}',
        '{{%course_sections}}',
        
        '{{%locations}}',
        '{{%meta_fields}}',
        '{{%user}}',
    ];

    public function up()
    {
        foreach ($this->_solfDeleteTables as $table) {
            $this->addColumn($table, 'is_deleted', "bit(1) DEFAULT b'0' AFTER status");
        }
    }

    public function down()
    {
        foreach ($this->_solfDeleteTables as $table) {
            $this->dropColumn($table, 'is_deleted');
        }
        
        echo "m160407_083652_is_deleted has been reverted.\n";

        return true;
    }

}
