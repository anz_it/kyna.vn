<?php

use yii\db\Migration;

class m170915_042930_init_data_for_hacking_coupon_can_use_for_combo extends Migration
{
    public function up()
    {
        $this->insert('meta_fields', [
            'key' => 'code_can_use_for_combo',
            'name' => 'Coupon có thể áp dụng cho Combo',
            'type' => 1,
            'model' => \kyna\base\models\MetaField::MODEL_SETTING,
            'status' => 1,
            'created_time' => time(),
        ]);
    }

    public function down()
    {
        $this->delete('meta_fields', ['key' => 'code_can_use_for_combo']);
    }
}
