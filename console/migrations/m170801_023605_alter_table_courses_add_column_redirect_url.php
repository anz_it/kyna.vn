<?php

use yii\db\Migration;

class m170801_023605_alter_table_courses_add_column_redirect_url extends Migration
{
    public function up()
    {
        $this->addColumn(
            'courses',
            'redirect_url',
            $this->text());
    }

    public function down()
    {
        $this->dropColumn('courses', 'redirect_url');
//        echo "m170801_023605_alter_table_courses_add_column_redirect_url cannot be reverted.\n";

//        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
