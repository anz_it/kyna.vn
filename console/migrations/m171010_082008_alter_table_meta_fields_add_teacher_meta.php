<?php

use yii\db\Migration;

class m171010_082008_alter_table_meta_fields_add_teacher_meta extends Migration
{
    public function up()
    {
        $this->insert('{{%meta_fields}}', [
            'key' => 'seo_title',
            'name' => 'Seo Title',
            'type' => 1,
            'model' => 'teacher',
            'status' => 1
        ]);
        $this->insert('{{%meta_fields}}', [
            'key' => 'seo_description',
            'name' => 'Meta Description',
            'type' => 2,
            'model' => 'teacher',
            'status' => 1
        ]);
        $this->insert('{{%meta_fields}}', [
            'key' => 'seo_keyword',
            'name' => 'Meta Keyword',
            'type' => 2,
            'model' => 'teacher',
            'status' => 1
        ]);
        $this->insert('{{%meta_fields}}', [
            'key' => 'seo_robot_index',
            'name' => 'Meta Robots Index',
            'type' => 7,
            'data_set' => '{"index":"index","noindex":"noindex"}',
            'model' => 'teacher',
            'status' => 1
        ]);
        $this->insert('{{%meta_fields}}', [
            'key' => 'seo_robot_follow',
            'name' => 'Meta Robots Follow',
            'type' => 7,
            'data_set' => '{"follow":"follow","nofollow":"nofollow"}',
            'model' => 'teacher',
            'status' => 1
        ]);
        $this->insert('{{%meta_fields}}', [
            'key' => 'seo_canonical',
            'name' => 'Canonical URL',
            'type' => 1,
            'model' => 'teacher',
            'status' => 1
        ]);
        $this->insert('{{%meta_fields}}', [
            'key' => 'seo_sitemap',
            'name' => 'Include in Sitemap?',
            'type' => 7,
            'data_set' => '{"0":"always","1":"never"}',
            'model' => 'teacher',
            'status' => 1
        ]);

        // insert value for sitemaps
        $this->insert('sitemaps', [
            'name' => 'teacher',
            'priority' => 0.5,
            'changefreg' => 4,
            'created_time' => time(),
            'updated_time' => time(),
        ]);
    }

    public function down()
    {
        $this->delete('{{%sitemaps}}', [
            'name' => 'teacher'
        ]);

        $this->delete('{{%meta_fields}}', [
            'key' => ['seo_title', 'seo_description', 'seo_keyword', 'seo_robot_index', 'seo_robot_follow', 'seo_canonical'],
            'model' => 'teacher'
        ]);

        return true;
    }
}
