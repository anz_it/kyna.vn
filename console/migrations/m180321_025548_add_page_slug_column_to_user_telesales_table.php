<?php

use yii\db\Migration;

/**
 * Handles adding page_slug to table `user_telesales`.
 */
class m180321_025548_add_page_slug_column_to_user_telesales_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('user_telesales', 'page_slug', $this->string(255));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('user_telesales', 'page_slug');
    }
}
