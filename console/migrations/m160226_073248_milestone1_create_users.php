<?php

use console\components\KynaMigration;

class m160226_073248_milestone1_create_users extends KynaMigration
{

    public function up()
    {
        $this->createTableUser();

        $this->createTableUserCustomFieldValue();

        return true;
    }

    public function down()
    {
        $this->dropTableUser();

        $this->dropTableCourseCustomFieldValue();

        echo "m160226_073248_milestone1_create_users has been reverted.\n";

        return true;
    }

    /**
     * @desc create table `users`
     * @return boolean
     */
    private function createTableUser()
    {
        try {
            $this->createTable('{{%users}}', [
                'id' => $this->primaryKey(),
                'username' => $this->string()->notNull()->unique(),
                'email' => $this->string(100)->notNull()->unique(),
                'full_name' => $this->string(100)->notNull(),
                'role' => $this->string(20)->defaultValue('user'),
                'phone_number' => $this->string(20),
                'delta_xu' => $this->integer(10)->defaultValue(0),
                'real_delta_xu' => $this->integer(10)->defaultValue(0),
                'is_confirmed' => "bit(1) DEFAULT b'0'",
                'auth_key' => $this->string(32)->notNull(),
                'password_hash' => $this->string(255)->notNull(),
                'password_reset_token' => $this->string(255)->unique(),
                'status' => $this->smallInteger(3)->defaultValue(1),
                'created_time' => $this->integer(10)->defaultValue(0),
                'updated_time' => $this->integer(10)->defaultValue(0),
                    ], self::$_tableOptions
            );

            $this->createIndex('index_full_name', '{{%users}}', ['full_name']);
            $this->createIndex('index_phone_number', '{{%users}}', ['phone_number']);
        } catch (yii\console\Exception $ex) {
            echo $ex->getMessage();
            return false;
        }

        return true;
    }

    /**
     * @desc drop table `users` and related indices
     * @return boolean
     */
    private function dropTableUser()
    {
        try {
            $this->dropIndex('index_full_name', '{{%users}}');
            $this->dropIndex('index_phone_number', '{{%users}}');

            $this->dropTable('{{%users}}');
        } catch (yii\console\Exception $ex) {
            echo $ex->getMessage();
            return false;
        }

        return true;
    }

    /**
     * @desc create table `user_custom_field_values`,
     * this is meta fields table for `users`
     */
    private function createTableUserCustomFieldValue()
    {
        try {
            $this->createTable('{{%user_custom_field_values}}', [
                'id' => $this->primaryKey(),
                'user_id' => $this->integer(11)->notNull(),
                'custom_field_id' => $this->integer(11)->notNull(),
                'value' => $this->text(),
                    ], self::$_tableOptions
            );

            $this->createIndex('index_user_id', '{{%user_custom_field_values}}', ['user_id']);
            $this->createIndex('index_custom_field_id', '{{%user_custom_field_values}}', ['custom_field_id']);
        } catch (yii\console\Exception $ex) {
            echo $ex->getMessage();
            return false;
        }

        return true;
    }

    /**
     * @desc drop table `user_custom_field_values` and related indices
     * @return boolean
     */
    private function dropTableCourseCustomFieldValue()
    {
        try {
            $this->dropIndex('index_user_id', '{{%user_custom_field_values}}');
            $this->dropIndex('index_custom_field_id', '{{%user_custom_field_values}}');

            $this->dropTable('{{%user_custom_field_values}}');
        } catch (yii\console\Exception $ex) {
            echo $ex->getMessage();
            return false;
        }

        return true;
    }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
