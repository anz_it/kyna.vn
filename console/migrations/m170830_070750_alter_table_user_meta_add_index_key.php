<?php

use yii\db\Migration;

class m170830_070750_alter_table_user_meta_add_index_key extends Migration
{
    public function up()
    {
        $this->createIndex('index_key', '{{%user_meta}}', 'key');
    }

    public function down()
    {
        $this->dropIndex('index_key', '{{%user_meta}}');

        return true;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
