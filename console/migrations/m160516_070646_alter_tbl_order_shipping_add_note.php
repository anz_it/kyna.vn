<?php

use yii\db\Migration;

class m160516_070646_alter_tbl_order_shipping_add_note extends Migration
{

    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->addColumn('{{%order_shipping}}', 'note', 'text NULL AFTER `fee`');
    }

    public function safeDown()
    {
        $this->dropColumn('{{%order_shipping}}', 'note');
        return true;
    }

}
