<?php

use yii\db\Migration;

class m161219_064159_create_table_donggia extends Migration
{
    public function up()
    {
        $this->createTable('{{%donggia}}', [
            'id' => $this->primaryKey(),
            'code' => $this->string()->notNull(),
            'serial' => $this->string()->notNull(),
            'price' => $this->integer(11)->notNull(),
            'course_list' => $this->string()->notNull(),
            'course_id' => $this->integer(11),
            'user_id' => $this->integer(11),
            'order_id' => $this->integer(11),
            'campaign' => $this->string()->notNull(),
            'is_activated' => $this->smallInteger(1)->defaultValue(0),
            'created_time' => $this->integer(10)->defaultValue(0),
            'updated_time' => $this->integer(10)->defaultValue(0),
        ]);

        $this->createIndex('idx_donggia_code', '{{%donggia}}', 'code');
    }

    public function down()
    {
        $this->dropIndex('idx_donggia_code', '{{%donggia}}');

        $this->dropTable('{{%donggia}}');

        return true;
    }
}
