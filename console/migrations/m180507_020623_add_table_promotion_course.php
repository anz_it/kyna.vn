<?php

use yii\db\Migration;

class m180507_020623_add_table_promotion_course extends Migration
{
    const PPROMOTION_COURSE_TABLE = "{{%promotion_course}}";
    public function up()
    {
        $this->createTable(self::PPROMOTION_COURSE_TABLE, [
            'id' => $this->primaryKey(),
            'promotion_id' => $this->integer(),
            'course_id' => $this->integer(),

        ]);
    }

    public function down()
    {
        echo "m180507_020623_add_table_promotion_course cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
