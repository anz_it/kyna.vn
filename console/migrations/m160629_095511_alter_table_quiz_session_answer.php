<?php

use yii\db\Migration;

class m160629_095511_alter_table_quiz_session_answer extends Migration
{
    public function up()
    {
        $this->addColumn("{{%quiz_session_answers}}", "quiz_question_id", $this->integer(11));
        $this->addColumn("{{%quiz_session_answers}}", "position", $this->smallInteger(4));
    }

    public function down()
    {
        return true;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
