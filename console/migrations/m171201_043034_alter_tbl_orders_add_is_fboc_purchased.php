<?php

use yii\db\Migration;

class m171201_043034_alter_tbl_orders_add_is_fboc_purchased extends Migration
{
    public function up()
    {
        $this->addColumn(
            'orders',
            'is_purchased_fboc',
            $this->boolean()->defaultValue(false)->comment('is purchased to Facebook Offline Conversion')
        );
    }

    public function down()
    {
        $this->dropColumn(
            'orders',
            'is_purchased_fboc'
        );
    }

}
