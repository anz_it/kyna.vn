<?php

use yii\db\Migration;
use kyna\base\models\MetaField;

class m170919_022714_init_combo_meta_fields_for_seo extends Migration
{
    public function up()
    {
        $this->insert('meta_fields', [
            'key' => 'seo_title',
            'name' => 'SEO Title',
            'type' => 1, // text
            'model' => MetaField::MODEL_COURSE_COMBO,
            'status' => 1,
            'created_time' => time(),
        ]);

        $this->insert('meta_fields', [
            'key' => 'seo_description',
            'name' => 'SEO Description',
            'type' => 2, // text area
            'model' => MetaField::MODEL_COURSE_COMBO,
            'status' => 1,
            'created_time' => time(),
        ]);

        $this->insert('meta_fields', [
            'key' => 'seo_keyword',
            'name' => 'SEO keyword',
            'type' => 2, // text area
            'model' => MetaField::MODEL_COURSE_COMBO,
            'status' => 1,
            'created_time' => time(),
        ]);
    }

    public function down()
    {
        $this->delete('meta_fields', ['key' => 'seo_title']);
        $this->delete('meta_fields', ['key' => 'seo_description']);
        $this->delete('meta_fields', ['key' => 'seo_keyword']);
    }

}
