<?php

class m160316_040209_add_tbl_user_courses extends console\components\KynaMigration
{

    public function up()
    {
        $this->createTable('{{%user_courses}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(11)->notNull(),
            'course_id' => $this->integer(11)->notNull(),
            'activation_code' => $this->string(16),
            'activation_date' => $this->integer(10)->defaultValue(0),
            'is_activated' => "bit(1) DEFAULT b'0'",
            'is_started' => "bit(1) DEFAULT b'0'",
            'expiration_date' => $this->integer(10)->defaultValue(0),
            'is_graduated' => "bit(1) DEFAULT b'0'",
            'method' => $this->smallInteger(4)->defaultValue(0),
            'status' => $this->smallInteger(3)->defaultValue(1),
            'created_time' => $this->integer(10)->defaultValue(0),
            'updated_time' => $this->integer(10)->defaultValue(0),
                ], self::$_tableOptions
        );
        
        $this->createIndex('index_user_id', '{{%user_courses}}', 'user_id');
        $this->createIndex('index_course_id', '{{%user_courses}}', 'course_id');
        $this->createIndex('index_user_course', '{{%user_courses}}', ['user_id', 'course_id']);
        $this->createIndex('index_user_activation_code', '{{%user_courses}}', ['user_id', 'activation_code']);
    }

    public function down()
    {
        $this->dropIndex('index_user_id', '{{%user_courses}}');
        $this->dropIndex('index_course_id', '{{%user_courses}}');
        $this->dropIndex('index_user_course', '{{%user_courses}}');
        $this->dropIndex('index_user_activation_code', '{{%user_courses}}');
        
        $this->dropTable("{{%user_courses}}");
        
        echo "m160316_040209_add_tbl_user_courses has been reverted.\n";

        return true;
    }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
