<?php

use yii\db\Migration;
use kyna\partner\models\Partner;

class m180301_071535_migrate_course_type extends Migration
{
    const TYPE_VIDEO = 1;
    const TYPE_COMBO = 2;
    const TYPE_SOFTWARE = 3;
    const TYPE_SPECIAL_JUNIOR_MONKEY = 3;
    const TYPE_EMBED = 4;
    const TYPE_SPECIAL_TAAMKRU = 5;
    const TYPE_PARTNER = 6;

    public function up()
    {
        $constant = function ($name)
        {
            return constant('self::' . $name);
        };

        // update Monkey Junior Partner
        $mkPartner = Partner::findOne([
            'partner_type' => Partner::PARTNER_TYPE_NORMAL,
            'class' => 'monkey_junior'
        ]);
        if (empty($mkPartner)) {
            $mkPartner = new Partner();
            $mkPartner->partner_type = Partner::PARTNER_TYPE_NORMAL;
            $mkPartner->class = 'monkey_junior';
            $mkPartner->name = 'Monkey Junior';
            $mkPartner->status = Partner::STATUS_ACTIVE;
            $mkPartner->save(false);
        }
        $this->execute("
            UPDATE courses c SET c.type = {$constant('TYPE_SOFTWARE')}, c.partner_id = {$mkPartner->id} WHERE c.type = {$constant('TYPE_SPECIAL_JUNIOR_MONKEY')};          
        ");

        $this->execute("
            UPDATE courses c SET c.type = {$constant('TYPE_VIDEO')} WHERE c.type = {$constant('TYPE_EMBED')};
            UPDATE courses c SET c.type = {$constant('TYPE_SOFTWARE')} WHERE c.type = {$constant('TYPE_SPECIAL_TAAMKRU')};
            UPDATE courses c SET c.type = {$constant('TYPE_SOFTWARE')} WHERE c.type = {$constant('TYPE_PARTNER')};
        ");
    }

    public function down()
    {
        echo "m171125_040007_migrate_course_type cannot be reverted.\n";

        return false;
    }
}