<?php

use yii\db\Migration;

class m160718_041601_alter_tbl_course_leaner_qna_add_col_status extends Migration
{

    public function up()
    {
        $this->addColumn('{{%course_learner_qna}}', 'is_approved', "BIT(1)");
        $this->addColumn('{{%course_learner_qna}}', 'is_send_approved', "BIT(1) DEFAULT 0");
        $this->addColumn('{{%course_learner_qna}}', 'is_send_unapproved', "BIT(1) DEFAULT 0");
        $this->addColumn('{{%course_learner_qna}}', 'is_send_to_teacher', "BIT(1) DEFAULT 0");
        $this->addColumn('{{%course_learner_qna}}', 'is_send_moved', "BIT(1) DEFAULT 0");
        $this->addColumn('{{%course_learner_qna}}', 'is_send_deleted', "BIT(1) DEFAULT 0");
        $this->addColumn('{{%course_learner_qna}}', 'is_send_answered', "BIT(1) DEFAULT 0");
        
        $this->addColumn('{{%course_learner_qna}}', 'status', $this->smallInteger(3)->defaultValue(0));
        $this->addColumn('{{%course_learner_qna}}', 'updated_time', $this->integer(10));
    }

    public function down()
    {
        $this->dropColumn('{{%course_learner_qna}}', 'updated_time');
        $this->dropColumn('{{%course_learner_qna}}', 'status');
        
        $this->dropColumn('{{%course_learner_qna}}', 'is_send_answered');
        $this->dropColumn('{{%course_learner_qna}}', 'is_send_deleted');
        $this->dropColumn('{{%course_learner_qna}}', 'is_send_moved');
        $this->dropColumn('{{%course_learner_qna}}', 'is_send_to_teacher');
        $this->dropColumn('{{%course_learner_qna}}', 'is_send_unapproved');
        $this->dropColumn('{{%course_learner_qna}}', 'is_send_approved');
        $this->dropColumn('{{%course_learner_qna}}', 'is_approved');

        return true;
    }

}
