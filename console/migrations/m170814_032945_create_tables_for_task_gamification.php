<?php

use yii\db\Migration;

class m170814_032945_create_tables_for_task_gamification extends Migration
{
    public function up()
    {
        $this->createTable('{{%user_points}}', [
            'user_id' => $this->primaryKey(11),
            'k_point' => $this->integer(10)->defaultValue(0),
        ]);

        $this->createTable('{{%user_point_histories}}', [
            'id' => $this->primaryKey(11),
            'user_id' => $this->integer(11)->notNull(),
            'k_point' => $this->integer(10)->defaultValue(0),
            'type' => $this->smallInteger(3),
            'mission_id' => $this->integer(11),
            'reference_id' => $this->integer(11),
            'description' => $this->string(100)->notNull(),
            'created_time' => $this->integer(10)->notNull(),
            'created_user_id' => $this->integer(11),
        ]);

        $this->createIndex('index_user_id', '{{%user_point_histories}}', 'user_id');
        $this->createIndex('index_user_mission_id', '{{%user_point_histories}}', ['user_id', 'mission_id']);

        $this->createTable('{{%missions}}', [
            'id' => $this->primaryKey(11),
            'name' => $this->string(64)->notNull(),
            'k_point' => $this->integer(10)->defaultValue(0),
            'k_point_for_free_course' => $this->integer(10),
            'expiration_time' => $this->integer(10),
            'condition_operator' => "ENUM('AND', 'OR') DEFAULT 'AND'",
            'apply_course_type' => "ENUM('ALL', 'SOME', 'CATEGORY') DEFAULT 'ALL'",
            'is_deleted' => 'BIT(1) DEFAULT 0',
            'status' => $this->smallInteger(3)->defaultValue(0),
            'created_time' => $this->integer(10)->defaultValue(0),
            'created_user_id' => $this->integer(11),
            'updated_time' => $this->integer(10)->defaultValue(0),
            'updated_user_id' => $this->integer(11)
        ]);

        $this->createTable('{{%mission_conditions}}', [
            'id' => $this->primaryKey(11),
            'mission_id' => $this->integer(11)->notNull(),
            'type' => $this->string(64)->notNull(),
            'title' => $this->string(64),
            'min_value' => $this->integer(10)->defaultValue(0),
            'is_deleted' => 'BIT(1) DEFAULT 0',
            'created_time' => $this->integer(10)->defaultValue(0),
            'created_user_id' => $this->integer(11),
            'updated_time' => $this->integer(10)->defaultValue(0),
            'updated_user_id' => $this->integer(11)
        ]);

        $this->createIndex('index_mission_id', '{{%mission_conditions}}', 'mission_id');

        $this->createTable('{{%mission_courses}}', [
            'id' => $this->primaryKey(11),
            'mission_id' => $this->integer(11)->notNull(),
            'course_id' => $this->integer(11)->notNull(),
            'is_deleted' => 'BIT(1) DEFAULT 0',
            'created_time' => $this->integer(10)->defaultValue(0),
            'created_user_id' => $this->integer(11),
            'updated_time' => $this->integer(10)->defaultValue(0),
            'updated_user_id' => $this->integer(11)
        ]);

        $this->createIndex('index_unique_mission_and_course', '{{%mission_courses}}', ['mission_id', 'course_id'], true);

        $this->createTable('{{%mission_categories}}', [
            'id' => $this->primaryKey(11),
            'mission_id' => $this->integer(11)->notNull(),
            'category_id' => $this->integer(11)->notNull(),
            'is_deleted' => 'BIT(1) DEFAULT 0',
            'created_time' => $this->integer(10)->defaultValue(0),
            'created_user_id' => $this->integer(11),
            'updated_time' => $this->integer(10)->defaultValue(0),
            'updated_user_id' => $this->integer(11)
        ]);

        $this->createIndex('index_unique_mission_and_category', '{{%mission_categories}}', ['mission_id', 'category_id'], true);
    }

    public function down()
    {
        $this->dropTable('{{%user_point_histories}}');

        $this->dropTable('{{%user_points}}');

        $this->dropTable('{{%mission_categories}}');

        $this->dropTable('{{%mission_courses}}');

        $this->dropTable('{{%mission_conditions}}');

        $this->dropTable('{{%missions}}');
    }
}
