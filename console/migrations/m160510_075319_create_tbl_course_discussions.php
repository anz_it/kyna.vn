<?php

/**
 * Handles the creation for table `tbl_course_discussions`.
 */
class m160510_075319_create_tbl_course_discussions extends console\components\KynaMigration
{

    /**
     * @inheritdoc
     */
    public function up()
    {
        // create table course_discussions
        $this->createTable('{{%course_discussions}}', [
            'id' => $this->primaryKey(),
            'course_id' => $this->integer(11)->notNull(),
            'user_id' => $this->integer(11)->notNull(),
            'parent_id' => $this->integer(11)->defaultValue(0),
            'comment' => $this->text()->notNull(),
            'status' => $this->smallInteger(3)->defaultValue(1),
            'created_time' => $this->integer(10)->defaultValue(0),
            'updated_time' => $this->integer(10)->defaultValue(0),
        ]);
        
        // add index for col course_id for relation query
        $this->createIndex("index_course_id", '{{%course_discussions}}', 'course_id');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%course_discussions}}');
        
        return true;
    }

}
