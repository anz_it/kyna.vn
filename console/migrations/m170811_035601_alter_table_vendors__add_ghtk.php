<?php

use yii\db\Migration;

class m170811_035601_alter_table_vendors__add_ghtk extends Migration
{
    public function up()
    {
        $this->insert(
            'vendors', [
                'name' => 'Giao hang tiet kiem',
                'vendor_type' => 2,
                'status' => 1,
                'alias' => 'ghtk',
                'created_time' => time(),
                'updated_time' => time(),
        ]);
    }

    public function down()
    {
        $this->delete('vendors', [
           'alias' => 'ghtk',
        ]);
//        echo "m170811_035601_alter_table_vendors__add_ghtk cannot be reverted.\n";

//        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
