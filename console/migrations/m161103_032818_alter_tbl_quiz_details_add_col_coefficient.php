<?php

use yii\db\Migration;

class m161103_032818_alter_tbl_quiz_details_add_col_coefficient extends Migration
{

    public function up()
    {
        $this->renameColumn('{{%quiz_details}}', 'score', 'coefficient');
        
        $this->renameColumn('{{%quiz_session_answers}}', 'answer_text', 'answer');
        
        $this->renameTable('{{%quiz_session_answers_trash}}', '{{%quiz_session_answer_trash}}');
        
        $this->renameColumn('{{%quiz_session_answer_trash}}', 'answer_text', 'answer');
        
        $this->addColumn('{{%quiz_sessions}}', 'total_quiz_score', "FLOAT DEFAULT 0 AFTER total_score");
        $this->alterColumn('{{%quiz_sessions}}', 'total_score', "FLOAT DEFAULT 0");
        
        $this->addColumn('{{%quiz_session_trash}}', 'total_quiz_score', "FLOAT DEFAULT 0 AFTER total_score");
        $this->alterColumn('{{%quiz_session_trash}}', 'total_score', "FLOAT DEFAULT 0");
    }

    public function down()
    {
        $this->alterColumn('{{%quiz_session_trash}}', 'total_score', "INT(10) DEFAULT 0");
        $this->dropColumn('{{%quiz_session_trash}}', 'total_quiz_score');
        
        $this->alterColumn('{{%quiz_sessions}}', 'total_score', "INT(10) DEFAULT 0");
        $this->dropColumn('{{%quiz_sessions}}', 'total_quiz_score');
        
        $this->renameColumn('{{%quiz_session_answer_trash}}', 'answer', 'answer_text');
        
        $this->renameTable('{{%quiz_session_answer_trash}}', '{{%quiz_session_answers_trash}}');
        
        $this->renameColumn('{{%quiz_session_answers}}', 'answer', 'answer_text');
        
        $this->renameColumn('{{%quiz_details}}', 'coefficient', 'score');

        return true;
    }

}
