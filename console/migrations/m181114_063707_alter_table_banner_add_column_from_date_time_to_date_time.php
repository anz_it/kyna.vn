<?php

use yii\db\Migration;

class m181114_063707_alter_table_banner_add_column_from_date_time_to_date_time extends Migration
{
    public function up()
    {
        $this->addColumn('banners','from_date_time', $this->integer());
        $this->addColumn('banners','to_date_time', $this->integer());
    }

    public function down()
    {
        echo "m181114_063707_alter_table_banner_add_column_from_date_time_to_date_time cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
