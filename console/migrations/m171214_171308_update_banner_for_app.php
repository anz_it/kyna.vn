<?php

use yii\db\Migration;

class m171214_171308_update_banner_for_app extends Migration
{
    public function safeUp()
    {
        $this->dropColumn('banners','tag');
        $this->dropColumn('banners','keyword');
        $this->addColumn('banners','app_action',$this->integer(1));
        $this->addColumn('banners','app_action_data',$this->string());
    }

    public function safeDown()
    {
       $this->addColumn('banners','tag',$this->string());
       $this->addColumn('banners','keyword',$this->string());
       $this->dropColumn('banners','app_action');
       $this->dropColumn('banners','app_action_data');
    }


}
