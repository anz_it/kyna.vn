<?php

use yii\db\Migration;

class m180301_073356_alter_table_course_add_new_columns extends Migration
{
    public function up()
    {
        $this->addColumn('courses', 'purchase_type', $this->integer(11));
        $this->addColumn('courses', 'mac_app_link',$this->string(255));
        $this->addColumn('courses', 'window_app_link',$this->string(255));
        $this->addColumn('courses', 'ios_app_link',$this->string(255));
        $this->addColumn('courses', 'android_app_link',$this->string(255));
        $this->addColumn('courses', 'what_you_learn',$this->text());
        $this->addColumn('courses', 'max_device', $this->integer(11));
    }

    public function down()
    {
        $this->dropColumn('courses', 'purchase_type');
        $this->dropColumn('courses', 'mac_app_link');
        $this->dropColumn('courses', 'window_app_link');
        $this->dropColumn('courses', 'ios_app_link');
        $this->dropColumn('courses', 'android_app_link');
        $this->dropColumn('courses', 'what_you_learn');
        $this->dropColumn('courses', 'max_device');
    }
}
