<?php

use yii\db\Migration;

class m160818_100814_alter_tbl_user_courses_add_is_deleted extends Migration
{

    public function safeUp()
    {
        $this->addColumn('{{%user_courses}}', 'is_deleted', "BIT(1) default 0 after `method`");
    }

    public function safeDown()
    {
        $this->dropColumn('{{%user_courses}}', 'is_deleted');
        
        return true;
    }

}
