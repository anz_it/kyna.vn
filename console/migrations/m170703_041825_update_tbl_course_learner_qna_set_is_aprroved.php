<?php

use yii\db\Migration;
use kyna\course\models\CourseLearnerQna;

class m170703_041825_update_tbl_course_learner_qna_set_is_aprroved extends Migration
{

    public function up()
    {
        $qnaTblName = CourseLearnerQna::tableName();

        $this->update($qnaTblName, ['is_approved' => CourseLearnerQna::BOOL_YES], [
            'AND',
            ['is_approved' => CourseLearnerQna::BOOL_NO],
            ['is not', 'question_id', null],
            ['<', 'posted_time', 1496077200], // 2017-05-30, time when new qna features went Production
        ]);
    }

    public function down()
    {
        return true;
    }

}
