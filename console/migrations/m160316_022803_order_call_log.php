<?php

use yii\db\Migration;

class m160316_022803_order_call_log extends Migration
{
    public function up()
    {
        $this->createTable('{{%order_call_log}}', [
            'id' => $this->primaryKey(),
            'order_id' => $this->integer(),
            'user_id' => $this->integer(),
            'start_time' => $this->integer(),
            'end_time' => $this->integer(),
            'status' => $this->smallInteger(),
        ]);
    }

    public function down()
    {
        $this->dropTable('{{%order_call_log}}');
        
        echo "m160316_022803_order_call_log cannot be reverted.\n";

        return true;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
