<?php

use yii\db\Migration;

use yii\db\Query;

class m170901_040055_insert_into_table_setting_meta__hotline_international_format extends Migration
{
    public function up()
    {
        $query = new Query();
        $query->createCommand()->insert(
            'meta_fields',
            [
                'key' => 'hotline_international_format',
                'name' => 'Hotline International Format',
                'type' => 1, // text
                'model' => 'setting',
//                'status' => true,
                'created_time' => time(),
                'updated_time' => time(),
            ]
        )->execute();


        $metaField = $query->select('*')
            ->from('meta_fields')
            ->where([
                'key' => 'hotline_international_format',
            ])->one();

        $query->createCommand()->insert(
              'setting_meta',
              [
                  'key' => 'hotline_international_format',
                  'value' => '+841900636409',
                  'meta_field_id' => $metaField['id'],
                  'created_time' => time(),
                  'updated_time' => time(),
              ]
        )->execute();
    }

    public function down()
    {
        $query = new Query();

        $query->createCommand()->delete(
            'setting_meta',
            [
                'key' => 'hotline_international_format',
            ]
        )->execute();

        $query->createCommand()->delete(
            'meta_fields',
            [
                'key' => 'hotline_international_format',
            ]
        )->execute();

        //        echo "m170901_040055_insert_into_table_setting_meta__hotline_international_format cannot be reverted.\n";

//        return false;
    }


    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
