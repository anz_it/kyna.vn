<?php

use console\components\KynaMigration;

class m160227_012940_milestone1_create_orders extends KynaMigration
{

    public function up()
    {
        // create table `orders`
        $this->createTableOrder();
        
        // create table `order_details`
        $this->createTableOrderDetail();
        
        // create table `order_actions`
        $this->createTableOrderAction();
        
        // create table `order_action_metas`
        $this->createTableOrderActionMeta();
    }

    public function down()
    {
        $this->dropTableOrderActionMeta();
        
        $this->dropTableOrderAction();
        
        $this->dropTableOrderDetail();
        
        $this->dropTableOrder();
        
        echo "m160227_012940_milestone1_create_orders has been reverted.\n";

        return true;
    }
    
    /**
     * @desc create table `orders` and related
     * @return boolean
     */
    private function createTableOrder()
    {
        try {
            $this->createTable('{{%orders}}', [
                'id' => $this->primaryKey(),
                'order_number' => $this->string(20)->notNull(),
                'order_date' => $this->integer(10)->defaultValue(0),
                'user_id' => $this->integer(11)->notNull(),
                'coupon_code' => $this->string(10),
                'discount_amount' => $this->double()->defaultValue(0),
                'subtotal' => $this->double()->defaultValue(0),
                'shipping_address' => $this->text(),
                'shipping_fee' => $this->double()->defaultValue(0),
                'shipping_method_id' => $this->integer(11),
                'shipping_code' => $this->string(20),
                'is_paid' => "bit(1) DEFAULT b'0'",
                'payment_method_id' => $this->integer(11),
                'point_of_sale' => $this->string(45),
                'reference_id' => $this->string(45),
                'status' => $this->smallInteger(3)->defaultValue(1),
                'created_time' => $this->integer(10)->defaultValue(0),
                'updated_time' => $this->integer(10)->defaultValue(0),
                    ], self::$_tableOptions
            );

            $this->createIndex('index_user_id', '{{%orders}}', ['user_id']);
            $this->createIndex('index_shipping_method_id', '{{%orders}}', ['shipping_method_id']);
            $this->createIndex('index_payment_method_id', '{{%orders}}', ['payment_method_id']);
        } catch (yii\console\Exception $ex) {
            echo $ex->getMessage();
            return false;
        }
        
        return true;
    }
    
    private function dropTableOrder()
    {
        try {
            $this->dropIndex('index_payment_method_id', '{{%orders}}');
            $this->dropIndex('index_shipping_method_id', '{{%orders}}');
            $this->dropIndex('index_user_id', '{{%orders}}');

            $this->dropTable('{{%orders}}');
        } catch (yii\console\Exception $ex) {
            echo $ex->getMessage();
            return false;
        }
        
        return true;
    }
    
    /**
     * @desc create table `order_details` and related
     * @return boolean
     */
    private function createTableOrderDetail()
    {
        try {
            $this->createTable('{{%order_details}}', [
                'id' => $this->primaryKey(),
                'order_id' => $this->integer(11)->notNull(),
                'course_id' => $this->integer(11)->notNull(),
                'course_meta' => $this->text(),
                'unit_price' => $this->double()->defaultValue(0),
                'discount_amount' => $this->double()->defaultValue(0),
                'quantity' => $this->integer(5)->defaultValue(1),
                'item_total' => $this->double()->defaultValue(0),
                'is_delivered' => "bit(1) DEFAULT b'0' COMMENT '=1 Nếu khóa học đã được kích hoạt\n=0 Nếu khóa học chưa được kích hoạt'",
                    ], self::$_tableOptions
            );

            $this->createIndex('index_order_id', '{{%order_details}}', ['order_id']);
            $this->createIndex('index_course_id', '{{%order_details}}', ['course_id']);
        } catch (yii\console\Exception $ex) {
            echo $ex->getMessage();
            return false;
        }
        
        return true;
    }
    
    private function dropTableOrderDetail()
    {
        try {
            $this->dropIndex('index_course_id', '{{%order_details}}');
            $this->dropIndex('index_order_id', '{{%order_details}}');

            $this->dropTable('{{%order_details}}');
        } catch (yii\console\Exception $ex) {
            echo $ex->getMessage();
            return false;
        }
        
        return true;
    }
    
    /**
     * @desc create table `order_actions` and related
     * @return boolean
     */
    private function createTableOrderAction()
    {
        try {
            $this->createTable('{{%order_actions}}', [
                'id' => $this->primaryKey(),
                'order_id' => $this->integer(11)->notNull(),
                'user_id' => $this->integer(11)->notNull(),
                'name' => $this->string(50) . " COMMENT 'action type: create: create a new order with status = new - update: modify order information- update_status: modify order status- pay: choose payment method, change status to waiting for payment, store payment method as action meta- paid: change status to waiting for process. is_paid = 1. store payment method & transaction code from payment gateway- contact: fire when telesale call to customer, store calling time and calling status- deliver: change status to delivering send order to GHN, store order code from GHN- cancel: change status to cancelled, store reason (return, deny, intouchable...), note- complete: change order status to complete, all courses are activated.'",
                'user_type' => $this->string(5) . " COMMENT 'user_type =user OR staff'",
                'action_time' => $this->integer(10)->defaultValue(0),
                    ], self::$_tableOptions
            );

            $this->createIndex('index_order_id', '{{%order_actions}}', ['order_id']);
            $this->createIndex('index_user_id', '{{%order_actions}}', ['user_id']);
        } catch (yii\console\Exception $ex) {
            echo $ex->getMessage();
            return false;
        }
        
        return true;
    }
    
    private function dropTableOrderAction()
    {
        try {
            $this->dropIndex('index_user_id', '{{%order_actions}}');
            $this->dropIndex('index_order_id', '{{%order_actions}}');

            $this->dropTable('{{%order_actions}}');
        } catch (yii\console\Exception $ex) {
            echo $ex->getMessage();
            return false;
        }
        
        return true;
    }
    
    /**
     * @desc create table `order_action_meta` and related
     * @return boolean
     */
    private function createTableOrderActionMeta()
    {
        try {
            $this->createTable('{{%order_action_meta}}', [
                'id' => $this->primaryKey(),
                'action_id' => $this->integer(11)->notNull(),
                'key' => $this->string(50),
                'value' => $this->text(),
                'data_type' => $this->string(10),
                    ], self::$_tableOptions
            );

            $this->createIndex('index_action_id', '{{%order_action_meta}}', ['action_id']);
        } catch (yii\console\Exception $ex) {
            echo $ex->getMessage();
            return false;
        }
        
        return true;
    }
    
    private function dropTableOrderActionMeta()
    {
        try {
            $this->dropIndex('index_action_id', '{{%order_action_meta}}');

            $this->dropTable('{{%order_action_meta}}');
        } catch (yii\console\Exception $ex) {
            echo $ex->getMessage();
            return false;
        }
        
        return true;
    }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
