<?php

use yii\db\Migration;
use kyna\tag\models\Tag;

class m180123_021638_create_tag_mua_nhieu_tang_nhieu extends Migration
{
    public function up()
    {
        $tag = Tag::findOne(['slug' => 'muanhieutangnhieu']);
        if (empty($tag)) {
            $this->insert('tags', [
                'tag' => 'Mua nhiều Tặng nhiều',
                'slug' => 'muanhieutangnhieu',
                'status' => 1
            ]);
            $tag = Tag::findOne(['slug' => 'muanhieutangnhieu']);
        }
        if ($tag) {
            // insert courses from group discount for tag
            $this->execute("
                INSERT INTO course_tags (course_id, tag_id)
                SELECT c.course_id, {$tag->id} 
                FROM group_discount_courses c
            ");
        }
    }

    public function down()
    {
        echo "m180123_021638_create_tag_mua_nhieu_tang_nhieu cannot be reverted.\n";

        return false;
    }
}
