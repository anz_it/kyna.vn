<?php

use yii\db\Migration;

class m181108_104915_add_permission_user_follow_teacher extends Migration
{
    public function up()
    {
        $auth = Yii::$app->authManager;

        $allRelation = $auth->createPermission('Relation.All');
        $auth->add($allRelation);

        //Relation Manager
        $allRelationManager = $auth->createPermission('RelationManager.All');
        $auth->add($allRelationManager);

    }

    public function down()
    {
        $auth = Yii::$app->authManager;

        $RelationAll = $auth->getPermission('Relation.All');
        $auth->remove($RelationAll);

        $allRelationManager = $auth->getPermission('RelationManager.All');
        $auth->remove($allRelationManager);
    }
}
