<?php

use yii\db\Migration;

class m180911_070119_alter_table_affiliate_categories_add_category_can_not_override extends Migration
{
    const AFFILIATE_CATEGORIES_TABLE = '{{%affiliate_categories}}';

    public function up()
    {
        $this->addColumn(self::AFFILIATE_CATEGORIES_TABLE, 'category_can_not_override', $this->string());
    }

    public function down()
    {
        $this->dropColumn(self::AFFILIATE_CATEGORIES_TABLE, 'category_can_not_override');
    }
}
