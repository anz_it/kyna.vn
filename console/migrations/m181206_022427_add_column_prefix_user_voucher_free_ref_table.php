<?php

use yii\db\Migration;

class m181206_022427_add_column_prefix_user_voucher_free_ref_table extends Migration
{
    const USER_VOUCHER_FREE_TABLE_REF = "user_voucher_free_ref";
    public function up()
    {
        $this->dropIndex('user_id',self::USER_VOUCHER_FREE_TABLE_REF);
        $this->dropIndex('idx_user_id',self::USER_VOUCHER_FREE_TABLE_REF);
        $this->addColumn(self::USER_VOUCHER_FREE_TABLE_REF,'prefix', $this->string(255)->after('user_id')->defaultValue('FREEKYNAREF2711'));
        $this->createIndex('idx_user_id_prefix', self::USER_VOUCHER_FREE_TABLE_REF, ['user_id','prefix'],true);
    }

    public function down()
    {
        $this->dropIndex('idx_user_id_prefix',self::USER_VOUCHER_FREE_TABLE_REF);
        $this->dropColumn(self::USER_VOUCHER_FREE_TABLE_REF,'prefix');
    }
}
