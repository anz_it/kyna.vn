<?php

use yii\db\Migration;

class m161017_042228_update_promotion_data extends Migration
{
    public function up()
    {
        $this->execute(
            "update promotions set current_number_usage = 1 where is_used = 1 and current_number_usage = 0;
             update promotions set number_usage = 1 where number_usage = 0 and (is_used is null or is_used = 0)");

    }

    public function down()
    {
        echo "m161017_042228_update_promotion_data cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
