<?php

use yii\db\Migration;

/**
 * Handles the creation for table `user_telesales`.
 */
class m160704_095937_create_tbl_user_telesales extends Migration
{

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('{{%user_telesales}}', [
            'id' => $this->primaryKey(),
            'email' => $this->string(100),
            'phone_number' => $this->string(20),
            'full_name' => $this->string(100),
            'street_address' => $this->string(255),
            'location_id' => $this->integer(11),
            'type' => $this->string(45),
            'user_id' => $this->integer(11),
            'affiliate_id' => $this->integer(11),
            'old_affiliate_id' => $this->integer(11),
            'tel_id' => $this->integer(11),
            'form_name' => $this->string(250),
            'note' => $this->text(),
            'course_id' => $this->integer(11),
            'bonus' => $this->smallInteger(4),
            'amount' => $this->float(10)->defaultValue(0),
            'is_success' => "BIT(1) DEFAULT 0",
            'is_deleted' => "BIT(1) DEFAULT 0",
            'last_call_date' => $this->integer(11),
            'status' => $this->smallInteger(3)->defaultValue(1),
            'created_time' => $this->integer(10),
            'updated_time' => $this->integer(10),
        ]);
        
        $this->createIndex('index_affiliate_id', '{{%user_telesales}}', 'affiliate_id');
        $this->createIndex('index_old_affiliate_id', '{{%user_telesales}}', 'old_affiliate_id');
        $this->createIndex('index_tel_id', '{{%user_telesales}}', 'tel_id');
        $this->createIndex('index_email', '{{%user_telesales}}', 'email');
        
        $this->createTable('{{%user_telesale_actions}}', [
            'id' => $this->primaryKey(11),
            'user_telesale_id' => $this->integer(11)->notNull(),
            'user_id' => $this->integer(11)->notNull(),
            'name' => $this->string(50),
            'action_time' => $this->integer(10),
        ]);
        
        $this->createIndex('index_user_telesale_id', '{{%user_telesale_actions}}', 'user_telesale_id');
        
        $this->createTable('{{%user_telesale_action_meta}}', [
            'id' => $this->primaryKey(11),
            'user_telesale_action_id' => $this->integer(11)->notNull(),
            'key' => $this->string(20)->notNull(),
            'value' => $this->text(),
        ]);
        
        $this->createIndex('index_user_telesale_action_id', '{{%user_telesale_action_meta}}', 'user_telesale_action_id');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('{{%user_telesale_action_meta}}');
        
        $this->dropTable('{{%user_telesale_actions}}');
        
        $this->dropTable('{{%user_telesales}}');
        
        return true;
    }

}
