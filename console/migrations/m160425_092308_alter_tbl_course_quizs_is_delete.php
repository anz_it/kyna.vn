<?php

class m160425_092308_alter_tbl_course_quizs_is_delete extends console\components\KynaMigration
{

    private $_solfDeleteTables = [
        '{{%course_quizs}}',
    ];

    public function safeUp()
    {
        foreach ($this->_solfDeleteTables as $table) {
            $this->addColumn($table, 'is_deleted', "bit(1) DEFAULT b'0' AFTER status");
        }
    }

    public function safeDown()
    {
        foreach ($this->_solfDeleteTables as $table) {
            $this->dropColumn($table, 'is_deleted');
        }
        
        echo "m160425_092308_alter_tbl_course_quizs_is_delete has been reverted.\n";

        return true;
    }
}
