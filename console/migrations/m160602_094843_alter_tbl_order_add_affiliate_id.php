<?php

use yii\db\Migration;

class m160602_094843_alter_tbl_order_add_affiliate_id extends Migration
{

    public function safeUp()
    {
        $this->addColumn('{{%orders}}', 'affiliate_id', 'INT(11) default 0 after reference_id');
    }

    public function safeDown()
    {
        $this->dropColumn('{{%orders}}', 'affiliate_id');
        
        return true;
    }
}
