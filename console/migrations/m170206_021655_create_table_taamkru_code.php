<?php

use yii\db\Migration;

class m170206_021655_create_table_taamkru_code extends Migration
{
    public function up()
    {
        $this->createTable('{{%taamkru_code}}', [
            'id' => $this->primaryKey(),
            'serial' => $this->string(50)->unique()->notNull(),
            'code' => $this->string(50)->unique()->notNull(),
            'retailer_id' => $this->integer(11),
            'category_id' => $this->integer(11),
            'status' => $this->smallInteger(1)->defaultValue(0),
            'is_deleted' => $this->smallInteger(1)->defaultValue(0),
            'created_time' => $this->integer(10)->defaultValue(0),
            'updated_time' => $this->integer(10)->defaultValue(0),
            'activation_date' => $this->integer(10)->defaultValue(0),
        ]);
        $this->createIndex('idx_taamkru_code_serial', '{{%taamkru_code}}', 'serial');
    }

    public function down()
    {
        $this->dropIndex('idx_taamkru_code_serial', '{{%taamkru_code}}');
        $this->dropTable('{{%taamkru_code}}');
        return true;
    }
}
