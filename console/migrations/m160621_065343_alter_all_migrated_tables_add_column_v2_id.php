<?php

use yii\db\Migration;

class m160621_065343_alter_all_migrated_tables_add_column_v2_id extends Migration
{

    public function safeUp()
    {
        $this->addColumn('{{%user}}', 'v2_id', $this->integer(11)->defaultValue(null));
        $this->createIndex('index_v2_id', '{{%user}}', 'v2_id');
        
        $this->addColumn('{{%courses}}', 'v2_id', $this->integer(11)->defaultValue(null));
        $this->createIndex('index_v2_id', '{{%courses}}', 'v2_id');
        $this->addColumn('{{%course_sections}}', 'v2_id', $this->integer(11)->defaultValue(null));
        $this->createIndex('index_v2_id', '{{%course_sections}}', 'v2_id');
        $this->addColumn('{{%course_lessons}}', 'v2_id', $this->integer(11)->defaultValue(null));
        $this->createIndex('index_v2_id', '{{%course_lessons}}', 'v2_id');
        $this->addColumn('{{%quizes}}', 'v2_id', $this->integer(11)->defaultValue(null));
        $this->createIndex('index_v2_id', '{{%quizes}}', 'v2_id');
        $this->addColumn('{{%quiz_questions}}', 'v2_id', $this->integer(11)->defaultValue(null));
        $this->createIndex('index_v2_id', '{{%quiz_questions}}', 'v2_id');
        $this->addColumn('{{%quiz_answers}}', 'v2_id', $this->integer(11)->defaultValue(null));
        $this->createIndex('index_v2_id', '{{%quiz_answers}}', 'v2_id');
        $this->addColumn('{{%course_missions}}', 'v2_id', $this->integer(11)->defaultValue(null));
        $this->createIndex('index_v2_id', '{{%course_missions}}', 'v2_id');
        
        $this->addColumn('{{%promotions}}', 'v2_id', $this->integer(11)->defaultValue(null));
        $this->createIndex('index_v2_id', '{{%promotions}}', 'v2_id');
        
        $this->addColumn('{{%categories}}', 'v2_id', $this->integer(11)->defaultValue(null));
        $this->createIndex('index_v2_id', '{{%categories}}', 'v2_id');
        
        $this->addColumn('{{%orders}}', 'v2_id', $this->integer(11)->defaultValue(null));
        $this->createIndex('index_v2_id', '{{%orders}}', 'v2_id');
        
        $this->addColumn('{{%promotion_schedules}}', 'v2_id', $this->integer(11)->defaultValue(null));
        $this->createIndex('index_v2_id', '{{%promotion_schedules}}', 'v2_id');
    }

    public function safeDown()
    {
        $this->dropIndex('index_v2_id', '{{%user}}');
        $this->dropColumn('{{%user}}', 'v2_id');
        
        $this->dropIndex('index_v2_id', '{{%courses}}');
        $this->dropColumn('{{%courses}}', 'v2_id');
        $this->dropIndex('index_v2_id', '{{%course_sections}}');
        $this->dropColumn('{{%course_sections}}', 'v2_id');
        $this->dropIndex('index_v2_id', '{{%course_lessons}}');
        $this->dropColumn('{{%course_lessons}}', 'v2_id');
        $this->dropIndex('index_v2_id', '{{%quizes}}');
        $this->dropColumn('{{%quizes}}', 'v2_id');
        $this->dropIndex('index_v2_id', '{{%quiz_questions}}');
        $this->dropColumn('{{%quiz_questions}}', 'v2_id');
        $this->dropIndex('index_v2_id', '{{%quiz_answers}}');
        $this->dropColumn('{{%quiz_answers}}', 'v2_id');
        $this->dropIndex('index_v2_id', '{{%course_missions}}');
        $this->dropColumn('{{%course_missions}}', 'v2_id');
        
        $this->dropIndex('index_v2_id', '{{%promotions}}');
        $this->dropColumn('{{%promotions}}', 'v2_id');
        
        $this->dropIndex('index_v2_id', '{{%categories}}');
        $this->dropColumn('{{%categories}}', 'v2_id');
        
        $this->dropIndex('index_v2_id', '{{%orders}}');
        $this->dropColumn('{{%orders}}', 'v2_id');
        
        $this->dropIndex('index_v2_id', '{{%promotion_schedules}}');
        $this->dropColumn('{{%promotion_schedules}}', 'v2_id');
        
        return true;
    }

}
