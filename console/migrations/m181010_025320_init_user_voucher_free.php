<?php

use yii\db\Migration;

class m181010_025320_init_user_voucher_free extends Migration
{
    const USER_VOUCHER_FREE_TABLE = "user_voucher_free";
    public function up()
    {
        $this->createTable(self::USER_VOUCHER_FREE_TABLE, [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull()->unique(),
            'code' => $this->string('255')->notNull()->unique(),
            'prefix' => $this->string('255')->notNull(),
            'total_introduce' => $this->integer(),
            'total_received' => $this->integer(),
            'total_consume' => $this->integer(),
            'created_time' => $this->integer(),
            'updated_time' => $this->integer(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
        ]);
        $this->createIndex('idx_user_id_code', self::USER_VOUCHER_FREE_TABLE, ['user_id', 'code']);
    }

    public function down()
    {
        $this->dropIndex('idx_user_id_code',self::USER_VOUCHER_FREE_TABLE);
        $this->dropTable(self::USER_VOUCHER_FREE_TABLE);
    }
}
