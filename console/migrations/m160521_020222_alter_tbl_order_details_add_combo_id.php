<?php

use yii\db\Migration;

class m160521_020222_alter_tbl_order_details_add_combo_id extends Migration
{

    public function safeUp()
    {
        $this->addColumn('{{%order_details}}', "course_combo_id", 'INT(11) DEFAULT 0 AFTER `course_id`');
        
        $this->createIndex('index_course_combo_id', '{{%order_details}}', 'course_combo_id');
    }

    public function safeDown()
    {
        $this->dropIndex('index_course_combo_id', '{{%order_details}}');
        
        $this->dropColumn('{{%order_details}}', 'course_combo_id');

        return true;
    }

}