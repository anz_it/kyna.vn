<?php

use yii\db\Migration;

class m170222_075405_create_scheme_tags extends Migration
{
    public function up()
    {
        $this->createTable('{{%tags}}', [
            'id' => $this->primaryKey(),
            'tag' => $this->string()->notNull()->unique(),
            'slug' => $this->string()->notNull()->unique(),
            'description' => $this->string(),
            'status' => $this->smallInteger(1)->defaultValue(0),
            'is_desktop' => $this->smallInteger(1)->defaultValue(0),
            'is_mobile' => $this->smallInteger(1)->defaultValue(0),
            'image_url' => $this->text(),
            'is_deleted' => $this->smallInteger(1)->defaultValue(0),
            'created_time' => $this->integer(10)->defaultValue(0),
            'updated_time' => $this->integer(10)->defaultValue(0),
        ]);
        $this->createIndex('idx_tags_tag', '{{%tags}}', 'tag');

        $this->createTable('{{%course_tags}}', [
            'course_id' => $this->integer(10)->notNull(),
            'tag_id' => $this->integer(10)->notNull(),
        ]);
        $this->createIndex('idx_course_tags_course_id_tag_id', '{{%course_tags}}', ['course_id', 'tag_id'], true);
    }

    public function down()
    {
        $this->dropIndex('idx_tags_tag', '{{%tags}}');
        $this->dropTable('{{%tags}}');

        $this->dropIndex('idx_course_tags_course_id', '{{%course_tags}}');
        $this->dropIndex('idx_course_tags_tag_id', '{{%course_tags}}');
        $this->dropTable('{{%course_tags}}');
        return true;
    }

}