<?php

use yii\db\Migration;

class m161013_084848_create_table_subscribers extends Migration
{

    public function up()
    {
        $this->createTable('{{%subscribers}}', [
            'id' => $this->primaryKey(),
            'email' => $this->string()->notNull()->unique(),
            'landing_page' => $this->string(),
            'subscribe_time' => $this->integer(11),
            'opt_out' => $this->smallInteger(2)->defaultValue(0),
            'created_time' => $this->integer(11),
            'updated_time' => $this->integer(11)
        ]);
        // tạo index
        $this->createIndex(
                'idx-subscribers-email', 'subscribers', 'email'
        );
    }

    public function down()
    {
        $this->dropTable('{{%subscribers}}');
        return true;
    }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
