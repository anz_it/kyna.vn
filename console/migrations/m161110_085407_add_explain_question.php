<?php

use yii\db\Migration;

class m161110_085407_add_explain_question extends Migration
{
    public function up()
    {
        $this->addColumn(\kyna\course\models\QuizQuestion::tableName(), "answer_explain", "text null");
    }

    public function down()
    {
        echo "m161110_085407_add_explain_question cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
