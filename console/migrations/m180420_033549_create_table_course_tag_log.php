<?php

use yii\db\Migration;

class m180420_033549_create_table_course_tag_log extends Migration
{
    const COURSE_TAG_LOG_TABLE = '{{%course_tags_log}}';
    public function up()
    {

        $this->createTable(
            self::COURSE_TAG_LOG_TABLE,
            [
                'id' => $this->primaryKey(11),
                'course_id' => $this->integer(11)->notNull(),
                'tag_ids' => $this->string(500),
                'add_ids' => $this->string(500),
                'remove_ids' => $this->string(500),
                'updated_time' => $this->integer(10),
                'created_by' => $this->integer(10),
            ]
        );
    }

    public function down()
    {
        echo "m180420_033549_create_table_course_tag_log cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
