<?php

use console\components\KynaMigration;

/**
 * Handles the creation for table `table_mana_educations`.
 */
class m160712_031806_create_table_mana_educations extends KynaMigration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        try {
            $this->createTable('{{%mana_educations}}', [
                'id' => $this->primaryKey(),
                'name' => $this->string(255)->notNull(),
                'slug' => $this->string(255)->notNull(),
                'description' => $this->text(),
                'image_url' => $this->text(),
                'order' => $this->integer(11)->defaultValue(0),
                'status' => $this->smallInteger(3)->defaultValue(1),
                'created_time' => $this->integer(10)->defaultValue(0),
                'updated_time' => $this->integer(10)->defaultValue(0),
            ], self::$_tableOptions
            );

            $this->createIndex('index_name', '{{%mana_educations}}', ['name']);
            $this->createIndex('index_slug', '{{%mana_educations}}', ['slug']);
        } catch (yii\console\Exception $ex) {
            echo $ex->getMessage();
            return false;
        }

        return true;
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%mana_educations}}');
        echo "m160712_031806_create_table_mana_educations has been reverted.\n";
        return true;
    }
}
