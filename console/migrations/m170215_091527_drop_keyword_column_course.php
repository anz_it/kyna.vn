<?php

use yii\db\Migration;
use \kyna\course\models\CourseMeta;
use \kyna\base\models\MetaField;
use \kyna\course\models\Course;

class m170215_091527_drop_keyword_column_course extends Migration
{
    public function up()
    {
        $this->dropColumn(Course::tableName(),"keyword");
        $this->delete(MetaField::tableName(), ['key' => 'facebook_thumbnail']);
        $this->delete(CourseMeta::tableName(), ['key' => 'facebook_thumbnail']);
    }

    public function down()
    {
        echo "m170215_091527_drop_keyword_column_course cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
