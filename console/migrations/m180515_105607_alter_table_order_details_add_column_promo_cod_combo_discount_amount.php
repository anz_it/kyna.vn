<?php

use yii\db\Migration;

class m180515_105607_alter_table_order_details_add_column_promo_cod_combo_discount_amount extends Migration
{
    const ORDER_DETAILS_TABLE = "{{%order_details}}";
    public function up()
    {
        $this->addColumn(self::ORDER_DETAILS_TABLE, 'promo_code', $this->string(255));
        $this->addColumn(self::ORDER_DETAILS_TABLE, 'combo_discount_amount', $this->float());
    }

    public function down()
    {
        echo "m180515_105607_alter_table_order_details_add_column_promo_cod_combo_discount_amount cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
