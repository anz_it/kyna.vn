<?php

use yii\db\Migration;

class m160527_025349_delete_foreign_keys extends Migration
{

    public function safeUp()
    {
        $this->dropForeignKey('fk_promotion_course', '{{%coupon_courses}}');
        $this->dropForeignKey('fk_course_promotion', '{{%coupon_courses}}');
        
        $this->createIndex('index_promotion_id', '{{%coupon_courses}}', 'promotion_id');
        $this->createIndex('index_course_id', '{{%coupon_courses}}', 'course_id');
    }

    public function safeDown()
    {
        $this->dropIndex('index_course_id', '{{%coupon_courses}}');
        $this->dropIndex('index_promotion_id', '{{%coupon_courses}}');
        
        $this->addForeignKey('fk_course_promotion', "{{%coupon_courses}}", 'course_id', "{{%courses}}", 'id');
        $this->addForeignKey('fk_promotion_course', "{{%coupon_courses}}", 'promotion_id', "{{%promotions}}", 'id');
        
        return true;
    }

}
