<?php

use yii\db\Migration;

class m170113_163453_add_table_tracking extends Migration
{
    public function up()
    {

        $this->execute("
          CREATE TABLE `tracking` (
          `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
          `number` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
          `piwik_id` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
          `updated_time` int(11) NOT NULL,
          `piwik_visitor_id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
          `update_piwik_times` int(11) default 0,
          `number_called` int (11) default 0,
          PRIMARY KEY (`id`),
          UNIQUE KEY `number` (`number`)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
        ");
    }

    public function down()
    {
       $this->dropTable("tracking");
        return true;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
