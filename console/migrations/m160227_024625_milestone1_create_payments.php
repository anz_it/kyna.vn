<?php

use console\components\KynaMigration;

class m160227_024625_milestone1_create_payments extends KynaMigration
{

    public function up()
    {
        $this->createTableTransaction();
        
        $this->createTablePaymentMethod();
        
        $this->createTablePaymentTransaction();
        
        $this->createTablePaymentMethodPricing();
        
        $this->createTablePaymentTransactionMeta();
    }

    public function down()
    {
        $this->dropTablePaymentTransactionMeta();
        
        $this->dropTablePaymentMethodPricing();
        
        $this->dropTablePaymentTransaction();
        
        $this->dropTablePaymentMethod();
        
        $this->dropTableTransaction();
        
        echo "m160227_024625_milestone1_create_payments has been reverted.\n";

        return true;
    }
    
    /**
     * @desc create table `transactions` and related
     * @return boolean
     */
    private function createTableTransaction()
    {
        try {
            $this->createTable('{{%transactions}}', [
                'id' => $this->primaryKey(),
                'user_id' => $this->integer(11)->notNull(),
                'amount' => $this->double()->defaultValue(0),
                'type' => $this->smallInteger(3)->defaultValue(0),
                'is_credit' => "bit(1) DEFAULT b'0' COMMENT 'credit/debit'",
                'updated_balance' => $this->double()->defaultValue(0),
                'status' => $this->smallInteger(3)->defaultValue(1),
                'created_time' => $this->integer(10)->defaultValue(0),
                'updated_time' => $this->integer(10)->defaultValue(0),
                    ], self::$_tableOptions
            );

            $this->createIndex('index_user_id', '{{%transactions}}', ['user_id']);
        } catch (yii\console\Exception $ex) {
            echo $ex->getMessage();
            return false;
        }
        
        return true;
    }
    
    private function dropTableTransaction()
    {
        try {
            $this->dropIndex('index_user_id', '{{%transactions}}');

            $this->dropTable('{{%transactions}}');
        } catch (yii\console\Exception $ex) {
            echo $ex->getMessage();
            return false;
        }
        
        return true;
    }
    
    /**
     * @desc create table `payment_methods` and related
     * @return boolean
     */
    private function createTablePaymentMethod()
    {
        try {
            $this->createTable('{{%payment_methods}}', [
                'id' => $this->primaryKey(),
                'payment_vendor_id' => $this->integer(11)->defaultValue(0),
                'name' => $this->string(45)->notNull(),
                'vendor_method_id' => $this->integer(11)->defaultValue(0),
                'is_auto_calculate' => "bit(1) DEFAULT b'0'",
                'status' => $this->smallInteger(3)->defaultValue(1),
                'created_time' => $this->integer(10)->defaultValue(0),
                'updated_time' => $this->integer(10)->defaultValue(0),
                    ], self::$_tableOptions
            );

            $this->createIndex('index_payment_vendor_id', '{{%payment_methods}}', ['payment_vendor_id']);
            $this->createIndex('index_vendor_method_id', '{{%payment_methods}}', ['vendor_method_id']);
        } catch (yii\console\Exception $ex) {
            echo $ex->getMessage();
            return false;
        }
        
        return true;
    }
    
    private function dropTablePaymentMethod()
    {
        try {
            $this->dropIndex('index_vendor_method_id', '{{%payment_methods}}');
            $this->dropIndex('index_payment_vendor_id', '{{%payment_methods}}');

            $this->dropTable('{{%payment_methods}}');
        } catch (yii\console\Exception $ex) {
            echo $ex->getMessage();
            return false;
        }
        
        return true;
    }
    
    /**
     * @desc create table `payment_transactions` and related
     * @return boolean
     */
    private function createTablePaymentTransaction()
    {
        try {
            $this->createTable('{{%payment_transactions}}', [
                'id' => $this->primaryKey(),
                'transaction_id' => $this->integer(11)->defaultValue(0),
                'payment_method_id' => $this->integer(11)->notNull(),
                'payment_fee' => $this->double()->defaultValue(0),
                'total_amount' => $this->double()->defaultValue(0),
                    ], self::$_tableOptions
            );

            $this->createIndex('index_transaction_id', '{{%payment_transactions}}', ['transaction_id']);
            $this->createIndex('index_payment_method_id', '{{%payment_transactions}}', ['payment_method_id']);
        } catch (yii\console\Exception $ex) {
            echo $ex->getMessage();
            return false;
        }
        
        return true;
    }
    
    private function dropTablePaymentTransaction()
    {
        try {
            $this->dropIndex('index_payment_method_id', '{{%payment_transactions}}');
            $this->dropIndex('index_transaction_id', '{{%payment_transactions}}');

            $this->dropTable('{{%payment_transactions}}');
        } catch (yii\console\Exception $ex) {
            echo $ex->getMessage();
            return false;
        }
        
        return true;
    }
    
    /**
     * @desc create table `payment_method_pricing` and related
     * @return boolean
     */
    private function createTablePaymentMethodPricing()
    {
        try {
            $this->createTable('{{%payment_method_pricing}}', [
                'id' => $this->primaryKey(),
                'payment_method_id' => $this->integer(11)->notNull(),
                'transaction_fee' => $this->double()->defaultValue(0),
                'commission_fee' => $this->double()->defaultValue(0),
                'begin_date' => $this->integer(10)->defaultValue(0),
                'end_date' => $this->integer(10)->defaultValue(0),
                'status' => $this->smallInteger(3)->defaultValue(1),
                'created_time' => $this->integer(10)->defaultValue(0),
                'updated_time' => $this->integer(10)->defaultValue(0),
                    ], self::$_tableOptions
            );

            $this->createIndex('index_payment_method_id', '{{%payment_method_pricing}}', ['payment_method_id']);
        } catch (yii\console\Exception $ex) {
            echo $ex->getMessage();
            return false;
        }
        
        return true;
    }
    
    private function dropTablePaymentMethodPricing()
    {
        try {
            $this->dropIndex('index_payment_method_id', '{{%payment_method_pricing}}');
 
            $this->dropTable('{{%payment_method_pricing}}');
        } catch (yii\console\Exception $ex) {
            echo $ex->getMessage();
            return false;
        }
        
        return true;
    }
    
    /**
     * @desc create table `payment_transaction_meta` and related
     * @return boolean
     */
    private function createTablePaymentTransactionMeta()
    {
        try {
            $this->createTable('{{%payment_transaction_meta}}', [
                'id' => $this->primaryKey(),
                'payment_transaction_id' => $this->integer(11)->notNull(),
                'name' => $this->string(255),
                'value' => $this->text(),
                    ], self::$_tableOptions
            );

            $this->createIndex('index_payment_transaction_id', '{{%payment_transaction_meta}}', ['payment_transaction_id']);
        } catch (yii\console\Exception $ex) {
            echo $ex->getMessage();
            return false;
        }
        
        return true;
    }
    
    private function dropTablePaymentTransactionMeta()
    {
        try {
            $this->dropIndex('index_payment_transaction_id', '{{%payment_transaction_meta}}');
 
            $this->dropTable('{{%payment_transaction_meta}}');
        } catch (yii\console\Exception $ex) {
            echo $ex->getMessage();
            return false;
        }
        
        return true;
    }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
