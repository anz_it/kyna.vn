<?php

use yii\db\Migration;

class m170829_044911_init_gamification_data extends Migration
{
    public function up()
    {
        $authManager = Yii::$app->authManager;
        // Create permissions
        $authManager->add($authManager->createPermission('Gamification.Mission.All'));
        $authManager->add($authManager->createPermission('Gamification.Mission.View'));
        $authManager->add($authManager->createPermission('Gamification.Mission.Create'));
        $authManager->add($authManager->createPermission('Gamification.Mission.Update'));
        $authManager->add($authManager->createPermission('Gamification.Mission.Delete'));

        $authManager->addChild($authManager->getPermission('Gamification.Mission.Create'), $authManager->getPermission('Gamification.Mission.View'));
        $authManager->addChild($authManager->getPermission('Gamification.Mission.Update'), $authManager->getPermission('Gamification.Mission.View'));
        $authManager->addChild($authManager->getPermission('Gamification.Mission.Delete'), $authManager->getPermission('Gamification.Mission.View'));

        // Add other permission to Notification All
        $authManager->addChild($authManager->getPermission('Gamification.Mission.All'), $authManager->getPermission('Gamification.Mission.View'));
        $authManager->addChild($authManager->getPermission('Gamification.Mission.All'), $authManager->getPermission('Gamification.Mission.Create'));
        $authManager->addChild($authManager->getPermission('Gamification.Mission.All'), $authManager->getPermission('Gamification.Mission.Update'));
        $authManager->addChild($authManager->getPermission('Gamification.Mission.All'), $authManager->getPermission('Gamification.Mission.Delete'));


        // Create permissions
        $authManager->add($authManager->createPermission('Gamification.Gift.All'));
        $authManager->add($authManager->createPermission('Gamification.Gift.View'));
        $authManager->add($authManager->createPermission('Gamification.Gift.Create'));
        $authManager->add($authManager->createPermission('Gamification.Gift.Update'));
        $authManager->add($authManager->createPermission('Gamification.Gift.Delete'));

        $authManager->addChild($authManager->getPermission('Gamification.Gift.Create'), $authManager->getPermission('Gamification.Gift.View'));
        $authManager->addChild($authManager->getPermission('Gamification.Gift.Update'), $authManager->getPermission('Gamification.Gift.View'));
        $authManager->addChild($authManager->getPermission('Gamification.Gift.Delete'), $authManager->getPermission('Gamification.Gift.View'));

        // Add other permission to Notification All
        $authManager->addChild($authManager->getPermission('Gamification.Gift.All'), $authManager->getPermission('Gamification.Gift.View'));
        $authManager->addChild($authManager->getPermission('Gamification.Gift.All'), $authManager->getPermission('Gamification.Gift.Create'));
        $authManager->addChild($authManager->getPermission('Gamification.Gift.All'), $authManager->getPermission('Gamification.Gift.Update'));
        $authManager->addChild($authManager->getPermission('Gamification.Gift.All'), $authManager->getPermission('Gamification.Gift.Delete'));

        // Create permissions
        $authManager->add($authManager->createPermission('Gamification.User.All'));
        $authManager->add($authManager->createPermission('Gamification.User.View'));
        $authManager->add($authManager->createPermission('Gamification.User.Create'));
        $authManager->add($authManager->createPermission('Gamification.User.Update'));
        $authManager->add($authManager->createPermission('Gamification.User.Delete'));

        $authManager->addChild($authManager->getPermission('Gamification.User.Create'), $authManager->getPermission('Gamification.User.View'));
        $authManager->addChild($authManager->getPermission('Gamification.User.Update'), $authManager->getPermission('Gamification.User.View'));
        $authManager->addChild($authManager->getPermission('Gamification.User.Delete'), $authManager->getPermission('Gamification.User.View'));

        // Add other permission to Notification All
        $authManager->addChild($authManager->getPermission('Gamification.User.All'), $authManager->getPermission('Gamification.User.View'));
        $authManager->addChild($authManager->getPermission('Gamification.User.All'), $authManager->getPermission('Gamification.User.Create'));
        $authManager->addChild($authManager->getPermission('Gamification.User.All'), $authManager->getPermission('Gamification.User.Update'));
        $authManager->addChild($authManager->getPermission('Gamification.User.All'), $authManager->getPermission('Gamification.User.Delete'));

        $authManager->add($authManager->createPermission('Gamification.All'));

        $authManager->addChild($authManager->getPermission('Gamification.All'), $authManager->getPermission('Gamification.Mission.All'));
        $authManager->addChild($authManager->getPermission('Gamification.All'), $authManager->getPermission('Gamification.Gift.All'));
        $authManager->addChild($authManager->getPermission('Gamification.All'), $authManager->getPermission('Gamification.User.All'));

        // Add permission.All to Admin
        $authManager->addChild($authManager->getRole('Dev'), $authManager->getPermission('Gamification.All'));
        $authManager->addChild($authManager->getRole('Admin'), $authManager->getPermission('Gamification.All'));

        $this->insert('meta_fields', [
            'key' => 'is_end_quiz',
            'name' => 'Bài quiz cuối khóa',
            'type' => 4,
            'model' => 'course_quiz',
            'status' => 1,
            'created_time' => time(),
        ]);
    }

    public function down()
    {
        $this->delete('meta_fields', ['key' => 'is_end_quiz']);

        $authManager = Yii::$app->authManager;
        $authManager->removeChild($authManager->getRole('Dev'), $authManager->getPermission('Gamification.All'));
        $authManager->removeChild($authManager->getRole('Admin'), $authManager->getPermission('Gamification.All'));

        $authManager->removeChild($authManager->getPermission('Gamification.All'), $authManager->getPermission('Gamification.Mission.All'));
        $authManager->removeChild($authManager->getPermission('Gamification.All'), $authManager->getPermission('Gamification.Gift.All'));
        $authManager->removeChild($authManager->getPermission('Gamification.All'), $authManager->getPermission('Gamification.User.All'));

        $authManager->remove($authManager->getPermission('Gamification.All'));

        // User
        $authManager->removeChild($authManager->getPermission('Gamification.User.All'), $authManager->getPermission('Gamification.User.View'));
        $authManager->removeChild($authManager->getPermission('Gamification.User.All'), $authManager->getPermission('Gamification.User.Create'));
        $authManager->removeChild($authManager->getPermission('Gamification.User.All'), $authManager->getPermission('Gamification.User.Update'));
        $authManager->removeChild($authManager->getPermission('Gamification.User.All'), $authManager->getPermission('Gamification.User.Delete'));

        $authManager->removeChild($authManager->getPermission('Gamification.User.Create'), $authManager->getPermission('Gamification.User.View'));
        $authManager->removeChild($authManager->getPermission('Gamification.User.Update'), $authManager->getPermission('Gamification.User.View'));
        $authManager->removeChild($authManager->getPermission('Gamification.User.Delete'), $authManager->getPermission('Gamification.User.View'));

        // Delete permisstion
        $authManager->remove($authManager->getPermission('Gamification.User.View'));
        $authManager->remove($authManager->getPermission('Gamification.User.Create'));
        $authManager->remove($authManager->getPermission('Gamification.User.Update'));
        $authManager->remove($authManager->getPermission('Gamification.User.Delete'));
        $authManager->remove($authManager->getPermission('Gamification.User.All'));

        // Gift
        $authManager->removeChild($authManager->getPermission('Gamification.Gift.All'), $authManager->getPermission('Gamification.Gift.View'));
        $authManager->removeChild($authManager->getPermission('Gamification.Gift.All'), $authManager->getPermission('Gamification.Gift.Create'));
        $authManager->removeChild($authManager->getPermission('Gamification.Gift.All'), $authManager->getPermission('Gamification.Gift.Update'));
        $authManager->removeChild($authManager->getPermission('Gamification.Gift.All'), $authManager->getPermission('Gamification.Gift.Delete'));

        $authManager->removeChild($authManager->getPermission('Gamification.Gift.Create'), $authManager->getPermission('Gamification.Gift.View'));
        $authManager->removeChild($authManager->getPermission('Gamification.Gift.Update'), $authManager->getPermission('Gamification.Gift.View'));
        $authManager->removeChild($authManager->getPermission('Gamification.Gift.Delete'), $authManager->getPermission('Gamification.Gift.View'));

        // Delete permisstion
        $authManager->remove($authManager->getPermission('Gamification.Gift.View'));
        $authManager->remove($authManager->getPermission('Gamification.Gift.Create'));
        $authManager->remove($authManager->getPermission('Gamification.Gift.Update'));
        $authManager->remove($authManager->getPermission('Gamification.Gift.Delete'));
        $authManager->remove($authManager->getPermission('Gamification.Gift.All'));

        $authManager->removeChild($authManager->getPermission('Gamification.Mission.Create'), $authManager->getPermission('Gamification.Mission.View'));
        $authManager->removeChild($authManager->getPermission('Gamification.Mission.Update'), $authManager->getPermission('Gamification.Mission.View'));
        $authManager->removeChild($authManager->getPermission('Gamification.Mission.Delete'), $authManager->getPermission('Gamification.Mission.View'));

        // Mission
        $authManager->removeChild($authManager->getPermission('Gamification.Mission.All'), $authManager->getPermission('Gamification.Mission.View'));
        $authManager->removeChild($authManager->getPermission('Gamification.Mission.All'), $authManager->getPermission('Gamification.Mission.Create'));
        $authManager->removeChild($authManager->getPermission('Gamification.Mission.All'), $authManager->getPermission('Gamification.Mission.Update'));
        $authManager->removeChild($authManager->getPermission('Gamification.Mission.All'), $authManager->getPermission('Gamification.Mission.Delete'));

        // Delete permisstion
        $authManager->remove($authManager->getPermission('Gamification.Mission.View'));
        $authManager->remove($authManager->getPermission('Gamification.Mission.Create'));
        $authManager->remove($authManager->getPermission('Gamification.Mission.Update'));
        $authManager->remove($authManager->getPermission('Gamification.Mission.Delete'));
        $authManager->remove($authManager->getPermission('Gamification.Mission.All'));
    }

}
