<?php

use yii\db\Migration;

class m180626_042552_add_column_is_type_column_notification_table extends Migration
{
    public function up()
    {
        $this->addColumn('notification', 'is_type', $this->integer(1)->comment('mobile : 1, desktop: 2'));
    }

    public function down()
    {
        $this->dropColumn('notification', 'is_type');
    }
}
