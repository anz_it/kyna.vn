<?php

use yii\db\Migration;

class m160523_103647_alter_table_coupon extends Migration
{
    public function up()
    {
        $this->alterColumn("{{%coupons}}", 'start_date', $this->integer(10));
        $this->alterColumn("{{%coupons}}", 'end_date', $this->integer(10));

    }

    public function down()
    {
        return true;
    }
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */

}
