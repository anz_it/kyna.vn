<?php

use yii\db\Migration;

class m160317_045830_order_action_meta_alter extends Migration
{
    public function up()
    {
        $this->dropTable('{{%order_call_log}}');
        $this->renameColumn('{{%order_action_meta}}', 'action_id', 'order_action_id');
        return true;
    }

    public function down()
    {
        echo "m160317_045830_order_action_meta_alter has been reverted.\n";

        return true;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
