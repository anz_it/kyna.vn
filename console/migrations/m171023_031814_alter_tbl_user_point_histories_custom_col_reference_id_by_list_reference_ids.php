<?php

use yii\db\Migration;

class m171023_031814_alter_tbl_user_point_histories_custom_col_reference_id_by_list_reference_ids extends Migration
{
    public function up()
    {
        $this->alterColumn('user_point_histories', 'reference_id', $this->text());
        $this->renameColumn('user_point_histories', 'reference_id', 'list_reference_ids');
    }

    public function down()
    {
        $this->renameColumn('user_point_histories', 'list_reference_ids', 'reference_id');
        $this->alterColumn('user_point_histories', 'reference_id', $this->integer());
    }
}
