<?php

/**
 * Handles the creation for table `table_mana_certificates`.
 */
class m160713_024300_create_table_mana_certificates extends \console\components\KynaMigration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        try {
            $this->createTable('{{%mana_certificates}}', [
                'id' => $this->primaryKey(),
                'name' => $this->string(255)->notNull(),
                'slug' => $this->string(255)->notNull(),
                'description' => $this->text(),
                'image_url' => $this->text(),
                'order' => $this->integer(11)->defaultValue(0),
                'status' => $this->smallInteger(3)->defaultValue(1),
                'created_time' => $this->integer(10)->defaultValue(0),
                'updated_time' => $this->integer(10)->defaultValue(0),
            ], self::$_tableOptions
            );

            $this->createIndex('index_name', '{{%mana_certificates}}', ['name']);
            $this->createIndex('index_slug', '{{%mana_certificates}}', ['slug']);
        } catch (yii\console\Exception $ex) {
            echo $ex->getMessage();
            return false;
        }

        return true;
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%mana_certificates}}');
        echo "m160713_024300_create_table_mana_certificates has been reverted.\n";
        return true;
    }
}
