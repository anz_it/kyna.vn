<?php

use yii\db\Migration;

class m170714_063154_new_permission_notification extends Migration
{
    // Jobs
    // Create permission Notification.All, .View, .Create, .Update, .Delete
    // Add notification.All to Role Admin

    public function up()
    {
        // Create permissions
        Yii::$app->authManager->add(Yii::$app->authManager->createPermission('Notification.All'));
        Yii::$app->authManager->add(Yii::$app->authManager->createPermission('Notification.View'));
        Yii::$app->authManager->add(Yii::$app->authManager->createPermission('Notification.Create'));
        Yii::$app->authManager->add(Yii::$app->authManager->createPermission('Notification.Update'));
        Yii::$app->authManager->add(Yii::$app->authManager->createPermission('Notification.Delete'));

        // Add other permission to Notification All
        Yii::$app->authManager->addChild(Yii::$app->authManager->getPermission('Notification.All'), Yii::$app->authManager->getPermission('Notification.View'));
        Yii::$app->authManager->addChild(Yii::$app->authManager->getPermission('Notification.All'), Yii::$app->authManager->getPermission('Notification.Create'));
        Yii::$app->authManager->addChild(Yii::$app->authManager->getPermission('Notification.All'), Yii::$app->authManager->getPermission('Notification.Update'));
        Yii::$app->authManager->addChild(Yii::$app->authManager->getPermission('Notification.All'), Yii::$app->authManager->getPermission('Notification.Delete'));

        // Add permission.All to Admin
        Yii::$app->authManager->addChild(Yii::$app->authManager->getRole('Admin'), Yii::$app->authManager->getPermission('Notification.All'));

    }

    public function down()
    {
//        echo "m170714_063154_new_permission_notification cannot be reverted.\n";
//
//        return false;

        // Remove permisstions child
        Yii::$app->authManager->removeChild(Yii::$app->authManager->getPermission('Notification.All'), Yii::$app->authManager->getPermission('Notification.View'));
        Yii::$app->authManager->removeChild(Yii::$app->authManager->getPermission('Notification.All'), Yii::$app->authManager->getPermission('Notification.Create'));
        Yii::$app->authManager->removeChild(Yii::$app->authManager->getPermission('Notification.All'), Yii::$app->authManager->getPermission('Notification.Update'));
        Yii::$app->authManager->removeChild(Yii::$app->authManager->getPermission('Notification.All'), Yii::$app->authManager->getPermission('Notification.Delete'));

        Yii::$app->authManager->removeChild(Yii::$app->authManager->getRole('Admin'), Yii::$app->authManager->getPermission('Notification.All'));

        // Delete permisstion
        Yii::$app->authManager->remove(Yii::$app->authManager->getPermission('Notification.View'));
        Yii::$app->authManager->remove(Yii::$app->authManager->getPermission('Notification.Create'));
        Yii::$app->authManager->remove(Yii::$app->authManager->getPermission('Notification.Update'));
        Yii::$app->authManager->remove(Yii::$app->authManager->getPermission('Notification.Delete'));
        Yii::$app->authManager->remove(Yii::$app->authManager->getPermission('Notification.All'));

    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
