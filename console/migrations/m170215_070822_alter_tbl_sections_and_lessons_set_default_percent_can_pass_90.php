<?php

use yii\db\Migration;

class m170215_070822_alter_tbl_sections_and_lessons_set_default_percent_can_pass_90 extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('{{%course_sections}}', 'percent_can_pass', $this->float()->defaultValue(90));
        $this->alterColumn('{{%course_lessons}}', 'percent_can_pass', $this->float()->defaultValue(90));
    }

    public function safeDown()
    {
        $this->alterColumn('{{%course_sections}}', 'percent_can_pass', $this->float()->defaultValue(1));
        $this->alterColumn('{{%course_lessons}}', 'percent_can_pass', $this->float()->defaultValue(1));

        return false;
    }
}
