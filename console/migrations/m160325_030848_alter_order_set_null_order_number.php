<?php

use yii\db\Migration;

class m160325_030848_alter_order_set_null_order_number extends console\components\KynaMigration
{

    public function up()
    {
        $this->alterColumn('{{%orders}}', 'order_number', $this->string(20)->defaultValue(NULL));
    }

    public function down()
    {
        echo "m160325_030848_alter_order_set_null_order_number has been reverted.\n";

        return true;
    }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
