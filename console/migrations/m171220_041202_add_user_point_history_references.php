<?php

use yii\db\Migration;

class m171220_041202_add_user_point_history_references extends Migration
{
    public function up()
    {
        $this->createTable('{{%user_point_history_references}}', [
            'id' => $this->primaryKey(11),
            'user_point_history_id' => $this->integer(11)->notNull(),
            'reference_id' => $this->integer(11)->notNull(),
        ]);

        $this->createIndex('index_reference_id', '{{%user_point_history_references}}', 'reference_id');
        $this->createIndex('index_user_point_history_id', '{{%user_point_history_references}}', ['user_point_history_id']);

        $this->createTable('{{%user_point_history_videos}}', [
            'id' => $this->primaryKey(11),
            'user_point_history_id' => $this->integer(11)->notNull(),
            'video_id' => $this->integer(11)->notNull(),
        ]);

        $this->createIndex('index_user_point_history_id', '{{%user_point_history_videos}}', 'user_point_history_id');
        $this->createIndex('index_video_id', '{{%user_point_history_videos}}', ['video_id']);

        $this->renameColumn('user_point_histories', 'list_course_ids', 'course_id');
        $this->alterColumn('user_point_histories', 'course_id', $this->integer(11));
        $this->createIndex('index_course_user_mission', 'user_point_histories', ['user_id', 'mission_id', 'course_id']);
    }

    public function down()
    {
        $this->dropIndex('index_course_user_mission', 'user_point_histories');
        $this->alterColumn('user_point_histories', 'course_id', $this->string(125));
        $this->renameColumn('user_point_histories', 'course_id', 'list_course_ids');

        $this->dropTable('user_point_history_references');
        $this->dropTable('user_point_history_videos');
    }

}
