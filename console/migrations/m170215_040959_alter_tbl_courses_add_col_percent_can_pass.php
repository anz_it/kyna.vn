<?php

use yii\db\Migration;

class m170215_040959_alter_tbl_courses_add_col_percent_can_pass extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%courses}}', 'percent_can_pass', $this->float()->defaultValue(100));
    }

    public function down()
    {
        $this->dropColumn('{{%courses}}', 'percent_can_pass');

        return true;
    }
}
