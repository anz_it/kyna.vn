<?php

use yii\db\Migration;

class m160509_115537_course_learner_questions extends Migration
{
    public function up()
    {
        $this->createTable('{{%course_learner_qna}}', [
            'id' => $this->primaryKey(),
            'course_id' => $this->integer(),
            'user_id' => $this->integer(),
            'content' => $this->text(),
            'question_id' => $this->integer(),
            'posted_time' => $this->integer(),
        ]);

        return true;
    }

    public function down()
    {
        $this->dropTable('{{%course_learner_qna}}');
        
        return true;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
