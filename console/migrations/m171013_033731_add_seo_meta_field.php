<?php

use yii\db\Migration;

class m171013_033731_add_seo_meta_field extends Migration
{
    public function up()
    {
        $this->execute("
            insert into meta_fields(`key`, `name`, `type`, `model`, `status`)
            values ('seo_teacher_title', 'SEO Teacher Title', 2, 'seo_setting', 1);  
            insert into meta_fields(`key`, `name`, `type`, `model`, `status`)
            values ('seo_teacher_description', 'SEO Teacher Description', 2, 'seo_setting', 1);
            insert into seo_setting_meta(`key`, `value`) values ('seo_teacher_title', 'Thông tin giàng viên {teacher_name}');
            insert into seo_setting_meta(`key`, `value`) values ('seo_teacher_description', 'Tổng hợp các khóa học của giảng viên {teacher_name}, {teacher_title}. Ưu đãi học phí khi học Online cùng {teacher_name}. Đăng kí ngay!');
        ");
    }

    public function down()
    {
        echo "m171013_033731_add_seo_meta_field cannot be reverted.\n";

        return false;
    }
}
