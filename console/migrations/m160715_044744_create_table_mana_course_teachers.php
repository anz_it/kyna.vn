<?php

use yii\db\Migration;

/**
 * Handles the creation for table `table_mana_course_teachers`.
 */
class m160715_044744_create_table_mana_course_teachers extends \console\components\KynaMigration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        try {
            $this->createTable('{{%mana_course_teachers}}', [
                'id' => $this->primaryKey(),
                'course_id' => $this->bigInteger()->notNull(),
                'teacher_id' => $this->bigInteger()->notNull(),
                'created_time' => $this->integer(10)->defaultValue(0),
                'updated_time' => $this->integer(10)->defaultValue(0),
            ], self::$_tableOptions
            );

            $this->createIndex('index_course_id', '{{%mana_course_teachers}}', ['course_id']);
            $this->createIndex('index_teacher_id', '{{%mana_course_teachers}}', ['teacher_id']);
        } catch (yii\console\Exception $ex) {
            echo $ex->getMessage();
            return false;
        }

        return true;
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%mana_course_teachers}}');
        echo "m160715_044744_create_table_mana_course_teachers has been reverted.\n";
        return true;
    }
}
