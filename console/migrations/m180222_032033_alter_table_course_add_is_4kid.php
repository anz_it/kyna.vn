<?php

use yii\db\Migration;

class m180222_032033_alter_table_course_add_is_4kid extends Migration
{
    public function up()
    {
        $this->addColumn(
            'courses',
            'is_4kid',
            $this->smallInteger(1).' DEFAULT 0');
    }

    public function down()
    {
        $this->dropColumn('courses', 'is_4kid');
        echo "m180222_032033_alter_table_course_add_is_4kid cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
