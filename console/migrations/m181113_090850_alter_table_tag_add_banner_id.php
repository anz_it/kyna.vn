<?php

use yii\db\Migration;

class m181113_090850_alter_table_tag_add_banner_id extends Migration
{

    public function up()
    {
        $this->addColumn('tags','banner_id', $this->integer());
    }

    public function down()
    {
        echo "m181113_090850_alter_table_tag_add_banner_id cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
