<?php

use yii\db\Migration;

class m160704_163458_alter_table_quiz extends Migration
{
    public function up()
    {
        $this->addColumn("{{%quizes}}", 'type', $this->smallInteger(2));
    }

    public function down()
    {
        return true;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
