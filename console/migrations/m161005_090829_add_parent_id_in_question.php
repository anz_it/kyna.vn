<?php

use yii\db\Migration;

class m161005_090829_add_parent_id_in_question extends Migration
{
    public function up()
    {
        $this->addColumn(\kyna\course\models\QuizQuestion::tableName(), "parent_id", "int null default 0");
    }

    public function down()
    {
        echo "m161005_090829_add_parent_id_in_question cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
