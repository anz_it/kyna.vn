<?php

use yii\db\Migration;

class m180328_040614_add_acp_with_teacher extends Migration
{
    public function up()
    {
        $this->addColumn('commission_incomes','acp_with_teacher',$this->float()->defaultValue(50)->comment('affliate commission percent with teacher role'));

    }

    public function down()
    {
        $this->dropColumn('commission_incomes','acp_with_teacher');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
