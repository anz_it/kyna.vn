<?php

use yii\db\Migration;

class m161104_035823_alter_tbl_quiz_session_answers_col_score_to_float extends Migration
{

    public function up()
    {
        $this->alterColumn('{{%quiz_session_answers}}', 'score', "FLOAT DEFAULT 0");
        
        $this->alterColumn('{{%quiz_session_answer_trash}}', 'score', "FLOAT DEFAULT 0");
    }

    public function down()
    {
        $this->alterColumn('{{%quiz_session_answer_trash}}', 'score', $this->integer(10)->defaultValue(0));
        
        $this->alterColumn('{{%quiz_session_answers}}', 'score', $this->integer(10)->defaultValue(0));

        return true;
    }

}
