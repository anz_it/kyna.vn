<?php

use yii\db\Migration;

class m170208_022209_create_table_taamkru_retailer extends Migration
{
    public function up()
    {
        $this->createTable('{{%taamkru_retailer}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(11)->notNull()->unique(),
            'status' => $this->smallInteger(1)->defaultValue(0),
            'is_deleted' => $this->smallInteger(1)->defaultValue(0),
            'created_time' => $this->integer(10)->defaultValue(0),
            'updated_time' => $this->integer(10)->defaultValue(0),
        ]);
        $this->createIndex('idx_taamkru_retailer_user_id', '{{%taamkru_retailer}}', 'user_id');
    }

    public function down()
    {
        $this->dropIndex('idx_taamkru_retailer_user_id', '{{%taamkru_retailer}}');
        $this->dropTable('{{%taamkru_retailer}}');
        return true;
    }
}
