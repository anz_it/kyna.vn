<?php

use yii\db\Migration;

class m160721_094602_alter_table_user_course extends Migration
{
    public function up()
    {
        $this->addColumn('{{%user_courses}}', 'is_quick', "BIT(1)");


    }

    public function down()
    {
        $this->dropColumn('{{%user_courses}}', 'is_quick');
        return true;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
