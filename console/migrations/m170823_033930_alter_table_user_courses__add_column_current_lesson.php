<?php

use yii\db\Migration;
// alter table user_courses: add column current lesson

class m170823_033930_alter_table_user_courses__add_column_current_lesson extends Migration
{
    public function up()
    {
        $this->addColumn(
            'user_courses',     // table
            'current_lesson',   // column
            $this->integer(11)
        );
    }

    public function down()
    {
        $this->dropColumn(
            'user_courses',     // table
            'current_lesson'    // column
        );

//        echo "m170823_033930_alter_table_user_courses__add_column_current_lesson cannot be reverted.\n";

//        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
