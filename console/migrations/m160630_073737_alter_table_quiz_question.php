<?php

use yii\db\Migration;

class m160630_073737_alter_table_quiz_question extends Migration
{
    public function up()
    {
        $this->addColumn("{{%quiz_questions}}", 'number_of_dots', $this->smallInteger(2));
    }

    public function down()
    {
        return true;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
