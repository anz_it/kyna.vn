<?php

use yii\db\Migration;
use kyna\gamification\models\UserPointHistory;
use kyna\course\models\QuizSessionAnswer;

class m180802_023247_improve_db_index extends Migration
{
    public function up()
    {
        // Gamification
        $this->createIndex('idx_type_is_added', UserPointHistory::tableName(), ['type', 'is_added']);

        // Quiz
        $this->createIndex('idx_quiz_session_id_parent_id', QuizSessionAnswer::tableName(), ['quiz_session_id', 'parent_id']);
    }

    public function down()
    {
        $this->dropIndex('idx_type_is_added', UserPointHistory::tableName());
        $this->dropIndex('idx_quiz_session_id_parent_id', QuizSessionAnswer::tableName());
    }
}
