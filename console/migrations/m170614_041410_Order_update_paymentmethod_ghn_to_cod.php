<?php

use yii\db\Migration;

class m170614_041410_Order_update_paymentmethod_ghn_to_cod extends Migration
{
    public function up()
    {
        $this->execute("
          UPDATE `orders`
          SET `payment_method` = 'cod', `shipping_method` = 'ghn'
          WHERE `payment_method` = 'ghn';        
        ");

    }

    public function down()
    {
        return true;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
