<?php

use yii\db\Migration;

class m160328_095108_payment_method extends Migration
{
    public function up()
    {
        $this->dropColumn('{{%payment_methods}}', 'payment_vendor_id');
        $this->renameColumn('{{%payment_methods}}', 'method', 'class');
        $this->renameColumn('{{%payment_methods}}', 'is_direct_payment', 'payment_type');
        $this->alterColumn('{{%payment_methods}}', 'payment_type', $this->integer(4));
        $this->alterColumn('{{%payment_methods}}', 'name', $this->string(255));
        $this->addColumn('{{%payment_methods}}', 'content', $this->text());

        return true;
    }

    public function down()
    {
        echo "m160328_095108_payment_method cannot be reverted.\n";

        return true;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
