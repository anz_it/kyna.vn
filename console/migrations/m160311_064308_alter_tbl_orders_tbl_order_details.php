<?php

use yii\db\Migration;

class m160311_064308_alter_tbl_orders_tbl_order_details extends Migration
{
    public function up()
    {
        $this->dropColumn('{{%orders}}', 'discount_amount');
        $this->dropColumn('{{%orders}}', 'subtotal');
        $this->addColumn('{{%orders}}', 'direct_discount_amount', 'double');
        $this->addColumn('{{%orders}}', 'group_discount_amount', 'double');
        $this->dropColumn('{{%order_details}}', 'item_total');
        return true;
    }

    public function down()
    {
        $this->addColumn('{{%orders}}', 'discount_amount', 'double');
        $this->dropColumn('{{%orders}}', 'direct_discount_amount');
        $this->dropColumn('{{%orders}}', 'group_discount_amount');
        $this->addColumn('{{%order_details}}', 'item_total', 'double');
        //echo "m160311_064308_alter_tbl_orders_tbl_order_details cannot be reverted.\n";

        return true;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
