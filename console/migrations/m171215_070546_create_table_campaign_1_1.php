<?php

use yii\db\Migration;

class m171215_070546_create_table_campaign_1_1 extends Migration
{
    public function up()
    {
        $this->createTable('campaign_1_1', [
            'id' => $this->primaryKey(11),
            'course_id' => $this->integer(11)->notNull(),
        ]);

        $this->createIndex('index_campaign_1_1_course_id', 'campaign_1_1','course_id', true);
    }

    public function down()
    {
        $this->dropTable('campaign_1_1');
    }
}
