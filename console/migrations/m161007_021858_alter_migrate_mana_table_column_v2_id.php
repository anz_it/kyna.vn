<?php

use yii\db\Migration;

class m161007_021858_alter_migrate_mana_table_column_v2_id extends Migration
{
    public function up()
    {
        $this->addColumn('{{%mana_certificates}}', 'v2_id', $this->integer(11)->defaultValue(null));
        $this->createIndex('index_v2_id', '{{%mana_certificates}}', 'v2_id');

        $this->addColumn('{{%mana_contacts}}', 'v2_id', $this->integer(11)->defaultValue(null));
        $this->createIndex('index_v2_id', '{{%mana_contacts}}', 'v2_id');

        $this->addColumn('{{%mana_course_subjects}}', 'v2_id', $this->integer(11)->defaultValue(null));
        $this->createIndex('index_v2_id', '{{%mana_course_subjects}}', 'v2_id');

        $this->addColumn('{{%mana_course_teachers}}', 'v2_id', $this->integer(11)->defaultValue(null));
        $this->createIndex('index_v2_id', '{{%mana_course_teachers}}', 'v2_id');

        $this->addColumn('{{%mana_courses}}', 'v2_id', $this->integer(11)->defaultValue(null));
        $this->createIndex('index_v2_id', '{{%mana_courses}}', 'v2_id');

        $this->addColumn('{{%mana_educations}}', 'v2_id', $this->integer(11)->defaultValue(null));
        $this->createIndex('index_v2_id', '{{%mana_educations}}', 'v2_id');

        $this->addColumn('{{%mana_organizations}}', 'v2_id', $this->integer(11)->defaultValue(null));
        $this->createIndex('index_v2_id', '{{%mana_organizations}}', 'v2_id');

        $this->addColumn('{{%mana_subjects}}', 'v2_id', $this->integer(11)->defaultValue(null));
        $this->createIndex('index_v2_id', '{{%mana_subjects}}', 'v2_id');

        $this->addColumn('{{%mana_teachers}}', 'v2_id', $this->integer(11)->defaultValue(null));
        $this->createIndex('index_v2_id', '{{%mana_teachers}}', 'v2_id');
    }

    public function down()
    {
        echo "m161007_021858_alter_migrate_mana_table_column_v2_id cannot be reverted.\n";

        return true;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
