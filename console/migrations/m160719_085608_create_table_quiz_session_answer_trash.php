<?php

use yii\db\Migration;

/**
 * Handles the creation for table `table_quiz_session_answer_trash`.
 */
class m160719_085608_create_table_quiz_session_answer_trash extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('{{%quiz_session_answers_trash}}', [
            'id' => $this->primaryKey(),
            'quiz_session_trash_id'   => $this->integer(11),
            'quiz_detail_id'    => $this->integer(11),
            'answer_id'         => $this->integer(11),
            'answer_text'       => $this->text(),
            'is_correct'        => $this->smallInteger(1),
            'score'             => $this->integer(11),
            'quiz_question_id'  => $this->integer(11),
            'position'          => $this->smallInteger(4),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%quiz_session_answers_trash}}');
        return true;
    }
}
