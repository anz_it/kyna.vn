<?php

use yii\db\Migration;

class m161209_095100_create_tbl_api_credentials extends Migration
{
    public function up()
    {
        $this->createTable('{{api_credentials}}', [
            'id' => $this->primaryKey(),
            'username' => $this->string(255)->notNull(),
            'password_hash' => $this->string(255)->notNull(),
            'auth_key' => $this->text(),
            'access_token' => $this->text(),
            'status' => $this->smallInteger(3)->defaultValue(1),
            'created_time' => $this->integer(10)->defaultValue(0),
            'updated_time' => $this->integer(10)->defaultValue(0),
        ]);

        $this->createIndex('index_unique_username', '{{%api_credentials}}', 'username', true);
    }

    public function down()
    {
        $this->dropTable('{{%api_credentials}}');

        return true;
    }
}
