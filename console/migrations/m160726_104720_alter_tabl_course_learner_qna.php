<?php

use yii\db\Migration;

class m160726_104720_alter_tabl_course_learner_qna extends Migration
{

    public function up()
    {
        $this->alterColumn('{{%course_learner_qna}}', 'is_approved', "BIT(1) DEFAULT 0");
    }

    public function down()
    {
        $this->alterColumn('{{%course_learner_qna}}', 'is_approved', "BIT(1)");
        
        return true;
    }

}
