<?php

use yii\db\Migration;

/**
 * Handles the creation for table `table_mana_course_subjects`.
 */
class m160715_042801_create_table_mana_course_subjects extends \console\components\KynaMigration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        try {
            $this->createTable('{{%mana_course_subjects}}', [
                'id' => $this->primaryKey(),
                'course_id' => $this->bigInteger()->notNull(),
                'subject_id' => $this->bigInteger()->notNull(),
                'created_time' => $this->integer(10)->defaultValue(0),
                'updated_time' => $this->integer(10)->defaultValue(0),
            ], self::$_tableOptions
            );

            $this->createIndex('index_course_id', '{{%mana_course_subjects}}', ['course_id']);
            $this->createIndex('index_course_subject_id', '{{%mana_course_subjects}}', ['subject_id']);
        } catch (yii\console\Exception $ex) {
            echo $ex->getMessage();
            return false;
        }

        return true;
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%mana_course_subjects}}');
        echo "m160715_042801_create_table_mana_course_subjects has been reverted.\n";
        return true;
    }
}
