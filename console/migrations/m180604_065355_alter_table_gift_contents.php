<?php

use yii\db\Migration;

class m180604_065355_alter_table_gift_contents extends Migration
{
    const GIFT_CONTENT_COURSE_TABLE = "{{%gift_contents}}";
    public function up()
    {
        $this->addColumn(self::GIFT_CONTENT_COURSE_TABLE, 'apply_condition', $this->integer());
        $this->addColumn(self::GIFT_CONTENT_COURSE_TABLE, 'apply_all_single_course', $this->integer());
        $this->addColumn(self::GIFT_CONTENT_COURSE_TABLE, 'apply_all_single_course_double', $this->integer());
        $this->addColumn(self::GIFT_CONTENT_COURSE_TABLE, 'apply_all_combo', $this->integer());
        $this->addColumn(self::GIFT_CONTENT_COURSE_TABLE, 'apply_all', $this->integer());
    }

    public function down()
    {
        echo "m180604_065355_alter_table_gift_contents cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
