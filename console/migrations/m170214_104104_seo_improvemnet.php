<?php

use yii\db\Migration;

class m170214_104104_seo_improvemnet extends Migration
{
    public function up()
    {
        $this->execute("
            delete from meta_fields
            where id not in (
                select maxid from 
                (select max(id) as maxid from meta_fields group by `key`, `model`) tmp
        )");

        $this->createIndex("meta_field_model",  "meta_fields", ["model"]);
        $this->createIndex("meta_field_model_key", "meta_fields", ["model", "key"], true);
        $this->execute("
            insert into meta_fields(`key`, `name`, `type`, `model`, `status`)
            values ('seo_title', 'SEO Title', 1, 'course', 1),
            ('seo_description', 'SEO Description', 2, 'course', 1),
            ('seo_keyword', 'SEO keyword', 2, 'course', 1),
            ('seo_facebook_img', 'SEO facebook image', 1, 'course', 1 );
        ");
    }

    public function down()
    {
        echo "m170214_104104_seo_improvemnet cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
