<?php

use yii\db\Migration;

class m170608_043109_alter_table_tags_add_seo_columns extends Migration
{
    public function up()
    {
        $this->addColumn('{{%tags}}', 'seo_title', $this->string(255));
        $this->addColumn('{{%tags}}', 'seo_description', $this->string(255));
        $this->addColumn('{{%tags}}', 'seo_keyword', $this->text());
        $this->addColumn('{{%tags}}', 'seo_robot_index', $this->string(16));
        $this->addColumn('{{%tags}}', 'seo_robot_follow', $this->string(16));
        $this->addColumn('{{%tags}}', 'seo_sitemap', $this->smallInteger(2));
        $this->addColumn('{{%tags}}', 'seo_canonical', $this->string(255));

        $this->execute("
            insert into meta_fields(`key`, `name`, `type`, `model`, `status`)
            values ('seo_anchor_text', 'SEO Anchor Text', 2, 'seo_setting', 1);        
        ");
    }

    public function down()
    {
        $this->execute("
            delete from meta_fields
            where `key` = 'seo_anchor_text';        
        ");
        $this->dropColumn('{{%tags}}', 'seo_canonical');
        $this->dropColumn('{{%tags}}', 'seo_sitemap');
        $this->dropColumn('{{%tags}}', 'seo_robot_follow');
        $this->dropColumn('{{%tags}}', 'seo_robot_index');
        $this->dropColumn('{{%tags}}', 'seo_keyword');
        $this->dropColumn('{{%tags}}', 'seo_description');
        $this->dropColumn('{{%tags}}', 'seo_title');
    }
}
