<?php

use yii\db\Migration;

class m181124_095824_add_column_relation_user_ref_table_user_voucher_free_ref extends Migration
{
    const USER_VOUCHER_FREE_TABLE_REF = "user_voucher_free_ref";
    public function up()
    {
        $this->addColumn(self::USER_VOUCHER_FREE_TABLE_REF,'user_relation_ref', $this->string(255));
    }

    public function down()
    {
        $this->dropColumn(self::USER_VOUCHER_FREE_TABLE_REF,'user_relation_ref');
    }
}
