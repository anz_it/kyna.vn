<?php

use yii\db\Migration;

class m160507_021035_course_learning extends Migration
{
    public function up()
    {
        $this->createTable('user_course_actions', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'user_course_id' => $this->integer(),
            'name' => $this->string(255),
            'action_time' => $this->integer(),
        ]);
        $this->createTable('user_course_action_meta', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'user_course_action_id' => $this->integer(),
            'key' => $this->string(50),
            'value' => $this->text(),
        ]);
        return true;
    }

    public function down()
    {
        echo "m160507_021035_course_learning cannot be reverted.\n";

        return true;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
