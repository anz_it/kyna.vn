<?php

use yii\db\Migration;

class m160622_105106_alter_table_order extends Migration
{
    public function up()
    {
        $this->alterColumn("{{%orders}}", 'promotion_code', $this->string(50));
    }

    public function down()
    {
        return true;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
