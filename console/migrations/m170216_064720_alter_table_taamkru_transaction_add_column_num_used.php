<?php

use yii\db\Migration;

class m170216_064720_alter_table_taamkru_transaction_add_column_num_used extends Migration
{
    public function up()
    {
        $this->addColumn('{{%taamkru_transaction}}', 'num_used', $this->smallInteger(2));
    }

    public function down()
    {
        $this->dropColumn('{{%taamkru_transaction}}', 'num_used');

        return true;
    }
}
