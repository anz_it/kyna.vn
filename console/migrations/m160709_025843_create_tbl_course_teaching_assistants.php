<?php

use yii\db\Migration;

/**
 * Handles the creation for table `tbl_course_teaching_assistants`.
 */
class m160709_025843_create_tbl_course_teaching_assistants extends Migration
{

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('{{%course_teaching_assistants}}', [
            'id' => $this->primaryKey(),
            'course_id' => $this->integer(11)->notNull(),
            'teaching_assistant_id' => $this->integer(11)->notNull(),
            'type' => $this->smallInteger(3)->defaultValue(1),
            'status' => $this->smallInteger(3)->defaultValue(1),
            'created_time' => $this->integer(10),
            'updated_time' => $this->integer(10),
        ]);
        
        $this->createIndex('index_course_id', '{{%course_teaching_assistants}}', 'course_id');
        $this->createIndex('index_teaching_assistant_id', '{{%course_teaching_assistants}}', 'teaching_assistant_id');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('{{%course_teaching_assistants}}');
        
        return true;
    }

}
