<?php

use yii\db\Migration;

class m160508_115102_course_doc extends Migration
{
    public function up()
    {
        $this->createTable('course_documents', [
            'id' => $this->primaryKey(),
            'title' => $this->string(255),
            'save_path' => $this->string(255),
            'course_id' => $this->integer(),
            'file_ext' => $this->string(3),
        ]);

        return true;
    }

    public function down()
    {
        echo "m160508_115102_course_doc cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
