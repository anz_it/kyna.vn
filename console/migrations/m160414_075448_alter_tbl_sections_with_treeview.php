<?php

class m160414_075448_alter_tbl_sections_with_treeview extends console\components\KynaMigration
{

    public function safeUp()
    {
        $this->dropTable("{{%course_sections}}");

        $this->createTable("{{%course_sections}}", [
            'id' => $this->primaryKey(),
            'course_id' => $this->integer(11)->notNull(),
            'name' => $this->string(255)->notNull(),
            'root' => $this->integer(11),
            'lft' => $this->integer(11)->notNull(),
            'rgt' => $this->integer(11)->notNull(),
            'lvl' => $this->smallInteger(5)->notNull(),
            'icon' => $this->string(255),
            'icon_type' => $this->smallInteger(1)->notNull()->defaultValue(1),
            'active' => $this->smallInteger(1)->notNull()->defaultValue(true),
            'selected' => $this->smallInteger(1)->notNull()->defaultValue(false),
            'disabled' => $this->smallInteger(1)->notNull()->defaultValue(false),
            'readonly' => $this->smallInteger(1)->notNull()->defaultValue(false),
            'visible' => $this->smallInteger(1)->notNull()->defaultValue(true),
            'collapsed' => $this->smallInteger(1)->notNull()->defaultValue(false),
            'movable_u' => $this->smallInteger(1)->notNull()->defaultValue(true),
            'movable_d' => $this->smallInteger(1)->notNull()->defaultValue(true),
            'movable_l' => $this->smallInteger(1)->notNull()->defaultValue(true),
            'movable_r' => $this->smallInteger(1)->notNull()->defaultValue(true),
            'removable' => $this->smallInteger(1)->notNull()->defaultValue(true),
            'removable_all' => $this->smallInteger(1)->notNull()->defaultValue(false),
            'status' => $this->smallInteger(3)->defaultValue(1),
            'created_time' => $this->integer(10)->defaultValue(0),
            'updated_time' => $this->integer(10)->defaultValue(0),
        ], self::$_tableOptions);

        $this->createIndex("index_course_id", "{{%course_sections}}", 'course_id');
        $this->createIndex("index_root", "{{%course_sections}}", 'root');
        $this->createIndex("index_lft", "{{%course_sections}}", 'lft');
        $this->createIndex("index_rgt", "{{%course_sections}}", 'rgt');
        $this->createIndex("index_lvl", "{{%course_sections}}", 'lvl');
        $this->createIndex("index_active", "{{%course_sections}}", 'active');

        return true;
    }

    public function safeDown()
    {
        $this->dropTable("{{%course_sections}}");

        $this->createTable('{{%course_sections}}', [
                'id' => $this->primaryKey(),
                'course_id' => $this->integer(11)->notNull(),
                'name' => $this->string(255)->notNull(),
                'type' => "enum('chapter', 'section') NOT NULL COMMENT 'Chương hoac Phần'",
                'parent_id' => $this->integer(11),
                'description' => $this->text(),
                'order' => $this->integer(3)->defaultValue(0),
                'status' => $this->smallInteger(3)->defaultValue(1),
                'is_deleted' => "bit(1) DEFAULT b'0'",
                'created_time' => $this->integer(10)->defaultValue(0),
                'updated_time' => $this->integer(10)->defaultValue(0),
            ], self::$_tableOptions);

        $this->createIndex('index_course_id', '{{%course_sections}}', ['course_id']);
        $this->createIndex('index_parent_id', '{{%course_sections}}', ['parent_id']);

        echo "m160414_075448_alter_tbl_sections_with_treeview has been reverted.\n";

        return true;
    }

}
