<?php

use yii\db\Migration;

class m160325_183744_update_payment_method_id_to_payment_method extends Migration
{
    public function up()
    {
        $this->renameColumn('{{%orders}}', 'payment_method_id', 'payment_method');
        $this->alterColumn('{{%orders}}', 'payment_method', $this->string(20));
        return true;
    }

    public function down()
    {
        echo "m160325_183744_update_payment_method_id_to_payment_method cannot be reverted.\n";

        return true;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
