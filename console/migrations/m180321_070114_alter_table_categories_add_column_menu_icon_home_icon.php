<?php

use yii\db\Migration;

class m180321_070114_alter_table_categories_add_column_menu_icon_home_icon extends Migration
{
    public function up()
    {
        $this->addColumn('categories', 'menu_icon', $this->string(255));
        $this->addColumn('categories', 'home_icon', $this->string(255));
    }

    public function down()
    {
        $this->dropColumn('categories', 'home_icon');
        $this->dropColumn('categories', 'menu_icon');
    }
}
