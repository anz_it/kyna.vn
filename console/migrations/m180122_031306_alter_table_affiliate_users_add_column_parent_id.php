<?php

use yii\db\Migration;

class m180122_031306_alter_table_affiliate_users_add_column_parent_id extends Migration
{
    public function up()
    {
        $this->addColumn(
            'affiliate_users',
            'manager_id',
            $this->integer(11)->comment('id of parent affiliate')
        );
        $this->addColumn(
            'affiliate_users',
            'is_manager',
            $this->integer(11)->defaultValue(0)->comment('id of manager affiliate')
        );
    }

    public function down()
    {
        $this->dropColumn('affiliate_users', 'is_manager');
        $this->dropColumn('affiliate_users', 'manager_id');
    }
}
