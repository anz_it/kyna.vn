<?php

use yii\db\Migration;

class m160907_065540_create_table_page extends Migration
{
    public function up()
    {
        $this->createTable('{{%page}}',[
            'id'    =>  $this->primaryKey(),
            'title' => $this->string(),
            'slug' => $this->string(),
            'is_hidden' => "bit(1) DEFAULT b'0'",
            'external_script'           => $this->text(),
            'success_script'         => $this->text()            
        ]);
        // tạo index
        $this->createIndex(
            'idx-page-slug',
            'page',
            'slug'
        );
    }

    public function down()
    {
        $this->dropTable('{{%page}}');
        return true;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
