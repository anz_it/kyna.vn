<?php

use yii\db\Migration;

class m161209_035246_create_table_career extends Migration
{
    public function up()
    {
        $this->createTable('{{%career}}', [
            'id' => $this->primaryKey(),
            'title' => $this->string()->notNull(),
            'short_title' => $this->string()->notNull(),
            'slug' => $this->string()->notNull(),
            'category_id' => $this->integer(11)->notNull(),
            'content' => $this->text(),
            'quantity' => $this->integer(4)->notNull(),
            'language_id' => $this->integer(11)->notNull(),
            'publish_date' => $this->integer(10)->notNull(),
            'end_date' => $this->integer(10)->notNull(),
            'status' => $this->smallInteger(1)->defaultValue(0),
            'created_time' => $this->integer(10)->defaultValue(0),
            'updated_time' => $this->integer(10)->defaultValue(0),
        ]);

        $this->createIndex('idx_career_slug', '{{%career}}', 'slug');
    }

    public function down()
    {
        $this->dropIndex('idx_career_slug', '{{%career}}');

        $this->dropTable('{{%career}}');

        return true;
    }
}
