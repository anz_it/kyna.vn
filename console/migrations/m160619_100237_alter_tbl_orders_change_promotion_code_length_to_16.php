<?php

use yii\db\Migration;

class m160619_100237_alter_tbl_orders_change_promotion_code_length_to_16 extends Migration
{

    public function safeUp()
    {
        $this->alterColumn('{{%orders}}', 'promotion_code', $this->string(16));
    }

    public function safeDown()
    {
        $this->alterColumn('{{%orders}}', 'promotion_code', $this->string(10));
        
        return true;
    }

}
