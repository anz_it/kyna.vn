<?php

use yii\db\Migration;

class m180227_034133_alter_table_order_add_column_is_4kid extends Migration
{
    public function up()
    {
        $this->addColumn(
            'orders',
            'is_4kid',
            $this->smallInteger(1).' DEFAULT 0');
    }

    public function down()
    {
        $this->dropColumn('orders', 'is_4kid');
        echo "m180227_034133_alter_table_order_add_column_is_4kid cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
