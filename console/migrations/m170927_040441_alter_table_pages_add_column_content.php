<?php

use yii\db\Migration;
use kyna\page\models\Page;

class m170927_040441_alter_table_pages_add_column_content extends Migration
{
    public function up()
    {
        $pageTblName = Page::tableName();

        $this->addColumn($pageTblName, 'content', "MEDIUMTEXT");
    }

    public function down()
    {
        $pageTblName = Page::tableName();

        $this->dropColumn($pageTblName, 'content');
    }
}
