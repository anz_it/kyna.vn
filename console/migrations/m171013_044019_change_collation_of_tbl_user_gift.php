<?php

use yii\db\Migration;

class m171013_044019_change_collation_of_tbl_user_gift extends Migration
{
    public function up()
    {
        $this->execute('alter table user_gifts convert to character set utf8 collate utf8_unicode_ci;');
    }

    public function down()
    {
        return true;
    }

}
