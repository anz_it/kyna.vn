<?php

use yii\db\Migration;

/**
 * Handles the creation for table `table_mana_courses`.
 */
class m160715_035815_create_table_mana_courses extends \console\components\KynaMigration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        try {
            $this->createTable('{{%mana_courses}}', [
                'id' => $this->primaryKey(),
                'course_id' => $this->bigInteger()->notNull(),
                'course_ref_id' => $this->bigInteger()->defaultValue(0),
                'education_id' => $this->bigInteger()->notNull(),
                'certificate_id' => $this->bigInteger()->notNull(),
                'organization_id' => $this->bigInteger()->notNull(),
                'description_for' => $this->text(),
                'condition' => $this->text(),
                'learning_time' => $this->text(),
                'learning_method' => $this->text(),
                'learning_start_date' => $this->text(),
                'order' => $this->integer(11)->defaultValue(0),
                'status' => $this->smallInteger(3)->defaultValue(1),
                'created_time' => $this->integer(10)->defaultValue(0),
                'updated_time' => $this->integer(10)->defaultValue(0),
            ], self::$_tableOptions
            );

            $this->createIndex('index_course_id', '{{%mana_courses}}', ['course_id']);
            $this->createIndex('index_education_id', '{{%mana_courses}}', ['education_id']);
            $this->createIndex('index_certificate_id', '{{%mana_courses}}', ['certificate_id']);
            $this->createIndex('index_organization_id', '{{%mana_courses}}', ['organization_id']);
        } catch (yii\console\Exception $ex) {
            echo $ex->getMessage();
            return false;
        }

        return true;
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%mana_courses}}');
        echo "m160715_035815_create_table_mana_courses has been reverted.\n";
        return true;
    }
}
