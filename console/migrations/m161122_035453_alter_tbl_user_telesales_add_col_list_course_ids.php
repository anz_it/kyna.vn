<?php

use yii\db\Migration;

class m161122_035453_alter_tbl_user_telesales_add_col_list_course_ids extends Migration
{
    public function up()
    {
        $this->addColumn('{{%user_telesales}}', 'list_course_ids', $this->text());
    }

    public function down()
    {
        $this->dropColumn('{{%user_telesales}}', 'list_course_ids');

        return true;
    }

}
