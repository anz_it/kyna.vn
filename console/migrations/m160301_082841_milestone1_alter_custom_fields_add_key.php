<?php

use yii\db\Migration;

class m160301_082841_milestone1_alter_custom_fields_add_key extends Migration
{
    public function up()
    {
        $this->addColumn('{{%custom_fields}}', 'key', $this->string(50)->notNull() . " AFTER id");
    }

    public function down()
    {
        $this->dropColumn('{{%custom_fields}}', 'key');
        
        echo "m160301_082841_milestone1_alter_custom_fields_add_key has been reverted.\n";

        return true;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
