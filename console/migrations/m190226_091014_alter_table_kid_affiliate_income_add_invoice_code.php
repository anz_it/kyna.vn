<?php

use yii\db\Migration;

class m190226_091014_alter_table_kid_affiliate_income_add_invoice_code extends Migration
{
    const TABLE_KID_AFFILIATE_INCOME = 'kid_affiliate_income';
    public function up()
    {
        $this->addColumn(self::TABLE_KID_AFFILIATE_INCOME, 'invoice_code', $this->string(255)->after('order_id'));
    }

    public function down()
    {
        echo "m190226_091014_alter_table_kid_affiliate_income_add_invoice_code cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
