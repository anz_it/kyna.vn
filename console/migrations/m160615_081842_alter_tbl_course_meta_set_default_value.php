<?php

use yii\db\Migration;

class m160615_081842_alter_tbl_course_meta_set_default_value extends Migration
{

    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->alterColumn('{{%course_meta}}', 'meta_field_id', $this->integer(11)->defaultValue(0));
    }

    public function safeDown()
    {
        return true;
    }

}
