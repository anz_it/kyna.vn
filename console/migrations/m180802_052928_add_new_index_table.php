<?php

use yii\db\Migration;
use kyna\user\models\UserCourse;
use kyna\user\models\User;

class m180802_052928_add_new_index_table extends Migration
{
    public function up()
    {
        $this->createIndex('idx_user_id_is_deleted', UserCourse::tableName(), ['user_id', 'is_deleted']);
        $this->createIndex('idx_is_deleted', User::tableName(), ['is_deleted']);

    }

    public function down()
    {
        $this->dropIndex('idx_user_id_is_deleted', UserCourse::tableName());
        $this->dropIndex('idx_is_deleted', User::tableName());
    }
}
