<?php

use yii\db\Migration;

class m171106_080844_add_Tiki_shipping extends Migration
{
    public function up()
    {
        $this->insert(
            'vendors', [
            'name' => 'Tiki',
            'vendor_type' => 2,
            'status' => 1,
            'alias' => 'tiki',
            'created_time' => time(),
            'updated_time' => time(),
        ]);
    }

    public function down()
    {
        $this->delete('vendors', [
            'alias' => 'tiki',
        ]);
    }
}
