<?php

use console\components\KynaMigration;

class m160227_041249_milestone1_create_wallet extends KynaMigration
{

    public function up()
    {
        try {
            $this->createTable('{{%wallets}}', [
                'id' => $this->primaryKey(),
                'user_id' => $this->integer(11)->notNull(),
                'balance' => $this->double()->defaultValue(0),
                'last_transaction_id' => $this->integer(11)->defaultValue(0),
                'status' => $this->smallInteger(3)->defaultValue(1),
                'created_time' => $this->integer(10)->defaultValue(0),
                'updated_time' => $this->integer(10)->defaultValue(0),
                    ], self::$_tableOptions
            );

            $this->createIndex('index_user_id', '{{%wallets}}', ['user_id']);
            $this->createIndex('index_last_transaction_id', '{{%wallets}}', ['last_transaction_id']);
        } catch (yii\console\Exception $ex) {
            echo $ex->getMessage();
            return false;
        }

        return true;
    }

    public function down()
    {
        $this->dropIndex('index_last_transaction_id', '{{%wallets}}');
        $this->dropIndex('index_user_id', '{{%wallets}}');
            
        $this->dropTable('{{%wallets}}');
        
        echo "m160227_041249_milestone1_create_wallet has been reverted.\n";

        return true;
    }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
