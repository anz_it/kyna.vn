<?php

use yii\db\Migration;

class m170927_031123_app_access_token extends Migration
{
    public function up()
    {

        $this->addColumn("user", "access_token", "text");
    }

    public function down()
    {
        echo "m170927_031123_app_access_token cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
