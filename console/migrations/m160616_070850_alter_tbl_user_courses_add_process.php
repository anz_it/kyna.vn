<?php

use yii\db\Migration;

class m160616_070850_alter_tbl_user_courses_add_process extends Migration
{

    public function safeUp()
    {
        $this->addColumn('{{%user_courses}}', 'process', 'INT(11) DEFAULT 0 AFTER course_id');
        $this->addColumn('{{%user_courses}}', 'started_date', 'INT(10) DEFAULT 0 AFTER is_started');
    }

    public function safeDown()
    {
        $this->dropColumn('{{%user_courses}}', 'started_date');
        $this->dropColumn('{{%user_courses}}', 'process');
        return true;
    }

}
