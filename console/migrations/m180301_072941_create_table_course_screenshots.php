<?php

use yii\db\Migration;

class m180301_072941_create_table_course_screenshots extends Migration
{
    public function up()
    {
        $this->createTable('course_screenshots', [
            'id'=> $this->primaryKey(),
            'course_id' => $this->integer()->notNull(),
            'description'=>$this->text()->notNull(),
            'image_url' => $this->text(),
            'status' => $this->smallInteger(1)->defaultValue(1),
            'is_deleted' => $this->smallInteger(1)->defaultValue(0),
        ]);

        $this->createIndex('idx_course_screenshot_course_id','course_screenshots', 'course_id');
    }

    public function down()
    {
        $this->dropIndex('idx_course_screenshot_course_id', 'course_screenshots');
        $this->dropTable('course_screenshots');
    }
}
