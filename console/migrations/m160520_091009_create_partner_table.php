<?php

use yii\db\Migration;

/**
 * Handles the creation for table `partner_table`.
 */
class m160520_091009_create_partner_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('{{%partners}}', [
            'id' => $this->primaryKey(),
            'partner_code' => $this->string(32),
            'partner_name'  => $this->string(120),
            'status'                => $this->smallInteger(3)->defaultValue(1),
            'is_deleted'            => "bit(1) DEFAULT b'0'",
            'created_time'          => $this->integer(10)->defaultValue(0),
            'updated_time'          => $this->integer(10)->defaultValue(0),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%partners}}');
        
        return true;
    }
}
