<?php

use yii\db\Migration;

class m160329_071253_add_tbl_order_meta extends \console\components\KynaMigration
{

    public function up()
    {
        $this->createTable('{{%order_meta}}', [
                'id' => $this->primaryKey(),
                'order_id' => $this->integer(11)->defaultValue(0),
                'meta_field_id' => $this->integer(11)->defaultValue(0),
                'key' => $this->string(50),
                'value' => $this->text(),
            ], self::$_tableOptions
        );
        
        $this->createIndex('index_order_id', '{{%order_meta}}', 'order_id');
        $this->createIndex('index_meta_field_id', '{{%order_meta}}', 'meta_field_id');
    }

    public function down()
    {
        $this->dropTable('{{%order_meta}}');
        echo "m160329_071253_add_tbl_order_meta has been reverted.\n";

        return true;
    }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
