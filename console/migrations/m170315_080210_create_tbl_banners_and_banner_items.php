<?php

use yii\db\Migration;

class m170315_080210_create_tbl_banners_and_banner_items extends Migration
{
    public function up()
    {
        $this->createTable('{{%banners}}', [
            'id' => $this->primaryKey(11),
            'title' => $this->string(50)->notNull(),
            'description' => $this->text(),
            'type' => $this->smallInteger(3)->notNull(),
            'link' => $this->text(),
            'image_url' => $this->text(),
            'category_id' => $this->integer(10),
            'from_date' => $this->date(),
            'to_date' => $this->date(),
            'status' => $this->smallInteger(1)->defaultValue(1),
            'created_time' => $this->integer(10),
            'updated_time' => $this->integer(10)
        ]);

        $this->createIndex('index_type', '{{%banners}}', 'type');
        $this->createIndex('index_category_id', '{{%banners}}', 'category_id');
        $this->createIndex('index_from_date', '{{%banners}}', 'from_date');
        $this->createIndex('index_to_date', '{{%banners}}', 'to_date');

        $this->createTable('{{%banner_groups}}', [
            'id' => $this->primaryKey(11),
            'title' => $this->string(50)->notNull(),
            'description' => $this->text(),
            'type' => $this->smallInteger(3)->notNull(),
            'from_date' => $this->date(),
            'to_date' => $this->date(),
            'status' => $this->smallInteger(1)->defaultValue(1),
            'created_time' => $this->integer(10),
            'updated_time' => $this->integer(10)
        ]);

        $this->createIndex('index_from_date', '{{%banner_groups}}', 'from_date');
        $this->createIndex('index_to_date', '{{%banner_groups}}', 'to_date');

        $this->createTable('{{%banner_group_items}}', [
            'id' => $this->primaryKey(11),
            'banner_group_id' => $this->integer(11)->notNull(),
            'banner_id' => $this->integer(11)->notNull(),
            'position' => $this->string(20),
        ]);

        $this->createIndex('index_banner_group_id', '{{%banner_group_items}}', 'banner_group_id');
        $this->createIndex('index_banner_banner_id', '{{%banner_group_items}}', 'banner_id');
    }

    public function down()
    {
        $this->dropTable('{{%banner_group_items}}');
        $this->dropTable('{{%banner_groups}}');

        $this->dropTable('{{%banners}}');
    }
}
