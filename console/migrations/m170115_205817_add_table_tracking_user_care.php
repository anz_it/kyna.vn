<?php

use yii\db\Migration;

class m170115_205817_add_table_tracking_user_care extends Migration
{
    public function up()
    {
        $this->execute("
            CREATE TABLE `tracking_user_care` (
              `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
              `user_id` int(11) NOT NULL,
              `last_assign` int(11) DEFAULT NULL,
              PRIMARY KEY (`id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
            
            CREATE TABLE `tracking_settings` (
              `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
              `key` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
              `value` text COLLATE utf8_unicode_ci,
              PRIMARY KEY (`id`),
              UNIQUE KEY `key` (`key`)
            ) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
            
            /*!40000 ALTER TABLE `tracking_settings` DISABLE KEYS */;

            INSERT INTO `tracking_settings` (`id`, `key`, `value`)
            VALUES
                (1,'auto_assign','0'),
                (2,'last_assign_id','0'),
                (3,'schedule','1 hour'),
                (4,'last_run','0');
            
            /*!40000 ALTER TABLE `tracking_settings` ENABLE KEYS */;
            
            "
        );
    }

    public function down()
    {
        echo "m170115_205817_add_table_tracking_user_care cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
