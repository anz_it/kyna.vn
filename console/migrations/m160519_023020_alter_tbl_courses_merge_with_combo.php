<?php

use yii\db\Migration;

class m160519_023020_alter_tbl_courses_merge_with_combo extends Migration
{

    public function safeUp()
    {
        $this->addColumn('{{%courses}}', 'type', "TINYINT(3) default 1 COMMENT '1: single, 2: combo' after `level`");
        $this->addColumn('{{%courses}}', 'position', 'SMALLINT(3) default 0 after teacher_id');
        
        $this->createIndex('index_type', '{{%courses}}', 'type');
        
        $this->renameColumn('{{%course_combo_items}}', 'order', 'position');
        
        $this->dropTable('{{%course_combos}}');
    }

    public function safeDown()
    {
        $this->renameColumn('{{%course_combo_items}}', 'position', 'order');
        
        $this->dropIndex('index_type', '{{%courses}}');
        
        $this->dropColumn('{{%courses}}', 'position');
        $this->dropColumn('{{%courses}}', 'type');

        return true;
    }

}
