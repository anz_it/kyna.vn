<?php

use yii\db\Migration;

/**
 * Handles the creation for table `tbl_affiliates`.
 */
class m160531_044757_create_tbl_affiliates extends Migration
{

    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTableAffiliateCategories();

        $this->createTableAffiliateUsers();
        
        $this->createTableCommissionCalculations();
        
        $this->createTableAffiliateCommissions();
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%affiliate_commissions}}');
        
        $this->dropTable('{{%affiliate_calculations}}');
        
        $this->dropTable('{{%affiliate_user_rangers}}');
        
        $this->dropTable('{{%affiliate_users}}');
        
        $this->dropTable('{{%affiliate_categories}}');
        
        return true;
    }
    
    private function createTableAffiliateCategories()
    {
        $this->createTable('{{%affiliate_categories}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(50)->notNull(),
            'group' => $this->smallInteger(3)->notNull(),
            'is_override_commission' => "BIT(1) default 0",
            'commission_percent' => $this->float(2)->defaultValue(0),
            'default_cookie_day' => $this->integer(5)->defaultValue(0),
            'is_deleted' => "BIT(1) default 0",
            'status' => $this->smallInteger(3)->defaultValue(1),
            'created_time' => $this->integer(10)->defaultValue(0),
            'updated_time' => $this->integer(10)->defaultValue(0),
        ]);
    }
    
    private function createTableAffiliateUsers()
    {
        $this->createTable('{{%affiliate_users}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(11)->notNull(),
            'affiliate_category_id' => $this->integer(11)->notNull(),
            'is_override_commission' => "BIT(1) default 0",
            'commission_percent' => $this->float(2)->defaultValue(0),
            'cookie_day' => $this->integer(5)->defaultValue(0),
            'is_deleted' => "BIT(1) default 0",
            'status' => $this->smallInteger(3)->defaultValue(1),
            'created_time' => $this->integer(10)->defaultValue(0),
            'updated_time' => $this->integer(10)->defaultValue(0),
        ]);
        
        $this->createIndex('index_user_id', '{{%affiliate_users}}', 'user_id');
        $this->createIndex('index_affiliate_category_id', '{{%affiliate_users}}', 'affiliate_category_id');
        
        $this->createTable('{{%affiliate_user_rangers}}', [
            'id' => $this->primaryKey(),
            'affiliate_user_id' => $this->integer(11)->notNull(),
            'commission_percent' => $this->float(2)->defaultValue(0),
            'cookie_day' => $this->integer(5)->defaultValue(0),
            'start_date' => $this->integer(10)->defaultValue(0),
            'end_date' => $this->integer(10)->defaultValue(0),
            'status' => $this->smallInteger(3)->defaultValue(1),
            'created_time' => $this->integer(10)->defaultValue(0),
            'updated_time' => $this->integer(10)->defaultValue(0),
        ]);
        
        $this->createIndex('index_affiliate_user_id', '{{%affiliate_user_rangers}}', 'affiliate_user_id');
        $this->createIndex('index_start_date', '{{%affiliate_user_rangers}}', 'start_date');
        $this->createIndex('index_end_date', '{{%affiliate_user_rangers}}', 'end_date');
    }
    
    private function createTableCommissionCalculations()
    {
        $this->createTable('{{%affiliate_calculations}}', [
            'id' => $this->primaryKey(),
            'group' => $this->smallInteger(3)->notNull(),
            'instuctor_percent' => $this->float(2)->notNull(),
            'affiliate_percent' => $this->float(2)->notNull(),
            'start_date' => $this->integer(10)->defaultValue(0),
            'end_date' => $this->integer(10)->defaultValue(0),
            'is_deleted' => "BIT(1) default 0",
            'status' => $this->smallInteger(3)->defaultValue(1),
            'created_time' => $this->integer(10)->defaultValue(0),
            'updated_time' => $this->integer(10)->defaultValue(0),
        ]);
        
        $this->createIndex('index_group', '{{%affiliate_calculations}}', 'group');
        $this->createIndex('index_start_date', '{{%affiliate_calculations}}', 'start_date');
        $this->createIndex('index_end_date', '{{%affiliate_calculations}}', 'end_date');
    }
    
    private function createTableAffiliateCommissions()
    {
        $this->createTable('{{%affiliate_commissions}}', [
            'id' => $this->primaryKey(),
            'order_id' => $this->integer(11)->defaultValue(0),
            'user_id' => $this->integer(11)->notNull(),
            'course_id' => $this->integer(11)->notNull(),
            'affiliate_calculation_id' => $this->integer(11)->defaultValue(0),
            'commission_amount' => $this->integer(10)->defaultValue(0),
            'date' => $this->integer(10)->defaultValue(0)
        ]);
        
        $this->createIndex('index_order_id', '{{%affiliate_commissions}}', 'order_id');
        $this->createIndex('index_user_id', '{{%affiliate_commissions}}', 'user_id');
        $this->createIndex('index_course_id', '{{%affiliate_commissions}}', 'course_id');
        $this->createIndex('index_date', '{{%affiliate_commissions}}', 'date');
    }

}
