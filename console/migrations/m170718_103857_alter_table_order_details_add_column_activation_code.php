<?php

use yii\db\Migration;
use kyna\order\models\OrderDetails;

class m170718_103857_alter_table_order_details_add_column_activation_code extends Migration
{
    public function up()
    {
        $orderDetailsTblName = OrderDetails::tableName();

        $this->addColumn($orderDetailsTblName, 'activation_code', $this->string(50));
        $this->createIndex('idx_activation_code', $orderDetailsTblName, 'activation_code');
    }

    public function down()
    {
        $orderDetailsTblName = OrderDetails::tableName();

        $this->dropIndex('idx_activation_code', $orderDetailsTblName);
        $this->dropColumn($orderDetailsTblName, 'activation_code');
    }
}
