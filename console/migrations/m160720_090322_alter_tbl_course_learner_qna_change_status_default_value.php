<?php

use yii\db\Migration;

class m160720_090322_alter_tbl_course_learner_qna_change_status_default_value extends Migration
{

    public function safeUp()
    {
        $this->alterColumn('{{%course_learner_qna}}', 'status', $this->smallInteger(3)->defaultValue(1));
    }

    public function safeDown()
    {
        $this->alterColumn('{{%course_learner_qna}}', 'status', $this->smallInteger(3)->defaultValue(0));

        return true;
    }

}
