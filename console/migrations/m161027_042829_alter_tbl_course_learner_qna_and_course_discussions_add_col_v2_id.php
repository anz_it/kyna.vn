<?php

use yii\db\Migration;

class m161027_042829_alter_tbl_course_learner_qna_and_course_discussions_add_col_v2_id extends Migration
{

    public function up()
    {
        $this->addColumn('{{%course_learner_qna}}', 'v2_id', $this->integer(11));
        
        $this->addColumn('{{%course_discussions}}', 'v2_id', $this->integer(11));
        
        $this->addColumn('{{%course_discussions}}', 'comment_to_user_id', "INT(11) AFTER `comment`");
    }

    public function down()
    {
        $this->dropColumn('{{%course_discussions}}', 'comment_to_user_id');
        
        $this->dropColumn('{{%course_discussions}}', 'v2_id');
        
        $this->dropColumn('{{%course_learner_qna}}', 'v2_id');

        return true;
    }

}
