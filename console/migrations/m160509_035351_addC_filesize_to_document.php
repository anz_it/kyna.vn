<?php

use yii\db\Migration;

class m160509_035351_addC_filesize_to_document extends Migration
{
    public function up()
    {
        $this->addColumn('course_documents', 'size', $this->integer()->unsigned());
        $this->addColumn('course_documents', 'mime_type', $this->string(127));
        return true;
    }

    public function down()
    {
        echo "m160509_035351_addC_filesize_to_document cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
