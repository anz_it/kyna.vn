<?php

use yii\db\Migration;

class m160620_110925_alter_tbl_commission_incomes_set_money_field_to_double extends Migration
{

    public function safeUp()
    {
        $this->alterColumn('{{%commission_incomes}}', 'affiliate_commission_amount', $this->double()->defaultValue(0));
        $this->alterColumn('{{%commission_incomes}}', 'teacher_commission_amount', $this->double()->defaultValue(0));
        $this->alterColumn('{{%commission_incomes}}', 'kyna_commission_amount', $this->double()->defaultValue(0));
    }

    public function safeDown()
    {
        return true;
    }

}
