<?php

use yii\db\Migration;

class m170805_064543_alter_table_notification_edit_datatype extends Migration
{
    public function up()
    {
        $this->alterColumn(
            'notification',
            'title',
            'text collate utf8mb4_unicode_ci'
        );
        $this->alterColumn(
            'notification',
            'body',
            'text collate utf8mb4_unicode_ci'
        );
        $this->alterColumn(
            'notification',
            'description',
            'text collate utf8mb4_unicode_ci'
        );
    }

    public function down()
    {
        echo "m170805_064543_alter_table_notification_edit_datatype cannot be reverted.\n";

        return false;
//        return true;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
