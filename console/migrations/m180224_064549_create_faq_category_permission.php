<?php

use yii\db\Migration;

/**
 * Class m180224_064549_create_faq_category_permission
 */
class m180224_064549_create_faq_category_permission extends Migration
{
    public function safeUp()
    {
        $auth = Yii::$app->authManager;

        //=============================================================================
        // add "Faq.Category.View" permission
        $viewFaqCategory = $auth->createPermission('Faq.Category.View');
        $viewFaqCategory->description = 'FAQ Category View';
        $auth->add($viewFaqCategory);

        // add "Faq.Category.Create" permission
        $createFaqCategory = $auth->createPermission('Faq.Category.Create');
        $createFaqCategory->description = 'FAQ Category Create';
        $auth->add($createFaqCategory);
        $auth->addChild($createFaqCategory, $viewFaqCategory);

        // add "Faq.Category.Update" permission
        $updateFaqCategory = $auth->createPermission('Faq.Category.Update');
        $updateFaqCategory->description = 'FAQ Category Update';
        $auth->add($updateFaqCategory);
        $auth->addChild($updateFaqCategory, $viewFaqCategory);

        // add "Faq.Category.Delete" permission
        $deleteFaqCategory = $auth->createPermission('Faq.Category.Delete');
        $deleteFaqCategory->description = 'FAQ Category Delete';
        $auth->add($deleteFaqCategory);
        $auth->addChild($deleteFaqCategory, $viewFaqCategory);
        $auth->addChild($deleteFaqCategory, $updateFaqCategory);

        // add "Faq.Category.All" permission
        $allFaqCategory = $auth->createPermission('Faq.Category.All');
        $allFaqCategory->description = 'FAQ Category All';
        $auth->add($allFaqCategory);
        $auth->addChild($allFaqCategory, $viewFaqCategory);
        $auth->addChild($allFaqCategory, $createFaqCategory);
        $auth->addChild($allFaqCategory, $updateFaqCategory);
        $auth->addChild($allFaqCategory, $deleteFaqCategory);
        //=============================================================================

        // add "faqcatAdmin" role
        $faqCatAdmin = $auth->createRole('FaqCatAdmin');
        $faqCatAdmin->description = 'The admin of a Faq Category module';
        $auth->add($faqCatAdmin);
        $auth->addChild($faqCatAdmin, $allFaqCategory);

        // add permission for Admin role
        $admin = $auth->getRole('Admin');
        $auth->addChild($admin, $faqCatAdmin);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $auth = Yii::$app->authManager;

        $faqCatAdmin = $auth->getRole('FaqCatAdmin');
        $admin = $auth->getRole('Admin');
        $auth->removeChild($admin, $faqCatAdmin);
        $auth->remove($faqCatAdmin);

        $allFaqCategory = $auth->getPermission('Faq.Category.All');
        $deleteFaqCategory = $auth->getPermission('Faq.Category.Delete');
        $updateFaqCategory = $auth->getPermission('Faq.Category.Update');
        $createFaqCategory = $auth->getPermission('Faq.Category.Create');
        $viewFaqCategory = $auth->getPermission('Faq.Category.View');
        $auth->remove($allFaqCategory);
        $auth->remove($deleteFaqCategory);
        $auth->remove($updateFaqCategory);
        $auth->remove($createFaqCategory);
        $auth->remove($viewFaqCategory);
    }
}
