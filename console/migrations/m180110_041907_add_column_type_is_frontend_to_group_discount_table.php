<?php

use yii\db\Migration;

class m180110_041907_add_column_type_is_frontend_to_group_discount_table extends Migration
{
    public function up()
    {
        $this->addColumn('group_discounts', 'type', "SMALLINT(1) DEFAULT 0 AFTER course_quantity ");
        $this->addColumn('group_discounts', 'is_frontend', "SMALLINT(1) DEFAULT 1 AFTER type ");
    }

    public function down()
    {
        echo "m180110_041907_add_column_type_is_frontend_to_group_discount_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
