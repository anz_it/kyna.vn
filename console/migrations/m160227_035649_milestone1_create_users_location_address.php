<?php

use console\components\KynaMigration;

class m160227_035649_milestone1_create_users_location_address extends KynaMigration
{

    public function up()
    {
        // create table `locations`
        $this->createTableLocation();
        
        // create table `user_addresses`
        $this->createTableUserAddress();
    }

    public function down()
    {
        $this->dropTableUserAddress();

        $this->dropTableLocation();
        
        echo "m160227_035649_milestone1_create_users_location_address has been reverted.\n";

        return true;
    }
    
    private function createTableLocation()
    {
        try {
            $this->createTable('{{%locations}}', [
                'id' => $this->primaryKey(),
                'name' => $this->string(255)->notNull(),
                'parent_id' => $this->integer(11)->defaultValue(0) . " COMMENT 'Self relation table. City is level 0, district is level 1'",
                'status' => $this->smallInteger(3)->defaultValue(1),
                'created_time' => $this->integer(10)->defaultValue(0),
                'updated_time' => $this->integer(10)->defaultValue(0),
                    ], self::$_tableOptions
            );

            $this->createIndex('index_parent_id', '{{%locations}}', ['parent_id']);
            $this->createIndex('index_name', '{{%locations}}', ['name']);
        } catch (yii\console\Exception $ex) {
            echo $ex->getMessage();
            return false;
        }
        
        return true;
    }
    
    private function dropTableLocation()
    {
        try {
            $this->dropIndex('index_name', '{{%locations}}');
            $this->dropIndex('index_parent_id', '{{%locations}}');

            $this->dropTable('{{%locations}}');
        } catch (yii\console\Exception $ex) {
            echo $ex->getMessage();
            return false;
        }
        
        return true;
    }
    
    private function createTableUserAddress()
    {
        try {
            $this->createTable('{{%user_addresses}}', [
                'id' => $this->primaryKey(),
                'user_id' => $this->integer(11)->notNull(),
                'contact_name' => $this->string(255)->notNull(),
                'email' => $this->string(255)->notNull(),
                'phone_number' => $this->string(255)->notNull(),
                'street_address' => $this->string(255),
                'location_id' => $this->integer(11)->defaultValue(0),
                'status' => $this->smallInteger(3)->defaultValue(1),
                'created_time' => $this->integer(10)->defaultValue(0),
                'updated_time' => $this->integer(10)->defaultValue(0),
                    ], self::$_tableOptions
            );

            $this->createIndex('index_user_id', '{{%user_addresses}}', ['user_id']);
            $this->createIndex('index_location_id', '{{%user_addresses}}', ['location_id']);
        } catch (yii\console\Exception $ex) {
            echo $ex->getMessage();
            return false;
        }
        
        return true;
    }
    
    private function dropTableUserAddress()
    {
        try {
            $this->dropIndex('index_location_id', '{{%user_addresses}}');
            $this->dropIndex('index_user_id', '{{%user_addresses}}');

            $this->dropTable('{{%user_addresses}}');
        } catch (yii\console\Exception $ex) {
            echo $ex->getMessage();
            return false;
        }
        
        return true;
    }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
