<?php

use yii\db\Migration;

class m170830_033802_create_table_teacher_tags extends Migration
{
    public function up()
    {
        $this->createTable('{{%teacher_tags}}', [
            'teacher_id' => $this->integer(11)->notNull(),
            'tag_id' => $this->integer(11)->notNull(),
        ]);
        $this->createIndex('idx_teacher_tags_teacher_id_tag_id', '{{%teacher_tags}}', ['teacher_id', 'tag_id'], true);
    }

    public function down()
    {
        $this->dropIndex('idx_teacher_tags_teacher_id_tag_id', '{{%teacher_tags}}');
        $this->dropTable('{{%teacher_tags}}');
        return true;
    }
}
