<?php

use yii\db\Migration;

/**
 * Handles the creation for table `tbl_coupon_course`.
 */
class m160520_081539_create_tbl_coupon_course extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('{{%coupon_courses}}', [
            'id' => $this->primaryKey(),
            'course_id' => $this->integer()->notNull(),
            'coupon_id' => $this->integer()->notNull(),
        ]);
        $this->addForeignKey('fk_coupon_course', "{{%coupon_courses}}", 'coupon_id', "{{%coupons}}", 'id');
        $this->addForeignKey('fk_course_coupon', "{{%coupon_courses}}", 'course_id', "{{%courses}}", 'id');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%coupon_courses}}');
        
        return true;
    }
}
