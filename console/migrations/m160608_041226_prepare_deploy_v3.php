<?php

use yii\db\Migration;

class m160608_041226_prepare_deploy_v3 extends Migration
{
    public function safeup()
    {
        $this->alterColumn('{{%courses}}', 'course_commission_type_id', $this->integer(11)->defaultValue(0));
    }

    public function safeDown()
    {

        return true;
    }

}
