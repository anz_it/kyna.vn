<?php

use console\components\KynaMigration;

class m160227_033518_create_vendors extends KynaMigration
{
    public function up()
    {
        try {
            $this->createTable('{{%vendors}}', [
                'id' => $this->primaryKey(),
                'name' => $this->string(45)->notNull(),
                'api_account_id' => $this->integer(11)->defaultValue(0),
                'vendor_type' => $this->smallInteger(3)->defaultValue(0) . " COMMENT '1: payment, 2: logistic, 3: video, 4: cdn'",
                'total_amount' => $this->double()->defaultValue(0),
                'status' => $this->smallInteger(3)->defaultValue(1),
                'created_time' => $this->integer(10)->defaultValue(0),
                'updated_time' => $this->integer(10)->defaultValue(0),
                    ], self::$_tableOptions
            );

            $this->createIndex('index_api_account_id', '{{%vendors}}', ['api_account_id']);
            $this->createIndex('index_name', '{{%vendors}}', ['name']);
        } catch (yii\console\Exception $ex) {
            echo $ex->getMessage();
            return false;
        }
    }

    public function down()
    {
        $this->dropTable('vendors');
        
        echo "m160227_033518_create_vendors has been reverted.\n";

        return true;
    }
    
}
