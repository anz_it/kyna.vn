<?php

use yii\db\Migration;

class m181106_070505_create_table_kid_commission extends Migration
{
    const KID_COMMISSION_INCOME = 'kid_commission_income';
    public function up()
    {
        $this->createTable(self::KID_COMMISSION_INCOME, [
            'id' => $this->primaryKey(),
            'package_subscription_id' => $this->integer(11),
            'order_id' => $this->integer(11),
            'register_email' => $this->string(255),
            'register_name' => $this->string(255),
            'register_phone_number' => $this->string(20),
            'package_name' => $this->string(255),
            'subscription_name' => $this->string(255),
            'affiliate_id' => $this->integer(11),
            'total_amount' => $this->integer(11),
            'affiliate_commission_percent' => $this->float(),
            'affiliate_commission_amount' =>$this->double(),
            'kyna_commission_amount' => $this->double(),
            'description' => $this->string(500),
            'created_time' => $this->integer(11),
            'updated_time' => $this->integer(11)

        ]);
        $this->createIndex('idx_kid_affiliate_id', self::KID_COMMISSION_INCOME, ['affiliate_id']);
        $this->createIndex('idx_kid_register_email', self::KID_COMMISSION_INCOME, ['register_email']);
        $this->createIndex('idx_kid_register_name', self::KID_COMMISSION_INCOME, ['register_name']);
        $this->createIndex('idx_kid_register_phone_number', self::KID_COMMISSION_INCOME, ['register_phone_number']);
        $this->createIndex('idx_kid_package_name', self::KID_COMMISSION_INCOME, ['package_name']);
        $this->createIndex('idx_kid_subscription_name', self::KID_COMMISSION_INCOME, ['subscription_name']);
    }

    public function down()
    {
        echo "m181106_070505_create_table_kid_commission cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
