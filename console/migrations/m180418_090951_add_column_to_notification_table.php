<?php

use yii\db\Migration;

class m180418_090951_add_column_to_notification_table extends Migration
{
    public function up()
    {
        $this->addColumn('notification', 'category', $this->string());
        $this->addColumn('notification', 'tag', $this->string());
        $this->addColumn('notification', 'course', $this->string());
        $this->addColumn('notification', 'search', $this->string());
        $this->addColumn('notification', 'webview', $this->string());
    }

    public function down()
    {
        $this->dropColumn('notification', 'category');
        $this->dropColumn('notification', 'tag');
        $this->dropColumn('notification', 'course');
        $this->dropColumn('notification', 'search');
        $this->dropColumn('notification', 'webview');
    }
}
