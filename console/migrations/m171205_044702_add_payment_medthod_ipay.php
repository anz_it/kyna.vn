<?php

use yii\db\Migration;

class m171205_044702_add_payment_medthod_ipay extends Migration
{
    public function up()
    {
        $this->insert('payment_methods', [
            'name' => 'Thanh toán bằng mã thẻ cào điện thoại',
            'class' => 'ipay',
            'payment_type' => 2,
            'status' => 0,
            'created_time' => time()
        ]);
    }

    public function down()
    {
        $this->delete('payment_methods', ['class' => 'ipay']);
    }

}
