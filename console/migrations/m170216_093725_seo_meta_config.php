<?php

use yii\db\Migration;

class m170216_093725_seo_meta_config extends Migration
{
    public function up()
    {

        $this->execute("
            insert into meta_fields(`key`, `name`, `type`, `model`, `status`)
            values ('seo_course_view_title', 'Course View Title', 2, 'seo_setting', 1),
            ('seo_course_view_description', 'Course View Description', 2, 'seo_setting', 1),
            ('seo_course_view_keyword', 'Course View Keyword', 2, 'seo_setting', 1),
            ('seo_category_view_title', 'Category View Title', 2, 'seo_setting', 1),
            ('seo_category_view_description', 'Category View Description', 2, 'seo_setting', 1 ),
            ('seo_category_view_keyword', 'Category View Keyword', 2, 'seo_setting', 1),
            ('seo_tag_view_title', 'Tag View Title', 2, 'seo_setting', 1),
            ('seo_tag_view_description', 'Tag View Description', 2, 'seo_setting', 1),
            ('seo_tag_view_keyword', 'Tag View Keyword', 2, 'seo_setting', 1)
        ");
    }

    public function down()
    {
        echo "m170216_093725_seo_meta_config cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
