<?php

class m160415_070839_alter_tbl_course_lessons_add_video_link extends \console\components\KynaMigration
{

    public function safeUp()
    {
        $this->dropColumn('{{%course_lessons}}', 'prev_lession_id');
        
        $this->addColumn('{{%course_lessons}}', 'type', 'varchar(10) NOT NULL after `day_can_learn`');
        $this->addColumn('{{%course_lessons}}', 'video_link', 'text NULL after `type`');
    }

    public function safeDown()
    {
        $this->dropColumn('{{%course_lessons}}', 'video_link');
        $this->dropColumn('{{%course_lessons}}', 'type');
        
        $this->addColumn('{{%course_lessons}}', 'prev_lession_id', $this->integer(11)->defaultValue(NULL));
        
        echo "m160415_070839_alter_tbl_course_lessons_add_video_link has been reverted.\n";

        return true;
    }

}
