<?php

use yii\db\Migration;

class m160323_022122_update_tbl_payment_trans extends Migration
{
    public function up()
    {
        $this->alterColumn('{{%payment_transactions}}', 'transaction_id', $this->string(30));
        $this->renameColumn('{{%payment_transactions}}', 'transaction_id', 'transaction_code');
        $this->alterColumn('{{%payment_transactions}}', 'payment_method_id', $this->string(20));
        $this->addColumn('{{%payment_transactions}}', 'order_id', $this->integer());
        $this->renameColumn('{{%payment_transactions}}', 'payment_method_id', 'payment_method');

        return true;
    }

    public function down()
    {
        echo "m160323_022122_update_tbl_payment_trans cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
