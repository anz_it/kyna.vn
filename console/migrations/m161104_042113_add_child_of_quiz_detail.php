<?php

use yii\db\Migration;

class m161104_042113_add_child_of_quiz_detail extends Migration
{
    public function up()
    {
        $this->addColumn(\kyna\course\models\QuizDetail::tableName(), 'parent_id', 'int null default null');
    }

    public function down()
    {
        echo "m161104_042113_add_child_of_quiz_detail cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
