<?php

use yii\db\Migration;

class m170713_091806_create_tbl_affiliate_payment_links extends Migration
{
    public function up()
    {
        $this->createTable('{{%affiliate_payment_links}}', [
            'id' => $this->primaryKey(11),
            'affiliate_id' => $this->integer(11)->notNull(),
            'code' => $this->string(20)->null(),
            'course_ids' => $this->text(),
            'status' => $this->smallInteger(3)->defaultValue(1),
            'created_time' => $this->integer(10)->defaultValue(0),
            'updated_time' => $this->integer(10)->defaultValue(0),
        ]);

        $this->createIndex('index_aff_id', '{{%affiliate_payment_links}}', 'affiliate_id');
    }

    public function down()
    {
        $this->dropTable('{{%affiliate_payment_links}}');
    }

}
