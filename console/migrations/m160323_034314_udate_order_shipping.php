<?php

use yii\db\Migration;

class m160323_034314_udate_order_shipping extends Migration
{
    public function up()
    {
        $this->addColumn('{{%order_shipping}}', 'vendor_id', $this->integer());
        return true;
    }

    public function down()
    {
        echo "m160323_034314_udate_order_shipping cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
