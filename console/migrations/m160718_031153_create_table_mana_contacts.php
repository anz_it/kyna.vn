<?php

use yii\db\Migration;

/**
 * Handles the creation for table `table_mana_contacts`.
 */
class m160718_031153_create_table_mana_contacts extends \console\components\KynaMigration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        try {
            $this->createTable('{{%mana_contacts}}', [
                'id' => $this->primaryKey(),
                'name' => $this->string(255),
                'phone' => $this->string(255),
                'email' => $this->string(255),
                'subject_id' => $this->bigInteger()->defaultValue(0),
                'certificate_id' => $this->bigInteger()->defaultValue(0),
                'education_id' => $this->bigInteger()->defaultValue(0),
                'status' => $this->smallInteger(3)->defaultValue(0),
                'created_time' => $this->integer(10)->defaultValue(0),
                'updated_time' => $this->integer(10)->defaultValue(0),
            ], self::$_tableOptions
            );

            $this->createIndex('index_email', '{{%mana_contacts}}', ['email']);
            $this->createIndex('index_phone', '{{%mana_contacts}}', ['phone']);
        } catch (yii\console\Exception $ex) {
            echo $ex->getMessage();
            return false;
        }

        return true;
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%mana_contacts}}');
        echo "m160718_031153_create_table_mana_contacts has been reverted.\n";
        return true;
    }
}
