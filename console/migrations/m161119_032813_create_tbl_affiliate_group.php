<?php

use yii\db\Migration;

class m161119_032813_create_tbl_affiliate_group extends Migration
{

    public function up()
    {
        $this->createTable('{{%affiliate_groups}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(50)->notNull(),
            'is_none' => "BIT(1) default 0",
            'is_deleted' => "BIT(1) default 0",
            'status' => $this->smallInteger(3)->defaultValue(1),
            'created_time' => $this->integer(10)->defaultValue(0),
            'updated_time' => $this->integer(10)->defaultValue(0),
        ]);

        $this->renameColumn('{{%affiliate_categories}}', 'group', 'affiliate_group_id');
        $this->alterColumn('{{%affiliate_categories}}', 'affiliate_group_id', $this->integer(11)->notNull());
        $this->createIndex('index_aff_group_id', '{{%affiliate_categories}}', 'affiliate_group_id');

        $this->renameColumn('{{%commission_calculations}}', 'group', 'affiliate_group_id');
        $this->alterColumn('{{%commission_calculations}}', 'affiliate_group_id', $this->integer(11)->notNull());
        $this->createIndex('index_aff_group_id', '{{%commission_calculations}}', 'affiliate_group_id');
    }

    public function down()
    {
        $this->dropIndex('index_aff_group_id', '{{%commission_calculations}}');
        $this->alterColumn('{{%commission_calculations}}', 'affiliate_group_id', $this->smallInteger(3)->notNull());
        $this->renameColumn('{{%commission_calculations}}', 'affiliate_group_id', 'group');

        $this->dropIndex('index_aff_group_id', '{{%affiliate_categories}}');
        $this->alterColumn('{{%affiliate_categories}}', 'affiliate_group_id', $this->smallInteger(3)->notNull());
        $this->renameColumn('{{%affiliate_categories}}', 'affiliate_group_id', 'group');

        $this->dropTable('{{%affiliate_groups}}');

        return true;
    }

}
