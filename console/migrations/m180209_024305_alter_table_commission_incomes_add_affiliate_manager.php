<?php

use yii\db\Migration;

class m180209_024305_alter_table_commission_incomes_add_affiliate_manager extends Migration
{
    public function up()
    {
        $this->addColumn(
            'commission_incomes',
            'affiliate_manager_id',
            $this->integer(11)
        );
        $this->addColumn(
            'commission_incomes',
            'affiliate_manager_percent',
            $this->float()->defaultValue(0)
        );
        $this->addColumn(
            'commission_incomes',
            'affiliate_manager_amount',
            $this->double()->defaultValue(0)
        );

        $this->createIndex('index_manager_amount', 'commission_incomes', 'affiliate_manager_amount');
    }

    public function down()
    {
        $this->dropIndex('index_manager_amount', 'commission_incomes');
        $this->dropColumn('commission_incomes', 'affiliate_manager_amount');
        $this->dropColumn('commission_incomes', 'affiliate_manager_percent');
        $this->dropColumn('commission_incomes', 'affiliate_manager_id');
    }
}
