<?php

class m160425_101953_create_tbl_course_questions_answers extends console\components\KynaMigration
{

    public function safeUp()
    {
        $this->createTableQuestion();
        
        $this->createTableAnswer();
        
        $this->createTableQuizQuestion();
    }
    
    public function safeDown()
    {
        $this->dropTable('{{%course_quiz_questions}}');
        
        $this->dropTable('{{%course_answers}}');
        
        $this->dropTable('{{%course_questions}}');
        
        echo "m160425_101953_create_tbl_course_questions_answers has been reverted successfully.";
        return true;
    }
    
    private function createTableQuestion()
    {
        // create table questions
        $this->createTable('{{%course_questions}}', [
            'id' => $this->primaryKey(),
            'course_id' => $this->integer(11)->notNull(),
            'type' => "TINYINT(2) default 1 COMMENT '1: multi-choice, 2: comment'",
            'content' => $this->text()->notNull(),
            'image_url' => $this->text(),
            'explaination' => $this->text(),
            'status' => $this->smallInteger(3)->defaultValue(1),
            'is_deleted' => "bit(1) DEFAULT b'0'",
            'created_time' => $this->integer(10)->defaultValue(0),
            'updated_time' => $this->integer(10)->defaultValue(0),
        ]);
        // add index for col course_id for relation query
        $this->createIndex("index_course_id", '{{%course_questions}}', 'course_id');
    }
    
    private function createTableAnswer()
    {
        // create table questions
        $this->createTable('{{%course_answers}}', [
            'id' => $this->primaryKey(),
            'course_id' => $this->integer(11)->notNull(),
            'question_id' => $this->integer(11)->notNull(),
            'content' => $this->text()->notNull(),
            'image_url' => $this->text(),
            'is_exactly' => "bit(1) DEFAULT b'0'",
            'order' => $this->smallInteger(3)->defaultValue(0),
            'status' => $this->smallInteger(3)->defaultValue(1),
            'is_deleted' => "bit(1) DEFAULT b'0'",
            'created_time' => $this->integer(10)->defaultValue(0),
            'updated_time' => $this->integer(10)->defaultValue(0),
        ]);
        
        // add index for col course_id for relation query
        $this->createIndex("index_course_id", '{{%course_answers}}', 'course_id');
        $this->createIndex("index_question_id", '{{%course_answers}}', 'question_id');
    }
    
    private function createTableQuizQuestion()
    {
        $this->createTable('{{%course_quiz_questions}}', [
            'id' => $this->primaryKey(),
            'course_id' => $this->integer(11)->notNull(),
            'course_quiz_id' => $this->integer(11)->notNull(),
            'course_question_id' => $this->integer(11)->notNull(),
            'order' => $this->smallInteger(3)->defaultValue(0),
            'status' => $this->smallInteger(3)->defaultValue(1),
            'is_deleted' => "bit(1) DEFAULT b'0'",
            'created_time' => $this->integer(10)->defaultValue(0),
            'updated_time' => $this->integer(10)->defaultValue(0),
        ]);
        
        $this->createIndex("index_course_id", '{{%course_quiz_questions}}', 'course_id');
        $this->createIndex("course_quiz_id", '{{%course_quiz_questions}}', 'course_quiz_id');
        $this->createIndex("course_question_id", '{{%course_quiz_questions}}', 'course_question_id');
    }

}
