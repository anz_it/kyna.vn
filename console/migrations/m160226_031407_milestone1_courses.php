<?php

use console\components\KynaMigration;

class m160226_031407_milestone1_courses extends KynaMigration
{

    public function up()
    {
        // create table `courses`
        $this->createTableCourse();
        
        // create table `course_custom_field_values`
        $this->createTableCourseCustomFieldValue();
        
        // create table `course_sections`
        $this->createTableCourseSection();
        
        // create table `course_lessons`
        $this->createTableCourseLesson();
        
        // create table `course_lesson_contents`
        $this->createTableCourseLessonContent();
        
        // create table `course_missions`
        $this->createTableCourseMission();
        
        // create table `course_quizs`
        $this->createTableCourseQuiz();
        
        return true;
    }
    
    public function down()
    {
        // drop table indices and table `courses` 
        $this->dropTableCourse();
        
        // drop table indices and table `course_custom_field_values` 
        $this->dropTableCourseCustomFieldValue();
        
        // drop table indices and table `course_sections` 
        $this->dropTableCourseSection();
        
        // drop table indices and table `course_lessons` 
        $this->dropTableCourseLesson();
        
        // drop table indices and table `course_lesson_contents` 
        $this->dropTableCourseLessonContent();
        
        // drop table indices and table `course_missions` 
        $this->dropTableCourseMission();
        
        // drop table indices and table `course_quizs` 
        $this->dropTableCourseQuiz();
        
        // echo result and return
        echo "m160226_031407_milestone1_courses has been reverted.\n";

        return true;
    }

    /**
     * @desc create table `courses` and related
     * @return boolean
     */
    private function createTableCourse()
    {
        try {
            $this->createTable('{{%courses}}', [
                'id' => $this->primaryKey(),
                'name' => $this->string(255)->notNull(),
                'short_name' => $this->string(45),
                'level' => $this->integer(11)->defaultValue(1),
                'price' => $this->integer(11)->notNull()->defaultValue(0),
                'price_discount' => $this->integer(11)->defaultValue(0),
                'percent_discount' => $this->float(11)->defaultValue(0),
                'slug' => $this->text()->notNull(),
                'image_url' => $this->text(),
                'video_url' => $this->text(),
                'video_cover_image_url' => $this->text(),
                'total_time' => $this->double()->defaultValue(0),
                'category_id' => $this->integer(11)->defaultValue(0),
                'teacher_id' => $this->integer(11)->notNull()->defaultValue(0),
                'status' => $this->smallInteger(3)->defaultValue(1),
                'created_time' => $this->integer(10)->defaultValue(0),
                'updated_time' => $this->integer(10)->defaultValue(0),
                    ], self::$_tableOptions
            );

            $this->createIndex('index_name', '{{%courses}}', ['name']);
            $this->createIndex('index_category_id', '{{%courses}}', ['category_id']);
            $this->createIndex('index_teacher_id', '{{%courses}}', ['teacher_id']);
        } catch (yii\console\Exception $ex) {
            echo $ex->getMessage();
            return false;
        }
        
        return true;
    }
    
    private function dropTableCourse()
    {
        try {
            $this->dropIndex('index_name', '{{%courses}}');
            $this->dropIndex('index_category_id', '{{%courses}}');
            $this->dropIndex('index_teacher_id', '{{%courses}}');

            $this->dropTable('{{%courses}}');
        } catch (yii\console\Exception $ex) {
            echo $ex->getMessage();
            return false;
        }
        
        return true;
    }
    
    /**
     * @desc create table `course_custom_field_values`,
     * this is meta fields table for `courses`
     */
    private function createTableCourseCustomFieldValue()
    {
        try {
            $this->createTable('{{%course_custom_field_values}}', [
                'id' => $this->primaryKey(),
                'course_id' => $this->integer(11)->notNull(),
                'custom_field_id' => $this->integer(11)->notNull(),
                'value' => $this->text(),
                    ], self::$_tableOptions
            );

            $this->createIndex('index_course_id', '{{%course_custom_field_values}}', ['course_id']);
            $this->createIndex('index_custom_field_id', '{{%course_custom_field_values}}', ['custom_field_id']);
        } catch (yii\console\Exception $ex) {
            echo $ex->getMessage();
            return false;
        }
        
        return true;
    }
    
    private function dropTableCourseCustomFieldValue()
    {
        try {
            $this->dropIndex('index_course_id', '{{%course_custom_field_values}}');
            $this->dropIndex('index_custom_field_id', '{{%course_custom_field_values}}');

            $this->dropTable('{{%course_custom_field_values}}');
        } catch (yii\console\Exception $ex) {
            echo $ex->getMessage();
            return false;
        }
        
        return true;
    }
    
    /**
     * @desc create table `course_sections`, to store course chapters or sections
     * @return boolean
     */
    private function createTableCourseSection()
    {
        try {
            $this->createTable('{{%course_sections}}', [
                'id' => $this->primaryKey(),
                'course_id' => $this->integer(11)->notNull(),
                'name' => $this->string(255)->notNull(),
                'type' => "enum('chapter', 'section') NOT NULL COMMENT 'Chương hoac Phần'",
                'description' => $this->text(),
                'order' => $this->integer(3)->defaultValue(0),
                'status' => $this->smallInteger(3)->defaultValue(1),
                'created_time' => $this->integer(10)->defaultValue(0),
                'updated_time' => $this->integer(10)->defaultValue(0),
                    ], self::$_tableOptions
            );

            $this->createIndex('index_course_id', '{{%course_sections}}', ['course_id']);
        } catch (yii\console\Exception $ex) {
            echo $ex->getMessage();
            return false;
        }
        
        return true;
    }
    
    private function dropTableCourseSection()
    {
        try {
            $this->dropIndex('index_course_id', '{{%course_sections}}');

            $this->dropTable('{{%course_sections}}');
        } catch (yii\console\Exception $ex) {
            echo $ex->getMessage();
            return false;
        }
        
        return true;
    }
    
    /**
     * @desc create table `course_lessons` to store lessions of course
     * @return boolean
     */
    private function createTableCourseLesson()
    {
        try {
            $this->createTable('{{%course_lessons}}', [
                'id' => $this->primaryKey(),
                'course_id' => $this->integer(11)->notNull(),
                'section_id' => $this->integer(11)->notNull(),
                'name' => $this->string(255)->notNull(),
                'day_can_learn' => $this->integer(10)->defaultValue(0),
                'note' => $this->text(),
                'content' => $this->text(),
                'quiz_id' => $this->integer(11)->defaultValue(0),
                'prev_lession_id' => $this->integer(11)->defaultValue(0),
                'order' => $this->integer(3)->defaultValue(0),
                'status' => $this->smallInteger(3)->defaultValue(1),
                'created_time' => $this->integer(10)->defaultValue(0),
                'updated_time' => $this->integer(10)->defaultValue(0),
                    ], self::$_tableOptions
            );

            $this->createIndex('index_course_id_vs_section_id', '{{%course_lessons}}', ['course_id', 'section_id']);
        } catch (yii\console\Exception $ex) {
            echo $ex->getMessage();
            return false;
        }
        
        return true;
    }
    
    private function dropTableCourseLesson()
    {
        try {
            $this->dropIndex('index_course_id_vs_section_id', '{{%course_lessons}}');

            $this->dropTable('{{%course_lessons}}');
        } catch (yii\console\Exception $ex) {
            echo $ex->getMessage();
            return false;
        }
        
        return true;
    }
    
    private function createTableCourseLessonContent()
    {
        try {
            $this->createTable('{{%course_lesson_contents}}', [
                'id' => $this->primaryKey(),
                'course_id' => $this->integer(11)->notNull(),
                'section_id' => $this->integer(11)->notNull(),
                'lesson_id' => $this->integer(11)->notNull(),
                'title' => $this->string(255)->notNull(),
                'type' => "enum('pdf', 'doc', 'excel', 'video', 'image')",
                'source' => $this->text(),
                'detail' => $this->text(),
                'order' => $this->integer(3)->defaultValue(0),
                'status' => $this->smallInteger(3)->defaultValue(1),
                'created_time' => $this->integer(10)->defaultValue(0),
                'updated_time' => $this->integer(10)->defaultValue(0),
                    ], self::$_tableOptions
            );

            $this->createIndex('index_course_id_vs_section_id_vs_lesson_id', '{{%course_lesson_contents}}', ['course_id', 'section_id', 'lesson_id']);
        } catch (yii\console\Exception $ex) {
            echo $ex->getMessage();
            return false;
        }
        
        return true;
    }
    
    private function dropTableCourseLessonContent()
    {
        try {
            $this->dropIndex('index_course_id_vs_section_id_vs_lesson_id', '{{%course_lesson_contents}}');

            $this->dropTable('{{%course_lesson_contents}}');
        } catch (yii\console\Exception $ex) {
            echo $ex->getMessage();
            return false;
        }
        
        return true;
    }
    
    private function createTableCourseMission()
    {
        try {
            $this->createTable('{{%course_missions}}', [
                'id' => $this->primaryKey(),
                'course_id' => $this->integer(11)->notNull(),
                'section_id' => $this->integer(11)->defaultValue(0),
                'name' => $this->string(255)->notNull(),
                'type' => $this->string(45)->notNull(),
                'day_can_do' => $this->integer(5)->defaultValue(0),
                'max_executing_day' => $this->integer(11)->defaultValue(0),
                'bonus_delta_xu' => $this->integer(11)->defaultValue(0),
                'description' => $this->text(),
                'status' => $this->smallInteger(3)->defaultValue(1),
                'created_time' => $this->integer(10)->defaultValue(0),
                'updated_time' => $this->integer(10)->defaultValue(0),
                    ], self::$_tableOptions
            );

            $this->createIndex('index_course_id', '{{%course_missions}}', ['course_id']);
            $this->createIndex('index_section_id', '{{%course_missions}}', ['section_id']);
        } catch (yii\console\Exception $ex) {
            echo $ex->getMessage();
            return false;
        }
        
        return true;
    }
    
    private function dropTableCourseMission()
    {
        try {
            $this->dropIndex('index_section_id', '{{%course_missions}}');
            $this->dropIndex('index_course_id', '{{%course_missions}}');

            $this->dropTable('{{%course_missions}}');
        } catch (yii\console\Exception $ex) {
            echo $ex->getMessage();
            return false;
        }
        
        return true;
    }
    
    private function createTableCourseQuiz()
    {
        try {
            $this->createTable('{{%course_quizs}}', [
                'id' => $this->primaryKey(),
                'course_id' => $this->integer(11)->notNull(),
                'name' => $this->string(255)->notNull(),
                'image_url' => $this->text(),
                'description' => $this->text(),
                'status' => $this->smallInteger(3)->defaultValue(1),
                'created_time' => $this->integer(10)->defaultValue(0),
                'updated_time' => $this->integer(10)->defaultValue(0),
                    ], self::$_tableOptions
            );

            $this->createIndex('index_course_id', '{{%course_quizs}}', ['course_id']);
        } catch (yii\console\Exception $ex) {
            echo $ex->getMessage();
            return false;
        }
        
        return true;
    }
    
    private function dropTableCourseQuiz()
    {
        try {
            $this->dropIndex('index_course_id', '{{%course_quizs}}');

            $this->dropTable('{{%course_quizs}}');
        } catch (yii\console\Exception $ex) {
            echo $ex->getMessage();
            return false;
        }
        
        return true;
    }
    
    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
