<?php

use console\components\KynaMigration;

class m160226_072109_create_table_custom_fields extends KynaMigration
{
    public function up()
    {
        try {
            $this->createTable('{{%custom_fields}}', [
                'id' => $this->primaryKey(),
                'name' => $this->string(255)->notNull(),
                'type' => "int(3) DEFAULT 1 COMMENT '1: text, 2: number, 3 datetime, 4: checkbox, 5: checkbox list, 6: radio button list, 7: dropdown list'",
                'model' => $this->string(20)->notNull(),
                'extra_validate' => $this->text(),
                'data_set' => $this->text(),
                'is_required' => "bit(1) DEFAULT b'0'",
                'is_index_es' => "bit(1) DEFAULT b'0' COMMENT 'Is index to elastic search this field?'",
                'is_readonly' => "bit(1) DEFAULT b'0' COMMENT 'Admin can edit in CRUD form at Backend'",
                'note' => $this->text(),
                'status' => $this->smallInteger(3)->defaultValue(1),
                'created_time' => $this->integer(10)->defaultValue(0),
                'updated_time' => $this->integer(10)->defaultValue(0),
                    ], self::$_tableOptions
            );

        } catch (yii\console\Exception $ex) {
            echo $ex->getMessage();
            return false;
        }
        
        return true;
    }

    public function down()
    {
        $this->dropTable('{{%custom_fields}}');
        echo "m160226_072109_create_table_custom_fields has been reverted.\n";
        
        return true;
    }
}
