<?php

use yii\db\Migration;

class m160621_065338_alter_table_promotion extends Migration
{
    public function up()
    {
        $this->addColumn("{{%promotions}}", "prefix", $this->string(255));
    }

    public function down()
    {
        return true;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
