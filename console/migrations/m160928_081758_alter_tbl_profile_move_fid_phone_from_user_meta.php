<?php

use yii\db\Migration;

class m160928_081758_alter_tbl_profile_move_fid_phone_from_user_meta extends Migration
{

    public function up()
    {
        $this->addColumn('{{%profile}}', 'phone_number', $this->string(15));
        $this->addColumn('{{%profile}}', 'facebook_id', $this->string(20));
        
        $this->createIndex('index_phone', '{{%profile}}', 'phone_number');
        $this->createIndex('index_facebook_id', '{{%profile}}', 'facebook_id');
    }

    public function down()
    {
        $this->dropIndex('index_facebook_id', '{{%profile}}');
        $this->dropIndex('index_phone', '{{%profile}}');
        
        $this->dropColumn('{{%profile}}', 'facebook_id');
        $this->dropColumn('{{%profile}}', 'phone_number');

        return true;
    }

}
