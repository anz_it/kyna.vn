<?php

use yii\db\Migration;

class m170413_032236_create_schema_multi_tenant extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%partner}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'description' => $this->string(),
            'class' => $this->string()->unique()->notNull()->comment('Class name of Partner Library. Ex: taamkru, touch_n_math'),
            'partner_type' => $this->integer(4)->notNull(),
            'status' => $this->smallInteger(1)->defaultValue(0),
            'is_deleted' => $this->smallInteger(1)->defaultValue(0),
            'created_time' => $this->integer(10)->defaultValue(0),
            'updated_time' => $this->integer(10)->defaultValue(0),
        ]);

        $this->createIndex('idx_partner_name', '{{%partner}}', 'name');

        $this->execute("
            RENAME TABLE 
                taamkru_category TO partner_category, 
                taamkru_code TO partner_code,
                taamkru_retailer TO partner_retailer,
                taamkru_transaction TO partner_transaction;
        ");
        $this->addColumn('{{%partner_category}}', 'partner_id', $this->integer(10)->notNull());
        $this->addColumn('{{%partner_code}}', 'partner_id', $this->integer(10)->notNull());
        $this->addColumn('{{%partner_retailer}}', 'partner_id', $this->integer(10)->notNull());
        $this->addColumn('{{%partner_transaction}}', 'partner_id', $this->integer(10)->notNull());

        $this->addColumn('{{%courses}}', 'partner_id', $this->integer(10));

        $this->dropIndex('value', '{{%partner_category}}');
        $this->createIndex('idx_partner_category_value_partner_id', '{{%partner_category}}', ['value', 'partner_id'], true);

        $this->dropIndex('user_id', '{{%partner_retailer}}');
        $this->createIndex('idx_partner_retailer_user_id_partner_id', '{{%partner_retailer}}', ['user_id', 'partner_id'], true);

        $this->createIndex('idx_partner_code_partner_id', '{{%partner_code}}', 'partner_id');

        // create index for activation code
        $this->createIndex('idx_orders_activation_code', '{{%orders}}', 'activation_code');

        // create index for order shipping
        $this->createIndex('idx_order_shipping_order_id', '{{%order_shipping}}', 'order_id');
    }

    public function safeDown()
    {
        $this->dropIndex('idx_order_shipping_order_id', '{{%order_shipping}}');

        $this->dropIndex('idx_orders_activation_code', '{{%orders}}');

        $this->dropIndex('idx_partner_code_partner_id', '{{%partner_code}}');

        $this->dropIndex('idx_partner_retailer_user_id_partner_id', '{{%partner_retailer}}');
        $this->createIndex('user_id', '{{%partner_retailer}}', 'user_id', true);

        $this->dropIndex('idx_partner_category_value_partner_id', '{{%partner_category}}');
        $this->createIndex('value', '{{%partner_category}}', 'value', true);

        $this->dropColumn('{{%courses}}', 'partner_id');

        $this->dropColumn('{{%partner_transaction}}', 'partner_id');
        $this->dropColumn('{{%partner_retailer}}', 'partner_id');
        $this->dropColumn('{{%partner_code}}', 'partner_id');
        $this->dropColumn('{{%partner_category}}', 'partner_id');

        $this->execute("
            RENAME TABLE 
                partner_category TO taamkru_category, 
                partner_code TO taamkru_code,
                partner_retailer TO taamkru_retailer,
                partner_transaction TO taamkru_transaction;
        ");

        $this->dropIndex('idx_partner_name', '{{%partner}}');
        $this->dropTable('{{%partner}}');
    }
}
