<?php

use yii\db\Migration;

class m170825_084113_alter_table_meta_fields_add_key_slug extends Migration
{
    public function up()
    {
        $this->addColumn('{{%meta_fields}}', 'is_unique', $this->smallInteger(1));
        $this->insert('{{%meta_fields}}', [
            'key' => 'slug',
            'name' => 'Slug',
            'type' => 1,
            'model' => 'teacher',
            'is_required' => 1,
            'is_index_es' => 1,
            'is_unique' => 1,
            'status' => 1
        ]);
    }

    public function down()
    {
        $this->delete('{{%meta_fields}}', [
            'key' => 'slug',
            'model' => 'teacher'
        ]);

        $this->dropColumn('{{%meta_fields}}', 'is_unique');

        return true;
    }
}
