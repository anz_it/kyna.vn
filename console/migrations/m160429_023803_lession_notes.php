<?php

use yii\db\Migration;

class m160429_023803_lession_notes extends \console\components\KynaMigration
{
    public function up()
    {
        $this->createTable('{{%course_lession_note}}', [
            'id' => $this->primaryKey(),
            'course_lession_id' => $this->integer(),
            'user_id' => $this->integer(),
            'note' => $this->string(50),
            'current_run_time' => $this->integer(),
        ], self::$_tableOptions);
    }

    public function down()
    {
        echo "m160429_023803_lession_notes cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
