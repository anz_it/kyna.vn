<?php

use yii\db\Migration;

class m180727_075348_addReportTable extends Migration
{
    const REPORT_TABLE = "report";
    const REPORT_PARAM_TABLE = "report_param";
    const REPORT_USER_TABLE = "report_user";

    public function up()
    {
        $this->createTable(self::REPORT_TABLE, [
            'id' => $this->primaryKey(),
            'name' => $this->string(256)->notNull(),
            'description' => $this->string(),
            'sql_command' => $this->string()->notNull(),
            'status' => $this->integer(),
            'note'=>$this->string(),
            'columns_name'=>$this->string()->notNull(),
            'created_time' => $this->integer(10)->defaultValue(0),
            'updated_time' => $this->integer(10)->defaultValue(0),

        ]);


        $this->createTable(self::REPORT_PARAM_TABLE, [
            'id' => $this->primaryKey(),
            'report_id' => $this->integer()->notNull(),
            'name' => $this->string(64)->notNull(),
            'description' => $this->string(256)->notNull(),
            'default_value' => $this->string()
        ]);

    }

    public function down()
    {
       $this->dropTable(self::REPORT_PARAM_TABLE);
       $this->dropTable(self::REPORT_TABLE);
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
