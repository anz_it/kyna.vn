<?php

use yii\db\Migration;

class m160317_043957_add_tbl_time_slot extends console\components\KynaMigration
{

    public function up()
    {
        $this->createTable('{{%time_slot}}', [
            'id' => $this->primaryKey(),
            'start_time' => $this->time()->notNull(),
            'end_time' => $this->time()->notNull(),
            'monday' => $this->string(128),
            'tuesday' => $this->string(128),
            'wednesday' => $this->string(128),
            'thursday' => $this->string(128),
            'friday' => $this->string(128),
            'saturday' => $this->string(128),
            'sunday' => $this->string(128),
            'type' => $this->string(128),
            'user_type' => $this->string(45)->defaultValue('default'),
            'related_id' => $this->integer(11)->defaultValue(0),
            'status' => $this->smallInteger(3)->defaultValue(1),
            'created_time' => $this->integer(10)->defaultValue(0),
            'updated_time' => $this->integer(10)->defaultValue(0),
                ], self::$_tableOptions
        );
    }

    public function down()
    {
        $this->dropTable('{{%time_slot}}');
        
        echo "m160317_043957_add_tbl_time_slot has been reverted.\n";

        return true;
    }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
