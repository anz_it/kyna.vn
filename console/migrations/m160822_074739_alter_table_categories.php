<?php

use yii\db\Migration;

class m160822_074739_alter_table_categories extends Migration
{
    public function up()
    {
        $this->addColumn('{{%categories}}', 'css_class', $this->string());
    }

    public function down()
    {
        $this->dropColumn('{{%categories}}', 'css_class');

        return true;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
