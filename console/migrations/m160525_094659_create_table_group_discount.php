<?php

use yii\db\Migration;

/**
 * Handles the creation for table `table_group_discount`.
 */
class m160525_094659_create_table_group_discount extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('{{%group_discount}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(50)->notNull(),
            'percent_discount' => $this->float(11)->defaultValue(0),
            'course_quantity' => $this->smallInteger(5)->defaultValue(0),
            'is_deleted' => "bit(1) DEFAULT b'0'",
            'status' => $this->smallInteger(3)->defaultValue(1),
            'created_time' => $this->integer(10)->defaultValue(0),
            'updated_time' => $this->integer(10)->defaultValue(0),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%group_discount}}');
    }
}
