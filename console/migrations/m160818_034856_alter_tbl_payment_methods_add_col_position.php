<?php

use yii\db\Migration;

class m160818_034856_alter_tbl_payment_methods_add_col_position extends Migration
{

    public function safeUp()
    {
        $this->addColumn('{{%payment_methods}}', 'position', $this->integer(10)->defaultValue(0));
    }

    public function safeDown()
    {
        $this->dropColumn('{{%payment_methods}}', 'position');
        
        return true;
    }
}
