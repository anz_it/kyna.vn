<?php

use yii\db\Migration;

class m180629_034708_add_topic_column_notification_table extends Migration
{
    public function up()
    {
        $this->addColumn('notification', 'topic', $this->string(255)->comment('Loai Topic'));
    }

    public function down()
    {
        $this->dropColumn('notification', 'topic');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
