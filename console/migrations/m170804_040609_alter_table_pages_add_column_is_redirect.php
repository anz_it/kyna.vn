<?php

use yii\db\Migration;
use kyna\page\models\Page;

class m170804_040609_alter_table_pages_add_column_is_redirect extends Migration
{
    public function up()
    {
        $pageTblName = Page::tableName();

        $this->addColumn($pageTblName, 'is_redirect', $this->smallInteger(1)->defaultValue(0));
    }

    public function down()
    {
        $pageTblName = Page::tableName();

        $this->dropColumn($pageTblName, 'is_redirect');
    }
}
