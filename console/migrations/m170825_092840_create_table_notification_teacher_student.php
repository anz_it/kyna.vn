<?php

use yii\db\Migration;

class m170825_092840_create_table_notification_teacher_student extends Migration
{
    public function up()
    {
        $this->createTable('{{%teacher_notify}}', [
            'teacher_id' => $this->integer(11)->notNull(),
            'user_id' => $this->integer(11)->notNull(),
            'created_time' => $this->integer(11)->defaultValue(0),
            'updated_time' => $this->integer(11)->defaultValue(0),
        ]);
        $this->createIndex('idx_teacher_notify_teacher_id_user_id', '{{%teacher_notify}}', ['teacher_id', 'user_id'], true);
    }

    public function down()
    {
        $this->dropIndex('idx_notification_teacher_student_teacher_id_user_id', '{{%teacher_notify}}');
        $this->dropTable('{{%teacher_notify}}');

        return true;
    }
}
