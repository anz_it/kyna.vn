<?php

use yii\db\Migration;

class m161011_083614_alter_table_payment_transaction extends Migration
{
    public function up()
    {
        $this->addColumn('{{%payment_transactions}}', 'amount', $this->double());
    }

    public function down()
    {
        $this->dropColumn('{{%payment_transactions}}', 'amount');

        return TRUE;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
