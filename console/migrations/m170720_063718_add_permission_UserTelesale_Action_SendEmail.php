<?php

use yii\db\Migration;

class m170720_063718_add_permission_UserTelesale_Action_SendEmail extends Migration
{
    // 1. Create permission UserTelesale.Action.SendEmail

    // 2. Add permission to UserTelesale.All, Telesale
    public function up()
    {
        // 1.
        Yii::$app->authManager->add(Yii::$app->authManager->createPermission('UserTelesale.Action.SendEmail'));

        // 2.
        Yii::$app->authManager->addChild(Yii::$app->authManager->getPermission('UserTelesale.All'), Yii::$app->authManager->getPermission('UserTelesale.Action.SendEmail'));
        Yii::$app->authManager->addChild(Yii::$app->authManager->getRole('Telesale'), Yii::$app->authManager->getPermission('UserTelesale.Action.SendEmail'));

    }

    public function down()
    {
        // 2.
        Yii::$app->authManager->removeChild(Yii::$app->authManager->getPermission('UserTelesale.All'), Yii::$app->authManager->getPermission('UserTelesale.Action.SendEmail'));
        Yii::$app->authManager->removeChild(Yii::$app->authManager->getRole('Telesale'), Yii::$app->authManager->getPermission('UserTelesale.Action.SendEmail'));

        // 1.
        Yii::$app->authManager->remove(Yii::$app->authManager->getPermission('UserTelesale.Action.SendEmail'));


//        echo "m170720_063718_add_permission_UserTelesale_Action_SendEmail cannot be reverted.\n";
//
//        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
