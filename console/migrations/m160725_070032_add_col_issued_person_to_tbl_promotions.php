<?php

use yii\db\Migration;

class m160725_070032_add_col_issued_person_to_tbl_promotions extends Migration
{

    public function up()
    {
        $this->addColumn('{{%promotions}}', 'issued_person', $this->integer(11));
        
        $this->createIndex('index_issued_person', '{{%promotions}}', 'issued_person');
    }

    public function down()
    {
        $this->dropIndex('index_issued_person', '{{%promotions}}');
        
        $this->dropColumn('{{%promotions}}', 'issued_person');

        return true;
    }

}
