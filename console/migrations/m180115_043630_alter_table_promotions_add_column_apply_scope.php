<?php

use yii\db\Migration;

class m180115_043630_alter_table_promotions_add_column_apply_scope extends Migration
{
    const PROMOTIONS_TABLE = 'promotions';

    public function up()
    {
        $this->addColumn(self::PROMOTIONS_TABLE, 'apply_scope', $this->smallInteger(1)->defaultValue(0)->comment('0:all,1:frontend,2:backend'));
    }

    public function down()
    {
        $this->dropColumn(self::PROMOTIONS_TABLE, 'apply_scope');
    }
}
