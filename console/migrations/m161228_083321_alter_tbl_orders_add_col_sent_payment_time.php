<?php

use yii\db\Migration;

class m161228_083321_alter_tbl_orders_add_col_sent_payment_time extends Migration
{
    public function up()
    {
        $this->addColumn('{{%orders}}', 'sent_payment_request_time', $this->integer(10));
    }

    public function down()
    {
        $this->dropColumn('{{%orders}}', 'sent_payment_request_time');

        return true;
    }

}
