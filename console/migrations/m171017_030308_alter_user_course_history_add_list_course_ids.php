<?php

use yii\db\Migration;

class m171017_030308_alter_user_course_history_add_list_course_ids extends Migration
{
    public function up()
    {
        $this->addColumn('user_point_histories', 'list_course_ids', $this->string(125));
    }

    public function down()
    {
        $this->dropColumn('user_point_histories', 'list_course_ids');
    }

}
