<?php

use yii\db\Migration;

class m160822_111626_alter_table_categories extends Migration
{
    public function up()
    {
        $this->dropColumn('{{%categories}}', 'css_class');
    }

    public function down()
    {
        $this->addColumn('{{%categories}}', 'css_class', $this->string());

        return true;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
