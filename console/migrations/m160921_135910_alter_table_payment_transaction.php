<?php

use yii\db\Migration;

class m160921_135910_alter_table_payment_transaction extends Migration
{
    public function up()
    {
        $this->addColumn('{{%payment_transactions}}', 'created_time', $this->integer(10)->defaultValue(0));
        $this->addColumn('{{%payment_transactions}}', 'updated_time', $this->integer(10)->defaultValue(0));
        $this->addColumn('{{%payment_transactions}}', 'is_call', $this->boolean());
    }

    public function down()
    {

        $this->dropColumn('{{%payment_transactions}}', 'created_time');
        $this->dropColumn('{{%payment_transactions}}', 'updated_time');
        $this->dropColumn('{{%payment_transactions}}', 'is_call');

        return true;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
