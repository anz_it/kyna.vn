<?php

use yii\db\Migration;

class m160511_073622_change_name_of_course_lession_notes extends Migration
{
    public function up()
    {
        $this->renameTable('course_lession_note', 'course_lesson_notes');
        return true;
    }

    public function down()
    {
        echo "m160511_073622_change_name_of_course_lession_notes has been reverted.\n";

        return true;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
