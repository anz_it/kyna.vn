<?php

use yii\db\Migration;

class m170414_080626_alter_table_commission_incomes_add_unique_order_id_course_id extends Migration
{
    public function up()
    {
        $this->createIndex('idx_commission_incomes_order_id_course_id', '{{%commission_incomes}}', ['order_id', 'course_id'], true);
    }

    public function down()
    {
        $this->dropIndex('idx_commission_incomes_order_id_course_id', '{{%commission_incomes}}');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
