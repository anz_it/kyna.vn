<?php

use yii\db\Migration;

class m160712_040237_alter_table_quiz_session extends Migration
{
    public function up()
    {
        $this->addColumn("{{%quiz_sessions}}", 'last_interactive', $this->integer(11));
        $this->addColumn("{{%quiz_sessions}}", 'submit_time', $this->integer(11));
        $this->addColumn("{{%quiz_sessions}}", 'time_remaining', $this->integer(11));
    }

    public function down()
    {
        $this->dropColumn('{{%quiz_sessions}}', 'time_remaining');
        $this->dropColumn('{{%quiz_sessions}}', 'submit_time');
        $this->dropColumn('{{%quiz_sessions}}', 'last_interactive');
        
        return true;
    }

}
