<?php

use yii\db\Migration;

class m180413_031544_create_news_permission_order_user_updateEmail extends Migration
{
    public function up()
    {
        // Create permissions
        Yii::$app->authManager->add(Yii::$app->authManager->createPermission('Order.User.UpdateEmail'));
        Yii::$app->authManager->add(Yii::$app->authManager->createPermission('Order.User.Confirm'));
        Yii::$app->authManager->add(Yii::$app->authManager->createPermission('Order.User.confirmNew'));
        Yii::$app->authManager->add(Yii::$app->authManager->createPermission('Order.User.Errors'));

        // Add other permission to Notification All
        Yii::$app->authManager->addChild(Yii::$app->authManager->getPermission('Order.User.UpdateEmail'), Yii::$app->authManager->getPermission('Order.User.Confirm'));
        Yii::$app->authManager->addChild(Yii::$app->authManager->getPermission('Order.User.UpdateEmail'), Yii::$app->authManager->getPermission('Order.User.confirmNew'));
        Yii::$app->authManager->addChild(Yii::$app->authManager->getPermission('Order.User.UpdateEmail'), Yii::$app->authManager->getPermission('Order.User.Errors'));

        // Add permission.All to Admin
        Yii::$app->authManager->addChild(Yii::$app->authManager->getRole('Admin'), Yii::$app->authManager->getPermission('Order.User.UpdateEmail'));
    }

    public function down()
    {
        Yii::$app->authManager->removeChild(Yii::$app->authManager->getPermission('Order.User.UpdateEmail'), Yii::$app->authManager->getPermission('Order.User.Confirm'));
        Yii::$app->authManager->removeChild(Yii::$app->authManager->getPermission('Order.User.UpdateEmail'), Yii::$app->authManager->getPermission('Order.User.confirmNew'));
        Yii::$app->authManager->removeChild(Yii::$app->authManager->getPermission('Order.User.UpdateEmail'), Yii::$app->authManager->getPermission('Order.User.Errors'));

        Yii::$app->authManager->removeChild(Yii::$app->authManager->getRole('Admin'), Yii::$app->authManager->getPermission('Order.User.UpdateEmail'));

        // Delete permisstion

        Yii::$app->authManager->remove(Yii::$app->authManager->createPermission('Order.User.UpdateEmail'));
        Yii::$app->authManager->remove(Yii::$app->authManager->createPermission('Order.User.Confirm'));
        Yii::$app->authManager->remove(Yii::$app->authManager->createPermission('Order.User.confirmNew'));
        Yii::$app->authManager->remove(Yii::$app->authManager->createPermission('Order.User.Errors'));

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
