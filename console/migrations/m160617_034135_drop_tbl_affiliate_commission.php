<?php

use yii\db\Migration;

/**
 * Handles the dropping for table `tbl_affiliate_commission`.
 */
class m160617_034135_drop_tbl_affiliate_commission extends Migration
{

    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->dropTable('{{%affiliate_commissions}}');
        
        $this->addColumn('{{%promotions}}', 'current_number_usage', 'INT(11) DEFAULT 0 AFTER number_usage');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('{{%promotions}}', 'current_num_usage');
        
        $this->createTable('{{%affiliate_commissions}}', ['id' => $this->primaryKey()]);
        
        return true;
    }

}
