<?php

use yii\db\Migration;

class m170530_102956_cod_restructure extends Migration
{
    // Jobs:
    // 1. Add permisssion Order.Action.CancelShipping, Order.Action.UpdateShippingStatus
    // 2. Alter table payment_transactions: add column shipping_status, expected_payment_fee, pick_up_location_id
    // 3. Edit table vendors: insert  vendor proship, edit alias of vendor ghn (Ghn -> ghn)
    // 4. Edit table payment_methods: edit class of payment_method ghn (ghn -> cod)
    public function up()
    {
        // 1.
        Yii::$app->authManager->add(Yii::$app->authManager->createPermission('Order.Action.CancelShipping'));
        Yii::$app->authManager->add(Yii::$app->authManager->createPermission('Order.Action.UpdateShippingStatus'));
        Yii::$app->authManager->addChild(Yii::$app->authManager->getPermission('Order.All'), Yii::$app->authManager->getPermission('Order.Action.CancelShipping'));
        Yii::$app->authManager->addChild(Yii::$app->authManager->getPermission('Order.All'), Yii::$app->authManager->getPermission('Order.Action.UpdateShippingStatus'));

        // 2.
        $this->addColumn('payment_transactions', 'shipping_status', $this->integer());
        $this->addColumn('payment_transactions', 'expected_payment_fee', $this->double());
        $this->addColumn('payment_transactions', 'pick_up_location_id', $this->text());

        // 3.
        $this->update('vendors', [
                'alias' => 'ghn',
                'updated_time' => time()
            ], 'alias = "Ghn"');
        $this->insert('vendors', [
            'name' => 'Proship',
            'vendor_type' => 2,
            'status' => 1,
            'created_time' => time(),
            'updated_time' => time(),
            'alias' => 'proship',
        ]);

        // 4.
        $this->update('payment_methods', [
            'class' => 'cod',
            'updated_time' => time(),
        ], 'class = "ghn"');
    }

    public function down()
    {
        // 1.
        Yii::$app->authManager->removeChild(Yii::$app->authManager->getPermission('Order.All'), Yii::$app->authManager->getPermission('Order.Action.CancelShipping'));
        Yii::$app->authManager->removeChild(Yii::$app->authManager->getPermission('Order.All'), Yii::$app->authManager->getPermission('Order.Action.UpdateShippingStatus'));
        Yii::$app->authManager->remove(Yii::$app->authManager->createPermission('Order.Action.CancelShipping'));
        Yii::$app->authManager->remove(Yii::$app->authManager->createPermission('Order.Action.UpdateShippingStatus'));

        // 2.
        $this->dropColumn('payment_transactions', 'shipping_status');
        $this->dropColumn('payment_transactions', 'expected_payment_fee');
        $this->dropColumn('payment_transactions', 'pick_up_location_id');

        // 3.
        $this->delete('vendors', 'name = "Proship"');
        $this->update('vendors', [
            'alias' => 'Ghn',
            'updated_time' => time(),
        ], 'alias = "ghn"');

        // 4.
        $this->update('payment_methods', [
            'class' => 'ghn',
            'updated_time' => time(),
        ], 'class = "cod"');

//        echo "m170530_102956_cod_restructure cannot be reverted.\n";

//        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
