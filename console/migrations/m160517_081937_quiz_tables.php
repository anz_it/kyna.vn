<?php

use yii\db\Migration;

class m160517_081937_quiz_tables extends Migration
{
    public function up()
    {
        $this->renameTable('course_quizs', 'quizes');

        $this->renameTable('course_quiz_meta', 'quiz_meta');

        $this->renameTable('course_questions', 'quiz_questions');
        $this->dropColumn('quiz_questions', 'explaination');

        $this->renameTable('course_answers', 'quiz_answers');
        $this->dropColumn('quiz_answers', 'course_id');
        $this->renameColumn('quiz_answers', 'is_exactly', 'is_correct');
        $this->addColumn('quiz_answers', 'explanation', $this->string(255));

        $this->renameTable('course_quiz_questions', 'quiz_details');
        $this->dropColumn('quiz_details', 'course_id');
        $this->renameColumn('quiz_details', 'course_question_id', 'quiz_question_id');
        $this->renameColumn('quiz_details', 'course_quiz_id', 'quiz_id');
        $this->addColumn('quiz_details', 'score', $this->integer()->defaultValue(1));

        $this->createTable('quiz_sessions', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'quiz_id' => $this->integer(),
            'start_time' => $this->integer(),
            'duration' => $this->integer(),
        ]);
        $this->createTable('quiz_session_answers', [
            'id' => $this->primaryKey(),
            'quiz_session_id' => $this->integer(),
            'quiz_detail_id' => $this->integer(),
            'answer_id' => $this->integer(),
            'answer_text' => $this->text(),
            'is_correct' => $this->boolean(),
            'score' => $this->integer(),
        ]);

        return true;
    }

    public function down()
    {
        echo "m160517_081937_quiz_tables cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
