<?php

use yii\db\Migration;

class m160324_065750_tbl_payment_method extends Migration
{
    public function up()
    {
        $this->renameColumn('{{%payment_methods}}', 'is_auto_calculate', 'is_direct_payment');
        $this->renameColumn('{{%payment_methods}}', 'vendor_method_id', 'method');
        $this->alterColumn('{{%payment_methods}}', 'method', $this->string(20));
        return true;
    }

    public function down()
    {
        echo "m160324_065750_tbl_payment_method cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
