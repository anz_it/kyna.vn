<?php

use yii\db\Migration;

class m180924_085717_old_phone_number_column_table_user extends Migration
{
    public function up()
    {
        $this->addColumn('user_telesales','old_phone_number',$this->string(255)->after('phone_number'));
        $this->addColumn('user_addresses','old_phone_number',$this->string(255)->after('phone_number'));
        $this->addColumn('profile','old_phone_number',$this->string(255)->after('phone_number'));
        $this->addColumn('order_shipping','old_phone_number',$this->string(255)->after('phone_number'));

    }

    public function down()
    {
        $this->dropColumn('user_telesales','old_phone_number');
        $this->dropColumn('user_addresses','old_phone_number');
        $this->dropColumn('profile','old_phone_number');
        $this->dropColumn('order_shipping','old_phone_number');
    }
}
