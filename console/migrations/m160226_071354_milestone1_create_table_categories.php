<?php

use console\components\KynaMigration;

class m160226_071354_milestone1_create_table_categories extends KynaMigration
{
    public function up()
    {
        try {
            $this->createTable('{{%categories}}', [
                'id' => $this->primaryKey(),
                'name' => $this->string(255)->notNull(),
                'slug' => $this->string(255)->notNull(),
                'description' => $this->text(),
                'order' => $this->integer(11)->defaultValue(0),
                'type' => $this->smallInteger(3)->defaultValue(0),
                'status' => $this->smallInteger(3)->defaultValue(1),
                'created_time' => $this->integer(10)->defaultValue(0),
                'updated_time' => $this->integer(10)->defaultValue(0),
                    ], self::$_tableOptions
            );

            $this->createIndex('index_name', '{{%categories}}', ['name']);
            $this->createIndex('index_slug', '{{%categories}}', ['slug']);
        } catch (yii\console\Exception $ex) {
            echo $ex->getMessage();
            return false;
        }
        
        return true;
    }
    
    public function down()
    {
        $this->dropTable('{{%categories}}');
        echo "m160226_071354_milestone1_create_table_categories has been reverted.\n";

        return true;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
