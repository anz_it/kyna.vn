<?php

use yii\db\Migration;

/**
 * Handles the creation for table `faq_categories`.
 */
class m180205_032404_create_faq_categories_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('faq_categories', [
            'id' => $this->primaryKey(),
            'title' => $this->string()->notNull(),
            'image' => $this->string()->notNull(),
            'is_deleted' => $this->integer(11),
            'status' => $this->integer(11)->defaultValue(0),
            'created_time' => $this->integer(11),
            'updated_time' => $this->integer(11),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('faq_categories');
    }
}
