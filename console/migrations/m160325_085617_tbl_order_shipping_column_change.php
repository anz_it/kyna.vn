<?php

use yii\db\Migration;

class m160325_085617_tbl_order_shipping_column_change extends Migration
{
    public function up()
    {
        $this->renameColumn('{{%order_shipping}}', 'receiver_name', 'contact_name');
        return true;
    }

    public function down()
    {
        echo "m160325_085617_tbl_order_shipping_column_change cannot be reverted.\n";

        return true;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
