<?php

use yii\db\Migration;
use kyna\course\models\QuizQuestion;
use kyna\course\models\QuizAnswer;

class m161101_085952_add_column_order_to_question extends Migration
{
    public function up()
    {

        $this->addColumn(QuizQuestion::tableName(), 'order', 'int null default 0');


    }

    public function down()
    {
        echo "m161101_085952_add_column_order_to_question cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
