<?php

use yii\db\Migration;

class m181120_032718_init_campaign_voucher_free_ref extends Migration
{
    const USER_VOUCHER_FREE_TABLE_REF = "user_voucher_free_ref";
    public function up()
    {
        $this->createTable(self::USER_VOUCHER_FREE_TABLE_REF, [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull()->unique(),
            'total_introduce' => $this->integer(),
            'total_received' => $this->integer(),
            'total_consume' => $this->integer(),
            'created_time' => $this->integer(),
            'updated_time' => $this->integer(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
        ]);
        $this->createIndex('idx_user_id', self::USER_VOUCHER_FREE_TABLE_REF, ['user_id']);
    }

    public function down()
    {
        $this->dropIndex('idx_user_id',self::USER_VOUCHER_FREE_TABLE_REF);
        $this->dropTable(self::USER_VOUCHER_FREE_TABLE_REF);
    }
}
