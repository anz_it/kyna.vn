<?php

/**
 * Handles the creation for table `tbl_course_combo`.
 */
class m160518_031940_create_tbl_course_combo extends console\components\KynaMigration
{

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        // create table course_combos
        $this->createTable('{{%course_combos}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(1024)->notNull(),
            'short_name' => $this->string(45)->notNull(),
            'image_url' => $this->text(),
            'object' => $this->text(),
            'benefit' => $this->text(),
            'order' => $this->smallInteger(3)->defaultValue(0),
            'is_deleted' => "bit(1) DEFAULT b'0'",
            'status' => $this->smallInteger(3)->defaultValue(1),
            'created_time' => $this->integer(10)->defaultValue(0),
            'updated_time' => $this->integer(10)->defaultValue(0),
        ]);
        
        // add index for col name for searching
        $this->createIndex("index_name", '{{%course_combos}}', 'name');
        
        $this->createTableComboItems();
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('{{%course_combo_items}}');
        
        $this->dropTable('{{%course_combos}}');
        
        return true;
    }
    
    public function createTableComboItems()
    {
        // create table course_combos
        $this->createTable('{{%course_combo_items}}', [
            'id' => $this->primaryKey(),
            'course_combo_id' => $this->integer(11)->notNull(),
            'course_id' => $this->integer(11)->notNull(),
            'price' => $this->integer(11)->notNull(),
            'order' => $this->smallInteger(3)->defaultValue(0),
            'reg_total' => $this->integer(11)->defaultValue(0),
            'status' => $this->smallInteger(3)->defaultValue(1),
            'created_time' => $this->integer(10)->defaultValue(0),
            'updated_time' => $this->integer(10)->defaultValue(0),
        ]);
        
        // add index for col name for searching
        $this->createIndex("index_course_combo_id", '{{%course_combo_items}}', 'course_combo_id');
        $this->createIndex("index_course_id", '{{%course_combo_items}}', 'course_id');
    }

}
