<?php

use yii\db\Migration;

class m190107_101033_after_contact_name_column_order_shiping_table extends Migration
{
    public function up()
    {
        $this->alterColumn('{{%order_shipping}}', 'contact_name', $this->string(255));
    }

    public function down()
    {
        $this->alterColumn('{{%order_shipping}}', 'contact_name', $this->string(30));
    }
}
