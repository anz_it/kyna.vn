<?php

use yii\db\Migration;

class m181011_081121_add_to_shipper_time extends Migration
{
    public function up()
    {
        $this->addColumn('orders','to_shipper_time', $this->integer());

    }

    public function down()
    {
       $this->dropColumn('orders','to_shipper_time');
    }

    /*
    // Use safeUp/safeDown to run migration code within a tr[ansaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
