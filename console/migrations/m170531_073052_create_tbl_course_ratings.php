<?php

use yii\db\Migration;

class m170531_073052_create_tbl_course_ratings extends Migration
{
    public function up()
    {
        $this->createTable('{{%course_ratings}}', [
            'id' => $this->primaryKey(11),
            'course_id' => $this->integer(11)->notNull(),
            'user_id' => $this->integer(11)->notNull(),
            'score' => $this->float()->defaultValue(0),
            'score_of_content' => $this->float()->defaultValue(0),
            'score_of_video' => $this->float()->defaultValue(0),
            'score_of_teacher' => $this->float()->defaultValue(0),
            'review_content' => $this->text(),
            'is_cheat' => $this->smallInteger(1)->defaultValue(0),
            'cheat_name' => $this->string(32),
            'status' => $this->smallInteger(3)->defaultValue(0),
            'created_time' => $this->integer(10)->defaultValue(0),
            'updated_time' => $this->integer(10)->defaultValue(0)
        ]);

        $this->createIndex('index_unique_course_user_and_name', '{{%course_ratings}}', [
            'course_id',
            'user_id',
            'cheat_name',
        ], true);

        $this->addColumn('{{%courses}}', 'is_disable_seeding', $this->smallInteger(1)->defaultValue(0));
        $this->addColumn('{{%courses}}', 'rating', $this->float()->defaultValue(0));
        $this->addColumn('{{%courses}}', 'rating_users_count', $this->integer(10)->defaultValue(0));
    }

    public function down()
    {
        $this->dropColumn('{{%courses}}', 'rating_users_count');
        $this->dropColumn('{{%courses}}', 'rating');
        $this->dropColumn('{{%courses}}', 'is_disable_seeding');

        $this->dropTable('{{%course_ratings}}');
    }

}
