<?php

use yii\db\Migration;

class m161009_141210_alter_table_time_slot extends Migration
{
    public function up()
    {
        $this->alterColumn('{{%time_slot}}', 'monday', $this->text());
        $this->alterColumn('{{%time_slot}}', 'tuesday', $this->text());
        $this->alterColumn('{{%time_slot}}', 'wednesday', $this->text());
        $this->alterColumn('{{%time_slot}}', 'thursday', $this->text());
        $this->alterColumn('{{%time_slot}}', 'friday', $this->text());
        $this->alterColumn('{{%time_slot}}', 'saturday', $this->text());
        $this->alterColumn('{{%time_slot}}', 'sunday', $this->text());
    }

    public function down()
    {
        echo "m161009_141210_alter_table_time_slot cannot be reverted.\n";

        return true;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
