<?php

use yii\db\Migration;

class m170204_082815_create_table_taamkru_category extends Migration
{

    public function up()
    {
        $this->createTable('{{%taamkru_category}}', [
            'id' => $this->primaryKey(),
            'title' => $this->string()->notNull(),
            'course_id' => $this->integer(11)->notNull(),
            'key' => $this->string()->notNull(),
            'value' => $this->integer(11)->notNull()->unique(),
            'status' => $this->smallInteger(1)->defaultValue(0),
            'is_deleted' => $this->smallInteger(1)->defaultValue(0),
            'created_time' => $this->integer(10)->defaultValue(0),
            'updated_time' => $this->integer(10)->defaultValue(0),
        ]);
        $this->createIndex('idx_taamkru_category_value', '{{%taamkru_category}}', 'value');
    }

    public function down()
    {
        $this->dropIndex('idx_taamkru_category_value', '{{%taamkru_category}}');
        $this->dropTable('{{%taamkru_category}}');
        return true;
    }

}
