<?php

use yii\db\Migration;

class m161129_033352_alter_tbl_commission_incomes_add_col_fee extends Migration
{
    public function up()
    {
        $this->addColumn('{{%commission_incomes}}', 'fee', $this->float()->defaultValue(0) . " AFTER total_amount");
    }

    public function down()
    {
        $this->dropColumn('{{%commission_incomes}}', 'fee');

        return true;
    }

}
