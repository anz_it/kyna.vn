<?php

use yii\db\Migration;

class m160415_072852_update_order_fields extends Migration
{
    public function up()
    {
        $this->alterColumn('{{%orders}}', 'reference_id', $this->integer());
        $this->alterColumn('{{%time_slot}}', 'start_time', $this->integer());
        $this->alterColumn('{{%time_slot}}', 'end_time', $this->integer());

        return true;
    }

    public function down()
    {
        echo "m160415_072852_update_order_fields cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
