<?php

use yii\db\Migration;

class m160616_035701_alter_tbl_course_missions extends Migration
{

    public function safeUp()
    {
        $this->addColumn('{{%course_missions}}', 'lesson_id', 'INT(11) DEFAULT 0 AFTER section_id');
        $this->addColumn('{{%course_missions}}', 'is_deleted', 'BIT(1) DEFAULT 0 AFTER description');
    }

    public function safeDown()
    {
        $this->dropColumn('{{%course_missions}}', 'is_deleted');
        $this->dropColumn('{{%course_missions}}', 'lesson_id');
        return true;
    }

}
