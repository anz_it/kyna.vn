<?php

use yii\db\Migration;

class m160629_092818_alter_table_quiz_session extends Migration
{
    public function up()
    {
        $this->addColumn("{{%quiz_sessions}}", 'status', $this->smallInteger(2));
    }

    public function down()
    {
        return true;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
