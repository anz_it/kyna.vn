<?php

use yii\db\Migration;

class m170426_044928_alter_table_orders_add_colum_page_id extends Migration
{
    public function up()
    {
        $this->addColumn('{{%orders}}', 'page_id', $this->integer(10));
        $this->createIndex('index_page_id', '{{%orders}}', 'page_id');
    }

    public function down()
    {
        $this->dropIndex('index_page_id', '{{%orders}}');
        $this->dropColumn('{{%orders}}', 'page_id');
    }

}
