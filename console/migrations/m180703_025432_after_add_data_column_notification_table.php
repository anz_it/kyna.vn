<?php

use yii\db\Migration;

class m180703_025432_after_add_data_column_notification_table extends Migration
{
    public function up()
    {
        $this->dropColumn('notification', 'category');
        $this->dropColumn('notification', 'tag');
        $this->dropColumn('notification', 'course');
        $this->dropColumn('notification', 'search');
        $this->dropColumn('notification', 'webview');
        $this->addColumn('notification', 'data', $this->string(255)->comment('data notification'));
    }

    public function down()
    {
        $this->dropColumn('notification', 'data');
    }
}
