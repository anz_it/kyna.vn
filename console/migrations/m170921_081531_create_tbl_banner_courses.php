<?php

use yii\db\Migration;

class m170921_081531_create_tbl_banner_courses extends Migration
{
    public function up()
    {
        $this->createTable('banner_courses', [
            'id' => $this->primaryKey(11),
            'banner_id' => $this->integer(11)->notNull(),
            'course_id' => $this->integer(11)->notNull(),
        ]);

        $this->createIndex('index_unique_banner_and_course', 'banner_courses', ['banner_id', 'course_id'], true);
    }

    public function down()
    {
        $this->dropTable('banner_courses');
    }

}
