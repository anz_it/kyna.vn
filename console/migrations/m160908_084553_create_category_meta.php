<?php

use yii\db\Migration;

class m160908_084553_create_category_meta extends Migration
{
    public function up()
    {
        // [['meta_field_id'], 'required'],
        // [['value'], 'required', 'on' => 'required', 'message' => $this->metaField->name . ' không được để trống.'],
        // [['course_id', 'meta_field_id'], 'integer'],
        // [['value', 'key'], 'string']
        $this->createTable('category_meta', [
            'id' => $this->primaryKey(),
            'category_id' => $this->integer(),
            'meta_field_id' => $this->integer(),
            'key' => $this->string(50),
            'value' => $this->text(),
        ]);
        return true;
    }

    public function down()
    {
        echo "m160908_084553_create_category_meta cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
