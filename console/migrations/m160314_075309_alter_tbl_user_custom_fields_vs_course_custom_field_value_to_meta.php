<?php

use yii\db\Migration;

class m160314_075309_alter_tbl_user_custom_fields_vs_course_custom_field_value_to_meta extends Migration
{
    public function up()
    {
        $this->renameTable('{{%course_custom_field_values}}', '{{%course_meta}}');
        $this->addColumn('{{%course_meta}}', 'key', $this->string(50) . ' AFTER custom_field_id');
        $this->renameColumn('{{%course_meta}}', 'custom_field_id', 'meta_field_id');
        
        // rename table custom_fields to meta_fields
        $this->renameTable('{{%custom_fields}}', '{{%meta_fields}}');
        
        // rename table user meta
        $this->renameTable('{{%user_custom_field_values}}', '{{%user_meta}}');
        $this->addColumn('{{%user_meta}}', 'key', $this->string(50) . ' AFTER custom_field_id');
        $this->renameColumn('{{%user_meta}}', 'custom_field_id', 'meta_field_id');
    }

    public function down()
    {
        $this->renameTable('{{%meta_fields}}', '{{%custom_fields}}');
        
        $this->renameColumn('{{%course_meta}}', 'meta_field_id', 'custom_field_id');
        $this->dropColumn('{{%course_meta}}', 'key');
        $this->renameTable('{{%course_meta}}', '{{%course_custom_field_values}}');
        
        $this->renameColumn('{{%user_meta}}', 'meta_field_id', 'custom_field_id');
        $this->dropColumn('{{%user_meta}}', 'key');
        $this->renameTable('{{%user_meta}}', '{{%user_custom_field_values}}');
        
        echo "m160314_075309_alter_tbl_user_custom_fields_vs_course_custom_field_value_to_meta has been reverted.\n";

        return true;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
