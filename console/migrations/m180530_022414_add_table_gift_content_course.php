<?php

use yii\db\Migration;

class m180530_022414_add_table_gift_content_course extends Migration
{
    const GIFT_CONTENT_COURSE_TABLE = "{{%gift_content_course}}";
    public function up()
    {
        $this->createTable(self::GIFT_CONTENT_COURSE_TABLE, [
            'id' => $this->primaryKey(),
            'gift_content_id' => $this->integer(),
            'course_id' => $this->integer(),

        ]);
    }

    public function down()
    {
        echo "m180530_022414_add_table_gift_content_course cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
