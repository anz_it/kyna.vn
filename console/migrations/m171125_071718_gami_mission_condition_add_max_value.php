<?php

use yii\db\Migration;

class m171125_071718_gami_mission_condition_add_max_value extends Migration
{
    public function up()
    {
        $this->addColumn('mission_conditions', 'max_value', $this->integer(10));
    }

    public function down()
    {
        $this->dropColumn('mission_conditions', 'max_value');
    }
}
