<?php

use yii\db\Migration;

class m161007_094718_add_index_quiz_question_parent_id extends Migration
{
    public function up()
    {
        $this->createIndex('index_parent_id', '{{%quiz_questions}}', 'parent_id');
    }

    public function down()
    {
        $this->dropIndex('index_parent_id', '{{%quiz_questions}}');

        return true;
    }

}
