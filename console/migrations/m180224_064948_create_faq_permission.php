<?php

use yii\db\Migration;

/**
 * Class m180224_064948_create_faq_permission
 */
class m180224_064948_create_faq_permission extends Migration
{
    public function safeUp()
    {
        $auth = Yii::$app->authManager;

        //=============================================================================
        // add "Faq.View" permission
        $viewFaq = $auth->createPermission('Faq.View');
        $viewFaq->description = 'FAQ View';
        $auth->add($viewFaq);

        // add "Faq.Create" permission
        $createFaq = $auth->createPermission('Faq.Create');
        $createFaq->description = 'FAQ Create';
        $auth->add($createFaq);
        $auth->addChild($createFaq, $viewFaq);

        // add "Faq.Update" permission
        $updateFaq = $auth->createPermission('Faq.Update');
        $updateFaq->description = 'FAQ Update';
        $auth->add($updateFaq);
        $auth->addChild($updateFaq, $viewFaq);

        // add "Faq.Delete" permission
        $deleteFaq = $auth->createPermission('Faq.Delete');
        $deleteFaq->description = 'FAQ Delete';
        $auth->add($deleteFaq);
        $auth->addChild($deleteFaq, $viewFaq);
        $auth->addChild($deleteFaq, $updateFaq);

        // add "Faq.All" permission
        $allFaq = $auth->createPermission('Faq.All');
        $allFaq->description = 'FAQ All';
        $auth->add($allFaq);
        $auth->addChild($allFaq, $viewFaq);
        $auth->addChild($allFaq, $createFaq);
        $auth->addChild($allFaq, $updateFaq);
        $auth->addChild($allFaq, $deleteFaq);
        //=============================================================================

        // add "FaqAdmin" role
        $faqAdmin = $auth->createRole('FaqAdmin');
        $faqAdmin->description = 'The admin of a Faq module';
        $auth->add($faqAdmin);
        $auth->addChild($faqAdmin, $allFaq);

        // add permission for Admin role
        $admin = $auth->getRole('Admin');
        $auth->addChild($admin, $faqAdmin);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $auth = Yii::$app->authManager;

        $faqAdmin = $auth->getRole('FaqAdmin');
        $admin = $auth->getRole('Admin');
        $auth->removeChild($admin, $faqAdmin);
        $auth->remove($faqAdmin);

        $allFaq = $auth->getPermission('Faq.All');
        $deleteFaq = $auth->getPermission('Faq.Delete');
        $updateFaq = $auth->getPermission('Faq.Update');
        $createFaq = $auth->getPermission('Faq.Create');
        $viewFaq = $auth->getPermission('Faq.View');
        $auth->remove($allFaq);
        $auth->remove($deleteFaq);
        $auth->remove($updateFaq);
        $auth->remove($createFaq);
        $auth->remove($viewFaq);
    }
}
