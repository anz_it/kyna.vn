<?php

use yii\db\Migration;

class m171107_064414_add_common_setting__seo_fbofflineconversion_data_set_id extends Migration
{
    public function up()
    {
        $this->insert(
            'meta_fields',
            [
                'key' => 'seo_fbofflineconversion_data_set_id',
                'name' => 'SEO Facebook Offline Conversion Dataset Id',
                'type' => 1, // text
                'model' => 'setting',
                'extra_validate' => '',
                'data_set' => '',
                'note' => '',
                'is_unique' => false,
                'status' => 1,
                'created_time' => time(),
                'updated_time' => time(),
            ]
        );
    }

    public function down()
    {
        $this->delete(
            'setting_meta',
            [
                'key' => [
                    'seo_fbofflineconversion_data_set_id',
                ]
            ]
        );
        $this->delete(
            'meta_fields',
            [
                'key' => [
                    'seo_fbofflineconversion_data_set_id',
                ]
            ]
        );

//        echo "m171107_064414_add_common_setting__seo_fbofflineconversion_data_set_id cannot be reverted.\n";
//
//        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
