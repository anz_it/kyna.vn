<?php

use yii\db\Migration;
use kyna\course\models\CourseLearnerQna;

class m170920_022510_alter_tbl_course_learner_qna_add_is_on_behalf_of_teacher extends Migration
{
    public function up()
    {
        $qnaTblName = CourseLearnerQna::tableName();

        $this->addColumn($qnaTblName, 'is_on_behalf_of_teacher', "BIT(1) DEFAULT 0");
    }

    public function down()
    {
        $qnaTblName = CourseLearnerQna::tableName();

        $this->dropColumn($qnaTblName, 'is_on_behalf_of_teacher');
    }
}
