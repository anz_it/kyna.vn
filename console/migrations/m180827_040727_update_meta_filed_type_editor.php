<?php

use yii\db\Migration;

class m180827_040727_update_meta_filed_type_editor extends Migration
{

    public function up()
    {
        $sql = "update meta_fields set type = 15 where id in (68,32)";
        $this->execute($sql);
    }

    public function down()
    {
        $sql = "update meta_fields set type = 10 where id in (68,32)";
        $this->execute($sql);
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
