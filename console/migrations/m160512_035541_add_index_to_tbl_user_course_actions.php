<?php

/**
 * Handles adding index to table `tbl_user_course_actions`.
 */
class m160512_035541_add_index_to_tbl_user_course_actions extends \console\components\KynaMigration
{

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->dropIndex('index_user_activation_code', '{{%user_courses}}');
        
        $this->createIndex('index_user_id', '{{%user_course_actions}}', 'user_id');
        $this->createIndex('index_user_course_id', '{{%user_course_actions}}', 'user_course_id');
        $this->createIndex('index_user_course', '{{%user_course_actions}}', ['user_id', 'user_course_id']);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropIndex('index_user_id', '{{%user_course_actions}}');
        $this->dropIndex('index_user_course_id', '{{%user_course_actions}}');
        $this->dropIndex('index_user_course', '{{%user_course_actions}}');
    }

}
