<?php

use yii\db\Migration;

class m180418_102558_add_column_images_thumb_to_categories_table extends Migration
{
    public function up()
    {
        $this->addColumn('categories', 'images_thumb', $this->string(255));
    }

    public function down()
    {
        $this->dropColumn('categories', 'images_thumb');
    }
}
