<?php

use yii\db\Migration;

class m180615_032022_add_video_type_column extends Migration
{
    public function up()
    {
        $this->addColumn('course_lessons','video_type', $this->integer(1)->defaultValue(0)->comment('0 : wowza link, 1 : uiza id, 2: h5p id'));
    }

    public function down()
    {
        $this->dropColumn('course_lessons','video_type');

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
