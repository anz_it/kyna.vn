<?php

use yii\db\Migration;

class m171109_033716_alter_tbl_orders_add_col_user_telesale_id extends Migration
{
    public function up()
    {
        $this->addColumn('orders', 'user_telesale_id', $this->integer(11));
        $this->createIndex('index_user_telesale_id', 'orders', 'user_telesale_id');
    }

    public function down()
    {
        $this->dropIndex('index_user_telesale_id', 'orders');
        $this->dropColumn('orders', 'user_telesale_id');
    }

}
