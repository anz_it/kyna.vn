<?php

use yii\db\Migration;

class m160602_024607_create_tbl_course_commissions extends Migration
{

    public function safeUp()
    {
        $this->createTable('{{%course_commission_types}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(50)->notNull(),
            'start_percent' => $this->float(2)->defaultValue(0),
            'end_percent' => $this->float(2)->defaultValue(100),
            'default_percent' => $this->float(2)->defaultValue(0),
            'is_required_expiration_date' => "BIT(1) default 0",
            'is_multiple_commissions' => "BIT(1) default 1",
            'status' => $this->smallInteger(3)->defaultValue(1),
            'created_time' => $this->integer(10)->defaultValue(0),
            'updated_time' => $this->integer(10)->defaultValue(0)
        ]);
        
        $this->addColumn('{{%courses}}', 'course_commission_type_id', "INT(11) not null after category_id");
        
        $this->createTable('{{%course_commissions}}', [
            'id' => $this->primaryKey(),
            'course_id' => $this->integer(11)->notNull(),
            'commission_percent' => $this->float(2)->defaultValue(0),
            'apply_from_date' => $this->integer(10)->defaultValue(0),
            'apply_to_date' =>$this->integer(10)->defaultValue(0),
            'is_default' => "BIT(1) default 0",
            'is_deleted' => "BIT(1) default 0",
            'status' => $this->smallInteger(3)->defaultValue(1),
            'created_time' => $this->integer(10)->defaultValue(0),
            'updated_time' => $this->integer(10)->defaultValue(0)
        ]);
        
        $this->createIndex('index_course_id', '{{%course_commissions}}', 'course_id');
        $this->createIndex('index_apply_from_date', '{{%course_commissions}}', 'apply_from_date');
        $this->createIndex('index_apply_to_date', '{{%course_commissions}}', 'apply_to_date');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('{{%course_commissions}}');
        
        $this->dropColumn('{{%courses}}', 'course_commission_type_id');
        
        $this->dropTable('{{%course_commission_types}}');
    }

}
