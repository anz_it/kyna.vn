<?php

use yii\db\Migration;
use kyna\tag\models\Tag;

class m171009_032735_alter_table_tags_edit_length_column_description extends Migration
{
    public function up()
    {
        $tagTblName = Tag::tableName();

        $this->alterColumn($tagTblName, 'description', $this->text());
    }

    public function down()
    {
        $tagTblName = Tag::tableName();

        $this->alterColumn($tagTblName, 'description', $this->string());
    }
}
