<?php

use yii\db\Migration;

class m170209_102836_create_table_taamkru_transaction extends Migration
{
    public function up()
    {
        $this->createTable('{{%taamkru_transaction}}', [
            'id' => $this->primaryKey(),
            'order_id' => $this->integer(11),
            'code' => $this->string(50)->notNull(),
            'category_id' => $this->integer(11)->notNull(),
            'num_activations' => $this->integer(11)->notNull(),
            'code_expiration' => $this->integer(10)->defaultValue(0),
            'product_expiration' => $this->integer(10)->defaultValue(0),
            'status' => $this->smallInteger(3)->defaultValue(0),
            'message' => $this->string(),
            'created_by' => $this->integer(11)->notNull(),
            'created_time' => $this->integer(10)->defaultValue(0),
            'updated_time' => $this->integer(10)->defaultValue(0),
        ]);
        $this->createIndex('idx_taamkru_transaction_code', '{{%taamkru_transaction}}', 'code');
    }
    public function down()
    {
        $this->dropIndex('idx_taamkru_transaction_code', '{{%taamkru_transaction}}');
        $this->dropTable('{{%taamkru_transaction}}');
        return true;
    }
}
