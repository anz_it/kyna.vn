<?php

use yii\db\Migration;

class m160528_041238_alter_table_order extends Migration
{
    public function up()
    {
        $this->renameColumn("{{%orders}}", "coupon_code", "promotion_code");
    }

    public function down()
    {
        return true;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
