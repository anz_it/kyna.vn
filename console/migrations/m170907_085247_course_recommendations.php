<?php

use yii\db\Migration;

class m170907_085247_course_recommendations extends Migration
{
    public function up()
    {
        $this->createTable(
            'recommendation_courses',
            [
                'id' => $this->primaryKey(11),
                'course_id' => $this->integer(11)->unique(),
                'hot_score_30d' => $this->float()->defaultValue(0),
                'sell_score_30d' => $this->float()->defaultValue(0),

                'created_time' => $this->integer(10),
                'updated_time' => $this->integer(10),
            ]
        );

        $this->createTable(
            'recommendation_categories',
            [
                'id' => $this->primaryKey(11),
                'category_id' => $this->integer(11)->unique(),
                'hot_score_30d' => $this->float()->defaultValue(0),
                'sell_score_30d' => $this->float()->defaultValue(0),

                'created_time' => $this->integer(10),
                'updated_time' => $this->integer(10),
            ]
        );
    }

    public function down()
    {

        $this->dropTable('recommendation_courses');

        $this->dropTable('recommendation_categories');

//        echo "m170907_085247_course_recommendations cannot be reverted.\n";
//
//        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
