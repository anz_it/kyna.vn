<?php

use console\components\KynaMigration;

class m160229_030201_milestone1_alter_categories_add_parent_and_level extends KynaMigration
{

    public function up()
    {
        $this->addColumn('{{%categories}}', 'parent_id', $this->integer(11)->defaultValue(0) . " AFTER id");
        $this->addColumn('{{%categories}}', 'level', $this->smallInteger(3)->defaultValue(0) . " AFTER slug");
    }

    public function down()
    {
        $this->dropColumn('{{%categories}}', 'parent_id');
        $this->dropColumn('{{%categories}}', 'level');
        
        echo "m160229_030201_milestone1_alter_categories_add_parent_and_level has been reverted.\n";

        return true;
    }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
