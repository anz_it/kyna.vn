<?php

use yii\db\Migration;

class m180301_071921_create_table_course_opinions extends Migration
{
    public function up()
    {
        $this->createTable('course_opinions', [
            'id' => $this->primaryKey(),
            'course_id' => $this->integer()->notNull(),
            'description'=>$this->text()->notNull(),
            'user_name' => $this->text()->notNull(),
            'avatar_url' => $this->text(),
            'status' => $this->smallInteger(1)->defaultValue(1),
            'is_deleted' => $this->smallInteger(1)->defaultValue(0),
        ]);

        $this->createIndex('idx_course_opinion_course_id','course_opinions', 'course_id');
    }

    public function down()
    {
        echo "m171020_161740_add_table_course_opinions cannot be reverted.\n";
        $this->dropIndex('idx_course_opinion_course_id','course_opinions');
        $this->dropTable('course_opinions');

    }
}
