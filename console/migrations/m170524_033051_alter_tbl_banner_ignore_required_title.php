<?php

use yii\db\Migration;

class m170524_033051_alter_tbl_banner_ignore_required_title extends Migration
{
    public function up()
    {
        $this->alterColumn('{{%banners}}', 'title', $this->string(50));
    }

    public function down()
    {
        $this->alterColumn('{{%banners}}', 'title', $this->string(50)->notNull());
    }
}
