<?php

use yii\db\Migration;

class m180604_081321_alter_table_payment_transaction_increase_transaction_code_length extends Migration
{
    public function up()
    {
        $this->alterColumn('payment_transactions', 'transaction_code', $this->string(50));
    }

    public function down()
    {
        $this->alterColumn('payment_transactions', 'transaction_code', $this->string(30));
    }
}
