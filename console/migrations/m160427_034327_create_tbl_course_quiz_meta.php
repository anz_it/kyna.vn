<?php

class m160427_034327_create_tbl_course_quiz_meta extends \console\components\KynaMigration
{

    public function safeUp()
    {
        $this->createTable('{{%course_quiz_meta}}', [
                'id' => $this->primaryKey(),
                'course_id' => $this->integer(11)->notNull(),
                'quiz_id' => $this->integer(11)->notNull(),
                'meta_field_id' => $this->integer(11)->defaultValue(0),
                'key' => $this->string(50)->notNull(),
                'value' => $this->text(),
            ], self::$_tableOptions
        );

        $this->createIndex('index_course_id', '{{%course_quiz_meta}}', ['course_id']);
        $this->createIndex('index_quiz_id', '{{%course_quiz_meta}}', ['quiz_id']);
        $this->createIndex('index_custom_field_id', '{{%course_quiz_meta}}', ['meta_field_id']);
    }

    public function safeDown()
    {
        $this->dropTable('{{%course_quiz_meta}}');
        
        return true;
    }

}
