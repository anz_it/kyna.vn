<?php

use yii\db\Migration;

class m160409_074426_add_operator_id_to_order extends Migration
{
    public function up()
    {
        $this->addColumn('{{%orders}}', 'operator_id', $this->integer());
        return true;
    }

    public function down()
    {
        return true;
    }
}
