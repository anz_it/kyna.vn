<?php

use yii\db\Migration;

class m160609_033811_alter_tbl_user_meta_set_default_value_for_col_meta_field_id extends Migration
{

    public function up()
    {
        $this->alterColumn('{{%user_meta}}', 'meta_field_id', $this->integer(11)->defaultValue(0));
    }

    public function down()
    {
        return true;
    }

}
