<?php

use yii\db\Migration;

class m160525_103637_alter_table_promotions extends Migration
{
    public function up()
    {
        $this->addColumn("{{%promotions}}", 'note', $this->string(100));
        $this->addColumn("{{%promotions}}", 'min_amount', $this->integer(7)->defaultValue(0));
    }

    public function down()
    {
        return true;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
