<?php

use yii\db\Migration;

// 1. create permission Facebook.All, Facebook.FBOfflineConversion.All, Facebook.FBOfflineConversion.Upload
// 2. add auth_item_child: Facebook.All -> Facebook.FBOfflineConversion.All -> Facebook.FBOfflineConversion.Upload

class m171030_084432_add_permission_fboffineconversion extends Migration
{
    public function up()
    {
        // 1.
        Yii::$app->authManager->add(Yii::$app->authManager->createPermission('Facebook.All'));
        Yii::$app->authManager->add(Yii::$app->authManager->createPermission('Facebook.FBOfflineConversion.All'));
        Yii::$app->authManager->add(Yii::$app->authManager->createPermission('Facebook.FBOfflineConversion.Upload'));

        // 2.
        Yii::$app->authManager->addChild(
            Yii::$app->authManager->getPermission('Facebook.FBOfflineConversion.All'),
            Yii::$app->authManager->getPermission('Facebook.FBOfflineConversion.Upload')
        );

        Yii::$app->authManager->addChild(
            Yii::$app->authManager->getPermission('Facebook.All'),
            Yii::$app->authManager->getPermission('Facebook.FBOfflineConversion.All')
        );

        Yii::$app->authManager->addChild(
            Yii::$app->authManager->getRole('Marketing'),
            Yii::$app->authManager->getPermission('Facebook.All')
        );
    }

    public function down()
    {
        // 2.
        Yii::$app->authManager->removeChild(
            Yii::$app->authManager->getPermission('Facebook.FBOfflineConversion.All'),
            Yii::$app->authManager->getPermission('Facebook.FBOfflineConversion.Upload')
        );

        Yii::$app->authManager->removeChild(
            Yii::$app->authManager->getPermission('Facebook.All'),
            Yii::$app->authManager->getPermission('Facebook.FBOfflineConversion.All')
        );

        Yii::$app->authManager->removeChild(
            Yii::$app->authManager->getRole('Marketing'),
            Yii::$app->authManager->getPermission('Facebook.All')
        );

        // 1.
        Yii::$app->authManager->remove(Yii::$app->authManager->getPermission('Facebook.FBOfflineConversion.Upload'));
        Yii::$app->authManager->remove(Yii::$app->authManager->getPermission('Facebook.FBOfflineConversion.All'));
        Yii::$app->authManager->remove(Yii::$app->authManager->getPermission('Facebook.All'));

//        echo "m171030_084432_add_permission_fboffineconversion cannot be reverted.\n";
//
//        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
