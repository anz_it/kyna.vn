<?php

use yii\db\Migration;

class m160603_041218_alter_table_settings extends Migration
{
    public function up()
    {
        $this->renameTable("{{%settings}}", "{{%setting_meta}}");
        $this->addColumn("{{%setting_meta}}", "meta_field_id", $this->integer());
    }

    public function down()
    {
        echo "m160603_041218_alter_table_settings cannot be reverted.\n";

        return true;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
