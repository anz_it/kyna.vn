<?php

use yii\db\Migration;

class m160617_095521_alter_tbl_course_add_v2_combo_id_and_set_default_for_slug extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('{{%courses}}', 'v2_group_promotion_id', 'INT(11) DEFAULT 0 AFTER position');
        
        $this->alterColumn('{{%courses}}', 'slug', $this->text());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('{{%courses}}', 'v2_group_promotion_id');
        
        return true;
    }
}
