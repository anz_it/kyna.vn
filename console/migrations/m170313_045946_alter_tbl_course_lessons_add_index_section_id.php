<?php

use yii\db\Migration;

class m170313_045946_alter_tbl_course_lessons_add_index_section_id extends Migration
{
    public function up()
    {
        $this->createIndex('index_section_id', '{{%course_lessons}}', 'section_id');
    }

    public function down()
    {
        $this->dropIndex('index_section_id', '{{%course_lessons}}');

        return true;
    }

}
