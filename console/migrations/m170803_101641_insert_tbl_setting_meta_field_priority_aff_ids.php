<?php

use yii\db\Migration;
use kyna\base\models\MetaField;

class m170803_101641_insert_tbl_setting_meta_field_priority_aff_ids extends Migration
{
    public function up()
    {
        $this->insert(MetaField::tableName(), [
            'key' => 'priority_aff_ids',
            'name' => 'Danh sách affiliate id không bị ghi đè bởi Kyna Other Display',
            'type' => 1,
            'model' => MetaField::MODEL_SETTING,
            'status' => MetaField::STATUS_ACTIVE
        ]);
    }

    public function down()
    {
        $this->delete(MetaField::tableName(), [
            'key' => 'priority_aff_ids'
        ]);
    }
}
