<?php

use yii\db\Migration;

class m180103_025812_alter_tbl_user_is_kynabiz_and_run_init_data extends Migration
{
    public function up()
    {
        $this->addColumn(
            'user',
            'is_kynabiz',
            $this->boolean()->defaultValue(false)->comment('is kynabiz user')
        );

        $emails = [
            'anbh@phuckhang.vn',
            'anhdh@phuckhang.vn',
            'baohq@phuckhang.vn',
            'binhtt@phuckhang.vn',
            'canhnd@phuckhang.vn',
            'chidtl@phuckhang.vn',
            'congcc@phuckhang.vn',
            'dangqc@phuckhang.vn',
            'danhlh@phuckhang.vn',
            'dungnth@phuckhang.vn',
            'duonglth@phuckhang.vn',
            'duynhx@phuckhang.vn',
            'hahtt@phuckhang.vn',
            'hailh@phuckhang.vn',
            'haipx@phuckhang.vn',
            'hienptm@phuckhang.vn',
            'hieuntx@phuckhang.vn',
            'hoaibq@phuckhang.vn',
            'hoaibt@phuckhang.vn',
            'hoanx@phuckhang.vn',
            'huett@phuckhang.vn',
            'hunghq@phuckhang.vn',
            'hungpq@phuckhang.vn',
            'khatt@phuckhang.vn',
            'khoadm@phuckhang.vn',
            'luongmdn@phuckhang.vn',
            'lynnh@phuckhang.vn',
            'lyntc@phuckhang.vn',
            'nganmtn@phuckhang.vn',
            'nganntt@phuckhang.vn',
            'nhamtq@phuckhang.vn',
            'nhutlt@phuckhang.vn',
            'nolmx@phuckhang.vn',
            'oanhvt@phuckhang.vn',
            'phatpt@phuckhang.vn',
            'phidh@phuckhang.vn',
            'sonnt@phuckhang.vn',
            'sungut@phuckhang.vn',
            'suongntt@phuckhang.vn',
            'tamhtt@phuckhang.vn',
            'tamnl@phuckhang.vn',
            'tamntn@phuckhang.vn',
            'tannv@phuckhang.vn',
            'thanhlh@phuckhang.vn',
            'thanhnt@phuckhang.vn',
            'thiln@phuckhang.vn',
            'thonna@phuckhang.vn',
            'thunv@phuckhang.vn',
            'thuongdh@phuckhang.vn',
            'thuongna@phuckhang.vn',
            'toantv@phuckhang.vn',
            'trangltn@phuckhang.vn',
            'tungpt@phuckhang.vn',
            'xuyenttm@phuckhang.vn',
            'yenvtb@phuckhang.vn',

            // Phim Cach Nhiet
            'mytrang@phimcachnhiet.com.vn',
            'lethuy@phimcachnhiet.com.vn',
            'dieulinh@phimcachnhiet.com.vn',
            'oanhpt@phimcachnhiet.com.vn',
            'tienvinh@phimcachnhiet.com.vn',
            'quanghuong@phimcachnhiet.com.vn',
            'nguyenhao@phimcachnhiet.com.vn',
            'quangtuyen@phimcachnhiet.com.vn',
            'thanhtu@phimcachnhiet.com.vn',
            'kimphuc@phimcachnhiet.com.vn',
            'phanhung@phimcachnhiet.com.vn',
            'nguyenhien@phimcachnhiet.com.vn',
            'vietan@phimcachnhiet.com.vn',
            'dinhle@phimcachnhiet.com.vn',
            'nguyenlac@phimcachnhiet.com.vn',
            'chienkd@phimcachnhiet.com.vn',
            'vanbinh@phimcachnhiet.com.vn',
            'thinkwarevietnam@gmail.com',
            'quangtiem@phimcachnhiet.com.vn',
            'hongson@phimcachnhiet.com.vn',
            'quyloc@phimcachnhiet.com.vn',
            'phuonglan@phimcachnhiet.com.vn',
            'phuongthao@phimcachnhiet.com.vn',
            'uyen@phimcachnhiet.com.vn',
            'chuly@phimcachnhiet.com.vn',
            'ngocnam@phimcachnhiet.com.vn',
            'thevow07@gmail.com',
            'kieutrang@phimcachnhiet.com.vn',
            'nguyenmanhvinh81@gmail.com',
            // mia duong
            'thanglh@attapeusugar.la',
            'als.head@attapeusugar.la',
            'lynts@attapeusugar.la',
            'acc.dep-mgr@attapeusugar.la',
            'thombt@attapeusugar.la',

            // LIN
            'huynhtinhhoainhan@gmail.com',
            'quyhyvong2017@gmail.com',
            'phamttduong@gmail.com',
            'haothai9@gmail.com',
            'nguyenthiphuongdung69@gmail.com',
            'baokhuyen.nguyen@gmail.com',
            'dinhhoang.minhngoc@gmail.com',
            'dminhphan@gmail.com',
            'nguyenyenphuc.awo@gmail.com',
            'krac2010@gmail.com',
            'thienanffsc@gmail.com',
            'thuvienduoibongcay@gmail.com',
            'thuynguyet72@gmail.com',
            'sahuy368@gmail.com',
            'tongtrongnhan@gmail.com',
            'mrluanmao@gmail.com',
            'kimthuyenkid@gmail.com',
            'arrowofsun@gmail.com',
        ];

        $this->update('user', ['is_kynabiz' => true], ['email' => $emails]);


        Yii::$app->db->createCommand("UPDATE user_points up LEFT JOIN user u ON u.id = up.user_id SET up.k_point = 0, up.k_point_usable = 0 WHERE u.email IN ('" . implode("','", $emails) . "')")->execute();
    }

    public function down()
    {
        $this->dropColumn(
            'user',
            'is_kynabiz'
        );
    }

}
