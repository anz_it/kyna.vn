<?php

use yii\db\Migration;

/**
 * Handles adding col_type to table `location_table`.
 */
class m160713_035821_add_col_type_to_location_table extends Migration
{

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('{{%locations}}', 'type', $this->smallInteger(3)->defaultValue(1));
        
        $this->createTable('{{%shipping_settings}}', [
            'id' => $this->primaryKey(),
            'city_id' => $this->integer(11),
            'type' => $this->smallInteger(3)->notNull(),
            'fee' => $this->integer(10)->notNull()->defaultValue(0),
            'min_amount' => $this->integer(10)->notNull()->defaultValue(0),
            'is_deleted' => "BIT(1) DEFAULT 0",
            'status' => $this->smallInteger(3)->defaultValue(1),
            'created_time' => $this->integer(10)->defaultValue(0),
            'updated_time' => $this->integer(10)->defaultValue(0),
        ]);
        
        $this->createIndex('index_city_id', '{{%shipping_settings}}', 'city_id');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('{{%locations}}', 'type');
        
        $this->dropTable('{{%shipping_settings}}');
    }

}
