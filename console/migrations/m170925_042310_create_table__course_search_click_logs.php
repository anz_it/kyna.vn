<?php

use yii\db\Migration;

class m170925_042310_create_table__course_search_click_logs extends Migration
{
    public function up()
    {
        $this->createTable(
            'course_search_click_logs',
            [
                'id' => $this->primaryKey(11),
                'user_id' => $this->integer(11),
                'search_type' => $this->integer(1)->comment('0: live search, 1: full search, 3: facet'),     // 0: live search, 1: full search, 3: facet
                'search_value' => $this->text(),
                'course_id' => $this->integer(11)->notNull(),
                'created_time' => $this->integer(10),
                'updated_time' => $this->integer(10)
            ]
        );
    }

    public function down()
    {
        $this->dropTable(
            'course_search_click_logs'
        );
//        echo "m170925_042310_create_table__course_search_click_logs cannot be reverted.\n";

//        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
