<?php

use console\components\KynaMigration;

class m160227_034124_milestone1_create_shipping_methods extends KynaMigration
{
    public function up()
    {
        // create table `shipping_methods`
        $this->createTableShippingMethod();
        
        // create table `shipping_method_pricing`
        $this->createTableShippingMethodPricing();
    }

    public function down()
    {
        $this->dropTableShippingMethodPricing();
        
        $this->dropTableShippingMethod();
        echo "m160227_034124_milestone1_create_shipping_methods has been reverted.\n";

        return true;
    }
    
    private function createTableShippingMethod()
    {
        try {
            $this->createTable('{{%shipping_methods}}', [
                'id' => $this->primaryKey(),
                'shipping_vendor_id' => $this->integer(11)->notNull(),
                'name' => $this->string(45)->notNull(),
                'vendor_method_id' => $this->integer(11)->defaultValue(0),
                'is_auto_calculate' => "bit(1) DEFAULT b'0'",
                'status' => $this->smallInteger(3)->defaultValue(1),
                'created_time' => $this->integer(10)->defaultValue(0),
                'updated_time' => $this->integer(10)->defaultValue(0),
                    ], self::$_tableOptions
            );

            $this->createIndex('index_shipping_vendor_id', '{{%shipping_methods}}', ['shipping_vendor_id']);
            $this->createIndex('index_vendor_method_id', '{{%shipping_methods}}', ['vendor_method_id']);
        } catch (yii\console\Exception $ex) {
            echo $ex->getMessage();
            return false;
        }
        
        return true;
    }
    
    private function dropTableShippingMethod()
    {
        try {
            $this->dropIndex('index_vendor_method_id', '{{%shipping_methods}}');
            $this->dropIndex('index_shipping_vendor_id', '{{%shipping_methods}}');

            $this->dropTable('{{%shipping_methods}}');
        } catch (yii\console\Exception $ex) {
            echo $ex->getMessage();
            return false;
        }
        
        return true;
    }
    
    private function createTableShippingMethodPricing()
    {
        try {
            $this->createTable('{{%shipping_method_pricing}}', [
                'id' => $this->primaryKey(),
                'shipping_method_id' => $this->integer(11)->notNull(),
                'dispatch_location_id' => $this->integer(11)->defaultValue(0),
                'destination_location_id' => $this->integer(11)->defaultValue(0),
                'fee' => $this->double()->defaultValue(0),
                'begin_date' => $this->integer(10)->defaultValue(0),
                'end_date' => $this->integer(10)->defaultValue(0),
                'status' => $this->smallInteger(3)->defaultValue(1),
                'created_time' => $this->integer(10)->defaultValue(0),
                'updated_time' => $this->integer(10)->defaultValue(0),
                    ], self::$_tableOptions
            );

            $this->createIndex('index_shipping_method_id', '{{%shipping_method_pricing}}', ['shipping_method_id']);
            $this->createIndex('index_dispatch_location_id', '{{%shipping_method_pricing}}', ['dispatch_location_id']);
            $this->createIndex('index_destination_location_id', '{{%shipping_method_pricing}}', ['destination_location_id']);
        } catch (yii\console\Exception $ex) {
            echo $ex->getMessage();
            return false;
        }
        
        return true;
    }
    
    private function dropTableShippingMethodPricing()
    {
        try {
            $this->dropIndex('index_destination_location_id', '{{%shipping_method_pricing}}');
            $this->dropIndex('index_dispatch_location_id', '{{%shipping_method_pricing}}');
            $this->dropIndex('index_shipping_method_id', '{{%shipping_method_pricing}}');

            $this->dropTable('{{%shipping_method_pricing}}');
        } catch (yii\console\Exception $ex) {
            echo $ex->getMessage();
            return false;
        }
        
        return true;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
