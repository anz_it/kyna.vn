<?php

use yii\db\Migration;

class m180326_041324_create_table_push_notification extends Migration
{
    const PUSH_NOTIFICATION_TABLE = '{{%push_notification}}';
    public function up()
    {
        $this->createTable(self::PUSH_NOTIFICATION_TABLE, [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'token' => $this->string(255)->null(),
            'os_type' => $this->string(255)->null(),
            'device_model' => $this->string(255)->null(),
            'created_time' => $this->integer(),
            'updated_time' => $this->integer(),

        ]);
    }

    public function down()
    {
        echo "m180326_041324_create_table_push_notification cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
