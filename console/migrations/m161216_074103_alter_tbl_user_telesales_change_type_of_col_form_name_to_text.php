<?php

use yii\db\Migration;

class m161216_074103_alter_tbl_user_telesales_change_type_of_col_form_name_to_text extends Migration
{
    public function up()
    {
        $this->alterColumn('{{%user_telesales}}', 'form_name', $this->text());
    }

    public function down()
    {
        $this->alterColumn('{{%user_telesales}}', 'form_name', $this->string(250));

        return true;
    }

}
