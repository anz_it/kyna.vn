<?php

use yii\db\Migration;

class m160316_103408_init_first_data_for_rbac extends Migration
{
    public function up()
    {
        $sql = "
            INSERT INTO `auth_item` VALUES 
                ('Admin',1,'',NULL,NULL,1458117542,1458118217),
                ('Analyzer',1,'',NULL,NULL,1458123896,1458123896),
                ('Course.Category.Create',2,'',NULL,NULL,1458122696,1458122733),
                ('Course.Category.Delete',2,'',NULL,NULL,1458122715,1458122742),
                ('Course.Category.Update',2,'',NULL,NULL,1458122706,1458122747),
                ('Course.Category.View',2,'',NULL,NULL,1458122725,1458122725),
                ('Course.Create',2,'',NULL,NULL,1458118172,1458118768),
                ('Course.Delete',2,'',NULL,NULL,1458118191,1458118684),
                ('Course.Update',2,'',NULL,NULL,1458118181,1458118695),
                ('Course.View',2,'',NULL,NULL,1458118674,1458120345),
                ('CustomerService',1,'',NULL,NULL,1458123916,1458123916),
                ('CustomerServiceHaNoi',1,'',NULL,NULL,1458123970,1458123970),
                ('Marketing',1,'',NULL,NULL,1458123943,1458123943),
                ('Module.Course',2,'Module Course Management',NULL,NULL,1458118108,1458123644),
                ('Relation',1,'',NULL,NULL,1458123954,1458123954),
                ('Teacher',1,'',NULL,NULL,1458117529,1458117529),
                ('Telesale',1,'',NULL,NULL,1458117563,1458117563),
                ('TelesaleLeader',1,'',NULL,NULL,1458117582,1458117582),
                ('User',1,'Normal User',NULL,NULL,1458117505,1458117505),
                ('Video',1,'',NULL,NULL,1458123933,1458123933);
            ";
        
        $this->execute($sql);
        
        $sql = "
            INSERT INTO `auth_item_child` VALUES 
                ('Module.Course','Course.Category.Create'),
                ('Module.Course','Course.Category.Delete'),
                ('Module.Course','Course.Category.Update'),
                ('Course.Category.Create','Course.Category.View'),
                ('Course.Category.Delete','Course.Category.View'),
                ('Course.Category.Update','Course.Category.View'),
                ('Module.Course','Course.Create'),
                ('Module.Course','Course.Delete'),
                ('Module.Course','Course.Update'),
                ('Course.Create','Course.View'),
                ('Course.Delete','Course.View'),
                ('Course.Update','Course.View'),
                ('Admin','Module.Course');
            ";
        
        $this->execute($sql);
    }

    public function down()
    {
        $this->truncateTable("{{%auth_item_child}}");
        $this->delete("{{%auth_item}}");
        
        echo "m160316_103408_init_first_data_for_rbac has been reverted.\n";

        return true;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
