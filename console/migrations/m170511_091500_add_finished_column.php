<?php

use yii\db\Migration;

class m170511_091500_add_finished_column extends Migration
{
    public function up()
    {
        $tableName = \kyna\user\models\UserCourse::tableName();
        $this->addColumn($tableName, "finished", "int default 0");
        $this->execute("update $tableName uc join courses c on c.id = uc.course_id set uc.finished = 1 where  uc.process >= c.percent_can_pass ");

    }

    public function down()
    {
       $this->dropColumn(\kyna\user\models\UserCourse::tableName(), "finished");

    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
