<?php

use yii\db\Migration;

class m190222_020702_addKidAffiliateIncome extends Migration
{
    const TABLE_KID_AFFILIATE_INCOME = 'kid_affiliate_income';
    public function up()
    {
        $this->createTable(self::TABLE_KID_AFFILIATE_INCOME, [
            'id' => $this->primaryKey(),
            'order_id' => $this->integer()->notNull(),
            'affiliate_id' => $this->integer()->notNull(),
            'category' => $this->text(),
            'order_status_id' => $this->integer(),
            'order_status' => $this->string(),
            'register_email' => $this->string(),
            'register_name' => $this->string(),
            'register_phone_number' => $this->string(20),
            'total_amount' => $this->decimal(13,2),
            'affiliate_commission_amount'  => $this->decimal(13,2),
            'description' => $this->text(),
            'created_time' => $this->integer(),
            'updated_time' => $this->integer()
        ]);

    }

    public function down()
    {
       $this->dropTable(self::TABLE_KID_AFFILIATE_INCOME);
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
