<?php

use yii\db\Migration;

class m170117_033055_alter_tbl_user_courses_add_last_lesson extends Migration
{

    public function up()
    {
        $this->addColumn('{{%user_courses}}', 'last_passed_lesson_id', $this->integer(10));

        $this->createTable('{{%user_course_lessons}}', [
            'id' => $this->primaryKey(),
            'user_course_id' => $this->integer(10)->notNull(),
            'course_lesson_id' => $this->integer(10)->notNull(),
            'start_time' => $this->integer(10),
            'is_passed' => "BIT(1) DEFAULT 0",
            'day_can_learn' => $this->integer(5),
            'passed_time' => $this->integer(10),
            'process' => $this->float(),
            'created_time' => $this->integer(10),
            'updated_time' => $this->integer(10),
            'status' => $this->smallInteger(3)->defaultValue(1),
        ]);

        $this->createIndex('index_user_course_lesson', '{{%user_course_lessons}}', ['user_course_id', 'course_lesson_id']);
    }

    public function down()
    {
        $this->dropColumn('{{%user_courses}}', 'last_passed_lesson_id');

        $this->dropTable('{{%user_course_lessons}}');

        return true;
    }

}
