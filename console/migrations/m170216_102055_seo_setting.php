<?php

use yii\db\Migration;

class m170216_102055_seo_setting extends Migration
{
    public function up()
    {

        $this->execute("
          CREATE TABLE `seo_setting_meta` (
          `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
          `key` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
          `value` text COLLATE utf8_unicode_ci,
          PRIMARY KEY (`id`),
          UNIQUE KEY `key_unique` (`key`)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
        ");
    }

    public function down()
    {
        echo "m170216_102055_seo_setting cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
