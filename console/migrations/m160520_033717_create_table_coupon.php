<?php

use yii\db\Migration;

/**
 * Handles the creation for table `table_coupon`.
 */
class m160520_033717_create_table_coupon extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('{{%coupons}}', [
            'id'                    => $this->primaryKey(),
            'coupon_code'           => $this->string(32)->notNull(),
            'start_date'            => $this->integer(10)->defaultValue(0),
            'end_date'              => $this->integer(10)->defaultValue(0),
            'coupon_type'           => $this->integer(2)->notNull(),
            'coupon_price'          => $this->integer(15)->notNull(),
            'coupon_quantity'       => $this->integer()->notNull(),
            'coupon_reg_number'     => $this->integer()->notNull(),
            'partner_id'            => $this->integer(),
            'status'                => $this->smallInteger(3)->defaultValue(1),
            'is_deleted'            => "bit(1) DEFAULT b'0'",
            'created_time'          => $this->integer(10)->defaultValue(0),
            'updated_time'          => $this->integer(10)->defaultValue(0),
        ]);
//        $this->addForeignKey('fk_partner', "{{%coupons}}", 'partner_id', "{{%partner}}", 'id');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%coupons}}');
        
        return true;
    }
}
