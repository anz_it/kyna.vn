<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-console',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'console\controllers',
    'components' => [
        'request' => null,
        'user' => null,
        'session' => null,
        'log' => [
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning', 'info'],
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'categories' => ['order'],
                    'logFile' => '@app/runtime/logs/order/order.log',
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'categories' => ['cod'],
                    'logFile' => '@app/runtime/logs/order/cod.log',
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'logVars' => [],
                    'categories' => ['commission'],
                    'logFile' => '@app/runtime/logs/order/commission.log',
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'logVars' => [],
                    'categories' => ['notification'],
                    'logFile' => '@app/runtime/logs/notification/notification.log',
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['info'],
                    'logVars' => [],
                    'categories' => ['user'],
                    'logFile' => '@app/runtime/logs/user/phone.log',
                    'prefix' => function ($message) {
                        $user = Yii::$app->has('user', true) ? Yii::$app->get('user') : null;
                        $userID = $user ? $user->getId(false) : '-';
                        return "[$userID]";
                    }
                ],
            ],
        ],
        'i18n' => [
            'translations' => [
                '*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@app/messages', // if advanced application, set @frontend/messages
                    'sourceLanguage' => 'en',
                    'fileMap' => [
                        //'main' => 'main.php',
                    ],
                ],
            ],
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'baseUrl' => 'https://kyna.vn',
            'hostInfo' => '/',
            'rules' => require dirname(dirname(__DIR__)).'/frontend/config/route.php',
        ],
    ],
    'modules' => [
        'user' => [
            //'identityClass' => 'kyna\user\models\User',
            'class' => 'dektrium\user\Module',
            'enableUnconfirmedLogin' => true,
            'admins' => ['admin'],
            'enableConfirmation' => false,
            'enableRegistration' => true,
            'layout' => '@app/views/layouts/lte',
            'modelMap' => [
                'User' => 'kyna\user\models\User',
                'Account' => 'kyna\user\models\Account',
                'Profile' => 'kyna\user\models\Profile',
                'Token' => 'kyna\user\models\Token',
                'UserSearch' => 'kyna\user\models\UserSearch',
                'RegistrationForm' => 'app\modules\user\models\RegistrationForm',
                'ResendForm' => 'app\modules\user\models\ResendForm',
                'LoginForm' => 'app\modules\user\models\LoginForm',
                'SettingsForm' => 'app\modules\user\models\SettingsForm',
                'RecoveryForm' => 'app\modules\user\models\RecoveryForm',
            ],
            'controllerMap' => [
                // declares "account" controller using a class name
                'lesson' => 'app\modules\user\controllers\LessonController',
                'mail' => 'app\modules\user\controllers\MailController',
                'teacher' => 'app\modules\user\controllers\TeacherController',
                'tele-sales' => 'app\modules\user\controllers\TeleSalesController',
                'phone' => 'app\modules\user\controllers\PhoneController',
                'combo' => 'app\modules\user\controllers\ComboController',
            ],

        ],
        'sync' => [
            'class' => 'app\modules\sync\SyncModule',
        ],
        'course' => [
            'class' => 'app\modules\course\CourseModule',
        ],
        'ghn' => [
            'class' => 'app\modules\ghn\GhnModule',
        ],
        'proship' => [
            'class' => 'app\modules\proship\ProshipModule',
        ],
        'ghtk' => [
            'class' => 'app\modules\ghtk\GhtkModule',
        ],
        'epay' => [
            'class' =>'app\modules\epay\EpayModule'
            ],
        'treemanager' =>  [
            'class' => '\kartik\tree\Module',
        ],
        'seo' => [
            'class' => 'app\modules\seo\SeoModule',
        ],
        'extra' => [
            'class' => 'app\modules\extra\ExtraModule',
        ],
        'export' => [
            'class' => 'app\modules\export\ExportModule',
        ],
        'import' => [
            'class' => 'app\modules\import\ImportModule',
        ],
        'order' => [
            'class' => 'app\modules\order\OrderModule',
        ],
        'tracking' => [
            'class' => 'app\modules\tracking\TrackingModule',
        ],
        'partner' => [
            'class' => 'app\modules\partner\PartnerModule',
        ],
        'tag' => [
            'class' => 'app\modules\tag\TagModule',
        ],
        'elastic' => [
            'class' => 'app\modules\elastic\ElasticModule',
        ],
        'gamification' => [
            'class' => 'app\modules\gamification\Module',
        ],
        'notification' => [
            'class' => 'app\modules\notification\NotificationModule',
        ],
        'recommendation' => [
            'class' => 'app\modules\recommendation\RecommendationModule',
        ],
        'promotion' => [
            'class' => 'app\modules\promotion\PromotionModule',
        ],
        'commission' => [
            'class' => 'app\modules\commission\CommissionModule',
        ],
        'campaign' => [
            'class' => 'app\modules\campaign\CampaignModule',
        ],
    ],
    'params' => $params,
    'on beforeRequest' => function () {
        return;
    }
];
