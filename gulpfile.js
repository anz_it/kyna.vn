// Include gulp
var gulp = require('gulp');

// Include Our Plugins
var sass = require('gulp-sass');
var rename = require('gulp-rename');
var livereload = require('gulp-livereload');
var minifyCss = require('gulp-cssnano');

// Compile Sass
// gulp.task('sass', function () {
//     return gulp.src('./frontend/web/src/scss/main.scss')
//         .pipe(sass())
//         //.pipe(minifyCss())
//         .pipe(rename('main.min.css'))
//         .pipe(gulp.dest('../media/css'))
//         .pipe(livereload());
// });

gulp.task('sass', function() {
    return gulp.src(['E:/Kyna/project/kyna.vn/frontend/web/src/scss/main.scss'])
        .pipe(sass({outputStyle: 'compressed'}))
        .pipe(rename('main.min.css'))
        .pipe(gulp.dest('E:/Kyna/project/kyna.vn/media/css'))
});

// Watch Files For Changes
gulp.task('watch', function () {
    livereload.listen();
    gulp.watch(['E:/Kyna/project/kyna.vn/frontend/web/src/scss/**/*.scss'], ['sass']);
});

// Default Task
gulp.task('default', ['sass', 'watch']);