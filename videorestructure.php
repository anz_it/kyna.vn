<?php
/**
 * Created by PhpStorm.
 * User: khanhphan
 * Date: 11/16/16
 * Time: 10:18 AM
 */

function processData()
{
    $file = fopen(__DIR__ . "/datavideo.csv", "r");
    $data = fgetcsv($file);
    $result = [];
    while ( $data !== false) {
        $result[] = ['course_id' => $data[0], 'video_link' => $data[1]];
        $data = fgetcsv($file);
    }
    fclose($file);
    unset($result[0]);

    foreach ($result as $item) {
        $folder = '/data1/content/vod/'.implode("/", str_split($item['course_id']));
        if (!file_exists($folder)) {
            mkdir($folder, 0777, true);
        }

        if (is_file("/data/content".$item['video_link'])) {
            $name = basename("/data/content".$item['video_link']);
            if (!file_exists($folder."/".$name)) {
                copy("/data/content".$item['video_link'], $folder."/".$name);

                echo "Copy from "."/data/content".$item['video_link'] . " to ". $folder."/".$name. "\n";

            }
        }

    }
}

processData();
?>

