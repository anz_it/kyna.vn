<?php
/**
 * Created by PhpStorm.
 * User: hoa
 * Date: 10/01/2019
 * Time: 14:33
 */

namespace common\campaign;

use common\helpers\ArrayHelper;
use common\helpers\CDNHelper;
use kyna\course\models\Course;
use kyna\course\models\Teacher;
use kyna\promo\models\PromotionSchedule;
use kyna\tag\models\CourseTag;
use kyna\tag\models\Tag;
use Yii;
use app\modules\cart\models\form\PromoCodeForm;
use yii\web\Session;
use yii\db\Expression;

class CampaignTet
{
    const DONG_GIA_199K = 'donggia199k';
    const DONG_GIA_299K = 'donggia299k';
    const DONG_GIA_399K = 'donggia399k';
    const FLASH_SALE = 'flashsale';
    const HOURS9H12H = '9_12';
    const HOURS14H17H = '14_17';
    const HOURS20H23H = '20_23';
    const HOURSFULL = 'FULL';
    const LIXITET5 = 'LIXITET5';
    const LIXITET10 = 'LIXITET10';
    const LIXITET15 = 'LIXITET15';
    const LIXITET20 = 'LIXITET20';
    const COURSE_NOT_APPLY_VOUCHER = [317,323,537,542,592,926,943,205,600,601,602,842,859,1227,1232,994,1246,846,845,844,936,935,924,413,1302,1295,1131,1130,860,858,832,773,716,570,204,1111,1110,1109,1108,777,776,775,774,523,819,724,709,708,707,692,691,690,689,688,596,584,583,558,436,385,759,559,663,658,657,656,593,578,566,540,399,316,314,310,308,307,305,303,294,645,746,745,744,1248,1104,1077,1076,1075,1074,1038,1037,1234,1233];
    const googleSheetUrl = 'https://spreadsheets.google.com/feeds/list/1o7qGa1lQliY_M5MmNU_P1GuS_RiHsrFcSU-YfnPGPU8/1/public/values?alt=json';

    public static function InTimesCampaign()
    {
        if (isset(Yii::$app->params['intervals_time_campaign_tet'])) {
            $date = \Yii::$app->params['intervals_time_campaign_tet'];
            $from = $date[0];
            $to = $date[1];
            $start_time = new \DateTime($from);
            $end_time = new \DateTime($to);
            $now = new \DateTime('now');
            if ($start_time < $now && $now < $end_time) {
                return true;
            }
        }
        return false;
    }

    public static function hoursFlashSale()
    {
        if (isset(Yii::$app->params['intervals_time_campaign_tet_hours'])) {
            $date = \Yii::$app->params['intervals_time_campaign_tet_hours'];
            return $date;
        }
        return null;
    }

    public static function isBetweenTimes($dates)
    {

        $start = $dates[0];
        $end = $dates[1];

        if ($start == null) {
            $start = '00:00:00';
        }
        if ($end == null) {
            $end = '23:59:59';
        }
        return ($start <= date('H:i:s') && date('H:i:s') <= $end);
    }

    public static function addCoursesTag($slugTag, $IdCourse)
    {
        $tag = Tag::findOne(['slug' => $slugTag]);
        if ($tag) {
            $courseTag = CourseTag::findOne(['course_id' => $IdCourse, 'tag_id' => $tag->id]);
            if (!$courseTag) {
                $courseTag = new CourseTag();
            }
            $courseTag->setCourse($IdCourse);
            $courseTag->setTag($tag->id);
            $courseTag->save(false);
        }
    }

    public static function removeCourseTag($slugTag, $IdCourse)
    {
        $tag = Tag::findOne(['slug' => $slugTag, 'status' => 1]);
        if (!empty($tag)) {
            $courseTag = CourseTag::findOne(['course_id' => $IdCourse, 'tag_id' => $tag->id]);
            if ($courseTag) {
                $courseTag->delete();
            }
        }

    }

    public static function updatePromotionSchedule($CourseId, $price, $hours)
    {
        $schedule = new PromotionSchedule();
        $start = null;
        $end = null;
        list($hours9_12, $hours14_17, $hours20_23) = CampaignTet::hoursFlashSale();

        if ($hours == CampaignTet::HOURS9H12H) {

            list($hourBegin, $minBegin, $secBegin) = explode(':', $hours9_12[0]);
            list($hourEnd, $minEnd, $secEnd) = explode(':', $hours9_12[1]);
            $start = CampaignTet::getHoursPromotionSchedule($hourBegin, $minBegin, $secBegin);
            $end = CampaignTet::getHoursPromotionSchedule($hourEnd, $minEnd, $secEnd);

        } elseif ($hours == CampaignTet::HOURS14H17H) {

            list($hourBegin, $minBegin, $secBegin) = explode(':', $hours14_17[0]);
            list($hourEnd, $minEnd, $secEnd) = explode(':', $hours14_17[1]);
            $start = CampaignTet::getHoursPromotionSchedule($hourBegin, $minBegin, $secBegin);
            $end = CampaignTet::getHoursPromotionSchedule($hourEnd, $minEnd, $secEnd);

        } elseif ($hours == CampaignTet::HOURS20H23H) {

            list($hourBegin, $minBegin, $secBegin) = explode(':', $hours20_23[0]);
            list($hourEnd, $minEnd, $secEnd) = explode(':', $hours20_23[1]);
            $start = CampaignTet::getHoursPromotionSchedule($hourBegin, $minBegin, $secBegin);
            $end = CampaignTet::getHoursPromotionSchedule($hourEnd, $minEnd, $secEnd);

        } else {
            $date = Yii::$app->params['intervals_time_campaign_tet'];
            $start = self::getDatePromotionSchedule(explode(' ', $date[0]));
            $end = self::getDatePromotionSchedule(explode(' ', $date[1]));
        }

        $schedule->course_id = intval($CourseId);
        $schedule->start_time = $start;
        $schedule->end_time = $end;

        $schedule->price = intval($price);
        $schedule->discount_amount = $schedule->course->price - $schedule->price;
        $schedule->discount_percent = $schedule->discount_amount * 100 / $schedule->course->price;
        $schedule->note = 'Campaign Tết';

        if ($schedule->save(false)) {
            return $schedule;
        }
        return false;
    }

    public static function getDatePromotionSchedule($time)
    {
        list($year, $month, $day) = explode('-', $time[0]);
        list($hour, $min, $sec) = explode(':', $time[1]);

        $dateEnd = new \DateTime();
        $dateEnd->format('Y-m-d 00:00:00');
        $dateEnd->setDate($year, $month, $day);
        $dateEnd->setTime($hour, $min, $sec);
        return $dateEnd->getTimestamp();
    }

    public static function getHoursPromotionSchedule($hour, $min, $sec)
    {
        $dateEnd = new \DateTime();
        $dateEnd->format('Y-m-d 00:00:00');
        $dateEnd->setTime($hour, $min, $sec);
        return $dateEnd->getTimestamp();
    }

    public static function voucherLixTet($priceOrder = 0)
    {
        if ($priceOrder == 0) {
            $priceOrder = self::getTotalPrice();
        }
        $voucher = '';
        if ($priceOrder >= 2000000) {
            return $voucher = self::LIXITET20;
        }
        if ($priceOrder >= 1000000) {
            return $voucher = self::LIXITET15;
        }

        if ($priceOrder >= 500000) {
            return $voucher = self::LIXITET10;
        }

        if ($priceOrder >= 300000) {
            return $voucher = self::LIXITET5;
        }
        return $voucher;
    }

    public static function voucherLixTetRender($priceOrder = 0)
    {
        $voucher = '';
        if ($priceOrder >= 2000000) {
            return $voucher = self::LIXITET20;
        }
        if ($priceOrder >= 1000000) {
            return $voucher = self::LIXITET15;
        }

        if ($priceOrder >= 500000) {
            return $voucher = self::LIXITET10;
        }

        if ($priceOrder >= 300000) {
            return $voucher = self::LIXITET5;
        }
        return $voucher;
    }

    public static function isVoucherLixTet($voucher = '')
    {
        switch ($voucher){
            case self::LIXITET5:
                return true;
            case self::LIXITET10:
                return true;
            case self::LIXITET15:
                return true;
            case self::LIXITET20:
                return true;
            default:
                return false;
        }
    }

    public static function getTotalPrice()
    {
        $cartPositions = Yii::$app->cart->positions;
        $total_order_can_apply = 0;
        foreach ($cartPositions as $key => $value) {
            $isCombo = $value->type == Course::TYPE_COMBO;
            if (!$isCombo) {
                $total_order_can_apply = $total_order_can_apply + $value->oldPrice - $value->originDiscount;
            }
        }
        return $total_order_can_apply;
    }

    public static function percentLiXiTet($priceOrder = 0)
    {
        if ($priceOrder == 0) {
            $priceOrder = self::getTotalPrice();
        }
        $percent = 0;
        if ($priceOrder >= 2000000) {
            return $percent = 20;
        }

        if ($priceOrder >= 1000000) {
            return $percent = 15;
        }

        if ($priceOrder >= 500000) {
            return $percent = 10;
        }

        if ($priceOrder >= 300000) {
            return $percent = 5;
        }

        return $percent;
    }

    public static function getSlugDongGia($priceDongGia)
    {

        if ($priceDongGia == 199000) {
            return self::DONG_GIA_199K;
        }
        if ($priceDongGia == 299000) {
            return self::DONG_GIA_299K;
        }
        if ($priceDongGia == 399000) {
            return self::DONG_GIA_399K;
        }
    }

    public static function getCourseFromGoogleSheet($url)
    {
        $file = file_get_contents($url);
        $json = json_decode($file);
        $rows = $json->{'feed'}->{'entry'};
        $listCourse = [];
        list($hours9_12,$hours14_17,$hours20_23) = CampaignTet::hoursFlashSale();

        foreach ($rows as $row) {
            $IdCourse = $row->{'gsx$id'}->{'$t'};
            $courseName = $row->{'gsx$tênkhóahọc'}->{'$t'};
            $giaGoc = $row->{'gsx$họcphígốc'}->{'$t'};
            $flashSale = $row->{'gsx$flashsale'}->{'$t'};
            $dongGia = $row->{'gsx$đồnggiá'}->{'$t'};
            $avatar = $row->{'gsx$avatargiảngviên'}->{'$t'};
            $teacher = $row->{'gsx$tênchứcvụngắngọncủagiảngviên'}->{'$t'};
            $teacher = explode('/', $teacher);
            $tenGiaoVien = isset($teacher[0]) ? $teacher[0] : '';
            $chucVu = isset($teacher[1]) ? $teacher[1] : '';
            $courseDesc = $row->{'gsx$shortdescription'}->{'$t'};
            $courseCount = $row->{'gsx$sốlượnghọcviênđăngký'}->{'$t'};
            $courseTag = $row->{'gsx$tag'}->{'$t'};

            $course = [];
            $course['id'] = $IdCourse;
            $course['name'] = $courseName;
            $course['gia_goc'] = intval($giaGoc);
            $course['flash_sale'] = intval($flashSale);

            if((CampaignTet::isBetweenTimes($hours9_12)
                ||CampaignTet::isBetweenTimes($hours14_17)
                || CampaignTet::isBetweenTimes($hours20_23)) && intval($flashSale) >0){
                $course['dong_gia'] = intval($flashSale);
            }else{
                $course['dong_gia'] = intval($dongGia);
            }
            $course['phan_tram_giam'] = (($course['gia_goc'] - $course['dong_gia']) * 100) / $course['gia_goc'];
            $course['avatar'] = $avatar;
            $course['ten_giao_vien'] = $tenGiaoVien;
            $course['chuc_vu'] = $chucVu;
            $course['course_desc'] = $courseDesc;
            $course['course_count'] = $courseCount;
            $course['tag'] = $courseTag;
            array_push($listCourse, $course);
        }
        return $listCourse;
    }

    public function getCoursePageFlashSaleOnCampaign($limit = 4)
    {
        $tag = Tag::findOne(['slug' => self::FLASH_SALE]);
        $courses = [];
        if ($tag) {
            $courses = Course::find()->leftJoin('course_tags', 'course_tags.course_id = courses.id')
                ->where(['course_tags.tag_id' => $tag->id])
                ->limit($limit)
                ->all();
        }
        return $courses;
    }


    public function getCoursePageBeforeDongGia()
    {
        $cacheText = 'getCoursePageBeforeDongGia';
//        Yii::$app->cache->delete($cacheText);
        $result = Yii::$app->cache->get($cacheText);
        if (!empty($result)) {
            return $result;
        }
        $url = 'https://spreadsheets.google.com/feeds/list/1-kglSmiFrfSR_61yPdoC6kseplsafDB-t22CsWiKm4k/2/public/values?alt=json';
        $file = file_get_contents($url);
        $json = json_decode($file);
        $rows = $json->{'feed'}->{'entry'};
        $listCourse = [];
        $tags = [];
        foreach ($rows as $row) {
            $priceTagDongGia = $row->{'gsx$đồnggiá'}->{'$t'};
            $priceTagDongGia = intval($priceTagDongGia);
            $priceFlashSale = $row->{'gsx$flashsale'}->{'$t'};
            $priceFlashSale = intval($priceFlashSale);
            if (!empty($priceTagDongGia) && empty($priceFlashSale)) {
                $IdCourse = $row->{'gsx$id'}->{'$t'};
                $courseName = $row->{'gsx$tênkhóahọc'}->{'$t'};
                $giaGoc = $row->{'gsx$họcphígốc'}->{'$t'};
                $giaGiam = $row->{'gsx$đồnggiá'}->{'$t'};
                $avatar = $row->{'gsx$avatargiảngviên'}->{'$t'};
                $teacher = $row->{'gsx$tênchứcvụngắngọncủagiảngviên'}->{'$t'};
                $teacher = explode('/', $teacher);
                $tenGiaoVien = isset($teacher[0]) ? $teacher[0] : '';
                $chucVu = isset($teacher[1]) ? $teacher[1] : '';
                $course_desc = $row->{'gsx$shortdescription'}->{'$t'};
                $course_count = $row->{'gsx$sốlượnghọcviênđăngký'}->{'$t'};
                $tagCourse = $row->{'gsx$tag'}->{'$t'};
                $course = [];
                $course['id'] = $IdCourse;
                $course['name'] = $courseName;
                $course['gia_goc'] = intval($giaGoc);
                $course['gia_giam'] = intval($giaGiam);
                $course['phan_tram_giam'] = (($course['gia_goc'] - $course['gia_giam']) * 100) / $course['gia_goc'];
                $course['avatar'] = $avatar;
                $course['ten_giao_vien'] = $tenGiaoVien;
                $course['chuc_vu'] = $chucVu;
                $course['course_desc'] = $course_desc;
                $course['course_count'] = $course_count;

                array_push($tags, $tagCourse);
                if (in_array($tagCourse, $tags)) {

                }
                array_push($listCourse, $course);
                if (count($listCourse) > 3) {
                    Yii::$app->cache->set($cacheText, $listCourse, 3600 * 5);
                    return $listCourse;
                }
            }
        }
        return $listCourse;
    }

    public function getCoursePageDongGiaOnCampaign($limit = 4)
    {
        $tag = Tag::findOne(['slug' => self::DONG_GIA_199K]);
        $courses = [];
        if ($tag) {
            $courses = Course::find()->leftJoin('course_tags', 'course_tags.course_id = courses.id')
                ->where(['course_tags.tag_id' => $tag->id])
                ->limit($limit)
                ->all();
        }
        return $courses;
    }

    public static function isBeforeTime($dates)
    {
        $start = $dates[0];
        $end = $dates[1];

        if ($start == null) {
            $start = '00:00:00';
        }
        if ($end == null) {
            $end = '23:59:59';
        }
        return (date('H:i:s') < $start);
    }

    public static function isAfterTime($dates)
    {
        $start = $dates[0];
        $end = $dates[1];

        if ($start == null) {
            $start = '00:00:00';
        }
        if ($end == null) {
            $end = '23:59:59';
        }
        return ($end < date('H:i:s'));
    }

    public static function isDateBeforeRunCampaign()
    {
        if (isset(\Yii::$app->params['intervals_time_campaign_tet'])) {
            $date = \Yii::$app->params['intervals_time_campaign_tet'];
            $from = $date[0];
            $start_time = new \DateTime($from);
            $now = new \DateTime('now');
            if ($now < $start_time) {
                return true;
            }
        }
        return false;
    }

    public static function isDateAfterRunCampaign()
    {
        if (isset(\Yii::$app->params['intervals_time_campaign_tet'])) {
            $date = \Yii::$app->params['intervals_time_campaign_tet'];
            $to = $date[1];
            $end_time = new \DateTime($to);
            $now = new \DateTime('now');
            if ($end_time < $now) {
                return true;
            }
        }
        return false;
    }

    public static function addCart($courseId)
    {
        if (self::checkCourseInCarts($courseId)) {
            return false;
        } else {
            $session = \Yii::$app->session;
            $carts = $session->get('carts_campaign_tet');
            if (!empty($carts)) {
                array_push($carts, $courseId);
            } else {
                $carts[] = $courseId;
            }
            $session->set('carts_campaign_tet', $carts);
            return true;
        }
    }

    public static function removeCart($courseId)
    {
        $session = \Yii::$app->session;
        $carts = $session->get('carts_campaign_tet');
        if (!empty($carts)) {
            if (($key = array_search($courseId, $carts)) !== false) {
                unset($carts[$key]);
                $session->set('carts_campaign_tet', $carts);
                return true;
            }
        }
        return false;
    }

    public static function renderListItemCart()
    {
        $cartHtml = '';
        $session = \Yii::$app->session;
        $carts = $session['carts_campaign_tet'];
        $csrf = Yii::$app->request->csrfToken;
        if ($carts) {
            foreach ($carts as $id) {
                $course = Course::findOne(['id' => $id]);
                if ($course) {
                    $cartHtml .= "
                        <li>
                            <span>{$course->name}</span>
                                <button onclick='removeCourse(this)' class='remove-course' data-csrf='{$csrf}' data-course-id='{$course->id}' data-course-name='{$course->name}' id='detele'></button>
                        </li>
                    ";
                }
            }
        }
        return $cartHtml;
    }

    public static function getTotalCourseCart()
    {
        $session = \Yii::$app->session;
        $carts = $session['carts_campaign_tet'];
        return count($carts);
    }

    public static function getTotalPriceCart()
    {
        $session = \Yii::$app->session;
        $carts = $session['carts_campaign_tet'];
        $totalPriceOld = 0;
        $totalPriceNew = 0;
        if ($carts) {
            foreach ($carts as $id) {
                $course = Course::findOne(['id' => $id]);
                $totalPriceOld += $course->getOldPrice();
                $totalPriceNew += $course->getSellPrice();
            }
        }
        return [$totalPriceOld, $totalPriceNew];
    }

    public static function getPriceDiscount($priceOrder)
    {
        $voucher = '';
        if ($priceOrder >= 2000000) {
            return [20, 2000000];
        }
        if ($priceOrder >= 1000000) {
            return [15, 1000000];
        }

        if ($priceOrder >= 500000) {
            return [10, 500000];
        }

        if ($priceOrder >= 300000) {
            return [5, 300000];
        }
        return [0, 0];
    }

    public static function renderCountCourseHtml()
    {
        $countCourse = CampaignTet::getTotalCourseCart();
        return "
            {$countCourse} khoá học
        ";
    }

    public static function renderButtonHasSelect($course_id,$post = [])
    {
        $course = Course::findOne(['id' => $course_id]);
        $totalUser = isset($post['totalUser'])? $post['totalUser'] : 0;
        return "
            <button id='button_{$course->id}' class='add-to-cart has-add-to-cart'>
                <span>Đã thêm</span>
                <strong>(đã có <b>{$totalUser} </b> người học)</strong>
            </button>
        ";
    }

    public static function renderButtonBuyCart($course_id,$post =[])
    {
        $course = Course::findOne(['id' => $course_id]);
        $totalUser = isset($post['totalUser'])? $post['totalUser'] : 0;
        $csrf = Yii::$app->request->csrfToken;
        return "
            <button id='button_{$course->id}' class='add-to-cart'
                    data-course-id='{$course->id}'
                    data-course-price='{$course->getSellPrice()}'
                    data-csrf='{$csrf}'
            >
            <span>Thêm khóa học</span>
            <strong>(đã có <b>{$totalUser} </b> người học)</strong>
        </button>
        ";
    }

    public static function renderFormHtmlCart($isAjax = false,$post =[])
    {
        $session = \Yii::$app->session;
        $carts = $session['carts_campaign_tet'];
        $listCourseIds = '';
        if ($carts) {
            $listCourseIds = implode(',', $carts);
        }
        $isDesktop = true;
        if (Yii::$app->devicedetect->isMobile() || Yii::$app->devicedetect->isTablet()) {
            $isDesktop = false;
        }

        $csrf = Yii::$app->request->csrfToken;
        list($totalPriceOld, $totalPriceNew) = self::getTotalPriceCart();
        list($percentDiscount, $priceDiscount) = self::getPriceDiscount($totalPriceNew);
        $giaLixi = ($totalPriceNew * $percentDiscount) / 100;
        $totalPriceOldString = number_format($totalPriceOld, "0", "", ".");
        $totalPriceNewString = number_format($totalPriceNew, '0', '', '.');
        $priceDiscount = number_format($priceDiscount, '0', '', '.');
        $giaSauGiamLiXi = $totalPriceNew - $giaLixi;
        $giaSauGiamLiXi = number_format($giaSauGiamLiXi, '0', '', '.');
        $form = '';

        $full_name = '';
        $email = '';
        $phonenumber = '';
        $full_name_mobile = '';
        $email_mobile = '';
        $phone_number_mobile = '';

        if($isAjax == true){
            $full_name = isset($post['full_name'])? $post['full_name'] : '';
            $email = isset($post['email'])? $post['email'] : '';
            $phonenumber = isset($post['phone_number'])? $post['phone_number'] : '';
            $full_name_mobile = isset($post['full_name_mobile'])? $post['full_name_mobile'] : '';
            $email_mobile = isset($post['email_mobile'])? $post['email_mobile'] : '';
            $phone_number_mobile = isset($post['phone_number_mobile'])? $post['phone_number_mobile'] : '';
        }
        $disableCod = '';
        if ($totalPriceNew < 149000) {
            $selectedBankTransfer = 'selected';
            $disableCod = 'disabled';
            $selectedCod = '';
        } else {
            $selectedBankTransfer = '';
            $selectedCod = 'selected';
        }
        if ($isDesktop == true) {
            $form .= "
                    <input type='hidden' value='{$csrf}' id='csrf' name='_csrf'>
                    <input type='hidden' value='{$listCourseIds}' id='list_course_ids' name='list_course_ids'/>
                    <input type='hidden' id='advice_name' name='advice_name'/>
                    <input type='hidden' id='slug' name='slug' value='campaign/flash-sale-dong-gia'/>
                    <input name='fullname' value='{$full_name}' id='full_name' type='text' placeholder='Họ và tên '>
                    <input name='email' value='{$email}' id='email' type='email' placeholder='Email'>
                    <input name='phonenumber' value='{$phonenumber}' id='phone_number' type='number' placeholder='Số điện thoại '>
                    <select name='payment_type' id='payment_method'>
                        <option value='' hidden >Hình thức đăng ký </option>
                        <option value='cod' {$selectedCod} {$disableCod}>Giao mã kích hoạt và thu tiền </option>
                        <option value='bank-transfer' {$selectedBankTransfer}>Chuyển khoản ngân hàng </option>
                        <option value='onepay_atm'>Thẻ ATM có Internet Banking </option>
                        <option value='onepay_cc'>Thẻ Visa / Mastercard </option>
                    </select>";
            if ($totalPriceNew >= 300000) {
                $form .= "<p>Đơn hàng trên {$priceDiscount}đ được giảm {$percentDiscount}%</p>";
            }
            $form .= "<div id='loading-submit'> </div>";
            $form .= "<button class='btn_box_register' type='submit'>
                        <span>ĐĂNG KÝ NGAY </span> <br>";
            if($totalPriceNew >=300000){
                $form .= "<strike>{$totalPriceNewString}đ </strike> <span> -";
            }
            if ($totalPriceNew > 0) {
                $form .= "{$giaSauGiamLiXi}đ </span>";
            }

            $form .= "</button>
                 ";
        } else {

            $form .= "
                    <input type='hidden' value='{$csrf}' id='csrf' name='_csrf'>
                    <input type='hidden' value='{$listCourseIds}' id='list_course_ids_mobile' name='list_course_ids'/>
                    <input type='hidden' id='advice_name_mobile' name='advice_name'/>
                    <input type='hidden' id='slug_mobile' name='slug' value='campaign/flash-sale-dong-gia'/>
                    <div class='input'>
                        <label for=''>Họ và tên</label>
                        <input name='fullname' value='{$full_name_mobile}' id='full_name_mobile' type='text' placeholder='Họ và tên...'>
                    </div>
        
                    <div class='input'>
                        <label for=''>Email</label>
                        <input name='email' value='{$email_mobile}'  id='email_mobile' type='text' placeholder='Email...'>
                    </div>
        
                    <div class='input'>
                        <label for=''>Số điện thoại</label>
                        <input name='phonenumber' value='{$phone_number_mobile}' id='phone_number_mobile' type='text' placeholder='Số điện thoại...'>
                    </div>
                    <select name='payment_type' id='payment_method_mobile'>
                        <option value='' hidden>Hình thức đăng ký </option>
                        <option value='cod' {$selectedCod} {$disableCod} >Giao mã kích hoạt và thu tiền </option>
                        <option value='bank-transfer'>Chuyển khoản ngân hàng </option>
                        <option value='onepay_atm'>Thẻ ATM có Internet Banking </option>
                        <option value='onepay_cc'>Thẻ Visa / Mastercard </option>
                    </select>";

                $form .= "<div style='text-align: center' id='loading-submit-mobile'></div>";

                if ($totalPriceNew >= 300000) {
                    $form .= "<strong style='margin-bottom: 5px;display: block;text-align: left;'>Đơn hàng trên {$priceDiscount}đ được giảm {$percentDiscount}%</strong>";
                }

                $form .= "<button id='box_register-mobile'>
                        <span>ĐĂNG KÍ NGAY</span> <br>";
                        if($totalPriceNew >=300000){
                            $form .= "<strike>{$totalPriceNewString}đ </strike> <span> -";
                        }
                        if ($totalPriceNew > 0) {
                            $form .= "{$giaSauGiamLiXi}đ </span>";
                        }
                    $form .="</button>
                ";
        }

        return $form;
    }

    public static function renderFormCartBottomMobile(){
        $session = \Yii::$app->session;
        $carts = $session['carts_campaign_tet'];
        $totalCart = count($carts);
        list($totalPriceOld, $totalPriceNew) = self::getTotalPriceCart();
        list($percentDiscount, $priceDiscount) = self::getPriceDiscount($totalPriceNew);
        $totalPriceNewString = number_format($totalPriceNew, '0', '', '.');

        $form = '';
        $form .= "
                <a class='page-scroll' id='checkout' href='javascript:'>
                    Thanh toán <br>
                    <strong>{$totalCart} khoá học ({$totalPriceNewString}đ)</strong>
                </a>
                <button type='button' id='searchMB'></button>
                <a class='page-scroll' href='javascript:' id='backToTop'>
                    <div class='clearfix'></div>
                </a>
        ";
        return $form;
    }

    public static function getCourseHtml($courses, $limit = 36)
    {
        $courseHtml = '';
        if ($courses) {
            $i = 0;
            foreach ($courses as $course) {
                /* @var $course Course */
                if ($course) {
                    $courseId = $course['id'];
                    $sellPrice = number_format($course['dong_gia'], "0", "", ".");
                    $oldPrice = number_format($course['gia_goc'], '0', '', '.');
                    $discountPercent = number_format($course['phan_tram_giam'], '0', '', '.');
                    $title = $course['name'];
                    $description = $course['course_desc'];
                    /* @var $teacher Teacher */
                    $teacherName = $course['ten_giao_vien'];
                    $teacherDesc = $course['chuc_vu'];
                    $avatar = $course['avatar'];
                    $courseCount = $course['course_count'];
                    $csrf = Yii::$app->request->csrfToken;

                    $courseHtml .= "
                        <div class='course-item'>
                            <h3 class='course-title'>{$title}</h3>
                            <div class='price'>
                                <div class='new-price'>{$sellPrice} đ </div>
                                <div class='old-price'>
                                    {$oldPrice} đ <span class='d-none d-lg-inline-block'>-{$discountPercent}%</span>
                                </div>
                            </div>
                            <div class='percent d-lg-none'>
                                <span>-{$discountPercent}%</span>
                            </div>
                            <div class='info-author'>
                                <img class='d-none d-sm-inline-block' src='{$avatar}' alt=''>
                                <div>
                                    <div class='name-author'>{$teacherName}</div>
                                    <div class='position-author'>{$teacherDesc}</div>
                                </div>
                            </div>
                            <div class='description'>
                                {$description}
                            </div>";
                    if (self::checkCourseInCarts($courseId)):
                        $courseHtml .= "
                                    <div class='d-lg-block text-center'>
                                            <button id='button_{$courseId}' class='add-to-cart has-add-to-cart'>
                                                <span>Đã thêm</span>
                                                <strong>(đã có <b>{$courseCount} </b> người học)</strong>
                                            </button>
                                        </div>
                                 </div>
                                ";
                    else:
                        $courseHtml .=
                            "<div class='d-lg-block text-center'>
                                            <button id='button_{$courseId}' class='add-to-cart'
                                                data-course-id='{$courseId}'
                                                data-course-price='{$sellPrice}'
                                                data-csrf='{$csrf}'
                                            >
                                                <span>Thêm khóa học</span>
                                                <strong>(đã có <b>{$courseCount} </b> người học)</strong>
                                            </button>
                                    </div>
                            </div>";
                    endif;
                    $i++;
                    if ($i >= $limit) {
                        return $courseHtml;
                    }
                }
            }
        }
        return $courseHtml;
    }

    public static function getCourseHtmlSieuSale($courses, $limit = 5,$tag, $isFlashSale = false)
    {
        list($hours9_12,$hours14_17,$hours20_23) = CampaignTet::hoursFlashSale();
        $courseHtml = '';
        if ($courses) {
            $i = 1;
            foreach ($courses as $course) {
                /* @var $course Course */
                if ($course) {
                    $courseId = $course['id'];
                    if($isFlashSale == true)
                        $sellPrice = number_format($course['flash_sale'], "0", "", ".");
                    else
                        $sellPrice = number_format($course['dong_gia'], "0", "", ".");

                    $oldPrice = number_format($course['gia_goc'], '0', '', '.');
                    $discountPercent = number_format($course['phan_tram_giam'], '0', '', '.');
                    $title = $course['name'];
                    $description = $course['course_desc'];
                    /* @var $teacher Teacher */
                    $teacherName = $course['ten_giao_vien'];
                    $teacherDesc = $course['chuc_vu'];
                    $avatar = $course['avatar'];
                    $courseCount = $course['course_count'];
                    $csrf = Yii::$app->request->csrfToken;

                    $courseHtml .= "
                        <div class='course-item'>
                            <h3 class='course-title'>{$title}</h3>
                            <div class='price'>
                                <div class='new-price'>{$sellPrice} đ </div>
                                <div class='old-price'>
                                    {$oldPrice} đ <span class='d-none d-lg-inline-block'>-{$discountPercent}%</span>
                                </div>
                            </div>
                            <div class='percent d-lg-none'>
                                <span>-{$discountPercent}%</span>
                            </div>
                            <div class='info-author'>
                                <img class='d-none d-sm-inline-block' src='{$avatar}' alt=''>
                                <div>
                                    <div class='name-author'>{$teacherName}</div>
                                    <div class='position-author'>{$teacherDesc}</div>
                                </div>
                            </div>
                            <div class='description'>
                                {$description}
                            </div>";
                    if (self::checkCourseInCarts($courseId)):
                        if(CampaignTet::isBetweenTimes($hours9_12) || CampaignTet::isBetweenTimes($hours14_17) ||
                            CampaignTet::isBetweenTimes($hours20_23)){
                            $courseHtml .= "
                                    <div class='d-lg-block text-center'>
                                            <button id='button_{$courseId}' class='add-to-cart has-add-to-cart'>
                                                <span>Đã thêm</span>
                                                <strong>(đã có <b>{$courseCount} </b> người học)</strong>
                                            </button>
                                        </div>
                                 </div>
                                ";
                        }else{
                            if($tag == self::FLASH_SALE){
                                $courseHtml .=
                                    "<div class='d-lg-block text-center'>
                                    </div>
                            </div>";
                            }else{
                                $courseHtml .=
                                    "<div class='d-lg-block text-center'>
                                            <button id='button_{$courseId}' class='add-to-cart'
                                                data-course-id='{$courseId}'
                                                data-course-price='{$sellPrice}'
                                                data-csrf='{$csrf}'
                                                data-tag='{$tag}'
                                            >
                                                <span>Đã thêm</span>
                                                <strong>(đã có <b>{$courseCount} </b> người học)</strong>
                                            </button>
                                    </div>
                                 </div>";
                            }
                        }

                    else:
                        if(CampaignTet::isBetweenTimes($hours9_12) || CampaignTet::isBetweenTimes($hours14_17) ||
                            CampaignTet::isBetweenTimes($hours20_23)){
                            $courseHtml .=
                                "<div class='d-lg-block text-center'>
                                            <button id='button_{$courseId}' class='add-to-cart'
                                                data-course-id='{$courseId}'
                                                data-course-price='{$sellPrice}'
                                                data-csrf='{$csrf}'
                                                data-tag='{$tag}'
                                            >
                                                <span>Thêm khóa học</span>
                                                <strong>(đã có <b>{$courseCount} </b> người học)</strong>
                                            </button>
                                    </div>
                            </div>";
                        }else{
                            if($tag == self::FLASH_SALE){
                                $courseHtml .=
                                    "<div class='d-lg-block text-center'>
                                    </div>
                            </div>";
                            }else{
                                $courseHtml .=
                                    "<div class='d-lg-block text-center'>
                                            <button id='button_{$courseId}' class='add-to-cart'
                                                data-course-id='{$courseId}'
                                                data-course-price='{$sellPrice}'
                                                data-csrf='{$csrf}'
                                                data-tag='{$tag}'
                                            >
                                                <span>Thêm khóa học</span>
                                                <strong>(đã có <b>{$courseCount} </b> người học)</strong>
                                            </button>
                                    </div>
                                 </div>";
                            }
                        }
                    endif;
                    $i++;
                    if ($i >= $limit) {
                        return $courseHtml;
                    }
                }
            }
        }
        return $courseHtml;
    }

    public static function checkCourseInCarts($courseId)
    {
        $session = \Yii::$app->session;
        $carts = $session['carts_campaign_tet'];
        if ($carts) {
            if (in_array($courseId, $carts)) {
                return true;
            }
        }
        return false;
    }

    public static function getCourseOnCampaignPageSieuSale($tagSlug, $limit = 4, $tagSlugCate = 'all', $argPage = 0)
    {
        return self::getCourseOnCampaign($limit, true, $argPage, $tagSlugCate, $tagSlug,'',$isSieuSale = true);
    }

    public static function getCourseList(){
        list($hours9_12,$hours14_17,$hours20_23) = CampaignTet::hoursFlashSale();
        $version = '_1.3';
        $cacheText = 'campaign_tet_get_course';
        if(CampaignTet::isBetweenTimes($hours9_12)){
            $cacheText = 'campaign_tet_get_course_'.$hours9_12[0];
        }
        if(CampaignTet::isBetweenTimes($hours14_17)){
            $cacheText = 'campaign_tet_get_course_'.$hours14_17[0];
        }
        if(CampaignTet::isBetweenTimes($hours20_23)){
            $cacheText = 'campaign_tet_get_course_'.$hours20_23[0];
        }
        $cacheText .= $version;
        $courseList = Yii::$app->cache->get($cacheText);
        if(empty($courseList)){
            $courseList = self::getCourseFromGoogleSheet(self::googleSheetUrl);
            // random
            shuffle($courseList);
            Yii::$app->cache->set($cacheText, $courseList, mktime(24,0,0) - time());
        }
        return $courseList;
    }

    public static function getCourseOnCampaign(
        $limit = 36,
        $isAjax = false,
        $argPage = 0,
        $cate = 'all',
        $tag = self::FLASH_SALE,
        $searchAjax = '',
        $isSieuSale = false
    ) {

        $session = \Yii::$app->session;

        $tagSlugCate = Yii::$app->request->getQueryParam('cate', 'all');
        $tagSlug = Yii::$app->request->getQueryParam('tag', self::FLASH_SALE);
        $search = Yii::$app->request->getQueryParam('search', '');

        $courseList = self::getCourseList();

        if ($isAjax == true) {
            $tagSlugCate = $cate;
            $tagSlug = $tag;
            $search = $searchAjax;
        }
        $carts = $session['carts_campaign_tet'] ? $session['carts_campaign_tet'] : [];
        if (!empty($search)) {
            $search = strtolower($search);
            $courseList = array_filter($courseList, function ($course) use ($search, $carts) {
                return stristr(strtolower($course['name']), $search) == true && !in_array($course['id'], $carts);
            });
            return self::getCourseHtml($courseList, $limit);
        } else {
            $courseList = array_filter($courseList, function ($course) use ($search, $carts, $tagSlug, $tagSlugCate,$isSieuSale) {
                $tagSlugCheck = false;
                switch ($tagSlug) {
                    case self::FLASH_SALE:
                        $tagSlugCheck = intval($course['flash_sale']) > 0;
                        break;
                    case self::DONG_GIA_199K:
                        $tagSlugCheck = intval($course['dong_gia']) == 199000;
                        break;
                    case self::DONG_GIA_299K:
                        $tagSlugCheck = intval($course['dong_gia']) == 299000;
                        break;
                    case self::DONG_GIA_399K:
                        $tagSlugCheck = intval($course['dong_gia']) == 399000;
                        break;
                    default:
                        break;
                }
                $tagSlugCateCheck = true;
                if ($tagSlugCate != 'all') {
                    $tagSlugCateCheck = $course['tag'] == $tagSlugCate;
                }
                if($isSieuSale == true){
                    return $tagSlugCheck && $tagSlugCateCheck;
                }else{
                    return $tagSlugCheck && $tagSlugCateCheck && !in_array($course['id'],$carts);
                }
            });
            // paging
            $argPage = intval($argPage);
            if (count($courseList) > $limit) {
                $courseList = array_slice($courseList, intval($argPage) * $limit, $limit);
            } else {
                if ($argPage > 0) {
                    return '';
                }
            }
        }
        if($isSieuSale == true){
            if($tagSlug == CampaignTet::FLASH_SALE){
                return self::getCourseHtmlSieuSale($courseList,5,$tag,true);
            }
            return self::getCourseHtmlSieuSale($courseList,5,$tag);
        }
        return self::getCourseHtml($courseList, $limit);

    }

    public static function checkActiveCate($param)
    {
        if (isset($_GET['cate'])) {
            if ($_GET['cate'] == $param) {
                return 'class="active"';
            } else {
                return '';
            }
        } else {
            return '';
        }
    }

    public static function getValueMenuCate($param){
        switch ($param){
            case 'all':
                return 1;
                break;
            case 'kinh-doanh':
                return 2;
            case 'marketing':
                return 3;
             case 'ky-nang';
                return 4;
            case 'ngoai-ngu':
                return 5;
            case 'thiet-ket':
                return 6;
            case 'lap-trinh':
                return 7;
            case 'life-style':
                return 8;
            case 'nuoi-con':
                return 9;
            case 'gia-dinh':
                return 10;
            default:
                return 0;
        }
    }

    public static function getValueMenuTag($param){
        switch ($param){
            case 'flashsale':
                return 1;
                break;
            case 'donggia199k':
                return 2;
            case 'donggia299k':
                return 3;
            case 'donggia399k';
                return 4;
            default:
                return 1;
        }
    }

    public static function checkActiveTag($param)
    {
        if (isset($_GET['tag'])) {
            if ($_GET['tag'] == $param) {
                return 'class="active"';
            } else {
                return '';
            }
        } else {
            if ($param == 'flashsale' && !isset($_GET['search'])) {
                return 'class="active"';
            }
        }
    }

    public static function isRediect()
    {
        if (!isset($_GET['tag'])) {
            return true;
        }
        return false;
    }

    public static function isTagFlashSale()
    {
        if (isset($_GET['tag']) && $_GET['tag'] == 'flashsale') {
            return true;
        }
        return false;
    }

    public static function modify_url($mod, $url = false)
    {
        // If $url wasn't passed in, use the current url
        if ($url == false) {
            $scheme = $_SERVER['SERVER_PORT'] == 80 ? 'http' : 'https';
            $url = $scheme . '://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
        }

        // Parse the url into pieces
        $url_array = parse_url($url);

        // The original URL had a query string, modify it.
        if (!empty($url_array['query'])) {
            parse_str($url_array['query'], $query_array);
            foreach ($mod as $key => $value) {
                if (!empty($query_array[$key])) {
                    $query_array[$key] = $value;
                }
            }
        } // The original URL didn't have a query string, add it.
        else {
            $query_array = $mod;
        }
        if (!isset($_GET['tag']) || empty($_GET['tag'])) {
            $query_array['tag'] = 'donggia199k';
        }
        return $url_array['scheme'] . '://' . $url_array['host'] . '' . $url_array['path'] . '?' . http_build_query($query_array);
    }

    public static function resetCart()
    {
        $session = \Yii::$app->session;
        $session->set('carts_campaign_tet', []);
    }

    public static function getCourseHasSelect()
    {
        $session = \Yii::$app->session;
        $carts = $session['carts_campaign_tet'];
        $courseList = self::getCourseList();
        $search = Yii::$app->request->getQueryParam('search', null);
        if (!empty($search)) {
            $search = strtolower($search);
            $courseList = array_filter($courseList, function ($course) use ($search, $carts) {
                if(empty($carts)){
                    return stristr(strtolower($course['name']), $search) == true && in_array($course['id'], []);
                }else{
                    return stristr(strtolower($course['name']), $search) == true && in_array($course['id'], $carts);
                }

            });
            return self::getCourseHtml($courseList);
        }
        if ($carts) {
            $tagSlug = Yii::$app->request->getQueryParam('tag', 'flashsale');
            $courseList = array_filter($courseList, function ($course) use ($carts, $tagSlug) {
                $tagSlugCheck = false;
                switch ($tagSlug) {
                    case self::FLASH_SALE:
                        $tagSlugCheck = intval($course['flash_sale']) > 0;
                        break;
                    case self::DONG_GIA_199K:
                        $tagSlugCheck = intval($course['dong_gia']) == 199000;
                        break;
                    case self::DONG_GIA_299K:
                        $tagSlugCheck = intval($course['dong_gia']) == 299000;
                        break;
                    case self::DONG_GIA_399K:
                        $tagSlugCheck = intval($course['dong_gia']) == 399000;
                        break;
                    default:
                        break;
                }
                return $tagSlugCheck && in_array($course['id'],$carts);
            });
            return self::getCourseHtml($courseList);
        }
        return '';
    }

    public static function percentLiXiTetOther($priceOrder)
    {

        $percent = 0;
        if ($priceOrder >= 2000000) {
            return $percent = 20;
        }

        if ($priceOrder >= 1000000) {
            return $percent = 15;
        }

        if ($priceOrder >= 500000) {
            return $percent = 10;
        }

        if ($priceOrder >= 300000) {
            return $percent = 5;
        }

        return $percent;
    }

    public static function getHoursConfig($dates)
    {
        $start = explode(':', $dates[0]);
        $end = explode(':', $dates[1]);
        return [$start[0] . ':' . $start[1], $end[0] . ':' . $end[1]];
    }

    public static function getHoursTogetherTime($hoursCheck){
        list($hours9_12,$hours14_17,$hours20_23) = CampaignTet::hoursFlashSale();
        $hour9 = $hours9_12[0];
        $hour14 = $hours14_17[0];
        $hour20 = $hours20_23[0];

        $currentTime = time();
        $hours = ['00:00:00','23:59:59'];
        if($currentTime < strtotime($hour9)){
            $hours = $hours9_12;
        }else if(date('H:i:s') < $hour14 ){
            $hours = $hours14_17;
        }else if(date('H:i:s') < $hour20 ){
            $hours = $hours20_23;
        }
        if(($hours[0] == $hoursCheck[0]) && ($hours[1] == $hoursCheck[1])){
            return true;
        }
        else{
            return false;
        }

    }
}
