<p>Kính gửi quý <?php echo $gender.' '.$fullName ?>, </p>
<br>
<p>Câu hỏi <strong>#<?= $questionId ?></strong> được học viên <?= $studentName ?> đặt cho <?php echo $gender ?> trong tuần qua được ban quản trị của Kyna.vn duyệt.</p>
<p>Thông tin câu hỏi:</p>
<p>&emsp;&emsp;Thời gian: <?= $questionDate ?></p>
<p>&emsp;&emsp;Khóa học: <?= $courseName ?></p>
<p>&emsp;&emsp;Nội dung: <?= $questionContent ?></p>
<br>
<p>Để trả lời câu hỏi này, <?php echo $gender ?> có thể click trả lời email này, Kyna.vn sẽ giúp <?php echo $gender ?> chuyển câu trả lời đến cho học viên.</p>
<p>Cám ơn <?php echo $gender ?> và chúc <?php echo $gender ?> một tuần mới nhiều năng lượng!</p>
<br>
<p>Trân trọng,<br />Ban quản trị Kyna.vn</p>
