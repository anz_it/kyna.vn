<p><?= $fullname ?> thân mến,</p>

<p>Câu hỏi #<?= $questionId ?> của bạn dành cho khóa học  "<?= $courseName ?>" với nội dung "<?php echo $questionContent ?>" đã được giảng viên trả lời.</p>

<p>Nội dung câu trả lời:<br />
   <?= $answerContent ?>
</p>

<p>Để xem chi tiết hơn, bạn có thể click vào link sau đây: <a href="<?= $qaLink ?>"><?= $qaLink ?></a></p>

<p>Cám ơn bạn đã đặt câu hỏi và chúc bạn một ngày nhiều năng lượng!</p>

<p>Thân mến,<br />
    Ban quản trị Kyna.vn</p>
