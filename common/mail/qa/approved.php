<p><?php echo $fullname ?> thân mến,</p>

<p>Câu hỏi #<?php echo $questionId ?> của bạn dành cho khóa học "<?php echo $courseName ?>" với nội dung "<?php echo $questionContent ?>" đã được ban quản trị Kyna.vn duyệt.</p>

<p>Câu hỏi này đang được Kyna.vn gửi đến cho giảng viên và chờ câu trả lời từ giảng viên. Kyna.vn sẽ thông báo qua email cho bạn biết khi Kyna.vn nhận được câu trả lời từ giảng viên.</p>

<p>Để xem lại câu hỏi này, bạn có thể click vào link sau đây: <a href="<?php echo $qaLink ?>"><?php echo $qaLink ?></a> </p>

<p>Cám ơn bạn đã đặt câu hỏi và chúc bạn một ngày nhiều năng lượng!</p>

<p>Thân mến,<br />
    Ban quản trị Kyna.vn</p>
