<p><?php echo $fullname ?> thân mến,</p>

<p>Câu hỏi #<?php echo $questionId ?> của bạn dành cho khóa học "<?php echo $courseName ?>" với nội dung "<?php echo $questionContent ?>" không phù hợp với mục Đặt câu hỏi cho giảng viên.</p>

<p>Câu hỏi của bạn thuộc nhóm câu hỏi / bình luận mang tính giao lưu, làm quen, chia sẻ trải nghiệm bản thân trong mục &ldquo;Thảo luận&rdquo; nên ban quản trị của Kyna.vn đã chuyển câu hỏi của bạn qua mục &ldquo;Thảo luận&rdquo;</p>

<p>Bạn có thể xem lại Quy định mục đặt câu hỏi cho giảng viên tại link:
    <a href="https://kyna.vn/bai-viet/quy-dinh-dat-cau-hoi-cho-giang-vien">https://kyna.vn/bai-viet/quy-dinh-dat-cau-hoi-cho-giang-vien</a>
</p>

<p>Cám ơn bạn và chúc bạn một ngày nhiều năng lượng!</p>

<p>Thân mến,<br />
    Ban quản trị Kyna.vn</p>
