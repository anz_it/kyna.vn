<p>Kính gửi quý <?php echo $gender.' '.$fullName ?>, </p>
<br>
<p>Câu hỏi được học viên đặt cho <?php echo $gender ?> trong tuần qua được ban quản trị của Kyna.vn duyệt như sau:</p>
<?php
foreach($questionCourses as $courseId => $questions) {
    $course = \app\modules\course\models\Course::findOne($courseId);
    echo "<p><strong>- {$course->name}</strong></p>";
    foreach ($questions as $questionId) {
        $question = \kyna\course\models\CourseLearnerQna::findOne($questionId);
        $date = \Yii::$app->formatter->asDatetime($question->posted_time, "php:d-m-Y  H:i:s");
        $content = strip_tags($question->content);
        echo "&emsp;&emsp;#{$questionId} - {$date} - {$content} <br>";
    }
} ?>
<br>
<p>Để trả lời những câu hỏi này, <?php echo $gender ?> có thể click trả lời email này, Kyna.vn sẽ giúp <?php echo $gender ?> chuyển câu trả lời đến cho học viên.</p>
<p>Cám ơn <?php echo $gender ?> và chúc <?php echo $gender ?> một tuần mới nhiều năng lượng!</p>
<br>
<p>Trân trọng,<br />Ban quản trị Kyna.vn</p>
