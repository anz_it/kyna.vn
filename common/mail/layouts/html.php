<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use kyna\settings\models\Setting;

$settings = ArrayHelper::map(Setting::find()->all(), 'key', 'value');
?>
<?php $this->beginPage() ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=<?= Yii::$app->charset ?>" />
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
    <?php $this->beginBody() ?>
    
        <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" style="font-family: Tahoma, Arial;background-color:#F5F5F5;padding:50px 0;" >
            <tbody>
                <tr>
                    <td width="100%" valign="top" style="padding-top:20px;" >
                        <table  width="682" border="0" cellpadding="0" cellspacing="0" align="center" style="border: 1px solid #DEDEDE; box-shadow:0 0 7px #DEDEDE;">
                            <tbody>
                                <tr>
                                    <td align="center" bgcolor="#fff" style="padding-top:24px; color: #fff">
                                        <div style="background: url(http://kyna.vn/media/images/layout_v2/logo_kyna.png); height:30px; width:181px; ">

                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" bgcolor="#fff" style="padding:20px 0; color: #fff">
                                        <div style="background: url(http://kyna.vn/media/images/layout_v2/cod_shipping_intro.png); height:30px; width:549px; ">

                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" bgcolor="#45C0A0" style="padding:6px 0;">
                                        <table style="color:#fff; font-size:11.91px">
                                            <tr>
                                                <td align ="center" style="padding:3px 20px; border-right:1px solid #fff">
                                                    <a href="http://kyna.vn/danh-sach-khoa-hoc/giao-tiep" style="text-decoration: none; color:#fff">Giao tiếp</a>
                                                </td>
                                                <td align ="center" style="padding:3px 20px; border-right:1px solid #fff">
                                                    <a href="http://kyna.vn/danh-sach-khoa-hoc/kham-pha-ban-than" style="text-decoration: none; color:#fff">Khám phá bản thân</a>
                                                </td>
                                                <td align ="center" style="padding:3px 20px; border-right:1px solid #fff">
                                                    <a href="http://kyna.vn/danh-sach-khoa-hoc/kinh-doanh-khoi-nghiep" style="text-decoration: none; color:#fff">Khởi nghiệp</a>
                                                </td>
                                                <td align ="center" style="padding:3px 20px; border-right:1px solid #fff">
                                                    <a href="http://kyna.vn/danh-sach-khoa-hoc/nuoi-day-con" style="text-decoration: none; color:#fff">Nuôi dạy con</a>
                                                </td>
                                                <td align ="center" style="padding:3px 20px; border-right:1px solid #fff">
                                                    <a href="http://kyna.vn/danh-sach-khoa-hoc/cong-nghe" style="text-decoration: none; color:#fff">Công nghệ</a>
                                                </td>
                                                <td align ="center" style="padding:3px 20px;">
                                                    <a href="http://kyna.vn/danh-sach-khoa-hoc/nghe-thuat-va-ngoai-ngu" style="text-decoration: none; color:#fff">Nghệ thuật</a>
                                                </td>
                                            </tr>
                                        </table>

                                    </td>
                                </tr>
                                <!-- Header -->

                                <tr>
                                    <td style="padding: 42px 26px 50px 26px;line-height: 1.3em; background-color: #fff; color: #666; font-size: 13px; text-align: justify">
                                        <div>
                                            <?= $content ?>
                                        </div>
                                    </td>
                                </tr>

                                <tr>
                                    <td align="left" style="padding: 20px 22px;; font-size:13px;border-top:3px solid #E6E6E6; line-height: 1.38; font-family:Tahoma" bgcolor="#F5F5F5">
                                        <div style="color:#868585">
                                            <p>Nếu không muốn tiếp tục nhận email giới thiệu nữa, bạn vui lòng hủy nhận <a href="[unsubscribe]" style="color:#1F6FB1; text-decoration:none">tại đây</a>.</p>
                                            <p>Mức giá, thông tin sản phẩm và các khuyến mãi trong email này là chính xác tại thời điểm gởi mail, tuy nhiên
                                                chúng có giới hạn thời gian và có thể thay đổi.</p>
                                        </div>
                                    </td>
                                </tr>

                                <tr>
                                    <td align="left" style="padding: 20px 22px;border:1px solid #ddd; border-width: 1px 0" bgcolor="#EAEAEA">
                                        <table style="width:100%; font-size:13px;line-height: 1.38; font-family:Tahoma" border="0">
                                            <tr>
                                                <td style="width:460px">
                                                    <div style="color:#727272">
                                                        © 2014 - Bản quyền của Công Ty Cổ Phần Dream Viet Education<br/>                                                        
                                                        <!--
                                                        setting company_address and bussiness_certificate
                                                        -->                                                        
                                                    </div>
                                                </td>
                                                <td>
                                                    <div style="font-size:14px;text-align:right;margin-bottom:13px"><strong>Follow us</strong></div>
                                                    <div style="text-align:right">
                                                        <a href="https://www.facebook.com/kyna.vn"><img src="http://kyna.vn/media/images/layout_v2/ico_facebook_26x26.png" /></a>
                                                        <a href="#"><img src="http://kyna.vn/media/images/layout_v2/ico_twitter_26x26.png" /></a>
                                                        <a href="#"><img src="http://kyna.vn/media/images/layout_v2/ico_pinterest_26x26.png" /></a>
                                                        <a href="http://plus.google.com/114419162248284854241?rel=author"><img src="http://kyna.vn/media/images/layout_v2/ico_google-plus_26x26.png" /></a>
                                                        <a href="https://www.youtube.com/user/hanhtrinhdelta"><img src="http://kyna.vn/media/images/layout_v2/ico_youtube_26x26.png" /></a>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>   
    <?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>