<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use kyna\settings\models\Setting;

$settings = ArrayHelper::map(Setting::find()->all(), 'key', 'value');

$hotline = !empty($settings['hot_line']) ? $settings['hot_line'] : '1900.6364.09';
$supportMail = !empty($settings['email_footer']) ? $settings['email_footer'] : 'hotro@kyna.vn';
?>
<?php $this->beginPage() ?>
<!doctype html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html;" charset="<?= Yii::$app->charset ?>"/>
        <title><?= Html::encode($this->title) ?></title>
        <style>
            /* ------------------------------------- 
            GLOBAL 
            ------------------------------------- */
            * { 
                margin:0;
                padding:0;
            }
            * { font-family: "arial",sans-serif }
            img { 
                max-width: 100%; 
            }
            .collapse {
                margin:0;
                padding:0;
            }
            body {
                -webkit-font-smoothing:antialiased; 
                -webkit-text-size-adjust:none; 
                width: 100%!important; 
                height: 100%;
            }
            /* ------------------------------------- 
            ELEMENTS 
            ------------------------------------- */
            a { color: #2BA6CB;}
            .btn {
                text-decoration:none;
                color: #FFF;
                background-color: #666;
                padding:10px 16px;
                font-weight:bold;
                margin-right:10px;
                text-align:center;
                cursor:pointer;
                display: inline-block;
            }
            p.callout {
                padding:15px;
                background-color:#ECF8FF;
                margin-bottom: 15px;
            }
            .callout a {
                font-weight:bold;
                color: #2BA6CB;
            }
            table.social {
                /*  padding:15px; */
                background-color: #ebebeb;
            }
            .social .soc-btn {
                padding: 3px 7px;
                font-size:12px;
                margin-bottom:10px;
                text-decoration:none;
                color: #FFF;font-weight:bold;
                display:block;
                text-align:center;
            }
            a.fb { background-color: #3B5998!important; }
            a.tw { background-color: #1daced!important; }
            a.gp { background-color: #DB4A39!important; }
            a.ms { background-color: #000!important; }
            .sidebar .soc-btn { 
                display:block;
                width:100%;
            }
            /* ------------------------------------- 
            HEADER 
            ------------------------------------- */
            table.head-wrap { width: 100%;}
            .header.container table td.logo { padding: 15px; }
            .header.container table td.label { padding: 15px; padding-left:0px;}
            /* ------------------------------------- 
            BODY 
            ------------------------------------- */
            table.body-wrap { width: 100%;}
            /* ------------------------------------- 
            FOOTER 
            ------------------------------------- */
            table.footer-wrap { width: 100%;    clear:both!important;
            }
            .footer-wrap .container td.content  p { border-top: 1px solid rgb(215,215,215); padding-top:15px;}
            .footer-wrap .container td.content p {
                font-size:10px;
                font-weight: bold;
            }
            /* ------------------------------------- 
            TYPOGRAPHY 
            ------------------------------------- */
            h1,h2,h3,h4,h5,h6 {
                font-family: "HelveticaNeue-Light", "Helvetica Neue Light", "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif; line-height: 1.1; margin-bottom:15px; color:#000;
            }
            h1 small, h2 small, h3 small, h4 small, h5 small, h6 small { font-size: 60%; color: #6f6f6f; line-height: 0; text-transform: none; }
            h1 { font-weight:200; font-size: 44px;}
            h2 { font-weight:200; font-size: 37px;}
            h3 { font-weight:500; font-size: 27px;}
            h4 { font-weight:500; font-size: 23px;}
            h5 { font-weight:900; font-size: 17px;}
            h6 { font-weight:900; font-size: 14px; text-transform: uppercase; color:#444;}
            .collapse { margin:0!important;}
            p, ul { 
                margin-bottom: 10px; 
                font-weight: normal; 
                font-size:14px; 
                line-height:1.6;
            }
            p.lead { font-size:17px; }
            p.last { margin-bottom:0px;}
            ul li {
                margin-left:5px;
                list-style-position: inside;
            }
            /* ------------------------------------- 
            SIDEBAR 
            ------------------------------------- */
            ul.sidebar {
                background:#ebebeb;
                display:block;
                list-style-type: none;
            }
            ul.sidebar li { display: block; margin:0;}
            ul.sidebar li a {
                text-decoration:none;
                color: #666;
                padding:10px 16px;
                /*  font-weight:bold; */
                margin-right:10px;
                /*  text-align:center; */
                cursor:pointer;
                border-bottom: 1px solid #777777;
                border-top: 1px solid #FFFFFF;
                display:block;
                margin:0;
            }
            ul.sidebar li a.last { border-bottom-width:0px;}
            ul.sidebar li a h1,ul.sidebar li a h2,ul.sidebar li a h3,ul.sidebar li a h4,ul.sidebar li a h5,ul.sidebar li a h6,ul.sidebar li a p { margin-bottom:0!important;}
            /* --------------------------------------------------- 
            RESPONSIVENESS
            Nuke it from orbit. It's the only way to be sure. 
            ------------------------------------------------------ */
            /* Set a max-width, and make it display as block so it will automatically stretch to that width, but will also shrink down on a phone or something */
            .container {
                display:block!important;
                max-width:800px!important;
                margin:0 auto!important; /* makes it centered */
                clear:both!important;
            }
            /* This should also be a block element, so that it will fill 100% of the .container */
            .content {
                padding:15px;
                max-width:800px;
                margin:0 auto;
                display:block; 
            }
            /* Let's make sure tables in the content area are 100% wide */
            .content table { width: 100%; }
            /* Odds and ends */
            .column {
                width: 300px;
                float:left;
            }
            .column tr td { padding: 15px; }
            .column-wrap { 
                padding:0!important; 
                margin:0 auto; 
                max-width:800px!important;
            }
            .column table { width:100%;}
            .social .column {
                width: 280px;
                min-width: 279px;
                float:left;
            }
            /* Be sure to place a .clear element after each set of columns, just to be safe */
            .clear { display: block; clear: both; }
            /* ------------------------------------------- 
            PHONE
            For clients that support media queries.
            Nothing fancy. 
            -------------------------------------------- */
            @media only screen and (max-width: 800px) {
                a[class="btn"] { display:block!important; margin-bottom:10px!important; background-image:none!important; margin-right:0!important;}
                div[class="column"] { width: auto!important; float:none!important;}
                table.social div[class="column"] {
                    width:auto!important;
                }
            }
        </style>
        <style>
            .bold {
                font-weight: bold;
            }
            /* HEADER */
            .header ul {
                padding: 0px;
                margin: 0px;
                list-style-type: none;
            }    
            .header .content {
                padding-left: 0px;
                padding-right: 0px;
            }
            .header .content ul li span {
                margin-left: 5px;
            }
            /* NAV */
            .content.nav {
                padding: 0px;
            }
            .content.nav tr td tr td {
                border-right: 1px solid white;
            }
            /* CONTENT */
            .title-main {
                color: #50ad4e; 
                font-size: 24px; 
                text-align: center;
                font-weight: bold;
                padding: 50px 0px;
                margin-bottom: 0px;
                line-height: 40px;
            }
            .content.main img {
                margin: 0px auto 35px;
                display: inherit;
            }
            .color-green {
                color: #50ad4e;
            }
            .content.main ul.top {
                padding: 0px;
                margin: 0px;
                list-style: none;
                text-align: center;
            }
            .content.main ul.top li {        
                margin: 0px auto 10px;;
            }
            .content.main ul.top li:last-child {
                border: 1px solid #00b8e3;
                padding: 10px;
                border-radius: 5px;
            }
            .content.main .text p {
                text-align: center;        
            }
            .content.main .line {
                margin: 10px 0px;
                display: inline-block;
                width: 100%;
            }
            .content.main .line hr {
                width: 50%;
                margin: 0px auto;
            }
            .content.main ul.bottom {
                padding: 0px;
                list-style: none;
                text-align: center;
            }
            .social {
                display: inline-block;
                width: 100%;
                text-align: center;
            }
            .social ul {
                padding: 0px;
                margin: 5px 0px;
                list-style: none;
                display: inline-block;
            }
            .social ul li {
                float: left;
                margin-right: 15px;
            }
            .content.main .social img {
                margin-bottom: 0px;
            }
            /* FOOTER */
            .footer.container {
                background: none;
            }
            .footer.container p {
                text-align: center;
                color: #7c7c7c;
            }
            .main_cta img { width: 100%; height: auto !important; }
            /* Top Links Navigation Pattern CSS */
            @media only screen and (max-width: 632px) {
                td[class="main_nav"] td {
                    display: inline-block;
                }
            }        
        </style>
        <?php $this->head() ?>
    </head>
    <body bgcolor="#f0f0f0">
        <?php $this->beginBody() ?>        
        <table align="center" border="0" cellpadding="0" cellspacing="0" style="font-family: Tahoma, Arial;background-color:#f0f0f0;" width="100%">
            <tbody>
                <tr>
                    <td>
                        <table class="body-wrap" style="width: 100%; line-height: 20px">
                            <tbody>
                                <tr>                     
                                    <td bgcolor="white" class="container" style="display: block!important;max-width:800px!important;margin: 0 auto!important;clear: both!important;">
                                        <div class="content main">
                                            <?= $content ?>                                            
                                        </div>
                                    </td>                   
                                </tr>
                            </tbody>
                        </table>

                    </td>
                </tr>
            </tbody>
        </table>
        <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>
