<?php
/**
 * Created by PhpStorm.
 * User: ngunp
 * Date: 7/19/2017
 * Time: 10:10 AM
 */
$mediaBaseUrl = Yii::$app->params['media_link'];
?>
<table align="center" border="0" cellpadding="0" cellspacing="0" style="border: 1px solid #DEDEDE; box-shadow:0 0 7px #DEDEDE;" width="660">
    <tbody>
    <tr>
        <td bgcolor="#fff" style="padding: 0 0 0px 0px; font-size: 12px; text-align: center;"><img alt="" src="<?= $mediaBaseUrl ?>/img/mail/user-telesale/quan-tam-khoa-hoc-khac-logo.png" style="width: 660px; height: 49px;"></td>
    </tr>
    <tr>
        <td bgcolor="#fff" style="padding: 0 0 0px 0px; font-size: 12px; text-align: center;"><a href="https://kyna.vn/danh-sach-khoa-hoc/phu-nu-va-gia-dinh"><img alt="Giảm thêm 20% - Rinh ngay khóa học phù hợp với bạn" src="<?= $mediaBaseUrl ?>/img/mail/user-telesale/quan-tam-khoa-hoc-khac-bigimage.png" style="width: 660px; height: 388px;"></a></td>
    </tr>
    <tr>
        <td style="padding: 0px 30px 20px 30px; text-align: justify; background-color: rgb(255, 255, 255);">
            <p style="text-align: center;"><span style="color:#FF8C00;"><span style="font-size:17px;"><span style="font-family:arial,helvetica,sans-serif;"><span style="text-align: justify; line-height: 20.8px; background-color: rgb(255, 255, 255);"><strong>LOAY HOAY CHƯA TÌM ĐƯỢC KHÓA HỌC BẠN CẦN?</strong></span></span></span></span></p>

            <p style="text-align: center;"><span style="color:#333333;"><span style="font-size:14px;"><span style="font-family:arial,helvetica,sans-serif;line-height: 23.8px;">Đừng vội nản chí, còn <strong>hơn 499 khóa học</strong> vô cùng hấp dẫn đang đợi bạn&nbsp;tại Kyna.vn đấy!</span></span></span><br>
                <span style="color: rgb(51, 51, 51); font-family: Arial; font-size: 14px; text-align: justify;line-height: 20.8px; background-color: rgb(255, 255, 255);"><strong>Đặc biệt</strong>, nhập mã </span><span style="font-family: Arial; text-align: justify; line-height: 23.8px; background-color: rgb(255, 255, 255);"><font color="#31a837"><b><?= $coupon ?></b></font></span><span style="color: rgb(51, 51, 51); font-family: Arial; font-size: 14px; text-align: justify;line-height: 20.8px; background-color: rgb(255, 255, 255);">&nbsp;– &nbsp;<strong>Tiết kiệm thêm&nbsp;20% học phí</strong></span><span style="color: rgb(51, 51, 51); font-family: Arial; font-size: 14px; text-align: justify; background-color: rgb(255, 255, 255);">&nbsp;–</span><span style="color: rgb(51, 51, 51); font-family: Arial; font-size: 14px; text-align: justify;line-height: 20.8px; background-color: rgb(255, 255, 255);"> "Rinh lại lần nữa"&nbsp;khóa học phù hợp nhất với bạn!</span></p>

            <table align="center" border="0" cellpadding="0" cellspacing="0" class="mobile_centered_table">
                <tbody>
                <tr>
                    <td align="center" bgcolor="#31a837" class="button" data-color-buttonorangebg="background-color" data-editable="button" height="40" style="display: block; border-radius: 2px; background-color: #31a837; border-color: rgb(167, 172, 176); border-width: 0px;" width="140">
                        <table border="0" cellpadding="0" cellspacing="0">
                            <tbody>
                            <tr>
                                <td align="center" style="font-family: 'Arial', sans-serif; font-size: 13px; color: #ffffff; text-align: center; font-weight: bold;"><a data-color-buttonwhitetext="color" href="https://kyna.vn/danh-sach-khoa-hoc/phu-nu-va-gia-dinh" style="text-decoration: none; color: #ffffff; width: 100%; display: block; line-height: 40px;" target="_blank">KHÁM PHÁ NGAY&nbsp;</a></td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>

            <p>&nbsp;</p>

            <hr>
            <p><em><strong><span style="color:#333333;"><span style="font-size:14px;"><span style="font-family:arial,helvetica,sans-serif;">Điều kiện áp dụng:</span></span></span></strong></em></p>

            <ul>
                <li><span style="color:#333333;"><span style="font-size:14px;line-height: 20.8px;"><span style="font-family:arial,helvetica,sans-serif;">Giảm 20% cho khóa học bất kỳ tại Kyna.vn</span></span></span></li>
<!--                <li><font color="#333333" face="arial, helvetica, sans-serif"><span style="font-size: 14px;line-height: 20.8px;">Áp dụng cho đơn hàng từ 500.000đ trở lên</span></font></li>-->
                <li><span style="color: rgb(51, 51, 51);"><span style="font-size: 14px;"><span style="font-family: arial, helvetica, sans-serif; line-height: 20.8px;">Không áp dụng cho những khóa học đang được khuyến mãi</span></span></span></li>
                <li><span style="color:#333333;"><span style="font-size:14px;"><span style="font-family:arial,helvetica,sans-serif;line-height: 20.8px;">Mã giảm giá chỉ được sử dụng 1 lần</span></span></span></li>
                <li><span style="color:#333333;"><span style="font-size:14px;"><span style="font-family:arial,helvetica,sans-serif;line-height: 20.8px;">Mã giảm chỉ áp dụng cho học viên nhận được Email này</span></span></span></li>
            </ul>
        </td>
    </tr>
    <tr>
        <td align="left" bgcolor="#F5F5F5" style="padding: 10px 10px;; font-size:13px;border-top:3px solid #31a837; line-height: 2.38; font-family:Tahoma">
            <div style="color:#868585">
                <p style="text-align: center;"><a href="mailto:hotro@kyna.vn" style="text-decoration: none"><span style="color:#696969;"><strong><span style="font-size:12px;"><span style="line-height: 20.8px;">HỖ TRỢ</span></span></strong></span></a><span style="color:#808080;"><span style="font-size:12px;"><span style="line-height: 20.8px;"> | </span></span></span><a href="[unsubscribe]" style="text-decoration:none;"><span style="color:#696969;"><strong><span style="font-size:12px;"><span style="line-height: 20.8px;">UNSUBSCRIBE</span></span></strong></span></a><br>
                    <span style="color:#808080;"><span style="font-size:12px;"><span style="line-height: 20.8px;">© 2014 - Bản quyền của Công Ty Cổ Phần Dream Viet Education<br>
						VP TPHCM: 178/8, Đường D1, Phường 25, Quận Bình Thạnh, TP Hồ Chí Minh<br>
						VP Hà Nội: 25 Vũ Ngọc Phan, Phường Láng Hạ, Quận Đống Đa, TP Hà Nội</span></span></span></p>
            </div>
        </td>
    </tr>
    </tbody>
</table>
