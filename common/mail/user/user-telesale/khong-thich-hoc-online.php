<?php
/**
 * Created by PhpStorm.
 * User: ngunp
 * Date: 7/18/2017
 * Time: 11:11 AM
 */
$mediaBaseUrl = Yii::$app->params['media_link'];
?>
<table align="center" border="0" cellpadding="0" cellspacing="0" style="border: 1px solid #DEDEDE; box-shadow:0 0 7px #DEDEDE;" width="660">
    <tbody>
    <tr>
        <td bgcolor="#fff" style="padding: 0 0 0px 0px; font-size: 12px; text-align: center;"><img alt="" src="<?= $mediaBaseUrl ?>/img/mail/user-telesale/khong-thich-hoc-online-logo.png" style="width: 660px; height: 49px;"></td>
    </tr>
    <tr>
        <td bgcolor="#fff" style="padding: 0 0 0px 0px; font-size: 12px; text-align: center;"><a href="https://kyna.vn/danh-sach-khoa-hoc?facets%5Bdiscount%5D%5B0%5D=0"><img alt="" src="<?= $mediaBaseUrl ?>/img/mail/user-telesale/khong-thich-hoc-online-bigimage.png" style="font-family: Arial; font-size: 12px; text-align: center; width: 660px; height: 252px;"></a></td>
    </tr>
    <tr>
        <td style="padding: 0px 30px 20px 30px; text-align: justify; background-color: rgb(255, 255, 255);">
            <p><span style="color: rgb(51, 51, 51); font-family: Arial; font-size: 14px; text-align: justify;line-height: 20.8px; background-color: rgb(255, 255, 255);"><strong><?= $full_name?></strong> thân mến,</span></p>

            <p><span style="color: rgb(51, 51, 51); font-family: Arial; font-size: 14px; text-align: justify;line-height: 20.8px; background-color: rgb(255, 255, 255);">Người đi làm luôn có nhu cầu cao và động lực thực tế nhất trong việc học. Thế nhưng công việc bận rộn, muốn học mà nhìn đâu cũng chỉ thấy khó!</span></p>

            <p><span style="color: rgb(51, 51, 51); font-family: Arial; font-size: 14px; text-align: justify;line-height: 20.8px; background-color: rgb(255, 255, 255);">&nbsp; &nbsp; &nbsp;</span><span style="font-size:16px;"><span style="color: rgb(51, 51, 51); font-family: Arial; text-align: justify; line-height: 20.8px; background-color: rgb(255, 255, 255);">•</span></span><span style="color: rgb(51, 51, 51); font-family: Arial; font-size: 14px; text-align: justify;line-height: 20.8px; background-color: rgb(255, 255, 255);"> Khó sắp xếp thời gian &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; </span><span style="color: rgb(51, 51, 51); font-family: Arial; font-size: 16px; text-align: justify; background-color: rgb(255, 255, 255);">•&nbsp;</span><span style="color: rgb(51, 51, 51); font-family: Arial; font-size: 14px; text-align: justify;line-height: 20.8px; background-color: rgb(255, 255, 255);">&nbsp;Khó tới lớp đều đặn</span><span style="color: rgb(51, 51, 51); font-family: Arial; font-size: 14px; text-align: justify; background-color: rgb(255, 255, 255);">&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;</span><span style="color: rgb(51, 51, 51); font-family: Arial; font-size: 16px; text-align: justify; background-color: rgb(255, 255, 255);">•&nbsp;</span><span style="color: rgb(51, 51, 51); font-family: Arial; font-size: 14px; text-align: justify;line-height: 20.8px; background-color: rgb(255, 255, 255);"> Khó tiếp thu kiến thức</span></p>

            <table align="center" border="0" cellpadding="0" cellspacing="0">
                <tbody>
                <tr>
                    <td align="center" class="button block black_border editing-element" data-color-buttonblackborder="border-color" data-editable="button" style="display: block !important; border: 1px solid #666666;" width="590">
                        <p><span style="color: rgb(51, 51, 51); font-family: Arial; font-size: 14px; text-align: justify;line-height: 20.8px; background-color: rgb(255, 255, 255);">&nbsp;</span><span style="font-size:16px;"><span style="color: rgb(51, 51, 51); font-family: Arial; text-align: justify; line-height: 20.8px; background-color: rgb(255, 255, 255);"><strong>HÓA GIẢI 3 KHÓ VỚI </strong></span><span style="color:#FF0000;"><span style="font-family: Arial; text-align: justify; line-height: 20.8px; background-color: rgb(255, 255, 255);"><strong>PHƯƠNG PHÁP HỌC TRỰC TUYẾN</strong></span></span><span style="color: rgb(51, 51, 51); font-family: Arial; text-align: justify; line-height: 20.8px; background-color: rgb(255, 255, 255);"><strong> TẠI KYNA.VN</strong></span></span></p>

                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tbody>
                            </tbody>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>

            <table align="center" border="0" cellpadding="1" cellspacing="1" style="width:600px;">
                <tbody>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td><span style="font-family:arial,helvetica,sans-serif;"><span style="font-size:14px;"><span style="color:#333333;"><img src="http://sendy.kyna.vn/uploads/1498729241.png" style="height: 28px; width: 28px;"></span></span></span></td>
                    <td>&nbsp;</td>
                    <td><span style="font-family:arial,helvetica,sans-serif;"><span style="font-size:14px;"><span style="color:#333333;line-height: 20.8px;">Học cùng chuyên gia, chương trình học đa dạng</span></span></span></td>
                    <td><span style="font-family:arial,helvetica,sans-serif;"><span style="font-size:14px;"><span style="color:#333333;">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;</span></span></span></td>
                    <td><span style="font-family:arial,helvetica,sans-serif;"><span style="font-size:14px;"><span style="color:#333333;"><img src="http://sendy.kyna.vn/uploads/1499677595.png" style="font-family: Arial; text-align: justify; background-color: rgb(255, 255, 255); height: 28px; width: 28px;"></span></span></span></td>
                    <td>&nbsp;</td>
                    <td><span style="font-family:arial,helvetica,sans-serif;"><span style="font-size:14px;line-height: 20.8px;"><span style="color:#333333;">Học Online mọi lúc, mọi nơi mọi thiết bị</span></span></span></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td><span style="font-family:arial,helvetica,sans-serif;"><span style="font-size:14px;"><span style="color:#333333;"><img src="http://sendy.kyna.vn/uploads/1498729253.png" style="font-family: Arial; text-align: justify; background-color: rgb(255, 255, 255); height: 28px; width: 28px;"></span></span></span></td>
                    <td>&nbsp;</td>
                    <td><span style="font-family:arial,helvetica,sans-serif;"><span style="font-size:14px;"><span style="color:#333333;line-height: 20.8px;">Thanh toán một lần, sở hữu khóa học trọn đời</span></span></span></td>
                    <td>&nbsp;</td>
                    <td><img src="http://sendy.kyna.vn/uploads/1498729268.png" style="height: 28px; width: 28px;"></td>
                    <td>&nbsp;</td>
                    <td><span style="font-family:arial,helvetica,sans-serif;"><span style="font-size:14px;"><span style="color:#333333;line-height: 20.8px;">Hoàn lại học phí nếu học không thấy hiệu quả</span></span></span></td>
                </tr>
                </tbody>
            </table>

            <p style="text-align: center;"><strong><span style="font-size:14px;"><span style="font-family:arial,helvetica,sans-serif;"><span style="color:#333333;">Tham gia học thử miễn phí - Hạ gục 3 khó ngay hôm nay</span></span></span></strong></p>

            <table align="center" border="0" cellpadding="0" cellspacing="0" class="mobile_centered_table">
                <tbody>
                <tr>
                    <td align="center" bgcolor="#31a837" class="button" data-color-buttonorangebg="background-color" data-editable="button" height="40" style="display: block; border-radius: 2px; background-color: #31a837; border-color: rgb(167, 172, 176); border-width: 0px;" width="140">
                        <table border="0" cellpadding="0" cellspacing="0">
                            <tbody>
                            <tr>
                                <td align="center" style="font-family: 'Arial', sans-serif; font-size: 13px; color: #ffffff; text-align: center; font-weight: bold;"><a data-color-buttonwhitetext="color" href="https://kyna.vn/danh-sach-khoa-hoc?facets%5Bdiscount%5D%5B0%5D=0" style="text-decoration: none; color: #ffffff; width: 100%; display: block; line-height: 40px;" target="_blank">HỌC NGAY</a></td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    <tr>
        <td bgcolor="#fff" style="padding: 0 0 0px 0px; font-size: 12px; text-align: center;">&nbsp;</td>
    </tr>
    <tr>
        <td align="left" bgcolor="#F5F5F5" style="padding: 10px 10px;; font-size:13px;border-top:3px solid #31a837; line-height: 2.38; font-family:Tahoma">
            <div style="color:#868585">
                <p style="text-align: center;"><a href="mailto:hotro@kyna.vn" style="text-decoration: none"><span style="color:#696969;"><strong><span style="font-size:12px;"><span style="line-height: 20.8px;">HỖ TRỢ</span></span></strong></span></a><span style="color:#808080;"><span style="font-size:12px;"><span style="line-height: 20.8px;"> | </span></span></span><a href="[unsubscribe]" style="text-decoration:none;"><span style="color:#696969;"><strong><span style="font-size:12px;"><span style="line-height: 20.8px;">UNSUBSCRIBE</span></span></strong></span></a><br>
                    <span style="color:#808080;"><span style="font-size:12px;"><span style="line-height: 20.8px;">© 2014 - Bản quyền của Công Ty Cổ Phần Dream Viet Education<br>
						VP TPHCM: 178/8, Đường D1, Phường 25, Quận Bình Thạnh, TP Hồ Chí Minh<br>
						VP Hà Nội: 25 Vũ Ngọc Phan, Phường Láng Hạ, Quận Đống Đa, TP Hà Nội</span></span></span></p>
            </div>
        </td>
    </tr>
    </tbody>
</table>