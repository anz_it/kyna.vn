<?php
/**
 * Created by PhpStorm.
 * User: ngunp
 * Date: 7/19/2017
 * Time: 9:32 AM
 */

use common\helpers\Html;
use yii\helpers\Url;

$formatter = Yii::$app->formatter;

$mediaBaseUrl = Yii::$app->params['media_link'];
?>
<table>
    <tbody>
        <tr class="block">
            <!-- Start of preheader -->
            <table width="100%" bgcolor="#f6f4f5" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="preheader">
                <tbody>
                <tr>
                    <td width="100%">
                        <table width="640" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
                            <tbody>
                            <!-- Spacing -->
                            <tr>
                                <td width="100%" height="5"></td>
                            </tr>
                            <!-- Spacing -->
                            <tr>
                                <td align="right" valign="middle" style="font-family: Helvetica, arial, sans-serif; font-size: 10px;color: #999999" st-content="preheader">
                                    If you cannot read this email, please <a class="hlite" href="#" style="text-decoration: none; color: #50ad4e">click here</a>
                                </td>
                            </tr>
                            <!-- Spacing -->
                            <tr>
                                <td width="100%" height="5"></td>
                            </tr>
                            <!-- Spacing -->
                            </tbody>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>
            <!-- End of preheader -->
        </tr>
        <tr class="block">
            <!-- start of header -->
            <table width="100%" bgcolor="#f6f4f5" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="header">
                <tbody>
                <tr>
                    <td>
                        <table width="640" bgcolor="#ffffff" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth" hlitebg="edit" shadow="edit">
                            <tbody>
                            <tr>
                                <td>
                                    <!-- logo -->
                                    <table width="600" cellpadding="0" cellspacing="0" border="0" align="left" class="devicewidth">
                                        <tbody>
                                        <tr>
                                            <td valign="middle" align="center" width="590" style="padding: 10px 0 10px 20px;" class="logo">
                                                <div class="imgpop">
                                                    <a href="https://kyna.vn/"><img src="<?= $mediaBaseUrl ?>/img/mail/user-telesale/huong-dan-thanh-toan-logo.png" alt="logo" border="0" style="display:block; border:none; outline:none; text-decoration:none;" st-image="edit" class="logo"></a>
                                                </div>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <!-- End of logo -->
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>
            <!-- end of header -->
        </tr>
        <tr class="block">
            <!-- image + text -->
            <table width="100%" bgcolor="#f6f4f5" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="bigimage">
                <tbody>
                <tr>
                    <td>
                        <table bgcolor="#ffffff" width="640" align="center" cellspacing="0" cellpadding="0" border="0" class="devicewidth" modulebg="edit">
                            <tbody>
                            <tr>
                                <td width="100%" height="20"></td>
                            </tr>
                            <tr>
                                <td>
                                    <table width="600" align="center" cellspacing="0" cellpadding="0" border="0" class="devicewidthinner">
                                        <tbody>
                                        <tr>
                                            <!-- start of image -->
                                            <td align="center">
                                                <a target="_blank" href="https://kyna.vn/p/kyna/cau-hoi-thuong-gap/huong-dan-thanh-toan-hoc-phi"><img width="600" border="0" height="253" alt="" style="display:block; border:none; outline:none; text-decoration:none;" src="<?= $mediaBaseUrl ?>/img/mail/user-telesale/huong-dan-thanh-toan-bigimage.png" class="bigimage"></a>
                                            </td>
                                        </tr>
                                        <!-- end of image -->
                                        <!-- Spacing -->
                                        <tr>
                                            <td width="100%" height="20"></td>
                                        </tr>
                                        <!-- Spacing -->
                                        <!-- content -->
                                        <tr>
                                            <td style="font-family: Helvetica, arial, sans-serif; font-size: 15px; color: #272727; text-align:left;line-height: 26px;" st-content="rightimage-paragraph">
                                                <p>
                                                    <b><?php echo $full_name; ?></b> thân mến,
                                                </p>
                                                <p>
                                                    Cám ơn bạn đã chọn mua khóa học trên Kyna.vn. Kyna.vn xin gửi bạn hướng dẫn thanh toán cho đơn hàng bạn vừa đặt.
                                                </p>
                                            </td>
                                        </tr>
                                        <!-- end of content -->
                                        <!-- Spacing -->
                                        <tr>
                                            <td width="100%" height="20"></td>
                                        </tr>
                                        <tr>
                                            <td style="font-family: Helvetica, arial, sans-serif; font-size: 15px; color: #ffffff; background: #50ad4e; text-align:left;line-height: 34px;" st-content="rightimage-paragraph">
                                                &nbsp;&nbsp;THÔNG TIN ĐƠN HÀNG
                                            </td>
                                        </tr>
                                        <!-- Spacing -->
                                        <tr>
                                            <td width="100%" height="10"></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>
        </tr>
        <tr class="block">
            <!-- 3-columns -->
            <table width="100%" bgcolor="#f6f4f5" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="3columns">
                <tbody>
                <tr>
                    <td>
                        <table bgcolor="#ffffff" width="640" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth" modulebg="edit">
                            <tbody>
                            <tr>
                                <td>
                                    <table width="600" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
                                        <tbody>
                                        <tr>
                                            <td>
                                                <!-- col 1 -->
                                                <table width="290" align="left" border="0" cellpadding="0" cellspacing="0" class="devicewidth">
                                                    <tbody>

                                                    <tr>
                                                        <td>
                                                            <!-- start of text content table -->
                                                            <table width="290" align="center" border="0" cellpadding="0" cellspacing="0" class="devicewidthinner">
                                                                <tbody>
                                                                <!-- Spacing -->
                                                                <tr>
                                                                    <td width="100%" height="15" style="font-size:1px; line-height:1px; mso-line-height-rule: exactly;">&nbsp;</td>
                                                                </tr>
                                                                <!-- Spacing -->

                                                                <!-- title -->
                                                                <tr>
                                                                    <td style="font-family: Helvetica, arial, sans-serif; font-size: 18px; color: #909090; text-align:left;line-height: 26px;" st-title="3col-title3">
                                                                        Thời gian đặt
                                                                    </td>
                                                                </tr>
                                                                <!-- end of title -->
                                                                <!-- Spacing -->
                                                                <tr>
                                                                    <td width="100%" height="15" style="font-size:1px; line-height:1px; mso-line-height-rule: exactly;">&nbsp;</td>
                                                                </tr>
                                                                <!-- Spacing -->
                                                                <!-- content -->
                                                                <tr>
                                                                    <td style="font-family: Helvetica, arial, sans-serif; font-size: 15px; color: #272727; text-align:left;line-height: 26px;" st-content="3col-para1">
                                                                        <?php echo $formatter->asDatetime($created_time); ?>
                                                                    </td>
                                                                </tr>
                                                                <!-- end of content -->
                                                                <!-- Spacing -->
                                                                <tr>
                                                                    <td width="100%" height="15" style="font-size:1px; line-height:1px; mso-line-height-rule: exactly;">&nbsp;</td>
                                                                </tr>
                                                                <!-- Spacing -->

                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <!-- end of text content table -->
                                                    </tbody>
                                                </table>
                                                <!-- spacing -->
                                                <table width="15" align="left" border="0" cellpadding="0" cellspacing="0" class="removeMobile">
                                                    <tbody>
                                                    <tr>
                                                        <td width="100%" height="15" style="font-size:1px; line-height:1px; mso-line-height-rule: exactly;">&nbsp;</td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                                <!-- end of spacing -->
                                                <!-- col 2 -->
                                                <table width="290" align="left" border="0" cellpadding="0" cellspacing="0" class="devicewidth">
                                                    <tbody>

                                                    <tr>
                                                        <td>
                                                            <!-- start of text content table -->
                                                            <table width="290" align="center" border="0" cellpadding="0" cellspacing="0" class="devicewidthinner">
                                                                <tbody>
                                                                <!-- Spacing -->
                                                                <tr>
                                                                    <td width="100%" height="15" style="font-size:1px; line-height:1px; mso-line-height-rule: exactly;">&nbsp;</td>
                                                                </tr>
                                                                <!-- Spacing -->
                                                                <!-- title2 -->
                                                                <tr>
                                                                    <td style="font-family: Helvetica, arial, sans-serif; font-size: 18px; color: #909090; text-align:left;line-height: 26px;" st-title="3col-title2">
                                                                        Sản phẩm
                                                                    </td>
                                                                </tr>
                                                                <!-- end of title2 -->
                                                                <!-- Spacing -->
                                                                <tr>
                                                                    <td width="100%" height="15" style="font-size:1px; line-height:1px; mso-line-height-rule: exactly;">&nbsp;</td>
                                                                </tr>
                                                                <!-- Spacing -->
                                                                <!-- content2 -->
                                                                <tr>
                                                                    <td style="font-family: Helvetica, arial, sans-serif; font-size: 15px; color: #272727; text-align:left;line-height: 26px;" st-content="3col-para2">
                                                                        <?php echo Html::ul($courses,[
                                                                            'style' => 'padding-left:17px;'
                                                                        ])?>
                                                                    </td>
                                                                </tr>
                                                                <!-- end of content2 -->
                                                                <!-- Spacing -->
                                                                <tr>
                                                                    <td width="100%" height="15" style="font-size:1px; line-height:1px; mso-line-height-rule: exactly;">&nbsp;</td>
                                                                </tr>
                                                                <!-- /Spacing -->

                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <!-- end of text content table -->
                                                    </tbody>
                                                </table>
                                                <!-- end of col 2 -->
                                                <!-- spacing -->
                                                <table width="1" align="left" border="0" cellpadding="0" cellspacing="0" class="removeMobile">
                                                    <tbody>
                                                    <tr>
                                                        <td width="100%" height="15" style="font-size:1px; line-height:1px; mso-line-height-rule: exactly;">&nbsp;</td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                                <!-- end of spacing -->

                                            </td>
                                            <!-- spacing -->
                                            <!-- end of spacing -->
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td width="100%" height="10"></td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>
            <!-- end of 3-columns -->
        </tr>

        <tr class="block">
            <!-- start textbox-with-title -->
            <table width="100%" bgcolor="#f6f4f5" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="fulltext">
                <tbody>
                <tr>
                    <td>
                        <table bgcolor="#ececec" width="640" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth" modulebg="edit" style="background-color: #ececec;">
                            <tbody>
                            <tr>
                                <td>
                                    <table width="600" align="center" cellpadding="0" cellspacing="0" border="0" class="devicewidthinner">
                                        <tbody>
                                        <!-- Spacing -->
                                        <tr>
                                            <td width="100%" height="20"></td>
                                        </tr>
                                        <!-- Spacing -->
                                        <!-- Title -->
                                        <tr>
                                            <td style="font-family: Helvetica, arial, sans-serif; font-size: 18px; color: #333333; text-align:center;line-height: 26px;" st-title="fulltext-title">
                                                Bạn cần thanh toán số tiền
                                            </td>
                                        </tr>
                                        <!-- End of Title -->
                                        <!-- spacing -->
                                        <tr>
                                            <td height="5"></td>
                                        </tr>
                                        <!-- End of spacing -->
                                        <!-- content -->
                                        <tr>
                                            <td style="font-family: Helvetica, arial, sans-serif; font-size: 24px; color: #50ad4e; font-weight:bold; text-align:center;line-height: 30px;" st-content="fulltext-paragraph">
                                                <?php echo $formatter->asCurrency($amount)?>
                                            </td>
                                        </tr>
                                        <!-- End of content -->
                                        <!-- Spacing -->
                                        <tr>
                                            <td width="100%" height="10"></td>
                                        </tr>
                                        <!-- Spacing -->
                                        <!-- Title -->
                                        <tr>
                                            <td style="font-family: Helvetica, arial, sans-serif; font-size: 18px; color: #333333; text-align:center;line-height: 26px;" st-title="fulltext-title">
                                                Bạn có thể chọn các hình thức thanh toán sau:
                                            </td>
                                        </tr>
                                        <!-- End of Title -->
                                        <!-- Spacing -->
                                        <tr>
                                            <td width="100%" height="20"></td>
                                        </tr>
                                        <!-- Spacing -->
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>
            <!-- end of textbox-with-title -->
        </tr>
        <tr class="block">
            <!-- start of left image -->
            <table width="100%" bgcolor="#f6f4f5" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="leftimage">
                <tbody>
                <tr>
                    <td>
                        <table bgcolor="#ffffff" width="640" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth" modulebg="edit">
                            <tbody>
                            <!-- Spacing -->
                            <tr>
                                <td height="20"></td>
                            </tr>
                            <!-- Spacing -->
                            <tr>
                                <td>
                                    <table width="600" align="center" border="0" cellpadding="0" cellspacing="0" class="devicewidthinner">
                                        <tbody>
                                        <tr>
                                            <td>
                                                <!-- start of text content table -->
                                                <table width="160" align="left" border="0" cellpadding="0" cellspacing="0" class="devicewidthinner">
                                                    <tbody>
                                                    <!-- image -->
                                                    <tr>
                                                        <td width="160" height="208" align="center">
                                                            <img src="<?= $mediaBaseUrl ?>/img/mail/user-telesale/huong-dan-thanh-toan-left-image.jpg" alt="" border="0" width="160" height="208" style="display:block; border:none; outline:none; text-decoration:none;">
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                                <!-- mobile spacing -->
                                                <table align="left" border="0" cellpadding="0" cellspacing="0" class="mobilespacing">
                                                    <tbody>
                                                    <tr>
                                                        <td width="100%" height="15" style="font-size:1px; line-height:1px; mso-line-height-rule: exactly;">&nbsp;</td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                                <!-- mobile spacing -->
                                                <!-- start of right column -->
                                                <table width="430" align="right" border="0" cellpadding="0" cellspacing="0" class="devicewidthinner">
                                                    <tbody>
                                                    <!-- title -->
                                                    <tr>
                                                        <td style="font-family: Helvetica, arial, sans-serif; font-size: 18px; font-weight: bold; color: #50ad4e; text-align:left;line-height: 26px;" st-title="leftimage-title">
                                                            CHUYỂN KHOẢN NGÂN HÀNG
                                                        </td>
                                                    </tr>
                                                    <!-- end of title -->
                                                    <!-- Spacing -->
                                                    <tr>
                                                        <td width="100%" height="10"></td>
                                                    </tr>
                                                    <!-- Spacing -->
                                                    <!-- content -->
                                                    <tr>
                                                        <td style="font-family: Helvetica, arial, sans-serif; font-size: 15px; color: #272727; text-align:left;line-height: 26px;" st-content="leftimage-paragraph">
                                                            <p>
                                                                • Số tài khoản: <b>0531 0025 11245</b>.
                                                            </p>
                                                            <p>
                                                                • Chủ tài khoản: Công ty cổ phần DREAM VIET EDUCATION.
                                                            </p>
                                                            <p>
                                                                • Ngân hàng: Vietcombank, Chi nhánh Đông Sài Gòn, TP.HCM.
                                                            </p>
                                                            <p>
                                                                • Ghi chú chuyển khoản: <b>Số điện thoại - Họ và tên - Email đăng ký học - Mã đơn hàng</b>. Ví dụ: 0909090909 - Nguyen Thi Huong Lan - nguyenthihuonglan@gmail.com - 2313123
                                                            </p>
                                                        </td>
                                                    </tr>
                                                    <!-- end of content -->
                                                    </tbody>
                                                </table>
                                                <!-- end of right column -->
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <!-- Spacing -->
                            <tr>
                                <td height="20"></td>
                            </tr>
                            <!-- Spacing -->
                            </tbody>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>
            <!-- end of left image -->
        </tr>
        <tr class="block">
            <!-- start of left image -->
            <table width="100%" bgcolor="#f6f4f5" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="leftimage">
                <tbody>
                <tr>
                    <td>
                        <table bgcolor="#ffffff" width="640" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth" modulebg="edit">
                            <tbody>
                            <!-- Spacing -->
                            <tr>
                                <td height="20"></td>
                            </tr>
                            <!-- Spacing -->
                            <tr>
                                <td>
                                    <table width="600" align="center" border="0" cellpadding="0" cellspacing="0" class="devicewidthinner">
                                        <tbody>
                                        <tr>
                                            <td>
                                                <!-- start of text content table -->
                                                <table width="160" align="left" border="0" cellpadding="0" cellspacing="0" class="devicewidthinner">
                                                    <tbody>
                                                    <!-- image -->
                                                    <tr>
                                                        <td width="160" height="208" align="center">
                                                            <img src="<?= $mediaBaseUrl ?>/img/mail/user-telesale/huong-dan-thanh-toan-left-image1.jpg" alt="" border="0" width="160" height="208" style="display:block; border:none; outline:none; text-decoration:none;">
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                                <!-- mobile spacing -->
                                                <table align="left" border="0" cellpadding="0" cellspacing="0" class="mobilespacing">
                                                    <tbody>
                                                    <tr>
                                                        <td width="100%" height="15" style="font-size:1px; line-height:1px; mso-line-height-rule: exactly;">&nbsp;</td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                                <!-- mobile spacing -->
                                                <!-- start of right column -->
                                                <table width="430" align="right" border="0" cellpadding="0" cellspacing="0" class="devicewidthinner">
                                                    <tbody>
                                                    <!-- title -->
                                                    <tr>
                                                        <td style="text-transform: uppercase; font-family: Helvetica, arial, sans-serif; font-size: 18px; font-weight: bold; color: #50ad4e; text-align:left;line-height: 26px;" st-title="leftimage-title">
                                                            Thẻ Visa/Mastercard qua cổng Onepay
                                                        </td>
                                                    </tr>
                                                    <!-- end of title -->
                                                    <!-- Spacing -->
                                                    <tr>
                                                        <td width="100%" height="10"></td>
                                                    </tr>
                                                    <!-- Spacing -->
                                                    <!-- content -->
                                                    <tr>
                                                        <td style="font-family: Helvetica, arial, sans-serif; font-size: 15px; color: #272727; text-align:left;line-height: 26px;" st-content="leftimage-paragraph">
                                                            <p>Giao dịch được đảm bảo và thực hiện trên cổng thanh toán Onepay</p>
                                                            <p>Click nút Thanh toán bên dưới để thực hiện thanh toán bằng cách nhập thông tin thẻ</p>
                                                        </td>
                                                    </tr>
                                                    <!-- end of content -->
                                                    <!-- Spacing -->
                                                    <tr>
                                                        <td width="100%" height="10"></td>
                                                    </tr>
                                                    <!-- button -->
                                                    <tr>
                                                        <td>
                                                            <table height="30" align="left" valign="middle" border="0" cellpadding="0" cellspacing="0" class="tablet-button" st-button="edit">
                                                                <tbody>
                                                                <tr>
                                                                    <td width="auto" align="center" valign="middle" height="30" style=" background-color:#50ad4e; border-top-left-radius:4px; border-bottom-left-radius:4px;border-top-right-radius:4px; border-bottom-right-radius:4px; background-clip: padding-box;font-size:15px; font-family:Helvetica, arial, sans-serif; text-align:center;  color:#ffffff; font-weight: 300; padding-left:18px; padding-right:18px;">
                                                <span style="color: #ffffff; font-weight: 300;">
                                                    <a style="color: #ffffff; text-align:center;text-decoration: none;" href="<?php echo $payment_link_visa_master; ?>">Thanh toán</a>
                                                 </span>
                                                                    </td>
                                                                </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <!-- /button -->
                                                    </tbody>
                                                </table>
                                                <!-- end of right column -->
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <!-- Spacing -->
                            <tr>
                                <td height="20"></td>
                            </tr>
                            <!-- Spacing -->
                            </tbody>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>
            <!-- end of left image -->
        </tr>
        <tr class="block">
            <!-- start of left image -->
            <table width="100%" bgcolor="#f6f4f5" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="leftimage">
                <tbody>
                <tr>
                    <td>
                        <table bgcolor="#ffffff" width="640" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth" modulebg="edit">
                            <tbody>
                            <!-- Spacing -->
                            <tr>
                                <td height="20"></td>
                            </tr>
                            <!-- Spacing -->
                            <tr>
                                <td>
                                    <table width="600" align="center" border="0" cellpadding="0" cellspacing="0" class="devicewidthinner">
                                        <tbody>
                                        <tr>
                                            <td>
                                                <!-- start of text content table -->
                                                <table width="160" align="left" border="0" cellpadding="0" cellspacing="0" class="devicewidthinner">
                                                    <tbody>
                                                    <!-- image -->
                                                    <tr>
                                                        <td width="160" height="208" align="center">
                                                            <img src="<?= $mediaBaseUrl ?>/img/mail/user-telesale/huong-dan-thanh-toan-left-image2.jpg" alt="" border="0" width="160" height="208" style="display:block; border:none; outline:none; text-decoration:none;">
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                                <!-- mobile spacing -->
                                                <table align="left" border="0" cellpadding="0" cellspacing="0" class="mobilespacing">
                                                    <tbody>
                                                    <tr>
                                                        <td width="100%" height="15" style="font-size:1px; line-height:1px; mso-line-height-rule: exactly;">&nbsp;</td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                                <!-- mobile spacing -->
                                                <!-- start of right column -->
                                                <table width="430" align="right" border="0" cellpadding="0" cellspacing="0" class="devicewidthinner">
                                                    <tbody>
                                                    <!-- title -->
                                                    <tr>
                                                        <td style="text-transform: uppercase; font-family: Helvetica, arial, sans-serif; font-size: 18px; font-weight: bold; color: #50ad4e; text-align:left;line-height: 26px;" st-title="leftimage-title">
                                                            Thẻ ATM Nội địa qua cổng Onepay
                                                        </td>
                                                    </tr>
                                                    <!-- end of title -->
                                                    <!-- Spacing -->
                                                    <tr>
                                                        <td width="100%" height="10"></td>
                                                    </tr>
                                                    <!-- Spacing -->
                                                    <!-- content -->
                                                    <tr>
                                                        <td style="font-family: Helvetica, arial, sans-serif; font-size: 15px; color: #272727; text-align:left;line-height: 26px;" st-content="leftimage-paragraph">
                                                            <p>Giao dịch được đảm bảo và thực hiện trên cổng thanh toán Onepay</p>
                                                            <p>Danh sách NH có thể thanh toán</p>
                                                            <img src="http://i.imgur.com/41QxGvQ.png" width="400px">
                                                            <p>Click nút Thanh toán bên dưới để thực hiện thanh toán bằng cách nhập thông tin thẻ</p>
                                                        </td>
                                                    </tr>
                                                    <!-- end of content -->
                                                    <!-- Spacing -->
                                                    <tr>
                                                        <td width="100%" height="10"></td>
                                                    </tr>
                                                    <!-- button -->
                                                    <tr>
                                                        <td>
                                                            <table height="30" align="left" valign="middle" border="0" cellpadding="0" cellspacing="0" class="tablet-button" st-button="edit">
                                                                <tbody>
                                                                <tr>
                                                                    <td width="auto" align="center" valign="middle" height="30" style=" background-color:#50ad4e; border-top-left-radius:4px; border-bottom-left-radius:4px;border-top-right-radius:4px; border-bottom-right-radius:4px; background-clip: padding-box;font-size:15px; font-family:Helvetica, arial, sans-serif; text-align:center;  color:#ffffff; font-weight: 300; padding-left:18px; padding-right:18px;">
                                                <span style="color: #ffffff; font-weight: 300;">
                                                  <a style="color: #ffffff; text-align:center;text-decoration: none;" href=<?php echo $payment_link_atm; ?>>Thanh toán</a>
                                                </span>
                                                                    </td>
                                                                </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <!-- /button -->
                                                    </tbody>
                                                </table>
                                                <!-- end of right column -->
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <!-- Spacing -->
                            <tr>
                                <td height="20"></td>
                            </tr>
                            <!-- Spacing -->
                            </tbody>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>
            <!-- end of left image -->
        </tr>

        <tr class="block">
            <!-- Full + text -->
            <table width="100%" bgcolor="#f6f4f5" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="fullimage">
                <tbody>
                <tr>
                    <td>
                        <table bgcolor="#ffffff" width="640" align="center" cellspacing="0" cellpadding="0" border="0" class="devicewidth" modulebg="edit">
                            <tbody>
                            <tr>
                                <td width="100%" height="20"></td>
                            </tr>
                            <tr>
                                <td>
                                    <table width="600" align="center" cellspacing="0" cellpadding="0" border="0" class="devicewidthinner">
                                        <tbody>
                                        <!-- title -->
                                        <tr>
                                            <td style="font-family: Helvetica, arial, sans-serif; font-size: 18px; color: #333333; text-align:left;line-height: 26px;" st-title="rightimage-title">
                                                <hr>
                                            </td>
                                        </tr>
                                        <!-- end of title -->
                                        <!-- Spacing -->
                                        <tr>
                                            <td width="100%" height="10"></td>
                                        </tr>
                                        <!-- Spacing -->
                                        <!-- content -->
                                        <tr>
                                            <td style="font-family: Helvetica, arial, sans-serif; font-size: 15px; color: #272727; text-align:left;line-height: 26px;" st-content="rightimage-paragraph">
                                                <p>
                                                    <span style="font-style: italic; font-weight: bold">Trong trường hợp chuyển khoản</span>, sau khi chúng tôi kiểm tra thanh toán thành công, khóa học sẽ được kích hoạt và thêm vào tài khoản học <b><?php echo $email; ?> </b>. Bạn chỉ cần đăng nhập Kyna.vn với tài khoản email trên và bắt đầu học.
                                                </p>
                                                <p style="margin-top: 20px !important">
                                                    <span style="font-style: italic; font-weight: bold">Trong trường hợp thanh toán online qua cổng Onepay</span>, sau khi thanh toán thành công, khóa học sẽ được kích hoạt và thêm vào tài khoản học <b><?php echo $email; ?> </b>. Bạn chỉ cần đăng nhập Kyna.vn với tài khoản email trên và bắt đầu học.
                                                </p>
                                                <p style="margin-top: 20px !important">
                                                    <span style="font-style: italic; font-weight: bold">Đối với các học viên đăng ký sản phẩm, ứng dụng học tập của Monkey Junior, Taamkru, TouchnMath và Alokiddy</span>, bạn sẽ nhận mã kích hoạt chương trình qua email và thực hiện các bước theo email hướng dẫn để tham gia học.
                                                </p>
                                                <p style="margin-top: 20px !important">
                                                    <span style="font-style: italic; font-weight: bold">Trong trường hợp bạn muốn chuyển sang thanh toán tiền mặt và nhận mã kích hoạt trực tiếp qua giao hàng</span>, vui lòng gọi hotline <b>1900 6364 09</b> để nhận được hỗ trợ.
                                                </p>
                                            </td>
                                        </tr>
                                        <!-- end of content -->
                                        <!-- Spacing -->
                                        <tr>
                                            <td width="100%" height="30"></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>
        </tr>


        <tr class="block">
            <!-- Start of preheader -->
            <table width="100%" bgcolor="#f6f4f5" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="postfooter">
                <tbody>
                <tr>
                    <td width="100%">
                        <table width="640" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
                            <tbody>
                            <!-- Spacing -->
                            <tr>
                                <td width="100%" height="5"></td>
                            </tr>
                            <!-- Spacing -->
                            <tr>
                                <td align="center" valign="middle" style="font-family: Helvetica, arial, sans-serif; font-size: 15px;color: #999999; line-height: 32px" st-content="preheader">
                                    <p><a class="hlite" href="https://kyna.vn/p/kyna/cau-hoi-thuong-gap" style="text-decoration: none; color: #50ad4e">Hướng dẫn sử dụng Kyna.vn</a></p>
                                </td>
                            </tr>
                            <!-- Spacing -->
                            <tr>
                                <td width="100%" height="5"></td>
                            </tr>
                            <!-- Spacing -->
                            <tr>
                                <td align="center" valign="middle" style="font-family: Helvetica, arial, sans-serif; font-size: 14px;color: #999999; line-height: 32px" st-content="preheader">
                                    <p>© 2014 - Bản quyền của Công Ty Cổ Phần Dream Viet Education</p>
                                    <p>VP TPHCM: 178/8, Đường D1, Phường 25, Quận Bình Thạnh, TP Hồ Chí Minh</p>
                                    <p>VP Hà Nội: 25 Vũ Ngọc Phan, Phường Láng Hạ, Quận Đống Đa, TP Hà Nội</p>
                                </td>
                            </tr>
                            <!-- Spacing -->
                            <tr>
                                <td width="100%" height="5"></td>
                            </tr>
                            <!-- Spacing -->
                            <tr>
                                <td align="center" valign="middle" style="font-family: Helvetica, arial, sans-serif; font-size: 10px;color: #999999" st-content="preheader">
                                    <p>If you don't want to receive updates. please <a class="hlite" href="#" style="text-decoration: none; color: #50ad4e">unsubscribe</a></p>
                                </td>
                            </tr>
                            <!-- Spacing -->
                            <tr>
                                <td width="100%" height="5"></td>
                            </tr>
                            <!-- Spacing -->
                            </tbody>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>
            <!-- End of preheader -->
        </tr>
    </tbody>
</table>