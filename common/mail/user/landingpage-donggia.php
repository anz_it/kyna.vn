<style>
    table {
        border-collapse: collapse;
    }

    table, th, td {
        border: 1px solid black;
    }

    table ul{
        list-style-type: none;
        margin: 0;
        padding: 0;
    }

</style>
<strong> <?php echo $full_name;?></strong> thân mến,<br>
<br>
<strong style="color: #50ad4e ">Kyna</strong> <span style="color: #000">cảm ơn bạn đã tin chọn các khóa học trong chương trình<strong> <?php echo $page_title;?></strong>, chỉ còn hôm nay nữa thôi chương trình sẽ khép lại. Bên dưới là những khóa học bạn đã chọn, bạn sẽ <strong>THANH TOÁN </strong> ngay để được giảm giá chứ!</span>
<br>
<br>
<table style="width:100%;border-width: 1px ;border-color:#000000;">
    <tbody>
    <tr style="background: #50ad4e;border: 1px solid black;">
        <td style="padding: 15px 10px 15px 10px; color: white; text-align: left"><strong>STT</strong></td>
        <td style="padding: 15px 10px 15px 10px; color: white;"><strong>Tên Khoá Học</strong></td>
        <td style="padding: 15px 10px 15px 10px; color: white;"><strong>Học Phí</strong></td>
    </tr>

    <?php
        $sum_price = 0;
        $formatter = \Yii::$app->formatter;
        $sum_discount = 0;
    ?>
    <?php
        $i =1;
    ?>
    <?php foreach($courses as $course):?>
        <?php
            $sum_price += $course->price;
            $sum_discount += $course->getDiscountAmount();
        ?>
        <tr style="background:#fff;">
            <td style="border: solid 1px; padding: 10px 5px; text-align: left;">
                <ul style="margin: 5px 0px 5px 0; list-style-type: none">
                    <li style="color: #272727;"><?php echo $i;?></li>
                </ul>
            </td>
            <td style="border: solid 1px; padding: 10px 10px;"><?php echo $course->name;?></td>
            <td style="border: solid 1px; padding: 10px 5px; text-align: left ;width: 90px;"><?php echo $formatter->format($course->price,array('decimal',0));?> đ</td>
        </tr>
        <?php $i++;?>
    <?php endforeach;?>

    <tr style="background: #fff;">
        <td colspan="2" style="border: solid 1px; padding: 10px 10px;color: #272727;">Học phí gốc</td>
        <td style="border: solid 1px; padding: 10px 5px;color: #272727; text-align: left ;width: 90px;"
            >
            <?php echo $formatter->format($sum_price,array('decimal',0));?> đ
        </td>
    </tr>


    <tr style="
            background: #fff;
            ">
        <td colspan="2" style="border: solid 1px; padding: 10px 10px;color: #272727;">Giảm giá</td>
        <td style="border: solid 1px; padding: 10px 5px;color: #272727; text-align: left ;width: 90px;"
            >
            <?php echo $formatter->format($sum_discount,array('decimal',0));?> đ
        </td>
    </tr>

    <tr style="
        background: #fff;
        ">
        <td colspan="2" style="border: solid 1px; padding: 10px 10px; font-weight: bold"> <strong style="color: #ff7818;">THÀNH TIỀN </strong></td>
        <td  style="border: solid 1px; padding: 10px 5px; text-align: left ;width: 90px; font-weight: bold"
            >
            <strong style="color: #ff7818;"><?php
            $total = $sum_price- $sum_discount;
            echo $formatter->format($total,array('decimal',0));?> đ</strong>
        </td>
    </tr>
     
    </tbody>
</table>
<table>
    <tr style="text-align: center;">
        <?php $products = serialize($products);?>
        <td> <a style="text-decoration: none !important; border: solid 1px; padding: 10px 30px;color: #fff; background-color: #ff7818; text-align: center"
                href="<?php echo \yii\helpers\Url::toRoute(['/cart/default/add-product-to-cart','products'=>$products], true);?> "><strong> THANH TOÁN NGAY </strong></a></td>
    </tr>
</table>
<br>
<span style="color: #000">Còn nếu bạn muốn thay đổi khóa học của mình vào phút chót, hãy chọn <a href="<?php echo $link_page.'/54961'?>">Xem thêm</a> và đổi lại khóa học bạn muốn nhé. Đừng quên thanh toán trước <strong>23:59 ngày hôm nay (<?php echo date('d/m/Y')?>)</strong> để được giá tốt nhất!</span>
<br><br>
 Thân mến,<br> <span style="color: #50ad4e;"><strong > Kyna </strong></span>
