<?php

use yii\helpers\Url;

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <!--[if !mso]><!-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <!--<![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title></title>

    <style type="text/css">
        /* Basics */
        body {
            Margin: 0;
            padding: 0;
            min-width: 100%;
            background-color: #ffffff;
            line-height: 1.34;
        }
        table {
            border-spacing: 0;
            font-family: 'Helvetica', Tahoma, Arial, sans-serif;
            color: #555555;
        }
        td {
            padding: 0;
        }
        img {
            border: 0;
        }
        .wrapper {
            width: 100%;
            table-layout: fixed;
            -webkit-text-size-adjust: 100%;
            -ms-text-size-adjust: 100%;
        }
        .webkit {
            max-width: 600px;
        }
        .outer {
            Margin: 0 auto;
            width: 100%;
            max-width: 600px;
        }
        .inner {
            padding: 10px;
            text-align: center;
        }
        .three-column .inner {
            padding: 10px;
            width: 180px;
            text-align: center;
        }
        .p-center {
            text-align: center
        }
        .footer  {
            font-size: 13px !important
        }
        .p-left {
            text-align: left
        }
        .p-right {
            text-align: right
        }
        .contents {
            width: 100%;
        }
        p {
            Margin: 0;
            line-height: 1.34;
        }
        a {
            color: #ee6a56;
            text-decoration: underline;
        }
        .h1 {
            font-size: 20px !important;
            font-weight: bold;
            Margin-bottom: 18px;
            color: #3fb34f;
        }
        .h2 {
            color: #3fb34f;
            font-weight: bold;
            Margin-bottom: 6px;
        }
        .linkstyle {
            color: #3fb34f;
            font-weight: bold;
            text-decoration: none
        }
        .full-width-image img {
            width: 100%;
            max-width: 600px;
            height: auto;
        }

        /* One column layout */
        .one-column .contents {
            text-align: left;
        }
        .one-column p {
            font-size: 14px;
            Margin-bottom: 10px;
        }

        /*Two column layout*/
        .two-column {
            text-align: center;
            font-size: 0;
        }
        .two-column .column {
            width: 100%;
            max-width: 300px;
            display: inline-block;
            vertical-align: top;
        }
        .two-column .contents {
            font-size: 14px;
            text-align: left;
        }
        .two-column img {
            max-width: 280px;
            height: auto;
        }
        .two-column .text {
            padding-top: 10px;
        }

        /*Three column layout*/
        .three-column {
            text-align: center;
            font-size: 0;
            padding-top: 10px;
            padding-bottom: 10px;
        }
        .three-column .column {
            width: 100%;
            max-width: 200px;
            display: inline-block;
            vertical-align: top;
            text-align: center
        }
        .three-column img {
            width: 100%;
            max-width: 180px;
            height: auto;
        }
        .three-column .contents {
            font-size: 14px;
            text-align: center;
        }
        .three-column .text {
            padding-top: 10px;
        }

        /* Left sidebar layout */
        .left-sidebar {
            text-align: center;
            font-size: 0;
        }
        .left-sidebar .column {
            width: 100%;
            display: inline-block;
            vertical-align: middle;
        }
        .left-sidebar .left {
            max-width: 500px;
        }
        .left-sidebar .right {
            max-width: 100px;
        }
        .left-sidebar .img {
            width: 100%;
            max-width: 80px;
            height: auto;
        }
        .left-sidebar .contents {
            font-size: 14px;
            text-align: center;
        }
        .left-sidebar a {
            color: #85ab70;
        }

        /* Right sidebar layout */
        .right-sidebar {
            text-align: center;
            font-size: 0;
        }
        .right-sidebar .column {
            width: 100%;
            display: inline-block;
            vertical-align: middle;
        }
        .right-sidebar .left {
            max-width: 100px;
        }
        .right-sidebar .right {
            max-width: 500px;
        }
        .right-sidebar .img {
            width: 100%;
            max-width: 80px;
            height: auto;
        }
        .right-sidebar .contents {
            font-size: 14px;
            text-align: center;
        }
        .right-sidebar a {
            color: #70bbd9;
        }

        /*Media Queries*/
        @media screen and (max-width: 400px) {
            .two-column .column,
            .three-column .column, .left-sidebar .column {
                max-width: 100% !important;
                display: table;
            }
            .two-column img {
                max-width: 100% !important;
            }
            .three-column img {
                max-width: 50% !important;
            }
            .two-column .contents , p{
                text-align: center
            }
            .three-column .inner {
                padding: 3px;
                width: 100%;
                text-align: center;
            }
            .inner {
                padding: 3px
            }
        }

        @media screen and (min-width: 401px) and (max-width: 650px) {
            .three-column .column {
                max-width: 33% !important;
            }
            .two-column .column {
                max-width: 50% !important;
            }
        }

    </style>

    <!--[if (gte mso 9)|(IE)]>
    <style type="text/css">
        table {border-collapse: collapse;}
    </style>
    <![endif]-->
</head>
<body>
<center class="wrapper">
    <div class="webkit">
        <!--[if (gte mso 9)|(IE)]>
        <table width="600" align="center">
            <tr>
                <td>
        <![endif]-->
        <table class="outer" align="center">
            <!-- CONTENT START -->
            <!-- TWO COLUMN -->
            <tr>
                <td class="two-column" style="border-bottom: 1px solid #eee">
                    <!--[if (gte mso 9)|(IE)]>
                    <table width="100%">
                        <tr>
                            <td width="50%" valign="top">
                    <![endif]-->
                    <div class="column">
                        <table width="100%">
                            <tr>
                                <td align="left" class="inner">
                                    <table class="contents">
                                        <tr>
                                            <td>
                                                <a href="https://kyna.vn" target="_blank"><img style="height: 38px !important" src="https://media-kyna.cdn.vccloud.vn/img/logo.png" /></a>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <!--[if (gte mso 9)|(IE)]>
                    </td><td width="50%" valign="top">
                    <![endif]-->
                    <div class="column">
                        <table width="100%">
                            <tr>
                                <td class="inner">
                                    <table class="contents">
                                        <tr>
                                            <td align="right">
                                                <p><strong>Hotline  1900 6364 09</strong></p>
                                                <p>Email <a class="linkstyle" href="mailto:hotro@kyna.vn">hotro@kyna.vn</a></p>
                                            </td>
                                        </tr>

                                    </table>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <!--[if (gte mso 9)|(IE)]>
                    </td>
                    </tr>
                    </table>
                    <![endif]-->
                </td>
            </tr>
            <!-- /TWO COLUMN -->


            <!-- ONE COLUMN -->
            <tr>
                <td class="one-column">
                    <table width="100%">
                        <tr>
                            <td align="center" class="inner contents">
                                <p class="p-center"><img style="height: 180px !important" src="https://i.imgur.com/oPNWduG.gif"/></p>
                                <h1 class="h1 p-center">Mừng Bạn Gia Nhập Gia Đình <a href="https://kyna.vn" class="linkstyle">Kyna.vn</a></h1>
                                <p class="p-left">Xin chào <strong><?= $fullName ?></strong>,</p>
                                <p class="p-left">Cám ơn bạn đã đăng ký khóa học Kỷ Luật Không Nước Mắt. Bạn vừa được tự động tạo tài khoản học trên Kyna.vn với mật khẩu ngẫu nhiên. Để bắt đầu học, bạn vui lòng đặt mật khẩu mới đăng nhập trên Kyna.vn.
                                </p>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <!-- /ONE COLUMN -->
            <!-- TWO COLUMN -->
            <tr>
                <td class="two-column">
                    <!--[if (gte mso 9)|(IE)]>
                    <table width="100%">
                        <tr>
                            <td width="50%" valign="top">
                    <![endif]-->
                    <div class="column">
                        <table width="100%">
                            <tr>
                                <td align="center" class="inner">
                                    <table class="contents">
                                        <tr>
                                            <td align="center">
                                                <p class="h2">Email tài khoản: </p>
                                                <p><?= $email ?></p>
                                                <p style="margin: 24px 5px"><a style="text-decoration: none; background: #3fb34f; color: #fff;  font-weight: bold; padding: 10px; margin: 5px; border-radius: 3px"
                                                                               href="<?= $link ?>">Bắt đầu học</a></p>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <!--[if (gte mso 9)|(IE)]>
                    </td><td width="50%" valign="top">
                    <![endif]-->
                    <div class="column">
                        <table width="100%">
                            <tr>
                                <td class="inner">
                                    <table class="contents">
                                        <tr>
                                            <td align="center">
                                                <p>Để đặt lại mật khẩu, vui lòng click <a class="linkstyle" href="<?= $link ?>">Đặt mật khẩu mới</a> hoặc ﻿nút bên dưới</p>
                                                <p style="margin: 24px 5px"><a style="text-decoration: none; background: #3fb34f; color: #fff;  font-weight: bold; padding: 10px; margin: 5px; border-radius: 3px" href="<?= $link ?>">Đặt mật khẩu mới</a></p>
                                            </td>
                                        </tr>

                                    </table>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <!--[if (gte mso 9)|(IE)]>
                    </td>
                    </tr>
                    </table>
                    <![endif]-->
                </td>
            </tr>
            <!-- /TWO COLUMN -->


            <!-- TWO COLUMN -->
            <tr>
                <td class="two-column" style="border-bottom: 1px solid #eee">
                    <!--[if (gte mso 9)|(IE)]>
                    <table width="100%">
                        <tr>
                            <td width="50%" valign="top">
                    <![endif]-->
                    <div class="column">
                        <table width="100%">
                            <tr>
                                <td align="center" class="inner">
                                    <table class="contents">
                                        <tr>
                                            <td align="center">
                                                <p><a href="https://kyna.vn/p/kyna/cau-hoi-thuong-gap"><img src="https://i.imgur.com/uaWKbze.png"/></a></p>
                                                <p class="h1"><a href="https://kyna.vn/p/kyna/cau-hoi-thuong-gap" class="linkstyle"> Xem thêm hướng dẫn khác</a></p>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <!--[if (gte mso 9)|(IE)]>
                    </td><td width="50%" valign="top">
                    <![endif]-->
                    <div class="column">
                        <table width="100%">
                            <tr>
                                <td class="inner">
                                    <table class="contents">
                                        <tr>
                                            <td align="center">
                                                <p><a href="https://kyna.vn/bai-viet/"> <img src="https://i.imgur.com/HE6S5rt.png"/><a/></p>
                                                <p class="h1"><a href="https://kyna.vn/bai-viet/" class="linkstyle"> Xem các bài viết hữu ích</a></p>
                                            </td>
                                        </tr>

                                    </table>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <!--[if (gte mso 9)|(IE)]>
                    </td>
                    </tr>
                    </table>
                    <![endif]-->
                </td>
            </tr>
            <!-- /TWO COLUMN -->









            <!-- ONE COLUMN -->
            <tr>
                <td class="one-column">
                    <table width="100%">
                        <tr>
                            <td class="inner contents">
                                <p>Nếu bạn gặp khó khăn trong quá trình sử dụng, hãy liên hệ với chúng tôi để được hỗ trợ:</p>
                                <p><strong>Điện thoại:</strong> 1900 6364 09 (08h30 - 22h00, 7 ngày trong tuần)</p>
                                <p><strong>Email:</strong> <a class="linkstyle" href="mailto:hotro@kyna.vn">hotro@kyna.vn</a></p>
                                <br/>
                                <p>Trân trọng</p>
                                <p>Kyna.vn</p>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <!-- /ONE COLUMN -->


            <!-- ONE COLUMN -->
            <tr>
                <td class="one-column">
                    <table width="100%">
                        <tr>
                            <td class="inner contents" style="font-size: 13px !important;border-top: 1px dotted #e5ebef">
                                <p class="footer p-center">Kyna.vn | Học online cùng chuyên gia</p>
                                <p class="footer p-center">VP Hồ Chí Minh: Lầu 6 Tòa nhà Thịnh Phát - 178/8 Đường D1, Phường 25, Quận Bình Thạnh, TP Hồ Chí Minh</p>
                                <p class="footer p-center">VP Hà Nội: Tầng 6, Tòa Nhà Viện Công Nghệ - 25 Vũ Ngọc Phan, Phường Láng Hạ, Quận Đống Đa, TP Hà Nội</p>
                                <p class="p-center">
                                    <a href="https://www.facebook.com/kyna.vn" title="Facebook" target="_blank">
                                        <img style="height: 30px" src="https://i.imgur.com/XhTjAca.png"/></a>
                                    <a href="https://www.youtube.com/user/kynavn" title="Facebook" target="_blank">
                                        <img  style="height: 30px" src="https://i.imgur.com/WQaczqb.png"/></a>
                                </p>
                        </tr>
                    </table>
                </td>
            </tr>
            <!-- /ONE COLUMN -->


            <!-- /CONTENT -->
        </table>
        <!--[if (gte mso 9)|(IE)]>
        </td>
        </tr>
        </table>
        <![endif]-->
    </div>
</center>
</body>
</html>
