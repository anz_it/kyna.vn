<?php

$quizLink = str_replace('dashboard.', '', $model->quiz->course->getLearningUrl($model->quiz->lesson->section_id, $model->quiz->lesson->id, true));
?>
<p><?= $model->user->profile->name ?> thân mến,</p>

<p>Kết quả làm bài <b><?= $model->quiz->name ?></b> của bạn dành cho khóa học "<?= $model->quiz->course->name ?>" đã được chấm với kết quả như sau:</p>

<ul>
    <li style="list-style-type: none">
        <p>Điểm: <?= $model->total_score . ' / ' . $model->total_quiz_score ?> (<?=  $model->total_quiz_score ? (number_format($model->total_score * 100 / $model->total_quiz_score, 2)) : 100 ?> %)</p>
        <p>Kết quả:
            <b>
                <?= $model->is_passed ? 'Đạt yêu cầu.' : 'Không đạt yêu cầu' ?>
            </b>
        </p>
    </li>
</ul>

<p>Bạn cũng có thể xem kết quả tại Kyna.vn tại link sau đây: <a href="<?= $quizLink ?>"><?= $quizLink ?></a> </p>

<p>Chúc bạn một ngày nhiều năng lượng!</p>

<p>Thân mến,<br />
    Ban quản trị Kyna.vn</p>
