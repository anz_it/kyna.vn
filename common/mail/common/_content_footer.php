<?php

use yii\helpers\Url;
use common\helpers\CDNHelper;
$mediaLink = CDNHelper::getMediaLink();
$hotline = !empty($settings['hot_line']) ? $settings['hot_line'] : '1900.6364.09';
$supportMail = !empty($settings['email_footer']) ? $settings['email_footer'] : 'hotro@kyna.vn';
?>
<p style="color: #272727">
    <span style="font-size: 16px;">N</span>ếu bạn cần hỗ trợ hoặc tư vấn th&ecirc;m th&ocirc;ng tin, h&atilde;y li&ecirc;n hệ với <span class="color-green bold" style="color: #50ad4e;font-weight: bold">Kyna.vn</span> qua email<br />
    <span class="color-green bold"><a href="mailto:<?= $supportMail ?>" target="_blank" style="color: #50ad4e;font-weight: bold; text-decoration: none;"><?= $supportMail ?></a></span> hoặc số điện thoại <span class="color-green bold" style="font-weight: bold;color: #50ad4e"><?= $hotline ?></span>.

</p>
<span class="line"> </span>

<hr /><span class="line"> </span>

<ul class="bottom" style="padding: none; list-style: none;padding: 0px;list-style: none;text-align: center;line-height: 25px;color: #272727;">
    <li><span class="color-green bold" style="font-weight: bold;color: #50ad4e">Kyna.vn</span> | Học online c&ugrave;ng chuy&ecirc;n gia</li>
    <li>VP Hồ Ch&iacute; Minh: Lầu 6 T&ograve;a nh&agrave; Thịnh Ph&aacute;t - 178/8 Đường D1, Phường 25, Quận B&igrave;nh Thạnh, TP Hồ Ch&iacute; Minh<br />
        VP H&agrave; Nội: Tầng 6, T&ograve;a Nh&agrave; Viện C&ocirc;ng Nghệ - 25 Vũ Ngọc Phan, Phường L&aacute;ng Hạ, Quận Đống Đa, TP H&agrave; Nội</li>
</ul>

<div class="social">
    <ul style="padding: 0px;text-align: center">
        <li style="display: inline-block;"><a href="https://www.facebook.com/kyna.vn" target="_blank" class="facebook"><img src="<?= $mediaLink ?>/img/mail/icon-f.png"/></a></li>
        <li style="display: inline-block;"><a href="https://www.youtube.com/user/kynavn" target="_blank" class="youtube"><img src="<?= $mediaLink ?>/img/mail/icon-y.png"/></a></li>
    </ul>
</div>
