<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 12/25/2017
 * Time: 3:08 PM
 */

$codeAmounts = $order->getRefundCodes($order->refund);
?>
&nbsp;
<table align="center" border="0" cellpadding="1" cellspacing="1" style="width:100%;">
    <tbody>
        <tr>
            <td>
                <span style="font-family:arial,helvetica,sans-serif;">
                    <span style="font-size:14px;">
                        <span style="color:#50ad4e;">&#9658;</span>
                        Chính sách hoàn tiền dư khi thanh toán bằng <span style="color:#50ad4e;"><strong>Thẻ cào điện thoại</strong></span><span style="color:#000000;">:</span><br />
                        <span style="color:#50ad4e;"><strong>Kyna.vn</strong></span> sẽ hoàn lại bạn số tiền dư bằng cách quy đổi thành <a href="https://kyna.vn/p/kyna/cau-hoi-thuong-gap#huong-dan-su-dung-voicher-coupon-12" style="text-decoration:none;"><strong>Coupon</strong></a> tương ứng
                    </span>
                </span>

                <ul>
                    <li><span style="font-family:arial,helvetica,sans-serif;"><span style="font-size:14px;">Số tiền dư &lt;50.000đ &rarr; Bạn nhận được coupon 50.000đ</span></span></li>
                    <li><span style="font-family:arial,helvetica,sans-serif;"><span style="font-size:14px;">Số tiền dư: 50.000đ - 100.000đ &rarr; Bạn nhận được coupon 100.000đ</span></span></li>
                    <li style="text-align: justify;">
                        <span style="font-family:arial,helvetica,sans-serif;">
                            <span style="font-size:14px;">Số tiền dư &gt;100.000đ &rarr; Bạn nhận được nhiều coupon 100.000đ (Phần lẻ c&ograve;n lại &aacute;p dụng quy đổi như tr&ecirc;n)</span>
                        </span>
                        <br />
                        <span style="color: rgb(80, 173, 78); background-color: rgb(255, 255, 255);">&#9658; <span style="font-size:14px;"><strong>Coupon quy đổi của bạn l&agrave;:</strong></span></span><br />
                        <span style="font-family:arial,helvetica,sans-serif;"><span style="font-size:14px;"><span style="font-size: 14px;">&bull;&nbsp; &nbsp; Số tiền dư: <?= number_format($order->refund, 0, ',', '.')?>đ&nbsp;&rarr;&nbsp;</span><span style="font-size: 14px;">Bạn nhận <?php if (!empty($codeAmounts[100000])) : ?><?= $codeAmounts[100000] ?> coupon 100.000đ<?php endif; ?><?php if (!empty($codeAmounts[50000]) && !empty($codeAmounts[100000])) { echo ' + '; } ?> <?php if (!empty($codeAmounts[50000])) : ?><?= $codeAmounts[50000] ?> coupon 50.000đ<?php endif; ?></span></span></span><br />
                        <span style="color: rgb(80, 173, 78); font-size: 14px; text-align: justify; background-color: rgb(255, 255, 255);">&#9658; </span><span style="text-decoration: none;"><span style="font-size: 14px; text-align: justify; background-color: rgb(255, 255, 255);"><strong><a href="http://kyna.vn/trang-ca-nhan/voucher-coupon" style="text-decoration:none;"><span style="color:#000000;">Xem coupon đã quy đổi của bạn&nbsp;tại đây!</span></a></strong></span></span>
                    </li>
                </ul>
            </td>
        </tr>
    </tbody>
</table>
&nbsp;