<?php

use yii\helpers\Url;
?>
<table>
    <tr>
        <td style="padding: 0px 20px;">
            <h2 class="title-main" style="color: #50ad4e;font-size: 26px; text-align: center;font-weight: bold;padding: 20px 0px;margin-bottom: 0px; line-height: 40px;">Thanh toán khóa học thành công</h2>
            <span style="display: block;"><img src="http://sendy.kyna.vn/uploads/1474945506.png" style="margin: 0px auto;display: inherit;"/></span>
            <div class="text">
                <ul class="top" style="list-style: none; padding: 0px;">
                    <li style="margin-bottom: 20px; color: #272727">
                        <span class="bold" style="font-weight: bold"><?= $order->user->profile->name ?></span> thân mến,
                    </li>
                    <li style="margin-bottom: 20px; color: #272727">
                        <span style="font-size: 16px;">C</span>ảm ơn bạn đã đăng ký khóa học trên <span class="color-green bold" style="font-weight: bold; color: #50ad4e">Kyna.vn</span>. Đơn đăng ký của bạn đã thanh toán thành công. Hệ thống đã kích hoạt khóa học cho bạn. Dưới đây là thông tin về các khóa học đã đăng ký:
                    </li>
                </ul>

                <?= $this->render('./register_success/order_summary', ['order' => $order]) ?>

                <?php if (!empty($code)): ?>
                    <table align="center" border="0" cellpadding="1" cellspacing="1" style="width:100%">
                        <tbody>
                        <tr>
                            <td>
                                <p style="font-size: 16px;text-align: left;margin-top: 25px;"><span style="color:#50ad4e"><strong>Kyna.vn</strong></span> gửi tặng bạn học bổng trị giá <strong>500.000đ</strong> áp dụng cho tiếng Anh và tiếng Trung Lingo trên biểu phí đang niêm yết.
                                    Để xem mã <strong>mã Coupon</strong>: <span style="color:#50ad4e"><strong><?= $code->code ?></strong></span>,
                                    bạn vào xem menu <a href="https://kyna.vn/trang-ca-nhan/voucher-coupon" style="text-decoration:none;color: #2BA6CB;" target="_blank"><strong>Voucher/Coupon</strong></a> trong trang Khóa học của tôi.
                                </p>
                                <p style="text-align: left;margin-top: 25px;margin-bottom: 10px">
                                    <span style="color:rgb(80,173,78);font-size:14px;text-align:justify;background-color:rgb(255,255,255)">► </span><span style="text-decoration:none"><span style="font-size:14px;text-align:justify;background-color:rgb(255,255,255)"><strong><a href="https://kyna.vn/trang-ca-nhan/voucher-coupon" style="text-decoration:none" target="_blank"><span style="color:#000000">Xem coupon đã qui đổi của bạn&nbsp;tại đây!</span></a></strong></span></span></li>
                                </p>

                                <p style="text-align: left;margin-top: 25px;margin-bottom: 25px">
                                    <span style="color:rgb(80,173,78);font-size:14px;text-align:justify;background-color:rgb(255,255,255)">► </span><span style="text-decoration:none"><span style="font-size:14px;text-align:justify;background-color:rgb(255,255,255)"><strong><a href="https://kyna.vn/p/kyna/cau-hoi-thuong-gap#huong-dan-su-dung-voicher-coupon-12" style="text-decoration:none" target="_blank"><span style="color:#000000">Xem hướng dẫn sử dụng</span></a></strong></span></span></li>
                                </p>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                <?php endif; ?>

                <?php
                if (in_array($order->payment_method, ['ipay', 'epay']) && $order->refund > 0) {
                    echo $this->render('./_refund', ['order' => $order]);
                }
                ?>

                <?= $this->render('./register_success/order_detail', ['order' => $order]) ?>

                <table cellpadding="0" cellspacing="0">
                    <tr>
                        <td class="pattern" width="600" align="center">
                            <table cellpadding="0" cellspacing="0">
                                <tr>
                                    <td style="padding: 20px 0px 20px 0px; font-size: 14px">
                                        <table cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td class="col col1" width="150" align="center" style="margin-bottom: 5px;">
                                                    <table cellpadding="0" cellspacing="0">
                                                        <tr>
                                                            <td style="padding: 0px 10px 0px 0px;">
                                                                <table cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td>
                                                                            <img src="<?= Url::base(true) ?>/img/mail/RegistrationCourse1.png" style="display: block;">
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td class="col" width="450" align="center">
                                                    <table cellpadding="0" cellspacing="0">
                                                        <tr>
                                                            <td style="padding:0px 10px 0px 0px;">
                                                                <p style="color: #272727; text-align: left;">
                                                                    <span style="font-weight: bold; color: #50ad4e; display: block;">Bước 1:</span> Bạn đăng nhập <span style="font-weight: bold; color: #50ad4e">Kyna.vn</span> với tài khoản đã đăng ký. Để bắt đầu học, bạn vào mục <span style="font-weight: bold;">Khóa học của tôi</span> và click chọn nút <span>Bắt đầu học</span> ở khóa học muốn tham gia.
                                                                </p>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>

                            </table>
                        </td>
                    </tr>
                </table>

                <?= $this->render('../common/_content_footer', ['settings' => $settings]) ?>
            </div>
            <!--end .center -->
        </td>
    </tr>
</table>
