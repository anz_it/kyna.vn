<?php

use yii\helpers\Url;
use common\helpers\CDNHelper;

$mediaLink = CDNHelper::getMediaLink();

?>

<table>
    <tr>
        <td style="padding: 0px 20px;">
            <h2 class="title-main" style="color: #50ad4e;font-size: 26px; text-align: center;font-weight: bold;padding: 20px 0px;margin-bottom: 0px; line-height: 40px;">Hướng dẫn thanh toán đơn hàng</h2>
            <span style="display: block;"><img src="<?= $mediaLink ?>/img/mail/email_xac_nhan_dang_ki.png" style="margin: 0px auto;display: inherit;"/></span>
            <div class="text">
                <p style="color: #272727; text-align: left"><span class="bold" style="font-weight: bold"><?= $order->user->profile->name ?></span> thân mến,</p>
                <p style="color: #272727; text-align: left">Dưới đây là hướng dẫn thanh toán cho đơn hàng <span class="color-green bold" style="font-weight: bold; color: #50ad4e">#<?= $order->id ?></span>: </p>

                <table cellpadding="0" cellspacing="0" style="margin: 15px 0px;">
                    <tr>
                        <td class="pattern" width="600" align="center">
                            <table cellpadding="0" cellspacing="0">
                                <tr>
                                    <td style="padding: 20px 0px 0px 0px; font-size: 14px">
                                        <table cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td class="col col1" width="150" align="center" style="margin-bottom: 5px;">
                                                    <table cellpadding="0" cellspacing="0">
                                                        <tr>
                                                            <td style="padding: 0px 10px 0px 0px;">
                                                                <table cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td>
                                                                            <img src="<?= $mediaLink ?>/img/mail/RegistrationCourse1.png" style="display: block;">
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td class="col" width="450" align="left">
                                                    <table cellpadding="0" cellspacing="0">
                                                        <tr>
                                                            <td style="padding:0px 10px 0px 0px;">
                                                                <p style="color: #272727; text-align: left">
                                                                    <span style="font-weight: bold; color: #50ad4e; display: block;">Bước 1:</span>
                                                                    Click vào nút <span style="font-weight: bold; color: #50ad4e;">Thanh toán lại</span> dưới đây. Sau khi thanh toán thành công, khóa học của bạn sẽ được kích hoạt. <br>
                                                                    <a class="btn btn-success" href="<?= str_replace('dashboard.', '', Url::to(['/cart/checkout/index', 'orderId' => $order->id], true))?>" style="font-weight: bold">Thanh toán lại</a>
                                                                </p>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>

                                <tr>
                                    <td style="padding: 20px 0px 20px 0px; font-size: 14px">
                                        <table cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td class="col col1" width="150" align="center" style="margin-bottom: 5px;">
                                                    <table cellpadding="0" cellspacing="0">
                                                        <tr>
                                                            <td style="padding: 0px 10px 0px 0px;">
                                                                <table cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td>
                                                                            <img src="<?= $mediaLink ?>/img/mail/RegistrationCourse2.png" style="display: block;">
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td class="col" width="450" align="left">
                                                    <table cellpadding="0" cellspacing="0">
                                                        <tr>
                                                            <td style="padding:0px 10px 0px 0px;">
                                                                <p style="color: #272727; text-align: left">
                                                                    <span style="font-weight: bold; color: #50ad4e; display: block;">Bước2:</span>
                                                                    Để bắt đầu học, bạn vào mục <span style="font-weight: bold; color: #50ad4e;">Khóa học của tôi</span> và click chọn nút <span style="font-weight: bold;">Bắt đầu học</span> ở khóa học muốn tham gia.
                                                                </p>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>

                            </table>
                        </td>
                    </tr>
                </table>

                <?= $this->render('./register_success/order_summary', ['order' => $order]) ?>

                <?= $this->render('./register_success/order_detail', ['order' => $order]) ?>

                <?= $this->render('../common/_content_footer', ['settings' => $settings]) ?>
            </div>
            <!--end .center -->
        </td>
    </tr>
</table>
