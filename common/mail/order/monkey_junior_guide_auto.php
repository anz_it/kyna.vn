<?php

use yii\helpers\Url;
?>
<html>
    <head>
        <title></title>
    </head>
    <body>
        <table align="center" border="0" cellpadding="0" cellspacing="0" style="font-family:Arial;background-color:#F5F5F5;padding:50px 0;" width="100%">
            <tbody>
                <tr>
                    <td style="padding-top:20px;" valign="top" width="100%">
                        <table align="center" border="0" cellpadding="0" cellspacing="0" style="border: 1px solid #DEDEDE; box-shadow:0 0 7px #DEDEDE;" width="682">
                            <tbody>
                                <tr>
                                    <td align="center" bgcolor="#fff" style="padding-top:25px; padding-bottom:15px;color: #fff">
                                        <div style="text-align:center"><span style="font-size:11px;"><font color="#000000" face="Times New Roman"><img src="https://media-kyna.cdn.vccloud.vn/img/logo.png" style="width: 150px; height: 27px;" /></font></span></div>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding: 5px 26px 50px; text-align: justify; background-color: rgb(255, 255, 255);">
                                        <p><span style="font-size:14px;"><font color="#333333"><span style="line-height: 20.8px;">Ba mẹ th&acirc;n mến,</span></font></span></p>

                                        <p><span style="font-size:14px;"><font color="#333333"><span style="line-height: 20.8px;">Để qu&aacute; tr&igrave;nh dạy b&eacute; học được hiệu quả v&agrave; gi&uacute;p b&eacute; c&oacute; những gi&acirc;y ph&uacute;t vui vẻ khi học tiếng Anh, Kyna.vn xin gửi tặng&nbsp;bạn kh&oacute;a học&nbsp;&quot;<strong>C&ugrave;ng Kyna.vn t&igrave;m hiểu c&aacute;ch sử dụng ứng dụng&nbsp;</strong></span></font><span style="color: rgb(34, 34, 34); font-family: arial, sans-serif; background-color: rgb(255, 255, 255);"><strong>Monkey Junior</strong>&quot;:</span></span></p>

                                        <p style="text-align:center"><span style="font-size:14px;"><a href="<?= str_replace('dashboard.', '', Url::toRoute(['/user/course/index'], true)) ?>"><font color="#000000" face="Times New Roman"><img alt="Để quá trình dạy bé học được hiệu quả và giúp bé có những giây phút vui vẻ khi học tiếng Anh, Kyna.vn xin gửi tặng bạn khóa học " c="" src="http://sendy.kyna.vn/uploads/1479441610.PNG" style="height: 259px; width: 500px;" /></font></a></span></p>

                                        <p><span style="font-size:14px;"><font color="#333333"><span style="line-height: 20.8px;">Kh&oacute;a học&nbsp;đ&atilde; được tự động th&ecirc;m v&agrave;o t&agrave;i khoản của bạn. H&atilde;y&nbsp;đăng&nbsp;nhập v&agrave;o t&agrave;i khoản của m&igrave;nh&nbsp;tr&ecirc;n Kyna.vn, v&agrave;o mục <strong>Kh&oacute;a học của t&ocirc;i, </strong>click chọn<strong> Bắt đầu học&nbsp;</strong>để xem ngay nh&eacute;.</span></font></span></p>

                                        <p style="text-align: left;"><a href="<?= str_replace('dashboard.', '', Url::toRoute(['/user/course/index'], true)) ?>" style="line-height: 1.3em; font-weight: bold; color: white; text-decoration: none; padding: 10px 20px; border-radius: 5px; display: inline-block; margin-top: 7px; margin-bottom: 7px; border-bottom-width: 2px; border-bottom-style: solid; border-bottom-color: rgb(0, 176, 52); background-image: initial; background-attachment: initial; background-color: rgb(0, 176, 52); background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><span style="color:#FFFFFF;">XEM NGAY</span></a></p>

                                        <p style="line-height: 1.3em;"><span style="font-size:14px;"><span style="color:#333333;"><span style="line-height: 1.3em;">Ch&uacute;c b&eacute;&nbsp;v&agrave; gia đ&igrave;nh c&oacute; những ph&uacute;t gi&acirc;y học tập thật hiệu quả!</span></span></span></p>

                                        <p style="line-height: 1.5em;"><span style="font-size:14px;"><span style="color:#333333;">Th&acirc;n mến,<br />
						<strong>Kyna.vn</strong></span></span></p>

                                        <hr style="line-height: 1.3em;" />
                                        <p style="line-height: 1.3em;"><span style="color:#333333;"><strong><span style="font-size: 12px;">Kh&oacute;a học đang được&nbsp;quan t&acirc;m nhất:&nbsp;</span></strong></span></p>

                                        <ul style="line-height: 1.3em;">
                                            <li><a href="https://kyna.vn/p/nhom-khoa-hoc/5-khoa-hoc-nuoi-day-con-danh-cho-cha-me-tre-v2/49840?utm_source=newsletter&amp;utm_campaign=16.11.16&amp;utm_medium=email&amp;utm_content=huong_dan_su_dung_monky_junior" style="text-decoration:none"><span style="color:#000080;"><span style="font-size: 13px;">Bộ kỹ năng nu&ocirc;i con cho cha mẹ trẻ</span></span></a></li>
                                            <li><a href="https://kyna.vn/tam-ly-va-giao-duc-gioi-tinh-cho-con-tuoi-day-thi/49840?utm_source=newsletter&amp;utm_campaign=16.11.16&amp;utm_medium=email&amp;utm_content=huong_dan_su_dung_monky_junior" style="text-decoration:none;"><span style="color:#000080;"><span style="font-size: 13px;">T&acirc;m l&yacute; trẻ tuổi dậy th&igrave;</span></span></a></li>
                                            <li><a href="https://kyna.vn/p/nhom-khoa-hoc/day-tre-thong-minh-som/49840?utm_source=newsletter&amp;utm_campaign=16.11.16&amp;utm_medium=email&amp;utm_content=huong_dan_su_dung_monky_junior" style="text-decoration:none;"><span style="color:#000080;"><span style="font-size: 13px;">Dạy con th&ocirc;ng minh sớm</span></span></a></li>
                                        </ul>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding:0 9px; background-color:#fff">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td align="left" bgcolor="#F5F5F5" style="padding: 20px 22px;; font-size:13px;border-top:3px solid #E6E6E6; line-height: 1.38; font-family:Tahoma">
                                        <div style="color:#868585">
                                            <p><span style="color:#666666;"><span style="font-size: 12px;"><font face="Times New Roman"><span style="line-height: normal;">Bạn nhận được thư n&agrave;y v&igrave; đ&atilde; đăng k&yacute; th&agrave;nh vi&ecirc;n tr&ecirc;n Kyna.vn. Ch&uacute;ng t&ocirc;i mong gửi đến bạn những th&ocirc;ng tin v&agrave; chia sẻ hữu &iacute;ch.</span></font></span></span></p>

                                            <p><span style="font-size:12px;"><font face="Times New Roman"><span style="line-height: normal;"><span style="color:#666666;">Nếu kh&ocirc;ng muốn tiếp tục nhận email nữa, bạn vui l&ograve;ng hủy nhận </span><a href="[unsubscribe]"><span style="color:#666666;">tại đ&acirc;y</span></a><span style="color:#666666;">.</span></span></font></span></p>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" bgcolor="#EAEAEA" style="padding: 20px 22px;border:1px solid #ddd; border-width: 1px 0">
                                        <table border="0" style="width:100%; font-size:13px;line-height: 1.38; font-family:Tahoma">
                                            <tbody>
                                                <tr>
                                                    <td style="width:460px">
                                                        <div style="color:#727272">
                                                            <div style="font-family: Tahoma; font-size: 13px; line-height: 17.94px; color: rgb(114, 114, 114); background-color: rgb(234, 234, 234);"><span style="font-size:12px;">&copy; 2014 - Bản quyền của C&ocirc;ng Ty Cổ Phần Dream Viet Education<br />
									VP TPHCM: 178/8, Đường D1, Phường 25, Quận B&igrave;nh Thạnh, TP Hồ Ch&iacute; Minh</span></div>

                                                            <div style="font-family: Tahoma; font-size: 13px; line-height: 17.94px; color: rgb(114, 114, 114); background-color: rgb(234, 234, 234);"><span style="font-size:12px;">VP H&agrave; Nội: 25 Vũ Ngọc Phan, Phường L&aacute;ng Hạ, Quận Đống Đa, TP H&agrave; Nội</span><br />
                                                                &nbsp;</div>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div style="font-size:14px;text-align:right;margin-bottom:13px"><font face="Times New Roman" size="3"><span style="line-height: normal;">Follow us</span></font></div>

                                                        <div style="text-align:right"><font color="#000000" face="Times New Roman" size="3"><span style="line-height: normal;"><a href="https://www.facebook.com/kyna.vn"><img alt="" src="http://kyna.vn/media/images/layout_v2/ico_facebook_26x26.png" /></a>&nbsp;<a href="http://plus.google.com/114419162248284854241?rel=author"><img alt="" src="http://kyna.vn/media/images/layout_v2/ico_google-plus_26x26.png" /></a> <a href="https://www.youtube.com/user/kynavn"><img alt="" src="http://kyna.vn/media/images/layout_v2/ico_youtube_26x26.png" /></a></span></font></div>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>
    </body>
</html>
