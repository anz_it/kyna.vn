<?php

use yii\helpers\Url;
use kyna\order\models\Order;

/* @var $order Order */

$order = Order::findOne($order->id);
$orderDetails = $order->details;

$groupComboDetails = [];
foreach ($orderDetails as $item) {
    if (!empty($item->course_combo_id)) {
        $groupComboDetails[$item->course_combo_id][] = $item;
    } else {
        $groupComboDetails[] = $item;
    }
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <!--[if !mso]><!-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <!--<![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title></title>

    <style type="text/css">
        /* Basics */
        body {
            Margin: 0;
            padding: 0;
            min-width: 100%;
            background-color: #ffffff;
            line-height: 1.34;
        }
        table {
            border-spacing: 0;
            font-family: 'Helvetica', Tahoma, Arial, sans-serif;
            color: #555555;
        }
        td {
            padding: 0;
        }
        img {
            border: 0;
        }
        .wrapper {
            width: 100%;
            table-layout: fixed;
            -webkit-text-size-adjust: 100%;
            -ms-text-size-adjust: 100%;
        }
        .webkit {
            max-width: 600px;
        }
        .outer {
            Margin: 0 auto;
            width: 100%;
            max-width: 600px;
        }
        .inner {
            padding: 10px;
            text-align: center;
        }
        .three-column .inner {
            padding: 10px;
            width: 180px;
            text-align: center;
        }
        .p-center {
            text-align: center
        }
        .footer  {
            font-size: 13px !important
        }
        .p-left {
            text-align: left
        }
        .p-right {
            text-align: right
        }
        .contents {
            width: 100%;
        }
        p {
            Margin: 0;
            line-height: 1.34;
        }
        a {
            color: #ee6a56;
            text-decoration: underline;
        }
        .h1 {
            font-size: 20px;
            font-weight: bold;
            Margin-bottom: 18px;
            color: #3fb34f;
        }
        .h2 {
            color: #3fb34f;
            font-weight: bold;
            Margin-bottom: 6px;
        }
        .linkstyle {
            color: #3fb34f;
            font-weight: bold;
            text-decoration: none
        }
        .full-width-image img {
            width: 100%;
            max-width: 600px;
            height: auto;
        }

        /* One column layout */
        .one-column .contents {
            text-align: left;
        }
        .one-column p {
            font-size: 14px;
            Margin-bottom: 10px;
        }

        /*Two column layout*/
        .two-column {
            text-align: center;
            font-size: 0;
        }
        .two-column .column {
            width: 100%;
            max-width: 300px;
            display: inline-block;
            vertical-align: top;
        }
        .two-column .contents {
            font-size: 14px;
            text-align: left;
        }
        .two-column img {
            max-width: 280px;
            height: auto;
        }
        .two-column .text {
            padding-top: 10px;
        }

        /*Three column layout*/
        .three-column {
            text-align: center;
            font-size: 0;
            padding-top: 10px;
            padding-bottom: 10px;
        }
        .three-column .column {
            width: 100%;
            max-width: 200px;
            display: inline-block;
            vertical-align: top;
            text-align: center
        }
        .three-column img {
            width: 100%;
            max-width: 180px;
            height: auto;
        }
        .three-column .contents {
            font-size: 14px;
            text-align: center;
        }
        .three-column .text {
            padding-top: 10px;
        }

        /* Left sidebar layout */
        .left-sidebar {
            text-align: center;
            font-size: 0;
        }
        .left-sidebar .column {
            width: 100%;
            display: inline-block;
            vertical-align: middle;
        }
        .left-sidebar .left {
            max-width: 500px;
        }
        .left-sidebar .right {
            max-width: 100px;
        }
        .left-sidebar .img {
            width: 100%;
            max-width: 80px;
            height: auto;
        }
        .left-sidebar .contents {
            font-size: 14px;
            text-align: center;
        }
        .left-sidebar a {
            color: #85ab70;
        }

        /* Right sidebar layout */
        .right-sidebar {
            text-align: center;
            font-size: 0;
        }
        .right-sidebar .column {
            width: 100%;
            display: inline-block;
            vertical-align: middle;
        }
        .right-sidebar .left {
            max-width: 100px;
        }
        .right-sidebar .right {
            max-width: 500px;
        }
        .right-sidebar .img {
            width: 100%;
            max-width: 80px;
            height: auto;
        }
        .right-sidebar .contents {
            font-size: 14px;
            text-align: center;
        }
        .right-sidebar a {
            color: #70bbd9;
        }

        /*Media Queries*/
        @media screen and (max-width: 400px) {
            .two-column .column,
            .three-column .column, .left-sidebar .column {
                max-width: 100% !important;
                display: table;
            }
            .two-column img {
                max-width: 100% !important;
            }
            .three-column img {
                max-width: 50% !important;
            }
            .two-column .contents , p{
                text-align: center
            }
            .three-column .inner {
                padding: 3px;
                width: 100%;
                text-align: center;
            }
            .inner {
                padding: 3px
            }
        }

        @media screen and (min-width: 401px) and (max-width: 650px) {
            .three-column .column {
                max-width: 33% !important;
            }
            .two-column .column {
                max-width: 50% !important;
            }
        }

    </style>

    <!--[if (gte mso 9)|(IE)]>
    <style type="text/css">
        table {border-collapse: collapse;}
    </style>
    <![endif]-->
</head>
<body>
<center class="wrapper">
    <div class="webkit">
        <!--[if (gte mso 9)|(IE)]>
        <table width="600" align="center">
            <tr>
                <td>
        <![endif]-->
        <table class="outer" align="center">
            <!-- CONTENT START -->
            <!-- TWO COLUMN -->
            <tr>
                <td class="two-column" style="border-bottom: 1px solid #eee">
                    <!--[if (gte mso 9)|(IE)]>
                    <table width="100%">
                        <tr>
                            <td width="50%" valign="top">
                    <![endif]-->
                    <div class="column">
                        <table width="100%">
                            <tr>
                                <td align="left" class="inner">
                                    <table class="contents">
                                        <tr>
                                            <td>
                                                <a href="https://kyna.vn" target="_blank"><img style="height: 38px !important" src="https://media-kyna.cdn.vccloud.vn/img/logo.png" /></a>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <!--[if (gte mso 9)|(IE)]>
                    </td><td width="50%" valign="top">
                    <![endif]-->
                    <div class="column">
                        <table width="100%">
                            <tr>
                                <td class="inner">
                                    <table class="contents">
                                        <tr>
                                            <td align="right">
                                                <p><strong>Hotline  1900 6364 09</strong></p>
                                                <p>Email <a class="linkstyle" href="mailto:hotro@kyna.vn">hotro@kyna.vn</a></p>
                                            </td>
                                        </tr>

                                    </table>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <!--[if (gte mso 9)|(IE)]>
                    </td>
                    </tr>
                    </table>
                    <![endif]-->
                </td>
            </tr>
            <!-- /TWO COLUMN -->


            <!-- ONE COLUMN -->
            <tr>
                <td class="one-column">
                    <table width="100%">
                        <tr>
                            <td align="center" class="inner contents">
                                <p class="p-center"><img src="https://i.imgur.com/J4eFHzI.gif"/></p>
                                <h1 class="h1 p-center">Thông tin đăng ký đơn hàng #<?= $order->id ?></h1>
                                <p class="p-left">Xin chào <strong><?= $order->user->profile->name ?></strong>,</p>
                                <p class="p-left">Cảm ơn bạn đã đăng ký khóa học trên <a href="https://kyna.vn" class="linkstyle">Kyna.vn</a>. Dưới đây là thông tin về các khóa học bạn đã đăng ký:</p>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <!-- /ONE COLUMN -->


            <!-- THREE COLUMN (SINGLE ROW) -->
            <tr>
                <td class="three-column">
                    <!--[if (gte mso 9)|(IE)]>
                    <table width="100%">
                        <tr>
                            <td width="200" valign="top">
                    <![endif]-->
                    <table class="column">
                        <tr>
                            <td class="inner" align="center">
                                <table class="contents">
                                    <tr>
                                        <td>
                                            <p class="h2">Email tài khoản: </p>
                                            <p><?= $order->user->email ?></p>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                    <!--[if (gte mso 9)|(IE)]>
                    </td><td width="200" valign="top">
                    <![endif]-->
                    <table class="column">
                        <tr>
                            <td class="inner" align="center">
                                <table class="contents">
                                    <tr>
                                        <td>
                                            <p class="h2">Phương thức thanh toán</p>
                                            <p><?= $order->paymentMethodName ?></p>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                    <!--[if (gte mso 9)|(IE)]>
                    </td><td width="200" valign="top">
                    <![endif]-->
                    <table class="column">
                        <tr>
                            <td class="inner" align="center">
                                <table class="contents">
                                    <tr>
                                        <td>
                                            <p class="h2">Thanh toán</p>
                                            <p><?= number_format($order->realPayment, "0", "", ".") ?>đ</p>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                    <!--[if (gte mso 9)|(IE)]>
                    </td>
                    </tr>
                    </table>
                    <![endif]-->
                </td>
            </tr>
            <!-- /THREE COLUMN (SINGLE) -->


            <!-- RIGHT SIDEBAR -->
            <tr style="background: #7a7a7a;">
                <td class="left-sidebar">
                    <!--[if (gte mso 9)|(IE)]>
                    <table width="100%" dir="rtl">
                        <tr>
                            <td width="100">
                    <![endif]-->
                    <table class="column left" style="color: #fff;">
                        <tr>
                            <td class="inner contents">
                                Khóa học
                            </td>
                        </tr>
                    </table>
                    <table class="column right" style="color: #fff;">
                        <tr>
                            <td class="inner contents">
                                Giá bán
                            </td>
                        </tr>
                    </table>
                    <!--[if (gte mso 9)|(IE)]>
                    </td><td width="500">
                    <![endif]-->

                    <!--[if (gte mso 9)|(IE)]>
                    </td>
                    </tr>
                    </table>
                    <![endif]-->
                </td>
            </tr>
            <!-- RIGHT SIDEBAR -->


            <!-- RIGHT SIDEBAR -->
            <?php foreach ($groupComboDetails as $item) : ?>
                <?php
                if (is_array($item)) {
                    $firstItem = $item[0];
                    $meta = json_decode($firstItem->course_meta);
                    $name = !empty($meta->combo_name) ? $meta->combo_name : $meta->name;
                    $subPrice = $discount = $price = 0;
                    foreach ($item as $course) {
                        $meta = json_decode($course->course_meta);
                        $subPrice += $course->unit_price;
                        $discount += $course->discount_amount;
                    }
                    $price = $subPrice - $discount;
                } else {
                    $meta = json_decode($item->course_meta);
                    $name = $meta->name;
                    $subPrice = $item->unit_price;
                    $discount = $item->discount_amount;
                    $price = $item->unit_price - $item->discount_amount;
                }
                ?>
                <tr>
                    <td class="left-sidebar" valign="top" style="border-bottom: 1px dotted #e5ebef">
                        <!--[if (gte mso 9)|(IE)]>
                        <table width="100%" dir="rtl">
                            <tr>
                                <td width="100">
                        <![endif]-->
                        <table class="column left">
                            <tr>
                                <td class="inner contents" valign="top">
                                    <?= $name ?>
                                </td>
                            </tr>
                        </table>
                        <table class="column right">
                            <tr>
                                <td class="inner contents" valign="top">
                                    <?php if ($subPrice > $price): ?>
                                        <p style="text-decoration: line-through"><?= (!empty($subPrice) ? number_format($subPrice, "0", "", ".") . 'đ' : 'Miễn phí') ?></p>
                                    <?php endif; ?>
                                    <p><?= (!empty($price) ? number_format($price, "0", "", ".") . 'đ' : 'Miễn phí') ?></p>
                                </td>
                            </tr>
                        </table>
                        <!--[if (gte mso 9)|(IE)]>
                        </td><td width="500">
                        <![endif]-->

                        <!--[if (gte mso 9)|(IE)]>
                        </td>
                        </tr>
                        </table>
                        <![endif]-->
                    </td>
                </tr>
            <?php endforeach; ?>
            <tr>
                <td class="left-sidebar" valign="top" style="border-bottom: 1px dotted #e5ebef">
                    <!--[if (gte mso 9)|(IE)]>
                    <table width="100%" dir="rtl">
                        <tr>
                            <td width="100">
                    <![endif]-->
                    <table class="column left">
                        <tr>
                            <td class="inner contents" valign="top">
                                <strong>Tổng giá gốc</strong>
                            </td>
                        </tr>
                    </table>
                    <table class="column right">
                        <tr>
                            <td class="inner contents" valign="top">
                                <p class="h2"><?= number_format($order->subTotal, "0", "", ".") ?>đ</p>
                            </td>
                        </tr>
                    </table>
                    <!--[if (gte mso 9)|(IE)]>
                    </td><td width="500">
                    <![endif]-->

                    <!--[if (gte mso 9)|(IE)]>
                    </td>
                    </tr>
                    </table>
                    <![endif]-->
                </td>
            </tr>
            <?php
            $totalDiscount = $order->totalDiscount;
            if ($totalDiscount > 0) :
                ?>
                <tr>
                    <td class="left-sidebar" valign="top" style="border-bottom: 1px dotted #e5ebef">
                        <!--[if (gte mso 9)|(IE)]>
                        <table width="100%" dir="rtl">
                            <tr>
                                <td width="100">
                        <![endif]-->
                        <table class="column left">
                            <tr>
                                <td class="inner contents" valign="top">
                                    <strong>Giảm giá</strong>
                                </td>
                            </tr>
                        </table>
                        <table class="column right">
                            <tr>
                                <td class="inner contents" valign="top">
                                    <p class="h2">-<?= number_format($totalDiscount, "0", "", ".") ?>đ</p>
                                </td>
                            </tr>
                        </table>
                        <!--[if (gte mso 9)|(IE)]>
                        </td><td width="500">
                        <![endif]-->

                        <!--[if (gte mso 9)|(IE)]>
                        </td>
                        </tr>
                        </table>
                        <![endif]-->
                    </td>
                </tr>
            <?php endif; ?>
            <tr>
                <td class="left-sidebar" valign="top" style="border-bottom: 1px solid #eee">
                    <!--[if (gte mso 9)|(IE)]>
                    <table width="100%" dir="rtl">
                        <tr>
                            <td width="100">
                    <![endif]-->
                    <table class="column left">
                        <tr>
                            <td class="inner contents" valign="top">
                                <strong>Tổng tiền</strong>
                            </td>
                        </tr>
                    </table>
                    <table class="column right">
                        <tr>
                            <td class="inner contents" valign="top">
                                <p class="h2"><?= number_format($order->realPayment, "0", "", ".") ?>đ</p>
                            </td>
                        </tr>
                    </table>
                    <!--[if (gte mso 9)|(IE)]>
                    </td><td width="500">
                    <![endif]-->

                    <!--[if (gte mso 9)|(IE)]>
                    </td>
                    </tr>
                    </table>
                    <![endif]-->
                </td>
            </tr>
            <!-- RIGHT SIDEBAR -->


            <!-- ONE COLUMN -->
            <?php if ($order->isCod): ?>
            <tr>
                <td class="one-column">
                    <table width="100%">
                        <tr>
                            <td class="inner contents"  style="border-bottom: 1px dotted #e5ebef">
                                <br/>
                                <p class="h2">Hướng dẫn thanh toán đơn hàng</p>
                                <p>Đơn đăng ký khóa học của bạn đang được xử lý và sẽ tiến hành giao mã kích hoạt cho bạn. Phong bì kích hoạt bao gồm: phiếu có in mã kích hoạt, giấy hướng dẫn kích hoạt.</p>
                                <p><strong>Thông tin giao hàng của bạn:</strong></p>
                                <p><strong>Họ tên người mua:</strong> <?= $order->user->profile->name ?></p>
                                <?php
                                $shippingAddress = $order->shippingAddress;
                                ?>
                                <?php if (!is_null($shippingAddress)) : ?>
                                    <p><strong>Địa chỉ giao hàng:</strong> <?= $shippingAddress->fullAddressText ?></p>
                                    <p><strong>Số điện thoại: </strong> <?= !is_null($shippingAddress) ? $shippingAddress->phone_number : $order->user->phone; ?></p>
                                <?php endif; ?>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <!-- /ONE COLUMN -->

            <!-- ONE COLUMN -->
            <tr>
                <td class="one-column" style="border-bottom: 1px dotted #e5ebef">
                    <table width="100%">
                        <tr>
                            <td class="inner contents">
                                <p class="h2">Hướng dẫn học trên <a href="https://kyna.vn" class="linkstyle">Kyna.vn</a> và ứng dụng/phần mềm</p>
                                <p style="font-weight:bold; text-decoration: underline">a. Đối với khóa học video trên Kyna.vn</p>
                                <p><strong>Bước 1:</strong> Truy cập <a href="https://kyna.vn" class="linkstyle">Kyna.vn</a>, chọn nút Kích hoạt COD trên thanh menu.</p>
                                <p><strong>Bước 2:</strong> Điền mã kích hoạt được cung cấp ở mặt trước và chọn Lấy mã xác thực OTP</p>
                                <p><strong>Bước 3: </strong> Một mã bao gồm 6 số sẽ được gửi đến điện thoại bạn đã đăng ký với <a href="https://kyna.vn" class="linkstyle">Kyna.vn</a>, vui
                                    lòng nhập mã này vào ô dưới nút Lấy mã xác thực OTP. Sau đó bạn có thể tham gia vào học.</p>
                                <br/>
                                <p style="font-weight:bold; text-decoration: underline">b. Đối với các ứng dụng / phần mềm</p>
                                <p><strong>Bước 1:</strong> Tải ứng dụng bạn đã chọn mua và mở ứng dụng lên; hoặc truy cập vào phần mềm bạn đã mua</p>
                                <p><strong>Bước 2:</strong> Vào menu của ứng dụng/phần mềm và chọn Nhập mã kích hoạt, chọn nút Kích Hoạt. Sau đó bạn có thể tham gia học.</p>
                                <p><a class="linkstyle" href="https://kyna.vn/p/kyna/cau-hoi-thuong-gap" rel="noopener" target="_blank" data-saferedirecturl="https://www.google.com/url?hl=en&amp;q=https://kyna.vn/p/kyna/cau-hoi-thuong-gap">Xem thêm hướng dẫn khác</a></p>
                        </tr>
                    </table>
                </td>
            </tr>
            <!-- /ONE COLUMN -->
            <?php endif; ?>

            <?php if (in_array($order->payment_method, ['auto', 'bank-transfer'])): ?>
            <!-- ONE COLUMN -->
            <tr>
                <td class="one-column">
                    <table width="100%">
                        <tr>
                            <td class="inner contents"  style="border-bottom: 1px dotted #e5ebef">
                                <br/>
                                <p class="h2">Hướng dẫn thanh toán đơn hàng</p>
                                <p> Bạn tiến hành chuyển khoản theo các thông tin sau:</p>
                                <p><strong>Số tài khoản:</strong> 0531 0025 11245.</p>
                                <p><strong>Chủ tài khoản:</strong> Công ty cổ phần DREAM VIET EDUCATION.</p>
                                <p><strong>Ngân hàng:</strong> Ngân hàng Vietcombank, Chi nhánh Đông Sài Gòn, Tp.HCM.</p>
                                <p><strong>Ghi chú chuyển khoản:  </strong> Số điện thoại - Họ và tên - Email đăng ký học - Mã đơn hàng. Cụ thể nội dung như sau: <strong><?= $order->user->profile->phone_number ?> - <?= $order->user->profile->name ?> -  <?= $order->user->email ?> - <?= $order->id ?></strong></p>
                                <p>Trong trường hợp chuyển khoản, sau khi chúng tôi kiểm tra thanh toán thành công, khóa học video sẽ được kích hoạt và thêm vào tài khoản học trên <a href="https://kyna.vn" class="linkstyle">Kyna.vn</a> . Đối với khóa học dạng ứng dụng/phần mềm, <a href="https://kyna.vn" class="linkstyle">Kyna.vn</a> sẽ gửi email chứa mã kích hoạt cho bạn</p>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <!-- /ONE COLUMN -->


            <!-- ONE COLUMN -->
            <tr>
                <td class="one-column" style="border-bottom: 1px dotted #e5ebef">
                    <table width="100%">
                        <tr>
                            <td class="inner contents">
                                <p class="h2">Hướng dẫn học trên <a href="https://kyna.vn" class="linkstyle">Kyna.vn</a> và ứng dụng/phần mềm</p>
                                <p style="font-weight:bold; text-decoration: underline">a. Đối với khóa học video trên Kyna.vn</p>
                                <p><strong>Bước 1:</strong> Truy cập <a href="https://kyna.vn" class="linkstyle">Kyna.vn</a>, đăng nhập tài khoản email <strong><?= $order->user->email ?></strong></p>
                                <p><strong>Bước 2: </strong> Để bắt đầu học, bạn vào mục Khóa học của tôi và click chọn nút Bắt đầu học ở khóa học muốn tham gia.</p>
                                <br/>
                                <p style="font-weight:bold; text-decoration: underline">b. Đối với các ứng dụng / phần mềm</p>
                                <p><strong>Bước 1:</strong> Tải ứng dụng bạn đã chọn mua và mở ứng dụng lên; hoặc truy cập vào phần mềm bạn đã mua</p>
                                <p><strong>Bước 2:</strong> Vào menu của ứng dụng/phần mềm và chọn Nhập mã kích hoạt, chọn nút Kích Hoạt. Sau đó bạn có thể tham gia học.</p>
                                <p><a class="linkstyle" href="https://kyna.vn/p/kyna/cau-hoi-thuong-gap" rel="noopener" target="_blank" data-saferedirecturl="https://www.google.com/url?hl=en&amp;q=https://kyna.vn/p/kyna/cau-hoi-thuong-gap">Xem thêm hướng dẫn khác</a></p>
                        </tr>
                    </table>
                </td>
            </tr>
            <!-- /ONE COLUMN -->
            <?php endif; ?>

            <?php if ($order->payment_method == 'payoo'): ?>
                <tr>
                    <td class="one-column" style="border-bottom: 1px dotted #e5ebef">
                        <table width="100%">
                            <tr>
                                <td class="inner contents">
                                    <p class="h2">Hướng dẫn thanh toán</p>
                                    <?php
                                        if($order->paymentTransactionPayoo)
                                            $codePayoo = $order->paymentTransactionPayoo->transaction_code;
                                        else
                                            $codePayoo = 0;

                                    $dateExpire = date('d/m/Y', strtotime("+2 day", time()));
                                    $dateExpireHour = date('H', strtotime("+2 day", time()));
                                    $dateExpireMi = date('i', strtotime("+2 day", time()));

                                    ?>
                                    <p style="font-weight:bold; font-size: 20px"><span style="">Mã thanh toán Payoo của bạn là:</span> <span style="color: #50ad4e"><?= $codePayoo ?></span></p>
                                    <p>
                                        Vui lòng thanh toán trước <strong style="font-size: 20px;color: #FB6A00;" ><?= $dateExpireHour?> : <?= $dateExpireMi?> Phút Ngày <?= $dateExpire?> </strong>
                                    </p>
                                    <p><strong>Bước 1:</strong> Bạn đến điểm giao dịch của Payoo như siêu thị tiện lợi (<a href="https://payoo.vn/diem-giao-dich" class="linkstyle">Xem danh sách</a>) và xuất trình mã thanh toán Payoo <span style="color: #50ad4e"><?= $codePayoo ?></span> để nhân viên thu ngân kích hoạt cho bạn. Nhân viên có thể sẽ xác nhận lại email / số điện thoại mua khoá học Kyna của bạn.
                                    </p>
                                    <p><strong>Bước 2:</strong> Sau khi nhân viên kích hoạt thành công cho bạn, bạn truy cập Kyna.vn và đăng nhập với tài khoản đã đăng ký. Để bắt đầu học, bạn vào mục Khóa học của tôi và click chọn nút Bắt đầu học ở khóa học muốn tham gia.</p>

                            </tr>
                        </table>
                    </td>
                </tr>
            <?php endif;?>


            <!-- ONE COLUMN -->
            <tr>
                <td class="one-column">
                    <table width="100%">
                        <tr>
                            <td class="inner contents">
                                <p>Nếu bạn gặp khó khăn trong quá trình kích hoạt, hãy liên hệ với chúng tôi để được hỗ trợ:</p>
                                <p><strong>Điện thoại:</strong> 1900 6364 09 (08h30 - 22h00, 7 ngày trong tuần)</p>
                                <p><strong>Email:</strong> <a class="linkstyle" href="mailto:hotro@kyna.vn">hotro@kyna.vn</a></p>
                                <br/>
                                <p>Trân trọng</p>
                                <p>Kyna.vn</p>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <!-- /ONE COLUMN -->


            <!-- ONE COLUMN -->
            <tr>
                <td class="one-column">
                    <table width="100%">
                        <tr>
                            <td class="inner contents" style="font-size: 13px !important;border-top: 1px dotted #e5ebef">
                                <p class="footer p-center">Kyna.vn | Học online cùng chuyên gia</p>
                                <p class="footer p-center">VP Hồ Chí Minh: Lầu 6 Tòa nhà Thịnh Phát - 178/8 Đường D1, Phường 25, Quận Bình Thạnh, TP Hồ Chí Minh</p>
                                <p class="footer p-center">VP Hà Nội: Tầng 6, Tòa Nhà Viện Công Nghệ - 25 Vũ Ngọc Phan, Phường Láng Hạ, Quận Đống Đa, TP Hà Nội</p>
                                <p class="p-center">
                                    <a href="https://www.facebook.com/kyna.vn" title="Facebook" target="_blank">
                                        <img style="height: 30px" src="https://i.imgur.com/XhTjAca.png"/></a>
                                    <a href="https://www.youtube.com/user/kynavn" title="Facebook" target="_blank">
                                        <img  style="height: 30px" src="https://i.imgur.com/WQaczqb.png"/></a>
                                </p>
                        </tr>
                    </table>
                </td>
            </tr>
            <!-- /ONE COLUMN -->


            <!-- /CONTENT -->
        </table>
        <!--[if (gte mso 9)|(IE)]>
        </td>
        </tr>
        </table>
        <![endif]-->
    </div>
</center>
</body>
</html>
