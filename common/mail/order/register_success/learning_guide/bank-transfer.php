<?php

use yii\helpers\Url;
?>
<p>Đơn đăng ký khóa học của bạn đã được ghi nhận. Để bắt đầu học, bạn thao tác theo các bước sau:</p>
<table cellpadding="0" cellspacing="0">
    <tr>
        <td class="pattern" width="600" align="center">
            <table cellpadding="0" cellspacing="0">
                <tr>
                    <td style="padding: 20px 0px 0px 0px; font-size: 14px">
                        <table cellpadding="0" cellspacing="0">
                            <tr>
                                <td class="col col1" width="150" align="center" style="margin-bottom: 5px;">
                                    <table cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td style="padding: 0px 10px 0px 0px;">
                                                <table cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td>
                                                            <img src="<?= Url::base(true)?>/img/mail/RegistrationCourseChuyenKhoan1.png" style="display: block;">
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td class="col" width="450" align="center">
                                    <table cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td style="padding:0px 10px 0px 0px;">
                                                <p>
                                                    <span style="font-weight: bold; color: #50ad4e; display: block;">Bước 1:</span>
                                                    Bạn chuyển khoản cho Kyna.vn theo các thông tin sau:<br/>
                                                    &#8226; Số tài khoản: 0531 0025 11245<br/>
                                                    &#8226; Chủ tài khoản: Công ty cổ phần DREAM VIET EDUCATION<br/>
                                                    &#8226; Ngân hàng: Ngân hàng Vietcombank, Chi nhánh Đông Sài Gòn, TP.HCM.<br/>
                                                    Ghi chú khi chuyển khoản:<br/>
                                                    &#8226; Tại mục "Ghi chú" khi chuyển khoản, bạn ghi rõ: Số điện thoại - Họ và tên - Email đăng ký học - Khóa học đăng ký<br/>
                                                    &#8226; Ví dụ: 0909090909 - Nguyen Thi Huong Lan - nguyenthi-huonglan@gmail.com - Kỹ năng quản lý cảm xúc<br/><br/>
                                                    Hoặc chuyển qua Paypal<br/>
                                                    &#8226; Địa chỉ email: ketoan@kyna.vn.
                                                    &#8226; Tại mục "Message" khi chuyển tiền, bạn ghi rõ: Số điện thoại - Họ và tên - Email đăng ký học - Khóa học đăng ký<br/>
                                                    &#8226; Tỉ giá áp dụng 1 USD = 21.000 VND (tỉ giá trên PayPay)

                                                </p>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td style="padding: 20px 0px 20px 0px; font-size: 14px">
                        <table cellpadding="0" cellspacing="0">
                            <tr>
                                <td class="col col1" width="150" align="center" style="margin-bottom: 5px;">
                                    <table cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td style="padding: 0px 10px 0px 0px;">
                                                <table cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td>
                                                            <img src="<?= Url::base(true)?>/img/mail/RegistrationCourse2.png" style="display: block;">
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td class="col" width="450" align="center">
                                    <table cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td style="padding:0px 10px 0px 0px;">
                                                <p>
                                                    <span style="font-weight: bold; color: #50ad4e; display: block;">Bước2:</span>
                                                    Kyna.vn sẽ tiến hành kích hoạt khóa học sau khi nhận thanh toán. Để bắt đầu học, bạn vào mục <a href="<?= Url::toRoute(['/user/course/index']) ?>" style="font-weight: bold; color: #50ad4e; text-decoration: none">Khóa học của tôi</a> và click chọn nút <span style="font-weight: bold">Bắt đầu học</span> ở khóa học muốn tham gia.
                                                </p>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>

            </table>
        </td>
    </tr>
</table>
