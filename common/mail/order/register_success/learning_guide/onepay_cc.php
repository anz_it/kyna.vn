<?php

use yii\helpers\Url;
?>
<table cellpadding="0" cellspacing="0">
    <tr>
        <td class="pattern" width="600" align="center">
            <table cellpadding="0" cellspacing="0">                                                                    
                <tr>
                    <td style="padding: 20px 0px 20px 0px; font-size: 14px">
                        <table cellpadding="0" cellspacing="0">
                            <tr>
                                <td class="col col1" width="150" align="center" style="margin-bottom: 5px;">
                                    <table cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td style="padding: 0px 10px 0px 0px;">
                                                <table cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td>                                                         
                                                            <img src="<?= Url::base(true)?>/img/mail/RegistrationCourseOnline.png" style="display: block;">
                                                        </td>
                                                    </tr>                                                    
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td class="col" width="450" align="center">
                                    <table cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td style="padding:0px 10px 0px 0px;">
                                                <p>
                                                    Đơn đăng ký khóa học đã thanh toán thành công. Khóa học của bạn đã được kích hoạt. Để bắt đầu học, bạn vào mục <a href="<?= Url::toRoute(['/user/course/index'], true) ?>" style="font-weight: bold; color: #50ad4e; text-decoration: none">Khóa học của tôi</a> và click chọn nút <span style="font-weight: bold">Bắt đầu học</span> ở khóa học muốn tham gia.
                                                </p>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>

            </table>
        </td>
    </tr>
</table>
