<?php

use yii\helpers\Url;

?>
<p>Đơn đăng ký khóa học của bạn đang được xử lý. Kyna.vn sẽ tiến hành giao mã kích hoạt cho bạn. Sau khi nhận mã và thanh toán, để bắt đầu học, bạn thao tác theo các bước sau:</p>

<table cellpadding="0" cellspacing="0">
    <tr>
        <td class="pattern" width="600" align="center">
            <table cellpadding="0" cellspacing="0">
                <tr>
                    <td style="padding: 20px 0px 0px 0px; font-size: 14px">
                        <table cellpadding="0" cellspacing="0">
                            <tr>
                                <td class="col col1" width="150" align="center" style="margin-bottom: 5px;">
                                    <table cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td style="padding: 0px 10px 0px 0px;">
                                                <table cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td>                                                         
                                                            <img src="<?= Url::base(true) ?>/img/mail/RegistrationCourse1.png" style="display: block;">
                                                        </td>
                                                    </tr>                                                    
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td class="col" width="450" align="center">
                                    <table cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td style="padding:0px 10px 0px 0px;">
                                                <p>
                                                    <span style="font-weight: bold; color: #50ad4e; display: block;">Bước 1:</span>
                                                    Truy cập <a href="https://kyna.vn/" style="font-weight: bold; color: #50ad4e; text-decoration: none;">Kyna.vn</a> và đăng nhập tài khoản <span style="font-weight: bold; color: #50ad4e;">Kyna</span> của bạn. Sau đó, vào menu <span style="font-weight: bold">Khóa học của tôi</span> ở góc phải trên thanh menu, bạn chọn Kích hoạt mã COD.                                                    
                                                </p>                                                
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td style="padding: 20px 0px 20px 0px; font-size: 14px">
                        <table cellpadding="0" cellspacing="0">
                            <tr>
                                <td class="col col1" width="150" align="center" style="margin-bottom: 5px;">
                                    <table cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td style="padding: 0px 10px 0px 0px;">
                                                <table cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td>                                                         
                                                            <img src="<?= Url::base(true) ?>/img/mail/RegistrationCourse2.png" style="display: block;">
                                                        </td>
                                                    </tr>                                                    
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td class="col" width="450" align="center">
                                    <table cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td style="padding:0px 10px 0px 0px;">
                                                <p>
                                                    <span style="font-weight: bold; color: #50ad4e; display: block;">Bước 2:</span>
                                                    Để bắt đầu học, bạn vào mục <a href="<?= Url::toRoute(['/user/course/index']) ?>" style="font-weight: bold; color: #50ad4e; text-decoration: none">Khóa học của tôi</a> và click chọn nút <span style="font-weight: bold">Bắt đầu học</span> ở khóa học muốn tham gia.
                                                </p>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>

            </table>
        </td>
    </tr>
</table>