<?php

use yii\helpers\Url;

?>
<table cellpadding="0" cellspacing="0">
    <tr>
        <td class="pattern" width="600" align="center">
            <table cellpadding="0" cellspacing="0">                                                                    
                <tr>
                    <td style="padding: 20px 0px 20px 0px; font-size: 14px">
                        <table cellpadding="0" cellspacing="0">
                            <tr>
                                <td class="col col1" width="150" align="center" style="margin-bottom: 5px;">
                                    <table cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td style="padding: 0px 10px 0px 0px;">
                                                <table cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td>                                                         
                                                            <img src="<?= Url::base(true) ?>/img/mail/RegistrationCourseOnline.png" style="display: block;">
                                                        </td>
                                                    </tr>                                                    
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td class="col" width="450" align="center">
                                    <table cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td style="padding:0px 10px 0px 0px;">
                                                <p>
                                                    <span style="font-weight: bold; color: #50ad4e; display: block;">Bước 1:</span>
                                                    Bạn đến trực tiếp văn phòng <span style="font-weight: bold; color: #50ad4e;">Kyna.vn</span> đóng tiền tại:<br/>
                                                    <span style="font-weight: bold;">&#8226;  Địa chỉ văn phòng: </span>178 D1, Phường 25, Quận Bình Thạnh, TP.HCM<br/>
                                                    <span style="font-weight: bold">&#8226;  Thời gian làm việc: </span> từ 08:30 - 17:30, từ thứ Hai tới thứ Bảy hàng tuần)
                                                </p>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>

                            <tr>
                                <td class="col col1" width="150" align="center" style="margin-bottom: 5px;">
                                    <table cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td style="padding: 0px 10px 0px 0px;">
                                                <table cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td>                                                         
                                                            <img src="<?= Url::base(true) ?>/img/mail/RegistrationCoursePickingup2.png" style="display: block;">
                                                        </td>
                                                    </tr>                                                    
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td class="col" width="450" align="center">
                                    <table cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td style="padding:0px 10px 0px 0px;">
                                                <p>
                                                    <span style="font-weight: bold; color: #50ad4e; display: block;">Bước 2:</span>
                                                    Nhân viên chăm sóc của <span style="font-weight: bold; color: #50ad4e">Kyna.vn</span> sẽ gửi email thông báo kích hoạt cho bạn sau khi nhận được thanh toán của bạn
                                                </p>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>

                            <tr>
                                <td class="col col1" width="150" align="center" style="margin-bottom: 5px;">
                                    <table cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td style="padding: 0px 10px 0px 0px;">
                                                <table cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td>                                                         
                                                            <img src="<?= Url::base(true) ?>/img/mail/RegistrationCoursePickingup3.png" style="display: block;">
                                                        </td>
                                                    </tr>                                                    
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td class="col" width="450" align="center">
                                    <table cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td style="padding:0px 10px 0px 0px;">
                                                <p>
                                                    <span style="font-weight: bold; color: #50ad4e; display: block;">Bước 3:</span>
                                                    Bạn đăng nhập <span style="font-weight: bold; color: #50ad4e">Kyna.vn</span> với tài khoản đã đăng ký. Để bắt đầu học, bạn vào mục <span style="font-weight: bold">Khóa học của tôi</span> và click chọn nút <span style="font-weight: bold">Bắt đầu học</span> ở khóa học muốn tham gia.
                                                </p>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>                                                                            
                        </table>
                    </td>
                </tr>


            </table>
        </td>
    </tr>
</table>