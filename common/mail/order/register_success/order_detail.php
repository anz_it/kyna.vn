<?php
$formatter = Yii::$app->formatter;
$orderDetails = $order->details;

$groupComboDetails = [];
foreach ($orderDetails as $item) {
    if (!empty($item->course_combo_id)) {
        $groupComboDetails[$item->course_combo_id][] = $item;
    } else {
        $groupComboDetails[] = $item;
    }
}
?>
<table style="width:100%">
    <tbody>
        <tr style="background: #50ad4e;">
            <td style="padding: 15px 10px 15px 10px; color: white;">Khóa học</td>
            <td style="padding: 15px 10px 15px 10px; color: white;">Đơn giá</td>
            <td style="padding: 15px 10px 15px 10px; color: white;">Giảm giá</td>
            <td style="padding: 15px 10px 15px 10px; color: white;">Tổng tạm</td>
        </tr>
        <?php foreach ($groupComboDetails as $item) : ?>
            <?php
            if (is_array($item)) {
                $firstItem = $item[0];
                $meta = json_decode($firstItem->course_meta);
                $name = !empty($meta->combo_name) ? $meta->combo_name : $meta->name;
                $subPrice = $discount = $price = 0;
            } else {
                $meta = json_decode($item->course_meta);
                $name = $meta->name;
                $subPrice = $item->unit_price;
                $discount = $item->discount_amount;
                $price = $item->unit_price - $item->discount_amount;
            }
            ?>
            <tr style="background: #f7f7f7;">
            <td style="padding: 10px 10px;">
                <?= $name ?>
                <?php if (is_array($item)) : ?>
                    <ul style="margin: 5px 0px 5px 15px">
                        <?php
                        foreach ($item as $course) : ?>
                            <?php
                            $meta = json_decode($course->course_meta);
                            $subPrice += $course->unit_price;
                            $discount += $course->discount_amount;
                            ?>
                            <li style="color: #272727;"><?= $meta->name ?></li>
                        <?php endforeach; ?>
                        <?php
                        $price = ($subPrice - $discount);
                        ?>
                    </ul>
                <?php endif; ?>
            </td>
            <td style="padding: 10px 10px;"><?= (!empty($subPrice) ? $formatter->asCurrency($subPrice) : 'Miễn phí') ?></td>
            <td style="padding: 10px 10px;"><?= $formatter->asCurrency($discount) ?></td>
            <td style="padding: 10px 10px; text-align: right;"><?= (!empty($price) ? $formatter->asCurrency($price) : 'Miễn phí') ?></td>
            </tr>
        <?php endforeach; ?>

        <tr style="
        background: #f0f0f0;
        ">
            <td style="padding: 10px 10px;color: #272727;"></td>
            <td style="padding: 10px 10px;color: #272727;">Tổng tạm</td>
            <td style="padding: 10px 10px;color: #272727; text-align: right"
                colspan="2"><?= $formatter->asCurrency($order->subTotal - $order->totalDiscount) ?></td>
        </tr>

        <?php
        $totalDiscount = $order->totalDiscount;
        if ($totalDiscount > 0) :
            ?>
            <tr style="
            background: #f0f0f0;
            ">
                <td style="padding: 10px 10px;"></td>
                <td style="padding: 10px 10px;color: #272727;">Giảm giá</td>
                <td style="padding: 10px 10px;color: #272727; text-align: right" colspan="2">
                    -<?= $formatter->asCurrency($totalDiscount) ?></td>
            </tr>
        <?php endif; ?>
        <?php if (!empty($order->shippingAmount)) : ?>
            <tr style="
            background: #f0f0f0;
            ">
                <td style="padding: 10px 10px;"></td>
                <td style="padding: 10px 10px;color: #272727;">Phí vận chuyển</td>
                <td style="padding: 10px 10px;color: #272727; text-align: right"
                    colspan="2"><?= $formatter->asCurrency($order->shippingAmount) ?></td>
            </tr>
        <?php endif; ?>

        <tr style="
        background: #f0f0f0;
        ">
            <td style="padding: 10px 10px;"></td>
            <td style="padding: 10px 10px;color: #272727; font-weight: bold">Tổng cộng</td>
            <td style="padding: 10px 10px;color: #272727; text-align: right; font-weight: bold"
                colspan="2"><?= $formatter->asCurrency($order->realPayment) ?></td>
        </tr>
         
    </tbody>
</table>
