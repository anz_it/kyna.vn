<table cellpadding="0" cellspacing="0">
  <tbody>
    <tr>
        <td class="pattern" width="600" align="center" style="border: 1px solid #00b8e3; border-radius: 5px;">
            <table cellpadding="0" cellspacing="0">
                <tr>
                    <td style="padding: 20px 0px 10px 0px; font-size: 14px">
                        <table cellpadding="0" cellspacing="0">
                            <tr style="text-align: left;">
                                <td class="col col1" width="300" style="margin-bottom: 5px;">
                                    <table cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td style="padding: 0px 10px 0px 20px;">
                                                <table cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td>
                                                            <span style="font-weight: bold" style="font-weight: bold; color: #272727;">Thông tin đơn hàng <span style="font-weight: bold; color: #50ad4e">#<?= (!empty($order->order_number) ? $order->order_number : $order->id) ?></span></span>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td class="col" width="300">
                                    <table cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td style="padding:0px 10px 0px 20px; color: #272727;">
                                                (Ngày <?= Yii::$app->formatter->asDate($order->order_date) ?>)
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="padding: 0px 0px 10px 0px; font-size: 14px">
                        <table cellpadding="0" cellspacing="0">
                            <tr style="text-align: left;">
                                <td class="col col1" width="300" style="margin-bottom: 5px;">
                                    <table cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td style="padding: 0px 10px 0px 20px;">
                                                <table cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td>
                                                            <span style="font-weight: bold;color: #272727;">Họ tên người mua:</span>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td class="col" width="300">
                                    <table cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td style="padding:0px 10px 0px 20px; color: #272727;">
                                                <?= $order->user->profile->name ?>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <?php
                $shippingAddress = $order->shippingAddress;
                ?>
                <?php if (!is_null($shippingAddress)) : ?>
                    <tr>
                        <td style="padding: 0px 0px 10px 0px; font-size: 14px">
                            <table cellpadding="0" cellspacing="0">
                                <tr style="text-align: left;">
                                    <td class="col col1" width="300" style="margin-bottom: 5px;">
                                        <table cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td style="padding: 0px 10px 0px 20px;">
                                                    <table cellpadding="0" cellspacing="0">
                                                        <tr>
                                                            <td>
                                                                <span style="font-weight: bold;color: #272727;">Địa chỉ giao hàng</span>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td class="col" width="300">
                                        <table cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td style="padding: 0px 10px 0px 20px;color: #272727;">
                                                    <?= $shippingAddress->fullAddressText ?>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                <?php endif; ?>
                <tr>
                    <td style="padding: 0px 0px 10px 0px; font-size: 14px">
                        <table cellpadding="0" cellspacing="0">
                            <tr style="text-align: left;">
                                <td class="col col1" width="300" style="margin-bottom: 5px;">
                                    <table cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td style="padding: 0px 10px 0px 20px;">
                                                <table cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td>
                                                            <span style="font-weight: bold;color: #272727;">Số điện thoại:</span>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td class="col" width="300">
                                    <table cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td style="padding: 0px 10px 0px 20px;color: #272727;">
                                                <?= !is_null($shippingAddress) ? $shippingAddress->phone_number : $order->user->phone; ?>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="padding: 0px 0px 20px 0px; font-size: 14px">
                        <table cellpadding="0" cellspacing="0">
                            <tr style="text-align: left;">
                                <td class="col col1" width="300" style="margin-bottom: 5px;">
                                    <table cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td style="padding: 0px 10px 0px 20px;">
                                                <table cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td>
                                                            <span style="font-weight: bold;color: #272727;">Phương thức thanh toán:</span>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td class="col" width="300">
                                    <table cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td style="padding: 0px 10px 0px 20px;color: #272727;">
                                                <?= $order->paymentMethodName ?>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
  </tbody>
</table>
