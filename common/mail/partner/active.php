<?php

use yii\helpers\Url;
use kyna\order\models\Order;
use kyna\course\models\Course;
use kyna\partner\models\Code;

/* @var $order Order */

$order = Order::findOne($order->id);
$orderDetails = $order->details;

$groupComboDetails = [];
foreach ($orderDetails as $item) {
    if (!empty($item->course_combo_id)) {
        $groupComboDetails[$item->course_combo_id][] = $item;
    } else {
        $groupComboDetails[] = $item;
    }
}

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <!--[if !mso]><!-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <!--<![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title></title>

    <style type="text/css">
        /* Basics */
        body {
            Margin: 0;
            padding: 0;
            min-width: 100%;
            background-color: #ffffff;
            line-height: 1.34;
        }
        table {
            border-spacing: 0;
            font-family: 'Helvetica', Tahoma, Arial, sans-serif;
            color: #555555;
        }
        td {
            padding: 0;
        }
        img {
            border: 0;
        }
        .wrapper {
            width: 100%;
            table-layout: fixed;
            -webkit-text-size-adjust: 100%;
            -ms-text-size-adjust: 100%;
        }
        .webkit {
            max-width: 600px;
        }
        .outer {
            Margin: 0 auto;
            width: 100%;
            max-width: 600px;
        }
        .inner {
            padding: 10px;
            text-align: center;
        }
        .three-column .inner {
            padding: 10px;
            width: 180px;
            text-align: center;
        }
        .p-center {
            text-align: center
        }
        .footer  {
            font-size: 13px !important
        }
        .p-left {
            text-align: left
        }
        .p-right {
            text-align: right
        }
        .contents {
            width: 100%;
        }
        p {
            Margin: 0;
            line-height: 1.34;
        }
        a {
            color: #ee6a56;
            text-decoration: underline;
        }
        .h1 {
            font-size: 20px;
            font-weight: bold;
            Margin-bottom: 18px;
            color: #3fb34f;
        }
        .h2 {
            color: #3fb34f;
            font-weight: bold;
            Margin-bottom: 6px;
        }
        .linkstyle {
            color: #3fb34f;
            font-weight: bold;
            text-decoration: none
        }
        .full-width-image img {
            width: 100%;
            max-width: 600px;
            height: auto;
        }

        /* One column layout */
        .one-column .contents {
            text-align: left;
        }
        .one-column p {
            font-size: 14px;
            Margin-bottom: 10px;
        }

        /*Two column layout*/
        .two-column {
            text-align: center;
            font-size: 0;
        }
        .two-column .column {
            width: 100%;
            max-width: 300px;
            display: inline-block;
            vertical-align: top;
        }
        .two-column .contents {
            font-size: 14px;
            text-align: left;
        }
        .two-column img {
            max-width: 280px;
            height: auto;
        }
        .two-column .text {
            padding-top: 10px;
        }

        /*Three column layout*/
        .three-column {
            text-align: center;
            font-size: 0;
            padding-top: 10px;
            padding-bottom: 10px;
        }
        .three-column .column {
            width: 100%;
            max-width: 200px;
            display: inline-block;
            vertical-align: top;
            text-align: center
        }
        .three-column img {
            width: 100%;
            max-width: 180px;
            height: auto;
        }
        .three-column .contents {
            font-size: 14px;
            text-align: center;
        }
        .three-column .text {
            padding-top: 10px;
        }

        /* Left sidebar layout */
        .left-sidebar {
            text-align: center;
            font-size: 0;
        }
        .left-sidebar .column {
            width: 100%;
            display: inline-block;
            vertical-align: middle;
        }
        .left-sidebar .left {
            max-width: 400px;
        }
        .left-sidebar .right {
            max-width: 200px;
        }
        .left-sidebar .img {
            width: 100%;
            max-width: 80px;
            height: auto;
        }
        .left-sidebar .contents {
            font-size: 14px;
            text-align: center;
        }
        .left-sidebar a {
            color: #85ab70;
        }

        /* Right sidebar layout */
        .right-sidebar {
            text-align: center;
            font-size: 0;
        }
        .right-sidebar .column {
            width: 100%;
            display: inline-block;
            vertical-align: middle;
        }
        .right-sidebar .left {
            max-width: 100px;
        }
        .right-sidebar .right {
            max-width: 500px;
        }
        .right-sidebar .img {
            width: 100%;
            max-width: 80px;
            height: auto;
        }
        .right-sidebar .contents {
            font-size: 14px;
            text-align: center;
        }
        .right-sidebar a {
            color: #70bbd9;
        }

        /*Media Queries*/
        @media screen and (max-width: 400px) {
            .two-column .column,
            .three-column .column, .left-sidebar .column {
                max-width: 100% !important;
                display: table;
            }
            .two-column img {
                max-width: 100% !important;
            }
            .three-column img {
                max-width: 50% !important;
            }
            .two-column .contents , p{
                text-align: center
            }
            .three-column .inner {
                padding: 3px;
                width: 100%;
                text-align: center;
            }
            .inner {
                padding: 3px
            }
        }

        @media screen and (min-width: 401px) and (max-width: 650px) {
            .three-column .column {
                max-width: 33% !important;
            }
            .two-column .column {
                max-width: 50% !important;
            }
        }

    </style>

    <!--[if (gte mso 9)|(IE)]>
    <style type="text/css">
        table {border-collapse: collapse;}
    </style>
    <![endif]-->
</head>
<body>
<center class="wrapper">
    <div class="webkit">
        <!--[if (gte mso 9)|(IE)]>
        <table width="600" align="center">
            <tr>
                <td>
        <![endif]-->
        <table class="outer" align="center">
            <!-- CONTENT START -->
            <!-- TWO COLUMN -->
            <tr>
                <td class="two-column" style="border-bottom: 1px solid #eee">
                    <!--[if (gte mso 9)|(IE)]>
                    <table width="100%">
                        <tr>
                            <td width="50%" valign="top">
                    <![endif]-->
                    <div class="column">
                        <table width="100%">
                            <tr>
                                <td align="left" class="inner">
                                    <table class="contents">
                                        <tr>
                                            <td>
                                                <a href="https://kyna.vn" target="_blank"><img style="height: 38px !important" src="https://media-kyna.cdn.vccloud.vn/img/logo.png" /></a>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <!--[if (gte mso 9)|(IE)]>
                    </td><td width="50%" valign="top">
                    <![endif]-->
                    <div class="column">
                        <table width="100%">
                            <tr>
                                <td class="inner">
                                    <table class="contents">
                                        <tr>
                                            <td align="right">
                                                <p><strong>Hotline  1900 6364 09</strong></p>
                                                <p>Email <a class="linkstyle" href="mailto:hotro@kyna.vn">hotro@kyna.vn</a></p>
                                            </td>
                                        </tr>

                                    </table>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <!--[if (gte mso 9)|(IE)]>
                    </td>
                    </tr>
                    </table>
                    <![endif]-->
                </td>
            </tr>
            <!-- /TWO COLUMN -->


            <!-- ONE COLUMN -->
            <tr>
                <td class="one-column">
                    <table width="100%">
                        <tr>
                            <td align="center" class="inner contents">
                                <p class="p-center"><img src="https://i.imgur.com/J4eFHzI.gif"/></p>
                                <h1 class="h1 p-center">Kyna for Kids - MÃ KÍCH HOẠT SẢN PHẨM</h1>
                                <p class="p-left">Xin chào <strong><?= $order->user->profile->name ?></strong>,</p>
                                <p class="p-left">Cảm ơn sự tin tưởng và ủng hộ của bạn dành cho công ty chúng tôi. Dưới đây là mã kích hoạt các ứng dụng/phần mềm bạn đã chọn mua:</p>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <!-- /ONE COLUMN -->


            <tr style="background: #7a7a7a;">
                <td class="left-sidebar">
                    <!--[if (gte mso 9)|(IE)]>
                    <table width="100%" dir="rtl">
                        <tr>
                            <td width="100">
                    <![endif]-->
                    <table class="column left" style="color: #fff;">
                        <tr>
                            <td class="inner contents">
                                Ứng dụng / Phần mềm
                            </td>
                        </tr>
                    </table>
                    <table class="column right" style="color: #fff;">
                        <tr>
                            <td class="inner contents">
                                Mã kích hoạt
                            </td>
                        </tr>
                    </table>
                    <!--[if (gte mso 9)|(IE)]>
                    </td><td width="500">
                    <![endif]-->

                    <!--[if (gte mso 9)|(IE)]>
                    </td>
                    </tr>
                    </table>
                    <![endif]-->
                </td>
            </tr>
            <!-- RIGHT SIDEBAR -->


            <!-- RIGHT SIDEBAR -->
            <?php foreach ($groupComboDetails as $key => $item) :
                $courseId = is_array($item) ? $key : $item->course_id;
                $course = Course::findOne($courseId);
                if (!$course->isPartner && !$course->isComboPartner) {
                    continue;
                }
                if (is_array($item)) {
                    $firstItem = $item[0];
                    $courseItem = Course::findOne($firstItem->course_id);
                    $partnerName = $courseItem->partner->name;
                    $code = Code::findOne(['serial' => $firstItem->activation_code]);
                } else {
                    $partnerName = $course->partner->name;
                    $code = Code::findOne(['serial' => $item->activation_code]);
                }
                ?>
                <?php if(!empty($code)):?>
                <tr>
                    <td class="left-sidebar" valign="top" style="border-bottom: 1px dotted #e5ebef">
                        <!--[if (gte mso 9)|(IE)]>
                        <table width="100%" dir="rtl">
                            <tr>
                                <td width="100">
                        <![endif]-->
                        <table class="column left">
                            <tr>
                                <td class="inner contents" valign="top">
                                    <?= $partnerName ?> / <?= $code->category->title ?>
                                </td>
                            </tr>
                        </table>
                        <table class="column right">
                            <tr>
                                <td class="inner contents" valign="top">
                                    <p><?= $code->code ?></p>
                                </td>
                            </tr>
                        </table>
                        <!--[if (gte mso 9)|(IE)]>
                        </td><td width="500">
                        <![endif]-->

                        <!--[if (gte mso 9)|(IE)]>
                        </td>
                        </tr>
                        </table>
                        <![endif]-->
                    </td>
                </tr>
            <?php endif;?>
            <?php endforeach; ?>


            <!-- ONE COLUMN -->
            <tr>
                <td class="one-column" style="border-bottom: 1px dotted #e5ebef">
                    <table width="100%">
                        <tr>
                            <td class="inner contents">
                                <p class="h2">Hướng dẫn học trên ứng dụng/phần mềm</p>

                                <p><strong>Bước 1:</strong> Tải ứng dụng bạn đã chọn mua và mở ứng dụng lên; hoặc truy cập vào phần mềm bạn đã mua</p>
                                <p><strong>Bước 2:</strong> Vào menu của ứng dụng/phần mềm và chọn Nhập mã kích hoạt, chọn nút Kích Hoạt. Sau đó bạn có thể tham gia học.</p>
                                <p><a class="linkstyle" href="https://kyna.vn/p/kyna/cau-hoi-thuong-gap" rel="noopener" target="_blank" data-saferedirecturl="https://www.google.com/url?hl=en&amp;q=https://kyna.vn/p/kyna/cau-hoi-thuong-gap">Xem thêm hướng dẫn khác</a></p>
                        </tr>
                    </table>
                </td>
            </tr>
            <!-- /ONE COLUMN -->


            <!-- ONE COLUMN -->
            <tr>
                <td class="one-column">
                    <table width="100%">
                        <tr>
                            <td class="inner contents">
                                <p>Nếu bạn gặp khó khăn trong quá trình kích hoạt, hãy liên hệ với chúng tôi để được hỗ trợ:</p>
                                <p><strong>Điện thoại:</strong> 1900 6364 09 (08h30 - 22h00, 7 ngày trong tuần)</p>
                                <p><strong>Email:</strong> <a class="linkstyle" href="mailto:hotro@kyna.vn">hotro@kyna.vn</a></p>
                                <br/>
                                <p>Trân trọng</p>
                                <p>Kyna.vn</p>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <!-- /ONE COLUMN -->


            <!-- ONE COLUMN -->
            <tr>
                <td class="one-column">
                    <table width="100%">
                        <tr>
                            <td class="inner contents" style="font-size: 13px !important;border-top: 1px dotted #e5ebef">
                                <p class="footer p-center">Kyna.vn | Học online cùng chuyên gia</p>
                                <p class="footer p-center">VP Hồ Chí Minh: Lầu 6 Tòa nhà Thịnh Phát - 178/8 Đường D1, Phường 25, Quận Bình Thạnh, TP Hồ Chí Minh</p>
                                <p class="footer p-center">VP Hà Nội: Tầng 6, Tòa Nhà Viện Công Nghệ - 25 Vũ Ngọc Phan, Phường Láng Hạ, Quận Đống Đa, TP Hà Nội</p>
                                <p class="p-center">
                                    <a href="https://www.facebook.com/kyna.vn" title="Facebook" target="_blank">
                                        <img style="height: 30px" src="https://i.imgur.com/XhTjAca.png"/></a>
                                    <a href="https://www.youtube.com/user/kynavn" title="Facebook" target="_blank">
                                        <img  style="height: 30px" src="https://i.imgur.com/WQaczqb.png"/></a>
                                </p>
                        </tr>
                    </table>
                </td>
            </tr>
            <!-- /ONE COLUMN -->


            <!-- /CONTENT -->
        </table>
        <!--[if (gte mso 9)|(IE)]>
        </td>
        </tr>
        </table>
        <![endif]-->
    </div>
</center>
</body>
</html>

