<?php

use yii\helpers\Url;
$hotline = !empty($settings['hot_line']) ? $settings['hot_line'] : '1900.6364.09';
$supportMail = !empty($settings['email_footer']) ? $settings['email_footer'] : 'hotro@kyna.vn';
?>
<!doctype html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>[KidsUP - Kyna for Kids] - MÃ KÍCH HOẠT SẢN PHẨM</title>
    <style>
        /* -------------------------------------
        GLOBAL
        ------------------------------------- */
        * {
            margin:0;
            padding:0;
        }
        * { font-family: "arial",sans-serif }
        img {
            max-width: 100%;
        }
        .collapse {
            margin:0;
            padding:0;
        }
        body {
            -webkit-font-smoothing:antialiased;
            -webkit-text-size-adjust:none;
            width: 100%!important;
            height: 100%;
        }
        /* -------------------------------------
        ELEMENTS
        ------------------------------------- */
        a { color: #2BA6CB;}
        .btn {
            text-decoration:none;
            color: #FFF;
            background-color: #666;
            padding:10px 16px;
            font-weight:bold;
            margin-right:10px;
            text-align:center;
            cursor:pointer;
            display: inline-block;
        }
        p.callout {
            padding:15px;
            background-color:#ECF8FF;
            margin-bottom: 15px;
        }
        .callout a {
            font-weight:bold;
            color: #2BA6CB;
        }
        table.social {
            /*  padding:15px; */
            background-color: #ebebeb;
        }
        .social .soc-btn {
            padding: 3px 7px;
            font-size:12px;
            margin-bottom:10px;
            text-decoration:none;
            color: #FFF;font-weight:bold;
            display:block;
            text-align:center;
        }
        a.fb { background-color: #3B5998!important; }
        a.tw { background-color: #1daced!important; }
        a.gp { background-color: #DB4A39!important; }
        a.ms { background-color: #000!important; }
        .sidebar .soc-btn {
            display:block;
            width:100%;
        }
        /* -------------------------------------
        HEADER
        ------------------------------------- */
        table.head-wrap { width: 100%;}
        .header.container table td.logo { padding: 15px; }
        .header.container table td.label { padding: 15px; padding-left:0px;}
        /* -------------------------------------
        BODY
        ------------------------------------- */
        table.body-wrap { width: 100%;}
        /* -------------------------------------
        FOOTER
        ------------------------------------- */
        table.footer-wrap { width: 100%;    clear:both!important;
        }
        .footer-wrap .container td.content  p { border-top: 1px solid rgb(215,215,215); padding-top:15px;}
        .footer-wrap .container td.content p {
            font-size:10px;
            font-weight: bold;
        }
        /* -------------------------------------
        TYPOGRAPHY
        ------------------------------------- */
        h1,h2,h3,h4,h5,h6 {
            font-family: "HelveticaNeue-Light", "Helvetica Neue Light", "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif; line-height: 1.1; margin-bottom:15px; color:#000;
        }
        h1 small, h2 small, h3 small, h4 small, h5 small, h6 small { font-size: 60%; color: #6f6f6f; line-height: 0; text-transform: none; }
        h1 { font-weight:200; font-size: 44px;}
        h2 { font-weight:200; font-size: 37px;}
        h3 { font-weight:500; font-size: 27px;}
        h4 { font-weight:500; font-size: 23px;}
        h5 { font-weight:900; font-size: 17px;}
        h6 { font-weight:900; font-size: 14px; text-transform: uppercase; color:#444;}
        .collapse { margin:0!important;}
        p, ul {
            margin-bottom: 10px;
            font-weight: normal;
            font-size:14px;
            line-height:1.6;
        }
        p.lead { font-size:17px; }
        p.last { margin-bottom:0px;}
        ul li {
            margin-left:5px;
            list-style-position: inside;
        }
        /* -------------------------------------
        SIDEBAR
        ------------------------------------- */
        ul.sidebar {
            background:#ebebeb;
            display:block;
            list-style-type: none;
        }
        ul.sidebar li { display: block; margin:0;}
        ul.sidebar li a {
            text-decoration:none;
            color: #666;
            padding:10px 16px;
            /*  font-weight:bold; */
            margin-right:10px;
            /*  text-align:center; */
            cursor:pointer;
            border-bottom: 1px solid #777777;
            border-top: 1px solid #FFFFFF;
            display:block;
            margin:0;
        }
        ul.sidebar li a.last { border-bottom-width:0px;}
        ul.sidebar li a h1,ul.sidebar li a h2,ul.sidebar li a h3,ul.sidebar li a h4,ul.sidebar li a h5,ul.sidebar li a h6,ul.sidebar li a p { margin-bottom:0!important;}
        /* ---------------------------------------------------
        RESPONSIVENESS
        Nuke it from orbit. It's the only way to be sure.
        ------------------------------------------------------ */
        /* Set a max-width, and make it display as block so it will automatically stretch to that width, but will also shrink down on a phone or something */
        .container {
            display:block!important;
            max-width:600px!important;
            margin:0 auto!important; /* makes it centered */
            clear:both!important;
        }
        /* This should also be a block element, so that it will fill 100% of the .container */
        .content {
            padding:15px;
            max-width:600px;
            margin:0 auto;
            display:block;
        }
        /* Let's make sure tables in the content area are 100% wide */
        .content table { width: 100%; }
        /* Odds and ends */
        .column {
            width: 300px;
            float:left;
        }
        .column tr td { padding: 15px; }
        .column-wrap {
            padding:0!important;
            margin:0 auto;
            max-width:600px!important;
        }
        .column table { width:100%;}
        .social .column {
            width: 280px;
            min-width: 279px;
            float:left;
        }
        /* Be sure to place a .clear element after each set of columns, just to be safe */
        .clear { display: block; clear: both; }
        /* -------------------------------------------
        PHONE
        For clients that support media queries.
        Nothing fancy.
        -------------------------------------------- */
        @media only screen and (max-width: 600px) {
            a[class="btn"] { display:block!important; margin-bottom:10px!important; background-image:none!important; margin-right:0!important;}
            div[class="column"] { width: auto!important; float:none!important;}
            table.social div[class="column"] {
                width:auto!important;
            }
        }
    </style>
    <style>
        .bold {
            font-weight: bold;
        }
        /* HEADER */
        .header ul {
            padding: 0px;
            margin: 0px;
            list-style-type: none;
        }
        .header .content {
            padding-left: 0px;
            padding-right: 0px;
        }
        .header .content ul li span {
            margin-left: 5px;
        }
        /* NAV */
        .content.nav {
            padding: 0px;
        }
        .content.nav tr td tr td {
            border-right: 1px solid white;
        }
        /* CONTENT */
        .title-main {
            color: #50ad4e;
            font-size: 26px;
            text-align: center;
            font-weight: bold;
            padding: 20px 0px;
            margin-bottom: 0px;
            line-height: 40px;
        }
        .content.main img {
            margin: 0px auto 35px;
            display: inherit;
        }
        .color-green {
            color: #50ad4e;
        }
        .content.main ul.top {
            padding: 0px;
            margin: 0px;
            list-style: none;
        }

        .content.main ul.top li:last-child {
            border: 1px solid #00b8e3;
            padding: 10px;
            border-radius: 5px;
        }
        .content.main .text p {
            margin-top: 10px;
        }
        .content.main .line {
            margin: 10px 0px;
            display: inline-block;
            width: 100%;
        }
        .content.main .line hr {
            width: 50%;
            margin: 0px auto;
        }
        .content.main ul.bottom {
            padding: 0px;
            list-style: none;
            text-align: center;
        }
        .social {
            display: inline-block;
            width: 100%;
            text-align: center;
        }
        .social ul {
            padding: 0px;
            margin: 5px 0px;
            list-style: none;
            display: inline-block;
        }
        .social ul li {
            float: left;
            margin-right: 15px;
        }
        .content.main .social img {
            margin-bottom: 0px;
        }
        /* FOOTER */
        .footer.container {
            background: none;
        }
        .footer.container p {
            text-align: center;
            color: #7c7c7c;
        }
        .main_cta img { width: 100%; height: auto !important; }
        /* Top Links Navigation Pattern CSS */
        @media only screen and (max-width: 632px) {
            td[class="main_nav"] td {
                display: inline-block;
            }
        }
    </style>
</head>
<body bgcolor="#f0f0f0">
<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" style="font-family: Tahoma, Arial;background-color:#f0f0f0;" >
    <tr>

        <td>
            <!-- HEADER -->
            <table class="head-wrap" style="width: 100%; line-height: 20px">
                <tr>

                    <td class="header container" style="display: block!important;max-width:600px!important;margin: 0 auto!important;clear: both!important;">
                        <div class="content">
                            <table style="width: 100%">
                                <tr>
                                    <td>
                                        <img src="https://i.imgur.com/ik5ejBo.png" style="height: 40px"/>
                                    </td>
                                    <td align="right">
                                        <ul style="list-style: none;">
                                            <li style="margin-bottom: 5px;"><a href="tel:1900.6364.09" style="text-decoration: blink;color: #272727;">Hotline <span class="bold" style="font-weight: bold;"><?= $hotline ?></span></a></li>
                                            <li><a href="mailto:hotro@kyna.vn" style="text-decoration: blink;color: #272727;">Email <span class="bold" style="color: #50ad4e;font-weight: bold;"><?= $supportMail ?></span></a></li>
                                        </ul>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </td>

                </tr>
            </table>
            <!-- /HEADER -->
            <!-- CONTENT -->

            <table class="body-wrap" style="width: 100%; line-height: 20px">

                <tr>

                    <td class="container" bgcolor="white" style="display: block!important;max-width:600px!important;margin: 0 auto!important;clear: both!important;">
                        <div class="content main">
                            <table>
                                <tr>
                                    <td style="padding: 20px;">
                                        <h2 class="title-main" style="color: #50ad4e;font-size: 26px; text-align: center;font-weight: bold;padding: 20px 0px;margin-bottom: 0px; line-height: 40px;">[KYNA FOR KIDS - KIDSUP] - MÃ KÍCH HOẠT SẢN PHẨM</h2>
                                        <div class="text">
                                            <p style="color: #272727">Xin chào <span class="bold" style="font-weight: bold"><?= $order->user->profile->name ?></span></p>
                                            <p style="color: #272727">Cảm ơn sự tin tưởng và ủng hộ của bạn dành cho công ty chúng tôi. </p>
                                            <p style="color: #272727;">Bạn đã đăng ký chương trình <span class="bold" style="font-weight: bold">Kids Up - Ứng dụng giúp trẻ thông minh qua các trò chơi tương tác</span></p>
                                            <table cellpadding="0" cellspacing="0" style="margin: 15px 0px;">
                                                <tbody>
                                                <tr>
                                                    <td class="pattern" width="600" align="center" style="border: 1px solid #969696; border-radius: 5px;">
                                                        <table cellpadding="0" cellspacing="0">
                                                            <tbody>
                                                            <tr>
                                                                <td style="padding: 10px 0px 10px 0px; font-size: 14px">
                                                                    <table cellpadding="0" cellspacing="0">
                                                                        <tbody>
                                                                        <tr style="text-align: left">
                                                                            <td class="col col1" width="320" style="margin-bottom: 5px;">
                                                                                <table cellpadding="0" cellspacing="0">
                                                                                    <tbody>
                                                                                    <tr>
                                                                                        <td style="padding: 0px 10px 0px 20px;">
                                                                                            <table cellpadding="0" cellspacing="0">
                                                                                                <tbody>
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <span style="font-weight: bold">Mã kích hoạt:</span>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                </tbody>
                                                                                            </table>
                                                                                        </td>
                                                                                    </tr>
                                                                                    </tbody>
                                                                                </table>
                                                                            </td>
                                                                            <td class="col" width="280">
                                                                                <table cellpadding="0" cellspacing="0">
                                                                                    <tbody>
                                                                                    <tr>
                                                                                        <td style="padding:0px 10px 0px 20px;">
                                                                                            <span style="font-size: 20px; color: #F65A86; font-weight:bold"><?= $code->code ?></span>
                                                                                        </td>
                                                                                    </tr>
                                                                                    </tbody>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                            </tr>


                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                            <p style="color: #272727; font-size: 20px; color: #F65A86; font-weight: bold">Hướng dẫn cài đặt:</p>
                                            <p style="color: #272727"><span style="font-weight: bold">Bước 1:</span> Truy cập App Store <img src="https://i.imgur.com/rkdK8tR.jpg" style="margin:0; display:inline; height: 30px"  /> hoặc Play Store  <img src="https://i.imgur.com/CjiZlPh.png" style="margin:0; display:inline; height: 30px"  /></p>
                                            <p style="color: #272727"><span style="font-weight: bold">Bước 2:</span> Tìm kiếm với từ khóa “Kids Up – Play & Learn”</p>
                                            <p style="color: #272727"><span style="font-weight: bold">Bước 3:</span> Ấn vào biểu tượng <img style="margin:0; display:inline; height: 30px" src="https://i.imgur.com/0HYqhvP.png" /> và bắt đầu cài đặt.</p>
                                            <p style="color: #272727"><span style="font-weight: bold">Bước 4:</span>Nhập mã kích hoạt ở trên và ấn “OK”</p>
                                            <p style="color: #272727">Ngay sau khi hệ thống xác nhận mã được nhập thành công, bố mẹ có thể bắt đầu tải nội dung khóa học về máy và sử dụng trọn đời.</p>
                                            <hr>
                                            <p style="color: #272727; font-size: 20px; color: #F65A86; font-weight: bold">Hướng dẫn sử dụng mã kích hoạt:</p>
                                            <p style="color: #272727"><span style="font-weight: bold">Bước 1:</span> Mở ứng dụng KidsUp <img src="https://i.imgur.com/0HYqhvP.png" style="margin:0; display:inline; height: 30px"  /></p>
                                            <p style="color: #272727"><span style="font-weight: bold">Bước 2:</span> Chọn icon <img src="https://i.imgur.com/0ZCg6cr.png" style="margin:0; display:inline; height: 30px"  /> ở góc trái phía trên</p>
                                            <p style="color: #272727"><span style="font-weight: bold">Bước 3:</span> Sau đó nhấp vào icon Giỏ hàng <img style="margin:0; display:inline; height: 30px" src="https://i.imgur.com/kBGRvhG.png" /> và bắt đầu cài đặt.</p>
                                            <p style="color: #272727"><span style="font-weight: bold">Bước 4:</span> Bố mẹ trả lời câu hỏi tính toán sau đó nhập mã kích hoạt được cung cấp vào ô Nhập mã kích hoạt và chọn dấu tick màu xanh để hoàn thành.</p>
                                            <p style="color: #272727">Ngay sau khi hệ thống xác nhận mã được nhập thành công, bố mẹ có thể bắt đầu tải nội dung khóa học về máy và sử dụng trọn đời.</p>
                                            <hr>
                                            <p style="color: #272727; font-size: 20px; color: #F65A86; font-weight: bold">Bố mẹ lưu ý:</p>
                                            <p style="color: #272727">Với mỗi mã kích hoạt, bố mẹ sẽ được dùng tối đa trên 2 thiết bị ở cùng một thời điểm. Kyna For Kids sẽ hỗ trợ đổi thiết bị không giới hạn.</p>

                                            <hr>
                                            <p style="color: #272727; font-size: 20px; color: #F65A86; font-weight: bold">Kyna For Kids – Trường học trực tuyến cho trẻ</p>
                                            <p style="color: #272727">Kyna For Kids là trường học trực tuyến cho trẻ, nơi bố mẹ và bé có thể học các chương trình Toán, Tiếng Anh, Phát triển kỹ năng khác ngay tại nhà, mọi lúc mọi nơi.</p>
                                            <p style="color: #272727">Với sứ mệnh giúp hàng triệu trẻ em Việt Nam phát triển toàn diện, Kyna For Kids hợp tác với các đơn vị phát triển nội dung giáo dục cho trẻ em hàng đầu tại Việt Nam và thế giới. Các chương trình đều được các chuyên gia của Kyna For Kids thẩm định trước khi đăng tải trên hệ thống, đảm bảo tính an toàn, hiệu quả giáo dục cho trẻ.</p>

                                            <p style="color: #272727">Nếu bạn cần hỗ trợ hoặc tư vấn th&ecirc;m th&ocirc;ng tin, h&atilde;y li&ecirc;n hệ với <span class="color-green bold" style="color: #50ad4e;font-weight: bold">Kyna.vn</span> qua email<br />
                                                <span class="color-green bold"><a href="mailto:<?= $supportMail ?>" target="_blank" style="color: #50ad4e;font-weight: bold; text-decoration: none;"><?= $supportMail ?></a></span> hoặc số điện thoại <span class="color-green bold" style="font-weight: bold;color: #50ad4e"><?= $hotline ?></span></p>


                                            <div class="social">
                                                <ul style="padding: 0px;text-align: center;">
                                                    <li style="display: inline-block;"><a class="facebook" href="https://www.facebook.com/kyna.vn" target="_blank"><img src="http://sendy.kyna.vn/uploads/1474442043.png" /></a></li>
                                                    <li style="display: inline-block;"><a class="youtube" href="https://www.youtube.com/user/kynavn" target="_blank"><img src="http://sendy.kyna.vn/uploads/1474442053.png" /></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </td>
                                </tr>

                            </table>
                        </div>
                    </td>

                </tr>

            </table>
            <!-- /CONTENT --><!--FOOTER -->

            <table class="body-wrap" style="width: 100%; line-height: 20px">

                <tr>

                    <td class="container footer" style="display: block!important;max-width:600px!important;margin: 0 auto!important;clear: both!important;">
                        <div class="content">
                            <p style="text-align: center; color: #272727"><!--Nếu không muốn tiếp tục nhận email giới thiệu nữa, bạn vui lòng hủy nhận <a href="#" class="bold" style="color: #7c7c7c; text-decoration: none">tại đây.</a><br/>-->Mức gi&aacute;, th&ocirc;ng tin sản phẩm v&agrave; c&aacute;c khuyến m&atilde;i trong email n&agrave;y l&agrave; ch&iacute;nh x&aacute;c tại thời điểm gởi mail, tuy nhi&ecirc;n ch&uacute;ng c&oacute; giới hạn thời gian v&agrave; c&oacute; thể thay đổi.</p>
                        </div>
                    </td>

                </tr>

            </table>
            <!--end /FOOTER-->
        </td>

    </tr>
    </tbody>
</table>
</body>
</html>
