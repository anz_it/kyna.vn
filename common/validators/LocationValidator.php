<?php

namespace common\validators;

use common\helpers\StringHelper;
use kyna\base\models\Location;

class LocationValidator extends \yii\validators\Validator
{
    public function validateAttribute($model, $attribute)
    {
        $locationId = $model->$attribute;
        if (!is_int($locationId) OR $locationId <= 0) return false;

        $locations = Location::find()->where(['>', 'parent_id', 0])->select('id')->column();

        return in_array($locationId, $locations);
    }
}
