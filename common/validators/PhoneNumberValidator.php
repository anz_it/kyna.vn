<?php

namespace common\validators;

use common\helpers\PhoneNumberHelper;

class PhoneNumberValidator extends \yii\validators\Validator
{
    public function validateAttribute($model, $attribute)
    {
        try {
            if (!PhoneNumberHelper::isPhoneNumber($model->$attribute)) {
                $this->addError($model, $attribute, 'Số điện thoại không đúng');
            }
        }
        catch(\Exception $e) {
            $this->addError($model, $attribute, 'Số điện thoại không đúng');
        }
    }
}
