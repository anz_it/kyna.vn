<?php
return [
    'adminEmail' => 'hotro@kyna.vn',
    'supportEmail' => 'hotro@kyna.vn',
    'user.passwordResetTokenExpire' => 3600,
    'pageSize' => 12,
    'epaySandBox' => true,
    'defaultPassword' => 'learningwithkyna',
    'topup' => [
        'sandbox' => [
            'ws_url' => 'http://itopup-test.megapay.net.vn:8086/CDV_Partner_Services_V1.0/services/Interfaces?wsdl',
            'partnerName' => 'partnerTest_PHP',
            'partnerPassword' => '123456abc',
            'key_sofpin' => '123456abc',

            //thời gian tối đa thực hiện giao dịch (tính bằng giây)
            'time_out' => 150
        ],
        'production' => [
            //'ws_url' => 'http://naptien.thanhtoan247.vn:8082/CDV_Partner_Services_V1.0/services/Interfaces?wsdl',
            'ws_url' => 'http://naptien.thanhtoan247.net.vn:8082/CDV_Partner_Services_V1.0/services/Interfaces?wsdl',
            'partnerName' => 'dreamviet_v3',
            'partnerPassword' => 'kyna123Asd',
            'key_sofpin' => 'D8C41557888D527F7EF52F67FFE30A69',

            //thời gian tối đa thực hiện giao dịch (tính bằng giây)
            'time_out' => 150
        ]
    ],
    'charge' => [
        'sandbox' => [

        ],
        'production' => [

        ]
    ],
    'mediaServer' => 'https://media.kyna.com.vn',
    'videoServer' => 'https://media.kyna.vn:1443/mobile',
    'rtmpServer' => 'rtmp://media.kyna.com.vn:1935',
    'flowplayer' => [
        'key' => '$631977711768310',
        'flashKey' => '#$9e934cdbe9535648374',
    ],

    'showKidCommission' => true,
];
