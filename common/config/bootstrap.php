<?php
Yii::setAlias('@common', dirname(__DIR__));
Yii::setAlias('@frontend', dirname(dirname(__DIR__)) . '/frontend');
Yii::setAlias('@donggia', dirname(dirname(__DIR__)) . '/donggia');
Yii::setAlias('@kyna', dirname(dirname(__DIR__)) . '/modules');
Yii::setAlias('@backend', dirname(dirname(__DIR__)) . '/backend');
Yii::setAlias('@upload', dirname(dirname(__DIR__)) . '/uploads');
Yii::setAlias('@console', dirname(dirname(__DIR__)) . '/console');
Yii::setAlias('@dektrium/user', dirname(dirname(__DIR__)) . '/vendor/dektrium/yii2-user');
Yii::setAlias('@mana', dirname(dirname(__DIR__)) . '/mana-frontend');
Yii::setAlias('@root', dirname(dirname(__DIR__)));
Yii::setAlias('@tracking', dirname(dirname(__DIR__)) . '/tracking');
Yii::setAlias('@cdn', '//media.kyna.com.vn');
Yii::setAlias('@media', dirname(dirname(__DIR__)) . '/media');
Yii::setAlias('@mobile', dirname(dirname(__DIR__)) . '/mobile-api');
Yii::setAlias('@kid', dirname(dirname(__DIR__)) . '/4kid-api');
//var_dump(Yii::getAlias('@dektrium\user'));die;
