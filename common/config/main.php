<?php
putenv('GOOGLE_CLOUD_PROJECT=crested-trilogy-167714');
putenv('GOOGLE_APPLICATION_CREDENTIALS=./../../kyna_pro_key.json');
return [
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'language' => 'vi',
    'bootstrap' => 'devicedetect',
    'timeZone' => 'Asia/Ho_Chi_Minh',
    'components' => [
        'piwik' => [
            'class' => 'common\components\PiwikComponent'
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'urlManager' => [
            'class' => 'yii\web\UrlManager',
            'showScriptName' => false,
            'enablePrettyUrl' => true,
            'baseUrl' => '/',
        ],
        'authManager' => [
            'class' => 'dektrium\rbac\components\DbManager',
            'cache' => 'cache'
        ],
        'assetManager' => [
            'linkAssets' => true,
        ],
        'formatter' => [
            'dateFormat' => 'php:d/m/Y',
            'datetimeFormat' => 'php:d/m/Y G:i',
            'timeFormat' => 'php:G:i:s',
            'locale' => 'vi_VN',
            'defaultTimeZone' => 'Asia/Ho_Chi_Minh',
            'nullDisplay' => 'N/A',
            'currencyCode' => 'VND',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@common/mail',
            'useFileTransport' => false,
            'transport' => [
                'class' => 'Swift_SendmailTransport',
            ],
            'messageConfig' => [
                'charset' => 'UTF-8',
                'from' => ['hotro@kyna.vn' => 'Hỗ trợ Kyna.vn']
            ]
        ],
        'i18n' => [
            'translations' => [
                'yii' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@common/messages'
                ],
                'kvtree' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@common/messages'
                ],
            ],
        ],
        'devicedetect' => [
            'class' => 'alexandernst\devicedetect\DeviceDetect'
        ],
        'logger' => [
            'class' => 'common\components\GoogleStackDriverLog',

        ]
    ],
    'on beforeRequest' => function () {
        if (Yii::$app->request->isAjax) {
            return;
        }
        $pathInfo = Yii::$app->request->pathInfo;
        $query = Yii::$app->request->queryString;
        if (!empty($pathInfo) && substr($pathInfo, -1) === '/') {
            $url = '/' . substr($pathInfo, 0, -1);
            if ($query) {
                $url .= '?' . $query;
            }
            Yii::$app->response->redirect($url, 301);
            Yii::$app->end();
        }
    },
];
