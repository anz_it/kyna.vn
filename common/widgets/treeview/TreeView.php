<?php

namespace common\widgets\treeview;

use common\helpers\TreeHelper;

class TreeView extends \yii\base\Widget
{
    public $dataProvider;

    public $listType = 'ol';
    public $beforeList = false;
    public $afterList = false;
    public $beforeItem = false;
    public $afterItem = false;
    public $itemHtml = false;
    public $itemWrapper = '<span>%s</span>';

    public $cssClass = false;
    public $childClass = false;
    public $itemClass = false;
    public $handleClass = false;
    public $collapsible = false;
    public $collapsed = false;
    public $sortable = false;

    //public $urlPattern  = false;

    public $idKey = 'id';
    public $slugKey = 'slug';
    public $parentKey = 'parent_id';
    public $labelKey = 'name';

    private $_treeData = [];
    private $_tree = [];

    public function init()
    {
        parent::init();
        if ($this->dataProvider === null) {
            throw new InvalidConfigException('The "dataProvider" property must be set.');
        }

        //$this->_treeData
        $models = $this->dataProvider->getModels();
        $this->_treeData = TreeHelper::dataMapper($models, $this->idKey, $this->parentKey, $this->labelKey);
    }

    public function run()
    {
        $tree = TreeHelper::createTree($this->_treeData);
        $this->_tree = $tree;

        $this->_functionCall($this->beforeList);

        $class = !empty($this->cssClass) ? ' class="'.$this->cssClass.'"' : '';
        $this->_renderTree($tree, $class);

        $this->_functionCall($this->afterList);

        $view = $this->getView();
        TreeViewAsset::register($view);
    }

    private function _renderTree($list, $class = '')
    {
        echo '<', $this->listType, $class, '>';
        foreach ($list as $branch) {
            $itemClass = !empty($this->itemClass) ? ' class="'.$this->itemClass.'"' : '';
            echo '<li', $itemClass, '>';

            if ($this->sortable) {
                $handleClass = !empty($this->handleClass) ? ' class="'.$this->handleClass.'"' : ' class="list-item-handle"';
                echo '<i', $handleClass, '></i>';
            }

            $itemString = $this->_functionCall($this->beforeItem, $branch);

            if (!$this->itemHtml) {
                $itemString .= $branch[$this->labelKey];
            } else {
                $itemString .= $this->_functionCall($this->itemHtml, $branch);
            }

            //echo $itemString;//$branch[$this->labelKey];

            $itemString .= $this->_functionCall($this->afterItem, $branch);
            //var_dump($itemString);

            printf($this->itemWrapper, $itemString);

            if (!$this->_isLeaf($branch)) {
                $class = !empty($this->childClass) ? ' class="'.$this->childClass.'"' : '';
                $this->_renderTree($branch['_children'], $class);
            }
            echo '</li>';
        }
        echo '</', $this->listType, '>';
    }

    private function _functionCall($function, $params = null)
    {
        if (!$function) {
            return;
        }
        if (is_string($function)) {
            return $function;
        }

        if (is_callable($function)) {
            return call_user_func($function, $params);
        }
    }

    private function _isLeaf($branch)
    {
        return !isset($branch['_children']);
    }
}
