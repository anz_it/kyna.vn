<?php

namespace common\widgets;

use Yii;
use yii\bootstrap\Widget;
use yii\bootstrap\Html;

class BtnAddWidget extends Widget
{

    public $options = [
        'class' => 'btn btn-success'
    ];

    public $url = ['create'];

    public $btnText = '';

    public function run()
    {
        $this->btnText = '<i class="fa fa-fw fa-plus"></i> ' . Yii::t('app', 'Thêm mới');
        return Html::a($this->btnText, $this->url, $this->options);
    }
}