<?php
namespace common\widgets\codeeditor;

use yii\web\AssetBundle;

class CodeEditorAsset extends AssetBundle
{
    public $sourcePath = '@bower/codemirror';
    public $js = [
        'lib/codemirror.js',
        'addon/hint/show-hint.js',
        'addon/hint/html-hint.js',
        'addon/hint/xml-hint.js',
        'mode/xml/xml.js',
        'mode/javascript/javascript.js',
        'mode/css/css.js',
        'mode/htmlmixed/htmlmixed.js'
    ];
    public $css = [
        'lib/codemirror.css',
        'addon/hint/show-hint.css',
    ];
}
