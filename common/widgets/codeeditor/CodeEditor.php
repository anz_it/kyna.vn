<?php

namespace common\widgets\codeeditor;

use yii\bootstrap\Html;

class CodeEditor extends \yii\widgets\InputWidget {
    private $_language = 'htmlmixed';

    public $language;
    public $theme = 'default';

    public static function supportedLanguages() {
        return ['htmlmixed', 'javascript', 'php', 'css', 'markdown', 'text/html'];
    }

    protected function registerClientScript() {
        $id = $this->options['id'];
        $view = $this->getView();
        $langAssetBundle = CodeEditorAsset::register($view);

        if (!in_array($this->language, static::supportedLanguages())) {
            $this->language = $this->_language;
        }

        $langAssetBundle->js[] = 'mode/'.$this->language.'/'.$this->language.'.js';

        $js = 'CodeMirror.fromTextArea(document.getElementById("'.$id.'"), {
            lineNumbers: true,
            mode: "'.$this->language.'",
            theme: "'.$this->theme.'",
            extraKeys: {"Ctrl-Space": "autocomplete"},
        });';
        $view->registerJs($js);
    }

    public function run()
    {
        if ($this->hasModel()) {
            echo Html::activeTextarea($this->model, $this->attribute, $this->options);
        } else {
            echo Html::textarea($this->name, $this->value, $this->options);
        }
        $this->registerClientScript();
    }
}
