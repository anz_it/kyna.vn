<?php

namespace common\widgets\tinymce;

class TinyMce extends \dosamigos\tinymce\TinyMce {

    /**
     * Layout has values of 'minimal', 'basic', 'advanced', 'full'
     * @var string
     */
    public $layout = 'basic';
    public $language = 'vi';

    public function init() {
        $this->options['class'] = 'form-control';
        switch ($this->layout) {
            case 'minimal':
                $this->options['rows'] = 3;
                $this->clientOptions = [
                    'menubar' => false,
                    'toolbar' => false,
                    'statusbar' => false,
                    'resize' => false,
                    'plugins' => 'link emoticons',
                ];
                break;
            case 'advanced':
                $this->options['rows'] = 10;
                $this->clientOptions = [
                    'menubar' => false,
                    'plugins' => 'link image preview media table emoticons',
                    'toolbar' => 'formatselect bold italic underline | link unlink image media | bullist numlist blockquote table | removeformat',
                    'statusbar' => false,
                ];
                break;
            case 'full':
                $this->options['rows'] = 30;
                $this->clientOptions = [
                    'menubar' => false,
                    'plugins' => 'link image preview media table emoticons textcolor code searchreplace lists',
                    'toolbar' => 'formatselect fontsizeselect forecolor backcolor | bold italic underline strikethrough subscript superscript numlist bullist  | '.
                        'alignleft aligncenter alignright alignjustify | link unlink image media emoticons | hr | '.
                        'bullist numlist indent outdent blockquote code table | undo redo searchreplace | removeformat',
                    'statusbar' => true,
                ];
                break;
            case 'basic':
            default:
                $this->options['rows'] = 3;
                $this->clientOptions = [
                    'menubar' => false,
                    'toolbar' => 'bold italic underline | link',
                    'statusbar' => false,
                    'resize' => false,
                    'plugins' => 'link image emoticons',
                ];
        }
        parent::init();
    }

}
