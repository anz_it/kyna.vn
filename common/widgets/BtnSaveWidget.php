<?php

namespace common\widgets;

use Yii;
use yii\bootstrap\Widget;
use yii\bootstrap\Html;

class BtnSaveWidget extends Widget
{

    public $model = null;

    public function run()
    {
        return Html::submitButton(Yii::t('app', 'Lưu') . ' <i class="fa fa-fw fa-save"></i>', ['class' => 'btn btn-success']);
    }
}