;(function ($, _) {
    $('.select2').select2();
    $('body').on('change', '[data-cascade="location"]', function (e) {
        var target = $(this).data('target'),
            value = $(this).val(),
            locationTree = window.locationTree || {},
            compiled = _.template('<option value="<%= id %>"><%= name %></option>');

        $(target).html(compiled({
            id: '',
            name: 'Quận/Huyện'
        }));
        if (locationTree[value] !== undefined && locationTree[value]['_children']) {
            var targetData = _.values(locationTree[value]['_children']);

            _(targetData).forEach(function (item) {
                $(target).append(compiled(item));
            });
            //$(target).prop('disabled', false);
        }
        else {
            //$(target).prop('disabled', true);
        }
        // $('.select2').select2();
    });

    window.locationField = window.locationField || {};
    window.locationField.setSelection = function (locationId) {
        var location = undefined;
        $.each(locationTree, function(id, item) {
            if (item.hasOwnProperty("_children") && item._children.hasOwnProperty(locationId)) {
                location = item._children[locationId];
                return false;
            }
        });
        if (location !== undefined) {
            var $parent = $('[data-cascade="location"]').val(location.parent_id).trigger("change"),
                target = $parent.data('target');
            $(target).val(locationId);
        }
    }
})(jQuery, _);
