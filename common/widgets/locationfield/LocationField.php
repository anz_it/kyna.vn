<?php

namespace common\widgets\locationfield;

use common\helpers\TreeHelper;
use kyna\base\models\Location;
use yii\widgets\InputWidget;
use yii\bootstrap\Html;
use yii\helpers\Json;

class LocationField extends InputWidget
{
    /**
     * Example:.
     *
     * [
     * 		[
     * 			'source'      => Category::className(),
     * 			'keyValue'    => ['id', 'name'],
     * 			],
     * 			[
     * 				'source'     => Category::className(),
     * 				'keyValue'   => ['id', 'name'],
     * 				'parentKey'  => 'parent_id',
     * 			],
     * 			[
     * 				'ajax'       => true,
     * 				'source'     => Url::toRoute(['/product/get/', 'categoryId' => $categoryId]),
     * 				'keyValue'   => ['id', 'title'],
     * 				'parentKey'  => 'category_id',
     * 			],
     * ]
     *
     * @var array
     */
    public $cityId;
    public $showFieldLabels;
    public $cityLabel = 'Tỉnh/Thành phố';
    public $districtLabel = 'Quận/Huyện';
    public $fieldGroupClass = '';
    public $fieldLabelClass = '';
    public $fieldInputClass = '';
    public $wrapper = '<div class="row">%s</div>';

    public function init()
    {
        parent::init();
    }
    public function run()
    {
        $locationModels = Location::find()->all();
        $locations = TreeHelper::dataMapper($locationModels);
        $locationTree = TreeHelper::createTree($locations);

        $currentDistrictId = $this->value;
        if (isset($this->model)) {
            $currentDistrictId = Html::getAttributeValue($this->model, $this->attribute);//$this->model->{$this->attribute};
        }
        $currentCityId = 0;
        $currentCityDistricts = [];
        if (isset($locations[$currentDistrictId])) {
            $currentDistrict = $locations[$currentDistrictId];
            //var_dump($currentDistrict);die;
            $currentCityId = $currentDistrict['parent_id'];
            $currentCityDistricts = $locationTree[$currentCityId]['_children'];
        }

        $cityData = ['' => 'Tỉnh/Thành phố'];
        foreach ($locations as $key => $location) {
            if ($location['parent_id'] == 0) {
                if ($key == $currentCityId) {
                    $location['selected'] = true;
                }
                $cityData[$key] = $location['name'];
            }
        }

        $districtData = ['' => 'Quận/Huyện'];
        if ($currentDistrictId > 0) {
            foreach ($locations as $key => $location) {
                if ($location['parent_id'] == $currentCityId) {
                    if ($key == $currentDistrictId) {
                        $location['selected'] = true;
                    }
                    $districtData[$key] = $location['name'];
                }
            }
        }
        //var_dump($locations);

        $this->cityId = $currentCityId;

        //$this->options['id'] .= '-district';
        //$this->options['id'] .= 'actionform-location_id';
        $districtOptions = [
            'id' => $this->options['id'],
            'class' => 'form-control select2',
            'style' => 'width: 100%'
        ];
        if ($currentDistrictId <= 0) {
            //$districtOptions['class'] = 'disabled form-control district-id select2';
            //$districtOptions['disabled'] = 'disabled';
        }
        $cityOptions = [
            'data-target' => '#'.$this->options['id'],
            'data-cascade' => 'location',
            'class' => 'form-control select2',
            'style' => 'width: 100%'
        ];
        if ($this->hasModel()) {
            $districtList = Html::activeDropDownList($this->model, $this->attribute, $districtData, $districtOptions);
        } else {
            $districtList = Html::dropDownList($this->name, $this->value, $districtData, $districtOptions);
        }

        $cityList = Html::dropDownList('', $currentCityId, $cityData, $cityOptions);

        $view = $this->getView();
        LocationFieldAsset::register($view);
        $js = 'window.locationTree = window.locationTree || '.Json::encode($locationTree).';';
        $view->registerJs($js);

        $html = '';
        if ($this->showFieldLabels and $this->cityLabel and $this->districtLabel) {
            $html .= '<div class="'.$this->fieldGroupClass.'">';
            $html .= '  <div class="'.$this->fieldLabelClass.'">';
            $html .= '    <label class="control-label" for="'.$this->options['id'].'">'.$this->cityLabel.'</label>';
            $html .= '  </div>';
            $html .= '  <div class="'.$this->fieldInputClass.'"><span class="icon"><i class="icon-location" aria-hidden="true"></i></span>';
            $html .=      $cityList;
            $html .= '  </div>';
            $html .= '</div>';

            $html .= '<div class="'.$this->fieldGroupClass.'">';
            $html .= '  <div class="'.$this->fieldLabelClass.'">';
            $html .= '    <label class="control-label" for="'.$this->options['id'].'">'.$this->districtLabel.'</label>';
            $html .= '  </div>';
            $html .= '  <div class="'.$this->fieldInputClass.'"><span class="icon"><i class="icon-location" aria-hidden="true"></i></span>';
            $html .=      $districtList;
            $html .= '  </div>';
            $html .= '</div>';
        } else {
            $html .= '<div class="col-sm-6">';
            $html .= $cityList;
            $html .= '</div>';
            $html .= '<div class="col-sm-6">';
            $html .= $districtList;
            $html .= '</div>';
        }

        if ($this->wrapper) {
            return sprintf($this->wrapper, $html);
        } else {
            return $html;
        }
    }
}
