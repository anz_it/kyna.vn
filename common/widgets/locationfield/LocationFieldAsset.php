<?php

namespace common\widgets\locationfield;

/**
 * Asset bundle for Typeahead advanced widget.
 *
 * @author Kartik Visweswaran <kartikv2@gmail.com>
 *
 * @since 1.0
 */
class LocationFieldAsset extends \yii\web\AssetBundle
{
    public $depends = [
        //'kartik\select2\Select2Asset',
        'common\assets\LodashAsset',
        'yii\web\YiiAsset',
    ];
    //public $basePath = __DIR__;
    public $sourcePath = __DIR__;
    public $js = [
        'js/select2.min.js',
        'js/locationfield.js',
        'js/format_number.js',
    ];
    public $css = [
        'css/select2.min.css',
        'css/select2-custom.css'
    ];
    public $publishOptions = [
        'forceCopy'=>true,
    ];
}
