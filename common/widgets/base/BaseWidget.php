<?php

namespace common\widgets\base;

use yii;
use yii\base\Widget;

class BaseWidget extends Widget
{

    public $rootCats;

    public function getSettings()
    {
        return Yii::$app->controller->settings;
    }

}
