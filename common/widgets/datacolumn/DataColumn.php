<?php

namespace common\widgets\datacolumn;

use yii\bootstrap\Html;

class DataColumn extends \yii\grid\DataColumn
{
    public $control = 'static';
    public $optionSet = [];
    public $controlOptions = [];
    public $name;

    protected function renderDataCellContent($model, $key, $index)
    {
        //var_dump($key, $index);
        $value = $this->getDataCellValue($model, $key, $index);
        $formattedValue = $this->grid->formatter->format($value, $this->format);

        if (is_callable($this->optionSet)) {
            $optionSet = call_user_func($this->optionSet, $model, $key, $index);
        } else {
            $optionSet = $this->optionSet;
        }
        $control = $this->control;
        $name = explode('\\', get_class($model));
        $name = end($name);

        $inputId = $name.'-'.$this->attribute.'-'.$model->id;
        $inputName = $name.'['.$model->id.']['.$this->attribute.']';

        $options = [
            'class' => 'form-control',
            'id' => $inputId,
            'data-id' => $model->id,
        ];
        $options = array_merge($options, $this->controlOptions);
        if (substr_compare($name, '[]', -2, 2)) {
            $name .= '[]';
        }

        switch ($control) {
            case 'checkbox':
                $html = '<label for="'.$inputId.'">';
                $html .= Html::checkbox($inputName, $value !== 0, ['id' => $inputId]);

                if (is_array($optionSet) and sizeof($optionSet) and array_key_exists($value, $optionSet)) {
                    $html .= ' '.$optionSet[$value];
                }

                $html .= '</label>';

                return $html;
            case 'input':
                return Html::input('text', $inputName, $value, $options);
            case 'dropdown':
                return Html::dropdownList($inputName, $value, $optionSet, $options);
            default:
                return parent::renderDataCellContent($model, $key, $index);
        }
        /*
        if ($this->content === null) {
            return $this->grid->formatter->format($this->getDataCellValue($model, $key, $index), $this->format);
        } else {
            return parent::renderDataCellContent($model, $key, $index);
        }
        */
    }
}
