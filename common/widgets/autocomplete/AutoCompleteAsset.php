<?php

namespace common\widgets\autocomplete;

/**
 * Asset bundle for Typeahead advanced widget.
 *
 * @author Kartik Visweswaran <kartikv2@gmail.com>
 *
 * @since 1.0
 */
class AutoCompleteAsset extends \yii\web\AssetBundle
{
    public $depends = [
        'backend\assets\AdminLteAsset',
    ];
    //public $basePath = __DIR__;
    public $sourcePath = __DIR__;
    public $js = [
        //'js/lodash.min.js',
        'js/typeahead.bundle.min.js',
    ];
    public $css = [
        'css/typeahead.css',
    ];
}
