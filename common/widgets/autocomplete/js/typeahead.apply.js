;(function ($) {
    var $control = $('[data-autocomplete="typeahead"]'),
        data = $control.data(),
        bloodhoundData = data['bloodhound'];
        initData = [],
        engine;

    if (bloodhound !== null && bloodhound !== undefined) {
        bloodhound = new Bloodhound(bloodhoundData);
    }

    $control.typeahead(data);
})(jQuery);
