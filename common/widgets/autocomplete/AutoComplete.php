<?php
/**
 * @link http://www.yiiframework.com/
 *
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace common\widgets\autocomplete;

use yii\base\InvalidConfigException;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\web\JsExpression;

class AutoComplete extends \yii\widgets\InputWidget
{
    /**
     * The name of the jQuery plugin to use for this widget.
     */
    const PLUGIN_NAME = 'typeahead';

    public $options = ['class' => 'form-control'];
    public $type = 'text';
    public $placeholder;

    public $clientOptions = [];
    public $dataset = [];
    public $bloodhound = [];
    public $on = [];

    protected $_hashVar;
    private $_events = [
        'typeahead:active',
        'typeahead:idle',
        'typeahead:open',
        'typeahead:close',
        'typeahead:change',
        'typeahead:render',
        'typeahead:select',
        'typeahead:autocomplete',
        'typeahead:cursorchange',
        'typeahead:asyncrequest',
        'typeahead:asynccancel',
        'typeahead:asyncreceive',
    ];

    /**
     * Initializes the widget.
     *
     * @throws InvalidConfigException if the "mask" property is not set.
     */
    public function init()
    {
        parent::init();
    }

    /**
     * {@inheritdoc}
     */
    public function run()
    {
        $this->registerClientScript();
        if ($this->hasModel()) {
            echo Html::activeInput($this->type, $this->model, $this->attribute, $this->options);
        } else {
            echo Html::input($this->type, $this->name, $this->value, $this->options);
        }
    }

    /**
     * Initializes client options.
     */
    protected function initClientOptions()
    {
        $options = $this->clientOptions;
        $options['data-autocomplete'] = 'typeahead';
        $this->clientOptions = $options;

        $on = $this->on;
        foreach ($on as $event => $eventData) {
            if (!array_key_exists($event, $on)) {
                unset($on['event']);
            }
        }
        $this->on = $on;

        $dataset = $this->dataset;
        if (is_array($this->bloodhound)) {
            $dataset['source'] = new JsExpression('engine');
        }
        $this->dataset = $dataset;
    }

    /**
     * Registers the needed client script and options.
     */
    public function registerClientScript()
    {
        $view = $this->getView();
        $this->initClientOptions();

        $id = $this->options['id'];
        $clientOptions = Json::encode($this->clientOptions);
        $dataset = Json::encode($this->dataset);
        $bloodhound = Json::encode($this->bloodhound);
        $on = $this->on;
        //$key = (empty($dataset['itemKey'])) ? 'id' : $dataset['itemKey'];

        $js = '';
        if (!empty($this->bloodhound)) {
            $js .= "var engine = new Bloodhound($bloodhound);";
        }

        $plugin = self::PLUGIN_NAME;
        $js .= "$('#$id').$plugin($clientOptions";
        if (!empty($dataset)) {
            $js .= ', '.$dataset;
        }
        $js .= ')';

        foreach ($on as $event => $eventData) {
            $js .= ".on('$event', function(obj, datum, name) { $eventData });";
        }

        /*
        $js .= '.on("typeahead:select", function(e, item) {
            var that = $(e.target),
                compiled  = _.template( that.data("result-template") );
            that.val("");
            $(that.data("target")).append(compiled(item));
        })';
        */
        $js .= ';';
        AutoCompleteAsset::register($view);
        $view->registerJs($js);
    }
}
