<?php

namespace common\widgets\upload;

use Yii;
use yii\web\Response;
use yii\web\UploadedFile;
use common\widgets\lib\Upload;

class UploadController extends \yii\web\Controller {
    public $uploadParamName = 'files';
    public $uploadRoot = '@upload';
    public $uploadRootUrl = '/uploads';

    private $_uploadAdapter;

    public init() {
        parent::init();
        $this->_uploadAdapter = new Upload();
        $this->_uploadAdapter->uploadRoot = $this->uploadRoot;
        $this->_uploadAdapter->uploadRootUrl = $this->uploadRootUrl;
    }

    public function actionDoUpload() {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $files = UploadedFile::getInstancesByName($this->uploadParamName);
        $result = [];
        foreach ($files as $file) {
            $result[] = $this->_uploadAdapter->upload($file);
        }

        if (!empty($result)) {
            Yii::$app->session->set('lastUploadFiles', $result);
        }

        return array_values($result);
    }

    public function actionLastUploadFiles() {
        Yii::$app->response->format = Response::FORMAT_JSON;
        return Yii::$app->session->get('lastUploadFiles');
    }
}
