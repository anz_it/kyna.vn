<?php

namespace common\widgets\upload\lib;

use Yii;

class Upload
{
    public $uploadRoot = '@upload';
    public $uploadRootUrl = '/uploads';

    public $symlinkDirs = [
        '@frontend',
        '@backend'
    ];

    public function __construct()
    {
    }

    public static function rmdir($dir)
    {
        $files = array_diff(scandir($dir), ['.', '..']);
        foreach ($files as $file) {
            (is_dir("$dir/$file")) ? self::rmdir("$dir/$file") : unlink("$dir/$file");
        }
        return rmdir($dir);
    }

    public function getFileName($file)
    {
        return md5($file->baseName) . '.' . $file->extension;
    }

    public function getUploadPath($relativeDir)
    {
        return Yii::getAlias($this->uploadRoot . $relativeDir);
    }

    public function getUploadUrl($relativeDir)
    {
        return Yii::getAlias($this->uploadRootUrl . $relativeDir);
    }

    public function upload($file, $dir = '')
    {
        $saveDir = $this->getUploadPath($dir);
        $filename = $this->getFileName($file);

        if (!file_exists($saveDir) and !is_dir($saveDir)) {
            mkdir($saveDir, 0777, true);
        }
        $savePath = $saveDir . '/' . $filename;
        if (!$file->saveAs($savePath)) {
            return false;
        }

        $savedUrl = $this->getUploadUrl($dir) . '/' . $filename;
        return $savedUrl;
    }
}
