<?php
use common\helpers\CDNHelper;
?>
<label class="input-group file-upload" for="<?= $id ?>" id="<?= $id ?>-wrapper"
       data-file-upload
    <?= $uploadUrl ? 'data-upload-url="'.$uploadUrl.'"' : ''?>
    <?= $lastUploadUrl ? 'data-last-upload-url="'.$lastUploadUrl.'"' : '' ?>>
    <?php if ($display == 'image') : ?>
        <?php $imgSrc = CDNHelper::getMediaLink() . '/' . $img; ?>
        <span class="input-group-image input-group-addon">
            <img src="<?= $imgSrc ?>" class="file-upload-img" data-img-large="<?= $imgSrc ?>" title="<?= $img ?>">
        </span>
    <?php endif; ?>
    <?= $input ?>
    <span class="form-control file-upload-text" style="word-wrap: break-word;"><?= Yii::t('yii', 'Please choose file') ?></span>
    <span class="input-group-btn">
        <button class="btn btn-default file-upload-btn" type="button"><?= Yii::t('yii', 'Choose file…!') ?></button>
    </span>
</label>
