<?php

namespace common\widgets\upload;

class UploadRequiredValidator extends \yii\validators\Validator
{
    public $skipOnEmpty = false;
    public $message = "Bạn phải chọn {attribute}";
    public $enableClientValidation = false;

    public function validateAttribute($model, $attribute)
    {
        //die($model->$attribute);
        if (empty($model->$attribute)) {
            $this->addError($model, $attribute, $this->message);
        }
    }
}
