<?php
namespace common\widgets\upload;

use yii\web\AssetBundle;

class UploadAsset extends AssetBundle
{
    public $sourcePath = __DIR__.'/dist';
    public $js = [
        'js/jquery.dotdotdot.min.js',
        //'js/trunk8.js',
        'js/upload.js?v=1',
    ];
    public $css = [
        'css/upload.css',
    ];
    public $depends = [
        'yii\bootstrap\BootstrapPluginAsset',
    ];
}
