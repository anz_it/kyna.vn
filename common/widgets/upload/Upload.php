<?php
namespace common\widgets\upload;

use yii\bootstrap\Html;

class Upload extends \yii\widgets\InputWidget
{
    public $autoUploadUrl = false;
    public $lastUploadUrl = false;
    public $multiple = false;
    public $accept = 'image/*';
    public $display = false;

    public $viewPath = 'default';

    public $inputOptions = [];

    public function init() {
        parent::init();
        if (empty($this->options['class'])) {
            $this->options['class'] = '';
        }
        if (!empty($this->accept)) {
            $this->inputOptions['accept'] = $this->accept;
        }
        if ($this->multiple === true) {
            $this->inputOptions['multiple'] = true;
        }
    }

    protected function registerClientScript() {
        $view = $this->getView();
        $assetBundle = UploadAsset::register($view);
    }

    public function run()
    {
        $this->registerClientScript();

        $id = $this->options['id'];
        $this->inputOptions['id'] = $id;
        $this->inputOptions['class'] = 'file-upload-input';

        if ($this->hasModel()) {
            $input = Html::activeFileInput($this->model, $this->attribute, $this->inputOptions);
            $value = $this->model->{$this->attribute};
        } else {
            $input = Html::fileInput($this->name, $this->value, $this->inputOptions);
            $value = $this->value;
        }

        $img = false;
        if ($this->display == 'image') {
            $img = $value;
        }

        return $this->render($this->viewPath, [
            'img' => $img,
            'id' => $id,
            'input' => $input,
            'uploadUrl' => $this->autoUploadUrl,
            'lastUploadUrl' => $this->lastUploadUrl,
            'display' => $this->display
        ]);
    }
}
