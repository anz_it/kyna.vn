+function($) {
    'use strict';

    var Upload = function(element, options) {
        this.$element = $(element),
            this.$text = this.$element.find(".file-upload-text"),
            this.$input = this.$element.find(".file-upload-input"),
            this.$inputHidden = this.$element.find("input[type='hidden']"),
            this.$button = this.$element.find(".file-upload-btn"),
            this.$img = this.$element.find(".file-upload-img");

        this.uploadUrl = this.$element.data('upload-url');

        var isAutoUpload = (this.uploadUrl != undefined && this.uploadUrl != null);

        if (isAutoUpload) {
            this.initAutoUpload();
        }

        this.$element.on("change", this.$input, function (e) {
            var control = e.delegateTarget,
                value = this.value;

            Plugin.call($(control), "updateHiddenInput");

            if (isAutoUpload) {
                Plugin.call($(control), "doUpload");
            }
            else {
                Plugin.call($(control), "replaceText");
                Plugin.call($(control), "replaceImg");
            }
        });

        this.init();
    };

    Upload.prototype.init = function () {
        this.$text.dotdotdot({
            ellipsis: '…',
            wrap: 'letter',
            watch: true
        });

        this.$img.popover({
            trigger: 'hover',
            content: function() {
                return '<img src="' + $(this).data("img-large") + '">';
            },
            html: true,
            placement: 'right',
            delay: 300,
            template: '<div class="popover" role="tooltip"><div class="arrow"></div><div class="popover-content"></div></div>'
        });
    };

    Upload.prototype.initAutoUpload = function () {
        var iFrameId = this.$element.attr("id") + '-iframe',
            csrfParam = $(document).find('[name="csrf-param"]').attr("content"),
            csrfToken = $(document).find('[name="csrf-token"]').attr("content");

        this.$form = this.$element.find(".file-upload-form");
        this.$iframe = this.$element.find(".file-upload-iframe");

        if (this.$form.length === 0) {
            if (this.$iframe.length === 0) {
                this.$iframe = $("<iframe>").attr({
                    "id": iFrameId,
                    "height": 0,
                    "width": 0,
                    "frameborder": 0,
                    "name": iFrameId,
                    "style": "display:none"
                }).addClass("file-upload-iframe").insertBefore(this.$input)
            }

            this.$form = $("<form>").attr({
                "action": this.uploadUrl,
                "method": "post",
                "enctype": "multipart/form-data",
                "target": iFrameId
            }).addClass("file-upload-form");
            this.$input.wrap(this.$form);

            this.$csrfTokenInput = $('<input>').attr({
                'type': 'hidden',
                'name': csrfParam
            }).val(csrfToken).insertBefore(this.$input);

            this.$submitButton = $("<button>").attr({
                'type': 'submit',
                'style': "display:none",
                'class': 'hide',
            }).insertAfter(this.$input);

            var $control = this.$element
            this.$iframe.on("load", function(e) {
                Plugin.call($control, "updateFiles");
            });
        }
    };

    Upload.prototype.updateHiddenInput = function() {
        var value = this.$input.val();
        this.$inputHidden.val(value);
    };

    Upload.prototype.replaceText = function() {
        var value = this.$input.val();
        this.$text.text(value)
    };

    Upload.prototype.replaceImg = function() {
        var that = this;
        var input = that.$input.get(0);
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                that.$img.attr('src', e.target.result);
                that.$img.data("img-large", e.target.result);
            };
            reader.readAsDataURL(input.files[0]);
        }
    };

    Upload.prototype.doUpload = function () {
        this.$submitButton.click();
    };

    Upload.prototype.updateFiles = function () {
        var url = this.$element.data("last-upload-url"),
            that = this;

        $.ajaxSetup({cache: false})
        $.getJSON(url, function(resp) {
            that.uploadedFiles = resp;
            //console.log(resp, that.$img)
            if (resp.length > 0 && that.$img != undefined && that.$img.length > 0) {
                var img = resp[resp.length-1];
                that.$img.attr("src", img);
                that.$img.data("img-large", img);
            }
        });
        $.ajaxSetup({cache: true});
    };

    Upload.VERSION = '1.0.0';

    Upload.DEFAULTS = {
        'uploadUrl': false,
    };

    function Plugin(option, _relatedTarget) {
        return this.each(function() {
            var $this = $(this)
            var data = $this.data('k.upload')
            var options = $.extend({}, Upload.DEFAULTS, $this.data(), typeof option == 'object' && option)

            if (!data) $this.data('k.upload', (data = new Upload(this, options)))
            if (typeof option == 'string') data[option](_relatedTarget)
        })
    }

    var old = $.fn.upload;

    $.fn.upload = Plugin;
    $.fn.upload.Constructor = Upload;

    $.fn.upload.noConflict = function() {
        $.fn.upload = old;
        return this
    };

    $(window).on("load", function() {
        $('[data-file-upload]').each(function() {
            var $uploader = $(this);
            Plugin.call($uploader, $uploader.data());
        });
    });
}(jQuery);
