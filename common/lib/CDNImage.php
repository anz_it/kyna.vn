<?php

namespace common\lib;

use kyna\servicecaller\traits\CurlTrait;
use Yii;

class CDNImage
{
    use CurlTrait;

    private $_apiUrl = 'https://static.kyna.vn';
    private $_apiSecretKey = 'kyna@2017@2ff4b8b2fdbaeef4528f742516b28cf4';

    protected static $defaultMethod = 'POST';

    public $uploadRootUrl = '/uploads';

    public $model;
    public $attribute;
    public $tableName;
    public $type = 'img';

    public function __construct($model = null, $attribute = null, $type = null)
    {
        if (isset(Yii::$app->params['static_link'])) {
            $this->_apiUrl = Yii::$app->params['static_link'];
        }
        if ($model) {
            $this->model = $model;
            $this->attribute = $attribute;

            if (!$this->tableName) {
                $this->tableName = $model->tableSchema->name;
            }
        }
        if (!is_null($type)) {
            $this->type = $type;
        }
    }

    protected function beforeCall(&$ch, &$data)
    {
        $headers = array();
        $headers[] = 'Content-Type: multipart/form-data';
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        if (!$data or !is_array($data)) {
            $data = [];
        }
    }

    protected function afterCall(&$response)
    {
        $response = json_decode($response, true);
    }

    public function getFileName($file)
    {
        return $this->attribute . '-' . time() . '.' . $file->extension;
    }

    public function getUploadUrl()
    {
        return Yii::getAlias($this->uploadRootUrl . '/' . $this->tableName . '/' . $this->model->id . '/' . $this->type);
    }

    public function upload($file)
    {
        $filename = $this->getFileName($file);
        $uploadPath = $this->getUploadUrl();
        $command = '';
        $params = [
            'secret' => $this->_apiSecretKey,
            'action' => 'upload',
            'file' => curl_file_create($file->tempName, $file->type, $filename),
            'file_name' => $filename,
            'upload_path' => $uploadPath,
            'size' => !empty($this->model->imageSize) ? json_encode($this->model->imageSize) : json_encode([])
        ];
        $endpoint = $this->_buildUrl($command);
        $result = $this->call($endpoint, $params);
        if (isset($result['status']) && $result['status'] == true) {
            return $result['uploaded_url'];
        }
        return false;
    }

    public function resize($originalFile, $size = [], $fixedRatio = false, $forceCreate = false)
    {
        $command = '';
        $params = [
            'secret' => $this->_apiSecretKey,
            'action' => 'resize',
            'originalFile' => $originalFile,
            'size' => json_encode($size),
            'fixedRatio' => $fixedRatio,
            'forceCreate' => $forceCreate
        ];
        $endpoint = $this->_buildUrl($command);
        $result = $this->call($endpoint, $params);
        if (isset($result['status'])) {
            return $result['status'];
        }
        return false;
    }

    private function _buildUrl($command, $params = false)
    {
        $url = $this->_apiUrl . $command;
        if ($params) {
            $url .= '?' . http_build_query($params);
        }

        return $url;
    }
}
