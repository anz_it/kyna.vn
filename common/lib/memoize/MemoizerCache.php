<?php
namespace common\lib\memoize;

interface MemoizerCache
{
    function get($key, callable $callback = null);

    function set($key, $value);
}