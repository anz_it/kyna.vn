<?php
/**
 * Created by PhpStorm.
 * User: khanhphan
 * Date: 4/18/17
 * Time: 11:43 AM
 */

namespace common\elastic;

use common\helpers\ElasticHelper;
use Elasticsearch\Client;
use kyna\course\models\Category;
use kyna\course\models\Teacher;
use kyna\course_combo\models\CourseCombo;
use yii\helpers\Json;
use kyna\course\models\Course as BaseCourse;

/**
 * Class Course
 * @package common\elastic
 * @property integer $id
 * @property string $name
 * @property string $short_name
 * @property integer $level
 * @property integer $price
 * @property integer $price_discount
 * @property double $percent_discount
 * @property string $slug
 * @property string $image_url
 * @property string $video_url
 * @property string $video_cover_image_url
 * @property double $total_time
 * @property integer $category_id
 * @property integer $course_commission_type_id
 * @property integer $teacher_id
 * @property integer $status
 * @property integer $created_time
 * @property integer $updated_time
 * @property integer $is_hot
 * @property integer $is_new
 * @property integer $partner_id
 * @property integer $is_disable_seeding
 * @property float $rating
 * @property integer $rating_users_count
 * @property integer $total_users_count
 * @property boolean $is_campaign_1_1
 */
class Course extends Base
{
    public static function getAlias()
    {
        return \Yii::$app->params['elasticsearch_name'];
    }

    public static function getIndex()
    {
        return 'courses';
    }

    public static function getType()
    {
        return 'page';
    }

    public static function getMappingQuery()
    {
        $json = file_get_contents(__DIR__."/mapping/course.json");

        return $json;
    }

    public static function update($course, $refresh = true)
    {
        $query = self::getIndexQuery($course);
        /** @var Client $client */
        $client = \Yii::$app->elasticsearch->client;

        $client->index([
            'index' => self::getAlias(),
            'type' => self::getType(),
            'refresh' => $refresh,
            'body' => $query,
            'id' => $course->id
        ]);
    }

    /**
     * @param $course \kyna\course\models\Course
     * @return array
     */
    public static function getIndexQuery($course)
    {
        $tags = $course->getTagsObjects();
        $tagsText = implode(", ", array_map(function ($p) {return $p->tag;}, $tags));
        $fullText = $course->description. "," . $tagsText;
        $fullTextBoosted = $course->name;

        //$fullTextNoSign = StringHelper::removeSign($fullText);
        //$fullTextBoostedNoSign = StringHelper::removeSign($fullTextBoosted);

        $discountPercent = $course->getDiscountPercent();
        $discountAmount = $course->getDiscountAmount();
        $category = $course->category;
        if ($category == null)
            $category = new Category();
        $teacher = $course->teacher;
        if ($teacher == null) {
            $teacher = new Teacher();
        }
        $numberFacets = [];
        $stringFacets = [];
        if ($course->price == 0 && $course->type == 1) {
            $stringFacets[] = ['facet-name' => 'discount', 'facet-value' => '0']; // Miễn phí
        }
        if ($discountAmount > 0) {
            $stringFacets[] = ['facet-name' => 'discount', 'facet-value' => '1']; // Khuyến mãi
        }
        if ($course->is_hot) {
            $stringFacets[] = ['facet-name' => 'hot', 'facet-value' => '0']; // Hot
        }
        if ($course->is_new) {
            $stringFacets[] = ['facet-name' => 'hot', 'facet-value' => '1']; // New
        }

        $stringFacets[] = ['facet-name' => 'level', 'facet-value' => $course->level];
        if ($course->type != BaseCourse::TYPE_COMBO) {
            $stringFacets[] = ['facet-name' => 'type', 'facet-value' => $course->type];
        }

        $totalTime = $course->getTotalTime();
        if ($totalTime < 3*3600) {
            $stringFacets[] = ['facet-name' => 'time', 'facet-value' => '0']; // dưới 3 tiếng
        }
        elseif ($totalTime < 24*3600 && $totalTime >= 3*3600) {
            $stringFacets[] = ['facet-name' => 'time', 'facet-value' => '1']; // 3 - 24 tiếng
        }
        elseif ($totalTime >= 24*3600 && $totalTime <= 24*3*3600) {
            $stringFacets[] = ['facet-name' => 'time', 'facet-value' => '2']; // 1 - 3 ngày
        } else {
            $stringFacets[] = ['facet-name' => 'time', 'facet-value' =>  '3']; // trên 3 ngày
        }

        if ($course->is_feature) {
            $numberFacets[] = ['facet-name' => 'is_feature', 'facet-value' => 1];
        }
        $numberFacets[] = ['facet-name' => 'status', 'facet-value' => $course->status];
        $numberFacets[] = ['facet-name' => 'type', 'facet-value' => $course->type];
        $numberFacets[] = ['facet-name' => 'price', 'facet-value' => $course->getSellPrice()];

        $cat = $course->category;
        while ($cat != null) {
            $numberFacets[] = ['facet-name' => 'category', 'facet-value' => $cat->id];
            $cat = $cat->parent;
        }

        // get teacher count
        $teacherCount = 1;
        $totalUserCount = 0;
        if ($course->type == BaseCourse::TYPE_COMBO) {
            $comboModel = CourseCombo::findOne($course->id);
            $teacherCount = $comboModel->teacherCount;
            $totalUserCount = $comboModel->total_users_count;
        }
        else{
            $totalUserCount = $course->total_users_count;
        }

        return [
            'search_result_data' => [
                'id' => $course->id,
                'name' => $course->name,
                'description' => $course->description,
                'image_url' => $course->image_url,
                'level' => $course->level,
                'short_name' => $course->short_name,
                'slug' => $course->slug,
                'price' => $course->price,
                'price_discount' => $course->price_discount,
                'video_url' => $course->video_url,
                'video_cover_image' => $course->video_cover_image_url,
                'total_time_text' => $course->getTotalTimeText(),
                'total_time' => $totalTime,
                'category_id' => $category->id,
                'category_name' => $category->name,
                'teacher_name' => $teacher->profile ? $teacher->profile->name : '',
                'teacher_email' => $teacher->email,
                'teacher_avatar' => $teacher->getAvatarImage(),
                'teacher_title' => $teacher->title,
                'teacher_count' => $teacherCount,
                'is_feature' => $course->is_feature,
                'sell_price' => $course->getSellPrice(),
                'discount_percent' => $discountPercent,
                'url' => "/".$course->slug,
                'discount_amount' => $discountAmount,
                'old_price' => $course->getOldPrice(),
                'is_hot' => $course->is_hot,
                'is_new' => $course->is_new,
                'status' => $course->status,
                'type' => $course->type,
                'combo_item_count' => $course->getComboItems()->count(),
                //Add more field to display here
                'rating' => $course->rating,
                'rating_users_count' => $course->rating_users_count,
                'is_disable_seeding' => $course->is_disable_seeding,
                'total_users_count' => $totalUserCount,
                'is_campaign_1_1' => $course->isCampaign11,
                'is_campaign_group_discount' => $course->isCampaignGroupDiscount,
                'is_birthday_discount' => $course->isBirthdayDiscount
            ],
            'search_data' => [
                [
                    'full_text' => $fullText,
                    //'full_text.no-sign' => $fullTextNoSign,
                    'full_text_boosted' => $fullTextBoosted,
                    //'full_text_boosted.no-sign' => $fullTextBoostedNoSign,
                    'string_facet' => $stringFacets,
                    'number_facet' => $numberFacets,

                    //FIXME: DO NOT add any field in this place, use number_facet and string_facet instead
                ]
            ],
            'tag' => array_map(function ($p) { return ['slug' => trim($p->slug), 'name' => trim($p->tag), 'is_desktop' => "{$p->is_desktop}", 'is_mobile' => "{$p->is_mobile}"]; }, $tags),
            'suggestion_terms' => [
                $course->name, $category->name
            ],
            'number_sort' => [
                'created_time' => $course->created_time,
                'updated_time' => $course->updated_time,
                'is_hot' => (int)$course->is_hot,
                'is_new' => (int)$course->is_new,
                'discount_percent' =>  $discountPercent,
                'discount_amount' => $discountAmount
                // Add more number to sort here
            ],
            "completion_terms" => [
                $course->name ? $course->name : '',
                $teacher->profile ? $teacher->profile->name : '',
            ],
        ];
    }

    public static function getFeatureCourse($size = 8)
    {
        $query = [
            'from' => 0,
            'size' => $size,
            'sort' => [
                ['number_sort.updated_time' => 'desc'],
                '_score',
            ],
            '_source' => ["search_result_data"],
            'query' => [
                'nested' => [
                    'path' => 'search_data',
                    'query' => [
                        'bool' => [
                            'must' => [
                                [
                                    'nested' => [
                                        'path' => 'search_data.number_facet',
                                        'query' => [
                                            'bool' => [
                                                'must' => [
                                                    [
                                                        'match' => ['search_data.number_facet.facet-name' => 'is_feature']
                                                    ],
                                                    [
                                                        'match' => ['search_data.number_facet.facet-value' => 1]
                                                    ]

                                                ]
                                            ]
                                        ]
                                    ]
                                ],
                                [
                                    'nested' => [
                                        'path' => 'search_data.number_facet',
                                        'query' => [
                                            'bool' => [
                                                'must' => [
                                                    [
                                                        'match' => ['search_data.number_facet.facet-name' => 'status']
                                                    ],
                                                    [
                                                        'match' => ['search_data.number_facet.facet-value' => 1]
                                                    ]

                                                ]
                                            ]
                                        ]
                                    ]
                                ],
                            ]
                        ]
                    ]
                ]
            ]
        ];
        $elasticResult = ElasticHelper::sendRequest([
            'index' => self::getAlias(),
            'body' => $query
        ]);
        $results = self::buildResult($elasticResult);
        return $results['data'];
    }


    public static function getListCourse($search = [], $offset = 0, $limit = null)
    {
        $must = [
            [
                'nested' => [
                    'path' => 'search_data.number_facet',
                    'query' => [
                        'bool' => [
                            'must' => [
                                [
                                    'match' => ['search_data.number_facet.facet-name' => 'status']
                                ],
                                [
                                    'match' => ['search_data.number_facet.facet-value' => 1]
                                ]

                            ]
                        ]
                    ]
                ]
            ]
        ];
        if (isset($search['q']) && trim($search['q']) != '') {
            $must[] = [
                'multi_match' => [
                    'fields' => [
                        "search_data.full_text_boosted^7",
                        "search_data.full_text_boosted.no-sign^6",
                        "search_data.full_text^2",
                        "search_data.full_text.no-sign^1"
                    ],
                    'type' => 'cross_fields',
                    'query' => $search['q']
                ]
            ];
        }

        if (isset($search['catId'])) {
            $must[] = [
                'nested' => [
                    'path' => 'search_data.number_facet',
                    'query' => [
                        'bool' => [
                            'must' => [
                                [
                                    'match' => ['search_data.number_facet.facet-name' => 'category']
                                ],
                                [
                                    'match' => ['search_data.number_facet.facet-value' => $search['catId']]
                                ]

                            ]
                        ]
                    ]
                ]
            ];
        }

        if (isset($search['course_type'])) {
            $must[] = [
                'nested' => [
                    'path' => 'search_data.number_facet',
                    'query' => [
                        'bool' => [
                            'must' => [
                                [
                                    'match' => ['search_data.number_facet.facet-name' => 'type']
                                ],
                                [
                                    'match' => ['search_data.number_facet.facet-value' => $search['course_type']]
                                ]

                            ]
                        ]
                    ]
                ]
            ];
        }

        $facets = [];
        if (isset($search['facets'])) {
            foreach ($search['facets'] as $key => $facet) {
                // String facets
                $facets[] = [
                    'nested' => [
                        'path' => 'search_data.string_facet',
                        'query' => [
                            'bool' => [
                                'must' => [
                                    [
                                        'term' => ['search_data.string_facet.facet-name' => $key]
                                    ],
                                    [
                                        'terms' => ['search_data.string_facet.facet-value' => array_values($facet)]
                                    ]

                                ]
                            ]
                        ]
                    ]
                ];
            }
        }
        $sort = [];
        if (isset($search['q']) && trim($search['q']) != '') {
            $sort[] = '_score';
        } elseif (isset($search['sort'])) {
            if ($search['sort'] == 'feature') {
                $sort[] = ['number_sort.is_hot' => 'desc'];
                $sort[] = ['number_sort.is_new' => 'desc'];
            } elseif ($search['sort'] == 'promotion') {
                $sort[] = ['number_sort.discount_percent' => 'desc'];
            }
        }
        if (empty($sort)) {
            $sort[] = ['number_sort.created_time' => 'desc'];
        }

        $parentMust = [
            [
                'nested' => [
                    'path' => 'search_data',
                    'query' => [
                        'bool' => [
                            'must' => array_merge($must, $facets)
                        ]
                    ]
                ]
            ]
        ];
        $parentAggMust = [
            [
                'nested' => [
                    'path' => 'search_data',
                    'query' => [
                        'bool' => [
                            'must' => $must
                        ]
                    ]
                ]
            ]
        ];

        if (isset($search['tag'])) {
            $q = [
                'nested' => [
                    'path' => 'tag',
                    'query' => [
                        'bool' => [
                            'must' => [
                                'match' => [
                                    'tag.slug' => $search['tag']
                                ]
                            ]
                        ]
                    ]
                ]
            ];
            $parentMust[] = $q;
            $parentAggMust[] = $q;

        }

        $query = [
            'from' => $offset,
            'size' => $limit === null ? \Yii::$app->params['pageSize'] : $limit,
            'sort' => $sort,
            '_source' => ["search_result_data"],
            'query' => [
                'bool' => [
                    'must' => $parentMust
                ],

            ]
        ];

        // Old aggregation Query
//        $aggQuery = [
//            'from' => $offset,
//            'size' => 0,
//            '_source' => ["search_result_data"],
//            'query' => [
//                'bool' => [
//                    'must' => $parentAggMust
//                ],
//            ],
//            'aggregations' => [
//                'agg_search_data' => [
//                    'nested' => [
//                        'path' => 'search_data'
//                    ],
//                    'aggregations' => [
//                        'string_facet' => [
//                            'nested' => [
//                                'path' => 'search_data.string_facet'
//                            ],
//                            'aggregations' => [
//                                'facet_name' => [
//                                    'terms' => [
//                                        'field' => 'search_data.string_facet.facet-name'
//                                    ],
//                                    'aggregations' => [
//                                        'facet_value' => [
//                                            'terms' => [
//                                                'field' => 'search_data.string_facet.facet-value'
//                                            ]
//                                        ]
//                                    ]
//                                ]
//                            ]
//                        ]
//                    ]
//                ]
//            ]
//        ];

        $aggQuery = [
            'from' => $offset,
            'size' => 0,
            '_source' => ["search_result_data"],
            'query' => [
                'bool' => [
                    'must' => $parentAggMust
                ],

            ],
            'aggregations' => [
                'agg_search_data' => [
                    'filter' => [
                        'bool' => [
                            'filter' => [
                            ]
                        ]
                    ],
                    'aggregations' => [
                        'string_facet' => [
                            'nested' => [
                                'path' => 'search_data.string_facet'
                            ],
                            'aggregations' => [
                                'facet_name' => [
                                    'terms' => [
                                        'field' => 'search_data.string_facet.facet-name'
                                    ],
                                    'aggregations' => [
                                        'facet_value' => [
                                            'terms' => [
                                                'field' => 'search_data.string_facet.facet-value'
                                            ]
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ]
                ]
            ]
        ];

        if (isset($search['facets'])) {
            foreach ($search['facets'] as $key => $facet) {
                // add filtered before aggregations
                $aggQuery['aggregations']['agg_search_data']['filter']['bool']['filter'][] = [
                    'nested' => [
                        'path' => 'search_data.string_facet',
                        'query' => [
                            'bool' => [
                                'filter' => [
                                    [
                                        'term' => [
                                            'search_data.string_facet.facet-name' => $key
                                        ]
                                    ],
                                    [
                                        'terms' => [
                                            'search_data.string_facet.facet-value' => array_values($facet)
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ]
                ];
            };
            foreach ($search['facets'] as $key => $facet) {
                // add special aggregations
                    // clone agrresion from basic aggresion
                $specialAggregations = $aggQuery['aggregations']['agg_search_data'];
                    // remove facet from filter
                        // find index of facet in filter array
                $aggValue = [
                    'nested' => [
                        'path' => 'search_data.string_facet',
                        'query' => [
                            'bool' => [
                                'filter' => [
                                    [
                                        'term' => [
                                            'search_data.string_facet.facet-name' => $key
                                        ]
                                    ],
                                    [
                                        'terms' => [
                                            'search_data.string_facet.facet-value' => array_values($facet)
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ]
                ];
                $aggIndex = array_search($aggValue, $specialAggregations['filter']['bool']['filter']);
                        // remove
                array_splice($specialAggregations['filter']['bool']['filter'], $aggIndex, 1);
                        // add special aggregations to aggregations
                $aggQuery['aggregations']['special_agg_'.$key] = $specialAggregations;
            }
        };

        $elasticResults = self::buildMultiResults(ElasticHelper::sendMultiRequest([
            'results' => [
                'index' => Course::getAlias(),
                'body' => $query
            ],
            'aggregations' => [
                'index' => Course::getAlias(),
                'body' => $aggQuery
            ]
        ]));

        $results = $elasticResults['results'];
        $aggregations = $elasticResults['aggregations'];

        if (isset($aggregations['agg_search_data'])) {
            $results['agg_search_data'] = $aggregations['agg_search_data'];
        }
        return $results;
    }

    //  Ham search theo completion_terms
    public static function getCompletionTerm($searchText)
    {
        // neu search text khac rong, tim binh thuong
        if ($searchText != '')
        {
            $query = [
                'from' => 0,
                'size' => 5,
                'sort' => [
                    '_score' => 'desc',
                ],
                '_source' => ["search_result_data"],
                'query' => [
                    'bool' => [
                        'must' => [
                            [
                                'nested' => [
                                    'path' => 'search_data.number_facet',
                                    'query' => [
                                        'bool' => [
                                            'must' => [
                                                [
                                                    'match' => ['search_data.number_facet.facet-name' => 'status']  // status = 1
                                                ],
                                                [
                                                    'match' => ['search_data.number_facet.facet-value' => 1]
                                                ]

                                            ]
                                        ]
                                    ]
                                ]
                            ],
                            [
                                'multi_match' => [                          // completion terms
                                    'query' => $searchText,
                                    'fields' => [
                                        'completion_terms',
                                        'completion_terms.no-sign'
                                    ],
                                    'type' => 'cross_fields'
                                ]
                            ],
                        ]
                    ]
                ]
            ];
            $elasticResult = ElasticHelper::sendRequest([
                'index' => self::getAlias(),
                'body' => $query
            ]);
            $results = self::buildResult($elasticResult);
            return $results;
        }
        // neu search text rong, tra ve hot courses list
        else {
            $query = [
                'from' => 0,
                'size' => 5,
                '_source' => ["search_result_data"],
                'query' => [
                    'bool' => [
                        'must' => [
                            [
                                'nested' => [
                                    'path' => 'search_data.number_facet',
                                    'query' => [
                                        'bool' => [
                                            'must' => [
                                                [
                                                    'match' => ['search_data.number_facet.facet-name' => 'status']  // status = 1
                                                ],
                                                [
                                                    'match' => ['search_data.number_facet.facet-value' => 1]
                                                ]

                                            ]
                                        ]
                                    ]
                                ]
                            ],
                            [
                                'nested' => [
                                    'path' => 'search_data.string_facet',
                                    'query' => [
                                        'bool' => [
                                            'must' => [
                                                [
                                                    'match' => ["search_data.string_facet.facet-name" => 'hot']  // hot = 0
                                                ],
                                                [
                                                    'match' => ["search_data.string_facet.facet-value" => '0']
                                                ]
                                            ]
                                        ]
                                    ]
                                ]
                            ],
                        ]
                    ]
                ]
            ];
            $elasticResult = ElasticHelper::sendRequest([
                'index' => self::getAlias(),
                'body' => $query
            ]);
            $results = self::buildResult($elasticResult);
            return $results;
        }
    }

    // Ham search theo id
    public static function getCourse($id)
    {
        $query = [
            'query' => [
                'match' => [
                    '_id' => $id,
                ]
            ]
        ];
        $elasticResult = ElasticHelper::sendRequest([
            'index' => self::getAlias(),
            'body' => $query
        ]);
        $results = self::buildResult($elasticResult);
        return $results;
    }

    private static function g($a, $k)
    {
        if (isset($a[$k]))
            return $a[$k];
        return '';
    }

    public static function getFacetDiscount($key = 'all')
    {
        $all = [
            '0' => 'Miễn phí',
            '1' => 'Khuyến mãi',
        ];
        if ($key == 'all')
            return $all;
        return self::g($all, $key);
    }

    public static function getFacetLevel($key = 'all')
    {
        $all = \kyna\course\models\Course::listLevel();
        if ($key == 'all')
            return $all;
        return self::g($all, $key);
    }

    public static function getFacetTime($key = 'all')
    {
        $all = [
            '0' => 'Dưới 3 tiếng',
            '1' => '3 - 24 tiếng',
            '2' => '1 - 3 ngày',
            '3' => 'Trên 3 ngày'
        ];
        if ($key == 'all')
            return $all;
        return self::g($all, $key);

    }

    public static function getFacetHot($key = 'all')
    {
        $all = [
            '0' => 'Hot',
            '1' => 'New',
        ];
        if ($key == 'all') {
            return $all;
        }
        return self::g($all, $key);
    }

    public static function getFacetType($key = 'all')
    {
        $all = [
            '1' => 'Học qua Video',
            '3' => 'App điện thoại, tablet',
        ];
        if ($key == 'all') {
            return $all;
        }
        return self::g($all, $key);
    }
}