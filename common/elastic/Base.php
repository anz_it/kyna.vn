<?php
/**
 * Created by PhpStorm.
 * User: khanhphan
 * Date: 4/18/17
 * Time: 11:46 AM
 */

namespace common\elastic;


use yii\base\Model;
use yii\helpers\Json;
use Yii;

abstract class Base extends Model
{

    protected static function buildResult($elasticResult)
    {

        $data = [];
        foreach ($elasticResult['hits']['hits'] as $item) {
            $data[] = $item['_source']['search_result_data'];
        }
        $results =  [
            'data' => $data,
            'offset' => isset($query['from']) ? $query['from'] : 0,
            'limit' => isset($query['size']) ? $query['size'] : \Yii::$app->params['pageSize'],
            'total' => $elasticResult['hits']['total']
        ];

        if (isset($elasticResult['aggregations'])) {
            // mix special_agg to agg_search_data
//            $results['agg_search_data'] = $elasticResult['aggregations']['agg_search_data']['string_facet']['facet_name']['buckets'];
            $results['agg_search_data'] = self::mixAggregations($elasticResult['aggregations']);
        }
        return $results;
    }

    protected static function buildMultiResults($elasticResults)
    {
        $results = [];
        foreach ($elasticResults as $key => $result){
            $results[$key] = static::buildResult($result);
        }
        return $results;
    }

    protected static function getResults($query,  $index, $type = null, $useFuture = true)
    {
        $options = [
            'index' => $index,
            'type' => $type,
            'body' =>  $query
        ];
        if ($useFuture) {
            $options['client'] = [
                'future' => 'lazy'
            ];
        }
        //$result = self::getDb()->post($url, $options, Json::encode($query));
    }

    /**
     * Hàm thay thế facet bucket trong agg_search_data bằng facet_bucket trong special_agg_[facet-name] nếu có
     * @param $aggregations
     * @return array
     */
    protected static function mixAggregations($aggregations)
    {
        $output = [];
        foreach ($aggregations['agg_search_data']['string_facet']['facet_name']['buckets'] as $index => $bucket) {
            // Get facet_name
            $facetName = $bucket['key'];
            // Generate special_agg_name
            $specialAggName = 'special_agg_'.$facetName;
            // If special_agg_name exist replace bucket
            if (array_key_exists($specialAggName, $aggregations)) {
                // Get special agg buckets
                $specialBuckets = $aggregations[$specialAggName]['string_facet']['facet_name']['buckets'];
                foreach ($specialBuckets as $specialIndex => $specialBucket) {
                    if ($specialBucket['key'] === $bucket['key'])
                        $output[] = $specialBucket;
                }
            } else {
                $output[] = $bucket;
            }
        }
        return $output;
    }

}