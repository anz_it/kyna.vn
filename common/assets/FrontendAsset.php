<?php

namespace common\assets;

use yii\web\AssetBundle;

class FrontendAsset extends AssetBundle
{
    public $allowCacheCss = [
        'main.min.css'
    ];
    public $allowCacheJs = [];

    public function __construct()
    {
        $this->basePath = '@media';
        $this->baseUrl = !empty(\Yii::$app->params['media_link']) ? \Yii::$app->params['media_link'] : 'https://media.kyna.vn';
        $this->modifyVersion();
    }

    public function modifyVersion() {
        $settings = \Yii::$app->controller->settings;
        $cacheVersion = !empty($settings['cache_version']) ? $settings['cache_version'] : '';
        foreach ($this->js as &$item) {
            if (self::canCache($item, $this->allowCacheJs)) {
                $item .= '?v=' . $cacheVersion;
            }
        }
        unset($item);
        foreach ($this->css as &$item) {
            if (self::canCache($item, $this->allowCacheCss)) {
                $item .= '?v=' . $cacheVersion;
            }
        }
    }

    public static function canCache($item, $allowArray)
    {
        if (empty($allowArray)) {
            return false;
        }
        foreach ($allowArray as $allowItem) {
            if (strpos($item, $allowItem) !== false) {
                return true;
            }
        }
        return false;
    }
}
