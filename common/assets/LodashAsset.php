<?php

namespace common\assets;

use yii\web\AssetBundle;
use \yii\web\View;

/**
 * This is class asset bunle for `home` layout
 */
class LodashAsset extends AssetBundle
{
    public $sourcePath = '@bower/lodash/dist';

    public $js = ['lodash.min.js'];
}
