<?php
/**
 * Created by PhpStorm.
 * User: macintosh
 * Date: 8/28/17
 * Time: 11:01 AM
 */

namespace common\assets;


use yii\web\AssetBundle;

class BootstrapNotifyAsset extends AssetBundle
{

    public $sourcePath = '@bower/';

    public $js = [
        'remarkable-bootstrap-notify/bootstrap-notify.js',
    ];

}