<?php
/**
 * Created by PhpStorm.
 * User: macintosh
 * Date: 8/31/17
 * Time: 4:32 PM
 */

namespace common\assets;

use yii\web\AssetBundle;
use yii\web\View;

class ClipboardAsset extends AssetBundle
{

    public function init()
    {
        $this->basePath = '@media';
        $this->baseUrl = !empty(\Yii::$app->params['media_link']) ? \Yii::$app->params['media_link'] : 'https://media.kyna.vn';

        parent::init();
    }

    public $js = [
        ["js/clipboard/clipboard.min.js", "position" => View::POS_END],
    ];
}