<?php
/**
 * Created by PhpStorm.
 * User: nguyenphanngu
 * Date: 9/7/17
 * Time: 4:21 PM
 */

namespace common\recommendation;

use common\components\ElasticSearch;
use kyna\course\models\Category;
use Yii;
use yii\base\Exception;
use yii\db\Query;
use common\recommendation\models\Course as RecommendationCourse;
use common\recommendation\models\Category as RecommendationCategory;
use common\recommendation\models\Combo as RecommendationCombo;
use kyna\course\models\Course;
use kyna\course_combo\models\CourseCombo;
class Recommendation
{
    public static $config = [
        'hot' => [
            'weight' => [
                'view' => 1,
                'student' => 1,
                'rating' => 1,
                'is_hot' => 1,
            ]
        ]
    ];


    public static function getHotCourse()
    {
        $cacheText = 'courses_hot_30days';

        $maxViewCount = 0;
        $maxRate = 5;
        $maxStudentCount = 0;

        if (empty($categoryId)) {
            // delete cache every time - for DEBUG only
            Yii::$app->cache->delete($cacheText);

            // Neu hot couses co san trong cache, return hot courses
            $result = Yii::$app->cache->get($cacheText);
            if (!empty($result)) {
                return $result;
            }
            // Khong co trong cache, search trong database
            $query = new Query();
            $result = $query->select('*')
                ->from('recommendation_courses')
                ->leftJoin('courses','courses.id = recommendation_courses.course_id')
                ->where(['courses.status'=>Course::STATUS_ACTIVE])
                ->orderBy('hot_score_30d desc')
                ->limit(30)
                ->all();

            // filter lai result
            $filter_result = [];
            foreach ($result as $key => $value) {
                $filter_result[] = \common\elastic\Course::getCourse($value['course_id'])['data'][0];
            }

            // Save filtered result to cache
            Yii::$app->cache->set($cacheText, $filter_result, 60 * 5);
            return ($filter_result);
        }
    }

    public static function getBestSellerCourses()
    {
        $cacheText = 'courses_best_seller_30days';

        // Neu hot couses co san trong cache, return hot courses
        $result = Yii::$app->cache->get($cacheText);
        if (!empty($result)) {
            return $result;
        }

        // Khong co trong cache, search course trong database
        $query = new Query();
        $result = $query->select('*')
            ->from('courses c')
            ->innerJoin('recommendation_courses cs', 'c.id = cs.course_id')
            ->orderBy('cs.sell_score_30d desc')
            ->where(['c.status'=>Course::STATUS_ACTIVE])
            ->limit(30)
            ->all()
        ;

        // filter lai result
        $filter_result = $result;

        // Save filtered result to cache
        Yii::$app->cache->set($cacheText, $filter_result, 60 * 5);
        return ($filter_result);
    }

    public static function getBestSellerCategories($parentId = null)
    {
        $cacheText = 'categories_best_seller_30days';

        // delete cache every time - for DEBUG only
        Yii::$app->cache->delete($cacheText);

        // Neu hot couses co san trong cache, return hot courses
        $result = Yii::$app->cache->get($cacheText);
        if (!empty($result)) {
            return $result;
        }

        $query = new Query();
        $query->select('c.*')
            ->from('categories c')
            ->innerJoin('recommendation_categories rc','c.id = rc.category_id')
            ->where([
                'c.status' => Category::STATUS_ACTIVE,
                'c.is_deleted' => false,
            ])
            ->andWhere('c.order is not null');
        if ($parentId != null) {
            $query->andWhere(['c.parent_id' => $parentId]);
        }
        $query->orderBy('rc.sell_score_30d desc')
            ->limit(6);

        $result = $query->all();

        // filter lai result
        $filter_result = [];
        foreach ($result as $key => $value) {
            $_category = Category::findOne($value['id']);
            if (isset($_category)) {
                $filter_result[] = $_category;
            }
        }

//        var_dump($filter_result);
        // Save filtered result to cache
        Yii::$app->cache->set($cacheText, $filter_result, 60 * 5);
        return ($filter_result);
    }

    public static function getRelatedCategories($id)
    {
        $category = Category::findOne([
            'id' => $id,
            'status' => Category::STATUS_ACTIVE,
            'is_deleted' => false
        ]);

        if (empty($category)) {
            return [];
        }

        // parse related categories_id
        $relatedCategoriesId = $category->related_categories_id;
        $relatedCategoriesId = mb_split(',', $relatedCategoriesId);
        array_splice($relatedCategoriesId, 0, 1);

        $result = [];
        foreach ($relatedCategoriesId as $key => $categoryId) {
            $_category = Category::findOne([
                'id' => $categoryId,
                'status' => Category::STATUS_ACTIVE,
                'is_deleted' => false
            ]);

            if (!empty($_category)) {
                $result[] = $_category;
            }
        }

        return $result;

    }

    public static function updateCourseSellScore()
    {
        try {
            $query = new Query();
            $result = $query
                ->select('od.course_id, count(*) as sellCount')
                ->from ('order_details od')
                ->innerJoin('orders o', 'od.order_id = o.id and o.status = 5 and o.order_date > UNIX_TIMESTAMP(DATE_SUB(now(), INTERVAL 30 DAY))')
                ->groupBy('od.course_id')
                ->all()
            ;

            foreach ($result as $key => $entry) {
                $recommendationCourse = RecommendationCourse::findOne([
                    'course_id' => $entry['course_id']
                ]);
                if (empty($recommendationCourse)) {
                    $recommendationCourse = new RecommendationCourse();
                    $recommendationCourse->course_id = $entry['course_id'];
                    $recommendationCourse->created_time = time();
                }

                $recommendationCourse->sell_score_30d = $entry['sellCount'];
                $recommendationCourse->updated_time = time();
                $recommendationCourse->save();
            }
            return true;

        } catch (Exception $err) {
            return $err;
        }

    }

    public static function updateCourseHotScore()
    {
        $maxViewCount = 0;
        $maxRate = 5;
        $maxStudentCount = 0;

        // Get all courses
        $courses = Course::findAll(['status' => Course::STATUS_ACTIVE]);
        // Calculate single course
        $result = [];
        foreach ($courses as $key => $course) {
            $query = new Query();
                // new Student count
            $resStudentCount = $query->select('course_id, count(*) as studentCount')
                ->from ('user_courses')
                ->where('course_id = '.$course->id.' and started_date > UNIX_TIMESTAMP(DATE_SUB(now(), INTERVAL 30 DAY))')
                ->one();
            ;
            if ($resStudentCount['studentCount'] > $maxStudentCount) {
                $maxStudentCount = $resStudentCount['studentCount'];
            }

                // detail page view Count
            $resViewCount = $query->select('course_id, count(*) as viewCount')
                ->from('course_view_logs')
                ->where('course_id = '.$course->id.' and updated_time > UNIX_TIMESTAMP(DATE_SUB(now(), INTERVAL 30 DAY))')
                ->one();
            ;
            if ($resViewCount['viewCount'] > $maxViewCount) {
                $maxViewCount = $resViewCount['viewCount'];
            }

                // avegrage ratings
            $resRatingAvg = $query->select('course_id, avg(score) as ratingAvg')
                ->from('course_ratings')
                ->where('course_id = '.$course->id.' and updated_time > UNIX_TIMESTAMP(DATE_SUB(now(), INTERVAL 30 DAY))')
                ->one();
            ;

            $result[] = [
                'course' => $course,
                'student_count' => $resStudentCount['studentCount'],
                'view_count' => $resViewCount['viewCount'],
                'rating_avg' => $resRatingAvg['ratingAvg'],
            ];
        }
        foreach ($result as $key => $value) {
                // Scale feature to [0:1]
            $value['scaled_student_count'] = ($maxStudentCount == 0)? 0: $value['student_count']/$maxStudentCount;
            $value['scaled_view_count'] = ($maxViewCount == 0)? 0 : $value['view_count']/$maxViewCount;
            $value['scaled_rating_avg'] = ($maxRate == 0)? 0 : $value['rating_avg']/$maxRate;
            $value['is_hot'] = $value['course']->is_hot? 1 : 0;
            $value['id'] = $value['course']->id;
                // calculate hot score
            $value['hot_score'] =
                (
                    $value['scaled_student_count'] * self::$config['hot']['weight']['student'] +
                    $value['scaled_view_count'] * self::$config['hot']['weight']['view'] +
                    $value['scaled_rating_avg'] * self::$config['hot']['weight']['rating'] +
                    $value['is_hot'] * self::$config['hot']['weight']['is_hot']
                )
                /
                (
                    self::$config['hot']['weight']['student'] +
                    self::$config['hot']['weight']['view'] +
                    self::$config['hot']['weight']['rating'] +
                    self::$config['hot']['weight']['is_hot']
                );
            $result[$key] = $value;

            $_course = RecommendationCourse::findOne(['course_id' => $value['id']]);
            if (empty($_course)) {
                $_course = new RecommendationCourse();
                $_course->course_id = $value['id'];
                $_course->created_time = time();
            }
            $_course->hot_score_30d = $value['hot_score'];
            $_course->updated_time = time();
            $_course->save();
        }

        return $result[0];

    }

    public static function updateCategorySellScore()
    {
        try {
            $query = new Query();
            $result = $query
                ->select('category_id, count(*) as sellCount')
                ->from ('order_details od')
                ->innerJoin('orders o', 'od.order_id = o.id and o.status = 5 and o.order_date > UNIX_TIMESTAMP(DATE_SUB(now(), INTERVAL 30 DAY))')
                ->innerJoin('courses c', 'od.course_id = c.id')
                ->groupBy('c.category_id')
                ->all()
            ;

            foreach ($result as $key => $entry) {
                $recommendationCategory = RecommendationCategory::findOne([
                    'category_id' => $entry['category_id']
                ]);
                if (empty($recommendationCategory)) {
                    $recommendationCategory = new RecommendationCategory();
                    $recommendationCategory->category_id = $entry['category_id'];
                    $recommendationCategory->created_time = time();
                }

                $recommendationCategory->sell_score_30d = $entry['sellCount'];
                $recommendationCategory->updated_time = time();
                $recommendationCategory->save();
            }
            return true;

        } catch (Exception $err) {
            return $err;
        }
    }

    public  static function getCourseWithinCourseId($limit = 4){


        $items = Yii::$app->cart;
        foreach ($items->positions as $item) {
            $category_id = $item->category_id;
            break;
        }

        if($category_id){
            $cacheText = "courses_top4_within_course_category_id_$category_id";

            // Neu hot couses co san trong cache, return hot courses
            $result = Yii::$app->cache->get($cacheText);
            if (!empty($result)) {
                return $result;
            }

            $query = new Query();
            $result = $query
                ->select('*')
                ->from ('courses cs')
                ->where(['category_id'=>$category_id])
                ->limit($limit)
                ->all()
            ;
            $filter_result = [];
            foreach ($result as $key => $value) {
                $data = \common\elastic\Course::getCourse($value['id']);
                if(isset($data['data'][0])){
                    $filter_result[] = $data['data'][0];
                }
            }


            // Save filtered result to cache
            Yii::$app->cache->set($cacheText, $filter_result, 60 * 5);
            return ($filter_result);
        }
    }


    public static function getCourseByTeacher($teacher_id,$limit=10){

        $cacheText = "courses_top4_within_teacher_id_$teacher_id";

        // Neu hot couses co san trong cache, return hot courses
        $result = Yii::$app->cache->get($cacheText);
        if (!empty($result)) {
            return $result;
        }

        $query = new Query();
        $result = $query
            ->select('*')
            ->from ('courses cs')
            ->where(['teacher_id'=>$teacher_id])
            ->andWhere(['cs.status'=>Course::STATUS_ACTIVE])
            ->limit($limit)
            ->all()
        ;
        $filter_result = [];
        foreach ($result as $key => $value) {
            $data = \common\elastic\Course::getCourse($value['id']);
            if(isset($data['data'][0])){
                $filter_result[] = $data['data'][0];
            }
        }
        // Save filtered result to cache
        Yii::$app->cache->set($cacheText, $filter_result, 60 * 5);
        return ($filter_result);
    }

    public static function updateRecommendationsCombo(){
        $connection = \Yii::$app->db;
        $model = $connection->createCommand('
                    SELECT od.`course_combo_id`, COUNT(*) AS \'total\', GROUP_CONCAT(DISTINCT (od.`course_id`)SEPARATOR \',\') AS \'courses_id\'
                    FROM order_details od
                    LEFT JOIN orders o ON od.`order_id` = o.`id`
                    WHERE o.`status` = 5
                    AND o.order_date > UNIX_TIMESTAMP(DATE_SUB(NOW(), INTERVAL 100 DAY))
                    GROUP BY od.`course_combo_id`
      
      ');
        try{
            $data = $model->queryAll();
            foreach ($data as $value){
                $recommendationCombo = RecommendationCombo::findOne(['combo_id' => $value['course_combo_id']]);
                if (empty($recommendationCombo)) {
                    $recommendationCombo = new RecommendationCombo();
                    $recommendationCombo->combo_id = $value['course_combo_id'];
                    $recommendationCombo->total = $value['total'];
                    $recommendationCombo->courses_id = $value['courses_id'];
                }
                $recommendationCombo->combo_id = $value['course_combo_id'];
                $recommendationCombo->total = $value['total'];
                $recommendationCombo->courses_id = $value['courses_id'];
                $recommendationCombo->save();
            }
            return true;
        }catch ( Exception $e){
            print_r($e->getMessage());die;
        }


    }

    public static function checkCourseHasManyCombo($course_id = 1)
    {
        $connection = \Yii::$app->db;
        $model = $connection->createCommand("
            SELECT MAX(total) AS sellcount,combo_id
            FROM recommendation_combo
            WHERE $course_id IN (SELECT cci.`course_id` FROM course_combo_items cci WHERE recommendation_combo.`combo_id` = cci.`course_combo_id`)
            AND combo_id !=0 
            group by combo_id
            ORDER BY sellcount DESC
            limit 1
      ");
        $data = $model->queryAll();
        return $data;
    }

    public static function getCourseByComboId($combo_id){

        $recommendationCombo = RecommendationCombo::findOne(['combo_id' => $combo_id]);
        $temps = explode(',',$recommendationCombo->courses_id);
        foreach($temps as $temp){
            $a_courses[] =$temp;
        }
        $courses = CourseCombo::find()
            ->where(['IN','id',$a_courses])
            ->all();

        return $courses;

    }
}