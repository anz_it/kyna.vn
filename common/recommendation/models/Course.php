<?php
/**
 * Created by PhpStorm.
 * User: nguyenphanngu
 * Date: 9/6/17
 * Time: 9:29 AM
 */

namespace common\recommendation\models;


/**
 * This is the model class for table "recommendation_courses".
 *
 * @property integer $id
 * @property integer $course_id
 * @property float $hot_score_30d
 * @property float $sell_score_30d
 */


class Course extends Base
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'recommendation_courses';
    }
}
