<?php
/**
 * Created by PhpStorm.
 * User: nguyenphanngu
 * Date: 9/11/17
 * Time: 2:28 PM
 */

namespace common\recommendation\models;

/**
 * This is the model class for table "recommendation_categories".
 *
 * @property integer $id
 * @property integer $category_id
 * @property float $hot_score_30d
 * @property float $sell_score_30d
 */

class Category extends Base
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'recommendation_categories';
    }
}