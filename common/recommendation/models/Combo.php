<?php
/**
 * Created by PhpStorm.
 * User: nguyenphanngu
 * Date: 9/6/17
 * Time: 9:29 AM
 */

namespace common\recommendation\models;


/**
 * This is the model class for table "recommendation_combo".
 *
 * @property integer $id
 * @property string $courses_id
 * @property integer $combo_id
 * @property integer $total
 */


class Combo extends Base
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'recommendation_combo';
    }
}
