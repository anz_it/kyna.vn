<?php
/**
 * Created by PhpStorm.
 * User: ngunp
 * Date: 6/30/2017
 * Time: 9:05 AM
 */

namespace common\helpers;

use kyna\servicecaller\traits\CurlTrait;
use Yii;
use yii\base\Exception;
use yii\db\Query;
use yii\helpers\Json;
use yii\web\UploadedFile;
use common\lib\CDNImage;


use app\modules\notification\models\Notification;
use app\modules\notification\models\Schedule;


class NotificationHelper
{
    use CurlTrait;

    const REPEAT_TYPE__ONE_TIME = 0;
    const REPEAT_TYPE__DAILY = 1;
    const REPEAT_TYPE__WEEKLY = 2;
    const REPEAT_TYPE__MONTHLY = 3;

    public static $repeatTypeText = [
        self::REPEAT_TYPE__ONE_TIME => 'one time',
        self::REPEAT_TYPE__DAILY => 'daily',
        self::REPEAT_TYPE__WEEKLY => 'weekly',
        self::REPEAT_TYPE__MONTHLY => 'monthly',
    ];

    const PRIORITY__LOW = 0;
    const PRIORITY__MEDIUM = 1;
    const PRIORITY__HIGH = 2;

    const COURSE = 1;
    const CATEGORY = 2;
    const TAG = 3;
    const SEARCH = 4;
    const URL = 5;

    const IS_MOBILE = 1;
    const IS_DESKTOP = 2;

//    private static $_secretKey = '';
//    private static $_defaultTopic = '';

    private function __construct()
    {
//        self::$_secretKey = Yii::$app->params['fcm']['_secretKey'];
//        self::$_defaultTopic = Yii::$app->params['fcm']['_defaultTopic'];
    }

    /**
     * Create/update token
     * @param $token
     * @param null $userId
     * @return bool
     */

    public static function updateToken($token, $userId = null)
    {
        $query = new Query();

        // Check if token existed in
        $data = $query->select('*')
            ->from("notification_user_token")
            ->where(['token' => $token])
            ->one();
        if (empty($data)) {
            // chua co token trong db -> create
            $data = $query->createCommand()->insert('notification_user_token', [
                'user_id' => $userId,
                'token' => $token,
                'created_at' => time(),
                'modified_at' => time(),
            ])->execute();
            // add token to topic all
//            return self::addTokenToTopic($token, self::$_defaultTopic);
            return self::addTokenToTopic($token);
        } else {
            // da co token trong db -> kiem tra user co doi khong, doi thi update lai userid
            if ((string)$data['user_id'] !== (string)$userId) {
                $data = $query->createCommand()->update(
                    'notification_user_token',                  // table name
                    [                                           // key -> value
                        'user_id' => $userId,
                        'modified_at' => time(),
                    ],
                    [                                           // condition
                        'token' => $token,
                    ]
                )->execute();
                // add token to topic all
                return self::addTokenToTopic($token);
            }
        }
        if ($data)
            return true;
        else
            return false;
    }

    public static function deleteToken($token)
    {
        $query = new Query();

        // delete in user token
        $data = $query->createCommand()->delete(
            'notification_user_token',                          // table name
            [                                                   // condition
                'token' => $token,
            ]
        )->execute();

        // delete in group token
    }

    public static function addTokenToTopic($token, $topic = null)
    {
        $notificationHelper = new NotificationHelper();
        if (empty($topic)) {
            $topic = Yii::$app->params['fcm']['_defaultTopic'];
        }
        // prepare url
        $url = 'https://iid.googleapis.com/iid/v1/' . $token . '/rel/topics/' . $topic;
        $data = [];
        $res = $notificationHelper->call($url, $data, 'POST');
        return $res;
    }

    public static function addBatchTokensToTopic($tokens, $topic = null)
    {
        if (empty($topic)) {
            $topic = Yii::$app->params['fcm']['_defaultTopic'];
        }
    }

    public static function sendMessage($params)
    {
        $notification = new NotificationHelper();

        $mediaBase = Yii::$app->params['media_link'];

        // get params
//        $title = "title with love";
        $inputData = [];
        $inputData['title'] = $params['title'];
        $inputData['body'] = $params['body'];

        if (!empty($params['icon'])) {
            $inputData['icon'] = isset($params['icon']) ? $mediaBase . $params['icon'] : $params['icon'];
        };
        if (!empty($params['image'])) {
            $inputData['image'] = isset($params['image']) ? $mediaBase . $params['image'] : $params['image'];
        };
        if (!empty($params['redirect_url'])) {
            $inputData['data']['url'] = $params['redirect_url'];
        };
        if (!empty($params['actions'])) {
            $inputData['actions'] = $params['actions'];
        };

        $topic = Yii::$app->params['fcm']['_defaultTopic'];

        // prepare url

        $url = 'https://fcm.googleapis.com/fcm/send';
        $data = [
            "to" => "/topics/" . $topic,
            "data" => $inputData,
        ];
        $res = $notification->call($url, $data, 'POST');
        return $res;
    }

    public static function sendMessageMobile($params)
    {
        if (!empty($params)) {
            $url = 'https://fcm.googleapis.com/fcm/send';
            //api_key available in Firebase Console -> Project Settings -> CLOUD MESSAGING -> Server key
            $server_key = $topic = Yii::$app->params['fcm']['_server_key_mobile'];

            $fields = array();
            $fields['notification'] = [];
            $fields['notification']['title'] = $params['title'];
            $fields['notification']['body'] = $params['body'];

            $fields['data'] = [];
            $fields['data']['screen_type'] = $params['screen_type_mobile'];
            $fields['data']['title'] = $params['title'];
            $fields['data']['data'] = $params['data'];
            $fields['to'] = "/topics/" . $params['topic'];
            //header with content_type api key
            $headers = array(
                'Content-Type:application/json',
                'Authorization:key=' . $server_key
            );


            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
            $result = curl_exec($ch);
            if ($result === FALSE) {
                Yii::info('Notification Send Error: ' . curl_error($ch),'notification');
            }
            curl_close($ch);
            return $result;
        }
    }

    // Table notification
    /**
     * Create Notification, Create Schedule, Init Schedule
     * @param $params
     * @return bool|\Exception|Exception
     */
    public static function createNotification($params)
    {
//        $query = new Query();
        $user = Yii::$app->user;

        // Validate params here

        try {
            // create notification
            $notification = new Notification();
            $notification['name'] = $params['name'];
            $notification['priority'] = isset($params['priority']) ? $params['priority'] : self::PRIORITY__MEDIUM;
            $notification['title'] = $params['title'];
            $notification['body'] = $params['body'];
            $notification['icon'] = isset($params['icon_url']) ? $params['icon_url'] : null;
            $notification['image'] = isset($params['image_url']) ? $params['image_url'] : null;
            $notification['redirect_url'] = isset($params['redirect_url']) ? $params['redirect_url'] : 'https://kyna.vn';
            $notification['description'] = isset($params['description']) ? $params['description'] : null;
            $notification['actions'] = isset($params['actions']) ? $params['actions'] : null;

            $notification['is_deleted'] = false;

            $notification['created_at'] = time();
            $notification['created_by'] = $user->id;
            $notification['modified_at'] = time();
            $notification['modified_by'] = $user->id;


            $notification->save();
            if (isset($params['icon_url'])) {
                $image_url = self::_uploadImage($notification, 'icon');
                $notification['icon'] = $image_url;
                $notification->save();
            }

            // create notification_schedule
            $schedule = new Schedule();
            $schedule['notification_id'] = $notification['id'];
            $schedule['is_enabled'] = $params['enable'];
            $schedule['type'] = $params['repeat_type'];

            $schedule['start_time'] = $params['start_time'];

            $schedule['setting'] = isset($params['settings']) ? $params['settings'] : null;
            $schedule['adv_setting'] = isset($params['adv_settings']) ? $params['adv_settings'] : null;

            $schedule['last_occure'] = null;
            $schedule['is_deleted'] = false;

            $schedule['created_at'] = time();
            $schedule['created_by'] = $user->id;
            $schedule['modified_at'] = time();
            $schedule['modified_by'] = $user->id;

            $schedule->save();

            // calculate next_occure
            $schedule['next_occure'] = self::calculateNextOccure($schedule['start_time'], $schedule['last_occure'], $schedule['type'], Json::decode($schedule['setting']));

            $schedule->save();

        } catch (Exception $err) {
            return (false);
        }
        return $notification['id'];
    }

    public static function getAvailableJobs()
    {
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $query = new Query();
        $result = $query->select(['*'])
            ->from('notification_schedule')
            ->where([
                'is_enabled' => 1,
            ])
            ->andFilterCompare('next_occure', time(), '<=')
            ->all();
        return $result;
    }

    /**
     * Gui nhung notification da toi thoi diem gui
     */
    public static function sendTriggeredNotification()
    {
        $query = new Query();
        $result = [];
        // Get trigger schedule list
        $schedules = self::getAvailableJobs();
        foreach ($schedules as $index => $value) {
            // Lay thong tin va send notification
            // Prepare params
            $notification = $query->select('*')
                ->from('notification')
                ->where([
                    'id' => $value['notification_id']
                ])
                ->one();
//            print_r($notification['is_type']);die;
            if (!empty($notification)) {
                $result[] = $notification;
                if($notification['is_type'] == self::IS_DESKTOP){
                    self::sendMessage($notification);
                }
                elseif ($notification['is_type'] == self::IS_MOBILE){
                    self::sendMessageMobile($notification);
                }

            }

            // Cap nhat lai Schedule's next occure
            $schedule = $query->createCommand()
                ->update(
                    'notification_schedule',
                    [
                        'last_occure' => $value['next_occure'],
                        'next_occure' => self::calculateNextOccure($value['start_time'], $value['next_occure'], $value['type'], Json::decode($value['setting']))],
                    ['id' => $value['id'],
                    ]
                )
                ->execute();
        }
        return $result;
    }

//    public static function calculateNextOccure($schedule)
    public static function calculateNextOccure($startTime, $lastOccure, $type, $settings, $endtime = null)
    {
        // Load settings
        switch ($type) {
            case self::REPEAT_TYPE__ONE_TIME:
                // First Time
                if (empty($lastOccure))
                    return $startTime;
                // Other times
                return null;
                break;
            case self::REPEAT_TYPE__DAILY:
                // First Time
                if (empty($lastOccure))
                    return $startTime;
                // Other times
                return strtotime('+' . $settings['daily']['repeat_rate'] . ' day', $lastOccure);
                break;
            case self::REPEAT_TYPE__WEEKLY:
                if (empty($settings['weekly']['days'])) {
                    // First Time
                    if (empty($lastOccure))
                        return $startTime;
                    // Other times
                    return strtotime('+' . $settings['weekly']['repeat_rate'] . ' week', $lastOccure);
                } else {
                    // First Time
                    if (empty($lastOccure)) {
                        $_yesterday = strtotime('-1 day', $startTime);
                        $_curWeek = date('W', $startTime);
                        $_nextOccure = null;
                        foreach ($settings['weekly']['days'] as $key => $value) {
                            $_tmpOccure = strtotime('next ' . $value . date('H:i:s', $_yesterday), $_yesterday);
                            $_tmpWeek = date('W', $_tmpOccure);
                            if ($_tmpWeek !== $_curWeek)
                                $_tmpOccure = strtotime('+' . ($settings['weekly']['repeat_rate'] - 1) . ' week', $_tmpOccure);
                            if (empty($_nextOccure) || $_nextOccure > $_tmpOccure)
                                $_nextOccure = $_tmpOccure;
                        }
                        return $_nextOccure;
                    }
                    // Other times
                    $_nextOccure = null;
                    $_curWeek = date('W', $lastOccure);
                    foreach ($settings['weekly']['days'] as $key => $value) {
                        $_tmpOccure = strtotime('next ' . $value . date('H:i:s', $lastOccure), $lastOccure);
                        $_tmpWeek = date('W', $_tmpOccure);
                        if ($_tmpWeek !== $_curWeek)
                            $_tmpOccure = strtotime('+' . ($settings['weekly']['repeat_rate'] - 1) . ' week', $_tmpOccure);
                        if (empty($_nextOccure) || $_nextOccure > $_tmpOccure)
                            $_nextOccure = $_tmpOccure;
                    }
                    return $_nextOccure;
                }
                break;
            case self::REPEAT_TYPE__MONTHLY:
                // First Time
                if (empty($lastOccure))
                    return $startTime;
                // Other times
                return strtotime('+' . $settings['monthly']['repeat_rate'] . ' month', $lastOccure);
                break;
            default:
                return null;

        }
    }

    public static function getNotification($id)
    {
        $query = new Query();

        // get notification
        $data = $query->select(['*'])
            ->from('notification')
            ->where([
                'id' => $id,
                'is_deleted' => false])
            ->one();
        return $data;
    }

    public static function deleteNotification($id)
    {
        $query = new Query();
        // Validate params here


        try {
            // delete notification_receiver
            $data = $query->createCommand()->delete(
                'notification_receiver',
                [
                    'notification_id' => $id,
                ]
            )->execute();
            // delete notification_schedule
            $data = $query->createCommand()->update(
                'notification_schedule',
                [
                    'is_deleted' => true,
                ],
                [
                    'notification_id' => $id,
                ]
            )->execute();
            // delete notification
            $data = $query->createCommand()->update(
                'notification',
                [
                    'is_deleted' => true,
                ],
                [
                    'id' => $id,
                ]
            )->execute();
            return true;
        } catch (Exception $err) {
            return false;
        }
    }

    // Table notification_group
    public static function createGroup($params)
    {

    }

    public static function updateGroup($params)
    {

    }

    public static function deleteGroup($params)
    {

    }

    // Table notification_user_token
    public static function createUserToken($params)
    {

    }

    public static function updateUserToken($params)
    {

    }

    public static function deleteUserToken($params)
    {

    }

    // Table notification_receiver
    public static function createReceiver($params)
    {

    }

    public static function updateReceiver($params)
    {

    }

    public static function deleteReceiver($params)
    {

    }

    // CURL Trail before and after call
    protected function beforeCall(&$ch, &$data, $method = false)
    {
        // do nothing
        // TODO: init Curl Handler and preserve data for fucntion calling
        // Json encode data
        if (isset($data))
            $data = json_encode($data);

        else
            $data = json_encode([]);

        // Add header
        switch ($method) {
            case 'POST':
                curl_setopt($ch, CURLOPT_HTTPHEADER, [
                        'Content-Type: application/json',
                        'Authorization: key=' . Yii::$app->params['fcm']['_secretKey']]
                );
                break;
            case 'PUT':
                curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'Content-Length: ' . strlen($data)));
                break;
            default:
                break;
        }
    }

    protected function afterCall(&$response)
    {
        // do nothing
//        try {
//            $response = Json::decode($response);
//        } catch (InvalidParamException $e) {
//            echo $response;
//        }
    }

    private function _buildUrl($command, $params = null)
    {
        $url = $this->_apiUrl . $command;
        if ($params) {
            $url .= '?' . http_build_query($params);
        }
        return $url;
    }
}