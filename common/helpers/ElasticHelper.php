<?php
/**
 * Created by PhpStorm.
 * User: khanhphan
 * Date: 4/30/17
 * Time: 1:33 PM
 */

namespace common\helpers;


use Elasticsearch\Client;
use Yii;

class ElasticHelper
{

    public static function sendMultiRequest($requests, $method = 'search')
    {
        /**
         * @var Client $client
         */

        $client = Yii::$app->elasticsearch->client;

        $results = [];
        foreach ($requests as $key => &$request) {
            if (!isset($request['client'])) {
                $request['client'] = [
                    'future' => 'lazy'
                ];
            }
            $results[$key] = $client->$method($request);
        }
        $results[$key]->wait();
        return $results;
    }

    public static function sendRequest($request, $method='search')
    {
        $client = Yii::$app->elasticsearch->client;
        return $client->$method($request);

    }

}