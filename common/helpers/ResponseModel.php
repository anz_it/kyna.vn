<?php
/**
 * @author: Hong Ta
 * @return: JSON Object
 * @desc: Response json object.
 */
namespace common\helpers;

use yii\helpers\Json;


class ResponseModel {

    const SUCCESS_CODE = 0;

    const VALIDATION_ERROR_CODE = 2;

    public $message;

    public $error_code;

    public $object;

    public static function load($response_text, $model) {

        $response = json_decode($response_text, true);
        if ($response == null) {
            echo $response_text;
            die;
        }

        $resp = new self;
        if (isset($response['error_code'])) {
            $resp->error_code = $response['error_code'];
        }
        if (isset($response['message'])) {
            $resp->message = $response['message'];
        }

        if (!$resp->isError()) {
            if (!empty($response['object'])) {
                $model->setAttributes($response['object']);
                $resp->object = $model;
            }
        }


        return $resp;
    }

    /**
     * Response json encoded object into client
     *
     * @param  Model $object Response object
     * @return string  Json data
     */
    public static function response($object) {
        $resp = new self;
        $resp->error_code = self::SUCCESS_CODE;
        $resp->message = "Success";
        $resp->object = $object;
        return $resp;
    }
    /**
     * Response json encoded object to client for AJAX Request
     */
    public static function responseJson($object, $error_code = self::SUCCESS_CODE) {
        $resp = new self;
        $resp->error_code = $error_code;
        $resp->message = "Success";
        $resp->object = $object;
        return Json::encode($resp);
    }
    /**
     * Check if response is error response
     *
     * @return boolean
     */
    public function isError() {
        return $this->error_code != self::SUCCESS_CODE;
    }
    /**
     * Load object, return array object
     */
    public static function loadObject($resp, $model){
        $obj = null;
        foreach($resp as $key => $value){
            $newModel = new $model;
            $newModel->setAttributes($value->attributes, true);
            $obj[] = $newModel;
        }
        return $obj;
    }
    /**
     * Load single object, return array
     */
    public static function loadSingleObject($resp, $model){

        $newModel =  new $model;

        $newModel->setAttributes($resp->attributes, true);

        return $newModel;
    }
}