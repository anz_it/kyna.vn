<?php
/**
 * Created by PhpStorm.
 * User: ngunp
 * Date: 7/21/2017
 * Time: 10:47 AM
 */

namespace common\helpers;


use Yii;
use yii\helpers\Url;
class PaymentLinkHelper
{
    public static function generatePaymentLink($params)
    {
        // validate params here;
        if (!self::generatePaymentLinkValidateParams($params))
            return false;

        $queryParams = [];

        $courseIdString = implode(',', json_decode($params['course_ids'], true));
        $queryParams['id'] = $courseIdString;
        if (!empty($params['affiliate_id'])) {
            $queryParams['aff_id'] = $params['affiliate_id'];
        }
        if (!empty($params['code'])) {
            $queryParams['code'] = $params['code'];
        }
        if (!empty($params['payment_method'])) {
            $queryParams['payment_method'] = $params['payment_method'];
        }
        // hash queryParams + add hash vao queryParams
        $hash = DocumentHelper::hashParams($queryParams);
        $queryParams['hash'] = $hash;
        // them url path len dau queryparams
        array_unshift($queryParams, '/cart/checkout/index');

        $ret = Yii::$app->params['baseUrl'] . Url::to($queryParams);
        return $ret;
    }

    private static function generatePaymentLinkValidateParams($params)
    {
        return true;
    }

}