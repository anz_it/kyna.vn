<?php

namespace common\helpers;

class TreeHelper
{
    public static function buildTree($sections)
    {
        if (empty ($sections))
            return [];
        $result = [];
        $count = count($sections);
        $currentRoot = $sections[0];

        for ($i = 1; $i < $count; $i++) {
            if ($sections[$i]->lft > $currentRoot->lft && $sections[$i]->rgt < $currentRoot->rgt && $sections[$i]->root == $currentRoot->id) {
                //This is leaf
                $sections[$i]->lesson_coefficient = $sections[$i]->lessons;
                $currentRoot->children[] = $sections[$i];
            } else {
                $result[] = $currentRoot;
                $currentRoot = $sections[$i];
            }
        }
        $result[] = $currentRoot;
        return $result;

    }
    public static function dataMapper($models, $idKey = 'id', $parentKey = 'parent_id', $labelKey = 'name')
    {
        $_treeData = [];
        foreach ($models as $model) {
            $_data = $model->toArray([$idKey, $labelKey, $parentKey]);
            if (is_null($_data[$parentKey])) {
                $_data[$parentKey] = 0;
            }
            $_treeData[$_data[$idKey]] = $_data;
        }

        return $_treeData;
    }
    public static function createTree($treeData, $parentId = 0, $depth = 0, $parentKey = 'parent_id')
    {
        $tree = [];
        foreach ($treeData as $id => $item) {
            if ($item[$parentKey] == $parentId) {
                $item['depth'] = $depth;
                $tree[$id] = $item;
                $_children = self::createTree($treeData, $id, $depth + 1);
                if (sizeof($_children)) {
                    $tree[$id]['_children'] = $_children;
                }
            }
        }

        return $tree;
    }

    public static function dropdownOptions($models, $idKey = 'id', $parentKey = 'parent_id', $labelKey = 'name')
    {
        $_data = self::dataMapper($models, $idKey, $parentKey, $labelKey);

        return self::createOption($_data, 0, 0, $idKey, $parentKey, $labelKey);
    }
    public static function createOption($treeData, $parentId = 0, $depth = 0, $idKey = 'id', $parentKey = 'parent_id', $labelKey = 'name')
    {
        $options = [];
        foreach ($treeData as $id => $item) {
            $indent = str_repeat('–', $depth);
            if ($item[$parentKey] == $parentId) {
                $options[$id] = trim($indent.' '.$item[$labelKey]);
                $_children = self::createOption($treeData, $id, $depth + 1);
                if (sizeof($_children)) {
                    $options += $_children;
                }
            }
        }

        return $options;
    }
}
