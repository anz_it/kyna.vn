<?php
/**
 * @author: Hong Ta
 * @desc: data helper class
 */

namespace common\helpers;

class DataHelper{
    /**
     * Converts array of Model errors to summary string
     *
     * @param  array $errors array of Model errrors
     * @return string joined error string
     */
    public static function errorsToString($errors) {
        if (empty($errors)) {
            return '';
        }

        $result = [];

        foreach ($errors as $key => $messages) {
            $text = '';
            if (is_string($messages)) {
                $text .= $key . ': ' . $messages;
            } else {
                $text .= $key . ': ' . implode(', ', $messages);
            }
            array_push($result, $text);
        }

        return implode('; ', $result);
    }
}