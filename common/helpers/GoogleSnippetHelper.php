<?php
/**
 * Created by PhpStorm.
 * User: ngunp
 * Date: 5/11/2017
 * Time: 3:05 PM
 */

namespace common\helpers;


class GoogleSnippetHelper
{
    /**
     * Generate Breadcrumb Template
     * @param $url
     * @param $label
     * @param $position
     * @param bool $isActive
     * @return string
     */
    public static function renderBreadcrumbTemplate($url, $label, $position, $isActive=false)
    {
        if (!$isActive)
            return "<li itemprop='itemListElement' itemscope itemtype='http://schema.org/ListItem'>
                        <a itemscope itemtype='http://schema.org/Thing' itemprop='item' id='$url' href='$url'>
                            <span itemprop='name'>$label</span>
                            <meta itemprop='url' content=$url>
                        </a>
                        <meta itemprop='position' content='$position'>
                    </li>\n";
        else
            return "<li itemprop='itemListElement' itemscope itemtype='http://schema.org/ListItem' class='active'>
                        <span itemprop='name'>$label</span>
                        <meta itemprop='url' content='$url'>
                        <meta itemprop='position' content='$position'>
                    </li>\n";

    }

}