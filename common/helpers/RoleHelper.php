<?php

namespace common\helpers;

class RoleHelper
{
    
    const ROLE_ADMIN = 'Admin';
    const ROLE_USER = 'User';
    const ROLE_TESTER = 'Tester';
    
    const ROLE_TEACHER = 'Teacher';
    const ROLE_TEACHING_ASSISTANT = 'TeachingAssistant';
    
    const ROLE_COD = 'Cod';
    const ROLE_CUSTOMER_SERVICE = 'CustomerService';
    const ROLE_CUSTOMER_SERVICE_HANOI = 'CustomerServiceHaNoi';
    
    const ROLE_TELESALE = 'Telesale';
    const ROLE_TELESALE_LEADER = 'TelesaleLeader';
    
    const ROLE_ANALYZER = 'Analyzer';
    
    const ROLE_BUSINESS_DEVELOPMENT = 'BusinessDevelopment';
    
    const ROLE_CONTENT_DEVELOPMENT = 'ContentDevelopment';
    
    const ROLE_MARKETING = 'Marketing';
    
    const ROLE_RELATION = 'Relation';
    
    const ROLE_VIDEO = 'Video';
    
    const ROLE_MANAGER = 'Manager';
    
}
