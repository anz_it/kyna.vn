<?php

namespace common\helpers;

class CaptchaHelper
{
    const CLIENT_SESSION_LIMIT = 3;
    const SITE_KEY = '6Le7SRwUAAAAAFxH3mo-EgQP3yuinBbe3L97RZSA';
    const SECRET_KEY = '6Le7SRwUAAAAAB2xiKaSLHey-i6se1gfR_21PGOO';
    /**
     * @return int
     */
    public static function getClientSessionLimit() {
        $clientSessionLimit = self::CLIENT_SESSION_LIMIT;
        $params = \Yii::$app->params;
        if (isset($params['re-captcha']) && isset($params['re-captcha']['client_session_limit'])) {
            $clientSessionLimit = $params['re-captcha']['client_session_limit'];
        }
        return $clientSessionLimit;
    }

    /**
     * @param string $key
     */
    public static function setClientSession($key = 'lp_client_session') {
        $clientSession = 1;
        if (\Yii::$app->session->has($key)) {
            $clientSession = \Yii::$app->session->get($key) + 1;
        }
        \Yii::$app->session->set($key, $clientSession);
    }

    /**
     * @param string $key
     * @return int|mixed
     */
    public static function getClientSession($key = 'lp_client_session') {
        $clientSession = 1;
        if (\Yii::$app->session->has($key)) {
            $clientSession = \Yii::$app->session->get($key);
        }
        return $clientSession;
    }

    /**
     * @param string $key
     * @return bool
     */
    public static function getIsEnable($key = 'lp_client_session') {
        if (self::getClientSession($key) > self::getClientSessionLimit()) {
            return true;
        }
        return false;
    }

    /**
     * @return array
     */
    public static function getReCaptchaParams() {
        $result = [
            'site_key' => self::SITE_KEY,
            'secret_key' => self::SECRET_KEY
        ];
        $params = \Yii::$app->params;
        if (isset($params['re-captcha'])) {
            $result['site_key'] = isset($params['re-captcha']['site_key']) ? $params['re-captcha']['site_key']: $result['site_key'];
            $result['secret_key'] = isset($params['re-captcha']['secret_key']) ? $params['re-captcha']['secret_key']: $result['secret_key'];
        }
        return $result;
    }

    /**
     * @param $post
     * @return bool
     */
    public static function verifyReCaptcha($post) {
        $reCaptchaParams = CaptchaHelper::getReCaptchaParams();
        if(isset($post['g-recaptcha-response']) && !empty($post['g-recaptcha-response'])) {
            $verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret=' . $reCaptchaParams['secret_key'] . '&response=' . $post['g-recaptcha-response']);
            $responseData = json_decode($verifyResponse);
            if ($responseData->success) {
                return true;
            }
        }
        return false;
    }
}