<?php
/**
 * Created by IntelliJ IDEA.
 * User: trangnqd
 * Date: 1/9/2017
 * Time: 10:49 AM
 */

namespace common\helpers;

class FileHelper
{

    public static function createFile($path)
    {
        clearstatcache();
        if (!file_exists($path)) {
            touch($path);
        }
    }

}
