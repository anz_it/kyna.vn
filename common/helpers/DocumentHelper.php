<?php

namespace common\helpers;

class DocumentHelper
{
    const DOCUMENT_KEY = 'qVBOdgl6ID2ZUmNe';

    public static function hashParams($queryParams)
    {
        if (!is_array($queryParams)) {
            return null;
        }

        $documentKey = self::DOCUMENT_KEY;
        $params = \Yii::$app->params;
        if (isset($params['document_key'])) {
            $documentKey = $params['document_key'];
        }

        if (isset($queryParams['hash'])) {
            unset($queryParams['hash']);
        }

        $queryParams['key'] = $documentKey;
        ksort($queryParams);
        $queryParams = http_build_query($queryParams);

        return hash('sha256', $queryParams);
    }
}
