<?php
/**
 * Created by PhpStorm.
 * User: nguyenphanngu
 * Date: 10/27/17
 * Time: 1:37 PM
 */

namespace common\helpers;


use kyna\servicecaller\traits\CurlTrait;
use kyna\settings\models\Setting;
use kyna\user\models\UserTelesale;
use yii\db\Exception;
use yii\helpers\Json;

use Yii;

class FbOfflineConversionHelper
{
    // Source : https://developers.facebook.com/docs/marketing-api/offline-conversions/

    use CurlTrait;

    public $_options = [
        'accessToken' => '',
        'apiVersion' => '',
        'systemUserId' => '',
        'business_manager_id' => '',
        'data_set_id' => '',
        'upload_tag' => '',
    ];

    function __construct()
    {
        $this->_options['accessToken'] = isset(Yii::$app->params['fb_offline_conversion']['accessToken']) ? Yii::$app->params['fb_offline_conversion']['accessToken'] : '';
        $this->_options['apiVersion'] = isset(Yii::$app->params['fb_offline_conversion']['apiVersion']) ? Yii::$app->params['fb_offline_conversion']['apiVersion'] : '';
        $this->_options['systemUserId'] = isset(Yii::$app->params['fb_offline_conversion']['systemUserId']) ? Yii::$app->params['fb_offline_conversion']['systemUserId'] : '';
        $this->_options['business_manager_id'] = isset(Yii::$app->params['fb_offline_conversion']['business_manager_id']) ? Yii::$app->params['fb_offline_conversion']['business_manager_id'] : '';

        $datasetId = Setting::findOne([
            'key' => 'seo_fbofflineconversion_data_set_id'
        ]);
        if (!empty($datasetId) && !empty($datasetId->value)) {
            $this->_options['data_set_id'] = $datasetId->value;
        } else {
            $this->_options['data_set_id'] = isset(Yii::$app->params['fb_offline_conversion']['data_set_id']) ? Yii::$app->params['fb_offline_conversion']['data_set_id'] : '';
        }

        $this->_options['upload_tag'] = isset(Yii::$app->params['fb_offline_conversion']['upload_tag']) ? Yii::$app->params['fb_offline_conversion']['upload_tag'] : '';


    }
//=============================================== API =================================================================
    public static function CreateEventSet($params)
    {
        $ret = [];
        $Fb = new FbOfflineConversionHelper();

        // Validate $_options here
        if (!$Fb->_validate()) {
            $ret['success'] = false;
            $ret['message'] = "invalid options";
            return $ret;
        }

        // Validate params here
        if (empty($params['name'])) {
            $ret['success'] = false;
            $ret['message'] = "offline_event name missing";
            return $ret;
        }
        if (empty($params['description'])) {
//            $ret['success'] = false;
//            $ret['message'] = "offline_event name missing";
//            return $ret;
            $params['description'] = 'no description';
        }

        // Prepare CURL params
        $endpoint = "https://graph.facebook.com/" . $Fb->_options['apiVersion'] . "/" . $params['business_manager_id'] . "offline_conversion_data_sets";

        $body = [
            'access_token' => $Fb->_options['accessToken'],
            'name' => $params['name'],
            'description' => $params['description'],
        ];

        $response = $Fb->call($endpoint, $body, 'POST');
//        var_dump($response);
    }

    public static function UploadEvents($params)
    {
        $ret = [];

        $Fb = new FbOfflineConversionHelper();

        if (!$Fb->_validate()) {
            $ret['success'] = false;
            $ret['message'] = "invalid options";
            return $ret;
        }

        // Validate params here
        // Neu khong truyen upload_tag thi lay upload_tag trong option, option khong co luon thi return false
        if (empty($params['upload_tag'])) {
            if (empty($Fb->_options['upload_tag'])) {
                $ret['success'] = false;
                $ret['message'] = "upload_tag missing";
                return $ret;
            }
            $params['upload_tag'] = $Fb->_options['upload_tag'];
        }
        // Neu khong truyen data_set_id thi lay data_set_id trong option, option khong co luon thi return false
        if (empty($params['data_set_id'])) {
            if (empty($Fb->_options['data_set_id'])) {
                $ret['success'] = false;
                $ret['message'] = "data_set_id missing";
                return $ret;
            }
            $params['data_set_id'] = $Fb->_options['data_set_id'];
        }
        if (empty($params['data'])) {
            $ret['success'] = false;
            $ret['message'] = "data missing";
            return $ret;
        }

        // Prepare curl params
        $endpoint = "https://graph.facebook.com/" . $Fb->_options['apiVersion'] . "/". $params['data_set_id'] ."/events";

        $body = [
            'access_token' => $Fb->_options['accessToken'],
            'upload_tag' => $params['upload_tag'],
            'data' => $params['data'],
        ];

        $response = $Fb->call($endpoint, $body, 'POST');
        if (isset($response['error'])) {
            $ret['success'] = false;
            $ret['message'] = "Something gone wrong";
            $ret['data'] = $response;
            return $ret;
        }

        $ret['success'] = true;
        $ret['message'] = "Upload successfully";
        $ret['data'] = $response;
        return $ret;
    }

//================================================= END API ============================================================

    protected function beforeCall(&$ch, &$data, $method=false)
    {
        // do nothing
        // TODO: init Curl Handler and preserve data for fucntion calling
        // Json encode data
        if (isset($data))
            $data = json_encode($data);
        else
            $data = json_encode([]);

        // Add
        switch ($method) {
            case 'POST':
                curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
                break;
            case 'PUT':
                curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json','Content-Length: ' . strlen($data)));
                break;
            default:
                break;
        }
    }

    protected function afterCall(&$response)
    {
        try {
            $response = Json::decode($response);
        } catch (Exception $err) {
            var_dump($err);
            return false;
        }
    }

    private function _buildUrl($command, $params=null)
    {
        $url = $this->_apiUrl.$command;
        if ($params) {
            $url .= '?'.http_build_query($params);
        }
        return $url;
    }

    private function _validate()
    {
        return (
            !empty($this->_options['accessToken'])
            && !empty($this->_options['apiVersion'])
            && !empty($this->_options['systemUserId'])
            && !empty($this->_options['business_manager_id'])
        );
    }


}