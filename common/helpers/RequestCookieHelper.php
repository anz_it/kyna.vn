<?php
/**
 * Created by PhpStorm.
 * User: nguyenphanngu
 * Date: 10/12/17
 * Time: 10:08 AM
 */

namespace common\helpers;

use Yii;
use yii\web\Cookie;

class RequestCookieHelper
{
    const MAX_REQUEST_COUNT = 10;

    const LAST_REQUEST_COOKIE_DURATION = 30;                     // 30 seconds
    const CURRENT_REQUEST_COOKIE_DURATION = 14*24*60*60;        // 2 weeks


    public static function refreshCookie()
    {
        $user = Yii::$app->user;

        if (empty($user->identity)) {
            return false;
        }

        // Cache
        $currentCacheKey = $user->id. '_request_current';
        $lastCacheKey = $user->id. '_request_last';

        $cachedCurrentRequestCookie = Yii::$app->cache->get($currentCacheKey);
//        $cachedLastCacheRequestCookie = Yii::$app->cache->get($lastCacheKey);

            // update last cookie = current cookie

        if (!empty($cachedCurrentRequestCookie)) {
//            $cachedLastCacheRequestCookie = $cachedCurrentRequestCookie;
            Yii::$app->cache->set(
                $lastCacheKey,
                $cachedCurrentRequestCookie,
                self::LAST_REQUEST_COOKIE_DURATION
            );
        }
            // Generate new cookie
//        $newCookie = $requestCurrentCookieKey = hash('sha256', $user->id . time());
        $newCookie = hash('sha256', $user->id . time());

            // update current cookie
        Yii::$app->cache->set(
            $currentCacheKey,
            [
                'cookie' => $newCookie,
                'value' => 0,
            ],
            self::CURRENT_REQUEST_COOKIE_DURATION
        );

        // Response cookie
        $cookie = new Cookie([
            'name' => '_request',
            'path' => '/',
            'domain' => Yii::$app->params['cookie_request']['domain'],
            'value' => $newCookie,
            'expire' => time() + self::CURRENT_REQUEST_COOKIE_DURATION
        ]);
        Yii::$app->response->cookies->add($cookie);
    }

    public static function removeCookie($id = null)
    {
        if (empty($id)) {
            $user = Yii::$app->user;

            if (empty($user->identity)) {
                return false;
            }
            $id = $user->id;
        }

        // Cache
        $currentCacheKey = $id. '_request_current';
        $lastCacheKey = $id. '_request_last';

//        $cachedCurrentRequestCookie = Yii::$app->cache->get($currentCacheKey);
//        $cachedLastCacheRequestCookie = Yii::$app->cache->get($lastCacheKey);

        // remove from cache (set remove after 5s if necessary)

        Yii::$app->cache->delete($currentCacheKey);
        Yii::$app->cache->delete($lastCacheKey);

        // delete _request cookie;
        $cookie = new Cookie([
            'name' => '_request',
            'path' => '/',
            'domain' => Yii::$app->params['cookie_request']['domain'],
            'value' => '',
            'expire' => time() - 1000
        ]);
        Yii::$app->response->cookies->add($cookie);
    }
}