<?php
/**
 * @author: Hong Ta
 * @desc: Response error code.
 * @return: error_code
 */
namespace common\helpers;

use common\helpers\DataHelper;

class ResponseError{
    const SUCCESS_CODE = 0;
    const ERROR_SERVER_BUSY = 1;
    const NOT_FOUND = 2;
    const UNKNOWN_ERROR_MESSAGE = 'Unknown error';
    /**
     * Error code messages
     *
     * @return array Error code => message
     */
    public static function errorMessages() {
        return [
            self::SUCCESS_CODE => 'Success',
            self::ERROR_SERVER_BUSY => 'Server Busy',
            self::NOT_FOUND => 'Not found',
        ];
    }
    /**
     * Get error message from code
     *
     * @param  int $code Error code
     *
     * @return string Error message
     */
    public static function getErrorMessage($code) {
        $labels = self::errorMessages();

        if (empty($labels[$code])) {
            return self::UNKNOWN_ERROR_MESSAGE;
        }

        return $labels[$code];
    }

    /**
     * Responses error data
     *
     * @param int $code Error code
     * @param string $message Error message
     *
     * @return array error data
     */
    public static function response($code, $message = null, $params = null) {
        if (empty($message)) {
            $message = self::getErrorMessage($code);
        }

        $resp = new ResponseModel;
        $resp->error_code = $code;
        $resp->message = $message;
        if (!empty($params)) {
            $resp->message = DataHelper::errorsToString($params);
        }

        return $resp;
    }
}