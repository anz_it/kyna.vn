<?php

namespace common\helpers;

use yii\helpers\HtmlPurifier;

class StringHelper extends \yii\helpers\StringHelper
{
    const STRING_TYPE_EMAIL = 1;
    const STRING_TYPE_PHONE_NUMBER = 2;
    const STRING_TYPE_ORDER_CODE = 3;
    const STRING_TYPE_LOCATION = 4;

    public static function startWith($needle, $haystack)
    {
        return $needle === '' || strrpos($haystack, $needle, -strlen($haystack)) !== false;
    }

    public static function isEmail($string)
    {
        return filter_var($string, FILTER_VALIDATE_EMAIL) !== false;
    }

    public static function toSlug($value)
    {
        $value = preg_replace('~[^\pL\d]+~u', '-', $value);
        // transliterate
        $value = iconv('utf-8', 'us-ascii//TRANSLIT', $value);
        // remove unwanted characters
        $value = preg_replace('~[^-\w]+~', '', $value);
        // trim
        $value = trim($value, '-');
        // remove duplicate -
        $value = preg_replace('~-+~', '-', $value);
        // lowercase
        $value = strtolower($value);
        if (!empty($value)) {
            return $value;
        }

        return false;
    }

    public static function slugify($value)
    {
        $value = self::removeSign($value);
        $value = preg_replace('~[^\pL\d]+~u', '-', $value);
        // transliterate
        $value = iconv('utf-8', 'us-ascii//TRANSLIT', $value);
        // remove unwanted characters
        $value = preg_replace('~[^-\w]+~', '', $value);
        // trim
        $value = trim($value, '-');
        // remove duplicate -
        $value = preg_replace('~-+~', '-', $value);
        // lowercase
        $value = strtolower($value);
        if (!empty($value)) {
            return $value;
        }
        return false;

    }

    public static function isLocation($string)
    {
        return self::startWith('@', $string) !== false;
    }

    public static function typeDetect($string)
    {
        if (self::isEmail($string)) {
            return self::STRING_TYPE_EMAIL;
        }
        if (is_numeric($string) && StringHelper::isPhoneNumber($string)) {
            return self::STRING_TYPE_PHONE_NUMBER;
        }
        if (self::isLocation($string)) {
            return self::STRING_TYPE_LOCATION;
        }

        return false;
    }

    public static function isPhoneNumber($number){
        if (preg_match('/^(\+84[1-9]|0[1-9])[0-9]{8,9}$/', $number))
            return true;
        else{
            return false;
        }
    }

    public static function random($length = 6, $mixedCase = false)
    {
        $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        if ($mixedCase) {
            $characters .= 'abcdefghijklmnopqrstuvwxyz';
        }
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; ++$i) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }

        return $randomString;
    }

    public static function htmlContentFilter($string)
    {
        $config = self::htmlPurifierConfig('advanced');
        $string = HtmlPurifier::process($string, $config);
        return $string;
    }

    public static function htmlContentFilterNofollow($string)
    {
        $config = self::htmlPurifierConfig('advanced');
        $string = HtmlPurifier::process($string, $config);
        // remove nofollow for all kyna.vn link
        if (!empty($string)) {
            $string = self::filterRel($string, 'kyna.vn');
        }
        return $string;
    }

    /**
     * Remove nofollow with specific host
     * @param $string
     * @param $host
     * @return string
     */
    private static function filterRel($string, $host)
    {
        $removeRelArray = array('nofollow', 'noreferrer');
        $dom = new \DOMDocument();
        $dom->preserveWhitespace = FALSE;
        $dom->loadHTML(mb_convert_encoding($string, 'HTML-ENTITIES', 'UTF-8'), LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
        $aEl = $dom->getElementsByTagName('a');
        foreach ($aEl as $anchor) {
            $href = $anchor->attributes->getNamedItem('href')->nodeValue;
            if (preg_match('/https?:\/\/(www.)?' . $host . '[\w\.\/\-=?#]+/', $href, $results)) {
                $oldRelAtt = $anchor->attributes->getNamedItem('rel');
                if ($oldRelAtt != NULL) {
                    $oldRel = $oldRelAtt->nodeValue;
                    $oldRel = explode(' ', $oldRel);
                    foreach ($removeRelArray as $removeRel) {
                        if (in_array($removeRel, $oldRel)) {
                            foreach ($oldRel as $key => $relVal) {
                                if ($relVal == $removeRel) {
                                    unset($oldRel[$key]);
                                }
                            }
                        }
                    }
                    $newRel = implode($oldRel, ' ');
                }
                $newRelAtt = $dom->createAttribute('rel');
                $noFollowNode = $dom->createTextNode($newRel);
                $newRelAtt->appendChild($noFollowNode);
                $anchor->appendChild($newRelAtt);
            }
        }
        return $dom->saveHTML($dom->documentElement);
    }

    private static function htmlPurifierConfig($scope = 'advanced', $nofollow = true)
    {
        $config = [
            'Attr.AllowedClasses' => [],
            'Attr.AllowedFrameTargets' => ['_blank'],
            'Attr.DefaultImageAlt' => 'Kyna.vn\'s image',
            'Attr.DefaultInvalidImageAlt' => 'No Image',

            'AutoFormat.Linkify' => true,
            'AutoFormat.RemoveEmpty' => true,
            'AutoFormat.RemoveEmpty.RemoveNbsp' => true,
            'AutoFormat.RemoveSpansWithoutAttributes' => true,

            'CSS.AllowedFonts' => [],
            'CSS.AllowedProperties' => ['color', 'background-color', 'font-size', 'text-decoration', 'font-style', 'font-weight', 'float', 'margin-left', 'margin-right', 'border'],
            'CSS.MaxImgLength' => '800px',

            'Core.RemoveProcessingInstructions' => true,

            'HTML.FlashAllowFullScreen' => true,
            'HTML.ForbiddenAttributes' => ['*@on*', '*@data-*'],
            'HTML.Nofollow' => $nofollow,
            'HTML.SafeIframe' => true,
            'HTML.TargetBlank' => true,

            'Output.FlashCompat' => true,
            'Output.TidyFormat' => true,

            'URI.AllowedSchemes' => [
                'http' => true,
                'https' => true,
                'mailto' => true,
                'data' => true,
                'tel' => true
            ]
        ];
        if ($scope === 'advanced') {
            $config['AutoFormat.AutoParagraph'] = true;
            $config['HTML.AllowedElements'] = [
                'a', 'b', 'u', 'i', 's', 'span', 'iframe',
                'strike', 'strong', 'em', 'ins', 'del',
                'img', 'br',
                'p', 'ul', 'ol', 'li', 'blockquote',
                'h1', 'h2', 'h3', 'h4', 'h5', 'h6',
                'table', 'th', 'td', 'tr', 'thead', 'tfoot', 'tbody',
            ];
        } else {
            $config['HTML.AllowedElements'] = [
                'a', 'b', 'u', 'i', 's', 'strike', 'strong', 'em', 'ins', 'del', 'br', 'span', 'p'
            ];
        }
        return $config;
    }

    public static function getHomeUrl()
    {
        $domain = $_SERVER['HTTP_HOST'];
        $domainArray = explode('.', $domain);
        if (is_array($domainArray) && count($domainArray) > 2) {
            unset($domainArray[0]);
            $domain = implode('.', $domainArray);
        }
        return "//{$domain}";
    }

    public static function removeSign($str)
    {
        $unicode = array(
            'a'=>'á|à|ả|ã|ạ|ă|ắ|ặ|ằ|ẳ|ẵ|â|ấ|ầ|ẩ|ẫ|ậ',
            'd'=>'đ',
            'e'=>'é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ',
            'i'=>'í|ì|ỉ|ĩ|ị',
            'o'=>'ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ',
            'u'=>'ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự',
            'y'=>'ý|ỳ|ỷ|ỹ|ỵ',
            'A'=>'Á|À|Ả|Ã|Ạ|Ă|Ắ|Ặ|Ằ|Ẳ|Ẵ|Â|Ấ|Ầ|Ẩ|Ẫ|Ậ',
            'D'=>'Đ',
            'E'=>'É|È|Ẻ|Ẽ|Ẹ|Ê|Ế|Ề|Ể|Ễ|Ệ',
            'I'=>'Í|Ì|Ỉ|Ĩ|Ị',
            'O'=>'Ó|Ò|Ỏ|Õ|Ọ|Ô|Ố|Ồ|Ổ|Ỗ|Ộ|Ơ|Ớ|Ờ|Ở|Ỡ|Ợ',
            'U'=>'Ú|Ù|Ủ|Ũ|Ụ|Ư|Ứ|Ừ|Ử|Ữ|Ự',
            'Y'=>'Ý|Ỳ|Ỷ|Ỹ|Ỵ',

        );
        foreach($unicode as $nonUnicode=>$uni){
            $str = preg_replace("/($uni)/i", $nonUnicode, $str);
        }

        return $str;
    }

    public static function getUnicodeArray ()
    {
        return array(
            'a'=>'[aáàảãạăắặằẳẵâấầẩẫậ]',
            'd'=>'[dđ]',
            'e'=>'[eéèẻẽẹêếềểễệ]',
            'i'=>'[iíìỉĩị]',
            'o'=>'[oóòỏõọôốồổỗộơớờởỡợ]',
            'u'=>'[uúùủũụưứừửữự]',
            'y'=>'[yýỳỷỹỵ]',

            'A'=>'[AÁÀẢÃẠĂẮẶẰẲẴÂẤẦẨẪẬ]',
            'D'=>'[DĐ]',
            'E'=>'[EÉÈẺẼẸÊẾỀỂỄỆ]',
            'I'=>'[IÍÌỈĨỊ]',
            'O'=>'[OÓÒỎÕỌÔỐỒỔỖỘƠỚỜỞỠỢ]',
            'U'=>'[UÚÙỦŨỤƯỨỪỬỮỰ]',
            'Y'=>'[YÝỲỶỸỴ]',
        );
    }

    public static function getUnicodeCharaters () {
        return 'aáàảãạăắặằẳẵâấầẩẫậdđeéèẻẽẹêếềểễệiíìỉĩịoóòỏõọôốồổỗộơớờởỡợuúùủũụưứừửữựyýỳỷỹỵAÁÀẢÃẠĂẮẶẰẲẴÂẤẦẨẪẬDĐEÉÈẺẼẸÊẾỀỂỄỆIÍÌỈĨỊOÓÒỎÕỌÔỐỒỔỖỘƠỚỜỞỠỢUÚÙỦŨỤƯỨỪỬỮỰYÝỲỶỸỴ';
    }

    // Add sign cho str
    // E.g. tieng => t[iíìỉĩị][eéèẻẽẹêếềểễệ]ng
    public static function addSign($str)
    {
        // removesign
        $rmSignStr = self::removeSign($str);
        // get unicode array
        $unicode = self::getUnicodeArray();
        // replace sign and return
        return str_replace(array_keys($unicode), array_values($unicode), $rmSignStr);
    }
    public static function getYouTubeIdFromURL($url)
    {
      $pattern = '%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i';
      preg_match($pattern, $url, $matches);

      return isset($matches[1]) ? $matches[1] : false;
    }

    public static function standardPhonumber($phoneNumber)
    {
        $phone = $phoneNumber;
        $phone = preg_replace("/\D/", '', $phone);      // get number only
        $phone = preg_replace("/^0/", "84", $phone);    // if first character = 0, replace to 84
        return $phone;
    }

    public static function randomNumber($length = 6)
    {
        $characters = '0123456789';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; ++$i) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }

        return $randomString;
    }
}
