<?php

namespace common\helpers;

use common\lib\CDNImage;
use Yii;
use yii\helpers\Html as YiiHtml;


class CDNHelper
{
    const RESIZE_MODE_CROP = 'crop';
    const RESIZE_MODE_CONTAIN = 'contain';
    const RESIZE_MODE_COVER = 'cover';
    const RESIZE_MODE_ORIGINAL = 'original';

    const IMG_SIZE_ORIGINAL = '0x0';
    const IMG_SIZE_THUMBNAIL = '263x147';
    const IMG_SIZE_THUMBNAIL_SMALL = '160x90';
    const IMG_SIZE_AVATAR = '60x60';
    const IMG_SIZE_AVATAR_SMALL = '35x35';
    const IMG_SIZE_AVATAR_LARGE = '120x120';
    const IMG_SIZE_THUMBNAIL_YOUTUBE = '560x349';
    const IMG_SIZE_THUMBNAIL_YOUTUBE_LARGE = '730x436';
    const IMG_SIZE_CATEGORY_HOME_ICON = '55x55';
    const IMG_SIZE_CATEGORY_MENU_ICON_DESKTOP = '30x30';
    const IMG_SIZE_CATEGORY_MENU_ICON_MOBILE = '55x55';
    const IMG_SIZE_TAG_DESKTOP = '215x125';
    const IMG_SIZE_TAG_MOBILE = '165x80';
    const IMG_SIZE_HOME_TEACHER = '320x500';

    const IMG_RETURN_MODE_IMG_TAG = 'img';
    const IMG_RETURN_MODE_URL = 'url';

    public static $allowedMimeTypes = [
        'image/png' => '.png',
        'image/jpeg' => '.jpg',
    ];

    public static $mediaUrl = '/';
    public static $staticUrl = 'https://static.kyna.vn';

    public static function init()
    {
        if (isset(Yii::$app->params['media_link'])) {
            self::$mediaUrl = Yii::$app->params['media_link'];
        }
        if (isset(Yii::$app->params['static_link'])) {
            self::$staticUrl = Yii::$app->params['static_link'];
        }
    }

    private static $_options = [
        'size' => false,
        'resizeMode' => self::RESIZE_MODE_COVER,
        'forceCreate' => false,
        'fixedRatio' => false,
        'returnMode' => self::IMG_RETURN_MODE_IMG_TAG,

        'title' => false,
        'alt' => false,
        'class' => false,
    ];

    public static function image($originalFile, $options = [])
    {
        if (empty($originalFile)) {
            return;
        }

        self::init();
        $opt = ArrayHelper::extend([], self::$_options, $options);
        $returnMode = $opt['returnMode'];

        if (in_array($originalFile, ["/src/img/default.png", "/img/default_avatar.png"])) {
            if ($returnMode === self::IMG_RETURN_MODE_URL) {
                return self::$mediaUrl . $originalFile;
            } else {
                return YiiHtml::img(self::$mediaUrl . $originalFile, $opt);
            }
        }

        $originalFile = trim($originalFile, "/");

        $returnFile = self::_getResizedImage($originalFile, $opt['size'], $opt['resizeMode']);

        if (!empty($opt['suffix'])) {
            $returnFile .= '?_=' . $opt['suffix'];
        }

        if ($returnMode === self::IMG_RETURN_MODE_URL) {
            return self::$mediaUrl . '/' . $returnFile;
        }

        if (!$opt['alt']) {
            $opt['alt'] = $returnFile;
        }
        if (!$opt['title']) {
            $opt['title'] = $opt['alt'];
        }

        unset($opt['size']);
        unset($opt['suffix']);
        unset($opt['resizeMode']);
        unset($opt['fixedRatio']);
        unset($opt['returnMode']);
        unset($opt['forceCreate']);

        return YiiHtml::img(self::$mediaUrl . '/' . $returnFile, $opt);
    }

    private static function _getResizedImage($originalFile, $size = false, $resizeMode = false)
    {
        $size = self::_getAllowedSize($size);

        if (!$resizeMode) {
            $resizeMode = self::RESIZE_MODE_COVER;
        }

        if ($resizeMode == self::RESIZE_MODE_ORIGINAL) {
            return $originalFile;
        }

        $info = pathinfo(self::$staticUrl . '/' . $originalFile);
        if ($size == self::IMG_SIZE_ORIGINAL) {
            $filename = $info['filename'] . '.' . 'original'. '.' . $info['extension'];
        } else {
            $filename = $info['filename'] . '.' . $resizeMode . '-' . $size . '.' . $info['extension'];
        }

        $file = str_replace($info['basename'], $filename, $originalFile);

        return $file;
    }

    private static function _allowedSizes()
    {
        return [
            self::IMG_SIZE_ORIGINAL,
            self::IMG_SIZE_THUMBNAIL,
            self::IMG_SIZE_THUMBNAIL_SMALL,
            self::IMG_SIZE_AVATAR,
            self::IMG_SIZE_AVATAR_LARGE,
            self::IMG_SIZE_AVATAR_SMALL,
            self::IMG_SIZE_THUMBNAIL_YOUTUBE,
            self::IMG_SIZE_THUMBNAIL_YOUTUBE_LARGE,
            self::IMG_SIZE_CATEGORY_HOME_ICON,
            self::IMG_SIZE_CATEGORY_MENU_ICON_DESKTOP,
            self::IMG_SIZE_CATEGORY_MENU_ICON_MOBILE,
            self::IMG_SIZE_TAG_DESKTOP,
            self::IMG_SIZE_TAG_MOBILE,
            self::IMG_SIZE_HOME_TEACHER,
        ];
    }

    private static function _getAllowedSize($size = false)
    {
        $allowedSize = self::_allowedSizes();
        if (!in_array($size, $allowedSize)) {
            $size = self::IMG_SIZE_ORIGINAL;
        }

        return $size;
    }

    public static function getMediaLink()
    {
        self::init();
        return self::$mediaUrl;
    }
}
