<?php

namespace common\helpers;

class FontAwesomeHelper {
    public static function file($mime_type, $faOptions = [], $options = [], $colored = true) {
        $color = false;
        switch ($mime_type) {
            case 'application/pdf':
            case 'application/x-pdf':
            case 'pdf':
                $name = 'file-pdf-o';
                $color = '#c00';
                break;

            case 'text/plain':
            case 'txt':
            default:
                $name = 'file-text-o';
                break;
        }

        if ($colored and $color) {
            $options['style'] = 'color:'.$color;
        }

        return self::icon($name, $faOptions, $options);
    }

    /*
    $options = [
        'pull-left'|'pull-right',
        'fw', //fixed-width
        'li',
        'border',
        'spin'
    ]
     */
    public static function icon($name, $faOptions = [], $options = []) {
        foreach ($faOptions as &$option) {
            $option = 'fa-'.$option;
        }

        $extClass = '';
        if (isset($options['class'])) {
            $extClass = $options['class'];
            unset($options['class']);
        }

        $attr = '';
        foreach ($options as $key => $value) {
            $attr .= ' '.$key.'="'.$value.'"';
        }

        $html = '<i class="fa %s %s%s" aria-hidden="true"%s></i>';
        return sprintf($html, 'fa-'.$name, implode(' ', $faOptions), $extClass, $attr);
    }
}
