<?php

namespace common\helpers;

use libphonenumber\PhoneNumberUtil;
use libphonenumber\PhoneNumberFormat;
use libphonenumber\PhoneNumberToCarrierMapper;

class PhoneNumberHelper
{

    const VIETTEL = 'VTT';
    const MOBIPHONE = 'VMS';
    const VINAPHONE = 'VNP';

    private static function _getProto($number)
    {
        return PhoneNumberUtil::getInstance()->parse($number, 'VN');
    }

    public static function isPhoneNumber($number)
    {
        $proto = self::_getProto($number);
        return PhoneNumberUtil::getInstance()->isValidNumber($proto);
    }

    public static function formatPhoneNumber($number)
    {
        $proto = self::_getProto($number);
        return PhoneNumberUtil::getInstance()->format($proto, PhoneNumberFormat::NATIONAL);
    }

    public static function getCarrier($number)
    {
        $proto = self::_getProto($number);
        return PhoneNumberToCarrierMapper::getInstance()->getNameForNumber($proto, 'vn');
    }

    public static function detectProviderCode($phone)
    {
        $viettels = [
            '098', '097', '096', '0169', '0168', '0167', '0166', '0165', '0164', '0163', '0162', '086'
        ];
        $mobi_phones = [
            '090', '093', '0122', '0126', '0128', '0121', '0120', '089'
        ];
        $vina_phones = [
            '091', '094', '0123', '0124', '0125', '0127', '0129', ' 088'
        ];

        $phone_number_prefix = explode(substr($phone, -7), $phone);

        if (in_array($phone_number_prefix[0], $viettels)) {
            return self::VIETTEL;
        }
        if (in_array($phone_number_prefix[0], $mobi_phones)) {
            return self::MOBIPHONE;
        }
        if (in_array($phone_number_prefix[0], $vina_phones)) {
            return self::VINAPHONE;
        }

        return null;
    }

    public static function getServiceOptions()
    {
        return [
            self::VIETTEL => 'Viettel',
            self::MOBIPHONE => 'Mobiphone',
            self::VINAPHONE => 'Vinaphone',
        ];
    }

    public static function getServiceName($code)
    {
        $options = self::getServiceOptions();

        return isset($options[$code]) ? $options[$code] : null;
    }

}
