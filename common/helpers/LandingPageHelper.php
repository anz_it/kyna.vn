<?php
/**
 * Created by IntelliJ IDEA.
 * User: trangnqd
 * Date: 1/9/2017
 * Time: 10:49 AM
 */

namespace common\helpers;

use Yii;
use kyna\page\models\Page;

class LandingPageHelper
{
    public static function getView(Page $page)
    {
        // stuck: init old common template
        self::initCommonTemplates();
        $viewPath = "@media/pages/{$page->slug}";
        $viewPathAlias = Yii::getAlias($viewPath);
        $viewFileName = "index-{$page->updated_time}";
        $viewFileExt = ".php";
        $viewFile = "{$viewPathAlias}/{$viewFileName}{$viewFileExt}";;
        // create view folder
        if (!file_exists($viewPathAlias) && !is_dir($viewPathAlias)) {
            mkdir($viewPathAlias, 0777, true);
        }
        // create view file
        if (!file_exists($viewFile)) {
            array_map('unlink', glob("{$viewPathAlias}/index-*"));
            $fp = fopen($viewFile,"wb");
            fwrite($fp, $page->content);
            fclose($fp);
        }
        return "{$viewPath}/{$viewFileName}";
    }

    private static function initCommonTemplates()
    {
        $viewPath = Yii::getAlias("@media/pages/common/templates");
        if (!file_exists($viewPath) && !is_dir($viewPath)) {
            mkdir($viewPath, 0777, true);
            $lisTemplate = [
                'common-clock.php',
                'common-head-loading.php',
                'common-head.php',
                'common-metatags.php',
                'common-metatags-new.php',
                'footer.php',
                'footer-new.php',
                'head.php',
                'payment.php',
                'payment-small.php',
                'watch.php',
                'zopim.php'
            ];
            $staticLink = Yii::$app->params['static_link'];
            foreach ($lisTemplate as $item) {
                $viewFile = "{$viewPath}/{$item}";
                $content = file_get_contents("{$staticLink}/uploads/pages/common/templates/{$item}");
                $content = str_replace('"/pages', '"<?= Yii::$app->params["lp_media_link"] ?>/uploads/pages', $content);
                $content = str_replace("= '/pages", '= Yii::$app->params["lp_media_link"] . \'/uploads/pages', $content);
                $content = str_replace('($_SERVER[\'DOCUMENT_ROOT\'].\'', ' Yii::getAlias(\'@media', $content);
                $fp = fopen($viewFile,"wb");
                fwrite($fp, $content);
                fclose($fp);
            }
        }
    }
}
