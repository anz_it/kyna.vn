<?php
/**
 * Created by IntelliJ IDEA.
 * User: trangnqd
 * Date: 1/9/2017
 * Time: 10:49 AM
 */

namespace common\helpers;

use kyna\order\models\Order;
use kyna\partner\models\Code;
use kyna\partner\models\Partner;
use yii\base\Exception;
use kartik\mpdf\Pdf;
use kyna\promotion\models\BirthDaySoroban;

class PDFHelper
{
    public  static function addCustomFontsToMpdf($mpdf) {

        $fontdata = [
            'whitrabt' => [
                'R' => '../../../../media/fonts/whitrabt.ttf',
            ],
        ];

        foreach ($fontdata as $f => $fs) {
            // add to fontdata array
            $mpdf->fontdata[$f] = $fs;

            // add to available fonts array
            foreach (['R', 'B', 'I', 'BI'] as $style) {
                if (isset($fs[$style]) && $fs[$style]) {
                    // warning: no suffix for regular style! hours wasted: 2
                    $mpdf->available_unifonts[] = $f . trim($style, 'R');
                }
            }

        }

        $mpdf->default_available_fonts = $mpdf->available_unifonts;
    }

    /**
     * Generate COD PDF
     */
    public static function generateCOD($data, $type = 'I')
    {
        $margin = 0;
        $pdf = new \mPDF('utf8-s', 'A4', 0, 'dejavusans', $margin, $margin, $margin, $margin, 0, 0, 'P');
        $pdf->allow_charset_conversion = true;
        $pdf->charset_in = 'UTF-8';
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Kyna.vn System');
        $pdf->SetTitle('Kyna.vn Activation');
        $pdf->SetSubject('Kyna.vn Activation');
        $pdf->SetKeywords('Kyna.vn Activation');
        self::addCustomFontsToMpdf($pdf);

        $formatter = \Yii::$app->formatter;
        if (!empty($data)) {
            foreach ($data as $key => $order) {
                $codeBirthdaySoroban = '';
                if ($order && BirthDaySoroban::isDateRunCampaign() && BirthDaySoroban::isCampaignSorobanCOD($order->id)) {
                    $code = BirthDaySoroban::generateCode($order->id);
                    $codeBirthdaySoroban = "
                        <div style='font-size: 11px; margin-bottom: 12px;'>
                            Mã số may mắn: <strong style='color: #ff3681;font-size: 15px; margin-bottom: 12px;'>$code</strong>
                            </br>
                            <span style='font-size: 11px; margin-bottom: 12px;'>Giữ lại mã này để tham gia quay số nhận Lì xì mừng Tết Kỷ Hợi, từ 16/01-14/02.</span>
                        </div>
                    ";
                }

                $user_name = null;
                $phone = null;
                if (!empty($order->shippingAddress->contact_name)) {
                    $user_name = $order->shippingAddress->contact_name;
                    $phone = $order->shippingAddress->phone_number;
                } elseif (!empty($order->user->userAddress->contact_name)) {
                    $user_name = $order->user->userAddress->contact_name;
                    $phone = $order->user->userAddress->phone_number;
                } else {
                    $user_name = 'N/A';
                    $phone = 'N/A';
                }
                $orderCode = $order->shippingOrderCode;
                $barcode = '';
                if ($order->shipping_method != 'tiki') {
                    $barcode = $orderCode;
                }
                if ($order->shipping_method == 'ghtk') {
                    $barcodeArray = explode(".", $barcode);
                    $barcode = end($barcodeArray);
                }
                $user_email = !empty($order->user->email) ? $order->user->email : '';
                $address = !empty($order->shippingAddress->addressText) ? $order->shippingAddress->addressText : $order->printAddress;
                $pdf->AddPage();
                $pdf->WriteHTML('
                <div class="container" style="padding: 40px 73px;">
                    <div class="header" style="border-bottom: 3px solid #33aa36; padding-bottom: 15px; width: 100%; margin-bottom: 40px;">
                        <img style="float: left; margin-right: 175px;" src="img/logo.jpg" alt="Kyna.vn" height="35px" width="120" />
                        <div style="float: left; font-weight: bold; letter-spacing: -0.5px;">MÃ KÍCH HOẠT TÀI KHOẢN HỌC TẠI KYNA.VN</div>
                    </div>
                    <div class="content">
                        <div class="content-left" style="float: left; width: 240px;">
                            <div style="font-size: 11px; margin-bottom: 20px;">Thân gửi: <b style="color: #33aa36;">' . $user_name . '</b></div>
                            <div style="font-size: 11px; margin-bottom: 37px;">
                                Cảm ơn bạn đã đăng ký học tại Kyna.vn - Website học kĩ năng cho người đi làm.
                            </div>
                            <div style="font-size: 11px; margin-bottom: 12px;"><b>Mã kích hoạt COD các khóa học video trên Kyna.vn của bạn là:</b></div>
                            <div style="font-size: 11px; margin-bottom: 10px;">
                                Chỉ cần kích hoạt 1 lần duy nhất cho mỗi đơn hàng
                            </div>
                            <div style="text-align: center; border: 2px solid #676060; padding: 6px; vertical-align: middle;">
                                <b style="color: #33aa36; font-size: 20px; font-family: whitrabt;">' . $order->activation_code . '</b>                       
                            </div>
                            <div style="color: #E62544; font-size: 11px; text-align: center; margin-top: 12px; letter-spacing: -0.5px;">
                                (xem hướng dẫn ở mặt sau)
                            </div>
                            ' . $codeBirthdaySoroban . '
                          
                        </div>
                        <div class="content-right" style="float: left; width: 372px; padding-left: 35px;">
                            <div style="font-size: 11px; margin-bottom: 12px;"><b>Dưới đây là tài khoản đăng nhập của bạn!</b></div>
                            <div style="font-size: 11px; border: 2px solid #676060; padding: 10px 0px 10px 10px;">
                                Email: <b>' . $user_email . '</b>                               
                            </div>
                            <div style="font-size: 10px; border: 2px solid #676060; padding: 10px; margin-top: 40px; height: 130px; vertical-align: middle;">
                                <div>
                                    <div style="width: 150px; float: left;">
                                        <span><strong>Mã đơn hàng</strong> : ' . $orderCode . '</span><br>
                                        <span><strong>NN : </strong>' . $user_name . '</span><br>
                                        <span><strong>SĐT : </strong>' . $phone . '</span><br><br>
                                    </div>
                                    <div style="float: left; width: 165px; margin-left: -3px;">
                                        <barcode code="' . $barcode . '" type="C128B"/>
                                    </div>
                                </div>
                                <span><strong>ĐC : </strong>' . $address . '</span><br>
                                <span><strong>Tiền thu hộ (Người nhận phải trả) : </strong>' . $formatter->asCurrency($order->total) . '</span>
                            </div>
                        </div>
                    </div>
                </div>
                '
                );
            }
        }
        // $pdf->writeBarcode('GHJL56GS', 0, 138, 65, 1, 0, 0, 0, 0, 0, 1.5, false, false, 'C128B');
        $pdf->SetAutoPageBreak(false, 0);
        return self::outPDF($type, $pdf);
    }

    /**
     * Generate COD PDF
     */
    public static function generatePartnerCOD($data, $type = 'I')
    {
        $margin = 0;
        $pdf = new \mPDF('utf8-s', 'A4', 0, 'dejavusans', $margin, $margin, $margin, $margin, 0, 0, 'P');
        $pdf->allow_charset_conversion = true;
        $pdf->charset_in = 'UTF-8';
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Kyna.vn System');
        $pdf->SetTitle('Kyna.vn Activation');
        $pdf->SetSubject('Kyna.vn Activation');
        $pdf->SetKeywords('Kyna.vn Activation');
        $formatter = \Yii::$app->formatter;
        if (!empty($data)) {
            foreach ($data as $key => $order) {
                $user_name = null;
                $phone = null;
                if (!empty($order->shippingAddress->contact_name)) {
                    $user_name = $order->shippingAddress->contact_name;
                    $phone = $order->shippingAddress->phone_number;
                } elseif (!empty($order->user->userAddress->contact_name)) {
                    $user_name = $order->user->userAddress->contact_name;
                    $phone = $order->user->userAddress->phone_number;
                } else {
                    $user_name = 'N/A';
                    $phone = 'N/A';
                }
                $orderCode = $order->shippingOrderCode;
                $barcode = '';
                if ($order->shipping_method != 'tiki') {
                    $barcode = $orderCode;
                }
                if ($order->shipping_method == 'ghtk') {
                    $barcodeArray = explode(".", $barcode);
                    $barcode = end($barcodeArray);
                }
                $user_email = !empty($order->user->email) ? $order->user->email : '';
                $address = !empty($order->shippingAddress->addressText) ? $order->shippingAddress->addressText : $order->printAddress;

                $partnerCode = [];
                $partners = [];
                $partnerCodeHtml = "";
                if ($order->hasOnlyNormalPartner) {
                    // html for normal partner
                    foreach ($order->details as $detail) {
                        $partners[$detail->course->partner->id] = $detail->course->partner->name;
                    }
                    foreach ($partners as $item) {
                        $partnerName = 'Ứng dụng ' . $item;
                        $partnerCodeHtml .= '
                            <div style="font-size: 10px; margin-bottom: 5px; letter-spacing: -0.5px;"><b>' . $partnerName . '</b></div>                           
                        ';
                    }
                } else {
                    // html for service partner
                    foreach ($order->details as $detail) {
                        if ($detail->course->isPartner && $detail->course->partner->isServiceType) {
                            $partnerCode[$detail->activation_code] = $detail->course->partner->name;
                        }
                    }
                    foreach ($partnerCode as $serial => $name) {
                        $code = Code::findOne(['serial' => $serial]);
                        if (empty($code)) {
                            continue;
                        }
                        $partnerCodeHtml .= '
                            <div style="font-size: 10px; margin-bottom: 5px; letter-spacing: -0.5px;"><b>Mã kích hoạt COD - Ứng dụng ' . $name . ':</b></div>
                            <div style="text-align: center; border: 2px solid #676060; padding: 3px; vertical-align: middle; margin-bottom: 5px;">
                                    <b style="color: #33aa36; font-size: 15px;">' . $code->code . '</b>                       
                            </div>
                        ';
                    }
                }
                $pdf->AddPage();
                $pdf->WriteHTML('
                <div class="container" style="padding: 40px 73px;">
                    <div class="header" style="border-bottom: 3px solid #33aa36; padding-bottom: 15px; width: 100%; margin-bottom: 40px;">
                        <img style="float: left; margin-right: 175px;" src="img/logo.jpg" alt="Kyna.vn" height="35px" width="120" />
                        <div style="float: left; font-weight: bold; letter-spacing: -0.5px;">MÃ KÍCH HOẠT TÀI KHOẢN HỌC TẠI KYNA.VN</div>
                    </div>
                    <div class="content">
                        <div class="content-left" style="float: left; width: 260px;">
                            <div style="font-size: 11px; margin-bottom: 10px;">Thân gửi: <b style="color: #33aa36;">' . $user_name . '</b></div>
                            <div style="font-size: 11px; margin-bottom: 10px;">
                                Cảm ơn bạn đã đăng ký học tại Kyna.vn - Website học kĩ năng cho người đi làm.
                            </div>
                            ' . $partnerCodeHtml . '
                            <div style="color: #E62544; letter-spacing: -0.5px; font-size: 11px; text-align: center; margin-top: 12px;">
                                (xem hướng dẫn ở mặt sau)
                            </div>                           
                        </div>
                        <div class="content-right" style="float: left; width: 372px; padding-left: 15px;">
                            <div style="font-size: 11px; margin-bottom: 12px;"><b>Dưới đây là tài khoản đăng nhập của bạn!</b></div>
                            <div style="font-size: 11px; border: 2px solid #676060; padding: 10px 0px 10px 10px;">
                                Email đăng nhập: <b>' . $user_email . '</b>
                            </div>
                            <div style="font-size: 10px; border: 2px solid #676060; padding: 10px; margin-top: 40px; height: 130px; vertical-align: middle;">
                                <div>
                                    <div style="width: 150px; float: left;">
                                        <span><strong>Mã đơn hàng</strong> : ' . $orderCode . '</span><br>
                                        <span><strong>NN : </strong>' . $user_name . '</span><br>
                                        <span><strong>SĐT : </strong>' . $phone . '</span><br><br>
                                    </div>
                                    <div style="float: left; width: 165px; margin-left: -3px;">
                                        <barcode code="' . $barcode . '" type="C128B"/>
                                    </div>
                                </div>
                                <span><strong>ĐC : </strong>' . $address . '</span><br>
                                <span><strong>Tiền thu hộ (Người nhận phải trả) : </strong>' . $formatter->asCurrency($order->total) . '</span>
                            </div>
                        </div>
                    </div>
                </div>
                '
                );
            }
        }
        // $pdf->writeBarcode('GHJL56GS', 0, 138, 65, 1, 0, 0, 0, 0, 0, 1.5, false, false, 'C128B');
        $pdf->SetAutoPageBreak(false, 0);
        return self::outPDF($type, $pdf);
    }

    private static function outPDF($type, $pdf)
    {
        $file_name = 'Kyna.vn_activation.pdf';
        $pdf->SetJS("print();");
        $pdf->Output($file_name, $type);
        exit();
    }

}
