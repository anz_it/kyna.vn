<?php

namespace common\helpers;

use Yii;
use yii\caching\TagDependency;


class CacheHelper
{
    /**
     * Invalidates all of the cached data items that are associated with any of the specified [[tags]].
     * @param $tags
     */
    public static function invalidCacheByTag($tags)
    {
        TagDependency::invalidate(Yii::$app->cache, $tags);
    }

    /**
     * Uses query cache by tag for the queries performed with the callable.
     * @param $callable
     * @param $tags
     * @param $duration
     * @return mixed
     */
    public static function getQueryCacheByTag($callable, $tags, $duration = 0)
    {
        $dependency = new TagDependency(['tags' => $tags]);
        return Yii::$app->db->cache($callable, $duration, $dependency);
    }
}
