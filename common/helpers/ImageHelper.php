<?php

namespace common\helpers;

use Yii;
use yii\helpers\Html as YiiHtml;

/*
 * Class ImageHelper
 * @package common\helpers
 *
 * Hình thumb khóa học ở listing	263x147	16:9
 * Hình thumb khóa học ở giỏ hàng	151x85	16:9
 * Hình thumb khóa học phần giao dịch trong account	98x55	16:9
 * Hình học viên trang chủ	150x150	1:1
 * Banner nhỏ trang chủ	360x190
 * Avatar profile trang account	100x100	1:1
 * Avatar profile thanh header	35x35	1:1
 * Avatar profile phần câu hỏi trong account	60x60	1:1
 * Icon phương thức thanh toán IB , Credit card	95x47
 * Hình trong trắc nghiệm	80x80	1:1
 */

class ImageHelper
{
    const RESIZE_MODE_CROP = 'crop';
    const RESIZE_MODE_CONTAIN = 'contain';
    const RESIZE_MODE_COVER = 'cover';

    const IMG_SIZE_ORIGINAL = '0x0';
    const IMG_SIZE_THUMBNAIL = '480x270';
    const IMG_SIZE_THUMBNAIL_SMALL = '160x90';
    const IMG_SIZE_AVATAR = '60x60';
    const IMG_SIZE_AVATAR_SMALL = '35x35';
    const IMG_SIZE_AVATAR_LARGE = '120x120';
    const IMG_SIZE_THUMBNAIL_YOUTUBE = '560x349';
    const IMG_SIZE_THUMBNAIL_YOUTUBE_LARGE = '730x436';

    //const IMG_RETURN_MODE_FULL      = 'full';
    const IMG_RETURN_MODE_IMG_TAG = 'img';
    const IMG_RETURN_MODE_URL = 'url';

    public static $allowedMimeTypes = [
        'image/png' => '.png',
        'image/jpeg' => '.jpg',
    ];

    const ROOT_URL = '';
    const ROOT_PATH = '@root';
    const UPLOAD_ROOT = '@upload';

    private static $_options = [
        'size' => false,
        'resizeMode' => self::RESIZE_MODE_COVER,
        'forceCreate' => false,
        'fixedRatio' => false,
        'returnMode' => self::IMG_RETURN_MODE_IMG_TAG,

        'title' => false,
        'alt' => false,
        'class' => false,
    ];

    public static function getSaveDir($model, $uploadRoot = self::UPLOAD_ROOT)
    {
        if (empty($model->id)) {
            return false;
        }
        $saveDir = Yii::getAlias($uploadRoot . '/' . $model->tableSchema->name . '/' . $model->id . '/img');
        if (!file_exists($saveDir) OR !is_dir($saveDir)) {
            mkdir($saveDir, 0777, true);
        }
        return $saveDir;
    }

    public static function activeImage($model, $attribute, $options = [])
    {
        $file = $model->$attribute;
        if (!empty($model->updated_time)) {
            $options['suffix'] = $model->updated_time;
        } elseif (!empty($model->updated_at)) {
            $options['suffix'] = $model->updated_at;
        }
        //filter_var($model->$attribute, FILTER_VALIDATE_URL);
        return self::image($file, $options);
    }

    public static function saveModelImage($model, $attribute, $saveDir = false, $saveModelData = true)
    {
        if ($saveDir and filter_var($model->$attribute, FILTER_VALIDATE_URL) !== false) {
            if (!file_exists($saveDir)) {
                return false;
            }

            $file = self::saveImage($model->$attribute, $saveDir . '/' . $attribute);
            $uploadRoot = Yii::getAlias(self::UPLOAD_ROOT);
            $file = str_replace($uploadRoot, '/uploads', $file);

            if ($saveModelData and $file) {
                $model->$attribute = $file;
                $model->save();
            }
            return $file;
        }
        return false;
    }

    /**
     * Render HTML image tag with specific dimension
     * @param  string $url original image url
     * @param  string|false $size image size. defined in self::_allowedSizes()
     * @param  string $resizeMode crop | contain | cover
     * @return string       Valid Html image tag with resized image src
     */
    public static function image($originalFile, $options = [])
    {
        if (empty($originalFile)) {
            return;
        }
        $opt = ArrayHelper::extend([], self::$_options, $options);

        $originalFile = Yii::getAlias(self::ROOT_PATH . '/' . trim($originalFile, "/"));
        $file = self::_getResizedImage($originalFile, $opt['size'], $opt['resizeMode'], $opt['fixedRatio'], $opt['forceCreate']);

        if (empty($file) OR !file_exists($file) OR !is_file($file)) {
            $file =  $originalFile;
        }

        $url = str_replace(Yii::getAlias(self::ROOT_PATH), Yii::getAlias(self::ROOT_URL), $file);
        if (!empty($opt['suffix'])) {
            $url .= '?_=' . $opt['suffix'];
        }

        $returnMode = $opt['returnMode'];

        if ($returnMode === self::IMG_RETURN_MODE_URL) {
            if (empty($url)) {
                $url = '/img/default_avatar.png';
            }
            return $url;
        }

        if (!$opt['alt']) {
            $opt['alt'] = $url;
        }
        if (!$opt['title']) {
            $opt['title'] = $opt['alt'];
        }

        unset($opt['size']);
        unset($opt['suffix']);
        unset($opt['resizeMode']);
        unset($opt['fixedRatio']);
        unset($opt['returnMode']);
        unset($opt['forceCreate']);

        return YiiHtml::img($url, $opt);
    }

    public static function saveImage($url, $filename)
    {
        $tempFilename = $filename . '.' . StringHelper::random(5) . '.tmp';

        // save url to temporary file
        $fp = fopen($tempFilename, 'wb');
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_FILE, $fp);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_exec($ch);
        $contentType = curl_getinfo($ch, CURLINFO_CONTENT_TYPE);
        curl_close($ch);
        fclose($fp);

        // check valid image type
        $parts = explode(';', $contentType);
        $mimeType = $parts[0];
        $allowedTypes = self::$allowedMimeTypes;
        $ext = array_key_exists($mimeType, $allowedTypes) ? $allowedTypes[$mimeType] : false;

        if ($ext) { // save to newFileName if valid
            $filename .= $ext;
            rename($tempFilename, $filename);
        }

        // anyway, remove temporary file if failed to rename
        if (file_exists($tempFilename)) {
            unlink($tempFilename);
        }

        return $filename;
    }

    public static function flushThumbnail($originalFile)
    {
        /**
         * @var $dirname explode from origin file
         * @var $filename
         * @var $extension
         */
        if (!file_exists($originalFile) or !is_file($originalFile)) {
            extract(pathinfo($originalFile));
            $glob = $dirname . '/' . $filename . '.*.' . $extension;
            array_map('unlink', glob($glob));
        }
    }

    public static function flushOldThumbnail($originalFile)
    {
        /**
         * @var $dirname explode from origin file
         * @var $filename
         * @var $extension
         */
        if (file_exists($originalFile) and is_file($originalFile)) {
            extract(pathinfo($originalFile));
            $glob = $dirname . '/' . $filename . '.*.' . $extension;
            array_map('unlink', glob($glob));
        }
    }

    public static function _getResizedImage($originalFile, $size = false, $resizeMode = false, $fixedRatio = false, $forceCreate = false)
    {
        if (!file_exists($originalFile) or !is_file($originalFile)) {
            return false;
        }

        $dimension = self::_getDimension($size);
        $width = $dimension[0];
        $height = $dimension[1];

        if ($width + $height == 0 and $width * $height == 0) {
            return $originalFile;
        }

        $ratio = empty($height) ? 1 : ( $width / $height );

        $info = getimagesize($originalFile);
        $oWidth = $info[0];
        $oHeight = $info[1];

        $oRatio = empty ($oHeight) ? 1 : ($oWidth / $oHeight);

        $mime = $info['mime'];
        switch ($mime) {
            case 'image/png':
                $createImageFunction = 'imagecreatefrompng';
                $saveImageFunction = 'imagepng';
                $ext = '.png';
                $quality = 9;
                break;
            case 'image/jpeg':
                $createImageFunction = 'imagecreatefromjpeg';
                $saveImageFunction = 'imagejpeg';
                $ext = '.jpg';
                $quality = 90;
                break;
            default:
                return $originalFile;
        }

        if (!$resizeMode) {
            $resizeMode = self::RESIZE_MODE_COVER;
        }
        $info = pathinfo($originalFile);
        $filename = $info['filename'] . '.' . $resizeMode . '-' . $size . $ext;
        $file = str_replace($info['basename'], $filename, $originalFile);

        if (file_exists($file)) {
            if ($forceCreate === true) {
                unlink($file);
            } else {
                return $file;
            }
        }

        $oImage = $createImageFunction($originalFile);

        if ($resizeMode === self::RESIZE_MODE_CROP) {
            if ($oRatio >= $ratio) {
                $tempWidth = (int)($ratio * $oHeight);
                $tempHeight = (int)$oHeight;
            } else {
                $tempHeight = (int)($oWidth / $ratio);
                $tempWidth = (int)$oWidth;
            }
            $x = (int)($oWidth / 2 - $tempWidth / 2);
            $y = (int)($oHeight / 2 - $tempHeight / 2);

            $image = imagecreatetruecolor($width, $height);
            if ($mime === 'image/png') {
                self::_pngTransparent($image);
            }
            imagecopyresampled($image, $oImage, 0, 0, $x, $y, $width, $height, $tempWidth, $tempHeight);
        } elseif ($resizeMode === self::RESIZE_MODE_CONTAIN) {
            if (!$fixedRatio) {
                if ($oRatio >= $ratio) {
                    $height = $width / $oRatio;
                } else {
                    $width = $oRatio * $height;
                }
                $x = 0;
                $y = 0;
            } else {
                if ($oRatio >= $ratio) {
                    $tempHeight = $width / $oRatio;
                    $tempWidth = $width;
                } else {
                    $tempWidth = $oRatio * $height;
                    $tempHeight = $height;
                }
                $x = $width / 2 - $tempWidth / 2;
                $y = $height / 2 - $tempHeight / 2;
            }
            $image = imagecreatetruecolor($width, $height);
            if ($mime === 'image/png') {
                self::_pngTransparent($image);
            }
            imagecopyresampled($image, $oImage, $x, $y, 0, 0, $width, $height, $oWidth, $oHeight);
        } else {
            if ($oRatio > $ratio) {
                $width = $oRatio * $height;
            } else {
                $height = $width / $oRatio;
            }

            $image = imagecreatetruecolor($width, $height);
            if ($mime === 'image/png' and $fixedRatio) {
                self::_pngTransparent($image);
            }
            imagecopyresampled($image, $oImage, 0, 0, 0, 0, $width, $height, $oWidth, $oHeight);
        }

        $saveImageFunction($image, $file, $quality);

        if (isset($tempImage)) {
            imagedestroy($tempImage);
        }
        imagedestroy($oImage);
        imagedestroy($image);

        return $file;
    }

    private static function _pngTransparent(&$image)
    {
        //$transparent = imagecolorallocatealpha($image, 0, 0, 0, 127);
        //imagecolortransparent($image, $transparent);
        imagealphablending($image, false);
        imagesavealpha($image, true);
    }

    private static function _allowedSizes()
    {
        return [
            self::IMG_SIZE_ORIGINAL,
            self::IMG_SIZE_THUMBNAIL,
            self::IMG_SIZE_THUMBNAIL_SMALL,
            self::IMG_SIZE_AVATAR,
            self::IMG_SIZE_AVATAR_LARGE,
            self::IMG_SIZE_AVATAR_SMALL,
            self::IMG_SIZE_THUMBNAIL_YOUTUBE,
            self::IMG_SIZE_THUMBNAIL_YOUTUBE_LARGE
        ];
    }

    private static function _getDimension($size = false)
    {
        $allowedSize = self::_allowedSizes();
        if (!in_array($size, $allowedSize)) {
            $size = self::IMG_SIZE_ORIGINAL;
        }

        // return array [$width, $height]
        return explode('x', $size);
    }
}
