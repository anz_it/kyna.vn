<?php

namespace common\helpers;
use yii\bootstrap\Html as BaseHtml;
//use yii\helpers\BaseHtml;
//use yii\helpers\BaseHtml;
class Html extends BaseHtml {
    public static function iconInput($type, $name, $value, $icon, $options = []) {
        $class = isset($options['class']) ? $options['class'] : 'form-control';
        $options['class'] = $class;

        return '<div class="input-group">'.
            '<span class="input-group-addon">'.$icon.'</span>'.
            BaseHtml::input($type, $name, $value, $options).'</div>';
    }
    public static function activeIconInput($type, $model, $attribute, $icon, $options = []) {
        $name = isset($options['name']) ? $options['name'] : static::getInputName($model, $attribute);
        $value = isset($options['value']) ? $options['value'] : static::getAttributeValue($model, $attribute);
        if (!array_key_exists('id', $options)) {
            $options['id'] = static::getInputId($model, $attribute);
        }
        return static::iconInput($type, $name, $value, $icon, $options);
    }
}
