<?php

namespace common\helpers;

use yii\helpers\Url;

class GaHtmlHelper extends \yii\bootstrap\Html
{
    
    public static $gaOptions = [
        'data-ga' => 'event'
    ];
    
    public static function button($content = 'Button', $options = [])
    {
        return static::button($content, array_merge(static::$gaOptions, $options));
    }
    
    public static function a($text, $url = null, $options = [])
    {
        if ($url !== null) {
            $options['href'] = Url::to($url);
        }
        return static::tag('a', $text, array_merge(static::$gaOptions, $options));
    }
}
