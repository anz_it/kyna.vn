<?php

namespace common\helpers;

use Yii;

class DateTimeHelper
{

    public static function convertToHumanDate($systemDate)
    {
        $date = date_create_from_format('Y-m-d', $systemDate);

        if ($date != false) {
            return $date->format('d/m/Y');
        }

        return null;
    }

    public static function convertToSystemDate($humanDate)
    {
        $date = date_create_from_format('d/m/Y', $humanDate);

        if ($date != false) {
            return $date->format('Y-m-d');
        }

        return null;
    }

    public static function checkTimeRange($startTime, $endTime, $time = 'now')
    {
        $start_time = new \DateTime($startTime);
        $end_time = new \DateTime($endTime);
        $now = new \DateTime($time);
        if ($start_time < $now && $now < $end_time) {
            return true;
        }
        return false;
    }

    public static function checkAppPromotionTime()
    {

        $start_promotion_time = \Yii::$app->params['start_promotion_time'];
        $end_promotion_time = \Yii::$app->params['end_promotion_time'];
        if (!empty($start_promotion_time) && !empty($end_promotion_time)) {
            return self::checkTimeRange($start_promotion_time, $end_promotion_time, 'now');
        }
        return false;

    }

    public static function getCountDownTime()
    {

        $intervals_time = Yii::$app->params['intervals_time'];
        foreach ($intervals_time as $interval) {
            if (self::checkTimeRange($interval[0], $interval[1], 'now')) {
                $endTime = new \DateTime($interval[1]);
                $now = new \DateTime('now');
                return $endTime->getTimestamp() - $now->getTimestamp();
            }
        }
        return null;

    }

    public static function isWithinTimeRange($start, $end)
    {
        $now = new \DateTime('now');
        $now = $now->getTimestamp();
        if ($now >= strtotime($start) && $now <= strtotime($end)) {
            return true;
        }

        return false;
    }
}