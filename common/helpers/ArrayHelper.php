<?php

namespace common\helpers;

class ArrayHelper extends \yii\helpers\ArrayHelper
{

    public static function isAssoc($array)
    {
        if (!is_array($array)) {
            return false;
        }

        if (!sizeof($array)) {
            return true;
        }

        return array_keys($array) !== range(0, count($array) - 1);
    }

    public static function extend()
    {
        $arrays = func_get_args();
        $base = array_shift($arrays);
        foreach ($arrays as $array) {
            reset($base);
            while (list($key, $value) = @each($array))
                if (is_array($value) && @is_array($base[$key]))
                    $base[$key] = self::extend($base[$key], $value);
                else
                    $base[$key] = $value;
        }
        return $base;
    }

}
