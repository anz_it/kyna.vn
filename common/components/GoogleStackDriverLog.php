<?php
/**
 * Created by PhpStorm.
 * User: truongn
 * Date: 5/30/18
 * Time: 9:51 AM
 */

namespace common\components;


use Google\Cloud\Logging\LoggingClient;

class GoogleStackDriverLog extends LoggingClient
{
    const INFO = "INFO";
    const LEVEL_CRITICAL = "CRITICAL";

    public function log($subject, $level, $message)
    {
        $logger = $this->logger("kyna-log");
        $entry = $logger->entry(\Yii::$app->request->url . ' - ' . $subject, ['labels' => ['level' => $level, 'detail' => $message]]);
        $logger->write($entry);
    }

}