<?php

namespace common\components;

use yii\helpers\Html;
use yii\web\View;

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class SortableColumn extends \kotchuprik\sortable\grid\Column
{
    
    protected function renderDataCellContent($model, $key, $index)
    {
        return Html::tag('div', '<span class="fa fa-arrows"></span>', [
            'class' => 'sortable-widget-handler',
            'data-id' => $model->id,
        ]);
    }
}
