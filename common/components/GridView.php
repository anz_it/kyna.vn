<?php

namespace common\components;

class GridView extends \kartik\grid\GridView
{

    public $layout = "{items}\n<div style='width: 40%; margin: 0 0 15px 0' class='pull-left'>{summary}</div><div class='pull-right'>{pager}</div>";

    public $pager = [
        'maxButtonCount' => 5,
        'prevPageLabel' => '<i class="fa fa-angle-left"></i>',
        'nextPageLabel' => '<i class="fa fa-angle-right"></i>',
        'firstPageLabel' => '<i class="fa fa-angle-double-left"></i>',
        'lastPageLabel' => '<i class="fa fa-angle-double-right"></i>',
    ];

}