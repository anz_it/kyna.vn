<?php
/**
 * Created by PhpStorm.
 * User: khanhphan
 * Date: 4/30/17
 * Time: 10:02 AM
 */

namespace common\components;


use Elasticsearch\Client;
use Elasticsearch\ClientBuilder;
use yii\base\Component;

/**
 * Class ElasticSearch
 * @package common\components
 *
 * @property Client $client
 */
class ElasticSearch extends Component
{

    protected $_client;

    public $hosts;


    public function init()
    {
        $this->_client = ClientBuilder::create()
            ->setHosts($this->hosts)
            ->setHandler(ClientBuilder::defaultHandler(['max_handles' => 100]))
            ->build();
        parent::init(); // TODO: Change the autogenerated stub
    }

    public function getClient()
    {
        return $this->_client;
    }
}