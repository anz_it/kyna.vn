<?php
/**
 * Created by PhpStorm.
 * User: khanhphan
 * Date: 1/11/17
 * Time: 5:09 PM
 */

namespace common\components;


use yii\base\Component;

/**
 * Class PiwikComponent
 * @package common\components
 *
 * @property string $piwikId
 */
use Yii;
use yii\web\Cookie;


class PiwikComponent extends Component
{
    private $_piwikId = null;

    public function getPiwikId()
    {
        if ($this->_piwikId != null)
            return $this->_piwikId;

        if (!Yii::$app->request->cookies->has('piwikId')) {
            $piwikId = uniqid();
            $piwikIdCookie = new Cookie();
            $piwikIdCookie->name = 'piwikId';
            $piwikIdCookie->value = $piwikId;
            $piwikIdCookie->expire = strtotime("+60 days");
            Yii::$app->response->cookies->add($piwikIdCookie);
        } else {
            $piwikId = Yii::$app->request->cookies->get('piwikId');
        }
        $this->_piwikId = $piwikId;
        return $this->_piwikId;
    }

}