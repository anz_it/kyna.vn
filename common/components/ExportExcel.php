<?php

namespace common\components;

use Yii;
use yii\helpers\Html;
use yii\web\View;

class ExportExcel
{
    private $objPHPExcel;
    private $email = '';
    public $type = 'email';
    const TYPE_EMAIL = 'email';
    const TYPE_DOWNLOAD = 'download';

    public function renderData($data, $email, $fileName)
    {
        // Create new PHPExcel object
        $this->objPHPExcel = new \PHPExcel();

        // Init email to attach report
        $this->email = $email;

        // Set properties
        $this->objPHPExcel->getProperties()->setCreator("Kyna.vn");
        $this->objPHPExcel->getActiveSheet()->setTitle('Xuất bảng tính');
        $this->objPHPExcel->setActiveSheetIndex(0);
        $objWriter = \PHPExcel_IOFactory::createWriter($this->objPHPExcel, 'Excel2007');

        // Set Excel Header
        $this->_setHeader($data['header']);

        // Set Excel content
        $this->_setContentRows($data['content']);

        // do action base type
        $contentType = 'application/vnd.ms-excel; charset=utf-8';
        switch ($this->type) {
            case self::TYPE_EMAIL:
                // Save file temporary
                ob_start();
                $objWriter->save('php://output');
                $content = ob_get_clean();
                // Send mail with report attached
                $this->_attachEmail($content, $fileName, $contentType);
                break;
            case self::TYPE_DOWNLOAD:
                header('Content-Type: ' . $contentType);
                header('Content-Disposition: attachment;filename="' . $fileName . '"');
                header('Cache-Control: max-age=0');
                $objWriter->save('php://output');
                break;
            default:
                break;
        }
    }

    /**
     * set Header for excel file
     */
    private function _setHeader($data)
    {
        $this->objPHPExcel->getActiveSheet()->fromArray(array_values($data), NULL, 'A1');
        // set bold
        $this->objPHPExcel->getActiveSheet()->getStyle('A1:Z1')->getFont()->setBold(true);
        // set auto size
        foreach (range('A', $this->objPHPExcel->getActiveSheet()->getHighestDataColumn()) as $col) {
            $this->objPHPExcel->getActiveSheet()
                ->getColumnDimension($col)
                ->setAutoSize(true);
        }
    }

    /**
     * Get Content Row
     * @param $data
     */
    private function _setContentRows($data)
    {
        if (count($data) > 0) {
            $cell_name = "";
            $row = 2;
            foreach ($data as $item) {
                $cell_name = 'A' . $row;
                $this->objPHPExcel->getActiveSheet()->fromArray(array_values($item), NULL, $cell_name, true);
                $row ++;
            }
        }
    }


    /**
     * Sends an email to the specified email address using the information collected by this model.
     *
     * @param  string  $data the target email address
     * @return boolean whether the email was sent
     */
    private function _attachEmail($data, $fileName, $contentType)
    {
        try {
            Yii::$app->mailer->compose()
                ->setTo($this->email)
                ->setFrom(\Yii::$app->mailer->messageConfig['from'])
                ->setSubject($fileName)
                ->setTextBody('Please view attached file')
                ->attachContent($data, ['fileName' => $fileName, 'contentType' => $contentType])
                ->send();
        }
        catch (\Swift_TransportException $e) {
            Yii::error($e->getMessage(), 'app');
        }
    }
}
