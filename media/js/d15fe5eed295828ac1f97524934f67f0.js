
    ;(function($, window, document, undefined){
        $(document).ready(function(){
            $('#k-popup-account-login').on('hidden.bs.modal', function() {
                $('#login-form .error-summary').hide();
                $('#user-login').val('');
                $('#user-password').val('');
            })

            $('body').on('submit', '#login-form', function (event) {
                event.preventDefault();

                var data = $(this).serializeArray();
                data.push({name: 'currentUrl', value: window.location})

                $.post('/user/security/login', data, function (response) {
                    if (response.result) {
                        // login success
                        if (response.redirectUrl) {
                            window.location.replace(response.redirectUrl);
                        } else {
                            window.location.reload();
                        }
                    } else {
                        // login failed
                        var liHtml = '';
                        $.each(response.errors, function (key, value){
                            liHtml += '<li>' + value[0] + '</li>';
                        });

                        $('#login-form .error-summary ul').html(liHtml);
                        $('#login-form .error-summary').show();
                    }

                });
            });
        });
    })(window.jQuery || window.Zepto, window, document);
