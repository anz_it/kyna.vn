/* DETAIL */ 
$(document).ready(function() {
/* ADD BACKGROUND CURRICULUM */
    var getposcurr=$("#curriculum").offset().top;
    var getheightcurr = $('#curriculum').height() + 20;
    $('.background-script').attr('style', 'top: '+ getposcurr +'px !important; position: absolute; height: '+ getheightcurr +'px; width: 100%; background: #F5F5F5');   
 

/* JS SCROLL TO DIV */  
    var dt_height_header = $('.wrap-header').outerHeight(),
        dt_height_coursebar = $('.hidden-course-bar').outerHeight(); 
    var total_dt_height = dt_height_header + dt_height_coursebar;
    $('.scroll-detail-about').click(function(){
        var pos = $('#detail-text-about').offset().top - dt_height_coursebar + 55;    
        $("html,body").animate({scrollTop: pos}, '1200');
    });
    $('.scroll-curriculum').click(function(){
        var pos1 = $("#curriculum").offset().top - dt_height_coursebar;     
        $("html,body").animate({scrollTop: pos1}, '1200');
    });
    $('.scroll-detail-author').click(function(){
        var pos2 = $("#detail-author").offset().top - dt_height_coursebar;     
        $("html,body").animate({scrollTop: pos2}, '1200');
    });
    $('.scroll-detail-comment').click(function(){
        var pos3 = $("#detail-comment").offset().top - dt_height_coursebar;
        $("html,body").animate({scrollTop: pos3}, '1200');
    });
    $('body').scrollspy({ target: '#scrollspy-course', offset: 100});         
                 
/* READ MORE TEACHER */  
    var flaghidden = "show";
    $("#detail-author .click-read-more a").click(function(){
        if(flaghidden == "show"){
            $(".wrap-content-teacher").removeClass("max-height","fast");
            $('#detail-author .click-read-more a').addClass('rote').text( "Thu gọn" );   
            $('#detail-author .collapse-more').addClass('add');         
            flaghidden = "hidden";
        }
        else if (flaghidden == "hidden"){
            $(".wrap-content-teacher").addClass("max-height","fast");
            $('#detail-author .click-read-more a').removeClass('rote').text( "Xem thêm" );
            $('#detail-author .collapse-more').removeClass('add');
            flaghidden = "show";
        }            
   });    

/* SLIDER CO THE BAN QUAN TAM */       	
	$("#detail-content-bottom .detail-wrap-list").owlCarousel({
		autoPlay: true,
		items : 4,
		itemsDesktop : [1199,4], 
		itemsDesktopSmall:	[979,3],  
		itemsTablet:	[768,2],
		itemsMobile:	[479,1],            
		mouseDrag : true,  
		navigationText : ["<i class='fa fa-play rotate-180 icon'></i>",
											"<i class='fa fa-play icon'></i>"],
		navigation: true,
		pagination : false,
	}); 	                 

/* MENU TABS FIXED */      
    if ($(window).width() > 991 ){        
        var showNavOnThis = $('#detail-content-center',this.element);
        if(showNavOnThis.length){
            var hiddenCourseBar = $(".hidden-course-bar", this.element);
            var showNavOnThisTop = showNavOnThis.offset().top;
            var landingScrollTop;
            $(window).scroll(function () {
                landingScrollTop = $(this).scrollTop();
                if (landingScrollTop > showNavOnThisTop ) {
                    hiddenCourseBar.addClass("slideDown");
                } else {
                    hiddenCourseBar.removeClass("slideDown slideDown-header");
                }
            });
        }
    } 

/* SET HEIGHT POSITION LIKE FACEBOOK */  
    function setDivHeight() {                       
        //height of browser viewport 
        var documentHeight = $(document).height();        
        $('#list-fb-gg-like').css({top:($(window).height()-$('#list-fb-gg-like').height())/2});
        $('.cart-content-right .cart-sidebar-right li .text').css({top:( 55 - $('.cart-content-right .cart-sidebar-right li .text').height())/2});
    }
    // = onload and onresize
    $(window).on("load resize", setDivHeight);
          
});   
 

            

     