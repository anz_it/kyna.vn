function flyToElement(flyer, flyingTo) {
    var $func = $(this);
    var divider = 2;
    var flyerClone = $(flyer).clone();
    $(flyerClone).css({position: 'absolute', top: $(flyer).offset().top + "px", left: $(flyer).offset().left + "px", opacity: 1, 'z-index': 99999, 'border-radius': 50 + "%", 'width': 50 + "px", 'height': 50 + "px"});
    $('body').append($(flyerClone));
    var gotoX = $(flyingTo).offset().left + ($(flyingTo).width() / 6) - ($(flyer).width() / divider) / 6;
    var gotoY = $(flyingTo).offset().top + ($(flyingTo).height() / 7) - ($(flyer).height() / divider) / 7;

    $(flyerClone).animate({
        opacity: 0.9,
        left: gotoX,
        top: gotoY,
        width: "25px",
        height: "25px",
    }, 400,
            function () {
                $(flyingTo).fadeOut('fast', function () {
                    $(flyingTo).fadeIn('fast', function () {
                        $(flyerClone).fadeOut('fast', function () {
                            $(flyerClone).remove();
                        });
                    });
                });
            });
}

;
(function ($, window, document, undefined) {
    $(document).ready(function () {
        $('body').on('click', '.add-to-cart', function (e) {
            var thisBtn = $(this);
            e.preventDefault();
            var pid = $(this).data('pid');
            var csrfToken = $('meta[name="csrf-token"]').attr("content");

            $.ajax({
                url: '/cart/default/add',
                type: 'POST',
                data: {
                    pid: pid,
                    _csrf: csrfToken
                },
                success: function (response) {
                    if (response.result) {
                        // OK
                        var tempElement = $($.parseHTML(response.content));

                        // update total count
                        $('.wrap-header .click-button-cart-login .count-number').html(response.totalCount);
                        $('#detail-icon-cart .detail-number').html(response.totalCount);

                        // update shot cart html at header
                        $('#wrap-form-cart').html(tempElement.find('#wrap-form-cart').html());

                        // $("html, body").animate({ scrollTop: 0 }, "slow");

                        // show short cart
                        // $('.wrap-header').removeClass('menu-nav-up').addClass('menu-nav-down');


                        // $(".click-button-cart-login").addClass("add");
                        // $(".right-absolute-cart-login").show();

                        var parent = thisBtn.parent().parent().parent();
                        parent.find('#detail-form-register').remove();
                        parent.append(response.alertContent);

                        if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
                            window.location.href='/gio-hang';
                        }
                    } else {
                        // Failed
                        console.log(response.error);
                    }
                }
            });
        });
    });

    // Add To Cart - SCROLL DETAIL 
    $('body').on('click', '.add-to-cart-scrollspy', function () {
        //Select item image and pass to the function
        var itemImg = $(this).parents('body').find('.add-to-cart-scrollspy .icon-add-flyToElement').addClass('block');
        flyToElement($(itemImg), $('.cart_anchor_right'));
    });

    $('body').on('click', '.add-to-cart.register', function () {
        //Select item image and pass to the function
        var itemImg = $(this).parents('body').find('.add-to-cart.register .icon-add-flyToElement').addClass('block');
        flyToElement($(itemImg), $('.cart_anchor'));
    });
})(window.jQuery, window, document);
