function getTimeRemaining(endtime) {
    var t = Date.parse(endtime) - Date.parse(new Date());
    var seconds = Math.floor((t / 1000) % 60);
    var minutes = Math.floor((t / 1000 / 60) % 60);
    var hours = Math.floor((t / (1000 * 60 * 60)) % 24);
    var days = Math.floor(t / (1000 * 60 * 60 * 24));
    return {
        'total': t,
        'days': days,
        'hours': hours,
        'minutes': minutes,
        'seconds': seconds
    };
}

function initializeClock(classname, endtime) {
    var clocks = $('.'+classname);
    clocks.each(function() {
        var hoursSpan = $(this).find('.hours');
        var minutesSpan = $(this).find('.minutes');
        var secondsSpan = $(this).find('.seconds');

        function updateClock() {
            var t = getTimeRemaining(endtime);
            t.hours = t.days * 24 +  t.hours;
            if(t.hours > 100){
            hoursSpan[0].innerHTML = ('0' + t.hours).slice(-3);
            }else {
                hoursSpan[0].innerHTML = ('0' + t.hours).slice(-2);
            }

            minutesSpan[0].innerHTML = ('0' + t.minutes).slice(-2);
            secondsSpan[0].innerHTML = ('0' + t.seconds).slice(-2);

            if (t.total <= 0) {
                clearInterval(timeinterval);
            }
        }

        updateClock();
        var timeinterval = setInterval(updateClock, 1000);
    });


}

