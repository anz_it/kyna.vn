;(function ($){
    $(window).load(function() {
        var hash = window.location.hash.substr(1);
        if (hash === 'register') {
            $("#modal").modal("show").addClass("modal-narrow")
                .find(".modal-content").load("/user/registration/register")
        }
    });
})(jQuery);
