$(document).ready(function() {           
    $("#page-404 .wrap-list").owlCarousel({
    	autoPlay: true,
    	items : 4,
    	itemsDesktop : [1199,4], 
    	itemsDesktopSmall:	[979,3],  
    	itemsTablet:	[768,2],
    	itemsMobile:	[479,1],            
    	mouseDrag : true,  
    	navigationText : ["<i class='fa fa-play rotate-180 icon'></i>",
    										"<i class='fa fa-play icon'></i>"],
    	navigation: true,
    	pagination : false,
    }); 	                 
    
    
    /* RESPONSIVE EQUAL HEIGHT BLOCKS */    
    ;( function( $, window, document, undefined )
    {
        'use strict';
     
        var $list       = $( '#page-404 .main-content .wrap' ),
            $items      = $list.find( '.content' ),
            setHeights  = function()
            {
                $items.css( 'height', 'auto' );
     
                var perRow = Math.floor( $list.width() / $items.width() );
                if( perRow == null || perRow < 2 ) return true;
     
                for( var i = 0, j = $items.length; i < j; i += perRow )
                {
                    var maxHeight   = 0,
                        $row        = $items.slice( i, i + perRow );
     
                    $row.each( function()
                    {
                        var itemHeight = parseInt( $( this ).outerHeight() );
                        if ( itemHeight > maxHeight ) maxHeight = itemHeight;
                    });
                    $row.css( 'height', maxHeight );
                }
            };
     
        setHeights();
        $( window ).on( 'resize', setHeights );
     
    })( jQuery, window, document );
});    