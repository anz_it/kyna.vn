
$(document).ready(function () {
    /* Tìm các thẻ a của footer */
    var tag_a_list = $('footer a[href!=""]');
    var txtLength = "kyna.vn";
    if (tag_a_list.length > 0) {
        $.each(tag_a_list, function (index, element) {
            var href = $(element).attr('href');
            var index_string = href.indexOf("kyna.vn");
            if (index_string > 0) {
                var newHref = href.substring(index_string + txtLength.length + 1);
                if(newHref == 'chinh-sach/bao-mat'){
                    newHref = 'chinh-sach-bao-mat';
                }
                if(newHref == 'cau-hoi-thuong-gap-faq#ques-2'){
                    newHref = 'cau-hoi-thuong-gap#ques-2';
                }
                var arrSplit = newHref.split('/');
                if (arrSplit.length == 1) {
                    var rewriteURL = '/p/kyna/' + newHref;
                    $(element).attr('href', rewriteURL);
                }
            }

        });
    }
});