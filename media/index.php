<?php

// For Debug
//ini_set('error_reporting', E_ALL);
//ini_set('display_errors', 'on');
//error_reporting(-1);

// Add Header
header('Access-Control-Allow-Headers: token,content-type');
header('Access-Control-Allow-Origin: *');
header('Access-Control-Request-Method: POST');

// Only access method POST
if ($_SERVER['REQUEST_METHOD'] != 'POST') {
    header('HTTP/1.1 405 Method Not Allowed');
    exit(0);
}

// Require Image Lib
require("./class/image.php");

// Get data from $_POST and $_FILES
$data = [
    'posts' => $_POST,
    'files' => $_FILES
];

// Run Image Job
$image = new Image();
$image->auth($data)->run();

// End Application
exit(0);
