<?php

/**
 * Class Image
 * @author Trang Nguyen <trang.nguyen@kyna.vn>
 * @version 1.0
 * @desc Process Image Upload/Resize
 */

class Image
{

    const SECRET = 'kyna@2017@2ff4b8b2fdbaeef4528f742516b28cf4';
    const RESIZE_MODE_CROP = 'crop';
    const RESIZE_MODE_CONTAIN = 'contain';
    const RESIZE_MODE_COVER = 'cover';
    public $uploadPath = '';

    public $isAuth = false;
    public $data = [];

    /**
     * @param array $data
     * @return $this
     * @desc validate secret key
     */
    public function auth($data = [])
    {
        if (isset($data['posts']['secret']) && $data['posts']['secret'] == self::SECRET) {
            $this->isAuth = true;
            $this->data = $data;
            $this->uploadPath = dirname(__DIR__);
            return $this;
        }
        $this->output([
            'code' => '401',
            'status' => false,
            'msg' => 'Unauthorized'
        ]);
    }

    /**
     * @return mixed
     * @desc Route to Upload/Resize action method
     */
    public function run()
    {
        // call method base param $_POST['action']
        $posts = $this->data['posts'];
        if ($this->isAuth && !empty($posts['action']) && method_exists($this, $posts['action'])) {
            $action = $posts['action'];
            return $this->$action();
        }
        $this->output([
            'code' => '200',
            'status' => false,
            'msg' => 'Invalid Action'
        ]);
    }

    /**
     * @return $this
     * @desc Process uploaded image
     */
    public function upload()
    {
        $result = [
            'code' => '200',
            'status' => false,
            'msg' => 'Invalid Parameters'
        ];
        $files = $this->data['files'];
        $posts = $this->data['posts'];
        if ($this->isAuth && !empty($files['file']) && !empty($posts['upload_path']) && !empty($posts['file_name'])) {
            $saveDir = $this->uploadPath . $posts['upload_path'];
            $filename = $posts['file_name'];
            if (!file_exists($saveDir) and !is_dir($saveDir)) {
                mkdir($saveDir, 0777, true);
            }
            // flush old images
            // self::flushFiles($saveDir);
            $savePath = $saveDir . '/' . $filename;
            if (move_uploaded_file($files['file']['tmp_name'], $savePath)) {
                // do resize if any
                if (!empty($posts['size'])) {
                    $originalFile = $savePath;
                    $sizeArray = json_decode($posts['size']);
                    $fixedRatio = isset($posts['fixedRatio']) ? $posts['fixedRatio'] : false;
                    foreach ($sizeArray as $resizeMode => $sizes) {
                        foreach ($sizes as $size) {
                            $this->doResize($originalFile, $size, $resizeMode, $fixedRatio);
                        }
                    }
                }
                $result['status'] = true;
                $result['msg'] = '';
                $result['uploaded_url'] = $posts['upload_path'] . '/' . $filename;
            }
        }
        $this->output($result);
        return $this;
    }

    /**
     * @return $this
     * @desc Resizing Image with array size
     */
    public function resize()
    {
        $result = [
            'code' => '200',
            'status' => false,
            'msg' => 'Invalid Parameters'
        ];
        $posts = $this->data['posts'];
        if ($this->isAuth && !empty($posts['originalFile']) && !empty($posts['size'])) {
            $originalFile = $this->uploadPath . $posts['originalFile'];
            $sizeArray = json_decode($posts['size']);
            $fixedRatio = isset($posts['fixedRatio']) ? $posts['fixedRatio'] : false;
            $forceCreate = isset($posts['forceCreate']) ? $posts['forceCreate'] : false;
            if (!file_exists($originalFile) || !is_file($originalFile)) {
                $result['msg'] = 'Original file does not exists';
            } else {
                foreach ($sizeArray as $resizeMode => $sizes) {
                    foreach ($sizes as $size) {
                        $this->doResize($originalFile, $size, $resizeMode, $fixedRatio, $forceCreate);
                    }
                }
                $result['status'] = true;
                $result['msg'] = 'Resized';
            }
        }
        $this->output($result);
        return $this;
    }

    /**
     * @param $originalFile
     * @param $size
     * @param $resizeMode
     * @param $fixedRatio
     * @desc Resize image with input size
     */
    private function doResize($originalFile, $size, $resizeMode, $fixedRatio, $forceCreate = false) {
        $dimension = explode('x', $size);
        $width = $dimension[0];
        $height = $dimension[1];
        $ratio = empty($height) ? 1 : ($width / $height);
        $info = getimagesize($originalFile);
        $oWidth = $info[0];
        $oHeight = $info[1];
        $oRatio = empty ($oHeight) ? 1 : ($oWidth / $oHeight);
        $mime = $info['mime'];
        switch ($mime) {
            case 'image/png':
                $createImageFunction = 'imagecreatefrompng';
                $saveImageFunction = 'imagepng';
                $ext = '.png';
                $quality = 7;
                break;
            case 'image/jpeg':
                $createImageFunction = 'imagecreatefromjpeg';
                $saveImageFunction = 'imagejpeg';
                $ext = '.jpg';
                $quality = 70;
                break;
            default:
                return $originalFile;
        }

        $info = pathinfo($originalFile);
        $resizedFileName = $info['filename'] . '.' . $resizeMode . '-' . $size . $ext;

        if ($width * $height == 0) {
            $width = $oWidth;
            $height = $oHeight;
            $resizeMode = self::RESIZE_MODE_COVER;
            $resizedFileName = $info['filename'] . '.' . 'original' . $ext;
        }

        $file = str_replace($info['basename'], $resizedFileName, $originalFile);
        if (!$forceCreate && file_exists($file) && is_file($file)) {
            return;
        }
        $oImage = $createImageFunction($originalFile);
        if ($resizeMode === self::RESIZE_MODE_CROP) {
            if ($oRatio >= $ratio) {
                $tempWidth = (int)($ratio * $oHeight);
                $tempHeight = (int)$oHeight;
            } else {
                $tempHeight = (int)($oWidth / $ratio);
                $tempWidth = (int)$oWidth;
            }
            $x = (int)($oWidth / 2 - $tempWidth / 2);
            $y = (int)($oHeight / 2 - $tempHeight / 2);

            $image = imagecreatetruecolor($width, $height);
            if ($mime === 'image/png') {
                self::_pngTransparent($image);
            }
            imagecopyresampled($image, $oImage, 0, 0, $x, $y, $width, $height, $tempWidth, $tempHeight);
        } elseif ($resizeMode === self::RESIZE_MODE_CONTAIN) {
            if (!$fixedRatio) {
                if ($oRatio >= $ratio) {
                    $height = $width / $oRatio;
                } else {
                    $width = $oRatio * $height;
                }
                $x = 0;
                $y = 0;
            } else {
                if ($oRatio >= $ratio) {
                    $tempHeight = $width / $oRatio;
                    $tempWidth = $width;
                } else {
                    $tempWidth = $oRatio * $height;
                    $tempHeight = $height;
                }
                $x = $width / 2 - $tempWidth / 2;
                $y = $height / 2 - $tempHeight / 2;
            }
            $image = imagecreatetruecolor($width, $height);
            if ($mime === 'image/png') {
                self::_pngTransparent($image);
            }
            imagecopyresampled($image, $oImage, $x, $y, 0, 0, $width, $height, $oWidth, $oHeight);
        } else {
            if ($oRatio > $ratio) {
                $width = $oRatio * $height;
            } else {
                $height = $width / $oRatio;
            }

            $image = imagecreatetruecolor($width, $height);
            if ($mime === 'image/png' and $fixedRatio) {
                self::_pngTransparent($image);
            }
            imagecopyresampled($image, $oImage, 0, 0, 0, 0, $width, $height, $oWidth, $oHeight);
        }

        $saveImageFunction($image, $file, $quality);

        imagedestroy($oImage);
        imagedestroy($image);
    }

    /**
     * @param $in
     * @desc Return JSON result
     */
    private function output($in)
    {
        // header http status
        $returnCode = isset($in['code']) ? $in['code'] : '200';
        $this->httpStatus($returnCode);
        unset($in['code']);

        // send output
        $in = json_encode($in);
        print $in;
        exit(0);
    }

    /**
     * @param $code
     * @desc Return Header HTTP Status
     */
    private function httpStatus($code)
    {
        switch ($code) {
            case '200':
                header('HTTP/1.1 200 OK');
                break;
            case '401':
                header('HTTP/1.1 401 Unauthorized');
                break;
            default:
                break;
        }
    }

    /**
     * @param $path
     * @desc Flush all fiels in Path
     */
    private static function flushFiles($path)
    {
        if (file_exists($path) && is_dir($path)) {
            $glob = $path . '/*';
            array_map('unlink', glob($glob));
        }
    }

    /**
     * @param $image
     * @desc Make image transparent if PNG type
     */
    private static function _pngTransparent(&$image)
    {
        imagealphablending($image, false);
        imagesavealpha($image, true);
    }
}
