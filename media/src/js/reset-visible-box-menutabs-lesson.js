function resetVisibleBoxmenutabs(){
    if (window.innerWidth > 767){
        var obj = $(".box-of-menutabs.desktop");
        for(var i = 0; i < obj.length; i++)
        {
            if(!$(obj[i]).hasClass("hidden-sm-down"))
                $(obj[i]).addClass("hidden-sm-down");
        }
        var _obj = $(".mobile-tab li");
        var count = 0;
        var active = "";
        for(var i = 0; i < _obj.length; i++)
        {
            if(!$(_obj[i]).hasClass("hidden-md-up") && $(_obj[i]).hasClass("active-tab"))
                count = 1;
            else
                if($(_obj[i]).hasClass("hidden-md-up") && $(_obj[i]).hasClass("active-tab"))
                    active = $(_obj[i]).find('a').attr("href");
        }
        if (count == 0){
            $("a[href='#content-tab-lesson']").click();
            $(".menu-desktop a[href='" + active + "']").click();
        }
    }
}
$(window).ready(function(){
    resetVisibleBoxmenutabs();
})
$(window).resize(function(){
    resetVisibleBoxmenutabs();
})
