;(function($) {
    window.fbAsyncInit = function() {
        FB.init({
            appId      : window.facebook.appId,
            cookie     : true,
            xfbml      : true,  // parse social plugins on this page
            version    : 'v2.7' // use graph api version 2.5
        });

        var $btnFb = $(".button-facebook");
        $.ajaxSetup({
            "cache": false
        })
        $(document).on("click", ".button-facebook", function(e) {
            e.preventDefault();
            var that = this,
                remote = $(that).data("remote"),
                target = $(that).data("target"),
                $modal = $(target);

            $(document).on('hidden.bs.modal', $modal, function (e) {
                $modal.html("");
                // modal.removeData('bs.modal');
            });

            FB.login(function(response) {
                if (response.authResponse) {
                    FB.api('/me?fields=id,name,email', function(response) {
                        $.get(remote, response, function(remoteResponse) {
                            $modal.html(remoteResponse);
                            if (!$modal.hasClass("in")) {
                                $modal.modal("show");
                            }
                        })
                    });
                }
            }, {
                "scope": "email"
            });
        });
    }
    // });
})(jQuery);
