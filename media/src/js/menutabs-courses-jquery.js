/* Menu-tab Courses */
$('ul.nav-tabs.sub-menu').each(function(){
    // For each set of tabs, we want to keep track of
    // which tab is active and it's associated content
    var $active, $content, $links = $(this).find('a');

    // If the location.hash matches one of the links, use that as the active tab.
    // If no match is found, use the first link as the initial active tab.
    $active = $($links.filter('[href="'+location.hash+'"]')[0] || $links[0]);

    $active.parent().addClass('active-tab');

    $content = $($active[0].hash);
    // Hide the remaining content
    $links.not($active).each(function () {
        $(this.hash).hide();
    });
    // Bind the click event handler
    $(this).on('click', 'a', function(e){
        // Make the old tab inactive.
        var s = $(this).parent().parent().attr("id");
        $active.parent().removeClass('active-tab');
        $content.hide();

        // Update the variables with the new link and content
        $active = $(this);
        $content = $(this.hash);

        // Make the tab active.
        $active.parent().addClass('active-tab');
        if (window.innerWidth < 768){
            $content.removeClass("hidden-sm-down")
        }
        $content.fadeIn();

        // Prevent the anchor's default click action
        e.preventDefault();
    });
});
