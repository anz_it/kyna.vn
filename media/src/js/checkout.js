$(document).ready(function() {
    /* HIDE/SHOW BOX CACH THUC THANH TOAN */
    //$(".k-billing-item-wrap").hide();
    $("input[name=\'PaymentForm[method]\']").click(function() {
        if($(window).width() > 767 ){
            var valshow = $(this).val();
            $(".k-billing-item-wrap").removeClass("in");
            $("#checkout-show-" + valshow).addClass("in");
        }
        else
        {
            // Onlick Show Popup Checkout On Mobile
            $(".k-billing-item-wrap").removeClass("in");
            $(this).parent().find('.nav-link').click();
            $('html').css("overflow", "hidden");
        }
    });
    $(".close-modal").click(function(){
        $('html').css("overflow", "auto");
    });
    /* CHECKOUT */
    ;( function( $, window, document, undefined )
    {
        'use strict';
           var setHeights_3  = function()
            {
                if($(window).width() > 991 ){
                    var CheckoutHeaderHeight = $('.k-checkout-header').height() + 45;
                    $('.k-checkout-vertical').attr('style', 'padding-top:' + CheckoutHeaderHeight + 'px');
                }
            };
        setHeights_3();
        $( window ).on( 'resize', setHeights_3 );
    })( jQuery, window, document );



});


/* Fixed SCROLLING */
function checkoutOffset() {
    if($('#k-checkout-vertical').offset().top + $('#k-checkout-vertical').height() + parseInt($('#k-checkout-vertical').css("padding-top")) >= $('footer').offset().top)
        $('#k-checkout-vertical').css('position', 'absolute');
    if($(document).scrollTop() + window.innerHeight < $('footer').offset().top)
        $('#k-checkout-vertical').css('position', 'fixed');
}

$(document).scroll(function() {
    checkoutOffset();
});
