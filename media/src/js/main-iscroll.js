var scroller;
var mobile_tab;
$(window).ready(function() {
    if ($("#scroller").length > 0) {
        if ($("#scroller").width() < $("#scroller #bar-scroll").width()) {
            scroller = new IScroll('#scroller', {
                scrollX: true,
                scrollY: false,
                disablePointer: true,
                click: true
            });
        }
        mobile_tab = new IScroll('#mobile-tab', {
            scrollX: true,
            scrollY: false,
            disablePointer: true,
            click: true
        });
    }
})

// if ($("#mobile-tab").width() < 341){
//     getIScroll.active("mobile-tab");
// }
//getIScroll.active("content-tab-lesson");
//getIScroll.active("note-tab-lesson");
if ($(window).innerWidth() > 767) {
    $('#content-tab-lesson, #note-tab-lesson').bind('mousewheel DOMMouseScroll', function(e) {
        var scrollTo = null;

        if (e.type == 'mousewheel') {
            scrollTo = (e.originalEvent.wheelDelta * -1);
        } else if (e.type == 'DOMMouseScroll') {
            scrollTo = 20 * e.originalEvent.detail;
        }

        if (scrollTo) {
            e.preventDefault();
            $(this).scrollTop(scrollTo + $(this).scrollTop());
        }
    });
}
