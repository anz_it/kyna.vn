var _tmpHeight;
ActionLesson = {
    Init: function() {
        autosize($(".answer-lession"));
        autosize($(".add-question"));
        autosize($(".note-feedback"));
        autosize($(".ask-box"));

        $(window).on('load resize', function() {
            if (window.innerWidth > 767)
                ActionLesson.ResizeBoxView(true);
            else
                ActionLesson.ResizeBoxView(false);
        })
        $(".btn-start").click(function() {
            ActionLesson.AnswerShow();
            ActionLesson.CountDownTimer($(".timer"));
        })
        $(".btn-answer").click(function() {
            ActionLesson.CompleteShow();
        })
        $(".list-view").click(function() {
            ActionLesson.ListView();
        })
        $(".full-view").click(function() {
            ActionLesson.FullView();
        })
        $(".box-of-menutabs .title-part").click(function() {
            ActionLesson.ShowDetailPartLesson(this);
        })
        $(document).on("click", ".detail-note .title-note", function () {
            ActionLesson.ShowDetailNote(this);
        });
        $(".pre, .nex").click(function() {
            ActionLesson.PageOfResultQuiz(this);
        })
        $(".btn-feedback").click(function() {
            ActionLesson.ShowFormFeedback();
        })
        $(".set-question i, .set-question span").click(function() {
            ActionLesson.ShowAskBox();
        })
        $(".set-note i, .set-note span").click(function() {
            ActionLesson.ShowNoteBox();
        })
        $(".content-note .action-note .edit").click(function() {
            ActionLesson.EditNote(this);
        })
        $(".content-note .action-note .delete").click(function() {
            ActionLesson.DeleteNote(this);
        })
        $("textarea.add-question").keypress(function(e) {
            if (e.keyCode == 13 && e.shiftKey) {
                var content = this.value;
                var caret = getCaret(this);
                this.value = content.substring(0, caret) + "\n" + content.substring(carent, content.length - 1);
                e.stopPropagation();
            } else
            if (e.which == 13) {
                e.preventDefault();
                var btn = $(this).parents("form").find("button.btn-ask");
                $(btn).click();
            }
        });
        var part_view = $("#scroller").width();
        var part_bar = 0; //$("#bar-scroll li").length * 44;
        $("#bar-scroll li").each(function() {
            part_bar += $(this).width();
        });
        $("#bar-scroll").width(part_bar);
        $(".btn-reply-discuss").click(function() {
            var cur_rep = $(this).parents(".box-ask").find(".row-ask.hidden");
            if ($(cur_rep).length > 0)
                $(cur_rep).removeClass("hidden");
        })
        if ($("#content-tab-lesson").length > 0) {
            var offset = $("#content-tab-lesson li.see-ing").offset();
            if (offset !== undefined) {
                $('#content-tab-lesson').animate({
                    scrollTop: offset.top - 170
                }, 100);
            }
        }

        /* Nowarp Account */
        if ($(".account .text .user").length > 0) {
            var new_user = $(".account .text .user").html();
            var obj_full_name = new_user.split(' ');
            for ( var i = obj_full_name.length - 1; i >= 0; i--) {
                if ($(obj_full_name)[i] === '')
                    obj_full_name.pop();
                else
                    break;
            }
            var _num = obj_full_name.length;
            if (_num > 2) {
                new_user = obj_full_name[_num - 2] + " " + obj_full_name[_num - 1];
            }
            if (new_user.length > 14) $(".account .text .user").addClass('nowrap');
            $(".account .text .user").html(new_user);
        }
        /* Open Popup Search in mobile*/
        ActionLesson.OpenPopupSearchMobile();
        $('.btn-tutorial').on('click', function() {
            if (window.matchMedia('(min-width: 1025px)').matches) {
                ActionLesson.TutorialClickPc();
            } else {
                ActionLesson.TutorialClickTab();
            }
        })
        ActionLesson.PopupLesson();
        ActionLesson.BannerLesson();
    },
    AnswerShow: function() {
        $(".answer-lession, .box-quiz-test, #quiz-content").fadeIn();
        if ($(".quote-lesson").length > 0)
            $(".quote-lesson").hide();
        $(".btn-start").hide();
        $(".ready .countdown-time-start").hide();
        $(".btn-answer").show();
    },
    CompleteShow: function() {
        $(".timer").attr("stop", "true");
        $('#quiz-content form').trigger('submit');
        $(".ready, .box-quiz-test").fadeOut(function() {
            $(".complete").fadeIn();
        });
    },
    CountDownTimer: function(t) {
        var time = parseInt($(t).attr("data-time"));
        var stop = $(t).attr("stop");
        if (stop == "false") {
            setTimeout(function() {
                $(t).html(ActionLesson.SetTimer2String(time));
                time = time - 1;
                $(t).attr("data-time", time);
                if (time <= 0) {
                    $('#quiz-content form').trigger('submit');
                    $(t).attr("stop", "true");
                }
                ActionLesson.CountDownTimer(t);
            }, 1000);
        } else {
            $('#quiz-content form').trigger('submit');
            ActionLesson.CompleteShow();
        }
    },
    SetTimer2String: function(number) {
        var s = number % 60;
        var m = (number - s) / 60;
        m = m % 60;
        var h = (number - m * 60 - s) / 3600;
        var _s = s > 9 ? s : "0" + s;
        var _m = m > 9 ? m : "0" + m;
        var _h = h > 9 ? h : "0" + h;
        var str = _h + " : " + _m + " : " + _s;
        return str;
    },
    ListView: function() {
        $(".wrap-top").removeClass("width100per");
    },
    FullView: function() {
        $(".wrap-top").addClass("width100per");
    },
    ShowDetailPartLesson: function(t) {
        var current_click = $(t).parent();
        if ($(current_click).hasClass("show")) {
            $(current_click).removeClass("show");
            var obj_i = $(t).find('i');
            $(obj_i[0]).removeClass("icon-arrow-collapse-open").addClass("icon-arrow-collapse-close");
            $(obj_i[1]).removeClass("icon-arrow-down").addClass("icon-arrow-up");
        } else {
            $(current_click).addClass("show");
            var obj_i = $(t).find('i');
            $(obj_i[0]).removeClass("icon-arrow-collapse-close").addClass("icon-arrow-collapse-open");
            $(obj_i[1]).removeClass("icon-arrow-up").addClass("icon-arrow-down");
        }
    },
    ShowDetailNote: function(t) {
        var $obj = $(t).parent();
        if ($obj.hasClass("show")) {
            $obj.removeClass("show");
            $obj.find(".icon-arrow-up").addClass("icon-arrow-down").removeClass("icon-arrow-up");
            $obj.find(".icon-arrow-collapse-open").removeClass("icon-arrow-collapse-open").addClass("icon-arrow-collapse-close");
        } else {
            $obj.addClass("show");
            $obj.find(".icon-arrow-down").addClass("icon-arrow-up").removeClass("icon-arrow-down");
            $obj.find(".icon-arrow-collapse-close").removeClass("icon-arrow-collapse-close").addClass("icon-arrow-collapse-open");
        }
    },
    PageOfResultQuiz: function(t) {
        var _ty = t.className;
        var _pcurrent = parseInt($(t).parent().find(".page").html());
        var _pgoto = 0;
        if (_ty == "pre")
            _pgoto = _pcurrent - 1;
        else
            _pgoto = _pcurrent + 1;
        $(".k-listing-characteristics.slide[quiz='" + _pcurrent + "']").removeClass("show");
        $(".k-listing-characteristics.slide[quiz='" + _pgoto + "']").addClass("show");
        $(t).parent().find(".page").html(_pgoto);
    },
    ShowFormFeedback: function() {
        $('#feedback').modal();
        $('#feedback').on('shown.bs.modal');
        $('#feedback').on('hidden.bs.modal');
    },
    ShowAskBox: function($t) {
        $('#box-ask-lesson').toggleClass("show");
    },
    ShowNoteBox: function($t) {
        $('#box-note-lesson').toggleClass("show");
    },
    EditNote: function(t) {
        var currentText = $(t).parent().parent().find("div")[0];
        if ($(t).hasClass("update")) {
            $(t).html("Chỉnh sửa");
            $(t).removeClass("update");
            $(currentText).attr("contenteditable", "false");
        } else {
            $(t).html("Cập nhật");
            $(t).addClass("update");
            $(currentText).attr("contenteditable", "true").focus();
        }
    },
    DeleteNote: function(t) {
        $(t).parents(".detail-note").remove();
    },
    ResizeBoxView: function(flag) {},
    OpenPopupSearchMobile: function() {
        $("#k-button-search-course").click(function(event) {
            event.preventDefault();
            if ($("#search-form").css("display") === "none")
                $("#search-form").show();
            else
                $("#search-form").hide();
        });
        $("#k-close-search-popup").click(function() {
            $("#search-form").hide();
        });
    },
    TutorialClickPc: function() {
        var intro = introJs();
        intro.setOptions({
            steps: [{
                    element: '#content-tab-lesson',
                    intro: "<b>Mục lục nội dung của khóa học</b><br>Đây là danh sách bài học, bao gồm tên các phần và nội dung các bài học. Phần nội dung màu xám chính là tên bài mà bạn đang học.",
                    position: 'right'
                },
                {
                    element: '#li-note-tab',
                    intro: "<b>Ghi chú trong quá trình học</b><br>Nếu bạn chợt nghĩ ra một điều gì thú vị hoặc muốn ghi nhớ bất kỳ nội dung hay ho nào đó, trong khi đang xem video này. Hãy click vào đây và tạo ra ghi chú của riêng mình nhé.",
                },
                {
                    element: '#scroller',
                    intro: '<b>Nội dung trong 1 bài học</b><br>Các con số biểu hiện cho số video và dấu "?" biểu hiện cho bài tập có trong một bài học. Trong trường hợp, bạn nhìn thấy nhiều con số và cả dấu "?" ở đây, hãy click vào từng con số và dấu "?" để không bỏ sót video và bài tập cần làm nhé.',
                },
                {
                    element: '#mycourses',
                    intro: "<b>Tương tác với giảng viên và những người cùng học</b><br>Đây chính là diễn đàn của bạn. Tại đây, bạn có thể xem các tài liệu được Kyna đính kèm, hỏi giảng viên những điều bạn còn thắc mắc và thảo luận cùng bạn học.",
                    position: 'top'
                },
                {
                    element: '#view-mode',
                    intro: '<b>Chế độ xem bài học</b><br>Nhấn vào đây nếu bạn muốn thay đổi cách xem bài học: Nếu bạn muốn xem video toàn màn hình, nhấn icon bên phải. Nếu bạn muốn xem mục lục bài học, nhấn icon bên trái.'
                }
            ]
        });

        intro.start();
    },
    TutorialClickTab: function() {
        var intro = introJs();
        intro.setOptions({
            steps: [{
                    element: '#content-tab-lesson',
                    intro: "<b>Mục lục nội dung của khóa học</b><br>Đây là danh sách bài học, bao gồm tên các phần và nội dung các bài học. Phần nội dung màu xám chính là tên bài mà bạn đang học.",
                    position: 'right'
                },
                {
                    element: '#li-note-tab',
                    intro: "<b>Ghi chú trong quá trình học</b><br>Nếu bạn chợt nghĩ ra một điều gì thú vị hoặc muốn ghi nhớ bất kỳ nội dung hay ho nào đó, trong khi đang xem video này. Hãy click vào đây và tạo ra ghi chú của riêng mình nhé.",
                },
                {
                    element: '#scroller',
                    intro: '<b>Nội dung trong 1 bài học</b><br>Các con số biểu hiện cho số video và dấu "?" biểu hiện cho bài tập có trong một bài học. Trong trường hợp, bạn nhìn thấy nhiều con số và cả dấu "?" ở đây, hãy click vào từng con số và dấu "?" để không bỏ sót video và bài tập cần làm nhé.',
                },
                {
                    element: '#mycourses',
                    intro: "<b>Tương tác với giảng viên và những người cùng học</b><br>Đây chính là diễn đàn của bạn. Tại đây, bạn có thể xem các tài liệu được Kyna đính kèm, hỏi giảng viên những điều bạn còn thắc mắc và thảo luận cùng bạn học.",
                    position: 'top'
                }
            ]
        });

        intro.start();
    },
    PopupLesson: function() {
        var imgBanner = $('#modal-banner-lesson').find('img');
        if (window.matchMedia('(max-width: 767px)').matches)
            $(imgBanner).attr('src', $(imgBanner).data('src-mob'));
        else
            $(imgBanner).attr('src', $(imgBanner).data('src-pc'));
    },
    BannerLesson: function() {
        if (location.pathname.indexOf('/lop-hoc/913') > -1 ||
            location.pathname.indexOf('/lop-hoc/896') > -1) {
            if (window.matchMedia('(max-width: 1023px)').matches)
                $('.banner-lesson.small-banner').show();
            else
                $('.banner-lesson.large-banner').show();
        }
    }
}
ActionLesson.Init();
