CoursesAction = {
    Init: function () {

        var list_data_img_author = $(".box-courses .wrap .img-author");
        for (var i = 0; i < list_data_img_author.length; i++) {
            var data_img = $(list_data_img_author[i]).attr("data-img-author");
            $(list_data_img_author[i]).attr("style", "background-image: url(" + data_img + ");")
        }
        CoursesAction.ProfileUser();
        CoursesAction.ActiveResizeBoxProduct();

        var data_img = $("#img-avatar").attr("data-src");
        $("#img-avatar").attr("style", "background-image: url(" + data_img + ");")

        /* Hidden Edit Modal */
        /*$("#k-courses-header-btn-edit").click(function(){
            CoursesAction.ShowPopup(this);
            var data_img = $("#img-avatar").attr("data-src");
            $("#img-avatar").attr("style","background-image: url(" + data_img + ");")
        })*/
        /*$("#k-courses-header-btn-active").click(function(){
            CoursesAction.ShowPopup(this);
        })*/
        $(".wrap-upload").click(function () {
            CoursesAction.EditAvatarProfile($("#btn-upload-avatar"));
        })

        $(".description, .title").click(function () {
            CoursesAction.SeemoreMessenger(this);
        })
        $(".act-reply").click(function () {
            CoursesAction.ReplyMessenger(this);
        })
        $(".act-del").click(function () {
            CoursesAction.DelMessenger(this);
        })
        $(".box-question .see-more").click(function () {
            CoursesAction.ScrollSeemoreReplyOfQuestion(this);
        })
        $("#btn-reset-edit-profile").click(function () {
            CoursesAction.ResetProfile();
        })
        CoursesAction.ChartInit($("#cont"));
        CoursesAction.CheckAnswerHeight();
        CoursesAction.SelectionRatingCourses();
        CoursesAction.IscrollMenuCourseMobile();
    },
    ProfileUser: function () {
        $(document).ready(function () {
            /* CLICK SHOW CHANGE PASS */
            function setupToptions() {
                if ($('.popup-edit-profile .input input').length) {
                    $('.popup-edit-profile .input input').each(function () {
                        $(this).removeClass('active');
                    });
                    $('.popup-edit-profile .input input').each(function () {
                        $(this).parent('.popup-edit-profile .input').addClass('active');
                    });
                }
                ;
            };

            // Top options select
            $('.popup-edit-profile .input label').click(function () {
                setupToptions();
            });
            setupToptions();

            $('#profileform-gender input[type=radio]:checked').parent(".gender-label").addClass("selected");

            $("body").bind('click', function (e) {
                $("#profileform-gender .gender-label").click(function () {
                    $(this).parents().find('.gender-label').removeClass("selected");
                    $(this).addClass("selected");
                    // return false;
                });
                $("div[class^='form-group field-checkbox']").click(function () {
                    var t = $(this).find('label');
                    if ($(t).hasClass("selected")) {
                        $(t).removeClass("selected");
                        $(t).find('input').attr("checked", false);
                        $(t).find('input').attr("value", 0);
                    } else {
                        $(t).addClass("selected");
                        $(t).find('input').attr("checked", true);
                        $(t).find('input').attr("value", 1);
                    }

                    // return true;
                })
            });

            /* CLICK ACTIVE INPUT CHECKBOX */
            /*$('#profile-form .input.checkbox input[type=checkbox]').click(function(){
                $(this).toggleClass('selected');
            });*/

        });
    },
    ActiveResizeBoxProduct: function () {
        var tab_menu_active = $("#mycourses li.active-tab a");
        var id = $(tab_menu_active).attr("href");
        if (id != undefined) {
            var id_tab = id.split('#')[1];
            if (id_tab != undefined)
                CoursesAction.ResizeBoxProduct("#" + id_tab);
        }
    },
    ResizeBoxProduct: function (id) {
        var tab_active = $(id);
        $(document).ready(function () {
            /* HEIGHT BOX PRODUCT */
            ;(function ($, window, document, undefined) {
                'use strict';
                var $list = $(tab_active),
                    $items = $list.find('.content'),
                    $itemPrices = $list.find('.view-price'),
                    setHeights = function (items) {
                        items.css('height', 'auto');

                        var perRow = Math.floor($list.width() / items.width());
                        if (perRow == null || perRow < 2) return true;

                        for (var i = 0, j = items.length; i < j; i += perRow) {
                            var maxHeight = 0,
                                $row = items.slice(i, i + perRow);

                            $row.each(function () {
                                var itemHeight = parseInt($(this).outerHeight());
                                if (itemHeight > maxHeight) maxHeight = itemHeight;
                            });
                            $row.css('height', maxHeight);
                        }
                    };
                setHeights($items);
                setHeights($itemPrices);
                $(window).on('resize', setHeights($items));
                $(window).on('resize', setHeights($itemPrices));
            })(jQuery, window, document);


            var obj_image = $(id + " .k-box-card-list .k-box-card .img > img");
            if (window.innerWidth > 767) {
                var max_height = 0;
                for (var i = 0; i < $(obj_image).length; i++) {
                    if (max_height < $(obj_image[i]).height())
                        max_height = $(obj_image[i]).height();
                }
                for (var i = 0; i < $(obj_image).length; i++) {
                    $(obj_image[i]).height(max_height);
                }
            }
            else {
                for (var i = 0; i < $(obj_image).length; i++) {
                    $(obj_image[i]).height("auto");
                }
            }
        });
    },
    ShowPopup: function (t) {
        var div_currentid = $(t).find('a');
        if ($(div_currentid).length <= 0) {
            div_currentid = t;
        }
        var id = $(div_currentid).attr("data-target");
        $(id).modal();
        $(id).on('shown.bs.modal', function () {
            //$('#myModal .modal-body').html('');
        });
        $(id).on('hidden.bs.modal', function () {
            //$('#myModal .modal-body').html('');
        });
        $('#close-modal').click(function () {
            $(id).click();
        });
    },
    EditAvatarProfile: function (upload) {
        $(upload).click();
        $(upload).on("change", function () {
            if (this.files && this.files[0]) {
                var FR = new FileReader();
                FR.onload = function (e) {
                    $("#img-avatar").attr("style", "background-image:url(" + e.target.result + ")");
                };
                FR.readAsDataURL(this.files[0]);
            }
        });
    },
    SeemoreMessenger: function (t) {
        var $currentClass = $(t).parents(".row-messenger");
        if ($currentClass.hasClass("seemore")) {
            $currentClass.removeClass("seemore");
            $currentClass.removeClass("show-reply");
        }
        else {
            $currentClass.addClass("seemore");
        }
    },
    ReplyMessenger: function (t) {
        var $currentClass = $(t).parents(".row-messenger");
        if ($currentClass.hasClass("show-reply")) {
            $currentClass.removeClass("show-reply");
        }
        else {
            $currentClass.addClass("show-reply");
        }
    },
    DelMessenger: function (t) {
        var $currentClass = $(t).parents(".row-messenger");
        $currentClass.remove();
    },
    ScrollSeemoreReplyOfQuestion: function (t) {
        var $div_t = $(t).parents(".row-reply").find(".txt-reply");
        var $h_auto_div_t = $div_t.find(".inner");
        if ($(t).hasClass("on")) {
            $(t).removeClass("on");
            $(t).html('Xem thêm');
            $div_t.animate({height: 150}, 400);
        }
        else {
            $(t).addClass("on");
            $(t).html('Thu gọn');
            $div_t.animate({height: $h_auto_div_t.height() + 50}, 400);
        }

    },
    ChartInit: function (t) {
        var val = parseInt($(t).attr("data-pct"));
        var $circle = $('#svg #bar');
        if (isNaN(val)) {
            val = 100;
        }
        else {
            var r = $circle.attr('r');
            var c = Math.PI * (r * 2);

            if (val < 0) {
                val = 0;
            }
            if (val > 100) {
                val = 100;
            }

            var pct = ((100 - val) / 100) * c;

            $circle.css({strokeDashoffset: pct});
        }
    },
    ResetProfile: function () {
        $("#profile-form input[type='text'], #profile-form input[type='password']").each(function () {
            if ($(this).attr('id') != 'profileform-birthday')
                $(this).val('');
        });
    },
    CheckAnswerHeight: function (t) {
        $innerElement = $(".inner");
        if ($innerElement.length > 0) {
            $innerElement.each(function (index, item) {
                if ($(item).height() <= 150) {
                    $(item).parents(".row-reply").find(".see-more").remove();
                }

            });
        }
    },
    SelectionRatingCourses: function () {
        $(".s-rating .btn-group").each(function () {
            CoursesAction.DefaultRatingCourses(this);
        })
        $(".s-rating .btn-group .btn").click(function () {
            var block = $(this).parent();
            $(block).addClass('clicked');
        });
        $(".s-rating .btn-group .btn").hover(function () {
            var block = $(this).parent();
            if (!$(block).hasClass('clicked')) {
                var obj = $(block).find(".btn");
                $(obj).removeClass('active');
                $(obj).find('i').removeClass('icon-star-half');
                $(this).addClass('active');
                var flag = true;
                $(obj).each(function () {
                    if ($(this).hasClass('active'))
                        flag = false;
                    if (flag)
                        $(this).addClass('active');
                })
            }
        }, function () {
            var block = $(this).parent();
            if (!$(block).hasClass('clicked')) {
                var obj = $(block).find(".btn");
                $(obj).removeClass('active');
                CoursesAction.DefaultRatingCourses(block);
            }
        })
    },
    DefaultRatingCourses: function (t) {
        var sub_obj = $(t).find('button');
        var number = Math.floor($(t).attr("data-rating") / 10);
        var _rating = Math.floor(number / 2);
        for (var i = 0; i < _rating; i++) {
            $(sub_obj[i]).find('i').addClass('color-rating');
        }
        if (number % 2 > 0)
            $(sub_obj[_rating]).find('i').addClass('icon-star-half color-rating');
    },
    IscrollMenuCourseMobile: function () {
        var mobile_tab_menu;
        if ($("#scroll-tab-menu-course").length > 0) {
            mobile_tab_menu = new IScroll('#scroll-tab-menu-course', {
                scrollX: true,
                scrollY: false,
                disablePointer: true,
                click: true
            });
        }
    }
};
CoursesAction.Init();
