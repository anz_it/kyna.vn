(function ($, window, document, undefined) {
    // trigger event when user mouse out from kyna
    $(document).mouseleave(function(e) {
        var hasCart = countCart > 0;

        if (sendData && hasCart) {
            sendData = false;
            $.ajax({
                url: "/cart/default/add-user-care",
                method: "POST"
            });
        }
    });
})(window.jQuery, window, document);