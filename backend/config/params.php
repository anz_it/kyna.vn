<?php
return [
    'crudButtonIcons' => [
        'create' => "<i class='fa fa-fw fa-plus'></i>",
        'back' => "<i class='fa fa-arrow-circle-left'></i>",
        'delete' => "<i class='fa fa-trash'></i>",
        'update' => "<i class='fa fa-edit'></i>",
        'reset' => "<i class='fa fa-repeat'></i>",
        'save' => "<i class='fa fa-save'></i>",
        'upload' => "<i class='fa fa-upload'></i>"
    ],
    'crudTitles' => [
        'create' => 'Thêm',
        'update' => 'Cập nhật',
        'delete' => 'Xóa',
        'deleteConfirm' => 'Bạn có chắc là muốn xóa?',
        'yes' => 'Vâng',
        'no' => 'Không',
        'prompt' => '--Chọn--',
        'back' => 'Quay lại',
        'cancel' => 'Hủy',
        'reset' => 'Nhập lại',
        'save' => 'Lưu',
        'upload' => 'Tải lên',
    ],
    'time' => [
        'from' => 'Từ',
        'to' => 'đến',
    ],
    'timeOutPjax' => 5000,
];
