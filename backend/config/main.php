<?php

$params = array_merge(
    require(__DIR__.'/../../common/config/params.php'),
    require(__DIR__.'/../../common/config/params-local.php'),
    require(__DIR__.'/params.php'),
    require(__DIR__.'/params-local.php')
);

return [
    'id' => 'app-backend',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'bootstrap' => ['log'],
    'modules' => [
        'gii' => 'yii\gii\Module',
        'page' => [
            'class' => 'app\modules\page\PageModule',
        ],
        'course' => [
            'class' => 'app\modules\course\CourseModule'
        ],
        'setting' => [
            'class' => 'app\modules\setting\Module'
        ],
        'order' => [
            'class' => 'app\modules\order\Module'
        ],
        'base' => [
            'class' => 'app\modules\base\Module'
        ],
        'payment' => [
            'class' => 'app\modules\payment\Module'
        ],
        'promo' => [
            'class' => 'app\modules\promo\Module'
        ],
        'commission' => [
            'class' => 'app\modules\commission\CommissionModule',
        ],
        'community' => [
            'class' => 'app\modules\community\CommunityModule',
        ],
        'mana' => [
            'class' => 'app\modules\mana\ManaModule',
        ],
        'user' => [
            //'identityClass' => 'kyna\user\models\User',
            //'class' => 'dektrium\user\Module',
            'class' => 'app\modules\user\UserModule',
            'enableUnconfirmedLogin' => true,
            'admins' => ['admin'],
            'adminPermission' => 'Admin',
            'enableConfirmation' => false,
            'enableFlashMessages' => false,
            'enableRegistration' => false,
            'layout' => '@app/views/layouts/lte',
            'modelMap' => [
                'User' => 'kyna\user\models\User',
                'Account' => 'kyna\user\models\Account',
                'Profile' => 'kyna\user\models\Profile',
                'Token' => 'app\models\Token',
                'UserSearch' => 'kyna\user\models\UserSearch',
                'RegistrationForm' => 'app\modules\user\models\RegistrationForm',
                'ResendForm' => 'app\modules\user\models\ResendForm',
                'LoginForm' => 'app\modules\user\models\LoginForm',
                'SettingsForm' => 'app\modules\user\models\SettingsForm',
                'RecoveryForm' => 'app\modules\user\models\RecoveryForm',
            ],
            'controllerMap' => [
                'admin' => 'app\modules\user\controllers\AdminController',
                'profile' => 'app\modules\user\controllers\ProfileController',
                'recovery' => 'app\modules\user\controllers\RecoveryController',
                'registration' => 'app\modules\user\controllers\RegistrationController',
                'security' => 'app\modules\user\controllers\SecurityController',
                'settings' => 'app\modules\user\controllers\SettingsController',
                'search' => 'app\modules\user\controllers\SearchController',
            ],
        ],
        'rbac' => [
            'class' => 'dektrium\rbac\Module',
            'layout' => '@app/views/layouts/lte',
            'enableFlashMessages' => false,
        ],
        'treemanager' =>  [
            'class' => '\kartik\tree\Module',
        ],
        'gridview' =>  [
            'class' => '\kartik\grid\Module'
        ],
        'extra' => [
            'class' => 'app\modules\extra\ExtraModule',
        ],
        'career' => [
            'class' => 'app\modules\career\CareerModule',
        ],
        'faq' => [
            'class' => 'app\modules\faq\FaqModule',
        ],
        'api' => [
            'class' => 'app\modules\api\ApiModule',
        ],
        'export' => [
            'class' => 'app\modules\export\ExportModule',
        ],
        'tracking' => [
            'class' => 'app\modules\tracking\TrackingModule',
        ],
        'taamkru' => [
            'class' => 'app\modules\taamkru\TaamkruModule',
        ],
        'banner' => [
            'class' => 'app\modules\banner\Module',
        ],
        'tag' => [
            'class' => 'app\modules\tag\TagModule',
        ],
        'partner' => [
            'class' => 'app\modules\partner\PartnerModule',
        ],
        'teacher' => [
            'class' => 'app\modules\teacher\Module',
        ],
        'notification' => [
            'class' => 'app\modules\notification\NotificationModule',
        ],
        'gamification' => [
            'class' => 'app\modules\gamification\Module',
        ],
        'home' => [
            'class' => 'kyna\home\HomeModule',
        ],
        'promotion' => [
            'class' => 'app\modules\promotion\Module'
        ],
        'otp' => [
            'class' => 'kyna\otp\OtpModule',
        ],
        'report' => [
            'class' => 'app\modules\report\ReportModule',
        ],
    ],
    'controllerMap' => [
        'elfinder' => [
//            'class' => 'mihaildev\elfinder\PathController',
            'class' => 'app\components\controllers\ElfinderController',
            'access' => ['@'],
            'root' => [
                'basePath'=>'@webroot/../../frontend/web',
                'path' => 'pages',
                'name' => 'Landing pages'
            ],
            'watermark' => [
                'source'         => __DIR__.'/logo.png', // Path to Water mark image
                'marginRight'    => 5,          // Margin right pixel
                'marginBottom'   => 5,          // Margin bottom pixel
                'quality'        => 95,         // JPEG image save quality
                'transparency'   => 70,         // Water mark image transparency ( other than PNG )
                'targetType'     => IMG_GIF|IMG_JPG|IMG_PNG|IMG_WBMP, // Target image formats ( bit-field )
                'targetMinPixel' => 200         // Target image minimum pixel size
            ]
        ]
    ],
    'components' => [
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'user' => [
            'class' => 'app\components\WebUser',
            'identityClass' => 'kyna\user\models\User',
            'loginUrl' => ['/user/security/login'],
            'enableAutoLogin' => true,
        ],
        'urlManager' => [
            'rules' => [
                '<slug:[\w-]+>-p<id:\d+>/<affiliate_id:\d+>' => 'course/default/view',
                '<slug:[\w-]+>-p<id:\d+>' => 'course/default/view',
                '<slug:[\w-]+>/<affiliate_id:\d+>' => 'course/default/view',
                '<slug:[\w-]+>' => 'course/default/view',
                'lop-hoc/<id:\d+>/<section:\d+>/<lesson:\d+>'  => '/course/learning/index',
                'thanh-toan/<orderId:\d+>'                   => '/cart/checkout/index',
                'thanh-toan'                            => '/cart/checkout/index',
            ],
        ],
    ],
    'params' => $params,
];
