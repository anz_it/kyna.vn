<?php

namespace backend\assets;

use yii\web\AssetBundle;

class AngularAsset extends AssetBundle
{
    public $sourcePath = '@bower/';

    public $css = [];

    public $js = [
        'angular/angular.min.js',
        'angular-ui-tinymce/dist/tinymce.min.js'
    ];

    public $depends = [
        'dosamigos\tinymce\TinyMceAsset'
    ];
}
