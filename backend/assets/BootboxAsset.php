<?php
/**
 * Created by PhpStorm.
 * User: khanhphan
 * Date: 10/10/16
 * Time: 2:08 PM
 */

namespace backend\assets;

use yii\web\AssetBundle;

class BootboxAsset extends AssetBundle
{
    public $sourcePath = '@bower/';

    public $css = [];

    public $js = [
        'bootbox.js/bootbox.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];

}