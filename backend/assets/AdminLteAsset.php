<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AdminLteAsset extends AssetBundle
{
    public $sourcePath = '@bower/';

    public $css = [
        'admin-lte/dist/css/AdminLTE.css',
        /* AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. */
        'admin-lte/dist/css/skins/skin-blue.min.css',
        // iCheck
        'admin-lte/plugins/iCheck/flat/blue.css',
        'font-awesome/css/font-awesome.min.css',
        'Ionicons/css/ionicons.min.css',
        // Morris chart
        //'admin-lte/plugins/morris/morris.css',
        // jvectormap
        //'admin-lte/plugins/jvectormap/jquery-jvectormap-1.2.2.css',
        // Date Picker
        'admin-lte/plugins/datepicker/datepicker3.css',
        // Daterange picker
        'bootstrap-daterangepicker/daterangepicker.css',
        //'admin-lte/plugins/timepicker/bootstrap-timepicker.min.css',
        // wysihtml5 - text editor
        'admin-lte/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css',
        '/css/custom.css',
        'bootstrap-toggle/css/bootstrap-toggle.css',
    ];

    public $js = [
        // 'https://code.jquery.com/ui/1.11.4/jquery-ui.min.js',
        // Morris.js charts
        // 'https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js',
        // 'admin-lte/plugins/morris/morris.min.js',
        // Sparkline
        // 'admin-lte/plugins/sparkline/jquery.sparkline.min.js',
        // jvectormap
        // 'admin-lte/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js',
        // 'admin-lte/plugins/jvectormap/jquery-jvectormap-world-mill-en.js',
        // jQuery Knob Chart
        // 'admin-lte/plugins/knob/jquery.knob.js',
        // daterangepicker
        'moment/min/moment.min.js',
        'lodash/dist/lodash.min.js',
        'bootstrap-daterangepicker/daterangepicker.js',
        'admin-lte/plugins/timepicker/bootstrap-timepicker.min.js',
        // datepicker
        'admin-lte/plugins/datepicker/bootstrap-datepicker.js',
        // Bootstrap WYSIHTML5
        'admin-lte/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js',
        // Slimscroll
        //'admin-lte/plugins/slimScroll/jquery.slimscroll.min.js',
        // FastClick
        //'admin-lte/plugins/fastclick/fastclick.min.js',
        // AdminLTE App
        'admin-lte/dist/js/app.min.js',
        '/js/ajax-caller.js?v=1502249463',
        '/js/quiz-question.js',
        'bootstrap-toggle/js/bootstrap-toggle.js',
        '/js/status-toggle.js',
        'remarkable-bootstrap-notify/bootstrap-notify.js',
        '/js/ajax-form-popup.js?v=1503284193',
        '/js/ajax-delete.js',
        '/js/ajax-pagination.js',
    ];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset',
    ];

    public function init()
    {
        $ret = parent::init();

        switch (\Yii::$app->controller->module->id) {
            case 'app-backend':
                $this->js = array_merge($this->js, [
                    // AdminLTE dashboard demo (This is only for demo purposes)
                    'admin-lte/dist/js/pages/dashboard.js',
                    // AdminLTE for demo purposes
                    'admin-lte/dist/js/demo.js'
                ]);

                break;

            default:
                break;
        }

        return $ret;
    }
}
