<?php

namespace backend\assets;

/* 
 * Asset Bundle class for Login layout
 */
class AdminLteLoginAsset extends \yii\web\AssetBundle
{
    
    public $sourcePath = '@bower/';
    
    public $css = [
        'https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css',
        'https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css',
        'admin-lte/dist/css/AdminLTE.css',
        'admin-lte/plugins/iCheck/square/blue.css',
    ];
    public $js = [
        'admin-lte/plugins/iCheck/icheck.min.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset',
    ];
}

