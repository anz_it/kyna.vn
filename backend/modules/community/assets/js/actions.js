;(function ($, _) {
    $('body').on('click', '.btn-reply', function (e) {
        e.preventDefault();

        var $modal = $("#modal"),
            url = this.href;

        $modal.find(".modal-content").load(url, function () {
            $modal.modal("show");
        });

        $modal.on('hide.bs.modal', function () {
            tinymce.editors=[];
        });
    });

})(jQuery, _);
