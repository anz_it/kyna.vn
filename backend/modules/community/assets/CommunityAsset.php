<?php

namespace app\modules\community\assets;

class CommunityAsset extends \yii\web\AssetBundle
{

    public $sourcePath = __DIR__;
    public $js = [
        'js/actions.js',
        '/js/ajax-a-element.js',
        '/js/confirm-toggle.js',
    ];
    public $depends = [
        'app\assets\KynaAsset',
    ];
}
