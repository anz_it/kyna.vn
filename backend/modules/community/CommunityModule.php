<?php

namespace app\modules\community;

/**
 * community module definition class
 */
class CommunityModule extends \kyna\base\BaseModule
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\community\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
