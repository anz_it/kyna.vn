<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\JsExpression;
use kartik\widgets\Select2;

/* @var $this yii\web\View */
/* @var $model kyna\course\models\CourseRating */
/* @var $form yii\widgets\ActiveForm */

$crudTitles = Yii::$app->params['crudTitles'];
?>

<div class="course-rating-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php
    echo $form->field($model, 'course_id')->widget(Select2::classname(), [
        'initValueText' =>  empty($model->course_id) ? '' : $model->course->name, // set the initial display text
        'options' => [
            'placeholder' => $crudTitles['prompt'],
        ],
        'pluginOptions' => [
            'allowClear' => true,
            'minimumInputLength' => 3,
            'language' => [
                'errorLoading' => new JsExpression("function () { return 'Chờ nhận kết quả...'; }"),
            ],
            'ajax' => [
                'url' => Url::toRoute(['/course/api/search', 'auto' => true]),
                'dataType' => 'json',
                'data' => new JsExpression('function(params) { return {q:params.term}; }'),
            ],
            'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
            'templateResult' => new JsExpression('function(course) { return course.text; }'),
            'templateSelection' => new JsExpression('function (course) { if (course.price !== undefined) {$("#old-price").val(course.price); } return course.text; }'),
        ],
    ]);
    ?>

    <?= $form->field($model, 'score_of_content')->textInput() ?>

    <?= $form->field($model, 'score_of_video')->textInput() ?>

    <?= $form->field($model, 'score_of_teacher')->textInput() ?>

    <?= $form->field($model, 'review_content')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'cheat_name')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Thêm' : 'Cập nhật', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::a($crudTitles['cancel'], !empty($model->parent) ? ['view', 'id' => $model->parent_id] : ['index'],['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
