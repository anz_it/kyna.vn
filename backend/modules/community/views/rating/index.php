<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\web\JsExpression;

use kartik\widgets\Select2;
use common\helpers\RoleHelper;

use kyna\course\models\CourseRating;

$this->title = 'Đánh giá khóa học';
$this->params['breadcrumbs'][] = $this->title;

$crudTitles = Yii::$app->params['crudTitles'];
$crudButtonIcons = Yii::$app->params['crudButtonIcons'];
?>
<div class="course-discussion-index">
    <p>
        <?php
            echo Html::a($crudButtonIcons['create'] . ' ' . $crudTitles['create'], ['create'], ['class' => 'btn btn-success']);
        ?>
    </p>
    <?php Pjax::begin(['timeout' => Yii::$app->params['timeOutPjax'], 'id' => 'idPjaxReload']); ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                [
                    'attribute' => 'course_id',
                    'value' => function ($model) {
                        return !empty($model->course) ? $model->course->name : null;
                    },
                    'filter' => Select2::widget([
                        'model' => $searchModel,
                        'initValueText' => !empty($searchModel->course) ? $searchModel->course->name : '',
                        'attribute' => 'course_id',
                        'options' => ['placeholder' => $crudTitles['prompt']],
                        'pluginOptions' => [
                            'allowClear' => true,
                            'minimumInputLength' => 3,
                            'language' => [
                                'errorLoading' => new JsExpression("function () { return 'Chờ nhận kết quả...'; }"),
                            ],
                            'ajax' => [
                                'url' => Url::toRoute(['/course/api/search', 'auto' => true]),
                                'dataType' => 'json',
                            ],
                        ],
                    ]),
                    'options' => ['class' => 'col-xs-2']
                ],
                [
                    'attribute' => 'user_id',
                    'value' => function ($model) {
                        return $model->user->profile->name;
                    },
                    'filter' => Select2::widget([
                        'model' => $searchModel,
                        'initValueText' => !empty($searchModel->user) ? $searchModel->user->profile->name : '',
                        'attribute' => 'user_id',
                        'options' => ['placeholder' => $crudTitles['prompt']],
                        'pluginOptions' => [
                            'allowClear' => true,
                            'minimumInputLength' => 3,
                            'language' => [
                                'errorLoading' => new JsExpression("function () { return 'Chờ nhận kết quả...'; }"),
                            ],
                            'ajax' => [
                                'url' => Url::toRoute(['/user/api/search', 'auto' => true, 'role' => RoleHelper::ROLE_USER]),
                                'dataType' => 'json',
                            ],
                        ],
                    ]),
                    'options' => ['class' => 'col-xs-1']
                ],
                'score_of_video',
                'score_of_content',
                'score_of_teacher',
                [
                    'attribute' => 'review_content',
                    'format' => 'raw',
                    'value' => function ($model) {
                        $commentDivStyle = !empty($model->parent_id) ? 'style="margin-left:70px;"' : '';
                        
                        $commentDisplay = '<div class="commnet-display" ' . $commentDivStyle . '><div class="media-left"><img src="' . $model->user->avatarImage . '" width="50" alt="" /></div>'.
                            '<div class="media-body" style="word-wrap: break-word;word-break: break-all;">'.
                            '<h4 class="media-heading">' . $model->user->profile->name . ' - ' . $model->user->email . '</h4>'.
                            '<footer class="text-muted">'.
                            '<small>Đánh giá lúc <time>' . $model->postedTime . ' </time></small>'.
                            '</footer>'.
                            '<div class="media-content">' . strip_tags($model->review_content, "<p><font><img><a>") . '</div>'.
                            '</div></div>';
                        
                        return $commentDisplay;
                    },
                    'options' => ['class' => 'col-xs-4']
                ],
                [
                    'attribute' => 'status',
                    'label' => 'Trạng thái',
                    'value' => function ($model) {
                        if ($model->status == CourseRating::STATUS_ACTIVE) {
                            $isChecked = true;
                        } else {
                            $isChecked = false;
                        }
                        return '<input type="checkbox"
                            class="status-toggle" ' . ($isChecked ? 'checked' : '') . '
                            data-toggle="toggle"
                            data-on="Đã duyệt" data-off="Chưa duyệt"
                            confirm-message="Xác nhận thay đổi?"
                            data-href="' . Url::toRoute(['change-status', 'id' => $model->id]) . '" />';
                    },
                    'format' => 'raw',
                    'filter' => Select2::widget([
                        'model' => $searchModel,
                        'attribute' => 'status',
                        'data' => CourseRating::listStatus(),
                        'options' => ['placeholder' => $crudTitles['prompt']],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                        'hideSearch' => true,
                    ])
                ],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{update} {delete}',
                    'buttons' => [
                        'update' => function ($url, $model, $key) {
                            $url = Url::toRoute(['update', 'id' => $model->id]);

                            return Html::a('<span class="fa fa-edit"></span> Sửa', $url, [
                                'class' => 'btn btn-sm btn-default btn-block',
                                'title' => "Sửa đánh giá",
                                'data-pjax' => 0
                            ]);
                        },
                        'delete' => function ($url, $model, $key) {
                            $options = [
                                'title' => Yii::t('yii', 'Delete'),
                                'aria-label' => Yii::t('yii', 'Delete'),
                                'confirm-message' => Yii::t('yii', 'Are you sure you want to delete this item?'),
                                'class' => 'btn btn-sm btn-default btn-block btn-delete',
                            ];

                            return Html::a('<span class="fa fa-trash"></span> Xóa', $url, $options);
                        },
                    ],
                ],
            ],
        ]); ?>
    <?php Pjax::end(); ?>
</div>
