<?php

/* @var $this yii\web\View */
/* @var $model kyna\course\models\CourseRating */

$this->title = 'Cập nhật đánh giá: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Đánh giá khóa học', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Cập nhật đánh giá';
?>
<div class="course-rating-update">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
