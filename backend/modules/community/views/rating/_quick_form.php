<?php

use yii\bootstrap\ActiveForm;
/* @var $model \kyna\course\models\CourseRating */
?>
<?php $form = ActiveForm::begin([
    'id' => 'complete-order-form',
    'options' => ['class' => 'form-data-ajax'],
]); ?>

<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title" id="modal-label">Sửa đánh giá người dùng</h4>
</div>
    <div class="modal-body">
        <div class="media-left">
            <img src="<?= $model->user->avatarImage ?>" width="50" alt="" />
        </div>
        <div class="media-body">
            <h4 class="media-heading"><?= $model->user->profile->name ?></h4>
            <footer class="text-muted">
                <small>Đánh giá lúc <time><?= $model->postedTime ?> </time></small>
            </footer>
            <div class="media-content"><?= $model->review_content ?></div>
        </div>
                
        <?= $form->field($model, 'review_content')->textarea(['rows' => 5])->label('Sửa lại') ?>
    </div>
<div class="modal-footer">
    <button type="submit" href="#" class="btn btn-success btn-lg"><i class="fa fa-check fa-fw"></i> Submit</button>
</div>

<?php ActiveForm::end(); ?>
