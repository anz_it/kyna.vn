<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model kyna\course\models\CourseRating */

$this->title = 'Thêm đánh giá';
$this->params['breadcrumbs'][] = ['label' => 'Đánh giá khóa học', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="course-rating-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
