<?php

use yii\bootstrap\ActiveForm;
use common\widgets\tinymce\TinyMce;

?>
<?php $form = ActiveForm::begin([
    'id' => 'complete-order-form',
    'options' => ['class' => 'form-data-ajax'],
]); ?>

<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title" id="modal-label">Reply Form</h4>
</div>
    <div class="modal-body">
        <div class="media-left">
            <img src="<?= $model->parent->user->avatarImage ?>" width="50" alt="" />
        </div>
        <div class="media-body">
            <h4 class="media-heading"><?= $model->parent->user->profile->name ?></h4>
            <footer class="text-muted">
                <small>Trả lời lúc <time><?= $model->parent->postedTime ?> </time></small>
            </footer>
            <div class="media-content"><?= $model->parent->comment ?></div>
        </div>
                
        <?= $form->field($model, 'comment')->widget(TinyMce::className(), ['layout' => 'basic'])->label('Reply') ?>
    </div>
<div class="modal-footer">
    <button type="submit" href="#" class="btn btn-success btn-lg"><i class="fa fa-check fa-fw"></i> Submit</button>
</div>

<?php ActiveForm::end(); ?>
