<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\web\JsExpression;

use dosamigos\tinymce\TinyMceAsset;
use kartik\widgets\Select2;
use common\helpers\RoleHelper;

use kyna\course\models\CourseDiscussion;
use app\modules\community\assets\CommunityAsset;

CommunityAsset::register($this);
TinyMceAsset::register($this);

$this->title = 'Thảo luận';
$this->params['breadcrumbs'][] = $this->title;

$crudTitles = Yii::$app->params['crudTitles'];
?>
<div class="course-discussion-index">
    <?php Pjax::begin(['timeout' => Yii::$app->params['timeOutPjax'], 'id' => 'idPjaxReload']); ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                [
                    'attribute' => 'id',
                    'options' => ['class' => 'col-xs-1']
                ],
                [
                    'attribute' => 'comment',
                    'format' => 'raw',
                    'value' => function ($model) {
                        $commentDivStyle = !empty($model->parent_id) ? 'style="margin-left:70px;"' : '';
                        
                        $commentDisplay = '<div class="commnet-display" ' . $commentDivStyle . '><div class="media-left"><img src="' . $model->user->avatarImage . '" width="50" alt="" /></div>'.
                            '<div class="media-body" style="word-wrap: break-word;word-break: break-all;">'.
                            '<h4 class="media-heading">' . $model->user->profile->name . ' - ' . $model->user->email . '</h4>'.
                            '<footer class="text-muted">'.
                            '<small>Trả lời lúc <time>' . $model->postedTime . ' </time></small>'.
                            '</footer>'.
                            '<div class="media-content">' . strip_tags($model->comment, "<p><font><img><a>") . '</div>'.
                            '</div></div>';
                        
                        return $commentDisplay;
                    }
                ],
                [
                    'attribute' => 'user_id',
                    'value' => function ($model) {
                        return $model->user->profile->name;
                    },
                    'filter' => Select2::widget([
                        'model' => $searchModel,
                        'initValueText' => !empty($searchModel->user) ? $searchModel->user->profile->name : '',
                        'attribute' => 'user_id',
                        'options' => ['placeholder' => $crudTitles['prompt']],
                        'pluginOptions' => [
                            'allowClear' => true,
                            'minimumInputLength' => 3,
                            'language' => [
                                'errorLoading' => new JsExpression("function () { return 'Chờ nhận kết quả...'; }"),
                            ],
                            'ajax' => [
                                'url' => Url::toRoute(['/user/api/search', 'auto' => true, 'role' => RoleHelper::ROLE_USER]),
                                'dataType' => 'json',
                            ],
                        ],
                    ]),
                    'options' => ['class' => 'col-xs-1']
                ],
                [
                    'attribute' => 'course_id',
                    'value' => function ($model) {
                        return !empty($model->course) ? $model->course->name : null;
                    },
                    'filter' => Select2::widget([
                        'model' => $searchModel,
                        'initValueText' => !empty($searchModel->course) ? $searchModel->course->name : '',
                        'attribute' => 'course_id',
                        'options' => ['placeholder' => $crudTitles['prompt']],
                        'pluginOptions' => [
                            'allowClear' => true,
                            'minimumInputLength' => 3,
                            'language' => [
                                'errorLoading' => new JsExpression("function () { return 'Chờ nhận kết quả...'; }"),
                            ],
                            'ajax' => [
                                'url' => Url::toRoute(['/course/api/search', 'auto' => true]),
                                'dataType' => 'json',
                            ],
                        ],
                    ]),
                    'options' => ['class' => 'col-xs-2']
                ],
                [
                    'attribute' => 'status',
                    'label' => 'Spam',
                    'value' => function ($model) {
                        if ($model->status == CourseDiscussion::STATUS_SPAM) {
                            $isChecked = true;
                        } else {
                            $isChecked = false;
                        }
                        return '<input type="checkbox"
                            class="status-toggle" ' . ($isChecked ? 'checked' : '') . '
                            data-toggle="toggle"
                            data-on="Spam" data-off="Not spam"
                            confirm-message="Xác nhận thay đổi?"
                            data-href="' . Url::toRoute(['toggle-spam', 'id' => $model->id]) . '" />';
                    },
                    'format' => 'raw',
                    'filter' => Select2::widget([
                        'model' => $searchModel,
                        'attribute' => 'status',
                        'data' => CourseDiscussion::listStatus(),
                        'options' => ['placeholder' => $crudTitles['prompt']],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                        'hideSearch' => true,
                    ])
                ],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{move} {edit} {reply}',
                    'buttons' => [
                        'move' => function ($url, $model, $key) {
                            $url = Url::toRoute(['move-to-qna', 'id' => $model->id]);
                            $options = [
                                'class' => 'btn btn-sm btn-default btn-block a-ajax',
                                'title' => "Chuyển sang mục Hỏi giảng viên",
                                'confirm-message' => "Xác nhận chuyển câu hỏi này sang mục Hỏi giảng viên?",
                            ];

                            return Html::a('<span class="fa fa-paper-plane"></span> Chuyển', $url, $options);
                        },
                        'edit' => function ($url, $model, $key) {
                            $url = Url::toRoute(['update', 'id' => $model->id]);

                            return Html::a('<span class="fa fa-edit"></span> Edit', $url, [
                                'class' => 'btn btn-sm btn-default btn-block btn-reply',
                                'title' => "Sửa thảo luận",
                            ]);
                        },
                        'reply' => function ($url, $model, $key) {
                            $url = Url::toRoute(['reply', 'parent_id' => $model->id]);

                            return Html::a('<span class="fa fa-comment"></span> Reply', $url, [
                                'class' => 'btn btn-sm btn-default btn-block btn-reply',
                                'title' => "Reply",
                            ]);
                        },
                    ],
                    'visibleButtons' => [
                        'move' => function ($model) {
                            return empty($model->parent_id);
                        }
                    ]
                ],
            ],
        ]); ?>
    <?php Pjax::end(); ?>
</div>
