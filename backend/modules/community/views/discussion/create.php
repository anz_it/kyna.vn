<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model kyna\course\models\CourseDiscussion */

$this->title = 'Create Course Discussion';
$this->params['breadcrumbs'][] = ['label' => 'Course Discussions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="course-discussion-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
