<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model kyna\course\models\CourseLearnerQna */

$this->title = 'Create Course Learner Qna';
$this->params['breadcrumbs'][] = ['label' => 'Course Learner Qnas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="course-learner-qna-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
