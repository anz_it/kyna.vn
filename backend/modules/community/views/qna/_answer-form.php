<?php

use yii\bootstrap\ActiveForm;
use common\widgets\tinymce\TinyMce;

?>
<?php $form = ActiveForm::begin([
    'id' => 'complete-order-form',
    'options' => ['class' => 'form-data-ajax'],
]); ?>

<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title" id="modal-label">Giảng viên trả lời</h4>
</div>
<div class="modal-body">
    <div class="form-group field-courselearnerqna-question_content">
        <label class="control-label" for="courselearnerqna-question_content">Câu hỏi</label>

        <p><?= $model->question_content ?></p>
    </div>

    <?= $form->field($model, 'is_on_behalf_of_teacher')->checkbox() ?>

    <?= $form->field($model, 'content')->widget(TinyMce::className(), ['layout' => 'basic'])->label('Trả lời') ?>
</div>
<div class="modal-footer">
    <button type="submit" href="#" class="btn btn-success btn-lg"><i class="fa fa-check fa-fw"></i> Submit</button>
</div>

<?php ActiveForm::end(); ?>
