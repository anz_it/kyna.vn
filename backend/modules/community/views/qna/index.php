<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\web\JsExpression;

use kartik\widgets\Select2;
use kartik\daterange\DateRangePicker as DatePicker;

use kyna\course\models\CourseLearnerQna;
use app\modules\community\assets\CommunityAsset;
use dosamigos\tinymce\TinyMceAsset;

CommunityAsset::register($this);
TinyMceAsset::register($this);

$this->title = 'Câu hỏi cho giảng viên';
$this->params['breadcrumbs'][] = $this->title;

$crudTitles = Yii::$app->params['crudTitles'];
?>
<div class="course-learner-qna-index">
    <?php Pjax::begin(['timeout' => Yii::$app->params['timeOutPjax'], 'id' => 'idPjaxReload']); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'id',
                'options' => ['class' => 'col-xs-1']
            ],
            [
                'attribute' => 'date_range',
                'label' => 'Thời gian hỏi',
                'filter' => DatePicker::widget([
                    'model' => $searchModel,
                    'attribute' => 'date_range',
                    'convertFormat' => true,
                    'pluginOptions' => [
                        'locale' => [
                            'format' => 'd/m/Y',
                            'separator' => ' - ',
                        ]
                    ]
                ]),
                'options' => ['class' => 'col-xs-2'],
                'value' => function ($model) {
                    return Yii::$app->formatter->asDatetime($model->posted_time, "php:d/m/Y H:i");
                }
            ],
            [
                'attribute' => 'content',
                'format' => 'html',
                'value' => function ($model) {
                    $commentDivStyle = !empty($model->question_id) ? 'style="margin-left:70px;"' : '';

                    $commentDisplay = '<div class="commnet-display" ' . $commentDivStyle . '><div class="media-left"><img src="' . $model->user->avatarImage . '" width="50" alt="" /></div>'.
                        '<div class="media-body">'.
                        '<h4 class="media-heading">' . $model->user->profile->name . ' - ' . $model->user->email . '</h4>'.
                        '<footer class="text-muted">'.
                        '<small>Trả lời lúc <time>' . $model->postedTime . ' </time></small>'.
                        '</footer>'.
                        '<div class="media-content">' . strip_tags(($model->content), "<p><a>") . '</div>'.
                        '</div></div>';

                    return $commentDisplay;
                },
                'options' => ['class' => 'col-xs-6'],
            ],
            [
                'attribute' => 'course_id',
                'value' => function ($model) {
                    return !empty($model->course->name) ? $model->course->name : '';
                },
                'filter' => Select2::widget([
                    'model' => $searchModel,
                    'initValueText' => !empty($searchModel->course) ? $searchModel->course->name : '',
                    'attribute' => 'course_id',
                    'options' => ['placeholder' => $crudTitles['prompt']],
                    'pluginOptions' => [
                        'allowClear' => true,
                        'minimumInputLength' => 3,
                        'language' => [
                            'errorLoading' => new JsExpression("function () { return 'Chờ nhận kết quả...'; }"),
                        ],
                        'ajax' => [
                            'url' => Url::toRoute(['/course/api/search', 'auto' => true]),
                            'dataType' => 'json',
                        ],
                    ],
                ])
            ],
            [
                'attribute' => 'user_id',
                'value' => function ($model) {
                    return $model->user->profile->name;
                },
                'filter' => Select2::widget([
                    'model' => $searchModel,
                    'initValueText' => !empty($searchModel->user) ? $searchModel->user->profile->name : '',
                    'attribute' => 'user_id',
                    'options' => ['placeholder' => $crudTitles['prompt']],
                    'pluginOptions' => [
                        'allowClear' => true,
                        'minimumInputLength' => 3,
                        'language' => [
                            'errorLoading' => new JsExpression("function () { return 'Chờ nhận kết quả...'; }"),
                        ],
                        'ajax' => [
                            'url' => Url::toRoute(['/user/api/search', 'auto' => true]),
                            'dataType' => 'json',
                        ],
                    ],
                ])
            ],
            [
                'attribute' => 'teacher_id',
                'label' => 'Giảng viên',
                'value' => function ($model) {
                    return !empty($model->course->teacher->profile->name) ? $model->course->teacher->profile->name : '';
                }
            ],
            [
                'attribute' => 'is_approved',
                'format' => 'raw',
                'value' => function ($model) {
                    if ($model->is_approved == CourseLearnerQna::BOOL_NO) {
                        $isChecked = false;
                    } else {
                        $isChecked = true;
                    }
                    if ($model->question != null && $model->user_id != $model->question->user_id) {
                        return false;
                    }
                    return '<input type="checkbox"
                            class="status-toggle" ' . ($isChecked ? 'checked' : '') . '
                            data-toggle="toggle"
                            data-on="Đã duyệt" data-off="Chưa duyệt"
                            confirm-message="Xác nhận thay đổi?"
                            data-href="' . Url::toRoute(['toggle-approve', 'id' => $model->id]) . '" />';
                },
                'filter' => Select2::widget([
                    'model' => $searchModel,
                    'attribute' => 'is_approved',
                    'data' => CourseLearnerQna::getBooleanOptions(),
                    'options' => ['placeholder' => $crudTitles['prompt']],
                    'pluginOptions' => [
                        'allowClear' => true,
                    ],
                    'hideSearch' => true,
                ]),
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{move} {edit} {answer} {delete} {email-teacher}',
                'buttons' => [
                    'move' => function ($url, $model, $key) {
                        if ($model->question_id > 0) {
                            return null;
                        }

                        $url = Url::toRoute(['move-to-discuss', 'id' => $model->id]);

                        return Html::a('<span class="fa fa-paper-plane"></span> Chuyển', $url, [
                            'class' => 'btn btn-sm btn-default btn-block a-ajax',
                            'title' => "Chuyển sang mục thảo luận",
                            'aria-label' => Yii::t('yii', 'Chuyển'),
                            'confirm-message' => 'Xác nhận chuyển câu hỏi này sang mục thảo luận ? Sau khi chuyển sẽ gửi email thông báo cho học viên.',
                        ]);
                    },
                    'edit' => function ($url, $model, $key) {
                        $url = Url::toRoute(['edit', 'id' => $model->id]);

                        return Html::a('<span class="fa fa-edit"></span> Edit', $url, [
                            'class' => 'btn btn-sm btn-default btn-block btn-reply',
                        ]);
                    },
                    'answer' => function ($url, $model, $key) {
                        $url = Url::toRoute(['answer', 'question_id' => $model->id]);

                        return Html::a('<span class="fa fa-reply"></span> Trả lời', $url, [
                            'class' => 'btn btn-sm btn-default btn-block btn-reply',
                        ]);
                    },
                    'delete' => function ($url, $model, $key) {
                        $options = [
                            'title' => Yii::t('yii', 'Delete'),
                            'aria-label' => Yii::t('yii', 'Delete'),
                            'confirm-message' => Yii::t('yii', 'Are you sure you want to delete this item?'),
                            'class' => 'btn btn-sm btn-default btn-block btn-delete',
                        ];

                        return Html::a('<span class="fa fa-trash"></span> Delete', $url, $options);
                    },
                    'email-teacher' => function ($url, $model, $key) {
                        if ($model->is_approved && !$model->is_send_to_teacher) {
                            if ($model->question != null && $model->user_id != $model->question->user_id) {
                                return false;
                            }

                            $options = [
                                'title' => Yii::t('yii', 'Gửi mail'),
                                'aria-label' => Yii::t('yii', 'Gửi mail'),
                                'confirm-message' => Yii::t('yii', 'Bạn có muốn email câu hỏi này đến giảng viên?'),
                                'class' => 'btn btn-sm btn-default btn-block a-ajax',
                            ];

                            return Html::a('<span class="fa fa-envelope"></span> Gửi mail', $url, $options);
                        }
                        return false;
                    }
                ],
                'visibleButtons' => [
                    'answer' => function ($model) {
                        return $model->question_id == null;
                    },
                ]
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
