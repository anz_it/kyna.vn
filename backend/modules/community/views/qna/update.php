<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model kyna\course\models\CourseLearnerQna */

$this->title = 'Update Course Learner Qna: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Course Learner Qnas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="course-learner-qna-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
