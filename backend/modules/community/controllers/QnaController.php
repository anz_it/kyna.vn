<?php

namespace app\modules\community\controllers;

use common\helpers\ArrayHelper;
use kyna\settings\models\Setting;
use Yii;
use yii\base\Exception;
use yii\helpers\Json;
use yii\web\Response;
use yii\web\NotFoundHttpException;
use yii\filters\AccessControl;

use common\helpers\StringHelper;
use kyna\course\models\CourseLearnerQna;
use kyna\course\models\search\CourseLearnerQnaSearch;
use app\components\controllers\Controller;
use kyna\course\models\CourseDiscussion;


/**
 * QnaController implements the CRUD actions for CourseLearnerQna model.
 */
class QnaController extends Controller
{

    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->user->can('Course.Comment');
                        },
                    ],
                ],
            ],
        ]);
    }
    
    /**
     * Lists all CourseLearnerQna models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CourseLearnerQnaSearch();
        $searchModel->status = CourseLearnerQnaSearch::STATUS_ACTIVE;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionToggleApprove($id)
    {
        $model = $this->findModel($id);
        $model->setScenario(CourseLearnerQna::SCENARIO_APPROVE);
        $template = '';
        $successMsg = '';

        if ($model->is_approved == CourseLearnerQna::BOOL_NO) {
            $model->is_approved = CourseLearnerQna::BOOL_YES;
            
            if (!$model->is_send_approved) {
                // need to send mail approved
                $template = '@common/mail/qa/approved';
                $subject = "[Kyna.vn] Câu hỏi #$id của bạn đã được duyệt";
                
                $model->is_send_approved = CourseLearnerQna::BOOL_YES;
            }
            
            $successMsg = 'Đã duyệt';
        } else {
            $model->is_approved = CourseLearnerQna::BOOL_NO;
            if (!$model->is_send_unapproved) {
                // need to send mail unapproved
                $template = '@common/mail/qa/unapproved';
                $subject = "[Kyna.vn] Câu hỏi #$id của bạn không được duyệt";
                
                $model->is_send_unapproved = CourseLearnerQna::BOOL_YES;
            }
            
            $successMsg = 'Chuyển sang chưa duyệt';
        }
        
        if ($model->save()) {
            if (!empty($template)) {
                // send mail
                Yii::$app->mailer->compose()
                        ->setHtmlBody($this->renderPartial($template, [
                            'questionId' => $id,
                            'fullname' => $model->user->profile->name,
                            'courseName' => $model->course->name,
                            'questionContent' => StringHelper::truncate(strip_tags($model->content), 30),
                            'qaLink' => $model->qaLink,
                        ]))
                        ->setTo($model->user->email)
                        ->setSubject($subject)
                        ->send();
                
                $successMsg .= ' và gửi mail thông báo đến ' . $model->user->email;
            }

            Yii::$app->response->format = Response::FORMAT_JSON;
            $ret = [
                'success' => true,
                'message' => $successMsg,
            ];
            return $ret;
        }
        else {
            throw new Exception('Thay đổi thất bại!');
        }

    }

    public function actionEdit($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->save(false)) {
                $successMsg = 'Cập nhật thành công';

                Yii::$app->response->format = Response::FORMAT_JSON;

                $ret = [
                    'success' => true,
                    'message' => $successMsg,
                ];
                echo Json::encode($ret);
                Yii::$app->end();
            }
        }

        return $this->renderAjax('edit', ['model' => $model]);
    }
    
    public function actionAnswer($question_id)
    {
        $model = new CourseLearnerQna();
        $model->question_id = $question_id;
        $model->is_approved = CourseLearnerQna::BOOL_YES;
        $model->is_send_to_teacher = CourseLearnerQna::BOOL_YES;

        $model->course_id = $model->question->course_id;
        $model->user_id = Yii::$app->user->id;

        $model->question_content = $model->question->content;
        $needSendMail = false;
        
        if (!$model->is_send_answered) {
            $needSendMail = true;
            $model->is_send_answered = CourseLearnerQna::BOOL_YES;
        }

        if ($model->load(Yii::$app->request->post())) {
            // check is_null answer
            $successMsg = 'Trả lời thành công';

            if ($model->save()) {
                if ($needSendMail) {
                    $subject = "[Kyna.vn] Câu hỏi #{$question_id} của bạn đã được giảng viên trả lời";

                    Yii::$app->mailer->compose()
                        ->setHtmlBody($this->renderPartial('@common/mail/qa/notify_answer', [
                            'questionId' => $question_id,
                            'fullname' => $model->question->user->profile->name,
                            'courseName' => $model->course->name,
                            'questionContent' => StringHelper::truncate(strip_tags($model->question_content), 30),
                            'answerContent' => strip_tags($model->content),
                            'qaLink' => $model->question->qaLink
                        ], true))
                        ->setTo($model->question->user->email)
                        ->setSubject($subject)
                        ->send();

                    $successMsg .= ' và đã gửi mail tới học viên ' . $model->question->user->email;
                }

            }

            Yii::$app->response->format = Response::FORMAT_JSON;
            $ret = [
                'success' => true,
                'message' => $successMsg,
            ];
            echo Json::encode($ret);
            Yii::$app->end();
        }
        
        return $this->renderAjax('_answer-form', ['model' => $model]);
    }
    
    public function actionMoveToDiscuss($id)
    {
        $model = $this->findModel($id);
        $needSendMoved = false;
        
        $discussModel = new CourseDiscussion();
        $discussModel->course_id = $model->course_id;
        $discussModel->user_id = $model->user_id;
        $discussModel->comment = $model->content;
        
        if ($discussModel->save()) {
            $model->is_approved = CourseDiscussion::BOOL_NO;
            $model->status = CourseDiscussion::STATUS_DEACTIVE;
            if (!$model->is_send_moved) {
                $needSendMoved = true;
                $model->is_send_moved  = CourseDiscussion::BOOL_YES;
            }

            if ($model->save()) {
                $successMsg = 'Chuyển thành công';
                if ($needSendMoved) {
                    $subject = "[Kyna.vn] Câu hỏi #$id của bạn đã được chuyển qua mục “Thảo luận”";

                    Yii::$app->mailer->compose()
                            ->setHtmlBody($this->renderPartial('@common/mail/qa/move_to_discuss', [
                                'questionId' => $id,
                                'fullname' => $model->user->profile->name,
                                'courseName' => $model->course->name,
                                'questionContent' => StringHelper::truncate(strip_tags($model->content), 30)
                            ], true))
                            ->setTo($model->user->email)
                            ->setSubject($subject)
                            ->send();

                    $successMsg .= " và gửi thông báo chuyển câu hỏi đến email {$model->user->email}";
                }
                Yii::$app->response->format = Response::FORMAT_JSON;
                $ret = [
                    'success' => true,
                    'message' => $successMsg,
                ];
                return $ret;
            }
        } else {
            throw new Exception('Chuyển thất bại!');
        }
    }

    /**
     * Email to Teacher if question is approved and sent to student
     * @param $id
     */
    public function actionEmailTeacher($id)
    {
        $model = $this->findModel($id);
        if ($model->is_approved == CourseLearnerQna::BOOL_YES && $model->is_send_to_teacher == CourseLearnerQna::BOOL_NO) {
            $template = '@common/mail/qa/email_teacher';
            $subject = "[Kyna.vn] Câu hỏi #$id từ học viên";
            // send mail
            $questionCourse = $model->course;
            $courseTeacher = $questionCourse->teacher;
            // get setting cc email
            $settings = ArrayHelper::map(Setting::find()->all(), 'key', 'value');
            $fromEmail =  !empty($settings['email_teacher_from']) ? $settings['email_teacher_from'] : 'hotro@kyna.vn';
            $ccEmails = !empty($settings['email_teacher_cc']) ? $settings['email_teacher_cc'] : 'hotro@kyna.vn';
            $ccEmails = explode(',', $ccEmails);
            Yii::$app->mailer->compose()
                ->setHtmlBody($this->renderPartial($template, [
                    'questionId' => $id,
                    'studentName' => $model->user->profile->name,
                    'courseName' => $model->course->name,
                    'questionContent' => strip_tags($model->content),
                    'total_question' => 1,
                    'gender' => 'Thầy/Cô',
                    'questionDate' => \Yii::$app->formatter->asDatetime($model->posted_time, "php:d-m-Y  H:i:s"),
                    'fullName' => $courseTeacher->profile->name
                ]))
                ->setFrom($fromEmail)
                ->setTo($courseTeacher->email)
                ->setCc($ccEmails)
                ->setSubject($subject)
                ->send();
            $model->is_send_to_teacher = CourseLearnerQna::BOOL_YES;
            if ($model->save()) {
                $msg = "Câu hỏi #$id đã được chuyển đến giảng viên";
            } else {
                $msg = "Câu hỏi #$id đã được gửi mail đến giảng viên nhưng không cập nhật thành công";
                Yii::$app->session->setFlash('error', $msg);
            }
        } else {
            $msg = "Câu hỏi #$id chưa được duyệt hoặc đã được gửi mail đến giảng viên";
            Yii::$app->session->setFlash('error', $msg);
        }
        Yii::$app->response->format = Response::FORMAT_JSON;
        $ret = [
            'success' => true,
            'message' => $msg,
        ];
        return $ret;
    }
    
    protected function findModel($id)
    {
        if (($model = CourseLearnerQna::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
