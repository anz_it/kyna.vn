<?php

namespace app\modules\community\controllers;

use Yii;
use yii\base\Exception;
use yii\web\Response;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use yii\filters\AccessControl;

use kyna\course\models\CourseDiscussion;
use kyna\course\models\search\CourseDiscussionSearch;
use app\components\controllers\Controller;
use kyna\course\models\CourseLearnerQna;

/**
 * DiscussionController implements the CRUD actions for CourseDiscussion model.
 */
class DiscussionController extends Controller
{

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->user->can('Course.Comment');
                        },
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all CourseDiscussion models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CourseDiscussionSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            
            $ret = [
                'message' => 'Cập nhật thành công',
                'success' => true,
            ];
            
            echo Json::encode($ret);
            Yii::$app->end();
        }
        
        return $this->renderAjax('_form', [
            'model' => $model,
        ]);
    }

    public function actionReply($parent_id)
    {
        $model = new CourseDiscussion();
        $model->parent_id = $parent_id;
        $model->user_id = Yii::$app->user->id;
        $model->course_id = $model->parent->course_id;
        
        if ($model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            
            if ($model->validate()) {
                if (!empty($model->parent->parent_id)) {
                    $model->parent_id = $model->parent->parent_id;
                }

                $model->save(false);
                $ret = [
                    'message' => 'Phản hồi thành công',
                    'success' => true,
                ];
            } else {
                $ret = $model->errors;
            }
            
            echo Json::encode($ret);
            Yii::$app->end();
        }
        
        return $this->renderAjax('_reply-form', ['model' => $model]);
    }
    
    public function actionMoveToQna($id)
    {
        $model = $this->findModel($id);

        $qnaModel = new CourseLearnerQna();
        $qnaModel->course_id = $model->course_id;
        $qnaModel->user_id = $model->user_id;
        $qnaModel->content = $model->comment;

        if ($qnaModel->save()) {
            $model->delete();
            $successMsg = 'Chuyển thành công!';
            Yii::$app->response->format = Response::FORMAT_JSON;
            $ret = [
                'success' => true,
                'message' => $successMsg,
            ];
            return $ret;
        }
        else {
            throw new Exception('Thay đổi thất bại!');
        }
    }

    /**
     * @param $id
     * @return bool JSON
     */
    public function actionToggleSpam($id)
    {
        $model = $this->findModel($id);
        $result = false;
        if ($model->status == CourseDiscussion::STATUS_ACTIVE) {
            $model->status = CourseDiscussion::STATUS_SPAM;
        } else {
            if ($model->status == CourseDiscussion::STATUS_SPAM) {
                $model->status = CourseDiscussion::STATUS_ACTIVE;
            }
        }

        if ($model->save()) {
            $result = [
                'success' => true,
                'message' => 'Thay đổi thành công',
            ];
        };
        Yii::$app->response->format = Response::FORMAT_JSON;
        return $result;
    }
    
    protected function findModel($id)
    {
        if (($model = CourseDiscussion::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
