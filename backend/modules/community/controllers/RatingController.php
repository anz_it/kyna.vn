<?php
/**
 * Created by PhpStorm.
 * User: macintosh
 * Date: 6/2/17
 * Time: 11:25 AM
 */

namespace app\modules\community\controllers;

use Yii;
use yii\web\Response;
use yii\web\NotFoundHttpException;
use kyna\course\models\CourseRating;
use kyna\course\models\search\CourseRatingSearch;

class RatingController extends \app\components\controllers\Controller
{

    /**
     * Lists all CourseDiscussion models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CourseRatingSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCreate()
    {
        $model = new CourseRating();
        $model->user_id = Yii::$app->user->id;
        $model->is_cheat = CourseRating::BOOL_YES;
        $model->status = CourseRating::STATUS_ACTIVE;
        $model->setScenario('cheat');

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', 'Thêm thành công');

            return $this->redirect(['index']);
        }

        return $this->render('create', ['model' => $model]);
    }

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        if ($model->is_cheat == CourseRating::BOOL_YES) {
            $model->setScenario('cheat');
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', 'Cập nhật thành công');

            return $this->redirect(['index']);
        }

        return $this->render('update', ['model' => $model]);
    }

    public function actionQuickUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            return [
                'massage' => 'Cập nhật thành công',
                'success' => true,
            ];
        }

        return $this->renderAjax('_quick_form', [
            'model' => $model,
        ]);
    }

    protected function findModel($id)
    {
        if (($model = CourseRating::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionChangeStatus($id, $redirectUrl = false, $field = 'status')
    {
        $model = $this->findModel($id);

        if ($model->status == CourseRating::STATUS_ACTIVE) {
            $model->status = CourseRating::STATUS_DEACTIVE;
        } else {
            if ($model->status == CourseRating::STATUS_DEACTIVE) {
                $model->scenario = CourseRating::SCENARIO_APPROVE;
                $model->status = CourseRating::STATUS_ACTIVE;
            }
        }
        if ($model->save(false)) {
            return "Thay đổi thành công!";
        }
        else
            return "Thay đổi thất bại!";
    }
}