<?php
namespace app\modules\promotion;

class Module extends \kyna\base\BaseModule
{
    public $controllerNamespace = 'app\modules\promotion\controllers';

    public function init()
    {
        parent::init();
        // custom initialization code goes here
    }
}
