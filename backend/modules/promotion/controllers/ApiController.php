<?php

namespace app\modules\promotion\controllers;

use common\helpers\ArrayHelper;
use common\helpers\ResponseModel;
use kyna\commission\models\AffiliateCategory;
use kyna\commission\models\AffiliateUser;
use kyna\course\models\Category;
use kyna\settings\models\Setting;
use Yii;
use yii\web\Response;
use yii\db\Query;

use kyna\course\models\Course;
use kyna\user\models\User;
use kyna\user\models\AuthAssignment;
use kyna\user\models\Profile;
use kyna\course\models\CourseSection;
use kyna\course\models\CourseTeachingAssistant;
use kyna\course_combo\models\CourseComboItem;

/*
 * This is api controller class for module Promotion
 */
class ApiController extends \app\components\controllers\Controller
{

    /**
     * @desc function search courses

     * @param string $q
     * @param bool $auto
     *
     * @return json
     */
    public function actionSearchPartner($q = null){
        Yii::$app->response->format = Response::FORMAT_JSON;

        if (is_null($q)) {
            return [];
        }
        $query = new yii\db\Query;

       // $affiliateCategoryTable = AffiliateCategory::tableName();
        $users = $query->select(['profile.user_id', 'profile.name'])->from('profile')->andWhere(['like','profile.name', $q])
            ->leftJoin('affiliate_users', 'affiliate_users.user_id = profile.user_id')
            ->leftJoin('affiliate_categories', 'affiliate_categories.id = affiliate_users.affiliate_category_id')
            ->andWhere(["affiliate_categories.affiliate_group_id" => [
                Yii::$app->params['id_aff_group_third_party'],
                Yii::$app->params['id_aff_group_teacher_owner']
            ]])->all();
        $arr = [];
        foreach ($users as $affPartner){
            $item = [];
            $item['id'] = $affPartner['user_id'];
            $item['text'] = $affPartner['name'];
            $arr[] = $item;
        }
        $out['results'] =  $arr;
        return $out;
    }
}
