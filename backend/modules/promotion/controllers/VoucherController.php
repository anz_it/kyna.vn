<?php

namespace app\modules\promotion\controllers;

use kyna\commission\models\AffiliateCategory;
use kyna\commission\models\AffiliateUser;
use kyna\promotion\models\MinigameVoucher;
use kyna\promotion\models\PromotionSearch;
use kyna\promotion\models\Promotion;
use kyna\promotion\models\PromotionCourse;
use Yii;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

use app\components\controllers\Controller;

/**
 * VoucherController implements the CRUD actions for Voucher model.
 */
class VoucherController extends Controller
{

    public $mainTitle = 'Vouchers';
    
    public function behaviors()
    {

        return array_merge(parent::behaviors(), [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'view', 'print-html', 'print-view'],
                        'matchCallback' => function () {
                            return Yii::$app->user->can('Voucher.View');
                        },
                    ],
                    [
                        'allow' => true,
                        'actions' => ['create'],
                        'matchCallback' => function () {
                            return Yii::$app->user->can('Voucher.Create');
                        },
                    ],
                    [
                        'allow' => true,
                        'actions' => ['change-status'],
                        'matchCallback' => function () {
                            return Yii::$app->user->can('Voucher.Update');
                        },
                    ],
                    [
                        'allow' => true,
                        'actions' => ['delete'],
                        'matchCallback' => function () {
                            return Yii::$app->user->can('Voucher.Delete');
                        },
                    ],
                ],
            ],
        ]);
    }
    
    /**
     * Lists all Voucher models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PromotionSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new Voucher model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Promotion();
        $model->status = Promotion::STATUS_ACTIVE;
        $model->apply_all = 1;
        $model->apply_all_single_course = 1;
        $model->apply_all_single_course_double = 1;
        $model->apply_all_combo = 1;
        $model->min_amount = 0;
        $model->type = Promotion::KIND_ORDER_APPLY;
        $model->apply_condition = Promotion::ONE_COURSE_CONDITION;
        $model->number_usage = 1;
        $model->user_number_usage = 1;
        $model->current_number_usage = 0;
        $model->apply_scope = Promotion::APPLY_SCOPE_ALL;
        $model->apply_special_course = 1;
        //$model->loadDefaultValues();
        $model->created_by = Yii::$app->user->id;

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if(!empty($model->apply_all)){
                $model->apply_all_combo = 1;
                $model->apply_all_single_course = 1;
                $model->apply_all_single_course_double = 1;

            }
            else{
                if(!empty($model->apply_all_combo) && !empty($model->apply_all_single_course) && !empty($model->apply_all_single_course_double)){
                    $model->apply_all = 1;
                }
            }
            $block_id = '';
            if($model->quantity > 1){
                $block_id = uniqid();
            }
            for ($i = 1; $i <= $model->quantity; $i++) {
                $newVoucher = clone $model;
                if(!empty($block_id)){
                    $newVoucher->block_id = $block_id;
                }
                $newVoucher->save();

                if(MinigameVoucher::checkPrefix($newVoucher->code)){
                    if(isset(Yii::$app->params['campaign_minigame']) && !empty(Yii::$app->params['campaign_minigame'])){
                        $courseIds = Yii::$app->params['campaign_minigame'];
                        foreach ($courseIds as $id){
                            $newVoucher->assignCourse($id,$newVoucher->id);
                        }
                    }
                }else{
                    $isForAll = $model->apply_all ? $model->apply_all : 0;
                    if(empty($isForAll) /*&& $model->type == Promotion::KIND_COURSE_APPLY*/)
                    {
                        if(!empty($model->course_id)) {
                            foreach ($model->course_id as $courseId) {
                                $promo_course = new  PromotionCourse();
                                $promo_course->course_id = $courseId;
                                $promo_course->promotion_id = $newVoucher->id;
                                $promo_course->save();
                            }
                        }
                    }
                }
            }
            //Type apply for course

            
            Yii::$app->session->setFlash('success', 'Thêm thành công.');
            return $this->redirect(['index']);
        }
        
        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Voucher model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model->end_date = Yii::$app->formatter->asDate($model->end_date);

        if ($model->load(Yii::$app->request->post())) {
            if(!empty($model->apply_all)){
                $model->apply_all_combo = 1;
                $model->apply_all_single_course = 1;
                $model->apply_all_single_course_double = 1;
            }
            if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }
        
        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Finds the Voucher model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Voucher the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {

        if (($model = Promotion::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Lists all Voucher models.
     * @return mixed
     */
    public function actionPrintView()
    {
        $searchModel = new PromotionSearch();
        $dataProvider = $searchModel->searchPrintHtml(Yii::$app->request->queryParams);

        return $this->render('print_view', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionPrintHtml()
    {
        $searchModel = new PromotionSearch();
        $dataProvider = $searchModel->searchPrintHtml(Yii::$app->request->queryParams["query"]);

        return $this->renderPartial('print', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
}
