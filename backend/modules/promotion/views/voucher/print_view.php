<?php

use yii\helpers\Html;
use yii\grid\GridView;
use kartik\widgets\Select2;
use kyna\promotion\models\Promotion;
use kartik\export\ExportMenu;

$crudTitles = Yii::$app->params['crudTitles'];
$crudButtonIcons = Yii::$app->params['crudButtonIcons'];

$this->title = 'In Voucher HTML';
$this->params['breadcrumbs'][] = $this->title;

$user = Yii::$app->user;
$formatter = \Yii::$app->formatter;

$gridColumns = [
    'id',
    'code',
    [
        'attribute' => 'value',
        'value' => function ($model){
            return  $model->discount_type == Promotion::TYPE_PERCENTAGE ? $model->value .' %' : Yii::$app->formatter->asCurrency($model->value);
        }
    ],
    'note',
    [
        'attribute' => 'end_date',
        'value' => function ($model) {
            return (!empty($model->end_date) ? Yii::$app->formatter->asDatetime($model->end_date) : null);
        }
    ],
    [
        'attribute' => 'is_used',
        'format' => 'html',
        'value' => function ($model) {
            if ($model->is_used == Promotion::BOOL_YES) {
                return Promotion::BOOL_YES_TEXT;
            } else {
                return Promotion::BOOL_NO_TEXT;
            }
        },
    ],
    [
        'attribute' => 'used_date',
        'value' => function ($model) {
            return (!empty($model->used_date) ? Yii::$app->formatter->asDatetime($model->used_date) : null);
        }
    ],
];
?>
<div class="row">
    <div class="col-xs-12">
        <div class="voucher-index">
            <nav class="navbar navbar-default">

                <?= Html::a('In HTML', ['print-html', 'query'=>Yii::$app->request->getQueryString()], ['class' => 'btn btn-info navbar-btn navbar-left', 'target'=>'__blank']) ?>
            </nav>

            <div class="box">
                <?=
                GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],
                        [
                            'attribute' => 'id',
                            'options' => ['class' => 'col-xs-1']
                        ],
                        'code',
                        [
                            'attribute' => 'value',
                            'value' => function ($model){
                                return  $model->discount_type == Promotion::TYPE_PERCENTAGE ? $model->value .' %' : Yii::$app->formatter->asCurrency($model->value);
                            }
                        ],
                        [
                            'attribute' => 'note',
                            'options' => ['class' => 'col-xs-2']
                        ],
                        [
                            'attribute' => 'end_date',
                            'value' => function ($model) {
                                return (!empty($model->end_date) ? Yii::$app->formatter->asDatetime($model->end_date) : null);
                            },
                            'filter' => false
                        ],
                        [
                            'attribute' => 'is_used',
                            'format' => 'html',
                            'value' => function ($model) {
                                if ($model->is_used == Promotion::BOOL_YES) {
                                    return '<span class="label label-warning">' . Promotion::BOOL_YES_TEXT . '</span>';
                                } else {
                                    return '<span class="label label-success">' . Promotion::BOOL_NO_TEXT . '</span>';
                                }
                            },
                            'filter' => Select2::widget([
                                'attribute' => 'is_used',
                                'model' => $searchModel,
                                'data' => $searchModel->getBooleanOptions(),
                                'options' => ['placeholder' => $crudTitles['prompt']],
                                'pluginOptions' => [
                                    'allowClear' => true,
                                ],
                                'hideSearch' => true,
                            ]),
                        ],
                        [
                            'attribute' => 'used_date',
                            'value' => function ($model) {
                                return (!empty($model->used_date) ? Yii::$app->formatter->asDatetime($model->used_date) : null);
                            },
                            'filter' => false
                        ],
                        [
                            'class' => 'yii\grid\ActionColumn',
                            'visibleButtons' => [
                                'update' => false,
                                'view' => $user->can('Voucher.View'),
                                'delete' => $user->can('Voucher.Delete'),
                            ],
                        ],
                    ],
                ]);
                ?>
            </div>
        </div>
    </div>
</div>