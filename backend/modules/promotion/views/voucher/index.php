<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use kartik\widgets\Select2;
use kyna\promotion\models\Promotion;
use kartik\export\ExportMenu;

$crudTitles = Yii::$app->params['crudTitles'];
$crudButtonIcons = Yii::$app->params['crudButtonIcons'];

$this->title = Yii::$app->controller->mainTitle;
$this->params['breadcrumbs'][] = $this->title;

$user = Yii::$app->user;
$formatter = \Yii::$app->formatter;

$gridColumns = [
    'id',
    'code',
    [
        'attribute' => 'value',
        'value' => function ($model){
            return  $model->discount_type == 0 ? $model->value .' %' : Yii::$app->formatter->asCurrency($model->value);
        }
    ],
    [
        'attribute' => 'type',
        'value' => function ($model){
              return $model->listType[$model->type];
        }
    ],
    'note',
    [
        'attribute' => 'end_date',
        'value' => function ($model) {
            return (!empty($model->end_date) ? Yii::$app->formatter->asDatetime($model->end_date) : null);
        }
    ],
    [
        'attribute' => 'is_used',
        'format' => 'html',
        'value' => function ($model) {
            if ($model->is_used == Promotion::BOOL_YES) {
                return Promotion::BOOL_YES_TEXT;
            } else {
                return Promotion::BOOL_NO_TEXT;
            }
        },
    ],
    [
        'attribute' => 'used_date',
        'value' => function ($model) {
            return (!empty($model->used_date) ? Yii::$app->formatter->asDatetime($model->used_date) : null);
        }
    ],
];
?>
<div class="row">
    <div class="col-xs-12">
        <div class="voucher-index">
            <?php Pjax::begin(['timeout' => Yii::$app->params['timeOutPjax'], 'id' => 'idPjaxReload']) ?>
            <nav class="navbar navbar-default">
                <?php if ($user->can('Voucher.Create')) : ?>
                    <?= Html::a($crudButtonIcons['create'] . ' ' . $crudTitles['create'], ['create'], ['class' => 'btn btn-success navbar-btn navbar-left']) ?>
                <?php endif; ?>
                
                <?= ExportMenu::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => $gridColumns,
                    'fontAwesome' => true,
                    'container' => [
                        'class' => 'btn-group navbar-btn navbar-left'
                    ],
                    'columnSelectorOptions'=>[
                        'label' => 'Export Cols',
                    ],
                    'dropdownOptions' => [
                        'label' => 'Export All',
                        'class' => 'btn btn-default'
                    ],
                    'showConfirmAlert' => false,
                    'showColumnSelector' => false,
                    'target' => ExportMenu::TARGET_BLANK,
                    'filename' => 'voucher_export_'.date('Y-m-d_H-i', time()),
                    'exportConfig' => [
                        ExportMenu::FORMAT_PDF => false,
                        ExportMenu::FORMAT_HTML => false,
                    ]
                ]);
                ?>

                <?= Html::a('In Voucher HTML', ['print-view'], ['class' => 'btn btn-info navbar-btn navbar-left', 'target'=>'__blank']) ?>
            </nav>

            <div class="box">
                <?=
                GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],
                        [
                            'attribute' => 'id',
                            'options' => ['class' => 'col-xs-1']
                        ],
                        'code',
                        [
                            'attribute' => 'type',
                            'value' => function ($model){
                                return $model->listType[$model->type];
                            },
                            'filter' => Html::activeDropDownList($searchModel, 'type', Promotion::getListType(), ['class' => 'form-control', 'prompt' => $crudTitles['prompt']]),
                        ],
                        [
                            'attribute' => 'value',
                            'value' => function ($model){
                                 return  $model->discount_type == 0 ? $model->value .' %' : Yii::$app->formatter->asCurrency($model->value);
                                 }
                        ],

                        [
                            'attribute' => 'note',
                            'options' => ['class' => 'col-xs-2']
                        ],
                        [
                            'attribute' => 'start_date',
                            'value' => function ($model) {
                                return (!empty($model->start_date) ? Yii::$app->formatter->asDatetime($model->start_date) : null);
                            },
                            'filter' => false
                        ],
                        [
                            'attribute' => 'end_date',
                            'value' => function ($model) {
                                return (!empty($model->end_date) ? Yii::$app->formatter->asDatetime($model->end_date) : null);
                            },
                            'filter' => false
                        ],
                        'number_usage',
                        [
                            'attribute' => 'current_number_usage',
                            'value' => function ($model) {
                                return (!empty($model->current_number_usage) ? $model->current_number_usage : 0);
                            },
                        ],
                        'user_number_usage',
                        [
                            'header' => 'Có thể sử dụng',
                            'format' => 'html',
                            'value' => function ($model) {
                                if ($model->canUse == Promotion::BOOL_YES) {
                                    return '<span class="label label-success">' . Promotion::BOOL_YES_TEXT . '</span>';
                                } else {
                                    return '<span class="label label-danger">' . Promotion::BOOL_NO_TEXT . '</span>';
                                }
                            }
                        ],

                        [
                            'attribute' => 'status',
                            'format' => 'raw',
                            'value' => function ($model) {
                                return $model->statusButton;
                            },
                            'filter' => Html::activeDropDownList($searchModel, 'status', Promotion::listStatus(false), ['class' => 'form-control', 'prompt' => $crudTitles['prompt']]),
                        ],
                        [
                            'class' => 'app\components\ActionColumnCustom',
                            'visibleButtons' => [
                                'update' => false,
                                'view' => $user->can('Voucher.View'),
                                'delete' => $user->can('Voucher.Delete'),
                            ],
                        ],
                    ],
                ]);
                ?>
                <?php Pjax::end() ?>
            </div>
        </div>
    </div>
</div>