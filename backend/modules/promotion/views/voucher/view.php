<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kyna\promotion\models\Promotion;
use kyna\course\models\Course;
$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::$app->controller->mainTitle, 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$crudTitles = Yii::$app->params['crudTitles'];
?>
<div class="voucher-view">

    <p>
        <?= Html::a("<span class='glyphicon glyphicon-backward'></span> " . $crudTitles['back'], ['index'], ['class' => 'btn btn-info', 'icon' => 'glyphicon glyphicon-backward']) ?>
        
        <?php
        if (Yii::$app->user->can('Voucher.Delete')) {
            echo Html::a($crudTitles['delete'], ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Bạn có chắc là sẽ xóa mục này không?',
                    'method' => 'post',
                ],
            ]);
        }
        ?>
    </p>
    <?php
    $course_names = [];
    foreach ($model->promoCourses as $course)
    {
        $course_id = $course->course_id;
        $course = Course::findOne(['id'=>$course_id]);
        if(!empty($course))
            $course_names[] = $course->name;
    }
    ?>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'code',
            [
                'attribute' => 'type',
                'value' => $model->listType[$model->type]
            ],
            [
                'attribute' => 'discount_type',
                'value' => !empty($model->discountTypes[$model->discount_type]) ? $model->discountTypes[$model->discount_type] : 'N/A',
            ],
            [
                'attribute' => 'apply_condition',
                'value' => $model->type == Promotion::KIND_ORDER_APPLY ?  $model->listCondition[$model->apply_condition] : 'N/A'
            ],
            [
                'attribute' => 'apply_all',
                'value' => !empty($model->apply_all) ? 'Yes' : 'No'
            ],
            [
                'attribute' => 'apply_all_single_course',
                'value' => !empty($model->apply_all_single_course) ? 'Yes' : 'No'
            ],
            [
                'attribute' => 'apply_all_single_course_double',
                'value' => !empty($model->apply_all_single_course_double) ? 'Yes' : 'No'
            ],
            [
                'attribute' => 'apply_all_combo',
                'value' => !empty($model->apply_all_combo) ? 'Yes' : 'No'
            ],
            [
                'label' => 'Khóa học áp dụng',
                'value' => !empty($model->apply_all) ? 'N/A' : implode(",", $course_names)
            ],
            [
                'attribute' => 'value',
                'value' => ($model->discount_type == 0) ? $model->value .' %' : Yii::$app->formatter->asCurrency($model->value)
            ],

            'min_amount:currency',
            [
                'attribute' => 'start_date',
                'value' => (!empty($model->start_date) ? Yii::$app->formatter->asDatetime($model->start_date) : null)
            ],
            [
                'attribute' => 'end_date',
                'value' => (!empty($model->end_date) ? Yii::$app->formatter->asDatetime($model->end_date) : null)
            ],
            [
                'attribute' => 'partner_id',
                'label' => 'Partner',
                'value' => (!empty($model->partner_id) ? $model->partner->profile->name : 'N/A')
            ],
            'number_usage',
            'user_number_usage',
            [
                'attribute' => 'current_number_usage',
                'value' => empty($model->current_number_usage) ? 0 : $model->current_number_usage,
            ],
            [
                'attribute' => 'created_by',
                'value' => !empty($model->createdUser->profile->name) ? $model->createdUser->profile->name : $model->createdUser->email,
            ],
            'note',
            /*[
                'attribute' => 'apply_scope',
                'value' => !empty($model->listScope[$model->apply_scope]) ? $model->listScope[$model->apply_scope] : 'N/A'
            ],*/
            [
                'attribute' => 'status',
                'value' => !empty($model->status) ? 'Hoạt động' : 'Không hoạt động'
            ],
            'created_time:datetime',
            'updated_time:datetime',
        ],
    ]) ?>

</div>
