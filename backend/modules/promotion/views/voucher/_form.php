<?php

use yii\web\View;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\DateTimePicker;
use yii\widgets\MaskedInput;
use kartik\select2\Select2;
use kyna\promotion\models\Promotion;
use yii\web\JsExpression;
$crudTitles = Yii::$app->params['crudTitles'];
?>

<div class="voucher-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="col-sm-6">
        <?= $form->field($model, 'value')->widget(MaskedInput::className(), [
            'clientOptions' => [
                'alias' =>  'integer',
            ],
        ]) ?>
        
        <?= $form->field($model, 'auto')->checkbox() ?>
        
        <?= $form->field($model, 'quantity')->textInput(['readonly' => 'readonly']) ?>

        <?= $form->field($model, 'prefix')->textInput(['readonly' => 'readonly']) ?>

        <?= $form->field($model, 'code')->textInput(['maxlength' => true]) ?>



        <?= $form->field($model, 'type',['template'=>'<label class="radio-lbl">{label}</label>{input}'])->radioList(Promotion::getListType(),['id'=>'promotion-type','class'=>'iradio_minimal-blue','item' => function($index, $label, $name, $checked, $value) {

                                    $return = '<label>';
                                    if($checked)
                                        $return .= '<input type="radio" name="' . $name . '" value="' . $value . '"checked=true"'. '">';
                                    else
                                        $return .= '<input type="radio" name="' . $name . '" value="' . $value. '">';
                                    $return .= '<span>' . ucwords($label) . '</span>';
                                    $return .= '</label>';

                                    return $return;
                                }
                            ])->label('Loại voucher'); ?>
        <div class="order_apply">
            <?= $form->field($model, 'apply_condition',['template'=>'<label class="radio-lbl">{label}</label>{input}'])->radioList(Promotion::getListCondition())->label('Điều kiện áp dụng'); ?>
        </div>
        <div class="apply_condition ">
            <div class="required">
                <label class="control-label">Phạm vi áp dụng</label>
            </div>
            <div class="row">
                <div class="col-lg-6">
                     <?= $form->field($model, 'apply_all_single_course')->checkbox() ?>
                </div>
                <div id= "applyDouble" class="col-lg-6">
                  <?= $form->field($model, 'apply_all_single_course_double')->checkbox() ?>
                </div>
            </div>
            <?= $form->field($model, 'apply_all_combo')->checkbox() ?>

            <?= $form->field($model, 'apply_all')->checkbox() ?>
            <?= $form->field($model, 'apply_special_course')->checkbox() ?>

            <div id='couponCourse' >
                <?php if ($model->isNewRecord) { ?>
                    <?= $form->field($model, 'course_id')->widget(Select2::className(), [
                        'data' => Promotion::getAvailableCourses(),
                        'options' => [
                            'id' => 'courses',
                            'multiple' => true
                        ],
                    ]);
                    ?>
                <?php } else { ?>
                    <?php
                    $modelC = ArrayHelper::map($model->courses, 'id', 'name');
                    foreach ($modelC as $k => $val)
                        $row[] = $k;

                    echo '<label class="control-label" for="coupon-course_id">Courses</label>';
                    echo Select2::widget([
                        'name' => 'Coupon[course_id]',
                        'value' => $row, //
                        'data' => Promotion::getAvailableCourses(),
                        'options' => ['placeholder' => 'Select a group ...', 'multiple' => true],
                        'pluginOptions' => [
                            'tags' => true,
                            'maximumInputLength' => 10
                        ],
                    ]);

                    ?>
                <?php } ?>
            </div>
        </div>



    </div>
    <?php
    $resultsJs = <<< JS
    function (data, params) {
    params.page = params.page || 1;
    return {
        results: data.results,
        pagination: {
            more: (params.page * 30) < data.total_count
        }
    };
    }
JS;
    ?>
    <div class="col-sm-6">
        <?= $form->field($model, 'discount_type')->dropDownList(Promotion::getDiscountTypes()) ?>
        <?= $form->field($model, 'partner_id')->widget(Select2::className(), [

            'options' => ['prompt' => $crudTitles['prompt']],
            'pluginOptions' => [
                'allowClear' => true,
                'minimumInputLength' => 3,
                'ajax' => [
                    'url' => "/promotion/api/search-partner",
                    'dataType' => 'json',
                    'delay' => 250,
                    'data' => new JsExpression('function(params) { return {q:params.term, page: params.page}; }'),
                    'cache' => true,
                    'processResults' => new JsExpression($resultsJs),

                ],

            ],
        ]) ?>

        <?= $form->field($model, 'note')->textInput(['maxlength' => true]) ?>
        
        <?php
        if(empty($model->min_amount)){
            $model->min_amount = 0;
        }
        ?>
        <?= $form->field($model, 'min_amount')->widget(MaskedInput::className(), [
            'clientOptions' => [
                'alias' =>  'integer',
            ],

        ]) ?>
        <?= $form->field($model, 'start_date')->widget(DateTimePicker::className(), [
            'pluginOptions' => [
                'autoclose'=>true,
                'format' => 'dd/mm/yyyy hh:ii',
                'startDate' => date('d/m/Y 00:00')
            ],
            'pluginEvents' => [
                "changeDate" => "function(selected) { 
                   
                    var startDate = new Date(selected.date.valueOf());                       
                    var endDate = new Date(startDate.getFullYear(), startDate.getMonth(), startDate.getDate(), startDate.getHours(), startDate.getMinutes());
                    $('#promotion-end_date-datetime').datetimepicker('setStartDate', startDate);
                    var toDate = $('#promotion-end_date-datetime').datetimepicker('getDate');
                    if (startDate.getTime() > toDate.getTime()) {
                       $('#promotion-end_date-datetime').datetimepicker('setDate', endDate) 
                    }
                }",
            ]
        ]) ?>
        <?= $form->field($model, 'end_date')->widget(DateTimePicker::className(), [
                'pluginOptions' => [
                    'autoclose'=>true,
                    'format' => 'dd/mm/yyyy hh:ii',
                    'startDate' => date('d/m/Y 00:00')
                ]
            ]) ?>
        <?=$form->field($model, 'number_usage')->textInput(['type'=>'number']) ?>
        <?=$form->field($model, 'user_number_usage')->textInput(['type'=>'number']) ?>


    </div>
    
     <div class="clearfix">
    </div>
    
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? $crudTitles['create'] : $crudTitles['update'], ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::a($crudTitles['cancel'], ['index'], ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<?php
$script = <<< SCRIPT
$(document).ready(function(){
    $('body').on('change', '#promotion-auto', function(){
        var checked = $(this).prop("checked");
        if (checked == true) {
            $('#promotion-quantity').removeAttr('readonly');
            $('#promotion-prefix').removeAttr('readonly');
        
            $('#promotion-code').attr('readonly', 'readonly');
            $('#promotion-number_usage').val(1);
            $('#promotion-number_usage').attr('readonly', 'readonly');
            
            $('#promotion-user_number_usage').val(1);
            $('#promotion-user_number_usage').attr('readonly', 'readonly');
             
             
             
        } else {
            $('#promotion-quantity').val(1);
            $('#promotion-prefix').val('');
        
            $('#promotion-quantity').attr('readonly', 'readonly');
            $('#promotion-prefix').attr('readonly', 'readonly');
        
            $('#promotion-code').removeAttr('readonly');
            
             $('#promotion-number_usage').removeAttr('readonly');
            $('#promotion-user_number_usage').removeAttr('readonly');
        }
    });
    $('.voucher-form').find('form').submit(function()
    {
        var all_check = $('#promotion-apply_all').is(':checked');
        if(!all_check)
        {
              var all_single_course_check = $('#promotion-apply_all_single_course').is(':checked');
              var all_combo_check = $('#promotion-apply_all_combo').is(':checked');
              
             var courses = $('#courses').val();
          
             if(!all_single_course_check && !all_combo_check && courses == null){
               alert("Bạn phải chọn ít nhất một phạm vi áp dụng!");
               return false;
            }
        }
     
    });
});
SCRIPT;

$this->registerJs($script, View::POS_END, 'change-override-commission');

/** @var $this \yii\web\View */
$this->registerJs("

    var all_check = $('#promotion-apply_all').is(':checked');
    if(all_check){
         $('#couponCourse').hide();
         disableSingleCombo();
    }
    $('#promotion-apply_all').change(function (e) {
        
        if ($(this).is(':checked')) {
            $('#couponCourse').hide();
             disableSingleCombo();
             
        } else {
            
            enableSingleCombo();
            $('#applyDouble').hide();
            $('#couponCourse').show();
        }
    });
    $('#promotion-apply_all_single_course').change(function(e){
        
        if(checkAllClick()){
            $('#promotion-apply_all').trigger('click');
        }
    });
    $('#promotion-apply_all_single_course_double').change(function(e){
        
        if(checkAllClick()){
            $('#promotion-apply_all').trigger('click');
        }
    });
    $('#promotion-apply_all_combo').change(function(e){
        
        if(checkAllClick()){
            $('#promotion-apply_all').trigger('click');
        }
    });
    function checkAllClick(){
       return $('#promotion-apply_all_combo').is(':checked') && $('#promotion-apply_all_single_course').is(':checked') && $('#promotion-apply_all_single_course_double').is(':checked') 
    }
    
    $('#promotion-apply_all_single_course').change(function (e) {
        
        if ($(this).is(':checked')) {
            $('#applyDouble').show();
        } else {
            $('#applyDouble').hide();
        }
    });
    
    var showOrderApply = $(\"input[name='Promotion[type]']\").val();
    
    
     if(showOrderApply == 1)
       {
          hideOderApply();
          $('#promotion-min_amount').attr('disabled', true);
       }
       else{
         showOderApply();
         $('#promotion-min_amount').attr('disabled', false);
       }
     var showApplyDouble = $('#promotion-apply_all_single_course').is(':checked');
     if(showApplyDouble)
     {
      $('#applyDouble').show();
     }
     else{
         $('#applyDouble').hide();
     }
    
    $(\"input[name='Promotion[type]']\").on('change', function() {
       var type = $(this).val();
       if(type == 1)
       {
          hideOderApply();
          $('#promotion-min_amount').attr('disabled', true);
       }
       else{
         showOderApply();
         $('#promotion-min_amount').attr('disabled', false);
       }
    });
    function showOderApply()
    {
       $('.order_apply').show();
       $('.course_apply').hide();
    }
     function hideOderApply()
    {
       $('.order_apply').hide();
       $('.course_apply').show();
    }
    function enableSingleCombo()
    {
       $('#promotion-apply_all_single_course').attr('disabled', false);
       $('#promotion-apply_all_single_course').prop('checked', false);
       
       $('#promotion-apply_all_combo').attr('disabled', false);
       $('#promotion-apply_all_combo').prop('checked', false);
      
        $('#applyDouble').show();
        $('#promotion-apply_all_single_course_double').attr('disabled', false);
        $('#promotion-apply_all_single_course_double').prop('checked', false);
       
    }
    function disableSingleCombo()
    {
       $('#promotion-apply_all_single_course').prop('checked', true);
       $('#promotion-apply_all_single_course').attr('disabled', true);
       $('#promotion-apply_all_combo').prop('checked', true);
       $('#promotion-apply_all_combo').attr('disabled', true);
       $('#applyDouble').show();
       $('#promotion-apply_all_single_course_double').prop('checked', true);
       $('#promotion-apply_all_single_course_double').attr('disabled', true);
    }
"
);
?>
<style type="text/css">
    .iradio_minimal-blue label:last-child{
        margin-left: 100px;
    }
    .radio-lbl label{
        color: black!important;
    }
</style>
