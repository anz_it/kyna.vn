<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model kyna\faq\models\Faq */

$this->title = Yii::$app->controller->mainTitle;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Faqs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="faq-view">

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'title',
            'slug',
            [
                'attribute' => 'category_id',
                'value' => !empty($model->category) ? $model->category->title : null
            ],
            'order',
            [
                'attribute' => 'important',
                'value' => $model->importantText
            ],
            [
                'attribute' => 'status',
                'value' => $model->statusText
            ],
            'created_time:datetime',
            'updated_time:datetime',
        ],
    ]) ?>

</div>
