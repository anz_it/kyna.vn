<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use kyna\faq\models\Faq;
use kyna\faq\models\FaqCategory;
use yii\helpers\ArrayHelper;
use common\widgets\tinymce\TinyMce;

/* @var $this yii\web\View */
/* @var $model kyna\faq\models\Faq */
/* @var $form yii\widgets\ActiveForm */

$crudTitles = Yii::$app->params['crudTitles'];

$category = FaqCategory::findAllActive();
$category = ArrayHelper::map($category,'id','title');
?>

<div class="faq-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-lg-10">
            <div class="row">
                <div class="col-lg-6">
                    <?=
                    $form->field($model, 'title')->textInput([
                        'maxlength' => true,
                        'onchange' => "$('#cat-slug').val(convertToSlug($(this).val()))",
                    ])
                    ?>
                </div>
                <div class="col-lg-6">
                    <?= $form->field($model, 'slug')->textInput([
                        'id' => 'cat-slug',
                        'maxlength' => true,
                    ]) ?>
                </div>
            </div>
        </div>
        <div class="col-lg-2">
            <?=
            $form->field($model, 'is_important')->widget(Select2::classname(), [
                'data' => Faq::listImportant(),
                'hideSearch' => true,
                'options' => ['placeholder' => $crudTitles['prompt']],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">

        <div class="col-lg-4">
            <?=
            $form->field($model, 'status')->widget(Select2::classname(), [
                'data' => Faq::listStatus(),
                'hideSearch' => true,
                'options' => ['placeholder' => $crudTitles['prompt']],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ])
            ?>
        </div>
        <div class="col-lg-4">
            <?=
            $form->field($model, 'category_id')->widget(Select2::classname(), [
                'data' => $category,
                'hideSearch' => true,
                'options' => ['placeholder' => $crudTitles['prompt']],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ])
            ?>
        </div>
        <div class="col-lg-4">
            <?=
            $form->field($model, 'order')->textInput([
                'maxlength' => true,
                'type' => 'number',
                'min' => '1',
                'value' => $model->order == "" ? '1' : $model->order,
            ])
            ?>
        </div>
    </div>

    <?= $form->field($model, 'content')->widget(TinyMce::className(), [
        'layout' => 'full',
    ])
    ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<?= $this->render('//_partials/stringjs') ?>