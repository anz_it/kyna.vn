<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\grid\GridView;
use yii\widgets\Pjax;
use kartik\select2\Select2;
use kyna\faq\models\Faq;
use kyna\faq\models\FaqCategory;
/* @var $this yii\web\View */
/* @var $searchModel kyna\faq\models\search\FaqSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$crudTitles = Yii::$app->params['crudTitles'];
$crudButtonIcons = Yii::$app->params['crudButtonIcons'];

$this->title = Yii::$app->controller->mainTitle;
$this->params['breadcrumbs'][] = $this->title;

$user = Yii::$app->user;
?>

<div class="row faq-index">
    <div class="col-xs-12">
        <div class="beuser-index">
            <p>
                <?= Html::a($crudButtonIcons['create'] . ' ' . $crudTitles['create'], ['create'], ['class' => 'btn btn-success']) ?>
            </p>
            <div>
                <?php Pjax::begin(['timeout' => Yii::$app->params['timeOutPjax'], 'id' => 'idPjaxReload']); ?>
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],
                        'title',
                        'slug',
                        [
                            'attribute' => 'category_id',
                            'format' => 'html',
                            'content' => function ($model) {
                                return !empty($model->category) ? $model->category->title : null;
                            },
                            'filter' => Select2::widget([
                                'model' => $searchModel,
                                'attribute' => 'category_id',
                                'data' => ArrayHelper::map(FaqCategory::findAllActive(), 'id', 'title'),
                                'options' => ['placeholder' => $crudTitles['prompt']],
                                'pluginOptions' => [
                                    'allowClear' => true
                                ],
                            ])
                        ],
                        'order',
                        [
                            'attribute' => 'status',
                            'format' => 'raw',
                            'value' => function ($model) {
                                return $model->statusButton;
                            },
                            'contentOptions' => [
                                'style' => 'min-width: 115px;'
                            ],
                            'filter' => Html::activeDropDownList($searchModel, 'status', Faq::listStatus(false), ['class' => 'form-control', 'prompt' => $crudTitles['prompt']]),
                        ],
                        [
                            'class' => 'app\components\ActionColumnCustom',
                            'visibleButtons' => [
                                'update' => $user->can('Faq.Update'),
                                'view' => $user->can('Faq.View'),
                                'delete' => $user->can('Faq.Delete'),
                            ],
                        ],
                    ],
                ]); ?>
                <?php Pjax::end(); ?>
            </div>
        </div>
    </div>
</div>
