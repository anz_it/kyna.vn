<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model kyna\faq\models\FaqCategory */

$this->title = Yii::t('app', 'Thêm mới');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Faq Categories'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="faq-category-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
