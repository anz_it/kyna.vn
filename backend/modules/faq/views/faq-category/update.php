<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model kyna\faq\models\FaqCategory */

$this->title = Yii::t('app', 'Cập nhật {modelClass}: ', [
    'modelClass' => 'Danh mục Faq',
]) . $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Danh mục Faq'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Cập nhật');
?>
<div class="faq-category-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
