<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use common\helpers\CDNHelper;

/* @var $this yii\web\View */
/* @var $model kyna\faq\models\FaqCategory */

$this->title = Yii::$app->controller->mainTitle;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Faq Categories'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="faq-category-view">

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'title',
            [
                'attribute' => 'image',
                'format' => 'raw',
                'value' => Html::img(CDNHelper::getMediaLink() . $model->image, ['style' => 'max-height: 50px;']),
            ],
            [
                'attribute' => 'status',
                'value' => $model->statusText
            ],
            'created_time:datetime',
            'updated_time:datetime',
        ],
    ]) ?>

</div>
