<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\widgets\upload\Upload;
use kartik\select2\Select2;
use kyna\faq\models\FaqCategory;

/* @var $this yii\web\View */
/* @var $model kyna\faq\models\FaqCategory */
/* @var $form yii\widgets\ActiveForm */

$crudTitles = Yii::$app->params['crudTitles'];
?>

<div class="faq-category-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
    <div class="row">
        <div class="col-lg-12">
            <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6">
            <?= $form->field($model, 'image')->widget(Upload::className(), ['display' => 'image']) ?>
        </div>
        <div class="col-lg-6">
            <?=
            $form->field($model, 'status')->widget(Select2::classname(), [
                'data' => FaqCategory::listStatus(),
                'hideSearch' => true,
                'options' => ['placeholder' => $crudTitles['prompt']],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ])
            ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
