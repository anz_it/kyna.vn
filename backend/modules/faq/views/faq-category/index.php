<?php

use yii\helpers\Html;
use common\components\GridView;
use yii\widgets\Pjax;
use common\widgets\BtnAddWidget;
use kyna\faq\models\FaqCategory;
use common\components\SortableColumn;
use common\helpers\CDNHelper;

/* @var $this yii\web\View */
/* @var $searchModel kyna\faq\models\search\FaqCategorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$crudTitles = Yii::$app->params['crudTitles'];

$this->title = Yii::$app->controller->mainTitle;
$this->params['breadcrumbs'][] = $this->title;

$user = Yii::$app->user;
?>
<div class="">
    <div class="box">
        <?php if (Yii::$app->user->can('Faq.Category.Create')) : ?>
            <div class="box-header">
                <?= BtnAddWidget::widget() ?>
            </div>
        <?php endif; ?>

        <div class="box-body">
            <?php Pjax::begin(['timeout' => Yii::$app->params['timeOutPjax'], 'id' => 'idPjaxReload']) ?>
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    'id',
                    'title',
                    [
                        'attribute' => 'image',
                        'format' => 'html',
                        'value' => function ($model) {
                            return Html::img(CDNHelper::getMediaLink() . $model->image, ['style' => 'max-height: 50px;']);
                        },
                        'filter' => false,
                        'options' => ['class' => 'col-xs-1']
                    ],
                    [
                        'attribute' => 'status',
                        'format' => 'raw',
                        'value' => function ($model) {
                            return $model->statusButton;
                        },
                        'contentOptions' => [
                            'style' => 'min-width: 115px;'
                        ],
                        'filter' => Html::activeDropDownList($searchModel, 'status', FaqCategory::listStatus(false), ['class' => 'form-control', 'prompt' => $crudTitles['prompt']]),
                    ],
                    [
                        'class' => 'app\components\ActionColumnCustom',
                        'visibleButtons' => [
                            'update' => $user->can('Faq.Category.Update'),
                            'view' => $user->can('Faq.Category.View'),
                            'delete' => $user->can('Faq.Category.Delete'),
                        ],
                    ],
                ]
            ]); ?>
            <?php Pjax::end() ?>
        </div>
    </div>
</div>
