<?php

namespace app\modules\faq\controllers;

use common\lib\CDNImage;
use Yii;
use kyna\faq\models\FaqCategory;
use kyna\faq\models\search\FaqCategorySearch;
use yii\filters\AccessControl;
use app\components\controllers\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * FaqCategoryController implements the CRUD actions for FaqCategory model.
 */
class FaqCategoryController extends Controller
{
    public $mainTitle = 'Danh mục Câu hỏi thường gặp';
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'view'],
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->user->can('Faq.Category.View');
                        },
                    ],
                    [
                        'allow' => true,
                        'actions' => ['create'],
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->user->can('Faq.Category.Create');
                        },
                    ],
                    [
                        'allow' => true,
                        'actions' => ['update','change-status'],
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->user->can('Faq.Category.Update');
                        },
                    ],
                    [
                        'allow' => true,
                        'actions' => ['delete'],
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->user->can('Faq.Category.Delete');
                        },
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all FaqCategory models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new FaqCategorySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single FaqCategory model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new FaqCategory model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new FaqCategory();
        $model->setScenario('create');

        if ($model->load(Yii::$app->request->post())) {
            if ($model->status == null)
                $model->status = 1;
            if ($model->is_deleted == null)
                $model->is_deleted = 0;
            if ($model->save()) {

                $flashData = 'Khóa học đã được tạo' . PHP_EOL;
                $alert = 'success';

                $flashData .= '<ul class="fa-ul">';
                if ($image_url = $this->_uploadImage($model, 'image')) {
                    $model->image = $image_url;
                    $model->save(false);
                    $flashData .= '<li><i class="fa fa-li fa-check"></i> Upload image thành công</li>';
                }

                $flashData .= '</ul>';

                Yii::$app->session->setFlash($alert, $flashData);

                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing FaqCategory model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            if ($image_url = $this->_uploadImage($model, 'image')) {
                $model->image = $image_url;
            }
            if ($model->save(false)) {
                Yii::$app->session->setFlash('success', 'Cập nhật thành công.');

                $noError = true;

                if ($noError) {
                    return $this->redirect(['view', 'id' => $model->id]);
                }
            }
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Finds the FaqCategory model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return FaqCategory the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = FaqCategory::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    private function _uploadImage($model, $attribute) {
        $files = UploadedFile::getInstances($model, $attribute);

        if (!sizeof($files)) {
            $oldAttributes = $model->oldAttributes;

            return isset($oldAttributes[$attribute]) ? $oldAttributes[$attribute] : false;
        }

        $file = $files[0];
        $cdnImage = new CDNImage($model, $attribute);
        $result = $cdnImage->upload($file);

        return $result;
    }
}
