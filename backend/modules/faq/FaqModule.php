<?php

namespace app\modules\faq;

/**
 * faq module definition class
 */
class FaqModule extends \kyna\base\BaseModule
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\faq\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
