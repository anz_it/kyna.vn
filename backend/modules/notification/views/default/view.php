<?php
/**
 * Created by PhpStorm.
 * User: ngunp
 * Date: 7/4/2017
 * Time: 1:39 PM
 */
/* @var $this Yii\web\View */

use app\modules\notification\assets\NotificationAsset;
use yii\web\View;
use yii\widgets\DetailView;
use common\helpers\Html;
use yii\helpers\Url;


NotificationAsset::Register($this);

$this->title = $this->context->mainTitle;
$this->params['breadcrumbs'][] = ['label' => 'Notification', 'url' => ['/notification/default/index']];
$this->params['breadcrumbs'][] = 'Details';

$formatter = Yii::$app->formatter;

$mediaBase = Yii::$app->params['media_link'];

?>

<?php
    echo DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'attribute' => 'id',
                'value' => $model['notification_id'],
            ],
            'name',
            'title',
            'body',
            [
                'attribute' => 'icon',
                'value' =>  isset($model['icon'])? (Html::img($mediaBase.$model['icon'], [
                    'width' => '40px',
                ])) : null,
                'format' => 'raw',
            ],
            [
                'attribute' => 'image',
                'value' =>  isset($model['image'])? (Html::img($mediaBase.$model['image'], [
                    'width' => '100px',
                ])) : null,
                'format' => 'raw',

            ],
            'redirect_url',
            'type',
            'setting',
            'is_enabled',
            'start_time',
            'last_occure',
            'next_occure',
        ]
    ]);
    echo Html::a('Update',
        Url::to('/notification/action/update?id='.$model['notification_id']),
        [
            'class' => 'btn btn-success'

    ]);
//    var_dump($model);
?>
<?php
//    $this->registerJsFile('/js/notification/push-notification-preview.js', ['position' => View::POS_END]);
?>