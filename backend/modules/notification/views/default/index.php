<?php
/**
 * Created by PhpStorm.
 * User: ngunp
 * Date: 7/4/2017
 * Time: 10:38 AM
 */

use app\modules\notification\assets\NotificationAsset;
use yii\bootstrap\Nav;
use yii\widgets\Pjax;
use yii\grid\GridView;
use common\helpers\Html;
use common\helpers\NotificationHelper;

NotificationAsset::Register($this);
$this->title = $this->context->mainTitle;
$this->params['breadcrumbs'][] = $this->title;

$mediaUrl = Yii::$app->params['media_link'];
$formatter = Yii::$app->formatter;
$user = Yii::$app->user;
?>

<div class="notification-index-content">
    <?= Nav::widget([
        'items' => $tabs,
        'options' => ['class' => 'nav-pills'],
    ]) ?>

    <?php Pjax::begin(['id' => 'idPjaxReload']) ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            [
                'header' => 'ID',
                'format' => 'raw',
                'value' => function ($model, $key, $index) use ($formatter) {
                    $html = '<strong>#' . $model['id'] . '</strong><br>';
                    return $html;
                }
            ],
            [
                'header' => 'NAME',
                'format' => 'raw',
                'value' => function ($model, $key, $index) use ($formatter) {
                    $html = $model['name'].'<br>';
                    return $html;
                }
            ],
            [
                'header' => 'DETAILS',
                'format' => 'raw',
                'value' => function ($model, $key, $index) use ($formatter) {
                    $mediaUrl = Yii::$app->params['media_link'];
                    $html = '<strong>Title: </strong>'.$model['title'].'<br>';
                    $html .= '<strong>Body: </strong>'. (empty($model['body'])? 'N/A' : $model['body']) .'<br>';
                    if (!empty($model['icon'])) {
                        $html .= '<strong>Icon: </strong>'. (empty($model['icon'])? 'N/A' : '<image src="' . $mediaUrl . $model['icon']).'" style="height: 40px;">' .'<br>';
                    }
                    if (!empty($model['image'])) {
                        $html .= '<strong>Image: </strong>'. (empty($model['image'])? 'N/A' : '<image src="' . $mediaUrl . $model['image'] .'" style="height: 80px;">') .'<br>';
                    }
                    if (!empty($model['redirect_url'])) {
                        $html .= '<strong>Redirect URL: </strong>'. (empty($model['redirect_url'])? 'N/A' : $model['redirect_url']) .'<br>';
                    }
                    return $html;
                }
            ],
            [
                'header' => 'REPEAT DETAILS',
                'format' => 'raw',
                'value' => function ($model, $key, $index) use ($formatter) {
                    $html = '<strong>Repeat Type: </strong>'. NotificationHelper::$repeatTypeText[$model['type']] .'<br>';
                    $html .= '<strong>Start Time: </strong>'. $formatter->asDatetime($model['start_time']).'<br>';
                    $html .= '<strong>Last Occure: </strong>'. (empty($model['last_occure'])? 'null' : $formatter->asDatetime($model['last_occure'])) .'<br>';
                    $html .= '<strong>Next Occure: </strong>'. (empty($model['next_occure'])? 'null' : $formatter->asDatetime($model['next_occure'])) .'<br>';

                    return $html;
                }
            ],
            [
                'header' => 'ENABLE',
                'format' => 'raw',
                'value' => function ($model, $key, $index) use ($formatter) {
                    $html = (($model['is_enabled'])? 'Enabled' : 'Disable') .'<br>';
                    return $html;
                }
            ],
            [
                'header' => 'Xử lý',
                'class' => 'yii\grid\ActionColumn',
                'contentOptions' => ['style' => 'width: 90px;'],
                'template' => '{view}{update}{copy}{delete}',
                'buttons' => [
                    'view' => function ($url, $model, $key) {
                        $url = "view?id=".$model['notification_id'];
                        return Html::a('<i class="ion-android-search"></i>', $url, [
                            'class' => 'btn btn-sm btn-default btn-block ',
                            'title' => "Xem chi tiết"
                        ]);
                    },
                    'update' => function ($url, $model, $key) {
                        $url = '/notification/action/update?id='.$model['notification_id'];
                        return Html::a('<i class="ion-edit"></i>', $url, [
                            'class' => 'btn btn-sm btn-default btn-block',
                            'title' => "Cập nhật"
                        ]);
                    },
                    'copy' => function ($url, $model, $key) {
                        $url = '/notification/action/copy?id='.$model['notification_id'];
                        return Html::a('<i class="ion-ios-copy"></i>', $url, [
                            'class' => 'btn btn-sm btn-default btn-block',
                            'title' => "Sao chép"
                        ]);
                    },
                    'delete' => function ($url, $model, $key) {
                        $url = '/notification/action/delete?id='.$model['notification_id'];
                        return Html::a('<i class="ion-trash-a"></i>', $url, [
                            'class' => 'btn btn-sm btn-default btn-block',
                            'data-confirm' => 'Bạn có chắc chắn muốn xóa notification này?',
                            'data-method' => 'post',
                            'title' => "Xóa"
                        ]);
                    },
                ],
            ],
        ]
    ]); ?>
    <?php Pjax::end() ?>
</div>