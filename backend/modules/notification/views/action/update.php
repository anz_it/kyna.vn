<?php
/**
 * Created by PhpStorm.
 * User: ngunp
 * Date: 7/10/2017
 * Time: 2:56 PM
 */

use yii\widgets\ActiveForm;
use kartik\widgets\DateTimePicker;

$this->params['breadcrumbs'][] = ['label' => 'Notification', 'url' => ['/notification/default/index']];
$this->params['breadcrumbs'][] = $this->title;

$formId = 'notification-update-form';
$repeatTypeList = array_values($repeatTypeList);

$formatter = Yii::$app->formatter;
?>

<?= $this->render('_form', [
    'repeatTypeList' => $repeatTypeList,
//    'model' => $model,
    'notification' => $notification,
    'schedule' => $schedule,
    'action' => Yii::$app->controller->action->id,
]);
?>

<?php
$js = "
    var settings = ". $schedule->setting .";
    // Set default value
    $('#schedule-start_time_formatted').datetimepicker('setDate', new Date('". $formatter->asDatetime($schedule->start_time, "Y-M-d H:m:s") ."'));
    
    $('#setting-daily-repeat').val(settings.daily.repeat_rate);
    $('#setting-weekly-repeat').val(settings.weekly.repeat_rate);
    $.each(settings.weekly.days, function(index, value) {
//        console.log(value);
        $('#settings-weekly input[value=\"'+value+'\"]').click();
    });
    $('#setting-monthly-repeat').val(settings.monthly.repeat_rate);

 
//    $('#schedule-type input[type=\"radio\"][value=\"". $schedule->type ."\"]').click();

    $('.sub-settings').hide()
    switch($('div#schedule-type input[type=\"radio\"]:checked').val()) {
        case '0':             // ONE TIME
            $('#settings-container').hide();
            $('#settings').hide();
            break;
        case '1':             // DAILY
            $('#settings-daily').show();
            $('#settings-container').show();
            $('#settings').show();

            break;
        case '2':             // WEEKLY
            $('#settings-weekly').show();
            $('#settings-container').show();
            $('#settings').show();

            break;
        case '3':             // MONTHLY
            $('#settings-monthly').show();
            $('#settings-container').show();
            $('#settings').show();
            break;
        default:
            break;
    }

";

$this->RegisterJs($js);
