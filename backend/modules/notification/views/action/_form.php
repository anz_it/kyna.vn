<?php
/**
 * Created by PhpStorm.
 * User: ngunp
 * Date: 7/13/2017
 * Time: 10:00 AM
 */

use yii\widgets\ActiveForm;
use kartik\widgets\DateTimePicker;
use yii\web\View;
use common\helpers\Html;
use common\widgets\upload\Upload;
use kartik\widgets\Select2;
use yii\web\JsExpression;
use yii\helpers\Url;
use \kyna\course\models\Category;
use app\modules\notification\models\Notification;
$mediaBase = Yii::$app->params['media_link']
?>

<?php $form = ActiveForm::begin([
    'options' => [
        'class' => 'notification-form',
        'enctype' => 'multipart/form-data',
    ]
]) ?>
<div class="notification-form">
    <div class="row">
        <section class="col-md-8 col-sm-6">
            <h3>Notification detail</h3>
            <div class="row">
                <div class="col-sm-12">
                    <?= $form->field($notification, 'is_type')->dropDownList(app\modules\notification\models\Notification::listTypeDevice()) ?>
                </div>
            </div>

            <div class="row group-type">
                <div class="col-sm-12">
                    <?= $form->field($notification, 'topic')->dropDownList(app\modules\notification\models\Notification::listTopic()) ?>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-12">
                    <?= $form->field($notification, 'name') ?>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <?= $form->field($notification, 'title')->textarea(['rows' => 3]) ?>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <?= $form->field($notification, 'body')->textarea(['rows' => 3])?>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <?= $form->field($notification, 'icon')->widget(Upload::className(), ['display' => 'image'])?>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <?= $form->field($notification, 'image')->widget(Upload::className(), ['display' => 'image'])?>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <?= $form->field($notification, 'redirect_url')?>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <?= $form->field($notification, 'description')->textarea(['rows' => 6])?>
                </div>
            </div>
            <div class="row group-type">
                <div class="col-sm-12">
                    <h3>Chọn loại data gửi </h3>
                    <?= $form->field($notification, 'screen_type_mobile')->dropDownList(app\modules\notification\models\Notification::$screen_type_mobile) ?>
                </div>
                <div class="col-sm-12">
                    <?php
                    $url = Url::toRoute(['/notification/default/get-data']);
                    $name = '';
                    if(!empty($notification->data)){
                        $name = Notification::getName($notification->screen_type_mobile,$notification->data);
                    }
                    echo $form->field($notification, 'data')->widget(Select2::classname(), [
                        'initValueText' => $name,
                        'options' => ['placeholder' => 'Search data ...'],
                        'pluginOptions' => [
                            'allowClear' => true,
                            'minimumInputLength' => 1,
                            'language' => [
                                'errorLoading' => new JsExpression("function () { return 'Waiting for results...'; }"),
                            ],
                            'ajax' => [
                                'url' => $url,
                                'dataType' => 'json',
                                'data' => new JsExpression('function(params) { return {
                                q:params.term, 
                                type: $("#notification-screen_type_mobile").val()
                                }; }')
                            ],
                            'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                            'templateResult' => new JsExpression('function(city) { return city.text; }'),
                            'templateSelection' => new JsExpression('function (city) { return city.text; }'),
                        ],
                    ]);
                    ?>
                </div>
            </div>
        </section>
        <section class="col-md-4 col-sm-6">
            <h3>Scheduler</h3>
            <div class="row">
                <div class="col-sm-12">
                    <?= $form->field($schedule, 'is_enabled')->checkbox(['checked' => 'checked'])?>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <?= $form->field($schedule, 'start_time_formatted')->widget(DateTimePicker::className(), [
                        'removeButton' => false,
                        'pluginOptions' => [
                            'todayHighlight' => true,
                            'todayBtn' => true,
                            'autoclose' => true,
                        ],
                    ])?>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <?= $form->field($schedule, 'type')->radioList($repeatTypeList)?>
                </div>
            </div>
            <div id="settings-container" style="display: none;">
                <div class="row">
                    <div class="col-sm-12">
                        <?= $form->field($schedule, 'setting')->hiddenInput() ?>
                    </div>
                </div>
            </div>
            <div id="settings" class="settings" style="display: none;">
                <div id="settings-daily" class="panel panel-default sub-settings">
                    <div class="panel-body">
                        Occure every <input id="setting-daily-repeat" type="number" min="1" max="365"> days
                    </div>
                </div>
                <div id="settings-weekly" class="panel panel-default sub-settings">
                    <div class="panel-body">
                        Occure every <input id="setting-weekly-repeat" type="number" min="1" max="53"> weeks on<br>
                        <input class="col-sm-12" id="setting-weekly-days" "type="text" disabled>
                        <label class="col-sm-3"><input type="checkbox" value="Sunday"> Sunday </label>
                        <label class="col-sm-3"><input type="checkbox" value="Monday"> Monday </label>
                        <label class="col-sm-3"><input type="checkbox" value="Tuesday"> Tuesday </label>
                        <label class="col-sm-3"><input type="checkbox" value="Wednesday"> Wednesday </label>
                        <label class="col-sm-3"><input type="checkbox" value="Thursday"> Thursday </label>
                        <label class="col-sm-3"><input type="checkbox" value="Friday"> Friday </label>
                        <label class="col-sm-3"><input type="checkbox" value="Saturday"> Saturday </label>
                    </div>
                </div>
                <div id="settings-monthly" class="panel panel-default sub-settings">
                    <div class="panel-body">
                        Occure every <input id="setting-monthly-repeat" type="number" min="1" max="12"> months<br>
                    </div>
                </div>
            </div>

            <div id="adv_settings" style="display: none;">adv setting</div>

            <?php if ($action=='update'): ?>
                <div id="id-container" style="display: none;">
                    <div class="row">
                        <div class="col-sm-12">
                            <?= $form->field($notification, 'id')->hiddenInput(['value' => $notification->id]) ?>
                        </div>
                    </div>
                </div>

            <?php endif; ?>
            <div class="row">
                <div class="col-sm-6">
                    <button type="submit" class="btn btn-primary btn-block"><?= ($action == 'update') ? 'Update' : 'Create' ?></button>
                </div>
                <div class="col-sm-6">
                    <a href="#" class="btn btn-info btn-block notification-preview">Preview</button></a>
                </div>
            </div>
            <div class="row" style="margin-top: 15px">
                <div class="col-sm-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Emoji
                        </div>
                        <div class="panel-body" style="font-size: large">
                            😀 😃 😄 😁 😆 😅 😂 🤣 ☺️ 😊 😇 🙂 🙃 😉 😌 😍 😘 😗 😙 😚 😋 😜 😝 😛 🤑 🤗 🤓 😎 🤡 🤠 😏 😒 😞 😔 😟 😕 🙁 ☹️ 😣 😖 😫 😩 😤 😠 😡 😶 😐 😑 😯 😦 😧 😮 😲 😵 😳 😱 😨 😰 😢 😥 🤤 😭 😓 😪 😴 🙄 🤔 🤥 😬 🤐 🤢 🤧 😷 🤒 🤕 😈 👿 👹 👺 💩 👻 💀 ☠️ 👽 👾 🤖 🎃 😺 😸 😹 😻 😼 😽 🙀 😿 😾 👐 🙌 👏 🙏 🤝 👍 👎 👊 ✊ 🤛 🤜 🤞 ✌️ 🤘 👌 👈 👉 👆 👇 ☝️ ✋ 🤚 🖐 🖖 👋 🤙 💪 🖕 ✍️ 🤳 💅 🖖 💄 💋 👄 👅 👂 👃 👣 👁 👀 🗣 👤 👥 👶 👦 👧 👨 👩 👱‍♀️ 👱 👴 👵 👲 👳‍♀️ 👳 👮‍♀️ 👮 👷‍♀️ 👷 💂‍♀️ 💂 🕵️‍♀️ 🕵️ 👩‍⚕️ 👨‍⚕️ 👩‍🌾 👨‍🌾 👩‍🍳 👨‍🍳 👩‍🎓 👨‍🎓 👩‍🎤 👨‍🎤 👩‍🏫 👨‍🏫 👩‍🏭 👨‍🏭 👩‍💻 👨‍💻 👩‍💼 👨‍💼 👩‍🔧 👨‍🔧 👩‍🔬 👨‍🔬 👩‍🎨 👨‍🎨 👩‍🚒 👨‍🚒 👩‍✈️ 👨‍✈️ 👩‍🚀 👨‍🚀 👩‍⚖️ 👨‍⚖️ 🤶 🎅 👸 🤴 👰 🤵 👼 🤰 🙇‍♀️ 🙇 💁 💁‍♂️ 🙅 🙅‍♂️ 🙆 🙆‍♂️ 🙋 🙋‍♂️ 🤦‍♀️ 🤦‍♂️ 🤷‍♀️ 🤷‍♂️ 🙎 🙎‍♂️ 🙍 🙍‍♂️ 💇 💇‍♂️ 💆 💆‍♂️ 🕴 💃 🕺 👯 👯‍♂️ 🚶‍♀️ 🚶 🏃‍♀️ 🏃 👫 👭 👬 💑 👩‍❤️‍👩 👨‍❤️‍👨 💏 👩‍❤️‍💋‍👩 👨‍❤️‍💋‍👨 👪 👨‍👩‍👧 👨‍👩‍👧‍👦 👨‍👩‍👦‍👦 👨‍👩‍👧‍👧 👩‍👩‍👦 👩‍👩‍👧 👩‍👩‍👧‍👦 👩‍👩‍👦‍👦 👩‍👩‍👧‍👧 👨‍👨‍👦 👨‍👨‍👧 👨‍👨‍👧‍👦 👨‍👨‍👦‍👦 👨‍👨‍👧‍👧 👩‍👦 👩‍👧 👩‍👧‍👦 👩‍👦‍👦 👩‍👧‍👧 👨‍👦 👨‍👧 👨‍👧‍👦 👨‍👦‍👦 👨‍👧‍👧 👚 👕 👖 👔 👗 👙 👘 👠 👡 👢 👞 👟 👒 🎩 🎓 👑 ⛑ 🎒 👝 👛 👜 💼 👓 🕶 🌂 ☂️
                        </div>
                    </div>
                </div>
                <div class="col-sm-12">
                    <?= Html::a('More Emoji',
                        'http://getemoji.com/',
                        [
                            'class' => 'btn btn-success',
                            'target' => '_blank',
                        ]);
                    ?>
                </div>
            </div>

        </section>
    </div>
</div>

<div id="idPjaxReload"></div>

<?php ActiveForm::end() ?>

<?php
    $_formJs = "
        // date time picker format
        $('#schedule-start_time_formatted').prop('disabled', true);
    
        var weekdays_pattern = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
        var weekly_days = [];

                // show settings
        $('div#schedule-type input[type=\"radio\"]').on('change', function(e) {
            $('.sub-settings').hide()
//            console.log($('#schedule-type input[type=\"radio\"]:checked').val());
            switch($('div#schedule-type input[type=\"radio\"]:checked').val()) {
                case '0':             // ONE TIME
                    $('#settings-container').hide();
                    $('#settings').hide();
                    break;
                case '1':             // DAILY
                    $('#settings-daily').show();
                    $('#settings-container').show();
                    $('#settings').show();

                    break;
                case '2':             // WEEKLY
                    $('#settings-weekly').show();
                    $('#settings-container').show();
                    $('#settings').show();

                    break;
                case '3':             // MONTHLY
                    $('#settings-monthly').show();
                    $('#settings-container').show();
                    $('#settings').show();
                    break;
                default:
                    break;
            }
        });
        $('#settings-weekly input[type=\"checkbox\"]').on('change', function(e) {
            if ($(this).is(':checked')) {
                weekly_days.push($(this).val());
                // sort by patterns
                var _sorted = [];
                $.each(weekdays_pattern, function(index, value) {
                    if (weekly_days.indexOf(value) >= 0) {
                        _sorted.push(value)
                    }
                });
                weekly_days = _sorted;
            } else {
                weekly_days.splice(weekly_days.indexOf($(this).val()), 1);
            }
            $('#setting-weekly-days').val(weekly_days)
            console.log(weekly_days);
            
        });

        $('.notification-form').on('submit', function(e) {
            console.log(weekly_days);
            var settings = {};
            // daily settings
            settings['daily'] = {
                'repeat_rate' : $('#setting-daily-repeat').val()
            }
            // weekly settings
            settings['weekly'] = {
                'repeat_rate' : $('#setting-weekly-repeat').val(),
                'days': weekly_days,
            }
            // monthly settings
            settings['monthly'] = {
                'repeat_rate' : $('#setting-monthly-repeat').val()
            }

            $('#settings-container input').val(JSON.stringify(settings));
            
            $('#schedule-start_time_formatted').prop('disabled', false);
        });
        
        iconTmp = null;
        $('#notification-icon').on('change', function(e) {
            var icon = $('#notification-icon')[0].files[0];
            if (icon != null) {
               var reader = new FileReader();
               reader.readAsDataURL(icon);
               reader.onload = function () {
                    iconTmp = reader.result;
               };
               reader.onerror = function (error) {
                 console.log('Error: ', error);
               };
            }
        });
        
        imageTmp = null;
        $('#notification-image').on('change', function(e) {
            var image = $('#notification-image')[0].files[0];
            if (image != null) {
               var reader = new FileReader();
               reader.readAsDataURL(image);
               reader.onload = function () {
                    imageTmp = reader.result;
               };
               reader.onerror = function (error) {
                 console.log('Error: ', error);
               };
            }
        });

        
        $('.notification-preview').on('click', function(e) {
            e.preventDefault();
            // Get icon 
            icon = null;
            if (iconTmp) {
                icon = iconTmp;
            } else {
                icon = $('#notification-icon-wrapper img').attr('src');
                console.log(icon);
            }
            // get image
            image = null;
            if (imageTmp) {
                image = imageTmp;
            } else {
                image = $('#notification-image-wrapper img').attr('src');
                console.log(image);
            }

            
            if (Notification.permission == 'granted') {
                navigator.serviceWorker.getRegistration('/js/notification/')
                    .then(function(reg) {
                        // prepare param
                        var title = $('#notification-title').val();
                        var options = {
                            body: $('#notification-body').val(),
                            icon: icon,
                            image: image,
                            data: {
                                url: $('#notification-redirect_url').val()
                            }
                        };
                        console.log(options);
                        reg.showNotification(title, options);
                    });
            }
            
        });
        
        $('select#notification-is_type').each(function() {
            var defaultValue = $(this).val();
                switch(defaultValue) {
                    case '1':
                        $('.field-notification-icon').hide();
                        $('.field-notification-image').hide();
                        $('.field-notification-redirect_url').hide();
                        $('.field-notification-description').hide();
                        $('.group-type').show();
                        $('.group-select-data').show();
                        break;
                    case '2':
                        $('.field-notification-icon').show();
                        $('.field-notification-image').show();
                        $('.field-notification-redirect_url').show();
                        $('.field-notification-description').show();
                        $('.group-type').hide();
                        $('.group-select-data').hide();
                        break;
                    default:
                        $('.field-notification-icon').hide();
                        $('.field-notification-image').hide();
                        $('.field-notification-redirect_url').hide();
                        $('.field-notification-description').hide();
                        $('.group-type').hide();
                        $('.group-select-data').hide();
                }
        });
        
        $('select#notification-is_type').on('change', function(){
                var value = this.value;
                switch(value) {
                    case '1':
                        $('.field-notification-icon').hide();
                        $('.field-notification-image').hide();
                        $('.field-notification-redirect_url').hide();
                        $('.field-notification-description').hide();
                        $('.group-type').show();
                        $('.group-select-data').show();
                        break;
                    case '2':
                        $('.field-notification-icon').show();
                        $('.field-notification-image').show();
                        $('.field-notification-redirect_url').show();
                        $('.field-notification-description').show();
                        $('.group-type').hide();
                        $('.group-select-data').hide();
                        break;
                    default:
                        $('.field-notification-icon').hide();
                        $('.field-notification-image').hide();
                        $('.field-notification-redirect_url').hide();
                        $('.field-notification-description').hide();
                        $('.group-type').hide();
                        $('.group-select-data').hide();
                }
        });     
        
    ";
    $this->RegisterJs($_formJs, View::POS_END);
//    $this->registerJsFile('/js/notification/push-notification-preview.js', ['position' => View::POS_END]);
?>