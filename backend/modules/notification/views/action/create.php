<?php
/**
 * Created by PhpStorm.
 * User: ngunp
 * Date: 7/4/2017
 * Time: 2:07 PM
 */


$this->title = 'Create Notification';
$this->params['breadcrumbs'][] = ['label' => 'Notification', 'url' => ['/notification/default/index']];
$this->params['breadcrumbs'][] = $this->title;

$formId = 'notification-create-form';
$repeatTypeList = array_values($repeatTypeList);
//phpinfo();
?>

<?=
    $this->render('_form', [
        'repeatTypeList' => $repeatTypeList,
        'notification' => $notification,
        'schedule' => $schedule,
//        'model' => $model,
        'action' => 'Create'
    ])
?>

<?php
    $js = "
        // Set default value
        
        $('#schedule-start_time_formatted').datetimepicker('setDate', new Date());
//        $('#schedule-is_enabled').attr('checked', 'checked');
        $('#schedule-type input[type=\"radio\"][value=\"0\"]').prop('checked',true);
        
        $('#setting-daily-repeat').val(1);
        $('#setting-weekly-repeat').val(1);
        $('#setting-monthly-repeat').val(1);
        ";

    $this->RegisterJs($js);
