<?php
/**
 * @link http://www.yiiframework.com/
 *
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\modules\notification\assets;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 *
 * @since 2.0
 */
class NotificationAsset extends \yii\web\AssetBundle
{
    //public $basePath = '@webroot';
    //public $baseUrl = '@web';
    public $sourcePath = __DIR__;
    public $js = [
//        'js/ajax-form-popup.js'
//        'js/render-cart.js',
//        'js/render-user-form.js',
//        'js/change-status.js',
//        'js/timer.jquery.min.js',
//        'js/order-call-timer.js',
        '/js/notification/push-notification-main.js?v=1501468569'
    ];
    public $depends = [
        'app\assets\KynaAsset',
    ];
}
