<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

use kartik\widgets\Select2;
use kartik\widgets\DatePicker;

use common\widgets\upload\Upload;
use kyna\settings\models\Banner;

/* @var $this yii\web\View */
/* @var $model kyna\settings\models\Banner */
/* @var $form yii\widgets\ActiveForm */

$crudTitles = Yii::$app->params['crudTitles'];
?>

<div class="banner-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'image_url')->widget(Upload::className(), ['display' => 'image']) ?>

    <?= $form->field($model, 'link')->textInput() ?>

    <?= $form->field($model, 'type')->radioList(Banner::getCrossProductTypes()) ?>

    <div class="row">
        <?= $form->field($model, 'from_date', ['options' => ['class' => 'col-md-6']])->widget(DatePicker::className(), [
            'pluginOptions' => [
                'autoclose' => true,
                'format' => 'dd/mm/yyyy',
            ]
        ]) ?>

        <?= $form->field($model, 'to_date', ['options' => ['class' => 'col-md-6']])->widget(DatePicker::className(), [
            'pluginOptions' => [
                'autoclose' => true,
                'format' => 'dd/mm/yyyy',
            ]
        ]) ?>
    </div>

    <div class="row">
        <?= $form->field($model, 'status', ['options' => ['class' => 'col-xs-2']])->widget(Select2::classname(), [
            'data' => Banner::listStatus(),
            'hideSearch' => true,
            'options' => [
                'placeholder' => $crudTitles['prompt'],
            ],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]) ?>
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? $crudTitles['create'] : $crudTitles['update'], ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::a('Hủy', ['index'], ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
