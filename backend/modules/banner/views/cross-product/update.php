<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model kyna\settings\models\Banner */

$this->title = 'Cập nhật cross product banner: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Quản lý cross product banner', 'url' => ['index']];
$this->params['breadcrumbs'][] = $model->title;
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="banner-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
