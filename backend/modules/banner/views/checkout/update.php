<?php

/* @var $this yii\web\View */
/* @var $model kyna\settings\models\Banner */

$this->title = 'Cập nhật checkout banner: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Quản lý checkout banners', 'url' => ['index', 'type' => $model->type]];
$this->params['breadcrumbs'][] = $model->title;
$this->params['breadcrumbs'][] = 'Cập nhật';
?>
<div class="banner-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
