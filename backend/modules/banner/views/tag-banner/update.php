<?php

/* @var $this yii\web\View */
/* @var $model kyna\settings\models\Banner */

$this->title = 'Cập nhật app home banner: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Quản lý tag banners', 'url' => ['index', 'type' => $model->type]];
$this->params['breadcrumbs'][] = $model->title;
$this->params['breadcrumbs'][] = 'Cập nhật';
?>
<div class="banner-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
