<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

use common\widgets\upload\Upload;

use kyna\settings\models\Banner;
use kyna\course\models\Category;
use kartik\widgets\DatePicker;
use kartik\widgets\Select2;
use yii\web\JsExpression;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model kyna\settings\models\Banner */
/* @var $form yii\widgets\ActiveForm */

$crudTitles = Yii::$app->params['crudTitles'];
$selectedTagIds = ArrayHelper::map($model->tags, 'id', 'tag');
$model->listTagIds = array_keys($selectedTagIds);
//var_dump($selectedTagIds); exit;
?>

<div class="banner-form">

    <?php $form = ActiveForm::begin([
        'options' => ['enctype' => 'multipart/form-data'],
    ]); ?>
    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'link')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'type')->hiddenInput(['value' => Banner::TYPE_TAG_PAGE])->label(false) ?>

    <?= $form->field($model, 'image_url')->widget(Upload::className(), ['display' => 'image']) ?>

    <?php echo $form->field($model, 'listTagIds')->widget(Select2::classname(), [
        'initValueText' => $selectedTagIds,
        'options' => [
            'placeholder' => $crudTitles['prompt'],
            'multiple' => true
        ],
        'theme' => 'default',
        'pluginOptions' => [
            'allowClear' => true,
            'minimumInputLength' => 3,
            'language' => [
                'errorLoading' => new JsExpression("function () { return 'Chờ nhận kết quả...'; }"),
            ],
            'ajax' => [
                'url' => Url::toRoute(['/tag/api/tag-list', 'auto' => true]),
                'dataType' => 'json',
                'data' => new JsExpression('function(params) { return {q:params.term}; }'),
            ],

        ],
    ])->label('Tags');
    ?>

    <div class="row">
        <?= $form->field($model, 'from_date_time', ['options' => ['class' => 'col-md-6']])->widget(\kartik\widgets\DateTimePicker::className(), [
            'pluginOptions' => [
                'autoclose' => true,
                'format' => 'dd/mm/yyyy hh:ii',
                'todayHighlight' => true,
                'startDate' => date('d/m/Y 00:00')
            ]
        ]) ?>

        <?= $form->field($model, 'to_date_time', ['options' => ['class' => 'col-md-6']])->widget(\kartik\widgets\DateTimePicker::className(), [
            'pluginOptions' => [
                'autoclose' => true,
                'format' => 'dd/mm/yyyy hh:ii',
                'todayHighlight' => true,
                'startDate' => date('d/m/Y 00:00')
            ]
        ]) ?>
    </div>

    <div class="row">
        <?= $form->field($model, 'status', ['options' => ['class' => 'col-xs-2']])->widget(Select2::classname(), [
            'data' => Banner::listStatus(),
            'hideSearch' => true,
            'options' => [
                'placeholder' => $crudTitles['prompt'],
            ],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]) ?>
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? $crudTitles['create'] : $crudTitles['update'], ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::a('Hủy', ['index'], ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
