<?php

use yii\helpers\Html;
use yii\grid\GridView;
use kartik\widgets\Select2;
use kyna\settings\models\Banner;
use common\helpers\CDNHelper;
use common\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel kyna\settings\models\search\BannerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Quản lý tag banners';
$this->params['breadcrumbs'][] = $this->title;

$crudTitles = Yii::$app->params['crudTitles'];
$crudButtonIcons = Yii::$app->params['crudButtonIcons'];

$webUser = Yii::$app->user;
?>
<div class="banner-index">
    <?php if ($webUser->can('Banner.Create')) : ?>
    <p>
        <?= Html::a($crudButtonIcons['create'] . ' ' . $crudTitles['create'], ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?php endif; ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'title',
            [
                'attribute' => 'image_url',
                'format' => 'html',
                'value' => function ($model) {
                    return CDNHelper::image($model->image_url, [
                        'size' => CDNHelper::IMG_SIZE_ORIGINAL,
                        'alt' => $model->image_url,
                        'width' => '100px'
                    ]);
                },
                'filter' => false,
                'options' => ['class' => 'col-xs-2']
            ],
            'from_date_time:datetime',
            'to_date_time:datetime',
            [
                'label' => 'Tags',
                'value' => function (Banner $model){
                    $tags = $model->tags;
                    if(!empty($tags)) {
                        $tags = ArrayHelper::getColumn($model->tags, 'slug');
                        return implode(", ", $tags);
                    }
                    return null;
                },
            ],
            [
                'attribute' => 'status',
                'value' => function ($model) {
                    return $model->statusButton;
                },
                'format' => 'raw',
                'filter' => Select2::widget([
                    'model' => $searchModel,
                    'attribute' => 'status',
                    'data' => Banner::listStatus(false),
                    'options' => ['placeholder' => $crudTitles['prompt']],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                    'hideSearch' => true,
                ])
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update} {delete}',
                'visibleButtons' => [
                    'update' => function () use ($webUser) {
                        return $webUser->can('Banner.Update');
                    },
                    'delete' => function () use ($webUser) {
                        return $webUser->can('Banner.Delete');
                    }
                ],
            ],
        ],
    ]); ?>
</div>
