<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\web\JsExpression;

use common\widgets\upload\Upload;

use kartik\widgets\Select2;
use kyna\settings\models\Banner;
use kartik\widgets\DatePicker;

/* @var $this yii\web\View */
/* @var $model kyna\settings\models\Banner */
/* @var $form yii\widgets\ActiveForm */

$crudTitles = Yii::$app->params['crudTitles'];

$types = [
    Banner::TYPE_POPUP => 'Home',
    Banner::TYPE_POPUP_LEARNING => 'Bài học',
    Banner::TYPE_POPUP_MY_COURSE => 'Khóa học của tôi'
];

$selectedCourseIds = $model->getBannerCourses()
    ->joinWith('course')
    ->select(['name', 'course_id'])->indexBy('course_id')->column();

$model->listCourseIds = array_keys($selectedCourseIds);
?>

<div class="banner-form">

    <?php $form = ActiveForm::begin([
        'options' => ['enctype' => 'multipart/form-data'],
        'enableAjaxValidation' => true
    ]); ?>

    <?= $form->field($model, 'image_url')->widget(Upload::className(), ['display' => 'image']) ?>

    <?= $form->field($model, 'mobile_image_url')->widget(Upload::className(), ['display' => 'image']) ?>

    <?= $form->field($model, 'link')->textInput() ?>

    <?= $form->field($model, 'title')->textInput() ?>

    <?= $form->field($model, 'type')->radioList($types, ['id' => 'radio-type']) ?>

    <div id="list-course" class="<?= (($model->type == Banner::TYPE_POPUP_LEARNING) || ($model->type == Banner::TYPE_POPUP_MY_COURSE))  ? 'open' : 'hide' ?>">
        <?php echo $form->field($model, 'listCourseIds')->widget(Select2::classname(), [
            'initValueText' => $selectedCourseIds,
            'options' => [
                'placeholder' => $crudTitles['prompt'],
                'multiple' => true
            ],
            'theme' => 'default',
            'pluginOptions' => [
                'allowClear' => true,
                'minimumInputLength' => 3,
                'language' => [
                    'errorLoading' => new JsExpression("function () { return 'Chờ nhận kết quả...'; }"),
                ],
                'ajax' => [
                    'url' => Url::toRoute(['/course/api/search', 'auto' => true]),
                    'dataType' => 'json',
                    'data' => new JsExpression('function(params) { return {q:params.term}; }'),
                ],
                'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                'templateResult' => new JsExpression('function(course) { return course.text; }'),
                'templateSelection' => new JsExpression('function (course) { if (course.price !== undefined) {$("#old-price").val(course.price); } return course.text; }'),
            ],
        ])->label('Khóa học');
        ?>
    </div>


    <div class="row">
        <?= $form->field($model, 'from_date', ['options' => ['class' => 'col-md-6']])->widget(DatePicker::className(), [
            'pluginOptions' => [
                'autoclose' => true,
                'format' => 'dd/mm/yyyy',
            ]
        ]) ?>

        <?= $form->field($model, 'to_date', ['options' => ['class' => 'col-md-6']])->widget(DatePicker::className(), [
            'pluginOptions' => [
                'autoclose' => true,
                'format' => 'dd/mm/yyyy',
            ]
        ]) ?>
    </div>

    <div class="row">
        <?= $form->field($model, 'status', ['options' => ['class' => 'col-xs-2']])->widget(Select2::classname(), [
            'data' => Banner::listStatus(),
            'hideSearch' => true,
            'options' => [
                'placeholder' => $crudTitles['prompt'],
            ],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]) ?>
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? $crudTitles['create'] : $crudTitles['update'], ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::a('Hủy', ['index'], ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<?php
$script = "
    $(document).ready(function () {
        $('body').on('change', 'input[name=\'Banner[type]\']', function () {
            var selectedType = $('input[name=\'Banner[type]\']:checked').val();
            
            switch (selectedType) {
                case '" . Banner::TYPE_POPUP . "':
                    $('#list-course').removeClass('open');
                    $('#list-course').addClass('hide');
                    break;
                    
                case '" . Banner::TYPE_POPUP_LEARNING . "':
                    $('#list-course').removeClass('hide');
                    $('#list-course').addClass('open');
                    break;
                    
                case '" . Banner::TYPE_POPUP_MY_COURSE . "':
                    $('#list-course').removeClass('hide');
                    $('#list-course').addClass('open');
                    break;
                    
                default:
                    $('#list-course').addClass('hide');
                    break;
            }
        });
    });
";

$this->registerJs($script, \yii\web\View::POS_END);
?>