<?php

/* @var $this yii\web\View */
/* @var $model kyna\settings\models\Banner */

$this->title = 'Thêm popup';
$this->params['breadcrumbs'][] = ['label' => 'Quản lý popups', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="banner-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
