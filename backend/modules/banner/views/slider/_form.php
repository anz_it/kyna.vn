<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

use kartik\widgets\DatePicker;
use kartik\widgets\Select2;

use kyna\settings\models\Banner;
use kyna\settings\models\BannerGroup;

/* @var $this yii\web\View */
/* @var $model kyna\settings\models\BannerGroup */
/* @var $form yii\widgets\ActiveForm */

$crudTitles = Yii::$app->params['crudTitles'];

$banners = Banner::find()->select(['id', 'concat(link, " ", title) as title'])->where(['type' => Banner::TYPE_HOME_SLIDER])->all();
$bannersData = ArrayHelper::map($banners, 'id', 'title');
?>

<div class="banner-group-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <div class="row">
        <?= $form->field($model, 'from_date', ['options' => ['class' => 'col-md-6']])->widget(DatePicker::className(), [
            'pluginOptions' => [
                'autoclose' => true,
                'format' => 'dd/mm/yyyy',
            ]
        ]) ?>

        <?= $form->field($model, 'to_date', ['options' => ['class' => 'col-md-6']])->widget(DatePicker::className(), [
            'pluginOptions' => [
                'autoclose' => true,
                'format' => 'dd/mm/yyyy',
            ]
        ]) ?>
    </div>

    <div class="row">
        <?= $form->field($model, 'status', ['options' => ['class' => 'col-xs-2']])->widget(Select2::classname(), [
            'data' => BannerGroup::listStatus(),
            'hideSearch' => true,
            'options' => [
                'placeholder' => $crudTitles['prompt'],
            ],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]) ?>
    </div>

    <div class="row">
        <div class="col-md-6">
            <?php
            foreach ($mainBanners as $key => $mainBanner) {
                echo $form->field($mainBanner, "[{$mainBanner->position}][{$key}]banner_id")->widget(Select2::className(), [
                    'data' => $bannersData,
                    'hideSearch' => true,
                    'options' => [
                        'placeholder' => $crudTitles['prompt'],
                    ],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ])->label('Banner lớn');
            }
            ?>
        </div>

        <div class="col-md-6">
            <?php
            foreach ($subBanners as $key => $subBanner) {
                echo $form->field($subBanner, "[{$subBanner->position}][{$key}]banner_id")->widget(Select2::className(), [
                    'data' => $bannersData,
                    'hideSearch' => true,
                    'options' => [
                        'placeholder' => $crudTitles['prompt'],
                    ],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ])->label('Banner nhỏ');
            }
            ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
