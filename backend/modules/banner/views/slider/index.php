<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use kartik\widgets\Select2;
use kyna\settings\models\BannerGroup;

/* @var $this yii\web\View */
/* @var $searchModel kyna\settings\models\search\BannerGroupSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Banner Groups';
$this->params['breadcrumbs'][] = $this->title;

$crudTitles = Yii::$app->params['crudTitles'];
$crudButtonIcons = Yii::$app->params['crudButtonIcons'];

$webUser = Yii::$app->user;
?>
<div class="banner-group-index">
    <?php if ($webUser->can('Banner.Create')) : ?>
    <p>
        <?= Html::a($crudButtonIcons['create'] . ' ' . $crudTitles['create'], ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?php endif; ?>

    <?php Pjax::begin(); ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                'id',
                'title',
                [
                    'attribute' => 'type',
                    'value' => function ($model) {
                        return $model->typeText;
                    },
                    'filter' => Select2::widget([
                        'model' => $searchModel,
                        'attribute' => 'type',
                        'data' => BannerGroup::getTypes(),
                        'options' => ['placeholder' => $crudTitles['prompt']],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                        'hideSearch' => true,
                    ])
                ],
                'from_date:date',
                'to_date:date',
                [
                    'attribute' => 'status',
                    'value' => function ($model) {
                        return $model->statusButton;
                    },
                    'format' => 'raw',
                    'filter' => Select2::widget([
                        'model' => $searchModel,
                        'attribute' => 'status',
                        'data' => BannerGroup::listStatus(false),
                        'options' => ['placeholder' => $crudTitles['prompt']],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                        'hideSearch' => true,
                    ])
                ],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{update} {delete}',
                    'visibleButtons' => [
                        'update' => function () use ($webUser) {
                            return $webUser->can('Banner.Update');
                        },
                        'delete' => function () use ($webUser) {
                            return $webUser->can('Banner.Delete');
                        }
                    ],
                ],
            ],
        ]); ?>
    <?php Pjax::end(); ?>
</div>
