<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model kyna\settings\models\BannerGroup */

$this->title = 'Update Banner Group: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Banner Groups', 'url' => ['index']];
$this->params['breadcrumbs'][] = $model->title;
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="banner-group-update">
    <?= $this->render('_form', [
        'model' => $model,
        'mainBanners' => $mainBanners,
        'subBanners' => $subBanners,
    ]) ?>

</div>
