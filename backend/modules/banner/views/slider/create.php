<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model kyna\settings\models\BannerGroup */

$this->title = 'Create Banner Group';
$this->params['breadcrumbs'][] = ['label' => 'Banner Groups', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="banner-group-create">

    <?= $this->render('_form', [
        'model' => $model,
        'mainBanners' => $mainBanners,
        'subBanners' => $subBanners,
    ]) ?>

</div>
