<?php

/* @var $this yii\web\View */
/* @var $model kyna\settings\models\Banner */

$this->title = 'Cập nhật top banner: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Quản lý top banner', 'url' => ['index', 'type' => $model->type]];
$this->params['breadcrumbs'][] = $model->title;
$this->params['breadcrumbs'][] = 'Cập nhật';
?>
<div class="banner-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
