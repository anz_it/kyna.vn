<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

use common\widgets\upload\Upload;

use kyna\settings\models\Banner;
use kyna\course\models\Category;
use kartik\widgets\DatePicker;
use kartik\widgets\Select2;

/* @var $this yii\web\View */
/* @var $model kyna\settings\models\Banner */
/* @var $form yii\widgets\ActiveForm */

$crudTitles = Yii::$app->params['crudTitles'];
$courseCategoriesData = ArrayHelper::map(Category::find()->all(), 'id', 'name');
?>

<div class="banner-form">

    <?php $form = ActiveForm::begin([
        'options' => ['enctype' => 'multipart/form-data'],
    ]); ?>
    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'type')->hiddenInput(['value' => Banner::TYPE_APP_HOME_SLIDER])->label(false) ?>

    <?= $form->field($model, 'image_url')->widget(Upload::className(), ['display' => 'image']) ?>



    <?= $form->field($model, 'app_action')->widget(Select2::classname(), [
        'data' => Banner::listAppActions(),
        'hideSearch' => true,
        'options' => ['placeholder' => $crudTitles['prompt']],
        'pluginOptions' => [
            'allowClear' => true
        ],
        'pluginEvents' => [
            'change' => 'function() { 
                            if(this.value == ' . Banner::APP_ACTION_SEARCH . ' ){                            
                                $(\'.software-group\').show();
                                $(\'.partner-group\').show();
                            }else {
                                $(\'.software-group\').hide();
                                $(\'.partner-group\').hide();
                            }                        
                        }',
        ]

    ]) ?>

    <?= $form->field($model, 'app_action_data')->textInput() ?>

    <div class="row">
        <?= $form->field($model, 'from_date', ['options' => ['class' => 'col-md-6']])->widget(DatePicker::className(), [
            'pluginOptions' => [
                'autoclose' => true,
                'format' => 'dd/mm/yyyy',
            ]
        ]) ?>

        <?= $form->field($model, 'to_date', ['options' => ['class' => 'col-md-6']])->widget(DatePicker::className(), [
            'pluginOptions' => [
                'autoclose' => true,
                'format' => 'dd/mm/yyyy',
            ]
        ]) ?>
    </div>

    <div class="row">
        <?= $form->field($model, 'status', ['options' => ['class' => 'col-xs-2']])->widget(Select2::classname(), [
            'data' => Banner::listStatus(),
            'hideSearch' => true,
            'options' => [
                'placeholder' => $crudTitles['prompt'],
            ],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]) ?>
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? $crudTitles['create'] : $crudTitles['update'], ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::a('Hủy', ['index'], ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
