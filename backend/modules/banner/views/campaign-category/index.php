<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;

use kartik\widgets\Select2;
use kyna\settings\models\Banner;
use kyna\course\models\Category;
use common\helpers\CDNHelper;

/* @var $this yii\web\View */
/* @var $searchModel kyna\settings\models\search\BannerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Quản lý campaign category banners';
$this->params['breadcrumbs'][] = $this->title;

$crudTitles = Yii::$app->params['crudTitles'];
$crudButtonIcons = Yii::$app->params['crudButtonIcons'];

$courseCategoriesData = ArrayHelper::map(Category::find()->all(), 'id', 'name');

$webUser = Yii::$app->user;
?>
<div class="banner-index">
    <?php if ($webUser->can('Banner.Create')) : ?>
    <p>
        <?= Html::a($crudButtonIcons['create'] . ' ' . $crudTitles['create'], ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?php endif; ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'image_url',
                'format' => 'html',
                'value' => function ($model) {
                    return CDNHelper::image($model->image_url, [
                        'size' => CDNHelper::IMG_SIZE_ORIGINAL,
                        'alt' => $model->title,
                        'width' => '100px'
                    ]);
                },
                'filter' => false,
                'options' => ['class' => 'col-xs-2']
            ],
            'link:url',
            'title',
            [
                'attribute' => 'category_id',
                'value' => function ($model) {
                    return $model->category->name;
                },
                'format' => 'raw',
                'filter' => Select2::widget([
                    'model' => $searchModel,
                    'attribute' => 'category_id',
                    'data' => $courseCategoriesData,
                    'options' => ['placeholder' => $crudTitles['prompt']],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ])
            ],
            [
                'attribute' => 'type',
                'value' => function ($model) {
                    return $model->typeText;
                },
                'filter' => Select2::widget([
                    'model' => $searchModel,
                    'attribute' => 'type',
                    'data' => Banner::getCampaignCategoryTypes(),
                    'options' => ['placeholder' => $crudTitles['prompt']],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                    'hideSearch' => true,
                ])
            ],
            'from_date:date',
            'to_date:date',
            [
                'attribute' => 'status',
                'value' => function ($model) {
                    return $model->statusButton;
                },
                'format' => 'raw',
                'filter' => Select2::widget([
                    'model' => $searchModel,
                    'attribute' => 'status',
                    'data' => Banner::listStatus(false),
                    'options' => ['placeholder' => $crudTitles['prompt']],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                    'hideSearch' => true,
                ])
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update} {delete}',
                'visibleButtons' => [
                    'update' => function () use ($webUser) {
                        return $webUser->can('Banner.Update');
                    },
                    'delete' => function () use ($webUser) {
                        return $webUser->can('Banner.Delete');
                    }
                ],
            ],
        ],
    ]); ?>
</div>
