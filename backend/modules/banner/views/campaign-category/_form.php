<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

use common\widgets\upload\Upload;

use kartik\widgets\Select2;
use kartik\widgets\DatePicker;

use kyna\settings\models\Banner;
use kyna\course\models\Category;

/* @var $this yii\web\View */
/* @var $model kyna\settings\models\Banner */
/* @var $form yii\widgets\ActiveForm */

$crudTitles = Yii::$app->params['crudTitles'];
$courseCategoriesData = ArrayHelper::map(Category::find()->all(), 'id', 'name');

$types = [
    Banner::TYPE_CAMPAIGN_CATEGORY_LARGE => 'Large',
    Banner::TYPE_CAMPAIGN_CATEGORY_SMALL => 'Small'
];
?>

<div class="banner-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data', 'id' => 'banner-form']]); ?>

    <?= $form->field($model, 'image_url')->widget(Upload::className(), ['display' => 'image']) ?>

    <?= $form->field($model, 'link')->textInput() ?>

    <?= $form->field($model, 'title')->textInput(['id' => 'title', 'readonly' => $model->type == Banner::TYPE_CAMPAIGN_CATEGORY_LARGE]) ?>

    <?= $form->field($model, 'type')->radioList($types, ['id' => 'radio-type']) ?>

    <?= $form->field($model, 'category_id')->widget(Select2::className(), [
        'data' => $courseCategoriesData,
        'options' => [
            'placeholder' => $crudTitles['prompt'],
        ],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]) ?>

    <div class="row">
        <?= $form->field($model, 'from_date', ['options' => ['class' => 'col-md-6']])->widget(DatePicker::className(), [
            'pluginOptions' => [
                'autoclose' => true,
                'format' => 'dd/mm/yyyy',
            ]
        ]) ?>

        <?= $form->field($model, 'to_date', ['options' => ['class' => 'col-md-6']])->widget(DatePicker::className(), [
            'pluginOptions' => [
                'autoclose' => true,
                'format' => 'dd/mm/yyyy',
            ]
        ]) ?>
    </div>

    <?= $form->field($model, 'description')->textarea(['id' => 'description', 'readonly' => $model->type == Banner::TYPE_CAMPAIGN_CATEGORY_LARGE]) ?>

    <div class="row">
        <?= $form->field($model, 'status', ['options' => ['class' => 'col-xs-2']])->widget(Select2::classname(), [
            'data' => Banner::listStatus(),
            'hideSearch' => true,
            'options' => [
                'placeholder' => $crudTitles['prompt'],
            ],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]) ?>
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? $crudTitles['create'] : $crudTitles['update'], ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::a('Hủy', ['index'], ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>
<?php
$campaignMain = Banner::TYPE_CAMPAIGN_CATEGORY_LARGE;
$campaignSub = Banner::TYPE_CAMPAIGN_CATEGORY_SMALL;

$script = <<<JS
    $(document).ready(function() {
        $('body').on('change', '#radio-type', function () {
            var type = $('input[type=\'radio\']:checked', '#banner-form').val();
            if (type == $campaignMain) {
                $('#title').attr('readonly', 'readonly');
                $('#description').attr('readonly', 'readonly');
                $('#title').val('');
                $('#description').val('');
            } else if (type == $campaignSub) {
                $('#title').removeAttr('readonly');
                $('#description').removeAttr('readonly');
            }
        });
    });
JS;

$this->registerJs($script, \yii\web\View::POS_END);
?>
