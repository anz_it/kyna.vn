<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model kyna\settings\models\Banner */

$this->title = 'Update campagin category banner: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Quản lý campaign category banner', 'url' => ['index']];
$this->params['breadcrumbs'][] = $model->title;
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="banner-update">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
