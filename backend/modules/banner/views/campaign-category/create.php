<?php

/* @var $this yii\web\View */
/* @var $model kyna\settings\models\Banner */

$this->title = 'Thêm campaign category banner';
$this->params['breadcrumbs'][] = ['label' => 'Quản lý campaign category banner', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="banner-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
