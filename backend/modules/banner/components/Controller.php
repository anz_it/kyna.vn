<?php
/**
 * Created by PhpStorm.
 * User: macintosh
 * Date: 5/11/17
 * Time: 3:12 PM
 */

namespace app\modules\banner\components;

use Yii;
use yii\web\UploadedFile;
use yii\web\Response;
use yii\widgets\ActiveForm;
use yii\web\NotFoundHttpException;

use common\helpers\DateTimeHelper;
use common\lib\CDNImage;
use kyna\settings\models\Banner;

class Controller extends \app\components\controllers\Controller
{

    public $scenario;

    /**
     * Creates a new Banner model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = $this->findModel();

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->from_date = DateTimeHelper::convertToSystemDate($model->from_date);
            $model->to_date = DateTimeHelper::convertToSystemDate($model->to_date);

            if ($model->save(false)) {
                $saveAgain = false;
                if ($imageUrl = $this->_uploadImage($model, 'image_url')) {
                    $model->image_url = $imageUrl;
                    $saveAgain = true;
                }
                if ($mobileImageUrl = $this->_uploadImage($model, 'mobile_image_url')) {
                    $model->mobile_image_url = $mobileImageUrl;
                    $saveAgain = true;
                }
                if ($saveAgain) {
                    $model->save(false);
                }

                Yii::$app->session->setFlash('success', 'Thêm thành công.');
                return $this->redirect(['index']);
            }
        }

        $model->from_date = DateTimeHelper::convertToHumanDate($model->from_date);
        $model->to_date = DateTimeHelper::convertToHumanDate($model->to_date);

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    protected function _uploadImage($model, $attribute)
    {
        $files = UploadedFile::getInstances($model, $attribute);
        if (!sizeof($files)) {
            $oldAttributes = $model->oldAttributes;

            return isset($oldAttributes[$attribute]) ? $oldAttributes[$attribute] : false;
        }

        $file = $files[0];

        $cdnImage = new CDNImage($model, $attribute);
        $result = $cdnImage->upload($file);

        return $result;
    }

    /**
     * Updates an existing Banner model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->from_date = DateTimeHelper::convertToSystemDate($model->from_date);
            $model->to_date = DateTimeHelper::convertToSystemDate($model->to_date);

            if ($imageUrl = $this->_uploadImage($model, 'image_url')) {
                $model->image_url = $imageUrl;
            }
            if ($mobileImageUrl = $this->_uploadImage($model, 'mobile_image_url')) {
                $model->mobile_image_url = $mobileImageUrl;
            }
            if ($model->save(false)) {
                Yii::$app->session->setFlash('success', 'Cập nhật thành công.');
                return $this->redirect(['index']);
            }
        }

        $model->from_date = DateTimeHelper::convertToHumanDate($model->from_date);
        $model->to_date = DateTimeHelper::convertToHumanDate($model->to_date);

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Banner model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Banner model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Banner the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id = null)
    {
        if ($id == null) {
            $model = new Banner();
        } else {
            $model = Banner::findOne($id);
        }
        if ($model !== null) {
            if ($this->scenario !== null) {
                $model->setScenario($this->scenario);
            }
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}