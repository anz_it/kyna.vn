<?php

namespace app\modules\banner\controllers;

use Yii;

use kyna\settings\models\search\BannerSearch;

/**
 * CrossProductController implements the CRUD actions for Banner model.
 */
class CrossProductController extends \app\modules\banner\components\Controller
{

//    public $scenario = 'cross-product';

    /**
     * Lists all Banner models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new BannerSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

}
