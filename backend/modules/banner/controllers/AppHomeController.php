<?php
/**
 * Created by PhpStorm.
 * User: macintosh
 * Date: 10/6/17
 * Time: 11:17 AM
 */

namespace app\modules\banner\controllers;

use kyna\settings\models\search\BannerSearch;
use app\modules\banner\components\Controller;
use Yii;

class AppHomeController extends Controller
{

    public $scenario = 'app-home';

    /**
     * Lists all Banner models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new BannerSearch();
        $searchModel->type = BannerSearch::TYPE_APP_HOME_SLIDER;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
}