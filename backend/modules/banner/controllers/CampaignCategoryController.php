<?php

namespace app\modules\banner\controllers;

use Yii;
use kyna\settings\models\search\BannerSearch;

/**
 * CampaignCategoryController implements the CRUD actions for Banner model.
 */
class CampaignCategoryController extends \app\modules\banner\components\Controller
{

    public $scenario = 'campaign-category';

    /**
     * Lists all Banner models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new BannerSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, [
            BannerSearch::TYPE_CAMPAIGN_CATEGORY_LARGE,
            BannerSearch::TYPE_CAMPAIGN_CATEGORY_SMALL,
        ]);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

}
