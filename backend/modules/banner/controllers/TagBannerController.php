<?php
/**
 * Created by PhpStorm.
 * User: macintosh
 * Date: 10/6/17
 * Time: 11:17 AM
 */

namespace app\modules\banner\controllers;

use kyna\base\ActiveForm;
use kyna\settings\models\search\BannerSearch;
use app\modules\banner\components\Controller;
use Yii;
use yii\web\Response;

class TagBannerController extends Controller
{

    public $scenario = 'tag-banner';

    /**
     * Lists all Banner models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new BannerSearch();
        $searchModel->type = BannerSearch::TYPE_TAG_PAGE;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    public function actionCreate()
    {
        $model = $this->findModel();

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {

            if ($model->save(false)) {
                $saveAgain = false;
                if ($imageUrl = $this->_uploadImage($model, 'image_url')) {
                    $model->image_url = $imageUrl;
                    $saveAgain = true;
                }
                if ($mobileImageUrl = $this->_uploadImage($model, 'mobile_image_url')) {
                    $model->mobile_image_url = $mobileImageUrl;
                    $saveAgain = true;
                }
                if ($saveAgain) {
                    $model->save(false);
                }

                Yii::$app->session->setFlash('success', 'Thêm thành công.');
                return $this->redirect(['index']);
            }
        }


        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model->from_date_time = date('d/m/Y H:i', $model->from_date_time);
        $model->to_date_time = date('d/m/Y H:i', $model->to_date_time);
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {

            if ($imageUrl = $this->_uploadImage($model, 'image_url')) {
                $model->image_url = $imageUrl;
            }
            if ($mobileImageUrl = $this->_uploadImage($model, 'mobile_image_url')) {
                $model->mobile_image_url = $mobileImageUrl;
            }
            if ($model->save(false)) {
                Yii::$app->session->setFlash('success', 'Cập nhật thành công.');
                return $this->redirect(['index']);
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

}