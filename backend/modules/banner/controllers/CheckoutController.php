<?php
/**
 * Created by PhpStorm.
 * User: macintosh
 * Date: 8/8/17
 * Time: 10:50 AM
 */

namespace app\modules\banner\controllers;

use Yii;
use kyna\settings\models\search\BannerSearch;

class CheckoutController extends \app\modules\banner\components\Controller
{

    public $scenario = 'checkout-banner';

    /**
     * Lists all Banner models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new BannerSearch();
        $searchModel->type = BannerSearch::TYPE_CHECKOUT_PAGE;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
}