<?php

namespace app\modules\banner\controllers;

use common\helpers\DateTimeHelper;
use kyna\base\ActiveForm;
use kyna\settings\models\BannerCourse;
use Yii;
use kyna\settings\models\Banner;
use kyna\settings\models\search\BannerSearch;
use yii\web\Response;

/**
 * BannerController implements the CRUD actions for Banner model.
 */
class TopController extends \app\modules\banner\components\Controller
{

    /**
     * Lists all Banner models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new BannerSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, [
            Banner::TYPE_TOP,
            Banner::TYPE_TOP_SUB,
            Banner::TYPE_TOP_MY_COURSE,
            Banner::TYPE_TOP_MY_COURSE_BOTTOM
        ]);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCreate()
    {
        $model = $this->findModel();

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->from_date = DateTimeHelper::convertToSystemDate($model->from_date);
            $model->to_date = DateTimeHelper::convertToSystemDate($model->to_date);

            if ($model->save(false)) {
                $saveAgain = false;
                if ($imageUrl = $this->_uploadImage($model, 'image_url')) {
                    $model->image_url = $imageUrl;
                    $saveAgain = true;
                }
                if ($mobileImageUrl = $this->_uploadImage($model, 'mobile_image_url')) {
                    $model->mobile_image_url = $mobileImageUrl;
                    $saveAgain = true;
                }
                if ($saveAgain) {
                    $model->save(false);
                }
                switch ($model->type) {
                    case Banner::TYPE_TOP_MY_COURSE_BOTTOM:
                        $this->saveCourses($model->id, $model->listCourseIds);
                        break;

                    default:
                        BannerCourse::deleteAll(['banner_id' => $model->id]);
                        break;
                }
                Yii::$app->session->setFlash('success', 'Thêm thành công.');
                return $this->redirect(['index']);
            }
        }

        $model->from_date = DateTimeHelper::convertToHumanDate($model->from_date);
        $model->to_date = DateTimeHelper::convertToHumanDate($model->to_date);

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->from_date = DateTimeHelper::convertToSystemDate($model->from_date);
            $model->to_date = DateTimeHelper::convertToSystemDate($model->to_date);

            if ($imageUrl = $this->_uploadImage($model, 'image_url')) {
                $model->image_url = $imageUrl;
            }
            if ($mobileImageUrl = $this->_uploadImage($model, 'mobile_image_url')) {
                $model->mobile_image_url = $mobileImageUrl;
            }
            if ($model->save(false)) {
                switch ($model->type) {
                    case Banner::TYPE_TOP_MY_COURSE_BOTTOM:
                        $this->saveCourses($model->id, $model->listCourseIds);
                        break;

                    default:
                        BannerCourse::deleteAll(['banner_id' => $model->id]);
                        break;
                }
                Yii::$app->session->setFlash('success', 'Cập nhật thành công.');
                return $this->redirect(['index']);
            }
        }

        $model->from_date = DateTimeHelper::convertToHumanDate($model->from_date);
        $model->to_date = DateTimeHelper::convertToHumanDate($model->to_date);

        return $this->render('update', [
            'model' => $model,
        ]);
    }


    protected function saveCourses($banner_id, $list_course_ids)
    {
        foreach ($list_course_ids as $key => $courseId) {
            $isExist = BannerCourse::find()->where([
                'banner_id' => $banner_id,
                'course_id' => $courseId
            ])->exists();

            if ($isExist) {
                continue;
            }

            $missionCat = new BannerCourse();
            $missionCat->banner_id = $banner_id;
            $missionCat->course_id = $courseId;
            $missionCat->save();
        }

        BannerCourse::deleteAll(['and',
            ['banner_id' => $banner_id],
            ['not in', 'course_id', $list_course_ids]
        ]);
    }


}
