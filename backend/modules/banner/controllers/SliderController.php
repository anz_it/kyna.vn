<?php

namespace app\modules\banner\controllers;

use Yii;
use yii\web\NotFoundHttpException;

use kyna\settings\models\BannerGroup;
use kyna\settings\models\BannerGroupItem;
use kyna\settings\models\search\BannerGroupSearch;

use common\helpers\DateTimeHelper;
use app\components\controllers\Controller;

/**
 * SliderController implements the CRUD actions for BannerGroup model.
 */
class SliderController extends Controller
{

    /**
     * Lists all BannerGroup models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new BannerGroupSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new BannerGroup model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new BannerGroup();
        $model->type = BannerGroup::TYPE_HOME_SLIDER;

        $mainBanners = $model->getItemsByPositionQuery(BannerGroup::POSITION_MAIN)->limit(2)->all();
        if (empty($mainBanners)) {
            for ($i = 1; $i <= 2; $i ++) {
                $mainBanner = new BannerGroupItem();
                $mainBanner->position = BannerGroup::POSITION_MAIN;

                $mainBanners[] = $mainBanner;
            }
        }
        $subBanners = $model->getItemsByPositionQuery(BannerGroup::POSITION_SUB)->limit(3)->all();
        if (empty($subBanners)) {
            for ($i = 1; $i <= 3; $i ++) {
                $subBanner = new BannerGroupItem();
                $subBanner->position = BannerGroup::POSITION_SUB;

                $subBanners[] = $subBanner;
            }
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->from_date = DateTimeHelper::convertToSystemDate($model->from_date);
            $model->to_date = DateTimeHelper::convertToSystemDate($model->to_date);

            if ($model->save(false)) {
                $post = Yii::$app->request->post();
                $postMainBanners = $post['BannerGroupItem']['main'];
                $postSubBanners = $post['BannerGroupItem']['sub'];

                foreach ($mainBanners as $key => $mainBanner) {
                    /**
                     * @var $mainBanner BannerGroupItem
                     */
                    $mainBanner->banner_group_id = $model->id;
                    $mainBanner->banner_id = $postMainBanners[$key]['banner_id'];
                    $mainBanner->save();
                }

                foreach ($subBanners as $key => $subBanner) {
                    /**
                     * @var $mainBanner BannerGroupItem
                     */
                    $subBanner->banner_group_id = $model->id;
                    $subBanner->banner_id = $postSubBanners[$key]['banner_id'];
                    $subBanner->save();
                }

                Yii::$app->session->setFlash('success', 'Thêm thành công');

                return $this->redirect(['index']);
            }
        }

        $model->from_date = DateTimeHelper::convertToHumanDate($model->from_date);
        $model->to_date = DateTimeHelper::convertToHumanDate($model->to_date);

        return $this->render('create', [
            'model' => $model,
            'mainBanners' => $mainBanners,
            'subBanners' => $subBanners,
        ]);
    }

    /**
     * Updates an existing BannerGroup model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $mainBanners = $model->getItemsByPositionQuery(BannerGroup::POSITION_MAIN)->limit(2)->all();
        $subBanners = $model->getItemsByPositionQuery(BannerGroup::POSITION_SUB)->limit(3)->all();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->from_date = DateTimeHelper::convertToSystemDate($model->from_date);
            $model->to_date = DateTimeHelper::convertToSystemDate($model->to_date);

            if ($model->save(false)) {
                $post = Yii::$app->request->post();
                $postMainBanners = $post['BannerGroupItem']['main'];
                $postSubBanners = $post['BannerGroupItem']['sub'];

                foreach ($mainBanners as $key => $mainBanner) {
                    /**
                     * @var $mainBanner BannerGroupItem
                     */
                    $mainBanner->banner_group_id = $model->id;
                    $mainBanner->banner_id = $postMainBanners[$key]['banner_id'];
                    $mainBanner->save();
                }

                foreach ($subBanners as $key => $subBanner) {
                    /**
                     * @var $mainBanner BannerGroupItem
                     */
                    $subBanner->banner_group_id = $model->id;
                    $subBanner->banner_id = $postSubBanners[$key]['banner_id'];
                    $subBanner->save();
                }

                Yii::$app->session->setFlash('success', 'Cập nhật thành công');

                return $this->redirect(['index']);
            }
        }

        $model->from_date = DateTimeHelper::convertToHumanDate($model->from_date);
        $model->to_date = DateTimeHelper::convertToHumanDate($model->to_date);

        return $this->render('update', [
            'model' => $model,
            'mainBanners' => $mainBanners,
            'subBanners' => $subBanners,
        ]);
    }

    /**
     * Deletes an existing BannerGroup model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the BannerGroup model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return BannerGroup the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = BannerGroup::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
