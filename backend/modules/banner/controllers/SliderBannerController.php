<?php
/**
 * Created by PhpStorm.
 * User: macintosh
 * Date: 5/15/17
 * Time: 5:20 PM
 */

namespace app\modules\banner\controllers;

use Yii;
use yii\web\Response;

use app\modules\banner\components\Controller;
use kyna\settings\models\search\BannerSearch;
use kyna\settings\models\BannerGroup;

class SliderBannerController extends Controller
{

    /**
     * Lists all Banner models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new BannerSearch();
        $searchModel->type = BannerSearch::TYPE_HOME_SLIDER;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionChangeStatus($id, $redirectUrl = false, $field = 'status')
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = $this->findModel($id);

        if ($model->{$field} == BannerSearch::STATUS_ACTIVE) {
            $hasActiveBannerGroup = $model->getBannerGroups()->where(['status' => BannerGroup::STATUS_ACTIVE])->exists();
            if ($hasActiveBannerGroup) {
                return [
                    'status' => false,
                    'msg' => 'Không thể ẩn, tồn tại trong banner group'
                ];
            }
            $model->{$field} = BannerSearch::STATUS_DEACTIVE;
        } else {
            if ($model->{$field} == BannerSearch::STATUS_DEACTIVE) {
                $model->{$field} = BannerSearch::STATUS_ACTIVE;
            }
        }
        if ($model->save(false)) {
            return [
                'status' => true,
                'msg' => 'Thay đổi thành công!'
            ];
        }
        else {
            return [
                'status' => true,
                'msg' => 'Thay đổi thất bại!'
            ];
        }
    }

    /**
     * Deletes an existing Banner model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $model = $this->findModel($id);

        $hasActiveBannerGroup = $model->getBannerGroups()->where(['status' => BannerGroup::STATUS_ACTIVE])->exists();
        if ($hasActiveBannerGroup) {
            return [
                'result' => false,
                'message' => 'Không thể xóa, tồn tại trong banner group'
            ];
        }

        $model->delete();

        return [
            'result' => true,
            'message' => 'Đã xóa'
        ];
    }
}