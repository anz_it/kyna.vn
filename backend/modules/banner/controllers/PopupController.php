<?php
/**
 * Created by PhpStorm.
 * User: macintosh
 * Date: 8/9/17
 * Time: 9:34 AM
 */

namespace app\modules\banner\controllers;

use Yii;
use yii\web\Response;
use yii\widgets\ActiveForm;

use kyna\settings\models\Banner;
use kyna\settings\models\BannerCourse;
use kyna\settings\models\search\BannerSearch;
use app\modules\banner\components\Controller;

use common\helpers\DateTimeHelper;

class PopupController extends Controller
{

    public $scenario = 'popup';

    /**
     * Lists all Banner models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new BannerSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, [
            BannerSearch::TYPE_POPUP,
            BannerSearch::TYPE_POPUP_LEARNING,
            BannerSearch::TYPE_POPUP_MY_COURSE,
        ]);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new Banner model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = $this->findModel();
        $model->type = Banner::TYPE_POPUP;

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->from_date = DateTimeHelper::convertToSystemDate($model->from_date);
            $model->to_date = DateTimeHelper::convertToSystemDate($model->to_date);

            if ($model->save(false)) {
                $saveAgain = false;
                if ($imageUrl = $this->_uploadImage($model, 'image_url')) {
                    $model->image_url = $imageUrl;
                    $saveAgain = true;
                }
                if ($mobileImageUrl = $this->_uploadImage($model, 'mobile_image_url')) {
                    $model->mobile_image_url = $mobileImageUrl;
                    $saveAgain = true;
                }
                if ($saveAgain) {
                    $model->save(false);
                }
                switch ($model->type) {
                    case Banner::TYPE_POPUP_LEARNING:
                        $this->saveCourses($model->id, $model->listCourseIds);
                        break;
                    case Banner::TYPE_POPUP_MY_COURSE:
                        $this->saveCourses($model->id, $model->listCourseIds);
                        break;

                    default:
                        BannerCourse::deleteAll(['banner_id' => $model->id]);
                        break;
                }

                Yii::$app->session->setFlash('success', 'Thêm thành công.');
                return $this->redirect(['index']);
            }
        }

        $model->from_date = DateTimeHelper::convertToHumanDate($model->from_date);
        $model->to_date = DateTimeHelper::convertToHumanDate($model->to_date);

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Banner model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->from_date = DateTimeHelper::convertToSystemDate($model->from_date);
            $model->to_date = DateTimeHelper::convertToSystemDate($model->to_date);

            if ($imageUrl = $this->_uploadImage($model, 'image_url')) {
                $model->image_url = $imageUrl;
            }
            if ($mobileImageUrl = $this->_uploadImage($model, 'mobile_image_url')) {
                $model->mobile_image_url = $mobileImageUrl;
            }
            if ($model->save(false)) {
                switch ($model->type) {
                    case Banner::TYPE_POPUP_LEARNING:
                        $this->saveCourses($model->id, $model->listCourseIds);
                        break;
                    case Banner::TYPE_POPUP_MY_COURSE:
                        $this->saveCourses($model->id, $model->listCourseIds);
                        break;
                    default:
                        BannerCourse::deleteAll(['banner_id' => $model->id]);
                        break;
                }

                Yii::$app->session->setFlash('success', 'Cập nhật thành công.');
                return $this->redirect(['index']);
            }
        }

        $model->from_date = DateTimeHelper::convertToHumanDate($model->from_date);
        $model->to_date = DateTimeHelper::convertToHumanDate($model->to_date);

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    protected function saveCourses($banner_id, $list_course_ids)
    {
        if(!empty($list_course_ids)){
            foreach ($list_course_ids as $key => $courseId) {
                $isExist = BannerCourse::find()->where([
                    'banner_id' => $banner_id,
                    'course_id' => $courseId
                ])->exists();

                if ($isExist) {
                    continue;
                }

                $missionCat = new BannerCourse();
                $missionCat->banner_id = $banner_id;
                $missionCat->course_id = $courseId;
                $missionCat->save();
            }

            BannerCourse::deleteAll(['and',
                ['banner_id' => $banner_id],
                ['not in', 'course_id', $list_course_ids]
            ]);
        }else{
            BannerCourse::deleteAll(['and',
                ['banner_id' => $banner_id],
            ]);
        }

    }

}