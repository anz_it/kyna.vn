<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\JsExpression;
use kartik\widgets\Select2;
use kyna\course\models\Course;
use kyna\commission\models\AffiliateCategoryCourse;

/* @var $this yii\web\View */
/* @var $model kyna\commission\models\AffiliateCourse */
/* @var $form yii\widgets\ActiveForm */

$crudTitles = Yii::$app->params['crudTitles'];
?>

<div class="affiliate-course-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php
    echo $form->field($model, 'course_id')->widget(Select2::classname(), [
        'initValueText' =>  empty($model->course_id) ? '' : $model->course->name, // set the initial display text
        'options' => [
            'placeholder' => '-- Chọn khóa học --',
        ],
        'pluginOptions' => [
            'allowClear' => true,
            'minimumInputLength' => 3,
            'language' => [
                'errorLoading' => new JsExpression("function () { return 'Chờ nhận kết quả...'; }"),
            ],
            'ajax' => [
                'url' => Url::toRoute(['/course/api/search', 'auto' => true, 'type' => array_keys(Course::listTypes())]),
                'dataType' => 'json',
                'data' => new JsExpression('function(params) { return {q:params.term}; }'),
            ],
            'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
            'templateResult' => new JsExpression('function(course) { return course.text; }'),
            'templateSelection' => new JsExpression('function (course) { if (course.price !== undefined) {$("#old-price").val(course.price); } return course.text; }'),
        ],
    ]);
    ?>

    <?= $form->field($model, 'affiliate_category_id')->widget(Select2::classname(), [
        'data' => $affCategories,
        'hideSearch' => true,
        'options' => [
            'placeholder' => '--Chọn loại affiliate--',
        ],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]) ?>

    <?= $form->field($model, 'commission_percent')->textInput() ?>

    <?= $form->field($model, 'status')->widget(Select2::classname(), [
        'data' => AffiliateCategoryCourse::listStatus(),
        'hideSearch' => true,
        'options' => [
            'placeholder' => $crudTitles['prompt'],
        ],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::a($crudTitles['cancel'], ['index'], ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
