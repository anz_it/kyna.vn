<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model kyna\commission\models\AffiliateCourse */

$this->title = 'Update Affiliate Course: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Thiết lập loại affiliate và khóa học', 'url' => ['index']];
$this->params['breadcrumbs'][] = $model->id;
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="affiliate-course-update">

    <?= $this->render('_form', [
        'model' => $model,
        'affCategories' => $affCategories,
    ]) ?>

</div>
