<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\web\JsExpression;
use yii\widgets\ActiveForm;
use kartik\widgets\Select2;
use kyna\course\models\Course;
use kyna\commission\models\AffiliateCategoryCourse;
/* @var $this yii\web\View */
/* @var $searchModel kyna\commission\models\search\AffiliateCategoryCourseSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Thiết lập loại affiliate và khóa học';
$this->params['breadcrumbs'][] = $this->title;

$crudTitles = Yii::$app->params['crudTitles'];
$crudButtonIcons = Yii::$app->params['crudButtonIcons'];

$webUser = Yii::$app->user;
$courseTypes = array_keys(Course::listTypes());
?>
<div class="affiliate-course-index">
    <nav class="navbar navbar-default">
        <?php $form = ActiveForm::begin([
            'options' => [
                'enctype' => 'multipart/form-data',
                'class' => 'navbar-form navbar-left'
            ]
        ]) ?>

        <?php
        $initText = $newModel->course != null ? $newModel->course->name : '';

        echo $form->field($newModel, 'course_id', [
            'options' => ['class' => 'form-group', 'style' => 'width: 200px'],
        ])->widget(Select2::classname(), [
            'initValueText' => $initText,
            'options' => ['placeholder' => '-- Chọn khóa học --'],
            'pluginOptions' => [
                'allowClear' => true,
                'minimumInputLength' => 3,
                'language' => [
                    'errorLoading' => new JsExpression("function () { return 'Chờ nhận kết quả...'; }"),
                ],
                'ajax' => [
                    'url' => Url::toRoute(['/course/api/search', 'auto' => true, 'type' => $courseTypes]),
                    'dataType' => 'json',
                ],
            ],
        ])->label(false)->error(false); ?>

        <?php
        $initText = $newModel->affiliateCategory != null ? $newModel->affiliateCategory->name : '';

        echo $form->field($newModel, 'affiliate_category_id', [
            'options' => ['class' => 'form-group', 'style' => 'width: 250px'],
        ])->widget(Select2::classname(), [
            'data' => $affCategories,
            'options' => ['placeholder' => '-- Chọn loại affiliate --'],
            'pluginOptions' => [
                'allowClear' => true
            ],
            'hideSearch' => true,
        ])->label(false)->error(false); ?>

        <?= $form->field($newModel, 'commission_percent', [
            'options' => [
                'class' => 'form-group',
            ]
        ])->textInput(['placeholder' => 'Phần trăm hoa hồng'])->label(false)->error(false) ?>


        <?= Html::submitButton($crudButtonIcons['create'] . ' ' . $crudTitles['create'], ['class' => 'btn btn-success']) ?>

        <?php ActiveForm::end() ?>
        <?php
        if ($newModel->hasErrors()) {
            $errors = [];
            foreach($newModel->errors as $error) {
                $errors[] = $error[0];
            }
            echo "<span class='navbar-text navbar-left' style='color: red'>". implode(', ', $errors) ."</span>";
        }
        ?>
    </nav>
    <?php Pjax::begin(); ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                [
                    'attribute' => 'id',
                    'options' => ['class' => 'col-xs-1'],
                ],
                [
                    'attribute' => 'course_id',
                    'value' => function ($model) {return $model->course->name; },
                    'options' => ['class' => 'col-xs-3'],
                    'filter' => Select2::widget([
                        'model' => $searchModel,
                        'initValueText' => !empty($searchModel->course) ? $searchModel->course->name : '',
                        'attribute' => 'course_id',
                        'options' => ['placeholder' => $crudTitles['prompt']],
                        'pluginOptions' => [
                            'allowClear' => true,
                            'minimumInputLength' => 3,
                            'language' => [
                                'errorLoading' => new JsExpression("function () { return 'Chờ nhận kết quả...'; }"),
                            ],
                            'ajax' => [
                                'url' => Url::toRoute(['/course/api/search', 'auto' => true, 'type' => $courseTypes]),
                                'dataType' => 'json',
                            ],
                        ],
                    ])
                ],
                [
                    'attribute' => 'affiliate_category_id',
                    'value' => function ($model) {
                        return $model->affiliateCategory->name;
                    },
                    'format' => 'raw',
                    'filter' => Select2::widget([
                        'model' => $searchModel,
                        'attribute' => 'affiliate_category_id',
                        'data' => $affCategories,
                        'options' => ['placeholder' => $crudTitles['prompt']],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                        'hideSearch' => true,
                    ])
                ],
                'commission_percent',
                [
                    'attribute' => 'status',
                    'value' => function ($model) {
                        return $model->statusButton;
                    },
                    'format' => 'raw',
                    'filter' => Select2::widget([
                        'model' => $searchModel,
                        'attribute' => 'status',
                        'data' => AffiliateCategoryCourse::listStatus(false),
                        'options' => ['placeholder' => $crudTitles['prompt']],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                        'hideSearch' => true,
                    ])
                ],

                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{update} {delete}'
                ],

            ],
        ]); ?>
    <?php Pjax::end(); ?>
</div>
