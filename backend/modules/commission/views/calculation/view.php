<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model kyna\affiliate\models\CommissionCalculation */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::$app->controller->mainTitle, 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$crudTitles = Yii::$app->params['crudTitles'];
?>
<div class="affiliate-calculation-view">

    <p>
        <?= Html::a("<span class='glyphicon glyphicon-backward'></span> " . $crudTitles['back'], ['index'], ['class' => 'btn btn-info', 'icon' => 'glyphicon glyphicon-backward']) ?>
        <?= Html::a($crudTitles['update'], ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a($crudTitles['delete'], ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'affiliate_group_id',
            'instructor_percent',
            'affiliate_percent',
            'start_date',
            'end_date',
            'is_deleted:boolean',
            'status',
            'created_time:datetime',
            'updated_time:datetime',
        ],
    ]) ?>

</div>
