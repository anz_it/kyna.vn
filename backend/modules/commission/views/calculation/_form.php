<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\Select2;
use kartik\widgets\DateTimePicker;
use kyna\commission\Affiliate;
use kyna\commission\models\CommissionCalculation;
use kyna\commission\models\AffiliateGroup;

$crudTitles = Yii::$app->params['crudTitles'];
?>

<div class="affiliate-calculation-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'affiliate_group_id')->widget(Select2::classname(), [
                'data' => AffiliateGroup::getHtmlSelectData(),
                'hideSearch' => true,
                'options' => ['placeholder' => $crudTitles['prompt']],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]) ?>

    <?= $form->field($model, 'instructor_percent')->textInput() ?>

    <?= $form->field($model, 'affiliate_percent')->textInput() ?>

    <?= $form->field($model, 'start_date')->widget(DateTimePicker::classname(), [
        'options' => ['placeholder' => 'Chọn ngày'],
        'pluginOptions' => [
            'autoclose' => true,
            'format' => 'dd/mm/yyyy H:ii',
        ]
    ]); ?>

    <?= $form->field($model, 'end_date')->widget(DateTimePicker::classname(), [
        'options' => ['placeholder' => 'Chọn ngày'],
        'pluginOptions' => [
            'autoclose' => true,
             'format' => 'dd/mm/yyyy H:ii',
        ]
    ]); ?>

    <?= $form->field($model, 'status')->widget(Select2::classname(), [
                'data' => CommissionCalculation::listStatus(),
                'hideSearch' => true,
                'options' => ['placeholder' => $crudTitles['prompt']],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? $crudTitles['create'] : $crudTitles['update'], ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::a($crudTitles['cancel'], ['index'], ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
