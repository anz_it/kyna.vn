<?php

use kartik\grid\GridView;
use yii\bootstrap\Nav;

$this->title = 'Danh sách học viên';
$this->params['breadcrumbs'][] = ['label' => Yii::$app->controller->mainTitle, 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$crudTitles = Yii::$app->params['crudTitles'];
$gridColumns = [
    ['class' => 'yii\grid\SerialColumn'],
    'created_time:date',
    [
        'attribute' => 'course_id',
        'value' => function ($model) {return $model->course->name; },
        'options' => ['class' => 'col-xs-3'],
    ],
    [
        'label' => 'Học viên',
        'value' => function ($model, $key) {
            $user = $model->order->user;
            return !empty($user) ? $user->email : null;
        }
    ],
    [
        'label' => 'Học phí thanh toán',
        'format' => 'currency',
        'value' => function ($model) {
            return $model->total_amount + $model->fee;
        }
    ],
    [
        'label' => 'Thực nhận học phí',
        'format' => 'currency',
        'value' => function ($model) {
            return $model->realIncome;
        }
    ],
    [
        'label' => 'Hoa hồng (%)',
        'value' => function ($model, $key) {
            return $model->teacher_commission_percent;
        }
    ],
    'teacher_commission_amount:currency:Hoa hồng giảng dạy',
];
?>
<style>
    .dl-horizontal dt {
        width: 165px;
    }
</style>
<div class="commission-income-view">
    <?= Nav::widget([
        'items' => $tabs,
        'options' => ['class' => 'nav-pills'],
    ]) ?>
    <nav class="navbar navbar-default">
        <div class="btn-group navbar-btn navbar-left">
            <div class="btn-group">
                <button id="export_xls" class="btn btn-default dropdown-toggle" title="Export data in selected format" data-toggle="dropdown"><i class="glyphicon glyphicon-export"></i> Export Excel </button>
            </div>
        </div>
    </nav>
    <div class="row">
        <div class="col-xs-12">
            <div class="commission-income-index">
                <nav class="navbar navbar-default">
                    <h4 class="navbar-text navbar-left" style="font-weight: bold">Thống kê học viên theo học</h4>
                    <?= $this->render('_filter', ['searchModel' => $searchModel]) ?>
                </nav>
                <div class="box">
                    <?=
                    GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => false,
                        'columns' => $gridColumns,
                    ]);
                    ?>
                </div>
            </div>
        </div>
    </div>

</div>
<?php
$script = "
    ;(function($, window, document, undefined){
        $(document).ready(function(){
            $('body').on('click', '#export_xls', function (event) {            
                event.preventDefault();
                var exportDialog = new BootstrapDialog();
                exportDialog.setTitle('Vui lòng nhập email nhận file export');
                exportDialog.setMessage($('<input id=\"email_export\" class=\"form-control\" placeholder=\"Email\" value=\"".Yii::$app->user->identity->email."\"></input>'));
                exportDialog.setType(BootstrapDialog.TYPE_SUCCESS);
                exportDialog.setButtons([{
                    label: 'Export',
                    action: function(dialog) {                       
                        var email = $('#email_export').val(); 
                        if (email == '') {
                            alert('Vui lòng nhập email');
                        }
                        var data = {email: email, type: 'export'};
                        $.post('', data, function (response) {
                            if (response.result) {
                                var bootstrapDialog = new BootstrapDialog();
                                bootstrapDialog.setMessage('Vui lòng kiểm tra email '+email+'. File Export sẽ được gửi đến trong vài phút.');
                                bootstrapDialog.setType(BootstrapDialog.TYPE_SUCCESS);
                                bootstrapDialog.open(); 
                                exportDialog.close();          
                            } else {
                                
                            }
                        });
                    }
                }]);
                exportDialog.open(); 
            });
        });
    })(window.jQuery || window.Zepto, window, document);";
$css = "
    .modal-content .modal-header {
        border-radius: 0;
    }
    ";
?>
<?php
$this->registerCss($css);
$this->registerJs($script, \yii\web\View::POS_END, 'export-teacher-student');
?>

