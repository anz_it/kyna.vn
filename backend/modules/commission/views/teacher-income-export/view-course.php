<?php

use common\helpers\RoleHelper;
use kartik\export\ExportMenu;
use kartik\widgets\Select2;
use kyna\course\models\Category;
use yii\bootstrap\Nav;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;

$this->title = 'Danh sách khóa học đang giảng dạy';
$frontendUrl = Yii::$app->params['baseUrl'];
$crudTitles = Yii::$app->params['crudTitles'];
$teacherDesc = empty($searchModel->teacher_id) ? '' : $searchModel->teacher->profile->name;
$this->params['breadcrumbs'][] = ['label' => Yii::$app->controller->mainTitle, 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$gridColumns = [
    [
        'attribute' => 'id',
        'options' => ['class' => 'col-xs-1']
    ],
    'name',
//    [
//        'attribute' => 'teacher_id',
//        'value' => function ($model) {
//            return !empty($model->teacher) ? $model->teacher->profile->name : null;
//        },
//        'filter' => Select2::widget([
//            'model' => $searchModel,
//            'initValueText' => $teacherDesc,
//            'attribute' => 'teacher_id',
//            'options' => ['placeholder' => $crudTitles['prompt']],
//            'pluginOptions' => [
//                'allowClear' => true,
//                'minimumInputLength' => 3,
//                'language' => [
//                    'errorLoading' => new JsExpression("function () { return 'Chờ nhận kết quả...'; }"),
//                ],
//                'ajax' => [
//                    'url' => Url::toRoute(['/user/api/search', 'role' => RoleHelper::ROLE_TEACHER, 'selectNameOnly' => true]),
//                    'dataType' => 'json',
//                    'data' => new JsExpression('function(params) { return {q:params.term}; }'),
//                ],
//                'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
//                'templateResult' => new JsExpression('function(teacher) { return teacher.text; }'),
//                'templateSelection' => new JsExpression('function (teacher) { return teacher.text; }'),
//            ],
//        ])
//    ],
    [
        'attribute' => 'price',
        'format' => 'currency',
        'options' => ['class' => 'col-xs-1']
    ],
    [
        'label' => 'Hoa hồng (%)',
        'value' => function ($model) {
            return $model->getCommissionPercent();
        }
    ],
];
?>

<div class="commission-income-view">
    <?= Nav::widget([
        'items' => $tabs,
        'options' => ['class' => 'nav-pills'],
    ]) ?>
    <nav class="navbar navbar-default">
        <?php
        echo ExportMenu::widget([
            'dataProvider' => $dataProvider,
            'columns' => $gridColumns,
            'fontAwesome' => true,
            'container' => [
                'class' => 'btn-group navbar-btn navbar-left'
            ],
            'columnSelectorOptions'=>[
                'label' => 'Export Cols',
            ],
            'dropdownOptions' => [
                'label' => 'Export All',
                'class' => 'btn btn-default'
            ],
            'showConfirmAlert' => false,
            'showColumnSelector' => false,
            'target' => ExportMenu::TARGET_BLANK,
            'filename' => 'teacher_course_export_'.date('Y-m-d_H-i', time()),
            'exportConfig' => [
                ExportMenu::FORMAT_PDF => false,
            ]
        ]);
        ?>
    </nav>
    <div class="box">
        <?=
        GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => $gridColumns
        ]);
        ?>
    </div>

</div>
