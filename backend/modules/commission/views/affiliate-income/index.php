<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\grid\GridView;

$crudTitles = Yii::$app->params['crudTitles'];

$this->title = Yii::$app->controller->mainTitle;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-xs-12">
        <div class="commission-income-index">
            <nav class="navbar navbar-default">
                <?= $this->render('_search', ['model' => $searchModel]) ?>
            </nav>
            <div class="box">
                <?=
                GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => false,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],
                        [
                            'attribute' => 'user_id',
                            'format' => 'raw',
                            'label' => 'Người giới thiệu',
                            'value' => function ($model) {
                                if (empty($model->user)) {
                                    return null;
                                }
                                $str = "Username: {$model->user->username}";
                                if (!empty($model->user->profile->name)) {
                                    $str.= "<br>Họ tên: {$model->user->profile->name}";
                                }
                                $str .= "<br>Email: {$model->user->email}";
                                
                                return $str;
                            },
                        ],
                        [
                            'attribute' => 'affiliate_total_amount',
                            'label' => 'Tổng thu nhập',
                            'format' => 'currency',
                            'value' => function ($model) use ($searchModel) {
                                $managerAmount = $model->getTotalManagerAmount(null, $searchModel->date_ranger);
                                $totalAmount = $model->affiliate_total_amount != null ? $model->affiliate_total_amount : 0;
                                return $totalAmount + $managerAmount;
                            }
                        ],
                        [
                            'class' => 'yii\grid\ActionColumn',
                            'template' => '{view}',
                            'buttons' => [
                                'view' => function ($url, $model, $key) {
                                    $url = Url::toRoute(['view', 'id' => $model->user_id]);
                                
                                    $options = [
                                        'title' => 'Xem chi tiết',
                                        'data-pjax' => '0',
                                    ];
                                    return Html::a('<span class="glyphicon glyphicon-eye-open"></span> Xem chi tiết', $url, $options);
                                }
                            ]
                        ],
                    ],
                ]);
                ?>
            </div>
        </div>
    </div>
</div>