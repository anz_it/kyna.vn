<?php

use yii\helpers\Url;
use yii\helpers\Html;
use common\components\GridView;

$crudTitles = Yii::$app->params['crudTitles'];

$this->title = 'Quản lý đại lý cấp 2';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-xs-12">
        <div class="box commission-income-index">
            <div class="box-header with-border">
                <?= $this->render('_search_manager', ['model' => $searchModel]) ?>
            </div>
            <div class="box-body">
                <?=
                GridView::widget([
                    'dataProvider' => $dataProvider,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],
                        [
                            'attribute' => 'user_id',
                            'format' => 'raw',
                            'label' => 'Người giới thiệu',
                            'value' => function ($model) {
                                if (empty($model->user)) {
                                    return null;
                                }
                                $str = '';
                                if (!empty($model->user->profile->name)) {
                                    $str.= "Họ tên: {$model->user->profile->name}";
                                    $str .= "<br>Email: {$model->user->email}";
                                } else {
                                    $str .= "Email: {$model->user->email}";
                                }
                                
                                return $str;
                            },
                        ],
                        [
                            'attribute' => 'affiliate_total_amount',
                            'label' => 'Tổng thu nhập',
                            'format' => 'currency',
                            'value' => function ($model) {
                                return $model->affiliate_total_amount != null ? $model->affiliate_total_amount : 0;
                            }
                        ],
                        [
                            'class' => 'yii\grid\ActionColumn',
                            'template' => '{view}',
                            'buttons' => [
                                'view' => function ($url, $model, $key) {
                                    $url = Url::toRoute(['view', 'id' => $model->user_id]);
                                
                                    $options = [
                                        'title' => 'Xem chi tiết',
                                        'data-pjax' => '0',
                                    ];
                                    return Html::a('<span class="fa fa-eye"></span> Xem chi tiết', $url, $options);
                                }
                            ]
                        ],
                    ],
                ]);
                ?>
            </div>
        </div>
    </div>
</div>