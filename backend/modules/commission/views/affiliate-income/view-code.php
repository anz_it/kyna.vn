<?php

use yii\grid\GridView;
use yii\bootstrap\Nav;
use yii\helpers\Html;

use kartik\form\ActiveForm;
use kartik\widgets\DatePicker;

use common\helpers\ArrayHelper;
use kyna\promotion\models\Promotion;

$crudTitles = Yii::$app->params['crudTitles'];
$crudButtonIcons = Yii::$app->params['crudButtonIcons'];

$formatter = Yii::$app->formatter;

$this->title = 'Các mã giảm giá đang sử dụng';
$this->params['breadcrumbs'][] = $this->title;

$viewCouponUrl = ['view-code', 'id' => Yii::$app->request->get('id')];
?>

<div class="commission-income-view">
    <?= Nav::widget([
        'items' => $tabs,
        'options' => ['class' => 'nav-pills'],
    ]) ?>
    
    <h1>Danh sách mã giảm giá</h1>
    <div class="commission-income-search">
        <nav class="navbar navbar-default">
            <?php $form = ActiveForm::begin([
                'action' => $viewCouponUrl,
                'options' => ['class' => 'navbar-form navbar-left'],
                'method' => 'get',
            ]); ?>

            <?= $form->field($searchModel, 'code')->textInput()->error(false) ?>
            
            <?= $form->field($searchModel, 'from_date')->widget(DatePicker::className(), [
                'pluginOptions' => [
                    'autoclose' => true,
                    'format' => 'dd/mm/yyyy',
                ]
            ])->label('Từ ngày')->error(false) ?>
            
            <?= $form->field($searchModel, 'to_date')->widget(DatePicker::className(), [
                'pluginOptions' => [
                    'autoclose' => true,
                    'format' => 'dd/mm/yyyy',
                ]
            ])->label('đến ngày')->error(false) ?>

            <div class="form-group">
                <?= Html::submitButton('<i class="ion-android-search"></i> Tìm kiếm', ['class' => 'btn btn-primary']) ?>
                <?= Html::a('Hủy tìm kiếm', $viewCouponUrl, ['class' => 'btn btn-default']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </nav>
    </div>
    <div class="box">
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => null,
            'columns' => [
                'code',
                [
                    'attribute' => 'value',
                    'format'    => 'raw',
                    'label' => 'Phần trăm giảm giá',
                    'value' => function($model) use ($formatter) {
                        if ($model->discount_type == Promotion::TYPE_PERCENTAGE) {
                            return "<b>" . $model->value . ' % </b>';
                        } else {
                            return "<b> " . $formatter->asCurrency($model->value) . "</b>";
                        }
                    }
                ],
                'number_usage:integer:Số lần sử dụng tối đa',
                'current_number_usage:integer: Số lần sử dụng hiện tại',
                [
                    'attribute' => 'start_date',
                    'value' => function ($model) use ($formatter) {
                        return !empty($model->start_date) ? $formatter->asDatetime($model->start_date) : null;
                    }
                ],
                [
                    'attribute' => 'end_date',
                    'value' => function ($model) use ($formatter) {
                        return !empty($model->end_date) ? $formatter->asDatetime($model->end_date) : null;
                    }
                ],
                [
                    'label' => 'Các khóa học áp dụng',
                    'value' => function ($model) {
                        return implode(', ', ArrayHelper::map($model->promoCourses, 'id', 'course.name'));
                    }
                ],
                [
                    'header' => 'Có thể sử dụng',
                    'format' => 'html',
                    'value' => function ($model) {
                        if ($model->canUse == Promotion::BOOL_YES) {
                            return '<span class="label label-success">' . Promotion::BOOL_YES_TEXT . '</span>';
                        } else {
                            return '<span class="label label-danger">' . Promotion::BOOL_NO_TEXT . '</span>';
                        }
                    }
                ]
            ],
        ]); ?>
    </div>
</div>
