<?php

use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Nav;
use yii\widgets\Pjax;

use kyna\course\models\Course;
use kyna\commission\models\AffiliatePaymentLink;

/* @var $this yii\web\View */
/* @var $searchModel kyna\commission\models\search\AffiliatePaymentLinkSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Link thanh toán';
$this->params['breadcrumbs'][] = $this->title;

$crudTitles = Yii::$app->params['crudTitles'];
$crudButtonIcons = Yii::$app->params['crudButtonIcons'];

$courseData = ArrayHelper::map(Course::find()->select(['id', 'name'])->all(), 'id', 'name');
?>
<div class="affiliate-payment-link-index">
    <?= Nav::widget([
        'items' => $tabs,
        'options' => ['class' => 'nav-pills'],
    ]) ?>

    <?php echo $this->render('_form_payment_link', ['model' => $model]); ?>

    <h1>Danh sách link thanh toán</h1>
    <?php Pjax::begin(['timeout' => Yii::$app->params['timeOutPjax'], 'id' => 'idPjaxReload']) ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                [
                    'attribute' => 'code',
                    'options' => [
                        'class' => 'col-xs-2'
                    ],
                ],
                [
                    'label' => 'Khóa học',
                    'value' => function ($model) use ($courseData) {
                        $courseIds = json_decode($model->course_ids, true);
                        $names = array_filter($courseData, function ($key) use ($courseIds) {
                            return in_array($key, $courseIds);
                        }, ARRAY_FILTER_USE_KEY);
                        return implode(', ', $names);
                    },
                    'options' => [
                        'class' => 'col-xs-4'
                    ],
                ],
                [
                    'label' => 'Link thanh toán',
                    'format' => 'url',
                    'value' => function ($model) {
                        return $model->link;
                    },
                    'options' => [
                        'class' => 'col-xs-4',
                    ],
                ],
                [
                    'class' => 'app\components\ActionColumnCustom',
                    'template' => '{update} {delete}',
                    'buttons' => [
                        'update' => function ($url, $model) {
                            return Html::a('<i class="fa fa-pencil"></i>', Url::to(['view-payment-link', 'id' => Yii::$app->request->get('id'), 'link_id' => $model->id]), ['data-pjax' => 0]);
                        }
                    ],
                ],
            ],
        ]); ?>
    <?php Pjax::end() ?>
</div>
