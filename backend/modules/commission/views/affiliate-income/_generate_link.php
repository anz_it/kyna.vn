<?php

use yii\helpers\Url;
use kartik\widgets\Select2;
use yii\web\JsExpression;

$affiliateUser = $searchModel->affiliateUser;
?>
<!--<div class="alert alert-info">
    <h1 class="lined thin" style="text-align: center">Chương trình DeltaViet Affiliate Marketing</h1>
    <ul class="aff">
        <li>Hưởng Hoa hồng <?/*= $affiliateUser->commissionPercent */?>% học phí (đã trừ đi phí thanh toán) đối với tất cả khóa học mà người được giới thiệu thanh toán.</li>
        <li>Cookie trong <?/*= $affiliateUser->cookieDay */?> ngày</li>
        <li>Người giới thiệu sau cùng sẽ thắng.</li>
    </ul>
</div>-->
<div class="row">
    <div class="form-group col-xs-6">
        <label class="control-label">Bước 1: Chọn khoá học mà bạn muốn giới thiệu.</label>
        <?= Select2::widget([
                'name' => 'affiliate-course',
                'options' => [
                    'placeholder' => '-- Chọn khóa học --',
                    'class' => 'form-control'
                ],
                'pluginOptions' => [
                    'allowClear' => true,
                    'minimumInputLength' => 3,
                    'language' => [
                        'errorLoading' => new JsExpression("function () { return 'Chờ nhận kết quả...'; }"),
                    ],
                    'ajax' => [
                        'url' => Url::toRoute(['/course/api/search', 'auto' => true]),
                        'dataType' => 'json',
                        'data' => new JsExpression('function(params) { return {q:params.term}; }'),
                    ],
                    'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                    'templateResult' => new JsExpression('function(course) { return course.text; }'),
                    'templateSelection' => new JsExpression('function (course) { if (course.price !== undefined) {$("#old-price").val(course.price); } return course.text; }'),
                ],
                'pluginEvents' => [
                    "change" => "function() {
                        var affiliate_id = " . $searchModel->user_id . ";
                        var course_id = $(this).val();
                        if (!course_id.length) {
                            $('#affiliate-link').val('');
                            return;
                        }
                        $.ajax({
                            url: '" . Url::toRoute(['/commission/affiliate-income/generate-link']) . "',
                            data: {
                                id: course_id,
                                affiliate_id: affiliate_id
                            },
                            success: function (response) {
                                $('#affiliate-link').val(response);
                            }
                        });
                    }",
                ]
            ]);
        ?>
    </div>
</div>
<div class="form-group">
    <label class="control-label">Bước 2: Gửi link giới thiệu khoá học này đến cho bạn bè hoặc một người dùng (user) bất kỳ nào đó (Chỉ khi người dùng click vào các link này thì hoa hồng mới được tính cho bạn).</label>
    <input id="affiliate-link" class="form-control" readonly/>
</div>