<?php

use common\components\GridView;
use yii\bootstrap\Nav;
use kyna\commission\models\AffiliateUser;

$crudTitles = Yii::$app->params['crudTitles'];

if (!(Yii::$app->user->can('Teacher') || Yii::$app->user->isAffiliate)) {
    $this->params['breadcrumbs'][] = ['label' => $this->context->mainTitle, 'url' => ['index']];
    $this->title = 'Xem chi tiết: ' . $searchModel->user->profile->name . ' - ' . $searchModel->user->email;
} else {
    $this->title = 'Thu nhập giới thiệu';
}
$this->params['breadcrumbs'][] = $this->title;

$affiliate = AffiliateUser::findOne(['user_id' => $searchModel->user_id]);
if ($affiliate->isAffiliateManager || $totalManagerAmount > 0) {
    $class = 'col-xs-3';
} else {
    $class = 'col-xs-4';
}
?>
<?php
if (count($this->context->viewTabs) > 1) {
    echo Nav::widget([
        'items' => $this->context->viewTabs,
        'options' => ['class' => 'nav-pills'],
    ]); 
} 
?>
<br>
<div class="row">
    <div class="col-xs-12">
        <?= $this->render('_generate_link', ['searchModel' => $searchModel]); ?>
    </div>
</div>
<div class="row">
    <div class="col-xs-12">
        <div class="commission-income-index">
            <nav class="navbar navbar-default">
                <h4 class="navbar-text navbar-left" style="font-weight: bold">Thống kê học viên theo học</h4>
                <?= $this->render('_filter', ['searchModel' => $searchModel]) ?>
            </nav>
            <div class="row">
                <div class="<?= $class ?>">
                    <div class="box box-solid">
                        <div class="box-body">
                            <dl class="dl-horizontal">
                                <dt>Số học viên theo bộ lọc</dt>
                                <dd><?= $totalRegFilter ?> HV</dd>
                                <dt>Trong đó thanh toán</dt>
                                <dd><?= $totalPaidFilter ?></dd>
                          </dl>
                        </div>
                    </div>
                </div>
                <div class="<?= $class ?>">
                    <div class="box box-solid">
                        <div class="box-body">
                            <dl class="dl-horizontal">
                                <dt>Tổng số học viên</dt>
                                <dd><?= $totalReg ?> HV</dd>
                                <dt>Trong đó thanh toán</dt>
                                <dd><?= $totalPaid ?></dd>
                          </dl>
                        </div>
                    </div>
                </div>
                <div class="<?= $class ?>">
                    <div class="box box-solid">
                        <div class="box-body">
                            <dl class="dl-horizontal">
                                <dt>Thu nhập theo bộ lọc</dt>
                                <dd><?= Yii::$app->formatter->asCurrency($totalAmountFilter) ?></dd>
                                <dt>Tổng thu nhập</dt>
                                <dd><?= Yii::$app->formatter->asCurrency($totalAmount) ?></dd>
                          </dl>
                        </div>
                    </div>
                </div>
                <?php if ($affiliate->isAffiliateManager || $totalManagerAmount > 0): ?>
                <div class="<?= $class ?>">
                    <div class="box box-solid">
                        <div class="box-body">
                            <dl class="dl-horizontal">
                                <dt>Doanh thu từ affiliate cấp 2</dt>
                                <dd><?= Yii::$app->formatter->asCurrency($totalManagerAmount) ?></dd>
                                <dt>Tổng thu nhập từ affiliate cấp 2</dt>
                                <dd><?= Yii::$app->formatter->asCurrency($totalChildAmount) ?></dd>
                            </dl>
                        </div>
                    </div>
                </div>
                <?php endif; ?>
            </div>
            <div class="box">
                <?php
                $roles = implode(",",
                    array_map(function ($p) { return $p->item_name; }, \kyna\user\models\AuthAssignment::find()->where(['user_id' => Yii::$app->user->id])->all())
                );
                echo GridView::widget([
                    'dataProvider' => $dataProvider,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],
                        'created_time:datetime',
                        [
                            'label' => 'Học viên',
                            'value' => function ($model, $key) use ($roles){
                                $user = $model->order->user;
                                if ($user == null)
                                    return "";
//                                if (strpos($roles, "Teacher") !== false) {
//                                    return substr($user->email, 0, 5).'******'.substr($user->email, strlen($user->email) - 5, 5);
//                                }
                                return $user->email;
                            }
                        ],
                        [
                            'label' => 'Điện thoại',
                            'value' => function ($model, $key) use ($roles) {
                                $user = $model->order->user;

                                if ($user == null)
                                    return "";
//                                if (strpos($roles, "Teacher") !== false) {
//                                    return substr($user->profile->phone_number, 0, 2).'******'.substr($user->profile->phone_number, strlen($user->profile->phone_number) - 3, 3);
//                                }
                                return $user->profile->phone_number;

                            }
                        ],
                        [
                            'attribute' => 'course_id',
                            'value' => function ($model) {
                                return !is_null($model->course) ? $model->course->name : null; 
                            },
                            'options' => ['class' => 'col-xs-3'],
                        ],
                        'total_amount:currency:Thực nhận học phí',
                        'affiliate_commission_amount:currency:Hoa hồng',
                    ],
                ]);
                ?>
            </div>
        </div>
    </div>
</div>
<?php
$css = "
    @media (min-width: 768px) {
        .dl-horizontal dt {            
            width: 195px;           
        }   
        .dl-horizontal dd {
            margin-left: 215px;
        }
    }
";
$this->registerCss($css);
?>