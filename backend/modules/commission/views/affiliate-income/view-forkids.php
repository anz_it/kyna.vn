<?php

use common\components\GridView;
use yii\bootstrap\Nav;
use yii\helpers\Html;

$crudTitles = Yii::$app->params['crudTitles'];

if (!(Yii::$app->user->can('Teacher') || Yii::$app->user->isAffiliate)) {
    $this->params['breadcrumbs'][] = ['label' => $this->context->mainTitle, 'url' => ['index']];
    $this->title = 'Xem chi tiết: ' . $searchModel->user->profile->name . ' - ' . $searchModel->user->email;
} else {
    $this->title = 'Thu nhập giới thiệu';
}
$this->params['breadcrumbs'][] = $this->title;

if (count($this->context->viewTabs) > 1) {
    echo Nav::widget([
        'items' => $this->context->viewTabs,
        'options' => ['class' => 'nav-pills'],
    ]);
}

$listStatus = [
    1 => 'Đang liên hệ',
    2 => 'Đã xác nhận',
    3 => 'Hoàn thành',
    4 => 'Hủy',
];
?>
    <br>
    <div class="row">
        <div class="col-xs-12">
            <? /*= $this->render('_generate_link', ['searchModel' => $searchModel]); */ ?>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="commission-income-index">
                <nav class="navbar navbar-default">
                    <h4 class="navbar-text navbar-left" style="font-weight: bold">Thống kê học viên theo học</h4>
                    <!-- --><? /*= $this->render('_filter', ['searchModel' => $searchModel]) */ ?>
                </nav>
                <div class="box">
                    <?php
                    echo GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],
                            'invoice_code',
                            'created_time:datetime',
                            'register_email',
                            'register_phone_number',
                            'category',
                            'total_amount:currency:Thực nhận học phí',
                            'affiliate_commission_amount:currency:Hoa hồng',
                            [
                                'attribute' => 'order_status_id',
                                'label' => 'Trạng thái',
                                'value' => function ($model) use ($listStatus) {
                                    return !empty($listStatus[$model->order_status_id]) ? $listStatus[$model->order_status_id] : $model->order_status;
                                },
                                'filter' => Html::activeDropDownList($searchModel, 'order_status_id', $listStatus, ['class' => 'form-control', 'prompt' => '-- Trạng thái --']),

                            ],
                        ],
                    ]);
                    ?>
                </div>
            </div>
        </div>
    </div>
<?php
$css = "
    @media (min-width: 768px) {
        .dl-horizontal dt {            
            width: 195px;           
        }   
        .dl-horizontal dd {
            margin-left: 215px;
        }
    }
";
$this->registerCss($css);
?>