<?php

use yii\helpers\Url;
use yii\helpers\Html;
use kartik\form\ActiveForm;
use yii\web\JsExpression;
use kartik\widgets\Select2;
use kartik\daterange\DateRangePicker;
use kyna\commission\models\AffiliateCategory;
use yii\helpers\ArrayHelper;

$affiliateId = !empty(Yii::$app->user->affiliate) ? Yii::$app->user->affiliate->user_id : '';
?>

<div class="commission-income-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'options' => ['class' => 'navbar-form navbar-left', 'style' => 'padding-left: 0;'],
        'method' => 'get',
    ]); ?>
        <?php
        $initText = !empty($model->user_id) ? $model->user->profile->name . ' - ' . $model->user->email : '';

        echo $form->field($model, 'user_id', [
            'options' => ['class' => 'form-group', 'style' => 'width: 300px'],
        ])->widget(Select2::classname(), [
            'initValueText' => $initText,
            'options' => ['placeholder' => '--Chọn affiliate--'],
            'pluginOptions' => [
                'allowClear' => true,
                'minimumInputLength' => 3,
                'language' => [
                    'errorLoading' => new JsExpression("function () { return 'Chờ nhận kết quả...'; }"),
                ],
                'ajax' => [
                    'url' => Url::toRoute(['/commission/api/search-affiliate']),
                    'dataType' => 'json',
                    'data' => new JsExpression('function(params) { return {q:params.term,key:"user",managerId:'.$affiliateId.'}; }'),
                ],
                'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                'templateResult' => new JsExpression('function(res) { return res.text; }'),
                'templateSelection' => new JsExpression('function (res) { return res.text; }'),
            ],
        ])->label(false)->error(false); ?>

    <?=
        $form->field($model, 'date_ranger', [
            'addon' => ['prepend' => ['content' => '<i class="glyphicon glyphicon-calendar"></i>']],
            'options' => ['class' => 'form-group'],
        ])->widget(DateRangePicker::classname(), [
            'options' => ['class' => 'form-control', 'placeholder' => '--Chọn thời gian--'],
            'useWithAddon' => true,
            'convertFormat' => true,
            'pluginOptions'=>[
                'timePicker' => true,
                'timePicker24Hour' => true,
                'locale'=>[
                    'format' => 'd/m/yy',
                    'separator'=> " - ", // after change this, must update in controller
                ],
            ]
        ])->label(false)->error(false)
        ?>

    <div class="form-group">
        <?= Html::submitButton('<i class="ion-android-search"></i> Lọc', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
