<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use yii\web\JsExpression;
use kartik\select2\Select2;

use kyna\promotion\models\Promotion;
use kyna\course\models\Course;

$crudTitles = Yii::$app->params['crudTitles'];
$model->course_ids = json_decode($model->course_ids, true);
$selectedCourses = ArrayHelper::map(Course::find()->where(['id' => $model->course_ids])->select(['id', 'name'])->all(), 'id', 'name');

$coupons = ArrayHelper::map(Promotion::find()->select('code')->where(['partner_id' => $model->affiliate_id])->all(), 'code', 'code');
?>

<div class="course-answer-form">

    <?php $form = ActiveForm::begin([
        'action' => Url::to(['/commission/affiliate-income/submit-payment-link', 'affiliate_id' => Yii::$app->request->get('id'), 'id' => $model->id]),
        'enableAjaxValidation' => true,
    ]); ?>

    <?php
    echo $form->field($model, 'course_ids')->widget(Select2::classname(), [
        'initValueText' => $selectedCourses,
        'options' => [
            'placeholder' => $crudTitles['prompt'],
            'multiple' => true
        ],
        'pluginOptions' => [
            'allowClear' => true,
            'minimumInputLength' => 3,
            'language' => [
                'errorLoading' => new JsExpression("function () { return 'Chờ nhận kết quả...'; }"),
            ],
            'ajax' => [
                'url' => Url::toRoute(['/course/api/search', 'auto' => true, 'type' => [Course::TYPE_VIDEO, Course::TYPE_SOFTWARE, Course::TYPE_COMBO]]),
                'dataType' => 'json',
                'data' => new JsExpression('function(params) { return {q:params.term}; }'),
            ],
            'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
            'templateResult' => new JsExpression('function(teacher) { return teacher.text; }'),
            'templateSelection' => new JsExpression('function (teacher) { return teacher.text; }'),
        ],
    ]);
    ?>

    <?php
    echo $form->field($model, 'code')->widget(Select2::classname(), [
        'data' => $coupons,
        'options' => [
            'placeholder' => $crudTitles['prompt'],
        ],
        'pluginOptions' => [
            'allowClear' => true,
        ],
    ]);
    ?>

    <?= Html::activeHiddenInput($model, 'affiliate_id') ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? $crudTitles['create'] : $crudTitles['update'], ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
        <?php
        if (!$model->isNewRecord) {
            echo Html::a('Cancel', Url::to(['view-payment-link', 'id' => Yii::$app->request->get('id')]), ['class' => 'btn btn-default']);
        }
        ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
