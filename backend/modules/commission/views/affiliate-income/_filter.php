<?php

use yii\helpers\Url;
use yii\helpers\Html;
use kartik\form\ActiveForm;
use kartik\daterange\DateRangePicker;
use kartik\widgets\Select2;
use yii\web\JsExpression;

?>

<?php
$form = ActiveForm::begin([
            'action' => ['view', 'id' => $searchModel->user_id],
            'options' => ['class' => 'navbar-form navbar-left'],
            'method' => 'get',
        ]);
?>

<?=
$form->field($searchModel, 'course_id', [
    'options' => [
        'class' => 'form-group',
        'style' => 'width: 250px'
    ],
])->widget(Select2::classname(), [
    'initValueText' => empty($searchModel->course_id) ? '' : $searchModel->course->name, // set the initial display text
    'options' => [
        'placeholder' => '-- Chọn khóa học --',
    ],
    'pluginOptions' => [
        'allowClear' => true,
        'minimumInputLength' => 3,
        'language' => [
            'errorLoading' => new JsExpression("function () { return 'Chờ nhận kết quả...'; }"),
        ],
        'ajax' => [
            'url' => Url::toRoute(['/course/api/search', 'auto' => true]),
            'dataType' => 'json',
            'data' => new JsExpression('function(params) { return {q:params.term}; }'),
        ],
        'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
        'templateResult' => new JsExpression('function(course) { return course.text; }'),
        'templateSelection' => new JsExpression('function (course) { if (course.price !== undefined) {$("#old-price").val(course.price); } return course.text; }'),
    ],
])->label(false)->error(false);
?>

<?=
$form->field($searchModel, 'date_ranger', [
    'addon' => ['prepend' => ['content' => '<i class="glyphicon glyphicon-calendar"></i>']],
    'options' => ['class' => 'form-group'],
])->widget(DateRangePicker::classname(), [
    'useWithAddon' => true,
    'convertFormat' => true,
    'pluginOptions' => [
        'timePicker' => true,
        'locale' => [
            'format' => 'd/m/yy',
            'separator' => " - ", // after change this, must update in controller
        ],
    ]
])->label(false)->error(false)
?>

<div class="form-group">
<?= Html::submitButton('<i class="ion-android-search"></i> Lọc', ['class' => 'btn btn-default']) ?>
</div>

<?php ActiveForm::end(); ?>