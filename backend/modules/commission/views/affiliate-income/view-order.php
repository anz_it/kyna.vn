<?php

use yii\helpers\Url;
use yii\grid\GridView;
use yii\bootstrap\Nav;
use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\web\JsExpression;
use kartik\widgets\Select2;
use kartik\form\ActiveForm;
use app\modules\commission\models\Order;

$month = [];
for ($i = 1; $i < 13; $i++) {
    $m = str_pad($i, 2, "0", STR_PAD_LEFT);
    $month[$m] = $m;
}
$year = [];
for ($i = date('Y'); $i > date('Y') - 5 ; $i--) {
    $year[$i] = $i;
}

$formatter = Yii::$app->formatter;
$user = Yii::$app->user;

$statusLabelCss = Order::getStatusLabelCss();
$statusLabelTeleSaleCss = \kyna\user\models\UserTelesale::getStatusLabelCss();
$status_order_labels = Order::getStatusLabels();
$status_telesales_labels = \kyna\user\models\UserTelesale::listStatusForAffiliate();
$this->title = 'Đơn đăng ký';

$viewOrderUrl = ['view-order', 'id' => Yii::$app->request->get('id')];
?>

<div class="commission-income-view">
    <?= Nav::widget([
        'items' => $tabs,
        'options' => ['class' => 'nav-pills'],
    ]) ?>
    
    <h1>Danh sách đơn đăng ký</h1>
    <div class="commission-income-search">
        <nav class="navbar navbar-default">
            <?php $form = ActiveForm::begin([
                'action' => $viewOrderUrl,
                'options' => ['class' => 'navbar-form navbar-left'],
                'method' => 'get',
            ]); ?>

            <?= $form->field($searchModel, 'month')->dropDownList($month)->label('Tháng')->error(false) ?>
            
            <?= $form->field($searchModel, 'year')->dropDownList($year)->label('Năm')->error(false) ?>
             -- Hoặc chọn ngày cụ thể --
            <?=
            $form->field($searchModel, 'date', [
                'options' => ['class' => 'form-group'],
            ])->widget(\kartik\widgets\DatePicker::classname(), [
                'pluginOptions' => [
                    'timePicker' => true,
                    'locale' => [
                        'format' => 'd/m/yy',
                        'separator' => " - ", // after change this, must update in controller
                    ],
                ]
            ])->label(false)->error(false)
            ?>

            <?=
            $form->field($searchModel, 'course_id', [
                'options' => [
                    'class' => 'form-group',
                    'style' => 'width: 250px'
                ],
            ])->widget(Select2::classname(), [
                'initValueText' => empty($searchModel->course_id) ? '' : $searchModel->course->name, // set the initial display text
                'options' => [
                    'placeholder' => '-- Chọn khóa học --',
                ],
                'pluginOptions' => [
                    'allowClear' => true,
                    'minimumInputLength' => 3,
                    'language' => [
                        'errorLoading' => new JsExpression("function () { return 'Chờ nhận kết quả...'; }"),
                    ],
                    'ajax' => [
                        'url' => Url::toRoute(['/course/api/search', 'auto' => true, 'course_type' => 'all']),
                        'dataType' => 'json',
                        'data' => new JsExpression('function(params) { return {q:params.term}; }'),
                    ],
                    'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                    'templateResult' => new JsExpression('function(course) { return course.text; }'),
                    'templateSelection' => new JsExpression('function (course) { if (course.price !== undefined) {$("#old-price").val(course.price); } return course.text; }'),
                ],
            ])->label(false)->error(false);
            ?>


            <div class="form-group">
                <?= Html::submitButton('<i class="ion-android-search"></i> Lọc', ['class' => 'btn btn-primary']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </nav>
    </div>
    <div class="row">
        <div class="col-xs-6">
            <div class="box box-solid">
                <div class="box-body">
                    <dl class="dl-horizontal">
                        <dt>Số học viên theo bộ lọc:</dt>
                        <dd><?= $dataProvider->totalCount ?> HV</dd>
                  </dl>
                </div>
            </div>
        </div>
        <div class="col-xs-6">
            <div class="box box-solid">
                <div class="box-body">
                    <dl class="dl-horizontal">
                        <dt>Thu nhập theo bộ lọc</dt>
                        <dd><?= $formatter->asCurrency($totalIncome) ?></dd>
                  </dl>
                </div>
            </div>
        </div>
    </div>
    <?php Pjax::begin(); ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                'id',
                [
                    'header' => 'Ngày đăng ký',
                    'attribute' => 'order_date',
                    'value' => function ($model, $key, $index) use ($formatter) {
                        $html = '<time>' . $formatter->asDatetime(!empty($model->order_date) ? $model->order_date : null) . '</time><br>';

                        return $html;
                    },
                    'format' => 'raw',
                ],
                [
                    'header' => 'Học viên',
                    'format' => 'raw',
                    'value' => function ($model, $key, $index) use ($formatter) {
                        $user = $model->user;
                        if (!empty($user)) {
                            return $formatter->asEmail($user->email) . '<br>' . (!empty($user->userAddress) ? $user->userAddress->toString(false) : '');
                        } else {
                            return $formatter->asEmail($model->email) . '<br>' . $model->full_name . '<br>' . $model->street_address;
                        }
                    },
                ],
                [
                    'header' => 'Điện thoại',
                    'format' => 'raw',
                    'value' => function ($model, $key, $index) use ($formatter) {
                        $user = $model->user;
                        if (!empty($user)) {
                            return $user->profile->phone_number;
                        } else {
                            return $model->phone_number;
                        }
                    },
                ],
                [
                    'label' => 'Khóa học',
                    'format' => 'raw',
                    'value' => function ($model) use ($formatter) {
                        if (is_null($model->id)) {
                            // user care
                            return $model->form_name . " <b>{$formatter->asCurrency($model->totalDetailIncome)}</b>({$model->originalAffiliateUser->commissionPercent}%)";
                        }
                        $details = $model->getDetails()->all();

                        $data = [];
                        foreach ($details as $detail) {
                            if ($detail->course_combo_id) {
                                $data[$detail->course_combo_id][] = $detail;
                            } else {
                                $data[$detail->course_id][] = $detail;
                            }
                        }

                        $html = '<address>';
                        foreach ($data as $items) {
                            $detail = $items[0];
                            $meta = json_decode($detail->course_meta, true);

                            if (count($items) == 1) {
                                // single course
                                $income = Yii::$app->formatter->asCurrency($detail->realIncome * $detail->affiliateCommissionPercent / 100);
                                $name = !empty($meta['name']) ? $meta['name'] : (!empty($detail->course) ? $detail->course->name : '');

                                $html .= $name . '. <strong>' . $income . "</strong>({$detail->affiliateCommissionPercent}%)";
                                $html .= '<br>';
                            } else {
                                // combo
                                $subName = '<ul>';

                                foreach ($items as $subDetail) {
                                    $income = Yii::$app->formatter->asCurrency($subDetail->realIncome * $subDetail->affiliateCommissionPercent / 100);

                                    $subMeta = json_decode($subDetail->course_meta, true);
                                    $subName .= "<li>{$subMeta['name']} <strong>{$income}</strong>({$subDetail->affiliateCommissionPercent}%)</li>";
                                }

                                $subName .= '</ul>';

                                if (isset($meta['combo_name'])) {
                                    $comboName = $meta['combo_name'];
                                } else {
                                    $comboName = $detail->combo->name;
                                }

                                $html .= $comboName . $subName;
                            }
                        }
                        $html .= '</address>';
                        return $html;
                    }
                ],
                'promotion_code',
                [
                    'label' => 'Tổng thực nhận học phí',
                    'format' => 'currency',
                    'value' => function ($model) {
                        return $model->totalRealIncome;
                    }
                ],
                [
                    'label' => 'Hoa hồng',
                    'format' => 'currency',
                    'value' => function ($model) {
                        return $model->totalDetailIncome;
                    }
                ],
                [
                    'label' => 'Trạng thái',
                    'format' => 'raw',
                    'value' => function ($model) use ($statusLabelCss,$statusLabelTeleSaleCss, $status_order_labels,$status_telesales_labels) {
                        if($model->is_order == 1)
                        {
                            if (isset($statusLabelCss[$model->is_order])) {
                                return '<span class="label ' . (isset($statusLabelCss[$model->status]) ? $statusLabelCss[$model->status] : '') . '">' . $status_order_labels[$model->status] . '</span>';
                            } else {
                                return $model->status;
                            }
                        } else {
                            if (isset($statusLabelCss[$model->status])) {
                                return '<span class="label ' . (isset($statusLabelTeleSaleCss[$model->status]) ? $statusLabelTeleSaleCss[$model->status] : '') . '">' . $status_telesales_labels[$model->status] . '</span>';
                            } else {
                                return $model->status;
                            }
                        }


                    }
                ],
            ],
        ]); ?>
    <?php Pjax::end(); ?>
</div>