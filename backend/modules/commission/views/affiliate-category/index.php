<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use kartik\select2\Select2;
use kyna\commission\models\AffiliateGroup;
use kyna\commission\models\AffiliateCategory;

$this->title = Yii::$app->controller->mainTitle;
$this->params['breadcrumbs'][] = $this->title;

$user = Yii::$app->user;

$crudTitles = Yii::$app->params['crudTitles'];
$crudButtonIcons = Yii::$app->params['crudButtonIcons'];
?>
<div class="row">
    <div class="col-xs-12">
        <div class="affiliate-category-index">
            <p>
                <?php
                if ($user->can('Affiliate.Category.Create')) {
                    echo Html::a($crudButtonIcons['create'] . ' ' . $crudTitles['create'], ['create'], ['class' => 'btn btn-success']);
                }
                ?>
            </p>

            <div class="box">
                <?php Pjax::begin(['timeout' => Yii::$app->params['timeOutPjax'], 'id' => 'idPjaxReload']) ?>
                <?=
                GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],
                        [
                            'attribute' => 'id',
                            'options' => ['class' => 'col-xs-1']
                        ],
                        'name',
                        [
                            'attribute' => 'affiliate_group_id',
                            'value' => function ($model) {
                                return $model->affiliateGroup != null ? $model->affiliateGroup->name : null;
                            },
                            'format' => 'html',
                            'filter' => Select2::widget([
                                'model' => $searchModel,
                                'attribute' => 'affiliate_group_id',
                                'data' => AffiliateGroup::getHtmlSelectData(),
                                'options' => ['placeholder' => $crudTitles['prompt']],
                                'pluginOptions' => [
                                    'allowClear' => true
                                ],
                                'hideSearch' => true,
                            ])
                        ],
                        [
                            'attribute' => 'is_override_commission',
                            'format' => 'boolean',
                            'filter' => Select2::widget([
                                'model' => $searchModel,
                                'attribute' => 'is_override_commission',
                                'data' => AffiliateCategory::getBooleanOptions(),
                                'options' => ['placeholder' => $crudTitles['prompt']],
                                'pluginOptions' => [
                                    'allowClear' => true
                                ],
                                'hideSearch' => true,
                            ])
                        ],
                        [
                            'attribute' => 'commission_percent',
                            'value' => function ($model) {
                                return $model->is_override_commission ? $model->commission_percent : 'Theo Calculation của Group';
                            }
                        ],
                        'default_cookie_day',
                        [
                            'attribute' => 'status',
                            'value' => function ($model) use ($user) {
                                return $user->can('Affiliate.Category.Update') ? $model->statusButton : $model->statusHtml;
                            },
                            'format' => 'raw',
                            'filter' => Select2::widget([
                                'model' => $searchModel,
                                'attribute' => 'status',
                                'data' => AffiliateCategory::listStatus(false),
                                'options' => ['placeholder' => $crudTitles['prompt']],
                                'pluginOptions' => [
                                    'allowClear' => true
                                ],
                                'hideSearch' => true,
                            ])
                        ],
                        [
                            'class' => 'app\components\ActionColumnCustom',
                            'visibleButtons' => [
                                'update' => $user->can('Affiliate.Category.Update'),
                                'view' => $user->can('Affiliate.Category.View'),
                                'delete' => $user->can('Affiliate.Category.Delete'),
                            ],
                        ],
                    ],
                ]);
                ?>
                <?php Pjax::end() ?>
            </div>
        </div>
    </div>
</div>