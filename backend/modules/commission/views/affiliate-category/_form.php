<?php

use yii\web\View;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\Select2;
use kyna\commission\models\AffiliateCategory;
use kyna\commission\models\AffiliateGroup;
use common\helpers\ArrayHelper;

$crudTitles = Yii::$app->params['crudTitles'];
$categoryData = ArrayHelper::map(AffiliateCategory::find()->where(['status' => AffiliateCategory::STATUS_ACTIVE])->all(), 'id', 'name');
?>

<div class="affiliate-category-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput() ?>

    <?= $form->field($model, 'affiliate_group_id')->widget(Select2::classname(), [
                'data' => AffiliateGroup::getHtmlSelectData(),
                'hideSearch' => true,
                'options' => ['placeholder' => $crudTitles['prompt']],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]) ?>

    <?= $form->field($model, 'is_override_commission')->checkbox()->hint('(Nếu không sẽ dựa vào Calculation từ Group)') ?>

    <?= $form->field($model, 'commission_percent', ['options' => ['class' => $model->is_override_commission ? 'show' : 'hide']])->textInput() ?>

    <?= $form->field($model, 'default_cookie_day')->textInput() ?>

    <?= $form->field($model, 'category_can_not_override')->widget(Select2::classname(), [
        'data' => $categoryData,
        'hideSearch' => true,
        'options' => ['placeholder' => $crudTitles['prompt']],
        'pluginOptions' => [
            'allowClear' => true,
            'multiple' => true,
        ],
    ]) ?>

    <?= $form->field($model, 'status')->widget(Select2::classname(), [
                'data' => AffiliateCategory::listStatus(),
                'hideSearch' => true,
                'options' => ['placeholder' => $crudTitles['prompt']],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? $crudTitles['create'] : $crudTitles['update'], ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::a($crudTitles['cancel'], ['index'], ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<?php
$script = <<< SCRIPT
;(function ($, _){
    $('body').on('change', '#affiliatecategory-is_override_commission', function() {
        var comEle = $('.field-affiliatecategory-commission_percent');
        if ($(this).is(':checked')) {
            comEle.removeClass('hide');
            comEle.addClass('show');
        } else {
            comEle.find('#affiliatecategory-commission_percent').val(0);
            comEle.removeClass('show');
            comEle.addClass('hide');
        }
    });
})(jQuery, _);
SCRIPT;

$this->registerJs($script, View::POS_END, 'change-override-commission');
