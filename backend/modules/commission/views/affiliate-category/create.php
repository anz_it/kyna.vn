<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model kyna\affiliate\models\AffiliateCategory */

$this->title = Yii::$app->params['crudTitles']['create'];
$this->params['breadcrumbs'][] = ['label' => Yii::$app->controller->mainTitle, 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="affiliate-category-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
