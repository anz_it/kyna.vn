<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model kyna\affiliate\models\AffiliateCategory */

$crudTitles = Yii::$app->params['crudTitles'];

$this->title = $crudTitles['update'] . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::$app->controller->mainTitle, 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = $crudTitles['update'];
?>
<div class="affiliate-category-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
