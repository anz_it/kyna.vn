<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ActiveForm;
use yii\web\JsExpression;
use kartik\widgets\Select2;
use yii\helpers\ArrayHelper;
use kyna\commission\models\AffiliateCategory;
use kyna\commission\models\AffiliateUser;

$crudTitles = Yii::$app->params['crudTitles'];
?>

<div class="affiliate-user-form becourse-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <?php
        $initText = empty($model->user_id) ? '' : $model->user->email;

        echo $form->field($model, 'user_id', ['options' => ['class' => 'col-xs-6']])->widget(Select2::classname(), [
            'initValueText' => $initText, // set the initial display text
            'options' => ['placeholder' => $crudTitles['prompt'], 'disabled' => 'disabled'],
            'pluginOptions' => [
                'allowClear' => true,
                'minimumInputLength' => 3,
                'language' => [
                    'errorLoading' => new JsExpression("function () { return 'Chờ nhận kết quả...'; }"),
                ],
                'ajax' => [
                    'url' => Url::toRoute(['/user/api/search-by-email']),
                    'dataType' => 'json',
                    'data' => new JsExpression('function(params) { return {q:params.term}; }'),
                ],
                'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                'templateResult' => new JsExpression('function(res) { return res.text; }'),
                'templateSelection' => new JsExpression('function (res) { return res.text; }'),
            ],
        ]);
        ?>
    </div>

    <?= $form->field($model, 'is_manager')->checkbox() ?>
    <?php
    $initAffText = empty($model->parentAffiliate) ? '' : $model->parentAffiliate->user->email;
    $class = !$model->isAffiliateManager ? 'show' : 'hide';

    echo $form->field($model, 'manager_id', ['options' => ['class' => ' ' . $class]])->widget(Select2::classname(), [
        'initValueText' => $initAffText, // set the initial display text
        'options' => ['placeholder' => $crudTitles['prompt']],
        'pluginOptions' => [
            'allowClear' => true,
            'minimumInputLength' => 3,
            'language' => [
                'errorLoading' => new JsExpression("function () { return 'Chờ nhận kết quả...'; }"),
            ],
            'ajax' => [
                'url' => Url::toRoute(['/commission/api/search-affiliate']),
                'dataType' => 'json',
                'data' => new JsExpression('function(params) { return {q:params.term, key: "user", managerId: null, isManager: true}; }'),
            ],
            'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
            'templateResult' => new JsExpression('function(res) { return res.text; }'),
            'templateSelection' => new JsExpression('function (res) { return res.text; }'),
        ],
    ]);
    ?>

    <?= $form->field($model, 'affiliate_category_id')->widget(Select2::classname(), [
        'data' => ArrayHelper::map(AffiliateCategory::find()->where(['status' => AffiliateCategory::STATUS_ACTIVE])->all(), 'id', 'name'),
        'hideSearch' => true,
        'options' => [
            'placeholder' => '--Chọn loại affiliate--',
        ],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]) ?>

    <?= $form->field($model, 'is_override_commission')->checkbox() ?>

    <?= $form->field($model, 'commission_percent', ['options' => ['class' => $model->is_override_commission ? 'show' : 'hide']])->textInput() ?>

    <?= $form->field($model, 'cookie_day')->textInput() ?>

    <?= $form->field($model, 'status')->widget(Select2::classname(), [
                'data' => AffiliateUser::listStatus(),
                'hideSearch' => true,
                'options' => ['placeholder' => $crudTitles['prompt']],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? $crudTitles['create'] : $crudTitles['update'], ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::a($crudTitles['cancel'], ['index'], ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<?php
$script = <<< SCRIPT
;(function ($, _){
    $('body').on('change', '#affiliateuser-is_override_commission', function() {
        var comEle = $('.field-affiliateuser-commission_percent');
        if ($(this).is(':checked')) {
            comEle.removeClass('hide');
            comEle.addClass('show');
        } else {
            comEle.find('#affiliateuser-commission_percent').val(0);
            comEle.removeClass('show');
            comEle.addClass('hide');
        }
    });
    $('body').on('change', '#affiliateuser-is_manager', function() {
        var comEle = $('.field-affiliateuser-manager_id');
        if ($(this).is(':checked')) {
            comEle.addClass('hide');
        } else {
            comEle.removeClass('hide');
        }
    });
})(jQuery, _);
SCRIPT;
        
$this->registerJs($script, View::POS_END, 'change-override-commission');