<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\web\View;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\widgets\Pjax;
use yii\web\JsExpression;
use kartik\widgets\Select2;
use kyna\commission\models\AffiliateCategory;
use kyna\commission\models\AffiliateUser;

$crudTitles = Yii::$app->params['crudTitles'];
$crudButtonIcons = Yii::$app->params['crudButtonIcons'];

$this->title = Yii::$app->controller->mainTitle;
$this->params['breadcrumbs'][] = $this->title;

$user = Yii::$app->user;
$categoryData = ArrayHelper::map(AffiliateCategory::find()->where(['status' => AffiliateCategory::STATUS_ACTIVE])->all(), 'id', 'name');
$initAffManagerText = !empty($searchModel->parentAffiliate) ? $searchModel->parentAffiliate->user->email : '';
?>
<div class="row">
    <div class="col-xs-12">
        <div class="affiliate-user-index">
            <?php if ($user->can('Affiliate.User.Create')) : ?>
                <nav class="navbar navbar-default">
                    <?php $form = ActiveForm::begin([
                        'options' => [
                            'enctype' => 'multipart/form-data',
                            'class' => 'navbar-form navbar-left'
                        ],
                        'action' => Url::toRoute(['index'])
                    ]) ?>

                        <?php
                        $initText = !empty($model->user_id) ? $model->user->email : '';

                        echo $form->field($model, 'user_id', [
                            'options' => ['class' => 'form-group', 'style' => 'width: 200px'],
                        ])->widget(Select2::classname(), [
                            'initValueText' => $initText,
                            'options' => ['placeholder' => '--Chọn user--'],
                            'pluginOptions' => [
                                'allowClear' => true,
                                'minimumInputLength' => 3,
                                'language' => [
                                    'errorLoading' => new JsExpression("function () { return 'Chờ nhận kết quả...'; }"),
                                ],
                                'ajax' => [
                                    'url' => Url::toRoute(['/user/api/search-by-email']),
                                    'dataType' => 'json',
                                    'data' => new JsExpression('function(params) { return {q:params.term}; }'),
                                ],
                                'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                                'templateResult' => new JsExpression('function(res) { return res.text; }'),
                                'templateSelection' => new JsExpression('function (res) { return res.text; }'),
                            ],
                        ])->label(false)->error(false); ?>

                        <?php
                        $initAffText = empty($model->parentAffiliate) ? '' : $model->parentAffiliate->user->email;

                        echo $form->field($model, 'manager_id', [
                            'options' => ['class' => 'form-group', 'style' => 'width: 200px'],
                        ])->widget(Select2::classname(), [
                            'initValueText' => $initAffText,
                            'options' => ['placeholder' => '--Chọn affiliate manager--'],
                            'pluginOptions' => [
                                'allowClear' => true,
                                'minimumInputLength' => 3,
                                'language' => [
                                    'errorLoading' => new JsExpression("function () { return 'Chờ nhận kết quả...'; }"),
                                ],
                                'ajax' => [
                                    'url' => Url::toRoute(['/commission/api/search-affiliate']),
                                    'dataType' => 'json',
                                    'data' => new JsExpression('function(params) { return {q:params.term, key: "user", managerId: null, isManager: true}; }'),
                                ],
                                'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                                'templateResult' => new JsExpression('function(res) { return res.text; }'),
                                'templateSelection' => new JsExpression('function (res) { return res.text; }'),
                            ],
                        ])->label(false)->error(false); ?>

                        <?= $form->field($model, 'affiliate_category_id', [
                            'options' => ['class' => 'form-group', 'style' => 'width: 200px'],
                        ])->widget(Select2::classname(), [
                            'data' => $categoryData,
                            'hideSearch' => true,
                            'options' => [
                                'placeholder' => '--Chọn loại affiliate--',
                            ],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ])->error(false)->label(false) ?>

                        <?= $form->field($model, 'commission_percent', [
                            'options' => [
                                'class' => 'form-group' . (!$model->is_override_commission ? ' hide' : ''),
                            ]
                            ])->textInput(['placeholder' => 'Phần trăm hoa hồng'])->label(false)->error(false) ?>

                        <?= $form->field($model, 'is_override_commission', ['options' => ['class' => 'form-group']])->checkbox()->label(false)->error(false) ?>

                        <?= $form->field($model, 'is_manager', ['options' => ['class' => 'form-group']])->checkbox()->label(false)->error(false) ?>

                        <?= Html::submitButton($crudButtonIcons['create'] . ' ' . $crudTitles['create'], ['class' => 'btn btn-success']) ?>

                    <?php ActiveForm::end() ?>
                    <?php
                            if ($model->hasErrors()) {
                                $errors = [];
                                foreach($model->errors as $error) {
                                    $errors[] = $error[0];
                                }
                                echo "<span class='navbar-text navbar-left error'>". implode(', ', $errors) ."</span>";
                            }
                         ?>
                </nav>
            <?php endif; ?>

            <div class="box">
                <?php Pjax::begin(['timeout' => Yii::$app->params['timeOutPjax'], 'id' => 'idPjaxReload']) ?>
                <?php
                $initText = empty($searchModel->user_id) ? '' : $searchModel->user->profile->name;
                echo GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],
                        [
                            'attribute' => 'user_id',
                            'label' => 'Affiliate Id',

                            'options' => ['class' => 'col-xs-1']
                        ],
                        [
                            'attribute' => 'email',
                            'format' => 'raw',
                            'value' => function ($model) {
                                if (empty($model->user)) {
                                    return null;
                                }
                                $text =  !empty($model->user->profile) ? $model->user->profile->name : "";
                                return "<b>$text</b><br>{$model->user->email}";
                            },
                        ],
                        [
                            'attribute' => 'is_manager',
                            'value' => function ($model) use ($user) {
                                return $model->is_manager ? 'Yes' : 'No';
                            },
                            'format' => 'raw',
                            'filter' => Select2::widget([
                                'model' => $searchModel,
                                'attribute' => 'is_manager',
                                'data' => AffiliateUser::listIsManagerText(false),
                                'options' => ['placeholder' => $crudTitles['prompt']],
                                'pluginOptions' => [
                                    'allowClear' => true
                                ],
                                'hideSearch' => true,
                            ])
                        ],
                        [
                            'attribute' => 'manager_id',
                            'label' => 'Affiliate Manager',
                            'format' => 'raw',
                            'value' => function ($model) {
                                if (empty($model->parentAffiliate)) {
                                    return null;
                                }
                                $text =  !empty($model->parentAffiliate->user->profile) ? $model->parentAffiliate->user->profile->name : "";
                                return "<b>$text</b><br>{$model->parentAffiliate->user->email}";
                            },
                            'filter' => Select2::widget([
                                'model' => $searchModel,
                                'initValueText' => $initAffManagerText,
                                'attribute' => 'manager_id',
                                'options' => ['placeholder' => $crudTitles['prompt']],
                                'pluginOptions' => [
                                    'allowClear' => true,
                                    'minimumInputLength' => 3,
                                    'language' => [
                                        'errorLoading' => new JsExpression("function () { return 'Chờ nhận kết quả...'; }"),
                                    ],
                                    'ajax' => [
                                        'url' => Url::toRoute(['/commission/api/search-affiliate']),
                                        'dataType' => 'json',
                                        'data' => new JsExpression('function(params) { return {q:params.term, key: "user", managerId: null, isManager: true}; }'),
                                    ],
                                    'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                                    'templateResult' => new JsExpression('function(res) { return res.text; }'),
                                    'templateSelection' => new JsExpression('function (res) { return res.text; }'),
                                ],
                            ])
                        ],
                        [
                            'attribute' => 'affiliate_category_id',
                            'value' => function ($model) {
                                return !empty($model->affiliateCategory) ? $model->affiliateCategory->name : null;
                            },
                            'filter' => Select2::widget([
                                'model' => $searchModel,
                                'attribute' => 'affiliate_category_id',
                                'data' => $categoryData,
                                'options' => ['placeholder' => $crudTitles['prompt']],
                                'pluginOptions' => [
                                    'allowClear' => true
                                ],
                                'hideSearch' => true,
                            ])
                        ],
                        [
                            'attribute' => 'is_override_commission',
                            'format' => 'boolean',
                            'filter' => Select2::widget([
                                'model' => $searchModel,
                                'attribute' => 'is_override_commission',
                                'data' => AffiliateUser::getBooleanOptions(),
                                'options' => ['placeholder' => $crudTitles['prompt']],
                                'pluginOptions' => [
                                    'allowClear' => true
                                ],
                                'hideSearch' => true,
                            ])
                        ],
                        [
                            'attribute' => 'commission_percent',
                            'value' => function ($model) {
                                return $model->is_override_commission ? $model->commission_percent : 'Theo loại Affiliate hoặc công thức';
                            }
                        ],
                        'cookie_day',
                        [
                            'attribute' => 'status',
                            'value' => function ($model) use ($user) {
                                return $user->can('Affiliate.User.Update') ? $model->statusButton : $model->statusHtml;
                            },
                            'format' => 'raw',
                            'filter' => Select2::widget([
                                'model' => $searchModel,
                                'attribute' => 'status',
                                'data' => AffiliateUser::listStatus(false),
                                'options' => ['placeholder' => $crudTitles['prompt']],
                                'pluginOptions' => [
                                    'allowClear' => true
                                ],
                                'hideSearch' => true,
                            ])
                        ],
                        [
                            'class' => 'app\components\ActionColumnCustom',
                            'visibleButtons' => [
                                'update' => $user->can('Affiliate.User.Update'),
                                'view' => $user->can('Affiliate.User.View'),
                                'delete' => $user->can('Affiliate.User.Delete'),
                            ],
                        ],
                    ],
                ]);
                ?>
                <?php Pjax::end() ?>
            </div>
        </div>
    </div>
</div>

<?php
$thirdPartyGroup = Yii::$app->params['id_aff_group_third_party'];

$script = <<<SCRIPT
;(function ($, _){
    $('body').on('change', '#affiliateuser-affiliate_category_id', function() {
        var cate_id =  $(this).val();
        if (cate_id.length) {
            var url = '/affiliate/category/get-group';
            var csrfToken = $('meta[name=\'csrf-token\']').attr('content');

            $.ajax({
                url: url,
                method: 'post',
                data: {
                    cate_id: cate_id,
                    _csrf: csrfToken
                },
                success: function (res) {
                    var overrideComCheckbox = $('#affiliateuser-is_override_commission');
                    if (res == '$thirdPartyGroup') {
                        overrideComCheckbox.prop('checked', true);
                    } else {
                        overrideComCheckbox.prop('checked', false);
                    }
                    overrideComCheckbox.trigger("change");
                }
            });
        }
    });
        
    $('body').on('change', '#affiliateuser-is_override_commission', function() {
        var comEle = $('.field-affiliateuser-commission_percent');
        if ($(this).is(':checked')) {
            comEle.removeClass('hide');
        } else {
            comEle.find('#affiliateuser-commission_percent').val(0);
            comEle.addClass('hide');
        }
    });
    
    $('body').on('change', '#affiliateuser-is_manager', function() {
        var managerEle = $('.field-affiliateuser-manager_id');
        if ($(this).is(':checked')) {
            managerEle.addClass('hide');
        } else {
            managerEle.removeClass('hide');
        }
    });
})(jQuery, _);
SCRIPT;
        
$this->registerJs($script, View::POS_END, 'change-override-commission');