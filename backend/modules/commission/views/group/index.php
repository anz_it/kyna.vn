<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use kartik\select2\Select2;
use kyna\commission\models\AffiliateGroup;

/* @var $this yii\web\View */
/* @var $searchModel kyna\commission\models\search\AffiliateGroupSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Affiliate Groups';
$this->params['breadcrumbs'][] = $this->title;

$crudTitles = Yii::$app->params['crudTitles'];
$crudButtonIcons = Yii::$app->params['crudButtonIcons'];

$user = Yii::$app->user;
?>
<div class="affiliate-group-index">
    <p>
        <?= Html::a($crudButtonIcons['create'] . ' ' . $crudTitles['create'], ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<?php Pjax::begin(['timeout' => Yii::$app->params['timeOutPjax'], 'id' => 'idPjaxReload']); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'id',
                'options' => ['class' => 'col-xs-1']
            ],
            'name',
            'created_time:datetime',
            [
                'attribute' => 'status',
                'value' => function ($model) use ($user) {
                    return $user->can('Affiliate.Category.Update') ? $model->statusButton : $model->statusHtml;
                },
                'format' => 'raw',
                'filter' => Select2::widget([
                    'model' => $searchModel,
                    'attribute' => 'status',
                    'data' => AffiliateGroup::listStatus(false),
                    'options' => ['placeholder' => $crudTitles['prompt']],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                    'hideSearch' => true,
                ])
            ],
            ['class' => 'app\components\ActionColumnCustom'],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
