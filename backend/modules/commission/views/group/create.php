<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model kyna\commission\models\AffiliateGroup */

$this->title = 'Create Affiliate Group';
$this->params['breadcrumbs'][] = ['label' => 'Affiliate Groups', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="affiliate-group-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
