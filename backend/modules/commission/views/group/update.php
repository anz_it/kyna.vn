<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model kyna\commission\models\AffiliateGroup */

$this->title = 'Update Affiliate Group: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Affiliate Groups', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="affiliate-group-update">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
