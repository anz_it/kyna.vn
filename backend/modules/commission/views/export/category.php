<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\grid\GridView;
use kyna\order\models\Order;
use kartik\export\ExportMenu;

$crudTitles = Yii::$app->params['crudTitles'];

$this->title = Yii::$app->controller->mainTitle;
$this->params['breadcrumbs'][] = $this->title;

$gridColumns = [
    [
        'label' => 'Affiliate ID',
        'format' => 'raw',
        'value' => function ($model) {
            return $model->user_id;
        },
    ],
    [
        'label' => 'Affiliate username',
        'format' => 'raw',
        'value' => function ($model) {
            if (empty($model->user)) {
                return null;
            }
            return $model->user->username;
        },
    ],
    [
        'label' => 'Affiliate email',
        'format' => 'raw',
        'value' => function ($model) {
            if (empty($model->user)) {
                return null;
            }
            return $model->user->email;
        },
    ],
    [
        'label' => 'Số học viên theo bộ lọc',
        'format' => 'raw',
        'value' => function ($model) use ($searchModel) {
            $listOrder = Order::getOrderByAffiliateID($model->user_id, $searchModel->date_ranger);
            return $listOrder->count();
        },
    ],
    [
        'label' => 'Số học viên thanh toán (theo bộ lọc)',
        'format' => 'raw',
        'value' => function ($model) {
            return $model->total_reg_filter;
        },
    ],
    [
        'label' => 'Tổng số học viên lũy kế',
        'format' => 'raw',
        'value' => function ($model) {
            $listOrder = Order::getOrderByAffiliateID($model->user_id);
            return $listOrder->count();
        },
    ],
    [
        'label' => 'Tổng số học viên thanh toán lũy kế',
        'format' => 'raw',
        'value' => function ($model) {
            return $model->getCommissionIncomeByUserId()->count();
        },
    ],
    [
        'label' => 'Hoa hồng (theo bộ lọc)',
        'format' => 'currency',
        'value' => function ($model) {
            return $model->affiliate_total_amount;
        },
    ],
    [
        'label' => 'Hoa hồng lũy kế',
        'format' => 'currency',
        'value' => function ($model) {
            return $model->getCommissionIncomeByUserId()->sum('affiliate_commission_amount');
        },
    ]
];
?>
<div class="row">
    <div class="col-xs-12">
        <div class="commission-income-index">
            <?= \yii\bootstrap\Nav::widget([
                'items' => $tabs,
                'options' => ['class' => 'nav-pills'],
            ]) ?>
            <nav class="navbar navbar-default">
                <?= $this->render('_search_category', ['model' => $searchModel]) ?>

                <?= ExportMenu::widget([
                    'dataProvider' => $dataProvider,
                    'columns' => $gridColumns,
                    'fontAwesome' => true,
                    'container' => [
                        'class' => 'btn-group navbar-btn navbar-btn-right navbar-right'
                    ],
                    'columnSelectorOptions'=>[
                        'label' => 'Export Cols',
                    ],
                    'dropdownOptions' => [
                        'label' => 'Export All',
                        'class' => 'btn btn-default'
                    ],
                    'showConfirmAlert' => false,
                    'showColumnSelector' => false,
                    'target' => ExportMenu::TARGET_BLANK,
                    'filename' => 'affiliate_user_category_export_'.date('Y-m-d_H-i', time()),
                    'exportConfig' => [
                        ExportMenu::FORMAT_PDF => false,
                    ]
                ]);
                ?>
            </nav>
            <div class="box">
                <?=
                GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => false,
                    'columns' => array_merge($gridColumns, [
                        [
                            'class' => 'yii\grid\ActionColumn',
                            'template' => '{view}',
                            'buttons' => [
                                'view' => function ($url, $model, $key) {
                                    $url = Url::toRoute(['view', 'id' => $model->user_id]);

                                    $options = [
                                        'title' => 'Xem chi tiết',
                                        'data-pjax' => '0',
                                    ];
                                    return Html::a('<span class="glyphicon glyphicon-eye-open"></span> Xem chi tiết', $url, $options);
                                }
                            ]
                        ]
                    ]),
//                    'columns' => [
//                        [
//                            'label' => 'Affiliate ID',
//                            'format' => 'raw',
//                            'value' => function ($model) {
//                                return $model->user_id;
//                            },
//                        ],
//                        [
//                            'label' => 'Affiliate username',
//                            'format' => 'raw',
//                            'value' => function ($model) {
//                                if (empty($model->user)) {
//                                    return null;
//                                }
//                                return $model->user->username;
//                            },
//                        ],
//                        [
//                            'label' => 'Affiliate email',
//                            'format' => 'raw',
//                            'value' => function ($model) {
//                                if (empty($model->user)) {
//                                    return null;
//                                }
//                                return $model->user->email;
//                            },
//                        ],
//                        [
//                            'label' => 'Số học viên theo bộ lọc',
//                            'format' => 'raw',
//                            'value' => function ($model) use ($searchModel) {
//                                $listOrder = Order::getOrderByAffiliateID($model->user_id, $searchModel->date_ranger);
//                                return $listOrder->count();
//                            },
//                        ],
//                        [
//                            'label' => 'Số học viên thanh toán (theo bộ lọc)',
//                            'format' => 'raw',
//                            'value' => function ($model) {
//                                return $model->total_reg_filter;
//                            },
//                        ],
//                        [
//                            'label' => 'Tổng số học viên lũy kế',
//                            'format' => 'raw',
//                            'value' => function ($model) {
//                                $listOrder = Order::getOrderByAffiliateID($model->user_id);
//                                return $listOrder->count();
//                            },
//                        ],
//                        [
//                            'label' => 'Tổng số học viên thanh toán lũy kế',
//                            'format' => 'raw',
//                            'value' => function ($model) {
//                                return $model->getCommissionIncomeByUserId()->count();
//                            },
//                        ],
//                        [
//                            'label' => 'Hoa hồng (theo bộ lọc)',
//                            'format' => 'currency',
//                            'value' => function ($model) {
//                                return $model->affiliate_total_amount;
//                            },
//                        ],
//                        [
//                            'label' => 'Hoa hồng lũy kế',
//                            'format' => 'currency',
//                            'value' => function ($model) {
//                                return $model->getCommissionIncomeByUserId()->sum('affiliate_commission_amount');
//                            },
//                        ],
//                        [
//                            'class' => 'yii\grid\ActionColumn',
//                            'template' => '{view}',
//                            'buttons' => [
//                                'view' => function ($url, $model, $key) {
//                                    $url = Url::toRoute(['view', 'id' => $model->user_id]);
//
//                                    $options = [
//                                        'title' => 'Xem chi tiết',
//                                        'data-pjax' => '0',
//                                    ];
//                                    return Html::a('<span class="glyphicon glyphicon-eye-open"></span> Xem chi tiết', $url, $options);
//                                }
//                            ]
//                        ],
//                    ],
                ]);
                ?>
            </div>
        </div>
    </div>
</div>