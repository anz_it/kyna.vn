<?php

use yii\grid\GridView;
use kartik\export\ExportMenu;

$crudTitles = Yii::$app->params['crudTitles'];
$user = $searchModel->user;
$exportFileName = $searchModel->user_id . '_' . $user->profile->name . '_' . $user->email . '_' . $user->profile->phone_number;
$this->title = 'Xem chi tiết: ' . $user->profile->name . ' - ' . $user->email;
$this->params['breadcrumbs'][] = ['label' => $this->context->mainTitle, 'url' => ['category']];
$this->params['breadcrumbs'][] = $this->title;
$formatter = Yii::$app->formatter;
$gridColumns = [
    [
        'label' => 'User ID',
        'value' => function ($model, $key) {
            $user = $model->order;
            return !empty($user) ? $user->user_id : null;
        }
    ],
    [
        'label' => 'Họ và tên',
        'value' => function ($model, $key) {
            $user = $model->order->user;
            return !empty($user) ? $user->profile->name : null;
        }
    ],
    [
        'label' => 'Email',
        'value' => function ($model, $key) {
            $user = $model->order->user;
            return !empty($user->email) ? $user->email : null;
        }
    ],
    [
        'label' => 'Điện thoại',
        'value' => function ($model, $key) {
            $user = $model->order->user;
            return !empty($user) ? $user->profile->phone_number : null;
        }
    ],
    'created_time:date',
    'created_time:date',
    [
        'label' => 'Ngày đăng ký',
        'value' => function ($model, $key) use ($formatter){
            $order = $model->order;
            return $formatter->asDatetime(!empty($order->order_date) ? $order->order_date : null);
        }
    ],
    [
        'attribute' => 'course_id',
        'value' => function ($model) {return $model->course->name; },
        'options' => ['class' => 'col-xs-3'],
    ],
    [
        'label' => 'Giá trị đơn hàng thực tế (sau khuyến mãi)',
//        'attribute' => 'total',
        'format' => 'raw',
        'value' => function($model) use ($formatter) {
            $order = $model->order;
            return $formatter->asCurrency(!empty($order->total) ? $order->total : null);
        }
    ],
    [
        'header' => 'Chi phí giao nhận (học viên phải chịu)',
        'format' => 'raw',
        'value' => function ($model) use ($formatter) {
            $shipping_fee = null;
            $order = $model->order;
            if ($order->isCod) {
                if (!empty($order->shippingAddress->fee)) {
                    $shipping_fee = '<b>' . $formatter->asCurrency($order->shippingAddress->fee) . '</b>';
                }
            }
            return $shipping_fee;
        }
    ],
    [
        'label' => 'Chi phí giao nhận (Kyna trả GHN, đối tác)',
        'format' => 'raw',
        'value' => function($model) use ($formatter) {
            $order = $model->order;
            return $formatter->asCurrency(!empty($order->payment_fee) ? $order->payment_fee : null);
        }
    ],
//    'Giá trị đơn hàng thuần (sau chi phí thanh toán, bằng giá trị đơn hàng thực tế + chi phí giao nhận học viên chịu - chi phí Kyna trả GHN/đối tác)',

    [
        'label' => 'Giá trị đơn hàng thuần',
//        'attribute' => 'realInCome',
        'format' => 'raw',
        'value' => function($model) use ($formatter) {
            $order = $model->order;
            return $formatter->asCurrency(!empty($order->realInCome) ? $order->realInCome : null);
        }
    ],
//    'total_amount:currency:Thực nhận học phí',
    'affiliate_commission_amount:currency:Hoa hồng',
];
?>
<div class="row">
    <div class="col-xs-12">
        <div class="commission-income-index">
            <?= \yii\bootstrap\Nav::widget([
                'items' => $tabs,
                'options' => ['class' => 'nav-pills'],
            ]) ?>
            <nav class="navbar navbar-default">
                <h4 class="navbar-text navbar-left" style="font-weight: bold">Thống kê học viên theo học</h4>
                <?= $this->render('_filter', ['searchModel' => $searchModel]) ?>
                <?= ExportMenu::widget([
                    'dataProvider' => $dataProvider,
                    'columns' => $gridColumns,
                    'fontAwesome' => true,
                    'container' => [
                        'class' => 'btn-group navbar-btn navbar-btn-right navbar-right'
                    ],
                    'columnSelectorOptions'=>[
                        'label' => 'Export Cols',
                    ],
                    'dropdownOptions' => [
                        'label' => 'Export All',
                        'class' => 'btn btn-default'
                    ],
                    'showConfirmAlert' => false,
                    'showColumnSelector' => false,
                    'target' => ExportMenu::TARGET_BLANK,
                    'filename' => $exportFileName.'_'.date('Y-m-d_H-i', time()),
                    'exportConfig' => [
                        ExportMenu::FORMAT_PDF => false,
                    ]
                ]);
                ?>
            </nav>
            <div class="row">
                <div class="col-xs-4">
                    <div class="box box-solid">
                        <div class="box-body">
                            <dl class="dl-horizontal">
                                <dt>Số học viên theo bộ lọc</dt>
                                <dd><?= $totalRegFilter ?> HV</dd>
                                <dt>Trong đó thanh toán</dt>
                                <dd><?= $totalPaidFilter ?></dd>
                          </dl>
                        </div>
                    </div>
                </div>
                <div class="col-xs-4">
                    <div class="box box-solid">
                        <div class="box-body">
                            <dl class="dl-horizontal">
                                <dt>Tổng số học viên</dt>
                                <dd><?= $totalReg ?> HV</dd>
                                <dt>Trong đó thanh toán</dt>
                                <dd><?= $totalPaid ?></dd>
                          </dl>
                        </div>
                    </div>
                </div>
                <div class="col-xs-4">
                    <div class="box box-solid">
                        <div class="box-body">
                            <dl class="dl-horizontal">
                                <dt>Thu nhập theo bộ lọc</dt>
                                <dd><?= Yii::$app->formatter->asCurrency($totalAmountFilter) ?></dd>
                                <dt>Tổng thu nhập</dt>
                                <dd><?= Yii::$app->formatter->asCurrency($totalAmount) ?></dd>
                          </dl>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box">
                <?=
                GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => false,
                    'columns' => $gridColumns
                ]);
                ?>
            </div>
        </div>
    </div>
</div>