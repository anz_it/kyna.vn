<?php

use kartik\export\ExportMenu;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\grid\GridView;
use kartik\select2\Select2;
use kyna\commission\Affiliate;
use kyna\commission\models\AffiliateCategory;

$this->title = Yii::$app->controller->mainTitle;
$this->params['breadcrumbs'][] = $this->title;

$crudTitles = Yii::$app->params['crudTitles'];
$crudButtonIcons = Yii::$app->params['crudButtonIcons'];
$gridColumns = [
    'id',
];
?>
<div class="row">
    <div class="col-xs-12">
        <div class="affiliate-category-index">
            <?= \yii\bootstrap\Nav::widget([
                'items' => $tabs,
                'options' => ['class' => 'nav-pills'],
            ]) ?>

            <nav class="navbar navbar-default">
                <?= ExportMenu::widget([
                    'dataProvider' => $dataProvider,
                    'columns' => $gridColumns,
                    'fontAwesome' => true,
                    'container' => [
                        'class' => 'btn-group navbar-btn navbar-btn-right navbar-right'
                    ],
                    'columnSelectorOptions'=>[
                        'label' => 'Export Cols',
                    ],
                    'dropdownOptions' => [
                        'label' => 'Export All',
                        'class' => 'btn btn-default'
                    ],
                    'showConfirmAlert' => false,
                    'showColumnSelector' => false,
                    'exportConfig' => [
                        ExportMenu::FORMAT_PDF => false,
                    ]
                ]);
                ?>
            </nav>

            <div class="box">
                <?=
                GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],
                        [
                            'attribute' => 'id',
                            'options' => ['class' => 'col-xs-1']
                        ],
                        'name',
                        [
                            'attribute' => 'group',
                            'value' => function ($model) {
                                return $model->affiliateGroup->name;
                            },
                            'format' => 'html',
                            'filter' => Select2::widget([
                                'model' => $searchModel,
                                'attribute' => 'group',
                                'data' => Affiliate::getGroupOptions(),
                                'options' => ['placeholder' => $crudTitles['prompt']],
                                'pluginOptions' => [
                                    'allowClear' => true
                                ],
                                'hideSearch' => true,
                            ])
                        ],
                        [
                            'attribute' => 'is_override_commission',
                            'format' => 'boolean',
                            'filter' => Select2::widget([
                                'model' => $searchModel,
                                'attribute' => 'is_override_commission',
                                'data' => AffiliateCategory::getBooleanOptions(),
                                'options' => ['placeholder' => $crudTitles['prompt']],
                                'pluginOptions' => [
                                    'allowClear' => true
                                ],
                                'hideSearch' => true,
                            ])
                        ],
                        [
                            'attribute' => 'commission_percent',
                            'value' => function ($model) {
                                return $model->is_override_commission ? $model->commission_percent : 'Theo Calculation của Group';
                            }
                        ],
                        'default_cookie_day',
                        [
                            'attribute' => 'status',
                            'value' => function ($model) {
                                return Html::a($model->statusHtml, Url::toRoute(['/affiliate/category/change-status', 'id' => $model->id]), ['title' => 'Thay đổi tình trạng']);
                            },
                            'format' => 'html',
                            'filter' => Select2::widget([
                                'model' => $searchModel,
                                'attribute' => 'status',
                                'data' => AffiliateCategory::listStatus(false),
                                'options' => ['placeholder' => $crudTitles['prompt']],
                                'pluginOptions' => [
                                    'allowClear' => true
                                ],
                                'hideSearch' => true,
                            ])
                        ],
                        ['class' => 'yii\grid\ActionColumn'],
                    ],
                ]);
                ?>
            </div>
        </div>
    </div>
</div>