<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\grid\GridView;
use kartik\export\ExportMenu;
use kyna\order\models\Order;

$crudTitles = Yii::$app->params['crudTitles'];

$this->title = Yii::$app->controller->mainTitle;
$this->params['breadcrumbs'][] = $this->title;
$gridColumns = [
//    'id',
    [
        'label' => 'Affiliate category',
        'format' => 'raw',
        'value' => function ($model) {
            if ($model->affiliateCategory != null) {
                return $model->affiliateCategory->name;
            }
        },
    ],
    [
        'label' => 'Số học viên theo bộ lọc',
        'format' => 'html',
        'value' => function ($model) use ($searchModel) {
            $listOrder = Order::getOrderByAffiliateCategoryID($model->affiliate_category_id, $searchModel->date_ranger);
            $countNumber = $listOrder->count();
            return !empty($countNumber)?$countNumber:'=0';
        },
    ],
    [
        'label' => 'Số học viên thanh toán (theo bộ lọc)',
        'format' => 'raw',
        'value' => function ($model) {
            return $model->total_reg_filter;
        },
    ],
    [
        'label' => 'Tổng số học viên lũy kế',
        'format' => 'raw',
        'value' => function ($model) {
            $listOrder = Order::getOrderByAffiliateCategoryID($model->affiliate_category_id);
            $countNumber = $listOrder->count();
            return !empty($countNumber)?$countNumber:'=0';
        },
    ],
    [
        'label' => 'Tổng số học viên thanh toán lũy kế',
        'format' => 'raw',
        'value' => function ($model) {
            $countNumber = $model->getCommissionIncomeByUserId()->count();
            return !empty($countNumber)?$countNumber:null;
        },
    ],
    [
        'label' => 'Hoa hồng (theo bộ lọc)',
        'format' => 'currency',
        'value' => function ($model) {
            return $model->affiliate_total_amount;
        },
    ],
    [
        'label' => 'Hoa hồng lũy kế',
        'format' => 'currency',
        'value' => function ($model) {
            return $model->getCommissionIncomeByAffiliateCategoryId()->sum('affiliate_commission_amount');
        },
    ]

];
?>
<div class="row">
    <div class="col-xs-12">
        <div class="commission-income-index">
            <?= \yii\bootstrap\Nav::widget([
                'items' => $tabs,
                'options' => ['class' => 'nav-pills'],
            ]) ?>
            <nav class="navbar navbar-default">
                <?= $this->render('_search', ['model' => $searchModel]) ?>
                <?= ExportMenu::widget([
                    'dataProvider' => $dataProvider,
                    'columns' => $gridColumns,
                    'fontAwesome' => true,
                    'container' => [
                        'class' => 'btn-group navbar-btn navbar-btn-right navbar-right'
                    ],
                    'columnSelectorOptions'=>[
                        'label' => 'Export Cols',
                    ],
                    'dropdownOptions' => [
                        'label' => 'Export All',
                        'class' => 'btn btn-default'
                    ],
                    'showConfirmAlert' => false,
                    'showColumnSelector' => false,
                    'target' => ExportMenu::TARGET_BLANK,
                    'filename' => 'affiliate_all_category_export_'.date('Y-m-d_H-i', time()),
                    'exportConfig' => [
                        ExportMenu::FORMAT_PDF => false,
                        ExportMenu::FORMAT_TEXT => false,
                    ],
                ]);
                ?>
            </nav>
            <div class="box">
                <?=
                GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => false,
                    'columns' => [
                        [
                            'label' => 'Affiliate category',
                            'format' => 'raw',
                            'value' => function ($model) {
                                if ($model->affiliateCategory != null) {
                                    return $model->affiliateCategory->name;
                                }

                            },
                        ],
                        [
                            'label' => 'Số học viên theo bộ lọc',
                            'format' => 'html',
                            'value' => function ($model) use ($searchModel) {
                                $listOrder = Order::getOrderByAffiliateCategoryID($model->affiliate_category_id, $searchModel->date_ranger);
                                $countNumber = $listOrder->count();
                                return !empty($countNumber)?$countNumber:'0';
                            },
                        ],
                        [
                            'label' => 'Số học viên thanh toán (theo bộ lọc)',
                            'format' => 'raw',
                            'value' => function ($model) {
                                return $model->total_reg_filter;
                            },
                        ],
                        [
                            'label' => 'Tổng số học viên lũy kế',
                            'format' => 'raw',
                            'value' => function ($model) {
                                $listOrder = Order::getOrderByAffiliateCategoryID($model->affiliate_category_id);
                                $countNumber = $listOrder->count();
                                return !empty($countNumber)?$countNumber:'0';
                            },
                        ],
                        [
                            'label' => 'Tổng số học viên thanh toán lũy kế',
                            'format' => 'raw',
                            'value' => function ($model) {
                                $countNumber = $model->getCommissionIncomeByUserId()->count();
                                return !empty($countNumber)?$countNumber:null;
                            },
                        ],
                        [
                            'label' => 'Hoa hồng (theo bộ lọc)',
                            'format' => 'currency',
                            'value' => function ($model) {
                                return $model->affiliate_total_amount;
                            },
                        ],
                        [
                            'label' => 'Hoa hồng lũy kế',
                            'format' => 'currency',
                            'value' => function ($model) {
                                return $model->getCommissionIncomeByAffiliateCategoryId()->sum('affiliate_commission_amount');
                            },
                        ],
                        [
                            'class' => 'yii\grid\ActionColumn',
                            'template' => '{view}',
                            'buttons' => [
                                'view' => function ($url, $model, $key) {
                                    $url = Url::toRoute(['category', 'id' => $model->affiliate_category_id]);

                                    $options = [
                                        'title' => 'Xem chi tiết',
                                        'data-pjax' => '0',
                                    ];
                                    return Html::a('<span class="glyphicon glyphicon-eye-open"></span> Xem chi tiết', $url, $options);
                                }
                            ]
                        ]

                    ]
                ]);
                ?>
            </div>
        </div>
    </div>
</div>