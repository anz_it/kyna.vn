<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\grid\GridView;
use kartik\export\ExportMenu;

$crudTitles = Yii::$app->params['crudTitles'];

$this->title = Yii::$app->controller->mainTitle;
$this->params['breadcrumbs'][] = $this->title;

$exportColumns = [
    'teacher_id',
    [
        'label' => 'Ngày thêm vào hệ thống',
        'format' => 'datetime',
        'value' => function ($model) {
            if ($model->teacher) {
                return $model->teacher->created_at;
            }
            return '';
        }
    ],
    [
        'label' => 'Họ và tên',
        'format' => 'raw',
        'value' => function ($model) {
            if ($model->teacher && $model->teacher->profile) {
                return $model->teacher->profile->name;
            }
            return '';
        }
    ],
    [
        'label' => 'Tổng học viên miễn phí',
        'format' => 'raw',
        'value' => function($model) use ($searchModel) {
            return $model->getTotalUserFree($searchModel->date_ranger);
        }
    ],
    [
        'label' => 'Tổng học viên trả phí',
        'format' => 'raw',
        'value' => function($model) use ($searchModel) {
            return $model->getTotalUserPaid($searchModel->date_ranger, true);
        }
    ],
    [
        'label' => 'Tổng học viên',
        'format' => 'raw',
        'value' => function($model) use ($searchModel) {
            return $model->getTotalUserPaid($searchModel->date_ranger);
        }
    ],
    [
        'label' => 'Thu nhập giới thiệu',
        'value' => function($model) use ($searchModel) {
            Yii::$app->formatter->thousandSeparator = '';
            Yii::$app->formatter->decimalSeparator = '';
            return Yii::$app->formatter->asDecimal($model->getTotalAffiliateCommissionAmount($searchModel->date_ranger), 0);
        }
    ],
    [
        'label' => 'Thu nhập giảng dạy',
        'attribute' => 'teacher_total_amount',
        'value' => function($model) use ($searchModel) {
            Yii::$app->formatter->thousandSeparator = '';
            Yii::$app->formatter->decimalSeparator = '';
            return Yii::$app->formatter->asDecimal($model->teacher_total_amount, 0);
        }
    ],
    [
        'label' => 'Tổng thu nhập',
        'value' => function($model) use ($searchModel) {
            Yii::$app->formatter->thousandSeparator = '';
            Yii::$app->formatter->decimalSeparator = '';
            return Yii::$app->formatter->asDecimal($model->getTotalAffiliateCommissionAmount($searchModel->date_ranger) + $model->teacher_total_amount, 0);
        }
    ],
    [
        'class' => 'yii\grid\ActionColumn',
        'template' => '{view}',
        'buttons' => [
            'view' => function ($url, $model, $key) {
                $url = Url::toRoute(['/commission/teacher-income-export/view', 'id' => $model->teacher_id]);

                $options = [
                    'title' => 'Xem chi tiết',
                    'data-pjax' => '0',
                ];
                return Html::a('<span class="glyphicon glyphicon-eye-open"></span> Xem chi tiết', $url, $options);
            }
        ]
    ],
];
$gridColumns = [
    'teacher_id',
    [
        'label' => 'Ngày thêm vào hệ thống',
        'format' => 'datetime',
        'value' => function ($model) {
            if ($model->teacher) {
                return $model->teacher->created_at;
            }
            return '';
        }
    ],
    [
        'label' => 'Họ và tên',
        'format' => 'raw',
        'value' => function ($model) {
            if ($model->teacher && $model->teacher->profile) {
                return $model->teacher->profile->name;
            }
            return '';
        }
    ],
    [
        'label' => 'Tổng học viên miễn phí',
        'format' => 'raw',
        'value' => function($model) use ($searchModel) {
            return $model->getTotalUserFree($searchModel->date_ranger);
        }
    ],
    [
        'label' => 'Tổng học viên trả phí',
        'format' => 'raw',
        'value' => function($model) use ($searchModel) {
            return $model->getTotalUserPaid($searchModel->date_ranger, true);
        }
    ],
    [
        'label' => 'Tổng học viên',
        'format' => 'raw',
        'value' => function($model) use ($searchModel) {
            return $model->getTotalUserPaid($searchModel->date_ranger);
        }
    ],
    [
        'label' => 'Thu nhập giới thiệu',
        'format' => 'currency',
        'value' => function($model) use ($searchModel) {
            return $model->getTotalAffiliateCommissionAmount($searchModel->date_ranger);
        }
    ],
    [
        'label' => 'Thu nhập giảng dạy',
        'format' => 'currency',
        'attribute' => 'teacher_total_amount'
    ],
    [
        'label' => 'Tổng thu nhập',
        'format' => 'currency',
        'value' => function($model) use ($searchModel) {
            return $model->getTotalAffiliateCommissionAmount($searchModel->date_ranger) + $model->teacher_total_amount;
        }
    ],
    [
        'class' => 'yii\grid\ActionColumn',
        'template' => '{view}',
        'buttons' => [
            'view' => function ($url, $model, $key) {
                $url = Url::toRoute(['/commission/teacher-income-export/view', 'id' => $model->teacher_id]);

                $options = [
                    'title' => 'Xem chi tiết',
                    'data-pjax' => '0',
                ];
                return Html::a('<span class="glyphicon glyphicon-eye-open"></span> Xem chi tiết', $url, $options);
            }
        ]
    ],
];
?>
<div class="row">
    <div class="col-xs-12">
        <div class="commission-income-index">
            <?= \yii\bootstrap\Nav::widget([
                'items' => $tabs,
                'options' => ['class' => 'nav-pills'],
            ]) ?>
            <nav class="navbar navbar-default">
                <?= $this->render('_search_teacher_income', ['model' => $searchModel]) ?>
                <?= ExportMenu::widget([
                    'dataProvider' => $dataProvider,
                    'columns' => $exportColumns,
                    'fontAwesome' => true,
                    'container' => [
                        'class' => 'btn-group navbar-btn navbar-btn-right navbar-right'
                    ],
                    'columnSelectorOptions'=>[
                        'label' => 'Export Cols',
                    ],
                    'dropdownOptions' => [
                        'label' => 'Export All',
                        'class' => 'btn btn-default'
                    ],
                    'showConfirmAlert' => false,
                    'showColumnSelector' => false,
                    'target' => ExportMenu::TARGET_BLANK,
                    'filename' => 'teacher_income_all_export_'.date('Y-m-d_H-i', time()),
                    'exportConfig' => [
                        ExportMenu::FORMAT_PDF => false,
                    ]
                ]);
                ?>
            </nav>
            <div class="box">
                <?=
                GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => false,
                    'columns' => $gridColumns
                ]);
                ?>
            </div>
        </div>
    </div>
</div>