<?php

use yii\helpers\Url;
use yii\helpers\Html;
use kartik\form\ActiveForm;
use yii\web\JsExpression;
use kartik\widgets\Select2;
use kartik\daterange\DateRangePicker;
use kyna\commission\models\AffiliateCategory;
use yii\helpers\ArrayHelper;
?>

<div class="commission-income-search">

    <?php $form = ActiveForm::begin([
        'action' => ['income'],
        'options' => ['class' => 'navbar-form navbar-left'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'affiliate_category_id', [
        'options' => ['class' => 'form-group', 'style' => 'width: 250px'],
    ])->widget(Select2::classname(), [
        'data' => ArrayHelper::map(AffiliateCategory::find()->where(['status' => AffiliateCategory::STATUS_ACTIVE])->all(), 'id', 'name'),
        'hideSearch' => true,
        'options' => ['placeholder' => '--Chọn loại affiliate--'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ])->label(false)->error(false) ?>

    <?php
    $initText = !empty($model->user_id) ? $model->user->username . ' - ' . $model->user->email : '';

    echo $form->field($model, 'user_id', [
        'options' => ['class' => 'form-group', 'style' => 'width: 300px'],
    ])->widget(Select2::classname(), [
        'initValueText' => $initText,
        'options' => ['placeholder' => '--Chọn user--'],
        'pluginOptions' => [
            'allowClear' => true,
            'minimumInputLength' => 3,
            'language' => [
                'errorLoading' => new JsExpression("function () { return 'Chờ nhận kết quả...'; }"),
            ],
            'ajax' => [
                'url' => Url::toRoute(['/user/api/search']),
                'dataType' => 'json',
                'data' => new JsExpression('function(params) { return {q:params.term}; }'),
            ],
            'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
            'templateResult' => new JsExpression('function(res) { return res.text; }'),
            'templateSelection' => new JsExpression('function (res) { return res.text; }'),
        ],
    ])->label(false)->error(false); ?>

    <?=
    $form->field($model, 'date_ranger', [
        'addon' => ['prepend' => ['content' => '<i class="glyphicon glyphicon-calendar"></i>']],
        'options' => ['class' => 'form-group'],
    ])->widget(DateRangePicker::classname(), [
        'useWithAddon' => true,
        'convertFormat' => true,
        'pluginOptions'=>[
            'timePicker' => true,
            'locale'=>[
                'format' => 'd/m/yy',
                'separator'=> " - ", // after change this, must update in controller
            ],
        ]
    ])->label(false)->error(false)
    ?>

    <div class="form-group">
        <?= Html::submitButton('<i class="ion-android-search"></i> Lọc', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
