<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Nav;

if (!Yii::$app->user->can('Teacher')) {
    $this->title = $searchModel->teacher->email;
    $this->params['breadcrumbs'][] = ['label' => Yii::$app->controller->mainTitle, 'url' => ['index']];
} else {
    $this->title = 'Thu nhập giảng dạy';
}
$this->params['breadcrumbs'][] = $this->title;

$crudTitles = Yii::$app->params['crudTitles'];
?>
<style>
    .dl-horizontal dt {
        width: 165px;
    }
</style>
<div class="commission-income-view">
    <?= Nav::widget([
        'items' => $tabs,
        'options' => ['class' => 'nav-pills'],
    ]) ?>
    <br>
    <div class="row">
        <div class="col-xs-12">
            <div class="commission-income-index">
                <nav class="navbar navbar-default">
                    <h4 class="navbar-text navbar-left" style="font-weight: bold">Thống kê học viên theo học</h4>
                    <?= $this->render('_filter', ['searchModel' => $searchModel]) ?>
                </nav>
                <div class="row">
                    <div class="col-xs-6">
                        <div class="box box-solid">
                            <div class="box-body">
                                <dl class="dl-horizontal">
                                    <dt>Số học viên theo bộ lọc</dt>
                                    <dd><?= $totalRegFilter ?> HV</dd>
                                    <dt>Tổng thu nhập theo bộ lọc</dt>
                                    <dd><?= Yii::$app->formatter->asCurrency($totalAmountFilter) ?></dd>
                              </dl>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="box box-solid">
                            <div class="box-body">
                                <dl class="dl-horizontal">
                                    <dt>Tổng số học viên</dt>
                                    <dd><?= $totalReg ?> HV</dd>
                                    <dt>Tổng thu nhập</dt>
                                    <dd><?= Yii::$app->formatter->asCurrency($totalAmount) ?></dd>
                              </dl>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box">
                    <?php
                    $roles = implode(",",
                           array_map(function ($p) { return $p->item_name; }, \kyna\user\models\AuthAssignment::find()->where(['user_id' => Yii::$app->user->id])->all())
                    );

                    echo GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => false,
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],
                            'created_time:datetime',
                            [
                                'label' => 'Học viên',
                                'value' => function ($model, $key) use ($roles){
                                    $user = $model->order->user;
                                    if ($user == null)
                                        return "";
                                    if (strpos($roles, "Teacher") !== false
                                        || $roles == 'RelationManager' || $roles == 'Relation'
                                    ) {
                                        return substr($user->email, 0, 5).'******'.substr($user->email, strlen($user->email) - 5, 5);
                                    }
                                    return $user->email;
                                }
                            ],
                            [
                                'attribute' => 'course_id',
                                'value' => function ($model) {return $model->course->name; },
                                'options' => ['class' => 'col-xs-3'],
                            ],
                            [
                                'attribute' => 'total_amount',
                                'format' => 'raw',
                                'label' => 'Thực nhận học phí',
                                'value' => function ($model) {
                                    return Html::a(Yii::$app->formatter->asCurrency($model->realIncome), '#', [
                                        'data-content' => $model->explainTotalText,
                                        'data-toggle' => 'popover',
                                    ]) . " &#63;";
                                },
                            ],
                            [
                                'attribute' => 'teacher_commission_percent',
                                'label' => 'Hoa hồng',
                                'value' => function ($model) {
                                    return $model->teacher_commission_percent . '%';
                                }
                            ],
                            'teacher_commission_amount:currency:Thu nhập giảng dạy',
                        ],
                    ]);
                    ?>
                </div>
            </div>
        </div>
    </div>

</div>
<?php
$js = <<< SCRIPT
/* To initialize BS3 tooltips set this below */
$(function () { 
    $("[data-toggle='popover']").popover({ trigger: "hover", html: true }); 
});;
SCRIPT;

$this->registerJs($js);
?>
