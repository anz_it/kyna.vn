<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\grid\GridView;

$crudTitles = Yii::$app->params['crudTitles'];

$this->title = Yii::$app->controller->mainTitle;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-xs-12">
        <div class="commission-income-index">
            <nav class="navbar navbar-default">
                <?= $this->render('_search', ['model' => $searchModel]) ?>
            </nav>
            <div class="box">
                <?=
                GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => false,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],
                        [
                            'attribute' => 'teacher_id',
                            'format' => 'raw',
                            'value' => function ($model) {
                                if (empty($model->teacher)) {
                                    return null;
                                }
                                $str = "Username: {$model->teacher->username}";
                                if (!empty($model->teacher->profile->name)) {
                                    $str.= "<br>Họ tên: {$model->teacher->profile->name}";
                                }
                                $str .= "<br>Email: {$model->teacher->email}";
                                
                                return $str;
                            },
                        ],
                        'teacher_total_amount:currency',
                        [
                            'class' => 'yii\grid\ActionColumn',
                            'template' => '{view}',
                            'buttons' => [
                                'view' => function ($url, $model, $key) {
                                    $url = Url::toRoute(['view', 'id' => $model->teacher_id]);
                                
                                    $options = [
                                        'title' => 'Xem chi tiết',
                                        'data-pjax' => '0',
                                    ];
                                    return Html::a('<span class="glyphicon glyphicon-eye-open"></span> Xem chi tiết', $url, $options);
                                }
                            ]
                        ],
                    ],
                ]);
                ?>
            </div>
        </div>
    </div>
</div>