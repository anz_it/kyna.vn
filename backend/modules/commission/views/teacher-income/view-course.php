<?php

use yii\bootstrap\Nav;
use yii\helpers\Html;

$this->title = 'Danh sách khóa học đang giảng dạy';
$this->params['breadcrumbs'][] = $this->title;

$frontendUrl = Yii::$app->params['baseUrl'];
?>

<div class="commission-income-view">
    <?= Nav::widget([
        'items' => $tabs,
        'options' => ['class' => 'nav-pills'],
    ]) ?>
    
    <br>
    
    <div class="row">
        <?php foreach ($courses as $course) : ?>
            <?php
            $learningUrl = $frontendUrl . $course->learningUrl;
            ?>
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail" style="height: 300px;">
                    <a href="<?= $learningUrl ?>" title="<?= $course->name ?>">
                        <?= Html::img($course->image_url, ['style' => 'height: 200px;']) ?>
                    </a>
                    <div class="caption">
                        <h3><a href="<?= $learningUrl ?>" title="<?= $course->name ?>"><?= $course->name ?></a></h3>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
</div>
