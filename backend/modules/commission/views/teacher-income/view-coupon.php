<?php

use yii\grid\GridView;
use yii\bootstrap\Nav;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

use kyna\promotion\models\Promotion;

$crudTitles = Yii::$app->params['crudTitles'];
$crudButtonIcons = Yii::$app->params['crudButtonIcons'];

$formatter = Yii::$app->formatter;

$this->title = 'Các Coupon đang sử dụng';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="commission-income-view">
    <?= Nav::widget([
        'items' => $tabs,
        'options' => ['class' => 'nav-pills'],
    ]) ?>
    <br>
    
    <?= $this->render('_coupon_form', ['model' => $model]) ?>
    
    <h1>Danh sách coupon</h1>
    <div class="box">
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                'code',
                [
                    'attribute' => 'value',
                    'format'    => 'raw',
                    'label' => 'Phần trăm giảm giá',
                    'value'     => function($model) use ($formatter){
                        if($model->discount_type == Promotion::TYPE_PERCENTAGE){
                            return "<b>" . $model->value . ' % </b>';
                        }else{
                            return "<b> " . $formatter->asCurrency($model->value) . "</b>";
                        }
                    }
                ],
                'number_usage:integer:Số lần sử dụng tối đa',
                'user_number_usage:integer:Số lần sử dụng của user',
                'current_number_usage:integer:Số lần sử dụng hiện tại',
                'start_date:datetime',
                'end_date:datetime',
                [
                    'label' => 'Các khóa học áp dụng',
                    'value' => function ($model) {
                        return implode(', ', ArrayHelper::map($model->promoCourses, 'id', 'course.name'));
                    }
                ],
                [
                    'attribute' => 'status',
                    'format' => 'html',
                    'value' => function ($model) {
                        return Html::a($model->statusHtml);
                    },
                    'filter' => Html::activeDropDownList($searchModel, 'status', Promotion::listStatus(false), ['class' => 'form-control', 'prompt' => $crudTitles['prompt']]),
                ],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{delete}' ,
                    'visibleButtons' => [
                        'delete' => Yii::$app->user->can('Coupon.Delete') || Yii::$app->user->can('Teacher'),
                    ],
                ],
            ],
        ]); ?>
    </div>
</div>
