<?php

namespace app\modules\commission\controllers;

use Yii;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;

use kyna\commission\models\AffiliateCategoryCourse;
use kyna\commission\models\search\AffiliateCategoryCourseSearch;
use kyna\commission\models\AffiliateCategory;
use app\components\controllers\Controller;

/**
 * CourseController implements the CRUD actions for AffiliateCategoryCourse model.
 */
class CourseController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index'],
                        'matchCallback' => function () {
                            return Yii::$app->user->can('Affiliate.Category.Course.View');
                        },
                    ],
                    [
                        'allow' => true,
                        'actions' => ['update', 'change-status'],
                        'matchCallback' => function () {
                            return Yii::$app->user->can('Affiliate.Category.Course.Update');
                        },
                    ],
                    [
                        'allow' => true,
                        'actions' => ['delete'],
                        'matchCallback' => function () {
                            return Yii::$app->user->can('Affiliate.Category.Course.Delete');
                        },
                    ],
                ],
            ],
        ]);
    }

    /**
     * Lists all AffiliateCategoryCourse models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new AffiliateCategoryCourseSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $affCategories = ArrayHelper::map(AffiliateCategory::find()->all(), 'id', 'name');
        $newModel = new AffiliateCategoryCourse();

        if ($newModel->load(Yii::$app->request->post()) && $newModel->save()) {
            Yii::$app->session->setFlash('success', 'Thêm thành công.');

            $newModel = new AffiliateCategoryCourse();
        }

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'affCategories' => $affCategories,
            'newModel' => $newModel
        ]);
    }

    /**
     * Updates an existing AffiliateCategoryCourse model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $affCategories = ArrayHelper::map(AffiliateCategory::find()->all(), 'id', 'name');

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', 'Cập nhật thành công.');
        }

        return $this->render('update', [
            'model' => $model,
            'affCategories' => $affCategories,
        ]);
    }

    /**
     * Deletes an existing AffiliateCategoryCourse model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the AffiliateCategoryCourse model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return AffiliateCategoryCourse the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = AffiliateCategoryCourse::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
