<?php

namespace app\modules\commission\controllers;

use app\models\OrderAffiliateView;
use app\modules\commission\models\AffiliateUser;
use kyna\commission\models\search\KidAffiliateIncomeSearch;
use Yii;
use yii\helpers\Url;
use yii\web\Response;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\data\Pagination;
use yii\widgets\ActiveForm;

use kyna\commission\models\CommissionIncome;
use kyna\commission\models\search\CommissionIncomeSearch;
use kyna\course\models\Course;
use kyna\promotion\models\PromotionSearch;
use kyna\commission\models\AffiliatePaymentLink;
use kyna\commission\models\search\AffiliatePaymentLinkSearch;

use app\components\controllers\Controller;
use app\modules\commission\models\search\OrderSearch;
use app\components\ActiveDataProvider;
use app\modules\commission\models\Order;

/**
 * IncomeController implements the CRUD actions for CommissionIncome model.
 */
class AffiliateIncomeController extends Controller
{
    public $viewPath = '@backend/modules/commission/views/affiliate-income/';

    const TAB_INCOME = 'view';
    const TAB_INCOME_FORKIDS = 'view-forkids';
    const TAB_ORDER = 'view-order';
    const TAB_PROMOTION_CODE = 'view-code';
    const TAB_PAYMENT_LINK = 'view-payment-link';

    public $mainTitle = 'Thu nhập Giới thiệu';

    public $managerId = null;

    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index'],
                        'matchCallback' => function () {
                            return Yii::$app->user->can('Affiliate.Dashboard.View') || Yii::$app->user->isAffiliateManager;
                        },
                    ],
                    [
                        'allow' => true,
                        'actions' => ['view', 'generate-link', 'view-forkids'],
                        'matchCallback' => function () {
                            return Yii::$app->user->can('Affiliate.Dashboard.View') || Yii::$app->user->can('Teacher') || Yii::$app->user->isAffiliate;
                        },
                    ],
                    [
                        'allow' => true,
                        'actions' => ['view-order', 'view-code', 'view-payment-link', 'submit-payment-link', 'change-status', 'delete'],
                        'matchCallback' => function () {
                            return Yii::$app->user->can('Affiliate.Dashboard.View') || Yii::$app->user->affiliateGroup === Yii::$app->params['id_aff_group_third_party'];
                        },
                    ],
                    [
                        'allow' => true,
                        'actions' => ['manager', 'view', 'view-order', 'view-code', 'view-payment-link', 'submit-payment-link', 'generate-link'],
                        'matchCallback' => function () {
                            return Yii::$app->user->isAffiliateManager;
                        },
                    ],
                ],
            ],
        ]);
    }

    public function actionIndex()
    {
        $searchModel = new CommissionIncomeSearch();
        $view = 'index';
        if (!empty($this->managerId)) {
            $searchModel->affiliate_manager_id = $this->managerId;
            $view = 'manager';
        }
        $dataProvider = $searchModel->affiliateSearch(Yii::$app->request->queryParams);

        return $this->render($this->viewPath . $view, [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionView($id = null)
    {
        if (empty($id) && Yii::$app->user->isAffiliate) {
            $id = Yii::$app->user->id;
        }
        if (empty($id)) {
            throw new NotFoundHttpException('Invalid Parameter!');
        }
        
        $searchModel = new CommissionIncomeSearch();
        $searchModel->user_id = $id;
        if (!empty($this->managerId)) {
            $searchModel->affiliate_manager_id = $this->managerId;
        }
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        
        list($totalReg, $totalPaid, $totalRegFilter, $totalPaidFilter, $totalAmount, $totalAmountFilter, $totalChildAmount, $totalManagerAmount) = $this->_extractSummaryVarriables($id, $searchModel->course_id, $searchModel->date_ranger);
        
        return $this->render($this->viewPath . 'view', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'totalReg' => $totalReg,
            'totalPaid' => $totalPaid,
            'totalRegFilter' => $totalRegFilter,
            'totalPaidFilter' => $totalPaidFilter,
            'totalAmount' => $totalAmount,
            'totalAmountFilter' => $totalAmountFilter,
            'totalChildAmount' => $totalChildAmount,
            'totalManagerAmount' => $totalManagerAmount
        ]);
    }
    public function actionViewForkids($id = null)
    {
        if (empty($id) && Yii::$app->user->isAffiliate) {
            $id = Yii::$app->user->id;
        }
        if (empty($id)) {
            throw new NotFoundHttpException('Invalid Parameter!');
        }

        $searchModel = new KidAffiliateIncomeSearch();
        $searchModel->affiliate_id = $id;
        /*if (!empty($this->managerId)) {
            $searchModel->affiliate_manager_id = $this->managerId;
        }*/
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        //list($totalReg, $totalPaid, $totalRegFilter, $totalPaidFilter, $totalAmount, $totalAmountFilter, $totalChildAmount, $totalManagerAmount) = $this->_extractSummaryVarriables($id, $searchModel->course_id, $searchModel->date_ranger);

        return $this->render($this->viewPath . 'view-forkids', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,

        ]);
    }
    /**
     * @desc show list promotion codes for Distribution Partners
     * @param integer $id
     * @return view
     * @throws NotFoundHttpException
     */
    public function actionViewCode($id = null)
    {
        if (empty($id) && Yii::$app->user->affiliateGroup == Yii::$app->params['id_aff_group_third_party']) {
            $id = Yii::$app->user->id;
        }
        if (empty($id)) {
            throw new NotFoundHttpException();
        }
        
        $searchModel = new PromotionSearch();
        $searchModel->partner_id = $id;
        $searchModel->type = null;
        
        return $this->render('view-code', [
            'tabs' => $this->getViewTabs(),
            'searchModel' => $searchModel,
            'dataProvider' => $searchModel->search(Yii::$app->request->queryParams),
        ]);
    }
    
    /**
     * @desc show list order for Distribution Partners
     * @param integer $id
     * @return view
     * @throws NotFoundHttpException
     */
    public function actionViewOrder($id = null)
    {
        if (empty($id) && Yii::$app->user->affiliateGroup == Yii::$app->params['id_aff_group_third_party']) {
            $id = Yii::$app->user->id;
        }
        if (empty($id)) {
            throw new NotFoundHttpException();
        }

        ini_set('memory_limit', '-1');
        
        $searchModel = new OrderSearch();
        $searchModel->affiliate_id = $id;
        
        $sql = $searchModel->search(Yii::$app->request->queryParams);

        $pagination = new Pagination();
        $pagination->totalCount = Order::findBySql($sql)->count();;

        $query = Order::findBySql($sql . " LIMIT {$pagination->limit} OFFSET {$pagination->offset}");

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'attributes' => ['order_date', 'id'],
                'defaultOrder' => [
                    'order_date' => SORT_DESC,
                    'id' => SORT_DESC
                ]
            ],
            'pagination' => $pagination
        ]);

        $totalIncome = $this->calculateTotalIncome($sql, $pagination->totalCount);

        return $this->render('view-order', [
            'tabs' => $this->getViewTabs(),
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'totalIncome' => $totalIncome
        ]);
    }

    public function actionViewPaymentLink($id = null)
    {
        if (empty($id) && Yii::$app->user->affiliateGroup == Yii::$app->params['id_aff_group_third_party']) {
            $id = Yii::$app->user->id;
        }
        if (empty($id)) {
            throw new NotFoundHttpException();
        }

        $searchModel = new AffiliatePaymentLinkSearch();
        $searchModel->affiliate_id = $id;

        if ($linkId = Yii::$app->request->get('link_id')) {
            $model = AffiliatePaymentLink::findOne($linkId);
        } else {
            $model = new AffiliatePaymentLink();
            $model->affiliate_id = $id;
        }

        return $this->render('view-payment-link', [
            'tabs' => $this->getViewTabs(),
            'searchModel' => $searchModel,
            'dataProvider' => $searchModel->search(Yii::$app->request->queryParams),
            'model' => $model
        ]);
    }

    public function actionSubmitPaymentLink($id = null)
    {
        if ($id !== null) {
            $model = AffiliatePaymentLink::findOne($id);
        } else {
            $model = new AffiliatePaymentLink();
        }

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }
        if ($model->load(Yii::$app->request->post())) {
            $model->course_ids = json_encode($model->course_ids);
            if ($model->save()) {
                Yii::$app->session->setFlash('success', 'Thêm link thanh toán thành công.');

                $this->redirect(['view-payment-link', 'id' => Yii::$app->request->get('affiliate_id')]);
            }
        }
    }

    public function calculateTotalIncome($sql, $totalCount)
    {
        $totalIncome = 0;

        $offset = 0;
        $limit = 100;
        $pageCount = ceil($totalCount / $limit);

        for ($i = 1; $i <= $pageCount; $i++) {
            $orders = Order::findBySql($sql . " LIMIT {$limit} OFFSET {$offset}")->all();
            foreach ($orders as $order) {
                $details = $order->details;
                foreach ($details as $detail) {
                    $totalIncome += ($detail->realIncome * $detail->affiliateCommissionPercent / 100);
                }
            }
            $offset += $limit;
        }

        return $totalIncome;
    }

    /**
     * Generate affiliate link with by course and affiliate id
     * @return type
     * @throws NotFoundHttpException
     */
    public function actionGenerateLink()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        
        $id = Yii::$app->request->get('id');
        $affiliate_id = Yii::$app->request->get('affiliate_id');
        
        $course = Course::findOne($id);
        if (is_null($course)) {
            throw new NotFoundHttpException();
        }
        
        $frontendUrl = Yii::$app->params['baseUrl'];
        
        return $frontendUrl . Url::toRoute(['/course/default/view', 'slug' => $course->slug, 'affiliate_id' => $affiliate_id]);
    }
    
    private function _extractSummaryVarriables($affiliate_id, $course_id = 0, $date_ranger = '')
    {
        // summary
        $orderQuery = Order::find();
        $orderQuery->alias('t');

        $incomeTable = CommissionIncome::tableName();
        $orderQuery->leftJoin($incomeTable, $incomeTable . '.order_id = t.id');
        $orderQuery->andWhere([$incomeTable . '.user_id' => $affiliate_id]);
        $orderQuery->andWhere(['!=', $incomeTable . '.course_id', 10]);

        if (!empty($this->managerId)) {
            $orderQuery->andWhere([$incomeTable . '.affiliate_manager_id' => $this->managerId]);
        }

        $totalReg = $orderQuery->count('t.id');
        $totalAmount = $orderQuery->sum($incomeTable . '.affiliate_commission_amount');

        $paidQuery = clone $orderQuery;
        $paidQuery->andWhere(['is not', $incomeTable . '.id', NULL]);
        $paidQuery->andWhere(['>', $incomeTable . '.affiliate_commission_amount', '0']);

        $totalPaid = $paidQuery->count('t.id');

        if (!empty($course_id)) {
            // filter by course (if not empty)
            $orderQuery->andFilterWhere([$incomeTable . '.course_id' => $course_id]);
            
            $paidQuery->andFilterWhere([$incomeTable . '.course_id' => $course_id]);
        }

        if (!empty($date_ranger)) {
            $dateRange = explode(' - ', $date_ranger);
            $beginDate = date_create_from_format('d/m/Y H:i:s', $dateRange[0] . ' 00:00:00');
            $endDate = date_create_from_format('d/m/Y H:i:s', $dateRange[1] . ' 23:59:59');

            $orderQuery->andWhere(['>=', 'order_date', $beginDate->getTimestamp()]);
            $orderQuery->andWhere(['<=', 'order_date', $endDate->getTimestamp()]);
            
            $paidQuery->andWhere(['>=', $incomeTable . '.created_time', $beginDate->getTimestamp()]);
            $paidQuery->andWhere(['<=', $incomeTable . '.created_time', $endDate->getTimestamp()]);
        }

        $totalRegFilter = $orderQuery->count('t.id');
        $totalPaidFilter = $paidQuery->count('t.id');
        $totalAmountFilter = $paidQuery->sum($incomeTable . '.affiliate_commission_amount');

        $affiliate = AffiliateUser::findOne(['user_id' => $affiliate_id]);
        $totalChildAmount = $affiliate->getTotalChildAmount($course_id, $date_ranger);
        $totalManagerAmount = $affiliate->getTotalManagerAmount($course_id, $date_ranger);

        return [
            $totalReg,
            $totalPaid,
            $totalRegFilter,
            $totalPaidFilter,
            $totalAmount,
            $totalAmountFilter,
            $totalChildAmount,
            $totalManagerAmount
        ];
    }
    
    public function getViewTabs()
    {
        $webUser = Yii::$app->user;
        $id = Yii::$app->request->get('id');
        $group = null;
        
        $urlParams = [];
        
        if (!empty($id)) {
            $urlParams = ['id' => $id];
        } else if ($webUser->isAffiliate) {
            $id = $webUser->id;
            $group = $webUser->affiliateGroup;
        }
        $idOfAffGroupThirdParty = Yii::$app->params['id_aff_group_third_party'];
        
        $tabs = [
            [
                'label' => 'Tổng quan',
                'url' => ['/commission/affiliate-income/' . self::TAB_INCOME] + $urlParams,
                'active' => $this->action->id === self::TAB_INCOME,
            ],
            [
                'label' => 'Kyna ForKids',
                'url' => ['/commission/affiliate-income/' . self::TAB_INCOME_FORKIDS] + $urlParams,
                'active' => $this->action->id === self::TAB_INCOME_FORKIDS,
                'visible' => Yii::$app->params['showKidCommission']
            ],
            [
                'label' => 'Mã giảm giá',
                'url' => ['/commission/affiliate-income/' . self::TAB_PROMOTION_CODE] + $urlParams,
                'active' => $this->action->id === self::TAB_PROMOTION_CODE,
                'visible' => $webUser->can('Affiliate.Dashboard.View') || $group === $idOfAffGroupThirdParty || Yii::$app->user->isAffiliateManager
            ],
            [
                'label' => 'Đơn đăng ký',
                'url' => ['/commission/affiliate-income/' . self::TAB_ORDER] + $urlParams,
                'active' => $this->action->id === self::TAB_ORDER,
                'visible' => $webUser->can('Affiliate.Dashboard.View') || $group === $idOfAffGroupThirdParty || Yii::$app->user->isAffiliateManager
            ],
            [
                'label' => 'Link thanh toán',
                'url' => ['/commission/affiliate-income/' . self::TAB_PAYMENT_LINK] + $urlParams,
                'active' => $this->action->id === self::TAB_PAYMENT_LINK,
                'visible' => $webUser->can('Affiliate.Dashboard.View') || $group === $idOfAffGroupThirdParty || Yii::$app->user->isAffiliateManager
            ],
        ];
        
        return $tabs;
    }

    public function findModel($id)
    {
        $model = AffiliatePaymentLink::findOne($id);

        if ($model === null) {
            throw new NotFoundHttpException();
        }

        return $model;
    }
}
