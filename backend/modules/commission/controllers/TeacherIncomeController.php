<?php

namespace app\modules\commission\controllers;

use kyna\promotion\models\PromotionSearch;
use Yii;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

use kyna\commission\models\CommissionIncome;
use kyna\commission\models\search\CommissionIncomeSearch;
use kyna\course\models\Course;
use kyna\promotion\models\Promotion;
use app\components\controllers\Controller;


/**
 * IncomeController implements the CRUD actions for CommissionIncome model.
 */
class TeacherIncomeController extends Controller
{
    
    const TAB_INCOME = 'view';
    const TAB_COURSE = 'view-course';
    const TAB_COUPON = 'view-coupon';
    
    public $mainTitle = 'Thu nhập Giảng dạy';
    
    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index'],
                        'matchCallback' => function () {
                            return Yii::$app->user->can('Teacher.Income.View');
                        },
                    ],
                    [
                        'allow' => true,
                        'actions' => ['view', 'view-course', 'view-coupon'],
                        'matchCallback' => function () {
                            return Yii::$app->user->can('Teacher.Income.View') || Yii::$app->user->can('Teacher');
                        },
                    ],
                    [
                        'allow' => true,
                        'actions' => ['delete'],
                        'matchCallback' => function () {
                            return Yii::$app->user->can('Coupon.Delete') || Yii::$app->user->can('Teacher');
                        },
                    ],
                ],
            ],
        ]);
    }
    
    public function actionIndex()
    {
        $searchModel = new CommissionIncomeSearch();
        $isSearchAllUserFollow = false;
        if((Yii::$app->user->can('Teacher.Income.View')
            && Yii::$app->user->can('RelationManager')) ||
            Yii::$app->user->can('Module.Commission')
        )
        {
            $isSearchAllUserFollow = true;
        }
        $params = Yii::$app->request->queryParams;
        $params['user_id'] = Yii::$app->getUser()->getId();
        $dataProvider = $searchModel->teacherSearch($params,$isSearchAllUserFollow);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionView($id = null)
    {
        $searchModel = new CommissionIncomeSearch();
        if (Yii::$app->user->can('Teacher') && false) {
           $id = Yii::$app->user->id;
        }
        if (empty($id)) {
            throw new NotFoundHttpException();
        }
        
        $searchModel->teacher_id = $id;


        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        
        list($totalReg, $totalAmount, $totalRegFilter, $totalAmountFilter) = $this->extractSummaryVarriables($id, $searchModel->course_id, $searchModel->date_ranger);

       return $this->render('view', [
            'tabs' => $this->getViewTabs(),
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'totalReg' => $totalReg,
            'totalRegFilter' => $totalRegFilter,
            'totalAmount' => $totalAmount,
            'totalAmountFilter' => $totalAmountFilter,
        ]);
    }
    
    public function actionViewCourse($id = null)
    {
        if (Yii::$app->user->can('Teacher')) {
            $id = Yii::$app->user->id;
        }
        if (empty($id)) {
            throw new NotFoundHttpException();
        }
        $courses = Course::find()->andWhere(['teacher_id' => $id])->all();
        
        return $this->render('view-course', [
            'tabs' => $this->getViewTabs(),
            'courses' => $courses,
        ]);
    }
    
    public function actionViewCoupon($id = null)
    {
       if (Yii::$app->user->can('Teacher')) {
            $id = Yii::$app->user->id;
        }
        if (empty($id)) {
            throw new NotFoundHttpException();
        }
        
        $searchModel = new PromotionSearch();
        $searchModel->issued_person = $id;
        
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        
        $model = $this->newModel($id);
        
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if (!$model->canTeacherCreate()) {
                Yii::$app->session->setFlash('warning', 'Không thể tạo nhiều hơn 100 coupon trong tháng.');
            } else {
                if ($model->save()) {
                    Yii::$app->session->setFlash('success', 'Thêm coupon thành công.');
                    $model = $this->newModel($id);
                }
            }
        }




        return $this->render('view-coupon', [
            'tabs' => $this->getViewTabs(),
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'model' => $model
        ]);
    }
    
    private function getViewTabs()
    {
        $id = Yii::$app->request->get('id');
        
        return [
            [
                'label' => 'Thống kê thu nhập',
                'url' => ['/commission/teacher-income/' . self::TAB_INCOME, 'id' => $id],
                'active' => $this->action->id === self::TAB_INCOME,
            ],
            [
                'label' => 'Các khóa học đang giảng dạy',
                'url' => ['/commission/teacher-income/' . self::TAB_COURSE, 'id' => $id],
                'active' => $this->action->id === self::TAB_COURSE,
            ],
            [
                'label' => 'Danh sách coupon',
                'url' => ['/commission/teacher-income/' . self::TAB_COUPON, 'id' => $id],
                'active' => $this->action->id === self::TAB_COUPON,
            ],
        ];
    }


    private function extractSummaryVarriables($teacher_id, $course_id = 0, $date_ranger = '')
    {
        $query = CommissionIncome::find();
        $query->andWhere(['teacher_id' => $teacher_id]);
        $query->andWhere(['!=', 'teacher_commission_percent', 0]);
        $query->andWhere(['!=', 'course_id', 10]);
        
        $totalReg = $query->count('id');
        $totalAmount = $query->sum('teacher_commission_amount');
        
        $query->andFilterWhere(['course_id' => $course_id]);
        
        if (!empty($date_ranger)) {
            $dateRange = explode(' - ', $date_ranger);
            $beginDate = date_create_from_format('d/m/Y H:i:s', $dateRange[0] . ' 00:00:00');
            $endDate = date_create_from_format('d/m/Y H:i:s', $dateRange[1] . ' 23:59:59');

            $query->andWhere(['>=', 'created_time', $beginDate->getTimestamp()]);
            $query->andWhere(['<=', 'created_time', $endDate->getTimestamp()]);
        }

        $totalRegFilter = $query->count('id');
        $totalAmountFilter = $query->sum('teacher_commission_amount');
        
        return [
            $totalReg,
            $totalAmount,
            $totalRegFilter,
            $totalAmountFilter,
        ];
    }
    
    public function newModel($teacher_id)
    {
        $model = new Promotion();
        $model->setScenario('backend-teacher');
        $model->issued_person = $teacher_id;
        $model->created_by = Yii::$app->user->id;
        $model->type = Promotion::KIND_COURSE_APPLY;
        $model->discount_type = Promotion::TYPE_PERCENTAGE;
        $model->current_number_usage = 0;
        $model->status = Promotion::BOOL_YES;
        return $model;
    }
    
    public function actionDelete($id)
    {
        $model = Promotion::findOne($id);
        if (is_null($model)) {
            throw NotFoundHttpException();
        }
        
        $model->delete();
        Yii::$app->session->setFlash('warning', 'Đã xóa');

        $url = ['view-coupon', 'id' => $model->issued_person];
        if (!Yii::$app->user->can('Teacher.Income.View')) {
            unset($url['id']);
        }

        return $this->redirect($url);
    }

}
