<?php

namespace app\modules\commission\controllers;

use Yii;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

use kyna\commission\models\CommissionCalculation;
use kyna\commission\models\search\CommissionCalculationSearch;
use app\components\controllers\Controller;

/**
 * CalculationController implements the CRUD actions for CommissionCalculation model.
 */
class CalculationController extends Controller
{

    public $mainTitle = 'Affiliate Calculations';
    
    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'view'],
                        'matchCallback' => function () {
                            return Yii::$app->user->can('Commission.Calculation.View');
                        },
                    ],
                    [
                        'allow' => true,
                        'actions' => ['create'],
                        'matchCallback' => function () {
                            return Yii::$app->user->can('Commission.Calculation.Create');
                        },
                    ],
                    [
                        'allow' => true,
                        'actions' => ['update', 'change-status'],
                        'matchCallback' => function () {
                            return Yii::$app->user->can('Commission.Calculation.Update');
                        },
                    ],
                    [
                        'allow' => true,
                        'actions' => ['delete'],
                        'matchCallback' => function () {
                            return Yii::$app->user->can('Commission.Calculation.Delete');
                        },
                    ],
                ],
            ],
        ]);
    }

    /**
     * Lists all CommissionCalculation models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CommissionCalculationSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new CommissionCalculation model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new CommissionCalculation();
        $model->status = CommissionCalculation::STATUS_ACTIVE;

        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                Yii::$app->session->setFlash('success', 'Thêm thành công');
                return $this->redirect(['index']);
            }
        } 
        
        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing CommissionCalculation model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        if (!empty($model->start_date)) {
            $model->start_date = Yii::$app->formatter->asDatetime($model->start_date);
        }
        if (!empty($model->end_date)) {
            $model->end_date = Yii::$app->formatter->asDatetime($model->end_date);
        }
        
        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                Yii::$app->session->setFlash('success', 'Cập nhật thành công');
                
                return $this->redirect(['index']);
            }
        } 
        
        return $this->render('update', [
            'model' => $model,
        ]);
    }

    protected function findModel($id)
    {
        if (($model = CommissionCalculation::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
