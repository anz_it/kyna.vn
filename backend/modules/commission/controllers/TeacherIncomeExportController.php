<?php

namespace app\modules\commission\controllers;

use app\modules\commission\models\TeacherIncomeExportSearch;
use kyna\commission\models\CommissionIncome;
use Yii;
use app\components\controllers\Controller;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * UserController implements the CRUD actions for AffiliateUser model.
 */
class TeacherIncomeExportController extends Controller
{

    public $mainTitle = 'Export Teacher Income';
    
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    public function actionView($id)
    {
        $searchModel = new TeacherIncomeExportSearch();
        $searchModel->teacher_id = $id;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        list($totalReg, $totalAmount, $totalRegFilter, $totalAmountFilter) = $this->extractSummaryVarriables($id, $searchModel->course_id, $searchModel->date_ranger);

        if (Yii::$app->request->isAjax && Yii::$app->request->post('type', null) == 'export') {
            // init pagination for raw SQL
            $sql = $dataProvider->query->createCommand()->getRawSql();
            $sql = str_replace('`', '', $sql);
            $email = empty(Yii::$app->request->post('email'))?Yii::$app->user->identity->email:Yii::$app->request->post('email');
            // execute command in console
            $rootPath = \Yii::getAlias('@root');
            $exportLog = \Yii::getAlias('@console/runtime/export_teacher_student.log');
            $exportErrorLog = \Yii::getAlias('@console/runtime/export_teacher_student-error.log');
            // check log file exist
            self::_checkExistFile($exportLog);
            self::_checkExistFile($exportErrorLog);
            $command = "php {$rootPath}/yii export/run teacher-income \"{$sql}\" {$email}" . " > {$exportLog} 2>>{$exportErrorLog} &";
            exec($command);
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'result' => true,
            ];
        }

        return $this->render('view', [
            'tabs' => $this->loadTabs($id),
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'totalReg' => $totalReg,
            'totalRegFilter' => $totalRegFilter,
            'totalAmount' => $totalAmount,
            'totalAmountFilter' => $totalAmountFilter,
        ]);
    }

    public function actionViewCourse($id)
    {
        $searchModel = new TeacherIncomeExportSearch();
        $searchModel->teacher_id = $id;
        $dataProvider = $searchModel->searchCourse(Yii::$app->request->queryParams);

        return $this->render('view-course', [
            'tabs' => $this->loadTabs($id),
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
//        $courses = Course::find()->andWhere(['teacher_id' => $id])->all();
    }


    private function loadTabs($id)
    {
        $tabsItems = [
            [
                'label' => 'Khóa học và mức hoa hồng',
                'url' => Url::toRoute(['/commission/teacher-income-export/view-course', 'id' => $id]),
                'active' => $this->action->id == 'view-course',
                'encode' => false,
            ],
            [
                'label' => 'Danh sách học viên',
                'url' => Url::toRoute(['/commission/teacher-income-export/view', 'id' => $id]),
                'active' => $this->action->id == 'view',
                'encode' => false,
            ],
            [
                'label' => 'Danh sách học viên hoàn tiền',
                'url' => Url::toRoute(['/commission/teacher-income-export/refund', 'id' => $id]),
                'active' => $this->action->id == 'refund',
                'visible' => false,
                'encode' => false,
            ],
        ];

        return $tabsItems;
    }

    private function extractSummaryVarriables($teacher_id, $course_id = 0, $date_ranger = '')
    {
        $query = CommissionIncome::find();
        $query->andWhere(['teacher_id' => $teacher_id]);

        $totalReg = $query->count('id');
        $totalAmount = $query->sum('teacher_commission_amount');

        $query->andFilterWhere(['course_id' => $course_id]);

        if (!empty($date_ranger)) {
            $dateRange = explode(' - ', $date_ranger);
            $beginDate = date_create_from_format('d/m/Y H:i', $dateRange[0] . ' 00:00');
            $endDate = date_create_from_format('d/m/Y H:i', $dateRange[1] . ' 00:00');

            $query->andWhere(['>=', 'created_time', $beginDate->getTimestamp()]);
            $query->andWhere(['<=', 'created_time', $endDate->getTimestamp()]);
        }

        $totalRegFilter = $query->count('id');
        $totalAmountFilter = $query->sum('teacher_commission_amount');

        return [
            $totalReg,
            $totalAmount,
            $totalRegFilter,
            $totalAmountFilter,
        ];
    }

    private function _checkExistFile ($file) {
        clearstatcache();
        if(!file_exists($file))
        {
            touch($file);
        }
    }
}
