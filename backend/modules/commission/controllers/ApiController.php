<?php

namespace app\modules\commission\controllers;

use app\modules\commission\models\AffiliateUser;
use kyna\user\models\UserTelesale;
use Yii;
use yii\web\Response;
use yii\db\Query;
use kyna\user\models\Profile;
use kyna\user\models\User;
use kyna\user\models\AuthAssignment;
use yii\db\Expression;

class ApiController extends \app\components\controllers\Controller
{
    
    public function actionSearchAffiliate($q = null, $key = 'affiliate', $managerId = null, $isManager = null)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        
        $out = ['results' => []];
        if (!is_null($q)) {
            $query = new Query();
            
            $userTable = User::tableName();
            $profileTbl = Profile::tableName();

            if ($key == 'user') {
                $select = "$userTable.id";
            } else {
                $select = "t.id";
            }
            $query->select([
                $select,
                new Expression("CONCAT_WS(' - ', $profileTbl.name, $userTable.email) AS text"),
                "$userTable.email AS name"
            ])->from(AffiliateUser::tableName() . ' t')
                ->join('JOIN', $userTable, $userTable . '.id = t.user_id')
                ->join('LEFT JOIN', $profileTbl, "{$profileTbl}.user_id = t.user_id")
                ->where("($userTable.email LIKE :q OR $userTable.username LIKE :q OR $profileTbl.name LIKE :q)", [':q' => "%{$q}%"]);

            if (!empty($isManager)) {
                $query->andWhere(['t.is_manager' => AffiliateUser::STATUS_ACTIVE]);
            }

            if (!empty($managerId)) {
                $query->andWhere(['t.manager_id' => $managerId]);
            }

            $query->groupBy("$userTable.id");
            
            $command = $query->createCommand();
            $data = $command->queryAll();

            $out['results'] = array_values($data);
        }

        return $out;
    }

    public function actionSearchLead($q = null)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $out = ['results' => []];
        if (!is_null($q)) {
            $query = new Query();
            $query->select([
                't.form_name as id',
                't.form_name as text',
                't.created_time'
            ])
                ->from(UserTelesale::tableName() . ' t')
                ->where("(t.form_name LIKE :q)", [':q' => "%{$q}%"])
                ->groupBy('id')
                ->limit(10)
                ->orderBy('t.created_time DESC');
            $command = $query->createCommand();
            $data = $command->queryAll();
            $out['results'] = array_values($data);
        }
        return $out;
    }
}
