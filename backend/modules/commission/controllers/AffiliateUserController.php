<?php

namespace app\modules\commission\controllers;

use Yii;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

use kyna\commission\models\AffiliateUser;
use kyna\commission\models\search\AffiliateUserSearch;
use app\components\controllers\Controller;

/**
 * UserController implements the CRUD actions for AffiliateUser model.
 */
class AffiliateUserController extends Controller
{

    public $mainTitle = 'Affiliate Users';
    
    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'view'],
                        'matchCallback' => function () {
                            return Yii::$app->user->can('Affiliate.User.View');
                        },
                    ],
                    [
                        'allow' => true,
                        'actions' => ['create'],
                        'matchCallback' => function () {
                            return Yii::$app->user->can('Affiliate.User.Create');
                        },
                    ],
                    [
                        'allow' => true,
                        'actions' => ['update', 'change-status'],
                        'matchCallback' => function () {
                            return Yii::$app->user->can('Affiliate.User.Update');
                        },
                    ],
                    [
                        'allow' => true,
                        'actions' => ['delete'],
                        'matchCallback' => function () {
                            return Yii::$app->user->can('Affiliate.User.Delete');
                        },
                    ],
                ],
            ],
        ]);
    }

    /**
     * Lists all AffiliateUser models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new AffiliateUserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        
        $model = new AffiliateUser();
        $model->loadDefaultValues();
        
        if ($model->load(Yii::$app->request->post())) {
            if ($model->is_manager) {
                $model->manager_id = null;
            }
            if ($model->save()) {
                Yii::$app->session->setFlash('success', 'Thêm thành công');
                
                return $this->redirect(['index']);
            }
        }

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'model' => $model
        ]);
    }

    /**
     * Creates a new AffiliateUser model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new AffiliateUser();
        $model->status = AffiliateUser::STATUS_ACTIVE;

        if ($model->load(Yii::$app->request->post())) {
            if ($model->is_manager) {
                $model->manager_id = null;
            }
            if ($model->save()) {
                Yii::$app->session->setFlash('success', 'Thêm thành công');
                
                return $this->redirect(['index']);
            }
        }
        
        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing AffiliateUser model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            if ($model->is_manager) {
                $model->manager_id = null;
            }
            if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        } 
        
        return $this->render('update', [
            'model' => $model,
        ]);
    }

    protected function findModel($id)
    {
        if (($model = AffiliateUser::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
