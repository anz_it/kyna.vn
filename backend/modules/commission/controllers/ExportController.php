<?php

namespace app\modules\commission\controllers;

use kyna\commission\models\CommissionIncome;
use kyna\commission\models\search\AffiliateCategorySearch;
use kyna\commission\models\search\CommissionIncomeSearch;
use kyna\order\models\Order;
use kyna\order\models\OrderDetails;
use Yii;
use kyna\commission\models\AffiliateUser;
use kyna\commission\models\search\AffiliateUserSearch;
use app\components\controllers\Controller;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * UserController implements the CRUD actions for AffiliateUser model.
 */
class ExportController extends Controller
{

    public $mainTitle = 'Export Affiliate Users';
    
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all AffiliateUser models.
     * @return mixed
     */
    public function actionCategory($id = null)
    {
        $searchModel = new CommissionIncomeSearch();
        if ($id) {
            $searchModel->affiliate_category_id = $id;
        }
        $dataProvider = $searchModel->exportCategorySearch(Yii::$app->request->queryParams);
        return $this->render('category', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'tabs' => $this->loadTabs(),
        ]);
    }

    public function actionIndex()
    {
        $searchModel = new CommissionIncomeSearch();
        $dataProvider = $searchModel->exportSearch(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'tabs' => $this->loadTabs(),
        ]);
    }

    public function actionView($id)
    {
        $searchModel = new CommissionIncomeSearch();
        $searchModel->user_id = $id;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        list($totalReg, $totalPaid, $totalRegFilter, $totalPaidFilter, $totalAmount, $totalAmountFilter) = $this->extractSummaryVarriables($id, $searchModel->course_id, $searchModel->date_ranger);

        return $this->render('view', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'totalReg' => $totalReg,
            'totalPaid' => $totalPaid,
            'totalRegFilter' => $totalRegFilter,
            'totalPaidFilter' => $totalPaidFilter,
            'totalAmount' => $totalAmount,
            'totalAmountFilter' => $totalAmountFilter,
            'tabs' => $this->loadTabs(),
        ]);
    }

    private function extractSummaryVarriables($affiliate_id, $course_id = 0, $date_ranger = '')
    {
        // summary
        $orderQuery = Order::find();
        $orderQuery->alias('t');
        $orderQuery->andWhere(['t.affiliate_id' => $affiliate_id]);

        $incomeTable = CommissionIncome::tableName();
        $orderQuery->leftJoin($incomeTable, $incomeTable . '.order_id = t.id');

        if (!empty($course_id)) {
            // filter by course (if not empty)
            $orderDetailTable = OrderDetails::tableName();
            $orderQuery->leftJoin($orderDetailTable, $orderDetailTable . '.order_id = t.id');
            $orderQuery->andFilterWhere([$orderDetailTable . '.course_id' => $course_id]);
        }

        $paidQuery = clone $orderQuery;
        $paidQuery->andWhere(['is not', $incomeTable . '.id', NULL]);

        $totalReg = $orderQuery->count('t.id');
        $totalAmount = $orderQuery->sum($incomeTable . '.affiliate_commission_amount');
        $totalPaid = $paidQuery->count('t.id');

        if (!empty($date_ranger)) {
            $dateRange = explode(' - ', $date_ranger);
            $beginDate = date_create_from_format('d/m/Y H:i:s', $dateRange[0] . ' 00:00:00');
            $endDate = date_create_from_format('d/m/Y H:i:s', $dateRange[1] . ' 23:59:59');

            $orderQuery->andWhere(['>=', 'order_date', $beginDate->getTimestamp()]);
            $orderQuery->andWhere(['<=', 'order_date', $endDate->getTimestamp()]);

            $paidQuery->andWhere(['>=', 'order_date', $beginDate->getTimestamp()]);
            $paidQuery->andWhere(['<=', 'order_date', $endDate->getTimestamp()]);
        }

        $totalRegFilter = $orderQuery->count('t.id');
        $totalPaidFilter = $paidQuery->count('t.id');
        $totalAmountFilter = $paidQuery->sum($incomeTable . '.affiliate_commission_amount');

        return [
            $totalReg,
            $totalPaid,
            $totalRegFilter,
            $totalPaidFilter,
            $totalAmount,
            $totalAmountFilter,
        ];
    }

    public function actionTeacherIncome()
    {
        $searchModel = new CommissionIncomeSearch();
        $dataProvider = $searchModel->exportTeacherSearch(Yii::$app->request->queryParams);

        return $this->render('teacher_income', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'tabs' => $this->loadTabs(),
        ]);
    }

    private function loadTabs()
    {
        $tabsItems = [
            [
                'label' => 'Tổng',
                'url' => Url::toRoute(['/commission/export/index']),
                'active' => $this->action->id == 'index',
                'encode' => false,
            ],
            [
                'label' => 'Chi tiết danh mục Affiliate',
                'url' => Url::toRoute(['/commission/export/category']),
                'active' => $this->action->id == 'category',
                'encode' => false,
            ],
            [
                'label' => 'Chi tiết Affiliate User',
                'url' => '#',
                'active' => $this->action->id == 'view',
                'visible' => $this->action->id == 'view',
                'encode' => false,
            ],
            [
                'label' => 'Teacher Income',
                'url' => Url::toRoute(['/commission/export/teacher-income']),
                'active' => $this->action->id == 'teacher-income',
                'encode' => false,
            ],
        ];

        return $tabsItems;
    }

    private function loadTeacherTabs()
    {
        $tabsItems = [
            [
                'label' => 'Khóa học và mức hoa hồng',
                'url' => Url::toRoute(['/commission/export/teacher-view']),
                'active' => $this->action->id == 'index',
                'encode' => false,
            ],
            [
                'label' => 'Danh sách học viên',
                'url' => Url::toRoute(['/commission/export/teacher-']),
                'active' => $this->action->id == 'category',
                'encode' => false,
            ],
            [
                'label' => 'Danh sách học viên hoàn tiền',
                'url' => '#',
                'active' => $this->action->id == 'view',
                'visible' => $this->action->id == 'view',
                'encode' => false,
            ],
            [
                'label' => 'Teacher Income',
                'url' => Url::toRoute(['/commission/export/teacher-income']),
                'active' => $this->action->id == 'teacher-income',
                'encode' => false,
            ],
        ];

        return $tabsItems;
    }
}
