<?php
/**
 * Created by PhpStorm.
 * User: Hien Nguyen
 * Date: 8/23/2016
 * Time: 10:33 AM
 */

namespace app\modules\commission\models;

use kyna\commission\models\search\CommissionIncomeSearch;
use kyna\course\models\Course;
use kyna\course\models\Teacher;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use kyna\commission\models\CommissionIncome;
use kyna\commission\models\AffiliateUser;

/**
 * CommissionIncomeSearch represents the model behind the search form about `kyna\commission\models\CommissionIncome`.
 */
class TeacherIncomeExportSearch extends CommissionIncomeSearch
{

    public function searchCourse($params)
    {
        $query = Course::find()->with(['category', 'teacher']);
        $query->andWhere([Course::tableName() . '.type' => Course::TYPE_VIDEO]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
//            Course::tableName() . '.id' => $this->id,
//            'level' => $this->level,
//            'price' => $this->price,
//            'price_discount' => $this->price_discount,
//            'percent_discount' => $this->percent_discount,
//            'total_time' => $this->total_time,
//            'category_id' => $this->category_id,
//            'course_commission_type_id' => $this->course_commission_type_id,
            'teacher_id' => $this->teacher_id,
            'id' => $this->id,
//            Course::tableName() . '.status' => $this->status,
//            Course::tableName() . '.created_time' => $this->created_time,
//            Course::tableName() . '.updated_time' => $this->updated_time,
        ]);

//        $query->andFilterWhere(['like', 'name', $this->name])
//            ->andFilterWhere(['like', 'short_name', $this->short_name])
//            ->andFilterWhere(['like', 'slug', $this->slug])
//            ->andFilterWhere(['like', 'image_url', $this->image_url])
//            ->andFilterWhere(['like', 'video_url', $this->video_url])
//            ->andFilterWhere(['like', 'video_cover_image_url', $this->video_cover_image_url]);

        return $dataProvider;
    }


}
