<?php

namespace app\modules\commission\models;

use kyna\course\models\Course;

class Order extends \kyna\order\models\Order
{
    
    // for search
    public $email;
    public $phone_number;
    public $full_name;
    public $street_address;
    public $course_id;
    public $amount;
    public $form_name;
    public $is_order;
    
    public function getTotalDetailIncome()
    {
        if ($this->id != null) {
            // order
            return parent::getTotalDetailIncome();
        }
        
        // user care
        return $this->amount * $this->originalAffiliateUser->commissionPercent / 100;
    }

    public function getTotalRealIncome()
    {
        if ($this->id != null) {
            // order
            return $this->getRealIncome();
        }

        // user care
        return $this->amount;
    }

    public function getCourse()
    {
        return $this->hasOne(Course::className(), ['id' => 'course_id']);
    }

    public static function getStatusLabels()
    {
        return [
            self::ORDER_STATUS_PENDING_CONTACT => 'Telesales đang follow',
            self::ORDER_STATUS_DELIVERING => 'Đang giao hàng',
            self::ORDER_STATUS_COMPLETE => 'Hoàn thành',
            self::ORDER_STATUS_IN_COMPLETE => 'Thanh toán chưa hoàn tất',
            self::ORDER_STATUS_CANCELLED => 'Bị hủy hoặc/và trả lại',
            self::ORDER_STATUS_NEW => 'Mới tạo',
            self::ORDER_STATUS_PENDING_PAYMENT => 'Chờ thanh toán',
            self::ORDER_STATUS_COMPLETE => 'Hoàn thành',
            self::ORDER_STATUS_RETURN => 'Bị hủy hoặc/và trả lại',
            self::ORDER_STATUS_WAITING_RETURN => 'Chờ trả lại',
            self::ORDER_STATUS_PAYMENT_FAILED => 'Thanh toán thất bại',
            self::ORDER_STATUS_REQUESTING_PAYMENT => 'Yêu cầu thanh toán',
        ];
    }
}