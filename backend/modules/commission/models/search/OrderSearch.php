<?php

namespace app\modules\commission\models\search;

use yii\base\Model;
use app\modules\commission\models\Order;


class OrderSearch extends Order
{
    
    public $month;
    public $year;
    public $date;
    
    public function init()
    {
        parent::init();
        
        $this->month = date('m');
        $this->year = date('Y');
    }
    
    public function rules()
    {
        return [
            [['month', 'year', 'course_id'], 'integer'],
            [['affiliate_id', 'date'], 'safe'],
        ];
    }
    
    /**
     * @return array
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }
    
    public function search($params)
    {
        $this->load($params);

        $orderWhere = "o.`affiliate_id` = {$this->affiliate_id}";
        $userCareWhere = "affiliate_id = {$this->affiliate_id} and order_id is null";
        
        if (!empty($this->date)) {
            $date = date_create_from_format('d/m/Y', $this->date);
            $queryDate = date('Y-m-d', $date->getTimestamp());

            $orderWhere .= " AND date(from_unixtime(o.order_date)) = '$queryDate'";

            $userCareWhere .= " AND date(from_unixtime(created_time)) = '$queryDate'";
        } else {
            $orderWhere .= " AND DATE_FORMAT(from_unixtime(o.order_date), '%m') = {$this->month} 
                AND DATE_FORMAT(from_unixtime(o.order_date), '%Y') = {$this->year}";

            $userCareWhere .= " AND DATE_FORMAT(from_unixtime(created_time), '%m') = {$this->month} 
                AND DATE_FORMAT(from_unixtime(created_time), '%Y') = {$this->year}";
        }

        $orderJoin = "";
        if (!empty($this->course_id)) {
            $orderJoin = "LEFT JOIN order_details od on od.order_id = o.id";
            $orderWhere .= " AND (od.course_id = $this->course_id OR od.course_combo_id = $this->course_id)";

            $userCareWhere .= " AND (course_id = $this->course_id OR list_course_ids LIKE '%{$this->course_id}%')";
        }

        $sql = "SELECT * FROM (
              SELECT o.id, o.affiliate_id, o.order_date, o.status, o.promotion_code, o.user_id, NULL as email, NULL as phone_number, NULL as full_name, NULL as street_address, NULL as form_name, NULL as amount, NULL as course_id, total, shipping_fee, payment_fee ,1 as is_order 
              FROM `orders` o 
              {$orderJoin}
              WHERE $orderWhere 
            UNION 
                SELECT NULL as id, affiliate_id, `created_time` as order_date, status as status, NULL as promotion_code, NULL as user_id, email, phone_number, full_name, street_address, form_name, amount, course_id, NULL as total, NULL as shipping_fee, NULL as payment_fee , 0 as is_order
                FROM user_telesales 
                WHERE $userCareWhere) a ORDER BY a.order_date DESC";

        return $sql;
    }
    
}

