<?php

namespace app\modules\commission\models\search;

use yii\data\ActiveDataProvider;

use kyna\order\models\Order;
use kyna\order\models\OrderDetails;

class OrderDetailSearch extends OrderDetails
{
    
    public $partnerId;
    public $month;
    public $year;
    
    public function init()
    {
        parent::init();
        
        $this->month = date('m');
        $this->year = date('Y');
    }
    
    public function rules()
    {
        return [
            [['order_id', 'month', 'year'], 'integer'],
            [['partnerId'], 'safe'],
        ];
    }
    
    public function search($params)
    {
        $query = OrderDetails::find();
        $query->alias('t');
        
        $tblOrder = Order::tableName();
        $query->joinWith('order');
        
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'attributes' => [
                    "order_id" => [
                        'asc' => ['orders.order_date' => SORT_ASC],
                        'desc' => ['orders.order_date' => SORT_DESC],
                    ]
                ],
                'defaultOrder' => ['order_id' => SORT_DESC]
            ],
        ]);
        
        $this->load($params);
        
        if (!$this->validate()) {
            $query->where('0=1');
            return $dataProvider;
        }
        
        $query->andWhere(["$tblOrder.affiliate_id" => $this->partnerId]);
        
        if (!empty($this->month)) {
            $query->andWhere("DATE_FORMAT(from_unixtime(order_date), '%m') = :month", ['month' => $this->month]);
        }

        if (!empty($this->year)) {
            $query->andWhere("DATE_FORMAT(from_unixtime(order_date), '%Y') = :year", ['year' => $this->year]);
        }
        
        return $dataProvider;
    }
}

