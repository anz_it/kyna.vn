<?php

namespace app\modules\tracking\controllers;

use kyna\user\models\User;
use Yii;
use kyna\tracking\models\TrackingUserCare;
use kyna\tracking\models\TrackingUserCareSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * UserController implements the CRUD actions for TrackingUserCare model.
 */
class UserController extends Controller
{
    public $layout = 'main';
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all TrackingUserCare models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TrackingUserCareSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $createModel = new TrackingUserCare();
        if ($createModel->load(Yii::$app->request->post())) {

            $createModel->save();
            $createModel = new TrackingUserCare();
        }
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'createModel' => $createModel
        ]);
    }

    /**
     * Displays a single TrackingUserCare model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }


    /**
     * Deletes an existing TrackingUserCare model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the TrackingUserCare model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return TrackingUserCare the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = TrackingUserCare::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionAll($term = "")
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $data = User::find()->where(['like', 'email', $term])->limit(20)->all();
        $result =  array_map(function ($p) {
            return ['id' => "{$p->id}", 'email' => $p->email, 'text' => "{$p->id} ({$p->email})"];
        }, $data);
        return ['results' => $result];
    }
}
