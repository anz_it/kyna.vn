<?php
/**
 * Created by PhpStorm.
 * User: khanhphan
 * Date: 1/14/17
 * Time: 10:19 AM
 * @var $this \yii\web\View
 */
use yii\bootstrap\Nav;

$this->beginContent(Yii::getAlias('@backend').'/views/layouts/lte.php');

echo Nav::widget([
    'options' => [
        'class' => 'nav-tabs',
        'style' => 'margin-bottom: 15px',
    ],
    'items' => [
        [
            'label'   => 'Danh sách',
            'url'     => ['/tracking/default/index'],
          //  'visible' => Yii::$app->user->can('Tracking.View')
        ],
        [
            'label'   => 'Nhân viên chăm sóc',
            'url'     => ['/tracking/user/index'],
          //  'visible' => Yii::$app->user->can('Tracking.Assign')
        ],
        [
            'label' => 'Phân công',
            'url'   => ['/tracking/default/assign'],
           // 'visible' => Yii::$app->user->can('Tracking.Assign')
        ],

    ],
]);

echo $content;

$this->endContent();