<?php
/**
 * Created by PhpStorm.
 * User: khanhphan
 * Date: 1/20/17
 * Time: 10:00 AM
 * @var $this \yii\web\View
 */
use \yii\bootstrap\ActiveForm;

?>

<div class="col-md-6">
    <?php
    $form = ActiveForm::begin();
    echo $form->field($model, 'auto_assign')->checkbox()->label("Phân công tự động");
    echo $form->field($model, 'schedule')->dropDownList([
        '30 minutes' => 'Cập nhật mỗi 30 phút',
        '1 hour' => 'Cập nhật mỗi giờ',
        '6 hour' => 'Cập nhật mỗi 6 giờ',

    ])->label("Tần suất phân công tự động");

    echo $form->field($model, 'last_run')->textInput([
        'value' => date('Y-m-d H:i:s', empty($model->last_run) ? 0 : $model->last_run),
        'disabled' => true
    ])->label('Lần cập nhật cuối');

    echo "<button type='submit' class='btn btn-success'>Cập nhật</button>";
    ActiveForm::end();
    ?>

</div>
<?php
$this->registerJs("$('[type=checkbox]').bootstrapToggle();");

