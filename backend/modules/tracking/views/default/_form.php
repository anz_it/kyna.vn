<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model kyna\tracking\models\Tracking */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tracking-form">

    <?php
    $form = ActiveForm::begin([
        'options' => [
            'id' => 'updateNoteForm'
        ]
    ]);
    if ($model->number_called == 0)
        $model->number_called = 1;
    ?>

    <?= $form->field($model, 'number_called')->checkbox() ?>
    <?= $form->field($model, 'note')->textarea();//widget(\common\widgets\tinymce\TinyMce::className(), []) ?>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
