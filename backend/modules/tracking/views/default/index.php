<?php

use yii\helpers\Html;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel kyna\tracking\models\TrackingSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */


$this->params['breadcrumbs'][] = $this->title;
?>
    <div class="tracking-index">
        <?php \yii\widgets\Pjax::begin([
            'options' => [
                'id' => 'pjaxTracking'
            ]
        ]) ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'options' => [
                'id' => 'trackingGrid'
            ],
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                'number',
                [
                    'attribute' => 'piwik_id',
                    'value' => function ($model) {
                        if (empty ($model->piwik_visitor_id))
                            return $model->piwik_id;
                        $url = "https://track.kyna.vn/index.php?module=Widgetize&action=iframe&widget=1&moduleToWidgetize=Live&actionToWidgetize=getVisitorProfilePopup&idSite=1&period=day&date=today&disableLink=1&visitorId={$model->piwik_visitor_id}&token_auth=4ce417dd4083c2cef83c051c646efd28";
                        return Html::a($model->piwik_id, '#', ['data-url' => $url, 'class' => 'piwik-viewer']);
                    },
                    'format' => 'raw'
                ],
                'updated_time:datetime',
                'is_registered',
                'user_id_care',
                'page_id',
                [
                    'attribute' => 'number_called',
                    'value' => function ($model) {
                        /** @var $model \kyna\tracking\models\Tracking */
                        return Html::checkbox("number_called[]", $model->number_called,
                            [
                                'data' => [
                                    'toggle' => 'toggle',
                                    'on' => 'Đã gọi',
                                    'off' => 'Chưa gọi',
                                    'id' => $model->id,
                                ],
                                'class' => 'number_called_status',
                                'disabled' => true,


                            ])." ".
                        Html::button("<i class = 'fa fa-commenting'></i> ", [
                            'class' => 'btn btn-comment '.(empty($model->note) ? 'btn-default' : 'btn-info'),
                            'data' => [
                                'id' => $model->id,

                            ]
                        ]);
                    },
                    'format' => 'raw',

                ],

            ],
        ]); ?>
        <?php \yii\widgets\Pjax::end() ?>
    </div>
    <div class="modal fade" tabindex="-1" role="dialog" id="piwikModal">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <div id="modalContentPiwik"></div>
                </div>

            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    <div class="modal fade" tabindex="-1" role="dialog" id="noteModal">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Cập nhật note</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <div id="modalContentNote"></div>
                </div>

            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

<?php
$this->registerJs("
    $('body').on('click', '.piwik-viewer', function () {
        var url = $(this).attr('data-url');
        $('#modalContentPiwik').html('');
        $('#modalContentPiwik').html(\"<iframe src='\"+url+\"' frameborder='0' width='100%' height='100%'></iframe>\");
        $('#piwikModal').modal('show');
        return false;
    });
   
   $('body').on('click', '.btn-comment', function () {
        $('#noteModal').modal('show');
        $('#modalContentNote').html('');
        $('#modalContentNote').load('/tracking/default/update?id='+$(this).attr('data-id'));
   });
   
   $('body').on('submit', '#updateNoteForm', function () {
        var data = $(this).serialize(),
            url = $(this).attr('action');
        $.ajax({
            url : url,
            data: data,
            type: 'POST',
            success: function (response) {
                $('#noteModal').modal('hide');
            }
        });
        $.pjax.reload('#pjaxTracking');
        return false;
   });
   $('body').on('pjax:success', '#pjaxTracking', function () {
        $('.number_called_status').bootstrapToggle();
   });

");

$this->registerCss("
#piwikModal.modal {
  width: 90%; /* desired relative width */
  left: 5%; /* (100%-width)/2 */
  /* place center */
  margin-left:auto;
  margin-right:auto; 
}
#piwikModal.modal .modal-lg {
    width: 100%;
    height: 500px;
}
#piwikModal .modal-content {
    width: 100%;
    height: 100%;
}
#piwikModal .modal-body {
    height: 100%;
    padding: 0;
    margin: 0;
}
.modal-title {
  display: inline-block;
  font-weight: bold;
}

#modalContentPiwik {
    height: 100%;
    background: url('/img/loading.svg') no-repeat center;

}");