<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model kyna\tracking\models\Tracking */

?>
<div class="tracking-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
