<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model kyna\tracking\models\TrackingUserCare */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tracking-user-care-form" style="margin-bottom: 40px">

    <?php $form = ActiveForm::begin(
        [
            'layout' => 'default'
        ]
    ); ?>

    <div class="col-md-8">
        <?= $form->field($model, 'user_id')->widget(\kartik\select2\Select2::className(), [
            'options' => [
                'placeholder' => 'Thêm mới user (tìm bằng email)',
                'data' => [
                    'ajax--url' => '/tracking/user/all',
                    'ajax--delay' => 300
                ]
            ]
        ])->label(false) ?>
    </div>
    <div class="col-md-4">
        <?= Html::submitButton('Thêm mới ', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
     <div class="clearfix"></div>
    <?php ActiveForm::end(); ?>

</div>
