<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel kyna\tracking\models\TrackingUserCareSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tracking-user-care-index">

    <?php
    echo $this->render('_form', ['model' => $createModel])
    ?>


    <?= \kartik\grid\GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => null,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'user_id',
            [
                'attribute' => 'user_id',
                'label' => 'Email',
                'value' => function ($model) {
                    return \kyna\user\models\User::findOne($model->user_id)->email;
                }
            ],
            'last_assign',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{delete}'
            ],
        ],
    ]); ?>
</div>
<div class="clearfix"></div>
