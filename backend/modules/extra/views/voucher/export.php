<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Export vouchers';

?>

<div class="export-voucher-form">
    <?php $form = ActiveForm::begin([
        'options' => [
            'enctype' => 'multipart/form-data',
            'class' => 'navbar-form navbar-left'
        ],
        'action' => Url::toRoute(['export'])
    ]) ?>
        <div class="row">
            <?= $form->field($uploadModel, 'file', [
                'template' => '<label class="btn btn-default" for="student-upload"><i class="fa fa-upload fa-fw"></i> Nhập từ file excel {input}</label>{error}',
            ])->fileInput([
                'id' => 'student-upload',
                'class' => 'form-control hide',
            ]) ?>
        </div>
    
        <div class="form-group">
            <?= Html::submitButton('Upload', ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end() ?>
</div>