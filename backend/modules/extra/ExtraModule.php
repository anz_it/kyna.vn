<?php

namespace app\modules\extra;

/**
 * extra module definition class
 */
class ExtraModule extends \kyna\base\BaseModule
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\extra\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
