<?php

namespace app\modules\extra\models\forms;

class ExcelUploadForm extends \yii\base\Model
{
    
    public $file;
    
    public function rules()
    {
        return [
            ['file', 'required'],
            [['file'], 'file', 'skipOnEmpty' => false, 'extensions' => 'xls, xlsx, csv'],
        ];
    }
    
    public function attributeLabels()
    {
        return [
            'file' => 'File',
        ];
    }
}
