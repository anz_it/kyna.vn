<?php

namespace app\modules\extra\controllers;

use Yii;
use yii\web\UploadedFile;
use kyna\promotion\models\Promotion;
use app\modules\extra\models\forms\ExcelUploadForm;

class VoucherController extends \app\components\controllers\Controller
{
    
    private $_voucherArray = [
        2 => 200000,
        3 => 150000,
        4 => 100000,
        5 => 50000,
        6 => 40000,
        7 => 30000,
        8 => 20000,
        9 => 10000,
    ];
    private $_minAmountArray = [
        200000 => 500000,
        150000 => 400000,
        100000 => 300000,
        50000 => 200000,
        40000 => 150000,
        30000 => 100000,
        20000 => 50000,
        10000 => 30000,
    ];
    
    public function actionExport()
    {
        $uploadModel = new ExcelUploadForm();
        
        if ($uploadModel->load(Yii::$app->request->post())) {
            $uploadModel->file = UploadedFile::getInstance($uploadModel, 'file');

            if ($uploadModel->validate()) {
                $total = 0;
                $inputFilePath = $uploadModel->file->tempName;
                
                try {
                    $inputFileType = \PHPExcel_IOFactory::identify($inputFilePath);
                    $objReader = \PHPExcel_IOFactory::createReader($inputFileType);
                    $objExcel = $objReader->load($inputFilePath);
                } catch (Exception $ex) {
                    Yii::$app->session->setFlash('error', $ex->getMessage());
                    return $this->redirect(['export']);
                }
                
                $sheet = $objExcel->getSheet(0);
                $highestRow = $sheet->getHighestRow();
                $highestColumn = $sheet->getHighestColumn();
                
                $dbTransaction = Yii::$app->db->beginTransaction();
                try {
                    ini_set('memory_limit', '512M');
                    ini_set('max_execution_time', 0);
                    
                    header('Content-Type: text/csv; charset=UTF-8');
                    header('Content-Disposition: attachment; filename=export_vouchers.csv');
        
                    $output = fopen('php://output', 'w');
                    fputs($output, "\xEF\xBB\xBF");
        
                    for ($row = 2; $row <= $highestRow; $row++) {
                        $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, NULL);

                        $voucherRow = $this->_generateVouchers($rowData, $row); 
                        if (!empty($voucherRow)) {
                            fputcsv($output, $voucherRow);
                        }
                    }
                    
                    fclose($output);

                    $dbTransaction->commit();
                    exit();
                    
                } catch (Exception $ex) {
                    $dbTransaction->rollBack();
                }
            }
        }
        
        return $this->render('export', ['uploadModel' => $uploadModel]);
    }
    
    private function _generateVouchers($rowData, $row)
    {
        $name = '';
        $email = trim($rowData[0][0]);
        $amount = trim($rowData[0][1]);
        
        $return = [
            $name,
            $email,
            Yii::$app->formatter->asCurrency($amount),
        ];
        
        for ($i = 2; $i <= 9; $i ++) {
            $quantity = intval($rowData[0][$i]);
            
            if (!empty($quantity)) {
                $value = $this->_voucherArray[$i];
                $minAmount = $this->_minAmountArray[$value];
                
                $codes = Promotion::create($value, $quantity, $minAmount);
                $return[] = $quantity;
                $return[] = str_replace('"', '', implode('  ', $codes));
            } else {
                $return[] = $quantity;
                $return[] = '';
            }
        }
        
        return $return;
    }
    
}