<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model kyna\report\models\Report */
/* @var $form yii\widgets\ActiveForm */


?>


<div class="report-form">
    <h1>  <?= $model->name ?> </h1>

    <?php $form = ActiveForm::begin(['id'=>'report-form']); ?>
        <?php foreach ($model->getReportParams()->all() as $key => $param) { ?>

            <b> <?= $param->description ?> : </b>
            <?=
            Html::input('text', $param->name, $param->default_value, $options = ['required' => true, 'class' => 'form-control', 'maxlength' => 10, 'style' => 'width:350px']);
            ?> </br>
        <?php } ?>
        <?=
        Html::hiddenInput('id', $model->id);

        ?> </br>

        <?=
        Html::hiddenInput('email', Yii::$app->user->identity->email);

        ?> </br>


        <a class="btn btn-info  navbar-btn navbar-left" data-toggle="modal" data-target="#myModal">
                <i class="fa fa-file-excel-o"></i> Xuất báo cáo
        </a>
     <?php ActiveForm::end(); ?>

</div>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">

            <div class="modal-body">
                <form class="form-horizontal" id="exportForm">
                    <div class="form-group" style="padding:10px">
                        <label>Bạn vui lòng cung cấp email: </label>
                        <input id="exportEmail" type="email" value="<?=Yii::$app->user->identity->email?>" class="form-control">
                        <span class="help-block">
                            Hệ thống sẽ chạy export ngầm, sau khi có kết quả sẽ gửi mail tới cho bạn.
                        </span>
                    </div>
                    <button type="submit" class="btn btn-success">Xác nhận</button>
                    <button type="button" data-dismiss="modal" class="btn btn-default">Đóng</button>
                </form>
            </div>

        </div>
    </div>
</div>


<?php
/** @var $this \yii\web\View */
\backend\assets\BootboxAsset::register($this);
$this->registerJs("
    $('#exportForm').submit(function () {
        if (exportEmail.value.trim().length == 0) {
            bootbox.alert('Bạn vui lòng cung cấp email.');
            return false;
        }
        var email = $( \"#exportEmail\" ).val();
        $('input[name=\"email\"]').val(email);
        $(\"#report-form\").submit();
        return false;
    });
");
?>