<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model kyna\report\models\Report */

$this->title = 'Update Report: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Reports', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="report-update">


    <?=
    \yii\jui\Tabs::widget([
        'items' => $this->context->getTabItems(['model' => $model], Yii::$app->request->get('tab'))
    ]);
    ?>

</div>
