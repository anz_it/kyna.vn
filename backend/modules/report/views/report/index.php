<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel kyna\report\models\search\ReportSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Reports';
$this->params['breadcrumbs'][] = $this->title;
$user = \Yii::$app->user;
?>
<style>
    .word-wrap{
        word-break:break-all;
    }
</style>
<div class="report-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?php if ($user->can('Report.Create')) : ?>
        <p>
            <?= Html::a('Create Report', ['create'], ['class' => 'btn btn-success']) ?>
        </p>
    <?php endif; ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'id',
            'name',
            'description',
            [
                'label' => 'SQL Command',
                'attribute' => 'sql_command',
                'visible' => $user->can('Report.All'),
                'contentOptions' => ['class' => 'word-wrap'],
            ],
            [
                'attribute' => 'status',
                'value' => function ($model) {
                    return $model->statusButton;
                },
                'format' => 'raw',
                'filter' => \common\widgets\Select2::widget([
                    'model' => $searchModel,
                    'attribute' => 'status',
                    'data' => \kyna\report\models\Report::listStatus(false),
                    'options' => ['placeholder' => 'Ẩn/Hiện'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                    'hideSearch' => true,
                ]),
                'visible' => $user->can('Report.All'),

            ],
            // 'note',
            // 'created_time:datetime',
            // 'updated_time:datetime',
            [
                'header' => 'Xử lý',
                'template' => '{delete}{update}{run}{param}',
                'class' => 'yii\grid\ActionColumn',
                'contentOptions' => ['style' => 'width: 90px;'],
                'visibleButtons' => [
                    'update' => function ($model, $key) use ($user) {
                        return $user->can('Report.Update');
                    },
                    'delete' => function ($model, $key) use ($user) {
                        return $user->can('Report.Delete');
                    },
                    'run' => function ($model, $key) use ($user) {
                        return $user->can('Report.Run');
                    },
                    'param' => function ($model, $key) use ($user) {
                        return $user->can('Report.Update');
                    },

                ],
                'buttons' => [
                    'update' => function ($url, $model, $key) {
                        $url = \yii\helpers\Url::toRoute(['/report/report/update', 'id' => $model->id]);

                        return Html::a('<span class="fa fa-pencil"></span> Update', $url, [
                            'class' => 'btn btn-sm btn-default btn-block',
                        ]);
                    }, 'delete' => function ($url, $model, $key) {
                        $url = \yii\helpers\Url::toRoute(['/report/report/delete', 'id' => $key]);
                        return Html::a('<span class="fa fa-edit"></span> Delete', $url, [
                            'class' => 'btn btn-sm btn-default btn-block btn-delete',
                        ]);
                    }
                    , 'param' => function ($url, $model, $key) {
                        $url = \yii\helpers\Url::toRoute(['/report/report-param', 'id' => $key]);
                        return Html::a('<span class="fa fa-edit"></span> Parameter', $url, [
                            'class' => 'btn btn-sm btn-default btn-block',
                        ]);
                    }
                    , 'run' => function ($url, $model, $key) {
                        $url = \yii\helpers\Url::toRoute(['/report/report/run', 'id' => $key]);
                        return Html::a('<span class="fa fa-edit"></span> Run', $url, [
                            'class' => 'btn btn-sm btn-default btn-block btn-view',
                        ]);
                    },
                ]
            ],

        ],
    ]); ?>
</div>
