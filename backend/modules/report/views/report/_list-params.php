<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel kyna\report\models\search\ReportParamSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */


$searchModel = new \kyna\report\models\search\ReportParamSearch();
$searchModel->report_id = $model->id;
$dataProvider = $searchModel->search(Yii::$app->request->queryParams);

$this->title = 'Report Params';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="report-param-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Param', ['report-param/create?report_id=' . @$model->id,], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'id',
            'name',
            'default_value',
            [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{update} {delete}',
           // 'controller' => 'report-param',
            'buttons' => [
                'update' => function ($url, $model, $key) {
                    $url = \yii\helpers\Url::toRoute(['report-param/update',
                        'id' => $model->id,
                        'report_id' => $model->report_id,
                    ]);
                    $options = [
                        'title' => Yii::t('yii', 'Update'),
                        'aria-label' => Yii::t('yii', 'Update'),
                        'data-pjax' => '0',
                    ];
                    return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, $options);

                },
                'delete' => function ($url, $model, $key) {
                    $url = \yii\helpers\Url::toRoute([
                        'report-param/delete',
                        'id' => $model->id
                    ]);

                    $options = array_merge([
                        'title' => Yii::t('yii', 'Delete'),
                        'aria-label' => Yii::t('yii', 'Delete'),
                        'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
                        'data-method' => 'post',
                        'data-pjax' => '0',
                    ]);
                    return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, $options);
                },

            ]
                ]
        ],
    ]); ?>
</div>
