<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model kyna\report\models\ReportParam */

$this->title = 'Create Report Param';
$this->params['breadcrumbs'][] = ['label' => 'Report Params', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="report-param-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
