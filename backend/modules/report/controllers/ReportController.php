<?php

namespace app\modules\report\controllers;

use app\modules\sync\models\User;
use Yii;
use kyna\report\models\Report;
use kyna\report\models\search\ReportSearch;
use app\components\controllers\Controller;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ReportController implements the CRUD actions for Report model.
 */
class ReportController extends Controller
{

    const TAB_INFO = 'info';
    const TAB_LIST_PARAMS = 'list-params';

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],

                ]],
                'access' => [
                    'class' => AccessControl::className(),
                    'rules' => [
                        [
                            'allow' => true,
                            'actions' => ['create'],
                            'matchCallback' => function () {
                                return Yii::$app->user->can('Report.Create');
                            },
                        ],
                        [
                            'allow' => true,
                            'actions' => ['create'],
                            'matchCallback' => function () {
                                return Yii::$app->user->can('Report.Create');
                            },
                        ],
                        [
                            'allow' => true,
                            'actions' => ['update'],
                            'matchCallback' => function () {
                                return Yii::$app->user->can('Report.Update');
                            },
                        ],
                        [
                            'allow' => true,
                            'actions' => ['Delete'],
                            'matchCallback' => function () {
                                return Yii::$app->user->can('Report.Delete');
                            },
                        ],
                        [
                            'allow' => true,
                            'actions' => ['run'],
                            'matchCallback' => function () {
                                return Yii::$app->user->can('Report.Run');
                            },
                        ],

                        [
                            'allow' => true,
                            'actions' => ['index'],
                            'matchCallback' => function () {
                                return Yii::$app->user->can('Report.View') ||Yii::$app->user->can('Report.Run') ;
                            },
                        ],
                        [
                            'allow' => true,
                            'roles' => ['@'],
                        ],

                    ]
                ]


        ];
    }

    /**
     * Lists all Report models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ReportSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Report model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Finds the Report model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Report the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Report::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Creates a new Report model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Report();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Report model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Report model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionRun($id = 0)
    {
        $model = $this->findModel($id);
        if (empty($model)) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        if (Yii::$app->request->isGet) {
            return $this->render('run', [
                'model' => $model,
            ]);
        } else {
            $sql_command = $model->sql_command;
            foreach ($model->getReportParams()->all() as $key => $reportParam) {
                $param_name = $reportParam->name;
                $param_value = Yii::$app->request->getBodyParam($param_name);
                $sql_command = str_replace('{' . $param_name . '}', "'$param_value'", $sql_command);
            }
            $rootPath = \Yii::getAlias('@root');
            //$email  = "truong.nguyen@kyna.vn";
            $current_user_email = \kyna\user\models\User::findOne(Yii::$app->user->id)->email;
            $email = Yii::$app->request->post('email');

            $column_name = $model->columns_name;
            $exportLog = \Yii::getAlias('@console/runtime/report_export.log');
            $command = "php {$rootPath}/yii export/report/export  \"{$sql_command}\" {$email} \"{$model->name}\" \"{$column_name}\" \"{$current_user_email}\" " . " >> {$exportLog} &";
          //   var_dump($command); die();

            exec($command);
            Yii::$app->session->setFlash('success', "Hệ thống xử lý xong sẽ gửi mail cho bạn trong vài phút. Vui lòng check mail để xem kết quả.");
            return $this->render('run', [
                'model' => $model,
            ]);
        }
    }

    public function getTabItems($data, $tab = self::TAB_INFO)
    {
        $tabItems = [
            [
                'label' => 'Thông tin',
                'content' => $this->renderPartial('_form', $data, true),
                'active' => $tab == self::TAB_INFO ? true : false
            ],
            [
                'label' => 'Danh sách tham số',
                'content' => $this->renderPartial('_list-params', $data, true),
                'active' => $tab == self::TAB_LIST_PARAMS ? true : false
            ]
        ];

        return $tabItems;
    }


}
