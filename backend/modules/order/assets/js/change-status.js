;(function ($, _) {
    //$(Order-status-3)
    $("body").on("click", ".btn-order-status a, a.btn-note, a.btn-move, a.btn-update-shipping", function (e) {
        e.preventDefault();
        var data = $(this).data();
        var $modal = $("#modal"),
            url = this.href;

        if (data.redirect) {
            window.location.href = url;
            return;
        }

        $modal.find(".modal-content").load(url, function (resp) {
            $modal.modal("show");
        });
    });
})(jQuery, _);
