;(function ($, _) {
    $("body").on("click", ".btn-call", function (e) {
        e.preventDefault();

        var $modal = $("#modal"),
            url = this.href;

        $modal.find(".modal-content").load(url, function () {
            $modal.modal("show");


            var $timer = $(".timer"),
                $form = $("#form-call-status");

            $timer.timer();

            $(".btn-end-call").on("click", function (e) {
                e.preventDefault();
                $timer.timer("pause");
                $(".call-timer").addClass("hide");
                $form.removeClass("hide");
            });
        });
    });
})(jQuery, _);
