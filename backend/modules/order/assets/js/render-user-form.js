;
(function($, _) {
    var $selector = $("[name*='[user_email]']"),
        $contactName = $("[name*='[shipping_contact_name]']"),
        $phoneNumber = $("[name*='[shipping_phone_number]']"),
        $streetAddress = $("[name*='[shipping_street_address]']"),
        $locationId = $("[name*='[shipping_location_id]']");

    $selector.on("keypress blur", function(e) {
        if (e.type === "keypress" && e.keyCode !== 13) {
            return true;
        }
        e.preventDefault();
        updateView();
    });

    $("body").on("change", "[name*='[payment_method]']", function() {
        updateView();
    });

    var updateView = function() {
        var value = $selector.val();

        $.getJSON('/user/search/address', {
            's': value,
        }, function(response) {
            if (response[0] !== undefined) {
                $contactName.val(response[0].contactName);
                $phoneNumber.val(response[0].phoneNumber);
                $streetAddress.val(response[0].streetAddress);
                window.locationField.setSelection(response[0].locationId);
                
                var method = $('#payment-method').val();
                if (method ==  'cod') {
                    $('#createorderform-shipping_location_id-district').trigger('change');
                }
            }
        });
    }
})(jQuery, _);
