;(function ($, _) {
    $("body").on('typeahead:select', "#course-typeahead", function(e, datum) {
        addToCart(datum.id); $(this).typeahead("val","");
    });
    
    $('body').on('click', '.remove-cart-item', function (e) {
        e.preventDefault();
        
        var id = $(this).data('id');
        
        removeCartItem(id);
    });
    
    var $cartItems = $('#cart-items');
    
    function addToCart(id) {
        var itemArray = [];
        if ($cartItems.val() !== "") {
            itemArray = $cartItems.val().split(',');
        }
        if(checkExistCourse(id) == 0) {
            itemArray.push(id);
            itemArray = _.uniq(itemArray);

            $cartItems.val(itemArray.join(','));
            updateCart();
        }
        else
        {
            alert("Khóa này đã tồn tại trong combo / khóa học đã được chọn!");
        }
    }
    
    function removeCartItem(id) {
        var itemArray = $cartItems.val().split(',');
        
        itemArray = jQuery.grep(itemArray, function( a ) {
            return a != id;
        });
        itemArray = _.uniq(itemArray);

        $cartItems.val(itemArray.join(','));
        updateCart();
    }
    
    function updateCart() {
        var url = '/order/cart/render';
        var location_id = $('#createorderform-shipping_location_id-district').val();
        
        $.post(url, { "ids": $cartItems.val(), "location_id" : location_id }, function (response) {
            $("#cart-content").html(response);

            var subtotal = $("#order-formatted-subtotal").val();
            var voucherLixi = $('#voucherLixi').val();

            $("#subtotal-text").html(subtotal);
            $('#createorderform-promotion_code').val(voucherLixi);
            $("#group_discount_amount").val($("#discount-amount").val());
            $("#group_discount_amount_text").html($("#discount-amount-text").val());
            if ($('#payment-method').val() == 'cod') {
                $("#shipping-amount").html($("#order-formatted-shipping-fee").val());
            } else {
                $("#shipping-amount").html($('#order-formatted-shipping-fee-0').val());
            }
            $("#group_discount_id").val($("#group-discount-id").val());
        });
    }
    function checkExistCourse(course_id) {
        var url = '/order/cart/check-exist-course';
        var is_exist = 0;
        $.ajaxSetup({async: false});
        $.post(url, { "ids": $cartItems.val(), "course_id" : course_id }, function (response) {
            is_exist = response;
        });
        return is_exist;
    }
    
    $(document).ready(function() {
        var cartItems = $('#cart-items');
        if (cartItems.length && cartItems.val().length > 0) {
            updateCart();
        }
    });
})(jQuery, _);
