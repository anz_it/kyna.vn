<?php

namespace app\modules\order;

class Module extends \kyna\base\BaseModule
{
    public $controllerNamespace = 'app\modules\order\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }

}
