<?php

namespace app\modules\order\controllers;

use Yii;
use yii\db\Transaction;
use yii\web\Controller;
use yii\web\Response;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;
use yii\widgets\ActiveForm;
use yii\helpers\Json;
use yii\filters\AccessControl;
use yii\base\Model;

use app\modules\order\models\ActionForm;

use kyna\base\models\Vendor;
use kyna\order\models\actions\OrderAction;
use kyna\order\models\Order;
use kyna\payment\events\TransactionEvent;
use kyna\payment\lib\proship\Proship;
use kyna\payment\models\PaymentMethod;
use kyna\payment\models\PaymentTransaction;
use kyna\payment\models\TopupTransaction;
use kyna\servicecaller\traits\CurlTrait;
use kyna\user\models\UserCourse;
use kyna\order\models\OrderDetails;
use kyna\partner\models\Transaction as PartnerTransaction;
use kyna\payment\lib\ghtk\Ghtk;

use kyna\base\models\Location;


class ActionController extends Controller
{
    private $_form;
    private $_model;
    private $_view;
    
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['send-to-shipping'],
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->user->can('Order.Action.Shipping');
                        },
                    ],
                    [
                        'allow' => true,
                        'actions' => ['complete'],
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->user->can('Order.Action.Complete');
                        },
                    ],
                    [
                        'allow' => true,
                        'actions' => ['cod-return'],
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->user->can('Order.Action.Return');
                        },
                    ],
                    [
                        'allow' => true,
                        'actions' => ['call'],
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->user->can('Order.Action.Call');
                        },
                    ],
                    [
                        'allow' => true,
                        'actions' => ['note'],
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->user->can('Order.Action.Note');
                        },
                    ],
                    [
                        'allow' => true,
                        'actions' => ['cancel'],
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->user->can('Order.Action.Cancel');
                        },
                    ],
                    [
                        'allow' => true,
                        'actions' => ['request-payment'],
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->user->can('Order.Action.RequestPayment');
                        },
                    ],
                    [
                        'allow' => true,
                        'actions' => ['move-to-cs'],
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->user->can('Order.Action.MoveToCs');
                        },
                    ],
                    [
                        'allow' => true,
                        'actions' => ['update-shipping'],
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->user->can('Order.Action.UpdateShipping');
                        },
                    ],
                    [
                        'allow' => true,
                        'actions' => ['update-partner-code'],
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->user->can('Order.Action.UpdatePartnerCode');
                        },
                    ],
                    [
                        'allow' => true,
                        'actions' => ['update-shipping-status'],
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->user->can('Order.Action.UpdateShippingStatus');
                        },
                    ],
                    [
                        'allow' => true,
                        'actions' => ['cancel-shipping'],
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->user->can('Order.Action.CancelShipping');
                        },
                    ],
                    [
                        'allow' => true,
                        'actions' => ['update-payment-method'],
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->user->can('Order.Action.UpdatePaymentMethod');
                        },
                    ],
                    [
                        'allow' => true,
                        'actions' => ['history'],
                        'matchCallback' => function ($rule, $action) {
                            return true;
                        },
                    ],
                ],
            ],
        ];
    }

    public function init()
    {
        $this->_form = new ActionForm();

        TransactionEvent::on(PaymentMethod::className(), PaymentMethod::EVENT_PAYMENT_SUCCESS, [$this, 'sendToShip']);
        TransactionEvent::on(Order::className(), Order::EVENT_ORDER_COMPLETED, [$this, 'afterOrderComplete']);
    }

    public function beforeAction($action)
    {
        $this->_form->setScenario($action->id);
        $this->_view = $action->id;

        $_id = Yii::$app->request->get('id');
        $this->_model = $this->findModel($_id);

        $this->_form->order_id = $_id;
        return parent::beforeAction($action);
    }

    /**
     * Send to ship function.
     */
    public function sendToShip($eventAction)
    {
        return true;
    }

    public function afterOrderComplete($event)
    {
        $data = $event->actionMeta;
        $order = $data['order'];
        $details = OrderDetails::findAll(['order_id' => $order['id']]);

        $ids = [];
        $contents = [];
        foreach ($details as $detail) {
            $ids[] = $detail->course_id;
            $contents[] = [
                'id' => $detail->course_id,
                'item_price' => $detail->unit_price - $detail->discount_amount,
                'quantity' => 1
            ];
        }

        Yii::$app->session->setFlash('need-pixel-tracking', [
            'ids' => $ids,
            'contents' => $contents,
            'total' => ($order['total'] - $order['shipping_fee'])
        ]);
    }

    /**
     * @param $id
     * @return string
     */
    public function actionCall($id)
    {
        // generate d to init form
        $start = time();
        $_checksum = md5($id . '-' . $start);

        $formData = [
            'order_id' => $id,
            'start' => $start,
            'checksum' => $_checksum,
        ];

        $formModel = $this->_form;
        $formModel->attributes = $formData;

        $this->_submitAction('endCall');

        $model = $this->_model;
        $address = $model->user->userAddress;

        $phoneNumber = isset($model->shippingAddress->phone_number) ?
                $model->shippingAddress->phone_number :
                (!empty($address) ?
                        $address->phone_number : false
                );
        $contactName = isset($model->shippingAddress->contact_name) ?
                $model->shippingAddress->contact_name :
                ((!empty($address) and ! empty($address->contact_name)) ?
                        $address->contact_name : false
                );

        return $this->_render([
                    'phoneNumber' => $phoneNumber,
                    'contactName' => $contactName,
                    'actions' => $model->actionHistory('call'),
                    'statuses' => OrderAction::callStatuses(),
        ]);
    }

    public function actionHistory($id)
    {
        $model = $this->findModel($id);
        $name = \Yii::$app->request->get('name');
        $dataProvider = $model->actionHistory($name);
        $availableActions = ['' => 'Tất cả'] + ActionForm::getEnableActions($id);

        return $this->_render([
            'actionDataProvider' => $dataProvider,
            'model' => $model,
            'availableActions' => $availableActions,
            'actionName' => $name,
        ]);
    }

    
    /**
     * @desc add note and change call status
     */
    public function actionNote($id)
    {
        $formData = [
            'order_id' => $id,
        ];

        $formModel = $this->_form;
        $formModel->attributes = $formData;
        
        $this->_submitAction('note');
        
        return $this->_render([
            'statuses' => OrderAction::callStatuses(),
        ]);
    }
    
    /**
     * @desc update shipping address with order cod
     */
    public function actionUpdateShipping($id)
    {
        if (!empty($this->_model->shippingAddress)) {
            $this->_form->setAttributes($this->_model->shippingAddress->attributes);
        } elseif (!empty($this->_model->user->userAddress)) {
            $this->_form->setAttributes($this->_model->user->userAddress->attributes);
        }

        $this->_submitAction('updateShipping');

        return $this->_render([
        ]);
    }

    /**
     * @desc update partner code with order cod
     */
    public function actionUpdatePartnerCode($id)
    {
        $details = $this->_model->details;
        $partnerDetails = [];
        foreach ($details as $detail) {
            $course = $detail->course;
            if ($course && $course->isPartner && $course->partner->isServiceType) {
                // check if AC is activated
                $transaction = PartnerTransaction::find()
                    ->where([
                        'code' => $detail->partnerCode ? $detail->partnerCode->code : null,
                        'order_id' => $id
                    ])
                    ->exists();
                if ($transaction) {
                    continue;
                }
                $detail->setScenario(OrderDetails::SCENARIO_UPDATE_PARTNER);
                $combo = $detail->combo;
                if ($combo && $combo->isComboPartner) {
                    $partnerDetails[$combo->id] = $detail;
                } else {
                    $partnerDetails[$course->id] = $detail;
                }
            }
        }

        if (Model::loadMultiple($partnerDetails, Yii::$app->request->post())) {
            $validation = Model::validateMultiple($partnerDetails);
            // check duplicate serial
            $serialData = [];
            $duplicateSerial = false;
            foreach ($partnerDetails as &$partnerDetail) {
                if (in_array($partnerDetail->activation_code, $serialData)) {
                    $partnerDetail->addError('activation_code', 'Serial Code không hợp lệ');
                    $duplicateSerial = true;
                } else {
                    $serialData[] = $partnerDetail->activation_code;
                }
            }
            if ($validation && $duplicateSerial == false) {
                $this->_form->partner_code = [];
                foreach ($partnerDetails as $detail) {
                    $course = $detail->course;
                    $combo = $detail->combo;
                    if ($combo && $combo->isComboPartner) {
                        foreach ($details as $orderDetail) {
                            if ($orderDetail->combo && $orderDetail->combo->id == $combo->id) {
                                $orderDetail->activation_code = $detail->activation_code;
                                $orderDetail->save(false);
                            }
                        }
                        $this->_form->partner_code[$combo->id] = $detail->activation_code;
                    } else {
                        $detail->save(false);
                        $this->_form->partner_code[$course->id] = $detail->activation_code;
                    }
                }
                $actionName = 'updatePartnerCode';
                if ($this->_form->$actionName()) {
                    if (Yii::$app->request->isAjax) {
                        Yii::$app->response->format = Response::FORMAT_JSON;
                        $ret = [
                            'success' => true,
                            'message' => 'Xử lý thành công!',
                        ];
                        echo Json::encode($ret);

                        Yii::$app->end();
                    }
                }
            }
        }

        return $this->_render([
            'partnerDetails' => $partnerDetails
        ]);
    }

    public function actionRequestPayment($id)
    {
        $paymentMethods = PaymentMethod::getAvailable();
        $paymentMethods = ArrayHelper::map($paymentMethods, 'class', 'name');

        $this->_submitAction('requestPayment');
        return $this->_render([
                    'paymentMethods' => $paymentMethods
        ]);
    }

    public function actionUpdatePaymentMethod($id)
    {
        $paymentMethods = PaymentMethod::getAvailable(PaymentMethod::GET_AVAILABLE_ANOTHER);
        $paymentMethods = ArrayHelper::map($paymentMethods, 'class', 'name');
        if (!empty($this->_model->shippingAddress)) {
            $this->_form->setAttributes($this->_model->shippingAddress->attributes);
        } elseif (!empty($this->_model->user->userAddress)) {
            $this->_form->setAttributes($this->_model->user->userAddress->attributes);
        }

        $this->_submitAction('updatePaymentMethod');
        return $this->_render([
            'paymentMethods' => $paymentMethods
        ]);
    }

    public function actionSendToShipping($id)
    {
        // Get order attribute
        if (!empty($this->_model->shippingAddress)) {
            $this->_form->setAttributes($this->_model->shippingAddress->attributes);
            $this->_form->activation_code = $this->_model->activation_code;
        } elseif (!empty($this->_model->user->userAddress)) {
            $this->_form->setAttributes($this->_model->user->userAddress->attributes);
            $this->_form->activation_code = $this->_model->activation_code;
        }
        // Get method => lay thong tin shipping methods -> render/popup modal
        if (Yii::$app->request->isGet) {
            $paymentMethods = PaymentMethod::getAvailable(true);
            $paymentMethods = ArrayHelper::map($paymentMethods, 'class', 'name');

            if (sizeof($paymentMethods) == 1 && key($paymentMethods) == 'cod') {
                $shippingMethods = Vendor::find()->where([
                    'status' => Vendor::STATUS_ENABLED,
                    'vendor_type' => Vendor::VENDOR_TYPE_LOGISTIC,
                ])->all();
                $shippingMethods = ArrayHelper::map($shippingMethods, 'alias', 'name');
            } else {
                $shippingMethods = [];
            }
            return $this->_render([
                'paymentMethods' => $paymentMethods,
                'shippingMethods' => $shippingMethods,
            ]);
        }
        // Post method => submit action
            // If COD: update shipping_fee before send_to_shipping
        $form = Yii::$app->request->getBodyParam('ActionForm');
        if (
            isset($form)
            && isset($form['payment_method'])
            && $form['payment_method'] == 'cod'
            && isset($form['location_id'])
        ) {
            $order = Order::findOne([
                'id' => $id
            ]);
            if(!empty($order)) {
                $order->shipping_fee = Location::calculateFee($form['location_id'], $order->sub_total - $order->total_discount);

                $order->total = $order->sub_total - $order->total_discount + $order->shipping_fee;
                if ($order->total < 0) {
                    $order->total = 0;
                }
                
                $order->save();
            }
        }

        $this->_submitAction('sendToShipping');
    }

    public function actionComplete($id)
    {
        if (Yii::$app->request->isGet)
            return $this->_render();
        $this->_submitAction('complete');
    }

    public function actionCancel($id)
    {
        // Neu request = 'GET', show confirm pop up
        if (Yii::$app->request->isGet) {
            return $this->_render();
        }
        // Khong phai 'GET', khong phai 'POST' -> return false
        if (!Yii::$app->request->isPost) {
            return false;
        }
        // Filter some action here
        // request = 'POST', validate form info and do stuff here
            // Try load form infomation
        if (!$this->_form->load(Yii::$app->request->post()))
            return false;
            // Validate form
        if (!$this->_form->validate()) {
            // Valicate Failed - show failed message
            Yii::$app->response->format = Response::FORMAT_JSON;
            $ret = [
                'success' => false,
                'message' => 'VALIDATE FORM ERROR',
            ];
            echo Json::encode($ret);
            Yii::$app->end();
        }

            // Get payment transaction from order_id
        $transaction = PaymentTransaction::find()->where([
            'status' => TopupTransaction::STATUS_ACTIVE,
            'order_id' => $id,
        ])->one();
        if (isset($transaction) && isset($transaction['payment_method'])) {
            switch ($transaction['payment_method']) {
                case 'cod':
                    // Get order from order_id
                    $order = Order::findOne([
                        'id' => $id
                    ]);
                    switch ($order['shipping_method']) {
                        case 'proship':
                            $proship = new Proship();
                            // Is proship shipper, call proship cancel API
                            $_respone = json_decode($proship->cancelOrder($transaction['transaction_code']), true);
                            if ($_respone['code'] != 0) {
                                $ret = [
                                    'success' => false,
//                                'message' => $_return['message'],
                                    'message' => "Khong the huy don hang - vui long cap nhat tinh trang don hang",
                                ];
                                echo Json::encode($ret);
                                Yii::$app->end();
                            }
                            // Cancel success, update
                            $proship->updateShippingStatus($transaction['transaction_code'], $_respone['data']['invoiceStatus']);
                            break;
                        case 'ghtk':
                            $ghtk = new Ghtk();
                            // Is proship shipper, call proship cancel API
                            $_respone = $ghtk->cancelOrder([
                                'transaction_code' => $transaction['transaction_code']
                            ]);
                            if ($_respone['success'] != true) {
                                $ret = [
                                    'success' => false,
//                                'message' => $_return['message'],
                                    'message' => "Khong the huy don hang - vui long cap nhat tinh trang don hang",
                                ];
                                echo Json::encode($ret);
                                Yii::$app->end();
                            }
                            // Cancel success, update
                            $ghtk->updateShippingStatus([
                                'transaction_code' => $transaction['transaction_code'],
                                'status' => Ghtk::SHIPPING_STATUS__HUY_DON_HANG,
                            ]);
                            break;
                        default:
                            break;
                    }
                    break;
                default:
                    break;
            }
        }
        // Continue do old stuff here
        $this->_submitAction('cancel');
    }

    public function actionCodReturn($id)
    {
        $this->_submitAction('codReturn');

        return $this->_render();
    }
    
    public function actionMoveToCs($id)
    {
        $this->_submitAction('moveToCs');
        return $this->_render();
    }

    /**
     * Get shipping status
     * @param $transactionId
     * @param $payment_method
     */
    public function actionUpdateShippingStatus($id)
    {
//        echo $id;
        // GET request -> do nothing
        if (Yii::$app->request->isGet) {
            return false;
        }
        // Khong phai 'GET', khong phai 'POST' -> return false
        if (!Yii::$app->request->isPost) {
            return false;
        }
        // POST request -> start Update
            // Get payment transaction from order_id
        $transaction = PaymentTransaction::find()->where([
            'status' => TopupTransaction::STATUS_ACTIVE,
            'order_id' => $id,
        ])->one();
        if (isset($transaction) && isset($transaction['payment_method'])) {
//            echo $transaction['payment_method'];
            switch ($transaction['payment_method']) {
                case 'cod':
                    // Get order from order_id
                    $order = Order::findOne([
                        'id' => $id,
                    ]);
                    // If payment method is 'cod' - get Shipping method
                    switch ($order['shipping_method']) {
                        case 'proship':
                            $proship = new Proship();
                            $_respone = Json::decode($proship->getOrderDetail($transaction['transaction_code']));
                            if ($_respone['code'] != 0) {
                                // Failed - show FAILED message
                                $ret = [
                                    'success' => false,
                                    'message' => $_respone['message'],
                                ];
                                return Json::encode($ret);
                            }
                            // Success
                            // Update shipping_status on DB
                            // Call Proship updateShippingStatus method
                            $proship->updateShippingStatus($transaction['transaction_code'], $_respone['data']['invoiceStatus'], $_respone['data']['sumFee']);
                            // show SUCCESS message
                            $ret = [
                                'success' => true,
                                'message' => $_respone['message'],
                            ];
                            return Json::encode($ret);
                            break;
                        case 'ghtk':
                            $ghtk = new Ghtk();
                            $_respone = $ghtk->getShippingStatus(['transaction_code' => $transaction['transaction_code']]);
                            if ($_respone['success'] != true) {
                                // Failed - show FAILED message
                                $ret = [
                                    'success' => false,
                                    'message' => $_respone['message'],
                                ];
                                return Json::encode($ret);
                            }
                            // Success
                            // Update shipping_status on DB
                            // Call Proship updateShippingStatus method
                            $ghtk->updateShippingStatus([
                                'transaction_code' => $transaction['transaction_code'],
                                'status' => $_respone['order']['status'],
                            ]);
                            // show SUCCESS message
                            $ret = [
                                'success' => true,
                                'message' => $_respone['message'],
                            ];
                            return Json::encode($ret);
                            break;
                        default:
                            break;
                    }
                    break;
                default:
                    break;
            }
        } else
            // Can't get transaction
            return false;
    }

    /**
     * Cancel Shipping order if possible
     */
    public function actionCancelShipping($id)
    {
        // Neu request = 'GET', show confirm pop up
        if (Yii::$app->request->isGet) {
            return $this->_render();
        }
        // Khong phai 'GET', khong phai 'POST' -> return false
        if (!Yii::$app->request->isPost) {
            return false;
        }

        // Neu request = 'POST', validate form info and do stuff here
        // request = 'POST', validate form info and do stuff here
            // Try load form infomation
        if (!$this->_form->load(Yii::$app->request->post()))
            return false;
        // Validate form
        if (!$this->_form->validate()) {
            // Valicate Failed - show failed message
            Yii::$app->response->format = Response::FORMAT_JSON;
            $ret = [
                'success' => false,
                'message' => 'VALIDATE FORM ERROR',
            ];
            echo Json::encode($ret);
            Yii::$app->end();
        }
            // Get payment transaction from order_id
        $transaction = PaymentTransaction::find()->where([
            'status' => TopupTransaction::STATUS_ACTIVE,
            'order_id' => $id,
        ])->one();
        if (isset($transaction) && isset($transaction['payment_method'])) {
//            echo $transaction['payment_method'];
            switch ($transaction['payment_method']) {
                case 'cod':
                    // Get order from order_id
                    $order = Order::findOne([
                        'id' => $id,
                    ]);
                    // payment_method is COD -> get shipping_method:
                    switch ($order['shipping_method']) {
                        case 'proship':
                            // If shipping method is 'Proship' - call Cancel Invoice API
                            $proship = new Proship();
                            $_respone = Json::decode($proship->cancelOrder($transaction['transaction_code']));
                            if ($_respone['code'] != 0) {
                                // Failed - show FAILED message
                                Yii::$app->response->format = Response::FORMAT_JSON;
                                $ret = [
                                    'success' => false,
                                    'message' => $_respone['message'],
                                ];
                                echo Json::encode($ret);
                                Yii::$app->end();
                            }
                            // Success
                            // Update shipping_status on DB
                            // Call Proship updateShippingStatus method
                            $proship->updateShippingStatus($transaction['transaction_code'], $_respone['data']['invoiceStatus']);
                            Order::cancelShipping($order['id'], [
                                'note' => $this->_form->note,
                            ]);
                            // show SUCCESS message
                            Yii::$app->response->format = Response::FORMAT_JSON;
                            $ret = [
                                'success' => true,
                                'message' => $_respone['message'],
                            ];
                            echo Json::encode($ret);
                            Yii::$app->end();
                            break;
                        case 'ghtk':
                            $ghtk = new Ghtk();
                            $_respone = $ghtk->cancelOrder([
                                'transaction_code' => $transaction['transaction_code']
                            ]);
                            if ($_respone['success'] != true) {
                                // Failed - show FAILED message
                                Yii::$app->response->format = Response::FORMAT_JSON;
                                $ret = [
                                    'success' => false,
                                    'message' => $_respone['message'],
                                ];
                                echo Json::encode($ret);
                                Yii::$app->end();
                            }
                            // Success
                            // Update shipping_status on DB
                            // Call Proship updateShippingStatus method
                            $ghtk->updateShippingStatus([
                                'transaction_code' => $transaction['transaction_code'],
                                'status' => Ghtk::SHIPPING_STATUS__HUY_DON_HANG,
                            ]);
                            Order::cancelShipping($order['id'], [
                                'note' => $this->_form->note,
                            ]);
//                                 $_respone['data']['invoiceStatus']);
                            // show SUCCESS message
                            Yii::$app->response->format = Response::FORMAT_JSON;
                            $ret = [
                                'success' => true,
//                                'message' => $_respone['message'],
                                'message' => "Hủy đơn hàng thành công",
                            ];
                            echo Json::encode($ret);
                            Yii::$app->end();
                            break;
                        default:
                            break;
                    }
                        break;
                    default:
                        break;
                }
            }
    }


    private function _submitAction($actionName, $callback = false)
    {
        // Action co load form
        if ($this->_form->load(Yii::$app->request->post())) {
            if ($this->_form->validate() && $order = $this->_form->$actionName()) {
                if ($callback and is_callable($callback)) {
                    call_user_func_array($callback, [$order]);
                }
                if (Yii::$app->request->isAjax) {
                    Yii::$app->response->format = Response::FORMAT_JSON;
                    $ret = [
                        'success' => true,
                        'message' => 'Xử lý thành công!',
                    ];
                    echo Json::encode($ret);

                    Yii::$app->end();
                }
            } else {
                if (Yii::$app->request->isAjax) {
                    Yii::$app->response->format = Response::FORMAT_JSON;
                    $validateMessage = $this->formatErrors();
                    $ret = [
                        'success' => false,
                        'message' => !empty($validateMessage) ? $validateMessage : 'Xử lý thất bại!',
                    ];
                    echo Json::encode($ret);
                    Yii::$app->end();
                }
            }
        }
        // action khong load form here
    }

    private function formatErrors()
    {
        $error = '';
        if ($this->_form->hasErrors()) {
            foreach ($this->_form->getErrors() as $field => $errors) {
                $error .= $this->_form->getFirstError($field) . ' <br>';
            }
        }
        return $error;
    }

    private function _render($actionParams = [])
    {
        $params = [
            'model' => $this->_model,
            'formModel' => $this->_form,
        ];
        return $this->renderAjax($this->_view, $params + $actionParams);
    }

    /**
     * Finds the Order model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param int $id
     *
     * @return Order the loaded model
     *
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Order::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
