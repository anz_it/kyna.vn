<?php

namespace app\modules\order\controllers;

use Yii;
use yii\helpers\Url;
use yii\filters\AccessControl;
use app\modules\order\models\RevenueSearch;
use kyna\order\models\actions\OrderAction;
use kyna\order\models\Order;

/**
 * DefaultController implements the CRUD actions for Order model.
 */
class RevenueController extends \app\components\controllers\Controller
{

    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'matchCallback' => function () {
                            return Yii::$app->user->can('Order.Revenue');
                        },
                    ],
                ],
            ],
        ]);
    }

    public function actionIndex()
    {
        $searchModel = new RevenueSearch();
        $dataProvider = $searchModel->searchRevenue(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'tabs' => $this->loadTabs()
        ]);
    }

    public function actionDaily()
    {
        $searchModel = new RevenueSearch();
        $params = Yii::$app->request->queryParams;
        $date = new \DateTime();
        if (isset($params['RevenueSearch']['date'])) {
            $searchModel->date = $params['RevenueSearch']['date'];
            $date = date_create_from_format('d/m/Y H:i', $searchModel->date . ' 00:00');
        }
        return $this->render('daily', [
                    'searchModel' => $searchModel,
                    'date' => $date,
                    'tabs' => $this->loadTabs()
        ]);
    }

    public function actionCod()
    {
        $searchModel = new RevenueSearch();
        $dataProvider = $searchModel->searchCod(Yii::$app->request->queryParams);
        $order = $dataProvider->getModels();

        $callStatuses = OrderAction::callStatuses();
        $callStatuses[-1] = "Chưa gọi";
        $callStatusSummary = array_map(function($item) {
            return 0;
        }, $callStatuses);

        $orderStatuses = Order::getCodStatuses();
        $statusSummary = array_map(function($item) {
            return 0;
        }, $orderStatuses);

        $timeSummary = [
            'count' => 0,
            'summary' => 0,
            'summaryFailed' => 0,
            'countIn5Minutes' => 0,
            'countFrom5To15Minutes' => 0,
            'countMore15Minutes' => 0,
        ];
        $orderCount = 0;
        foreach ($order as $item) {
            $orderCount++;
            $cs = $item->call_status;
            $orderStatus = $item->status;

            if (is_null($cs))
                $cs = -1;
            if (array_key_exists($cs, $callStatusSummary)) {
                $callStatusSummary[$cs] ++;
            }
            if (array_key_exists($orderStatus, $statusSummary)) {
                $statusSummary[$orderStatus] ++;
            }

            $listCall = $item->actionHistory('call')->getModels();
            foreach ($listCall as $k => $call) {
                $timeSummary['summary'] += $call->duration;
                $timeSummary['count'] ++;
                if ($cs == OrderAction::CALL_STATUS_UNTOUCHABLE)
                    $timeSummary['summaryFailed'] ++;

                if ($call->duration <= 300) {
                    $timeSummary['countIn5Minutes'] ++;
                } elseif (($call->duration > 300) && ($call->duration <= 900)) {
                    $timeSummary['countFrom5To15Minutes'] ++;
                } else {
                    $timeSummary['countMore15Minutes'] ++;
                }
            }
        }

        return $this->render('cod', [
                    'searchModel' => $searchModel,
                    'callStatusSummary' => $callStatusSummary,
                    'callStatuses' => $callStatuses,
                    'orderStatuses' => $orderStatuses,
                    'statusSummary' => $statusSummary,
                    'timeSummary' => $timeSummary,
                    'orderCount' => $orderCount,
                    'tabs' => $this->loadTabs()
        ]);
    }

    private function loadTabs()
    {
        $tabsItems = [
            [
                'label' => 'Doanh thu',
                'url' => Url::toRoute(['/order/revenue/index']),
                'active' => $this->action->id == 'index',
                'encode' => false,
            ],
            [
                'label' => 'Đơn đăng ký theo ngày',
                'url' => Url::toRoute(['/order/revenue/daily']),
                'active' => $this->action->id == 'daily',
                'encode' => false,
            ],
            [
                'label' => 'Đơn hàng COD ( COD Report)',
                'url' => Url::toRoute(['/order/revenue/cod']),
                'active' => $this->action->id == 'cod',
                'encode' => false,
            ],
        ];

        return $tabsItems;
    }

}
