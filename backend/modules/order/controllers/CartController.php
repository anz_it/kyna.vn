<?php

namespace app\modules\order\controllers;

use kyna\promo\models\GroupDiscount;
use Yii;
use kyna\order\models\Order;
use kyna\course\models\Course;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use kyna\base\models\Location;

class CartController extends \yii\web\Controller
{

    public function actionRender()
    {
        $query = Course::find();
        $model = new Order();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        $discountCourseIds = GroupDiscount::getGroupDiscountCourses();
        $courseQuantity = 0;
        $discountCourseQuantity = 0;
        $shippingFee = 0;

        $ids = [];

        if ($ids = Yii::$app->request->post('ids')) {
            $ids = explode(',', $ids);
            $allComboItems = $this->getComboItems($ids);

            foreach ($ids as $key => $id) {
                if (in_array($id, $allComboItems)) {
                    unset($ids[$key]);
                }
                if(in_array($id,$discountCourseIds) && !in_array($id, $allComboItems))
                    $discountCourseQuantity ++;
            }

            $courseQuantity = count($ids) - 1;
        }

        $query->where(['id' => $ids]);

        if ($locationId = Yii::$app->request->post('location_id')) {
            $courses = $query->all();
            $total = 0;
            foreach ($courses as $course) {
                $total += $course->sellPrice;
            }

            if (!empty($total)) {
                $shippingFee = Location::calculateFee($locationId, $total);
            }
        }

        return $this->renderPartial('@app/modules/order/views/create/_cart', [
            'dataProvider' => $dataProvider,
            'model' => $model,
            'groupDiscount' => $this->getGroupDiscount($discountCourseQuantity + 1),
            'currentGroupDiscount' => GroupDiscount::getCurrentGroupDiscount($discountCourseQuantity),
            'discountCourses' => $discountCourseIds,
            'courseQuantity' => $courseQuantity,
            'shippingFee' => $shippingFee,
        ]);
    }

    private function getComboItems($ids)
    {
        $result = [];
        $courses = Course::find()->where(['id' => $ids])->andWhere(['type' => Course::TYPE_COMBO])->all();

        foreach ($courses as $course) {
            $result = array_merge($result, ArrayHelper::map($course->comboItems, 'course_id', 'course_id'));
        }

        return $result;
    }

    protected function getGroupDiscount($courseQuantity)
    {
        return GroupDiscount::find()
            ->andWhere(['<=', 'course_quantity', $courseQuantity])
            ->andWhere(['status' => GroupDiscount::STATUS_ACTIVE])
            ->orderBy(['course_quantity' => SORT_DESC])
            ->one();
    }

    public function actionGetShippingFee()
    {
        if (Yii::$app->request->isAjax) {
            $locationId = Yii::$app->request->post('location_id');
            $totalPayment = Yii::$app->request->post('total');

            if (empty($locationId) || empty($totalPayment)) {
                return 0;
            }

            $shippingFee = Location::calculateFee($locationId, $totalPayment);

            return json_encode(['error_code' => 0, 'object' => [
                'fee' => $shippingFee,
                'feeFormat' => Yii::$app->formatter->asCurrency($shippingFee),
            ]]);
        }
    }

    public function actionCheckExistCourse()
    {
        $course_id = Yii::$app->request->post('course_id');

        $course_add = Course::find()->where(['id' => $course_id])->one();

        $ids = Yii::$app->request->post('ids');
        $ids = explode(",", $ids);
        $all_ids = array();
        $courses = Course::find()->where(['id' => $ids])->all();
        if (!empty($courses)) {
            foreach ($courses as $course) {
                if ($course->type == Course::TYPE_COMBO) {
                    $all_ids = array_merge($all_ids, ArrayHelper::map($course->comboItems, 'course_id', 'course_id'));
                } else
                    $all_ids = array_merge($all_ids, [$course->id]);
            }
        }
        if ($course_add->type == Course::TYPE_COMBO) {
            $course_add_child_ids = ArrayHelper::getColumn($course_add->comboItems, 'course_id');
            $interset_ids = array_intersect($all_ids, $course_add_child_ids);
            if (empty($interset_ids))
                return 0;
            return 1;
        } else {
            if (in_array($course_id, $all_ids)) {
               return 1;
            }
            return 0;
        }
        return 0;
    }

}
