<?php

namespace app\modules\order\controllers;

use common\helpers\PDFHelper;
use console\models\Ghn;
use Faker\Provider\at_AT\Payment;
use kyna\base\models\Vendor;
use kyna\payment\models\PaymentTransaction;
use Yii;
use yii\data\ArrayDataProvider;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;
use yii\helpers\Url;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\web\Response;

use kyna\user\models\Profile;
use kyna\order\models\Order;
use kyna\order\models\OrderDetails;
use kyna\user\models\User;
use kyna\order\models\actions\OrderAction;
use kyna\payment\models\PaymentMethod;

use common\helpers\StringHelper;

use app\modules\order\models\OrderSearch;
use app\modules\order\models\ActionForm;
use kyna\course\models\Course;

/**
 * DefaultController implements the CRUD actions for Order model.
 */
class DefaultController extends \yii\web\Controller
{

    public $layout;
    public $mainTitle = 'Đơn hàng';

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'matchCallback' => function () {
                            return Yii::$app->user->can('Order.View');
                        },
                    ],
                    [
                        'allow' => true,
                        'actions' => ['index'],
                        'matchCallback' => function () {
                            return Yii::$app->user->can('Order.View.All');
                        },
                    ],
                    [
                        'allow' => true,
                        'actions' => ['update'],
                        'matchCallback' => function () {
                            return Yii::$app->user->can('Order.Action.Update');
                        },
                    ],
                ],
            ],
        ];
    }

    public function extractPermission($userId)
    {
        $orderClass = Order::className();
        $permissions = Yii::$app->authManager->getPermissionsByUser($userId);

        $allowedStatus = [];
        $allowedAction = [];
        $canViewAll = false;

        foreach ($permissions as $permissionName => $permissions) {
            if ($permissionName === 'Order.View.All') {
                $canViewAll = true;
            }

            $permissionName = strtoupper($permissionName);

            if (StringHelper::startWith('ORDER.VIEW.', $permissionName)) {
                $permissionName = str_replace('ORDER.VIEW.', '', $permissionName);
                if (defined($orderClass . '::ORDER_STATUS_' . $permissionName)) {
                    $allowedStatus[] = constant($orderClass . '::ORDER_STATUS_' . $permissionName);
                }
            }
            if (StringHelper::startWith('ORDER.ACTION.', $permissionName)) {
                $permissionName = str_replace('ORDER.ACTION.', '', $permissionName);
                $allowedAction[] = $permissionName;
            }
        }

        $defaultStatus = array_diff($allowedStatus, [
            Order::ORDER_STATUS_DRAFT,
            Order::ORDER_STATUS_COMPLETE,
            Order::ORDER_STATUS_IN_COMPLETE,
            Order::ORDER_STATUS_CANCELLED,
        ]);

        return [
            $defaultStatus,
            $allowedStatus,
            $allowedAction,
            $canViewAll,
        ];
    }

    public function actionIndex()
    {
        $userId = Yii::$app->user->identity->id;

        list($defaultStatus, $allowedStatus, $allowedAction, $canViewAll) = $this->extractPermission($userId);
        $defaultStatus = $allowedStatus;
        $this->loadParams($userId, $params, $allowedStatus, $defaultStatus);

        $searchModel = $this->loadSearchModel();
        $searchModel->attributes = $params;
        $searchModel->allowedStatus = $allowedStatus;
        $dataProvider = $searchModel->search($params);
        $newOrderCount = $dataProvider->totalCount;

        return $this->render('index', [
            'queryParams' => $params,
            'dataProvider' => $dataProvider,
            'tabs' => $this->loadTabs(),
            'statusLabelCss' => Order::getStatusLabelCss(),
            'callStatuses' => OrderAction::callStatuses(),
            'allowedStatus' => $allowedStatus,
        ]);
    }

    /**
     * @return
     */
    public function actionPrintCod()
    {
        $userId = Yii::$app->user->identity->id;

        list($defaultStatus, $allowedStatus, $allowedAction, $canViewAll) = $this->extractPermission($userId);
        $defaultStatus = $allowedStatus;
        $this->loadParams($userId, $params, $allowedStatus, $defaultStatus);

        $defaultMethod = 'cod';

        $searchModel = $this->loadSearchModel();
        $searchModel->attributes = $params;
        $searchModel->allowedStatus = $allowedStatus;
        $searchModel->payment_method = $defaultMethod;

        $codMethod = PaymentMethod::find()->where(['class' => $defaultMethod])->one();

        $codSetting = $codMethod->clientSettings;

        if (!empty(Yii::$app->request->get('location'))) {
            $params['location'] = Yii::$app->request->get('location');
        } else {
            $params['location'] = null;
        }
        if (Yii::$app->request->get('date-range')) {
            $params['date-range'] = Yii::$app->request->get('date-range');
        } else {
            $params['from_date'] = strtotime('today 0:00');
            $params['to_date'] = time();
            $searchModel->from_date = $params['from_date'];
            $searchModel->to_date = $params['to_date'];
            $params['date-range'] = date("d/m/Y h:i", $params['from_date']) . ' - ' . date("d/m/Y h:i", $params['to_date']);
        }
        if (Yii::$app->request->get('shipping_method')) {
            $params['shipping_method'] = Yii::$app->request->get('shipping_method');
        }

        if (Yii::$app->request->get('course_type')) {
            $params['course_type'] = Yii::$app->request->get('course_type');
        }

        $dataProvider = $searchModel->search($params);
        $newOrderCount = $dataProvider->totalCount;
        $dataProvider->pagination->pageSize = $newOrderCount;

        if (Yii::$app->request->isAjax && Yii::$app->request->post('type', null) == 'export') {
            // init pagination for raw SQL
            $sql = $dataProvider->query->createCommand()->getRawSql();
            $sql = str_replace('`', '', $sql);
            $email = empty(Yii::$app->request->post('email'))?Yii::$app->user->identity->email:Yii::$app->request->post('email');
            // execute command in console
            $rootPath = \Yii::getAlias('@root');
            $exportLog = \Yii::getAlias('@console/runtime/export_print.log');
            $exportErrorLog = \Yii::getAlias('@console/runtime/export_print-error.log');
            // check log file exist
            self::_checkExistFile($exportLog);
            self::_checkExistFile($exportErrorLog);
            $command = "php {$rootPath}/yii export/run cod \"{$sql}\" {$email} {$params['location']}" . " > {$exportLog} 2>>{$exportErrorLog} &";
            exec($command);
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'result' => true,
            ];
        }

        $allData = $dataProvider->getModels();

        // filter normal partner
        $allData = $this->formatNormalPartner($allData, $params);

        $filterData = $this->filterData($allData, $codSetting, $params['location']);

        $isPrint = Yii::$app->request->get('print');
        if (!empty($isPrint)) {
            $this->layout = 'print';
            if ($params['course_type'] == Course::TYPE_VIDEO) {
                return PDFHelper::generateCOD($filterData, 'I');
            } elseif ($params['course_type'] == Course::TYPE_SOFTWARE) {
                return PDFHelper::generatePartnerCOD($filterData, 'I');
            }
        }

        $arrDataProvider = new ArrayDataProvider([
            'allModels' => $filterData
        ]);

        $shippingMethods = Vendor::findAll([
            'status' => Vendor::STATUS_ACTIVE,
            'vendor_type' => Vendor::VENDOR_TYPE_LOGISTIC,
        ]);
        $shippingMethods = ArrayHelper::map($shippingMethods, 'alias', 'name');

        return $this->render('print_cod', [
            'queryParams' => $params,
            'dataProvider' => $arrDataProvider,
            'tabs' => $this->loadTabs(),
            'statusLabelCss' => Order::getStatusLabelCss(),
            'callStatuses' => OrderAction::callStatuses(),
            'allowedStatus' => $allowedStatus,
            'codSettings' => $codSetting['Pickups'],
            'shippingMethods' => $shippingMethods,
            'courseTypes' => Course::listTypes()
        ]);
    }

    private function _checkExistFile($file)
    {
        clearstatcache();
        if (!file_exists($file)) {
            touch($file);
        }
    }

    public function formatNormalPartner($data, $params)
    {
        $result = [];
        if (!empty($params['course_type']) && $params['course_type'] == Course::TYPE_SOFTWARE) {
            foreach ($data as $item) {
                if ($item->hasOnlyNormalPartner || $item->hasPartner) {
                    $result[] = $item;
                }
            }
        } else {
            $result = $data;
        }

        return $result;
    }

    public function actionWaitProcess()
    {
        $userId = Yii::$app->user->identity->id;
        $params['operator_id'] = $userId;

        list($defaultStatus, $allowedStatus, $allowedAction, $canViewAll) = $this->extractPermission($userId);
        $this->loadParams($userId, $params, $allowedStatus, $defaultStatus);

        $finishStatus = [
            Order::ORDER_STATUS_CANCELLED,
            Order::ORDER_STATUS_COMPLETE,
        ];
        foreach ($allowedStatus as $key => $fStatus) {
            if (in_array($fStatus, $finishStatus)) {
                unset($allowedStatus[$key]);
            }
        }

        $searchModel = $this->loadSearchModel($params);
        $searchModel->attributes = $params;
        $searchModel->allowedStatus = $allowedStatus;
        $dataProvider = $searchModel->search($params);

        return $this->render('index', [
            'queryParams' => $params,
            'dataProvider' => $dataProvider,
            'tabs' => $this->loadTabs(),
            'statusLabelCss' => Order::getStatusLabelCss(),
            'callStatuses' => OrderAction::callStatuses(),
            'allowedStatus' => $allowedStatus,
        ]);
    }

    public function actionByOperator()
    {
        $userId = Yii::$app->user->id;
        $params['operator_id'] = $userId;

        list($defaultStatus, $allowedStatus, $allowedAction, $canViewAll) = $this->extractPermission($userId);
        $defaultStatus = [
            Order::ORDER_STATUS_CANCELLED,
            Order::ORDER_STATUS_COMPLETE,
        ];

        $this->loadParams($userId, $params, $allowedStatus, $defaultStatus);

        $searchModel = $this->loadSearchModel();
        $searchModel->attributes = $params;
        $searchModel->allowedStatus = $allowedStatus;
        $dataProvider = $searchModel->search($params);

        return $this->render('index', [
            'queryParams' => $params,
            'dataProvider' => $dataProvider,
            'tabs' => $this->loadTabs(),
            'statusLabelCss' => Order::getStatusLabelCss(),
            'callStatuses' => OrderAction::callStatuses(),
            'allowedStatus' => $defaultStatus,
        ]);
    }

    public function actionAffiliate()
    {
        $userId = Yii::$app->user->identity->id;
        $params['affiliate_id'] = $userId;

        if (Yii::$app->request->get('date-range')) {
            $dateRange = explode(' - ', Yii::$app->request->get('date-range'));
            $beginDate = date_create_from_format('d/m/Y', $dateRange[0]);
            $endDate = date_create_from_format('d/m/Y', $dateRange[1]);

            $params['from_date'] = $beginDate->getTimestamp();
            $params['to_date'] = $endDate->getTimestamp();
        }
        $params['call_status'] = Yii::$app->request->get('call_status');
        $params['is_4kid'] = Yii::$app->request->get('is_4kid');
        $searchModel = $this->loadSearchModel();
        $searchModel->attributes = $params;
        $dataProvider = $searchModel->search($params);

        return $this->render('index', [
            'queryParams' => $params,
            'dataProvider' => $dataProvider,
            'tabs' => $this->loadTabs(),
            'statusLabelCss' => Order::getStatusLabelCss(),
            'callStatuses' => OrderAction::callStatuses(),
            'allowedStatus' => Order::getAllStatuses()
        ]);
    }

    public function loadSearchModel()
    {
        $searchModel = new OrderSearch();
        $searchModel->is_done_telesale_process = OrderSearch::BOOL_YES;

        return $searchModel;
    }

    /**
     * @param $data
     * @param $settings
     * @param $location_id
     * @return array|null
     */
    public function filterData($data, $settings, $location_id)
    {
        if (!empty($data) && !empty($location_id)) {
            $all_data = null;
            $filter_data = null;
            //Get all data at warehouse
            foreach ($data as $key => $item) {
                switch ($item->shipping_method) {
                    case 'ghn':
                        // lay pickupid by ghn setting (ghn.ini)
                        if (!empty($item->shippingAddress->location->parent_id)) {
                            $ghn = new \kyna\payment\lib\ghn\Ghn();
                            $provId = ($item->shippingAddress->location->parent_id);
                            $pickupId = $ghn->settings['Locations'][$provId]['pickup'];
                            // convert to Cod pickup id
//                            $_pickupId = $settings['Pickups'][$settings['GhnAlias']['pickupId'][$pickupId]];
                            $_pickupId = $settings['Pickups'][Yii::$app->params['cod']['GhnAlias']['pickupId'][$pickupId]];
                            if (isset($pickupId))
                                $all_data[$key][$_pickupId] = $item;
                        }
                        break;
                    case 'proship':
                        // Get pick up id from payment_transaction
                        $transactions = PaymentTransaction::findOne([
                            'status' => PaymentTransaction::STATUS_ACTIVE,
                            'order_id' => $item->id
                        ]);
                        if (isset($transactions)) {
                            $pickupId = $transactions['pick_up_location_id'];
                            // convert to Cod pickup id
//                            $_pickupId = $settings['Pickups'][$settings['ProshipAlias']['pickupId'][$pickupId]];
                            $_pickupId = $settings['Pickups'][Yii::$app->params['cod']['ProshipAlias']['pickupId'][$pickupId]];
                            if (isset($_pickupId))
                                $all_data[$key][$_pickupId] = $item;
                        }
                        break;
                    case 'ghtk':
                        // Get pick up id from payment_transaction
                        $transactions = PaymentTransaction::findOne([
                            'status' => PaymentTransaction::STATUS_ACTIVE,
                            'order_id' => $item->id
                        ]);
                        if (isset($transactions)) {
                            $pickupId = $transactions['pick_up_location_id'];
                            // convert to Cod pickup id
//                            $_pickupId = $settings['Pickups'][$settings['ProshipAlias']['pickupId'][$pickupId]];
                            $_pickupId = $settings['Pickups'][Yii::$app->params['cod']['GhtkAlias']['pickupId'][$pickupId]];
                            if (isset($_pickupId))
                                $all_data[$key][$_pickupId] = $item;
                        }
                        break;
                    case 'default':
                        break;
                }
            }
            // Filter by warehouse ID
            if (!empty($all_data)) {
                foreach ($all_data as $item) {
                    if (empty($item[$location_id]))
                        continue;
                    $filter_data[] = $item[$location_id];
                }
            }
            return $filter_data;
        }
        return $data;
    }

    private function loadParams($userId, &$params, $allowedStatus = [], $defaultStatus = false)
    {
        if ($status = Yii::$app->request->get('status')) {
            $params['status'] = Yii::$app->request->get('status');
        } else {
            $statusBy = false;
            if ($search = Yii::$app->request->get('s')) {
                $search = trim($search);
                $stringType = StringHelper::typeDetect($search);
             
                if ($stringType == StringHelper::STRING_TYPE_EMAIL) {
                    $user = User::find()->where(['email' => $search])->one();
                    $params['user_id'] = empty($user) ? null : $user->id;
                } elseif ($stringType == StringHelper::STRING_TYPE_PHONE_NUMBER) {
                    $params['phone_number'] = $search;
                } else {
                    $params['id'] = (int)$search;
                }

                $statusBy = 'allowedStatus';
            } else {
                $statusBy = 'defaultStatus';
            }

            if ($status and in_array($status, $allowedStatus)) {
                $status = explode(',', $status);
                $statusBy = 'status';
            }

            $params['status'] = ${$statusBy};
        }

        if (Yii::$app->request->get('date-range')) {
            $dateRange = explode(' - ', Yii::$app->request->get('date-range'));
            if (Yii::$app->controller->action->id == 'print-cod' || Yii::$app->controller->action->id == 'update-cod') {
                $beginDate = date_create_from_format('d/m/Y H:i', $dateRange[0]);
                $endDate = date_create_from_format('d/m/Y H:i', $dateRange[1]);
            } else {
                $beginDate = date_create_from_format('d/m/Y H:i:s', $dateRange[0] . ' 00:00:00');
                $endDate = date_create_from_format('d/m/Y H:i:s', $dateRange[1] . ' 00:00:00');
            }

            $params['from_date'] = $beginDate->getTimestamp();
            $params['to_date'] = $endDate->getTimestamp();
            $params['date-range'] = Yii::$app->request->get('date-range');
        }

        if (Yii::$app->request->get('print_type')) {
            $params['print_type'] = Yii::$app->request->get('print_type');
        }



        if (Yii::$app->request->get('partner_id')) {
            $params['partner_id'] = Yii::$app->request->get('partner_id');
        }

        $params['call_status'] = Yii::$app->request->get('call_status');

        $params['shipping_method'] = Yii::$app->request->get('shipping_method');
        $params['is_4kid'] = Yii::$app->request->get('is_4kid');
    }

    private function loadTabs()
    {
        $tabsItems = [
            [
                'label' => 'Chờ xử lý',
                'url' => Url::toRoute(['wait-process']),
                'active' => $this->action->id == 'wait-process',
                'encode' => false,
            ],
            [
                // Danh sách các đơn hàng mà operator hiện tại là A
                'label' => 'Tôi đã xử lý',
                'url' => Url::toRoute(['by-operator']),
                'active' => $this->action->id == 'by-operator',
                'encode' => false,
            ],
            [
                // Danh sách các đơn hàng mà operator hiện tại là A
                'label' => 'Tôi đã giới thiệu',
                'url' => Url::toRoute(['affiliate']),
                'active' => $this->action->id == 'affiliate',
                'encode' => false,
            ],
        ];

        if (Yii::$app->user->can('Order.View.All')) {
            $tabsItems[] = [
                'label' => 'Tất cả đơn hàng',
                'url' => Url::toRoute(['index']),
                'active' => $this->action->id == 'index',
                'encode' => false,
            ];
        }

        if (Yii::$app->user->can('Order.Action.Create')) {
            $tabsItems[] = [
                'label' => '<i class="icon ion-android-add"></i> Tạo đơn hàng',
                'url' => Url::toRoute(['/order/create']),
                'encode' => false,
            ];
        }

        return $tabsItems;
    }

    public function actionView($id)
    {
        $orderDetails = new OrderDetails();
        $dataProvider = new ActiveDataProvider([
            'query' => $orderDetails->find()->where(['order_id' => $id]),
        ]);

        return $this->render('view', [
            'model' => $this->findModel($id),
            'dataProvider' => $dataProvider,
            'statusLabelCss' => Order::getStatusLabelCss(),
            //'actionDataProvider' =>
        ]);
    }

    public function actionHistory($id)
    {
        $model = $this->findModel($id);
        $name = \Yii::$app->request->get('name');
        $dataProvider = $model->actionHistory($name);
        $availableActions = ['' => 'Tất cả'] + ActionForm::getEnableActions($id);

        return $this->render('history', [
            'actionDataProvider' => $dataProvider,
            'model' => $model,
            'availableActions' => $availableActions,
            'actionName' => $name,
        ]);
    }

    protected function findModel($id)
    {
        if (($model = Order::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    private function _detectUser($string)
    {
        //$userQuery = User::find();
        $stringType = StringHelper::typeDetect($string);
        switch ($stringType) {
            case StringHelper::STRING_TYPE_EMAIL:
                $params['email'] = $string;
                if ($user = User::find()->where($params)->one()) {
                    return $user->id;
                }

                return false;
            case StringHelper::STRING_TYPE_PHONE_NUMBER:
                $params['phone_number'] = $string;
                if ($userAddress = Profile::find()->select('user_id')->where($params)->asArray()->all()) {
                    return ArrayHelper::getColumn($userAddress, 'user_id');
                }

                return false;
            default:
                return false;
        }
    }

}
