<?php

namespace app\modules\order\controllers;

use Yii;
use yii\base\InvalidParamException;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;

use kyna\user\models\User;
use kyna\order\models\ShippingMethod;
use kyna\order\models\Order;

use app\modules\order\models\UpdateOrderForm;

class UpdateController extends \yii\web\Controller
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index'],
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->user->can('Order.Action.Update');
                        },
                    ],
                ],
            ],
        ];
    }

    /**
     * Creates a new Order model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     *
     * @return mixed
     */
    public function actionIndex($id)
    {
        $model = $this->loadModel($id);
        if (empty($model->payment_method)) {
            $model->payment_method = 'cod';
        }

        $paymentMethods = [
            'cod' => 'Cash on Delivery (COD)',
            'auto' => 'Thanh toán Online/chuyển khoản',
            'onepay_cc' => 'Thẻ Visa / Mastercard',
            'onepay_atm' => 'Thẻ ATM có Internet Banking',
            'ipay' => 'Thanh toán bằng mã thẻ cào điện thoại',
            'bank-transfer' => 'Chuyển khoản Ngân hàng',
            'free' => 'Khóa học miễn phí'
        ];

        $shippingMethods = ['' => ''] + ArrayHelper::map(ShippingMethod::getAvailable(), 'id', 'name');

        if ($model->load(\Yii::$app->request->post()) and ($order = $model->update())) {

            if (!empty($model->user_telesale_id)) {
                Yii::$app->session->setFlash('success', 'Đơn hàng đã được chỉnh sửa thành công.');
                return $this->redirect(['/user/user-telesale/index']);
            }

            $this->redirect(['default/view', 'id' => $order->id]);
        }

        return $this->render('index', [
            'model' => $model,
            'shippingMethods' => $shippingMethods,
            'paymentMethods' => $paymentMethods,
        ]);
    }

    public function loadModel($id)
    {
        $model = new UpdateOrderForm();
        $order = Order::findOne([
            'id' => $id,
            'status' => [
                Order::ORDER_STATUS_NEW,
                Order::ORDER_STATUS_PENDING_CONTACT,
                Order::ORDER_STATUS_PENDING_PAYMENT,
                Order::ORDER_STATUS_PAYMENT_FAILED,
            ]
        ]);
        if (!$order) {
            throw new InvalidParamException('Can\'t find an order with entered `orderId`');
        }
        $user = $order->user;
        $shippingAddress = $order->shippingAddress;

        $model->payment_method = $order->payment_method;
        $model->user_email = $user->email;
        if (!empty($shippingAddress)) {
            $model->shipping_contact_name = $shippingAddress->contact_name;
            $model->shipping_phone_number = $shippingAddress->phone_number;
            $model->shipping_street_address = $shippingAddress->street_address;
            $model->shipping_location_id = $shippingAddress->location_id;
            $model->note = $shippingAddress->note;
        }
        $courseIds = [];
        foreach ($order->details as $detail) {
            $course = $detail->course;
            $combo = $detail->combo;
            if ($combo) {
                $itemId = $combo->id;
            } else {
                $itemId = $course->id;
            }
            if (!in_array($itemId, $courseIds)) {
                $courseIds[] = $itemId;
            }
        }
        $model->course_list = implode(',', $courseIds);
        $model->promotion_code = $order->promotion_code;
        $model->user_telesale_id = $order->user_telesale_id;
        if (!empty($order->group_discount_amount) && $order->group_discount_amount > 0) {
            $model->group_discount = Order::BOOL_YES;
            $model->group_discount_amount = $order->group_discount_amount;
        }
        $model->order_id = $order->id;
        $model->order = $order;
        $model->activation_code = $order->activation_code;

        return $model;
    }

    public function actionGetUser($email, $view = 'editable')
    {
        $model = new UpdateOrderForm();

        if ($user = $this->getUser($email)) {
            $model->shipping_address = $user->address;
        }

        return $this->renderPartial('_user-'.$view, [
            'model' => $model,
        ]);
    }

    public function getUser($email)
    {
        return User::find()->where(['email' => $email])->one();
    }
}