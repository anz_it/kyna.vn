<?php

namespace app\modules\order\controllers;

use Yii;
use yii\filters\AccessControl;
use kyna\user\models\User;
use kyna\order\models\Order;
use app\modules\user\models\RegistrationUserForm;
use app\modules\user\models\EmailForm;
use kyna\order\models\OrderDetails;
use yii\data\ActiveDataProvider;
use kyna\user\models\Profile;
use yii\db\Query;
use kyna\course\models\Course;
class UserController extends \yii\web\Controller
{
    private $_form;
    private $_model;
    private $_view;

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index'],
                        'matchCallback' => function () {
                            return Yii::$app->user->can('Order.User.UpdateEmail');
                        },
                        
                    ],
                    [
                        'allow' => true,
                        'actions' => ['confirm'],
                        'matchCallback' => function () {
                            return Yii::$app->user->can('Order.User.UpdateEmail');
                        },
                    ],
                    [
                        'allow' => true,
                        'actions' => ['confirm-new'],
                        'matchCallback' => function () {
                            return Yii::$app->user->can('Order.User.UpdateEmail');
                        },
                    ],
                    [
                        'allow' => true,
                        'actions' => ['errors'],
                        'matchCallback' => function () {
                            return Yii::$app->user->can('Order.User.UpdateEmail');
                        },
                    ],
                ],
            ],
        ];
    }

    /**
     * Update Email User or Create new a user
     *
     * If count order of user only one then update email user .
     * Else create a new a user account within new email and We update all row user - courser
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $order_id = Yii::$app->request->get('id');
        $order = $this->findModel($order_id);
        $orderDetails = new OrderDetails();
        $dataProvider = new ActiveDataProvider([
            'query' => $orderDetails->find()->where(['order_id' => $order_id]),
        ]);
//        var_dump($order->isUpdateEmail());die;
        if($order->isUpdateEmail() == false){
            Yii::$app->session->setFlash('warning', "
                Đơn hàng này không ở trạng thái được đổi email.</br>
                - Hình thức COD: Đang giao hàng, Hoàn thành, Hủy </br>
                - Các hình thức khác (Thẻ cào, visa/master, ATM, Chuyển khoản): Hoàn thành, Hủy
                ");
            return $this->render('errors', [
                'id' => $order_id,
            ]);
        }
        if(!isset(Yii::$app->session['referrer']) || empty(Yii::$app->session['referrer']))
            Yii::$app->session['referrer'] = Yii::$app->request->referrer;



        if(isset($order->user_id) && !empty($order->user_id)){
            $this->_form = new EmailForm();
            if ($this->_form->load(Yii::$app->request->post())) {
                $email = $this->_form->email;
                if($this->checkUserOrderCouser($this->_form->email,$order_id) == false){
                        Yii::$app->session->setFlash('warning', "Email $email và  đơn hàng # $order_id có khóa học đã mua trong hệ thống bạn ko thể đổi Email ");
                        return Yii::$app->response->redirect(['order/user/index', 'id' => $order_id]);
                    }
                elseif(User::find()->where(['email' => $this->_form->email])->count() >0){
                    Yii::$app->session->setFlash('warning', "Email $email đã tồn tại trong hệ thống, bạn có muốn cập nhật email này theo đơn hàng # $order_id ?");
                    return $this->render('confirm', [
                        'model' => $this->_form,
                        'order' => $this->findModel($order_id),
                        'dataProvider' => $dataProvider,
                        'statusLabelCss' => Order::getStatusLabelCss(),
                    ]);
                }
                else{
                    $email = $this->_form->email;
                    $this->_form = new RegistrationUserForm();
                    $this->_form->email = $email;
                    return $this->render('regis', [
                        'model' => $this->_form,
                        'order' => $this->findModel($order_id),
                        'dataProvider' => $dataProvider,
                        'statusLabelCss' => Order::getStatusLabelCss(),
                    ]);
                }
            }

            return $this->render('index', [
                'model' => $this->_form,
                'order' => $this->findModel($order_id),
                'dataProvider' => $dataProvider,
                'statusLabelCss' => Order::getStatusLabelCss(),
            ]);

        }else{
            $this->_form = new RegistrationUserForm();
            return $this->render('regis', [
                'model' => $this->_form,
                'order' => $this->findModel($order_id),
                'dataProvider' => $dataProvider,
                'statusLabelCss' => Order::getStatusLabelCss(),
            ]);
        }

    }

    public function actionConfirmNew(){
        $order_id = Yii::$app->request->get('id');
        $order = $this->findModel($order_id);
        if($order->isUpdateEmail() == false){
            Yii::$app->session->setFlash('success', "
                Đơn hàng này không ở trạng thái được đổi email.</br>
                - Hình thức COD: Đang giao hàng, Hoàn thành, Hủy </br>
                - Các hình thức khác (Thẻ cào, visa/master, ATM, Chuyển khoản): Hoàn thành, Hủy
                ");
            return Yii::$app->response->redirect(['order/user/index', 'id' => $order_id]);
        }

        $orderDetails = new OrderDetails();
        $dataProvider = new ActiveDataProvider([
            'query' => $orderDetails->find()->where(['order_id' => $order_id]),
        ]);
        $this->_form = new RegistrationUserForm();
        /** @var User $user */
        $model = \Yii::createObject([
            'class'    => RegistrationUserForm::className(),
        ]);
        if ($this->_form->load(Yii::$app->request->post()) ) {

            if($this->checkUserOrderCouser($this->_form->email,$order_id) == false){
                $email = $this->_form->email;
                Yii::$app->session->setFlash('warning', "Email $email và  đơn hàng # $order_id có khóa học đã mua trong hệ thống bạn ko thể đổi Email ");
                return Yii::$app->response->redirect(['order/user/index', 'id' => $order_id]);
            }

            if(User::find()->where(['email' => $this->_form->email])->count() >0){
                $email = $this->_form->email;
                $this->_form = new EmailForm();
                $this->_form->email =$email;
                Yii::$app->session->setFlash('warning', "Email $email đã tồn tại trong hệ thống, bạn có muốn cập nhật email này theo đơn hàng # $order_id ?");
                return $this->render('confirm', [
                    'model' => $this->_form,
                    'order' => $this->findModel($order_id),
                    'dataProvider' => $dataProvider,
                    'statusLabelCss' => Order::getStatusLabelCss(),
                ]);
            }
            if ($this->_form->validate()){
                $model->load(Yii::$app->request->post());
                $user = $model->register();
                $this->saveUserProfile($user,$this->_form);

                $this->updateUserForOrder($user->id,$order_id);
                Yii::$app->session->setFlash('success', "Cập nhật Email cho Order #$order_id thành công.");
                return Yii::$app->response->redirect(Yii::$app->session['referrer']);
            }
        }

        return $this->render('regis', [
            'model' => $this->_form,
            'order' => $this->findModel($order_id),
            'dataProvider' => $dataProvider,
            'statusLabelCss' => Order::getStatusLabelCss(),
        ]);
    }

    public function actionConfirm(){
        $order_id = Yii::$app->request->get('id');
        $order = $this->findModel($order_id);
        if($order->isUpdateEmail() == false){
            Yii::$app->session->setFlash('success', "
                Đơn hàng này không ở trạng thái được đổi email.</br>
                - Hình thức COD: Đang giao hàng, Hoàn thành, Hủy </br>
                - Các hình thức khác (Thẻ cào, visa/master, ATM, Chuyển khoản): Hoàn thành, Hủy
                ");
            return Yii::$app->response->redirect(['order/user/index', 'id' => $order_id]);
        }
        $this->_form = new EmailForm();

        if ($this->_form->load(Yii::$app->request->post())) {

            if($this->checkUserOrderCouser($this->_form->email,$order_id) == false){
                $email = $this->_form->email;
                Yii::$app->session->setFlash('warning', "Email $email và  đơn hàng # $order_id có khóa học đã mua trong hệ thống bạn ko thể đổi Email ");
                return Yii::$app->response->redirect(['order/user/index', 'id' => $order_id]);
            }

            if ($this->_form->validate()){

                $user = User::find()->where(['email' => $this->_form->email])->one();
                if($user){
                    $new_user = $user->id;
                    $this->updateUserForOrder($new_user,$order_id);
                    Yii::$app->session->setFlash('success', "Cập nhật Email cho Order #$order_id thành công.");
                    return Yii::$app->response->redirect(Yii::$app->session['referrer']);
                }else{

                    $orderDetails = new OrderDetails();
                    $dataProvider = new ActiveDataProvider([
                        'query' => $orderDetails->find()->where(['order_id' => $order_id]),
                    ]);
                    $email = $this->_form->email;
                    $this->_form = new RegistrationUserForm();
                    $this->_form->email = $email;
                    return $this->render('regis', [
                        'model' => $this->_form,
                        'order' => $this->findModel($order_id),
                        'dataProvider' => $dataProvider,
                        'statusLabelCss' => Order::getStatusLabelCss(),
                    ]);
                }
            }
        }
    }

    protected function findModel($id)
    {
        if (($model = Order::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested order does not exist.');
        }
    }

    protected function updateUserForOrder($user_id,$order_id){
        $user = Order::updateAll(
            ['user_id'=>$user_id],
            "id = $order_id"
        );
        return $user;
    }

    public function saveUserProfile($user,$form){
        $profile = $user->profile;
        $profile->name = $form->name;
        $profile->phone_number = $form->phonenumber;
        $profile->save(true);
    }


    public function checkUserOrderCouser($email,$orderId){
        $connection = \Yii::$app->db;

        $model = $connection->createCommand("
            SELECT COUNT(*) as total FROM user
                LEFT JOIN user_courses ON user.`id` = user_courses.`user_id`
                WHERE user.`email` = '$email'
                AND user_courses.`course_id` IN (
                    SELECT od.`course_id`
                    FROM order_details od
                    WHERE (od.`order_id` = $orderId)
                )

          ");
        $data = $model->queryOne();

        if($data['total']== 0){
            return true;
        }else{
            return false;
        }

    }

}
