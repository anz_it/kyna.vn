<?php
namespace app\modules\order\controllers;
use Yii;
use yii\data\ArrayDataProvider;
use backend\modules\order\models\Banking;
use yii\filters\AccessControl;

/**
 * DefaultController implements the CRUD actions for Order model.
 */
class BankingTransferController extends \yii\web\Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index'],
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->user->can('Order.TransactionLog');
                        },
                    ],
                ],
            ],
        ];
    }
   public function actionIndex()
   {
       //1361923200
        $model = new Banking();
        $request = Yii::$app->request;
        $list_charging = $model->getListCharging();
        if($request->post())
        {
            $username = $email = $phone_number = $trans_date_begin = $trans_date_end = $filter = NULL;
            $form_data = $request->post()['Banking'];
            $username = $form_data['username'];
            $email = $form_data['email'];
            $phone_number = $form_data['phone_number'];
            $trans_date_begin = strtotime(date('Y-m-d',strtotime($form_data['trans_date_begin'])));
            $trans_date_end = strtotime(date('Y-m-d',strtotime($form_data['trans_date_end'])));
            $filter = $form_data['filter'];
            $data = $model->searchTranferLog($username , $email , $phone_number , $trans_date_begin , $trans_date_end , $filter);
        }
        else
        {
            $data = $model->getDataTranfer();
            /*$user_add_tranfer = array();
            foreach($data as $key => $val)
            {
                if($val['course_combo_id'] != 0)
                {
                    $data[$key]['course_name'] = $model->getCourseName($val['course_combo_id']);
                }
                else
                {
                    $data[$key]['course_name'] = $model->getCourseName($val['course_id']);
                }
                if($val['operator_id'] != NULL)
                {
                    $data[$key]['user_add_tranfer'] = $model->getUserAddTranfer($val['operator_id']);
                }
                else
                {
                    $data[$key]['user_add_tranfer'] = '';
                }
            }*/
        }
        $dataprovider = new ArrayDataProvider([
               'allModels' => $data,
               'pagination' => ['pageSize' => 20],
           ]);
        return $this->render('index',['model' => $model , 'dataProvider' => $dataprovider , 'list_charging' => $list_charging]);
    }
}
