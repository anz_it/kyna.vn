<?php

namespace app\modules\order\controllers;

use Yii;
use yii\web\NotFoundHttpException;
use yii\helpers\Url;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\web\Response;

use app\modules\order\models\OrderSearch;
use app\modules\order\models\ActionForm;
use app\modules\order\models\RevenueSearch;

use common\helpers\StringHelper;
use kyna\order\models\Order;
use kyna\order\models\OrderDetails;
use kyna\user\models\User;
use kyna\user\models\Address;
use kyna\order\models\actions\OrderAction;


/**
 * DefaultController implements the CRUD actions for Order model.
 */
class ExportController extends \yii\web\Controller
{

    public function behaviors()
    {
        return [
             'access' => [
                 'class' => AccessControl::className(),
                 'rules' => [
                     [
                         'allow' => true,
                         'matchCallback' => function ($rule, $action) {
                             return Yii::$app->user->can('Order.Export');
                         },
                     ],
                 ],
             ],
        ];
    }

    /**
     * Order.View.COMPLETE
     * Order.View.DRAFT
     * Order.View.NEW
     * Order.View.PENDING_PAYMENT
     * Order.View.DELIVERING
     * Order.View.COMPLETE
     * Order.View.All.
     */
    public function extractPermission($userId)
    {
        $orderClass = Order::className();
        $permissions = Yii::$app->authManager->getPermissionsByUser($userId);

        $allowedStatus = [];
        $allowedAction = [];
        $canViewAll = false;

        foreach ($permissions as $permissionName => $permissions) {
            if ($permissionName === 'Order.View.All') {
                $canViewAll = true;
            }

            $permissionName = strtoupper($permissionName);

            if (StringHelper::startWith('ORDER.VIEW.', $permissionName)) {
                $permissionName = str_replace('ORDER.VIEW.', '', $permissionName);
                if (defined($orderClass . '::ORDER_STATUS_' . $permissionName)) {
                    $allowedStatus[] = constant($orderClass . '::ORDER_STATUS_' . $permissionName);
                }
            }
            if (StringHelper::startWith('ORDER.ACTION.', $permissionName)) {
                $permissionName = str_replace('ORDER.ACTION.', '', $permissionName);
                $allowedAction[] = $permissionName;
            }
        }

        $defaultStatus = array_diff($allowedStatus, [
            Order::ORDER_STATUS_DRAFT,
            Order::ORDER_STATUS_COMPLETE,
            Order::ORDER_STATUS_IN_COMPLETE,
            Order::ORDER_STATUS_CANCELLED,
        ]);

        return [
            $defaultStatus,
            $allowedStatus,
            $allowedAction,
            $canViewAll,
        ];
    }

    public function actionIndex()
    {
        ini_set('max_execution_time', 0);
        set_time_limit(0);
        $userId = Yii::$app->user->identity->id;

        list($defaultStatus, $allowedStatus, $allowedAction, $canViewAll) = $this->extractPermission($userId);
        $defaultStatus = $allowedStatus;
        $this->loadParams($userId, $params, $allowedStatus, $defaultStatus);

        $searchModel = new RevenueSearch();
        $searchModel->attributes = $params;
        $searchModel->allowedStatus = $allowedStatus;
        $dataProvider = $searchModel->searchExport($params);
        $newOrderCount = $dataProvider->totalCount;

        if (Yii::$app->request->isAjax) {
            // init pagination for raw SQL
            $sql = $dataProvider->query->createCommand()->getRawSql();
            //$page = (Yii::$app->request->get('page')) ? Yii::$app->request->get('page') : 1;
            //$limit = (Yii::$app->request->get('per-page')) ? Yii::$app->request->get('per-page') : 200;
            //$offset = $limit * ($page - 1);
            //$sql = str_replace('`', '', $sql) . " LIMIT {$limit} OFFSET {$offset}";
            $sql = str_replace('`', '', $sql);
            $email = empty(Yii::$app->request->post('email'))?Yii::$app->user->identity->email:Yii::$app->request->post('email');
            // execute command in console
            $rootPath = \Yii::getAlias('@root');
            $exportLog = \Yii::getAlias('@console/runtime/export.log');
            $exportErrorLog = \Yii::getAlias('@console/runtime/export-error.log');
            // check log file exist
            self::_checkExistFile($exportLog);
            self::_checkExistFile($exportErrorLog);
            $command = "php {$rootPath}/yii export/run order-report \"{$sql}\" {$email}" . " > {$exportLog} 2>>{$exportErrorLog} &";
            exec($command);
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'result' => true,
            ];
        }

        return $this->render('index', [
            'queryParams' => $params,
            'dataProvider' => $dataProvider,
            'tabs' => $this->loadTabs(),
            'statusLabelCss' => $this->_statusLabelCss(),
            'callStatuses' => OrderAction::callStatuses(),
            'allowedStatus' => $allowedStatus,
        ]);
    }

    public function actionWaitProcess()
    {
        $userId = Yii::$app->user->identity->id;
        $params['operator_id'] = $userId;

        list($defaultStatus, $allowedStatus, $allowedAction, $canViewAll) = $this->extractPermission($userId);
        $this->loadParams($userId, $params, $allowedStatus, $defaultStatus);

        $finishStatus = [
            Order::ORDER_STATUS_CANCELLED,
            Order::ORDER_STATUS_COMPLETE,
        ];
        foreach ($allowedStatus as $key => $fStatus) {
            if (in_array($fStatus, $finishStatus)) {
                unset($allowedStatus[$key]);
            }
        }

        $searchModel = new OrderSearch();
        $searchModel->attributes = $params;
        $searchModel->allowedStatus = $allowedStatus;
        $dataProvider = $searchModel->search();
        $newOrderCount = $dataProvider->totalCount;

        return $this->render('index', [
            'queryParams' => $params,
            'dataProvider' => $dataProvider,
            'tabs' => $this->loadTabs(),
            'statusLabelCss' => $this->_statusLabelCss(),
            'callStatuses' => OrderAction::callStatuses(),
            'allowedStatus' => $allowedStatus,
        ]);
    }

    public function actionByOperator()
    {
        $userId = Yii::$app->user->id;
        $params['operator_id'] = $userId;

        list($defaultStatus, $allowedStatus, $allowedAction, $canViewAll) = $this->extractPermission($userId);
        $defaultStatus = [
            Order::ORDER_STATUS_CANCELLED,
            Order::ORDER_STATUS_COMPLETE,
        ];

        $this->loadParams($userId, $params, $allowedStatus, $defaultStatus);

        $searchModel = new OrderSearch();
        $searchModel->attributes = $params;
        $searchModel->allowedStatus = $allowedStatus;
        $dataProvider = $searchModel->search();

        return $this->render('index', [
            'queryParams' => $params,
            'dataProvider' => $dataProvider,
            'tabs' => $this->loadTabs(),
            'statusLabelCss' => $this->_statusLabelCss(),
            'callStatuses' => OrderAction::callStatuses(),
            'allowedStatus' => $defaultStatus,
        ]);
    }

    public function actionAffiliate()
    {
        $userId = Yii::$app->user->identity->id;
        $params['affiliate_id'] = $userId;

        if (Yii::$app->request->get('date-range')) {
            $dateRange = explode(' - ', Yii::$app->request->get('date-range'));
            $beginDate = date_create_from_format('d/m/Y', $dateRange[0]);
            $endDate = date_create_from_format('d/m/Y', $dateRange[1]);

            $params['from_date'] = $beginDate->getTimestamp();
            $params['to_date'] = $endDate->getTimestamp();
        }
        $params['call_status'] = Yii::$app->request->get('call_status');

        $searchModel = new OrderSearch();
        $searchModel->attributes = $params;
        $dataProvider = $searchModel->search();

        return $this->render('index', [
            'queryParams' => $params,
            'dataProvider' => $dataProvider,
            'tabs' => $this->loadTabs(),
            'statusLabelCss' => $this->_statusLabelCss(),
            'callStatuses' => OrderAction::callStatuses(),
            'allowedStatus' => Order::getAllStatuses()
        ]);
    }

    public function actionLearner()
    {
        $params = [
            'completeOnly' => Yii::$app->request->get('completeOnly'),
            'ids' => Yii::$app->request->get('ids', []),
        ];

        $ids = Yii::$app->request->get('ids', []);
        $completeOnly = explode(',', Yii::$app->request->get('completeOnly'));

        $orderQuery = Order::find()->orderBy(['created_time' => SORT_DESC])
            ->innerJoin('order_details', 'orders.id = order_details.order_id');

        // condition with export all
        $courseType = Yii::$app->request->post('course_type', 'filter');
        if (Yii::$app->request->post('type', null) != 'export' || $courseType == 'filter') {
            $orderQuery->where([
                'or', ['course_combo_id' => $ids], [
                    'course_id' => $ids,
                    'course_combo_id' => 0,
                ]
            ]);
        }

        if ($completeOnly) {
            $orderQuery->andWhere([
                'status' => Order::ORDER_STATUS_COMPLETE,
            ]);
        }

        if ($range = Yii::$app->request->get('date-range')) {
            $dateRange = explode(' - ', $range);
            $beginDate = date_create_from_format('d/m/Y H:i', $dateRange[0] . ' 00:00');
            $endDate = date_create_from_format('d/m/Y H:i', $dateRange[1] . ' 23:59');

            $params['from_date'] = $beginDate->getTimestamp();
            $params['to_date'] = $endDate->getTimestamp();
            $params['date-range'] = $range;
        }

        if (!isset($params['to_date'])) {
            $params['to_date'] = strtotime('tomorrow 0:00');
        }
        if (!isset($params['from_date'])) {
            $params['from_date'] = $params['to_date'] - 24 * 7 * 3600;
        }

        if (isset($params['to_date']) and isset($params['from_date'])) {
            $orderQuery->andWhere(['between', 'created_time', $params['from_date'], $params['to_date']]);
        }

        // group by order details
        $orderQuery->groupBy('order_details.order_id');

        $dataProvider = new ActiveDataProvider([
            'query' => $orderQuery,//->select('orders.id, user_id, status, affiliate_id'),
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);

        if (Yii::$app->request->isAjax && Yii::$app->request->post('type', null) == 'export') {
            // init pagination for raw SQL
            $sql = $dataProvider->query->createCommand()->getRawSql();
            $sql = str_replace('`', '', $sql);
            $email = empty(Yii::$app->request->post('email'))?Yii::$app->user->identity->email:Yii::$app->request->post('email');
            // execute command in console
            $rootPath = \Yii::getAlias('@root');
            $exportLog = \Yii::getAlias('@console/runtime/export_learner.log');
            $exportErrorLog = \Yii::getAlias('@console/runtime/export_learner-error.log');
            // check log file exist
            self::_checkExistFile($exportLog);
            self::_checkExistFile($exportErrorLog);
            $command = "php {$rootPath}/yii export/run learner \"{$sql}\" {$email}" . " > {$exportLog} 2>>{$exportErrorLog} &";
            exec($command);
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'result' => true,
            ];
        }

        return $this->render('learner', [
            'dataProvider' => $dataProvider,
            'queryParams' => $params,
        ]);
    }

    private function loadParams($userId, &$params, $allowedStatus = [], $defaultStatus = false)
    {
        if ($status = Yii::$app->request->get('status')) {
            $params['status'] = Yii::$app->request->get('status');
        } else {
            $statusBy = false;
            if ($search = Yii::$app->request->get('s')) {
                if ($userId = $this->_detectUser($search)) {
                    $params['user_id'] = $userId;
                } elseif (StringHelper::typeDetect($search) === StringHelper::STRING_TYPE_PHONE_NUMBER) {
                    $params['phone_number'] = $search;
                } else {
                    $params['search'] = $search;
                }
                $statusBy = 'allowedStatus';
            } else {
                $statusBy = 'defaultStatus';
            }

            if ($status and in_array($status, $allowedStatus)) {
                $status = explode(',', $status);
                $statusBy = 'status';
            }

            $params['status'] = ${$statusBy};
        }

        if (Yii::$app->request->get('date-range')) {
            $dateRange = explode(' - ', Yii::$app->request->get('date-range'));
            $beginDate = date_create_from_format('d/m/Y H:i', $dateRange[0] . ' 00:00');
            $endDate = date_create_from_format('d/m/Y H:i', $dateRange[1] . ' 23:59');
            //$beginDate = date_create_from_format('d/m/Y', $dateRange[0]);
            //$endDate = date_create_from_format('d/m/Y', $dateRange[1]);

            $params['from_date'] = $beginDate->getTimestamp();
            $params['to_date'] = $endDate->getTimestamp();
            $params['date-range'] = Yii::$app->request->get('date-range');
        }

        $params['call_status'] = Yii::$app->request->get('call_status');
    }

    private function loadTabs()
    {
        $tabsItems = [
            [
                'label' => 'Chờ xử lý',
                'url' => Url::toRoute(['/order/default/wait-process']),
                'active' => $this->action->id == 'wait-process',
                'encode' => false,
            ],
            [
                // Danh sách các đơn hàng mà operator hiện tại là A
                'label' => 'Tôi đã xử lý',
                'url' => Url::toRoute(['/order/default/by-operator']),
                'active' => $this->action->id == 'by-operator',
                'encode' => false,
            ],
            [
                // Danh sách các đơn hàng mà operator hiện tại là A
                'label' => 'Tôi đã giới thiệu',
                'url' => Url::toRoute(['/order/default/affiliate']),
                'active' => $this->action->id == 'affiliate',
                'encode' => false,
            ],
            [
                'label' => 'Tất cả đơn hàng',
                'url' => Url::toRoute(['/order/default/index']),
                'active' => $this->action->id == 'index',
                'encode' => false,
            ],
        ];

        if (Yii::$app->user->can('Order.Action.Create')) {
            $tabsItems[] = [
                'label' => '<i class="icon ion-android-add"></i> Tạo đơn hàng',
                'url' => Url::toRoute(['/order/create']),
                'encode' => false,
            ];
        }

        return $tabsItems;
    }

    public function actionView($id)
    {
        $orderDetails = new OrderDetails();
        $dataProvider = new ActiveDataProvider([
            'query' => $orderDetails->find()->where(['order_id' => $id]),
        ]);

        return $this->render('view', [
            'model' => $this->findModel($id),
            'dataProvider' => $dataProvider,
            'statusLabelCss' => $this->_statusLabelCss(),
            //'actionDataProvider' =>
        ]);
    }

    public function actionHistory($id)
    {
        $model = $this->findModel($id);
        $name = \Yii::$app->request->get('name');
        $dataProvider = $model->actionHistory($name);
        $availableActions = ['' => 'Tất cả'] + ActionForm::getEnableActions($id);

        return $this->render('history', [
            'actionDataProvider' => $dataProvider,
            'model' => $model,
            'availableActions' => $availableActions,
            'actionName' => $name,
        ]);
    }

    protected function findModel($id)
    {
        if (($model = Order::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    private function _statusLabelCss()
    {
        return [
            Order::ORDER_STATUS_CANCELLED => 'label-default',
            Order::ORDER_STATUS_RETURN => 'label-default',
            // temporary on admin only
            Order::ORDER_STATUS_DRAFT => 'label-default',
            // newly created order (shopping cart)
            Order::ORDER_STATUS_NEW => 'label-warning',
            // when user decide to choose an online payment method,
            // wait for response from payment gateway
            Order::ORDER_STATUS_PENDING_PAYMENT => 'label-info', // Telesale only
            Order::ORDER_STATUS_PENDING_CONTACT => 'label-warning', // Telesale only
            Order::ORDER_STATUS_IN_COMPLETE => 'label-warning', // Telesale only
            // sent to logistic service already
            Order::ORDER_STATUS_DELIVERING => 'label-info',
            // payment completed & service delivered
            Order::ORDER_STATUS_COMPLETE => 'label-success',
        ];
    }

    private function _detectUser($string)
    {
        //$userQuery = User::find();
        $stringType = StringHelper::typeDetect($string);
        switch ($stringType) {
            case StringHelper::STRING_TYPE_EMAIL:
                $params['email'] = $string;
                if ($user = User::find()->where($params)->one()) {
                    return $user->id;
                }

                return false;
            case StringHelper::STRING_TYPE_PHONE_NUMBER:
                $params['phone_number'] = $string;
                if ($userAddress = Address::find()->where($params)->one()) {
                    return $userAddress->user_id;
                }

                return false;
            default:
                return false;
        }
    }

    private function _checkExistFile($file)
    {
        clearstatcache();
        if (!file_exists($file)) {
            touch($file);
        }
    }

}
