<?php

namespace app\modules\order\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;

use kyna\user\models\User;
use kyna\order\models\ShippingMethod;
use kyna\user\models\UserTelesale;

use app\modules\order\models\CreateOrderForm;

class CreateController extends \yii\web\Controller
{
    
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index'],
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->user->can('Order.Action.Create');
                        },
                    ],
                ],
            ],
        ];
    }
    
    /**
     * Creates a new Order model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $model = $this->loadModel();

        $paymentMethods = [
            'cod' => 'Cash on Delivery (COD)',
            'auto' => 'Thanh toán Online/chuyển khoản',
        ];

        $shippingMethods = ['' => ''] + ArrayHelper::map(ShippingMethod::getAvailable(), 'id', 'name');

        $model->operator_id = \Yii::$app->user->id;
        if (!empty(Yii::$app->params['campaign_discount_group']) && Yii::$app->params['campaign_discount_group']) {
            $model->group_discount = true;
        }
        if ($model->load(\Yii::$app->request->post()) and ($order = $model->create())) {

            if (!empty($model->user_telesale_id)) {
                Yii::$app->session->setFlash('success', 'Đơn hàng đã được tạo thành công.');
                return $this->redirect(['/user/user-telesale/index']);
            }
            
            $this->redirect(['default/view', 'id' => $order->id]);
        }

        return $this->render('index', [
            'model' => $model,
            'shippingMethods' => $shippingMethods,
            'paymentMethods' => $paymentMethods,
        ]);
    }
    
    public function loadModel()
    {
        $model = new CreateOrderForm();

        if ($userTelId = Yii::$app->request->get('user_tel_id')) {
            $userTelesale = UserTelesale::findOne($userTelId);
            if (is_null($userTelesale)) {
                throw new NotFoundHttpException();
            }
            
            $model->user_email = $userTelesale->email;
            $model->shipping_contact_name = $userTelesale->full_name;
            $model->shipping_phone_number = $userTelesale->phone_number;
            $model->shipping_street_address = $userTelesale->street_address;
            $model->shipping_location_id = $userTelesale->location_id;
           
            $model->user_telesale_id = $userTelesale->id;
        }
        
        return $model;
    }

    public function actionGetUser($email, $view = 'editable')
    {
        $model = new CreateOrderForm();

        if ($user = $this->getUser($email)) {
            $model->shipping_address = $user->address;
        }

        return $this->renderPartial('_user-'.$view, [
            'model' => $model,
        ]);
    }

    public function getUser($email)
    {
        return User::find()->where(['email' => $email])->one();
    }
}
