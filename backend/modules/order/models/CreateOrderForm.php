<?php

namespace app\modules\order\models;


use common\lib\memoize\Memoize;
use kyna\partner\models\Code;
use kyna\payment\models\PaymentMethod;
use kyna\promo\models\GroupDiscount;
use kyna\promotion\models\PromoCodeForm;
use kyna\promotion\models\UserVoucherFree;
use Yii;
use yii\base\InvalidValueException;
use yii\helpers\ArrayHelper;

use kyna\settings\models\Setting;
use kyna\course\models\Course;
use kyna\order\models\Order;
use kyna\order\models\OrderQueue;
use kyna\user\models\Address;
use kyna\user\models\UserCourse;
use kyna\commission\models\AffiliateUser;
use kyna\user\models\UserTelesale;
use kyna\commission\Affiliate;
use kyna\promo\models\GroupDiscountCourse;
use kyna\promotion\models\Promotion;
use kyna\user\models\User;

class CreateOrderForm extends \yii\base\Model
{

    public $user_email;
    public $direct_discount_amount;
    public $group_discount;
    public $payment_method;
    public $course_list;
    public $shipping_contact_name;
    public $shipping_phone_number;
    public $shipping_street_address;
    public $shipping_location_id;
    public $operator_id;
    public $group_discount_amount;
    public $user_telesale_id;
    public $activation_code;
    public $note;
    public $group_discount_id;
    public $promotion_code;
    public $_promotion;
    public $settings;
    public $is_4kid;
    public $auto_password;

    public function scenarios()
    {
        return [
            'default' => [
                'user_email',
                'direct_discount_amount',
                'group_discount_amount',
                'group_discount',
                'course_list',
                'payment_method',
                'shipping_contact_name',
                'shipping_phone_number',
                'shipping_street_address',
                'shipping_location_id',
                'operator_id',
                'point_of_sale',
                'activation_code',
                'note',
                'group_discount_id',
                'promotion_code',
                'is_4kid',
                'auto_password'
            ],
        ];
    }

    public function rules()
    {
        return [
            ['user_email', 'email'],
            ['operator_id', 'integer'],
            [['direct_discount_amount', 'group_discount_amount'], 'number'],
            [['direct_discount_amount', 'group_discount_amount', 'is_4kid'], 'default', 'value' => 0],
            ['group_discount', 'boolean'],
            ['shipping_method', 'string', 'max' => 20],
            ['activation_code', 'string', 'max' => 50],

            [['shipping_contact_name', 'shipping_phone_number', 'shipping_street_address', 'shipping_location_id'], 'required', 'when' => function ($model) {
                return $model->payment_method == 'cod';
            }, 'whenClient' => 'function (attribute, value) {
                return $(\'[name*="[payment_method]"]\').val() == "cod";
            }', 'message' => 'Thông tin này bắt buộc nếu đơn hàng là COD'],
            [['note', 'promotion_code'], 'string'],
            [['user_email', 'course_list'], 'required'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'user_email' => 'Email học viên',
            'direct_discount_amount' => 'Giảm giá trực tiếp',
            'group_discount' => 'Giảm giá theo nhóm',
            'group_discount_amount' => 'Mức giảm giá theo nhóm',
            'payment_method' => 'Hình thức thanh toán',
            'order_details' => 'Danh sách khóa học',
            'shipping_vendor_id' => 'Dịch vụ giao nhận',
            'course_list' => 'Danh sách đăng ký',
            'subtotal' => 'Thành tiền',
            'shipping_contact_name' => 'Tên người nhận',
            'shipping_phone_number' => 'Số điện thoại',
            'activation_code' => 'Code hoặc số Series [Dùng Tham Chiếu]',
            'shipping_street_address' => 'Địa chỉ',
            'shipping_location_id' => 'Địa phương',
            'note' => 'Ghi chú',
            'promotion_code' => 'Mã Coupon/Voucher',
            'is_4kid' => 'Loại ForKid',
            'auto_password' => 'Tạo mật khẩu mặc định'
        ];
    }


    public function create()
    {
        /* Validate user before create */
        if (!$this->user_email) {
            return false;
        }

        $user = $this->findUser($this->user_email, $this->auto_password);

        if (!$user) {
            throw new InvalidValueException("Can't create user with this email");
        } else {
            // check duplicate user courses
            $courseIds = explode(',', $this->course_list);
            $removedCourses = [];

            foreach ($courseIds as $key => $courseId) {
                $course = Course::findOne($courseId);
                if (empty($course)) {
                    unset($courseIds[$key]);
                    continue;
                }
                $alreadyCourses = [];
                $removed = false;

                if ($course->type == Course::TYPE_COMBO) {
                    foreach ($course->comboItems as $item) {
                        if (UserCourse::find()->where(['user_id' => $user->id, 'course_id' => $item->course_id])->exists()) {
                            $removed = true;
                            $alreadyCourses[] = $item->course->name;
                        }
                    }
                } else {
                    if (UserCourse::find()->where(['user_id' => $user->id, 'course_id' => $course->id])->exists()) {
                        $removed = true;
                    }
                }

                if ($removed) {
                    unset($courseIds[$key]);
                    $removedCourses[] = "<b>{$course->name}</b>" . (!empty($alreadyCourses) ? "(đã học: <b>" . implode(', ', $alreadyCourses) . "</b>)" : '');
                }
            }

            if (!empty($removedCourses)) {
                $this->course_list = implode(',', $courseIds);

                Yii::$app->session->setFlash('warning', "Đã loại bỏ các khóa học sau: " . implode(', ', $removedCourses) . ", học viên <b>{$user->profile->name}</b> đã tham gia các khóa học này.");
                return false;
            }
        }

        // validate promotion code
        /* if (!empty($this->promotion_code) && !$this->validatePromotionCode()) {
             return false;
         }*/

        /* Start creating order */
        $order = Order::createEmpty(Order::SCENARIO_CREATE_BACKEND);
        $order->on(Order::EVENT_ORDER_CREATED, [$this, 'pushToQueue']);

        $order->direct_discount_amount = $this->direct_discount_amount;
        $order->activation_code = $this->activation_code;
        if ($this->group_discount) {
            $order->group_discount_amount = $this->group_discount_amount;
        }
        $order->payment_method = $this->payment_method;
        $order->user_id = $user->id;
        $order->reference_id = $this->operator_id;
        $order->operator_id = $this->operator_id;
        $order->is_done_telesale_process = Order::BOOL_YES;
        $order->is_4kid = $this->is_4kid;

        $affiliateId = Yii::$app->user->id;

        if (!empty($this->user_telesale_id)) {
            $userTelesale = UserTelesale::findOne($this->user_telesale_id);
            if (!empty($userTelesale->affiliate_id)) {
                $affiliateId = $userTelesale->affiliate_id;
            }
            if (!empty($userTelesale->email))
                $order->point_of_sale = Order::POINT_OF_SALE_LANDING_PAGE;
            $order->user_telesale_id = $this->user_telesale_id;
        }

        if ($order->isCod and $address = $this->updateUserAddress($user->id)) {
            $order->shippingAddress = !empty($this->note) ? ($address->attributes + ['note' => $this->note]) : $address;
        }

        $affiliateUser = AffiliateUser::find()->where(['user_id' => $affiliateId, 'status' => AffiliateUser::STATUS_ACTIVE])->one();
        if (!empty($affiliateUser)) {
            // set affiliate
            $order->affiliate_id = $affiliateUser->user_id;
        }

        // save order details
        $courseIds = explode(',', $this->course_list);
        $products = Product::find()->where(['id' => $courseIds])->indexBy('id')->all();
        if(!empty($this->promotion_code)) {
            $result = $this->applyPromotionCode($products, $this->promotion_code, $this->user_email);

            if (!empty($result['errors'])) {
                $errorMessage = [];
                foreach ($result['errors'] as $error) {
                    foreach ($error as $mesage) {
                        $errorMessage[] = $mesage;
                    }
                }
                $errors = implode(", ", $errorMessage);
                Yii::$app->session->setFlash('warning', $errors);
                return false;
            }
            $products = $result['courses'];
            if (!empty($result['promotion_code'])) {
                $order->promotion_code = $this->promotion_code;
            }
        }

        $details = $this->prepareOrderItems($products);
        $order->addDetails($details);
        if ($this->validate() && $order->save()) {
            if (!empty($this->user_telesale_id)) {
                if (!is_null($userTelesale) && ($userTelesale->email == null || $userTelesale->email === $user->email)) {
                    $userTelesale->scenario = UserTelesale::SCENARIO_CREATE_ORDER;
                    $userTelesale->note = $this->payment_method == 'cod' ? 'Chuyển đơn COD' : 'Chuyển đơn hàng thanh toán Online/Chuyển khoản';
                    $userTelesale->amount = $order->total - $order->shipping_fee;
                    $userTelesale->user_id = $user->id;
                    $userTelesale->save(false);
                }
            }
            // apply promotion code if any
            $order = Order::findOne($order->id);
            if (!empty($this->promotion_code)) {
                Order::applyPromotion($order->id, $this->promotion_code);
            }
            return $order;
        }

        return false;
    }


    protected function prepareOrderItems($products)
    {


        if (!empty($this->group_discount_id) && $this->group_discount) {
            $groupDiscount = GroupDiscount::findOne(['id' => $this->group_discount_id]);
        }
        $addItems = [];
        $checkCampaign = 0;
        $prevCampaign = null;
        /* @var $course Course */
        foreach ($products as $course) {

            if ($course->type == Course::TYPE_COMBO) {

                $total_combo_price =  $course->getOriginalPrice() - $course->discountAmount ; //tông gia combo da giam chua ap dung
                $comboItems = $course->comboItems;
                foreach ($comboItems as $item) {
                    if (!array_key_exists($item->course_id, $addItems)) {
                        $pItem = Product::findOne($item->course_id);
                        $pItem->setCombo($course);
                        $discountAmount = $pItem->price - $item->price;
                        if($total_combo_price > 0) {
                            $pItem->setVoucherDiscountAmount($course->voucherDiscountAmount * ($item->price / $total_combo_price));
                        }
                        else{
                            $pItem->setVoucherDiscountAmount(0);
                        }
                        $pItem->setDiscountAmount($discountAmount + $pItem->getVoucherDiscountAmount());

                        $pItem->setPromotionCode($course->getPromotionCode());
                        $addItems[$pItem->id] = $pItem;
                    }
                }
            } else {
                // if course is gift
               /* if ($course->getIsCampaign11()) {
                    $checkCampaign++;
                    if ($checkCampaign == 2) {
                        $course->price_discount = $course->getDiscountAmount() + $course->sellPrice / 2;
                        $prevCampaign->price_discount = $prevCampaign->getDiscountAmount() + $prevCampaign->sellPrice / 2;
                        $addItems[$prevCampaign->id] = $prevCampaign;
                        $checkCampaign = 0;
                    }
                    $prevCampaign = $course;
                }
                if ($course->getIsCampaignGroupDiscount()) {
                    if (!empty($groupDiscount)) {
                        $course->setGroupDiscount($groupDiscount);
                    }
                }*/
                $new_item = clone $course;
                $new_item->discountAmount = $course->discountAmount + $course->voucherDiscountAmount;
                $addItems[$course->id] = $new_item;
            }
        }

        return $addItems;
    }

    protected function updateUserAddress($userId)
    {
        if (!$address = Address::find()->where(['user_id' => $userId])->one()) {
            $address = new Address();
            $address->user_id = $userId;
        }

        $address->attributes = [
            'email' => $this->user_email,
            'contact_name' => $this->shipping_contact_name,
            'phone_number' => $this->shipping_phone_number,
            'street_address' => $this->shipping_street_address,
            'location_id' => $this->shipping_location_id,
        ];

        if (!$address->save()) {
            $this->errors = $address->errors;
            return false;
        }

        return $address;
    }

    protected function findUser($email,$auto_password)
    {
        $user = User::find()->where(['email' => $email])->one();

        if (!$user) {
            $user = new User([
                'scenario' => 'create',
            ]);
            $time = time();
            if($auto_password == 0){
                $password = (string)$time;
            }else{
                $password = '12345678';
            }
            $user->load(['User' => [
                'email' => $email,
                'username' => $email,
                'created_at' => $time,
                'password' => $password
            ]]);
            // update meta fields
            $user = $this->beforeCreateUser($user);
            if ($user->create()) {
                // register profile
                $this->afterCreateUser($user);
            }
        }

        return $user;
    }

    /**
     * Update meta fields
     * @param $user
     * @return mixed
     */
    public function beforeCreateUser($user)
    {
        $user->phone = $this->shipping_phone_number;
        $user->address = $this->shipping_street_address;
        $user->location_id = $this->shipping_location_id;
        return $user;
    }

    /**
     * Update profile if Telesale user
     * @param $user
     */
    public function afterCreateUser($user)
    {
        $profile = $user->profile;
        $profile->public_email = $this->user_email;
        $profile->name = $this->shipping_contact_name;
        $profile->phone_number = $this->shipping_phone_number;
        $profile->location = $this->shipping_location_id;
        $profile->save(false);
    }

    public function pushToQueue($actionEvent)
    {
        $order = $actionEvent->sender;
        OrderQueue::pushToQueue($order);
    }

    /**
     * Check if all course is partner
     * @return bool
     */
    public function isPartnerCourse()
    {
        $courseIds = explode(',', $this->course_list);
        $courses = Course::find()->where(['id' => $courseIds])->all();
        foreach ($courses as $course) {
            if (empty($course) || !($course->isPartner || $course->isComboPartner)) {
                return false;
            }
        }
        return true;
    }

    /**
     * Get partner of list course, if exist course without partner => return null
     * @return mixed|null
     */
    public function getPartner()
    {
        $partner = null;
        if ($this->isPartnerCourse()) {
            $courseIds = explode(',', $this->course_list);
            $course = Course::findOne(end($courseIds));
            if (!empty($course) && ($course->isPartner || $course->isComboPartner)) {
                $partner = $course->isComboPartner ? $course->comboItems{0}->course->partner : $course->partner;
            }
        }

        return $partner;
    }

    /**
     * Get Payment method
     * @return array|null|\yii\db\ActiveRecord
     */
    public function getPaymentMethod()
    {
        return PaymentMethod::find()->where(['class' => $this->payment_method])->one();
    }

    /**
     * Check if payment method is COD
     * @return bool
     */
    public function getIsCod()
    {
        $paymentMethod = $this->paymentMethod;
        return !empty($paymentMethod) ? $paymentMethod->isCod : false;
    }

    public function getDisCountAmount()
    {
        $discountAmount = 0;
        $courseIds = explode(',', $this->course_list);
        if (!empty($courseIds)) {
            foreach ($courseIds as $courseId) {
                $product = Product::findOne($courseId);
                $discountAmount += $product->getDiscountAmount();
            }
        }
        return $discountAmount;
    }

    public function getCost()
    {
        $cost = 0;
        $courseIds = explode(',', $this->course_list);
        if (!empty($courseIds)) {
            foreach ($courseIds as $courseId) {
                $product = Product::findOne($courseId);
                $cost += $product->getSellPrice();
            }
        }
        return $cost;
    }

    public function applyPromotionCode($products, $promotion_code, $user_email)
    {

        $result = array('courses' => $products, 'promotion_code' => '', 'errors'=> '');
        $promoForm = new PromoCodeForm();
        $promoForm->user_email = $user_email;
        $promoForm->products = $products;
        $promoForm->code = $promotion_code;
        if ($promoForm->validate()) {
            //tính tổng giá trị đơn hàng được apply vourcher
            $total_order_can_apply = 0;
            $listCourseCanApply = $promoForm->getCoursesCanUseCode();

            list($code,$userId) = UserVoucherFree::isUserVoucherFree($promotion_code);
            if(!empty($code) && !empty($userId)){
                $listCourseCanApply = UserVoucherFree::getListMaxPriceProducts($products,$listCourseCanApply);
            }
            
            foreach ($products as $course) {
                    if (in_array($course->id, $listCourseCanApply)) {
                        $total_order_can_apply = $total_order_can_apply + $course->oldPrice - $course->originDiscount;
                    }
                }

                // tổng giá trị vourcher được giảm
                $total_vourcher_value = 0;

                // case %
                if ($promoForm->getDiscountType() == Promotion::TYPE_PERCENTAGE) {
                    $total_vourcher_value = $promoForm->getPercentagePrice($promoForm->percentage, $total_order_can_apply);
                } else {
                    $total_vourcher_value = $promoForm->getDiscountPrice();
                }
                // case fix

                $voucherValueDiscount = 0;
                $final_products = [];
                if ($promoForm->_promotion->type == Promotion::KIND_COURSE_APPLY) {
                    foreach ($products as $course) {
                        if (in_array($course->id, $listCourseCanApply)) {
                            $price_after_original_discount = $course->oldPrice - $course->originDiscount;
                            if ($promoForm->getDiscountType() == Promotion::TYPE_FEE) // giá cố định
                            {
                                $voucherValueDiscount = $price_after_original_discount > $total_vourcher_value ? $total_vourcher_value : $price_after_original_discount;
                            } else { //giảm phần trăm
                                $voucherValueDiscount = $promoForm->getPercentagePrice($promoForm->getPercentage(), $price_after_original_discount);
                            }
                            $course->voucherDiscountAmount = $voucherValueDiscount;
                            $course->setPromotionCode($promoForm->code);
                        }
                        $final_products[] = $course;
                    }
                } else {
                    foreach ($products as $course) {
                        $price_after_original_discount = $course->oldPrice - $course->originDiscount;
                        if ($total_vourcher_value >= $total_order_can_apply) {
                            $voucherValueDiscount = $course->oldPrice - $course->discountAmount;
                        } else {
                            $voucherValueDiscount = $total_vourcher_value * ($price_after_original_discount / $total_order_can_apply);
                        }
                        if (in_array($course->id, $listCourseCanApply)) {
                            $course->voucherDiscountAmount = $voucherValueDiscount;
                            $course->setPromotionCode($promoForm->code);}
                        $final_products[] = $course;
                    }
                }
            $result['courses'] = $final_products;
            $result['promotion_code'] = $this->promotion_code;
        }else {
            $result['errors'] = $promoForm->getErrors();
        }
        return $result;
    }


}
