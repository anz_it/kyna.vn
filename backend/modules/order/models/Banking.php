<?php
    namespace backend\modules\order\models;
    use kyna\base\ActiveRecord;

    class Banking extends ActiveRecord
    {
        public $username;
        public $email;
        public $phone_number;
        public $trans_date_begin;
        public $trans_date_end;
        public $filter;

        public static function tableName()
        {
            return 'orders';
        }

        public function rules()
        {
            return
            [
                [['username' , 'trans_date_begin' , 'trans_date_end'] , 'string'],
                [['email'] , 'email'],
                [['phone_number' , 'delta_trans_date_begin' , 'delta_trans_date_end'] , 'integer']
            ];
        }

        public function attributeLabels()
        {
            return [
                    'username' => 'Họ tên',
                    'email' => 'email',
                    'phone_number' => 'Số điện thoại',
                    'trans_date_begin' => 'Ngày chuyển khoản từ',
                    'trans_date_end' => 'Đến',
                    'filter' => 'Phương thức thanh toán',
                ];
        }

        public function getDataTranfer()
        {
            $query = (new \yii\db\Query());
            $data = $query->select('orders.order_date , orders.total , orders.operator_id , user.username , user.email , user_addresses.phone_number , order_details.course_id , order_details.course_combo_id')
                    ->from(self::tableName())
                    ->join('JOIN', 'user', 'orders.user_id = user.id')
                    ->join('JOIN', 'user_addresses', 'user_addresses.user_id = user.id')
                    ->join('JOIN', 'order_details', 'orders.id = order_details.order_id')
                    ->all();
            return $data;
        }

        public function getCourseName($course_id)
        {
            $query = (new \yii\db\Query());
            $course_name = $query->select('name')->from('courses')->where(['id' => $course_id])->scalar();
            return $course_name;
        }

        public function getUserAddTranfer($user_id)
        {
            $query = (new \yii\db\Query());
            $user_name = $query->select('username')->from('user')->where(['id' => $user_id])->scalar();
            return $user_name;
        }

        public function searchTranferLog($username , $email , $phone_number , $trans_date_begin , $trans_date_end , $filter)
        {
            $query = (new \yii\db\Query());
//            if($trans_date_begin > 0 && $trans_date_end > 0 && $filter != 'none')
//            {
//                $data = $query->select('orders.order_date , orders.payment_method , orders.total , orders.operator_id , user.username , user.email , user_addresses.phone_number , order_details.course_id , order_details.course_combo_id')
//                        ->from(self::tableName())
//                        ->join('LEFT JOIN', 'user', 'orders.user_id = user.id')
//                        ->join('LEFT JOIN', 'user_addresses', 'user_addresses.user_id = user.id')
//                        ->join('LEFT JOIN', 'order_details', 'orders.id = order_details.order_id')
//                        ->andFilterWhere(['user.username' => $username])
//                        ->andFilterWhere(['user.email' => $email])
//                        ->andFilterWhere(['user_addresses.phone_number' => $phone_number])
//                        ->andFilterWhere(['orders.payment_method' => $filter])
//                        ->andFilterWhere(['between', 'orders.order_date', $trans_date_begin , $trans_date_end])
//                        ->all();
//            }
//            if($trans_date_begin > 0 && $trans_date_end > 0 && $filter == 'none')
//            {
//                $data = $query->select('orders.order_date , orders.payment_method , orders.total , orders.operator_id , user.username , user.email , user_addresses.phone_number , order_details.course_id , order_details.course_combo_id')
//                        ->from(self::tableName())
//                        ->join('LEFT JOIN', 'user', 'orders.user_id = user.id')
//                        ->join('LEFT JOIN', 'user_addresses', 'user_addresses.user_id = user.id')
//                        ->join('LEFT JOIN', 'order_details', 'orders.id = order_details.order_id')
//                        ->andFilterWhere(['user.username' => $username])
//                        ->andFilterWhere(['user.email' => $email])
//                        ->andFilterWhere(['user_addresses.phone_number' => $phone_number])
//                        ->andFilterWhere(['between', 'orders.order_date', $trans_date_begin , $trans_date_end])
//                        ->all();
//            }
//            if($trans_date_begin < 0 && $trans_date_end < 0 && $filter != 'none')
//            {
//                $data = $query->select('orders.order_date , orders.payment_method , orders.total , orders.operator_id , user.username , user.email , user_addresses.phone_number , order_details.course_id , order_details.course_combo_id')
//                    ->from(self::tableName())
//                    ->join('LEFT JOIN', 'user', 'orders.user_id = user.id')
//                    ->join('LEFT JOIN', 'user_addresses', 'user_addresses.user_id = user.id')
//                    ->join('LEFT JOIN', 'order_details', 'orders.id = order_details.order_id')
//                    ->andFilterWhere(['user.username' => $username])
//                    ->andFilterWhere(['user.email' => $email])
//                    ->andFilterWhere(['user_addresses.phone_number' => $phone_number])
//                    ->andFilterWhere(['orders.payment_method' => $filter])
//                    ->all();
//            }
            $data = $query->select('orders.order_date , orders.payment_method , orders.total , orders.operator_id , user.username , user.email , user_addresses.phone_number , order_details.course_id , order_details.course_combo_id')
                        ->from(self::tableName())
                        ->join('LEFT JOIN', 'user', 'orders.user_id = user.id')
                        ->join('LEFT JOIN', 'user_addresses', 'user_addresses.user_id = user.id')
                        ->join('LEFT JOIN', 'order_details', 'orders.id = order_details.order_id')
                        ->andFilterWhere(['user.username' => $username])
                        ->andFilterWhere(['user.email' => $email])
                        ->andFilterWhere(['user_addresses.phone_number' => $phone_number]);
            if($trans_date_begin > 0 && $trans_date_end > 0 && $filter != 'none')
            {
                $data = $data->andFilterWhere(['orders.payment_method' => $filter])
                     ->andFilterWhere(['between', 'orders.order_date', $trans_date_begin , $trans_date_end])
                     ->all();
            }
            if($trans_date_begin > 0 && $trans_date_end > 0 && $filter == 'none')
            {
                $data = $data->andFilterWhere(['between', 'orders.order_date', $trans_date_begin , $trans_date_end])->all();
            }
            if($trans_date_begin < 0 && $trans_date_end < 0 && $filter != 'none')
            {
                $data = $data->andFilterWhere(['orders.payment_method' => $filter])->all();
            }
            if($trans_date_begin < 0 && $trans_date_end < 0 && $filter == 'none')
            {
                $data = $data->all();
            }
            return $data;
        }

        public function getListCharging()
        {
            $query = (new \yii\db\Query());
            $list_charging['none'] = '';
            $data = $query->select('name , class')->from('payment_methods')->all();
            foreach($data as $val)
            {
                $list_charging[$val['class']] = $val['name'];
            }
            return $list_charging;
        }
    }

