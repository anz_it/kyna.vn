<?php

namespace app\modules\order\models;

use kyna\order\models\Order;
use kyna\order\models\ShippingMethod;

class ChangeStatusForm extends \yii\base\Model
{
    public $note;
    public $status;
    public $order_id;
    public $confirm = false;
    public $payment_method = 'bank-transfer';
    public $shipping_vendor_id;
    public $contact_name;
    public $phone_number;
    public $location_id;
    public $street_address;
    public $payment_receipt_number;

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios['pending-payment'] = ['payment_method', 'note', 'confirm', 'order_id'];
        $scenarios['delivering'] = ['contact_name', 'phone_number', 'location_id', 'street_address', 'note', 'order_id', 'shipping_vendor_id'];
        $scenarios['cancelled'] = ['note', 'confirm', 'order_id'];
        $scenarios['restore'] = ['note', 'confirm', 'order_id'];
        $scenarios['completed'] = ['note', 'confirm', 'payment_receipt_number', 'order_id'];

        return $scenarios;
    }

    public function rules()
    {
        return [
            [['order_id', 'status', 'shipping_vendor_id', 'location_id', 'phone_number'], 'integer'],
            ['order_id', 'required'],
            ['status', 'required'],
            ['confirm', 'boolean'],
            ['note', 'safe'],
            [['contact_name', 'street_address'], 'string', 'max' => 255],

            ['confirm', 'required', 'on' => ['pending-payment', 'cancelled', 'restore', 'completed']],

            ['note', 'required', 'on' => ['cancelled', 'restore']],

            ['payment_method', 'required', 'on' => 'pending-payment'],

            [['shipping_vendor_id', 'contact_name', 'phone_number', 'location_id', 'street_address'], 'required', 'on' => 'delivering'],

            ['payment_receipt_number', 'required', 'on' => 'completed'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'note' => 'Lý do',
            'status' => 'Trạng thái mới',
            'order_id' => 'Đơn hàng',
            'confirm' => 'Xác nhận',
            'payment_method' => 'Hình thức thanh toán',
            'shipping_vendor_id' => 'Đơn vị giao vận',
            'contact_name' => 'Người nhận',
            'phone_number' => 'Số điện thoại',
            'location_id' => 'Quận/Huyện',
            'street_address' => 'Địa chỉ',
            'payment_receipt_number' => 'Số chứng từ/biên lai/mã giao dịch',
        ];
    }

    public function change()
    {
        $order = Order::find()->where(['id' => $this->order_id])->one();
        if (!$order) {
            return false;
        }
        if ($this->scenario == 'delivering') {
            $shipping_address = [
                'contact_name' => $this->contact_name,
                'phone_number' => $this->phone_number,
                'location_id' => $this->location_id,
                'street_address' => $this->street_address,
                'vendor_id' => $this->shipping_vendor_id,
            ];
            $order->shipping_address = $shipping_address;
            $order->saveShippingAddress();

            $order->on(Order::EVENT_ORDER_STATUS_CHANGED, [$this, 'sendToShipping']);
        }

        $actionData = $this->attributes;
        unset($actionData['order_id']);
        unset($actionData['status']);

        return $order->updateStatus($this->status, $actionData);
    }

    public function sendToShipping($event)
    {
        $order = $event->sender;
        $shipingMethod = new ShippingMethod();
        $response = $shipingMethod->via($order->orderShipping->vendor_id)->shipTo($order->orderShipping)->withAmount($order->realPayment)->createOrder();
        if ($response) {
            $orderShipping = $response;
        }

        return $response !== false;
    }
}
