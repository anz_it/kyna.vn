<?php

namespace app\modules\order\models;

use kyna\course\models\Course;
use yii\data\ActiveDataProvider;
use kyna\order\models\Order;
use common\validators\PhoneNumberValidator;
use kyna\order\models\OrderMeta;

/**
 * OrderSearch represents the model behind the search form about `common\models\Order`.
 */
class OrderSearch extends Order
{
    public $id;
    public $search;
    public $from_date;
    public $to_date;
    public $operator_id;
    public $user_id;
    public $phone_number;
    public $payment_method;
    public $order_ids;
    public $call_status = false;
    public $print_type;
    public $partner_id;
    public $location;
    public $shipping_method;
    public $course_type;
    public $search_type;

    public $forceEmpty = false;

    public $allowedStatus = false;

    public $is_4kid;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'point_of_sale', 'course_type','search_type'], 'safe'],
            ['status', 'validateStatus'],
            [['from_date', 'to_date', 'operator_id', 'print_type', 'partner_id', 'location'], 'integer'],
        
        ];
    }

    public function validateStatus($attribute, $params)
    {
        $allowedStatus = $this->allowedStatus;
        $status = $this->$attribute;

        if ($status == (int) $status) {
            if ($allowedStatus AND !in_array($status, $allowedStatus)) {
                $this->addError($attribute, 'Status must be a subset of `allowedStatus`');
            }
        }
        elseif (is_array($status)) {
            $status = filter_var($status, FILTER_VALIDATE_INT, array(
              'flags'   => FILTER_REQUIRE_ARRAY,
            ));
            if (array_filter($status, 'is_int') === $status) {
                if ($allowedStatus AND (count(array_intersect($status, $allowedStatus)) != count($status))) {
                    $this->addError($attribute, 'Status must be a subset of `allowedStatus`');
                }
            }
            else {
                $this->addError($attribute, 'Status must be integer or array of integer');
            }
        }
        else {
            $this->addError($attribute, 'Status must be integer or array of integer');
        }
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        return \yii\base\Model::scenarios();
    }

    public function safeAttributes()
    {
        return [
            'id',
            'search',
            'from_date',
            'to_date',
            'status',
            'operator_id',
            'phone_number',
            'user_id',
            'order_ids',
            'affiliate_id',
            'call_status',
            'print_type',
            'partner_id',
            'forceEmpty',
            'promotion_code',
            'shipping_method',
            'course_type',
            'is_4kid',
            'search_type'
        ];
    }

    /**
     * Creates data provider instance with search query applied.
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Order::find();
        $query->alias('t');
        $query->andWhere(['t.is_done_telesale_process' => $this->is_done_telesale_process]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'attributes' => ['created_time'],
                'defaultOrder' => ['created_time' => SORT_DESC]
            ]
        ]);
        $this->load($params);
        if (!$this->validate() or $this->forceEmpty) {
            $query->where('0=1');

            return $dataProvider;
        }

        if ($this->id) {
            $query->andFilterWhere(['id' => $this->id]);
            return $dataProvider;
        }

        $query->leftJoin('order_details d','t.id = d.order_id');

        if(!empty($params['course_type']) && empty($this->course_type)){
            $query->leftJoin('courses c', 'd.course_id = c.id')
                ->andFilterWhere(['c.type' => $params['course_type']]);
        }

        if(!empty($this->course_type)){
            $query->leftJoin('courses c', 'd.course_id = c.id')
                ->andFilterWhere(['c.type' => $this->course_type]);
        }

        if (!empty($params['partner_id'])) {
            $query->leftJoin('partner p', 'c.partner_id = p.id')
                ->andFilterWhere(['c.type' => Course::TYPE_SOFTWARE, 'p.id' => $params['partner_id']]);
        }

        if (!$this->to_date) {
            $this->to_date = strtotime('tomorrow 0:00');
        }
        if (!$this->from_date) {
            $this->from_date = $this->to_date - 24*7*3600;
        }


        if (\Yii::$app->controller->action->id == 'print-cod') {
            $dateSearchField = 't.to_shipper_time';
            $this->to_date += 1;

        }
        else {
            $dateSearchField = 't.created_time';
            $this->to_date += 24*3600;
        }



        $query->andFilterWhere(['between', $dateSearchField, $this->from_date, $this->to_date]);

        if ($status = $this->status) {
            if (is_array($this->status)) {
                $status = filter_var($this->status, FILTER_VALIDATE_INT, [
                  'flags'   => FILTER_REQUIRE_ARRAY,
                ]);
            }
        }
        
        if ($this->operator_id) {
            $query->andWhere(['or', '`operator_id`=:operatorId', '`reference_id`=:operatorId'], [
                ':operatorId' => $this->operator_id
            ]);
        }
        if (!empty($this->affiliate_id)) {
            $query->andWhere(['affiliate_id' => $this->affiliate_id]);
        }

        if (!empty($this->call_status)) {
            $orderMetaTable = OrderMeta::tableName();
            $query->joinWith('orderMeta');
            $query->andWhere([$orderMetaTable . '.value' => $this->call_status, '`key`' => 'call_status']);
        }

        /*
         * Check user_id as array
         */
        if (is_array($this->user_id)) {
            $query->andFilterWhere(['IN' , 't.user_id', $this->user_id]);
        } else {
            $query->andFilterWhere(['t.user_id' => $this->user_id]);
        }

        if (!empty($this->shipping_method)) {
            $query->andFilterWhere(['shipping_method' => $this->shipping_method]);
        }

        $query->andFilterWhere(['payment_method' => $this->payment_method])
            ->andFilterWhere(['t.status' => $status])
            ->andFilterWhere(['t.id' => $this->order_ids])
            ->andFilterWhere(['t.user_id' => $this->user_id])
            ->andFilterWhere(['t.promotion_code' => $this->promotion_code]);

        if (isset($this->is_4kid)) {
            $query->andFilterWhere(['is_4kid' => $this->is_4kid]);
        }


        if ($this->phone_number) {
            $queryProfile = clone $query;
            $queryUserAddress = clone $query;
            $query->joinWith('orderShipping os')
                ->andFilterWhere(['os.phone_number' => $this->phone_number]);

            $queryProfile->leftJoin('profile p', 'p.user_id = t.user_id')
                ->andFilterWhere(['p.phone_number' => $this->phone_number]);

            $queryUserAddress->leftJoin('user_addresses ua', 'ua.user_id = t.user_id')
                ->andFilterWhere(['ua.phone_number' => $this->phone_number]);

            $query->union($queryProfile)->union($queryUserAddress);
        }

        return $dataProvider;
    }
}