<?php

namespace app\modules\order\models;

use Yii;
use yii\base\InvalidParamException;
use yii\base\ActionEvent;
use yii\helpers\Json;
use yii\web\Response;

use kyna\base\models\Location;
use kyna\order\models\Order;
use kyna\payment\models\PaymentTransaction;
use kyna\payment\models\PaymentMethod;
use kyna\payment\lib\proship\Proship;
use kyna\taamkru\models\Transaction;

use Elasticsearch\Common\Exceptions\InvalidArgumentException;


class ActionForm extends \yii\base\Model
{

    public $order_id;
    public $status;
    public $start;
    public $duration;
    public $note;
    public $checksum;
    public $payment_method;
    public $shipping_method;
    public $activation_code;
    public $confirm;
    public $contact_name;
    public $phone_number;
    public $location_id;
    public $city_id;
    public $street_address;
    public $payment_receipt;

    public $pick_up_location;

    public $expected_payment_fee;

    public $partner_code;

    public function init()
    {
        $ret = parent::init();

        ActionEvent::on(Order::className(), Order::EVENT_ORDER_BEFORE_SENT_TO_SHIPPING, [$this, 'doPayment']);

        return $ret;
    }

    public function scenarios()
    {
        return [
            'request-payment' => ['order_id', 'payment_method', 'note'],
            'cancel' => ['order_id', 'note', 'confirm'],
            'cod-return' => ['order_id', 'note', 'confirm'],
            'complete' => ['order_id', 'activation_code', 'payment_receipt', 'note', 'confirm'],
            'update-shipping' => ['order_id', 'contact_name', 'phone_number', 'location_id', 'street_address', 'note', 'payment_method', 'activation_code'],
            'send-to-shipping' => ['order_id', 'contact_name', 'phone_number', 'location_id', 'street_address', 'note', 'payment_method', 'shipping_method', 'pick_up_location', 'activation_code', 'expected_payment_fee', 'partner_code'],
            'change-to-cod' => ['order_id', 'contact_name', 'phone_number', 'location_id', 'street_address'],
            'call' => ['start', 'note', 'status', 'order_id', 'checksum'],
            'note' => ['note', 'status', 'order_id'],
            'move-to-cs' => ['order_id', 'note', 'confirm'],
            'update-shipping-status' => ['order_id'],
            'cancel-shipping' => ['order_id', 'note', 'confirm'],
            'update-payment-method' => ['order_id', 'note', 'confirm', 'payment_method', 'contact_name', 'phone_number', 'location_id', 'street_address'],
        ];
    }

    public function rules()
    {
        return [
            ['order_id', 'required'],
            [['order_id', 'duration', 'start', 'status'], 'integer'],
            ['checksum', function ($attribute, $params) {
                    $_checksum = md5($this->order_id . '-' . $this->start);
                    if ($this->$attribute !== $_checksum) {
                        $this->addError($attribute, $attribute . ' not matched');

                        return false;
                    }

                    return true;
                }],
            ['confirm', 'boolean'],
            ['contact_name', 'string', 'max' => 50],
            ['phone_number', 'string', 'max' => 50],
            ['activation_code', 'string', 'max' => 50],
            [['payment_method'], 'required', 'on' => ['request-payment', 'update-payment-method']],
            [['contact_name', 'phone_number', 'location_id', 'street_address', 'shipping_vendor_id', 'payment_method', 'shipping_method'], 'required', 'on' => ['send-to-shipping']],
            [['contact_name', 'phone_number', 'location_id', 'street_address', 'shipping_vendor_id'], 'required', 'on' => ['update-shipping']],
            [['note'], 'required', 'on' => ['cancel', 'cancel-shipping', 'complete', 'call', 'note', 'move-to-cs', 'cod-return', 'update-payment-method']],
            ['confirm', 'required', 'on' => ['cancel', 'cancel-shipping', 'complete', 'move-to-cs', 'cod-return'], 'requiredValue' => 1, 'message' => 'Xác nhận để hoàn thành'],
            [['start', 'status', 'checksum'], 'required', 'on' => 'call'],
            ['status', 'required', 'on' => 'note'],
            [['partner_code'], 'safe'],
            [['contact_name', 'phone_number', 'location_id', 'street_address', 'shipping_vendor_id'],'required', 'when'=> function($model, $attribute){
               return  $model->payment_method == 'cod';
            }, 'whenClient' => "function (attribute, value) {
                return $('input:radio[name=\"ActionForm[payment_method]\"]:checked').val() == 'cod';
             }",]
        ];
    }

    public function attributeLabels()
    {
        return [
            'start' => 'Lý do',
            'duration' => 'Trạng thái mới',
            'order_id' => 'Đơn hàng',
            'status' => 'Xác nhận',
            'payment_method' => 'Phương thức thanh toán',
            'note' => 'Ghi chú',
            'location_id' => 'Địa chỉ'
        ];
    }

    public static function getEnableActions($status, $payment_method = null, $shipping_method = null)
    {
        $webUser = Yii::$app->user;

        $canRequestPayment = $webUser->can('Order.Action.RequestPayment');
        $canCancel = $webUser->can('Order.Action.Cancel');
        $canUpdate = $webUser->can('Order.Action.Update');
        $canComplete = $webUser->can('Order.Action.Complete');
        $canShipping = $webUser->can('Order.Action.Shipping');
        $canMoveToCs = $webUser->can('Order.Action.MoveToCs');
        $canReturn = $webUser->can('Order.Action.Return');
        $canUpdateShippingStatus = $webUser->can('Order.Action.UpdateShippingStatus');
        $canCancelShipping = $webUser->can('Order.Action.CancelShipping');
        $canUpdatePartnerCode = false; //$webUser->can('Order.Action.UpdatePartnerCode');
        $canUpdatePaymentMethod = $webUser->can('Order.Action.UpdatePaymentMethod');

        // Set up canUpdateShippingStatus & canCancelShipping dua vao $payment_method & $shipping_method
        if ($canUpdateShippingStatus) {
            if (!isset($payment_method) || !isset($shipping_method))
                $canUpdateShippingStatus = false;
            if ($payment_method != 'cod' || ($shipping_method != 'proship' && $shipping_method!= 'ghtk'))
                $canUpdateShippingStatus = false;
        }

        if ($canCancelShipping) {
            if (!isset($payment_method) || !isset($shipping_method))
                $canCancelShipping = false;
            if ($payment_method != 'cod' || ($shipping_method != 'proship' && $shipping_method!= 'ghtk'))
                $canCancelShipping = false;
        }

        $actions = [];
        switch ($status) {
            case Order::ORDER_STATUS_NEW:
            case Order::ORDER_STATUS_PAYMENT_FAILED:
                if ($canRequestPayment) {
                    $actions['request-payment'] = 'Yêu cầu thanh toán';
                }
                if ($canUpdatePaymentMethod) {
                    $actions['update-payment-method'] = 'Đổi phương thức thanh toán';
                }
                if ($canUpdate) {
                    $actions['update'] = 'Update';
                }
                break;

            case Order::ORDER_STATUS_PENDING_PAYMENT:
            case Order::ORDER_STATUS_IN_COMPLETE:
                if ($canComplete) {
                    $actions['complete'] = 'Hoàn thành';
                }
                if ($canUpdatePaymentMethod) {
                    $actions['update-payment-method'] = 'Đổi phương thức thanh toán';
                }
                if ($canUpdate) {
                    $actions['update'] = 'Update';
                }
                break;

            case Order::ORDER_STATUS_DELIVERING:
                if ($canComplete) {
                    $actions['complete'] = 'Hoàn thành';
                }
                if ($canReturn) {
                    $actions['cod-return'] = 'Đã trả về';
                }
                if ($canUpdateShippingStatus) {
                    $actions['update-shipping-status'] = 'Update Shipping Status';
                }
                if ($canCancelShipping) {
                    $actions['cancel-shipping'] = "Hủy giao vận";
                }
                if ($canUpdatePartnerCode) {
                    $actions['update-partner-code'] = 'Partner Code';
                }
                break;

            case Order::ORDER_STATUS_WAITING_RETURN:
                if ($canReturn) {
                    $actions['cod-return'] = 'Đã trả về';
                }
                if ($canUpdatePaymentMethod) {
                    $actions['update-payment-method'] = 'Đổi phương thức thanh toán';
                }
                break;

            case Order::ORDER_STATUS_PENDING_CONTACT:
                if ($canShipping) {
                    $actions['send-to-shipping'] = 'Chuyển qua Giao vận';
                }
                if ($canUpdatePartnerCode) {
                    $actions['update-partner-code'] = 'Partner Code';
                }
                if ($canUpdatePaymentMethod) {
                    $actions['update-payment-method'] = 'Đổi phương thức thanh toán';
                }
                if ($canUpdate) {
                    $actions['update'] = 'Cập nhật đơn hàng';
                }
                break;

            default:
                return [];
        }

        if ($canCancel) {
            $actions['cancel'] = 'Hủy Đơn hàng';
        }

        if ($canMoveToCs) {
            $actions['move-to-cs'] = 'Chuyển sang CS';
        }

        return $actions;
    }

    public function complete()
    {
        return Order::complete($this->order_id, $this->payment_receipt, [
            'note' => $this->note
        ]);
    }

    public function codReturn()
    {
        return Order::codReturn($this->order_id, $this->payment_receipt, [
            'note' => $this->note
        ]);
    }

    public function cancel()
    {
        return Order::cancel($this->order_id, [
                    'note' => $this->note
        ]);
    }

    public function moveToCs()
    {
        return Order::moveToCs($this->order_id);
    }

    public function sendToShipping()
    {
        $this->updateShipping();
        return Order::sendToShipping($this->order_id, $this->attributes, $this->attributes);
    }

    public function updateShipping()
    {
        $order = Order::findOne($this->order_id);

        return Order::updateShippingAddress($order, $this->attributes, $this->attributes);
    }

    public function updatePartnerCode()
    {
        $order = Order::findOne($this->order_id);

        return Order::updatePartnerCode($order, $this->partner_code, $this->attributes);
    }

    public function updatePaymentMethod()
    {
        $order = Order::findOne($this->order_id);
        if($this->payment_method == 'cod'){
            $this->updateShipping();
        }
        return Order::updatePaymentMethod($order, $this->payment_method, $this->attributes);
    }

    /**
     * @param $order
     * @return bool
     */
    public function doPayment($actionEvent)
    {
        $order = Order::findOne($this->order_id);
        $location = Location::findOne($this->location_id);
        $this->city_id = $location->parent_id;
        $payment_method = PaymentMethod::loadModel($this->payment_method, $this->attributes);
        if (empty($payment_method)) {
            throw new InvalidParamException('System could not found the payment method: ' . $this->method);
        }

        if ($payment_method->isPayable()) {
            if ($this->payment_method == 'cod') {
                Yii::$app->response->format = Response::FORMAT_JSON;
                if (empty($this->shipping_method)) {
//                    throw new \InvalidArgumentException('Missing param shipping method');
                    $ret = [
                        'success' => false,
                        'message' => 'Xử lý thất bại!',
                    ];
                    echo Json::decode($ret);
                    Yii::$app->end();
                }
                if ($this->shipping_method == 'proship') {
                    if (empty($this->pick_up_location)) {
//                        throw new \InvalidArgumentException("Missing param pick up location");
                        $ret = [
                            'success' => false,
                            'message' => 'Xử lý thất bại. Xin chọn Địa điểm lấy hàng!',
                        ];
                        echo Json::decode($ret);
                        Yii::$app->end();
                    }
                    if (empty($this->expected_payment_fee)) {
//                        throw new \InvalidArgumentException("Missing param pick up location");
                        $ret = [
                            'success' => false,
                            'message' => 'Xử lý thất bại. Xin chọn Địa điểm lấy hàng!',
                        ];
                        echo Json::decode($ret);
                        Yii::$app->end();
                    }
                }
                if (isset($this->pick_up_location))
                    $params = ['pick_up_location' => $this->pick_up_location];
                if (isset($this->expected_payment_fee))
                    $params['expected_payment_fee'] = $this->expected_payment_fee;
                $params['note'] = 'Không cho khách mở hàng';
                $response = $payment_method->pay($order, $this->shipping_method, $params);
                if ($response === false || isset($response['error'])) {
                    $ret = [
                        'success' => false,
//                        'message' => 'Xử lý thất bại!',
                        'message' => isset($response['error'])? $response['error'] : 'Xử lý thất bại!'
                    ];
                    echo Json::encode($ret);
                    Yii::$app->end();
                }
            }
            else {
                $response = $payment_method->pay($order);
            }
        } else {
            $response = true;
        }
        return $response;
    }

    /**
     * @return bool|Order
     */
    public function requestPayment()
    {
        return Order::requestPayment($this->order_id, $this->payment_method);
    }

    public function endCall()
    {
        return Order::endCall($this->order_id, $this->status, $this->attributes);
    }

    public function note()
    {
        return Order::note($this->order_id, $this->status, $this->attributes);
    }

    public function validPartnerCode($attribute)
    {
        $order = Order::findOne($this->order_id);
        foreach ($order->details as $detail) {
            $course = $detail->course;
            if ($course && $course->isPartner && $course->partner->isServiceType && empty($detail->activation_code)) {
                $this->addError($attribute, 'Vui lòng nhập mã serial code cho khóa học đặt biệt');
                break;
            }
        }
    }
}
