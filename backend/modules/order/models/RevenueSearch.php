<?php

namespace app\modules\order\models;

use kyna\course\models\Course;
use kyna\order\models\actions\OrderAction;
use kyna\order\models\Order;
use kyna\order\models\OrderMeta;
use kyna\payment\models\PaymentMethod;
use yii\data\ActiveDataProvider;

/**
 * Created by PhpStorm.
 * User: Hien Nguyen
 * Date: 8/24/2016
 * Time: 10:25 AM
 */
class RevenueSearch extends OrderSearch
{
    public $total_reg_filter;
    public $payment_type;
    public $date_ranger;
    public $date;

    public function searchRevenue($params)
    {
        $query = self::find();

        $query->alias('t');
        $query->select('t.payment_method as payment_type, sum(t.total) as total, count(t.id) as total_reg_filter');

        $query->andWhere(['!=', 't.user_id', 0]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (isset($params['RevenueSearch']['date_ranger'])) {
            $this->date_ranger = $params['RevenueSearch']['date_ranger'];
        }

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        if (!empty($this->date_ranger)) {
            $dateRange = explode(' - ', $this->date_ranger);
            $beginDate = date_create_from_format('d/m/Y H:i', $dateRange[0] . ' 00:00');
            $endDate = date_create_from_format('d/m/Y H:i', $dateRange[1] . ' 00:00');

            $query->andWhere(['>=', 't.created_time', $beginDate->getTimestamp()]);
            $query->andWhere(['<=', 't.created_time', $endDate->getTimestamp()]);
        }

        $query->groupBy('t.payment_method');

        return $dataProvider;
    }

    public function searchCod($params)
    {
        $query = Order::find()->where(['payment_method' => PaymentMethod::getPaymentIsCodClasses()]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (isset($params['RevenueSearch']['date_ranger'])) {
            $this->date_ranger = $params['RevenueSearch']['date_ranger'];
        }

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }
        $query->andFilterWhere([
            'operator_id' => $this->operator_id,
        ]);

        if (!empty($this->date_ranger)) {
            $dateRange = explode(' - ', $this->date_ranger);
            $beginDate = date_create_from_format('d/m/Y H:i', $dateRange[0] . ' 00:00');
            $endDate = date_create_from_format('d/m/Y H:i', $dateRange[1] . ' 00:00');

            $query->andWhere(['>=', 'created_time', $beginDate->getTimestamp()]);
            $query->andWhere(['<=', 'created_time', $endDate->getTimestamp()]);
        }

        return $dataProvider;
    }

    public static function SelectRegisteredByDay($day, &$total_num, &$total_money)
    {

        $query = self::find();
        $query->alias('t');
        $query->select('sum(t.total) as total, count(t.id) as total_reg_filter');
        $query->where(['payment_method' => 'cod']);
//        $query->leftJoin(PaymentMethod::tableName(), PaymentMethod::tableName() . '.class = t.payment_type');
        if (!empty($day)) {
            $beginDate = date_create_from_format('d/m/Y H:i', $day . ' 00:00');
            $endDate = date_create_from_format('d/m/Y H:i', $day . ' 23:59');

            $query->andWhere(['>=', 't.created_time', $beginDate->getTimestamp()]);
            $query->andWhere(['<=', 't.created_time', $endDate->getTimestamp()]);
        }
        $result = $query->one();
        if ($result) {
            $total_money = $result->total;
            $total_num = $result->total_reg_filter;
        }

    }

    public static function SelectRegisteredByDayOnline($day, &$total_num, &$total_money)
    {

        $query = self::find();
        $query->alias('t');
        $query->select('sum(t.total) as total, count(t.id) as total_reg_filter');
        $query->where(['!=', 'payment_method', 'cod']);

        if (!empty($day)) {
            $beginDate = date_create_from_format('d/m/Y H:i', $day . ' 00:00');
            $endDate = date_create_from_format('d/m/Y H:i', $day . ' 23:59');

            $query->andWhere(['>=', 't.created_time', $beginDate->getTimestamp()]);
            $query->andWhere(['<=', 't.created_time', $endDate->getTimestamp()]);
        }
        $result = $query->one();
        if ($result) {
            $total_money = $result->total;
            $total_num = $result->total_reg_filter;
        }
    }

    public static function SelectRegisteredByDayEstimate($day, &$total_num, &$total_money)
    {

        $costDangGiao = $costDaKichHoat = $costChoLienHe = 0;
        $numDangGiao = $numDaKichHoat = $numChoLienHe = 0;

        $query = Order::find();
        $query->alias('t');
        $query->select('t.total, t.id, t.status');
        $query->where(['payment_method' => 'cod']); // TODO: update cod when apply Proship of Mr Ngu, do the same for all COD
        if (!empty($day)) {
            $beginDate = date_create_from_format('d/m/Y H:i', $day . ' 00:00');
            $endDate = date_create_from_format('d/m/Y H:i', $day . ' 23:59');

            $query->andWhere(['>=', 't.created_time', $beginDate->getTimestamp()]);
            $query->andWhere(['<=', 't.created_time', $endDate->getTimestamp()]);
        }

        $results = $query->all();
        foreach ($results as $key => $item) {

            if ($item->status == self::ORDER_STATUS_DELIVERING) {
                $costDangGiao += $item->total;
                $numDangGiao++;
                continue;
            }

            if ($item->status == self::ORDER_STATUS_COMPLETE) {
                $costDaKichHoat += $item->total;
                $numDaKichHoat++;
                continue;
            }

            if ($item->status == self::ORDER_STATUS_PENDING_CONTACT) {
                $costChoLienHe += $item->total;
                $numChoLienHe++;
                continue;
            }
        }
        $total_money = ($costDangGiao * 90 / 100) + ($costDaKichHoat) + ($costChoLienHe * 90 / 100);
        $total_num = $numDangGiao + $numDaKichHoat + $numChoLienHe;
    }

    public static function SelectPaymentToReportByDay($day, &$total_money)
    {

        $query = self::find();
        $query->alias('t');
        $query->select('sum(t.total) as total');
        $query->where(['!=', 'payment_method', 'cod']);
        $query->andWhere(['status' => self::ORDER_STATUS_COMPLETE]);

        if (!empty($day)) {
            $beginDate = date_create_from_format('d/m/Y H:i', $day . ' 00:00');
            $endDate = date_create_from_format('d/m/Y H:i', $day . ' 23:59');

            $query->andWhere(['>=', 't.created_time', $beginDate->getTimestamp()]);
            $query->andWhere(['<=', 't.created_time', $endDate->getTimestamp()]);
        }

        $result = $query->one();
        if ($result) {
            $total_money = $result->total;
        }

    }

    /**
     * Creates data provider instance with search query applied.
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function searchExport($params)
    {
        $query = Order::find();
        $query->alias('t');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 200,
            ],
        ]);

        $this->load($params);
        if (!$this->validate() or $this->forceEmpty) {
            $query->where('0=1');

            return $dataProvider;
        }

        if ($this->id) {
            $query->andFilterWhere(['id' => $this->id]);
            return $dataProvider;
        }

        if ($this->phone_number) {
            $query->joinWith('orderShipping os')
                ->andFilterWhere(['os.phone_number' => $this->phone_number]);;
        }

        if (!empty($this->print_type) && $this->print_type == 1) {

            $query->leftJoin('order_details d', 't.id = d.order_id')
                ->leftJoin('courses c', 'd.course_id = c.id')
                ->andFilterWhere(['c.type' => Course::TYPE_VIDEO]);
        }

        if (!empty($this->print_type) && $this->print_type == 2) {
            $query->leftJoin('order_details d', 't.id = d.order_id')
                ->leftJoin('courses c', 'd.course_id = c.id')
                ->andFilterWhere(['c.type' => Course::TYPE_SOFTWARE]);
        }
//        var_dump($this);die;

        /*if (!$this->to_date) {
            $this->to_date = strtotime('tomorrow 0:00');
        }
        if (!$this->from_date) {
            $this->from_date = $this->to_date - 24 * 7 * 3600;
        }*/
        $dateSearchField = 't.created_time';

        $query->andFilterWhere(['between', $dateSearchField, $this->from_date, $this->to_date]);

        if ($status = $this->status) {
            if (is_array($this->status)) {
                $status = filter_var($this->status, FILTER_VALIDATE_INT, [
                    'flags' => FILTER_REQUIRE_ARRAY,
                ]);
            }
        }

        if ($this->operator_id) {
            $query->andWhere(['or', '`operator_id`=:operatorId', '`reference_id`=:operatorId'], [
                ':operatorId' => $this->operator_id
            ]);
        }
        if (!empty($this->affiliate_id)) {
            $query->andWhere(['affiliate_id' => $this->affiliate_id]);
        }

        if (!empty($this->call_status)) {
            $orderMetaTable = OrderMeta::tableName();
            $query->leftJoin($orderMetaTable, $orderMetaTable . ".order_id = t.id AND `key` = 'call_status'");
            $query->andWhere([$orderMetaTable . '.value' => $this->call_status]);
        }

        $query->andFilterWhere(['payment_method' => $this->payment_method])
            ->andFilterWhere(['t.status' => $status])
            ->andFilterWhere(['t.id' => $this->order_ids])
            ->andFilterWhere(['t.user_id' => $this->user_id])
            ->orderBy(['t.created_time' => SORT_DESC]);

        return $dataProvider;
    }
}