<?php

namespace app\modules\order\models;

use kyna\order\models\Order;

class CallLogForm extends \yii\base\Model
{
    public $order_id;
    public $status;
    public $start;
    public $duration;
    public $note;
    public $checksum;

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios['default'] = ['start', 'duration', 'note', 'status', 'order_id', 'checksum'];

        return $scenarios;
    }

    public function rules()
    {
        return [
            [['start', 'status', 'order_id', 'checksum', 'note'], 'required'],
            [['order_id', 'duration', 'start', 'status'], 'integer'],

            ['duration', 'safe'],

            ['checksum', function ($attribute, $params) {
                $_checksum = md5($this->order_id.'-'.$this->start);
                if ($this->$attribute !== $_checksum) {
                    $this->addError($attribute, $attribute.' not matched');

                    return false;
                }

                return true;
            }],
        ];
    }

    public function attributeLabels()
    {
        return [
            'start' => 'Lý do',
            'duration' => 'Trạng thái mới',
            'order_id' => 'Đơn hàng',
            'status' => 'Xác nhận',
        ];
    }

    public function logCall()
    {
        $order = Order::endCall($this->order_id, $this->status, $this->attributes);
        return $order;
    }
}
