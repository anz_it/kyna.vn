<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
/* @var $this yii\web\View */
$url = Yii::$app->request->referrer ? Yii::$app->request->referrer : ['index'];
$this->title = 'Đổi Email - Đơn hàng số #'.$order->id;
$this->params['breadcrumbs'][] = ['label' => 'Order - Đổi Email', 'url' => $url];
$this->params['breadcrumbs'][] = $this->title;
?>
<h2>Tạo thông tin tài khoản cho email mới</h2>

<div class="user-update">
    <div class="row">
        <?php $form = ActiveForm::begin(['action' => Yii::$app->urlManager->createUrl(['order/user/confirm-new',
            'id' => $order->id,
            'model' => $order,
            'statusLabelCss' => $statusLabelCss,
            'dataProvider' => $dataProvider,
        ])]); ?>
            <div class="col-md-6">
                <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
                <?= $form->field($model, 'password')->passwordInput(['maxlength' => true]) ?>
                <?= $form->field($model, 'phonenumber')->textInput(['maxlength' => true]) ?>
                <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
                <div class="form-group">
                    <?= Html::submitButton('Update', ['class' => 'btn btn-primary']) ?>
                </div>
            </div>
        <?php ActiveForm::end(); ?>
        <?= $this->render('_order', [
            'model' => $order,
            'statusLabelCss' => $statusLabelCss,
            'dataProvider' => $dataProvider,
        ]) ?>
    </div>


</div>


