<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
/* @var $this yii\web\View */
$this->title = 'Đổi Email - Đơn hàng số #'.$id;
$this->params['breadcrumbs'][] = ['label' => 'Thông báo lỗi', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-update">
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <a class="btn btn-primary" href="<?php echo $_SERVER['HTTP_REFERER'];?>">Quay Lại trang trước </a>
            </div>
        </div>

    </div>
</div>


