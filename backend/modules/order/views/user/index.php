<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */

$this->title = 'Đổi Email - Đơn hàng số #'.$order->id;
$this->params['breadcrumbs'][] = ['label' => 'Danh sách đơn hàng', 'url' => Yii::$app->request->referrer];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="education-create">
    <span>Nhập email bạn muốn thay đổi .</span>
    <div class="row">
        <?php $form = ActiveForm::begin(); ?>
            <div class="col-md-6">
                <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
                <div class="form-group">
                    <?= Html::submitButton('Update', ['class' => 'btn btn-primary']) ?>
                </div>
            </div>

        <?php ActiveForm::end(); ?>

        <?= $this->render('_order', [
            'model' => $order,
            'statusLabelCss' => $statusLabelCss,
            'dataProvider' => $dataProvider,
        ]) ?>
    </div>






</div>
