<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
$this->title = 'Đổi Email - Đơn hàng số #'.$order->id;
$this->params['breadcrumbs'][] = ['label' => 'Order - Đổi Email', 'url' => ['index','id'=>$order->id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="education-create">
    <div class="row">
        <?php
        $form = ActiveForm::begin(['action' => Yii::$app->urlManager->createUrl(['order/user/confirm',
            'id' => $order->id,
            'model' => $order,
        ])]);
        ?>
            <div class="col-md-6">
                <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
                <div class="form-group">
                    <?= Html::submitButton('Yes', ['class' => 'btn btn-primary']) ?>
                    <?= Html::a('No',  Yii::$app->urlManager->createUrl(['order/user/index',
                        'id' => $order->id,
                    ]),
                        ['class' => 'btn btn-primary']) ?>
                </div>
            </div>

        <?php ActiveForm::end(); ?>

        <?= $this->render('_order', [
            'model' => $order,
            'statusLabelCss' => $statusLabelCss,
            'dataProvider' => $dataProvider,
        ]) ?>
    </div>






</div>
