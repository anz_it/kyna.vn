<?php
use yii\bootstrap\Nav;
use yii\bootstrap\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;
use yii\widgets\Pjax;

use kartik\grid\GridView;
?>

<div class="col-md-6">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">
                <?= $this->title ?>
                <span class="label <?= $statusLabelCss[$model->status] ?>"><?= $model->statusLabel ?></span>
            </h3>
        </div>
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                [
                    'attribute' => 'order_date',
                    'label' => 'Ngày lập',
                    'format' => 'datetime',
                    'value' => !empty($model->order_date) ? $model->order_date : null
                ],
                'user.email:email:Người mua',
                'activation_code:raw:Mã kích hoạt',
                'total:currency:Tổng cộng',
                //'reference_id:raw:Referer',
            ],
        ]) ?>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Thông tin COD</h3>
        </div>
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'shippingAddress.contact_name:raw:Người nhận',
                'shippingAddress.phone_number:raw:Số điện thoại',
                'shippingAddress.addressText:html:Địa chỉ',
                'shippingAddress.vendor_id:raw:Vận chuyển',
                'shippingAddress.note:raw:Ghi chú',
            ],
        ]) ?>
    </div>
</div>
