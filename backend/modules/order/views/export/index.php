<?php

use kartik\grid\GridView;
use yii\bootstrap\Html;
use kyna\user\models\UserCourse;
use app\modules\order\assets\BackendAsset;
use kartik\export\ExportMenu;
use yii\data\ArrayDataProvider;

BackendAsset::Register($this);
$this->title = 'Order Export';

$this->params['breadcrumbs'][] = $this->title;

$formatter = Yii::$app->formatter;
$user = Yii::$app->user;

$gridColumns = [
    ['class' => 'yii\grid\SerialColumn'],
    [
        'header' => 'Order ID',
        'attribute' => 'id',
        'format' => 'raw',
        'value' => function ($model) {
            return $model->id;
        },
    ],
    [
        'header' => 'User ID',
        'attribute' => 'user_id',
        'format' => 'raw',
        'value' => function ($model) {
            return $model->user_id;
        },
    ],
//    'user_type',
    [
        'header' => 'Ngày đăng ký',
        'attribute' => 'order_date',
        'value' => function ($model) use ($formatter) {
            return $formatter->asDatetime(!empty($model->order_date) ? $model->order_date : null);
        },
        'format' => 'raw',
    ],
//    'Ngày thanh toán',
    [
        'header' => 'Ngày kích hoạt',
        'format' => 'raw',
        'value' => function ($model) use ($formatter) {
            $details = $model->getDetails()->one();
            if ($details) {
                $userCourseModel = UserCourse::find()->where([
                    'user_id' => $model->user_id,
                    'course_id' => $details->course_id,
                    'is_activated' => UserCourse::BOOL_YES,
                ])->one();
                if ($userCourseModel) {
                    return $formatter->asDatetime(!empty($userCourseModel->activation_date) ? $userCourseModel->activation_date : null);
                }
                return null;
            }
            return null;
        },
    ],
    [
        'header' => 'Họ tên',
        'format' => 'raw',
        'value' => function ($model) {
            $user = $model->user;
            if ($user && $user->profile) {
                return $user->profile->name;
            }
            return null;
        },
    ],
    [
        'header' => 'Số điện thoại',
        'format' => 'raw',
        'value' => function ($model) {
            $user = $model->user;
            if (empty($user)) {
                return null;
            }
            if (is_null($user->profile->phone_number) && $user->userAddress) {
                return $user->userAddress->phone_number;
            }
            return $user->profile->phone_number;
        },
    ],
    [
        'header' => 'Email',
        'format' => 'raw',
        'value' => function ($model) {
            $user = $model->user;
            if (empty($user)) {
                return null;
            }
            return $user->email;
        },
    ],
    [
        'header' => 'Tỉnh thành',
        'value' => function ($model) {
            $shippingAddress = $model->shippingAddress;
            if ($shippingAddress && $shippingAddress->location && $shippingAddress->location->parent) {
                return $shippingAddress->location->parent->name;
            }
            return null;
        },
        'format' => 'raw',
    ],
    [
        'header' => 'Quyện huyện',
        'value' => function ($model) {
            $shippingAddress = $model->shippingAddress;
            if ($shippingAddress && $shippingAddress->location) {
                return $shippingAddress->location->name;
            }
            return null;
        },
        'format' => 'raw',
    ],
    [
        'header' => 'Địa chỉ',
        'value' => function ($model) {
            $shippingAddress = $model->shippingAddress;
            if ($shippingAddress) {
                return $shippingAddress->street_address;
            }
            return null;
        },
        'format' => 'raw',
    ],
    [
        'label' => 'Sản phẩm',
        'attribute' => 'details',
        'value' => 'detailsText',
        'format' => 'raw',
    ],
//    'Giảng viên',
    [
        'label' => 'Giá trị đơn hàng thực tế (sau khuyến mãi)',
        'attribute' => 'total',
        'format' => 'raw',
    ],
    [
        'header' => 'Chi phí giao nhận (học viên phải chịu)',
        'format' => 'raw',
        'value' => function ($model) use ($formatter) {
            $shipping_fee = null;
            if ($model->isCod) {
                if (!empty($model->shippingAddress->fee)) {
                    $shipping_fee = '<b>' . $formatter->asCurrency($model->shippingAddress->fee) . '</b>';
                }
            }
            return $shipping_fee;
        }
    ],
    [
        'label' => 'Chi phí giao nhận (Kyna trả GHN, đối tác)',
        'attribute' => 'payment_fee',
        'format' => 'raw',
    ],
//    'Giá trị đơn hàng thuần (sau chi phí thanh toán, bằng giá trị đơn hàng thực tế + chi phí giao nhận học viên chịu - chi phí Kyna trả GHN/đối tác)',

    [
        'label' => 'Giá trị đơn hàng thuần',
        'attribute' => 'realInCome',
        'format' => 'raw',
    ],
    [
        'label' => 'Affiliate cuối',
        'attribute' => 'operator_id',
        'value' => function ($model) {
            $affiliate = $model->affiliateLastUser;
            if ($affiliate && $affiliate->user && $affiliate->user->profile) {
                return $affiliate->user->profile->name;
            }
            return null;
        },
        'format' => 'raw',
    ],
    [
        'label' => 'Original affiliate',
        'attribute' => 'affiliate_id',
        'value' => function ($model) {
            $affiliate = $model->originalAffiliateUser;
            if ($affiliate && $affiliate->user && $affiliate->user->profile) {
                return $affiliate->user->profile->name;
            }
            return null;
        },
        'format' => 'raw',
    ],
    [
        'label' => 'Affiliate category',
        'value' => function ($model) {
            $affiliate = $model->originalAffiliateUser;
            if ($affiliate && $affiliate->affiliateCategory) {
                return $affiliate->affiliateCategory->name;
            }
            return null;
        },
        'format' => 'raw',
    ],
    [
        'header' => 'Status',
        'value' => function ($model, $key, $index) use ($callStatuses) {
            $html = "";
            $html .=  $model->statusLabel;

//            if (array_key_exists($model->call_status, $callStatuses)) {
//                $html .= $callStatuses[$model->call_status];
//            }

            return $html;
        },
        'format' => 'raw',
    ],
    [
        'header' => 'Telesales',
        'format' => 'raw',
        'value' => function ($model) {
            $user = $model->operator;
            if ($user && $user->profile) {
                return $user->profile->name;
            }
            return null;
        },
    ],
    [
        'header' => 'Đơn vị giao nhận',
        'format' => 'raw',
        'attribute' => 'payment_method'
    ],
    [
        'header' => 'Mã đơn hàng giao nhận',
        'format' => 'raw',
        'value' => function ($model) {
            $data = $model->shippingAddress;
            if ($data) {
                return $data->shipping_code;
            }
            return null;
        },
    ],

];
?>
<div class="order-index">
    <nav class="navbar navbar-default">
        <?= $this->render('_search', ['queryParams' => $queryParams, 'callStatuses' => $callStatuses, 'allowedStatus' => $allowedStatus]) ?>
        <div class="navbar-form hide-resize">
            <div class="btn-group">
                <button id="w3" class="btn btn-default dropdown-toggle" title="Xuất" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-share-square-o"></i> Export Data <span class="caret"></span></button>
                <ul id="w4" class="dropdown-menu dropdown-menu-right">
                    <li title="Microsoft Excel 95+">
                        <a id="export_xls" class="export-xls" href="javascript:" data-format="application/vnd.ms-excel" tabindex="-1"><i class="text-success fa fa-file-excel-o"></i> Excel</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <div class="box">
        <?=
        GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => false,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                [
                    'header' => 'Thông tin đăng ký',
                    'attribute' => 'user_id',
                    'format' => 'raw',
                    'value' => function ($model) use ($formatter) {
                        $str = '';
                        $str .= 'OrderID: '. $model->id. '<br />';
                        $str .= 'UserID: '. $model->user_id. '<br />';
                        $str .= 'Ngày đăng ký: '. $formatter->asDatetime(!empty($model->order_date) ? $model->order_date : null). '<br />';

                        $user = $model->user;
                        if ($user && $user->profile) {
                            $str .= 'Họ tên: '. $user->profile->name. '<br />';
                            $str .= 'SĐT: '. $user->profile->phone_number . '<br />';
                            $str .= 'Email: '. $user->email. '<br />';
                        } elseif ($user) {
                            $address = $user->userAddress;
                            if (!empty($address)) {
                                $str .= 'Họ tên: '. $address->contact_name. '<br />';
                                $str .= 'SĐT: '. $address->phone_number. '<br />';
                                $str .= 'Email: '. $address->email. '<br />';
                            }
                        }

                        return $str;
                    },
                ],
                [
                    'header' => 'Ngày kích hoạt',
                    'format' => 'raw',
                    'value' => function ($model) use ($formatter) {
                        $details = $model->getDetails()->one();
                        if ($details) {
                            $userCourseModel = UserCourse::find()->where([
                                'user_id' => $model->user_id,
                                'course_id' => $details->course_id,
                                'is_activated' => UserCourse::BOOL_YES,
                            ])->one();
                            if ($userCourseModel) {
                                return $formatter->asDatetime(!empty($userCourseModel->activation_date) ? $userCourseModel->activation_date : null);
                            }
                            return null;
                        }
                        return null;
                    },
                ],
                [
                    'header' => 'Địa chỉ',
                    'value' => function ($model) {
                        $str = '';
                        $shippingAddress = $model->shippingAddress;
                        if ($shippingAddress) {
                            $str .= $shippingAddress->street_address. '<br />';
                        }
                        if ($shippingAddress && $shippingAddress->location) {
                            $str .= $shippingAddress->location->name. ' - ' . $shippingAddress->location->parent->name;
                        }
                        return $str;
                    },
                    'format' => 'raw',
                ],
                [
                    'label' => 'Sản phẩm',
                    'attribute' => 'details',
                    'value' => 'detailsText',
                    'format' => 'raw',
                ],
                [
                    'header' => 'Giá trị đơn hàng',
                    'format' => 'raw',
                    'value' => function($model) use ($formatter){
                        $str = '';
                        $str .= 'Giá trị đơn hàng thực tế: '. $formatter->asCurrency($model->total) .'<br />';
                        $str .= 'Chi phí giao nhận: '. $formatter->asCurrency($model->payment_fee) .'<br />';
                        $str .= 'Giá trị đơn hàng thuần: '. $formatter->asCurrency($model->realInCome) .'<br />';

                        if ($model->isCod) {
                            if (!empty($model->shippingAddress->fee)) {
                                $str .= 'Chi phí giao nhận: ' . $formatter->asCurrency($model->shippingAddress->fee) . '<br />';
                            }
                        }

                        return $str;
                    }
                ],
                [
                    'label' => 'Affiliate',
                    'format' => 'raw',
                    'value' => function ($model) {
                        $str = '';
                        $affiliate = $model->affiliateLastUser;
                        if ($affiliate && $affiliate->user && $affiliate->user->profile) {
                            $str .= 'Affiliate cuối: '.$affiliate->user->profile->name. '<br />';
                        }

                        $affiliate = $model->originalAffiliateUser;
                        if ($affiliate && $affiliate->user && $affiliate->user->profile) {
                            $str .= 'Original affiliate: '.$affiliate->user->profile->name. '<br />';
                        }

                        $affiliate = $model->originalAffiliateUser;
                        if ($affiliate && $affiliate->affiliateCategory) {
                            $str .= 'Affiliate category'. $affiliate->affiliateCategory->name;
                        }
                        return $str;
                    }
                ],
                [
                    'header' => 'Status',
                    'value' => function ($model, $key, $index) use ($callStatuses) {
                        $html = "";
                        $html .=  $model->statusLabel;

//                    if (array_key_exists($model->call_status, $callStatuses)) {
//                        $html .= $callStatuses[$model->call_status];
//                    }

                        return $html;
                    },
                    'format' => 'raw',
                ],
                [
                    'header' => 'Telesales',
                    'format' => 'raw',
                    'value' => function ($model) {
                        $user = $model->operator;
                        if ($user && $user->profile) {
                            return $user->profile->name;
                        }
                        return null;
                    },
                ],
                [
                    'header' => 'Giao nhận',
                    'format' => 'raw',
                    'value' => function ($model) {
                        $data = $model->shippingAddress;
                        if ($data && $data->vendor) {
                            return $data->shipping_code. ' - '.$data->vendor->name;
                        }
                        return null;
                    },
                ],
            ]
        ]);
        ?>
    </div>
</div>
<?php
$script = "
    ;(function($, window, document, undefined){
        $(document).ready(function(){
            $('body').on('click', '#export_xls', function (event) {
                event.preventDefault();
                var exportDialog = new BootstrapDialog();
                exportDialog.setTitle('Vui lòng nhập email nhận file báo cáo');
                exportDialog.setMessage($('<input id=\"email_export\" class=\"form-control\" placeholder=\"Email\" value=\"".Yii::$app->user->identity->email."\"></input>'));
                exportDialog.setType(BootstrapDialog.TYPE_SUCCESS);
                exportDialog.setButtons([{
                    label: 'Export',
                    action: function(dialog) {
                        var email = $('#email_export').val(); 
                        if (email == '') {
                            alert('Vui lòng nhập email');
                        }
                        var data = {email: email};
                        $.post('', data, function (response) {
                            if (response.result) {
                                var bootstrapDialog = new BootstrapDialog();
                                bootstrapDialog.setMessage('Vui lòng kiểm tra email '+email+'. File Báo cáo sẽ được gửi đến trong vài phút.');
                                bootstrapDialog.setType(BootstrapDialog.TYPE_SUCCESS);
                                bootstrapDialog.open(); 
                                exportDialog.close();          
                            } else {
                                
                            }
        
                        });
                    }
                }]);
                exportDialog.open(); 
            });
            $('body').on('click', '#export_xls_submit', function (event) {
                event.preventDefault();
                var data = {};
                $.post('', data, function (response) {
                    if (response.result) {
                        var bootstrapDialog = new BootstrapDialog();
                        bootstrapDialog.setMessage('Vui lòng kiểm tra email ".Yii::$app->user->identity->email.". File Báo cáo sẽ được gửi đến trong vài phút.');
                        bootstrapDialog.setType(BootstrapDialog.TYPE_SUCCESS);
                        bootstrapDialog.open();                  
                    } else {
                        
                    }

                });
            });
        });
    })(window.jQuery || window.Zepto, window, document);";
$css = "
    .modal-content .modal-header {
        border-radius: 0;
    }
    ";
?>
<?php
$this->registerCss($css);
$this->registerJs($script, \yii\web\View::POS_END, 'export-order');
?>
