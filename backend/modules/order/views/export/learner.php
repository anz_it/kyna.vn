<?php
use kartik\grid\GridView;
use yii\grid\SerialColumn;
use kartik\export\ExportMenu;

$this->title = 'Thông tin học viên đăng ký';

$gridColumns = [
    [
    'class' => SerialColumn::className(),
    ],
    'id:text:Đơn hàng số',
    'created_time:datetime:Ngày tạo',
    // 'order_date:datetime:Ngày thanh toán',
    'statusLabel:html:Tình trạng',
    'detailsText:html:Khóa học',
    'affiliate_id',
    'user.email:email:Học viên',
    'user.profile.phone_number:text:SĐT đăng ký',
    'shippingAddress.phone_number:text:ĐT liên hệ',
    'shippingAddress.addressText:text:Địa chỉ nhận code',
];
?>

<nav class="navbar navbar-default">
    <?= $this->render('_search-learner', ['queryParams' => $queryParams]) ?>
    <?php
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => $gridColumns,
        // the toolbar setting is default
        'toolbar' => [
        ],
        'layout' => '{toolbar}<div class=\'hidden\'>{items}</div>',
        // configure your GRID inbuilt export dropdown to include additional items
        'options' => [
            'class' => 'navbar-form'
        ]
    ]);
    ?>
</nav>

<?=  GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => $gridColumns,
]) ?>
<?php
$script = "
    ;(function($, window, document, undefined){
        $(document).ready(function(){
            $('body').on('click', '.export-xls', function (event) {            
                event.preventDefault();
                var courseType = $(this).data('type');
                var exportDialog = new BootstrapDialog();
                exportDialog.setTitle('Vui lòng nhập email nhận file export');
                exportDialog.setMessage($('<input id=\"email_export\" class=\"form-control\" placeholder=\"Email\" value=\"".Yii::$app->user->identity->email."\"></input>'));
                exportDialog.setType(BootstrapDialog.TYPE_SUCCESS);
                exportDialog.setButtons([{
                    label: 'Export',
                    action: function(dialog) {                       
                        var email = $('#email_export').val(); 
                        if (email == '') {
                            alert('Vui lòng nhập email');
                        }
                        var data = {email: email, type: 'export', course_type: courseType};
                        $.post('', data, function (response) {
                            if (response.result) {
                                var bootstrapDialog = new BootstrapDialog();
                                bootstrapDialog.setMessage('Vui lòng kiểm tra email '+email+'. File Export sẽ được gửi đến trong vài phút.');
                                bootstrapDialog.setType(BootstrapDialog.TYPE_SUCCESS);
                                bootstrapDialog.open(); 
                                exportDialog.close();          
                            } else {
                                
                            }
                        });
                    }
                }]);
                exportDialog.open(); 
            });
        });
    })(window.jQuery || window.Zepto, window, document);";
$css = "
    .modal-content .modal-header {
        border-radius: 0;
    }
    ";
?>
<?php
$this->registerCss($css);
$this->registerJs($script, \yii\web\View::POS_END, 'export-user-care');
?>

