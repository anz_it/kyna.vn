<?php

use yii\bootstrap\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use kyna\base\assets\DateRangeAsset;
use kyna\order\models\Order;
use kartik\select2\Select2;

DateRangeAsset::register($this);

$fromDate = date('d/m/Y', strtotime('last week'));
$toDate = date('d/m/Y', strtotime('today'));
if (isset($queryParams['from_date'])) {
    $fromDate = date('d/m/Y', $queryParams['from_date']);
}
if (isset($queryParams['to_date'])) {
    $toDate = date('d/m/Y', $queryParams['to_date']);
}

?>

<?= Html::beginForm(Url::toRoute(['/order/export/learner']), 'get', [
    'class' => 'navbar-form navbar-left',
    'role' => 'search',
]) ?>
    <div class="form-group">
        <?= Select2::widget([
            'data' => [],
            'value' => $queryParams['ids'],
            'name' => 'ids',
            'options' => [
                'multiple' => true,
                'placeholder' => '-- Chọn danh sách khóa học --',
                'class' => 'form-control'
            ],
            'showToggleAll' => false,
            'pluginOptions' => [
                'width' => '400px',
                //'tags' => true,
                'allowClear' => true,
                'minimumInputLength' => 3,
                'language' => [
                    'errorLoading' => new JsExpression("function () { return 'Chờ nhận kết quả...'; }"),
                ],
                'ajax' => [
                    'url' => Url::toRoute(['/course/api/search', 'auto' => true]),
                    'dataType' => 'json',
                    'data' => new JsExpression('function(params) { return {q: params.term, course_type: "all"}; }'),
                ],
                'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                'templateResult' => new JsExpression('function(teacher) { return teacher.text; }'),
                'templateSelection' => new JsExpression('function (teacher) { return teacher.text; }'),
            ],
        ]) ?>
    </div>
    <div class="form-group">
        <?= Html::textInput('date-range', isset($queryParams['date-range']) ? $queryParams['date-range'] : '', [
            'placeholder' => 'Ngày mua',
            'class' => 'form-control',
            'style' => 'width: 170px;',
            'data-control' => 'daterangepicker',
            'data-start-date' => $fromDate,
            'data-end-date' => $toDate,
            'data-max-date' => date('d/m/Y', strtotime('today')),
            //'data-auto-update-input' => 'false',
        ]) ?>
    </div>
    <div class="form-group">
        <label>
        <?= Html::checkbox('completeOnly', $queryParams['completeOnly'] != false) ?> Chỉ hiện các đơn hàng đã thanh toán
        </label>
    </div>

    <?= Html::submitButton('<i class="ion-android-search"></i> Tìm đơn hàng', ['class' => 'btn btn-default']) ?>

    <div class="btn-group">
        <button id="w3" class="btn btn-default dropdown-toggle" title="Export data in selected format" data-toggle="dropdown"><i class="glyphicon glyphicon-export"></i> Export Excel <span class="caret"></span></button>
        <ul id="w4" class="dropdown-menu">
            <li>
                <a data-type="filter" class="export-xls" href="javascript:" data-format="application/vnd.ms-excel" tabindex="-1"><i class="text-success fa fa-file-excel-o"></i> Danh sách khóa học được chọn</a>
                <a data-type="all" class="export-xls" href="javascript:" data-format="application/vnd.ms-excel" tabindex="-1"><i class="text-success fa fa-file-excel-o"></i> Tất cả khóa học</a>
            </li>
        </ul>
    </div>
<?= Html::endForm(); ?>
