<?php

use yii\helpers\Url;
use yii\helpers\Html;
use kartik\form\ActiveForm;
use yii\web\JsExpression;
use kartik\widgets\Select2;
use kartik\daterange\DateRangePicker;
use kyna\commission\models\AffiliateCategory;
use yii\helpers\ArrayHelper;
?>

<div class="revenue-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'options' => ['class' => 'navbar-form navbar-left'],
        'method' => 'get',
    ]); ?>

    <?=
    $form->field($model, 'date_ranger', [
        'addon' => ['prepend' => ['content' => '<i class="glyphicon glyphicon-calendar"></i>']],
        'options' => ['class' => 'form-group'],
    ])->widget(DateRangePicker::classname(), [
        'useWithAddon' => true,
        'convertFormat' => true,
        'pluginOptions'=>[
            'timePicker' => true,
            'locale'=>[
                'format' => 'd/m/yy',
                'separator'=> " - ", // after change this, must update in controller
            ],
        ]
    ])->label(false)->error(false)
    ?>

    <div class="form-group">
        <?= Html::submitButton('<i class="ion-android-search"></i> Lọc', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
