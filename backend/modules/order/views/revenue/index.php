<?php

use app\modules\order\models\ActionForm;
use yii\bootstrap\Html;
use yii\grid\GridView;
use yii\bootstrap\Nav;
use yii\bootstrap\ButtonDropdown;
use kyna\user\models\UserCourse;
use app\modules\order\assets\BackendAsset;
use kartik\export\ExportMenu;

BackendAsset::Register($this);
$this->title = 'Thống kê';

$this->params['breadcrumbs'][] = $this->title;

$formatter = Yii::$app->formatter;
$user = Yii::$app->user;
$total = 0;
$totalPayment = 0;
?>
<div class="order-index">
    <?= \yii\bootstrap\Nav::widget([
        'items' => $tabs,
        'options' => ['class' => 'nav-pills'],
    ]) ?>
    <nav class="navbar navbar-default">
        <?= $this->render('_search', ['model' => $searchModel]) ?>
    </nav>
    <div class="box">
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            //'filterModel' => $searchModel,
            'columns' => [
                [
                    'attribute' => 'payment_type',
                    'label' => 'Loại thanh toán'
                ],
                [
                    'attribute' => 'total',
                    'format' => 'currency',
                    'label' => 'Tổng thực nhận',
                    'value' => function($model) use (&$total) {
                        $total += $model->total;
                        return $model->total;
                    }
                ],
                [
                    'attribute' => 'total_reg_filter',
                    'label' => 'Tổng thanh toán',
                    'value' => function($model) use (&$totalPayment) {
                        $totalPayment += $model->total_reg_filter;
                        return $model->total_reg_filter;
                    }
                ],
            ]
        ]); ?>
    </div>

    <div class="row">
        <div class="col-xs-12">
            <div class="box box-solid">
                <div class="box-body">
                    <dl class="dl-horizontal">
                        <dt>Tổng thực nhận</dt>
                        <dd><?=$formatter->asCurrency($total)?></dd>
                        <dt>Tổng thanh toán</dt>
                        <dd><?=$totalPayment?></dd>
                    </dl>
                </div>
            </div>
        </div>
    </div>

</div>
