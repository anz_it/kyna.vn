<?php

use kartik\daterange\DateRangePicker;
use kyna\order\models\Order;
use yii\bootstrap\Html;
use app\modules\order\assets\BackendAsset;
use kartik\form\ActiveForm;
use kartik\widgets\Select2;
use yii\helpers\Url;
use yii\web\JsExpression;

BackendAsset::Register($this);
$this->title = 'Thống kê';

$this->params['breadcrumbs'][] = $this->title;

$formatter = Yii::$app->formatter;
$user = Yii::$app->user;
$total = 0;
$totalPayment = 0;
?>
<div class="order-index">
    <?= \yii\bootstrap\Nav::widget([
        'items' => $tabs,
        'options' => ['class' => 'nav-pills'],
    ]) ?>
    <nav class="navbar navbar-default">
        <div class="revenue-search">

            <?php $form = ActiveForm::begin([
                'action' => ['cod'],
                'options' => ['class' => 'navbar-form navbar-left'],
                'method' => 'get',
            ]); ?>

            <?=
            $form->field($searchModel, 'date_ranger', [
                'addon' => ['prepend' => ['content' => '<i class="glyphicon glyphicon-calendar"></i>']],
                'options' => ['class' => 'form-group'],
            ])->widget(DateRangePicker::classname(), [
                'useWithAddon' => true,
                'convertFormat' => true,
                'pluginOptions'=>[
                    'timePicker' => true,
                    'locale'=>[
                        'format' => 'd/m/yy',
                        'separator'=> " - ", // after change this, must update in controller
                    ],
                ]
            ])->label(false)->error(false)
            ?>

            <?php
            $initText = !empty($searchModel->operator_id) ? $searchModel->operator->username : '';
            echo $form->field($searchModel, 'operator_id', [
                'options' => ['class' => 'form-group', 'style' => 'width: 200px'],
            ])->widget(Select2::classname(), [
                'initValueText' => $initText,
                'options' => ['placeholder' => '--Chọn user--'],
                'pluginOptions' => [
                    'allowClear' => true,
                    'minimumInputLength' => 3,
                    'language' => [
                        'errorLoading' => new JsExpression("function () { return 'Chờ nhận kết quả...'; }"),
                    ],
                    'ajax' => [
                        'url' => Url::toRoute(['/user/api/search']),
                        'dataType' => 'json',
                        'data' => new JsExpression('function(params) { return {q:params.term}; }'),
                    ],
                    'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                    'templateResult' => new JsExpression('function(res) { return res.text; }'),
                    'templateSelection' => new JsExpression('function (res) { return res.text; }'),
                ],
            ])->label(false)->error(false);?>

            <div class="form-group">
                <?= Html::submitButton('<i class="ion-android-search"></i> Lọc', ['class' => 'btn btn-default']) ?>
            </div>

            <?php ActiveForm::end(); ?>

        </div>
    </nav>
    <div class="row">
        <div class="col-xs-6">
            <div class="box box-solid">
                <div class="box-header with-border">
                    <h3 class="box-title">Đơn hàng</h3>
                    <div class="box-tools pull-right">
                        <!-- Buttons, labels, and many other things can be placed here! -->
                        <!-- Here is a label for example -->
                        <span class="badge label-primary"> Tổng đơn hàng: <?=$orderCount?></span>
                    </div><!-- /.box-tools -->
                </div><!-- /.box-header -->
                <div class="box-body">
                    <dl class="dl-horizontal">
                        <?php foreach($callStatusSummary as $k => $v) { ?>
                                <dt><?=$callStatuses[$k]?>:</dt>
                                <dd><?=$v?> ( <?= $formatter->asPercent(!empty($orderCount)? $v / $orderCount :0)?>)</dd>
                        <?php } ?>
                    </dl>
                </div>
            </div>
        </div>

        <div class="col-xs-6">
            <div class="box box-solid">
                <div class="box-header with-border">
                    <h3 class="box-title">Thời gian gọi</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <dl>
                        <dt>Trung bình 1 cuộc:</dt>
                        <dd><?=$formatter->asDecimal(!empty($timeSummary['count'])?$timeSummary['summary'] / $timeSummary['count']:0)?></dd>
                        <dt>Tổng thời gian gọi:</dt>
                        <dd><?=$timeSummary['summary']?></dd>
                        <dt>Tổng số cuộc gọi chưa liên hệ được:</dt>
                        <dd><?=$timeSummary['summaryFailed']?></dd>
                    </dl>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-6">
            <div class="box box-solid">
                <div class="box-header with-border">
                    <h3 class="box-title">Kết quả chung</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <dl class="dl-horizontal">
                        <dt><?=$orderStatuses[Order::ORDER_STATUS_COMPLETE]?>:</dt>
                        <dd><?=$statusSummary[Order::ORDER_STATUS_COMPLETE]?> ( <?= $formatter->asPercent(!empty($orderCount)? $statusSummary[Order::ORDER_STATUS_COMPLETE] / $orderCount :0)?>)</dd>
                    </dl>
                </div>
            </div>
        </div>

        <div class="col-xs-6">
            <div class="box box-solid">
                <div class="box-header with-border">
                    <h3 class="box-title">Mức độ gọi nhanh</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <dl class="dl-horizontal">
                        <dt>Tổng số gọi:</dt>
                        <dd><?=$timeSummary['count']?></dd>
                        <dt>Gọi trong 5 phút:</dt>
                        <dd><?=$timeSummary['countIn5Minutes']?> (<?= $formatter->asPercent(!empty($orderCount) && !empty($timeSummary['count']) ? $timeSummary['countIn5Minutes'] / $timeSummary['count'] :0)?>)</dd>
                        <dt>Gọi trong 5 – 15 phút:</dt>
                        <dd><?=$timeSummary['countFrom5To15Minutes']?> (<?= $formatter->asPercent(!empty($orderCount) && !empty($timeSummary['count']) ? $timeSummary['countFrom5To15Minutes'] / $timeSummary['count'] :0)?>)</dd>
                        <dt>Gọi trên 15 phút:</dt>
                        <dd><?=$timeSummary['countMore15Minutes']?> (<?= $formatter->asPercent(!empty($orderCount) && !empty($timeSummary['count']) ? $timeSummary['countMore15Minutes'] / $timeSummary['count'] :0)?>)</dd>
                    </dl>
                </div>
            </div>
        </div>
    </div>

</div>
