<?php

use app\modules\order\models\ActionForm;
use app\modules\order\models\RevenueSearch;
use kartik\date\DatePicker;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use yii\grid\GridView;
use yii\bootstrap\Nav;
use yii\bootstrap\ButtonDropdown;
use kyna\user\models\UserCourse;
use app\modules\order\assets\BackendAsset;
use kartik\export\ExportMenu;

BackendAsset::Register($this);
$this->title = 'Thống kê';

$this->params['breadcrumbs'][] = $this->title;

$formatter = Yii::$app->formatter;
$user = Yii::$app->user;
$total = 0;
$totalPayment = 0;
?>
<div class="order-index">
    <?= \yii\bootstrap\Nav::widget([
        'items' => $tabs,
        'options' => ['class' => 'nav-pills'],
    ]) ?>
    <nav class="navbar navbar-default">
        <div class="revenue-search">

            <?php $form = ActiveForm::begin([
                'action' => ['daily'],
                'options' => ['class' => 'navbar-form navbar-left'],
                'method' => 'get',
            ]); ?>

            <?=
            $form->field($searchModel, 'date', [
//                'addon' => ['prepend' => ['content' => '<i class="glyphicon glyphicon-calendar"></i>']],
                'options' => ['class' => 'form-group'],
            ])->widget(DatePicker::classname(), [
//                'useWithAddon' => true,
                'convertFormat' => true,
                'pluginOptions'=>[
                    'todayHighlight' => true,
                    'todayBtn' => true,
                ]
            ])->label(false)->error(false)
            ?>

            <div class="form-group">
                <?= Html::submitButton('<i class="ion-android-search"></i> Lọc', ['class' => 'btn btn-default']) ?>
            </div>

            <?php ActiveForm::end(); ?>

        </div>
    </nav>
    <div class="box">
        <table class="table table-bordered table-striped text-center">
            <tr>
                <th rowspan="2">Ngày đăng ký</th>
                <th colspan="3">Order Value</th>
                <th colspan="3">Ước lượng doanh thu</th>
            </tr>
            <tr>
                <th>Đăng ký COD</th>
                <th>Đăng ký Online</th>
                <th>Tổng tiền</th>
                <th>Doanh thu COD ước tính</th>
                <th>Thực nạp tiền Online và CK</th>
                <th>Tổng tiền Ước tính</th>
            </tr>

            <?php
            $codNum = 0;
            $codTotal = 0;
            $onlineNum = 0;
            $onlineTotal = 0;
            $totalMoney = 0;

            $codNumEstimate = 0;
            $codTotalEstimate = 0;

            $paymentTotal = 0;
            for($i=1; $i<14; $i++) {
                $day = $date->format('d/m/Y');

                // Calculate for order value
                RevenueSearch::SelectRegisteredByDay($day, $codNum, $codTotal);
                RevenueSearch::SelectRegisteredByDayOnline($day, $onlineNum, $onlineTotal);
                $totalMoney = $onlineTotal + $codTotal;

                // estimate revenue
                RevenueSearch::SelectRegisteredByDayEstimate($day, $codNumEstimate, $codTotalEstimate); // COD
                RevenueSearch::SelectPaymentToReportByDay($day, $paymentTotal);
                $percentPayment = 0;
                if (!empty($onlineTotal) && $onlineTotal > 0)
                    $percentPayment = $formatter->asPercent($paymentTotal / $onlineTotal);

                $percentCodEstimate = 0;
                if (!empty($codTotal) && $codTotal > 0)
                    $percentCodEstimate = $formatter->asPercent($codTotalEstimate / $codTotal);

                $totalEstimate = $paymentTotal + $codTotalEstimate;
                $percentTotalEstimate = 0;
                if (!empty($totalMoney) && $totalMoney > 0)
                    $percentTotalEstimate = $formatter->asPercent($totalEstimate / $totalMoney);

                ?>
                <tr>
                    <td><?= $day ?></td>
                    <td><?= $codNum ?> / <?= $formatter->asCurrency($codTotal) ?></td>
                    <td><?= $onlineNum ?> / <?= $formatter->asCurrency($onlineTotal) ?></td>
                    <td><?= $formatter->asCurrency($totalMoney) ?></td>
                    <td><?= $codNumEstimate ?> / <?= $formatter->asCurrency($codTotalEstimate) ?> ( <?= $percentCodEstimate ?>)</td>
                    <td><?= $formatter->asCurrency($paymentTotal) ?> ( <?= $percentPayment ?>)</td>
                    <td><?= $formatter->asCurrency($totalEstimate) ?> ( <?= $percentTotalEstimate ?>)</td>
                </tr>
                <?php
                $date = $date->sub(new DateInterval('P1D'));
            }
            ?>
        </table>
    </div>

</div>
