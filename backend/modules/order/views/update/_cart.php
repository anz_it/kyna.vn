<?php

use yii\grid\GridView;
use yii\bootstrap\Html;
use kyna\course\models\Course;
use kyna\promo\models\GroupDiscount;

$formatter = Yii::$app->formatter;

$subtotal = 0;
$subtotalNotCombo = 0;
$discountAmount = 0;
?>
<?php
if ($groupDiscount) {
    echo "<div class='text-green'>{$groupDiscount->name}" . (!empty($groupDiscount->max_discount_amount) ? ", tối đa " . $formatter->asCurrency($groupDiscount->max_discount_amount) : '') . "</div>";
} else {
    $nearGroupDiscount = GroupDiscount::find()
                ->andWhere(['>', 'course_quantity', $courseQuantity])
                ->andWhere(['status' => GroupDiscount::STATUS_ACTIVE])
                ->orderBy(['course_quantity' => SORT_ASC])
                ->one();
    if (!is_null($nearGroupDiscount)) {
        echo "<div class='text-yellow'>{$nearGroupDiscount->name} " . (!empty($nearGroupDiscount->max_discount_amount) ? ", tối đa " . $formatter->asCurrency($nearGroupDiscount->max_discount_amount) : '') . "</div>";
    }
}
    
echo GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        [
            'attribute' => 'name',
            'format' => 'raw',
            'value' => function ($model) {
                if ($model->type == Course::TYPE_COMBO) {
                    return $model->comboName;
                }
                
                return $model->name;
            }
        ],
        [
            'attribute' => 'price',
            'label' => 'Giá gốc',
            'format' => 'currency',
            'value' => function ($model) {
                return $model->oldPrice;
            },
        ],
        [
            'attribute' => 'price_discount',
            'label' => 'Giảm',
            'format' => 'currency',
            'value' => function ($model) {
                return $model->discountAmount;
            },
        ],
        [
            'attribute' => 'total_price',
            'label' => 'Thành tiền',
            'format' => 'currency',
            'value' => function ($model) use (&$subtotal,&$subtotalNotCombo) {
                $subtotal += $model->sellPrice;
                if(($model->type != Course::TYPE_COMBO) && (!in_array($model->id,\common\campaign\CampaignTet::COURSE_NOT_APPLY_VOUCHER))){
                    $subtotalNotCombo += $model->sellPrice;
                }
                return $model->sellPrice;
            },
        ],
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{delete}',
            'buttons' => [
                'delete' => function ($url, $model, $key) {
                    $options = [
                        'title' => Yii::t('yii', 'Delete'),
                        'aria-label' => Yii::t('yii', 'Delete'),
                        'data-id' => $model->id,
                        'class' => 'remove-cart-item',
                    ];
                    return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, $options);
                }
            ]
        ],
    ],
]);
if (!is_null($groupDiscount)) {
    $discountAmount = $subtotal * $groupDiscount->percent_discount / 100;
    if (!empty($groupDiscount->max_discount_amount) && $discountAmount > $groupDiscount->max_discount_amount) {
        $discountAmount = $groupDiscount->max_discount_amount;
    }
}
echo Html::hiddenInput('discount-amount', $discountAmount, ['id' => 'discount-amount']);
echo Html::hiddenInput('discount-amount-text', $formatter->asCurrency($discountAmount), ['id' => 'discount-amount-text']);
echo Html::hiddenInput('subtotal', $subtotal, ['id' => 'order-subtotal']);
echo Html::hiddenInput('subtotal-text', $formatter->asCurrency($subtotal), ['id' => 'order-formatted-subtotal']);
echo Html::hiddenInput('shipping-fee-format', $formatter->asCurrency($shippingFee), ['id' => 'order-formatted-shipping-fee']);
echo Html::hiddenInput('shipping-fee-0-format', $formatter->asCurrency(0), ['id' => 'order-formatted-shipping-fee-0']);
if(\common\campaign\CampaignTet::InTimesCampaign()){
    $voucherLixi = \common\campaign\CampaignTet::voucherLixTetRender($subtotalNotCombo);
    echo Html::hiddenInput('voucherName', $voucherLixi, ['id' => 'voucherLixi']);
}
