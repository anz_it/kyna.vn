<?php

use yii\helpers\Url;
use yii\web\View;
use common\widgets\locationfield\LocationField;

?>
<div class="row">
    <div class="col-sm-6">
        <?= $form->field($model, 'shipping_contact_name') ?>
    </div>
    <div class="col-sm-6">
        <?= $form->field($model, 'shipping_phone_number') ?>
    </div>
</div>
<?= $form->field($model, 'shipping_street_address') ?>
<?= $form->field($model, 'shipping_location_id')->widget(LocationField::className())->label(false) ?>
<script type="text/javascript">
    var csrfToken = "<?= Yii::$app->request->getCsrfToken(); ?>";
    var shipping_fee_url = " <?php echo Url::toRoute(['/order/cart/get-shipping-fee']) ?> ";
</script>
<?php
$script = "
    ;(function ($, _) {
        $('body').on('change', '#createorderform-shipping_location_id-district', function (e) {
            var location_id = $(this).val();
            var total = $('#order-subtotal').val();
            var payment_method = $('#payment-method').val();

            if (payment_method !== 'cod') {
                return;
            }
            
            $.ajax({
                url: shipping_fee_url,
                type: 'POST',
                async: false,
                dataType: 'json',
                data: {location_id: location_id, total: total, _csrf: csrfToken},
                success: function(resp){
                    if (resp.error_code == 0){
                        $('#shipping-amount').html(resp.object.feeFormat);
                    }
                }
            })
        });
    })(jQuery, _);
    ";

$this->registerJs($script, View::POS_END, 'get-shipping-fee');
?>
