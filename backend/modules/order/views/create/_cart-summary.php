<?php

use yii\bootstrap\Html;

?>
<div class="form form-horizontal">
    <div class="form-group">
        <label class="control-label col-sm-5">Tổng thành tiền</label>
        <div class="col-sm-7">
            <p class="text-right form-control-static" id='subtotal-text'><?= Yii::$app->formatter->asCurrency(0) ?></p>
        </div>
    </div>
    <div class="form-group">
        <?= Html::activeLabel($model, 'promotion_code', ['class' => 'control-label col-sm-5']); ?>
        <?= $form->field($model, 'promotion_code', [
            'options' => ['class' => 'col-sm-7'],
            'inputOptions' => ['class' => 'form-control text-right'],
        ])->input('text')->label(false) ?>
    </div>
    <div class="form-group">
        <?= Html::activeLabel($model, 'direct_discount_amount', ['class' => 'control-label col-sm-5']); ?>
        <?= $form->field($model, 'direct_discount_amount', [
            'options' => ['class' => 'col-sm-7'],
            'inputOptions' => ['class' => 'form-control text-right'],
        ])->input('number')->label(false) ?>
    </div>
    <div class="form-group">
        <?= Html::activeLabel($model, 'group_discount', ['class' => 'control-label col-sm-5']); ?>
        <?= $form->field($model, 'group_discount', [
            'options' => ['class' => 'col-sm-7'],
        ])->checkbox()->label('Cho phép') ?>
    </div>
    <div class="form-group">
        <?= Html::activeLabel($model, 'group_discount_amount', ['class' => 'control-label col-sm-5']); ?>
        <div class="col-sm-7">
            <?= Html::activeHiddenInput($model, 'group_discount_amount', ['id' => 'group_discount_amount']) ?>
            <p class="text-right form-control-static" id='group_discount_amount_text'>
                <?= Yii::$app->formatter->asCurrency(!empty($model->group_discount_amount) ? $model->group_discount_amount : 0) ?>
            </p>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-5">Phí giao hàng</label>
        <div class="col-sm-7">
            <p id="shipping-amount" class="text-right form-control-static">VND 0.00</p>
        </div>
    </div>
</div>
