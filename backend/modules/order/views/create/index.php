<?php

use yii\web\JsExpression;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use yii\web\View;

use common\widgets\autocomplete\AutoComplete;
use kyna\course\models\Course;
use app\modules\order\assets\BackendAsset;

BackendAsset::register($this);

$this->title = 'Create Order';
$this->params['breadcrumbs'][] = ['label' => 'Orders', 'url' => ['/order/default/index']];
$this->params['breadcrumbs'][] = $this->title;

$formId = 'shopping-cart';

$form = ActiveForm::begin();
?>
<div class="order-create">
    <div class="row">
        <section class="col-md-8 col-sm-6">
            <h3>Thông tin đăng ký</h3>
            <div class="row">
                <div class="col-sm-6">
                    <?= $form->field($model, 'user_email') ?>
                </div>
                <div class="col-sm-6">
                    <?= $form->field($model, 'payment_method')->dropDownList($paymentMethods, [
                        'id' => 'payment-method',
                    ]) ?>
                </div>
            </div>
                <div class="row">
                    <div class="col-sm-12">
                        <?= $form->field($model, 'auto_password')->checkbox() ?>
                    </div>
                </div>
            <div class="row">
                <div class="col-sm-12">
                    <?= $form->field($model, 'activation_code')->textInput(['placeHolder' => 'Số serial là bắt buộc nếu là Khoá học giành cho đối tác']) ?>
                </div>
            </div>
            <div id="order-user">
                <?= $this->render('_user-editable', [
                    'model' => $model,
                    'form' => $form,
                ]) ?>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <?= $form->field($model, 'note')->textarea() ?>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <?= $form->field($model, 'is_4kid')->checkbox() ?>
                </div>
            </div>
            <h3>Danh sách khóa học</h3>
            <div class="form-group">
                <label for="course-search">Tên khóa học hoặc combo muốn thêm</label>
                <?= AutoComplete::widget([
                    'id' => 'course-typeahead',
                    'name' => 'course',
                    'options' => [
                        'class' => 'form-control',
                        'placeholder' => 'Nhập tên khóa học',
                    ],
                    'clientOptions' => [
                        'minLength' => 2,
                        'highlight' => 'true',
                    ],
                    'dataset' => [
                        'displayKey' => 'name',
                        'limit' => 100
                    ],
                    'bloodhound' => [
                        'datumTokenizer' => new JsExpression('Bloodhound.tokenizers.obj.whitespace("name")'),
                        'queryTokenizer' => new JsExpression('Bloodhound.tokenizers.whitespace'),
                        'remote' => [
                            'url' => Url::to(['/course/api/search', 'type' => [Course::TYPE_VIDEO, Course::TYPE_COMBO, Course::TYPE_SOFTWARE]]) . '&q=%QUERY',
                            'wildcard' => '%QUERY',
                        ],
                    ],
                ]) ?>
                <?= $form->field($model, 'course_list')->hiddenInput(['id' => 'cart-items'])->label(false); ?>
                <?= $form->field($model, 'group_discount_id')->hiddenInput(['id' => 'group_discount_id'])->label(false); ?>
            </div>
            <div class="form-group" id="cart-content"></div>
        </section>
        <aside class="col-md-4 col-sm-6">
            <div class="panel panel-default">
                <div class="panel-heading">Tổng kết</div>
                <div class="panel-body">
                    <?= $this->render('_cart-summary', [
                        'model' => $model,
                        'form' => $form,
                    ]) ?>
                </div>
                <div class="panel-footer">
                    <button type="submit" class="btn btn-primary btn-block">Tạo đơn hàng</button>
                </div>
            </div>
        </aside>
    </div>
</div>
<?php ActiveForm::end(); ?>


<?php
$script = "
    ;(function ($, _) {
        $('body').on('change', '#payment-method', function (e) {
            var method = $(this).val();
            if (method == 'cod') {
                $('#createorderform-shipping_location_id-district').trigger('change');
            } else {
                $('#shipping-amount').html('" . Yii::$app->formatter->asCurrency(0) . "');
            }
        });
    })(jQuery, _);
    ";

$this->registerJs($script, View::POS_END, 'payment-method-change');
?>