<?php
use yii\bootstrap\Nav;
use yii\bootstrap\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;

$this->title = 'Đơn hàng số #'.$model->id;
$this->params['breadcrumbs'][] = ['label' => 'Orders', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>
<?= Nav::widget([
    'items' => [
        [
            'label' => 'Thông tin đơn hàng',
            'url' => Url::toRoute(['view', 'id' => $model->id]),
        ],
        [
            'label' => 'Lịch sử',
            'url' => Url::toRoute(['history', 'id' => $model->id]),
            'active' => true,
        ]
    ],
    'options' => ['class' => 'nav-pills'],
]) ?>
<nav class="navbar navbar-default">
    <?= Html::beginForm(Url::toRoute(['history', 'id' => $model->id]), 'get', [
        'class' => 'navbar-form navbar-left',
        'role' => 'search',
        'id' => 'action-form'
    ]) ?>
        <div class="form-group">
            <?= Html::dropDownList('name', $actionName, $availableActions, [
                'class' => 'form-control',
                'id' => 'action-list',
            ]) ?>
        </div>
    <?= Html::endForm(); ?>
</nav>
<?= GridView::widget([
    'dataProvider' => $actionDataProvider,
    'columns' => [
        'name:raw:Thao tác',
        'user.profile.name:raw:Người thực hiện',
        'user.email:email',
        'action_time:datetime:Thực hiện lúc',
        [
            'label' => 'Ghi chú',
            'format' => 'raw',
            'value' => function ($model) {
                /* @var $model \kyna\order\models\actions\OrderAction */
                $orderActionMeta = ArrayHelper::map($model->getOrderActionMeta()->select(['key', 'value'])->all(), 'key', 'value');
                $str = '';
                foreach ($orderActionMeta as $key => $val) {
                    $str .= "$key: $val<br>";
                }

                return $str;
            }
        ],
    ]
]) ?>

<?php
$js = <<<JS
;(function($){
    $("#action-list").on("change", function (e) {
        $("#action-form").unbind("submit").submit();
    });
})(jQuery);
JS;
$this->registerJs($js);
