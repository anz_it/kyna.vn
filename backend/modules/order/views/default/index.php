<?php

use yii\bootstrap\Html;
use yii\grid\GridView;
use yii\bootstrap\Nav;
use yii\bootstrap\ButtonDropdown;
use yii\helpers\Url;
use yii\widgets\Pjax;

use app\modules\order\models\ActionForm;
use app\modules\order\assets\BackendAsset;

use kyna\payment\models\PaymentTransaction;
use kyna\payment\lib\proship\Proship;
use kyna\payment\lib\ghtk\Ghtk;
use kyna\payment\lib\ghn\Ghn;

use common\helpers\CDNHelper;

BackendAsset::Register($this);
$this->title = $this->context->mainTitle;

$this->params['breadcrumbs'][] = $this->title;

$formatter = Yii::$app->formatter;
$user = Yii::$app->user;

$cdnUrl = CDNHelper::getMediaLink();
$isTrackingFb = Yii::$app->session->hasFlash('need-pixel-tracking');
?>

<?php if (!empty($this->context->userCartTabs)) : ?>
    <?= Nav::widget([
        'items' => $this->context->userCartTabs,
        'options' => ['class' => 'nav-pills'],
    ]) ?>
    <br>
<?php endif; ?>

<div class="order-index">
    <?= Nav::widget([
        'items' => $tabs,
        'options' => ['class' => 'nav-pills'],
    ]) ?>
    <nav class="navbar navbar-default">
        <?= $this->render('_search', ['queryParams' => $queryParams, 'callStatuses' => $callStatuses, 'allowedStatus' => $allowedStatus]) ?>
    </nav>
    <?php Pjax::begin(['id' => 'idPjaxReload']) ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            //'filterModel' => $searchModel,
            'columns' => [
                [
                    'header' => 'Đơn hàng',
                    'attribute' => 'order_date',
                    'value' => function ($model, $key, $index) use ($formatter, $statusLabelCss, $callStatuses) {
                        $html = '<strong>#' . $model->id . '</strong><br>';
                        $html .= '<time>' . $formatter->asDatetime(!empty($model->order_date) ? $model->order_date : null) . '</time><br>';
                        $html .= '<span class="label ' . $statusLabelCss[$model->status] . '">' . $model->statusLabel . '</span><br>';
                        if ($model->payment_method == 'cod' && isset($model->shipping_method)) {
                            switch ($model->shipping_method) {
                                case 'proship':
                                    // Get proship shipping status:
                                    $transaction = PaymentTransaction::find()->where([
                                        'order_id' => $model->id,
                                    ])->orderBy('id DESC')->one();
                                    if (isset($transaction) && isset($transaction['shipping_status'])) {
                                        $html .= '<div class="label label-default">'.Proship::getInvoiceStatusText($transaction['shipping_status']).'</div><br>';
                                    }
                                    break;
                                case 'ghtk':
                                    // Get proship shipping status:
                                    $transaction = PaymentTransaction::find()->where([
                                        'order_id' => $model->id,
                                    ])->orderBy('id DESC')->one();
                                    if (isset($transaction) && isset($transaction['shipping_status'])) {
                                        $html .= '<div class="label label-default">'.Ghtk::getShippingStatusText($transaction['shipping_status']).'</div><br>';
                                    }
                                    break;
                                case 'ghn':
                                    // Get ghn shipping status:
                                    $transaction = PaymentTransaction::find()->where([
                                        'order_id' => $model->id,
                                    ])->orderBy('id DESC')->one();
                                    if (isset($transaction) && isset($transaction['shipping_status'])) {
                                        $html .= '<div class="label label-default">'. Ghn::getShippingStatusText($transaction['shipping_status']) . '</div><br>';
                                    }
                                    break;

                                default:
                                    break;
                            }
                        }
                        if (array_key_exists($model->call_status, $callStatuses)) {
                            $html .= $callStatuses[$model->call_status];
                        }


                        return $html;
                    },
                    'format' => 'raw',
                ],
                [
                    'attribute' => 'details',
                    'value' => 'detailsText',
                    'format' => 'raw',
                ],
                [
                    'header' => 'Người đăng ký',
                    'format' => 'raw',
                    'value' => function ($model, $key, $index) use ($formatter) {
                        $user = $model->user;
                        if (empty($user)) {
                            return null;
                        }
                        if (!empty($user->userAddress)) {
                            $info = $user->userAddress->toString(false);
                        } else {
                            // get phone number from user register
                            $info = $user->profile->phone_number;
                        }
                        return $formatter->asEmail($user->email) . '<br>' . $info;
                    },
                ],
                [
                    'header' => 'Địa chỉ nhận hàng',
                    'value' => function ($model) {
                        //var_dump($model);die;
                        return $model->shippingAddressText;
                    },
                    'format' => 'raw',
                ],
                [
                    'header' => 'Phí giao hàng',
                    'format' => 'raw',
                    'value' => function ($model) use ($formatter) {
                        $shipping_fee = null;
                        if ($model->isCod) {
                            if (!empty($model->shippingAddress->fee)) {
                                $shipping_fee = '<b>' . $formatter->asCurrency($model->shippingAddress->fee) . '</b>';
                            }
                        }
                        return $shipping_fee;
                    }
                ],
                [
                    'header' => 'Giá (chưa có COD)',
                    'format' => 'raw',
                    'value' => function ($model) use ($formatter) {
                        if ($model->payment_method == 'cod') {
                            $html = '<strong>' . $formatter->asCurrency($model->total) . '</strong>' . '<br><small>' . $model->payment_method . ($model->shipping_method ? ' ('.$model->shipping_method.')' : '') . '</small><br>';

                        } else {
                            $html = '<strong>' . $formatter->asCurrency($model->total) . '</strong>' . '<br><small>' . $model->payment_method . ($model->isCod ? ' (cod)' : '') . '</small><br>';
                        }
                        if ($model->is_activated) {
                            $html .= '<span class="label label-success">Đã kích hoạt</span>';
                            if (!empty($model->activation_date)) {
                                $html .= '<br>(<time>' . $formatter->asDatetime($model->activation_date) . '</time>)';
                            }
                        } else {
                            $html .= '<span class="label label-warning">Chưa kích hoạt</span>';
                        }

                        return $html;
                    },
                ],
                'promotion_code',
                [
                    'header' => 'Note',
                    'format' => 'raw',
                    'value' => function ($model) {
                        $str = '';

                        if (!empty($model->operator)) {
                            $str .= '<strong>' . 'Chủ : ' . $model->operator->profile->name .'</strong>';
                        }
                        $url = Url::toRoute(['/order/action/history', 'id' => $model->id]);
                        $str .= "<a class='btn btn-sm btn-default btn-block btn-note' href='$url'><span class='ion-android-search'></span> History</a>";
                        return $str;
                    }
                ],
                [
                    'label' => 'Affiliate',
                    'value' => function($model) {
                        if ($model->originalAffiliate != null) {
                            return $model->originalAffiliate->email;
                        }
                        return '';
                    }
                ],
                [
                    'label' => 'ForKid',
                    'value' => function($model) {

                      return !empty($model->is_4kid) ? 'Yes' : 'No';
                    }
                ],
                [
                    'header' => 'Xử lý',
                    'template' => '{action}{view}{update}{call}{note}{update-shipping}{update-email-user}',
                    'class' => 'yii\grid\ActionColumn',
                    'contentOptions' => ['style' => 'width: 90px;'],
                    'visibleButtons' => [
                        'call' => function ($model, $key) use ($user) {
                            return $user->can('Order.Action.Call') && !($user->can('Telesale') && $model->is_done_telesale_process);
                        },
                        'note' => function ($model, $key) use ($user) {
                            return $user->can('Order.Action.Note') && !($user->can('Telesale') && $model->is_done_telesale_process);
                        },
                        'action' => function ($model, $key) use ($user) {
                            return !($user->can('Telesale') && $model->is_done_telesale_process);
                        },
                        'update-shipping' => function ($model, $key) use ($user) {
                            return $user->can('Order.Action.UpdateShipping') && !($user->can('Telesale') && $model->is_done_telesale_process) && $model->isCod;
                        },
                        'update' => function ($model, $key) use ($user) {
                            return $user->can('Order.Action.Update') && $model->canUpdate();
                        },
                    ],
                    'buttons' => [
                        'action' => function ($url, $model, $key) {
                            $enableActions = ActionForm::getEnableActions($model->status, $model->payment_method, $model->shipping_method);
                            if (!$enableActions) {
                                return;
                            }
                            if ($model->is_done_telesale_process && array_key_exists('move-to-cs', $enableActions)) {
                                unset($enableActions['move-to-cs']);
                            }
                            // Transaction
                            // Get proship shipping status:
                            $transaction = PaymentTransaction::find()->where([
                                'status' => \kyna\payment\models\TopupTransaction::STATUS_SUCCESS,
                                'order_id' => $model->id,
                            ])->one();
                            $items = [];
                            foreach ($enableActions as $action => $label) {
                                $updateUrl = Url::toRoute(['/order/action/' . $action, 'id' => $key]);
                                switch ($action) {
                                    // nhung button khong can load submit form
                                    case 'update-shipping-status':
                                        $items[] = [
                                            'label' => $label,
                                            'url' => $updateUrl,
                                            'options' => [
                                                'class' => 'ajax-button',
                                            ],
                                        ];
                                        break;
                                    case 'cancel-shipping':
                                    case 'cancel':
                                        // no break
//                                      case 'cod-return':
                                        // kiem tra co cancel shipping duoc khong neu khong thi khong xuat hien
                                        if ($model->payment_method == 'cod' && isset($model->shipping_method)) {
                                            switch ($model->shipping_method) {
                                                case 'proship':
                                                    if (isset($transaction) && $transaction['shipping_status'] != Proship::INVOICE_STATUS__CHO_LAY_HANG) {
                                                        $items[] = [
                                                            'label' => $label,
                                                            'url' => $updateUrl,
                                                            'options' => [
                                                                'class' => 'disabled',
                                                            ],
                                                        ];
                                                        break;
                                                    }
                                                    $items[] = [
                                                        'label' => $label,
                                                        'url' => $updateUrl,
                                                    ];
                                                    break;
                                                case 'ghtk':
                                                    if (isset($transaction) && $transaction['shipping_status'] != Ghtk::SHIPPING_STATUS__CHUA_TIEP_NHAN) {
                                                        $items[] = [
                                                            'label' => $label,
                                                            'url' => $updateUrl,
                                                            'options' => [
                                                                'class' => 'disabled',
                                                            ],
                                                        ];
                                                        break;
                                                    }
                                                    $items[] = [
                                                        'label' => $label,
                                                        'url' => $updateUrl,
                                                    ];
                                                    break;
                                                default:
                                                    $items[] = [
                                                        'label' => $label,
                                                        'url' => $updateUrl,
                                                    ];
                                                    break;
                                            }
                                            break;
                                        }
                                        $items[] = [
                                            'label' => $label,
                                            'url' => $updateUrl,
                                        ];
                                        break;
                                    // other buttons (se load submit form)
                                    default:
                                        $items[] = [
                                            'label' => $label,
                                            'url' => $updateUrl,
                                        ];
                                        break;
                                }
                            }
                            return ButtonDropdown::widget([
                                'label' => 'Chuyển',
                                'containerOptions' => [
                                    'class' => 'btn-block btn-order-status',
                                ],
                                'options' => [
                                    'class' => 'btn-block btn-sm btn-default',
                                ],
                                'dropdown' => [
                                    'items' => $items,
                                    'options' => [
                                        'class' => 'dropdown-menu-right',
                                    ],
                                ],
                            ]);
                        },
                        'view' => function ($url, $model, $key) {
                            return Html::a('<i class="ion-android-search"></i>', $url, [
                                'class' => 'btn btn-sm btn-default btn-block',
                                'data-pjax' => '0',
                            ]);
                        },
                        'call' => function ($url, $model, $key) {
                            if (!in_array($model->status, [1, 2, 3])) {
                                return;
                            }

                            $updateUrl = Url::toRoute(['/order/action/call', 'id' => $key]);
                            if (!isset($model->shippingAddress->phone_number) and !isset($model->user->address)) {
                                return;
                            }

                            return Html::a('<i class="ion-android-call"></i> GỌI', $updateUrl, [
                                'class' => 'btn btn-sm btn-success btn-call btn-block',
                            ]);
                        },
                        'note' => function ($url, $model, $key) {
                            $url = Url::toRoute(['/order/action/note', 'id' => $key]);

                            return Html::a('<span class="fa fa-plus"></span> Add note', $url, [
                                'class' => 'btn btn-sm btn-default btn-block btn-note',
                            ]);
                        },
                        'update-shipping' => function ($url, $model, $key) {
                            $url = Url::toRoute(['/order/action/update-shipping', 'id' => $key]);

                            return Html::a('<span class="fa fa-edit"></span> Shipping address', $url, [
                                'class' => 'btn btn-sm btn-default btn-block btn-update-shipping',
                            ]);
                        },
                        'update' => function ($url, $model, $key) {
                            $url = Url::toRoute(['/order/update', 'id' => $model->id]);

                            return Html::a('<span class="fa fa-pencil"></span> Update', $url, [
                                'class' => 'btn btn-sm btn-default btn-block',
                            ]);
                        },
                        'update-email-user' => function ($url, $model, $key) {
                            $url = Url::toRoute(['/order/user/index', 'id' => $key]);
                            return Html::a('<span class="fa fa-edit"></span> Update Email', $url, [
                                'class' => 'btn btn-sm btn-default btn-block btn-update-email',
                            ]);
                        },
                    ],
                ],
            ],
        ]); ?>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script type="text/javascript">
        $("li.disabled").click(false);
    </script>
    <?php if ($isTrackingFb) : ?>
        <?php
        $data = Yii::$app->session->getFlash('need-pixel-tracking');
        ?>
        <!-- Facebook Pixel Code -->
        <script>
            !function(f,b,e,v,n,t,s)
            {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                n.callMethod.apply(n,arguments):n.queue.push(arguments)};
                if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
                n.queue=[];t=b.createElement(e);t.async=!0;
                t.src=v;s=b.getElementsByTagName(e)[0];
                s.parentNode.insertBefore(t,s)}(window, document,'script',
                'https://connect.facebook.net/en_US/fbevents.js');
            fbq('init', '548540191923540');
        </script>
        <noscript>
            <img height="1" width="1" style="display:none"
                   src="https://www.facebook.com/tr?id=548540191923540&ev=PageView&noscript=1"
            />
        </noscript>

        <script type="text/javascript">
            var productObj = {
                'content_ids': <?= json_encode($data['ids']) ?>,
                'contents': <?= json_encode($data['contents']) ?>,
                'content_type': 'product',
                'value': <?= $data['total'] ?>,
                'currency': 'VND'
            };

            fbq('track', 'Purchase', productObj);
        </script>

        <!-- End Facebook Pixel Code -->
    <?php endif; ?>

    <?php Pjax::end(); ?>
</div>
