<?php
/**
 * Created by IntelliJ IDEA.
 * User: trangnqd
 * Date: 1/7/2017
 * Time: 11:48 AM
 */
use \Picqer\Barcode\BarcodeGeneratorPNG;

$formatter = Yii::$app->formatter;
$generator = new BarcodeGeneratorPNG();
?>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>In</title>
</head>
<link href='https://fonts.googleapis.com/css?family=Roboto:400,500,700,300' rel='stylesheet' type='text/css'>

<style type="text/css">

    body{
        background-color: #ccc;
        font-family: 'Roboto', sans-serif !important;
        font-size: 16pt;
        line-height: 1.5
    }
    body .SA4 p{
        margin-top: 0px;
        margin-bottom: 0px;
    }
    .SA4{
        margin: 0 auto;
        background-color: #fff;
        position: relative}
    .A4_body{
        margin: 0 auto;
        padding:15px 0;
        position: absolute;
        bottom: 90px;

    }
    .barcode .A4_body {
        bottom: 45px !important;
        top: 47px;
    }
    .barcode2 .A4_body {
        bottom: 45px !important;
        top: 80px;
    }
    .red_color{
        color: #c50243;
        font-weight: bold;
    }
    table{
        border-collapse: initial;
        border: 2px solid #676060;
        font-size: 13px;
    }
    table td{
        padding: 8px 5px 8px 8px;
        border: none;
    }
    table tr{
        border: none;
    }
    table tr p{
        margin-bottom: 0px !important;
        font-size: 16px;
    }
    .print-header{
        padding-bottom: 20px;
        height: 55px;
    }
    .print-header h2{
        margin: 0px;
    }
    .A4_left{
        width: 515px;
        float: left;
    }
    .A4_left .wrap{
        padding: 0px 25px 0px 73px;
        position: relative;
        top: -50px
    }
    .A4_right{
        width: 675px;
        float: right;

    }
    .A4_right .wrap{
        padding: 0px 180px 0px 0px;
        width: 485px;
        float: right;
        position: relative;
        top: -50px
    }
    .A4_right .print-header{
        display: table;
        width: 100%;
        text-align: center;
    }
    .A4_right .print-header h2{
        font-size: 17pt;
        text-transform: uppercase;
        font-weight: bold;
        color: #33aa36;
        display: table-cell;
        vertical-align: middle;
    }
    .print-author{
        color: #33aa36;
        font-weight: bold;
        font-size: 18pt;
        text-align: center;
        display: inline-block;
    }
    .print-content{
        margin-top: 35px;
    }
    .print_mdh{
        border: 2px solid #676060;
        padding: 10px 15px 10px 15px;
        margin-top: 45px;
        position: relative;
        bottom: -12px;
        height: 200px
    }
    .print_mdh .sub-wrap {
        position: absolute;
        left: 0;
        top: 50%;
        transform: translateX(-50%) translateY(-50%);
        -webkit-transform: translateX(15px) translateY(-50%);
        width: 450px;
        font-size: 16px;
    }
    .print-code{
        border: 2px solid #676060;
        padding: 5px;
        text-align: center;
        color: #33aa36;
        text-transform: uppercase;
        font-size: 16pt;
        font-weight: bold;
        margin: 3px 0px;
    }
    .print-giam-gia ul {
        display: inline-block;
        padding: 0px;
        list-style: none;
        border: 2px solid #ef4244;
        padding: 15px;
        margin: 10px 0px 10px 0px;
        background: #ffdede;
    }
    .print-giam-gia ul li {
        float: left;
    }
    .print-giam-gia ul li.img {
        width: 70px;
    }
    .print-giam-gia ul li.img img {
        margin-top: 10px;
    }
    .print-giam-gia ul li.text {
        width: 281px;
        text-align: center;
        font-size: 18px;
    }
    .print-giam-gia ul li.text p {
        padding: 0px 0px 0px 20px;
    }
    .print-giam-gia ul li.text span {
        text-transform: uppercase;
        color: #d40507;
        font-weight: bold;
        font-size: 16pt;
    }

    @media print {
        @page { margin: 0; size: A4}
        .A4_body{padding: 15px 0;}
        .SA4{width: 1230px; height: 700px;}
        body { margin: 1cm; }
        .print_btn{display: none}
        .cod_id{bottom: -30px; right: 0}
        .topN .A4_body {position: relative; top: 80px}
    }
    @media screen {
        .SA4{width: 1230px; height: 700px;  box-shadow: 1px 2px 2px #333; margin-bottom: 10px}
        .A4_body{padding: 30px 0px; display: inline-block; width: 100%;}
        .cod_id{bottom: 25px; right: 30px}

    }

    .SA4{page-break-after: always}
    .cod_id{position: absolute; color: #666; font-size: 0.85em}
</style>
<body>
<div class="line"></div>
<div class="line2"></div>
<div class="line3"></div>
<div class="line4"></div>
<div class="line5"></div>
<div class="line6"></div>
<div class="line7"></div>
<div class="print_btn" style="text-align: right"><button type="button" onclick="printDiv('printableArea')" style="position: fixed; right: 40px; padding: 15px 10px; top: 100px">IN GHN</button></div>
<div id="printableArea">
    <?php
    $vclass = '';
    if(!empty($dataProvider)){
        foreach ($dataProvider as $key => $d) {
            /*  tính từ cái thứ 2 */
            if ($key > 0) {
                $vclass = 'topN';
            } else {
                $vclass = '';
            }
            ?>
            <div class="SA4 barcode <?= $vclass ?>" id="page-container">
                <div class="A4_body">
                    <div class="A4_left">
                        <div class="wrap">
                            <div class="print-header">
                                <h2 class="A4_logo"><img src="https://kyna.vn/media/images/introduction/logo/logo_v5_acp.jpg" alt="Kyna.vn" height="55px" /></h2>
                            </div><!-- print-header-->
                            <?php
                            $user_name = null;
                            $phone = null;
                            if(!empty($d->shippingAddress->contact_name)){
                                $user_name = $d->shippingAddress->contact_name;
                                $phone = $d->shippingAddress->phone_number;
                            }elseif(!empty($d->user->userAddress->contact_name)){
                                $user_name = $d->user->userAddress->contact_name;
                                $phone = $d->user->userAddress->phone_number;
                            }else{
                                $user_name = 'N/A';
                                $phone = 'N/A';
                            }
                            ?>
                            <div class="print-content">
                                <p><strong>Thân gửi: </strong><span class="print-author"><?= $user_name ?></span></p>
                                <p>Cảm ơn bạn đã đăng ký học tại <span style="color:#33aa36; font-weight: bold;">Kyna.vn</span> Website học kỹ năng dành cho người đi làm.</p>
                                <hr style="width: 70%; border-bottom: 1px solid #676060" />
                                <p style="text-align: center;"><strong>Mã kích hoạt của bạn là:</strong></p>
                                <div class="print-code">
                                    <span><?= $d->activation_code ?></span>
                                </div><!--end .print-code-->
                                <p style="text-align: center;">(Xem hướng dẫn ở mặt sau)</p>
                            </div><!--end print-content-->
                        </div>
                    </div><!--end A4_left-->
                    <div class="A4_right">
                        <div class="wrap">
                            <div class="print-header">
                                <h2>Mã kích hoạt tài khoản học tại Kyna.vn</h2>
                            </div><!--end print-header-->
                            <span style="text-align: center; display: inline-block; width: 100%; position: relative; top: -30px;"><span style="border-bottom: 2px solid #33aa36; width: 35%; display: inline-block;"></span></span>
                            <div class="print-content">
                                <p style="text-align: center;"><strong>Dưới đây là tài khoản đăng nhập của bạn!</strong></p>
                                <table border="1" cellspacing="0" cellpadding="0" style="width: 100%; margin: 3px auto" class="user_info">
                                    <tbody>
                                    <tr>
                                        <td width="70">
                                            <p>
                                                Email
                                            </p>
                                        </td>
                                        <td width="342">
                                            <p class="red_color">
                                                <?= !empty($d->user->email) ? $d->user->email : '' ?>
                                            </p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="70">
                                            <p>
                                                Mật khẩu
                                            </p>
                                        </td>
                                        <td width="342">
                                            <p class="red_color">
                                                (Vui lòng kiểm tra email để lấy thông tin đăng nhập)
                                            </p>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                                <div class="print_mdh">
                                    <div class="sub-wrap">
                                        <?php
                                        $payment_transaction = \kyna\payment\models\PaymentTransaction::find()->where(['order_id' => $d->id])->orderBy(['id' => SORT_DESC])->one();
                                        $ghnCode = '';
                                        $barcode = '';
                                        if (!empty($payment_transaction['transaction_code'])) {
                                            $ghnCode = $payment_transaction['transaction_code'];
                                            $barcode = '<img src="data:image/png;base64,' . base64_encode($generator->getBarcode($ghnCode, $generator::TYPE_CODE_128_B, 1.5, 50)) . '">';
                                        }
                                        ?>
                                        <div class="wrap-barcode" style="position: absolute; right: 0px;">
                                            <div class="img-barcode" ><?= $barcode ?></div>
                                            <p class="number-barcode" style="font-size: 12px; text-align: center;"><?= $ghnCode ?></p>
                                        </div>
                                        <p><strong>Mã đơn hàng : </strong><?= $ghnCode ?></p>
                                        <p><strong>NN : </strong><?= $user_name ?></p>
                                        <p><strong>SĐT : </strong><?= $phone ?></p>
                                        <p><strong>ĐC : </strong><?= !empty($d->shippingAddress->addressText) ? $d->shippingAddress->addressText : $d->printAddress ?></p>
                                        <p><strong>Tiền thu hộ (Người nhận phải trả) : </strong><?= $formatter->asCurrency($d->total) ?></p>
                                    </div>
                                </div><!--end .print_mdh-->
                            </div><!--end .print-content-->
                        </div>
                    </div><!--end A4_right-->

                </div><!--end .A4_body-->
            </div><!--end SA4 -->
        <?php } ?>
    <?php } ?>
</div>
<script type="text/javascript">
    function printDiv(divName) {
        var printContents = document.getElementById(divName).innerHTML;
        var originalContents = document.body.innerHTML;
        document.body.innerHTML = printContents;
        window.print();
        document.body.innerHTML = originalContents;
    }
    printDiv('printableArea');
</script>
</body>
</html>

