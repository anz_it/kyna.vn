<?php
use Picqer\Barcode\BarcodeGeneratorPNG;

$formatter = Yii::$app->formatter;
$generator = new BarcodeGeneratorPNG();
?>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<style type="text/css">
    body {
        background: rgb(204,204,204); font-family: Arial; font-size: 12px;
    }
    .page_container {
        background: white;
        width: 21cm;
        /*height: 29.7cm;*/
        display: block;
        margin: 0 auto;
        margin-bottom: 0.5cm;
        box-shadow: 0 0 0.5cm rgba(0,0,0,0.5);
    }
    @media print {
        Header, Footer { display: none !important; }
        @page{margin: 0.5cm 1.0cm; size:  A4; }
        body,.page_container {
            margin: 0;
            box-shadow: 0;
        }
        .print_btn{display: none}
    }

    ul.list-unstyled{list-style-type: none; padding: 0}
    @media screen {
        page{padding: 15px }
    }

    table { page-break-inside:auto }
    tr    { page-break-inside:avoid; page-break-after:always }

    .break_page{page-break-after: always}
    .cod_id{float: right; color: #666; font-size: 0.85em}

</style>
<body>
<div class="print_btn" style="text-align: right"><button type="button" onclick="window.print();" style="position: fixed; right: 40px; padding: 15px 10px; top: 100px">IN GHN</button></div>
<div class="page_container">
    <style>
        .wrapper {
            padding: 0px 15px 10px 15px;
        }
        .wrap-content {
            display: inline-block;
            width: 100%;
            border-bottom: 1px dashed #ccc;
            list-style: none;
            padding: 20px 0px 20px 0px;
            margin: 0px;
            font-size: 14px;
        }
        .wrap-content .wrap-col {
            float: left;
        }
        .wrap-content .wrap-col ul li {
            margin: 3px 0px;
        }
        .wrap-content .wrap-col:nth-child(1){
            width: 42%;
        }
        .wrap-content .wrap-col:nth-child(2) {
            width: 40%;
        }
        .wrap-content .wrap-col:nth-child(2) ul {
            padding: 0px 15px;
        }
        .wrap-content .wrap-col:nth-child(3) {
            width: 18%;
            text-align: center;
        }
        .wrap-content .wrap-img img {
            /*width: 100%;*/
            margin-top: 5px;
        }
    </style>
    <div class="wrapper">
        <?php
        $i=0;
        if(!empty($dataProvider)){
        foreach($dataProvider as $order){
        $i++;
        $break_page = '';
        ?>
        <?php
        $user_name = null;
        $phone = null;
        if(!empty($order->shippingAddress->contact_name)){
            $user_name = $order->shippingAddress->contact_name;
            $phone = $order->shippingAddress->phone_number;
        }elseif(!empty($order->user->userAddress->contact_name)){
            $user_name = $order->user->userAddress->contact_name;
            $phone = $order->user->userAddress->phone_number;
        }else{
            $user_name = 'N/A';
            $phone = 'N/A';
        }
        $barcode = '';
        $orderCode = $order->shippingOrderCode;
        if (!empty($orderCode)) {
            $barcode = '<img src="data:image/png;base64,' . base64_encode($generator->getBarcode($orderCode, $generator::TYPE_CODE_128_B, 1, 45)) . '">';
        }
        ?>
        <ul class="wrap-content">
            <li class="wrap-col">
                <ul class="list-unstyled">
                    <li><strong>Mã đơn hàng: </strong> <?= $orderCode ?></li>
                    <li><strong>NN: </strong> <span><?= $user_name ?></span> | Phone: <?= $phone ?></li>
                    <li><strong>ÐC: </strong> <?= !empty($order->shippingAddress->addressText) ? $order->shippingAddress->addressText : $order->printAddress ?></li>
                    <li><strong>Tiền thu hộ (Người nhận phải trả):</strong> <?= $formatter->asCurrency($order->total) ?> </li>
                </ul>
            </li>
            <li class="wrap-col">
                <ul class="list-unstyled">
                    <li><strong>Nội dung hàng:</strong> <?= $order->details[0]->course->name; ?> Monkey Junior</li>
                    <li><strong>Ghi chú: </strong> Vui lòng không mở bìa thư trước khi thanh toán cho người giao thư. </li>
                    <li><strong>Thời gian tạo vận đơn: </strong><?= $formatter->asDatetime(!empty($order->order_date) ? $order->order_date : null) ?></li>
                </ul>
            </li>
            <li class="wrap-col">
                <div class="wrap-img">
                    <?= $barcode ?>
                </div>
                <p class="number-barcode" style="font-size: 10px; text-align: center;"><?= $orderCode ?></p>
            </li>
        </ul>
        <?php } ?>
        <?php } ?>
    </div>

</div>
<script type="text/javascript">
    (function(){
        window.print();
    })();
</script>
</body>
</html>
