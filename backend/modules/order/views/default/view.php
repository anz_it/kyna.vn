<?php

use yii\bootstrap\Nav;
use yii\bootstrap\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;
use yii\widgets\Pjax;

use kartik\grid\GridView;

use kyna\order\models\Order;
use kyna\promotion\models\Promotion;
use app\modules\order\models\ActionForm;
use kyna\payment\models\PaymentTransaction;
use kyna\payment\lib\proship\Proship;

\app\modules\order\assets\BackendAsset::Register($this);

$this->title = 'Đơn hàng số #'.$model->id;
$this->params['breadcrumbs'][] = ['label' => $this->context->mainTitle, 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$formatter = Yii::$app->formatter;
$webUser = Yii::$app->user;
?>
<?= Nav::widget([
    'items' => [
        [
            'label' => 'Thông tin đơn hàng',
            'url' => Url::toRoute(['view', 'id' => $model->id]),
            'active' => true,
        ],
        [
            'label' => 'Lịch sử',
            'url' => Url::toRoute(['history', 'id' => $model->id]),
        ]
    ],
    'options' => ['class' => 'nav-pills'],
]) ?>
<hr>
<div class="order-view row">
    <?php Pjax::begin(['timeout' => Yii::$app->params['timeOutPjax'], 'id' => 'idPjaxReload']) ?>
    <div class="col-sm-9">
        <div class="row">
            <div class="col-sm-4">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            <?= $this->title ?>
                            <span class="label <?= $statusLabelCss[$model->status] ?>"><?= $model->statusLabel ?></span>
                        </h3>
                    </div>
                    <?= DetailView::widget([
                        'model' => $model,
                        'attributes' => [
                            [
                                'attribute' => 'order_date',
                                'label' => 'Ngày lập',
                                'format' => 'datetime',
                                'value' => !empty($model->order_date) ? $model->order_date : null
                            ],
                            'user.email:email:Người mua',
                            'activation_code:raw:Mã kích hoạt',
                            'total:currency:Tổng cộng',
                            //'reference_id:raw:Referer',
                            [
                                'attribute' => 'is_4kid',
                                'label' => 'Loại ForKid',
                                'value' => !empty($model->is_4kid) ? 'Yes' : 'No'

                            ],

                        ],
                    ]) ?>
                </div>
            </div>

            <div class="col-sm-8">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Thông tin COD</h3>
                    </div>
                    <?= DetailView::widget([
                        'model' => $model,
                        'attributes' => [
                            'shippingAddress.contact_name:raw:Người nhận',
                            'shippingAddress.phone_number:raw:Số điện thoại',
                            'shippingAddress.addressText:html:Địa chỉ',
                            'shippingAddress.vendor_id:raw:Vận chuyển',
                            'shippingAddress.note:raw:Ghi chú',
                        ],
                    ]) ?>
                </div>
            </div>
        </div>
        <h3>Khóa học</h3>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'export' => false,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                'courseInfo.name:raw:Khóa học',
                [
                    'attribute'=>'course_combo_id', 
                    'value'=>function ($model) { 
                        return !empty($model->combo) ? $model->combo->name : null;
                    },
                    'group'=>true,  // enable grouping
                ],
                'unit_price:currency:Học phí gốc',
                'discount_amount:currency:Giảm giá',
                'itemTotal:currency:Thành tiền',
                [
                    'attribute' => 'activation_code',
                    'value' => function ($model) {
                        return $model->activation_code;
                    },
                    'group' => true,  // enable grouping
                ],
                [
                    'header' => 'Kích hoạt?',
                    'value' => function ($model) {
                        return $model->partnerCode ? $model->partnerCode->statusText : null;
                    },
                    'group' => true,  // enable grouping
                ],
            ],
        ]) ?>
    </div>
    <div class="col-sm-3">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Summary</h3>
            </div>
            <table class="table">
                <tbody>
                    <tr>
                        <th>Tổng học phí</th>
                        <td></td>
                        <td class="text-right"><?= $formatter->asCurrency($model->subtotal) ?></td>
                    </tr>
                    <tr>
                        <th>Phí vận chuyển</th>
                        <td>&plus;</td>
                        <td class="text-right"><?= isset($model->shippingAddress->fee) ? $formatter->asCurrency($model->shippingAddress->fee) : $formatter->asCurrency(0) ?></td>
                    </tr>
                    <?php if (!empty($model->direct_discount_amount)) : ?>
                    <tr>
                        <th>Giảm giá Trực tiếp</th>
                        <td>&minus;</td>
                        <td class="text-right"><?= $formatter->asCurrency($model->direct_discount_amount) ?></td>
                    </tr>
                    <?php endif; ?>
                    <?php if (!empty($model->group_discount_amount)) : ?>
                    <tr>
                        <th>Giảm giá nhóm</th>
                        <td>&minus;</td>
                        <td class="text-right"><?= $formatter->asCurrency($model->group_discount_amount) ?></td>
                    </tr>
                    <?php endif; ?>
                    <?php if (!empty($model->promotion_code)) : ?>
                    <tr>
                        <th>Mã voucher/ Coupon</th>
                        <td>&minus;</td>
                        <td class="text-right"><?= $model->promotion_code ?></td>
                    </tr>
                    <tr>
                        <th>Mức giảm </th>
                        <td>&minus;</td>
                        <td class="text-right">
                            <?php
                            $promo_info = Order::promotionInformation($model->promotion_code);
                            if ($promo_info->discount_type == Promotion::TYPE_FEE) {
                                echo $formatter->asCurrency($promo_info->value);
                            } elseif ($promo_info->discount_type == Promotion::TYPE_PERCENTAGE) {
                                echo $promo_info->value . '%';
                            } elseif ($promo_info->discount_type == Promotion::TYPE_PARITY) {
                                echo "Đồng giá " . $formatter->asCurrency($promo_info->value);
                            }
                            ?>
                        </td>
                    </tr>
                    <?php endif; ?>
                    <?php if (!empty($model->totalDiscount)) : ?>
                    <tr>
                        <th>Tổng giảm giá</th>
                        <td>&minus;</td>
                        <td class="text-right"><?= $formatter->asCurrency($model->totalDiscount) ?></td>
                    </tr>
                    <?php endif; ?>
                </tbody>
                <tfoot>
                    <tr>
                        <th style="vertical-align: text-bottom">Giá chưa bao gồm phí vận chuyển</th>
                        <th style="vertical-align: text-bottom;font-size:1.5em">=</th>
                        <td class="text-right" style="vertical-align: text-bottom;font-size:1.5em">
                            <?= $formatter->asCurrency($model->realPayment - $model->shippingAmount) ?>
                        </td>
                    </tr>
                    <tr>
                        <th style="vertical-align: text-bottom">Tổng cộng(Giá đã bao gồm coupon - nếu có)</th>
                        <th style="vertical-align: text-bottom;font-size:1.5em">=</th>
                        <td class="text-right" style="vertical-align: text-bottom;font-size:1.5em">
                            <?php
                            echo $formatter->asCurrency($model->realPayment);
                            ?>
                        </td>
                    </tr>
                    <?php
                    /* if ($model->status == Order::ORDER_STATUS_NEW) : ?>
                    <tr>
                        <td colspan="3"><button class="btn btn-primary btn-block">Lưu đơn hàng</button></td>
                    </tr>
                    <?php endif;
                    */?>
                </tfoot>
            </table>
            <div class="panel-footer">
                <?php if (!($webUser->can('Telesale') && $model->is_done_telesale_process)) : ?>
                    <?php if ($webUser->can('Order.Action.Call') && in_array($model->status, [1, 2, 3]) and (isset($model->shippingAddress->phone_number) or isset($model->user->address))) :
                        $url = Url::toRoute(['/order/action/call', 'id' => $model->id]); ?>
                        <?= Html::a('<i class="ion-android-call"></i> GỌI', $url, [
                            'class' => 'btn btn-success btn-call btn-block',
                        ]) ?>
                    <?php endif; ?>
                    <div class="btn-order-status btn-block">
                        <?php
                        if (isset($model->shipping_method))
                            $enableActions = ActionForm::getEnableActions($model->status, $model->payment_method, $model->shipping_method);
                        else
                            $enableActions = ActionForm::getEnableActions($model->status, $model->payment_method);
                        if (!$enableActions) {
                            return;
                        }
                        // Get transaction
                        $transaction = PaymentTransaction::find()->where([
                            'status' => \kyna\payment\models\TopupTransaction::STATUS_SUCCESS,
                            'order_id' => $model->id,
                        ])->one();
                        $items = [];
                        foreach ($enableActions as $action => $label) {
                            switch ($action) {
                                case 'update':
                                    $url = Url::toRoute(['/order/' . $action, 'id' => $model->id]);
                                    echo '<a data-redirect="true" href="'.$url.'" class="btn btn-block btn-default">'.$label.'</a>';
                                    break;
                                case 'update-shipping-status':
                                    // Tam thoi bo button nay - se cap nhat sau
//                                    $url = Url::toRoute(['/order/action/' . $action, 'id' => $model->id]);
//                                    echo '<a href="'.$url.'" class="btn btn-block btn-default ajax-button">'.$label.'</a>';
                                    break;
                                case 'cancel-shipping':
                                case 'cancel':
                                    // no break
//                                case 'cod-return':
                                    $url = Url::toRoute(['/order/action/' . $action, 'id' => $model->id]);
                                    // kiem tra co cancel shipping duoc khong neu khong thi khong xuat hien
                                    if ($model->payment_method == 'cod' && isset($model->shipping_method)) {
                                        switch ($model->shipping_method) {
                                            case 'proship':
                                                if (isset($transaction) && $transaction['shipping_status'] != Proship::INVOICE_STATUS__CHO_LAY_HANG) {
                                                    echo '<a href="'.$url.'" class="btn btn-block btn-default disabled">'.$label.'</a>';
                                                    break;
                                                }
                                                echo '<a href="'.$url.'" class="btn btn-block btn-default">'.$label.'</a>';
                                                break;
                                            default:
                                                echo '<a href="'.$url.'" class="btn btn-block btn-default">'.$label.'</a>';
                                                break;
                                        };
                                        break;
                                    }
                                    echo '<a href="'.$url.'" class="btn btn-block btn-default">'.$label.'</a>';
                                    break;
                                default:
                                    $url = Url::toRoute(['/order/action/' . $action, 'id' => $model->id]);
                                    echo '<a href="'.$url.'" class="btn btn-block btn-default">'.$label.'</a>';
                                    break;
                            }
                        }
                        ?>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
    <!--        Script xu ly cho button ajax truc tiep - khong load submit form -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script type="text/javascript">
        $('a.ajax-button').on('click', function (e) {
            e.preventDefault();
            $('#loader').addClass('loading');
            $("#modal").modal("hide");

            $.ajax({
                type: 'POST',
                url: $(this).attr('href'),
                success: function (res) {
                    data = JSON.parse(res);
                    if (data.success) {
                        $("#modal").modal("hide");
                        $('#loader').removeClass('loading');
                        $.pjax.reload({container:'#idPjaxReload',timeout: 15000});
//                            $.notify(data.message,{
                        $.notify("Hoàn tất cập nhật tình trạng giao vận",{
                            type: 'success',
                            allow_dismiss: true
                        });
                    } else {
                        $('#modal').modal("hide");
                        $('#loader').removeClass('loading');
                        $.pjax.reload({container:'#idPjaxReload',timeout: 15000});
                        $.notify('Xử lý thất bại!',{
                            type: 'danger',
                            allow_dismiss: true
                        });
                        return false;
                    }
                }
            });
        });
    </script>
    <?php Pjax::end() ?>
</div>
