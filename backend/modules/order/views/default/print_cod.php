<?php

use app\modules\order\models\ActionForm;
use yii\bootstrap\Html;
use kartik\grid\GridView;
use yii\bootstrap\Nav;
use yii\bootstrap\ButtonDropdown;
use yii\helpers\Url;
use app\modules\order\assets\BackendAsset;
use kartik\export\ExportMenu;

BackendAsset::register($this);
$this->title = 'Orders COD Printing';

$this->params['breadcrumbs'][] = $this->title;

$formatter = Yii::$app->formatter;
$user = Yii::$app->user;

$gridColumns = [
    [
        'header' => 'Mã đơn hàng - Hệ thống Kyna',
        'attribute' => 'id',
        'format' => 'raw',
        'value' => function($model){
            return $model->id;
        }
    ],
    [
        'header'    => 'Mã đơn hàng - GHN, GHTK',
        'format' => 'raw',
        'value' => function($model){
            $paymentTransaction = \kyna\payment\models\PaymentTransaction::find()->where(['order_id' => $model->id])->orderBy(['id' => SORT_DESC])->one();
            return !empty($paymentTransaction->transaction_code) ? $paymentTransaction->transaction_code : '';
        }
    ],
    [
        'header' => 'Bill Code - Proship',
        'format' => 'raw',
        'value' => function($model) {
            $paymentTransaction = \kyna\payment\models\PaymentTransaction::find()->where(['order_id' => $model->id])->orderBy(['id' => SORT_DESC])->one();
            return !empty($paymentTransaction->bill_code) ? $paymentTransaction->bill_code : '';
        }
    ],
    [
        'header' => 'Code hoặc số Series [Dùng Tham Chiếu]',
        'format' => 'raw',
        'value' => function($model){
            return $model->activation_code;
        }
    ],
    [
        'header' => 'Trạng thái',
        'format' => 'raw',
        'value' => function($model){
            return $model->statusLabel;
        }
    ],
    [
        'header' => 'Họ và tên',
        'format'    => 'raw',
        'value' => function($model){
            return !empty($model->user->userAddress->contact_name) ? $model->user->userAddress->contact_name : '';
        }
    ],
    [
        'header' => 'Số điện thoại',
        'format' => 'raw',
        'value'  => function($model){
            return !empty($model->user->userAddress->phone_number) ? $model->user->userAddress->phone_number : '';
        }
    ],
    [
        'header' => 'Đơn vị giao vận',
        'format' => 'raw',
        'value'  => function($model){
            return !empty($model->shipping_method) ? $model->shipping_method : '';
        }
    ],

];
?>
<div class="order-index">
    <nav class="navbar navbar-default">
        <div class="search-menu">
            <?= $this->render('_search_print', [
                'queryParams' => $queryParams,
                'callStatuses' => $callStatuses,
                'allowedStatus' => $allowedStatus,
                'provider' => $dataProvider,
                'codSettings' => $codSettings,
                'shippingMethods' => $shippingMethods,
                'courseTypes' => $courseTypes]) ?>
        </div>
        <!-- Button export -->
        <a class="btn btn-info navbar-btn navbar-left" id="btn_export">
            <i class="fa fa-share"></i> Export file
        </a>
    </nav>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
//        'filterModel' => $searchModel,
        'columns' => [
            [
                'header' => 'Đơn hàng',
                'attribute' => 'order_date',
                'value' => function ($model, $key, $index) use ($formatter, $statusLabelCss, $callStatuses) {
                    $html = '<strong>#' . $model->id . '</strong><br>';
                    $html .= '<time>' . $formatter->asDatetime(!empty($model->order_date) ? $model->order_date : null) . '</time><br>';
                    $html .= '<time>' . $formatter->asDatetime(!empty($model->to_shipper_time) ? $model->to_shipper_time : null) . '</time><br>';
                    $html .= '<span class="label ' . $statusLabelCss[$model->status] . '">' . $model->statusLabel . '</span><br>';

                    if (array_key_exists($model->call_status, $callStatuses)) {
                        $html .= $callStatuses[$model->call_status];
                    }

                    return $html;
                },
                'format' => 'raw',
            ],
            [
                'attribute' => 'details',
                'value' => 'detailsText',
                'format' => 'raw',
            ],
            [
                'header' => 'Người đăng ký',
                'format' => 'raw',
                'value' => function ($model, $key, $index) use ($formatter) {
                    $user = $model->user;
                    if (empty($user)) {
                        return null;
                    }
                    return $formatter->asEmail($user->email) . '<br>' . (!empty($user->userAddress) ? $user->userAddress->toString(false) : '');
                },
            ],
            [
                'header' => 'Địa chỉ nhận hàng',
                'value' => function ($model) {
                    //var_dump($model);die;
                    return $model->shippingAddressText;
                },
                'format' => 'raw',
            ],
            [
                'header' => 'Phí giao hàng',
                'format' => 'raw',
                'value' => function ($model) use ($formatter) {
                    $shipping_fee = null;
                    if ($model->isCod) {
                        if (!empty($model->shippingAddress->fee)) {
                            $shipping_fee = '<b>' . $formatter->asCurrency($model->shippingAddress->fee) . '</b>';
                        }
                    }
                    return $shipping_fee;
                }
            ],
            [
                'header' => 'Code hoặc số Series [Dùng Tham Chiếu]',
                'format' => 'raw',
                'value' => function ($model)  {

                    return $model->activation_code;
                }
            ],
            [
                'header' => 'Giá (chưa có COD)',
                'format' => 'raw',
                'value' => function ($model) use ($formatter) {
//                    var_dump($model); die();
                    return '<strong>' . $formatter->asCurrency($model->total) . '</strong>' . '<br><small>' . ($model->isCod ? ' cod' : '') . ' (' . $model->shipping_method .')</small>';
                },
            ],
            [
                'header' => 'Note',
                'value' => function ($model) {
                    $str = '';

                    if (!empty($model->operator)) {
                        $str .= 'Chủ : ' . $model->operator->profile->name;
                    }

                    return $str;
                }
            ],
        ],
    ]); ?>

</div>
<?php
$script = "
    ;(function($, window, document, undefined){
        $(document).ready(function(){        
            $('body').on('click', '#btn_export', function (event) {            
                event.preventDefault();
                var exportDialog = new BootstrapDialog();
                exportDialog.setTitle('Vui lòng nhập email nhận file export');
                exportDialog.setMessage($('<input id=\"email_export\" class=\"form-control\" placeholder=\"Email\" value=\"".Yii::$app->user->identity->email."\"></input>'));
                exportDialog.setType(BootstrapDialog.TYPE_SUCCESS);
                exportDialog.setButtons([{
                    label: 'Export',
                    action: function(dialog) {                       
                        var email = $('#email_export').val(); 
                        if (email == '') {
                            alert('Vui lòng nhập email');
                        }
                        var data = {email: email, type: 'export'};
                        $.post('', data, function (response) {
                            if (response.result) {
                                var bootstrapDialog = new BootstrapDialog();
                                bootstrapDialog.setMessage('Vui lòng kiểm tra email '+email+'. File Export sẽ được gửi đến trong vài phút.');
                                bootstrapDialog.setType(BootstrapDialog.TYPE_SUCCESS);
                                bootstrapDialog.open(); 
                                exportDialog.close();          
                            } else {
                                
                            }
                        });
                    }
                }]);
                exportDialog.open(); 
            });           
        });
    })(window.jQuery || window.Zepto, window, document);";
$css = "
    .modal-content .modal-header {
        border-radius: 0;
    }
    ";
?>
<?php
$this->registerCss($css);
$this->registerJs($script, \yii\web\View::POS_END, 'export-order');
?>
