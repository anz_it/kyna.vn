<?php

use yii\bootstrap\Html;
use yii\helpers\Url;
use kyna\base\assets\DateRangeAsset;
use kyna\order\models\Order;

DateRangeAsset::register($this);

$method = isset($queryParams['shipping_method']) ? $queryParams['shipping_method'] : 'AUTO';

$allOrderStatuses = Order::getAllStatuses();
$allowedStatusLabels = array_intersect_key($allOrderStatuses, array_flip($allowedStatus));
?>

<?= Html::beginForm(Url::toRoute([$this->context->action->id, 'status' => Yii::$app->request->get('status')]), 'get', [
    'class' => 'navbar-form navbar-left',
    'role' => 'search',
]) ?>

    <div class="form-group">
        <?= Html::textInput('s', isset($queryParams['s']) ? $queryParams['s'] : '', [
            'placeholder' => 'Tìm email, số điện thoại hoặc mã đơn hàng',
            'class' => 'form-control',
        ]) ?>
    </div>
    <div class="form-group">
        <?= Html::textInput('date-range', isset($queryParams['date-range']) ? $queryParams['date-range'] : '', [
            'placeholder' => 'Ngày mua',
            'class' => 'form-control',
            'style' => 'width: 170px;',
            'data-control' => 'daterangepicker',
            'data-start-date' => date('d/m/Y', isset($queryParams['from_date']) ? $queryParams['from_date'] : strtotime('last week')),
            'data-end-date' => date('d/m/Y', isset($queryParams['to_date']) ? $queryParams['to_date'] : strtotime('today')),
            'data-max-date' => date('d/m/Y', strtotime('today')),
        ]) ?>
    </div>
    <div class="form-group">
        <?= Html::dropDownList('status', Yii::$app->request->get('status'), $allowedStatusLabels, [
            'class' => 'form-control',
            'prompt' => 'Tình trạng đơn hàng'
        ]); ?>
    </div>
    <div class="form-group">
        <?= Html::dropDownList('call_status', isset($queryParams['call_status']) ? $queryParams['call_status'] : '', $callStatuses, [
            'class' => 'form-control',
            'prompt' => 'Tình trạng liên lạc'
        ]); ?>
    </div>
    <div class="form-group">
        <?= Html::dropDownList('is_4kid', $queryParams['is_4kid'], [0=>'No',1=>'Yes'], [
            'class' => 'form-control',
            'prompt' => 'Is ForKid'
        ]); ?>
    </div>

    <?= Html::submitButton('<i class="ion-android-search"></i> Tìm đơn hàng', ['class' => 'btn btn-default']) ?>

<?= Html::endForm(); ?>
