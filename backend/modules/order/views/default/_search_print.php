<?php

use yii\bootstrap\Html;
use yii\helpers\Url;
use kyna\base\assets\DateRangeAsset;
use kyna\order\models\Order;
use kyna\partner\models\Partner;
use kyna\course\models\Course;

DateRangeAsset::register($this);

$method = isset($queryParams['shipping_method']) ? $queryParams['shipping_method'] : 'AUTO';

$allOrderStatuses = Order::getAllStatuses();
$allowedStatusLabels = array_intersect_key($allOrderStatuses, array_flip($allowedStatus));
$partners = Partner::findAllActive();
?>

<?= Html::beginForm(Url::toRoute(['/order/default/' . $this->context->action->id, 'status' => Yii::$app->request->get('status')]), 'get', [
    'class' => 'navbar-form navbar-left',
    'role' => 'search',
]) ?>
<div class="form-group">
    <?= Html::textInput('s', isset($queryParams['s']) ? $queryParams['s'] : '', [
        'placeholder' => 'Search everything',
        'class' => 'form-control',
    ]) ?>
</div>
<div class="form-group">
    <?=
        \kartik\daterange\DateRangePicker::widget([
            'name' => 'date-range',
            'value' => isset($queryParams['date-range']) ? $queryParams['date-range'] : '',
            'options' => [
                'class' => 'form-control'
            ],
            'pluginOptions' => [
//                'maxDate' => date('d/m/Y', strtotime('tomorrow')),
                'locale'=>[
                    'format' => 'DD/MM/YYYY HH:mm'
                ],
                'startDate' => date('d/m/Y', strtotime('today')),
                'endDate' => date('d/m/Y', strtotime('today')),
                'timePicker' => true
            ]
        ]);
    ?>

</div>
<div class="form-group">
    <?= Html::dropDownList('status', Yii::$app->request->get('status'), $allowedStatusLabels, [
        'class' => 'form-control',
        'prompt' => '--Trạng thái đơn hàng--'
    ]); ?>
</div>
<div class="form-group">
    <?= Html::dropDownList('shipping_method', $method, $shippingMethods, [
        'class' => 'form-control',
        'prompt' => '-- Shipping Method --'
    ]);?>
</div>

<div class="form-group">
    <?= Html::dropDownList('location', Yii::$app->request->get('location'), [
        $codSettings['northern'] => 'Miền Bắc',
        $codSettings['southern'] => 'Miền Nam'
    ], [
        'class' => 'form-control',
        'prompt' => '-- Chọn Kho --'
    ]); ?>
</div>
<div class="form-group">
    <?= Html::dropDownList('call_status', isset($queryParams['call_status']) ? $queryParams['call_status'] : '', $callStatuses, [
        'class' => 'form-control',
        'prompt' => '--Trạng thái gọi--'
    ]); ?>
</div>

<div class="form-group">
    <?= Html::dropDownList('course_type', isset($queryParams['course_type']) ? $queryParams['course_type'] : '', $courseTypes, [
        'class' => 'form-control',
        'prompt' => '--Loại khóa học--'
    ]); ?>
</div>

<?= Html::submitButton('<i class="ion-android-search"></i> Tìm đơn hàng', ['class' => 'btn btn-default']) ?>

<?php if(Yii::$app->request->get('date-range') && Yii::$app->request->get('course_type')) { ?>
    <div class="btn-group">
        <a href="<?= Url::toRoute(['/order/default/print-cod',
            'status' => Yii::$app->request->get('status'),
            'call_status' => Yii::$app->request->get('call_status'),
            'date-range' => Yii::$app->request->get('date-range'),
            'location' => Yii::$app->request->get('location'),
            'shipping_method' => Yii::$app->request->get('shipping_method'),
            'course_type' => Yii::$app->request->get('course_type'),
            'print' => 1
        ]) ?>" target="_blank" tabindex="-1" class="btn btn-success" title="In đơn hàng"><i class="glyphicon glyphicon-print"></i> In đơn hàng</span></a>
    </div>
<?php } ?>

<?= Html::endForm(); ?>
