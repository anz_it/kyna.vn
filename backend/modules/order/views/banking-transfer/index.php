<?php
    use yii\helpers\Html;
    use yii\widgets\ActiveForm;
    use kartik\widgets\DatePicker;
    use yii\grid\GridView;
$this->title = "Lịch sử thanh toán";
?>

<?php $form = ActiveForm::begin(); ?>
    <div class="box" style="border: 1px solid #3c3c3c;padding: 10px;width: 700px;margin: 20px 0 20px 0;">
        <?= $form->field($model, 'username')->textInput() ?>

        <?= $form->field($model, 'email')->textInput() ?>

        <?= $form->field($model, 'phone_number')->textInput() ?>

        <?= $form->field($model, 'filter')->dropDownList($list_charging) ?>

        <?=
            $form->field($model, 'trans_date_begin')->widget(DatePicker::classname(), [
            'options' => ['placeholder' => 'Enter transfer date ...'],
            'pluginOptions' => [
                'autoclose'=>true,
                'format' => 'yyyy-mm-dd'
            ]]);
        ?>

        <?=
            $form->field($model, 'trans_date_end')->widget(DatePicker::classname(), [
            'options' => ['placeholder' => 'Enter transfer date ...'],
            'pluginOptions' => [
                'autoclose'=>true,
                'format' => 'yyyy-mm-dd'

            ]]);
        ?>
        <?= Html::submitButton($content = 'Tìm kiếm', $options = ['class' => 'btn normal']) ?>
    </div>
<?php ActiveForm::end(); ?>

<div class="row">
    <div class="col-xs-12">
        <div class="becategory-index">
            <div class="box">
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],
                        [
                            'label' => 'Ngày chuyển khoản',
                            'value' => function($model)
                            {
                                return date('d/m/Y - H:i:s',$model['order_date']);
                            }
                        ],
                        [
                            'label' => 'Thông tin tài khoản',
                            'value' => function($model)
                            {
                                return $model['username'] . ' - ' . $model['email'] . ' - ' . $model['phone_number'];
                            }
                        ],
                        [
                            'label' => 'Số tiền',
                            'value' => function($model)
                            {
                                return number_format($model['total']) . ' VNĐ';
                            }
                        ],
                        [
                            'label' => 'Khóa học',
                            'value' => function($model) {
                                $modelBanking = new \backend\modules\order\models\Banking();
                                if($model['course_combo_id'] != 0)
                                {
                                    return $modelBanking->getCourseName($model['course_combo_id']);
                                }
                                else
                                {
                                    return $modelBanking->getCourseName($model['course_id']);
                                }
                            }
                        ],
                        [
                            'label' => 'Người thêm giao dịch',
                            'value' => function($model) {
                                $modelBanking = new \backend\modules\order\models\Banking();
                                if($model['operator_id'] != NULL)
                                {
                                    return $modelBanking->getUserAddTranfer($model['operator_id']);
                                }
                                else
                                {
                                    return '';
                                }
                            }
                        ],
                    ],
                ]); ?>
            </div>
        </div>
    </div>
</div>
