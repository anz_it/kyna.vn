<?php

use yii\bootstrap\ActiveForm;

?>

<?php $form = ActiveForm::begin([
    'id' => 'update-payment-form',
    'options' => ['class' => 'form-data-ajax'],
]); ?>

<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title" id="modal-label">Thay đổi phương thức thanh toán cho đơn hàng #<?= $formModel->order_id ?></h4>
</div>
<div class="modal-body">
    <?= $form->field($formModel, 'payment_method')->radioList($paymentMethods) ?>
    <div id="shipping-address" style="display: none">
        <?= $form->field($formModel, 'contact_name') ?>
        <?= $form->field($formModel, 'phone_number') ?>
        <?= $form->field($formModel, 'street_address') ?>
        <?= $form->field($formModel, 'location_id')->widget(\common\widgets\locationfield\LocationField::className())->label(false) ?>
    </div>
    <?= $form->field($formModel, 'note')->textarea(['rows' => 3]) ?>
</div>
<div class="modal-footer">
    <button type="submit" href="#" class="btn btn-default btn-lg"><i class="fa fa-money fa-fw"></i> Xác nhận thay đổi</button>
</div>

<?php ActiveForm::end(); ?>
<script type="text/javascript">
    $('input[name="ActionForm[payment_method]"]').on('change', function() {
       if($(this).val() == 'cod'){
           $('#shipping-address').show();
       }
       else{
           $('#shipping-address').hide();
       }
    });
</script>
