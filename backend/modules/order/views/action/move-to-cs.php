<?php

use yii\bootstrap\ActiveForm;

?>
<?php $form = ActiveForm::begin([
    'id' => 'move-order-form',
    'options' => ['class' => 'form-data-ajax'],
]); ?>

<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title" id="modal-label">Xác nhận chuyển đơn hàng #<?= $formModel->order_id ?> sang CS</h4>
</div>
<div class="modal-body">
    <?= $form->field($formModel, 'note')->textArea(['rows' => 5]) ?>
    <?= $form->field($formModel, 'confirm')->checkbox()->label('Xác nhận') ?>
</div>
<div class="modal-footer">
    <button type="submit" class="btn btn-link"><u class="text-danger">Tôi đồng ý chuyển đơn hàng này</u></button>
</div>

<?php ActiveForm::end(); ?>
