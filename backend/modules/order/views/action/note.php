<?php

use yii\bootstrap\Html;
use yii\bootstrap\ActiveForm;

?>
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title" id="modal-label">Cập nhật ghi chú cho đơn hàng #<?= $model->id ?></h4>
</div>

<?php $form = ActiveForm::begin([
    'id' => 'form-call-status',
    'options' => ['class' => 'form-data-ajax'],
]); ?>
<div class="modal-body">
    <?= Html::activeHiddenInput($formModel, 'order_id') ?>

    <?= $form->field($formModel, 'status')->radioList($statuses) ?>
    <?= $form->field($formModel, 'note')->textArea([
        'rows' => 5,
    ]) ?>
</div>
<div class="modal-footer">
    <button type="submit" class="btn btn-primary">Add note</button>
</div>
<?php ActiveForm::end(); ?>