<?php

use yii\bootstrap\ActiveForm;
use kyna\order\models\OrderDetails;
?>

<?php $form = ActiveForm::begin([
    'id' => 'partner-code-form',
    'options' => [
        'data-ajax' => true,
        'data-target' => '#modal .modal-content',
        'data-push-state' => false,
    ],
    ]); ?>

<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title" id="modal-label">Cập nhật serial cho đối tác (Order #<?= $formModel->order_id ?>)</h4>
</div>
<div class="modal-body">
    <?php foreach ($partnerDetails as $key => $partnerDetail): ?>
        <?php
        $combo = $partnerDetail->combo;
        $course = $partnerDetail->course;
        $courseName = ($combo && $combo->isComboPartner) ? $combo->name : $course->name;
        ?>
        <?= $form->field($partnerDetail, "[$key]activation_code")->label($courseName) ?>
    <?php endforeach; ?>
</div>
<div class="modal-footer">
    <button type="submit" href="#" class="btn btn-primary btn-lg"><i class="fa fa-edit"></i> Cập nhật</button>
</div>

<?php ActiveForm::end(); ?>
