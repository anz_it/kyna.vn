<?php
use yii\bootstrap\Nav;
use yii\bootstrap\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;

?>
    <style>
        .modal-dialog {
            width: auto !important;
            max-width: 1000px !important;
        }

    </style>
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                    aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="modal-label">Lịch sử thao tác đơn hàng #<?= $model->id ?></h4>
    </div>
    <div class="modal-body">
        <?= GridView::widget([
            'dataProvider' => $actionDataProvider,
            'columns' => [
                'name:raw:Thao tác',
                'user.profile.name:raw:Người thực hiện',
                'user.email:email',
                'action_time:datetime:Thực hiện lúc',
                [
                    'label' => 'Ghi chú',
                    'format' => 'raw',
                    'value' => function ($model) {
                        /* @var $model \kyna\order\models\actions\OrderAction */
                        $orderActionMeta = ArrayHelper::map($model->getOrderActionMeta()->select(['key', 'value'])->all(), 'key', 'value');
                        $str = '';
                        foreach ($orderActionMeta as $key => $val) {
                            $str .= "$key: $val<br>";
                        }

                        return $str;
                    }
                ],
            ]
        ]) ?>
    </div>
<?php

