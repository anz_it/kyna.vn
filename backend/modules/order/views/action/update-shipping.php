<?php

use yii\bootstrap\ActiveForm;
use common\widgets\locationfield\LocationField;
?>

<?php $form = ActiveForm::begin([
    'id' => 'send-to-ghn-form',
    'options' => ['class' => 'form-data-ajax'],
    ]); ?>

<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title" id="modal-label">Cập nhật địa chỉ giao hàng cho đơn hàng #<?= $formModel->order_id ?></h4>
</div>
<div class="modal-body">
    <?= $form->field($formModel, 'contact_name') ?>
    <?= $form->field($formModel, 'phone_number') ?>
    <?= $form->field($formModel, 'street_address') ?>
    <?= $form->field($formModel, 'location_id')->widget(LocationField::className())->label(false) ?>
    <?= $form->field($formModel, 'note')->textArea(['rows' => 4]) ?>
</div>
<div class="modal-footer">
    <button type="submit" href="#" class="btn btn-primary btn-lg"><i class="fa fa-edit"></i> Cập nhật</button>
</div>

<?php ActiveForm::end(); ?>
