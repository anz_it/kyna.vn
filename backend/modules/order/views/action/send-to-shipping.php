<?php
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;

use common\widgets\locationfield\LocationField;
?>

<?php $form = ActiveForm::begin([
    'id' => 'send-to-cod-form',
    'options' => ['class' => 'form-data-ajax'],
    ]); ?>
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title" id="modal-label">Chuyển đơn hàng #<?= $formModel->order_id ?> qua giao vận <span><strong>(COD)</strong></span></h4>
</div>
<div class="modal-body">
    <?= $form->field($formModel, 'payment_method')->textInput(['readonly' => true, 'value' => key($paymentMethods)]) ?>
    <?= $form->field($formModel, 'shipping_method')->radioList($shippingMethods) ?>
    <?= $form->field($formModel, 'pick_up_location')->radioList([]) ?>
    <?= $form->field($formModel, 'expected_payment_fee') ?>
    <?= $form->field($formModel, 'contact_name') ?>
    <?= $form->field($formModel, 'phone_number') ?>
    <?= $form->field($formModel, 'activation_code')->textInput(['readonly' => true]) ?>
    <?= $form->field($formModel, 'street_address') ?>
    <?= $form->field($formModel, 'location_id')->widget(LocationField::className())->label(false) ?>
    <?= $form->field($formModel, 'note')->textArea(['rows' => 4]) ?>
</div>
<div class="modal-footer">
    <button type="submit" href="#" class="btn btn-primary btn-lg"><i class="fa fa-truck fa-fw"></i> Chuyển qua giao vận</button>
</div>
<?php ActiveForm::end(); ?>


<script type="text/javascript">
    var curLocation = -1;

    // hide Payment Method
//    $(".form-group.field-actionform-payment_method").hide();

    // hide Pickup Location or start
    $(".form-group.field-actionform-pick_up_location").hide();
    // hide expedted payment_fee
    $(".form-group.field-actionform-expected_payment_fee").hide();
    // filter shipping Method on start
    filterShippingMethod();

    $("#actionform-location_id").on('change', function (e) {
        e.preventDefault();
        filterShippingMethod();
    });

    // Update pick-up-location, fee, when shipping method or shipping location changed
    $("#actionform-shipping_method input[type='radio']").on('change', function (e) {
        e.preventDefault();
        updateShippingFee();
    });

    // Update expected-fee whern pick-up-location changed
    $(".form-group.field-actionform-pick_up_location").on('change',  "input[type='radio']", function (e) {
        $("#actionform-expected_payment_fee").val($(this).attr('fee'));
    });

    // Function update Shipping fee
    function updateShippingFee() {
        $('.form-group.field-actionform-pick_up_location').hide();

        switch ($("#actionform-shipping_method input[type='radio']:checked").val()) {
            // Neu shipping method la proship thi hien thi, khong thi thoi
            case 'proship':
                locationId = $('#actionform-location_id :selected').val();
                if (!locationId)
                    break;
                if (locationId == 0)
                    break;
                params = {
                    _csrf: $("#send-to-cod-form input[name='_csrf']").val(),
                    locationId: locationId,
                    orderId: <?= $formModel->order_id ?>,
                    url: '/api/proship/getfeelist'
                };
                getShippingFees(params);
                break;
            case 'ghn':
                $('.form-group.field-actionform-pick_up_location').hide();
                break;
            case 'ghtk':
                locationId = $('#actionform-location_id :selected').val();
                if (!locationId)
                    break;
                if (locationId == 0)
                    break;
                params = {
                    _csrf: $("#send-to-cod-form input[name='_csrf']").val(),
                    locationId: locationId,
                    orderId: <?= $formModel->order_id ?>,
                    url: '/api/ghtk/getfeelist'
                };
                getShippingFees(params);
                break;
            default:
                $('.form-group.field-actionform-pick_up_location').hide();
                break;
        };
    }

    // Function get shipping fee
    function getShippingFees(params)
    {
        $('#loader').css('z-index', 9999);
        $('#loader').addClass('loading');
        $('#send-to-cod-form button[type="submit"]').prop('disabled', true);
        // Check if status change
//        if (curLocation == params.locationId) {
//            $('.form-group.field-actionform-pick_up_location').show();
//            $('#loader').removeClass('loading');
//            $('#send-to-cod-form button[type="submit"]').removeAttr('disabled');
//            return;
//        }
        // Prepare Url
        url = window.location.protocol + '//' + window.location.host + params.url;
        // Prepare params
        data = params;
        $.ajax({
            method: 'POST',
            url: url,
            data: data,
            success: function(res) {
                data = JSON.parse(res);
                // Failed to get fee list -> show error message
                if (data.success == false) {
                    $("#modal").modal("hide");
                    $('#loader').removeClass('loading');
                    $('#send-to-cod-form button[type="submit"]').removeAttr('disabled');
                    $.notify('Xử lý thất bại, không lấy được shipping fee',{
                        type: 'danger',
                        allow_dismiss: true
                    });
                    return false;
                }
                // Success -> generate PickUpLocation Radio button
                var renderHtml = "";
                data.forEach(function(pickUp) {
                    // Render here
                    renderHtml +=
                        "<div class='radio'>" +
                            "<label>" +
                                "<input type='radio' name='ActionForm[pick_up_location]' value='" + pickUp.id + "' fee='" + pickUp.fee +"'>" +
                                    pickUp.name + " : <strong>" + pickUp.displayFee + "</strong>" +
                            "</label>" +
                        "</div>";
                });
                $('#actionform-pick_up_location').html(renderHtml);

                // Choose the cheapest pickup Location
                chooseCheapestPickUpLocation();

                $('.form-group.field-actionform-pick_up_location').show();
                curLocation = params.locationId;
                $('#loader').removeClass('loading');
                $('#send-to-cod-form button[type="submit"]').removeAttr('disabled');
            },
            error: function (err) {
                $("#modal").modal("hide");
                $('#loader').removeClass('loading');
                $('#send-to-cod-form button[type="submit"]').removeAttr('disabled');
                $.notify('Xử lý thất bại, không lấy được shipping fee',{
                    type: 'danger',
                    allow_dismiss: true
                });
                return false;
            }
        });
    };

    // Function to auto choose the cheapest pick up location
    function chooseCheapestPickUpLocation()
    {
        var el = null;
        var min = null;
        $("#actionform-pick_up_location input[type='radio']").each(function () {
            if (min == null || parseFloat($(this).attr('fee')) < min) {
                el = this;
                min = parseFloat($(this).attr('fee'));
            }
        });
        $(el).prop('checked', true);
        $("#actionform-expected_payment_fee").val($(el).attr('fee'));
    };

    // Function filter shipping method
    function filterShippingMethod() {
        if ($('#actionform-location_id').val() == '') {
            $("#actionform-shipping_method input[value='ghtk']").parent().show();
            return;
        }

        // check if ghtk supported
        url = window.location.protocol + '//' + window.location.host + '/api/ghtk/is-location-support';
        data = {
            shipping_method: 'ghtk',
            locationId: $('#actionform-location_id').val(),
        };
        $.ajax({
            method: 'POST',
            url: url,
            data: data,
            success: function (res) {
                data = JSON.parse(res);
                if (data.success === false) {
                    $("#actionform-shipping_method input[value='ghtk']").parent().show();
                    updateShippingFee();
                    return;
                }
                if (!data.result) {
                    // not supported hide ghtk
                    $("#actionform-shipping_method input[value='ghtk']").parent().hide();
                    // if ghtk chosen -> click ghn
                    if ($("#actionform-shipping_method input[type='radio']:checked").val() == 'ghtk') {
                        $.notify('GHTK không hỗ trợ địa điểm này!!', {
                            type: 'danger',
                            allow_dismiss: true,
                            z_index: 999999
                        });
                        $("#actionform-shipping_method input[value='ghn']").parent().click();
                        return;
                    } else {
                        updateShippingFee();
                        return;
                    }
                } else {
                    $("#actionform-shipping_method input[value='ghtk']").parent().show();
                    updateShippingFee();
                    return;
                }
            },
            error: function (err) {
                // error thi khong lam gi
                $("#actionform-shipping_method input[value='ghtk']").parent().show();
                updateShippingFee();
            }
        });
    }
</script>
