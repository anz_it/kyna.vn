<?php

use yii\bootstrap\ActiveForm;

?>

<?php $form = ActiveForm::begin([
    'id' => 'request-payment-form',
    'options' => ['class' => 'form-data-ajax'],
    ]); ?>

<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title" id="modal-label">Yêu cầu khách hàng thanh toán cho đơn hàng #<?= $formModel->order_id ?></h4>
</div>
<div class="modal-body">
    <?= $form->field($formModel, 'payment_method')->radioList($paymentMethods) ?>
</div>
<div class="modal-footer">
    <button type="submit" href="#" class="btn btn-default btn-lg"><i class="fa fa-money fa-fw"></i> Gửi yêu cầu thanh toán</button>
</div>

<?php ActiveForm::end(); ?>
