<?php
use yii\bootstrap\Html;
use yii\bootstrap\ActiveForm;
use yii\grid\GridView;
?>

<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title" id="modal-label">Tiếp nhận đơn hàng #<?= $model->id ?></h4>
</div>
<div class="call-timer">
    <div class="modal-body">
        <div class="text-center">
            <div class="jumbotron">
                <div class="timer h1"></div>
                <a href="#" class="btn btn-primary btn-end-call">Kết thúc</a>
            </div>
            <p>Đang thực hiện cuộc gọi tới số <strong><?= $phoneNumber ?></strong> (<?= $contactName ?>)</p>
        </div>
        <?= GridView::widget([
            'dataProvider' => $actions,
            'columns' => [
                'user.email:email:Người gọi',
                'start:datetime:Gọi lúc',
                'duration:raw:Thời lượng',
                [
                    'header' => 'Tình trạng',
                    'attribute' => 'status',
                    'value' => function ($model) use ($statuses) {
                        if (array_key_exists($model->status, $statuses)) {
                            return $statuses[$model->status];
                        }
                        // var_dump($model->status);
                        // return $statuses[$model->status];
                    }
                ]
            ]
        ]) ?>

    </div>
</div>
<?php $form = ActiveForm::begin([
    'id' => 'form-call-status',
    'options' => ['class' => 'hide form-data-ajax'],
]); ?>
<div class="modal-body">
    <?= Html::activeHiddenInput($formModel, 'checksum') ?>
    <?= Html::activeHiddenInput($formModel, 'order_id') ?>
    <?= Html::activeHiddenInput($formModel, 'start') ?>

    <?= $form->field($formModel, 'status')->radioList($statuses) ?>
    <?= $form->field($formModel, 'note')->textArea([
        'rows' => 5,
    ]) ?>
</div>
<div class="modal-footer">
    <button type="submit" class="btn btn-primary">Lưu thông tin</button>
</div>
<?php ActiveForm::end(); ?>
