<?php

use yii\bootstrap\ActiveForm;

?>
<?php $form = ActiveForm::begin([
    'id' => 'complete-order-form',
    'options' => ['class' => 'form-data-ajax'],
]); ?>

<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title" id="modal-label">Yêu cầu hoàn thành đơn hàng #<?= $formModel->order_id ?></h4>
</div>
<div class="modal-body">
    <?= $form->field($formModel, 'payment_receipt') ?>
    <?= $form->field($formModel, 'note')->textArea(['rows' => 5]) ?>
    <?= $form->field($formModel, 'confirm')->checkbox()->label('Xác nhận đơn hàng đã hoàn thành') ?>
</div>
<div class="modal-footer">
    <button type="submit" href="#" class="btn btn-success btn-lg"><i class="fa fa-check fa-fw"></i> Hoàn tất &amp; kích hoạt khóa học</button>
</div>

<?php ActiveForm::end(); ?>
