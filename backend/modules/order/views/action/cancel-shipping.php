<?php

use yii\bootstrap\ActiveForm;

?>
<?php $form = ActiveForm::begin([
    'id' => 'cancel-shipping-form',
    'options' => ['class' => 'form-data-ajax'],
]); ?>

<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title" id="modal-label">Xác nhận Hủy giao vận cho đơn hàng #<?= $formModel->order_id ?></h4>
</div>
<div class="modal-body">
    <?= $form->field($formModel, 'note')->textArea(['rows' => 5]) ?>
    <?= $form->field($formModel, 'confirm')->checkbox()->label('Xác nhận hủy giao vận') ?>
</div>
<div class="modal-footer">
    <button type="submit" class="btn btn-link"><u class="text-danger">Tôi đồng ý hủy giao vận cho đơn hàng này</u></button>
</div>

<?php ActiveForm::end(); ?>
