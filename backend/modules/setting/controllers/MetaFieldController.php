<?php

namespace app\modules\setting\controllers;

use Yii;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

class MetaFieldController extends \app\components\controllers\MetaFieldController
{

    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'matchCallback' => function () {
                            return Yii::$app->user->can('Setting.MetaField');
                        },
                    ],
                ],
            ],
        ]);
    }
}