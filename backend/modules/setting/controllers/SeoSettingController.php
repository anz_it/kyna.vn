<?php

namespace app\modules\setting\controllers;

use kyna\base\models\MetaField;
use Yii;
use kyna\settings\models\SeoSetting;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * SeoSettingController implements the CRUD actions for SeoSetting model.
 */
class SeoSettingController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'matchCallback' => function () {
                            return Yii::$app->user->can('Setting.Seo') || Yii::$app->user->can('Admin');
                        },
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all SeoSetting models.
     * @return mixed
     */
    public function actionIndex()
    {
        $model = new SeoSetting();
        $metaValueModels = $this->getMetaField();
        if (Yii::$app->request->post()) {
            $data = Yii::$app->request->post();
            foreach ($data['SeoSetting'] as $key => $value) {
                $metaValueModels[$key]->value = $value['value'];
                $metaValueModels[$key]->save();
            }
            Yii::$app->session->setFlash('success', 'Cập nhật thành công.');
        }
        return $this->render('index', [
            'model' => $model,
            'metaValueModels' => $metaValueModels
        ]);
    }

    public function getMetaField()
    {
        $metaFields = MetaField::find()->andWhere([
            'status' => MetaField::STATUS_ACTIVE,
            'model' => MetaField::MODEL_SEO_SETTING
        ])->all();
        $metaValueModels = [];
        foreach ($metaFields as $metaField) {
            $metaValueModel = SeoSetting::find()->andWhere([
                'key' => $metaField->key,
            ])->one();
            if (empty($metaValueModel)) {
                $metaValueModel = new SeoSetting();
                $metaValueModel->key = $metaField->key;
            }
            $metaValueModel->key = $metaField->key;
            $metaValueModels[$metaField->key] = $metaValueModel;
        }
        return $metaValueModels;
    }

    /**
     * Displays a single SeoSetting model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new SeoSetting model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new SeoSetting();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing SeoSetting model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing SeoSetting model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the SeoSetting model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return SeoSetting the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SeoSetting::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
