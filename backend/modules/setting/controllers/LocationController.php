<?php

namespace app\modules\setting\controllers;

use Yii;
use yii\data\ActiveDataProvider;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

use kyna\base\models\Location;
use common\helpers\TreeHelper;

/**
 * LocationController implements the CRUD actions for Location model.
 */
class LocationController extends \app\components\controllers\Controller
{
    
    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'matchCallback' => function () {
                            return Yii::$app->user->can('Setting.Location');
                        },
                    ],
                ],
            ],
        ]);
    }

    /**
     * Lists all Location models.
     *
     * @return mixed
     */
    public function actionIndex($id = null)
    {
        if ($id) {
            $model = $this->findModel($id);
        } else {
            $model = new Location();
        }
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if ($id) {
                Yii::$app->session->setFlash('success', 'Cập nhật thành công.');
            } else {
                Yii::$app->session->setFlash('success', 'Thêm thành công.');
            }
            return $this->redirect(['index']);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => Location::find(),
            'pagination' => false,
        ]);

        $locModels = $dataProvider->getModels();
        $options = TreeHelper::dropdownOptions($locModels);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'model' => $model,
            'locs' => $options,
        ]);
    }

    /**
     * Finds the Location model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param int $id
     *
     * @return Location the loaded model
     *
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Location::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
