<?php
/**
 * Created by PhpStorm.
 * User: ngunp
 * Date: 7/27/2017
 * Time: 2:01 PM
 */

namespace app\modules\setting\controllers;

use kyna\seo\helpers\SitemapXmlHelper;
use kyna\servicecaller\traits\CurlTrait;
use yii\base\Exception;
use yii\data\ActiveDataProvider;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\Controller;
use yii\db\Query;
use Yii;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

class SitemapController extends Controller
{
    use CurlTrait;

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'update' => ['get','post'],
                    'generate' => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->user->can('Setting.Sitemap');
                        }
                    ],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        // Get Data Provider
        $query = new Query();
        $_query = $query->select('*')
            ->from(SitemapXmlHelper::TABLE_NAME)
            ->where([
                'is_deleted' => false,
            ]);
        $dataProvider = new ActiveDataProvider([
            'query' => $_query,
        ]);
        $data = $_query->all();

//        $dataProvider = new DataProvider()
        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'changeFregs' => SitemapXmlHelper::CHANGE_FREQS,
            'data' => $data,
        ]);
    }

    public function actionUpdate()
    {
        if (Yii::$app->request->isGet) {
            // Get Data Provider
            $query = new Query();
            $_query = $query->select('*')
                ->from(SitemapXmlHelper::TABLE_NAME)
                ->where([
                    'is_deleted' => false,
                ]);
            $dataProvider = new ActiveDataProvider([
                'query' => $_query,
            ]);
            $data = $_query->all();

            return $this->render('update', [
                'dataProvider' => $dataProvider,
                'changeFregs' => SitemapXmlHelper::CHANGE_FREQS,
                'data' => $data,
            ]);

        }
        if (Yii::$app->request->isPost) {
            // Get data
            $params = $_POST;
            try {
                foreach ($params as $key => $value) {
                    $query = new Query();
                    $query->createCommand()->update(
                        'sitemaps',
                        [
                            'changefreg' => $value['changeFreq'],
                            'priority' => $value['priority'],
                            'is_enabled' => $value['isEnabled'],

                            'updated_by' => Yii::$app->user->id,
                            'updated_time' => time(),
                        ],
                        [
                            'name' => $key,
                        ]
                    )->execute();
                }
//                $ret = [
//                    'success' => true,
//                    'message' => 'Update sitemaps setting successfully',
//                ];
//                echo Json::encode($ret);
//                Yii::$app->end();
                $redirectUrl = Url::to('index');
                Yii::$app->session->setFlash('success', 'SUCCESSFUL, sitemaps setting updated');
                return $this->redirect(empty($redirectUrl) ? ['index'] : $redirectUrl);

            } catch (Exception $err) {
//                $ret = [
//                    'success' => false,
//                    'message' => 'VALIDATE FORM ERROR',
//                ];
//                echo Json::encode($ret);
//                Yii::$app->end();
                $redirectUrl = Url::to('index');
                Yii::$app->session->setFlash('error', 'Update sitemaps setting FAILED');
                return $this->redirect(empty($redirectUrl) ? ['index'] : $redirectUrl);
            }
//            $ret = [
//                'success' => false,
//                'message' => 'VALIDATE FORM ERROR',
//            ];
//            echo Json::encode($ret);
//            Yii::$app->end();
            $redirectUrl = Url::to('index');
            Yii::$app->session->setFlash('error', 'Update sitemaps setting FAILED');
            return $this->redirect(empty($redirectUrl) ? ['index'] : $redirectUrl);
        }
    }

    public function actionGenerate() {
        if (Yii::$app->request->isPost) {
            try {
//                $query = new Query();
//                $data = $query->select(['*'])
//                    ->from(SitemapXmlHelper::TABLE_NAME)
//                    ->where([
//                        'is_deleted' => false,
//                        'is_enabled' => true,
//                    ])->all();
//
//                if (empty($data)) {
//                    $ret = [
//                        'success' => false,
//                        'message' => "none Sitemap selected",
//                    ];
//                    echo Json::encode($ret);
//                    Yii::$app->end();
//                }
//
//
//                $rootPath = Yii::getAlias('@root');
//                $command = "php {$rootPath}/yii seo/sitemap-xml/index -i=true -m=";
//                foreach ($data  as $key => $value) {
//                    $command .= $value['name'].',';
//                }
//                $command = substr($command, 0, -1);
////                var_dump($command); die();
//                exec($command);
                $hashString = 'ilovekyna@NobUnobu@2356';
                $password = hash('sha256', $hashString);
                $url = Yii::$app->params['template_server']['_baseUrl'].'/generate-sitemap.php?p='.$password;

//                var_dump($url);
                $respone = Json::decode($this->call($url));

                if ($respone['success']) {
                    $ret = [
                        'success' => true,
                        'message' => "Succecss!! Sitemap Generated",
                        'data' => $respone['data'],
                    ];
                    echo Json::encode($ret);
                    Yii::$app->end();
                } else {
                    $ret = [
                        'success' => false,
                        'message' => "Error!! Sitemap generate FAILED",
                        'data' => $respone['data'],
                    ];
                    echo Json::encode($ret);
                    Yii::$app->end();

                }
            } catch (Exception $err) {
                $ret = [
                    'success' => false,
                    'message' => "Error!! Sitemap generate FAILED",
                ];
                echo Json::encode($ret);
                Yii::$app->end();
            }

        }
    }

}