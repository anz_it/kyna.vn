<?php

namespace app\modules\setting\controllers;

use Yii;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

use kyna\base\models\MetaField;
use kyna\settings\models\Setting;
use app\components\controllers\Controller;

class SettingController extends Controller
{

    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'matchCallback' => function () {
                            return Yii::$app->user->can('Setting.Common');
                        },
                    ],
                ],
            ],
        ]);
    }

    public function actionIndex()
    {
        $model = new Setting();
        $metaValueModels = $this->getMetaField();
        if (Yii::$app->request->post()) {
            $data = Yii::$app->request->post();
            foreach ($data['Setting'] as $key => $value) {
                $metaValueModels[$key]->value = $value['value'];
                $metaValueModels[$key]->save();
            }

            // Update crontab
                // prepare params
            $params = [
                'seo_fbofflineconversion_daily_is_enabled' => Setting::findOne(['key' => 'seo_fbofflineconversion_daily_is_enabled'])->value,
                'seo_fbofflineconversion_daily_time' => Setting::findOne(['key' => 'seo_fbofflineconversion_daily_time'])->value,
            ];
//            var_dump($params);
            self::updateCrontab($params);
            Yii::$app->session->setFlash('success', 'Cập nhật thành công.');
        }
        return $this->render('index', [
            'model' => $model,
            'metaValueModels' => $metaValueModels
        ]);
    }

    public function actionCreate()
    {
        $model = new Setting();

        if (\Yii::$app->request->isPost) {

        }
        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionEdit($id)
    {

    }

    protected function loadModel($id)
    {
        if (($model = Setting::findOne($id)) !== null) {
            return $model;
        } else {
            throw new yii\web\NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function getMetaField()
    {
        $metaFields = MetaField::find()->andWhere([
            'status' => MetaField::STATUS_ACTIVE,
            'model' => MetaField::MODEL_SETTING
        ])->all();
        $metaValueModels = [];
        foreach ($metaFields as $metaField) {
            $metaValueModel = Setting::find()->andWhere([
                'meta_field_id' => $metaField->id,
            ])->one();
            if (empty($metaValueModel)) {
                $metaValueModel = new Setting();
                $metaValueModel->meta_field_id = $metaField->id;
            }
            $metaValueModel->key = $metaField->key;
            $metaValueModels[$metaField->key] = $metaValueModel;
        }
        return $metaValueModels;
    }

    private function updateCrontab($params)
    {
        // Facebook Offline Conversion
        $hashString = 'ilovekyna@fboc@2356';
        $password = hash('sha256', $hashString);

        $hour = 0;
        $minute = 0;
        $parsedTime = mb_split("(:|\s)", $params['seo_fbofflineconversion_daily_time']);
        if (count($parsedTime) == 3) {
            $hour = intval($parsedTime[0]);
            $minute = intval($parsedTime[1]);
            if ($parsedTime[2] == "PM") {
                $hour += 12;
            }
        }
        $isEnable = boolval($params['seo_fbofflineconversion_daily_is_enabled']);
        $data = [
            'p' => $password,
            'h' => $hour,
            'm' => $minute,
            'enabled' => $isEnable
        ];
        $command = Yii::$app->params['template_server']['_baseUrl'].'/update_crontab.php';
        $url = $command;
        $url .= '?'.http_build_query($data);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec ($ch);
        curl_close ($ch);
    }}
