<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\components\MetaFieldType;

$crudTitles = Yii::$app->params['crudTitles'];

$this->title = "Setting";
?>

<div class="group-discount-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-12">
            <?php
            foreach ($metaValueModels as $key => $metaValueModel) {
                echo MetaFieldType::render($form, $metaValueModel, "[{$key}]value", ['id' => "meta-{$key}"]);
            }
            ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton($crudTitles['update'], ['class' => 'btn btn-primary']) ?>
        <?= Html::a($crudTitles['cancel'], ['index'],['class' => 'btn btn-default']) ?>
    </div>
    <?php ActiveForm::end(); ?>

</div>
