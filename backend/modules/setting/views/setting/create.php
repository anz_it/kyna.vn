<?php

use yii\helpers\Html;

$this->title = Yii::$app->params['crudTitles']['create'];
$this->params['breadcrumbs'][] = ['label' => 'Setting', 'url' => ['index']];

if (!empty($model->parent)) {
    $this->params['breadcrumbs'][] = ['label' => $model->parent->name, 'url' => ['view', 'id' => $model->parent->id]];
}

$this->params['breadcrumbs'][] = $this->title;
?>
<div class="besetting-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>