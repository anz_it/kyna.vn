<?php
/**
 * Created by PhpStorm.
 * User: ngunp
 * Date: 7/27/2017
 * Time: 2:36 PM
 */
/* @var $this yii\web\View */

use yii\grid\GridView;
use common\helpers\Html;
use yii\helpers\Url;

$this->title = 'update';
$this->params['breadcrumbs'][] = ['label' => 'Sitemap', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;


$_data = [];
foreach ($data as $key => $value) {
    $_data[$value['name']] = $value;
}
?>

<?php
echo $this->render('_form', [
    'data' => $_data,
    'changeFregs' => $changeFregs,
]);
?>
<div class="col-sm-6">
    <?php
    echo Html::a('Save', Url::to('/setting/sitemap/update'), [
        'class' => 'btn btn-sm btn-success btn-block sitemap-save'
    ])
    ?>
</div>

<?php
$JS = "
    $('.sitemap-save').on('click', function(e) {
        e.preventDefault();
        // prepare params
        var data = {
            categories: {
                changeFreq: $('#setting-categories .change-frequency').val() ,
                priority: ($('#setting-categories .priority').val() != '')?$('#setting-categories .priority').val() : 0 ,
                isEnabled: $('#setting-categories .is-enabled').prop('checked')? 1 : 0,
            },
            courses: {
                changeFreq: $('#setting-courses .change-frequency').val(),
                priority: ($('#setting-courses .priority').val() != '')?$('#setting-courses .priority').val() : 0 ,
                isEnabled: $('#setting-courses .is-enabled').prop('checked')? 1 : 0,
            },
            tags: {
                changeFreq: ($('#setting-tags .change-frequency').val() != '')?$('#setting-tags .change-frequency').val() : 0 ,
                priority: ($('#setting-tags .priority').val() != '')?$('#setting-tags .priority').val() : 0 ,
                isEnabled: $('#setting-tags .is-enabled').prop('checked')? 1 : 0,
            },
            landing_pages: {
                changeFreq: ($('#setting-landing_pages .change-frequency').val() != '')?$('#setting-landing_pages .change-frequency').val() : 0 ,
                priority: ($('#setting-landing_pages .priority').val() != '')?$('#setting-landing_pages .priority').val() : 0 ,
                isEnabled: $('#setting-landing_pages .is-enabled').prop('checked')? 1 : 0,
            }
        }
        console.log(data)
        $.ajax({
            type: 'POST',
            url: '/setting/sitemap/update',
            data: data,
            dataType: 'JSON',
            success: function(data) {
                alert('vo day')
//                alert(data);
                console.log(data);
            },
            failue: function(error) {
                alert(error);
            }
            
        });
        
    });
    ";
$this->registerJs($JS);
?>
