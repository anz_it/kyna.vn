<?php
/**
 * Created by PhpStorm.
 * User: ngunp
 * Date: 7/27/2017
 * Time: 2:36 PM
 */
/* @var $this yii\web\View */

use yii\grid\GridView;
use common\helpers\Html;

?>

<div id="settings-container" class="col-sm-6">
    <?php
        foreach ($data as $key => $value) {
            echo $this->render('_panel', [
                'params' => $value,
                'changeFregs' => $changeFregs,
            ]);
        }
    ?>
</div>
<div id="settings-special-sitemap" class="col-sm-6">
    <div class="panel panel-default">
        <div class="panel-heading">Special sitemap </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-sm-3">
                    Sitemap Bai Viet
                </div>
                <div class="col-sm-5">
                    <a href="https://kyna.vn/bai-viet/sitemap_index.xml" target="_blank">https://kyna.vn/bai-viet/sitemap_index.xml</a>
                </div>
                <div class="col-sm-4">
                    <label>Enabled</label>
                    <?php echo Html::checkbox('isEnabled', true, [
                        'class' => 'is-enabled',
                        'disabled' => true,
                    ])?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
    $JS = "
        $('input.priority').keypress(function (evt) {
        evt.preventDefault();
        });
    ";
    $this->registerJs($JS);

?>