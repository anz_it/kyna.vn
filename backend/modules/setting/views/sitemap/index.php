<?php
/**
 * Created by PhpStorm.
 * User: ngunp
 * Date: 7/27/2017
 * Time: 2:36 PM
 */
/* @var $this yii\web\View */

use yii\grid\GridView;
use common\helpers\Html;
use yii\helpers\Url;

$this->title = 'Sitemap';
$this->params['breadcrumbs'][] = $this->title;

//var_dump($changeFregs);
$_data = [];
foreach ($data as $key => $value) {
    $_data[$value['name']] = $value;
}
?>

<?php
    echo $this->render('_form', [
        'data' => $_data,
        'changeFregs' => $changeFregs,
    ]);
?>
<div class="col-sm-3">
    <?php
    echo Html::a('EDIT', Url::to('/setting/sitemap/update'), [
        'class' => 'btn btn-sm btn-success btn-block'
    ])
    ?>
</div>
<div class="col-sm-3">
    <a href="#" id="sitemap-generate" class="btn btn-sm btn-info btn-block generate">GENERATE</a>
<!--    --><?php
//    echo Html::a('EDIT', Url::to('/setting/sitemap/update'), [
//        'class' => 'btn btn-sm btn-success  btn-block'
//    ])
//    ?>
</div>

<?php
    $JS = "
        $('.change-frequency').prop('disabled', true);
        $('.priority').prop('disabled', true);
        $('.is-enabled').prop('disabled', true);
        
        $('#sitemap-generate').on('click', function(e) {
            e.preventDefault();
            $('#loader').addClass('loading');
            $.ajax({
                type: 'POST',
                url: '/setting/sitemap/generate',
                data: {},
                dataType: 'JSON',
                success: function(data) {
                    $('#loader').removeClass('loading');
                    if (data.success == true || data.success == 'true') {
                        $.notify(data.message,{
                            type: 'success',
                            allow_dismiss: true
                        });
                    } else {
                        $.notify(data.message,{
                            type: 'error',
                            allow_dismiss: true
                        });
                    }
                },
                error: function(err) {
                    $('#loader').removeClass('loading');
                        $.notify('Error!! Sitemap generate FAILED',{
                            type: 'error',
                            allow_dismiss: true
                        });
                }
            });
            return false;
        });
        
    ";
    $this->registerJs($JS);
?>
