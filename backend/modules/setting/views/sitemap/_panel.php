<?php
/**
 * Created by PhpStorm.
 * User: ngunp
 * Date: 7/27/2017
 * Time: 4:34 PM
 */
use common\helpers\Html;
//$baseUrl = Yii::$app->params['baseUrl'];
//var_dump($baseUrl);
$baseUrl = "https://kyna.vn"
?>

<div id="setting-<?= $params['name'] ?>" class="panel panel-default">
    <div class="panel-heading">
            Sitemap <b><?= $params['name']?></b>: <a target="_blank" href="<?=$baseUrl?>/sitemap/<?= $params['name']?>.xml"><?=$baseUrl?>/sitemap/<?= $params['name']?>.xml</a>
    </div>
    <div class="panel-body">
        <div class="col-sm-4">
            <label>Change Fequency </label>
            <?php echo Html::dropDownList('change-frequency', $params['changefreg'], array_values($changeFregs), [
                'class' => 'change-frequency',
            ])?>
        </div>
        <div class="col-sm-3">
            <label>Priority</label>
            <?php echo Html::input('number', 'prioritiy', $params['priority'], [
                'step' => '0.1',
                'min' => 0,
                'max' => 1,
                'class' => 'priority',
        //                'disabled' => true
            ]) ?>
        </div>
        <div class="col-sm-2">
            <label>Enabled</label>
            <?php echo Html::checkbox('isEnabled', $params['is_enabled'], [
                'class' => 'is-enabled'
            ])?>
        </div>
    </div>
</div>
