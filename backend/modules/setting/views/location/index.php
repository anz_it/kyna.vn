<?php

use yii\bootstrap\Html;

use common\widgets\treeview\TreeView;
use kyna\base\assets\LocationFormAsset;

LocationFormAsset::Register($this);

$this->title = 'Địa phương';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="location-index">

        <div class="row">
            <div class="col-md-4">
                <p>
                    <a href="/setting/location/index/" class="btn btn-default"><i class="icon ion-android-add"></i> Thêm địa phương</a>
                </p>
                <?= $this->render('_form', [
                    'model' => $model,
                    'locs' => $locs,
                ]) ?>
            </div>
            <div class="col-md-8">
                <div class="dd">
                    <?= TreeView::widget([
                        'dataProvider' => $dataProvider,
                        //'urlPattern'    => '/membership/location/index/?id=%%id%%',
                        'cssClass' => 'dd-list',
                        'itemClass' => 'dd-item',
                        'childClass' => 'dd-list',
                        'sortable' => false,
                        'handleClass' => 'dd-handle icon ion-drag',
                        'itemWrapper' => "<div class='dd-label'>%s</div>",
                        'itemHtml' => function ($item) {
                            return '<span>'.$item['name'].'</span>';
                        },
                        'afterItem' => function ($item) {
                            return '<a href="/setting/location/index/?id='.$item['id'].'"><i class="glyphicon glyphicon-pencil"></i></a>'.
                                '<a href="/setting/location/delete/?id='.$item['id'].'" data-method="post"><i class="glyphicon glyphicon-trash"></i></a>';
                        },
                    ]); ?>
                </div>
            </div>
        </div>

</div>
