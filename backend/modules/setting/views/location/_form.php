<?php

use yii\bootstrap\Html;
use yii\bootstrap\ActiveForm;
use kyna\base\models\Location;

?>

<div class="location-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'parent_id')->dropdownList($locs, ['prompt' => '']) ?>
    
    <?= $form->field($model, 'type')->dropdownList(Location::getTypes(), ['prompt' => '--Chọn--']) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
