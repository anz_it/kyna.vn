<?php

use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use kyna\settings\models\ShippingSetting;
use kyna\base\models\Location;

$this->title = 'Shipping Settings';
$this->params['breadcrumbs'][] = $this->title;

$crudTitles = Yii::$app->params['crudTitles'];
$crudButtonIcons = Yii::$app->params['crudButtonIcons'];

?>
<div class="shipping-setting-index">
    <p>
        <?= Html::a($crudButtonIcons['create'] . ' ' . $crudTitles['create'], ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?php Pjax::begin(['timeout' => Yii::$app->params['timeOutPjax'], 'id' => 'idPjaxReload']) ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'id',
                'options' => ['class' => 'col-xs-1']
            ],
            [
                'attribute' => 'city_id',
                'value' => function ($model) {
                    return !empty($model->city_id) ? $model->city->name : 'Mặc định';
                },
                'filter' => Select2::widget([
                    'model' => $searchModel,
                    'attribute' => 'city_id',
                    'data' => ArrayHelper::map(Location::find()->andWhere(['status' => Location::STATUS_ACTIVE, 'parent_id' => 0])->all(), 'id', 'name'),
                    'options' => ['placeholder' => $crudTitles['prompt']],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ])
            ],
            [
                'attribute' => 'type',
                'value' => function ($model) {
                    return $model->typeText;
                },
                'filter' => Select2::widget([
                    'model' => $searchModel,
                    'attribute' => 'type',
                    'data' => Location::getTypes(),
                    'options' => ['placeholder' => $crudTitles['prompt']],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                    'hideSearch' => true,
                ])
            ],
            'fee',
            'min_amount',
            [
                'attribute' => 'status',
                'value' => function ($model) {
                    return $model->statusButton;
                },
                'format' => 'raw',
                'filter' => Select2::widget([
                    'model' => $searchModel,
                    'attribute' => 'status',
                    'data' => ShippingSetting::listStatus(false),
                    'options' => ['placeholder' => $crudTitles['prompt']],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                    'hideSearch' => true,
                ])
            ],
            ['class' => 'app\components\ActionColumnCustom'],
        ],
    ]); ?>
    <?php Pjax::end() ?>
</div>
