<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model kyna\settings\models\ShippingSetting */

$this->title = 'Create Shipping Setting';
$this->params['breadcrumbs'][] = ['label' => 'Shipping Settings', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="shipping-setting-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
