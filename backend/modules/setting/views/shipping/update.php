<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model kyna\settings\models\ShippingSetting */

$this->title = 'Update Shipping Setting: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Shipping Settings', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="shipping-setting-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
