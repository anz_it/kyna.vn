<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use kyna\settings\models\ShippingSetting;
use kyna\base\models\Location;
use yii\helpers\ArrayHelper;

$crudTitles = Yii::$app->params['crudTitles'];
?>

<div class="shipping-setting-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'city_id')->widget(Select2::classname(), [
        'data' => ArrayHelper::map(Location::find()->andWhere(['status' => Location::STATUS_ACTIVE, 'parent_id' => 0])->all(), 'id', 'name'),
        'options' => ['placeholder' => $crudTitles['prompt']],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]) ?>

    <?= $form->field($model, 'type')->widget(Select2::classname(), [
        'data' => Location::getTypes(),
        'hideSearch' => true,
        'options' => ['placeholder' => $crudTitles['prompt']],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]) ?>

    <?= $form->field($model, 'fee')->textInput() ?>

    <?= $form->field($model, 'min_amount')->textInput() ?>

    <?= $form->field($model, 'status')->widget(Select2::classname(), [
        'data' => ShippingSetting::listStatus(),
        'hideSearch' => true,
        'options' => ['placeholder' => $crudTitles['prompt']],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
