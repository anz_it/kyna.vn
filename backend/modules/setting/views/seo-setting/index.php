<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\components\MetaFieldType;

$crudTitles = Yii::$app->params['crudTitles'];

$this->title = "Setting";
?>

<div class="group-discount-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-6">
            <?php
            foreach ($metaValueModels as $key => $metaValueModel) {
                echo MetaFieldType::render($form, $metaValueModel, "[{$key}]value", ['id' => "meta-{$key}"]);
            }
            ?>
        </div>
        <div class="col-md-6">
            <h1>Các field có thể dùng</h1>

            <h2>Course</h2>
            <ul>
                <li>
                    <strong>{course_name}</strong>: Tên khóa học
                </li>
                <li>
                    <strong>{price}</strong>: Giá khóa học
                </li>
                <li>
                    <strong>{remaining_price}</strong>: Giá còn lại sau khi đã giảm
                </li>
                <li>
                    <strong>{teacher_name}</strong>: Tên giảng viên
                </li>
                <li>
                    <strong>{teacher_title}</strong>: Chức danh giảng viên
                </li>
                <li>
                    <strong>{percent_discount}</strong>: Giảm x%
                </li>
                <li>
                    <strong>{video_time}</strong>: Thời lượng video
                </li>
                <li>
                    <strong>{level}</strong>: Trình độ
                </li>
                <li>
                    <strong>{lesson_count}</strong>: Số lượng bài học
                </li>
            </ul>
            <h2>Teacher</h2>
            <ul>
                <li>
                    <strong>{teacher_name}</strong>: Tên giảng viên
                </li>
                <li>
                    <strong>{teacher_title}</strong>: Chức danh giảng viên
                </li>
            </ul>
            <h2>
                Category
            </h2>
            <h2>
                Tag
            </h2>
            <h2>
                Anchor Text
            </h2>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton($crudTitles['update'], ['class' => 'btn btn-primary']) ?>
        <?= Html::a($crudTitles['cancel'], ['index'],['class' => 'btn btn-default']) ?>
    </div>
    <?php ActiveForm::end(); ?>

</div>
