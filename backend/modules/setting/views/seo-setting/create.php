<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model kyna\settings\models\SeoSetting */

$this->title = 'Create Seo Setting';
$this->params['breadcrumbs'][] = ['label' => 'Seo Settings', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="seo-setting-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
