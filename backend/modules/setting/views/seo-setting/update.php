<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model kyna\settings\models\SeoSetting */

$this->title = 'Update Seo Setting: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Seo Settings', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="seo-setting-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
