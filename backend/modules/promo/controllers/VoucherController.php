<?php

namespace app\modules\promo\controllers;

use Yii;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

use kyna\promo\models\Voucher;
use kyna\promo\models\search\VoucherSearch;
use app\components\controllers\Controller;

/**
 * VoucherController implements the CRUD actions for Voucher model.
 */
class VoucherController extends Controller
{

    public $mainTitle = 'Vouchers';
    
    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'view', 'print-html', 'print-view'],
                        'matchCallback' => function () {
                            return Yii::$app->user->can('Voucher.View');
                        },
                    ],
                    [
                        'allow' => true,
                        'actions' => ['create'],
                        'matchCallback' => function () {
                            return Yii::$app->user->can('Voucher.Create');
                        },
                    ],
                    [
                        'allow' => true,
                        'actions' => ['change-status'],
                        'matchCallback' => function () {
                            return Yii::$app->user->can('Voucher.Update');
                        },
                    ],
                    [
                        'allow' => true,
                        'actions' => ['delete'],
                        'matchCallback' => function () {
                            return Yii::$app->user->can('Voucher.Delete');
                        },
                    ],
                ],
            ],
        ]);
    }
    
    /**
     * Lists all Voucher models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new VoucherSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new Voucher model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Voucher();
        $model->loadDefaultValues();
        $model->created_user_id = Yii::$app->user->id;

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            for ($i = 1; $i <= $model->quantity; $i++) {
                $newVoucher = clone $model;
                $newVoucher->save();
            }
            
            Yii::$app->session->setFlash('success', 'Thêm thành công.');
            return $this->redirect(['index']);
        }
        
        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Voucher model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model->expiration_date = Yii::$app->formatter->asDate($model->expiration_date);

        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }
        
        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Finds the Voucher model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Voucher the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Voucher::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Lists all Voucher models.
     * @return mixed
     */
    public function actionPrintView()
    {
        $searchModel = new VoucherSearch();
        $dataProvider = $searchModel->searchPrintHtml(Yii::$app->request->queryParams);

        return $this->render('print_view', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionPrintHtml()
    {
        $searchModel = new VoucherSearch();
        $dataProvider = $searchModel->searchPrintHtml(Yii::$app->request->queryParams["query"]);

        return $this->renderPartial('print', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
}
