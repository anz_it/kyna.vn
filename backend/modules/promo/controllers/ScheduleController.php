<?php

namespace app\modules\promo\controllers;

use Yii;
use yii\web\UploadedFile;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\Html;

use app\components\controllers\Controller;
use app\modules\promo\models\form\ScheduleUploadForm;
use kyna\promo\models\PromotionSchedule;
use kyna\promo\models\search\PromotionScheduleSearch;

/**
 * ScheduleController implements the CRUD actions for PromotionSchedule model.
 */
class ScheduleController extends Controller
{

    public $mainTitle = 'Promotion Schedules';
    
    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'view', 'download-sample'],
                        'matchCallback' => function () {
                            return Yii::$app->user->can('PromotionSchedule.View');
                        },
                    ],
                    [
                        'allow' => true,
                        'actions' => ['create', 'upload'],
                        'matchCallback' => function () {
                            return Yii::$app->user->can('PromotionSchedule.Create');
                        },
                    ],
                    [
                        'allow' => true,
                        'actions' => ['update', 'change-status'],
                        'matchCallback' => function () {
                            return Yii::$app->user->can('PromotionSchedule.Update');
                        },
                    ],
                    [
                        'allow' => true,
                        'actions' => ['delete'],
                        'matchCallback' => function () {
                            return Yii::$app->user->can('PromotionSchedule.Delete');
                        },
                    ],
                ],
            ],
        ]);
    }

    /**
     * Lists all PromotionSchedule models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PromotionScheduleSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCreate()
    {
        $model = new PromotionSchedule();
        $model->setScenario('create');
        $model->loadDefaultValues();
        $timeToText =Yii::$app->params['time']['to'];

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $dateArr = explode($timeToText, $model->date_ranger);
            if (count($dateArr) == 2) {
                $dateTimeFormat = str_replace('php:', '', Yii::$app->formatter->datetimeFormat);
                $model->start_time = date_create_from_format($dateTimeFormat, trim($dateArr[0]))->getTimestamp();
                $model->end_time = date_create_from_format($dateTimeFormat, trim($dateArr[1]))->getTimestamp();
            }
            
            if ($model->save()) {
                Yii::$app->session->setFlash('success', 'Thêm thành công.');
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }
        
        return $this->render('create', [
            'model' => $model,
            'timeToText' => $timeToText
        ]);
    }

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model->setScenario('update');
        $timeToText = Yii::$app->params['time']['to'];

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $dateArr = explode($timeToText, $model->date_ranger);
            if (count($dateArr) == 2) {
                $dateTimeFormat = str_replace('php:', '', Yii::$app->formatter->datetimeFormat);
                $model->start_time = date_create_from_format($dateTimeFormat, trim($dateArr[0]))->getTimestamp();
                $model->end_time = date_create_from_format($dateTimeFormat, trim($dateArr[1]))->getTimestamp();
            }
            
            if ($model->save()) {
                Yii::$app->session->setFlash('success', 'Cập nhật thành công.');
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }
        
        return $this->render('update', [
            'model' => $model,
            'timeToText' => $timeToText
        ]);
    }

    protected function findModel($id)
    {
        if (($model = PromotionSchedule::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    public function actionUpload()
    {
        $model = new ScheduleUploadForm();
        
        if ($model->load(Yii::$app->request->post())) {
            $model->file = UploadedFile::getInstance($model, 'file');
            if ($model->validate()) {
                $total = 0;
                $inputFilePath = $model->file->tempName;
                
                try {
                    $inputFileType = \PHPExcel_IOFactory::identify($inputFilePath);
                    $objReader = \PHPExcel_IOFactory::createReader($inputFileType);
                    $objExcel = $objReader->load($inputFilePath);
                } catch (Exception $ex) {
                    Yii::$app->session->setFlash('error', $ex->getMessage());
                    return $this->redirect(['index']);
                }
                
                $sheet = $objExcel->getSheet(0);
                $highestRow = $sheet->getHighestRow();
                $highestColumn = $sheet->getHighestColumn();
                $errors = [];

                for ($row = 2; $row <= $highestRow; $row++) {
                    $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, NULL);

                    if ($schedule = $this->saveExcelRow($rowData, $row, $errors)) {
                        $total++;
                    }
                }

                if ($total) {
                    Yii::$app->session->setFlash('success', "Import thành công $total schedule.");
                } else {
                    Yii::$app->session->setFlash('warning', "Không import được schedule nào.");
                }

                if (!empty($errors)) {
                    $errorTexts = [];
                    foreach ($errors as $row => $rowErrors) {
                        $errorTexts[$row] = "Row $row: " . implode(', ', $rowErrors);
                    }
                    Yii::$app->session->setFlash('warning', "Lỗi dữ liệu: <br>" . Html::ul($errorTexts));
                }
                
                return $this->redirect(['index']);
            }
        }
        
        return $this->render('upload', [
            'model' => $model,
        ]);
    }
    
    private function saveExcelRow($rowData, $row, &$errors = []) 
    {
        if (empty($rowData[0][0])) {
            return false;
        }
        $schedule = new PromotionSchedule();
        
        $schedule->course_id = intval($rowData[0][0]);
        
        if ($startTime = date_create_from_format("d/m/Y H:i", trim($rowData[0][1]))) {
            $schedule->start_time = $startTime->getTimestamp();
        } elseif ($startTime = date_create_from_format("d/m/Y", trim($rowData[0][1]))) {
            $schedule->start_time = $startTime->getTimestamp();
        } else {
            $errors[$row][] = "Sai định dạng Start time '{$rowData[0][1]}'";
        }
        
        if ($endTime = date_create_from_format("d/m/Y H:i", trim($rowData[0][2]))) {
            $schedule->end_time = $endTime->getTimestamp();
        } elseif (($endTime = date_create_from_format("d/m/Y", trim($rowData[0][2])))) {
            $schedule->end_time = $endTime->getTimestamp();
        } else {
            $errors[$row][] = "Sai định dạng End time '{$rowData[0][2]}'";
        }
        
        if (is_null($schedule->course)) {
            $errors[$row][] = "Khóa học với id '{$schedule->course_id}' không tồn tại";
        }        
        
        if (!empty($errors[$row])) {
            return false;
        }
        
        $schedule->price = intval($rowData[0][3]);
        $schedule->discount_amount = $schedule->course->price - $schedule->price;
        $schedule->discount_percent = $schedule->discount_amount * 100 / $schedule->course->price;
        $schedule->image_url = trim($rowData[0][4]);
        $schedule->note = trim($rowData[0][5]);
        
        if ($schedule->save()) {
            return $schedule;
        } else {
            $errArr = [];
            foreach ($schedule->errors as $error) {
                $errArr[] = $error[0];
            }
            $errors[$row][] = implode(', ', $errArr);
        }
        
        return false;
    }
    
    public function actionDownloadSample()
    {
        $filePath = Yii::getAlias('@upload/samples/promotion_temp.xls');
        
        if (!is_file($filePath)) {
            throw new NotFoundHttpException;
        } 
        
        return Yii::$app->response->sendFile($filePath);
    }
}
