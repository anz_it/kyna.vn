<?php

/**
 * @author: Hong Ta
 * @desc: Coupon controller
 */

namespace app\modules\promo\controllers;

use common\helpers\StringHelper;
use kyna\promo\models\CouponCourse;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;

use app\modules\promo\models\CouponSearch;
use kyna\promo\models\Coupon;
use yii\web\Response;

class CouponController extends \app\components\controllers\Controller
{

    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'view'],
                        'matchCallback' => function () {
                            return Yii::$app->user->can('Coupon.View');
                        },
                    ],
                    [
                        'allow' => true,
                        'actions' => ['create'],
                        'matchCallback' => function () {
                            return Yii::$app->user->can('Coupon.Create');
                        },
                    ],
                    [
                        'allow' => true,
                        'actions' => ['update', 'change-status'],
                        'matchCallback' => function () {
                            return Yii::$app->user->can('Coupon.Update');
                        },
                    ],
                    [
                        'allow' => true,
                        'actions' => ['delete'],
                        'matchCallback' => function () {
                            return Yii::$app->user->can('Coupon.Delete');
                        },
                    ],
                ],
            ],
        ]);
    }

    public function actionIndex()
    {
        $searchModel = new CouponSearch();

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        if (Yii::$app->request->isAjax && !empty(Yii::$app->request->post('email'))) {
            // init pagination for raw SQL
            $sql = $dataProvider->query->createCommand()->getRawSql();

            $sql = str_replace('`', '', $sql);
            $email =  Yii::$app->request->post('email');
            $rootPath = \Yii::getAlias('@root');
            $exportLog = \Yii::getAlias('@console/runtime/export-coupon.log');
            $exportErrorLog = \Yii::getAlias('@console/runtime/export-coupon-error.log');
            // check log file exist
            if (!file_exists($exportLog))
                touch($exportLog);
            if (!file_exists($exportErrorLog))
                touch($exportErrorLog);

            $command = "php {$rootPath}/yii export/run coupon \"{$sql}\" {$email}" . " > {$exportLog} 2>>{$exportErrorLog} &";
            exec($command);
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'result' => true,
            ];
        }

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCreate()
    {
        $model = new Coupon();
        $model->apply_for_all = 1;
        if (\Yii::$app->request->isPost) {

            $data = \Yii::$app->request->post();
            $model->load($data);
            $model->created_user_id = Yii::$app->user->id;
            if ($model->validate()) {
                $model->created_user_id = Yii::$app->user->id;
                if ($model->quantity > 1) {
                    $this->insertMulti($model);
                    Yii::$app->session->setFlash('success', 'Thêm thành công');
                    return $this->redirect(['index']);
                } else {
                    if ($model->save(false)) {
                        Yii::$app->session->setFlash('success', 'Thêm thành công');
                        return $this->redirect(['index']);
                    }
                }
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }


    /**
     * @param $model Coupon
     * @return bool
     */
    public function insertMulti($model)
    {
        $model->beforeSave(true);
        $tableName = Coupon::tableName();
        $couponCourseTableName = CouponCourse::tableName();
        $isForAll = $model->apply_for_all ? $model->apply_for_all : 0;
        $partner = $model->partner_id ? $model->partner_id : 0;
        $prefix = empty($model->prefix) ? "" : $model->prefix;
        $session = StringHelper::random(10);
        $createdTime = time();
        $query = "
          INSERT INTO {$tableName} (partner_id, code, start_date, kind, `type`, status, `value`, number_usage, expiration_date, apply_for_all, `session`, created_user_id, created_time )
          SELECT $partner,
          CONCAT('{$prefix}', SUBSTRING(UCASE(MD5(RAND())) FROM 1 FOR 10)),
          '{$model->start_date}',
          {$model->kind},
          {$model->type},
          {$model->status},
          {$model->value},
          '{$model->number_usage}',
          '{$model->expiration_date}',
          {$isForAll},
          '{$session}',
          {$model->created_user_id},
          {$createdTime}
           FROM {$couponCourseTableName} LIMIT {$model->quantity}";

        Yii::$app->db->createCommand($query)->execute();

        if (!$isForAll) {
            foreach ($model->course_id as $courseId) {
                Yii::$app->db->createCommand("INSERT INTO {$couponCourseTableName} (course_id, promotion_id)
                  SELECT {$courseId}, id from {$tableName} where `session` = '{$session}';
                ")->execute();
            }
        }
        return true;
    }

    public function actionViews($id)
    {

    }

    public function actionUpdate($id)
    {
//        $model = $this->findModel($id);
//        $model->start_date = date("M d, Y H:i", $model->start_date);
//        $model->expiration_date = date("M d, Y H:i", $model->expiration_date);
//
//        if(\Yii::$app->request->isPost){
//            $data = \Yii::$app->request->post();
//            $data['Coupon']['start_date'] = strtotime($data['Coupon']['start_date']);
//            $data['Coupon']['expiration_date'] = strtotime($data['Coupon']['expiration_date']);
//            $model->attributes = $data['Coupon'];
////            var_dump($model->validate());die;
//            if($model->validate()){
//                if($model->save())
//                    return $this->redirect(yii\helpers\Url::toRoute('/promo/coupon/index'));
//            }
//        }
//        return $this->render('update',[
//            'model' => $model,
//        ]);
    }

    public function actionDelete($id)
    {
        return parent::actionDelete($id); // TODO: Change the autogenerated stub
    }

    protected function findModel($id)
    {
        if (($model = Coupon::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
