<?php
/**
 * @author: Hong Ta
 * @desc: coupon assets
 */
namespace app\modules\promo\assets;

use yii\web\AssetBundle;

class CouponAsset extends AssetBundle{

    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $js = [
        'js/promotions.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        '\rmrevin\yii\fontawesome\AssetBundle'
    ];
}