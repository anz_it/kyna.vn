<?php

namespace app\modules\promo\models;


use yii;
use yii\base\Model;
use kyna\promo\models\Coupon;
use yii\data\ActiveDataProvider;

class CouponSearch extends Coupon
{
    
    public $teacher_id;
    public $course_id;
    public $kind = self::KIND_COUPON;

    public function rules()
    {
        return [
            [['value', 'type', 'status', 'is_used', 'teacher_id'], 'integer'],
            [['code', 'session', 'created_time', 'start_date', 'expiration_date', 'created_user_id'], 'string'],
            [['course_id'], 'safe'],
            [['code', 'session', 'created_user_id'], 'trim'],
        ];
    }

    /**
     * @return array
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Coupon::find()->with('courses');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'kind' => $this->kind,
            'code' => $this->code,
            'type' => $this->type,
            'value' => $this->value,
            'status' => $this->status,
            'is_used' => $this->is_used,

            'session' => $this->session
        ]);
        if (!empty ($this->created_time)) {
            $date = \DateTime::createFromFormat('d/m/Y', $this->created_time);
            $query->andWhere([
               'date(from_unixtime(created_time))' => $date->format('Y-m-d')
            ]);
        }

        if (!empty ($this->start_date)) {
            $date = \DateTime::createFromFormat('d/m/Y', $this->start_date);
            $query->andWhere([
                'date(from_unixtime(start_date))' => $date->format('Y-m-d')
            ]);
        }
        if (!empty ($this->expiration_date)) {
            $date = \DateTime::createFromFormat('d/m/Y', $this->expiration_date);
            $query->andWhere([
                'date(from_unixtime(expiration_date))' => $date->format('Y-m-d')
            ]);
        }
        if (!empty ($this->created_user_id)) {
            $query->joinWith('createdUser');
            $query->andWhere([
                'like', 'username', $this->created_user_id
            ]);
        }
        if (!empty($this->course_id)) {
//            var_dump($this->course_id);die;
        }
//        $query->andFilterWhere(['like', 'name', $this->name])
//            ->andFilterWhere(['like', 'slug', $this->slug])
//            ->andFilterWhere(['like', 'description', $this->description]);
        
        if (!empty($this->teacher_id)) {
            $query->andWhere(['issued_person' => $this->teacher_id]);
        }

        return $dataProvider;
    }

}
