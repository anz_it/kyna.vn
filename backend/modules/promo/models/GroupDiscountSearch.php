<?php

namespace app\modules\promo\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use kyna\promo\models\GroupDiscount;

/**
 * GroupDiscountSearch represents the model behind the search form about `kyna\promo\models\GroupDiscount`.
 */
class GroupDiscountSearch extends GroupDiscount
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'course_quantity', 'status', 'created_time', 'updated_time'], 'integer'],
            [['name'], 'safe'],
            [['percent_discount'], 'number'],
            [['is_deleted'], 'boolean'],
            ['name', 'trim'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = GroupDiscount::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'percent_discount' => $this->percent_discount,
            'course_quantity' => $this->course_quantity,
            'is_deleted' => $this->is_deleted,
            'status' => $this->status,
            'created_time' => $this->created_time,
            'updated_time' => $this->updated_time,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name]);

        return $dataProvider;
    }
}
