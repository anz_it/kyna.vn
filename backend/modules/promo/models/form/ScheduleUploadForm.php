<?php

namespace app\modules\promo\models\form;

class ScheduleUploadForm extends \yii\base\Model
{
    
    public $file;
    
    public function rules()
    {
        return [
            ['file', 'required'],
            [['file'], 'file', 'skipOnEmpty' => false, 'extensions' => 'xls, xlsx'],
        ];
    }
    
    public function attributeLabels()
    {
        return [
            'file' => 'File',
        ];
    }
}
