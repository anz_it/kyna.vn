<?php
/**
 * @author: Hong Ta
 * @desc: Module handle request for promo module
 */

namespace app\modules\promo;

class Module extends \kyna\base\BaseModule
{
    public $controllerNamespace = 'app\modules\promo\controllers';

    public function init()
    {
        parent::init();
        // custom initialization code goes here
    }
}
