<?php
/**
 * @author: Hong Ta
 * @desc: form update, create coupon
 */
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kyna\promo\models\Coupon;
use kartik\widgets\DateTimePicker;
use kartik\select2\Select2;
use app\modules\promo\assets\CouponAsset;
use kyna\promo\models\Promotion;

CouponAsset::register($this);

$crudTitles = Yii::$app->params['crudTitles'];

?>

<?php $form = ActiveForm::begin(); ?>
    <div class="col-sm-6">

        <?= $form->field($model, 'partner_id')->widget(Select2::className(), [
            'data' => Promotion::getAvailablePartners(), 
            'options' => ['prompt' => $crudTitles['prompt']]
        ]) ?>
        
        <div class="">
            <input type="checkbox" class="generate-code" /> Auto generate code
        </div>
        
        <?= $form->field($model, 'prefix')->textInput((['class' => 'form-control prefix-input', 'disabled' => 'disabled'])) ?>

        <?= $form->field($model, 'code')->textInput(['class' => 'form-control code-input']) ?>

        <?php if ($model->isNewRecord) { ?>
            <?= $form->field($model, 'quantity')->textInput(['class' => 'form-control quantity', 'disabled' => 'disabled', 'value' => 1]) ?>

            <?= $form->field($model, 'start_date')->widget(DateTimePicker::className(), [
                'pluginOptions' => [
                    'autoclose' => true,
                    'format' => 'dd/mm/yyyy hh:ii',
                ]
            ]) ?>

        <?php } else { ?>
            <?= $form->field($model, 'start_date')->widget(DateTimePicker::className(), [
                'pluginOptions' => [
                    'autoclose' => true,
                    'format' => 'dd/mm/yyyy hh:ii',
                ]
            ]) ?>
        <?php } ?>
        <?= $form->field($model, 'status')->dropDownList(Coupon::getStatus()) ?>
    </div>

    <div class="col-sm-6">

        <?= $form->field($model, 'type')->dropDownList(Coupon::getCouponType()) ?>

        <?= $form->field($model, 'value')->textInput() ?>

        <?= $form->field($model, 'number_usage')->textInput() ?>

        <?= $form->field($model, 'expiration_date')->widget(DateTimePicker::className(), [
            'pluginOptions' => [
                'autoclose' => true,
                'format' => 'dd/mm/yyyy hh:ii',
            ]
        ]) ?>
        <?= $form->field($model, 'is_deleted')->hiddenInput()->label(false); ?>
        <?= $form->field($model, 'apply_for_all')->checkbox() ?>
     <div id='couponCourse' style="display:none">
        <?php if ($model->isNewRecord) { ?>
            <?= $form->field($model, 'course_id')->widget(Select2::className(), [
                'data' => $model->getAvailableCourses(),
                'options' => [
                    'id' => 'courses',
                    'multiple' => true
                ],
            ]);
            ?>
        <?php } else { ?>
            <?php
            $modelC = ArrayHelper::map($model->courses, 'id', 'name');
            foreach ($modelC as $k => $val)
                $row[] = $k;

            echo '<label class="control-label" for="coupon-course_id">Courses</label>';
            echo Select2::widget([
                'name' => 'Coupon[course_id]',
                'value' => $row, //
                'data' => $model->getAvailableCourses(),
                'options' => ['placeholder' => 'Select a group ...', 'multiple' => true],
                'pluginOptions' => [
                    'tags' => true,
                    'maximumInputLength' => 10
                ],
            ]);

            ?>
        <?php } ?>
     </div>
    </div>

    <div class="clearfix">
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? $crudTitles['create'] : $crudTitles['update'], ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

<?php ActiveForm::end(); ?>

<?php
/** @var $this \yii\web\View */
$this->registerJs("
 $('#coupon-apply_for_all').change(function (e) {
        
        if ($(this).is(':checked')) {
            $('#couponCourse').hide();
        } else {
            $('#couponCourse').show();
        }
    });
");
?>
