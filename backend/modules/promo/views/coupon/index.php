<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\Pjax;
use kartik\grid\GridView;
use kartik\export\ExportMenu;
use kyna\promo\models\Coupon;

$crudTitles = Yii::$app->params['crudTitles'];
$crudButtonIcons = Yii::$app->params['crudButtonIcons'];

$this->title = 'Coupons';
$this->params['breadcrumbs'][] = $this->title;

$user = Yii::$app->user;
$formatter = \Yii::$app->formatter;
$gridColumns = [
    'id',
    'code',
    [
        'attribute' => 'type',
        'value' => function ($model) {
            return $model->couponText;
        },
    ],
    [
        'attribute' => 'value',
        'value'     => function($model) use ($formatter){
            if($model->type == Coupon::TYPE_PERCENTAGE){
                return $model->value . ' %';
            }else{
                return $formatter->asCurrency($model->value);
            }
        }
    ],
    [
        'attribute' => 'course_id',
        'format' => 'html',
        'value' => function($model){
            $courses_html = null;
            if(!empty($model->courses)){
                foreach($model->courses as $course){
                    $courses_html .= $course->name;
                    $courses_html .= "\n";
                }
            }
            return $courses_html;
        },
    ],
    [
        'attribute' => 'is_used',
        'format' => 'html',
        'value'     => function($model){
            return Html::a($model->isUsed);
        },
    ],
    // 'type',
    'start_date:datetime',
    'expiration_date:datetime',
    [
        'attribute' => 'status',
        'format' => 'html',
        'value' => function ($model) {
            return $model->statusHtml;
        },
    ]
];
?>
    <div class="row">
        <div class="col-sm-12">
            <div class="coupon-index">
                <nav class="navbar navbar-default">
                    <?php
                    if ($user->can('Coupon.Create')) {
                        echo Html::a($crudButtonIcons['create'] . ' ' . $crudTitles['create'], ['create'], ['class' => 'btn btn-success navbar-btn navbar-left']);
                    }

                    ?>
                    <!-- Button trigger modal -->
                    <a class="btn btn-info  navbar-btn navbar-left" data-toggle="modal" data-target="#myModal">
                        <i class="fa fa-file-excel-o"></i> Export excel
                    </a>
                </nav>
            <div class="box">
                <?php Pjax::begin(['id' => 'idPjaxReload', 'timeout' => Yii::$app->params['timeOutPjax']]) ?>
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'export' => [
                        'label' => 'Page',
                        'fontAwesome' => true,
                        'target' => GridView::TARGET_BLANK
                    ],
                    'columns' => [
                        'code',
                        [
                            'attribute' => 'type',
                            'format'  => 'raw',
                            'value' => function($model){
                                return "<b>" . $model->couponText . "</b>";
                            },
                            'filter'    => Html::activeDropDownList($searchModel, 'type', Coupon::getCouponType(), ['class' => 'form-control', 'prompt' => $crudTitles['prompt']]),
                        ],
                        [
                            'attribute' => 'value',
                            'format'    => 'raw',
                            'value'     => function($model) use ($formatter){
                                if($model->type == \kyna\promo\models\Coupon::TYPE_PERCENTAGE){
                                    return "<b>" . $model->value . ' % </b>';
                                }else{
                                    return "<b> " . $formatter->asCurrency($model->value) . "</b>";
                                }
                            }
                        ],
                        [
                            'attribute' => 'created_time',
                            'format' => 'datetime',
                            'filter' =>  \kartik\date\DatePicker::widget([
                                'model' => $searchModel,
                                'attribute' => 'created_time'
                            ])
                        ],
                        [
                            'attribute' => 'start_date',
                            'format' => 'datetime',
                            'filter' =>  \kartik\date\DatePicker::widget([
                                'model' => $searchModel,
                                'attribute' => 'start_date'
                            ])
                        ],
                        [
                            'attribute' => 'expiration_date',
                            'format' => 'datetime',
                            'filter' =>  \kartik\date\DatePicker::widget([
                                'model' => $searchModel,
                                'attribute' => 'expiration_date'
                            ])
                        ],
                        [
                            'header' => 'Có thể sử dụng',
                            'format' => 'html',
                            'value' => function ($model) {
                                if ($model->canUse == Coupon::BOOL_YES) {
                                    return '<span class="label label-success">' . Coupon::BOOL_YES_TEXT . '</span>';
                                } else {
                                    return '<span class="label label-danger">' . Coupon::BOOL_NO_TEXT . '</span>';
                                }
                            }
                        ],
                        'session',
                        [
                            'attribute' => 'created_user_id',
                            'value' => function ($data) {
                                if ($data->createdUser)
                                    return $data->createdUser->username;
                                return $data->created_user_id;
                            }
                        ],
                        [
                            'attribute' => 'status',
                            'format' => 'raw',
                            'value' => function ($model) {
                                return $model->statusButton;
                            },
                            'filter' => Html::activeDropDownList($searchModel, 'status', Coupon::listStatus(false), ['class' => 'form-control', 'prompt' => $crudTitles['prompt']]),
                        ],
                        [
                            'class' => 'app\components\ActionColumnCustom',
                            'template' => '{delete}',
                            'visibleButtons' => [
                                'update' => $user->can('Coupon.Update'),
                                'delete' => $user->can('Coupon.Delete'),
                            ],
                        ],
                    ],
                ]); ?>
                <?php Pjax::end() ?>
            </div>
        </div>
    </div>
</div>


<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">

            <div class="modal-body">
                <form class="form-horizontal" id="exportForm">
                    <div class="form-group" style="padding:10px">
                        <label>Bạn vui lòng cung cấp email: </label>
                        <input id="exportEmail" type="email" value="<?=Yii::$app->user->identity->email?>" class="form-control">
                        <span class="help-block">
                            Hệ thống sẽ chạy export ngầm, sau khi có kết quả sẽ gửi mail tới cho bạn.
                        </span>
                    </div>
                    <button type="submit" class="btn btn-success">Xác nhận</button>
                    <button type="button" data-dismiss="modal" class="btn btn-default">Đóng</button>
                </form>
            </div>

        </div>
    </div>
</div>

<?php
/** @var $this \yii\web\View */
\backend\assets\BootboxAsset::register($this);
$this->registerJs("
    $('#exportForm').submit(function () {
        if (window.location.href.indexOf('?') == -1) {
            bootbox.alert('Bạn vui lòng tìm kiếm trước khi export.');
            return false;
        }
        if (exportEmail.value.trim().length == 0) {
            bootbox.alert('Bạn vui lòng cung cấp email.');
            return false;
        }
        $.ajax({
            url : window.location.href,
            type: 'POST',
            data: {email: exportEmail.value.trim()},
            success: function () {
                $('#myModal').modal('hide');
                bootbox.alert('Hệ thống xử lý xong sẽ gửi mail cho bạn trong vài phút. Vui lòng check mail để xem kết quả');
    
            }
        });
        return false;
    });
");
?>