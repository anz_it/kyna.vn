<?php

use yii\helpers\Html;

$this->title = Yii::$app->params['crudTitles']['update'];
$this->params['breadcrumbs'][] = ['label' => 'Coupon', 'url' => ['index']];

if (!empty($model->parent)) {
    $this->params['breadcrumbs'][] = ['label' => $model->parent->name, 'url' => ['view', 'id' => $model->parent->id]];
}

$this->params['breadcrumbs'][] = $this->title;
?>
<div class="becoupon-update">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>