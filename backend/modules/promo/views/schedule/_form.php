<?php

use yii\web\View;
use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use yii\web\JsExpression;
use yii\helpers\Url;
use yii\widgets\MaskedInput;
use kartik\widgets\Select2;
use kartik\daterange\DateRangePicker;

use kyna\promo\models\PromotionSchedule;

$crudTitles = Yii::$app->params['crudTitles'];
?>

<div class="promotion-schedule-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <?php
            echo $form->field($model, 'course_id', ['options' => ['class' => 'col-xs-4']])->widget(Select2::classname(), [
                'initValueText' =>  empty($model->course_id) ? '' : $model->course->name, // set the initial display text
                'options' => [
                    'placeholder' => $crudTitles['prompt'],
                ],
                'pluginOptions' => [
                    'allowClear' => true,
                    'minimumInputLength' => 3,
                    'language' => [
                        'errorLoading' => new JsExpression("function () { return 'Chờ nhận kết quả...'; }"),
                    ],
                    'ajax' => [
                        'url' => Url::toRoute(['/course/api/search', 'auto' => true]),
                        'dataType' => 'json',
                        'data' => new JsExpression('function(params) { return {q:params.term}; }'),
                    ],
                    'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                    'templateResult' => new JsExpression('function(course) { return course.text; }'),
                    'templateSelection' => new JsExpression('function (course) { if (course.price !== undefined) {$("#old-price").val(course.price); } return course.text; }'),
                ],
            ]);
        ?>

        <?=
        $form->field($model, 'date_ranger', [
            'addon' => ['prepend' => ['content' => '<i class="glyphicon glyphicon-calendar"></i>']],
            'options' => ['class' => 'col-xs-4 drp-container form-group']
        ])->widget(DateRangePicker::classname(), [
            'useWithAddon' => true,
            'convertFormat' => true,
            'readonly' => true,
            'pluginOptions'=>[
                'timePicker' => true,
                'locale'=>[
                    'format' => 'd/m/yy G:i',
                    'separator'=> " $timeToText ", // after change this, must update in controller
                ],
            ]
        ])
        ?>
        
        <?= $form->field($model, 'old_price', ['options' => ['class' => 'col-xs-4']])->textInput(['readonly' => true, 'id' => 'old-price']) ?>
    </div>

    <div class="row">
        <?= $form->field($model, 'discount_percent', ['options' => ['class' => 'col-xs-4']])->widget(MaskedInput::className(), [
            'clientOptions' => [
                'alias' =>  'decimal',
            ],
            'options' => [
                'data-field' => 'discount_percent',
                'class' => 'form-control',
                'data-cal' => ''
            ]
        ]) ?>

        <?= $form->field($model, 'discount_amount', ['options' => ['class' => 'col-xs-4']])->widget(MaskedInput::className(), [
            'clientOptions' => [
                'alias' =>  'integer',
            ],
            'options' => [
                'data-field' => 'discount_amount',
                'class' => 'form-control',
                'data-cal' => ''
            ]
        ]) ?>

        <?= $form->field($model, 'price', ['options' => ['class' => 'col-xs-4']])->widget(MaskedInput::className(), [
            'clientOptions' => [
                'alias' =>  'integer',
            ],
            'options' => [
                'data-field' => 'price',
                'class' => 'form-control',
                'data-cal' => ''
            ]
        ]) ?>
    </div>

    <div class="row">
        <?= $form->field($model, 'note', ['options' => ['class' => 'col-xs-5']])->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'image_url', ['options' => ['class' => 'col-xs-5']])->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'status', ['options' => ['class' => 'col-xs-2']])->widget(Select2::classname(), [
            'data' => PromotionSchedule::listStatus(),
            'hideSearch' => true,
            'options' => [
                'placeholder' => $crudTitles['prompt'],
            ],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]) ?>
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? $crudTitles['create'] : $crudTitles['update'], ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::a($crudTitles['cancel'], ['index'], ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php
$script = <<< SCRIPT
;(function($) {
    $('body').on('change', 'input[data-cal]', function () {
        var field = $(this).data('field'),
            oldPrice = $('#old-price').val(),
            value = $(this).val();
        
        var discountPercentElement = $('input[data-field=\'discount_percent\']'),
            discountAmountElement = $('input[data-field=\'discount_amount\']'),
            priceElement = $('input[data-field=\'price\']');
        
        switch (field) {
            case 'discount_percent':
                var discountAmount = oldPrice * value / 100;
                discountAmountElement.val(discountAmount);
                priceElement.val(oldPrice - discountAmount);
        
                break;
        
            case 'discount_amount':
                discountPercentElement.val(value / oldPrice * 100);
                priceElement.val(oldPrice - value);
        
                break;
        
            case 'price':
                var discountAmount = oldPrice - value;
                discountAmountElement.val(discountAmount);
                discountPercentElement.val(discountAmount / oldPrice * 100);
        
                break;
        
            default:
                break;
        }
    });
})(jQuery);
SCRIPT;

$this->registerJs($script, View::POS_END, 'update-price');
?>
