<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\grid\GridView;
use yii\web\View;
use kartik\widgets\Select2;
use yii\web\JsExpression;
use yii\widgets\ActiveForm;

use kyna\promo\models\PromotionSchedule;
use app\modules\promo\models\form\ScheduleUploadForm;

$crudTitles = Yii::$app->params['crudTitles'];
$crudButtonIcons = Yii::$app->params['crudButtonIcons'];

$this->title = Yii::$app->controller->mainTitle;
$this->params['breadcrumbs'][] = $this->title;

$user = Yii::$app->user;
$model = new ScheduleUploadForm();
?>
<div class="row">
    <div class="col-xs-12">
        <div class="promotion-schedule-index">
            <?php if ($user->can('PromotionSchedule.Create')) : ?>
                <nav class="navbar navbar-default">
                    <?= Html::a($crudButtonIcons['create'] . ' ' . $crudTitles['create'], ['create'], ['class' => 'btn btn-success navbar-btn navbar-left']) ?>

                    <?php $form = ActiveForm::begin([
                        'options' => [
                            'enctype' => 'multipart/form-data',
                            'class' => 'navbar-form navbar-left'
                        ],
                        'action' => Url::toRoute(['upload'])
                    ]) ?>

                        <?= $form->field($model, 'file', [
                            'template' => '<label class="btn btn-default" for="schedule-upload"><i class="fa fa-upload fa-fw"></i> Nhập từ file excel {input}</label>',
                        ])->fileInput([
                            'id' => 'schedule-upload',
                            'class' => 'form-control hide',
                        ]) ?>

                    <?php ActiveForm::end() ?>

                    <p class="navbar-text navbar-left">(File mẫu: <a href="<?= Url::toRoute(['download-sample']) ?>" class="navbar-link"><i class="fa fa-download"></i>Tải về</a>, 
                        export danh sách khóa học tại : <?= Html::a('<i class="fa fa-hand-o-right"></i> đây', Url::toRoute(['/course/default/index']), ['class' => "navbar-link", 'target' => '_blank']) ?>)</p>
                </nav>
            <?php endif; ?>

            <div class="box">
                <?php Pjax::begin(['timeout' => Yii::$app->params['timeOutPjax'], 'id' => 'idPjaxReload']) ?>
                <?=
                GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],
                        [
                            'attribute' => 'id',
                            'options' => ['class' => 'col-xs-1'],
                        ],
                        [
                            'attribute' => 'course_id',
                            'value' => function ($model) {return $model->course->name; },
                            'options' => ['class' => 'col-xs-3'],
                            'filter' => Select2::widget([
                                'model' => $searchModel,
                                'initValueText' => !empty($searchModel->course) ? $searchModel->course->name : '',
                                'attribute' => 'course_id',
                                'options' => ['placeholder' => $crudTitles['prompt']],
                                'pluginOptions' => [
                                    'allowClear' => true,
                                    'minimumInputLength' => 3,
                                    'language' => [
                                        'errorLoading' => new JsExpression("function () { return 'Chờ nhận kết quả...'; }"),
                                    ],
                                    'ajax' => [
                                        'url' => Url::toRoute(['/course/api/search', 'auto' => true]),
                                        'dataType' => 'json',
                                    ],
                                ],
                            ])
                        ],
                        'price:currency',
                        [
                            'attribute' => 'start_time',
                            'format' => 'datetime',
                            'value' => function ($model) {
                                return !empty($model->start_time) ? $model->start_time : null;
                            },
                            'filter' => false
                        ],
                        [
                            'attribute' => 'end_time',
                            'format' => 'datetime',
                            'value' => function ($model) {
                                return !empty($model->end_time) ? $model->end_time : null;
                            },
                            'filter' => false
                        ],
                        'reg_count',
                        [
                            'attribute' => 'created_time',
                            'format' => 'datetime',
                            'filter' => false
                        ],
                        [
                            'attribute' => 'status',
                            'value' => function ($model) {
                                return $model->statusButton;
                            },
                            'format' => 'raw',
                            'filter' => Select2::widget([
                                'model' => $searchModel,
                                'attribute' => 'status',
                                'data' => PromotionSchedule::listStatus(false),
                                'options' => ['placeholder' => $crudTitles['prompt']],
                                'pluginOptions' => [
                                    'allowClear' => true
                                ],
                                'hideSearch' => true,
                            ])
                        ],
                        [
                            'class' => 'app\components\ActionColumnCustom',
                            'visibleButtons' => [
                                'update' => $user->can('PromotionSchedule.Update'),
                                'view' => $user->can('PromotionSchedule.View'),
                                'delete' => $user->can('PromotionSchedule.Delete'),
                            ],
                        ],
                    ],
                ]);
                ?>
                <?php Pjax::end() ?>
            </div>
        </div>
    </div>
</div>

<?php
if ($user->can('PromotionSchedule.Create')) {
    $js = <<<JS
;(function($) {
    $("#schedule-upload").on("change", function(e) {
        $("#$form->id").submit();
    });
})(jQuery);
JS;

    $this->registerJs($js, View::POS_END);
}
