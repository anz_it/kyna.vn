<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model kyna\promo\models\GroupDiscount */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::$app->controller->mainTitle, 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$crudTitles = Yii::$app->params['crudTitles'];
?>
<div class="group-discount-view">

    <p>
        <?= Html::a("<span class='glyphicon glyphicon-backward'></span> " . $crudTitles['back'], ['index'], ['class' => 'btn btn-info', 'icon' => 'glyphicon glyphicon-backward']) ?>
        <?= Html::a($crudTitles['update'], ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a($crudTitles['delete'], ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'percent_discount',
            'course_quantity',
            [
                'attribute'=>'type',
                'value'=>$model->types[$model->type],
            ],
            [
                'attribute'=>'is_frontend',
                'value'=>$model->listStatus()[$model->is_frontend],
            ],
            'is_deleted:boolean',
            [
                'attribute'=>'status',
                'value'=>$model->listStatus()[$model->status],
            ],
            'created_time:datetime',
            'updated_time:datetime',
        ],
    ]) ?>

</div>
