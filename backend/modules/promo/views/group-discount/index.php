<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Url;
use kyna\promo\models\GroupDiscount;

$crudTitles = Yii::$app->params['crudTitles'];

$this->title = Yii::$app->controller->mainTitle;
$this->params['breadcrumbs'][] = $this->title;

$user = Yii::$app->user;
?>
<div class="group-discount-index">
    <p>
        <?php
        if ($user->can('GroupDiscount.Create')) {
            echo Html::a($crudTitles['create'], ['create'], ['class' => 'btn btn-success']);
        }
        ?>
    </p>
    <div class="box">
        <?php Pjax::begin(['timeout' => Yii::$app->params['timeOutPjax'], 'id' => 'idPjaxReload']) ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                'id',
                'name',
                'percent_discount',
                [
                    'attribute' => 'type',
                    'format' => 'raw',
                    'value' => function ($model) {
                        $list = $model->types;
                        return $list[$model->type];
                    }
                ],
                'course_quantity',
                'max_discount_amount:currency',

                [
                    'attribute' => 'is_frontend',
                    'format' => 'raw',
                    'value' => function ($model) {
                        $list = $model->listStatus();
                        return $list[$model->is_frontend];
                    }
                ],
                [
                    'attribute' => 'status',
                    'format' => 'raw',
                    'value' => function ($model) {
                        return $model->statusButton;
                    },
                    'filter' => Html::activeDropDownList($searchModel, 'status', GroupDiscount::listStatus(false), ['class' => 'form-control', 'prompt' => $crudTitles['prompt']]),
                ],
                [
                    'class' => 'app\components\ActionColumnCustom',
                    'visibleButtons' => [
                        'update' => $user->can('GroupDiscount.Update'),
                        'view' => $user->can('GroupDiscount.View'),
                        'delete' => $user->can('GroupDiscount.Delete'),
                    ],
                ],
            ],
        ]); ?>
        <?php Pjax::end() ?>
    </div>
</div>
