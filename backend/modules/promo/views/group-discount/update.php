<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model kyna\promo\models\GroupDiscount */

$this->title = Yii::$app->params['crudTitles']['update'];
$this->params['breadcrumbs'][] = ['label' => Yii::$app->controller->mainTitle, 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="group-discount-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
