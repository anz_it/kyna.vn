<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$crudTitles = Yii::$app->params['crudTitles'];
?>

<div class="group-discount-form">

    <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'percent_discount')->textInput() ?>

        <?= $form->field($model, 'type')->dropDownList($model->getTypes(),['prompt'=>'- Choose discount type -']) ?>

        <?= $form->field($model, 'course_quantity')->textInput() ?>

        <?= $form->field($model, 'max_discount_amount')->textInput() ?>

        <?= $form->field($model, 'is_frontend')->dropDownList(\kyna\promo\models\GroupDiscount::listStatus(), ['prompt' => $crudTitles['prompt']]) ?>

        <?= $form->field($model, 'status')->dropDownList(\kyna\promo\models\GroupDiscount::listStatus(), ['prompt' => $crudTitles['prompt']]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? $crudTitles['create'] : $crudTitles['update'], ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::a($crudTitles['cancel'], ['index'],['class' => 'btn btn-default']) ?>
    </div>
    <?php ActiveForm::end(); ?>

</div>
