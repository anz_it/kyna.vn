<?php

use yii\web\View;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\DateTimePicker;
use yii\widgets\MaskedInput;
use kartik\select2\Select2;
use kyna\promo\models\Promotion;

$crudTitles = Yii::$app->params['crudTitles'];
?>

<div class="voucher-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="col-sm-6">
        <?= $form->field($model, 'value')->widget(MaskedInput::className(), [
            'clientOptions' => [
                'alias' =>  'integer',
            ],
        ]) ?>
        
        <?= $form->field($model, 'auto')->checkbox() ?>
        
        <?= $form->field($model, 'quantity')->textInput(['readonly' => 'readonly']) ?>

        <?= $form->field($model, 'prefix')->textInput(['readonly' => 'readonly']) ?>

        <?= $form->field($model, 'code')->textInput(['maxlength' => true]) ?>
    </div>
    
    <div class="col-sm-6">
        <?= $form->field($model, 'partner_id')->widget(Select2::className(), [
            'data' => Promotion::getAvailablePartners(), 
            'options' => ['prompt' => $crudTitles['prompt']]
        ]) ?>
        
        <?= $form->field($model, 'note')->textInput(['maxlength' => true]) ?>
        

        <?= $form->field($model, 'min_amount')->widget(MaskedInput::className(), [
            'clientOptions' => [
                'alias' =>  'integer',
            ],
        ]) ?>
        
        <?= $form->field($model, 'expiration_date')->widget(DateTimePicker::className(), [
                'pluginOptions' => [
                    'autoclose'=>true,
                    'format' => 'dd/mm/yyyy hh:ii',
                    'startDate' => date('d/m/Y 00:00')
                ]
            ]) ?>
        <?=$form->field($model, 'number_usage')->textInput() ?>
    </div>
    
     <div class="clearfix">
    </div>
    
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? $crudTitles['create'] : $crudTitles['update'], ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::a($crudTitles['cancel'], ['index'], ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<?php
$script = <<< SCRIPT
$(document).ready(function(){
    $('body').on('change', '#voucher-auto', function(){
        var checked = $(this).prop("checked");
        if (checked == true) {
            $('#voucher-quantity').removeAttr('readonly');
            $('#voucher-prefix').removeAttr('readonly');
        
            $('#voucher-code').attr('readonly', 'readonly');
        } else {
            $('#voucher-quantity').val(1);
            $('#voucher-prefix').val('');
        
            $('#voucher-quantity').attr('readonly', 'readonly');
            $('#voucher-prefix').attr('readonly', 'readonly');
        
            $('#voucher-code').removeAttr('readonly');
        }
    });
});
SCRIPT;

$this->registerJs($script, View::POS_END, 'change-override-commission');
?>