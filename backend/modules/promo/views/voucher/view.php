<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::$app->controller->mainTitle, 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$crudTitles = Yii::$app->params['crudTitles'];
?>
<div class="voucher-view">

    <p>
        <?= Html::a("<span class='glyphicon glyphicon-backward'></span> " . $crudTitles['back'], ['index'], ['class' => 'btn btn-info', 'icon' => 'glyphicon glyphicon-backward']) ?>
        
        <?php
        if (Yii::$app->user->can('Voucher.Delete')) {
            echo Html::a($crudTitles['delete'], ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ]);
        }
        ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'code',
            'value:currency',
            'min_amount:currency',
            [
                'attribute' => 'expiration_date',
                'value' => (!empty($model->expiration_date) ? Yii::$app->formatter->asDatetime($model->expiration_date) : null)
            ],
            'number_usage',
            'current_number_usage',
            [
                'attribute' => 'created_user_id',
                'value' => $model->createdUser->profile->name,
            ],
            'note',
            'is_deleted:boolean',
            'created_time:datetime',
            'updated_time:datetime',
        ],
    ]) ?>

</div>
