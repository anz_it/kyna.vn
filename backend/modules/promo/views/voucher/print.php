<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<link href='https://fonts.googleapis.com/css?family=Roboto:400,500,700,300' rel='stylesheet' type='text/css'>

<style type="text/css">



    .SA4{
        margin: 0 auto;
        background-color: #fff;
        position: relative}

    .magiamgia {
        width: 203px;
        height: 43px;
        position: absolute;
        top: 148px;
        left: 170px;
        text-align: center;
        font-size: 18px;
        color: red;
        line-height: 45px;
        font-weight: bold;
        text-transform: uppercase;
    }
    .ngay, .thang {
        width: 30px;
        height: 30px;
        position: relative;
        line-height: 30px;
        text-align: center;
    }
    .time {
        position: absolute;
        top: 196px;
        left: 220px;
        display: inline-block;
    }

    .wrap-n .magiamgia {
        width: 203px;
        height: 43px;
        position: absolute;
        top: 186px;
        left: 170px;
        text-align: center;
        font-size: 18px;
        color: red;
        line-height: 45px;
        font-weight: bold;
        text-transform: uppercase;
    }
    .wrap-n .ngay, .wrap-n .thang {
        width: 30px;
        height: 30px;
        position: relative;
        line-height: 30px;
        text-align: center;
    }
    .wrap-n .time {
        position: absolute;
        top: 234px;
        left: 220px;
        display: inline-block;
    }

    .time span {
        margin-right: 8px;
        display: inline-block
    }

    @media print {
        @page { margin: 0; size: A4}
        .SA4{width: 995px; height: 335px;}
        body { margin: 1cm; }
    }
    @media screen {
        .SA4{width: 995px; height: 335px;  box-shadow: 1px 2px 2px #333; margin-bottom: 10px}

    }

    .SA4{page-break-after: always}
</style>
<body>
<?php
 foreach ($dataProvider->getModels() as $key => $model) {
    ?>
     <div class="SA4 <?= ($key != 0)?'wrap-n':''?>">
         <div class="magiamgia"><?=$model->code?></div>
         <div class="time">
             <span class="ngay"><?= date('d', $model->expiration_date) ?></span>
             <span class="thang"><?= date('m', $model->expiration_date) ?></span>
         </div>
     </div><!--end .SA4-->
<?php
 }
?>
<script type="text/javascript">
    (function(){
        window.print();
    })();
</script>
</body>
</html>