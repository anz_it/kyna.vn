<?php

namespace app\modules\user\controllers;

use Yii;
use yii\helpers\Url;
use dektrium\user\controllers\AdminController as BaseAdminController;
use kyna\user\models\search\UserSearch;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use dektrium\user\filters\AccessRule;
use app\modules\user\models\User;

class AdminController extends BaseAdminController
{
    
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete'  => ['post'],
                    'confirm' => ['post'],
                    'block'   => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'ruleConfig' => [
                    'class' => AccessRule::className(),
                ],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'info'],
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->user->can('User.View');
                        },
                    ],
                    [
                        'allow' => true,
                        'actions' => ['create'],
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->user->can('User.Create');
                        },
                    ],
                    [
                        'allow' => true,
                        'actions' => ['update', 'update-profile'],
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->user->can('User.Update');
                        },
                    ],
                    [
                        'allow' => true,
                        'actions' => ['assignments', 'choose-leader'],
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->user->can('User.Assign');
                        },
                    ],
                    [
                        'allow' => true,
                        'actions' => ['confirm'],
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->user->can('User.Confirm');
                        },
                    ],
                    [
                        'allow' => true,
                        'actions' => ['block'],
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->user->can('User.Block');
                        },
                    ],
                    [
                        'allow' => true,
                        'actions' => ['delete'],
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->user->can('User.Delete');
                        },
                    ],
                ],
            ],
        ];
    }
    
    /**
     * Lists all User models.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        Url::remember('', 'actions-redirect');
        $searchModel  = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->get());

        return $this->render('@app/modules/user/views/admin/index', [
            'dataProvider' => $dataProvider,
            'searchModel'  => $searchModel,
        ]);
    }
    
    public function actionCreate()
    {
        /** @var User $user */
        $user = Yii::createObject([
            'class'    => User::className(),
            'scenario' => 'create',
        ]);
        $event = $this->getUserEvent($user);

        $this->performAjaxValidation($user);

        $this->trigger(self::EVENT_BEFORE_CREATE, $event);
        if ($user->load(Yii::$app->request->post()) && $user->create()) {
            Yii::$app->getSession()->setFlash('success', Yii::t('user', 'User has been created'));
            $this->trigger(self::EVENT_AFTER_CREATE, $event);
            return $this->redirect(['update', 'id' => $user->id]);
        } 

        return $this->render('create', [
            'user' => $user,
        ]);
    }
    
    public function actionUpdate($id)
    {
        Url::remember('', 'actions-redirect');
        $user = $this->findModel($id);
        $user->scenario = 'update';
        $event = $this->getUserEvent($user);

        $this->performAjaxValidation($user);

        $this->trigger(self::EVENT_BEFORE_UPDATE, $event);
        if ($user->load(\Yii::$app->request->post()) && $user->save()) {
            \Yii::$app->getSession()->setFlash('success', \Yii::t('user', 'Account details have been updated'));
            $this->trigger(self::EVENT_AFTER_UPDATE, $event);
            return $this->refresh();
        }

        return $this->render('@app/modules/user/views/admin/_account', [
            'user' => $user,
        ]);
    }
    
    public function actionUpdateProfile($id)
    {
        Url::remember('', 'actions-redirect');
        $user    = $this->findModel($id);
        $profile = $user->profile;

        if ($profile == null) {
            $profile = \Yii::createObject(Profile::className());
            $profile->link('user', $user);
        }
        $event   = $this->getProfileEvent($profile);

        $this->performAjaxValidation($profile);

        $this->trigger(self::EVENT_BEFORE_PROFILE_UPDATE, $event);

        if ($profile->load(\Yii::$app->request->post()) && $profile->save()) {
            \Yii::$app->getSession()->setFlash('success', \Yii::t('user', 'Profile details have been updated'));
            $this->trigger(self::EVENT_AFTER_PROFILE_UPDATE, $event);
            return $this->refresh();
        }

        return $this->render('@app/modules/user/views/admin/_profile', [
            'user'    => $user,
            'profile' => $profile,
        ]);
    }
    
    public function actionAssignments($id)
    {
        if (!isset(\Yii::$app->extensions['dektrium/yii2-rbac'])) {
            throw new NotFoundHttpException();
        }
        Url::remember('', 'actions-redirect');
        $user = $this->findModel($id);

        return $this->render('@app/modules/user/views/admin/_assignments', [
            'user' => $user,
        ]);
    }
    
    public function actionInfo($id)
    {
        Url::remember('', 'actions-redirect');
        $user = $this->findModel($id);

        return $this->render('@app/modules/user/views/admin/_info', [
            'user' => $user,
        ]);
    }
    
    public function actionChooseLeader($id)
    {
        Url::remember('', 'actions-redirect');
        $user = $this->findModel($id);
        $user->scenario = 'update';

        $this->performAjaxValidation($user);
        
        if (Yii::$app->request->isPost && $post = Yii::$app->request->post()) {
            $user->manager_id = $post['User']['manager_id'];
            if ($user->save()) {
                Yii::$app->getSession()->setFlash('success', \Yii::t('user', 'Cập nhật thành công Người quản lý'));
                return $this->refresh();
            }
        }

        return $this->render('@app/modules/user/views/admin/_choose_leader', [
            'user' => $user,
        ]);
    }
}
