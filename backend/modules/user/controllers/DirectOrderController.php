<?php

namespace app\modules\user\controllers;

use app\modules\order\models\OrderSearch;

class DirectOrderController extends \app\modules\order\controllers\DefaultController
{
    
    public $mainTitle = 'User Care Management';
    public $userCartTabs = [];
    
    public function init()
    {
        $ret = parent::init();
        
        $this->setViewPath('@app/modules/order/views/default');
        
        $this->userCartTabs = [
            [
                'label' => 'Danh sách User Care',
                'url' => ['/user/user-telesale/index'],
                'active' => false,
            ],
            [
                'label' => 'Đơn hàng trực tiếp',
                'url' => ['/user/direct-order/index'],
                'active' => $this->id == 'direct-order',
            ]
        ];
        
        return $ret;
    }
    
    public function loadSearchModel()
    {
        $searchModel = new OrderSearch();
        $searchModel->is_done_telesale_process = OrderSearch::BOOL_NO;
        
        return $searchModel;
    }
}