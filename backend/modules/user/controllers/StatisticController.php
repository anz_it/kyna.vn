<?php
namespace app\modules\user\controllers;
use app\modules\user\models\UserStatisticSearch;
use Yii;
use yii\helpers\Url;

/**
 * Created by PhpStorm.
 * User: Hien Nguyen
 * Date: 8/25/2016
 * Time: 10:11 AM
 */

class StatisticController extends \app\components\controllers\Controller {

    public function init()
    {
        parent::init();
        $this->setViewPath('@app/modules/user/views/statistic');
    }

    public function actionByYear() {
        $searchModel = new UserStatisticSearch();
        $searchModel->load(Yii::$app->request->queryParams);
        if (empty($searchModel->year)) {
            $searchModel->year = date('Y', time());
        }
        $listRegistrationCount = $this->sortData($searchModel->getStatistic());
        $listRegistrationConfirmCount = $this->sortData($searchModel->getStatisticWithConfirmEmail());
        $listOrderCount = $this->sortData($searchModel->getOrderStatistic());
        $listOrderIsPaidCount = $this->sortData($searchModel->getOrderIsPaidStatistic());
        $listOrderDistinctCount = $this->sortData($searchModel->getOrderDistinctStatistic());
        return $this->render('by_year', [
            'listRegistrationCount' => $listRegistrationCount,
            'listRegistrationConfirmCount' => $listRegistrationConfirmCount,
            'listOrderCount' => $listOrderCount,
            'listOrderIsPaidCount' => $listOrderIsPaidCount,
            'listOrderDistinctCount' => $listOrderDistinctCount,
            'searchModel' => $searchModel,
            'tabs' => $this->loadTabs(),
        ]);
    }

    protected function sortData($data) {
        $result = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
        if ($data) {
            foreach ($data as $key => $value) {
                $result[$value->month - 1] += $value->total;
            }
        }

        return $result;
    }

    public function actionByDate() {
        $searchModel = new UserStatisticSearch();
        $searchModel->load(Yii::$app->request->queryParams);
        if (empty($searchModel->year)) {
            $searchModel->year = date('Y', time());
        }
        $registrationCount = $searchModel->getStatisticByDate();
        $registrationConfirmCount = $searchModel->getStatisticWithConfirmEmailByDate();
        $orderCount = $searchModel->getOrderStatisticByDate();
        $orderIsPaidCount = $searchModel->getOrderIsPaidStatisticByDate();
        $orderDistinctCount = $searchModel->getOrderDistinctStatisticByDate();
        return $this->render('by_date', [
            'registrationCount' => $registrationCount,
            'registrationConfirmCount' => $registrationConfirmCount,
            'orderCount' => $orderCount,
            'orderIsPaidCount' => $orderIsPaidCount,
            'orderDistinctCount' => $orderDistinctCount,
            'searchModel' => $searchModel,
            'tabs' => $this->loadTabs(),
        ]);
    }

    private function loadTabs()
    {
        $tabsItems = [
            [
                'label' => 'Theo năm',
                'url' => Url::toRoute(['/user/statistic/by-year']),
                'active' => $this->action->id == 'by-year',
                'encode' => false,
            ],
            [
                'label' => 'Theo ngày',
                'url' => Url::toRoute(['/user/statistic/by-date']),
                'active' => $this->action->id == 'by-date',
                'encode' => false,
            ],
            [
                'label' => 'Retention Rate',
                'url' => Url::toRoute(['/user/retention-rate/index']),
                'active' => false,
                'encode' => false,
            ],
        ];

        return $tabsItems;
    }
}