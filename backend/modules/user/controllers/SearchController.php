<?php

namespace app\modules\user\controllers;

use Yii;
use yii\web\Controller;
use yii\web\Response;
use yii\helpers\ArrayHelper;
use dektrium\user\Finder;
use kyna\user\models\UserSearch;
use kyna\user\models\Address;
use kyna\user\models\User;
use common\helpers\StringHelper;

//use dektrium\user\controllers\AdminController as BaseAdminController;

class SearchController extends Controller
{

    protected $finder;

    public function __construct($id, $module, Finder $finder, $config = [])
    {
        $this->finder = $finder;
        parent::__construct($id, $module, $config);
    }

    public function actionAddress($s = false)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        if (!$s) {
            return;
        }

        $searchModel = Yii::createObject(UserSearch::className());
        $params = [];
        $params = Yii::$app->request->queryParams;

        $string = isset($params['s']) ? $params['s'] : '';
        unset($params['s']);

        $stringType = StringHelper::typeDetect($string);
        switch ($stringType) {
            case StringHelper::STRING_TYPE_EMAIL:
                $params['email'] = $string;
                break;

            default:
        }

        $users = $searchModel->search($params)->getModels();

        return ArrayHelper::toArray($users, [
                    User::className() => [
                        'id',
                        'email',
                        'phoneNumber' => 'userAddress.phone_number',
                        'contactName' => 'userAddress.contact_name',
                        'streetAddress' => 'userAddress.street_address',
                        'district' => 'userAddress.locations.0.name',
                        'province' => 'userAddress.locations.1.name',
                        'locationId' => 'userAddress.locations.0.id',
                    ],
        ]);
    }

}
