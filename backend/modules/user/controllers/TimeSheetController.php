<?php

namespace app\modules\user\controllers;

use Yii;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

use kyna\user\models\TimeSlot;
use kyna\user\models\Operator;
use app\modules\user\models\TimeSheet;

/**
 * TimeSheetController implements the CRUD actions for TimeSheet model.
 */
class TimeSheetController extends \app\components\controllers\Controller
{

    public function init()
    {
        parent::init();
        $this->setViewPath('@app/modules/user/views/time-sheet');
    }

    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'view'],
                        'matchCallback' => function () {
                            return Yii::$app->user->can('TimeSlot.View');
                        },
                    ],
                    [
                        'allow' => true,
                        'actions' => ['create'],
                        'matchCallback' => function () {
                            return Yii::$app->user->can('TimeSlot.Create');
                        },
                    ],
                    [
                        'allow' => true,
                        'actions' => ['update', 'update-operators'],
                        'matchCallback' => function () {
                            return Yii::$app->user->can('TimeSlot.Update');
                        },
                    ],
                    [
                        'allow' => true,
                        'actions' => ['delete'],
                        'matchCallback' => function () {
                            return Yii::$app->user->can('TimeSlot.Delete');
                        },
                    ],
                ],
            ],
        ]);
    }

    /**
     * Lists all TimeSheet models.
     * @return mixed
     */
    public function actionIndex()
    {
        $query = TimeSlot::find();
        $where = array_filter(Yii::$app->request->get());
        $query->andFilterwhere($where);
        $timeSheet = $query->orderBy([
                    'start_time' => 'asc',
                ])->all();

        $roles = ArrayHelper::map(Yii::$app->authManager->roles, 'name', 'name');
        $roles = ['' => 'Tất cả các team'] + $roles;

        return $this->render('index', [
                    'timeSheet' => $timeSheet,
                    'dow' => TimeSlot::getDaysOfWeek(),
                    'rowNumber' => sizeof($timeSheet),
                    'types' => $roles,
                    'userTypes' => TimeSlot::getUserTypes(),
        ]);
    }

    /**
     * Creates a new TimeSheet model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new TimeSlot();

        $model->type = Yii::$app->request->get('type');
        $model->user_type = Yii::$app->request->get('user_type');

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $this->redirect([
                '/user/time-sheet',
                'type' => $model->type,
                'user_type' => $model->user_type,
            ]);
        }

        return $this->renderAjax('forms/_timeslot-form', [
                    'model' => $model,
        ]);
    }

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->renderAjax('forms/_timeslot-view', [
                        'timeSlot' => $model,
            ]);
        }

        return $this->renderAjax('forms/_timeslot-form', [
                    'model' => $model,
        ]);
    }

    public function actionUpdateOperators($id, $day)
    {
        $model = $this->findModel($id);

        $role = $model->type;
        $operators = Operator::getUsersByRole($role);
        $operators = ArrayHelper::map($operators, 'id', 'profile.name');
        
        if ($post = Yii::$app->request->post()) {
            $model->load($post);
            if ($model->load($post) AND $model->save()) {
                return $this->renderAjax('forms/_operators-view', [
                            'timeSlot' => $model,
                            'day' => $day,
                ]);
            }
        }

        return $this->renderAjax('forms/_operators-update', [
                    'model' => $model,
                    'operators' => $operators,
                    'day' => $day,
        ]);
    }

    /**
     * Deletes an existing TimeSheet model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $url = [
            'index',
            'type' => $model->type,
            'user_type' => $model->user_type,
        ];
        $model->delete();

        return $this->redirect($url);
    }

    /**
     * Finds the TimeSheet model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return TimeSheet the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = TimeSlot::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
