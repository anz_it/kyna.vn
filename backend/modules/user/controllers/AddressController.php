<?php

namespace app\modules\user\controllers;

use Yii;
use kyna\user\models\Address;
use modules\user\shared\Location;
use modules\user\models\User;
use common\helpers\TreeHelper;
use yii\data\ActiveDataProvider;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * UserAddressController implements the CRUD actions for UserAddress model.
 */
class AddressController extends \app\components\controllers\Controller
{

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all UserAddress models.
     * @return mixed
     */
    public function actionIndex($userId = null)
    {
        $user = User::findOne($userId);

        if (($user = User::findOne($userId)) === null) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        if (!($model = $this->findModel($userId))) {
            $model = new UserAddress();
        }

        $locModels = Location::find()->all(); //->select(['id', 'name', 'parent_id'])->asArray()->all();
        //var_dump($locModels); die;
        $options = TreeHelper::dropdownOptions($locModels);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index', 'userId' => $userId]);
        } else {
            return $this->render('create', [
                        'model' => $model,
                        'user' => $user,
                        'options' => $options,
            ]);
        }
    }

    /**
     * Finds the UserAddress model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return UserAddress the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = UserAddress::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
