<?php

namespace app\modules\user\controllers;

use common\helpers\ArrayHelper;
use common\helpers\FbOfflineConversionHelper;
use Yii;
use yii\base\Exception;
use yii\db\Query;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;
use yii\web\Response;
use yii\helpers\Json;
use yii\filters\AccessControl;
use yii\widgets\ActiveForm;

use app\modules\user\models\UserTelesaleImportForm;

use common\helpers\RoleHelper;
use common\helpers\PaymentLinkHelper;

use kyna\user\models\TimeSlot;
use kyna\user\models\UserTelesale;
use kyna\user\models\search\UserTelesaleSearch;
use kyna\user\models\AuthAssignment;
use kyna\user\models\User;
use kyna\user\models\UserMeta;
use kyna\order\models\OrderInterface;
use kyna\page\models\Page;
use kyna\payment\models\PaymentMethod;
use kyna\promotion\models\Promotion;
use kyna\course\models\Course;
use common\helpers\StringHelper;


/**
 * UserTelesaleController implements the CRUD actions for UserTelesale model.
 */
class UserTelesaleController extends \app\components\controllers\Controller
{

    public $mainTitle = 'User Care Management';

    public function init()
    {
        parent::init();
        $this->setViewPath('@app/modules/user/views/user-telesale');
    }

    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'view'],
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->user->can('UserTelesale.View');
                        },
                    ],
                    [
                        'allow' => true,
                        'actions' => ['create', 'import-excel', 'download-sample'],
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->user->can('UserTelesale.Action.Create');
                        },
                    ],
                    [
                        'allow' => true,
                        'actions' => ['update', 'note','cancel'],
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->user->can('UserTelesale.Action.Update');
                        },
                    ],
                    [
                        'allow' => true,
                        'actions' => ['delete'],
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->user->can('UserTelesale.Action.Delete');
                        },
                    ],
                    [
                        'allow' => true,
                        'actions' => ['send-email'],
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->user->can('UserTelesale.Action.SendEmail');
                        },
                    ],
                    [
                        'allow' => true,
                        'actions' => ['preview-email'],
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->user->can('UserTelesale.Action.SendEmail');
                        },
                    ],
                    [
                        'allow' => true,
                        'actions' => ['upload-to-facebook-offline-conversion'],
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->user->can('Facebook.FBOfflineConversion.Upload');
                        },
                    ]
                ],
            ],
        ]);
    }

    public function actionIndex()
    {
        $telesaleUsers = [];
        $searchModel = new UserTelesaleSearch();

        $user = Yii::$app->user;
        $canManage = $user->can('UserTelesale.Manage');
        $isTelesaleLeader = $user->can(RoleHelper::ROLE_TELESALE_LEADER);

        $telUserQuery = User::find()->with('profile')->joinWith('authAssignments')->andWhere([
            AuthAssignment::tableName() . '.item_name' => RoleHelper::ROLE_TELESALE
        ]);

        if (!$canManage || $isTelesaleLeader) {
            // normal telesale user or telesale leader then just load data of them
            $searchModel->tel_id = $user->id;
            if ($isTelesaleLeader) {
                $userMetaTable = UserMeta::tableName();

                // get only telesale users whose has manager is current user
                $telesaleUsers = $telUserQuery->joinWith('userMeta')->andWhere([
                    "$userMetaTable.key" => 'manager_id',
                    "$userMetaTable.value" => $user->id
                ])->all();
            }
        } else {
            // manager user => get all telesale users
            $telesaleUsers = $telUserQuery->all();
        }

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $countRecallQuery = clone $dataProvider->query;
        $countRecallQuery->andWhere(['>', 'recall_date', strtotime('-11 days')]);
        $countRecallQuery->andWhere(['<=', 'recall_date', time()]);

        if (Yii::$app->request->isAjax && Yii::$app->request->post('type', null) == 'export') {
            // init pagination for raw SQL
            $sql = $dataProvider->query->createCommand()->getRawSql();
            $sql = str_replace('`', '', $sql);
            $email = empty(Yii::$app->request->post('email')) ? Yii::$app->user->identity->email : Yii::$app->request->post('email');
            // execute command in console
            $rootPath = \Yii::getAlias('@root');
            $exportLog = \Yii::getAlias('@console/runtime/export_user_care.log');
            $exportErrorLog = \Yii::getAlias('@console/runtime/export_user_care-error.log');
            // check log file exist
            self::_checkExistFile($exportLog);
            self::_checkExistFile($exportErrorLog);
            $command = "php {$rootPath}/yii export/run user-care \"{$sql}\" {$email}" . " > {$exportLog} 2>>{$exportErrorLog} &";
            exec($command);
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'result' => true,
            ];
        }

        // Show member in TimeSheet
        $query = TimeSlot::find();
        $timeOfDay = strtotime('today ' . date('H:i:s')) - strtotime('today');
        $query->where(['<=', 'start_time', $timeOfDay])->andWhere(['>=', 'end_time', $timeOfDay]);
        $userType = Yii::$app->request->get('user_type');
        if ($userType == null) {
            $userType = Yii::$app->request->get('UserTelesaleSearch');
            if (isset($userType['type'])) {
                $userType = $userType['type'];
            } else {
                $userType = 'default';
            }
        }
        $query->andFilterWhere(['user_type' => $userType])->andFilterWhere(['type' => 'Telesale']);
        $timeSheet = $query->all();
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'canManage' => $canManage,
            'telesaleUsers' => $telesaleUsers,
            'countRecall' => $countRecallQuery->count(),
            'userTypes' => TimeSlot::getUserTypes(),
            'userType' => $userType,
            'timeSheet' => $timeSheet,
        ]);
    }

    public function actionCreate()
    {
        $model = new UserTelesale();
        $model->setScenario('create');
        $model->tel_id = Yii::$app->user->id;

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if (empty($model->type)) {
                $model->type = UserTelesale::TYPE_LIST_FACEBOOK;
            }
            $bonusConfigs = UserTelesale::getBonusConfigs();
            $model->bonus = $bonusConfigs[$model->type];

            if ($model->save()) {
                Yii::$app->session->setFlash('success', 'Thêm thành công');

                return $this->redirect(['index']);
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $userId = Yii::$app->user->id;

        if ($model->canAccess && $model->tel_id !== $userId) {
            $model->tel_id = $userId;
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', 'Cập nhật thành công.');

            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    public function actionNote($id)
    {
        $model = $this->findModel($id);
        $model->setScenario(UserTelesale::SCENARIO_NOTE);
        $model->status = null;
        $model->note = null;
        $userId = Yii::$app->user->id;

        if (!empty($model->recall_date)) {
            $model->recall_date = Yii::$app->formatter->asDatetime($model->recall_date);
        }

        if ($model->load(Yii::$app->request->post())) {
            if (Yii::$app->request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;
            }

            if ($model->canAccess && $model->tel_id !== $userId) {
                $model->tel_id = $userId;
            }

            if ($model->validate()) {
                if ($model->status == UserTelesale::STATUS_FOLLOW) {
                    $model->follow_num++;
                }
                $model->last_call_date = time();

                $model->save(false);

                $ret = [
                    'success' => true,
                    'message' => 'Ghi chú thành công',
                ];
            } else {
                $ret = ActiveForm::validate($this->_form);
            }

            echo Json::encode($ret);
            Yii::$app->end();
        }

        return $this->renderAjax('note', [
            'model' => $model,
            'statusList' => UserTelesale::listStatus()
        ]);
    }

    protected function findModel($id)
    {
        if (($model = UserTelesale::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionImportExcel()
    {
        $model = new UserTelesaleImportForm();

        if ($model->load(Yii::$app->request->post())) {
            $model->file = UploadedFile::getInstance($model, 'file');

            if ($model->validate()) {
                $total = 0;
                $inputFilePath = $model->file->tempName;

                try {
                    $inputFileType = \PHPExcel_IOFactory::identify($inputFilePath);
                    $objReader = \PHPExcel_IOFactory::createReader($inputFileType);
                    $objExcel = $objReader->load($inputFilePath);
                } catch (Exception $ex) {
                    echo $ex->getMessage();
                    die;
                }

                $sheet = $objExcel->getSheet(0);
                $highestRow = $sheet->getHighestRow();
                $highestColumn = $sheet->getHighestColumn();

                for ($row = 2; $row <= $highestRow; $row++) {
                    $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, NULL);

                    if ($userTelesale = $this->saveExcelRow($rowData)) {
                        $total++;
                    }
                }

                if ($total) {
                    Yii::$app->session->setFlash('success', "Import thành công $total user.");
                } else {
                    Yii::$app->session->setFlash('warning', "Không import được user nào.");
                }
                $this->redirect(['index']);
            }
        } else {
            return $this->redirect(['index']);
        }
    }

    private function saveExcelRow($rowData)
    {
        $email = trim($rowData[0][0]);
        $phone = trim($rowData[0][1]);
        $fullName = trim($rowData[0][2]);
        $userType = trim($rowData[0][3]);
        $note = trim($rowData[0][4]);
        $telesale = trim($rowData[0][5]);
        $courseId = trim($rowData[0][6]);
        $course = Course::findOne($courseId);

        if (empty($email) || empty($phone) || empty($fullName) || empty($userType) || empty($telesale) || empty($course)) {
            return false;
        }

        if (Page::getIsExistOrder($email, $courseId)) {
            return false;
        }

        $userTelesale = Page::getUserTelesale($email, $userType, $course->name);
        if ($userTelesale) {
            // update old user telesale + note
            $userTelesale->setScenario(UserTelesale::SCENARIO_REREGISTER);
            if ($userTelesale->phone_number) {
                $userTelesale->reregister_message .= $userTelesale->getAttributeLabel('phone_number') . ' cũ : ' . $userTelesale->phone_number;
            }
            $userTelesale->reregister_message .= ($userTelesale->reregister_message ? ', ' : '') . 'Ngày đăng ký trước đó: ' . Yii::$app->formatter->asDatetime($userTelesale->created_time);
            $userTelesale->created_time = time();
        } else {
            $userTelesale = new UserTelesale();
        }

        $bonusConfigs = UserTelesale::getBonusConfigs();

        $userTelesale->email = $email;
        $userTelesale->phone_number = preg_replace('/[^0-9]/', '', $phone);
        $userTelesale->full_name = $fullName;
        $userTelesale->type = $userType;

        if (!empty($note)) {
            $userTelesale->note = $note;
        }
        if (!empty($telesale)) {
            $telData = explode('_', $telesale);
            $userTelesale->tel_id = (int)$telData[0];
            $userTelesale->bonus = $bonusConfigs[$userTelesale->type];
        }
        $userTelesale->course_id = $courseId;

        if (!empty($userTelesale->type)) {
            $userTelesale->form_name = $course->name;
            $userTelesale->amount = $course->sellPrice;
        }

        if (empty($userTelesale->tel_id)) {
            $userTelesale->tel_id = Yii::$app->user->id;
        }

        if ($userTelesale->save()) {
            return $userTelesale;
        }

        return false;
    }

    /**
     * Telesale gui email cho khach hang
     * @param $id
     * @return array|string
     */
    public function actionSendEmail($id)
    {
        // Find user_telesales's entry
        $model = $this->findModel($id);
        $model->setScenario(UserTelesale::SCENARIO_SEND_EMAIL);
        $userId = Yii::$app->user->id;

        // GET method - pop up Form send email
        if (Yii::$app->request->isGet) {
            return $this->renderAjax('email', [
                'model' => $model,
                'emailTypes' => $model->getemailTypes(),
                'id' => $model->id,
            ]);
        }

        // POST method - Send email
        if (Yii::$app->request->isPost) {

            // get Post params;
            $bodyParams = Yii::$app->request->getBodyParams();
            $emailType = $bodyParams['UserTelesale']['email_type'];
            $emailSubType = isset($bodyParams['UserTelesale']['email_sub_type']) ? $bodyParams['UserTelesale']['email_sub_type'] : null;
            $emailTypeOptions = $model->getemailTypes();
            $coupon = isset($bodyParams['UserTelesale']['coupon']) ? $bodyParams['UserTelesale']['coupon'] : null;

            $model->email_type = $emailType;
            $model->email_sub_type = $emailSubType;
            // prepare params
            // Templates
            if (!isset($emailSubType)) {
                $template = '@common/mail/user/user-telesale/' . $emailTypeOptions[$emailType]['name'];
            } else {
                $template = '@common/mail/user/user-telesale/' . $emailTypeOptions[$emailType]['name'] . '-' . $emailTypeOptions[$emailType]['subType'][$emailSubType]['name'];
            }
            // Other params
            $params = [];
            $subject = '';
            switch ($emailType) {
                case UserTelesale::EMAIL_TYPE__KHONG_MUON_HOC_ONLINE:
                    // Tên học viên là tên đã đăng ký lead
                    $subject = 'Đừng học Online, vì ' . $model->full_name . ' sẽ bị thích đấy';
                    $params['full_name'] = $model->full_name;
                    break;
                case UserTelesale::EMAIL_TYPE__QUAN_TAM_KHOA_HOC_KHAC:
                    // Tên học viên là tên đã đăng ký lead
                    // Mã coupon hệ thống tạo random với rule tương ứng trong mail
                    // Link category theo cat telesale chọn

                    switch ($emailSubType) {
                        default:
//                            $subject = '[' . $emailTypeOptions[$emailType]['subType'][$emailSubType]['name'] . '] Chào ' . $model->full_name . ', bạn chưa chọn được khóa học phù hợp';
                            $subject = 'Chào ' . $model->full_name . ', bạn chưa chọn được khóa học phù hợp';

                            $params['full_name'] = $model->full_name;
                            $params['coupon'] = isset($coupon) ? $coupon : $this->sendEmailGenerateCoupon(20);
                            $params['category'] = $emailTypeOptions[$emailType]['subType'][$emailSubType]['name'];
                            break;
                    }
                    break;
                case UserTelesale::EMAIL_TYPE__CHI_PHI_KHONG_PHU_HOP:
                    // Tên học viên là tên đã đăng ký lead
                    // Mã coupon hệ thống tạo random với rule tương ứng trong mail
                    $subject = $model->full_name . " ơi, bạn có quà từ Kyna.vn!";

                    $params['full_name'] = $model->full_name;
                    $params['coupon'] = isset($coupon) ? $coupon : $this->sendEmailGenerateCoupon(20);
                    break;
                case UserTelesale::EMAIL_TYPE__HUONG_DAN_THANH_TOAN:
                    // Tên học viên là tên đã đăng ký lead
                    // Link để thanh toán tiếp bằng Onepay (visa/mastercard và thẻ nội địa)
                    // Mã đơn hàng
                    // Thông tin sản phẩm
                    // Ngày tạo đơn
                    $subject = 'Hướng dẫn thanh toán';

                    // Get Courses list
                    $courseIds = explode(",", isset($model->list_course_ids) ? $model->list_course_ids : $model->course_id);
                    $courses = [];
                    foreach ($courseIds as $index => $id) {
                        $courses[] = \kyna\course\models\Course::findOne([
                            'id' => $id
                        ]);
                    }
                    $params['courses'] = [];
                    $params['amount'] = 0;
                    foreach ($courses as $index => $course) {
                        $params['courses'][] = $course->name;
                        $params['amount'] += $course->getSellPrice();
                    }
                    // Username, email
                    $params['full_name'] = $model->full_name;
                    $params['email'] = $model->email;

                    // onepay links
                    $params['payment_link_visa_master'] = PaymentLinkHelper::generatePaymentLink([
                        'course_ids' => json_encode(explode(",", isset($model->list_course_ids) ? $model->list_course_ids : $model->course_id)),
                        'affiliate_id' => $model->affiliate_id,
                        'payment_method' => 'onepay_cc',
                    ]);
                    $params['payment_link_atm'] = PaymentLinkHelper::generatePaymentLink([
                        'course_ids' => json_encode(explode(",", isset($model->list_course_ids) ? $model->list_course_ids : $model->course_id)),
                        'affiliate_id' => $model->affiliate_id,
                        'payment_method' => 'onepay_atm',
                    ]);

//                    $params['courses'] = explode(",", $model->form_name);
                    // created time
                    $params['created_time'] = $model->created_time;

                    break;
                default:
                    break;
                // Lưu ý với các email có gửi mã coupon: Mã khuyến mãi chỉ có thể sử dụng 1 lần với email nhận được và trong 3 tháng từ ngày tạo
            }

            switch ($bodyParams['UserTelesale']['action_value']) {
                case 'quick-send':
                    // Gửi mail
                    $email = Yii::$app->mailer->compose()
                        ->setHtmlBody($this->renderPartial($template, $params))
                        ->setTo($model->email)
                        ->setSubject($subject)
                        ->send();

                    if ($email) {
                        if (isset($params['coupon']))
                            $model->email_coupon = $params['coupon'];

                        $model->save(false);
                        Yii::$app->response->format = Response::FORMAT_JSON;
                        return [
                            'success' => true,
                            'message' => 'Gửi mail thành công',
                        ];
                    }
                    break;
//                case 'preview':
//
//                    return $this->renderPartial('preview', [
//                        'template' => $template,
//                        'params' => $params,
//                    ]);
                default:
                    break;
            }
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'success' => false,
                'message' => 'Xử lý thất bại',
            ];
        }
    }

    /**
     * Preview Email before send
     *
     * @return string
     */

    public function actionPreviewEmail()
    {
        if (Yii::$app->request->isGet) {
            $id = $_GET['id'];
            $emailType = $_GET['email_type'];
            $emailSubType = $_GET['email_sub_type'];
            if (!isset($emailSubType) || $emailSubType === 'null') {
                $emailSubType = null;
            }

            $model = $this->findModel($id);
            $model->setScenario(UserTelesale::SCENARIO_SEND_EMAIL);

            $emailTypeOptions = $model->getemailTypes();

            // prepare params
            // Templates
            if (!isset($emailSubType)) {
                $template = '@common/mail/user/user-telesale/' . $emailTypeOptions[$emailType]['name'];
            } else {
                $template = '@common/mail/user/user-telesale/' . $emailTypeOptions[$emailType]['name'] . '-' . $emailTypeOptions[$emailType]['subType'][$emailSubType]['name'];
            }
            // Other params
            $params = [];
            $subject = '';

            // Default values
            $params['coupon'] = 'XXXXXXXXXX';

            switch ($emailType) {
                case UserTelesale::EMAIL_TYPE__KHONG_MUON_HOC_ONLINE:
                    // Tên học viên là tên đã đăng ký lead
                    $subject = 'Đừng học Online, vì ' . $model->full_name . ' sẽ bị thích đấy';
                    $params['full_name'] = $model->full_name;
                    break;
                case UserTelesale::EMAIL_TYPE__QUAN_TAM_KHOA_HOC_KHAC:
                    // Tên học viên là tên đã đăng ký lead
                    // Mã coupon hệ thống tạo random với rule tương ứng trong mail
                    // Link category theo cat telesale chọn

                    switch ($emailSubType) {
                        default:
//                            $subject = '[' . $emailTypeOptions[$emailType]['subType'][$emailSubType]['name'] . '] Chào ' . $model->full_name . ', bạn chưa chọn được khóa học phù hợp';
                            $subject = 'Chào ' . $model->full_name . ', bạn chưa chọn được khóa học phù hợp';

                            $params['full_name'] = $model->full_name;
//                            $params['coupon'] = $this->sendEmailGenerateCoupon(20);
                            $params['category'] = $emailTypeOptions[$emailType]['subType'][$emailSubType]['name'];
                            break;
                    }
                    break;
                case UserTelesale::EMAIL_TYPE__CHI_PHI_KHONG_PHU_HOP:
                    // Tên học viên là tên đã đăng ký lead
                    // Mã coupon hệ thống tạo random với rule tương ứng trong mail
                    $subject = $model->full_name . " ơi, bạn có quà từ Kyna.vn!";

                    $params['full_name'] = $model->full_name;
//                    $params['coupon'] = $this->sendEmailGenerateCoupon(20);
                    break;
                case UserTelesale::EMAIL_TYPE__HUONG_DAN_THANH_TOAN:
                    // Tên học viên là tên đã đăng ký lead
                    // Link để thanh toán tiếp bằng Onepay (visa/mastercard và thẻ nội địa)
                    // Mã đơn hàng
                    // Thông tin sản phẩm
                    // Ngày tạo đơn
                    $subject = 'Hướng dẫn thanh toán';

                    // Get Courses list
                    $courseIds = explode(",", isset($model->list_course_ids) ? $model->list_course_ids : $model->course_id);
                    $courses = [];
                    foreach ($courseIds as $index => $id) {
                        $courses[] = \kyna\course\models\Course::findOne([
                            'id' => $id
                        ]);
                    }
                    $params['courses'] = [];
                    $params['amount'] = 0;
                    foreach ($courses as $index => $course) {
                        $params['courses'][] = $course->name;
                        $params['amount'] += $course->getSellPrice();
                    }
                    // Username, email
                    $params['full_name'] = $model->full_name;
                    $params['email'] = $model->email;

                    // onepay links
                    $params['payment_link_visa_master'] = PaymentLinkHelper::generatePaymentLink([
                        'course_ids' => json_encode(explode(",", isset($model->list_course_ids) ? $model->list_course_ids : $model->course_id)),
                        'affiliate_id' => $model->affiliate_id,
                        'payment_method' => 'onepay_cc',
                    ]);
                    $params['payment_link_atm'] = PaymentLinkHelper::generatePaymentLink([
                        'course_ids' => json_encode(explode(",", isset($model->list_course_ids) ? $model->list_course_ids : $model->course_id)),
                        'affiliate_id' => $model->affiliate_id,
                        'payment_method' => 'onepay_atm',
                    ]);

//                    $params['courses'] = explode(",", $model->form_name);
                    // created time
                    $params['created_time'] = $model->created_time;

                    break;

                default:
                    break;
                // Lưu ý với các email có gửi mã coupon: Mã khuyến mãi chỉ có thể sử dụng 1 lần với email nhận được và trong 3 tháng từ ngày tạo
            }
            return $this->renderPartial('preview',
                [
                    'model' => $model,
                    'email' => $model->email,
                    'subject' => $subject,
                    'template' => $template,
                    'params' => $params,
                ]);
        }
    }

    public function actionDownloadSample()
    {
        $filePath = Yii::getAlias('@upload/samples/user_telesale_import_template.xlsx');

        if (!is_file($filePath)) {
            throw new NotFoundHttpException;
        }

        return Yii::$app->response->sendFile($filePath);
    }

    public function actionUploadToFacebookOfflineConversion()
    {
        // get request query params
        $searchModel = new UserTelesaleSearch();

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $models = $dataProvider->getModels();
        // prepare params;
        $params = [
//            'upload_tag' => 'test_upload_tag',
//            'data_set_id' => '137414683575437',
            'data' => [],
        ];

        $data = [];
        foreach ($models as $key => $lead) {
//            [
//                'match_keys' => [
//                    "lead_id" => "226336",
//                    'email' => [hash('sha256', 'nguyendang.tst@gmail.com')],
//                    'phone' => [hash('sha256', '+841687265980')],
//                    'country' => [hash('sha256', 'VN')],
//                    'fn' => [hash('sha256', 'Trang')],
//                    'ln' => [hash('sha256', 'Nguyễn')],
//                ],
//                'event_name' => "Lead",
//                'event_time' => time(),
//                'content_type' => 'product',
//                'content_ids' => ['870'],
//                'value' => 149000,
//                'currency' => 'VND'
//            ]

            $lead = UserTelesale::standardize($lead);

            $match_keys = [
                'lead_id' => $lead['id'],
                'country' => hash('sha256', 'VN'),
            ];
            if (!empty($lead['email']))
                $match_keys['email'] = hash('sha256', $lead['email']);
            if (!empty($lead['phone_number']))
                $match_keys['phone'] = hash('sha256', $lead['phone_number']);
            if (!empty($lead['full_name'])) {
                // parse fullname
                // Tên tùm lum quá có nên làm hay không?
            }
            if (!empty($lead['course_id']))
                $content_ids = [$lead['course_id']];
            if (!empty($lead['list_course_ids'])) {
                $content_ids = explode(',', $lead['list_course_ids']);
            }
//            if (empty($content_ids)) {
//                continue;
//            }
            if (empty($lead['amount'])) {
                continue;
            }
            $entry = [
                'match_keys' => $match_keys,
                'event_name' => "Lead",
                'event_time' => $lead['created_time'],
                'content_type' => 'product',
//                'content_ids' => $content_ids,
                'value' => empty($lead['amount']) ? 0 : $lead['amount'],
                'currency' => 'VND',
            ];
            if (!empty($content_ids)) {
                $entry['content_ids'] = $content_ids;
            }
            $data[] = $entry;
        }

        $params['data'] = $data;

        if (!empty($params['data'])) {
            $result = FbOfflineConversionHelper::UploadEvents($params);
//            var_dump($result);
            return (Json::encode($result));
        } else {
            $ret = [
                'success' => false,
                'message' => 'Empty data',
            ];

            return Json::encode($ret);
        }
    }


    public function actionCancel($id)
    {
        $model = $this->findModel($id);
        $model->status = UserTelesale::STATUS_CANCELLED;
        $model->save();
        Yii::$app->response->format = Response::FORMAT_JSON;
        return [
            'success' => true,
            'message' => 'Hủy đơn thành công',
        ];
    }

    /**
     * Check file exist
     * @param $file
     */
    private function _checkExistFile($file)
    {
        clearstatcache();
        if (!file_exists($file)) {
            touch($file);
        }
    }

    /**
     * Generate Coupon for email
     * @param $discountRate
     * @return string
     */
    private function sendEmailGenerateCoupon($discountRate)
    {
        $coupon = new Promotion();
        $coupon->apply_all = 1;
        $coupon->auto = true;
        $coupon->status = Promotion::STATUS_ACTIVE;
        $coupon->number_usage = 1;
        $coupon->type = Promotion::KIND_COURSE_APPLY;
        $coupon->discount_type = Promotion::TYPE_PERCENTAGE;
        $coupon->value = $discountRate;
        $coupon->user_number_usage = 1;
        $coupon->number_usage = 1;
        $coupon->current_number_usage = 0;
        $coupon->created_by = Yii::$app->user->id;

        $coupon->start_date = time();
        $coupon->end_date = strtotime('+3 month', $coupon->start_date);

        $coupon->save();

        return $coupon->code;
    }


}
