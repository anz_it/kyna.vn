<?php

namespace app\modules\user\controllers;

use dektrium\user\controllers\SecurityController as BaseSecurityController;
use dektrium\user\models\LoginForm;
use Yii;

class SecurityController extends BaseSecurityController
{

    /**
     * Displays the login page.
     *
     * @return string|Response
     */
    public function actionLogin()
    {
        $this->layout = '@app/views/layouts/lte_login';

        if (!Yii::$app->user->isGuest) {
            $this->goHome();
        }

        /** @var LoginForm $model */
        $model = Yii::createObject(LoginForm::className());

        $this->performAjaxValidation($model);

        if ($model->load(Yii::$app->getRequest()->post()) && $model->login()) {
            return $this->goBack();
        }

        return $this->render('@app/views/site/login', [
                    'model' => $model,
                    'module' => $this->module,
        ]);
    }

}
