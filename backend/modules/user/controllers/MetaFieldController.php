<?php

namespace app\modules\user\controllers;

use kyna\base\models\MetaField;

/**
 * MetaFieldController implements the CRUD actions for MetaField model.
 */
class MetaFieldController extends \app\components\controllers\MetaFieldController
{

    public function init()
    {
        $ret = parent::init();

        $this->modelType = MetaField::MODEL_USER;

        return $ret;
    }
}
