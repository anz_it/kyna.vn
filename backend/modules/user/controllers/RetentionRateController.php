<?php
namespace app\modules\user\controllers;
use app\modules\user\models\UserStatisticSearch;
use kyna\order\models\OrderQueue;
use Yii;
use yii\helpers\Url;

/**
 * Created by PhpStorm.
 * User: Hien Nguyen
 * Date: 8/25/2016
 * Time: 10:11 AM
 */

class RetentionRateController extends \app\components\controllers\Controller {

    public function init()
    {
        parent::init();
        $this->setViewPath('@app/modules/user/views/retention-rate');
    }

    public function actionIndex() {
        $searchModel = new UserStatisticSearch();
        $dataProvider = $searchModel->searchUserHadOrder(Yii::$app->request->queryParams, false);
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'tabs' => $this->loadTabs(),
        ]);
    }

    public function actionByMonth() {
        $searchModel = new UserStatisticSearch();
        $dataProvider = $searchModel->searchByMonth(Yii::$app->request->queryParams);
        return $this->render('by_month', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'tabs' => $this->loadTabs(),
        ]);
    }

    public function actionBefore() {
        $searchModel = new UserStatisticSearch();
        $dataProvider = $searchModel->searchUserHadOrder(Yii::$app->request->queryParams, true);
        return $this->render('before', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'tabs' => $this->loadTabs(),
        ]);
    }

    private function loadTabs()
    {
        $tabsItems = [
            [
                'label' => 'Trước đó chưa mua (Retention 1)',
                'url' => Url::toRoute(['/user/retention-rate/index']),
                'active' => $this->action->id == 'index',
                'encode' => false,
            ],
            [
                'label' => 'Theo tháng',
                'url' => Url::toRoute(['/user/retention-rate/by-month']),
                'active' => $this->action->id == 'by-month',
                'encode' => false,
            ],
            [
                'label' => 'Trước đó đã mua (Trước đó)',
                'url' => Url::toRoute(['/user/retention-rate/before']),
                'active' => $this->action->id == 'before',
                'encode' => false,
            ],
        ];

        return $tabsItems;
    }
}