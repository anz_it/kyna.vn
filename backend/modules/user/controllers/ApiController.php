<?php

namespace app\modules\user\controllers;

use common\helpers\StringHelper;
use kyna\user\models\UserTelesale;
use Yii;
use yii\web\Response;
use yii\db\Query;
use kyna\user\models\Profile;
use kyna\user\models\User;
use kyna\user\models\AuthAssignment;
use yii\db\Expression;

class ApiController extends \app\components\controllers\Controller
{
    
    public function actionSearch($q = null)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $q = trim($q);
        $type = StringHelper::typeDetect($q);
        if($type === StringHelper::STRING_TYPE_EMAIL)
        {
            $email = $q;
        } else if($type == StringHelper::STRING_TYPE_PHONE_NUMBER) {
            $phone_number = $q;
        } else {
            $username = $q;
        }
        $out = ['results' => []];
        if (!is_null($q)) {
            $query = new Query();
            
            $profileTable = Profile::tableName();
            
            $selectNameOnly = Yii::$app->request->get('selectNameOnly');
            if(!empty($email)) // search từ bảng user
            {
                $query = User::find()->alias('user')
                    ->leftJoin('profile','profile.user_id = user.id')
                    ->select([
                        'id',
                        $selectNameOnly ? "profile.name AS text" : new Expression("CONCAT_WS(' - ', profile.name, user.username, user.email) AS text"),
                        "profile.name AS name"
                    ])
                    ->where(['user.email' => $email]);
            } else if(!empty($phone_number))
            {
                $query = User::find()->alias('user')
                    ->leftJoin('profile','profile.user_id = user.id')
                    ->select([
                        'id',
                        $selectNameOnly ? "profile.name AS text" : new Expression("CONCAT_WS(' - ', profile.name, user.username, user.email) AS text"),
                        "profile.name AS name"
                    ])->where(['like' ,'profile.phone_number' , $phone_number]);

            } else //user name
            {
                $query = User::find()->alias('user')
                    ->leftJoin('profile','profile.user_id = user.id')
                    ->select([
                        'id',
                        $selectNameOnly ? "profile.name AS text" : new Expression("CONCAT_WS(' - ', profile.name, user.username, usre.email) AS text"),
                        "profile.name AS name"
                    ])->where(['username' => $q])
                    ->orFilterWhere(['like','profile.name',$q]);
            }

          /*  $query->select([
                'id',
                $selectNameOnly ? "$profileTable.name AS text" : new Expression("CONCAT_WS(' - ', $profileTable.name, t.username, t.email) AS text"),
                "$profileTable.name AS name"
            ])->from(User::tableName() . ' t')
                ->join('LEFT JOIN', $profileTable, $profileTable . '.user_id = t.id')
                ->where("(t.email LIKE :q OR t.username LIKE :q OR $profileTable.name LIKE :q)", [':q' => "%{$q}%"]);
            */

            if ($role = Yii::$app->request->get('role')) {
                $authAssignTable = AuthAssignment::tableName();
                $query->join('LEFT JOIN', $authAssignTable, "$authAssignTable.user_id = user.id");
                
                $query->andWhere(["$authAssignTable.item_name" => $role]);
            }
                
          //  $query->groupBy('user.id');
            
            $command = $query->createCommand();
            $data = $command->queryAll();

            $out['results'] = array_values($data);
        }

        return $out;
    }

    public function actionSearchByEmail($q = null)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $out = ['results' => []];
        if (!is_null($q)) {
            $query = new Query();

            $query->select([
                'id',
                "t.email AS text",
                "t.email AS name"
            ])->from(User::tableName() . ' t')
                ->where("(t.email LIKE :q OR t.username LIKE :q)", [':q' => "%{$q}%"]);

            if ($role = Yii::$app->request->get('role')) {
                $authAssignTable = AuthAssignment::tableName();
                $query->join('LEFT JOIN', $authAssignTable, "$authAssignTable.user_id = t.id");

                $query->andWhere(["$authAssignTable.item_name" => $role]);
            }

            $query->groupBy('t.id');

            $command = $query->createCommand();
            $data = $command->queryAll();

            $out['results'] = array_values($data);
        }

        return $out;
    }

    public function actionSearchLead($q = null)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $out = ['results' => []];
        if (!is_null($q)) {
            $query = new Query();
            $query->select([
                't.form_name as id',
                't.form_name as text',
                't.created_time'
            ])
                ->from(UserTelesale::tableName() . ' t')
                ->where("(t.form_name LIKE :q)", [':q' => "%{$q}%"])
                ->groupBy('id')
                ->limit(10)
                ->orderBy('t.created_time DESC');
            $command = $query->createCommand();
            $data = $command->queryAll();
            $out['results'] = array_values($data);
        }
        return $out;
    }
}
