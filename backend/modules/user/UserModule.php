<?php

namespace app\modules\user;


class UserModule extends \dektrium\user\Module
{
    public $controllerNamespace = 'app\modules\user\controllers';
    //private $_viewPath = '@vendor\dektrium\yii2-user\views';
    //

    public function init() {
        parent::init();
        $this->setViewPath('@dektrium/user/views');
    }
}
