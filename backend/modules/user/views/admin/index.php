<?php

use yii\grid\GridView;
use yii\helpers\Html;
use yii\jui\DatePicker;
use yii\widgets\Pjax;
use yii\helpers\Url;

$this->title = Yii::t('user', 'Manage users');
$this->params['breadcrumbs'][] = $this->title;
?>

<?= $this->render('/_alert', [
    'module' => Yii::$app->getModule('user'),
]) ?>

<?= $this->render('_menu') ?>

<?php Pjax::begin(['timeout' => Yii::$app->params['timeOutPjax']]) ?>

<?= GridView::widget([
    'dataProvider' 	=> $dataProvider,
    'filterModel'  	=> $searchModel,
    'layout'  		=> "{items}\n{pager}",
    'columns' => [
        'username',
        'email:email',
        [
            'attribute' => 'name',
            'label' => 'Tên',
            'value' => function ($model) {
                return $model->profile ? $model->profile->name : '';
            }
        ],
        [
            'attribute' => 'profile.facebook_id',
            'label' => 'Facebook Id',
            'filter' => Html::activeCheckbox($searchModel, 'fb_id', ['label' => 'Lọc'])
        ],
        [
            'attribute' => 'registration_ip',
            'value' => function ($model) {
                return $model->registration_ip == null
                    ? '<span class="not-set">' . Yii::t('user', '(not set)') . '</span>'
                    : $model->registration_ip;
            },
            'format' => 'html',
        ],
        [
            'attribute' => 'created_at',
            'value' => function ($model) {
                if (extension_loaded('intl')) {
                    return Yii::t('user', '{0, date, MMMM dd, YYYY HH:mm}', [$model->created_at]);
                } else {
                    return date('Y-m-d G:i:s', $model->created_at);
                }
            },
            'filter' => false
        ],
        [
            'header' => Yii::t('user', 'Confirmation'),
            'value' => function ($model) {
                if ($model->isConfirmed) {
                    return '<div class="text-center"><span class="text-success">' . Yii::t('user', 'Confirmed') . '</span></div>';
                } else {
                    return Html::a(Yii::t('user', 'Confirm'), ['confirm', 'id' => $model->id], [
                        'class' => 'btn btn-xs btn-success btn-block',
                        'data-method' => 'post',
                        'data-confirm' => Yii::t('user', 'Are you sure you want to confirm this user?'),
                    ]);
                }
            },
            'format' => 'raw',
            'visible' => Yii::$app->getModule('user')->enableConfirmation,
        ],
        [
            'header' => Yii::t('user', 'Block status'),
            'value' => function ($model) {
                if (Yii::$app->user->can('User.Block')) {
                    // has permission
                    if ($model->isBlocked) {
                        return Html::a('Unblock', ['block', 'id' => $model->id], [
                            'class' => 'btn btn-xs btn-success btn-block',
                            'data-method' => 'post',
                            'data-confirm' => Yii::t('user', 'Are you sure you want to unblock this user?'),
                        ]);
                    } else {
                        return Html::a('Block', ['block', 'id' => $model->id], [
                            'class' => 'btn btn-xs btn-danger btn-block',
                            'data-method' => 'post',
                            'data-confirm' => Yii::t('user', 'Are you sure you want to block this user?'),
                        ]);
                    }
                } else {
                    // no permission
                    return $model->isBlocked ? 'Blocked' : '';
                }
            },
            'format' => 'raw',
        ],
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{info} {update} {delete}',
            'buttons' => [
                'info' => function ($url, $model) {
                    $url = Url::toRoute(['info', 'id' => $model->id]);
                    $options = [
                        'title' => Yii::t('yii', 'Info'),
                        'aria-label' => Yii::t('yii', 'Info'),
                        'data-pjax' => '0',
                    ];
                    
                    return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, $options);
                }
            ],
            'visibleButtons' => [
                'update' => Yii::$app->user->can('User.Update'),
                'delete' => Yii::$app->user->can('User.Delete'),
                'info' => Yii::$app->user->can('User.View'),
            ]
        ],
    ],
]); ?>

<?php Pjax::end() ?>
