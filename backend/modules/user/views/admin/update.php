<?php

/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

use dektrium\user\models\User;
use yii\bootstrap\Nav;
use yii\web\View;

$authManager= Yii::$app->authManager;
$roles = $authManager->getRolesByUser($user->id);
/**
 * @var View    $this
 * @var User    $user
 * @var string  $content
 */

$this->title = Yii::t('user', 'Update user account');
$this->params['breadcrumbs'][] = ['label' => Yii::t('user', 'Users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>

<?= $this->render('/_alert', [
    'module' => Yii::$app->getModule('user'),
]) ?>

<?= $this->render('_menu') ?>

<div class="row">
    <div class="col-md-3">
        <div class="panel panel-default">
            <div class="panel-body">
                <?= Nav::widget([
                    'options' => [
                        'class' => 'nav-pills nav-stacked',
                    ],
                    'items' => [
                        [
                            'label' => 'Account details',
                            'url' => ['/user/admin/update', 'id' => $user->id],
                            'visible' => Yii::$app->user->can('User.Update')
                        ],
                        [
                            'label' => 'Profile details',
                            'url' => ['/user/admin/update-profile', 'id' => $user->id],
                            'visible' => Yii::$app->user->can('User.Update')
                        ],
                        [
                            'label' => 'Information', 
                            'url' => ['/user/admin/info', 'id' => $user->id],
                            'visible' => Yii::$app->user->can('User.View')
                        ],
                        [
                            'label' => 'Assignments',
                            'url' => ['/user/admin/assignments', 'id' => $user->id],
                            'visible' => isset(Yii::$app->extensions['dektrium/yii2-rbac']) && Yii::$app->user->can('User.Assign'),
                        ],
                        [
                            'label' => 'Choose Leader',
                            'url' => ['/user/admin/choose-leader', 'id' => $user->id],
                            'visible' => array_key_exists('Telesale', $roles) && !array_key_exists('TelesaleLeader', $roles) && Yii::$app->user->can('User.Assign'),
                        ],
                        '<hr>',
                        [
                            'label' => 'Confirm',
                            'url'   => ['/user/admin/confirm', 'id' => $user->id],
                            'visible' => !$user->isConfirmed && Yii::$app->user->can('User.Confirm'),
                            'linkOptions' => [
                                'class' => 'text-success',
                                'data-method' => 'post',
                                'data-confirm' => Yii::t('user', 'Are you sure you want to confirm this user?'),
                            ],
                        ],
                        [
                            'label' => 'Block',
                            'url'   => ['/user/admin/block', 'id' => $user->id],
                            'visible' => !$user->isBlocked && Yii::$app->user->can('User.Block'),
                            'linkOptions' => [
                                'class' => 'text-danger',
                                'data-method' => 'post',
                                'data-confirm' => Yii::t('user', 'Are you sure you want to block this user?'),
                            ],
                        ],
                        [
                            'label' => 'Unblock',
                            'url'   => ['/user/admin/block', 'id' => $user->id],
                            'visible' => $user->isBlocked && Yii::$app->user->can('User.Block'),
                            'linkOptions' => [
                                'class' => 'text-success',
                                'data-method' => 'post',
                                'data-confirm' => Yii::t('user', 'Are you sure you want to unblock this user?'),
                            ],
                        ],
                        [
                            'label' => 'Delete',
                            'url'   => ['/user/admin/delete', 'id' => $user->id],
                            'visible' => Yii::$app->user->can('User.Delete'),
                            'linkOptions' => [
                                'class' => 'text-danger',
                                'data-method' => 'post',
                                'data-confirm' => Yii::t('user', 'Are you sure you want to delete this user?'),
                            ],
                        ],
                    ],
                ]) ?>
            </div>
        </div>
    </div>
    <div class="col-md-9">
        <div class="panel panel-default">
            <div class="panel-body">
                <?= $content ?>
            </div>
        </div>
    </div>
</div>
