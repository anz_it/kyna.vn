<?php

use kartik\date\DatePicker;
use kartik\daterange\DateRangePicker;
use kartik\export\ExportMenu;
use kartik\form\ActiveForm;
use yii\bootstrap\Nav;
use yii\helpers\Html;
use yii\grid\GridView;

$crudTitles = Yii::$app->params['crudTitles'];
$crudButtonIcons = Yii::$app->params['crudButtonIcons'];

$this->title = 'Thống kê học viên (Retention Rate)';
$this->params['breadcrumbs'][] = $this->title;

$user = Yii::$app->user;

$totalRegistration = 0;

$gridColumns = [
    ['class' => 'yii\grid\SerialColumn'],
    [
        'attribute' => 'order_date',
        'format' => 'date',
        'label' => 'Ngày đăng ký',
        'filter' => false
    ],
    'user_id:raw:UserID',
    [
        'format' => 'raw',
        'label' => 'Username',
        'value' => function($model) {
            if ($model->user) {
                return $model->user->username;
            }
        }
    ],
    [
        'format' => 'raw',
        'label' => 'Email',
        'value' => function($model) {
            if ($model->user) {
                return $model->user->email;
            }
        }
    ],
    [
        'format' => 'raw',
        'label' => 'Fullname',
        'value' => function($model) {
            if ($model->user && $model->user->profile) {
                return $model->user->profile->name;
            }
        }
    ],
    [
        'format' => 'raw',
        'label' => 'Phone',
        'value' => function($model) {
            if ($model->user) {
                return $model->user->profile->phone_number;
            }
        }
    ],
//    'email:email',
];

?>
<div class="row">
    <div class="col-xs-12">
        <div class="subject-index">
            <?= Nav::widget([
                'items' => $tabs,
                'options' => ['class' => 'nav-pills'],
            ]) ?>
            <nav class="navbar navbar-default">
                <?php $form = ActiveForm::begin([
                    'action' => ['index'],
                    'options' => ['class' => 'navbar-form navbar-left'],
                    'method' => 'get',
                ]); ?>

                <?=
                $form->field($searchModel, 'date_ranger', [
                    'addon' => ['prepend' => ['content' => '<i class="glyphicon glyphicon-calendar"></i>']],
                    'options' => ['class' => 'form-group'],
                ])->widget(DateRangePicker::classname(), [
                    'useWithAddon' => true,
                    'convertFormat' => true,
                    'pluginOptions'=>[
                        'timePicker' => true,
                        'locale'=>[
                            'format' => 'd/m/yy',
                            'separator'=> " - ", // after change this, must update in controller
                        ],
                    ]
                ])->label(false)->error(false)
                ?>

                <div class="form-group">
                    <?= Html::submitButton('<i class="ion-android-search"></i> Lọc', ['class' => 'btn btn-default']) ?>
                </div>

                <?php ActiveForm::end(); ?>

                <?= ExportMenu::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => $gridColumns,
                    'fontAwesome' => true,
                    'container' => [
                        'class' => 'btn-group navbar-btn navbar-left'
                    ],
                    'columnSelectorOptions'=>[
                        'label' => 'Export Cols',
                    ],
                    'dropdownOptions' => [
                        'label' => 'Export All',
                        'class' => 'btn btn-default'
                    ],
                    'showConfirmAlert' => false,
                    'showColumnSelector' => false,
                    'target' => ExportMenu::TARGET_BLANK,
                    'filename' => 'user_export_'.date('Y-m-d_H-i', time()),
                    'exportConfig' => [
                        ExportMenu::FORMAT_PDF => false,
                    ]
                ]);
                ?>
            </nav>
            <div class="box">
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => $gridColumns

                ]); ?>
            </div>
        </div>
    </div>
</div>