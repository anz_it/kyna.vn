<?php

use yii\bootstrap\ActiveForm;
use kartik\widgets\DateTimePicker;

?>
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title" id="modal-label">Cập nhật ghi chú cho user có thông tin: <b><?= !empty($model->email) ? $model->email : (!empty($model->phone_number) ? $model->phone_number : $model->full_name) ?><b></h4>
</div>

<?php $form = ActiveForm::begin([
    'id' => 'form-call-status',
    'options' => ['class' => 'form-data-ajax'],
]); ?>
<div class="modal-body">
    <?= $form->field($model, 'status')->radioList($statusList) ?>
    <?= $form->field($model, 'note')->textArea([
        'rows' => 5,
    ]) ?>
    
    <?= $form->field($model, 'recall_date')->widget(DateTimePicker::className(), [
        'pluginOptions' => [
        'autoclose' => true,
            'format' => 'dd/mm/yyyy h:ii',
        ]
    ]) ?>
</div>
<div class="modal-footer">
    <button type="submit" class="btn btn-primary">Add note</button>
</div>
<?php ActiveForm::end(); ?>