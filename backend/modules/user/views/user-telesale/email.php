<?php
/**
 * Created by PhpStorm.
 * User: ngunp
 * Date: 7/17/2017
 * Time: 5:55 PM
 */
use yii\bootstrap\ActiveForm;
use kartik\widgets\DateTimePicker;

$_emailTypesText = [];
foreach (array_values($emailTypes) as $index => $value) {
    $_emailTypesText[] = $value['text'];
}
?>
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="modal-label">Gửi mail cho user có email: <b><?= $model->email?><b></h4>
    </div>

<?php $form = ActiveForm::begin([
    'id' => 'form-call-status',
    'options' => ['class' => 'form-data-ajax'],
]); ?>
    <div class="modal-body">
        <?= $form->field($model, 'email_type')->radioList($_emailTypesText)?>
        <?= $form->field($model, 'email_sub_type')->dropDownList([]) ?>
        <?= $form->field($model, 'action_value')->textInput([]) ?>
    </div>
    <div class="modal-footer">
        <a id="preview-email" class="btn btn-info submit-button btn-popup-sub-modal" href="/user/user-telesale/preview-email?id=<?php echo $id?>&email_type=null&email_sub_type=null">Preview</a>
        <input type="submit" class="btn btn-primary submit-button" name="quick-send" value="Gửi ngay!!">
    </div>
<?php ActiveForm::end(); ?>

<script type="text/javascript">
    var options = <?= \yii\helpers\Json::encode($emailTypes) ?>;

    $('.form-group.field-usertelesale-email_sub_type').hide();
    $('.form-group.field-usertelesale-action_value').hide();

    $('#usertelesale-email_type input[type="radio"]')[0].click();
    $('#usertelesale-email_type input[type="radio"]').on('change', function(e) {
        // delete old data
        $('#usertelesale-email_sub_type').html('');
        $('.form-group.field-usertelesale-email_sub_type').hide();

        var _index = $('#usertelesale-email_type input[type="radio"]:checked').val();
        if (options[_index]['subType']) {
            var _html = '';
            $.each(options[_index]['subType'], function(index, value) {
                _html += '<option value='+index+'>'+value.text+'</option>';
            });
            $('#usertelesale-email_sub_type').html(_html);
            $('.form-group.field-usertelesale-email_sub_type').show();
        } else {

        }
    });

    $('#preview-email').on('click', function(e) {
        _href = $('#preview-email').attr('href');
        _href = _href.replace(/(email_type=)[a-z,0-9]+/ig, '$1'+$('#usertelesale-email_type input[type="radio"]:checked').val());
        _href = _href.replace(/(email_sub_type=)[a-z,0-9]+/ig, '$1'+$('#usertelesale-email_sub_type').val());

        $('#preview-email').attr('href', _href);
    });


    $('#sub-modal .modal-dialog').css(
        'width', '700px'
    );
</script>
