<?php
/**
 * Created by PhpStorm.
 * User: ngunp
 * Date: 7/20/2017
 * Time: 10:17 AM
 */
?>
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title" id="modal-label"><b>Mail Preview</b></h4>
    <h5><b>Email: </b><?php echo $email; ?></h5>
    <h5><b>Subject: </b><?php echo $subject; ?></h5>
</div>
<div class="modal-body">
    <div>
        <?php echo $this->render($template, $params); ?>
    </div>
</div>
<div class="modal-footer">
    <button class="btn btn-primary submit-button" name="quick-send">Gửi ngay!!</button>
</div>
