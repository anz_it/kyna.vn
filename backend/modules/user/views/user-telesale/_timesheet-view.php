<?php

use yii\bootstrap\Html;

?>

<div class="container-fluid">
    <b>Danh sách thành viên trong ca trực: </b>
    <div>
        <?= Html::beginForm('', 'GET', ['class' => 'navbar-form navbar-left']) ?>
        <span>User type </span>
        <?= Html::dropDownList('user_type', $userType, $userTypes, [
            'class' => 'form-control',
        ]) ?>

        <button type="submit" class="btn btn-default">Xem</button>
        <br>
        <?= Html::endForm() ?>
        <span>
            <?php
            foreach ($timeSheet as $timeSlot) {
                $operators = $timeSlot->getOperators(strtolower(date('l')));
                foreach ($operators as $operator) {
                    if (Yii::$app->user->id == $operator->profile->user_id) {
                        echo '<span class="label label-primary">' . $operator->profile->name . '</span>; ';
                    } else {
                        echo $operator->profile->name . '; ';
                    }
                }
            }
            ?>
        </span>
    </div>
</div>
