<?php

use yii\helpers\Url;
use yii\bootstrap\Html;
use yii\bootstrap\Nav;
use yii\widgets\Pjax;
use kartik\grid\GridView;
use app\modules\user\assets\UserTelesaleAsset;
use kyna\user\models\UserTelesale;
use common\helpers\CDNHelper;
UserTelesaleAsset::register($this);

$this->title = $this->context->mainTitle;
$this->params['breadcrumbs'][] = $this->title;

$crudTitles = Yii::$app->params['crudTitles'];
$crudButtonIcons = Yii::$app->params['crudButtonIcons'];

$user = Yii::$app->user;
$cdnUrl = CDNHelper::getMediaLink();
$gridColumns = [
    'id',
    'created_time:datetime',
    'email:email',
    'full_name',
    'phone_number',
    [
//        'attribute' => 'affiliate_id',
        'label' => 'Affiliate',
        'value' => function ($model) {
            if ($model->affiliate) {
                return $model->affiliate->username;
            }
            return '';
        }
    ],
    [
        'label' => 'Original Affiliate',
        'value' => function ($model) {
            if ($model->oldAffiliate) {
                return $model->oldAffiliate->username;
            }
            return '';
        }
    ],
    [
        'label' => 'Affiliate Category',
        'value' => function ($model) {
            if ($model->affiliateUser && $model->affiliateUser->affiliateCategory) {
                if ($model->oldAffiliateUser && $model->oldAffiliateUser->affiliateCategory) {
                    return $model->oldAffiliateUser->affiliateCategory->name;
                } else {
                    return $model->affiliateUser->affiliateCategory->name;
                }
            }
            return '';
        }
    ],
    [
        'attribute' => 'tel_id',
        'value' => function ($model) {
            if ($model->teler) {
                return $model->teler->username;
            }
            return '';
        }
    ],
    'form_name',
    [
        'attribute' => 'note',
        'format' => 'html',
        'value' => function ($model) {
            $historyDataProvider = $model->actionHistory();
            $logs = $historyDataProvider->getModels();

            $ret = "";

            foreach ($logs as $log) {
                $date = Yii::$app->formatter->asDatetime($log->action_time);
                $userInfo = (empty($log->user->profile) ? $log->user->email : $log->user->profile->name);
                $ret .= (!empty($ret) ? "\n" : '') . "[$date]:{$userInfo}: {$log->status}";
            }

            if (!empty($model->recall_date)) {
                $ret .= " \n\n Gọi lại vào: " . Yii::$app->formatter->asDatetime($model->recall_date);
            }

            return $ret;
        }
    ],
    [
        'label' => 'Trạng thái',
        'value' => function ($model) {
            if ($model->user && !empty($model->user->orders)) {
                return 'Thành công (Đã tạo đơn hàng)';
            }
            return 'Từ chối';
        }
    ],
    [
        'label' => 'Giá trị đơn hàng thực tế',
        'format' => 'currency',
        'value' => function ($model) {
            if ($model->user && !empty($model->user->orders)) {
                return $model->user->totalPriceOrders;
            }
            return 0;
        }
    ],

];
?>
    <div class="user-telesale-index">
        <?= Nav::widget([
            'items' => [
                [
                    'label' => 'Danh sách User Care',
                    'url' => ['/user/user-telesale/index'],
                    'active' => true,
                ],
                [
                    'label' => 'Đơn hàng trực tiếp',
                    'url' => ['/user/direct-order/index'],
                    'active' => false,
                ]
            ],
            'options' => ['class' => 'nav-pills'],
        ]) ?>
        <br>
        <nav class="navbar navbar-default">
            <?= $this->render('_search', [
                'model' => $searchModel,
                'canManage' => $canManage,
                'telesaleUsers' => $telesaleUsers,
                'countRecall' => $countRecall,
            ]) ?>
        </nav>
        <nav class="navbar navbar-default">
            <?= $this->render('_timesheet-view', [
                'timeSheet' => $timeSheet,
                'userTypes' => $userTypes,
                'userType' => $userType,
            ]) ?>
        </nav>
        <nav class="navbar navbar-default">
            <?php if ($user->can('UserTelesale.Action.Create')) : ?>
                <?= Html::a($crudButtonIcons['create'] . ' ' . $crudTitles['create'], ['create'], ['class' => 'btn btn-success navbar-btn navbar-left']) ?>
            <?php endif; ?>

            <div class="btn-group navbar-btn navbar-left">
                <div class="btn-group">
                    <button id="w3" class="btn btn-default dropdown-toggle" title="Export data in selected format"
                            data-toggle="dropdown"><i class="glyphicon glyphicon-export"></i> Export All <span
                                class="caret"></span></button>
                    <ul id="w4" class="dropdown-menu">
                        <li title="Microsoft Excel 2007+ (xlsx)">
                            <a id="export_xls" class="export-xls" href="javascript:"
                               data-format="application/vnd.ms-excel" tabindex="-1"><i
                                        class="text-success fa fa-file-excel-o"></i> Excel 2007+</a>
                        </li>
                    </ul>
                </div>
            </div>

            <?php if ($user->can('Facebook.FBOfflineConversion.Upload')) : ?>
                <?php
                $userTelesaleSearchParams = Yii::$app->request->getQueryParam('UserTelesaleSearch');
//                var_dump($userTelesaleSearchParams);
                echo Html::a(
                    'Upload To FB Offline Convension',
                    Url::toRoute(['/user/user-telesale/upload-to-facebook-offline-conversion',
                        'UserTelesaleSearch' => $userTelesaleSearchParams,
//                        'date_ranger' => $userTelesaleSearchParams['date_ranger'],
//                        'filter_date_by_col' => $userTelesaleSearchParams['filter_date_by_col'],
//                        'type' => $userTelesaleSearchParams['type'],
//                        'form_name_list' => $userTelesaleSearchParams['form_name_list'],
//                        'tel_id' => $userTelesaleSearchParams['tel_id'],
                    ]),
                    [
                        'id' => 'fboc-bulk-upload',
                        'class' => 'btn btn-default navbar-btn',
//                            'onclick' => "return confirm('Are you sure?')",
                    ]); ?>
            <?php endif; ?>

            <?php if ($user->can('UserTelesale.Action.Create')) : ?>
                <?= $this->render('_import', ['model' => $searchModel]) ?>
            <?php endif; ?>
        </nav>
        <?php Pjax::begin(['timeout' => Yii::$app->params['timeOutPjax'], 'id' => 'idPjaxReload']); ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
//        'columns' => $gridColumns,
            'columns' =>
                [
                    'id',
                    [
                        'attribute' => 'created_time',
                        'format' => 'datetime',
                        'label' => 'Ngày đăng ký',
                        'filter' => false
                    ],
                    'email:email',
                    'phone_number',
                    'full_name',
                    [
                        'attribute' => 'tel_id',
                        'value' => function ($model) {
                            return !is_null($model->teler) ? $model->teler->profile->name : null;
                        },
                        'filter' => false,
                    ],
                    'typeText:text:User type',
                    'follow_num',
                    [
                        'attribute' => 'note',
                        'filter' => false,
                        'format' => 'raw',
                        'value' => function ($model) {
                            $historyDataProvider = $model->actionHistory();
                            $logs = $historyDataProvider->getModels();

                            $ret = !empty($model->form_name) ? "<b>{$model->form_name}</b>" : '';

                            if (!empty($model->note)) {
                                $ret .= (!empty($ret) ? '<br>' : '') . $model->note;
                            }

                            foreach ($logs as $log) {
                                $date = Yii::$app->formatter->asDatetime($log->action_time);
                                $userInfo = '';
                                if (!empty($log->user)) {
                                    $userInfo = (empty($log->user->profile) ? $log->user->email : $log->user->profile->name) . ' - ';
                                }
                                $ret .= (!empty($ret) ? '<br>' : '') . "[$date] - " . $userInfo . Html::encode($log->note);
                            }

                            if (!empty($model->recall_date)) {
                                $ret .= '<br><br>Gọi lại vào: ' . Yii::$app->formatter->asDatetime($model->recall_date);
                            }

                            return $ret;
                        }
                    ],
                    [
                        'label' => 'Affiliate',
                        'value' => function ($model) {
                            if ($model->affiliate != null) {
                                return $model->affiliate->email;
                            }
                            return '';
                        }
                    ],
                    [
                        'header' => 'Xử lý',
                        'template' => '{note}{update}{assign}{order}{send-email}{cancel}',
                        'class' => 'yii\grid\ActionColumn',
                        'contentOptions' => ['style' => 'width: 90px;'],
                        'visibleButtons' => [
                            'note' => function ($model, $key, $index) use ($user, $searchModel) {
                                return $user->can('UserTelesale.Action.Note') && ($model->canAccess || empty($searchModel->teler_id)) && $model->status != UserTelesale::STATUS_CANCELLED;
                            },
                            'update' => function ($model, $key, $index) use ($user, $searchModel) {
                                return $user->can('UserTelesale.Action.Update') && ($model->canAccess || empty($searchModel->teler_id)) && $model->status != UserTelesale::STATUS_CANCELLED;
                            },
                            'order' => function ($model, $key, $index) use ($user, $searchModel) {
                                return $user->can('UserTelesale.Action.CreateOrder') && ($model->canAccess || empty($searchModel->teler_id)) && $model->status != UserTelesale::STATUS_CANCELLED;
                            },
                            'send-email' => function ($model, $key, $index) use ($user, $searchModel) {
                                return $user->can('UserTelesale.Action.SendEmail') && ($model->canAccess || empty($searchModel->teler_id)) && $model->isValidEmail();
                            },
                            'cancel' => function ($model, $key, $index) use ($user, $searchModel) {
                                return  $user->can('UserTelesale.Action.Note') && ($model->canAccess || empty($searchModel->teler_id)) && $model->status != UserTelesale::STATUS_CANCELLED;
                            },
                        ],
                        'buttons' => [
                            'note' => function ($url, $model, $key) {
                                $url = Url::toRoute(['note', 'id' => $key]);

                                return Html::a('<span class="fa fa-plus"></span> Add note', $url, [
                                    'class' => 'btn btn-sm btn-default btn-block btn-popup',
                                ]);
                            },
                            'update' => function ($url, $model, $key) {
                                return Html::a('<span class="fa fa-edit"></span> Chỉnh sửa', $url, [
                                    'class' => 'btn btn-sm btn-default btn-block ',
                                ]);
                            },
                            'order' => function ($url, $model, $key) {
                                $url = Url::toRoute(['/order/create/index', 'user_tel_id' => $model->id]);

                                return Html::a('<span class="fa fa-cart-plus"></span> Tạo đơn hàng', $url, [
                                    'class' => 'btn btn-sm btn-success btn-call btn-block',
                                    'target' => '_blank'
                                ]);
                            },
                            'send-email' => function ($url, $model, $key) {
                                $url = Url::toRoute(['send-email', 'id' => $model->id]);

                                return Html::a('<span class="fa fa-envelope"></span> Gửi mail', $url, [
                                    'class' => 'btn btn-sm btn-default btn-block btn-popup',
                                ]);
                            },
                            'cancel' => function ($url, $model, $key) {
                                $url = Url::toRoute(['cancel', 'id' => $model->id]);
                                return Html::a('<span class="fa fa-envelope"></span> Hủy đơn hàng', $url, [
                                    'class' => 'btn btn-sm btn-default btn-block ajax-button',
                                ]);
                            }
                        ],
                    ],
                ],
        ]); ?>
        <script src="<?= $cdnUrl ?>/js/jquery.min.js"></script>
        <script type="text/javascript">
            $('a.ajax-button').on('click', function (e) {
                e.preventDefault();
                $('#loader').addClass('loading');
                $("#modal").modal("hide");

                $.ajax({
                    type: 'POST',
                    url: $(this).attr('href'),
                    success: function (data) {
                       // console.log(res);
                       // data = JSON.parse(res);
                       // console.log(data);
                        if (data.success) {
                            $("#modal").modal("hide");
                            $('#loader').removeClass('loading');
                            $.pjax.reload({container:'#idPjaxReload',timeout: 15000});
                          //  $.notify(data.message,{
                           /* $.notify("Hoàn tất cập nhật tình trạng giao vận",{
                                type: 'success',
                                allow_dismiss: true
                            }); */
                        } else {
                            $('#modal').modal("hide");
                            $('#loader').removeClass('loading');
                            $.pjax.reload({container:'#idPjaxReload',timeout: 15000});
                            $.notify('Xử lý thất bại!',{
                                type: 'danger',
                                allow_dismiss: true
                            });
                            return false;
                        }
                    }
                });
            });
        </script>
        <?php Pjax::end(); ?></div>
<?php
$script = "
    ;(function($, window, document, undefined){
        $(document).ready(function(){
            $('body').on('click', '#export_xls', function (event) {
                event.preventDefault();
                var exportDialog = new BootstrapDialog();
                exportDialog.setTitle('Vui lòng nhập email nhận file export');
                exportDialog.setMessage($('<input id=\"email_export\" class=\"form-control\" placeholder=\"Email\" value=\"" . Yii::$app->user->identity->email . "\"></input>'));
                exportDialog.setType(BootstrapDialog.TYPE_SUCCESS);
                exportDialog.setButtons([{
                    label: 'Export',
                    action: function(dialog) {
                        var email = $('#email_export').val(); 
                        if (email == '') {
                            alert('Vui lòng nhập email');
                        }
                        var data = {email: email, type: 'export'};
                        $.post('', data, function (response) {
                            if (response.result) {
                                var bootstrapDialog = new BootstrapDialog();
                                bootstrapDialog.setMessage('Vui lòng kiểm tra email '+email+'. File Export sẽ được gửi đến trong vài phút.');
                                bootstrapDialog.setType(BootstrapDialog.TYPE_SUCCESS);
                                bootstrapDialog.open(); 
                                exportDialog.close();          
                            } else {
                                
                            }
        
                        });
                    }
                }]);
                exportDialog.open(); 
            });
            
            $('body').on('click', '#fboc-bulk-upload', function (e) {
                e.preventDefault();
                
                if (confirm('Bạn có chắc chắn muốn Upload?')) { 
                    $.ajax({
                        url: $(this).attr('href'),
                        dataType: 'JSON'
                    })
                    .success(function(data) {
                        if (data.success) {
                            alert('Upload successfully');
                        } else {
                            alert('Upload error')
                        }
                    })
                    .error(function(error) {
                        alert('Upload error');
                    });
                }
                                
                return false;                
                
            });
        });
        
    })(window.jQuery || window.Zepto, window, document);";
?>
<?php
$this->registerJs($script, \yii\web\View::POS_END, 'export-user-care');
?>