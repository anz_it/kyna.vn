<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model kyna\user\models\UserTelesale */

$this->title = 'Update User Telesale: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'User Telesales', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="user-telesale-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
