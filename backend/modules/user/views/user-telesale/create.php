<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model kyna\user\models\UserTelesale */

$this->title = 'Create User Telesale';
$this->params['breadcrumbs'][] = ['label' => 'User Telesales', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-telesale-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
