<?php

use yii\widgets\ActiveForm;
use yii\helpers\Url;
use app\modules\user\models\UserTelesaleImportForm;

$model = new UserTelesaleImportForm();
?>

<?php $form = ActiveForm::begin([
    'id' => 'user-telesale-import-form',
    'options' => [
        'enctype' => 'multipart/form-data',
        'class' => 'navbar-form navbar-left'
    ],
    'action' => Url::toRoute(['import-excel'])
]) ?>

    <?= $form->field($model, 'file', [
        'template' => '<label class="btn btn-default" for="user-telesale-upload"><i class="fa fa-upload fa-fw"></i> Nhập từ file excel {input}</label>',
    ])->fileInput([
        'id' => 'user-telesale-upload',
        'class' => 'form-control hide',
    ]) ?>

<?php ActiveForm::end() ?>

<p class="navbar-text navbar-left">(File mẫu: <a href="<?= Url::toRoute(['download-sample'])?>" class="navbar-link"><i class="fa fa-download"></i>Tải về</a>)</p>

<?php

$js = <<<JS
;(function($) {
    $("#user-telesale-upload").on("change", function(e) {
        $("#$form->id").submit();
    });
})(jQuery);
JS;

$this->registerJs($js);