<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;

use kartik\form\ActiveForm;
use kartik\daterange\DateRangePicker;
use kyna\user\models\search\UserTelesaleSearch;
use kartik\select2\Select2;
use yii\web\JsExpression;
use yii\helpers\Url;

if (empty($model->form_name_list)) {
    $model->form_name_list = [];
}
?>

<div class="user-telesale-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'role' => 'search',
            'class' => 'navbar-form navbar-left',
        ]
    ]); ?>

    <?=
        $form->field($model, 'date_ranger', [
            'addon' => ['prepend' => ['content' => '<i class="glyphicon glyphicon-calendar"></i>']],
            'options' => ['class' => 'form-group'],
        ])->widget(DateRangePicker::classname(), [
            'useWithAddon' => true,
            'convertFormat' => true,
            'pluginOptions'=>[
                'dateLimit' => [
                    'days' => 30
                ],
                'timePicker' => false,
                'locale'=>[
                    'format' => 'd/m/yy',
                    'separator'=> " - ", // after change this, must update in controller
                ],
            ],
        ])->label(false)->error(false)
    ?>
    
    <?= $form->field($model, 'filter_date_by_col', [
             'options' => ['class' => 'form-group', 'style' => 'width: 180px'],
        ])->widget(Select2::classname(), [
                'data' => UserTelesaleSearch::getFilterDateByOptions(),
                'pluginOptions' => [
                    'allowClear' => true,
                ],
                'hideSearch' => true,
            ])->label(false)->error(false) ?>
    
    <?= $form->field($model, 'type', [
             'options' => ['class' => 'form-group', 'style' => 'width: 180px'],
        ])->widget(Select2::classname(), [
                'data' => UserTelesaleSearch::getTypes(),
                'options' => ['placeholder' => '--User type--'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ])->label(false)->error(false) ?>

    <?= $form->field($model, 'form_name_list', [
        'options' => ['class' => 'form-group', 'style' => 'width: 180px'],
    ])->widget(Select2::classname(), [
        'options' => ['placeholder' => '-- Lead --', 'multiple' => true],
        'showToggleAll' => false,
        'pluginOptions' => [
            'allowClear' => true,
            'minimumInputLength' => 3,
            'language' => [
                'errorLoading' => new JsExpression("function () { return 'Chờ nhận kết quả...'; }"),
            ],
            'ajax' => [
                'url' => Url::toRoute(['/user/api/search-lead']),
                'dataType' => 'json',
                'data' => new JsExpression('function(params) { return {q:params.term}; }'),
            ],
            'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
            'templateResult' => new JsExpression('function(res) { return res.text; }'),
            'templateSelection' => new JsExpression('function (res) { return res.text; }'),
        ],
    ])->label(false)->error(false) ?>
    
    <?php
    if (!empty($telesaleUsers)) {
        echo $form->field($model, 'tel_id', [
            'options' => ['class' => 'form-group', 'style' => 'width: 200px'],
        ])->widget(Select2::classname(), [
            'data' => ArrayHelper::map($telesaleUsers, 'id', 'profile.name'),
            'options' => ['placeholder' => '--Chọn người gọi--'],
            'pluginOptions' => [
                'allowClear' => true,
            ],
        ])->label(false)->error(false); 
    }
    ?>

    <div class="form-group">
        <?= Html::submitButton('<i class="ion-android-search"></i> Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::submitButton("Gọi lại <span class='label label-danger'>$countRecall</span>", ['class' => 'btn btn-default', 'name' => 'btn-recall']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
