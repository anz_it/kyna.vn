<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\DetailView;
use common\lib\widgets\locationfield\LocationField;

/* @var $this yii\web\View */
/* @var $model common\models\UserAddress */
/* @var $form yii\widgets\ActiveForm */
?>
<?= Html::activeHiddenInput($model, 'user_id') ?>
<div class="user-address-form">
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'contact_name',               // title attribute (in plain text)
            'phone_number',    // description attribute in HTML
            'street_address',
            'locations.0.name:raw:Quận/Huyện',
            'locations.1.name:raw:Tỉnh/Thành phố',
        ],
    ]); ?>

</div>
