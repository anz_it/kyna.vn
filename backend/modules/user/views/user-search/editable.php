<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\bootstrap\InputWidget;
use common\lib\widgets\locationfield\LocationField;

/* @var $this yii\web\View */
/* @var $model common\models\UserAddress */
/* @var $form yii\widgets\ActiveForm */
?>
<?= Html::activeHiddenInput($model, 'user_id') ?>
<div class="user-address-form form">
    <div class="form-group"><?= Html::activeLabel($model, 'contact_name'); ?>
        <?= Html::activeTextInput($model, 'contact_name', ['class' => 'form-control']) ?>
    </div>
    <div class="form-group"><?= Html::activeLabel($model, 'phone_number'); ?>
        <?= Html::activeTextInput($model, 'phone_number', ['class' => 'form-control']) ?>
    </div>
    <div class="form-group"><?= Html::activeLabel($model, 'street_address'); ?>
        <?= Html::activeTextInput($model, 'street_address', ['class' => 'form-control']) ?>
    </div>

    <?= LocationField::widget([
        'model' => $model,
        'attribute' => 'location_id',
    ]) ?>

</div>
