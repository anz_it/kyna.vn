<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model kyna\base\models\TimeSheet */

$this->title = 'Create Time Sheet';
$this->params['breadcrumbs'][] = ['label' => 'Time Sheets', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="time-sheet-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'dow' => $dow
    ]) ?>

</div>
