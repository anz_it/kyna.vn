<?php

use yii\bootstrap\Html;
use yii\bootstrap\ActiveForm;
use yii\widgets\MaskedInput;
/* @var $this yii\web\View */
/* @var $model kyna\base\models\TimeSheet */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="time-sheet-form">

    <?php $form = ActiveForm::begin(); ?>

        <div class="row">
            <div class="col-md-2 col-sm-3">
                <?= $form->field($model, 'start')->widget(MaskedInput::className(), [
                    'mask' => 'h:s',
                    'clientOptions' => [
                        'placeholder' => '-',
                        'clearMaskOnLostFocus' => false
                    ],
                ]) ?>

                <?= $form->field($model, 'end')->widget(MaskedInput::className(), [
                    'mask' => 'h:s',
                    'clientOptions' => [
                        'placeholder' => '-',
                        'clearMaskOnLostFocus' => false
                    ],
                ]) ?>

                <?= $form->field($model, 'group')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-10 col-sm-9">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <?php for ($i = 0; $i < 7; $i++) : ?>
                                <th><?= $dow[$i + 1] ?></th>
                            <?php endfor; ?>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <?php for ($i = 0; $i < 7; $i++) : ?>
                                <td>
                                    <?= $form->field($model, 'operator_ids[]')->checkboxList($operators)->label(false) ?>
                                </td>
                            <?php endfor; ?>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>

        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>

        <?php ActiveForm::end(); ?>

</div>
