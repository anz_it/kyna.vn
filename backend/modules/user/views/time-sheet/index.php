<?php

use yii\bootstrap\Html;
use yii\helpers\Url;
use app\modules\user\assets\TimeSheetAsset;

TimeSheetAsset::Register($this);

$type = \Yii::$app->request->get('type', '');
$userType = \Yii::$app->request->get('user_type', 'default');

$user = Yii::$app->user;

$this->title = 'Team: ' . html_entity_decode($types[$type] . ' &ndash; ' . $userTypes[$userType]);
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="time-sheet-index">

    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <?= Html::beginForm(Url::toRoute('/user/time-sheet'), 'GET', ['class' => 'navbar-form navbar-left']) ?>
                <?= Html::dropDownList('type', $type, $types, [
                    'class' => 'form-control',
                ]) ?>
                <?= Html::dropDownList('user_type', $userType, $userTypes, [
                    'class' => 'form-control',
                ]) ?>

                <button type="submit" class="btn btn-default">Xem thời gian biểu</button>
            <?= Html::endForm() ?>
        </div>
    </nav>

    <table class="table table-striped">
        <thead>
            <tr>
                <th></th>
                <?php foreach ($dow as $day => $label) : ?>
                    <th><?= $label ?></th>
                <?php endforeach; ?>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($timeSheet as $timeSlot) : ?>
                <tr>
                    <td class="timeslot-<?= $timeSlot->id ?>">
                        <?= $this->render('forms/_timeslot-view', [
                            'timeSlot' => $timeSlot,
                        ]); ?>
                    </td>
                    <?php foreach ($dow as $day => $label) : ?>
                        <td class="operators-<?= $timeSlot->id ?>-<?= $day ?>">
                            <?= $this->render('forms/_operators-view', [
                                'timeSlot' => $timeSlot,
                                'day' => $day,
                            ]); ?>
                        </td>
                    <?php endforeach; ?>
                </tr>
            <?php endforeach; ?>
            <?php if ($user->can('TimeSlot.Create')) : ?>
                <tr>
                    <td class="timeslot-add">
                        <?= Html::a('<i class="ion-android-add"></i> Thêm</a>', Url::toRoute([
                            '/user/time-sheet/create',
                            'type' => $type,
                            'user_type' => $userType,
                        ]), [
                            'data-target' => '.timeslot-add',
                            'class' => 'update-timeslot btn btn-xs btn-success',
                        ]) ?>
                    </td>
                    <td colspan="7"></td>
                </tr>
            <?php endif; ?>
        </tbody>
    </table>
</div>
