<?php

use yii\bootstrap\Html;
use yii\helpers\Url;

$user = Yii::$app->user;
?>

<strong><?= $timeSlot->type ?>:</strong> <br>
<?= $timeSlot->startTime ?>
&ndash;
<?= $timeSlot->endTime ?><br>

<small>
    <?php 
    if ($user->can('TimeSlot.Update')) {
        echo Html::a('<i class="ion-edit"></i> Sửa</a>', Url::toRoute([
            '/user/time-sheet/update',
            'id' => $timeSlot->id,
        ]), [
            'data-target' => '.timeslot-'.$timeSlot->id,
            'class' => 'update-timeslot',
        ]);
        
        echo '&nbsp;&bull;&nbsp;';
    }
    
    if ($user->can('TimeSlot.Delete')) {
        echo Html::a('<i class="ion-android-delete"></i> Xóa', Url::toRoute([
            '/user/time-sheet/delete',
            'id' => $timeSlot->id,
        ]), [
            'class' => 'text-muted delete-timeslot',
        ]);
    }
    ?>
</small>
