<?php

use yii\bootstrap\Html;
use yii\helpers\Url;

$user = Yii::$app->user;
$operators = $timeSlot->getOperators($day);

foreach ($operators as $operator) {
    echo $operator->profile->name, '<br>';
} 
?>
<?php if ($user->can('TimeSlot.Update')) : ?>
    <small>
        <?= Html::a('<i class="ion-edit"></i> Cập nhật</a>', Url::toRoute([
            '/user/time-sheet/update-operators',
            'id' => $timeSlot->id,
            'day' => $day,
        ]), [
            'data-target' => '.operators-'.$timeSlot->id.'-'.$day,
            'class' => 'update-operators',
        ]) ?>
    </small>
<?php endif; ?>
