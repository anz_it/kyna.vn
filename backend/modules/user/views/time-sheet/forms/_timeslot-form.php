<?php

use yii\bootstrap\Html;
use yii\bootstrap\ActiveForm;
use yii\widgets\MaskedInput;
/* @var $this yii\web\View */
/* @var $model kyna\base\models\TimeSheet */
/* @var $form yii\widgets\ActiveForm */
?>
<?php $form = ActiveForm::begin(); ?>
<?= $form->field($model, 'startTime')->widget(MaskedInput::className(), [
    'mask' => 'h:s',
    'clientOptions' => [
        'placeholder' => '-',
        'clearMaskOnLostFocus' => false
    ],
]) ?>
<?= $form->field($model, 'endTime')->widget(MaskedInput::className(), [
    'mask' => 'h:s',
    'clientOptions' => [
        'placeholder' => '-',
        'clearMaskOnLostFocus' => false
    ],
]) ?>
<div class="form-group">
    <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
</div>

<?php ActiveForm::end(); ?>
