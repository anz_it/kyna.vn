<?php
use yii\bootstrap\ActiveForm;

?>
<?php $form = ActiveForm::begin(); ?>
    <?= $form->field($model, $day)->checkboxList($operators)->label(false); ?>
    <button type="submit" class="btn btn-primary btn-xs">Cập nhật</button>
<?php ActiveForm::end(); ?>
