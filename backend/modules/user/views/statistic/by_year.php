<?php

use kartik\date\DatePicker;
use kartik\daterange\DateRangePicker;
use kartik\form\ActiveForm;
use yii\bootstrap\Nav;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use kyna\mana\models\Subject;

$crudTitles = Yii::$app->params['crudTitles'];
$crudButtonIcons = Yii::$app->params['crudButtonIcons'];

$this->title = 'Thống kê học viên';
$this->params['breadcrumbs'][] = $this->title;

$user = Yii::$app->user;

$totalRegistration = 0;

?>
<div class="row">
    <div class="col-xs-12">
        <div class="subject-index">
            <?= Nav::widget([
                'items' => $tabs,
                'options' => ['class' => 'nav-pills'],
            ]) ?>
            <nav class="navbar navbar-default">
            <?php $form = ActiveForm::begin([
                'action' => ['by-year'],
                'options' => ['class' => 'navbar-form navbar-left'],
                'method' => 'get',
            ]); ?>

                <?=
                $form->field($searchModel, 'year', [
//                    'addon' => ['prepend' => ['content' => '<i class="glyphicon glyphicon-calendar"></i>']],
                    'options' => ['class' => 'form-group'],
                ])->widget(DatePicker::classname(), [
                    'convertFormat' => true,
                    'pluginOptions'=>[
                        'minViewMode' => 'years',
                        'format' => 'yyyy',
                    ]
                ])->label(false)->error(false)
                ?>

            <div class="form-group">
                <?= Html::submitButton('<i class="ion-android-search"></i> Lọc', ['class' => 'btn btn-default']) ?>
            </div>

            <?php ActiveForm::end(); ?>
            </nav>
            <div class="box">
                <table class="table table-striped table-bordered">
                    <tr>
                        <th>Năm <?= !empty($searchModel->year)?$searchModel->year:date('Y', time())?></th>
                        <th>Tháng 1</th>
                        <th>Tháng 2</th>
                        <th>Tháng 3</th>
                        <th>Tháng 4</th>
                        <th>Tháng 5</th>
                        <th>Tháng 6</th>
                        <th>Tháng 7</th>
                        <th>Tháng 8</th>
                        <th>Tháng 9</th>
                        <th>Tháng 10</th>
                        <th>Tháng 11</th>
                        <th>Tháng 12</th>
                    </tr>
                    <tr>
                        <td>Đăng ký thành viên</td>
                        <?php
                            if ($listRegistrationCount) {
                                for ($i = 0; $i < 12; $i++) {
                                    if (isset($listRegistrationCount[$i])) {
                                        $totalRegistration += $listRegistrationCount[$i];
                                        ?>
                                        <td><?=$listRegistrationCount[$i]?></td>
                                        <?php } else { ?>
                                            <td>0</td>
                                        <?php
                                    }
                                }
                            }
                        ?>
                    </tr>
                    <tr>
                        <td>Confirm Email</td>
                        <?php
                        if ($listRegistrationConfirmCount) {
                            for ($i = 0; $i < 12; $i++) {
                                if (isset($listRegistrationConfirmCount[$i])) { ?>
                                    <td><?=$listRegistrationConfirmCount[$i]?></td>
                                <?php } else { ?>
                                    <td>0</td>
                                    <?php
                                }
                            }
                        }
                        ?>
                    </tr>
                    <tr>
                        <td>Đăng ký khóa học</td>
                        <?php
                        if ($listOrderCount) {
                            for ($i = 0; $i < 12; $i++) {
                                if (isset($listOrderCount[$i])) { ?>
                                    <td><?=$listOrderCount[$i]?></td>
                                <?php } else { ?>
                                    <td>0</td>
                                    <?php
                                }
                            }
                        }
                        ?>
                    </tr>
                    <tr>
                        <td>Hoàn thành đăng ký</td>
                        <?php
                        if ($listOrderIsPaidCount) {
                            for ($i = 0; $i < 12; $i++) {
                                if (isset($listOrderIsPaidCount[$i])) { ?>
                                    <td><?=$listOrderIsPaidCount[$i]?></td>
                                <?php } else { ?>
                                    <td>0</td>
                                    <?php
                                }
                            }
                        }
                        ?>
                    </tr>
                    <tr>
                        <td>Thực thu học phí</td>
                        <?php
                        if ($listOrderDistinctCount) {
                            for ($i = 0; $i < 12; $i++) {
                                if (isset($listOrderDistinctCount[$i])) { ?>
                                    <td><?=$listOrderDistinctCount[$i]?></td>
                                <?php } else { ?>
                                    <td>0</td>
                                    <?php
                                }
                            }
                        }
                        ?>
                    </tr>
                    <tr>
                        <td>Tổng thành viên trong năm</td>
                        <td colspan="12"><?=$totalRegistration?></td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>