<?php

use kartik\date\DatePicker;
use kartik\daterange\DateRangePicker;
use kartik\form\ActiveForm;
use yii\bootstrap\Nav;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use kyna\mana\models\Subject;

$crudTitles = Yii::$app->params['crudTitles'];
$crudButtonIcons = Yii::$app->params['crudButtonIcons'];

$this->title = 'Thống kê học viên';
$this->params['breadcrumbs'][] = $this->title;

$user = Yii::$app->user;

$totalRegistration = 0;

?>
<div class="row">
    <div class="col-xs-12">
        <div class="subject-index">
            <?= Nav::widget([
                'items' => $tabs,
                'options' => ['class' => 'nav-pills'],
            ]) ?>
            <nav class="navbar navbar-default">
                <?php $form = ActiveForm::begin([
                    'action' => ['by-date'],
                    'options' => ['class' => 'navbar-form navbar-left'],
                    'method' => 'get',
                ]); ?>

                <?=
                $form->field($searchModel, 'date_ranger', [
                    'addon' => ['prepend' => ['content' => '<i class="glyphicon glyphicon-calendar"></i>']],
                    'options' => ['class' => 'form-group'],
                ])->widget(DateRangePicker::classname(), [
                    'useWithAddon' => true,
                    'convertFormat' => true,
                    'pluginOptions'=>[
                        'timePicker' => true,
                        'locale'=>[
                            'format' => 'd/m/yy',
                            'separator'=> " - ", // after change this, must update in controller
                        ],
                    ]
                ])->label(false)->error(false)
                ?>

                <div class="form-group">
                    <?= Html::submitButton('<i class="ion-android-search"></i> Lọc', ['class' => 'btn btn-default']) ?>
                </div>

                <?php ActiveForm::end(); ?>
            </nav>
            <div class="box">
                <table class="table table-striped table-bordered">
                    <tr>
                        <th>Đăng ký thành viên</th>
                        <th>Confirmed Email</th>
                        <th>Đăng ký khóa học</th>
                        <th>Hoàn thành đăng ký</th>
                        <th>Thực thu học phí</th>
                    </tr>
                    <tr>
                        <td><?=$registrationCount->count()?></td>
                        <td><?=$registrationConfirmCount->count()?></td>
                        <td><?=$orderCount->count()?></td>
                        <td><?=$orderIsPaidCount->count()?></td>
                        <td><?=$orderDistinctCount->count('distinct user_id')?></td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>