<?php

use yii\bootstrap\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'User Addresses';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-address-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create User Address', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'user_id',
                'label' => 'User',
                'format' => 'raw',
                'value' => function ($model) {
                    return Html::mailto($model->user->email);
                },
            ],
            'contact_name',
            //'email:email',
            'phone_number',
            'street_address',
            [
                'attribute' => 'location_id',
                'label' => 'Địa phương',
                'format' => 'raw',
                'value' => function ($model) {
                    $locations = [];
                    foreach ($model->locations as $loc) {
                        $locations[] = $loc->name;
                    }

                    return implode(', ', $locations);
                    //var_dump($model->location);
                    //return Html::mailto($model->location->email);
                },
            ],
            //'status',
            'created_time:datetime',
            //'updated_time:datetime',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
