<?php
/**
 * @link http://www.yiiframework.com/
 *
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\modules\user\assets;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 *
 * @since 2.0
 */
class TimeSheetAsset extends \yii\web\AssetBundle
{
    //public $basePath = '@webroot';
    //public $baseUrl = '@web';
    public $sourcePath = __DIR__;
    public $js = [
        'js/time-sheet.js',
    ];
    public $css = [
        'css/time-sheet.css',
    ];
    public $depends = [
        'app\assets\KynaAsset',
    ];
}
