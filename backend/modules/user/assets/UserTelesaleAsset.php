<?php

namespace app\modules\user\assets;

class UserTelesaleAsset extends \yii\web\AssetBundle
{
    
    public $sourcePath = __DIR__;
    public $js = [
//        '/js/ajax-form-popup.js?v=1501226978',
        '/js/user-telesale/send-email.js?v=1501226978'
    ];
    public $depends = [
        'app\assets\KynaAsset',
    ];
}
