;(function($) {
    $('body')
        .on("click", ".update-operators, .update-timeslot", function(e) {
            e.preventDefault();
            var target = $(this).data("target"),
                $target = $(target),
                url = this.href;

            $target.load(url, function() {
                $target.on("submit", "form", function(e) {
                    e.preventDefault();

                    var formData = $(this).serialize();
                    $.post(url, formData, function(resp) {
                        $target.html(resp);
                    });
                });
            });
        })
        .on("click", ".delete-timeslot", function(e) {
            e.preventDefault();
            var url = this.href,
                data = $(this).data();
            $.post(url, data, function (resp) {
                location.reload();
            });
        });
})(jQuery);
