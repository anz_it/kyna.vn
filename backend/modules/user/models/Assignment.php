<?php
/**
 * Created by PhpStorm.
 * User: macintosh
 * Date: 5/29/17
 * Time: 3:29 PM
 */

namespace app\modules\user\models;

use Yii;
use common\helpers\RoleHelper;
use kyna\settings\models\TeacherSetting;

class Assignment extends \dektrium\rbac\models\Assignment
{

    /**
     * Updates auth assignments for user.
     * @return boolean
     */
    public function updateAssignments()
    {
        if (!$this->validate()) {
            return false;
        }

        if (!is_array($this->items)) {
            $this->items = [];
        }

        $assignedItems = $this->manager->getItemsByUser($this->user_id);
        $assignedItemsNames = array_keys($assignedItems);

        foreach (array_diff($assignedItemsNames, $this->items) as $item) {
            $this->manager->revoke($assignedItems[$item], $this->user_id);
        }

        foreach (array_diff($this->items, $assignedItemsNames) as $item) {
            $this->manager->assign($this->manager->getItem($item), $this->user_id);
        }

        if (in_array(RoleHelper::ROLE_TEACHER, $this->items)) {
            $settingModel = TeacherSetting::findOne([
                'teacher_id' => $this->user_id,
                'key' => TeacherSetting::KEY_RECEIVE_MAIL_DATE,
            ]);

            if ($settingModel == null) {
                $settingModel = new TeacherSetting();
                $settingModel->teacher_id = $this->user_id;
                $settingModel->key = TeacherSetting::KEY_RECEIVE_MAIL_DATE;

                $settingModel->value = Yii::$app->params['default_receive_qna_emails'];
                $settingModel->save();
            }
        }

        $this->updated = true;

        return true;
    }
}