<?php

namespace app\modules\user\models;

use Yii;
use kyna\base\Mailer;

class User extends \dektrium\user\models\User
{
    
    protected function getMailer()
    {
        return Yii::$container->get(Mailer::className());
    }
}