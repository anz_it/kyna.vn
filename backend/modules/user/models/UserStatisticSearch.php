<?php
namespace app\modules\user\models;
use kyna\order\models\Order;
use kyna\user\models\User;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\helpers\ArrayHelper;

/**
 * Created by PhpStorm.
 * User: Hien Nguyen
 * Date: 8/25/2016
 * Time: 10:44 AM
 */

class UserStatisticSearch extends \kyna\user\models\User {
    public $total;
    public $month;
    public $date_ranger;
    public $year;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['year', 'date_ranger'], 'safe'],
        ]);
    }

    public function getStatistic() {
        $list = self::find()->select('MONTH(FROM_UNIXTIME(created_at)) as month, COUNT(*) AS total');

        if (!empty($this->year)) {
            $beginDate = date_create_from_format('d/m/Y H:i', '01/01/'.$this->year . ' 00:00');
            $endDate = date_create_from_format('d/m/Y H:i', '31/12/'.$this->year . ' 23:59');

            $list->andWhere(['>=', 'created_at', $beginDate->getTimestamp()]);
            $list->andWhere(['<=', 'created_at', $endDate->getTimestamp()]);
        }

        $data = $list->groupBy('MONTH(FROM_UNIXTIME(created_at))')->all();
        return $data;
    }

    public function getStatisticByDate() {
            $list = self::find();
            if (!empty($this->date_ranger)) {
                $dateRange = explode(' - ', $this->date_ranger);
                $beginDate = date_create_from_format('d/m/Y H:i', $dateRange[0] . ' 00:00');
                $endDate = date_create_from_format('d/m/Y H:i', $dateRange[1] . ' 23:59');

                $list->andWhere(['>=', 'created_at', $beginDate->getTimestamp()]);
                $list->andWhere(['<=', 'created_at', $endDate->getTimestamp()]);
            }

        return $list;
    }

    public function getStatisticWithConfirmEmail() {
        $list = self::find()->select('MONTH(FROM_UNIXTIME(created_at)) as month, COUNT(*) AS total')->where('YEAR(FROM_UNIXTIME(created_at))='.$this->year)->andWhere(['<>','confirmed_at', 0])->groupBy('MONTH(FROM_UNIXTIME(created_at))')->all();
        return $list;
    }

    public function getStatisticWithConfirmEmailByDate() {
        $list = self::find()->where(['<>','confirmed_at', 0]);
        if (!empty($this->date_ranger)) {
            $dateRange = explode(' - ', $this->date_ranger);
            $beginDate = date_create_from_format('d/m/Y H:i', $dateRange[0] . ' 00:00');
            $endDate = date_create_from_format('d/m/Y H:i', $dateRange[1] . ' 23:59');

            $list->andWhere(['>=', 'created_at', $beginDate->getTimestamp()]);
            $list->andWhere(['<=', 'created_at', $endDate->getTimestamp()]);
        }

        return $list;
    }

    public function getOrderStatistic() {
        $list = Order::find()->select('MONTH(FROM_UNIXTIME(order_date)) as month, COUNT(*) AS total')->where('YEAR(FROM_UNIXTIME(order_date))='.$this->year)->groupBy('MONTH(FROM_UNIXTIME(order_date))')->all();
        return $list;
    }

    public function getOrderStatisticByDate() {
        $list = Order::find();
        if (!empty($this->date_ranger)) {
            $dateRange = explode(' - ', $this->date_ranger);
            $beginDate = date_create_from_format('d/m/Y H:i', $dateRange[0] . ' 00:00');
            $endDate = date_create_from_format('d/m/Y H:i', $dateRange[1] . ' 23:59');

            $list->andWhere(['>=', 'order_date', $beginDate->getTimestamp()]);
            $list->andWhere(['<=', 'order_date', $endDate->getTimestamp()]);
        }
        return $list;
    }

    public function getOrderIsPaidStatistic() {
        $list = Order::find()->select('MONTH(FROM_UNIXTIME(order_date)) as month, COUNT(*) AS total')->where('YEAR(FROM_UNIXTIME(order_date))='.$this->year)->andWhere(['is_paid'=>Order::BOOL_YES])->groupBy('MONTH(FROM_UNIXTIME(order_date))')->all();
        return $list;
    }

    public function getOrderIsPaidStatisticByDate() {
        $list = Order::find()->where(['is_paid'=>Order::BOOL_YES]);
        if (!empty($this->date_ranger)) {
            $dateRange = explode(' - ', $this->date_ranger);
            $beginDate = date_create_from_format('d/m/Y H:i', $dateRange[0] . ' 00:00');
            $endDate = date_create_from_format('d/m/Y H:i', $dateRange[1] . ' 23:59');

            $list->andWhere(['>=', 'order_date', $beginDate->getTimestamp()]);
            $list->andWhere(['<=', 'order_date', $endDate->getTimestamp()]);
        }
        return $list;
    }

    public function getOrderDistinctStatistic() {
        $list = Order::find()->select('MONTH(FROM_UNIXTIME(order_date)) as month, COUNT(DISTINCT user_id) AS total')->where('YEAR(FROM_UNIXTIME(order_date))='.$this->year)->andWhere(['is_paid'=>Order::BOOL_YES])->andWhere(['>', 'total', 0])->groupBy('MONTH(FROM_UNIXTIME(order_date))')->all();
        return $list;
    }

    public function getOrderDistinctStatisticByDate() {
        $list = Order::find()->where(['is_paid'=>Order::BOOL_YES])->andWhere(['>', 'total', 0]);
        if (!empty($this->date_ranger)) {
            $dateRange = explode(' - ', $this->date_ranger);
            $beginDate = date_create_from_format('d/m/Y H:i', $dateRange[0] . ' 00:00');
            $endDate = date_create_from_format('d/m/Y H:i', $dateRange[1] . ' 23:59');

            $list->andWhere(['>=', 'order_date', $beginDate->getTimestamp()]);
            $list->andWhere(['<=', 'order_date', $endDate->getTimestamp()]);
        }
        return $list;
    }

    public function searchByMonth($params) {
        $query = Order::find();
        $query->alias('t');
        $query->leftJoin(User::tableName(), User::tableName().'.id = t.user_id');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        if (!empty($this->date_ranger)) {
            $dateRange = explode(' - ', $this->date_ranger);
            $beginDate = date_create_from_format('d/m/Y H:i', $dateRange[0] . ' 00:00');
            $endDate = date_create_from_format('d/m/Y H:i', $dateRange[1] . ' 23:59');

            $query->andWhere(['>=', 'order_date', $beginDate->getTimestamp()]);
            $query->andWhere(['<=', 'order_date', $endDate->getTimestamp()]);
        }

        $query->andWhere(['>', 'total', 0]);

        $query->groupBy('user_id');
        return $dataProvider;
    }

    public function searchUserHadOrder($params, $hadBoughtBefore = false) {
        $arrUserIds = [];
        $query = Order::find();
        $query->alias('t');
        $query->leftJoin(User::tableName(), User::tableName().'.id = t.user_id');

        $this->load($params);

        $hadBought = [];
        if (!empty($this->date_ranger)) {
            $dateRange = explode(' - ', $this->date_ranger);
            $beginDate = date_create_from_format('d/m/Y H:i', $dateRange[0] . ' 00:00');
            $endDate = date_create_from_format('d/m/Y H:i', $dateRange[1] . ' 23:59');

            $query->andWhere(['>=', 'order_date', $beginDate->getTimestamp()]);
            $query->andWhere(['<=', 'order_date', $endDate->getTimestamp()]);
            $arrUserIds = $this->getUserIdsFromDate($beginDate);
            // using anonymous function
            $result = ArrayHelper::getColumn($arrUserIds, function ($element) {
                return (int)$element;
            });
            $query->andWhere(['>', 'total', 0]);

            $query->groupBy('user_id');

            $dataResult = $query->all();
            if ($dataResult) {

                // User had order before
                if ($hadBoughtBefore) {
                    foreach ($dataResult as $item) {
                        if (in_array($item->user_id, $result)) {
                            $hadBought[] = $item;
                        }
                    }
                } else {
                    foreach ($dataResult as $item) {
                        if (!in_array($item->user_id, $result)) {
                            $hadBought[] = $item;
                        }
                    }
                }

            }

        }

        $dataProvider = new ArrayDataProvider([
            'allModels' => $hadBought,
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);
        return $dataProvider;
    }

    protected function getUserIdsFromDate($beginDate) {
        return Order::find()
            ->where(['<=', 'order_date', $beginDate->getTimestamp()])
            ->andWhere(['>', 'total', 0])
            ->select('user_id')
            ->groupBy('user_id')->column();
    }
}