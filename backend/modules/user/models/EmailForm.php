<?php

namespace app\modules\user\models;
use Yii;
use kyna\user\models\User;
class EmailForm extends \yii\base\Model
{
    public $email;

    public function rules()
    {
        return [
            [['email'], 'required'],
            ['email', 'string', 'max' => 255],
            ['email', 'email'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'email' => 'Email',
        ];
    }
}
