<?php

namespace app\modules\user\models;

class UserTelesaleImportForm extends \yii\base\Model
{
    
    public $file;
    
    public function rules()
    {
        return [
            [['file'], 'file', 'skipOnEmpty' => false, 'extensions' => 'xls, xlsx'],
        ];
    }
    
    public function attributeLabels()
    {
        return [
            'file' => 'File',
        ];
    }
}
