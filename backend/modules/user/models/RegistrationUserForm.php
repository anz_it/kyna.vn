<?php

namespace app\modules\user\models;

use dektrium\user\models\RegistrationForm as BaseRegistrationForm;
use yii\helpers\Html;
use common\validators\PhoneNumberValidator;

class RegistrationUserForm extends BaseRegistrationForm
{
    public $name;
    public $phonenumber;

    public function rules()
    {
        return array_merge(parent::rules(), [
            'nameRequired' => ['name', 'required'],
            'nameLength' => ['name', 'string', 'min' => 3, 'max' => 50],
            'phonenumberRequired' => ['phonenumber', 'required'],
            'phonenumberLength' => ['phonenumber', PhoneNumberValidator::className()]
        ]);
    }

    public function beforeValidate()
    {
        $this->username = Html::encode($this->email, false);
        $this->name = Html::encode($this->name, false);
        $this->phonenumber = Html::encode($this->phonenumber, false);
        return parent::beforeValidate();
    }
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'email'    => 'Email',
            'username' => 'Username',
            'password' => 'Mật khẩu',
            'phonenumber' => 'Số điện thoại',
            'name' => 'Họ Tên',
        ];
    }
    public function register()
    {
        if (!$this->validate()) {
            return false;
        }

        /** @var User $user */
        $user = \Yii::createObject(User::className());
        $user->setScenario('register');
        $this->loadAttributes($user);
        if (!$user->register()) {
            return false;
        }

        \Yii::$app->session->setFlash(
            'info',
            \Yii::t(
                'user',
                'Your account has been created and a message with further instructions has been sent to your email'
            )
        );
        return $user;
    }

}
