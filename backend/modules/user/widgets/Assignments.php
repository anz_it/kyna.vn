<?php

namespace app\modules\user\widgets;

use Yii;
use app\modules\user\models\Assignment;

/**
 * Created by PhpStorm.
 * User: macintosh
 * Date: 5/29/17
 * Time: 3:28 PM
 */
class Assignments extends \dektrium\rbac\widgets\Assignments
{

    public function run()
    {
        $model = Yii::createObject([
            'class'   => Assignment::className(),
            'user_id' => $this->userId,
        ]);

        if ($model->load(\Yii::$app->request->post())) {
            $model->updateAssignments();
        }

        return $this->render('form', [
            'model' => $model,
        ]);
    }
}