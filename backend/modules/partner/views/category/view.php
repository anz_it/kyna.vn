<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model kyna\partner\models\Category */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Danh mục'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="becourse-view">

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'title',
            [
                'attribute' => 'course_id',
                'value' => (!empty($model->course)) ? $model->course->name : ''
            ],
            [
                'attribute' => 'partner_id',
                'value' => !empty($model->partner) ? $model->partner->name : ''
            ],
            'value',
            /*[
                'attribute' => 'status',
                'value' => $model->statusText
            ],*/
            //'is_deleted',
            'created_time:datetime',
            'updated_time:datetime',
        ],
    ]) ?>

</div>