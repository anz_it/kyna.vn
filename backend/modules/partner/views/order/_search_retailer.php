<?php

use yii\bootstrap\Html;
use yii\helpers\Url;
use kyna\base\assets\DateRangeAsset;
use kyna\order\models\Order;
use kyna\partner\models\Partner;
use common\helpers\ArrayHelper;

DateRangeAsset::register($this);

$method = isset($queryParams['shipping_method']) ? $queryParams['shipping_method'] : 'AUTO';

$allOrderStatuses = Order::getAllStatuses();
$allowedStatusLabels = $allOrderStatuses;
$initRetailerText = !empty($searchModel->retailer) ? $searchModel->retailer->user->profile->name : '';
$partners = ArrayHelper::map(Partner::findAllByRetailer(), 'id', 'name');
?>

<?= Html::beginForm(Url::toRoute([$this->context->action->id, 'status' => Yii::$app->request->get('status')]), 'get', [
    'class' => 'navbar-form navbar-left',
    'role' => 'search',
]) ?>
    <div class="form-group">
        <?= Html::textInput('s', isset($queryParams['s']) ? $queryParams['s'] : '', [
            'placeholder' => 'Tìm email, số điện thoại hoặc mã đơn hàng',
            'class' => 'form-control',
        ]) ?>
    </div>
    <div class="form-group">
        <?= Html::dropDownList('partner_id', Yii::$app->request->get('partner_id'), $partners, [
            'class' => 'form-control',
            'prompt' => 'Chọn đối tác',
            'id' => 'partner_id'
        ]); ?>
    </div>
    <div class="form-group">
        <?= Html::textInput('date-range', isset($queryParams['date-range']) ? $queryParams['date-range'] : '', [
            'placeholder' => 'Ngày mua',
            'class' => 'form-control',
            'style' => 'width: 170px;',
            'data-control' => 'daterangepicker',
            'data-start-date' => date('d/m/Y', isset($queryParams['from_date']) ? $queryParams['from_date'] : strtotime('last week')),
            'data-end-date' => date('d/m/Y', isset($queryParams['to_date']) ? $queryParams['to_date'] : strtotime('today')),
            'data-max-date' => date('d/m/Y', strtotime('today')),
        ]) ?>
    </div>

    <?= Html::submitButton('<i class="ion-android-search"></i> Tìm đơn hàng', ['class' => 'btn btn-success']) ?>

    <a class="btn btn-info" id="btn_export">
        <i class="fa fa-share"></i> Export file
    </a>

<?= Html::endForm(); ?>

<?php
$script = "
    ;(function($, window, document, undefined){
        $(document).ready(function(){        
            $('body').on('click', '#btn_export', function (event) {            
                event.preventDefault();
                var exportDialog = new BootstrapDialog();
                exportDialog.setTitle('Vui lòng nhập email nhận file export');
                exportDialog.setMessage($('<input id=\"email_export\" class=\"form-control\" placeholder=\"Email\" value=\"".Yii::$app->user->identity->email."\"></input>'));
                exportDialog.setType(BootstrapDialog.TYPE_SUCCESS);
                exportDialog.setButtons([{
                    label: 'Export',
                    action: function(dialog) {                       
                        var email = $('#email_export').val(); 
                        if (email == '') {
                            alert('Vui lòng nhập email');
                        }
                        var data = {email: email, type: 'export'};
                        $.post('', data, function (response) {
                            if (response.result) {
                                var bootstrapDialog = new BootstrapDialog();
                                bootstrapDialog.setMessage('Vui lòng kiểm tra email '+email+'. File Export sẽ được gửi đến trong vài phút.');
                                bootstrapDialog.setType(BootstrapDialog.TYPE_SUCCESS);
                                bootstrapDialog.open(); 
                                exportDialog.close();          
                            } else {
                                
                            }
                        });
                    }
                }]);
                exportDialog.open(); 
            });           
        });
    })(window.jQuery || window.Zepto, window, document);";
$css = "
    .modal-content .modal-header {
        border-radius: 0;
    }
    ";
?>
<?php
$this->registerCss($css);
$this->registerJs($script, \yii\web\View::POS_END, 'export-code');
?>
