<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;

use app\modules\order\assets\BackendAsset;
use common\helpers\ArrayHelper;
use kyna\partner\models\Category;
use kyna\partner\models\form\CreateOrderForm;

BackendAsset::register($this);

$this->title = ($model->payment_method == 'ghn') ? 'Thêm đơn hàng COD' : 'Thêm đơn hàng qua email';
$this->params['breadcrumbs'][] = ['label' => 'Orders', 'url' => ['/order/default/index']];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="becourse-form">
    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
    <div id="order-user">
        <?= $this->render('_user-editable', [
            'model' => $model,
            'form' => $form,
        ]) ?>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <?= $form->field($model, 'note')->textarea() ?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6">
            <?= $form->field($model, 'category_id')->dropDownList(ArrayHelper::map(Category::findAllWithRetailer(), 'id', 'title')) ?>
        </div>
        <?php if ($model->getScenario() == CreateOrderForm::SCENARIO_COD): ?>
        <div class="col-sm-6">
            <?= $form->field($model, 'activation_code')->textInput() ?>
        </div>
        <?php endif; ?>
    </div>
    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Thêm mới'), ['class' => 'btn btn-success']) ?>
        <?= Html::a('Hủy', ['retailer'], ['class'=>'btn btn-default']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>