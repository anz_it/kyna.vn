<?php

use yii\bootstrap\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\ButtonDropdown;
use yii\helpers\Url;
use yii\widgets\Pjax;
use kartik\grid\GridView;
use app\modules\order\models\ActionForm;
use app\modules\order\assets\BackendAsset;
use kyna\partner\models\Code;

BackendAsset::Register($this);
$this->title = $this->context->mainTitle;

$this->params['breadcrumbs'][] = $this->title;

$formatter = Yii::$app->formatter;
$user = Yii::$app->user;
?>

<div class="order-index">
    <nav class="navbar navbar-default">
        <?= $this->render('_search', ['queryParams' => $queryParams, 'searchModel' => $searchModel]) ?>
    </nav>
    <?php Pjax::begin(['id' => 'idPjaxReload']) ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            //'filterModel' => $searchModel,
            'columns' => [
                [
                    'header' => 'Đơn hàng',
                    'attribute' => 'order_date',
                    'value' => function ($model, $key, $index) use ($formatter, $statusLabelCss, $callStatuses) {
                        $html = '<strong>#' . $model->id . '</strong><br>';
                        $html .= '<time>' . $formatter->asDatetime(!empty($model->order_date) ? $model->order_date : null) . '</time><br>';
                        $html .= '<span class="label ' . $statusLabelCss[$model->status] . '">' . $model->statusLabel . '</span><br>';

                        if (array_key_exists($model->call_status, $callStatuses)) {
                            $html .= $callStatuses[$model->call_status];
                        }

                        return $html;
                    },
                    'format' => 'raw',
                ],
                [
                    'label' => 'Retailer',
                    'format' => 'raw',
                    'value' => function ($model) {
                        $code = Code::findOne(['serial' => $model->activation_code]);
                        return !empty($code) ? (!empty($code->retailer) ? $code->retailer->user->profile->name : '') : null;
                    }
                ],
                [
                    'header' => 'Thông tin người mua',
                    'format' => 'raw',
                    'value' => function ($model, $key, $index) use ($formatter) {
                        $user = $model->user;
                        if (empty($user)) {
                            return null;
                        }
                        if (!empty($user->userAddress)) {
                            $info = $user->userAddress->toString(false);
                        } else {
                            // get phone number from user register
                            $info = $user->profile->phone_number;
                        }
                        return $formatter->asEmail($user->email) . '<br>' . $info;
                    },
                ],
                [
                    'label' => 'Nguồn',
                    'format' => 'raw',
                    'value' => function ($model) {
                        $str = '';
                        $affiliate = $model->affiliateLastUser;
                        if ($affiliate && $affiliate->user && $affiliate->user->profile) {
                            $str .= 'Affiliate cuối: '.$affiliate->user->profile->name. '<br />';
                        }

                        $affiliate = $model->originalAffiliateUser;
                        if ($affiliate && $affiliate->user && $affiliate->user->profile) {
                            $str .= 'Original affiliate: '.$affiliate->user->profile->name. '<br />';
                        }

                        $affiliate = $model->originalAffiliateUser;
                        if ($affiliate && $affiliate->affiliateCategory) {
                            $str .= 'Affiliate category'. $affiliate->affiliateCategory->name;
                        }
                        return $str;
                    }
                ],
                [
                    'attribute' => 'details',
                    'label' => 'Sản phẩm',
                    'value' => 'detailsText',
                    'format' => 'raw',
                ],
                [
                    'attribute' => 'activation_code',
                    'label' => 'Serial Code',
                    'format' => 'raw',
                ],
                [
                    'header' => 'Giá bán',
                    'format' => 'raw',
                    'value' => function ($model) use ($formatter) {
                        $html = '<strong>' . $formatter->asCurrency($model->total) . '</strong>';

                        return $html;
                    },
                ],
                [
                    'attribute' => 'payment_method',
                    'label' => 'Hình thức thanh toán',
                    'format' => 'raw',
                    'value' => function ($model) {
                        return $model->isCod ? 'COD' : 'Online';
                    },
                ],
                [
                    'header' => 'Note',
                    'value' => function ($model) {
                        $str = '';

                        if (!empty($model->operator)) {
                            $str .= 'Chủ : ' . $model->operator->profile->name;
                        }

                        return $str;
                    }
                ],
            ],
        ]); ?>
    <?php Pjax::end(); ?>
</div>
