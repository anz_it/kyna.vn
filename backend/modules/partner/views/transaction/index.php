<?php

use yii\helpers\Html;
use yii\web\JsExpression;
use yii\helpers\Url;
use kartik\grid\GridView;
use kartik\select2\Select2;
use kyna\partner\models\Transaction;
use kyna\partner\models\Category;
use kyna\partner\models\Partner;
use common\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel kyna\partner\models\search\TransactionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$crudTitles = Yii::$app->params['crudTitles'];
$crudButtonIcons = Yii::$app->params['crudButtonIcons'];
$this->title = Yii::$app->controller->mainTitle;
$this->params['breadcrumbs'][] = $this->title;
$user = Yii::$app->user;
$categoryData = ArrayHelper::map(Category::find()->where(['status' => Category::STATUS_ACTIVE])->all(), 'id', 'title');
$initText = !empty($searchModel->created_by) ? $searchModel->user->profile->name : '';
?>
<div class="row transaction-index">
    <div class="col-xs-12">
        <div class="beuser-index">
            <!--<nav class="navbar navbar-default">
                <a class="btn btn-info navbar-btn navbar-left" id="btn_status">
                    <i class="fa fa-refresh"></i> Cập nhật trạng thái kích hoạt
                </a>
            </nav>-->
            <div class="box">
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],
                        'id',
                        'order_id',
                        'code',
                        [
                            'attribute' => 'partner_id',
                            'value' => function ($model) {
                                return !empty($model->partner) ? $model->partner->name : null;
                            },
                            'filter' => Select2::widget([
                                'model' => $searchModel,
                                'attribute' => 'partner_id',
                                'data' => ArrayHelper::map(Partner::findAllActive(true), 'id', 'name'),
                                'options' => ['placeholder' => $crudTitles['prompt']],
                                'pluginOptions' => [
                                    'allowClear' => true
                                ],
                            ])
                        ],
                        [
                            'attribute' => 'partner_category_id',
                            'header' => 'Sản phẩm',
                            'value' => function ($model) {
                                return $model->category ? $model->category->title : null;
                            },
                            'format' => 'html',
                            'filter' => Select2::widget([
                                'model' => $searchModel,
                                'attribute' => 'partner_category_id',
                                'data' => $categoryData,
                                'options' => ['placeholder' => $crudTitles['prompt']],
                                'pluginOptions' => [
                                    'allowClear' => true
                                ],
                                'hideSearch' => false,
                            ])
                        ],
                        'num_activations',
                        'num_used',
                        [
                            'attribute' => 'message',
                            'format' => 'html',
                            'value' => function ($model) {
                                return $model->message;
                            }
                        ],
                        [
                            'attribute' => 'created_by',
                            'value' => function ($model) {
                                if (empty($model->user)) {
                                    return null;
                                }
                                return !empty($model->user->profile) ? $model->user->profile->name : $model->user->email;
                            },
                            'filter' => Select2::widget([
                                'model' => $searchModel,
                                'initValueText' => $initText,
                                'attribute' => 'created_by',
                                'options' => ['placeholder' => $crudTitles['prompt']],
                                'pluginOptions' => [
                                    'allowClear' => true,
                                    'minimumInputLength' => 3,
                                    'language' => [
                                        'errorLoading' => new JsExpression("function () { return 'Chờ nhận kết quả...'; }"),
                                    ],
                                    'ajax' => [
                                        'url' => Url::toRoute(['/taamkru/api/search-user']),
                                        'dataType' => 'json',
                                        'data' => new JsExpression('function(params) { return {q:params.term}; }'),
                                    ],
                                    'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                                    'templateResult' => new JsExpression('function(res) { return res.text; }'),
                                    'templateSelection' => new JsExpression('function (res) { return res.text; }'),
                                ],
                            ])
                        ],
                        [
                            'attribute' => 'status',
                            'format' => 'html',
                            'value' => function ($model) {
                                return $model->statusHtml;
                            },
                            'contentOptions' => [
                                'style' => 'min-width: 115px;'
                            ],
                            'filter' => Html::activeDropDownList($searchModel, 'status', Transaction::listStatus(false), ['class' => 'form-control', 'prompt' => $crudTitles['prompt']]),
                        ],
                    ],
                ]); ?>
            </div>
        </div>
    </div>
</div>

<?php
$script = "
    ;(function($, window, document, undefined){
        $(document).ready(function(){        
            $('body').on('click', '#btn_status', function (event) {            
                event.preventDefault();
                $.post('/partner/transaction/update-status', {}, function (response) {
                    if (response.result) {
                        BootstrapDialog.show({
                            title: 'Cập nhật trạng thái',
                            message: 'Trạng thái kích hoạt sẽ được cập nhật trong vài phút', 
                            type: BootstrapDialog.TYPE_WARNING,
                            onhide: function(dialogRef){
                                window.location.reload();
                            },
                        });
                    }
                });
            });           
        });
    })(window.jQuery || window.Zepto, window, document);";
$css = "
    .modal-content .modal-header {
        border-radius: 0;
    }
    ";
?>
<?php
$this->registerCss($css);
$this->registerJs($script, \yii\web\View::POS_END, 'update-status');
?>
