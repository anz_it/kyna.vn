<?php

use yii\helpers\Html;
use common\helpers\ArrayHelper;
use kartik\form\ActiveForm;
use kartik\daterange\DateRangePicker;
use kartik\select2\Select2;
use kyna\partner\models\Partner;

?>

<?php
$form = ActiveForm::begin([
    'options' => ['class' => 'navbar-form navbar-right'],
    'method' => 'get',
]);
?>

<?=
$form->field($searchModel, 'partner_id')->widget(Select2::classname(), [
    'data' => ArrayHelper::map(Partner::findAllByRetailer(), 'id', 'name'),
    'hideSearch' => true,
    'options' => ['placeholder' => 'Chọn đối tác'],
    'pluginOptions' => [
        'allowClear' => true
    ],
])->label(false)->error(false)
?>

<?=
$form->field($searchModel, 'date_ranger', [
    'addon' => ['prepend' => ['content' => '<i class="glyphicon glyphicon-calendar"></i>']],
    'options' => ['class' => 'form-group'],
])->widget(DateRangePicker::classname(), [
    'useWithAddon' => true,
    'convertFormat' => true,
    'pluginOptions' => [
        'timePicker' => true,
        'locale' => [
            'format' => 'd/m/yy',
            'separator' => " - ", // after change this, must update in controller
        ],
    ]
])->label(false)->error(false)
?>

    <div class="form-group">
        <?= Html::submitButton('<i class="ion-android-search"></i> Lọc', ['class' => 'btn btn-default']) ?>
    </div>

<?php ActiveForm::end(); ?>