<?php

use yii\helpers\Html;
use yii\web\JsExpression;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\grid\GridView;
use kartik\select2\Select2;
use kartik\export\ExportMenu;
use kyna\partner\models\Retailer;
use kyna\partner\models\Partner;

/* @var $this yii\web\View */
/* @var $searchModel kyna\partner\models\search\RetailerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$crudTitles = Yii::$app->params['crudTitles'];
$crudButtonIcons = Yii::$app->params['crudButtonIcons'];
$this->title = Yii::$app->controller->mainTitle;
$this->params['breadcrumbs'][] = $this->title;
$user = Yii::$app->user;
$initRetailerText = !empty($searchModel->user_id) ? $searchModel->user->email : '';
$gridColumns = [
    'id',
    [
        'label' => 'Partner',
        'value' => function ($model) {
            return !($model->partner) ? $model->partner->name : '';
        },
    ],
    [
        'label' => 'Họ tên',
        'value' => function ($model) {
            if (empty($model->user)) {
                return null;
            }
            return !empty($model->user->profile) ? $model->user->profile->name : null;
        },
    ],
    [
        'label' => 'Email',
        'value' => function ($model) {
            return $model->user->email;
        },
    ],
    [
        'label' => 'Số thẻ bán',
        'value' => function ($model) {
            return $model->totalSellCodCode() . '/' . $model->totalCodCode();
        },
    ],
    [
        'label' => 'Số đơn hàng',
        'value' => function ($model) {
            return $model->totalOrder();
        },
    ],
    [
        'attribute' => 'status',
        'value' => function ($model) {
            return $model->statusText;
        },
    ],
    [
        'attribute' => 'created_time',
        'label' => 'Ngày tạo',
        'value' => function ($model) {
            return (!empty($model->created_time) ? Yii::$app->formatter->asDatetime($model->created_time) : null);
        }
    ],

];
?>

<div class="row code-index">
    <div class="col-xs-12">
        <div class="beuser-index">
            <?php if ($user->can('Partner.Retailer.Create')) : ?>
                <nav class="navbar navbar-default">
                    <?php $form = ActiveForm::begin([
                        'options' => [
                            'enctype' => 'multipart/form-data',
                            'class' => 'navbar-form navbar-left'
                        ],
                        'action' => Url::toRoute(['index'])
                    ]) ?>
                    <?php
                    $initText = !empty($model->user_id) ? $model->user->profile->name : '';
                    echo $form->field($model, 'user_id', [
                        'options' => ['class' => 'form-group', 'style' => 'width: 400px'],
                    ])->widget(Select2::classname(), [
                        'initValueText' => $initText,
                        'options' => ['placeholder' => '--Nhập Email--'],
                        'pluginOptions' => [
                            'allowClear' => true,
                            'minimumInputLength' => 3,
                            'language' => [
                                'errorLoading' => new JsExpression("function () { return 'Chờ nhận kết quả...'; }"),
                            ],
                            'ajax' => [
                                'url' => Url::toRoute(['/partner/api/search-user']),
                                'dataType' => 'json',
                                'data' => new JsExpression('function(params) { return {q:params.term}; }'),
                            ],
                            'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                            'templateResult' => new JsExpression('function(res) { return res.text; }'),
                            'templateSelection' => new JsExpression('function (res) { return res.text; }'),
                        ],
                    ])->label(false)->error(false); ?>
                    <?= $form->field($model, 'partner_id', [
                        'options' => ['class' => 'form-group', 'style' => 'width: 200px'],
                        ])->widget(Select2::classname(), [
                            'data' => ArrayHelper::map(Partner::findAllActive(true), 'id', 'name'),
                            'hideSearch' => true,
                            'options' => ['placeholder' => '--Chọn Partner--'],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ])->label(false)->error(false);
                    ?>
                    <?= Html::submitButton($crudButtonIcons['create'] . ' Thêm Retailer', ['class' => 'btn btn-success']) ?>
                    <?php ActiveForm::end() ?>
                    <?php
                    if ($model->hasErrors()) {
                        $errors = [];
                        foreach($model->errors as $error) {
                            $errors[] = $error[0];
                            break;
                        }
                        echo "<span class='navbar-text navbar-left error' style='color: #dd4b39;'>Lỗi: ". implode(', ', $errors) ."</span>";
                    }
                    ?>
                    <?= ExportMenu::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'columns' => $gridColumns,
                        'fontAwesome' => true,
                        'container' => [
                            'class' => 'btn-group navbar-btn navbar-left'
                        ],
                        'columnSelectorOptions'=>[
                            'label' => 'Export Cols',
                        ],
                        'dropdownOptions' => [
                            'label' => 'Export All',
                            'class' => 'btn btn-default'
                        ],
                        'showConfirmAlert' => false,
                        'showColumnSelector' => false,
                        'target' => ExportMenu::TARGET_BLANK,
                        'filename' => 'partner_retailer_export_'.date('Y-m-d_H-i', time()),
                        'exportConfig' => [
                            ExportMenu::FORMAT_PDF => false,
                            ExportMenu::FORMAT_HTML => false,
                        ]
                    ]);
                    ?>
                </nav>
            <?php endif; ?>
            <div class="box">
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => [
                        //['class' => 'yii\grid\SerialColumn'],

                        [
                            'attribute' => 'id',
                            'format' => 'html',
                            'value' => function ($model) {
                                return $model->id;
                            },
                            'contentOptions' => [
                                'style' => 'width: 100px;'
                            ],
                        ],
                        [
                            'attribute' => 'partner_id',
                            'value' => function ($model) {
                                return !empty($model->partner) ? $model->partner->name : null;
                            },
                            'filter' => Select2::widget([
                                'model' => $searchModel,
                                'attribute' => 'partner_id',
                                'data' => ArrayHelper::map(Partner::findAllActive(), 'id', 'name'),
                                'options' => ['placeholder' => $crudTitles['prompt']],
                                'pluginOptions' => [
                                    'allowClear' => true
                                ],
                            ])
                        ],
                        [
                            'label' => 'Họ tên',
                            'format' => 'html',
                            'value' => function ($model) {
                                if (empty($model->user)) {
                                    return null;
                                }
                                return !empty($model->user->profile) ? $model->user->profile->name : null;
                            },
                            'contentOptions' => [
                                'style' => 'min-width: 115px;'
                            ],
                        ],
                        [
                            'attribute' => 'user_id',
                            'label' => 'Email',
                            'value' => function ($model) {
                                return $model->user->email;
                            },
                            'filter' => Select2::widget([
                                'model' => $searchModel,
                                'initValueText' => $initRetailerText,
                                'attribute' => 'user_id',
                                'options' => ['placeholder' => $crudTitles['prompt']],
                                'pluginOptions' => [
                                    'allowClear' => true,
                                    'minimumInputLength' => 3,
                                    'language' => [
                                        'errorLoading' => new JsExpression("function () { return 'Chờ nhận kết quả...'; }"),
                                    ],
                                    'ajax' => [
                                        'url' => Url::toRoute(['/partner/api/search-user']),
                                        'dataType' => 'json',
                                        'data' => new JsExpression('function(params) { return {q:params.term}; }'),
                                    ],
                                    'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                                    'templateResult' => new JsExpression('function(res) { return res.text; }'),
                                    'templateSelection' => new JsExpression('function (res) { return res.text; }'),
                                ],
                            ])
                        ],
                        [
                            'label' => 'Số thẻ bán',
                            'format' => 'html',
                            'value' => function ($model) {
                                return $model->totalSellCodCode() . '/' . $model->totalCodCode();
                            },
                            'contentOptions' => [
                                'style' => 'min-width: 115px;'
                            ],
                        ],
                        [
                            'label' => 'Số đơn hàng',
                            'format' => 'html',
                            'value' => function ($model) {
                                return $model->totalOrder();
                            },
                            'contentOptions' => [
                                'style' => 'min-width: 115px;'
                            ],
                        ],
                        [
                            'attribute' => 'status',
                            'format' => 'raw',
                            'value' => function ($model) {
                                return $model->statusButton;
                            },
                            'filter' => Html::activeDropDownList($searchModel, 'status', Retailer::listStatus(false), ['class' => 'form-control', 'prompt' => $crudTitles['prompt']]),
                        ],
                        [
                            'template' => '{code}{delete}',
                            'class' => 'yii\grid\ActionColumn',
                            'contentOptions' => ['style' => 'width: 90px;'],
                            'visibleButtons' => [
                                'delete' => function ($model, $key) use ($user) {
                                    return false;
                                },
                                'code' => function ($model, $key) use ($user) {
                                    return $user->can('Partner.Code.Create');
                                }
                            ],
                            'buttons' => [
                                'delete' => function ($url, $model, $key) {
                                    return Html::a('Xóa', $url, [
                                        'class' => 'btn btn-sm btn-default btn-block',
                                        'data-confirm' => 'Bạn có chắc là sẽ xóa Retailer này không?',
                                        'data-method' => 'post',
                                    ]);
                                },
                                'code' => function ($url, $model, $key) {
                                    return Html::a('Thêm thẻ', Url::toRoute(['/partner/code/create?retailer_id=' . $model->id]), [
                                        'class' => 'btn btn-sm btn-default btn-block',
                                    ]);
                                },
                            ],
                        ],
                    ],
                ]); ?>
            </div>
        </div>
    </div>
</div>
