<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model kyna\partner\models\Partner */

$this->title = 'Thêm đối tác';
$this->params['breadcrumbs'][] = ['label' => Yii::$app->controller->mainTitle, 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title
?>
<div class="partner-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
