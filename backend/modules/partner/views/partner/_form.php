<?php

use yii\bootstrap\Html;
use yii\bootstrap\ActiveForm;
use kartik\select2\Select2;
use kyna\partner\models\Partner;

/* @var $this yii\web\View */
/* @var $model kyna\partner\models\Partner */
/* @var $form yii\widgets\ActiveForm */

$crudTitles = Yii::$app->params['crudTitles'];
?>

<div class="becourse-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <div class="row">
        <div class="col-lg-6">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-lg-6">
            <?= $form->field($model, 'class')->textInput(['maxlength' => true])->label('Class (Ex: taamkru, touch_n_math)') ?>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-6">
            <?= $form->field($model, 'description')->textarea(['maxlength' => true]) ?>
        </div>
        <!--<div class="col-lg-6">
            <?/*= $form->field($model, 'status')->widget(Select2::classname(), [
                'data' => Partner::listStatus(),
                'hideSearch' => true,
                'options' => ['placeholder' => $crudTitles['prompt']],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ])
            */?>
        </div>-->
    </div>

    <div class="row">
        <div class="col-lg-6">
            <?= $form->field($model, 'partner_type')->radioList(Partner::getPartnerTypes()) ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Thêm mới') : Yii::t('app', 'Cập nhật'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::a('Hủy', ['index'], ['class'=>'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
