<?php

use yii\helpers\Html;
use yii\grid\GridView;
use kyna\partner\models\Partner;

/* @var $this yii\web\View */
/* @var $searchModel kyna\partner\models\search\PartnerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$crudTitles = Yii::$app->params['crudTitles'];
$crudButtonIcons = Yii::$app->params['crudButtonIcons'];
$this->title = Yii::$app->controller->mainTitle;
$this->params['breadcrumbs'][] = $this->title;
$user = Yii::$app->user;
?>
<div class="row code-index">
    <div class="col-xs-12">
        <div class="beuser-index">
            <nav class="navbar navbar-default">
                <?= Html::a($crudButtonIcons['create'] . ' Thêm đối tác' , ['create'], ['class' => 'btn btn-success navbar-btn navbar-left']) ?>
            </nav>
            <div class="box">
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],

                        'id',
                        'name',
                        'description',
                        'class',
                        [
                            'attribute' => 'partner_type',
                            'value' => function ($model) use ($partnerTypes) {
                                return $partnerTypes[$model->partner_type];
                            },
                            'filter' => Html::activeDropDownList($searchModel, 'partner_type', Partner::getPartnerTypes(), ['class' => 'form-control', 'prompt' => $crudTitles['prompt']]),
                        ],
                        /*[
                            'attribute' => 'status',
                            'format' => 'html',
                            'value' => function ($model) {
                                return $model->statusHtml;
                            },
                            'contentOptions' => [
                                'style' => 'min-width: 115px;'
                            ],
                            'filter' => Html::activeDropDownList($searchModel, 'status', Partner::listStatus(false), ['class' => 'form-control', 'prompt' => $crudTitles['prompt']]),
                        ],*/
                        // 'is_deleted',
                        // 'created_time:datetime',
                        // 'updated_time:datetime',

                        [
                            'class' => 'yii\grid\ActionColumn',
                            'visibleButtons' => [
                                'update' => $user->can('Partner.Partner.Update'),
                                'view' => $user->can('Partner.Partner.View'),
                                'delete' => $user->can('Partner.Partner.Delete'),
                            ],
                        ],
                    ],
                ]); ?>
            </div>
        </div>
    </div>
</div>
