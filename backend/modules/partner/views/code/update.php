<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model kyna\partner\models\Code */

$crudTitles = Yii::$app->params['crudTitles'];

$this->title = $crudTitles['update'] . ' ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::$app->controller->mainTitle, 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Chỉnh sửa thẻ';
?>
<div class="code-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
