<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\JsExpression;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use kyna\partner\models\Code;
use kartik\select2\Select2;
use kyna\partner\models\Partner;

/* @var $this yii\web\View */
/* @var $model kyna\partner\models\Code */
/* @var $form yii\widgets\ActiveForm */
$crudTitles = Yii::$app->params['crudTitles'];
?>
<div class="becourse-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <div class="row">
        <div class="col-lg-4">
            <?= $form->field($model, 'auto_serial')->checkbox() ?>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-4">
            <?= $form->field($model, 'serial')->textInput(['maxlength' => true, 'disabled' => intval($model->auto_serial) ? true : false]) ?>
        </div>
        <div class="col-lg-4">
            <?= $form->field($model, 'prefix')->textInput(['maxlength' => true, 'disabled' => !$model->auto_serial]) ?>
        </div>
        <div class="col-lg-4">
            <?= $form->field($model, 'quantity')->textInput([
                'type' => 'number',
                'disabled' => !$model->auto_serial
            ]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-4">
            <?= $form->field($model, 'partner_id')->widget(Select2::classname(), [
                'data' => ArrayHelper::map(Partner::findAllActive(true), 'id', 'name'),
                'hideSearch' => true,
                'options' => ['placeholder' => $crudTitles['prompt']],
                'pluginOptions' => [
                    'allowClear' => true
                ],
                'pluginEvents' => [
                    "change" => "function() { 
                        $('#createcodeform-retailer_id').empty().trigger('change');  
                    }",
                ]
            ])
            ?>
        </div>
        <div class="col-lg-4">
            <?php
            $initText = empty($model->retailer) ? '' : $model->retailer->user->profile->name;
            echo $form->field($model, 'retailer_id')->widget(Select2::classname(), [
                'initValueText' => $initText, // set the initial display text
                'options' => ['placeholder' => $crudTitles['prompt']],
                'pluginOptions' => [
                    'allowClear' => true,
                    'minimumInputLength' => 3,
                    'language' => [
                        'errorLoading' => new JsExpression("function () { return 'Chờ nhận kết quả...'; }"),
                    ],
                    'ajax' => [
                        'url' => Url::toRoute(['/partner/api/search-retailer']),
                        'dataType' => 'json',
                        'data' => new JsExpression('function(params) { return {q:params.term, partnerID:$(\'#createcodeform-partner_id\').val()}; }'),
                    ],
                    'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                    'templateResult' => new JsExpression('function(res) { return res.text; }'),
                    'templateSelection' => new JsExpression('function (res) { return res.text; }'),
                ],
            ]);
            ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Thêm mới') : Yii::t('app', 'Cập nhật'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<?php
$script = "
    ;(function($, window, document, undefined){
        function updateURLParameter(url, param, paramVal){
            var newAdditionalURL = \"\";
            var tempArray = url.split(\"?\");
            var baseURL = tempArray[0];
            var additionalURL = tempArray[1];
            var temp = \"\";
            if (additionalURL) {
                tempArray = additionalURL.split(\"&\");
                for (var i=0; i<tempArray.length; i++){
                    if(tempArray[i].split('=')[0] != param){
                        newAdditionalURL += temp + tempArray[i];
                        temp = \"&\";
                    }
                }
            }       
            var rows_txt = temp + \"\" + param + \"=\" + paramVal;
            return baseURL + \"?\" + newAdditionalURL + rows_txt;
        }
        $(document).ready(function(){
            $('body').on('change', '#createcodeform-auto_serial', function (event) {
                var paramValue = $(this).prop('checked') ? '1' : '0';
                window.location.href = updateURLParameter(window.location.href, 'auto_serial', paramValue);
            });           
        });
    })(window.jQuery || window.Zepto, window, document);";
?>
<?php
$this->registerJs($script, \yii\web\View::POS_END, 'create-code');
?>

