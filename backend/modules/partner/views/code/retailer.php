<?php

use yii\helpers\Html;
use yii\web\JsExpression;
use yii\helpers\Url;
use kartik\grid\GridView;
use kartik\select2\Select2;
use kartik\date\DatePicker;
use kyna\partner\models\Code;
use kyna\partner\models\Partner;
use common\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel kyna\partner\models\search\CodeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$crudTitles = Yii::$app->params['crudTitles'];
$crudButtonIcons = Yii::$app->params['crudButtonIcons'];
$this->title = Yii::$app->controller->mainTitle;
$this->params['breadcrumbs'][] = $this->title;
$user = Yii::$app->user;
$initText = !empty($searchModel->retailer) ? $searchModel->retailer->user->profile->name : '';
?>
<div class="row code-index">
    <div class="col-xs-12">
        <div class="beuser-index">
            <nav class="navbar navbar-default">
                <!-- Button export -->
                <a class="btn btn-info navbar-btn navbar-left" id="btn_export">
                    <i class="fa fa-share"></i> Export file
                </a>
            </nav>
            <div class="box">
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => [
                        //['class' => 'yii\grid\SerialColumn'],

                        'id',
                        'serial',
                        [
                            'attribute' => 'created_time',
                            'format' => 'datetime',
                            'filter' =>  DatePicker::widget([
                                'model' => $searchModel,
                                'attribute' => 'created_time'
                            ])
                        ],
                        [
                            'attribute' => 'partner_id',
                            'value' => function ($model) {
                                return !empty($model->partner) ? $model->partner->name : null;
                            },
                            'filter' => Select2::widget([
                                'model' => $searchModel,
                                'attribute' => 'partner_id',
                                'data' => ArrayHelper::map(Partner::findAllByRetailer(), 'id', 'name'),
                                'options' => ['placeholder' => $crudTitles['prompt']],
                                'pluginOptions' => [
                                    'allowClear' => true
                                ],
                            ])
                        ],
                        [
                            'attribute' => 'category_id',
                            'value' => function ($codeModel) {
                                return (!empty($codeModel->category)) ? $codeModel->category->title : null;
                            }
                        ],
                        [
                            'attribute' => 'status',
                            'format' => 'html',
                            'value' => function ($model) {
                                return $model->statusHtml;
                            },
                            'contentOptions' => [
                                'style' => 'min-width: 115px;'
                            ],
                            'filter' => Html::activeDropDownList($searchModel, 'status', Code::listStatusRetailer(false), ['class' => 'form-control', 'prompt' => $crudTitles['prompt']]),
                        ],
                        [
                            'label' => 'Hình thức thanh toán',
                            'format' => 'html',
                            'value' => function ($model) {
                                return $model->paymentMethodText;
                            },
                            'contentOptions' => [
                                'style' => 'min-width: 115px;'
                            ],
                            'filter' => Html::activeDropDownList($searchModel, 'payment_method', Code::listPaymentMethod(false), ['class' => 'form-control', 'prompt' => $crudTitles['prompt']]),
                        ],
                    ],
                ]); ?>
            </div>
        </div>
    </div>
</div>
<?php
$script = "
    ;(function($, window, document, undefined){
        $(document).ready(function(){        
            $('body').on('click', '#btn_export', function (event) {            
                event.preventDefault();
                var exportDialog = new BootstrapDialog();
                exportDialog.setTitle('Vui lòng nhập email nhận file export');
                exportDialog.setMessage($('<input id=\"email_export\" class=\"form-control\" placeholder=\"Email\" value=\"".Yii::$app->user->identity->email."\"></input>'));
                exportDialog.setType(BootstrapDialog.TYPE_SUCCESS);
                exportDialog.setButtons([{
                    label: 'Export',
                    action: function(dialog) {                       
                        var email = $('#email_export').val(); 
                        if (email == '') {
                            alert('Vui lòng nhập email');
                        }
                        var data = {email: email, type: 'export'};
                        $.post('', data, function (response) {
                            if (response.result) {
                                var bootstrapDialog = new BootstrapDialog();
                                bootstrapDialog.setMessage('Vui lòng kiểm tra email '+email+'. File Export sẽ được gửi đến trong vài phút.');
                                bootstrapDialog.setType(BootstrapDialog.TYPE_SUCCESS);
                                bootstrapDialog.open(); 
                                exportDialog.close();          
                            } else {
                                
                            }
                        });
                    }
                }]);
                exportDialog.open(); 
            });
            $(\"body\").on(\"click\", \"#btn_assign\", function (e) {
                e.preventDefault();
                var modal = $(\"#modal\"),
                    url = this.href;
       
                modal.find(\".modal-content\").load(url, function (resp) {
                    modal.modal(\"show\");
                });
            });
        });
    })(window.jQuery || window.Zepto, window, document);";
$css = "
    .modal-content .modal-header {
        border-radius: 0;
    }
    ";
?>
<?php
$this->registerCss($css);
$this->registerJs($script, \yii\web\View::POS_END, 'export-code');
?>
