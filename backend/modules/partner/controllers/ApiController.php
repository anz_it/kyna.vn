<?php

namespace app\modules\partner\controllers;

use Yii;
use yii\web\Response;
use yii\db\Query;
use yii\db\Expression;
use kyna\user\models\Profile;
use kyna\user\models\User;
use kyna\user\models\AuthAssignment;
use kyna\partner\models\Retailer;
use app\components\controllers\Controller;

class ApiController extends Controller
{
    public function actionSearchUser($q = null)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $out = ['results' => []];
        if (!is_null($q)) {
            $query = new Query();

            $query->select([
                'id',
                't.email AS text',
                "t.email AS name"
            ])->from(User::tableName() . ' t')
                ->where("(t.email LIKE :q OR t.username LIKE :q)", [':q' => "%{$q}%"]);

            $query->groupBy('t.id');

            $command = $query->createCommand();
            $data = $command->queryAll();

            $out['results'] = array_values($data);
        }

        return $out;
    }

    public function actionSearchRetailer($q = null, $partnerID = null)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $out = ['results' => []];
        if (!is_null($q)) {
            $query = new Query();

            $retailerTable = Retailer::tableName();
            $query->select([
                $retailerTable . '.id',
                't.email AS text',
                "t.email AS name"
            ])->from(User::tableName() . ' t')
                ->join('INNER JOIN', $retailerTable, $retailerTable . '.user_id = t.id')
                ->where("(t.email LIKE :q OR t.username LIKE :q)", [':q' => "%{$q}%"]);

            if ($partnerID != null) {
                $query->andWhere([
                    $retailerTable . '.partner_id' => $partnerID,
                    $retailerTable . '.status' => Retailer::STATUS_ACTIVE,
                ]);
            }

            $query->groupBy('t.id');

            $command = $query->createCommand();
            $data = $command->queryAll();

            $out['results'] = array_values($data);
        }

        return $out;
    }
}
