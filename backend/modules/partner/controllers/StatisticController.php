<?php

namespace app\modules\partner\controllers;

use Yii;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;
use yii\filters\AccessControl;

use kyna\order\models\Order;
use kyna\partner\models\Retailer;
use kyna\partner\models\search\StatisticSearch;
use kyna\partner\models\search\OrderSearch;

use common\helpers\StringHelper;

/**
 * OrderController implements the CRUD actions for Order model.
 */
class StatisticController extends \yii\web\Controller
{

    public $layout;
    public $mainTitle = 'Thống kê';

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['retailer'],
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->user->isPartnerRetailer;
                        }
                    ],
                ],
            ],
        ];
    }

    public function actionRetailer()
    {
        $searchModel = $this->loadSearchModel();
        $retailers = Yii::$app->user->partnerRetailer;
        $searchModel->retailer_id_array = ArrayHelper::getColumn($retailers, 'id');
        $searchModel->status = Order::ORDER_STATUS_COMPLETE;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $totalOrder = $dataProvider->query->count();
        $totalPrice = !is_null($dataProvider->query->sum('total')) ? $dataProvider->query->sum('total') : 0;
        $totalUser = $dataProvider->query->count('DISTINCT(user_id)');

        return $this->render('retailer', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'totalOrder' => $totalOrder,
            'totalPrice' => $totalPrice,
            'totalUser' => $totalUser
        ]);
    }

    public function loadSearchModel()
    {
        $searchModel = new StatisticSearch();
        $searchModel->is_done_telesale_process = OrderSearch::BOOL_YES;

        return $searchModel;
    }

    protected function findModel($id)
    {
        if (($model = Order::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
