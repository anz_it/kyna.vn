<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

$crudTitles = Yii::$app->params['crudTitles'];
/* @var $this yii\web\View */
/* @var $model kyna\mana\models\Contact */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="contact-form">

    <?php $form = ActiveForm::begin(); ?>


    <div class="row">
        <div class="col-md-6">

            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'status')->dropDownList(\kyna\mana\models\Contact::listStatus(), ['prompt' => $crudTitles['prompt']]) ?>

        </div>

        <div class="col-md-6">

            <?= $form->field($model, 'subject_id')->dropDownList(ArrayHelper::map($listSubjects, 'id', 'name'), ['prompt' => $crudTitles['prompt']]) ?>

            <?= $form->field($model, 'certificate_id')->dropDownList(ArrayHelper::map($listCertificates, 'id', 'name'), ['prompt' => $crudTitles['prompt']]) ?>

            <?= $form->field($model, 'education_id')->dropDownList(ArrayHelper::map($listEducations, 'id', 'name'), ['prompt' => $crudTitles['prompt']]) ?>

        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? $crudTitles['create'] : $crudTitles['update'], ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::a($crudTitles['cancel'], ['index'],['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
