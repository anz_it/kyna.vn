<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model kyna\mana\models\Contact */

$this->title = Yii::$app->params['crudTitles']['update'] . ':# ' . $model->email;
$this->params['breadcrumbs'][] = ['label' => 'Học viên đăng ký', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->email, 'url' => ['view', 'id' => $model->email]];
$this->params['breadcrumbs'][] = Yii::$app->params['crudTitles']['update'];
?>
<div class="contact-update">

    <?= $this->render('_form', [
        'model' => $model,
        'listEducations' => $listEducations,
        'listCertificates' => $listCertificates,
        'listSubjects' => $listSubjects
    ]) ?>

</div>
