<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\widgets\Pjax;
use kyna\mana\models\Contact;

$crudTitles = Yii::$app->params['crudTitles'];
$crudButtonIcons = Yii::$app->params['crudButtonIcons'];

$this->title = 'Học viên đăng ký';
$this->params['breadcrumbs'][] = $this->title;

$user = Yii::$app->user;
?>
<div class="row">
    <div class="col-xs-12">
        <div class="contact-index">
            <p>
                <?php
                if ($user->can('Mana.Contact.Create')) {
                    echo Html::a($crudButtonIcons['create'] . ' ' . $crudTitles['create'], ['create'], ['class' => 'btn btn-success']);
                }
                ?>
            </p>

            <div class="box">
                <?php Pjax::begin(['timeout' => Yii::$app->params['timeOutPjax'], 'id' => 'idPjaxReload']) ?>
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],

                        'name',
                        'phone',
                        'email:email',
                        [
                            'attribute' => 'created_time',
                            'format' => 'datetime',
                            'filter' => false
                        ],
                        [
                            'attribute' => 'status',
                            'format' => 'raw',
                            'value' => function ($model) use ($user) {
                                if (!$user->can('Mana.Contact.Update')) {
                                    return $model->statusHtml;
                                }
                                return $model->statusButton;
                            },
                            'filter' => Html::activeDropDownList($searchModel, 'status', Contact::listStatus(false), ['class' => 'form-control', 'prompt' => $crudTitles['prompt']]),
                        ],
                        [
                            'class' => 'app\components\ActionColumnCustom',
                            'visibleButtons' => [
                                'update' => $user->can('Mana.Contact.Update'),
                                'view' => $user->can('Mana.Contact.View'),
                                'delete' => $user->can('Mana.Contact.Delete'),
                            ],
                        ],
                    ],
                ]); ?>
                <?php Pjax::end() ?>
            </div>
        </div>
    </div>
</div>
