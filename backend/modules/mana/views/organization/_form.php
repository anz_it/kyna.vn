<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kyna\mana\models\Education;

$crudTitles = Yii::$app->params['crudTitles'];
/* @var $this yii\web\View */
/* @var $model kyna\mana\models\Education */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="organization-form">

    <?php $form = ActiveForm::begin(); ?>

        <div class="row">
            <div class="col-md-6">

                <?= $form->field($model, 'name')->textInput(['maxlength' => true, 'onchange' => "$('#org-slug').val(convertToSlug($(this).val()))"]) ?>

                <?= $form->field($model, 'slug')->textInput(['maxlength' => true, 'id' => 'org-slug']) ?>

                <?= $form->field($model, 'description')->widget(\common\widgets\tinymce\TinyMce::className(), [
                    'layout' => 'advanced',
                ]) ?>

            </div>

            <div class="col-md-6">

                <?= $form->field($model, 'image_url')->textInput() ?>

                <?= $form->field($model, 'order')->textInput() ?>

                <?= $form->field($model, 'status')->dropDownList(\kyna\mana\models\Organization::listStatus(), ['prompt' => $crudTitles['prompt']]) ?>

            </div>
        </div>

        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? $crudTitles['create'] : $crudTitles['update'], ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            <?= Html::a($crudTitles['cancel'], ['index'],['class' => 'btn btn-default']) ?>
        </div>

    <?php ActiveForm::end(); ?>

</div>

<?= $this->render('//_partials/stringjs') ?>
