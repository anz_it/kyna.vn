<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model kyna\mana\models\Education */
$this->title = Yii::$app->params['crudTitles']['update'] . ':# ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Tổ chức cấp chứng chỉ', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::$app->params['crudTitles']['update'];
?>
<div class="education-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
