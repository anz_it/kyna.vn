<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\widgets\Pjax;
use kyna\mana\models\Organization;

$crudTitles = Yii::$app->params['crudTitles'];
$crudButtonIcons = Yii::$app->params['crudButtonIcons'];

$this->title = 'Tổ chức cấp chứng chỉ';
$this->params['breadcrumbs'][] = $this->title;

$user = Yii::$app->user;

?>
    <div class="row">
        <div class="col-xs-12">
            <div class="organization-index">
                <p>
                    <?php
                    if ($user->can('Mana.Organization.Create')) {
                        echo Html::a($crudButtonIcons['create'] . ' ' . $crudTitles['create'], ['create'], ['class' => 'btn btn-success']);
                    }
                    ?>
                </p>

                <div class="box">
                    <?php Pjax::begin(['timeout' => Yii::$app->params['timeOutPjax'], 'id' => 'idPjaxReload']) ?>
                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],

                            'id',
                            'name',
                            'slug',
                            [
                                'attribute' => 'image_url',
                                'format' => 'html',
                                'value' => function ($model) {
                                    return !empty($model->image_url) ? Html::img($model->image_url, ['width' => '100px']) : null;
                                },
                                'filter' => false,
                                'options' => ['class' => 'col-xs-1']
                            ],
                             'order',
                            [
                                'attribute' => 'status',
                                'format' => 'raw',
                                'value' => function ($model) use ($user) {
                                    if (!$user->can('Mana.Organization.Update')) {
                                        return $model->statusHtml;
                                    }
                                    return $model->statusButton;
                                },
                                'filter' => Html::activeDropDownList($searchModel, 'status', Organization::listStatus(false), ['class' => 'form-control', 'prompt' => $crudTitles['prompt']]),
                            ],
                            [
                                'class' => 'app\components\ActionColumnCustom',
                                'visibleButtons' => [
                                    'update' => $user->can('Mana.Organization.Update'),
                                    'view' => $user->can('Mana.Organization.View'),
                                    'delete' => $user->can('Mana.Organization.Delete'),
                                ],
                            ],
                        ],
                    ]); ?>
                    <?php Pjax::end() ?>
                </div>
            </div>
        </div>
    </div>
