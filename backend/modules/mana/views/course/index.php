<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\widgets\Pjax;
use kyna\mana\models\Course;

$crudTitles = Yii::$app->params['crudTitles'];
$crudButtonIcons = Yii::$app->params['crudButtonIcons'];

$this->title = 'Khóa học';
$this->params['breadcrumbs'][] = $this->title;

$user = Yii::$app->user;
?>
<div class="row">

    <div class="col-xs-12">
        <div class="course-index">
            <p>
                <?php
                if ($user->can('Mana.Course.Create')) {
                    echo Html::a($crudButtonIcons['create'] . ' ' . $crudTitles['create'], ['create'], ['class' => 'btn btn-success']);
                }
                ?>
            </p>

            <div class="box">
                <?php Pjax::begin(['timeout' => Yii::$app->params['timeOutPjax'], 'id' => 'idPjaxReload']) ?>
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],
                        [
                            'attribute' => 'course_id',
                            'format' => 'html',
                            'value' => function ($model) {
                                if ($model->kynaCourse) {
                                    return $model->kynaCourse->name;
                                }
                                return null;
                            },
                        ],
                        [
                            'attribute' => 'course_ref_id',
                            'format' => 'html',
                            'value' => function ($model) {
                                if ($model->kynaCourseReference) {
                                    return $model->kynaCourseReference->name;
                                }
                                return null;
                            },
                        ],
                        [
                            'attribute' => 'education_id',
                            'format' => 'html',
                            'value' => function ($model) {
                                if ($model->education) {
                                    return $model->education->name;
                                }
                                return null;
                            },
                        ],
                        [
                            'attribute' => 'certificate_id',
                            'format' => 'html',
                            'value' => function ($model) {
                                if ($model->certificate) {
                                    return $model->certificate->name;
                                }
                                return null;
                            },
                        ],
                        [
                            'attribute' => 'organization_id',
                            'format' => 'html',
                            'value' => function ($model) {
                                if ($model->organization) {
                                    return $model->organization->name;
                                }
                                return null;
                            },
                        ],
                        'order',
                        [
                            'attribute' => 'status',
                            'format' => 'raw',
                            'value' => function ($model) use ($user) {
                                if (!$user->can('Mana.Course.Update')) {
                                    return $model->statusHtml;
                                }
                                return $model->statusButton;
                            },
                            'filter' => Html::activeDropDownList($searchModel, 'status', Course::listStatus(false), ['class' => 'form-control', 'prompt' => $crudTitles['prompt']]),
                        ],
                        [
                            'class' => 'app\components\ActionColumnCustom',
                            'visibleButtons' => [
                                'update' => $user->can('Mana.Course.Update'),
                                'view' => $user->can('Mana.Course.View'),
                                'delete' => $user->can('Mana.Course.Delete'),
                            ],
                        ],
                    ],
                ]); ?>
                <?php Pjax::end() ?>
            </div>
        </div>
    </div>
</div>
