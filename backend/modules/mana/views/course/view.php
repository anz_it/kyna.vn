<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model kyna\mana\models\Course */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Khóa học', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$crudTitles = Yii::$app->params['crudTitles'];
?>
<div class="course-view">

    <p>
        <?= Html::a("<span class='glyphicon glyphicon-backward'></span> " . $crudTitles['back'], ['index'], ['class' => 'btn btn-info', 'icon' => 'glyphicon glyphicon-backward']) ?>
        <?= Html::a($crudTitles['update'], ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a($crudTitles['delete'], ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'course_id',
            'education_id',
            'certificate_id',
            'organization_id',
            'description_for:html',
            'condition:html',
            'learning_time:ntext',
            'learning_method:html',
            'learning_start_date:ntext',
            'order',
            'status',
            'created_time:datetime',
            'updated_time:datetime',
        ],
    ]) ?>

</div>
