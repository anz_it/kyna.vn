<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model kyna\mana\models\Course */

$this->title = Yii::$app->params['crudTitles']['update'] . ':# ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Khóa học', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::$app->params['crudTitles']['update'];
?>
<div class="course-update">

    <?= $this->render('_form', [
        'model' => $model,
        'listKynaCourses' => $listKynaCourses,
        'listSubjects' => $listSubjects,
        'listEducations' => $listEducations,
        'listCertificates' => $listCertificates,
        'listOrganizations' => $listOrganizations,
        'listTeachers' => $listTeachers
    ]) ?>

</div>
