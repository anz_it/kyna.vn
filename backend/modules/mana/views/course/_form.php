<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\widgets\tinymce\TinyMce;
use yii\helpers\ArrayHelper;

$crudTitles = Yii::$app->params['crudTitles'];
/* @var $this yii\web\View */
/* @var $model kyna\mana\models\Course */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="course-form">

    <?php $form = ActiveForm::begin(); ?>


    <div class="row">
        <div class="col-md-6">

            <?= $form->field($model, 'course_id')->dropDownList(ArrayHelper::map($listKynaCourses, 'id', 'name'), ['prompt' => $crudTitles['prompt']]) ?>
            
            <?= $form->field($model, 'education_id')->dropDownList(ArrayHelper::map($listEducations, 'id', 'name'), ['prompt' => $crudTitles['prompt']]) ?>

            <?= $form->field($model, 'certificate_id')->dropDownList(ArrayHelper::map($listCertificates, 'id', 'name'), ['prompt' => $crudTitles['prompt']]) ?>

            <?= $form->field($model, 'organization_id')->dropDownList(ArrayHelper::map($listOrganizations, 'id', 'name'), ['prompt' => $crudTitles['prompt']]) ?>

            <?= $form->field($model, 'condition')->widget(TinyMce::className(), [
                'layout' => 'advanced',
            ]) ?>


            <?= $form->field($model, 'learning_method')->widget(TinyMce::className(), [
                'layout' => 'advanced',
            ]) ?>

            <?= $form->field($model, 'description_for')->widget(TinyMce::className(), [
                'layout' => 'advanced',
            ]) ?>

        </div>

        <div class="col-md-6">

            <?= $form->field($model, 'course_ref_id')->dropDownList(ArrayHelper::map($listKynaCourses, 'id', 'name'), ['prompt' => $crudTitles['prompt']]) ?>

            <?= $form->field($model, 'learning_time')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'learning_start_date')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'order')->textInput() ?>

            <?= $form->field($model, 'teachers')->widget(\kartik\select2\Select2::className(), [
                'data' => ArrayHelper::map($listTeachers, 'id', 'name'),
                'options' => ['placeholder' => 'Chọn giảng viên', 'multiple' => true],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);
            ?>

            <?= $form->field($model, 'subjects')->widget(\kartik\select2\Select2::className(), [
                    'data' => ArrayHelper::map($listSubjects, 'id', 'name'),
                    'options' => ['placeholder' => 'Chọn chuyên ngành', 'multiple' => true],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
            ?>
            
            <?= $form->field($model, 'status')->dropDownList(\kyna\mana\models\Course::listStatus(), ['prompt' => $crudTitles['prompt']]) ?>

        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? $crudTitles['create'] : $crudTitles['update'], ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::a($crudTitles['cancel'], ['index'],['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
