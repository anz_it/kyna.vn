<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model kyna\mana\models\Course */

$this->title = Yii::$app->params['crudTitles']['create'];
$this->params['breadcrumbs'][] = ['label' => 'Khóa học', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="course-create">

    <?= $this->render('_form', [
        'model' => $model,
        'listKynaCourses' => $listKynaCourses,
        'listSubjects' => $listSubjects,
        'listEducations' => $listEducations,
        'listCertificates' => $listCertificates,
        'listOrganizations' => $listOrganizations,
        'listTeachers' => $listTeachers
    ]) ?>

</div>
