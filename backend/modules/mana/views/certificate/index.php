<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use kyna\mana\models\Certificate;

$crudTitles = Yii::$app->params['crudTitles'];
$crudButtonIcons = Yii::$app->params['crudButtonIcons'];

$this->title = 'Chứng chỉ';
$this->params['breadcrumbs'][] = $this->title;

$user = Yii::$app->user;
?>
<div class="row">

    <div class="col-xs-12">
        <div class="certificate-index">
            <p>
                <?php
                if ($user->can('Mana.Certificate.Create')) {
                    echo Html::a($crudButtonIcons['create'] . ' ' . $crudTitles['create'], ['create'], ['class' => 'btn btn-success']);
                }
                ?>
            </p>

            <div class="box">
                <?php Pjax::begin(['timeout' => Yii::$app->params['timeOutPjax'], 'id' => 'idPjaxReload']) ?>
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],

                        'id',
                        'name',
                        'slug',
                        [
                            'attribute' => 'image_url',
                            'format' => 'html',
                            'value' => function ($model) {
                                return !empty($model->image_url) ? Html::img($model->image_url, ['width' => '100px']) : null;
                            },
                            'filter' => false,
                            'options' => ['class' => 'col-xs-1']
                        ],
                        'order',
                        [
                            'attribute' => 'status',
                            'format' => 'raw',
                            'value' => function ($model) use ($user) {
                                if (!$user->can('Mana.Certificate.Update')) {
                                    return $model->statusHtml;
                                }
                                return $model->statusButton;
                            },
                            'filter' => Html::activeDropDownList($searchModel, 'status', Certificate::listStatus(false), ['class' => 'form-control', 'prompt' => $crudTitles['prompt']]),
                        ],
                        [
                            'class' => 'app\components\ActionColumnCustom',
                            'visibleButtons' => [
                                'update' => $user->can('Mana.Certificate.Update'),
                                'view' => $user->can('Mana.Certificate.View'),
                                'delete' => $user->can('Mana.Certificate.Delete'),
                            ],
                        ],
                    ],
                ]); ?>
                <?php Pjax::end() ?>
            </div>
        </div>
    </div>
</div>
