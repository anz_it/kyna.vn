<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model kyna\mana\models\Certificate */

$this->title = Yii::$app->params['crudTitles']['update'] . ':# ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Chứng chỉ', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::$app->params['crudTitles']['update'];
?>
<div class="certificate-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
