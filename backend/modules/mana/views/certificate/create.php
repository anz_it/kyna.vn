<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model kyna\mana\models\Certificate */

$this->title = Yii::$app->params['crudTitles']['create'];
$this->params['breadcrumbs'][] = ['label' => 'Chứng chỉ', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="certificate-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
