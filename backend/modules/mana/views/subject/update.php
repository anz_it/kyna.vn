<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model kyna\mana\models\Subject */

$this->title = Yii::$app->params['crudTitles']['update'] . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Chuyên ngành', 'url' => ['index']];

if (!empty($model->parent)) {
    $this->params['breadcrumbs'][] = ['label' => $model->parent->name, 'url' => ['view', 'id' => $model->parent->id]];
}

$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::$app->params['crudTitles']['update'];
?>
<div class="subject-update">

    <?= $this->render('_form', [
        'model' => $model,
        'subjects' => $subjects
    ]) ?>

</div>
