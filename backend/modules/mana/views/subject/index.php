<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\widgets\Pjax;
use kyna\mana\models\Subject;

$crudTitles = Yii::$app->params['crudTitles'];
$crudButtonIcons = Yii::$app->params['crudButtonIcons'];

$this->title = 'Chuyên ngành';
$this->params['breadcrumbs'][] = $this->title;

$user = Yii::$app->user;
?>
<div class="row">
    <div class="col-xs-12">
        <div class="subject-index">
            <p>
                <?php
                if ($user->can('Mana.Subject.Create')) {
                    echo Html::a($crudButtonIcons['create'] . ' ' . $crudTitles['create'], ['create'], ['class' => 'btn btn-success']);
                }
                ?>
            </p>

            <div class="box">
                <?php Pjax::begin(['timeout' => Yii::$app->params['timeOutPjax'], 'id' => 'idPjaxReload']) ?>
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],

                        [
                            'attribute' => 'id',
                            'contentOptions' => ['style' => 'width: 100px;']
                        ],
                        'name',
                        'slug',
                        [
                            'attribute' => 'image_url',
                            'format' => 'html',
                            'value' => function ($model) {
                                return !empty($model->image_url) ? Html::img($model->image_url, ['width' => '100px']) : null;
                            },
                            'filter' => false,
                            'options' => ['class' => 'col-xs-1']
                        ],
                        'order',
                        [
                            'attribute' => 'status',
                            'format' => 'raw',
                            'value' => function ($model) use ($user) {
                                if (!$user->can('Mana.Subject.Update')) {
                                    return $model->statusHtml;
                                }
                                return $model->statusButton;
                            },
                            'filter' => Html::activeDropDownList($searchModel, 'status', Subject::listStatus(false), ['class' => 'form-control', 'prompt' => $crudTitles['prompt']]),
                        ],
                        [
                            'class' => 'app\components\ActionColumnCustom',
                            'visibleButtons' => [
                                'update' => $user->can('Mana.Subject.Update'),
                                'view' => $user->can('Mana.Subject.View'),
                                'delete' => $user->can('Mana.Subject.Delete'),
                            ],
                        ],
                    ],
                ]); ?>
                <?php Pjax::end() ?>
            </div>
        </div>
    </div>
</div>