<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model kyna\mana\models\Subject */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Chuyên ngành', 'url' => ['index']];

if (!empty($model->parent)) {
    $this->params['breadcrumbs'][] = ['label' => $model->parent->name, 'url' => ['view', 'id' => $model->parent->id]];
}

$this->params['breadcrumbs'][] = $this->title;

$crudTitles = Yii::$app->params['crudTitles'];
$user = Yii::$app->user;
?>
<div class="row">
    <div class="col-md-12">
        <div class="subject-view">

            <p>
                <?php
                echo Html::a("<span class='glyphicon glyphicon-backward'></span> " . $crudTitles['back'], !empty($model->parent_id) ? ['view', 'id' => $model->parent_id] : ['/mana/subject'], [
                        'class' => 'btn btn-info',
                        'icon' => 'glyphicon glyphicon-backward'
                    ]) . ' ';
                if ($user->can('Mana.Subject.Create')) {
                    echo Html::a(Yii::$app->params['crudTitles']['create'], ['create', 'parent_id' => $model->id], ['class' => 'btn btn-success']);
                }
                ?>
            </p>
            <div class="box">
                <?php Pjax::begin(['id' => 'idPjaxReload']) ?>
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],
                        [
                            'attribute' => 'id',
                            'contentOptions' => ['style' => 'width: 100px;']
                        ],
                        'name',
                        'slug',
//                        'description:ntext',
                        [
                            'attribute' => 'image_url',
                            'format' => 'html',
                            'value' => function ($model) {
                                return !empty($model->image_url) ? Html::img($model->image_url, ['width' => '100px']) : null;
                            },
                            'filter' => false,
                            'options' => ['class' => 'col-xs-1']
                        ],
                        'order',
                        // 'type',
                        [
                            'attribute' => 'status',
                            'format' => 'raw',
                            'value' => function ($model) {
                                return $model->statusButton;
                            },
                            'filter' => Html::activeDropDownList($searchModel, 'status', \kyna\mana\models\Subject::listStatus(false), ['class' => 'form-control', 'prompt' => $crudTitles['prompt']]),
                        ],
                        // 'created_time:datetime',
                        // 'updated_time:datetime',

                        [
                            'class' => 'app\components\ActionColumnCustom',
                            'visibleButtons' => [
                                'update' => $user->can('Mana.Subject.Update'),
//                                'view' => $user->can('Course.Category.View'),
                                'delete' => $user->can('Mana.Subject.Delete'),
                            ],
                        ],
                    ],
                ]); ?>
                <?php Pjax::end() ?>
            </div>

        </div>
    </div>
</div>

