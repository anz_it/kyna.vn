<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model kyna\mana\models\Subject */

$this->title = Yii::$app->params['crudTitles']['create'];
$this->params['breadcrumbs'][] = ['label' => 'Chuyên ngành', 'url' => ['index']];

if (!empty($model->parent)) {
    $this->params['breadcrumbs'][] = ['label' => $model->parent->name, 'url' => ['view', 'id' => $model->parent->id]];
}

$this->params['breadcrumbs'][] = $this->title;
?>
<div class="subject-create">

    <?= $this->render('_form', [
        'model' => $model,
        'subjects' => $subjects
    ]) ?>

</div>
