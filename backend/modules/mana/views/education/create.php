<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model kyna\mana\models\Education */

$this->title = Yii::$app->params['crudTitles']['create'];
$this->params['breadcrumbs'][] = ['label' => 'Hệ đào tạo', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="education-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
