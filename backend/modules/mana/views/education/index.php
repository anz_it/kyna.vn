<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\widgets\Pjax;
use kyna\mana\models\Education;

$crudTitles = Yii::$app->params['crudTitles'];
$crudButtonIcons = Yii::$app->params['crudButtonIcons'];

$this->title = 'Hệ đào tạo';
$this->params['breadcrumbs'][] = $this->title;

$user = Yii::$app->user;

?>
    <div class="row">
        <div class="col-xs-12">
            <div class="education-index">
                <p>
                    <?php
                    if ($user->can('Mana.Education.Create')) {
                        echo Html::a($crudButtonIcons['create'] . ' ' . $crudTitles['create'], ['create'], ['class' => 'btn btn-success']);
                    }
                    ?>
                </p>

                <div class="box">
                    <?php Pjax::begin(['timeout' => Yii::$app->params['timeOutPjax'], 'id' => 'idPjaxReload']) ?>
                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],

                            'id',
                            'name',
                            'slug',
                            [
                                'attribute' => 'image_url',
                                'format' => 'html',
                                'value' => function ($model) {
                                    return !empty($model->image_url) ? Html::img($model->image_url, ['width' => '100px']) : null;
                                },
                                'filter' => false,
                                'options' => ['class' => 'col-xs-1']
                            ],
                             'order',
                            [
                                'attribute' => 'status',
                                'format' => 'raw',
                                'value' => function ($model) use ($user) {
                                    if (!$user->can('Mana.Education.Update')) {
                                        return $model->statusHtml;
                                    }
                                    return $model->statusButton;
                                },
                                'filter' => Html::activeDropDownList($searchModel, 'status', Education::listStatus(false), ['class' => 'form-control', 'prompt' => $crudTitles['prompt']]),
                            ],
                            [
                                'class' => 'app\components\ActionColumnCustom',
                                'visibleButtons' => [
                                    'update' => $user->can('Mana.Education.Update'),
                                    'view' => $user->can('Mana.Education.View'),
                                    'delete' => $user->can('Mana.Education.Delete'),
                                ],
                            ],
                        ],
                    ]); ?>
                    <?php Pjax::end() ?>
                </div>
            </div>
        </div>
    </div>
