<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model kyna\mana\models\Teacher */

$this->title = Yii::$app->params['crudTitles']['update'] . ':# ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Giảng viên', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::$app->params['crudTitles']['update'];
?>
<div class="teacher-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
