<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model kyna\mana\models\Teacher */
/* @var $form yii\widgets\ActiveForm */

$crudTitles = Yii::$app->params['crudTitles'];
?>

<div class="teacher-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'description')->widget(\common\widgets\tinymce\TinyMce::className(), [
                'layout' => 'advanced',
            ]) ?>
        </div>

        <div class="col-md-6">
            <?= $form->field($model, 'image_url')->textInput() ?>

            <?= $form->field($model, 'order')->textInput() ?>

            <?= $form->field($model, 'status')->dropDownList(\kyna\mana\models\Teacher::listStatus(), ['prompt' => $crudTitles['prompt']]) ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? $crudTitles['create'] : $crudTitles['update'], ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::a($crudTitles['cancel'], ['index'],['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
