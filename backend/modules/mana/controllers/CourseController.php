<?php

namespace app\modules\mana\controllers;

use Yii;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

use kyna\mana\models\Teacher;
use kyna\mana\models\Course;
use kyna\course\models\Course as KynaCourse;
use kyna\mana\models\Subject;
use kyna\mana\models\Certificate;
use kyna\mana\models\Education;
use kyna\mana\models\Organization;

use app\modules\mana\models\CourseSearch;
use app\components\controllers\Controller;

/**
 * CourseController implements the CRUD actions for Course model.
 */
class CourseController extends Controller
{
    
    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'view'],
                        'matchCallback' => function () {
                            return Yii::$app->user->can('Mana.Course.View');
                        },
                    ],
                    [
                        'allow' => true,
                        'actions' => ['create'],
                        'matchCallback' => function () {
                            return Yii::$app->user->can('Mana.Course.Create');
                        },
                    ],
                    [
                        'allow' => true,
                        'actions' => ['update', 'change-status'],
                        'matchCallback' => function () {
                            return Yii::$app->user->can('Mana.Course.Update');
                        },
                    ],
                    [
                        'allow' => true,
                        'actions' => ['delete'],
                        'matchCallback' => function () {
                            return Yii::$app->user->can('Mana.Course.Delete');
                        },
                    ],
                ],
            ],
        ]);
    }

    /**
     * Lists all Course models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CourseSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCreate()
    {
        $model = new Course();
        $model->loadDefaultValues();
        $listKynaCourses = KynaCourse::find()->all();
        $listSubjects = Subject::findAllActive();
        $listEducations = Education::findAllActive();
        $listCertificates = Certificate::findAllActive();
        $listOrganizations = Organization::findAllActive();
        $listTeachers = Teacher::findAllActive();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', 'Thêm thành công.');
            if (isset(Yii::$app->request->post('Course')['teachers']) && Yii::$app->request->post('Course')['teachers']) {
                foreach (Yii::$app->request->post('Course')['teachers'] as $id) {
                    $teacher = Teacher::findOne($id);
                    $model->link('teachers', $teacher);
                }
            }

            if (isset(Yii::$app->request->post('Course')['subjects']) && Yii::$app->request->post('Course')['subjects']) {
                foreach (Yii::$app->request->post('Course')['subjects'] as $id) {
                    $subject = Subject::findOne($id);
                    $model->link('subjects', $subject);
                }
            }
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'listKynaCourses' => $listKynaCourses,
                'listSubjects' => $listSubjects,
                'listEducations' => $listEducations,
                'listCertificates' => $listCertificates,
                'listOrganizations' => $listOrganizations,
                'listTeachers' => $listTeachers
            ]);
        }
    }

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $listKynaCourses = KynaCourse::find()->all();
        $listSubjects = Subject::findAllActive();
        $listEducations = Education::findAllActive();
        $listCertificates = Certificate::findAllActive();
        $listOrganizations = Organization::findAllActive();
        $listTeachers = Teacher::findAllActive();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', 'Cập nhật thành công.');
            if (isset(Yii::$app->request->post('Course')['teachers']) && Yii::$app->request->post('Course')['teachers']) {
                // remove old teacher
                foreach ($model->teachers as $oldTeacher) {
                    if (!in_array($oldTeacher->id, Yii::$app->request->post('Course')['teachers'])) {
                        $model->unlink('teachers', $oldTeacher, true);
                    }
                }

                // add new teacher if not exists in relation table
                foreach (Yii::$app->request->post('Course')['teachers'] as $id) {
                    $teacher = Teacher::findOne($id);
                    if (!in_array($teacher->id, ArrayHelper::map($model->teachers, 'id', 'id'))) {
                        $model->link('teachers', $teacher);
                    }
                }
            }

            if (isset(Yii::$app->request->post('Course')['subjects']) && Yii::$app->request->post('Course')['subjects']) {

                    // remove old subject
                    foreach ($model->subjects as $oldSubject) {
                        if (!in_array($oldSubject->id, Yii::$app->request->post('Course')['subjects'])) {
                            $model->unlink('subjects', $oldSubject, true);
                        }
                    }

                    // add new subject if not exists in relation table
                    foreach (Yii::$app->request->post('Course')['subjects'] as $id) {
                        $subject = Subject::findOne($id);
                        if (!in_array($subject->id, ArrayHelper::map($model->subjects, 'id', 'id'))) {
                            $model->link('subjects', $subject);
                        }
                    }
            }
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'listKynaCourses' => $listKynaCourses,
                'listSubjects' => $listSubjects,
                'listEducations' => $listEducations,
                'listCertificates' => $listCertificates,
                'listOrganizations' => $listOrganizations,
                'listTeachers' => $listTeachers
            ]);
        }
    }

    /**
     * Finds the Course model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Course the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Course::findOne($id)) !== null) {
            $model->subjects = $model->getSubjects()->all();
            $model->teachers = $model->getTeachers()->all();
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
