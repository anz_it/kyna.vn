<?php

namespace app\modules\mana\controllers;

use Yii;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

use kyna\mana\models\Certificate;
use kyna\mana\models\Education;
use kyna\mana\models\Subject;
use kyna\mana\models\Contact;
use app\modules\mana\models\ContactSearch;
use app\components\controllers\Controller;


/**
 * ContactController implements the CRUD actions for Contact model.
 */
class ContactController extends Controller
{
    
    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'view'],
                        'matchCallback' => function () {
                            return Yii::$app->user->can('Mana.Contact.View');
                        },
                    ],
                    [
                        'allow' => true,
                        'actions' => ['create'],
                        'matchCallback' => function () {
                            return Yii::$app->user->can('Mana.Contact.Create');
                        },
                    ],
                    [
                        'allow' => true,
                        'actions' => ['update', 'change-status'],
                        'matchCallback' => function () {
                            return Yii::$app->user->can('Mana.Contact.Update');
                        },
                    ],
                    [
                        'allow' => true,
                        'actions' => ['delete'],
                        'matchCallback' => function () {
                            return Yii::$app->user->can('Mana.Contact.Delete');
                        },
                    ],
                ],
            ],
        ]);
    }

    /**
     * Lists all Contact models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ContactSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCreate()
    {
        $model = new Contact();
        $model->loadDefaultValues();
        $listSubjects = Subject::findAllActive();
        $listCertificates = Certificate::findAllActive();
        $listEducations = Education::findAllActive();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', 'Thêm thành công.');
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'listEducations' => $listEducations,
                'listCertificates' => $listCertificates,
                'listSubjects' => $listSubjects
            ]);
        }
    }

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $listSubjects = Subject::findAllActive();
        $listCertificates = Certificate::findAllActive();
        $listEducations = Education::findAllActive();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', 'Cập nhật thành công.');
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'listEducations' => $listEducations,
                'listCertificates' => $listCertificates,
                'listSubjects' => $listSubjects
            ]);
        }
    }

    /**
     * Finds the Contact model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Contact the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Contact::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
