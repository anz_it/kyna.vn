<?php

namespace app\modules\mana\controllers;

use Yii;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

use kyna\mana\models\CourseSubject;
use kyna\mana\models\Subject;
use app\modules\mana\models\SubjectSearch;
use app\components\controllers\Controller;

/**
 * SubjectController implements the CRUD actions for Subject model.
 */
class SubjectController extends Controller
{
    
    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'view'],
                        'matchCallback' => function () {
                            return Yii::$app->user->can('Mana.Subject.View');
                        },
                    ],
                    [
                        'allow' => true,
                        'actions' => ['create'],
                        'matchCallback' => function () {
                            return Yii::$app->user->can('Mana.Subject.Create');
                        },
                    ],
                    [
                        'allow' => true,
                        'actions' => ['update', 'change-status'],
                        'matchCallback' => function () {
                            return Yii::$app->user->can('Mana.Subject.Update');
                        },
                    ],
                    [
                        'allow' => true,
                        'actions' => ['delete'],
                        'matchCallback' => function () {
                            return Yii::$app->user->can('Mana.Subject.Delete');
                        },
                    ],
                ],
            ],
        ]);
    }

    /**
     * Lists all Subject models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SubjectSearch();
        $searchModel->parent_id = 0;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Subject model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $searchModel = new SubjectSearch();
        $searchModel->parent_id = $id;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('view', [
            'model' => $this->findModel($id),
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new Subject model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Subject();
        $model->loadDefaultValues();

        $subjects = Subject::find(['status' => Subject::STATUS_ACTIVE])->all();

        $get = Yii::$app->request->get();
        if (!empty($get['parent_id'])) {
            $parent = Subject::findOne(['id' => $get['parent_id']]);
            if (empty($parent)) {
                throw new \yii\web\HttpException(404);
            }
            $model->parent_id = $parent->id;
        }

        if ($model->load(Yii::$app->request->post())) {
            if (empty($model->parent_id)) {
                $model->parent_id = 0;
            }
            if ($model->save()) {
                Yii::$app->session->setFlash('success', 'Thêm thành công.');
                return $this->redirect(['view', 'id' => !empty($model->parent_id) ? $model->parent_id : $model->id]);
            } else {
                Yii::$app->session->setFlash('', 'Thêm thành công.');
            }
        }
        return $this->render('create', [
            'model' => $model,
            'subjects' => $subjects
        ]);
    }

    /**
     * Updates an existing Subject model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $subjects = Subject::findAllActive();

        if ($model->load(Yii::$app->request->post())) {
            if (empty($model->parent_id)) {
                $model->parent_id = 0;
            }
            if ($model->save()) {
                Yii::$app->session->setFlash('success', 'Cập nhật thành công.');
                return $this->redirect(['view', 'id' => !empty($model->parent_id) ? $model->parent_id : $model->id]);
            }
        }
        return $this->render('update', [
            'model' => $model,
            'subjects' => $subjects
        ]);
    }

    /**
     * Deletes an existing Subject model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $id = $model->id;
        if ($id) {
            $model->delete();
            CourseSubject::deleteAll(['subject_id' => $id]);
        }

        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $ret = [
                'success' => true,
                'message' => 'Đã xóa thành công',
            ];
            return $ret;
        } else {
            Yii::$app->session->setFlash('warning', 'Đã xóa');
            return $this->redirect(!empty($model->parent_id) ? ['view', 'id' => $model->parent_id] : ['index']);
        }
    }

    /**
     * Finds the Subject model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Subject the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Subject::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
