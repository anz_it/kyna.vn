<?php

namespace app\modules\mana\controllers;

use yii\web\Controller;

/**
 * Default controller for the `mana` module
 */
class DefaultController extends \app\components\controllers\Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
}
