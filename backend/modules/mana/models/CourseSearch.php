<?php

namespace app\modules\mana\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use kyna\mana\models\Course;

/**
 * CourseSearch represents the model behind the search form about `kyna\mana\models\Course`.
 */
class CourseSearch extends Course
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'course_id', 'course_ref_id', 'education_id', 'certificate_id', 'organization_id', 'order', 'status', 'created_time', 'updated_time'], 'integer'],
            [['description_for', 'condition', 'learning_time', 'learning_method', 'learning_start_date'], 'safe'],
            [['course_id', 'course_ref_id', 'education_id', 'certificate_id', 'organization_id'], 'trim'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Course::find()->joinWith(['education', 'certificate', 'organization']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'course_id' => $this->course_id,
            'course_ref_id' => $this->course_ref_id,
            'education_id' => $this->education_id,
            'certificate_id' => $this->certificate_id,
            'organization_id' => $this->organization_id,
            'order' => $this->order,
            'status' => $this->status,
            'created_time' => $this->created_time,
            'updated_time' => $this->updated_time,
        ]);

        $query->andFilterWhere(['like', 'description_for', $this->description_for])
            ->andFilterWhere(['like', 'condition', $this->condition])
            ->andFilterWhere(['like', 'learning_time', $this->learning_time])
            ->andFilterWhere(['like', 'learning_method', $this->learning_method])
            ->andFilterWhere(['like', 'learning_start_date', $this->learning_start_date]);

        return $dataProvider;
    }
}
