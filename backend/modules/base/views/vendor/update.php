<?php

use yii\bootstrap\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Vendor */

$this->title = 'Update Vendor: '.' '.$model->name;
$this->params['breadcrumbs'][] = ['label' => 'Vendors', 'url' => ['index']];
$this->params['breadcrumbs'][] = $model->name;
?>
<div class="vendor-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
