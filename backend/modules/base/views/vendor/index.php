<?php

use yii\bootstrap\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use kyna\base\models\Vendor;
use common\widgets\datacolumn\DataColumn;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Vendors';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vendor-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Vendor', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?php $form = ActiveForm::begin(); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],

            //'id',
            'name',
            'alias',
            [
                'class' => DataColumn::className(),
                'header' => 'Loại vendor',
                'control' => 'dropdown',
                'attribute' => 'vendor_type',
                'optionSet' => Vendor::getVendorTypes(),
                'options' => ['style' => 'width: 250px'],
            ],
            [
                'class' => DataColumn::className(),
                'header' => 'Tình trạng hoạt động',
                'control' => 'checkbox',
                'attribute' => 'status',
                'optionSet' => Vendor::getStatuses(),
                'options' => ['style' => 'width: 250px'],
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{settings} {update} {delete}',
                'buttons' => [
                    'settings' => function ($url, $model, $key) {
                        $url = Url::toRoute(['/base/vendor/settings/', 'id' => $model->id]);

                        return Html::a('<i class="fa fa-cogs"></i>', $url);
                    },
                ],
            ],
        ],
    ]); ?>
    <?php ActiveForm::end(); ?>
</div>
