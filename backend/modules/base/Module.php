<?php

namespace app\modules\base;

class Module extends \kyna\base\BaseModule
{
    public $controllerNamespace = 'app\modules\base\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
