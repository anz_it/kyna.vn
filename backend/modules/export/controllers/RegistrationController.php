<?php

namespace app\modules\export\controllers;

use Yii;
use yii\data\Pagination;
use yii\web\Response;

use app\components\ActiveDataProvider;
use app\components\controllers\Controller;
use app\modules\commission\models\Order;

class RegistrationController extends Controller
{

    public function actionIndex()
    {
        $params = $this->_loadParams();
        $sql = $this->_getSql($params);
        $count  = Order::findBySql($sql)->count();

        $pagination = new Pagination();
        $pagination->totalCount = $count;

        $query = Order::findBySql($sql. " LIMIT {$pagination->limit} OFFSET {$pagination->offset}");

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'attributes' => ['order_date', 'id'],
                'defaultOrder' => [
                    'order_date' => SORT_DESC,
                    'id' => SORT_DESC
                ]
            ],
            'pagination' => $pagination
        ]);

        if (Yii::$app->request->isAjax && Yii::$app->request->post('type', null) == 'export') {
            $sql = str_replace('`', '', $sql);
            
            $email = empty(Yii::$app->request->post('email')) ? Yii::$app->user->identity->email : Yii::$app->request->post('email');
            // execute command in console
            $rootPath = Yii::getAlias('@root');
            $exportLog = Yii::getAlias('@console/runtime/export_registration.log');
            $exportErrorLog = Yii::getAlias('@console/runtime/export_registration-error.log');
            // check log file exist
            self::_checkExistFile($exportLog);
            self::_checkExistFile($exportErrorLog);

            $command = "php {$rootPath}/yii export/run registration \"{$sql}\" {$email}" . " > {$exportLog} 2>>{$exportErrorLog} &";
            exec($command);
            
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'result' => true,
            ];
        }

        return $this->render('index', [
            'queryParams' => $params,
            'dataProvider' => $dataProvider
        ]);
    }

    private function _getSql($params)
    {
        if (!empty($params['ids'])) {
            $ids = implode(',', $params['ids']);
        } else {
            $ids = '-1';
        }
        $ors = [];

        foreach ($params['ids'] as $id) {
            $ors[] = "FIND_IN_SET($id, REPLACE(`list_course_ids`, ' ', '')) > 0";
        }

        $sql = "SELECT * FROM (SELECT o.id, o.affiliate_id, o.order_date, o.status, o.user_id, u.email, p.phone_number, p.name as full_name, NULL as street_address, NULL as form_name, NULL as amount, od.course_id, o.is_paid FROM `orders` `o` LEFT JOIN `order_details` od ON od.order_id = o.id LEFT JOIN `user` u ON u.id = o.user_id LEFT JOIN `profile` p ON p.user_id = o.user_id WHERE o.order_date >= {$params['from_date']} AND o.order_date <= {$params['to_date']} AND (od.course_id IN ({$ids}) OR course_combo_id IN ({$ids}))"
            . "UNION SELECT NULL as id, affiliate_id, `created_time` as order_date, 1 as status, NULL as user_id, email, phone_number, full_name, street_address, form_name, amount, course_id, 0 as is_paid FROM user_telesales WHERE created_time >= {$params['from_date']} AND created_time <= {$params['to_date']} AND (course_id IN ({$ids})" . (!empty($ors) ? (" OR " . implode(' OR ', $ors)) : '') . ")) a ORDER BY a.order_date DESC";

        return $sql;
    }

    private function _loadParams()
    {
        $queryParams = [
            'ids' => Yii::$app->request->get('ids', [])
        ];

        if ($range = Yii::$app->request->get('date-range')) {
            $dateRange = explode(' - ', $range);
            $beginDate = date_create_from_format('d/m/Y H:i', $dateRange[0] . ' 00:00');
            $endDate = date_create_from_format('d/m/Y H:i', $dateRange[1] . ' 23:59');

            $queryParams['from_date'] = $beginDate->getTimestamp();
            $queryParams['to_date'] = $endDate->getTimestamp();
            $queryParams['date-range'] = $range;
        }

        if (!isset($queryParams['to_date'])) {
            $queryParams['to_date'] = strtotime('tomorrow 0:00');
        }
        if (!isset($queryParams['from_date'])) {
            $queryParams['from_date'] = $queryParams['to_date'] - 24 * 7 * 3600;
        }

        return $queryParams;
    }

    private function _checkExistFile ($file)
    {
        clearstatcache();

        if (!file_exists($file)) {
            touch($file);
        }
    }
}