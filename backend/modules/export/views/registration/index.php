<?php

use kartik\grid\GridView;
use yii\web\View;

use app\modules\commission\models\Order;

$statusLabelCss = Order::getStatusLabelCss();
$statusLabels = Order::getStatusLabels();

$this->title = 'Danh sách học viên đăng ký';

$formatter = Yii::$app->formatter;
$gridColumns = [
    'id',
    [
        'header' => 'Ngày đăng ký',
        'attribute' => 'order_date',
        'value' => function ($model, $key, $index) use ($formatter) {
            $html = '<time>' . $formatter->asDatetime(!empty($model->order_date) ? $model->order_date : null) . '</time><br>';

            return $html;
        },
        'format' => 'raw',
    ],
    'full_name',
    'email',
    'phone_number',
    [
        'attribute' => 'is_paid',
        'label' => 'Đã thanh toán',
        'value' => function ($model) {
            return $model->is_paid ? Order::BOOL_YES_TEXT : Order::BOOL_NO_TEXT;
        }
    ],
    [
        'attribute' => 'course_id',
        'label' => 'Khóa học',
        'value' => function ($model) {
            return $model->course != null ? $model->course->name : $model->form_name;
        }
    ],
    [
        'label' => 'Trạng thái',
        'format' => 'raw',
        'value' => function ($model) use ($statusLabelCss, $statusLabels) {
            if (isset($statusLabelCss[$model->status])) {
                return '<span class="label ' . (isset($statusLabelCss[$model->status]) ? $statusLabelCss[$model->status] : '') . '">' . $statusLabels[$model->status] . '</span>';
            } else {
                return $model->status;
            }

        }
    ],
];
?>

<nav class="navbar navbar-default">
    <?php echo $this->render('_search', ['queryParams' => $queryParams]) ?>
</nav>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => $gridColumns,
]) ?>
<?php
$script = "
    ;(function($, window, document, undefined){
        $(document).ready(function(){
            $('body').on('click', '.export-xls', function (event) {            
                event.preventDefault();
                var courseType = $(this).data('type');
                var exportDialog = new BootstrapDialog();
                exportDialog.setTitle('Vui lòng nhập email nhận file export');
                exportDialog.setMessage($('<input id=\"email_export\" class=\"form-control\" placeholder=\"Email\" value=\"".Yii::$app->user->identity->email."\"></input>'));
                exportDialog.setType(BootstrapDialog.TYPE_SUCCESS);
                exportDialog.setButtons([{
                    label: 'Export',
                    action: function(dialog) {                       
                        var email = $('#email_export').val(); 
                        if (email == '') {
                            alert('Vui lòng nhập email');
                        }
                        var data = {email: email, type: 'export', course_type: courseType};
                        $.post('', data, function (response) {
                            if (response.result) {
                                var bootstrapDialog = new BootstrapDialog();
                                bootstrapDialog.setMessage('Vui lòng kiểm tra email '+email+'. File Export sẽ được gửi đến trong vài phút.');
                                bootstrapDialog.setType(BootstrapDialog.TYPE_SUCCESS);
                                bootstrapDialog.open(); 
                                exportDialog.close();          
                            } else {
                                
                            }
                        });
                    }
                }]);
                exportDialog.open(); 
            });
        });
    })(window.jQuery || window.Zepto, window, document);";
$css = "
    .modal-content .modal-header {
        border-radius: 0;
    }
    ";
?>
<?php
$this->registerCss($css);
$this->registerJs($script, View::POS_END, 'export-user-care');
?>