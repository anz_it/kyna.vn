<?php

use yii\bootstrap\Html;
use yii\helpers\Url;
use yii\web\JsExpression;

use yii\helpers\ArrayHelper;
use kyna\base\assets\DateRangeAsset;
use kyna\course\models\Course;
use kartik\select2\Select2;

DateRangeAsset::register($this);

$fromDate = date('d/m/Y', $queryParams['from_date']);
$toDate = date('d/m/Y', $queryParams['to_date']);

$initData = ArrayHelper::map(Course::find()->where(['id' => $queryParams['ids']])->select(['id', 'name'])->all(), 'id', 'name');
?>

<?= Html::beginForm(Url::toRoute(['/export/registration/index']), 'get', [
    'class' => 'navbar-form navbar-left',
    'role' => 'search',
]) ?>
<div class="form-group">
    <?= Select2::widget([
        'data' => $initData,
        'value' => $queryParams['ids'],
        'name' => 'ids',
        'options' => [
            'multiple' => true,
            'placeholder' => '-- Chọn danh sách khóa học --',
            'class' => 'form-control'
        ],
        'showToggleAll' => false,
        'pluginOptions' => [
            'width' => '400px',
            //'tags' => true,
            'allowClear' => true,
            'minimumInputLength' => 3,
            'language' => [
                'errorLoading' => new JsExpression("function () { return 'Chờ nhận kết quả...'; }"),
            ],
            'ajax' => [
                'url' => Url::toRoute(['/course/api/search', 'auto' => true]),
                'dataType' => 'json',
                'data' => new JsExpression('function(params) { return {q: params.term, course_type: "all"}; }'),
            ],
            'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
            'templateResult' => new JsExpression('function(teacher) { return teacher.text; }'),
            'templateSelection' => new JsExpression('function (teacher) { return teacher.text; }'),
        ],
    ]) ?>
</div>
<div class="form-group">
    <?= Html::textInput('date-range', isset($queryParams['date-range']) ? $queryParams['date-range'] : '', [
        'placeholder' => 'Ngày mua',
        'class' => 'form-control',
        'style' => 'width: 170px;',
        'data-control' => 'daterangepicker',
        'data-start-date' => $fromDate,
        'data-end-date' => $toDate,
        'data-max-date' => date('d/m/Y', strtotime('today')),
    ]) ?>
</div>

<?= Html::submitButton('<i class="ion-android-search"></i> Tìm đơn hàng', ['class' => 'btn btn-default']) ?>

<div class="btn-group">
    <button id="w3" class="btn btn-default export-xls" title="Export data in selected format"><i class="glyphicon glyphicon-export"></i> Export Excel</button>
</div>
<?= Html::endForm(); ?>
