<?php

namespace app\modules\export;

use kyna\base\BaseModule;

/**
 * export module definition class
 */
class ExportModule extends BaseModule
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\export\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
