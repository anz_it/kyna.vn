<?php

namespace app\modules\gamification\models;

use yii\base\Model;

/**
 * Created by PhpStorm.
 * User: macintosh
 * Date: 8/24/17
 * Time: 10:47 AM
 */
class UserPointForm extends Model
{

    public $user_id;
    public $k_point;

    public function rules()
    {
        return [
            [['user_id', 'k_point'], 'integer'],
            [['user_id', 'k_point'], 'required'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'user_id' => 'Học viên',
            'k_point' => 'Số điểm'
        ];
    }
}