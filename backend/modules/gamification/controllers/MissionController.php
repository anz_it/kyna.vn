<?php

namespace app\modules\gamification\controllers;

use kyna\gamification\models\MissionCategory;
use kyna\gamification\models\MissionCondition;
use kyna\gamification\models\MissionCourse;
use Yii;
use kyna\gamification\models\Mission;
use kyna\gamification\models\search\MissionSearch;
use app\components\controllers\Controller;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * MissionController implements the CRUD actions for Mission model.
 */
class MissionController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Mission models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MissionSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Mission model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Mission model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Mission();
        $model->loadDefaultValues();

        $conditionModel = new MissionCondition();

        if ($model->load(Yii::$app->request->post()) && $conditionModel->load(Yii::$app->request->post())) {
            $isValid = $model->validate();
            $isValid = $conditionModel->validate() && $isValid;
            if ($isValid) {
                $missionConditionTablName = MissionCondition::tableName();

                $existMission = Mission::find()
                    ->joinWith('missionConditions ' . $missionConditionTablName)
                    ->where([
                        $missionConditionTablName . '.type' => $conditionModel->type,
                        $missionConditionTablName . '.min_value' => $conditionModel->min_value,
                        'apply_course_type' => $model->apply_course_type,
                        'status' => Mission::STATUS_ACTIVE
                    ])
                    ->one();
                if ($existMission != null) {
                    switch ($model->apply_course_type) {
                        case Mission::APPLY_COURSE_TYPE_CATEGORY;
                            $catIds = ArrayHelper::map($existMission->missionCategories, 'category_id', 'category_id');
                            $diff = array_merge(array_diff($catIds, $model->listCategoryIds), array_diff($model->listCategoryIds, $catIds));
                            if (count($diff) == 0) {
                                $isValid = false;
                                Yii::$app->session->setFlash('warning', 'Đã tồn tại nhiệm vụ có điều kiện tương tự');
                            }

                            break;

                        case Mission::APPLY_COURSE_TYPE_SOME;
                            $courseIds = ArrayHelper::map($existMission->missionCourses, 'course_id', 'course_id');
                            $diff = array_merge(array_diff($courseIds, $model->listCourseIds), array_diff($model->listCourseIds, $courseIds));
                            if (count($diff) == 0) {
                                $isValid = false;
                                Yii::$app->session->setFlash('warning', 'Đã tồn tại nhiệm vụ có điều kiện tương tự');
                            }
                            break;

                        default:
                            $isValid = false;
                            Yii::$app->session->setFlash('warning', 'Đã tồn tại nhiệm vụ có điều kiện tương tự');

                            break;
                    }
                }
            }
            if ($isValid && $model->save(false)) {
                $conditionModel->mission_id = $model->id;
                $conditionModel->save(false);

                switch ($model->apply_course_type) {
                    case Mission::APPLY_COURSE_TYPE_CATEGORY:
                        $this->saveCategories($model->id, $model->listCategoryIds);
                        break;

                    case Mission::APPLY_COURSE_TYPE_SOME:
                        $this->saveCourses($model->id, $model->listCourseIds);
                        break;

                    default:
                        break;

                }

                Yii::$app->session->setFlash('success', 'Thêm thành công');

                return $this->redirect(['index']);
            }
        }

        return $this->render('create', [
            'model' => $model,
            'conditionModel' => $conditionModel
        ]);
    }

    /**
     * Creates a new Mission model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionClone($id)
    {
        $fromModel = $this->findModel($id);
        $fromModel->listCategoryIds = $fromModel->getMissionCategories()->select(['category_id'])->column();
        $selectedCourseIds = $fromModel->getMissionCourses()
            ->joinWith('course')
            ->select(['name', 'course_id'])->indexBy('course_id')->column();

        $fromModel->listCourseIds = array_keys($selectedCourseIds);

        $model = new Mission();
        $model->attributes = $fromModel->attributes;
        $model->listCategoryIds = $fromModel->listCategoryIds;
        $model->listCourseIds = $fromModel->listCourseIds;

        $conditionModel = new MissionCondition();
        $conditionModel->attributes = $fromModel->getMissionConditions()->one()->attributes;

        if ($model->load(Yii::$app->request->post()) && $conditionModel->load(Yii::$app->request->post())) {
            $isValid = $model->validate();
            $isValid = $conditionModel->validate() && $isValid;
            if ($isValid) {
                $missionConditionTablName = MissionCondition::tableName();

                $existMission = Mission::find()
                    ->joinWith('missionConditions ' . $missionConditionTablName)
                    ->where([
                        $missionConditionTablName . '.type' => $conditionModel->type,
                        $missionConditionTablName . '.min_value' => $conditionModel->min_value,
                        'apply_course_type' => $model->apply_course_type,
                        'status' => Mission::STATUS_ACTIVE
                    ])
                    ->one();
                if ($existMission != null) {
                    switch ($model->apply_course_type) {
                        case Mission::APPLY_COURSE_TYPE_CATEGORY;
                            $catIds = ArrayHelper::map($existMission->missionCategories, 'category_id', 'category_id');
                            $diff = array_merge(array_diff($catIds, $model->listCategoryIds), array_diff($model->listCategoryIds, $catIds));

                            if (count($diff) == 0) {
                                $isValid = false;
                            }

                            break;

                        case Mission::APPLY_COURSE_TYPE_SOME;
                            $courseIds = ArrayHelper::map($existMission->missionCourses, 'course_id', 'course_id');
                            $diff = array_merge(array_diff($courseIds, $model->listCourseIds), array_diff($model->listCourseIds, $courseIds));

                            if (count($diff) == 0) {
                                $isValid = false;
                            }
                            break;

                        default:
                            $isValid = false;

                            break;
                    }

                    if (!$isValid) {
                        Yii::$app->session->setFlash('warning', 'Đã tồn tại nhiệm vụ có điều kiện tương tự');
                    }
                }
            }

            if ($isValid && $model->save(false)) {
                $conditionModel->mission_id = $model->id;
                $conditionModel->save(false);

                switch ($model->apply_course_type) {
                    case Mission::APPLY_COURSE_TYPE_CATEGORY:
                        $this->saveCategories($model->id, $model->listCategoryIds);
                        break;

                    case Mission::APPLY_COURSE_TYPE_SOME:
                        $this->saveCourses($model->id, $model->listCourseIds);
                        break;

                    default:
                        break;

                }

                Yii::$app->session->setFlash('success', 'Thêm thành công');

                return $this->redirect(['index']);
            }
        } else {
            if (!empty($model->expiration_time)) {
                $model->expiration_time = date('d/m/Y H:i', $model->expiration_time);
            }
        }

        return $this->render('clone', [
            'model' => $model,
            'conditionModel' => $conditionModel
        ]);
    }

    /**
     * Updates an existing Mission model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model->listCategoryIds = $model->getMissionCategories()->select(['category_id'])->column();

        $conditionModel = MissionCondition::findOne(['mission_id' => $model->id]);

        if ($model->load(Yii::$app->request->post()) && $conditionModel->load(Yii::$app->request->post())) {
            $isValid = $model->validate();
            $isValid = $conditionModel->validate() && $isValid;
            if ($isValid && $model->save(false)) {
                $conditionModel->save(false);

                switch ($model->apply_course_type) {
                    case Mission::APPLY_COURSE_TYPE_CATEGORY:
                        $this->saveCategories($model->id, $model->listCategoryIds);
                        break;

                    case Mission::APPLY_COURSE_TYPE_SOME:
                        $this->saveCourses($model->id, $model->listCourseIds);
                        break;

                    default:
                        break;

                }

                Yii::$app->session->setFlash('success', 'Cập nhật thành công');

                return $this->redirect(['index']);
            }
        } else {
            if (!empty($model->expiration_time)) {
                $model->expiration_time = date('d/m/Y H:i', $model->expiration_time);
            }
        }

        return $this->render('update', [
            'model' => $model,
            'conditionModel' => $conditionModel
        ]);
    }

    private function saveCategories($mission_id, $list_category_ids)
    {
        foreach ($list_category_ids as $key => $catId) {
            $isExist = MissionCategory::find()->where([
                'mission_id' => $mission_id,
                'category_id' => $catId
            ])->exists();

            if ($isExist) {
                continue;
            }

            $missionCat = new MissionCategory();
            $missionCat->mission_id = $mission_id;
            $missionCat->category_id = $catId;
            $missionCat->save();
        }

        MissionCategory::deleteAll(['and',
            ['mission_id' => $mission_id],
            ['not in', 'category_id', $list_category_ids]
        ]);
    }

    private function saveCourses($mission_id, $list_course_ids)
    {
        foreach ($list_course_ids as $key => $courseId) {
            $isExist = MissionCourse::find()->where([
                'mission_id' => $mission_id,
                'course_id' => $courseId
            ])->exists();

            if ($isExist) {
                continue;
            }

            $missionCat = new MissionCourse();
            $missionCat->mission_id = $mission_id;
            $missionCat->course_id = $courseId;
            $missionCat->save();
        }

        MissionCourse::deleteAll(['and',
            ['mission_id' => $mission_id],
            ['not in', 'course_id', $list_course_ids]
        ]);
    }

    /**
     * Deletes an existing Mission model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Mission model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Mission the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Mission::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * @desc function to change status to all active records
     * @param integer $id
     */
    public function actionChangeStatus($id, $redirectUrl = false, $field = 'status')
    {
        $model = $this->findModel($id);

        if ($model->status == Mission::STATUS_ACTIVE) {
            $model->status = Mission::STATUS_DEACTIVE;
        } else {
            if ($model->status == Mission::STATUS_DEACTIVE) {
                $model->status = Mission::STATUS_ACTIVE;
                $missionConditionTablName = MissionCondition::tableName();
                $conditionModel = $model->getMissionConditions()->one();

                $existMission = Mission::find()
                    ->joinWith('missionConditions ' . $missionConditionTablName)
                    ->where([
                        $missionConditionTablName . '.type' => $conditionModel->type,
                        $missionConditionTablName . '.min_value' => $conditionModel->min_value,
                        'apply_course_type' => $model->apply_course_type,
                        'status' => Mission::STATUS_ACTIVE
                    ])
                    ->one();
                if ($existMission != null) {
                    switch ($model->apply_course_type) {
                        case Mission::APPLY_COURSE_TYPE_CATEGORY;
                            $catIds = ArrayHelper::map($existMission->missionCategories, 'category_id', 'category_id');
                            $diff = array_merge(array_diff($catIds, $model->listCategoryIds), array_diff($model->listCategoryIds, $catIds));

                            if (count($diff) == 0) {
                                $isValid = false;
                            }

                            break;

                        case Mission::APPLY_COURSE_TYPE_SOME;
                            $courseIds = ArrayHelper::map($existMission->missionCourses, 'course_id', 'course_id');
                            $diff = array_merge(array_diff($courseIds, $model->listCourseIds), array_diff($model->listCourseIds, $courseIds));

                            if (count($diff) == 0) {
                                $isValid = false;
                            }
                            break;

                        default:
                            $isValid = false;

                            break;
                    }

                    if (!$isValid) {
                        Yii::$app->response->format = Response::FORMAT_JSON;

                        return [
                            'status' => false,
                            'msg' => 'Đã tồn tại nhiệm vụ có điều kiện tương tự'
                        ];
                    }
                }
            }
        }
        if ($model->save(false)) {
            return "Thay đổi thành công!";
        }
        else
            return "Thay đổi thất bại!";
    }
}
