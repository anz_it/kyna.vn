<?php

namespace app\modules\gamification\controllers;

use app\modules\gamification\models\UserPointForm;
use kyna\gamification\models\search\UserPointHistorySearch;
use kyna\gamification\models\UserPointHistory;
use Yii;
use kyna\gamification\models\UserPoint;
use kyna\gamification\models\search\UserPointSearch;
use app\components\controllers\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * LeaderBoardController implements the CRUD actions for UserPoint model.
 */
class UserController extends Controller
{

    /**
     * Lists all UserPoint models.
     * @return mixed
     */
    public function actionIndex()
    {
        $model = new UserPointForm();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $userPoint = UserPoint::findOne(['user_id' => $model->user_id]);
            if ($userPoint == null) {
                $userPoint = new UserPoint();
                $userPoint->user_id = $model->user_id;
            }

            $userPoint->k_point += $model->k_point;
            $userPoint->k_point_usable += $model->k_point;

            if ($userPoint->save()) {
                $userPointHistory = new UserPointHistory();
                $userPointHistory->user_id = $userPoint->user_id;
                $userPointHistory->k_point = $model->k_point;
                $userPointHistory->type = UserPointHistory::TYPE_MANUAL;
                $userPointHistory->is_added = UserPointHistory::BOOL_YES;
                $userPointHistory->usable_date = time();
                $userPointHistory->description = ($model->k_point > 0 ? 'Được cộng' : 'Bị trừ') . " bởi quản trị viên";
                $userPointHistory->save(false);

                Yii::$app->session->setFlash('success', 'Cập nhật thành công');
                return $this->redirect('index');
            }
        }

        $searchModel = new UserPointSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        if (Yii::$app->request->isAjax && !empty(Yii::$app->request->post('email'))) {
            // init pagination for raw SQL
            $cloneQuery = clone $dataProvider->query;
            $cloneQuery->orderBy('k_point DESC');
            $sql = $cloneQuery->createCommand()->getRawSql();

            $sql = str_replace('`', '', $sql);
            $email =  Yii::$app->request->post('email');
            $rootPath = \Yii::getAlias('@root');
            $exportLog = \Yii::getAlias('@console/runtime/export-point.log');
            $exportErrorLog = \Yii::getAlias('@console/runtime/export-point-error.log');
            // check log file exist
            if (!file_exists($exportLog))
                touch($exportLog);
            if (!file_exists($exportErrorLog))
                touch($exportErrorLog);

            $command = "php {$rootPath}/yii export/run point \"{$sql}\" {$email}" . " > {$exportLog} 2>>{$exportErrorLog} &";
            exec($command);
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'result' => true,
            ];
        }

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'model' => $model
        ]);
    }

    /**
     * Displays a single UserPoint model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $searchModel = new UserPointHistorySearch();
        $searchModel->user_id = $id;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('view', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    /**
     * Creates a new UserPoint model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionPoint($id)
    {
        $userPoint = $this->findModel($id);

        $model = new UserPointForm();
        $model->user_id = $id;

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $userPoint->k_point += $model->k_point;
            $userPoint->k_point_usable += $model->k_point;

            Yii::$app->response->format = Response::FORMAT_JSON;

            $userPointHistory = new UserPointHistory();
            $userPointHistory->user_id = $userPoint->user_id;
            $userPointHistory->type = UserPointHistory::TYPE_MANUAL;
            $userPointHistory->k_point = $model->k_point;
            $userPointHistory->description = ($model->k_point > 0 ? 'Được cộng' : 'Bị trừ') . " bởi quản trị viên";
            $userPointHistory->is_added = UserPointHistory::BOOL_YES;
            $userPointHistory->usable_date = time();

            if ($userPoint->save()) {
                $userPointHistory->save(false);

                return [
                    'success' => true,
                    'message' => ($model->k_point > 0 ? 'Cộng thành công ' : 'Trừ thành công ') . $model->k_point . ' điểm cho học viên: ' . $userPoint->user->email,
                ];
            } else {
                return [
                    'success' => false,
                    'message' => 'Xử lý thất bại',
                    'errors' => $model->errors
                ];
            }
        }

        return $this->renderAjax('point', [
            'model' => $model,
            'userPoint' => $userPoint,
        ]);
    }

    /**
     * Finds the UserPoint model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return UserPoint the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = UserPoint::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
