<?php

namespace app\modules\gamification\controllers;

use kyna\promotion\models\Promotion;
use Yii;
use yii\web\UploadedFile;
use yii\web\NotFoundHttpException;

use kyna\gamification\models\Gift;
use kyna\gamification\models\search\GiftSearch;
use kyna\gamification\models\GiftContent;
use kyna\gamification\models\search\UserGiftSearch;
use app\components\controllers\Controller;
use common\lib\CDNImage;

/**
 * GiftController implements the CRUD actions for Gift model.
 */
class GiftController extends Controller
{

    /**
     * Lists all Gift models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new GiftSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new Gift model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Gift();
        $content = new GiftContent();
        $content->type = GiftContent::TYPE_VOUCHER;
        $content->discount_type = Promotion::TYPE_FEE;
        $content->apply_condition = Promotion::ONE_COURSE_CONDITION;
        $content->apply_all = 1;
        $content->min_amount = 0;
        if ($model->load(Yii::$app->request->post()) && $content->load(Yii::$app->request->post())) {
            if ($model->validate() && $content->validate()) {
                if(!empty($content->apply_all)){
                    $content->apply_all_single_course = 1;
                    $content->apply_all_single_course_double = 1;
                    $content->apply_all_combo  = 1;
                }
                if (!empty($model->expiration_date)) {
                    $dateTimeFormat = str_replace('php:', '', Yii::$app->formatter->dateFormat);
                    $model->expiration_date = date_create_from_format($dateTimeFormat . ' H:i:s', $model->expiration_date . ' 00:00:00')->getTimestamp();
                }
                $model->save(false);
                $content->gift_id = $model->id;
                $content->save(false);

                if ($imageUrl = $this->_uploadImage($model, 'image_url')) {
                    $model->image_url = $imageUrl;
                    $model->save(false);
                }
                Yii::$app->session->setFlash('success', 'Thêm thành công');

                return $this->redirect(['index']);
            }
        }

        return $this->render('create', [
            'model' => $model,
            'content' => $content
        ]);
    }

    /**
     * Updates an existing Gift model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $content = $model->giftContent;
        $dateTimeFormat = str_replace('php:', '', Yii::$app->formatter->dateFormat);

        if ($model->load(Yii::$app->request->post()) && $content->load(Yii::$app->request->post())) {
            if ($model->validate() && $content->validate()) {
                $date = date_create_from_format($dateTimeFormat . ' H:i:s', $model->expiration_date . ' 00:00:00');
                if ($date !== false) {
                    $model->expiration_date = $date->getTimestamp();
                } else {
                    $model->expiration_date = null;
                }

                if ($imageUrl = $this->_uploadImage($model, 'image_url')) {
                    $model->image_url = $imageUrl;
                }
                $model->save(false);
                $content->save(false);

                Yii::$app->session->setFlash('success', 'Cập nhật thành công');

                return $this->redirect(['index']);
            }
        } else {
            if (!empty($model->expiration_date)) {
                $model->expiration_date = date($dateTimeFormat, $model->expiration_date);
            }
        }

        return $this->render('update', [
            'model' => $model,
            'content' => $content
        ]);
    }

    public function actionHistory($id)
    {
        $searchModel = new UserGiftSearch();
        $searchModel->gift_id = $id;

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('history', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider
        ]);
    }

    /**
     * Finds the Gift model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Gift the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Gift::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    private function  _uploadImage($model, $attribute)
    {
        $files = UploadedFile::getInstances($model, $attribute);
        if (!sizeof($files)) {
            $oldAttributes = $model->oldAttributes;

            return isset($oldAttributes[$attribute]) ? $oldAttributes[$attribute] : false;
        }

        $file = $files[0];

        $cdnImage = new CDNImage($model, $attribute);
        $result = $cdnImage->upload($file);

        return $result;
    }
}
