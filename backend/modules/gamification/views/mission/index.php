<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use kyna\gamification\models\Mission;
/* @var $this yii\web\View */
/* @var $searchModel kyna\gamification\models\search\MissionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$crudTitles = Yii::$app->params['crudTitles'];
$crudButtonIcons = Yii::$app->params['crudButtonIcons'];

$this->title = 'Quản lý nhiệm vụ';
$this->params['breadcrumbs'][] = $this->title;

$webUser = Yii::$app->user;
?>
<div class="mission-index">
    <p>
        <?php
        if ($webUser->can('Gamification.Mission.Create')) {
            echo Html::a($crudButtonIcons['create'] . ' ' . $crudTitles['create'], ['create'], ['class' => 'btn btn-success']);
        }
        ?>
    </p>
<?php Pjax::begin(); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'name',
            'k_point',
            'k_point_for_free_course',
            'expiration_time:datetime',
            [
                'attribute' => 'status',
                'format' => 'raw',
                'value' => function ($model)
                {
                    return $model->statusButton;
                },
                'filter' => Html::activeDropDownList($searchModel, 'status', Mission::listStatus(false), ['class' => 'form-control', 'prompt' => $crudTitles['prompt']]),
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view} {clone}',
                'buttons' => [
                    'clone' => function ($url, $model, $key) {
                        $url = Url::toRoute(['clone', 'id' => $key]);

                        return Html::a('<span class="fa fa-copy"></span>', $url, [
                            'data-pjax' => 0,
                            'title' => 'Sao chép',
                        ]);
                    }
                ],
            ],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
