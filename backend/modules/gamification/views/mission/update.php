<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model kyna\gamification\models\Mission */

$this->title = 'Cập nhật nhiệm vụ: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Quản lý nhiệm vụ', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Cập nhật';
?>
<div class="mission-update">
    <?= $this->render('_form', [
        'model' => $model,
        'conditionModel' => $conditionModel
    ]) ?>

</div>
