<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kyna\gamification\models\Mission;

/* @var $this yii\web\View */
/* @var $model kyna\gamification\models\Mission */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Quản lý nhiệm vụ', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$webUser = Yii::$app->user;

$attributes = [
    'id',
    'name',
    'k_point',
    'k_point_for_free_course',
    'expiration_time:datetime',
    [
        'label' => 'Số liệu tổi thiểu',
        'value' => call_user_func(function ($model) {
            $condition = $model->getMissionConditions()->one();
            return $condition->min_value;
        }, $model)
    ],
    'applyCourseTypeText:text:Áp dụng cho',
];

if ($model->apply_course_type != Mission::APPLY_COURSE_TYPE_ALL) {
    if ($model->apply_course_type == Mission::APPLY_COURSE_TYPE_SOME) {
        $attributes[] = [
            'label' => 'Khóa học',
            'value' => call_user_func(function ($model) {
                $courses = $model->getMissionCourses()->joinWith('course')->select(['name'])->column();

                return implode(', ', $courses);
            }, $model)
        ];
    } else {
        $attributes[] = [
            'label' => 'Danh mục',
            'value' => call_user_func(function ($model) {
                $categories = $model->getMissionCategories()->joinWith('category')->select(['name'])->column();

                return implode(', ', $categories);
            }, $model)
        ];
    }

}

$attributes = array_merge($attributes, [
    'statusText:text:Trạng thái',
    'created_time:datetime',
    'createdUser.email:email:Email người tạo',
    'updated_time:datetime',
    'updatedUser.email:email:Email người cập nhật cuối',
]);
?>
<div class="mission-view">

    <p>
        <?php
        if ($webUser->can('Gamification.Mission.Create')) {
            echo Html::a('Sao chép', ['clone', 'id' => $model->id], ['class' => 'btn btn-primary']);
        }
        ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => $attributes,
    ]) ?>

</div>
