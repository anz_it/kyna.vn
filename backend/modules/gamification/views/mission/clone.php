<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model kyna\gamification\models\Mission */

$this->title = 'Sao chép nhiệm vụ';
$this->params['breadcrumbs'][] = ['label' => 'Quản lý nhiệm vụ', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mission-create">
    <?= $this->render('_form', [
        'model' => $model,
        'conditionModel' => $conditionModel
    ]) ?>

</div>
