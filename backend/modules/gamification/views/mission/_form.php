<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\helpers\ArrayHelper;
use yii\bootstrap\ActiveForm;

use kartik\select2\Select2;
use kartik\datetime\DateTimePicker;

use kyna\course\models\Course;
use kyna\course\models\Category;
use kyna\gamification\models\Mission;
use kyna\gamification\models\MissionCondition;

/* @var $this yii\web\View */
/* @var $model kyna\gamification\models\Mission */
/* @var $form yii\widgets\ActiveForm */

$crudTitles = Yii::$app->params['crudTitles'];

if (empty($model->listCourseIds)) {
    $selectedCourseIds = $model->getMissionCourses()
        ->joinWith('course')
        ->select(['name', 'course_id'])->indexBy('course_id')->column();

    $model->listCourseIds = array_keys($selectedCourseIds);
} else {
    $selectedCourseIds = Course::find()->where(['id' => $model->listCourseIds])->select(['name', 'id'])->indexBy('id')->column();
}
?>

<div class="mission-form">

    <?php $form = ActiveForm::begin(['layout' => 'horizontal']); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($conditionModel, 'type', [
        'wrapperOptions' => [
            'class' => "col-sm-3"
        ]
    ])->dropDownList(MissionCondition::getTypeOptions()) ?>

    <?= $form->field($conditionModel, 'min_value', [
        'wrapperOptions' => [
            'class' => "col-sm-3"
        ]
    ])->textInput() ?>

    <?= $form->field($conditionModel, 'max_value', [
        'wrapperOptions' => [
            'class' => "col-sm-3"
        ]
    ])->textInput() ?>

    <?= $form->field($model, 'k_point', [
        'wrapperOptions' => [
            'class' => "col-sm-2"
        ]
    ])->textInput()->hint('K Point') ?>

    <div id="free-k-point">
    <?= $form->field($model, 'k_point_for_free_course', [
        'wrapperOptions' => [
            'class' => "col-sm-2"
        ]
    ])->textInput()->hint('K Point') ?>
    </div>

    <?= $form->field($model, 'expiration_time', [
        'wrapperOptions' => [
            'class' => "col-sm-3"
        ]
    ])->widget(DateTimePicker::className(), [
        'convertFormat' => true,
        'pluginOptions' => [
            'autoclose'=>true,
            'format' => 'dd/MM/yyyy H:i',
        ]
    ]) ?>

    <div id="apply-type" class="<?= $conditionModel->type == MissionCondition::TYPE_ORDER_COMPLETE ? 'hide' : '' ?>">
        <?= $form->field($model, 'apply_course_type')->radioList(Mission::getApplyForCourseOptions(), ['prompt' => '']) ?>
    </div>

    <div id="list-course" class="<?= $model->apply_course_type == Mission::APPLY_COURSE_TYPE_SOME ? 'open' : 'hide' ?>">
        <?php echo $form->field($model, 'listCourseIds')->widget(Select2::classname(), [
            'initValueText' => $selectedCourseIds,
            'options' => [
                'placeholder' => $crudTitles['prompt'],
                'multiple' => true
            ],
            'theme' => 'default',
            'pluginOptions' => [
                'allowClear' => true,
                'minimumInputLength' => 3,
                'language' => [
                    'errorLoading' => new JsExpression("function () { return 'Chờ nhận kết quả...'; }"),
                ],
                'ajax' => [
                    'url' => Url::toRoute(['/course/api/search', 'auto' => true]),
                    'dataType' => 'json',
                    'data' => new JsExpression('function(params) { return {q:params.term}; }'),
                ],
                'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                'templateResult' => new JsExpression('function(course) { return course.text; }'),
                'templateSelection' => new JsExpression('function (course) { if (course.price !== undefined) {$("#old-price").val(course.price); } return course.text; }'),
            ],
        ])->label('Khóa học');
        ?>
    </div>

    <div id="list-category" class="<?= $model->apply_course_type == Mission::APPLY_COURSE_TYPE_CATEGORY ? 'open' : 'hide' ?>">
        <?= $form->field($model, 'listCategoryIds')->widget(Select2::classname(), [
            'data' => ArrayHelper::map(Category::find()->where(['status' => Category::STATUS_ACTIVE])->all(), 'id', 'name'),
            'options' => [
                'placeholder' => $crudTitles['prompt'],
                'multiple' => true
            ],
            'theme' => 'default',
        ])->label('Danh mục')
        ?>
    </div>

    <?= $form->field($model, 'status', [
        'wrapperOptions' => [
            'class' => "col-sm-3"
        ]
    ])->dropDownList(Mission::listStatus(), ['prompt' => $crudTitles['prompt']]) ?>

    <div class="form-group">
        <div class="buttons col-md-offset-3">
            <?= Html::submitButton($model->isNewRecord ? $crudTitles['create'] : $crudTitles['update'], ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            <?= Html::a($crudTitles['cancel'], ['index'], ['class' => 'btn btn-default']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<?php
$script = "
    $(document).ready(function () {
    
        function checkType() {
            var selectedType = $('input[name=\'Mission[apply_course_type]\']:checked').val();
            
            switch (selectedType) {
                case '" . Mission::APPLY_COURSE_TYPE_CATEGORY . "':
                    $('#list-course').removeClass('open');
                    $('#list-course').addClass('hide');
                    $('#list-category').removeClass('hide');
                    $('#list-category').addClass('open');
                    break;
                    
                case '" . Mission::APPLY_COURSE_TYPE_SOME . "':
                    $('#list-course').removeClass('hide');
                    $('#list-course').addClass('open');
                    $('#list-category').removeClass('open');
                    $('#list-category').addClass('hide');
                    break;
                    
                default:
                    $('#list-course').addClass('hide');
                    $('#list-category').addClass('hide');
                    break;
            }
        }
    
        $('body').on('change', 'input[name=\'Mission[apply_course_type]\']', function () {
            checkType();
        });
        
        function checkToShowMaxValue()
        {
            var selectedType = $('#missioncondition-type').val();
            console.log(selectedType);
            
            if (selectedType == '" . MissionCondition::TYPE_ORDER_COMPLETE . "') {
                $('.field-missioncondition-max_value').show();
            } else {
                $('.field-missioncondition-max_value').hide();
            }
        }
        
        $('body').on('change', '#missioncondition-type', function () {
            checkToShowMaxValue();
        });
        
        function checkFreeCoursePoint(){
            var hideKpointFreeCourse = false;
            var needToCheckAmount = false;
            
            var missionType = $('select[name=\'MissionCondition[type]\']').val();
            switch (missionType) {
                case '" . MissionCondition::TYPE_ORDER_COMPLETE . "':
                    hideKpointFreeCourse = true;
                    
                    break;
                    
                case '" . MissionCondition::TYPE_COURSE_REVIEW . "':
                case '" . MissionCondition::TYPE_COURSE_GRADUATED . "':
                case '" . MissionCondition::TYPE_COURSE_GRADUATED_EXCELLENT . "':
                    needToCheckAmount = true;
                    
                    break;
                    
                default:
                    break;
            }
            
            if (needToCheckAmount) {
                var amount = $('#missioncondition-min_value').val();
                if (amount > 1) {
                    hideKpointFreeCourse = true;
                }
            }
            
            if (hideKpointFreeCourse) {
                $('#mission-k_point_for_free_course').val('');
                $('#free-k-point').addClass('hide');
            } else {
                $('#free-k-point').removeClass('hide');
            }
        }
        
        $('#missioncondition-min_value').on('change', function() {
            checkFreeCoursePoint();
        });
        
        $('body').on('change', 'select[name=\'MissionCondition[type]\']', function () {
            var selectedType = $(this).val();
            
            if (selectedType == '" . MissionCondition::TYPE_ORDER_COMPLETE . "') {
                $('label[for=\'missioncondition-min_value\']').html('Giá trị đơn hàng tối thiểu');
                $('label[for=\'missioncondition-max_value\']').html('Giá trị đơn hàng tối đa');
            } else if (selectedType == '" . MissionCondition::TYPE_LESSON_VIDEO_PROCESS . "') {
                $('label[for=\'missioncondition-min_value\']').html('Phần trăm video');
            } else {
                $('label[for=\'missioncondition-min_value\']').html('Số lượng tối thiểu cần được');
            }
            
            if (selectedType == '" . MissionCondition::TYPE_ORDER_COMPLETE . "') {
                $('#apply-type').addClass('hide');
            } else {
                $('#apply-type').removeClass('hide');
            }
            
            checkFreeCoursePoint();
            checkType();
        });
        
        $(function () { 
            $('[data-toggle=\'popover\']').popover({ trigger: 'hover', html: true }); 
            
            checkFreeCoursePoint();
            
            checkToShowMaxValue();
        });
    });
";

$this->registerJs($script, \yii\web\View::POS_END);
?>
