<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model kyna\gamification\models\UserPoint */

$this->title = 'Create User Point';
$this->params['breadcrumbs'][] = ['label' => 'User Points', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title" id="modal-label">Cập nhật điểm cho học viên: <?= $userPoint->user->email ?></h4>
</div>

<div class="modal-body">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
