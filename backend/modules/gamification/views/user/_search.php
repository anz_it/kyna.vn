<?php

use yii\helpers\Html;

use kartik\form\ActiveForm;
use kartik\daterange\DateRangePicker;
use kartik\select2\Select2;
use kyna\gamification\models\UserPointHistory;
?>

<div class="user-telesale-search">

    <?php $form = ActiveForm::begin([
        'action' => ['view', 'id' => $model->user_id],
        'method' => 'get',
        'options' => [
            'role' => 'search',
            'class' => 'navbar-form navbar-left',
        ]
    ]); ?>

        <?=
            $form->field($model, 'date_ranger', [
                'addon' => ['prepend' => ['content' => '<i class="glyphicon glyphicon-calendar"></i>']],
                'options' => ['class' => 'form-group'],
            ])->widget(DateRangePicker::classname(), [
                'useWithAddon' => true,
                'convertFormat' => true,
                'pluginOptions'=>[
                    'dateLimit' => [
                        'days' => 30
                    ],
                    'timePicker' => false,
                    'locale'=>[
                        'format' => 'd/m/yy',
                        'separator'=> " - ", // after change this, must update in controller
                    ],
                ],
            ])->label(false)->error(false)
        ?>

        <?= $form->field($model, 'type', [
                 'options' => ['class' => 'form-group', 'style' => 'width: 180px'],
            ])->widget(Select2::classname(), [
                    'data' => UserPointHistory::getTypes(),
                    'options' => ['placeholder' => '--Nguồn--'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ])->label(false)->error(false) ?>

        <div class="form-group">
            <?= Html::submitButton('<i class="ion-android-search"></i> Search', ['class' => 'btn btn-primary']) ?>
        </div>

    <?php ActiveForm::end(); ?>

</div>
