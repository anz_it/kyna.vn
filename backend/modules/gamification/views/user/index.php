<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\widgets\ActiveForm;
use yii\web\JsExpression;
use kartik\widgets\Select2;
use kartik\export\ExportMenu;
/* @var $this yii\web\View */
/* @var $searchModel kyna\gamification\models\search\UserPointSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Bảng xếp hạng';
$this->params['breadcrumbs'][] = $this->title;

$crudTitles = Yii::$app->params['crudTitles'];
$crudButtonIcons = Yii::$app->params['crudButtonIcons'];

$webUser = Yii::$app->user;
?>
<div class="user-point-index">
    <?php if ($webUser->can('Affiliate.User.Create')) : ?>
        <nav class="navbar navbar-default">
            <?php $form = ActiveForm::begin([
                'options' => [
                    'class' => 'navbar-form navbar-left'
                ],
                'action' => Url::toRoute(['index'])
            ]) ?>

            <?php
            echo $form->field($model, 'user_id', [
                'options' => ['class' => 'form-group', 'style' => 'width: 200px'],
            ])->widget(Select2::classname(), [
                'options' => ['placeholder' => '--Chọn học viên--'],
                'pluginOptions' => [
                    'allowClear' => true,
                    'minimumInputLength' => 3,
                    'language' => [
                        'errorLoading' => new JsExpression("function () { return 'Chờ nhận kết quả...'; }"),
                    ],
                    'ajax' => [
                        'url' => Url::toRoute(['/user/api/search']),
                        'dataType' => 'json',
                        'data' => new JsExpression('function(params) { return {q:params.term}; }'),
                    ],
                    'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                    'templateResult' => new JsExpression('function(res) { return res.text; }'),
                    'templateSelection' => new JsExpression('function (res) { return res.text; }'),
                ],
            ])->label(false)->error(false); ?>

            <?= $form->field($model, 'k_point', [
                'options' => [
                    'class' => 'form-group',
                ]
            ])->textInput(['placeholder' => 'Số điểm'])->label(false)->error(false) ?>

            <?= Html::submitButton($crudButtonIcons['update'] . ' ' . $crudTitles['update'], ['class' => 'btn btn-success']) ?>

            <?php ActiveForm::end() ?>
            <?php
            if ($model->hasErrors()) {
                $errors = [];
                foreach($model->errors as $error) {
                    $errors[] = $error[0];
                }
                echo "<span class='navbar-text navbar-left error'>". implode(', ', $errors) ."</span>";
            }
            ?>
        </nav>
    <?php endif; ?>
    <?php Pjax::begin(['id' => 'idPjaxReload']); ?>
        <a class="btn btn-info  navbar-btn navbar-left" data-toggle="modal" data-target="#myModal">
            <i class="fa fa-file-excel-o"></i> Export excel
        </a>

        <div class="clearfix"></div>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                [
                    'attribute' => 'userInfo',
                    'label' => 'Học viên',
                    'format' => 'raw',
                    'value' => function ($model) {
                        if ($model->user == null) {
                            return;
                        }
                        return $model->profile->name . "</br><a href='mailto:{$model->user->email}'>" . $model->user->email . '</a></br>' . $model->profile->phone_number;
                    }
                ],
                'k_point',

                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{view} {point}',
                    'options' => [
                        'class' => 'col-xs-1',
                    ],
                    'buttons' => [
                        'view' => function ($url, $model, $key) {
                            $url = Url::toRoute(['view', 'id' => $key]);

                            return Html::a('<span class="fa fa-history"></span>', $url, [
                                'class' => 'btn btn-sm btn-default btn-block',
                                'data-pjax' => 0,
                                'title' => 'Xem'
                            ]);
                        },
                        'point' => function ($url, $model, $key) {
                            $url = Url::toRoute(['point', 'id' => $key]);

                            return Html::a('<span class="fa fa-minus-circle"></span>/<span class="fa fa-plus-circle"></span>', $url, [
                                'class' => 'btn btn-sm btn-default btn-block btn-popup',
                                'title' => 'Thay đổi điểm'
                            ]);
                        },
                    ],
                ],
            ],
        ]); ?>
    <?php Pjax::end(); ?>
</div>

    <!-- Modal -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">

                <div class="modal-body">
                    <form class="form-horizontal" id="exportForm">
                        <div class="form-group" style="padding:10px">
                            <label>Bạn vui lòng cung cấp email: </label>
                            <input id="exportEmail" type="email" value="<?=Yii::$app->user->identity->email?>" class="form-control">
                            <span class="help-block">
                            Hệ thống sẽ chạy export ngầm, sau khi có kết quả sẽ gửi mail tới cho bạn.
                        </span>
                        </div>
                        <button type="submit" class="btn btn-success">Xác nhận</button>
                        <button type="button" data-dismiss="modal" class="btn btn-default">Đóng</button>
                    </form>
                </div>

            </div>
        </div>
    </div>

<?php
/** @var $this \yii\web\View */
\backend\assets\BootboxAsset::register($this);
$this->registerJs("
    $('#exportForm').submit(function () {
        if (exportEmail.value.trim().length == 0) {
            bootbox.alert('Bạn vui lòng cung cấp email.');
            return false;
        }
        $.ajax({
            url : window.location.href,
            type: 'POST',
            data: {email: exportEmail.value.trim()},
            success: function () {
                $('#myModal').modal('hide');
                bootbox.alert('Hệ thống xử lý xong sẽ gửi mail cho bạn trong vài phút. Vui lòng check mail để xem kết quả');
    
            }
        });
        return false;
    });
");
?>