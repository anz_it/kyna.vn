<?php

use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Html;
/* @var $this yii\web\View */
/* @var $searchModel kyna\gamification\models\search\UserPointHistorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = "Học viên: " . $searchModel->user->email . " | Lịch sử điểm";
$this->params['breadcrumbs'][] = ['label' => 'Bảng xếp hạng', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<nav class="navbar navbar-default">
    <?= $this->render('_search', [
        'model' => $searchModel,
    ]) ?>
</nav>
<?php Pjax::begin(); ?>
<div class="user-point-history-index">
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                'id',
                'created_time:datetime',
                [
                    'attribute' => 'k_point',
                    'value' => function ($model) {
                        return ($model->k_point > 0 ? '+' : '') . $model->k_point;
                    }
                ],
                'typeText:text:Nguồn',
                'description:raw',
                [
                    'attribute' => 'referenceText',
                    'format' => 'raw',
                    'label' => 'Tham chiếu',
                    'value' => function ($model) {
                        return Html::a('<i class="fa fa-question"></i>', '#', [
                            'data-content' => $model->referenceText,
                            'data-toggle' => 'popover',
                        ]);
                    }
                ],
                [
                    'attribute' => 'created_user_id',
                    'value' => function ($model) {
                        return $model->createdUser != null ? $model->createdUser->email : null;
                    }
                ],
            ],
        ]); ?>
</div>
<?php
$js = <<< SCRIPT
/* To initialize BS3 tooltips set this below */
$(function () { 
    $("[data-toggle='popover']").popover({ trigger: "hover", html: true }); 
});
SCRIPT;

$this->registerJs($js);
?>
<?php Pjax::end(); ?>
