<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model kyna\gamification\models\UserPoint */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-point-form">

    <?php $form = ActiveForm::begin([
        'id' => 'form-user-point',
        'options' => ['class' => 'form-data-ajax'],
    ]); ?>

        <?= $form->field($model, 'k_point')->textInput() ?>

        <div class="form-group">
            <?= Html::submitButton('Submit', ['class' => 'btn btn-success']) ?>
        </div>

    <?php ActiveForm::end(); ?>

</div>
