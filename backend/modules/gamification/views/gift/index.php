<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use common\helpers\CDNHelper;
/* @var $this yii\web\View */
/* @var $searchModel kyna\gamification\models\search\GiftSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$crudTitles = Yii::$app->params['crudTitles'];
$crudButtonIcons = Yii::$app->params['crudButtonIcons'];

$this->title = 'Quản lý quà tặng';
$this->params['breadcrumbs'][] = $this->title;

$webUser = Yii::$app->user;
?>
<div class="gift-index">

    <p>
        <?php
        if ($webUser->can('Gamification.Mission.Create')) {
            echo Html::a($crudButtonIcons['create'] . ' ' . $crudTitles['create'], ['create'], ['class' => 'btn btn-success']);
        }
        ?>
    </p>
<?php Pjax::begin(); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'id',
            [
                'attribute' => 'image_url',
                'format' => 'html',
                'value' => function ($model) {
                    return CDNHelper::image($model->image_url, [
                        'size' => CDNHelper::IMG_SIZE_ORIGINAL,
                        'alt' => $model->image_url,
                        'width' => '100px'
                    ]);
                },
                'filter' => false,
                'options' => ['class' => 'col-xs-2']
            ],
            'title',
            'k_point',
            [
                'attribute' => 'giftContent.discount_value',
                'value' => function ($model){
                    return  $model->giftContent->discount_type == \kyna\promotion\models\Promotion::TYPE_PERCENTAGE ? $model->giftContent->discount_value .' %' : Yii::$app->formatter->asCurrency($model->giftContent->discount_value);
                }
            ],
            [
                'attribute' => 'status',
                'format' => 'raw',
                'value' => function ($model)
                {
                    return $model->statusButton;
                },
                'filter' => Html::activeDropDownList($searchModel, 'status', \kyna\gamification\models\Gift::listStatus(false), ['class' => 'form-control', 'prompt' => $crudTitles['prompt']]),
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view}'
            ],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
