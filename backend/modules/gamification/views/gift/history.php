<?php

use yii\bootstrap\Nav;
use yii\grid\GridView;
use yii\widgets\Pjax;
use kyna\promotion\models\Promotion;
use kyna\gamification\models\UserGift;
/* @var $this yii\web\View */
/* @var $searchModel kyna\gamification\models\search\GiftSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$crudTitles = Yii::$app->params['crudTitles'];
$crudButtonIcons = Yii::$app->params['crudButtonIcons'];

$this->title = $searchModel->gift->title;
$this->params['breadcrumbs'][] = ['label' => 'Quản lý quà tặng', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$webUser = Yii::$app->user;
?>
<div class="gift-index">
    <?= Nav::widget([
        'items' => [
            [
                'label' => 'Thông tin quà tặng',
                'url' => ['/gamification/gift/view', 'id' => $searchModel->gift_id],
                'active' => false,
            ],
            [
                'label' => 'Lịch sử sử dụng',
                'url' => ['/gamification/gift/history', 'id' => $searchModel->gift_id],
                'active' => true,
            ],
        ],
        'options' => ['class' => 'nav-pills pull-left'],
    ]) ?>

    <div class="clearfix"></div>

    <?php Pjax::begin(); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        //'layout' => '{items} {summary}',
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'userInfo',
                'label' => 'Thông tin học viên',
                'format' => 'raw',
                'value' => function ($model) {
                    return "{$model->user->profile->name}<br>
                            <a href='mailto:{$model->user->email}'>{$model->user->email}</a><br>
                            {$model->user->profile->phone_number}";
                }
            ],
            'code',
            [
                'attribute' => 'promotion.value',
                'value' => function ($model){
                    if(empty($model->promotion)){
                        return null;
                    }
                    return  $model->promotion->discount_type == Promotion::TYPE_PERCENTAGE ? $model->promotion->value .' %' : Yii::$app->formatter->asCurrency($model->promotion->value);
                }
            ],

            [
                'attribute' => 'expirationDate',
                'format' => 'datetime',
                'label' => 'Ngày hết hạn',
                'value' => function ($model) {
                    if(empty($model->promotion)){
                        return null;
                    }
                    return $model->promotion->end_date;
                },
                'filter' =>  \kartik\date\DatePicker::widget([
                    'model' => $searchModel,
                    'attribute' => 'expirationDate'
                ])
            ],
            [
                'header' => 'Có thể sử dụng',
                'format' => 'html',
                'value' => function ($model) {
                    if(empty($model->promotion)){
                        return null;
                    }
                    if (empty($model->user_date) && $model->promotion->canUse) {
                        return '<span class="label label-success">' . UserGift::BOOL_YES_TEXT . '</span>';
                    } else {
                        return '<span class="label label-danger">' . UserGift::BOOL_NO_TEXT . '</span>';
                    }
                }
            ],
            [
                'attribute' => 'used_date',
                'format' => 'datetime',
                'filter' =>  \kartik\date\DatePicker::widget([
                    'model' => $searchModel,
                    'attribute' => 'used_date'
                ])
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?></div>
