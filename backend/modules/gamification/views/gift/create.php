<?php


/* @var $this yii\web\View */
/* @var $model kyna\gamification\models\Gift */

$this->title = 'Thêm quà tặng';
$this->params['breadcrumbs'][] = ['label' => 'Quản lý quà tặng', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="gift-create">
    <?= $this->render('_form', [
        'model' => $model,
        'content' => $content
    ]) ?>

</div>
