<?php

use yii\bootstrap\Nav;
use yii\widgets\DetailView;
use kyna\promotion\models\Promotion;
use kyna\gamification\models\GiftContent;
/* @var $this yii\web\View */
/* @var $model kyna\gamification\models\Gift */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Quản lý quà tặng', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$giftContent = $model->giftContent;
$course_names = [];
foreach ($giftContent->promoCourses as $course)
{
    $course_id = $course->course_id;
    $course = \kyna\course\models\Course::findOne(['id'=>$course_id]);
    if(!empty($course))
        $course_names[] = $course->name;
}
?>
<div class="gift-view">

    <?= Nav::widget([
        'items' => [
            [
                'label' => 'Thông tin quà tặng',
                'url' => ['/gamification/gift/view', 'id' => $model->id],
                'active' => true,
            ],
            [
                'label' => 'Lịch sử sử dụng',
                'url' => ['/gamification/gift/history', 'id' => $model->id],
                'active' => false,
            ],
        ],
        'options' => ['class' => 'nav-pills pull-left'],
    ]) ?>
    <?php

    $discountTypes = Promotion::getDiscountTypes();
    $conditions = Promotion::getListCondition();
    ?>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'title',
            'image_url:ntext',
            'giftContent.typeText:text:Loại quà tặng',
            'k_point',


            [
                'attribute' => 'giftContent.discount_type',
                'value' => !empty($discountTypes[$giftContent->discount_type]) ? $discountTypes[$giftContent->discount_type] : 'N/A',
            ],
            [
                'attribute' => 'giftContent.discount_value',
                'value' => ($giftContent->discount_type == Promotion::TYPE_PERCENTAGE) ? $giftContent->discount_value .' %' : Yii::$app->formatter->asCurrency($giftContent->discount_value)
            ],
            [
                'attribute' => 'giftContent.min_amount',
                'value' =>  Yii::$app->formatter->asCurrency($giftContent->min_amount)
            ],
            [
                'attribute' => 'giftContent.apply_condition',
                'value' => $giftContent->type == GiftContent::TYPE_VOUCHER ?  $conditions[$giftContent->apply_condition] : 'N/A'
            ],
            [
                'attribute' => 'giftContent.apply_all',
                'value' => !empty($giftContent->apply_all) ? 'Yes' : 'No'
            ],
            [
                'attribute' => 'giftContent.apply_all_single_course',
                'value' => !empty($giftContent->apply_all_single_course) ? 'Yes' : 'No'
            ],
            [
                'attribute' => 'giftContent.apply_all_single_course_double',
                'value' => !empty($giftContent->apply_all_single_course_double) ? 'Yes' : 'No'
            ],
            [
                'attribute' => 'giftContent.apply_all_combo',
                'value' => !empty($giftContent->apply_all_combo) ? 'Yes' : 'No'
            ],
            [
                'label' => 'Khóa học áp dụng',
                'value' => !empty($giftContent->apply_all) ? 'N/A' : implode(",", $course_names)
            ],


            'is_deleted:boolean',
            'statusText',
            'created_time:datetime',
            'createdUser.email:text:Người tạo',
            'updated_time:datetime',
            'updatedUser.email:text:Người cập nhật',
        ],
    ]) ?>

</div>
