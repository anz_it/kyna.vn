<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use kartik\date\DatePicker;

use kyna\promotion\models\Promotion;
use kyna\gamification\models\Gift;
use kyna\gamification\models\GiftContent;

use common\widgets\upload\Upload;
/* @var $this yii\web\View */
/* @var $model kyna\gamification\models\Gift */
/* @var $form yii\widgets\ActiveForm */

$crudTitles = Yii::$app->params['crudTitles'];

?>

<div class="gift-form">

    <?php $form = ActiveForm::begin([
        'layout' => 'horizontal',
        'options' => ['enctype' => 'multipart/form-data'],
        'fieldConfig' => [
            'horizontalCssClasses' => [
                'label' => 'col-sm-3',
                'offset' => false,
                //'wrapper' => false,
            ],
        ],
    ]); ?>

        <?= $form->field($model, 'title', [
            'wrapperOptions' => [
                'class' => "col-sm-4"
            ]
        ])->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'image_url', [
            'wrapperOptions' => [
                'class' => "col-sm-4"
            ]
        ])->widget(Upload::className(), ['display' => 'image']) ?>

        <?= $form->field($model, 'k_point', [
            'wrapperOptions' => [
                'class' => "col-sm-2"
            ]
        ])->textInput()->hint('K Point') ?>

        <?= $form->field($content, 'type', [
            'wrapperOptions' => [
                'class' => "col-sm-4",
            ]
        ])->inline(true)->radioList(GiftContent::getTypes()) ?>

        <?= $form->field($content, 'discount_value', [
            'wrapperOptions' => [
                'class' => "col-sm-2"
            ]
        ])->textInput(['type'=>'number']) ?>

        <?= $form->field($content, 'discount_type', [
            'options' => [
                'class' => 'form-group ',
            ],
            'wrapperOptions' => [
                'class' => "col-sm-2"
            ]
        ])->dropDownList(Promotion::getDiscountTypes()) ?>


        <?= $form->field($content, 'min_amount', [
            'options' => [
                'class' => 'form-group ',
            ],
            'wrapperOptions' => [
                'class' => "col-sm-2"
            ]
        ])->textInput(['type'=>'number']) ?>


    <?= $form->field($content, 'apply_condition', [
        'wrapperOptions' => [
            'class' => "col-sm-4"
        ]
    ])->radioList(Promotion::getListCondition()) ?>




    <div class="form-group apply_condition required">
        <div class="row">
            <label class="control-label col-sm-3">Phạm vi áp dụng</label>
            <div class="col-sm-6">
                <div class="row">
                    <div class="col-lg-6">
                        <?= $form->field($content, 'apply_all_single_course')->checkbox() ?>
                    </div>
                    <div id= "applyDouble" class="col-lg-6">
                        <?= $form->field($content, 'apply_all_single_course_double')->checkbox() ?>
                    </div>
                </div>
                <div class="col-12">
                     <?= $form->field($content, 'apply_all_combo')->checkbox() ?>
                </div>
                <div class="col-12">
                     <?= $form->field($content, 'apply_all')->checkbox() ?>
                </div>
            </div>
        </div>



        <div id='couponCourse' >
            <?php if ($content->isNewRecord) { ?>
                <?= $form->field($content, 'course_id')->widget(\kartik\select2\Select2::className(), [
                    'data' => Promotion::getAvailableCourses(),
                    'options' => [
                        'id' => 'courses',
                        'multiple' => true
                    ],
                ]);
                ?>
            <?php } else { ?>
                <?php
                $modelC = \common\helpers\ArrayHelper::map($model->courses, 'id', 'name');
                foreach ($modelC as $k => $val)
                    $row[] = $k;

                echo '<label class="control-label" for="coupon-course_id">Courses</label>';
                echo \kartik\select2\Select2::widget([
                    'name' => 'Coupon[course_id]',
                    'value' => $row, //
                    'data' => $model->getAvailableCourses(),
                    'options' => ['placeholder' => 'Select a group ...', 'multiple' => true],
                    'pluginOptions' => [
                        'tags' => true,
                        'maximumInputLength' => 10
                    ],
                ]);

                ?>
            <?php } ?>
        </div>
    </div>


        <?= $form->field($model, 'status', [
            'wrapperOptions' => [
                'class' => "col-sm-3"
            ]
        ])->dropDownList(Gift::listStatus()) ?>

        <div class="form-group">
            <div class="buttons col-md-offset-3">
                <?= Html::submitButton($model->isNewRecord ? 'Thêm' : 'Cập nhật', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                <?= Html::a($crudTitles['cancel'], ['index'],['class' => 'btn btn-default']) ?>

            </div>
        </div>

    <?php ActiveForm::end(); ?>

</div>
<?php
$this->registerJs("

    $(document).ready(function () {
        if($('#giftcontent-apply_all').is(':checked')){
             $('#couponCourse').hide();
             disableSingleCombo();
        }
        
        $('.gift-form').find('form').submit(function()
        {
            var all_check = $('#giftcontent-apply_all').is(':checked');
            if(!all_check)
            {
                  var all_single_course_check = $('#giftcontent-apply_all_single_course').is(':checked');
                  var all_combo_check = $('#giftcontent-apply_all_combo').is(':checked');
                  
                 var courses = $('#courses').val();
              
                 if(!all_single_course_check && !all_combo_check && courses == null){
                   alert(\"Bạn phải chọn ít nhất một phạm vi áp dụng!\");
                   return false;
                }
            }
         
        });
        
    });
    
    $('#giftcontent-apply_all').change(function (e) {
        
        if ($(this).is(':checked')) {
            $('#couponCourse').hide();
             disableSingleCombo();
             
        } else {
            
            enableSingleCombo();
            $('#couponCourse').show();
             $('#applyDouble').hide();
        }
    });
    $('#giftcontent-apply_all_single_course').change(function(e){
        
        if(checkAllClick()){
            $('#giftcontent-apply_all').trigger('click');
        }
    });
    $('#giftcontent-apply_all_single_course_double').change(function(e){
        
        if(checkAllClick()){
            $('#giftcontent-apply_all').trigger('click');
        }
    });
    $('#giftcontent-apply_all_combo').change(function(e){
        
        if(checkAllClick()){
            $('#giftcontent-apply_all').trigger('click');
        }
    });
    function checkAllClick(){
       return $('#giftcontent-apply_all_combo').is(':checked') && $('#giftcontent-apply_all_single_course').is(':checked') && $('#giftcontent-apply_all_single_course_double').is(':checked') 
    }
    
    $('#giftcontent-apply_all_single_course').change(function (e) {
        
        if ($(this).is(':checked')) {
            $('#applyDouble').show();
        } else {
            $('#applyDouble').hide();
        }
    });
    
    var showOrderApply = $(\"input[name='Promotion[type]']\").val();
    
    
     if(showOrderApply == 1)
       {
          hideOderApply();
          $('#promotion-min_amount').attr('disabled', true);
       }
       else{
         showOderApply();
         $('#promotion-min_amount').attr('disabled', false);
       }
     var showApplyDouble = $('#promotion-apply_all_single_course').is(':checked');
     if(showApplyDouble)
     {
      $('#applyDouble').show();
     }
     else{
         $('#applyDouble').hide();
     }
    
    $(\"input[name='GiftContent[type]']\").on('change', function() {
       var type = $(this).val();
       if(type == 'coupon')
       {
          hideOderApply();
          $('#giftcontent-min_amount').attr('disabled', true);
       }
       else{
         showOderApply();
         $('#giftcontent-min_amount').attr('disabled', false);
       }
    });
    function showOderApply()
    {
       $('.field-giftcontent-apply_condition').show();
    
    }
     function hideOderApply()
    {
       $('.field-giftcontent-apply_condition').hide();
      
    }
    function enableSingleCombo()
    {
       $('#giftcontent-apply_all_single_course').attr('disabled', false);
       $('#giftcontent-apply_all_single_course').prop('checked', false);
       
       $('#giftcontent-apply_all_combo').attr('disabled', false);
       $('#giftcontent-apply_all_combo').prop('checked', false);
      
        $('#applyDouble').show();
        $('#giftcontent-apply_all_single_course_double').attr('disabled', false);
        $('#giftcontent-apply_all_single_course_double').prop('checked', false);
       
    }
    function disableSingleCombo()
    {
       $('#giftcontent-apply_all_single_course').prop('checked', true);
       $('#giftcontent-apply_all_single_course').attr('disabled', true);
       $('#giftcontent-apply_all_combo').prop('checked', true);
       $('#giftcontent-apply_all_combo').attr('disabled', true);
       $('#applyDouble').show();
       $('#giftcontent-apply_all_single_course_double').prop('checked', true);
       $('#giftcontent-apply_all_single_course_double').attr('disabled', true);
    }
    
"


);
?>

<style>
    .file-upload-text{
        max-width: 250px;
    }
</style>