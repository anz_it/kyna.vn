<?php

/* @var $this yii\web\View */
/* @var $model kyna\gamification\models\Gift */

$this->title = 'Cập nhật quà tặng: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Quản lý quà tặng', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="gift-update">

    <?= $this->render('_form', [
        'model' => $model,
        'content' => $content
    ]) ?>

</div>
