<?php

namespace app\modules\course\assets;

use yii\web\AssetBundle;

class UpdateAsset extends AssetBundle
{
    public $sourcePath = '@webroot/js/';

    public $css = [];

    public $js = [
        'angular/lib/ng-file-upload/ng-file-upload.min.js',
        'angular/update-question/controller.js',
    ];

    public $depends = [
        'backend\assets\AngularAsset',
    ];

}