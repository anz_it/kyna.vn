<?php

namespace app\modules\course;

use Yii;
use yii\filters\AccessControl;

class CourseModule extends \kyna\base\BaseModule
{

    public $controllerNamespace = 'app\modules\course\controllers';

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['default', 'category', 'meta-field'],
                'rules' => [
                    [
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->user->can('Course.View');
                        },
                    ],
                ],
            ],
        ];
    }

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }

}
