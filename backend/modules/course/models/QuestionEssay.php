<?php
/**
 * Created by PhpStorm.
 * User: khanhphan
 * Date: 10/28/16
 * Time: 3:14 PM
 */

namespace app\modules\course\models;


use kyna\course\models\QuizAnswer;
use kyna\course\models\QuizQuestion;
use yii\base\Object;

class QuestionEssay extends QuestionBase   implements QuizQuestionProcessInterface
{



    public function clearError()
    {
        unset($this->data['error']);

    }

    public function process()
    {

        if (!$this->validate()) {
            return ['success' => false, 'data' => $this->data];
        }

        $this->model->content = trim($this->data['questionContent']);
        $this->model->answer_explain = trim($this->data['answerExplain']);
        $this->model->sound_url = trim($this->data['audioUrl']);
        $this->model->image_url = trim($this->data['imageUrl']);
        $this->model->video_embed = trim($this->data['videoEmbed']);
        if ($this->model->save(false)) {

        }
        return ['success' => true, 'data' => $this->data];

    }

    public function validate()
    {
        $this->clearError();

        $noError = true;
        if (empty(trim($this->data['questionContent']))) {
            $this->data['error'] = 'Nội dung không được để trống';
            $noError = false;
        }

        return $noError;
    }

    public function buildData()
    {
        $jsonData = parent::buildData();
        return $jsonData;
    }
}