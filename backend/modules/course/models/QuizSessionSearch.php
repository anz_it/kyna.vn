<?php

namespace app\modules\course\models;

use yii\data\ActiveDataProvider;
use yii\base\Model;

use kyna\course\models\Quiz;
use kyna\course\models\QuizSession;
use kyna\course\models\Course;

class QuizSessionSearch extends QuizSession
{
    
    public $user;
    public $quizName;
    public $courseId;

    public function rules()
    {
        return [
            [['user', 'quizName'], 'string'],
            [['courseId', 'status'], 'integer'],
            [['user', 'quizName'], 'trim'],
        ] + parent::rules();
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = QuizSession::find();
        $query->alias('t');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'submit_time' => SORT_DESC
                ]
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
             $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            't.id' => $this->id,
            't.quiz_id' => $this->quiz_id,
            't.status' => $this->status
        ]);
        
        if (!empty($this->courseId) || !empty($this->quizName)) {
            $query->joinWith('quiz');
            $tblQuiz = Quiz::tableName();
            
            if (!empty($this->courseId)) {
                $query->andWhere(["$tblQuiz.course_id" => $this->courseId]);
            }
            
            if (!empty($this->quizName)) {
                $query->andWhere(["LIKE", "$tblQuiz.name", $this->quizName]);
            }
        }
        
        if (!empty($this->user)) {
            $query->join('LEFT JOIN', '{{%user}}', "user.id = t.user_id");
            $query->join('LEFT JOIN', '{{%profile}}', "profile.user_id = t.user_id");
            $query->andWhere('user.email LIKE :userInfo OR profile.name LIKE :userInfo OR profile.phone_number LIKE :userInfo', [
                ':userInfo' => "%{$this->user}%"
            ]);
        }
        
        return $dataProvider;
    }
    
    public function getCourse()
    {
        return Course::findOne($this->courseId);
    }

}