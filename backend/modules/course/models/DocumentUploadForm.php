<?php

namespace app\modules\course\models;

use Yii;
use common\lib\CDNImage;
use kyna\course\models\CourseDocument;
use kyna\course\models\Course;

class DocumentUploadForm extends \yii\base\Model
{
    public $files;
    public $course_id;
    public $allowedExt = 'pdf';

    public function rules()
    {
        return [
           [['files'], 'file', 'skipOnEmpty' => false, 'extensions' => $this->allowedExt, 'maxFiles' => 8],
       ];
    }

    public function upload()
    {
        if ($this->validate()) {
            foreach ($this->files as $file) {
                $course = Course::findOne($this->course_id);
                if (empty($course)) {
                    return false;
                }
                $course->imageSize = [];
                $cdnImage = new CDNImage($course, 'document', 'docs');
                $result = $cdnImage->upload($file);

                $document = new CourseDocument();
                $document->attributes = [
                    'title' => $file->baseName,
                    'save_path' => $result,
                    'course_id' => $this->course_id,
                    'file_ext' => $file->extension,
                    'size' => $file->size,
                    'mime_type' => $file->type,
                ];
                if (!$document->save()) {
                    return false;
                }
            }

            return true;
        } else {
            return false;
        }
    }
}
