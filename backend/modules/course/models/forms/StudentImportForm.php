<?php

namespace app\modules\course\models\forms;

class StudentImportForm extends \yii\base\Model
{
    
    public $file;
    
    public function rules()
    {
        return [
            ['file', 'required'],
            [['file'], 'file', 'skipOnEmpty' => false, 'extensions' => 'xls, xlsx'],
        ];
    }
    
    public function attributeLabels()
    {
        return [
            'file' => 'File',
        ];
    }
}
