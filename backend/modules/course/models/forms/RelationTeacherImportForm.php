<?php

namespace app\modules\course\models\forms;

use kyna\user\models\User;
use kyna\user\models\UserFollowTeacher;
use Yii;
use yii\web\UploadedFile;
use common\helpers\StringHelper;

class RelationTeacherImportForm extends \yii\base\Model
{
    
    public $file;

    public function rules()
    {
        return [
            [['file'], 'file', 'skipOnEmpty' => false, 'extensions' => 'xls, xlsx'],
        ];
    }
    
    public function attributeLabels()
    {
        return [
            'file' => 'File',
        ];
    }

    public function assign()
    {
        $this->file = UploadedFile::getInstance($this, 'file');
        if ($this->validate()) {
            $dir = Yii::getAlias("@upload") . '/relation/';
            if (!file_exists($dir)) {
                mkdir($dir, 0777, true);
            }
            $name = strtolower(StringHelper::random(15)). "." . $this->file->extension;
            $filepath = $dir . $name;
            if ($this->file->saveAs($filepath)) {
                $inputFilePath = $filepath;
                $res = $this->_process($inputFilePath);
                if(empty($res)){
                    return true;
                }
                return $res;
            }
        }
        return false;
    }

    private function _process ($filePath) {
        try {
            $inputFileType = \PHPExcel_IOFactory::identify($filePath);
            $objReader = \PHPExcel_IOFactory::createReader($inputFileType);
            $objExcel = $objReader->load($filePath);
        } catch (yii\console\Exception $ex) {
            return false;
        }

        $sheet = $objExcel->getSheet(0);
        $highestRow = $sheet->getHighestRow();
        $highestColumn = $sheet->getHighestColumn();

        $returnData = [];
        for ($row = 2; $row <= $highestRow; $row++) {
            $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, NULL);
            if ($result = $this->saveExcelRow($rowData)) {
                array_push($returnData, $result);
            }
        }
        // remove temple file
        $this->_removeFile($filePath);

        return $returnData;
    }

    private function _checkExistFile($file)
    {
        clearstatcache();
        if (!file_exists($file)) {
            touch($file);
        }
    }

    private function saveExcelRow($rowData)
    {
        $userTeacher = trim($rowData[0][0]);
        $userFollow = trim($rowData[0][1]);
        $userFollow = User::findOne(['email'=>$userFollow]);
        $userTeacher = User::findOne(['email'=>$userTeacher]);
        if($userFollow && $userTeacher){
            if($userFollow->hasRole('Relation') && $userTeacher->hasRole('Teacher')){
                $userFollowTeacher = UserFollowTeacher::findOne(['user_teacher'=>$userTeacher->getId()]);
                if(!$userFollowTeacher){
                    $userFollowTeacher = new UserFollowTeacher();
                    $userFollowTeacher->setUserTeacher($userTeacher->getId());
                    $userFollowTeacher->setUserFollow($userFollow->getId());
                    $userFollowTeacher->save();
                }else{
                    $userFollowTeacher->setUserTeacher($userTeacher->getId());
                    $userFollowTeacher->setUserFollow($userFollow->getId());
                    $userFollowTeacher->save(false);
                }
            }else{
                return 'Teacher :'.$userTeacher->email .'--' . 'Relation :' .$userFollow->email;
            }
        }else{
            return 'User với 1 trong 2 email này không tồn tại Teacher :'.trim($rowData[0][0]) .'--' . 'Relation :' .trim($rowData[0][1]);
        }
    }

    private function _removeFile($filePath)
    {
        unlink($filePath);
    }

}
