<?php
/**
 * Created by PhpStorm.
 * User: khanhphan
 * Date: 10/28/16
 * Time: 3:14 PM
 */

namespace app\modules\course\models;


use kyna\course\models\QuizAnswer;
use kyna\course\models\QuizQuestion;
use yii\base\Object;

class QuestionFillIn extends QuestionBase  implements QuizQuestionProcessInterface
{




    public function clearError()
    {
        unset($this->data['error']);
        foreach ($this->data['questions'] as &$question) {
            unset($question['error']);
            if (empty($question['answers']))
                continue;

            foreach ($question['answers'] as &$answer) {
                unset($answer['error']);
            }
        }
    }

    public function process()
    {

        if (!$this->validate()) {
            return ['success' => false, 'data' => $this->data];
        }
        $this->model->content = trim($this->data['questionContent']);
        $this->model->answer_explain = trim($this->data['answerExplain']);
        $this->model->sound_url = trim($this->data['audioUrl']);
        $this->model->image_url = trim($this->data['imageUrl']);
        $this->model->video_embed = trim($this->data['videoEmbed']);
        if ($this->model->save(false)) {
            foreach ($this->data['questions'] as &$question) {

                if (empty ($question['id'])) {
                    $questionObject = new QuizQuestion();
                } else {
                    $questionObject = QuizQuestion::findOne($question['id']);
                }

                $questionObject->parent_id = $this->model->id;
                $questionObject->type = QuizQuestion::TYPE_FILL_INTO_DOTS;
                $questionObject->content = $question['content'];
                $questionObject->course_id = $this->model->course_id;
                $questionObject->updated_time = time();

                if ($questionObject->save(false)) {
                    foreach ($question['answers'] as &$answer) {
                        if (empty ($answer['id'])) {
                            if (!empty($answer['isDeleted']))
                                continue;
                            $answerObject = new QuizAnswer();
                        } else {
                            $answerObject = QuizAnswer::findOne($answer['id']);

                            if (!empty ($answer['isDeleted'])) {
                                $answerObject->is_deleted = $answer['isDeleted'];
                            }
                        }

                        $answerObject->question_id = $questionObject->id;
                        $answerObject->content = trim($answer['content']);
                        $answerObject->created_time = time();
                        $answer['id'] = $answerObject->id;
                        $question['id'] =
                        $answerObject->save(false);
                        if (isset($answer['error']))
                            unset($answer['error']);
                    }
                }
                if (isset($question['error']))
                    unset($question['error']);
            }
        }
        unset($question);
        unset($answer);
        return ['success' => true, 'data' => $this->data];

    }

    public function validate()
    {
        $this->clearError();

        $noError = true;
        if (empty(trim($this->data['questionContent']))) {
            $this->data['error'] = 'Nội dung không được để trống';
            $noError = false;
        }
        foreach ($this->data['questions'] as &$question) {
            $answerCount = 0;
            if (empty ($question['answers'])) {
                $noError = false;
                $question['error'] = "Phải có ít nhất một câu trả lời.";
                continue;
            }
            foreach ($question['answers'] as &$answer) {
                if (!isset($answer['isDeleted']) || empty($answer['isDeleted']))
                    $answerCount += 1;
                if (empty (trim($answer['content'])) && trim($answer['content']) != "0") {
                    $answer['error'] = "Câu trả lời không được để trống.";
                    $noError = false;
                }
            }
            if ($answerCount == 0) {
                $question['error'] = "Phải có ít nhất một câu trả lời.";
                $noError = false;
            }
        }
        unset($answer);
        unset($question);
        return $noError;
    }

    /**
     * jsonData = {
     *      questionContent: "string content",
     *      questions : [
     *         {
     *           content : "",
     *           answers : [ {id : 1, content : "abc"}]
     *         },
     *         {
     *           content : "",
     *           answers : [ {id : 1, content: "xyz"}]
     *         },
     *      ]
     * }
     */
    public function buildData()
    {
        $jsonData = parent::buildData();
        $jsonData['questions'] = [];
        unset($this->model->children); // Clear cache

        foreach ($this->model->children as $child) {
            $item = [
                "content" => $child->content,
                "id" => $child->id,
                "answers" => []
            ];

            foreach ($child->answers as $answer) {
                if ($answer->is_deleted)
                    continue;
                $item["answers"][] = [
                    "id" => $answer->id,
                    "content" => $answer->content,
                    "imageUrl" => $answer->image_url
                ];
            }
            $jsonData["questions"][] = $item;
        }
        return $jsonData;
    }
}