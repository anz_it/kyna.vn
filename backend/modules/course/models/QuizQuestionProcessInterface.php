<?php
/**
 * Created by PhpStorm.
 * User: khanhphan
 * Date: 10/28/16
 * Time: 3:10 PM
 */

namespace app\modules\course\models;


interface QuizQuestionProcessInterface
{
    function clearError();

    function validate();

    function process();

    function buildData();

}