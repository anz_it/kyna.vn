<?php
/**
 * Created by PhpStorm.
 * User: khanhphan
 * Date: 10/28/16
 * Time: 3:14 PM
 */

namespace app\modules\course\models;

use kyna\course\models\QuizAnswer;
use kyna\course\models\QuizQuestion;
use yii\base\Object;

class QuestionConnect extends QuestionBase implements QuizQuestionProcessInterface
{

    public function clearError()
    {
        unset($this->data['error']);
        foreach ($this->data['questions'] as &$question) {
            unset($question['question']['error']);
            unset($question['answer']['error']);
        }
    }

    public function process()
    {
        if (!$this->validate()) {
            return ['success' => false, 'data' => $this->data];
        }
        $this->model->content = trim($this->data['questionContent']);
        $this->model->answer_explain = trim($this->data['answerExplain']);
        $this->model->sound_url = trim($this->data['audioUrl']);
        $this->model->image_url = trim($this->data['imageUrl']);
        $this->model->video_embed = trim($this->data['videoEmbed']);
        if ($this->model->save(false)) {
            foreach ($this->data['questions'] as &$question) {

                if (empty ($question['question']['id'])) {
                    if (!empty($question['isDeleted']))
                        continue;
                    $questionObject = new QuizQuestion();
                } else {
                    $questionObject = QuizQuestion::findOne($question['question']['id']);

                    if (!empty ($question['isDeleted'])) {
                        $questionObject->is_deleted = $question['isDeleted'];
                    }
                }

                $questionObject->parent_id = $this->model->id;
                $questionObject->type = QuizQuestion::TYPE_CONNECT_ANSWER;
                $questionObject->content = $question['question']['content'];
                $questionObject->image_url = isset($question['question']['imageUrl']) ? $question['question']['imageUrl'] : null;
                $questionObject->course_id = $this->model->course_id;
                $questionObject->updated_time = time();
                $questionObject->order = $question['question']['order'];

                if ($questionObject->save(false)) {
                    if (empty ($question['answer']['id'])) {
                        $answerObject = new QuizAnswer();
                    } else {
                        $answerObject = QuizAnswer::findOne($question['answer']['id']);

                    }

                    $answerObject->question_id = $questionObject->id;
                    $answerObject->content = isset($question['answer']['content']) ? trim($question['answer']['content']) : '';
                    $answerObject->image_url = isset($question['answer']['imageUrl']) ? $question['answer']['imageUrl'] : null;
                    $answerObject->created_time = time();
                    $answerObject->order = $question['answer']['order'];

                    $answerObject->save(false);

                    $question['answer']['id'] = $answerObject->id;
                    $question['question']['id'] = $questionObject->id;
                    if (isset($question['answer']['error']))
                        unset($question['answer']['error']);
                }
                if (isset($question['error']))
                    unset($question['error']);
            }
        }
        unset($question);
        unset($answer);
        return ['success' => true, 'data' => $this->data];

    }

    public function validate()
    {
        $this->clearError();

        $noError = true;
        if (empty(trim($this->data['questionContent']))) {
            $this->data['error'] = 'Nội dung không được để trống';
            $noError = false;
        }

        $questionCount = 0;
        foreach ($this->data['questions'] as &$question) {
            if (!isset($question['isDeleted']) || !$question['isDeleted'])
                $questionCount += 1;
            $questionContent = isset($question['question']['content']) ? trim($question['question']['content']) : '';
            if (empty($questionContent) && $questionContent != "0") {
                if (empty($question['question']['imageUrl'])) {
                    $noError = false;
                    $answer['error'] = "Nội dung cột A không được để trống.";
                }
            }
            $answerContent = isset($question['answer']['content']) ? trim($question['answer']['content']) : '';
            if (empty($answerContent) && $answerContent != "0") {
                if (empty($question['answer']['imageUrl'])) {
                    $noError = false;
                    $answer['error'] = "Nội dung cột B không được để trống.";
                }
            }
            if (!isset ($question['question']['order'])) {
                $question['question']['order'] = "";
            }
            if (!isset( $question['answer']['order'])) {
                $question['answer']['order'] = "";
            }

        }
        if ($questionCount == 0) {
            $noError = false;
            $this->data['error'] = "Phải có câu trả lời.";
        }
        unset($answer);
        unset($question);
        return $noError;
    }

    /**
     * jsonData = {
     *      questionContent: "string content",
     *      questions : [
     *         {
     *           question : {"id":1 , 'content' => "1" }
     *           answer : {id : 1, content : "abc"}
     *         },
     *
     *      ]
     * }
     */
    public function buildData()
    {
        $jsonData = parent::buildData();
        $jsonData['questions'] = [];

        unset($this->model->children);
        foreach ($this->model->children as $child) {

            if (empty($child->answers)) {
                $answer = new QuizAnswer();
            } else {
                $answer = array_values($child->answers)[0];
            }
            $item = [
                'question' => [
                    'content' => $child->content,
                    "id" => $child->id,
                    "order" => $child->order ? $child->order : '',
                    'imageUrl' => $child->image_url
                ],

                'answer' => [
                    'content' => $answer->content,
                    'id' => $answer->id,
                    'order' => $answer->order ? $answer->order : '',
                    'imageUrl' => $answer->image_url
                ]
            ];

            $jsonData["questions"][] = $item;
        }
        return $jsonData;
    }
}