<?php
/**
 * Created by PhpStorm.
 * User: khanhphan
 * Date: 10/28/16
 * Time: 3:14 PM
 */

namespace app\modules\course\models;

use kyna\course\models\QuizAnswer;

class QuestionChoice extends QuestionBase  implements QuizQuestionProcessInterface
{

    public function clearError()
    {
        unset($this->data['error']);
        foreach ($this->data['answers'] as &$answer) {
           unset($answer['error']);
        }
    }

    public function process()
    {
        if (!$this->validate()) {
            return ['success' => false, 'data' => $this->data];
        }

        $this->model->content = trim($this->data['questionContent']);
        $this->model->answer_explain = trim($this->data['answerExplain']);
        $this->model->sound_url = trim($this->data['audioUrl']);
        $this->model->image_url = trim($this->data['imageUrl']);
        $this->model->video_embed = trim($this->data['videoEmbed']);

        if ($this->model->save(false)) {
            foreach ($this->data['answers'] as &$answer) {

                if (empty ($answer['id'])) {
                    if (!empty($answer['isDeleted']))
                        continue;
                    $answerObject = new QuizAnswer();
                } else {
                    $answerObject = QuizAnswer::findOne($answer['id']);

                    if (!empty ($answer['isDeleted'])) {
                        $answerObject->is_deleted = $answer['isDeleted'];
                    }
                }
                $answerObject->question_id = $this->model->id;
                $answerObject->content = isset($answer['content']) ? trim($answer['content']) : '';
                $answerObject->is_correct = isset($answer['isCorrect']) ? $answer['isCorrect'] : 0;
                $answerObject->image_url = isset($answer['imageUrl']) ? $answer['imageUrl'] : null;
                $answerObject->created_time = time();
                $answerObject->save(false);
                $answer['id'] = $answerObject->id;
            }
        }

        unset($question);
        unset($answer);

        return ['success' => true, 'data' => $this->data];
    }

    public function validate()
    {
        $this->clearError();

        $noError = true;
        if (empty(trim($this->data['questionContent']))) {
            $this->data['error'] = 'Nội dung không được để trống';
            $noError = false;
        }
        if (empty ($this->data['answers'])) {
            $this->data['error'] = "Phải có ít nhất một câu trả lời";
        }

        $hasCorrect = 0;
        foreach ($this->data['answers'] as &$answer) {
            if (isset($answer['isCorrect']) && $answer['isCorrect']) {
                $hasCorrect += 1;
            }
            $content = isset($answer['content']) ? trim($answer['content']) : '';
            if (empty($content) && $content != "0") {
                if (empty($answer['imageUrl'])) {
                    $noError = false;
                    $answer['error'] = "Nội dung câu trả lời không được để trống.";
                }
            }
        }
        if ($hasCorrect == 0) {
            $this->data['error'] = "Phải có câu trả lời đúng.";
            $noError = false;
        }
        unset($answer);
        return $noError;
    }

    public function buildData()
    {
        $jsonData = parent::buildData();
        $jsonData['answers'] = [];
        foreach ($this->model->answers as $answer) {
            if ($answer->is_deleted)
                continue;
            $jsonData['answers'][] = [
                'id' => $answer->id,
                'content' => $answer->content,
                'isCorrect' => $answer->is_correct,
                "imageUrl" => $answer->image_url
            ];
        }
        return $jsonData;
    }
}