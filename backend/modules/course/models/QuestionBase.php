<?php
/**
 * Created by PhpStorm.
 * User: khanhphan
 * Date: 11/13/16
 * Time: 10:13 PM
 */

namespace app\modules\course\models;


use kyna\course\models\QuizQuestion;
use yii\base\Object;

/**
 * Class QuestionBase
 * @package app\modules\course\models
 * @property QuizQuestion $model
 */
class QuestionBase extends Object
{
    /**
     * @var QuizQuestion $model
     *
     */

    public $model;

    /**
     * @var $data
     * is raw data from client
     */
    public $data;

    public function buildData()
    {
        $jsonData = [];
        $jsonData['questionContent'] = $this->model->content;
        $jsonData['answerExplain'] = $this->model->answer_explain;
        $jsonData['id'] = $this->model->id;
        $jsonData['type'] = $this->model->type;
        $jsonData['imageUrl'] = $this->model->image_url;
        $jsonData['audioUrl'] = $this->model->sound_url;
        $jsonData['videoEmbed'] = $this->model->video_embed;

        return $jsonData;
    }
}