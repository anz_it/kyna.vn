<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use common\helpers\StringHelper;
use app\modules\course\components\TreeView;
use kartik\tree\Module;

$crudTitles = Yii::$app->params['crudTitles'];

?>

<div class="clearfix"></div>
<br>

<?php
echo TreeView::widget([
    'id' => 'course-treeview',
    'courseId' => $model->id,
    'query' => $sectionQuery,
    'headingOptions' => [
        'label' => 'Cấu trúc khóa học',
        'style' => 'margin-right: 0px',
    ],
    'rootOptions' => ['label' => '<span class="text-primary"> '.StringHelper::truncateWords($model->name, 8).' </span>'],
    'fontAwesome' => true,     // optional
    'isAdmin' => true,         // optional (toggle to enable admin mode)
    'displayValue' => $sectionId,        // initial display value
    'nodeView' => '@app/modules/course/views/tree-manager/_form',
    'gridView' => '@app/modules/course/views/tree-manager/_gridview',
    'nodeActions' => [
        Module::NODE_MANAGE => Url::to(['/course/tree-manager/manage', 'courseId' => $model->id]),
    ],
]);
?>

<?php
$script = "
    ;(function($, window, document, undefined) {
        $(document).ready(function(){
            $('a.btn').tooltip();

            addCss = function (el, css) {
                el.removeClass(css).addClass(css);
            }

            $('body').on('treeview.beforeselect', '#course-treeview', function (event, key, jqXHR, settings) {
                var gridview = $('#course-treeview-grid');

                console.log(key);

                $.ajax({
                    type: 'post',
                    dataType: 'json',
                    data: {
                        'id': key,
                        'courseId' : '".$model->id."'
                    },
                    url: '/course/tree-manager/gridview',
                    beforeSend: function (jqXHR, settings) {
                        gridview.html('');
                        addCss(gridview, 'kv-loading');
                    },
                    success: function (data, textStatus, jqXHR) {
                        gridview.html(data.out);
                        gridview.removeClass('kv-loading');
                        // form reset
                        gridview.find('a.btn').tooltip();
                    },
                    complete: function (jqXHR) {
                        //self.validateTooltips();
                    }
                });
            });
        });
    })(window.jQuery, window, document);
    ";

$this->registerJs($script, View::POS_END, 'tree-select');
?>
