<?php

use common\helpers\FontAwesomeHelper;
use yii\bootstrap\ActiveForm;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\bootstrap\Html;
?>
<nav class="navbar navbar-default">
    <?php $form = ActiveForm::begin([
            'options' => ['class' => 'navbar-form navbar-left'],
        ]); ?>
        <?= $form->field($formModel, 'files[]', [
            'template' => '<label class="btn btn-default" for="doc-upload"><i class="fa fa-cloud-upload fa-fw"></i> Upload {input}</label>',
        ])->fileInput([
            'id' => 'doc-upload',
            'class' => 'form-control hide',
            'multiple' => true,
            'accept' => 'application/pdf|application/x-pdf',
        ]) ?>
    <?php ActiveForm::end(); ?>
    <p class="navbar-text">
        chỉ hỗ trợ file <code>.<?= $formModel->allowedExt ?></code>
    </p>
</nav>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'emptyText' => 'Khóa học này hiện không có tài liệu bổ sung.',
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        [
            'attribute' => 'title',
            'format' => 'raw',
            'value' => function ($model) {
                return '<a href="' . Url::toRoute(['/course/view/doc-download/', 'id' => $model->id]) . '" style="display:block">' .
                        FontAwesomeHelper::file($model->mime_type, ['3x', 'pull-left']) .
                        '<strong>' . $model->title . '</strong> <small class="text-muted">[Click to download]</small>' .
                        '<br>' .
                        '<em class="text-muted">' . $model->save_path . '</em>' .
                        '</a>';
            }
        ],
        'size:shortSize',
        'mime_type',
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{delete} ',
            'controller' => 'course-view',
            'buttons' => [
                'delete' => function ($url, $model, $key) {
                    $url = Url::toRoute([
                        '/course/view/doc-delete',
                        'id' => $model->id,
                        'redirectUrl' => Url::toRoute(['/course/view/doc', 'id' => $model->course_id]),
                    ]);

                    $options = array_merge([
                        'title' => Yii::t('yii', 'Delete'),
                        'aria-label' => Yii::t('yii', 'Delete'),
                        'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
                        'data-method' => 'post',
                        'data-pjax' => '0',
                    ]);

                    return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, $options);
                },
            ],
        ],
    ],

]) ?>

<?php

$js = <<<JS
;(function($) {
    $("#doc-upload").on("change", function(e) {
        $("#$form->id").submit();
    });
})(jQuery);
JS;

$this->registerJs($js);
