<?php
use yii\bootstrap\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;

?>

<?= Html::beginForm(Url::toRoute(['/course/view/qna-answer/']), 'post', [
    'class' => 'reply-form',
]) ?>

<?= Html::activeHiddenInput($formModel, 'question_id') ?>
<?= Html::activeHiddenInput($formModel, 'course_id') ?>
<div class="form-group">
    <?= Html::activeTextarea($formModel, 'content', [
        'class' => 'form-control reply-text',
        'rows' => 1,
        'placeholder' => 'Trả lời',
    ]) ?>
</div>
<button type="submit" class="btn btn-primary hide reply-button">Gửi trả lời</button>

<?= Html::endForm(); ?>
