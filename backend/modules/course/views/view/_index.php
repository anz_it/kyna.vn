<?php

use yii\widgets\DetailView;
use yii\helpers\Html;
use common\helpers\CDNHelper;

$crudTitles = Yii::$app->params['crudTitles'];
$crudButtonIcons = Yii::$app->params['crudButtonIcons'];

?>

    <p class="pull-right">
        <?= Html::a($crudButtonIcons['back'] . " " . $crudTitles['back'], Yii::$app->request->referrer, ['class' => 'btn btn-info', 'icon' => 'glyphicon glyphicon-backward']) ?>
        <?php
        if ($user->can('Course.Update')) {
            echo Html::a($crudButtonIcons['update'] . " " . $crudTitles['update'], ['/course/default/update', 'id' => $model->id], ['class' => 'btn btn-primary']) . ' ';
        }
        if ($user->can('Course.Delete')) {
            echo Html::a($crudButtonIcons['delete'] . " " . $crudTitles['delete'], ['/course/default/delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => $crudTitles['deleteConfirm'],
                    'method' => 'post',
                ],
            ]);
        }
        ?>
    </p>

<?= DetailView::widget([
    'model' => $model,
    'attributes' => [
        'id',
        [
            'attribute' => 'image_url',
            'format' => 'raw',
            'value' => CDNHelper::image($model->image_url, [
                    'size' => CDNHelper::IMG_SIZE_THUMBNAIL_SMALL,
                    'alt' => $model->name,
                    'forceCreate' => true,
                ]).'<br>'.Html::a($model->image_url, $model->image_url, ['target' => '_blank'])
                .CDNHelper::image($model->image_url, [
                    'size' => CDNHelper::IMG_SIZE_THUMBNAIL,
                    'alt' => $model->name,
                    'forceCreate' => true,
                    'class'=>'hide'
                ]),
        ],
        [
            'attribute' => 'type',
            'value' => $model->typeText
        ],
        'name',

        'short_name',
        [
            'attribute' => 'level',
            'value' => $model->levelText
        ],
        [
            'attribute' => 'purchase_type',
            'value' => $model->purchaseTypeText
        ],
        'price',
        'price_discount',
//            'percent_discount',
        'slug:ntext',
        'video_url:url',
        [
            'attribute' => 'video_cover_image_url',
            'format' => 'raw',
            'value' => CDNHelper::image($model->video_cover_image_url, [
                    'size' => CDNHelper::IMG_SIZE_THUMBNAIL_SMALL,
                    'alt' => $model->name,
                    'forceCreate' => true,
                ]).'<br>'.Html::a($model->video_cover_image_url, $model->video_cover_image_url, ['target' => '_blank']),
        ],
        'totalTimeText',
        [
            'attribute' => 'category_id',
            'value' => !empty($model->category->name) ? $model->category->name : 'N/A'
        ],
        [
            'attribute' => 'teacher_id',
            'value' => !empty($model->teacher->profile->name) ? $model->teacher->profile->name : 'N/A'
        ],
        [
            'attribute' => 'is_4kid',
            'value' => !empty($model->is_4kid) ? 'Yes' : 'No'
        ],
        [
            'attribute' => 'status',
            'value' => $model->statusText
        ],

        'overview:html:Giới thiệu',
        'description:ntext',
        'created_time:datetime',
        'updated_time:datetime',
        'redirect_url:url',
        'affiliate_commission_percent'
    ],
]) ?>