<?php

use yii\grid\GridView;
use yii\bootstrap\Html;
use yii\widgets\Pjax;
use kartik\widgets\Select2;
use kyna\course\models\search\QuizQuestionSearch;
use yii\helpers\Url;
use app\modules\course\controllers\QuizQuestionController;
use kyna\course\models\QuizQuestion;
use \kyna\course\models\search\CourseOpinionSearch;
use common\helpers\CDNHelper;
use common\components\SortableColumn;


$crudTitles = Yii::$app->params['crudTitles'];
$crudButtonIcons = Yii::$app->params['crudButtonIcons'];
$user = Yii::$app->user;

?>
<div>
    <p class="pull-left">
        <?= Html::a($crudTitles['create'], ['opinion/create', 'course_id' => $model->id], ['class' => 'btn btn-primary']) ?>

    </p>
    <div class="clearfix"></div>

    <?php // Pjax::begin(['id' => 'abc', 'timeout' => Yii::$app->params['timeOutPjax']]) ?>
    <?=

    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'layout' => "{items}\n{pager}\n{summary}",
        'rowOptions' => function ($model, $key, $index, $grid) {
            return ['data-sortable-id' => $model->id];
        },
        'options' => [
            'data' => [
                'sortable-widget' => 1,
                'sortable-url' => \yii\helpers\Url::toRoute(['/course/opinion/sorting']),
            ]
        ],
        'columns' => [
            [
                'class' => SortableColumn::className(),
                'header' => Yii::t('yii', 'Order')
            ],
            [
                'attribute' => 'id',
                'options' => ['class' => 'col-xs-2'],
            ],
            'description:html',
            [
                'attribute' => 'avatar_url',
                'format' => 'html',
                'value' => function ($model) {
                    return CDNHelper::image($model->avatar_url, [
                        'size' => CDNHelper::IMG_SIZE_THUMBNAIL_SMALL,
                        'alt' => $model->avatar_url,
                        'forceCreate' => true,
                        'width' => '100px'
                    ]);
                },
                'filter' => false,
                'options' => ['class' => 'col-xs-1']
            ],
            [
                'attribute' => 'user_name',
                'options' => ['class' => 'col-xs-2'],
            ],
            [
                'attribute' => 'status',
                'value' => function ($model) {
                    return $model->getStatusButton(Url::to(['/course/opinion/change-status', 'id' => $model->id]));
                },
                'format' => 'raw',
                'filter' => Select2::widget([
                    'model' => $searchModel,
                    'attribute' => 'status',
                    'data' => CourseOpinionSearch::listStatus(),
                    'options' => ['placeholder' => $crudTitles['prompt']],
                    'pluginOptions' => [
                        'allowClear' => true,
                    ],
                    'hideSearch' => true,
                ]),
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update} {delete} ',
                'controller' => 'opinion',
            ],
        ],
    ]);
    ?>
    <?php // Pjax::end() ?>
</div>
<!-- Button trigger modal -->
