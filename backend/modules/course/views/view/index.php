<?php

use yii\helpers\Html;
use yii\bootstrap\Nav;

$this->title = $viewParams['model']->name;
$this->params['breadcrumbs'][] = ['label' => Yii::$app->controller->mainTitle, 'url' => ['/course/default/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="becourse-view">

    <?= Nav::widget([
        'items' => $tabs,
        'options' => ['class' => 'nav-pills'],
    ]) ?>

    <?= $this->render("_$view", $viewParams) ?>
</div>
