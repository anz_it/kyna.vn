<?php

use yii\widgets\ListView;

?>
<?= ListView::widget([
    'dataProvider' => $dataProvider,
    'emptyText' => 'Hãy trở thành người đầu tiên đặt câu hỏi cho giảng viên!',
    'layout' => '{items}',
    'options' => [
        'tag' => 'ul',
        'class' => 'media-list',
    ],
    'itemOptions' => [
        'tag' => 'li',
        'class' => 'media',
    ],
    'itemView' => function ($model) use ($formModel) {
        $formModel->question_id = $model->id;

        if (!$model->hasAnswer) {
            $answerForm = $this->render('_qna-reply-form', [
                'formModel' => $formModel
            ]);
        }
        else {
            $answer = $model->answer;
            $answerForm = '<div class="media-left"><img src="https://randomuser.me/api/portraits/lego/5.jpg" width="50" alt="" /></div>' .
                '<div class="media-body">' .
                '<h4 class="media-heading">' . $answer->user->profile->name . '</h4>' .
                '<footer class="text-muted">' .
                '<small>Trả lời lúc <time>' . $answer->postedTime . '</time></small>' .
                '</footer>' .
                '<div class="media-content">' . $answer->content . '</div>' .
                '</div>';
        }

        return '<div class="media-left"><img src="https://randomuser.me/api/portraits/lego/5.jpg" width="50" alt="" /></div>' .
            '<div class="media-body">' .
            '<h4 class="media-heading">' . $model->user->profile->name . '</h4>' .
            '<footer class="text-muted">' .
            '<small>Hỏi <time>' . $model->postedTime . '</time></small>' .
            '</footer>' .
            '<div class="media-content">' . $model->content . '</div>' .
            '<div class="media">' . $answerForm . '</div>' .
            '</div>';
    }
]) ?>

<?php

$js = <<<JS
;(function($) {
    $(".reply-text").focus(function() {
        $(this).attr("rows", 4);
        $(this).parent().next().removeClass("hide");
    });
    $('.reply-form').one("submit", function(e) {
        e.preventDefault();

        var theForm = $(this);
        $.post(this.action, theForm.serialize(), function(resp) {
            theForm.parent().html(resp);
        });
        return false;
    });
})(jQuery);
JS;

$this->registerJs($js);
