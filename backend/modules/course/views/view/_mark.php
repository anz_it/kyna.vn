<?php

use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\grid\GridView;
use yii\helpers\StringHelper;
use kyna\course\models\QuizSession;

$crudTitles = Yii::$app->params['crudTitles'];
$this->params['breadcrumbs'] = [];
$this->params['breadcrumbs'][] = ['label' => 'Khóa học', 'url' => ['/couse/default/index']];
$this->params['breadcrumbs'][] = ['label' => StringHelper::truncateWords($searchModel->course->name, 6), 'url' => ['/course/view/index', 'id' => $searchModel->courseId]];
$this->params['breadcrumbs'][] = 'Chấm điểm';

?>
<div class="row">
    <div class="col-xs-12">
        <div class="course-quiz-index">
            <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

            <p>
            </p>

            <div class="box">
                <?php Pjax::begin(['timeout' => Yii::$app->params['timeOutPjax']]) ?>
                <?=
                GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],
                        'start_time:datetime',
                        [
                            'attribute' => 'user',
                            'label' => 'Học viên',
                            'format'    => 'raw',
                            'value' => function($model) {
                                return "{$model->user->profile->name}<br>
                                <a href='mailto:{$model->user->email}'>{$model->user->email}</a><br>
                                {$model->user->profile->phone_number}";
                            }
                        ],
                        [
                            'header' => 'Tên bài quiz',
                            'attribute' => 'quizName',
                            'format'    => 'raw',
                            'value' => function($model){
                                return $model->quiz->name;
                            }
                        ],
                        [
                            'header' => 'Tổng điểm hiện có',
                            'value' => function($model){
                                return $model->total_score . ' / ' . $model->total_quiz_score;
                            }
                        ],
                        [
                            'attribute' => 'status',
                            'format' => 'html',
                            'value' => function ($model) {
                                return $model->statusText;
                            },
                            'filter' => Html::activeDropDownList($searchModel, 'status', QuizSession::listStatus(), ['class' => 'form-control', 'prompt' => $crudTitles['prompt']]), 
                        ],
                        [
                            'class' => 'yii\grid\ActionColumn',
                            'template' => "{mark}",
                            'buttons' => [
                                'mark' => function($url, $model, $key){
                                    $url = ['/course/mark/session', 'id' => $model->id];

                                    $options = [
                                        'title' => Yii::t('yii', 'Chấm điểm'),
                                        'aria-label' => Yii::t('yii', 'Chấm điểm'),
                                        'data-pjax' => '0',
                                    ];

                                    return Html::a('<span class="fa fa-pencil-square"></span>', $url, $options);
                                }
                            ]
                        ],
                    ],
                ]);
                ?>
                <?php Pjax::end() ?>
            </div>
        </div>
    </div>
</div>
