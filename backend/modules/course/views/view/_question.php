<?php

use yii\grid\GridView;
use yii\bootstrap\Html;
use yii\widgets\Pjax;
use kartik\widgets\Select2;
use kyna\course\models\search\QuizQuestionSearch;
use yii\helpers\Url;
use app\modules\course\controllers\QuizQuestionController;
use kyna\course\models\QuizQuestion;


$crudTitles = Yii::$app->params['crudTitles'];
$crudButtonIcons = Yii::$app->params['crudButtonIcons'];
?>
<div>
    <p class="pull-right">
        <?php // Html::a($crudButtonIcons['create'].' '.$crudTitles['create'], ['/course/quiz-question/create', 'courseId' => $model->id], ['class' => 'btn btn-success'])
        ?>
        <button type="button" class="btn btn-success" data-toggle="modal" data-target="#add-modal">
            <i class="fa fa-fw fa-plus"></i> Thêm
        </button>
    </p>
    <div class="clearfix"></div>

    <?php Pjax::begin(['id' => 'quiz-pjax', 'timeout' => Yii::$app->params['timeOutPjax']]) ?>
        <?=
        GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'layout' => "{items}\n{pager}\n{summary}",
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                [
                    'attribute' => 'id',
                    'options' => ['class' => 'col-xs-2'],
                ],
                [
                    'attribute' => 'type',
                    'value' => function ($model) {
                        return $model->typeText;
                    },
                    'filter' => Select2::widget([
                        'model' => $searchModel,
                        'attribute' => 'type',
                        'data' => QuizQuestionSearch::getTypes(),
                        'options' => ['placeholder' => $crudTitles['prompt']],
                        'pluginOptions' => [
                            'allowClear' => true,
                        ],
                        'hideSearch' => true,
                    ]),
                ],
                'content:html',
                [
                    'attribute' => 'image_url',
                    'format' => 'html',
                    'value' => function ($model) {
                        return !empty($model->image_url) ? Html::img($model->image_url, ['width' => '100px']) : null;
                    },
                    'filter' => false,
                    'options' => ['class' => 'col-xs-1'],
                ],
                [
                    'attribute' => 'status',
                    'value' => function ($model) {
                        return $model->getstatusButton(Url::to(['/course/quiz-question/change-status', 'id' => $model->id]));
                    },
                    'format' => 'raw',
                    'filter' => Select2::widget([
                        'model' => $searchModel,
                        'attribute' => 'status',
                        'data' => QuizQuestionSearch::listStatus(),
                        'options' => ['placeholder' => $crudTitles['prompt']],
                        'pluginOptions' => [
                            'allowClear' => true,
                        ],
                        'hideSearch' => true,
                    ]),
                ],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{update} {delete} {listQuestion}',
                    'controller' => 'quiz-question',
                    'visibleButtons' => [
                        'listQuestion' => function ($model, $key, $index) {
                            return $model->type !== QuizQuestion::TYPE_WRITING;
                        },
                    ],
                    'buttons' => [
                        'delete' => function ($url, $model, $key) {
                            $url = Url::toRoute([
                                '/course/quiz-question/delete',
                                'id' => $model->id,
                                'redirectUrl' => Url::toRoute(['/course/view/question', 'id' => $model->course_id]),
                            ]);

                            $options = array_merge([
                                'title' => Yii::t('yii', 'Delete'),
                                'aria-label' => Yii::t('yii', 'Delete'),
                                'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
                                'data-method' => 'post',
                                'data-pjax' => '0',
                            ]);

                            return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, $options);
                        },
                        'listQuestion' => function ($url, $model, $key) {
                            return Html::a('<span class="fa fa-plus"></span>',
                                ['/course/quiz-question/update', 'id' => $model->id, 'tab' => QuizQuestionController::TAB_ANSWER],
                                [
                                    'title' => 'Thêm câu trả lời',
                                ]
                            );
                        },
                    ],
                ],
            ],
        ]);
        ?>
    <?php Pjax::end() ?>
</div>
<!-- Button trigger modal -->


<!-- Modal -->
<div class="modal fade" id="add-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <?= $form = Html::beginForm(Url::to(['/course/quiz-question/create', 'courseId' => $model->id]), 'post', [
                    'id'=>'create-question',
                    'type' => 'inline'
                ]); ?>
                    <div class="form-group">
                        <label>Bạn muốn tạo câu hỏi cho loại nào ? </label>
                        <select class="form-control" name="type">
                            <?php
                            foreach (QuizQuestion::getTypes() as $type => $text) {
                                echo "<option value ='{$type}'>{$text}</option>";
                            }
                            ?>
                        </select>
                    </div>
                <?= Html::endForm() ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                <button type="button" class="btn btn-primary" onclick="$('#create-question').submit()">Tiếp tục <i class="glyphicon glyphicon-arrow-right"></i></button>
            </div>
        </div>
    </div>
</div>