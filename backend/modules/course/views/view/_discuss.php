<?php

use yii\widgets\ListView;
use yii\helpers\Url;

?>
<?= ListView::widget([
    'dataProvider' => $dataProvider,
    'emptyText' => 'Hãy trở thành người đầu tiên tham gia thảo luận.',
    'layout' => '{items}',
    'options' => [
        'tag' => 'ul',
        'class' => 'media-list',
    ],
    'itemOptions' => [
        'tag' => 'li',
        'class' => 'media',
    ],
    'itemView' => './discuss/_item',
]) ?>

<?php

$js = <<<JS
;(function($) {
    $(".reply-text").focus(function() {
        $(this).attr("rows", 4);
        $(this).parent().next().removeClass("hide");
    });

    $('.lesson-form-reply').bind('ajax:complete', function() {
        console.log('234');
        $(this).find('button[type=\'reset\']').trigger('click');
    });
})(jQuery);
JS;

$this->registerJs($js);