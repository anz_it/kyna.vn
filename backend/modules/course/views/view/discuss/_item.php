<div class="media-left"><img src="https://randomuser.me/api/portraits/lego/5.jpg" width="50" alt="" /></div>
<div class="media-body">
    <h4 class="media-heading"><?= $model->user->profile->name ?></h4>
    <footer class="text-muted">
        <small><time><?= $model->postedTime ?></time></small>
    </footer>
    <div class="media-content"><?= $model->comment ?></div>
    
    <ul class="bottom">
<!--        <li>1 <a href="#"><i class="fa fa-thumbs-o-up" aria-hidden="true"></i></a></li>
        <li class="icon">●</li>  -->
        <li><a href="#lesson-form-reply-<?= $model->id ?>" class="btn-reply"><i class="fa fa-comment-o" aria-hidden="true"></i> Trả lời</a></li>
    </ul>
    
    <div id='listview-replies-<?= $model->id ?>'>
        <?= $this->render('_reply-list', ['model' => $model]) ?>
    </div>
    
    <?= $this->render('_reply-form', ['model' => $model]) ?>
</div>