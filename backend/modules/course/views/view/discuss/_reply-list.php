<?php

use yii\widgets\ListView;
use yii\data\ActiveDataProvider;

?>

<?= ListView::widget([
    'dataProvider' => new ActiveDataProvider(['query' => $model->getComments()]),
    'layout' => '{items}',
    'options' => [
        'tag' => 'ul',
        'class' => 'media-list',
    ],
    'itemOptions' => [
        'tag' => 'li',
        'class' => 'media',
    ],
    'itemView' => '_reply-item',
    'emptyText' => false,
]) ?>