<?php

use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\helpers\Html;
use kyna\course\models\CourseDiscussion;

$user = Yii::$app->user->identity;

$mDiscuss = new CourseDiscussion();
$mDiscuss->course_id = $model->course_id;
$mDiscuss->user_id = $user->id;
$mDiscuss->parent_id = $model->id;
?>

<div class="wrap-reply lesson-form-reply " id="lesson-form-reply-<?= $model->id ?>" role="group"> 
    <div class="media">
        <div class="media-left">
            <a href="#"><img src="https://randomuser.me/api/portraits/lego/5.jpg" width="50" alt="" /></a>
        </div>
        <div class="media-body">
            <div class="content-sub">                                                                                                                                                                                                 
                <div class="box-comment">
                    <?php $form = ActiveForm::begin([
                        'id' => 'discuss-form',
                        'action' => Url::toRoute(['/course/view/discuss', 'id' => $model->course_id]),
                        'options' => [
                            'data-ajax' => true,
                            'data-target' => '#listview-replies-' . $model->id
                        ],
                    ]) ?>                                        
                        <?= Html::activeHiddenInput($mDiscuss, 'course_id') ?>
                        <?= Html::activeHiddenInput($mDiscuss, 'user_id') ?>
                        <?= Html::activeHiddenInput($mDiscuss, 'parent_id') ?>
                        <div class="form-group">
                            <?= Html::activeTextarea($mDiscuss, 'comment', [
                                'rows' => '1',
                                'placeholder' => 'Nhập trả lời của bạn ở đây',
                                'class' => 'form-control reply-text',
                            ]) ?>
                        </div>
                        <div class='buttons hide'>
                            <button type="submit" class="btn btn btn-primary button-reply">Trả lời</button>
                            <button type="reset" class="btn btn-default close-form">Hủy</button>
                        </div>
                    <?php ActiveForm::end() ?>
                </div>                                                                                           
            </div><!--end .content-sub--> 
        </div>
    </div><!--end .media-->                                                                                                                                                                                                                                                                                                                                                     
</div><!--end .wrap-reply-->  