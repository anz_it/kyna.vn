<?php

use yii\helpers\Url;
use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;
use yii\web\JsExpression;
use yii\widgets\ActiveForm;

use kartik\widgets\Select2;
use kyna\user\models\UserCourse;

$crudTitles = Yii::$app->params['crudTitles'];
$crudButtonIcons = Yii::$app->params['crudButtonIcons'];

$select2WidgetOptions = [
    'model' => $searchModel,
    'data' => $searchModel->getBooleanOptions(),
    'options' => ['placeholder' => $crudTitles['prompt']],
    'pluginOptions' => [
        'allowClear' => true,
    ],
    'hideSearch' => true,
]
?>
<nav class="navbar navbar-default">
    <?php $form = ActiveForm::begin([
        'options' => [
            'enctype' => 'multipart/form-data',
            'class' => 'navbar-form navbar-left'
        ]
    ]) ?>

        <?php
        $initText = !empty($newModel->user_id) ? ((!empty($newModel->student->profile->name) ? ($newModel->student->profile->name . ' - ') : '') . $newModel->student->email) : '';

        echo $form->field($newModel, 'user_id', [
            'options' => ['class' => 'form-group', 'style' => 'width: 250px'],
        ])->widget(Select2::classname(), [
            'options' => ['placeholder' => '--Chọn học viên--'],
            'initValueText' => $initText,
            'pluginOptions' => [
                'allowClear' => true,
                'minimumInputLength' => 8,
                'language' => [
                    'errorLoading' => new JsExpression("function () { return 'Chờ nhận kết quả...'; }"),
                ],
                'ajax' => [
                    'url' => Url::toRoute(['/user/api/search']),
                    'dataType' => 'json',
                    'data' => new JsExpression('function(params) { return {q:params.term}; }'),
                ],
                'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                'templateResult' => new JsExpression('function(res) { return res.text; }'),
                'templateSelection' => new JsExpression('function (res) { return res.text; }'),
            ],
        ])->label(false)->error(false); ?>

        <?= Html::submitButton($crudButtonIcons['create'] . ' ' . $crudTitles['create'], ['class' => 'btn btn-success']) ?>

        <?= Html::button(Html::tag('i', '', ['class' => 'glyphicon glyphicon-export']).' Export học viên', [
            'id' => 'export_xls',
            'class' => 'btn btn-default'
        ]) ?>

    <?php ActiveForm::end() ?>
    <?php
        if ($newModel->hasErrors()) {
            $errors = [];
            foreach($newModel->errors as $error) {
                $errors[] = $error[0];
            }
            echo "<span class='navbar-text navbar-left' style='color: red'>". implode(', ', $errors) ."</span>";
        }
     ?>
</nav>

<?php Pjax::begin() ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'layout' => "{items}\n{pager}\n{summary}",
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'userInfo',
                'format' => 'html',
                'value' => function ($model) {
                    return "{$model->student->profile->name}<br>
                            <a href='mailto:{$model->student->email}'>{$model->student->email}</a><br>
                            {$model->student->profile->phone_number}";
                },
            ],
            [
                'attribute' => 'created_time',
                'format' => 'datetime',
                'filter' => false,
            ],
            [
                'attribute' => 'is_started',
                'format' => 'raw',
                'value' => function ($model) {
                    if ($model->is_started == UserCourse::BOOL_NO) {
                        return '<span class="label label-warning">' . UserCourse::BOOL_NO_TEXT . '</span>';
                    } else {
                        $options = [
                            'title' => "Thiết đặt lại khóa học cho học viên",
                            'data-confirm' => Yii::t('yii', 'Bạn có chắc là thiết đặt lại k hóa học này cho học viên: ' . $model->student->profile->name . '?'),
                            'data-method' => 'post',
                        ];
                        return '<span class="label label-success">' . UserCourse::BOOL_YES_TEXT . '</span> <br><br><span class="fa fa-hand-o-right"></span> ' . Html::a('Reset', ['/course/learning/reset', 'id' => $model->id], $options);
                    }
                },
                'filter' => Select2::widget(array_merge($select2WidgetOptions, ['attribute' => 'is_started'])),
            ],
            [
                'attribute' => 'is_graduated',
                'format' => 'raw',
                'value' => function ($model) {
                    if ($model->is_graduated == UserCourse::BOOL_YES) {
                        $ret = '<span class="label label-success">' . UserCourse::BOOL_YES_TEXT . '</span><br><br>';
                        $ret .= Html::a('Tạo lại Chứng chỉ', ['/course/certificate/index', 'userId' => $model->user_id, 'courseId' => $model->course_id], [
                            'target' => '_blank',
                            'data-pjax' => '0'
                        ]);
                    } else {
                        $ret = '<span class="label label-warning">' . UserCourse::BOOL_NO_TEXT . '</span>';
                        if ($model->is_activated == UserCourse::BOOL_YES) {
                            $options = [
                                'title' => "Tốt nghiệp",
                                'data-confirm' => Yii::t('yii', 'Bạn có chắc là học viên: ' . $model->student->profile->name . ' đã tốt nghiệp khóa học này?'),
                                'data-method' => 'post',
                            ];

                            $ret .= '<br><br><span class="fa fa-hand-o-right"></span> ' . Html::a('Tốt nghiệp <span class="fa fa-graduation-cap"></span>', ['/course/learning/graduate', 'id' => $model->id], $options);
                        }
                    }
                    return $ret;
                },
                'filter' => Select2::widget(array_merge($select2WidgetOptions, ['attribute' => 'is_graduated'])),
            ],
            [
                'attribute' => 'is_activated',
                'format' => 'raw',
                'value' => function ($model) {
                    if ($model->is_activated == UserCourse::BOOL_YES) {
                        return '<span class="label label-success">' . UserCourse::BOOL_YES_TEXT . '</span>';
                    } else {
                        $options = [
                            'title' => Yii::t('yii', 'Active khóa học cho user: ' . $model->student->profile->name),
                            'data-confirm' => Yii::t('yii', 'Bạn có chắc là muốn active khóa học này cho học viên: ' . $model->student->profile->name),
                            'data-method' => 'post',
                        ];
                        return '<span class="label label-warning">' . UserCourse::BOOL_NO_TEXT . '</span> <br><br><span class="fa fa-hand-o-right"></span> ' . Html::a('Active', ['/course/learning/active', 'id' => $model->id], $options);
                    }
                },
                'filter' => Select2::widget(array_merge($select2WidgetOptions, ['attribute' => 'is_activated'])),
            ],
            [
                'attribute' => 'is_quick',
                'format' => 'raw',
                'value' => function ($model) {
                    if ($model->is_quick == UserCourse::BOOL_YES) {
                        $options = [
                            'title' => Yii::t('yii', 'Học cấp tốc cho user: ' . $model->student->profile->name),
                            'data-confirm' => Yii::t('yii', 'Bạn có chắc là huỷ khóa học này cho học viên: ' . $model->student->profile->name . ' học cấp tốc??'),
                            'data-method' => 'post',
                        ];
                        return '<span class="label label-success">' . UserCourse::BOOL_YES_TEXT . '</span><br><br><span class="fa fa-hand-o-right"></span> ' . Html::a('Huỷ học cấp tốc', ['/course/learning/un-quick', 'id' => $model->id], $options);
                    } else {
                        $options = [
                            'title' => Yii::t('yii', 'Học cấp tốc cho user: ' . $model->student->profile->name),
                            'data-confirm' => Yii::t('yii', 'Bạn có chắc là muốn chuyển khóa học này cho học viên: ' . $model->student->profile->name . ' học cấp tốc??'),
                            'data-method' => 'post',
                        ];
                        return '<span class="label label-warning">' . UserCourse::BOOL_NO_TEXT . '</span> <br><br><span class="fa fa-hand-o-right"></span> ' . Html::a('Học cấp tốc', ['/course/learning/quick', 'id' => $model->id], $options);
                    }
                },
                'filter' => Select2::widget(array_merge($select2WidgetOptions, ['attribute' => 'is_quick'])),
            ],
            [
                'attribute' => 'activation_date',
                'format' => 'datetime',
                'label' => 'Ngày kích hoạt',
                'filter' => false,
                'value' => function ($model) {
                    return !empty($model->activation_date) ? $model->activation_date : null;
                }
            ],
            [
                'attribute' => 'expiration_date',
                'format' => 'datetime',
                'label' => 'Ngày hết hạn kích hoạt',
                'filter' => false,
                'value' => function ($model) {
                    return !empty($model->expiration_date) ? $model->expiration_date : null;
                }
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{delete}',
                'buttons' => [
                    'delete' => function ($url, $model, $key) {
                        $url = Url::toRoute(['/course/view/delete-student', 'id' => $model->id]);

                        $options = [
                            'title' => Yii::t('yii', 'Delete'),
                            'aria-label' => Yii::t('yii', 'Delete'),
                            'data-confirm' => Yii::t('yii', 'Bạn có chắc là xóa học viên này khỏi khóa học?'),
                            'data-method' => 'post',
                            'data-pjax' => false,
                        ];

                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, $options);
                    }
                ],
            ]
        ],
    ]) ?>
<?php Pjax::end() ?>

<?php
$script = "
    ;(function($, window, document, undefined){
        $(document).ready(function(){
            $('body').on('click', '#export_xls', function (event) {            
                event.preventDefault();
                var exportDialog = new BootstrapDialog();
                exportDialog.setTitle('Vui lòng nhập email nhận file export');
                exportDialog.setMessage($('<input id=\"email_export\" class=\"form-control\" placeholder=\"Email\" value=\"".Yii::$app->user->identity->email."\"></input>'));
                exportDialog.setType(BootstrapDialog.TYPE_SUCCESS);
                exportDialog.setButtons([{
                    label: 'Export',
                    action: function(dialog) {                       
                        var email = $('#email_export').val(); 
                        if (email == '') {
                            alert('Vui lòng nhập email');
                        }
                        var data = {email: email, type: 'export'};
                        $.post('', data, function (response) {
                            if (response.result) {
                                var bootstrapDialog = new BootstrapDialog();
                                bootstrapDialog.setMessage('Vui lòng kiểm tra email '+email+'. File Export sẽ được gửi đến trong vài phút.');
                                bootstrapDialog.setType(BootstrapDialog.TYPE_SUCCESS);
                                bootstrapDialog.open(); 
                                exportDialog.close();          
                            } else {
                                
                            }
                        });
                    }
                }]);
                exportDialog.open(); 
            });
        });
    })(window.jQuery || window.Zepto, window, document);";
$css = "
    .modal-content .modal-header {
        border-radius: 0;
    }
    ";
?>
<?php
$this->registerCss($css);
$this->registerJs($script, \yii\web\View::POS_END, 'export-course-student');
?>
