<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model kyna\user\models\UserCourse */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'User Courses', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-course-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'user_id',
            'course_id',
            'process',
            'activation_date',
            'is_activated:boolean',
            'is_started:boolean',
            'started_date',
            'expiration_date',
            'is_graduated:boolean',
            'method',
            'is_deleted:boolean',
            'status',
            'created_time:datetime',
            'updated_time:datetime',
            'is_quick:boolean',
        ],
    ]) ?>

</div>
