<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\widgets\ActiveForm;
use yii\web\View;

$this->title = 'Danh sách học viên';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-course-index">
    <?php if (Yii::$app->user->can('User.Import')) : ?>
        <nav class="navbar navbar-default">
            <?php $form = ActiveForm::begin([
                'options' => [
                    'enctype' => 'multipart/form-data',
                    'class' => 'navbar-form navbar-left'
                ],
                'action' => Url::toRoute(['import-user'])
            ]) ?>

                <?= $form->field($importForm, 'file', [
                    'template' => '<label class="btn btn-default" for="student-upload"><i class="fa fa-upload fa-fw"></i> Nhập từ file excel {input}</label>',
                ])->fileInput([
                    'id' => 'student-upload',
                    'class' => 'form-control hide',
                ]) ?>

            <?php ActiveForm::end() ?>
        </nav>
    <?php endif; ?>
    <?php Pjax::begin(['timeout' => Yii::$app->params['timeOutPjax']]); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'user_id:integer:Id',
            [
                'attribute' => 'userFullName',
                'label' => 'Tên',
                'value' => function ($model) {
                    return !is_null($model->student) ? $model->student->profile->name : null;
                }
            ],
            [
                'attribute' => 'userPhone',
                'label' => 'Số điện thoại',
                'value' => function ($model) {
                    return !is_null($model->student) ? $model->student->profile->phone_number : null;
                }
            ],
            [
                'attribute' => 'userEmail',
                'label' => 'Email',
                'value' => function ($model) {
                    return !is_null($model->student) ? $model->student->email : null;
                }
            ],
            [
                'attribute' => 'totalCourse',
                'label' => 'Tổng số khóa học',
                'filter' => false
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{login}',
                'buttons' => [
                    'login' => function ($url, $model) {
                        if (is_null($model->student)) {
                            return null;
                        }
                        $frontendUrl = Yii::$app->params['baseUrl'];
                        $url = $frontendUrl . Url::toRoute(['/user/security/force-login', 'user_id' => $model->user_id, 'hash' => $model->student->auth_key]);
                        
                        return Html::a('<span class="fa fa-sign-in"></span>', $url, ['title' => 'Force login', 'target' => '_blank', 'data-pjax' => 0]);
                    }
                ],
            ],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>

<?php
if (Yii::$app->user->can('User.Import')) {
    $js = <<<JS
;(function($) {
    $("#student-upload").on("change", function(e) {
        $("#$form->id").submit();
    });
})(jQuery);
JS;

$this->registerJs($js, View::POS_END);
}
?>