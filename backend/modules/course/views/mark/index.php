<?php

use yii\helpers\Html;
use yii\grid\GridView;

$crudTitles = Yii::$app->params['crudTitles'];

$this->title = Yii::$app->controller->mainTitle;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-xs-12">
        <div class="course-quiz-index">
            <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

            <p>
            </p>

            <div class="box">
                <?=
                GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],
                        [
                            'attribute' => 'user',
                            'format'    => 'raw',
                            'value' => function($model){
                                return $model->user->profile->name;
                            }
                        ],
                        [
                            'header' => 'Tên khoá học',
                            'format'    => 'raw',
                            'value' => function($model){
                                $text = "<b>" . $model->quiz->course->name . "</b><br />";
//                                $text .= "<b>" . $model->quiz->course->section->name . "</b>";
                                return $text;
                            }
                        ],
                        [
                            'header'    => 'Loại bài quiz',
                            'format'    => 'raw',
                            'value'     => function($model){
                                return $model->quiz->typeText;
                            }
                        ],
                        [
                            'header'    => 'Trạng thái',
                            'format'    => 'raw',
                            'value'     => function($model){
                                return $model->statusesText;
                            }
                        ],
                        [
                            'header' => 'Tên bài quiz',
                            'format'    => 'raw',
                            'value' => function($model){
                                return $model->quiz->name;
                            }
                        ],
                        [
                            'class' => 'yii\grid\ActionColumn',
                            'template' => "{update}{delete}",
                            'buttons' => [
                                'update' => function($url, $model, $key){
                                    $url = ['/course/mark/update', 'id' => $model->id];

                                    $options = [
                                        'title' => Yii::t('yii', 'Update'),
                                        'aria-label' => Yii::t('yii', 'Update'),
                                        'data-pjax' => '0',
                                    ];

                                    return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, $options);
                                }
                            ]
                        ],
                    ],
                ]);
                ?>
            </div>
        </div>
    </div>
</div>
