<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\StringHelper;

$this->title = Yii::$app->controller->mainTitle;

$this->params['breadcrumbs'][] = ['label' => 'Khóa học', 'url' => ['/couse/default/index']];
$this->params['breadcrumbs'][] = ['label' => StringHelper::truncateWords($model->quiz->course->name, 6), 'url' => ['/course/view/index', 'id' => $model->quiz->course_id]];
$this->params['breadcrumbs'][] = ['label' => 'Chấm điểm', 'url' => ['/course/view/mark', 'id' => $model->quiz->course_id]];

?>

<div class="quiz-answer-update">
    <div class="alert alert-info">
        Lưu ý: Chấm trên thang điểm 10
    </div>
    <?php $form = ActiveForm::begin() ?>
        <ul>
            <?php foreach ($answerModels as $key => $answer): ?>
                <?php
                $answer->scenario = 'mark';
                ?>
                <?php if (!empty($answer->question)) : ?>
                    <li>
                        <div class="">
                            <label>Câu hỏi <?= $key + 1 ?>: <?= strip_tags($answer->question->content) ?></label>
                        </div>
                        <div class="">
                            <?= $form->field($answer, 'answer[' . $answer->id . ']')->textarea(['disabled' => true, 'value' => $answer->answer])->label(false); ?>
                        </div>
                        <?= $form->field($answer, 'score[' . $answer->id . ']')->textInput(['value' => null])->label("Số điểm"); ?>
                    </li>
                <?php else: ?>
                    Hiện tại những câu trả lời từ phiên bản cũ chưa được cập nhật trên hệ thống mới. Vui lòng thử lại
                <?php endif ?>
            <?php endforeach; ?>
        </ul>
        <div class="form-group">
            <button type="submit" class="btn btn-success"><i class="fa fa-pencil-square-o"></i> Chấm Điểm</button>
        </div>
    <?php ActiveForm::end() ?>

</div>
