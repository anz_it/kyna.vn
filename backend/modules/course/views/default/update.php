<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\BECourse */

$crudTitles = Yii::$app->params['crudTitles'];

$this->title = $crudTitles['update'] . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::$app->controller->mainTitle, 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['/course/view/index', 'id' => $model->id]];
$this->params['breadcrumbs'][] = $crudTitles['update'];
?>
<div class="becourse-update">

    <?= $this->render('_form', [
        'model' => $model,
        'metaValueModels' => $metaValueModels,
        'courseCommissionModel' => $courseCommissionModel,
        'teachingAssistant' => $teachingAssistant
    ]) ?>

</div>
