<?php

use yii\widgets\DetailView;
use yii\helpers\Html;

$crudTitles = Yii::$app->params['crudTitles'];
$crudButtonIcons = Yii::$app->params['crudButtonIcons'];

$user = Yii::$app->user;
?>

<p class="pull-right">
    <?= Html::a($crudButtonIcons['back'] . " " . $crudTitles['back'], ['index'], ['class' => 'btn btn-info', 'icon' => 'glyphicon glyphicon-backward']) ?>
    <?php
    if ($user->can('Course.Update')) {
        echo Html::a($crudButtonIcons['update'] . " " . $crudTitles['update'], ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) . ' ';
    }
    if ($user->can('Course.Delete')) {
        echo Html::a($crudButtonIcons['delete'] . " " . $crudTitles['delete'], ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => $crudTitles['deleteConfirm'],
                'method' => 'post',
            ],
        ]);
    }
    ?>
</p>

<?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'image_url',
                'format' => 'html',
                'value' => Html::img($model->image_url, ['width' => '200px']),
            ],
            'name',
            'short_name',
            [
                'attribute' => 'level',
                'value' => $model->levelText
            ],
            'price',
            'price_discount',
//            'percent_discount',
            'slug:ntext',
            'video_url:ntext',
            'video_cover_image_url:ntext',
            'totalTimeText',
            [
                'attribute' => 'category_id',
                'value' => $model->category->name
            ],
            [
                'attribute' => 'teacher_id',
                'value' => $model->teacher->profile->name
            ],
            [
                'attribute' => 'status',
                'value' => $model->statusText
            ],
            //'facebook_thumbnail:ntext',
            'overview:html',
            //'keyword',
            'description:ntext',
            'created_time:datetime',
            'updated_time:datetime',
        ],
    ]) ?>
