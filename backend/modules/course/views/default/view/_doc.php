<?php
use yii\bootstrap\Html;
use yii\bootstrap\ActiveForm;

?>
<?php $form = ActiveForm::begin(); ?>
    <?= $form->field($courseDocument, 'download_url')->fileInput() ?>
<?php ActiveForm::end(); ?>
