<?php

use yii\grid\GridView;
use yii\helpers\Url;
use yii\widgets\Pjax;
use yii\helpers\Html;
use kartik\widgets\Select2;
use common\components\SortableColumn;
use kyna\course\models\search\QuizSearch;
use app\modules\course\controllers\DefaultController;
use app\modules\course\controllers\QuizController;

$searchModel = new QuizSearch();
$searchModel->course_id = $model->id;
$dataProvider = $searchModel->search(Yii::$app->request->queryParams);

$crudTitles = Yii::$app->params['crudTitles'];
?>
<p class="pull-right">
    <?= Html::a($crudTitles['create'], ['/course/quiz/create', 'courseId' => $model->id], ['class' => 'btn btn-success']) ?>
</p>

<?php Pjax::begin(['id' => 'quiz-pjax']) ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'layout' => "{items}\n{pager}\n{summary}",
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'id',
                'options' => ['class' => 'col-xs-2']
            ],
            'name',
            'image_url:ntext',
            'description:ntext',
            [
                'attribute' => 'status',
                'value' => function ($model) {
                    return Html::a(
                            $model->statusHtml,
                            Url::toRoute([
                                '/course/quiz/change-status',
                                'id' => $model->id,
                                'redirectUrl' => Url::toRoute(['/course/view/index', 'id' => $model->course_id, 'tab' => DefaultController::TAB_QUIZ])
                            ]),
                            ['title' => 'Thay đổi tình trạng']
                        );
                },
                'format' => 'html',
                'filter' => Select2::widget([
                    'model' => $searchModel,
                    'attribute' => 'status',
                    'data' => QuizSearch::listStatus(),
                    'options' => ['placeholder' => $crudTitles['prompt']],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                    'hideSearch' => true,
                ])
            ],
            // 'status',
            // 'created_time:datetime',
            // 'updated_time:datetime',
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update} {delete} {listQuestion}',
                'controller' => 'quiz',
                'buttons' => [
                    'delete' => function ($url, $model, $key) {
                        $url = Url::toRoute([
                            '/course/quiz/delete', 
                            'id' => $model->id, 
                            'redirectUrl' => Url::toRoute(['/course/view/index', 'id' => $model->course_id, 'tab' => DefaultController::TAB_QUIZ])
                        ]);
                    
                        $options = array_merge([
                            'title' => Yii::t('yii', 'Delete'),
                            'aria-label' => Yii::t('yii', 'Delete'),
                            'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
                            'data-method' => 'post',
                            'data-pjax' => '0',
                        ]);
                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, $options);
                    },
                    'listQuestion' => function ($url, $model, $key) {
                        return Html::a('<span class="fa fa-plus"></span>', 
                            ['/course/quiz/update', 'id' => $model->id, 'tab' => QuizController::TAB_QUESTION],
                            [
                                'title' => 'Thêm câu hỏi'
                            ]
                        );
                    }
                ]
            ],
        ],
    ]);
    ?>
<?php Pjax::end() ?>