<?php

use yii\grid\GridView;
use yii\widgets\Pjax;
use kartik\widgets\Select2;
use kyna\user\models\search\UserCourseSearch;

$searchModel = new UserCourseSearch();
$searchModel->course_id = $model->id;
$dataProvider = $searchModel->search(Yii::$app->request->queryParams);

$crudTitles = Yii::$app->params['crudTitles'];

$select2WidgetOptions = [
    'model' => $searchModel,
    'data' => UserCourseSearch::getBooleanOptions(),
    'options' => ['placeholder' => $crudTitles['prompt']],
    'pluginOptions' => [
        'allowClear' => true
    ],
    'hideSearch' => true,
]
?>
<?php Pjax::begin() ?>
    <?=
        GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'layout' => "{items}\n{pager}\n{summary}",
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                [
                    'attribute' => 'userInfo',
                    'format' => 'html',
                    'value' => function ($model) {
                        return "{$model->student->profile->name}<br>
                                <a href='mailto:{$model->student->email}'>{$model->student->email}</a><br>
                                {$model->student->profile->phone_number}";
                    }
                ],
                'created_time:datetime',
                [
                    'attribute' => 'is_started',
                    'format' => 'boolean',
                    'filter' => Select2::widget(array_merge($select2WidgetOptions, ['attribute' => 'is_started']))
                ],
                [
                    'attribute' => 'is_graduated',
                    'format' => 'boolean',
                    'filter' => Select2::widget(array_merge($select2WidgetOptions, ['attribute' => 'is_graduated']))
                ],
                [
                    'attribute' => 'is_activated',
                    'format' => 'boolean',
                    'filter' => Select2::widget(array_merge($select2WidgetOptions, ['attribute' => 'is_activated']))
                ],
                'activation_date:datetime',
                'expiration_date:datetime',
//                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]);
    ?>
<?php Pjax::end() ?>