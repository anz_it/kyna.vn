<?php

use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\helpers\Url;
use kartik\widgets\Select2;

use app\modules\course\controllers\DefaultController;
use app\modules\course\controllers\QuizQuestionController;
use kyna\course\models\CourseQuestion;
use kyna\course\models\search\CourseQuestionSearch;

$searchModel = new CourseQuestionSearch();
$searchModel->course_id = $model->id;
$dataProvider = $searchModel->search(Yii::$app->request->queryParams);

$crudTitles = Yii::$app->params['crudTitles'];
$crudButtonIcons = Yii::$app->params['crudButtonIcons'];
?>
<div>
    <p class="pull-right">
        <?= Html::a($crudButtonIcons['create'] . " " . $crudTitles['create'], ['/course/quiz-question/create', 'courseId' => $model->id], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(['id' => 'quiz-pjax']) ?>
        <?=
        GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'layout' => "{items}\n{pager}\n{summary}",
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                [
                    'attribute' => 'id',
                    'options' => ['class' => 'col-xs-2']
                ],
                [
                    'attribute' => 'type',
                    'value' => function ($model) {
                        return $model->typeText;
                    },
                    'filter' => Select2::widget([
                        'model' => $searchModel,
                        'attribute' => 'type',
                        'data' => CourseQuestionSearch::getTypes(),
                        'options' => ['placeholder' => $crudTitles['prompt']],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                        'hideSearch' => true,
                    ])
                ],
                'content:html',
                [
                    'attribute' => 'image_url',
                    'format' => 'html',
                    'value' => function ($model) {
                        return Html::img($model->image_url, ['width' => '100px']);
                    },
                    'filter' => false,
                    'options' => ['class' => 'col-xs-1']
                ],
                [
                    'attribute' => 'status',
                    'value' => function ($model) {
                        return Html::a(
                                $model->statusHtml,
                                Url::toRoute([
                                    '/course/quiz-question/change-status',
                                    'id' => $model->id,
                                    'redirectUrl' => Url::toRoute(['/course/view/question', 'id' => $model->course_id])
                                ]),
                                ['title' => 'Thay đổi tình trạng']
                            );
                    },
                    'format' => 'html',
                    'filter' => Select2::widget([
                        'model' => $searchModel,
                        'attribute' => 'status',
                        'data' => CourseQuestionSearch::listStatus(),
                        'options' => ['placeholder' => $crudTitles['prompt']],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                        'hideSearch' => true,
                    ])
                ],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{update} {delete} {listQuestion}',
                    'controller' => 'quiz-question',
                    'visibleButtons' => [
                        'listQuestion' => function ($model, $key, $index) {
                            return $model->type !== CourseQuestion::TYPE_WRITING;
                        },
                    ],
                    'buttons' => [
                        'delete' => function ($url, $model, $key) {
                            $url = Url::toRoute([
                                '/course/quiz-question/delete', 
                                'id' => $model->id, 
                                'redirectUrl' => Url::toRoute(['/course/view/question', 'id' => $model->course_id])
                            ]);

                            $options = array_merge([
                                'title' => Yii::t('yii', 'Delete'),
                                'aria-label' => Yii::t('yii', 'Delete'),
                                'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
                                'data-method' => 'post',
                                'data-pjax' => '0',
                            ]);
                            return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, $options);
                        },
                        'listQuestion' => function ($url, $model, $key) {
                            return Html::a('<span class="fa fa-plus"></span>', 
                                ['/course/quiz-question/update', 'id' => $model->id, 'tab' => QuizQuestionController::TAB_ANSWER],
                                [
                                    'title' => 'Thêm câu trả lời'
                                ]
                            );
                        }
                    ]
                ],
            ],
        ]);
        ?>
    <?php Pjax::end() ?>
</div>