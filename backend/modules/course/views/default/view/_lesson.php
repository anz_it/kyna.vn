<?php

use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;

use app\modules\course\components\TreeView;
use kyna\course\models\CourseSection;
use kyna\course\models\search\CourseLessonSearch;
use kartik\tree\Module;

$searchModel = new CourseLessonSearch();
$searchModel->course_id = $model->id;
$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        
$crudTitles = Yii::$app->params['crudTitles'];

$user = Yii::$app->user;
?>

<div class="clearfix"></div>
<br>

<?php
echo TreeView::widget([
    'id' => 'course-treeview',
    'courseId' => $model->id,
    'query' => CourseSection::find()->where(['course_id' => $model->id])->addOrderBy('root, lft'), 
    'headingOptions' => [
        'label' => 'Cấu trúc khóa học',
        'style' => 'margin-right: 0px'
    ],
    'rootOptions' => ['label'=>'<span class="text-primary"> ' . \yii\helpers\StringHelper::truncateWords($model->name, 8) . ' </span>'],
    'fontAwesome' => true,     // optional
    'isAdmin' => true,         // optional (toggle to enable admin mode)
    'displayValue' => !empty(Yii::$app->request->get('sectionId')) ? Yii::$app->request->get('sectionId') : 0,        // initial display value
    'nodeView' => '@app/modules/course/views/tree-manager/_form',
    'gridView' => '@app/modules/course/views/tree-manager/_gridview',
    'nodeActions' => [
        Module::NODE_MANAGE => Url::to(['/course/tree-manager/manage', 'courseId' => $model->id]),
    ],
]);
?>

<?php
$script = "
    ;(function($, window, document, undefined) {
        $(document).ready(function(){
            $('a.btn').tooltip();
            
            addCss = function (el, css) {
                el.removeClass(css).addClass(css);
            }
            
            $('body').on('treeview.beforeselect', '#course-treeview', function (event, key, jqXHR, settings) {
                var gridview = $('#course-treeview-grid');
                
                console.log(key);

                $.ajax({
                    type: 'post',
                    dataType: 'json',
                    data: {
                        'id': key,
                        'courseId' : '" . $model->id . "'
                    },
                    url: '/course/tree-manager/gridview',
                    beforeSend: function (jqXHR, settings) {
                        gridview.html('');
                        addCss(gridview, 'kv-loading');
                    },
                    success: function (data, textStatus, jqXHR) {
                        gridview.html(data.out);
                        gridview.removeClass('kv-loading');
                        // form reset
                        gridview.find('a.btn').tooltip();
                    },
                    complete: function (jqXHR) {
                        //self.validateTooltips();
                    }
                });
            });
        });
    })(window.jQuery || window.Zepto, window, document);
    ";

$this->registerJs($script, View::POS_END, 'tree-select');
?>
