<?php

use yii\helpers\Html;

$this->title = Yii::$app->params['crudTitles']['create'];
$this->params['breadcrumbs'][] = ['label' => Yii::$app->controller->mainTitle, 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="becourse-create">

    <?= $this->render('_form', [
        'model' => $model,
        'metaValueModels' => $metaValueModels,
        'courseCommissionModel' => $courseCommissionModel,
        'teachingAssistant' => $teachingAssistant
    ]) ?>

</div>
