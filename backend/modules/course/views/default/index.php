<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use yii\web\JsExpression;
use kartik\export\ExportMenu;
use kartik\grid\GridView;

use common\helpers\RoleHelper;
use common\helpers\CDNHelper;
use kyna\course\models\Course;
use kyna\course\models\Category;

$crudTitles = Yii::$app->params['crudTitles'];
$crudButtonIcons = Yii::$app->params['crudButtonIcons'];

$this->title = Yii::$app->controller->mainTitle;
$this->params['breadcrumbs'][] = $this->title;

$user = Yii::$app->user;
?>
<div class="row">
    <div class="col-xs-12">
        <div class="becourse-index">
            <nav class="navbar navbar-default">
                <?php
                if ($user->can('Course.Create')) {
                    echo Html::a($crudButtonIcons['create'] . ' ' . $crudTitles['create'], ['create'], ['class' => 'btn btn-success navbar-btn navbar-left']);
                }
                ?>
                <?= $this->render('_search_custom', ['queryParams' => $queryParams]) ?>
            </nav>

            <div class="box">
                <?php
                $teacherDesc = empty($searchModel->teacher_id) ? '' : $searchModel->teacher->profile->name;
                $courseTypeDesc = $searchModel->typeText;
                ?>
                <?php Pjax::begin(['timeout' => Yii::$app->params['timeOutPjax'], 'id' => 'idPjaxReload']) ?>
                <?=
                GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],
                        [
                            'attribute' => 'id',
                            'options' => ['class' => 'col-xs-1']
                        ],
                        'name',
                        'slug',
                        [
                            'attribute' => 'type',
                            'value' => function (Course $model) {
                                return $model->typeText;
                            },
                            'filter' => Select2::widget([
                                'model' => $searchModel,
                                'initValueText' => $courseTypeDesc,
                                'attribute' => 'type',
                                'data' => Course::listTypes(),
                                'options' => ['placeholder' => $crudTitles['prompt']],
                                'pluginOptions' => [
                                    'allowClear' => true
                                ],
                            ])
                        ],
                        [
                            'attribute' => 'teacher_id',
                            'value' => function ($model) {
                                return !empty($model->teacher) ? $model->teacher->profile->name : null;
                            },
                            'filter' => Select2::widget([
                                'model' => $searchModel,
                                'initValueText' => $teacherDesc,
                                'attribute' => 'teacher_id',
                                'options' => ['placeholder' => $crudTitles['prompt']],
                                'pluginOptions' => [
                                    'allowClear' => true,
                                    'minimumInputLength' => 3,
                                    'language' => [
                                        'errorLoading' => new JsExpression("function () { return 'Chá» nháº­n káº¿t quáº£...'; }"),
                                    ],
                                    'ajax' => [
                                        'url' => Url::toRoute(['/user/api/search', 'role' => RoleHelper::ROLE_TEACHER, 'selectNameOnly' => true]),
                                        'dataType' => 'json',
                                        'data' => new JsExpression('function(params) { return {q:params.term}; }'),
                                    ],
                                    'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                                    'templateResult' => new JsExpression('function(teacher) { return teacher.text; }'),
                                    'templateSelection' => new JsExpression('function (teacher) { return teacher.text; }'),
                                ],
                            ])
                        ],
                        [
                            'attribute' => 'price',
                            'format' => 'currency',
                            'options' => ['class' => 'col-xs-1']
                        ],
                        [
                            'attribute' => 'category_id',
                            'value' => function ($model) {
                                return !empty($model->category) ? $model->category->name : null;
                            },
                            'filter' => Select2::widget([
                                'model' => $searchModel,
                                'attribute' => 'category_id',
                                'data' => ArrayHelper::map(Category::findAllActive(), 'id', 'name'),
                                'options' => ['placeholder' => $crudTitles['prompt']],
                                'pluginOptions' => [
                                    'allowClear' => true
                                ],
                            ])
                        ],
                        [
                            'attribute' => 'image_url',
                            'format' => 'html',
                            'value' => function ($model) {
                                return CDNHelper::image($model->image_url, [
                                    'size' => CDNHelper::IMG_SIZE_THUMBNAIL_SMALL,
                                    'alt' => $model->name,
                                    'forceCreate' => true,
                                    'width' => '100px'
                                ]);
                            },
                            'filter' => false,
                            'options' => ['class' => 'col-xs-1']
                        ],
                        [
                            'attribute' => 'status',
                            'value' => function ($model)
                            {
                                return $model->statusButton;
                            },
                            'format' => 'raw',
                            'filter' => Select2::widget([
                                'model' => $searchModel,
                                'attribute' => 'status',
                                'data' => Course::listStatus(false),
                                'options' => ['placeholder' => $crudTitles['prompt']],
                                'pluginOptions' => [
                                    'allowClear' => true
                                ],
                                'hideSearch' => true,
                            ])
                        ],
                        [
                            'class' => 'app\components\ActionColumnCustom',
                            'template' => '{view} {student} {lesson} {document} {question} {qna} {discuss} {update}',
                            'buttons' => [
                                'student' => function ($url, $model, $key) {
                                    $url = Url::toRoute(['/course/view/student', 'id' => $model->id]);

                                    $options = [
                                        'title' => 'Danh sÃ¡ch há»c viÃªn',
                                        'data-pjax' => '0',
                                    ];
                                    return Html::a('<span class="glyphicon glyphicon-user"></span>', $url, $options);
                                },
                                'lesson' => function ($url, $model, $key) {
                                    $url = Url::toRoute(['/course/view/lesson', 'id' => $model->id]);

                                    $options = [
                                        'title' => 'BÃ i giáº£ng/BÃ i táº­p',
                                        'data-pjax' => '0',
                                    ];
                                    return Html::a('<span class="fa fa-tasks"></span>', $url, $options);
                                },
                                'discuss' => function ($url, $model, $key) {
                                    $url = Url::toRoute(['/course/view/discuss', 'id' => $model->id]);

                                    $options = [
                                        'title' => 'Tháº£o luáº­n',
                                        'data-pjax' => '0',
                                    ];
                                    return Html::a('<span class="fa fa-comments"></span>', $url, $options);
                                },
                                'discuss' => function ($url, $model, $key) {
                                    $url = Url::toRoute(['/course/view/discuss', 'id' => $model->id]);

                                    $options = [
                                        'title' => 'Tháº£o luáº­n',
                                        'data-pjax' => '0',
                                    ];
                                    return Html::a('<span class="fa fa-comments"></span>', $url, $options);
                                },
                                'document' => function ($url, $model, $key) {
                                    $url = Url::toRoute(['/course/view/doc', 'id' => $model->id]);

                                    $options = [
                                        'title' => 'TÃ i liá»‡u',
                                        'data-pjax' => '0',
                                    ];
                                    return Html::a('<span class="fa fa-folder"></span>', $url, $options);
                                },
                                'qna' => function ($url, $model, $key) {
                                    $url = Url::toRoute(['/course/view/qna', 'id' => $model->id]);

                                    $options = [
                                        'title' => 'Há»i Ä‘Ã¡p',
                                        'data-pjax' => '0',
                                    ];
                                    return Html::a('<span class="fa fa-question-circle"></span>', $url, $options);
                                },
                                'view' => function ($url, $model, $key) {
                                    $url = Url::toRoute(['/course/view/index', 'id' => $model->id]);

                                    $options = [
                                        'title' => Yii::t('yii', 'ThÃ´ng tin khÃ³a há»c'),
                                        'aria-label' => Yii::t('yii', 'View'),
                                        'data-pjax' => '0',
                                    ];
                                    return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, $options);
                                }
                            ],
                            'visibleButtons' => [
                                'update' => $user->can('Course.Update'),
                                'view' => $user->can('Course.View'),
                                'delete' => $user->can('Course.Delete'),
                            ],
                        ],
                    ],
                ]);
                ?>
                <?php Pjax::end() ?>
            </div>
        </div>
    </div>
</div>