<?php

use yii\web\View;
use yii\helpers\Url;
use yii\bootstrap\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\web\JsExpression;

use common\widgets\upload\Upload;
use common\helpers\RoleHelper;
use kyna\course\models\Category;
use kyna\course\models\Course;
use kyna\course\models\CourseCommissionType;
use app\components\MetaFieldType;

use kartik\select2\Select2;
use kartik\widgets\DatePicker;
use kyna\partner\models\Partner;


$crudTitles = Yii::$app->params['crudTitles'];

$types = CourseCommissionType::find()->where(['status' => CourseCommissionType::STATUS_ACTIVE])->all();
$jsTypeData = [];
foreach ($types as $type) {
    $jsTypeData[$type->id] = [
        'default_percent' => $type->default_percent,
        'is_required_expiration_date' => $type->is_required_expiration_date
    ];
}

?>

<?= common\widgets\Alert::widget() ?>

    <div class="becourse-form">

        <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

        <div class="row">
            <div class="col-lg-6">
                <?= $form->field($model, 'name')->textInput([
                    'maxlength' => true,
                    'onchange' => "$('#cat-slug').val(convertToSlug($(this).val()))",
                ]) ?>
            </div>
            <div class="col-lg-3">
                <?= $form->field($model, 'level')->widget(Select2::classname(), [
                    'data' => Course::listLevel(),
                    'hideSearch' => true,
                    'options' => ['placeholder' => $crudTitles['prompt']],
                    'pluginOptions' => [
                        'allowClear' => true,

                    ],

                ]) ?>
            </div>
            <div class="col-lg-3">
                <div class="form-group field-course-total_time_number">
                    <?= Html::activeLabel($model, 'total_time_number') ?>
                    <div class="form-inline ">
                        <?= $form->field($model, 'total_time_number')->label(false)->error(false) ?>
                        <?= Html::dropDownList('total_time_unit', $model->totalTimePartials['unit'], Course::totalTimeUnits(), [
                            'class' => 'form-control'
                        ]) ?>
                        <?= Html::error($model, 'total_time_number', ['class' => 'help-block']); ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-6">
                <?= $form->field($model, 'short_name')->textInput([
                    'maxlength' => true,
                    'placeholder' => 'Tối đa 30 ký tự, không dấu, không ký tự đặc biệt (*)',
                    'onchange' => "$('#cat-slug').val(convertToSlug($(this).val()))",
                ])->hint("(SMS)") ?>
            </div>
            <div class="col-lg-6">
                <?= $form->field($model, 'slug')->textInput(['id' => 'cat-slug']) ?>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-3">
                <?= $form->field($model, 'purchase_type')->widget(Select2::classname(), [
                    'data' => Course::listPurchaseType(),
                    'hideSearch' => true,
                    'options' => ['placeholder' => $crudTitles['prompt']],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]) ?>
            </div>
            <div class="col-lg-3">
                <?= $form->field($model, 'price')->textInput(['data-cal' => '', 'data-field' => 'price']) ?>
            </div>
            <div class="col-lg-3">
                <?= $form->field($model, 'price_discount')->textInput(['data-cal' => '', 'data-field' => 'discount_amount']) ?>
            </div>
            <div class="col-lg-3">
                <?= $form->field($model, 'percent_discount')->textInput(['data-cal' => '', 'data-field' => 'discount_percent']) ?>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-4">
                <?= $form->field($model, 'image_url')->widget(Upload::className(), ['display' => 'image']) ?>
            </div>
            <div class="col-lg-4">
                <?= $form->field($model, 'video_cover_image_url')->widget(Upload::className(), ['display' => 'image']) ?>
            </div>
            <div class="col-lg-4">
                <?= $form->field($model, 'video_url')->textInput() ?>
            </div>
        </div>

        <?= $form->field($model, 'what_you_learn')->widget(\common\widgets\tinymce\TinyMce::className(), [
            'options' => ['rows' => 6],
            'language' => 'vi',
            'layout' => 'full'
        ]) ?>

        <div class="row">
            <div class="col-lg-6">
                <?= $form->field($model, 'keyword')->textarea() ?>
            </div>
            <div class="col-lg-6">
                <?php
                $model->tags_list = $model->tags;
                echo $form->field($model, 'tags_list')->widget(Select2::classname(), [
                    'maintainOrder' => false,
                    'options' => ['placeholder' => 'Separate tags with commas', 'multiple' => true],
                    'pluginOptions' => [
                        'allowClear' => false,
                        'tags' => true,
                        'tokenSeparators' => [','],
                        'minimumInputLength' => 3,
                        'language' => [
                            'errorLoading' => new JsExpression("function () { return 'Chá» nháº­n káº¿t quáº£...'; }"),
                        ],
                        'ajax' => [
                            'url' => Url::toRoute(['/tag/api/search']),
                            'dataType' => 'json',
                            'data' => new JsExpression('function(params) { return {q:params.term}; }'),
                        ],
                        'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                        'templateResult' => new JsExpression('function(res) { return res.text; }'),
                        'templateSelection' => new JsExpression('function (res) { return res.text; }'),
                    ],
                ]);
                ?>
            </div>

        </div>

        <?php
        foreach ($metaValueModels as $key => $metaValueModel) {
            echo MetaFieldType::renderByModel($form, $metaValueModel, $key, ['id' => "meta-{$key}"], $model);
        }
        ?>


        <div class="row">
            <div class="col-lg-3">
                <?= $form->field($model, 'category_id')->widget(Select2::classname(), [
                    'data' => ArrayHelper::map(Category::findAllActive(), 'id', 'name'),
                    'options' => ['placeholder' => $crudTitles['prompt']],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]) ?>
            </div>
            <div class="col-lg-3">
                <?php
                // Get the initial teacher name
                $teacherDesc = empty($model->teacher) ? '' : $model->teacher->profile->name;

                echo $form->field($model, 'teacher_id')->widget(Select2::classname(), [
                    'initValueText' => $teacherDesc, // set the initial display text
                    'options' => ['placeholder' => $crudTitles['prompt']],
                    'pluginOptions' => [
                        'allowClear' => true,
                        'minimumInputLength' => 3,
                        'language' => [
                            'errorLoading' => new JsExpression("function () { return 'Chá» nháº­n káº¿t quáº£...'; }"),
                        ],
                        'ajax' => [
                            'url' => Url::toRoute(['/user/api/search', 'role' => RoleHelper::ROLE_TEACHER, 'selectNameOnly' => true]),
                            'dataType' => 'json',
                            'data' => new JsExpression('function(params) { return {q:params.term}; }'),
                        ],
                        'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                        'templateResult' => new JsExpression('function(res) { return res.text; }'),
                        'templateSelection' => new JsExpression('function (res) { return res.text; }'),
                    ],
                ]);
                ?>
            </div>
            <div class="col-lg-3">
                <?php
                // Get the initial teacher name
                $teachingAssistantText = empty($teachingAssistant->teachingAssistant) ? '' : $teachingAssistant->teachingAssistant->profile->name;

                echo $form->field($teachingAssistant, 'teaching_assistant_id')->widget(Select2::classname(), [
                    'initValueText' => $teachingAssistantText, // set the initial display text
                    'options' => ['placeholder' => $crudTitles['prompt']],
                    'pluginOptions' => [
                        'allowClear' => true,
                        'minimumInputLength' => 3,
                        'language' => [
                            'errorLoading' => new JsExpression("function () { return 'Chá» nháº­n káº¿t quáº£...'; }"),
                        ],
                        'ajax' => [
                            'url' => Url::toRoute(['/user/api/search', 'role' => RoleHelper::ROLE_TEACHING_ASSISTANT, 'selectNameOnly' => true]),
                            'dataType' => 'json',
                            'data' => new JsExpression('function(params) { return {q:params.term}; }'),
                        ],
                        'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                        'templateResult' => new JsExpression('function(res) { return res.text; }'),
                        'templateSelection' => new JsExpression('function (res) { return res.text; }'),
                    ],
                ]);
                ?>
            </div>
            <div class="col-lg-3">
                <?= $form->field($model, 'status')->widget(Select2::classname(), [
                    'data' => Category::listStatus(),
                    'hideSearch' => true,
                    'options' => ['placeholder' => $crudTitles['prompt']],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]) ?>
            </div>
            <div class="col-lg-3">
                <?= $form->field($model, 'percent_can_pass')->textInput() ?>
            </div>
            <div class="col-lg-3">
                <?= $form->field($model, 'percent_can_be_excellent')->textInput() ?>
            </div>
            <div class="col-lg-2">
                <?= $form->field($model, 'is_hot')->checkbox() ?>
            </div>
            <div class="col-lg-2">
                <?= $form->field($model, 'is_new')->checkbox() ?>
            </div>
            <div class="col-lg-2">
                <?= $form->field($model, 'is_disable_seeding')->checkbox() ?>
            </div>
            <div class="col-lg-2">
                <?= $form->field($model, 'is_4kid')->checkbox() ?>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-3">
                <?= $form->field($model, 'course_commission_type_id')->widget(Select2::classname(), [
                    'data' => ArrayHelper::map($types, 'id', 'name'),
                    'hideSearch' => true,
                    'options' => ['placeholder' => $crudTitles['prompt']],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]) ?>
            </div>

            <div class="col-lg-3">
                <?= $form->field($courseCommissionModel, 'commission_percent')->textInput() ?>
            </div>
            <div class="col-lg-3">
                <?= $form->field($model, 'affiliate_commission_percent')->textInput() ?>
            </div>

            <div class="col-lg-3">
                <?= $form->field($courseCommissionModel, 'apply_to_date', [
                    'options' => [
                        'class' => ((!empty($model->commissionType) && $model->commissionType->is_required_expiration_date == CourseCommissionType::BOOL_YES) ? 'show' : 'hide') . ' required'
                    ]])->widget(DatePicker::className(), [
                    'options' => ['placeholder' => 'Chá»n ngÃ y'],
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format' => 'dd/mm/yyyy',
                    ]
                ]) ?>
            </div>



        </div>

        <div class="row">
            <div class="col-lg-3">
                <?= $form->field($model, 'type')->widget(Select2::classname(), [
                    'data' => Course::listTypes(),
                    'hideSearch' => true,
                    'options' => ['placeholder' => $crudTitles['prompt']],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                    'pluginEvents' => [
                        'change' => 'function() { 
                            if(this.value == ' . Course::TYPE_SOFTWARE . ' ){                            
                                $(\'.software-group\').show();
                                $(\'.partner-group\').show();
                            }else {
                                $(\'.software-group\').hide();
                                $(\'.partner-group\').hide();
                            }                        
                        }',
                    ]

                ]) ?>
            </div>
            <div class="col-lg-3 partner-group">
                <?= $form->field($model, 'partner_id')->widget(Select2::classname(), [
                    'data' => ArrayHelper::map(Partner::findAllActive(), 'id', 'name'),
                    'options' => ['placeholder' => $crudTitles['prompt']],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]) ?>
            </div>
        </div>

        <div class="software-group">
            <div class="row">
                <div class="col-lg-6">
                    <?= $form->field($model, 'window_app_link')->textInput() ?>
                </div>
                <div class="col-lg-6">
                    <?= $form->field($model, 'mac_app_link')->textInput() ?>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6">
                    <?= $form->field($model, 'ios_app_link')->textInput() ?>
                </div>
                <div class="col-lg-6">
                    <?= $form->field($model, 'android_app_link')->textInput() ?>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-6">
                    <?= $form->field($model, 'max_device')->textInput(['type' => 'number']) ?>
                </div>

            </div>
        </div>

        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? $crudTitles['create'] : $crudTitles['update'], ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            <?= Html::a($crudTitles['cancel'], ['index'], ['class' => 'btn btn-default']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>
<?= $model->isNewRecord ? $this->render('//_partials/stringjs') : '' ?>

<?php

$data = json_encode($jsTypeData);
$isHideSoftwareGroup = $model->type == Course::TYPE_SOFTWARE ? 0 : 1;
$script = <<< SCRIPT
;(function ($, _){
     var typeData = $data;
      if( $isHideSoftwareGroup == 1)
     {
         $('.software-group').hide();
         $('.partner-group').hide();
     }
    
    $('body').on('change', '#course-course_commission_type_id', function() {
        var selectedType = typeData[$(this).val()];
        var expirationDate = $('.field-coursecommission-apply_to_date');

        if (typeof selectedType != 'undefined') {
            $('#coursecommission-commission_percent').val(selectedType.default_percent);
            $(this).find('option:selected').attr('data-required-end-date', selectedType.is_required_expiration_date);

            if (selectedType.is_required_expiration_date) {
                expirationDate.removeClass('hide');
            } else {
                expirationDate.addClass('hide');
            }
        } else {
            expirationDate.addClass('hide');
        }
    });
    
    // calculate price
    $('body').on('change', 'input[data-cal]', function () {
        var field = $(this).data('field'),
            value = $(this).val();
        
        var discountPercentElement = $('input[data-field=\'discount_percent\']'),
            discountAmountElement = $('input[data-field=\'discount_amount\']'),
            priceElement = $('input[data-field=\'price\']');
        
        oldPrice = priceElement.val();

        switch (field) {
            case 'discount_percent':
                var discountAmount = oldPrice * value / 100;
                discountAmountElement.val(discountAmount);
        
                break;
        
            case 'discount_amount':
                var newPercent = value / oldPrice * 100;
                discountPercentElement.val(newPercent.toFixed(4));

                break;
        
            case 'price':
                discountAmountElement.val(0);
                discountPercentElement.val(0);
        
                break;
        
            default:
                break;
        }
    });
})(jQuery, _);
SCRIPT;

$this->registerJs($script, View::POS_END, 'change-override-commission');