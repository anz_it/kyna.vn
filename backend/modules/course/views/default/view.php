<?php

use yii\helpers\Html;
use yii\bootstrap\Nav;

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::$app->controller->mainTitle, 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$subView = !$tab ? 'info' : $tab;
?>
<div class="becourse-view">

    <?= Nav::widget([
        'items' => $tabs,
        'options' => ['class' => 'nav-pills pull-left'],
    ]) ?>

    <?= $this->render("view/_$subView", ['model' => $model]) ?>
</div>
