<?php

use yii\bootstrap\Html;
use yii\helpers\Url;
use yii\web\View;

if (!isset($queryParams['filter_price'])
    || (isset($queryParams['filter_price']) && ($queryParams['filter_price'] == '' || $queryParams['filter_price'] == '=0' || $queryParams['filter_price'] == '>100000'))
) {
    $freeDisabled = true;
} else {
    $freeDisabled = false;
}
?>

<?= Html::beginForm(Url::toRoute([$this->context->action->id]), 'get', [
    'class' => 'navbar-form navbar-left',
    'role' => 'search',
]) ?>
<div class="form-group">
    <?= Html::dropDownList('filter_price', isset($queryParams['filter_price']) ? $queryParams['filter_price'] : '', [
        '=0' => 'Miễn phí',
        '<100000' => 'Dưới 100.000',
        '<=100000' => 'Bằng hoặc dưới 100.000',
        '>100000' => 'Trên 100.000'
    ], [
        'class' => 'form-control',
        'prompt' => 'Học phí'
    ]); ?>
    <?= Html::dropDownList('filter_price_not_free', isset($queryParams['filter_price_not_free']) ? $queryParams['filter_price_not_free'] : '', [
        '' => 'Bao gồm khóa học miễn phí',
        '>0' => 'Không bao gồm khóa học miễn phí',
    ], [
        'class' => 'form-control',
        'disabled' => $freeDisabled
        //'prompt' => 'Khóa học miễn phí'
    ]); ?>
</div>

<?= Html::submitButton('<i class="ion-android-search"></i> Tìm khóa học', ['class' => 'btn btn-default']) ?>

<?= Html::endForm(); ?>

<?php
$script = <<< SCRIPT
;(function ($, _){
    $('body').on('change', 'select[name="filter_price"]', function() {
        if ($(this).val() == '' || $(this).val() == '=0' || $(this).val() == '>100000') {
            $('select[name="filter_price_not_free"]').prop('disabled', 'disabled');
        } else {
            $('select[name="filter_price_not_free"]').prop('disabled', false);
        }
    });
})(jQuery, _);
SCRIPT;

$this->registerJs($script, View::POS_END, 'change-filter-price');
?>
