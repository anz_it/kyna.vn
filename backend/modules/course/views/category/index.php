<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use kyna\course\models\Category;
use common\helpers\CDNHelper;

$crudTitles = Yii::$app->params['crudTitles'];
$crudButtonIcons = Yii::$app->params['crudButtonIcons'];

$this->title = 'Danh mục';
$this->params['breadcrumbs'][] = $this->title;

$user = Yii::$app->user;
?>
<div class="row">
    <div class="col-xs-12">
        <div class="becategory-index">
            <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
            <p>
                <?php
                if ($user->can('Course.Category.Create')) {
                    echo Html::a($crudButtonIcons['create'] . ' ' . $crudTitles['create'], ['create'], ['class' => 'btn btn-success']);
                }
                ?>
            </p>
            
            <div class="box">
                <?php Pjax::begin(['timeout' => Yii::$app->params['timeOutPjax'], 'id' => 'idPjaxReload']) ?>
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],
                        [
                            'attribute' => 'id',
                            'contentOptions' => ['style' => 'width: 100px;']
                        ],
                        'name',
                        'slug',
                        [
                            'attribute' => 'menu_icon',
                            'format' => 'html',
                            'value' => function ($model) {
                                return Html::img(CDNHelper::getMediaLink() . $model->menu_icon, ['style' => 'max-height: 50px;']);
                            },
                            'filter' => false,
                            'options' => ['class' => 'col-xs-1']
                        ],
                        [
                            'attribute' => 'home_icon',
                            'format' => 'html',
                            'value' => function ($model) {
                                return Html::img(CDNHelper::getMediaLink() . $model->home_icon, ['style' => 'max-height: 50px;']);
                            },
                            'filter' => false,
                            'options' => ['class' => 'col-xs-1']
                        ],
                        'description:ntext',
                        'redirect_url:url',
                        'order',
                        [
                            'attribute' => 'status',
                            'format' => 'raw',
                            'value' => function ($model)
                            {
                                return $model->statusButton;
                            },
                            'filter' => Html::activeDropDownList($searchModel, 'status', Category::listStatus(false), ['class' => 'form-control', 'prompt' => $crudTitles['prompt']]), 
                        ],
                        [
                            'class' => 'app\components\ActionColumnCustom',
                            'visibleButtons' => [
                                'update' => $user->can('Course.Category.Update'),
                                'view' => $user->can('Course.Category.View'),
                                'delete' => $user->can('Course.Category.Delete'),
                            ],
                        ],
                    ],
                ]); ?>
                <?php Pjax::end() ?>
            </div>
        </div>
    </div>
</div>