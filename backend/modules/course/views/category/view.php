<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use kyna\course\models\Category;

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Danh mục', 'url' => ['index']];

if (!empty($model->parent)) {
    $this->params['breadcrumbs'][] = ['label' => $model->parent->name, 'url' => ['view', 'id' => $model->parent->id]];
}

$this->params['breadcrumbs'][] = $this->title;

$crudTitles = Yii::$app->params['crudTitles'];
$user = Yii::$app->user;
?>
<div class="becategory-view">
    <p>
        <?php
        echo Html::a("<span class='glyphicon glyphicon-backward'></span> " . $crudTitles['back'], !empty($model->parent_id) ? ['view', 'id' => $model->parent_id] : ['/course/category'], [
            'class' => 'btn btn-info',
            'icon' => 'glyphicon glyphicon-backward'
        ]) . ' ';
        if ($user->can('Course.Category.Create')) {
            echo Html::a(Yii::$app->params['crudTitles']['create'], ['create', 'parent_id' => $model->id], ['class' => 'btn btn-success']);
        }
        ?>
    </p>
    <?php Pjax::begin(['id' => 'idPjaxReload']) ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'id',
            'name',
            'slug',
            'order',
            'redirect_url:url',
            [
                'attribute' => 'status',
                'format' => 'raw',
                'value' => function ($model)
                {
                    return $model->statusButton;
                },
                'filter' => Html::activeDropDownList($searchModel, 'status', Category::listStatus(false), ['class' => 'form-control', 'prompt' => $crudTitles['prompt']]),
            ],
            [
                'class' => 'app\components\ActionColumnCustom',
                'visibleButtons' => [
                    'update' => $user->can('Course.Category.Update'),
                    'view' => $user->can('Course.Category.View'),
                    'delete' => $user->can('Course.Category.Delete'),
                ],
            ],
        ],
    ]); ?>
    <?php Pjax::end() ?>
</div>
