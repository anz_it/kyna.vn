<?php
/**
 * Created by PhpStorm.
 * User: nguyenphanngu
 * Date: 9/16/17
 * Time: 1:52 PM
 */

use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

?>

<div>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
//            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'id',
                'contentOptions' => ['style' => 'width: 100px;']
            ],
            'name',
            'slug',
            [
                'header' => 'Actions',
                'class' => '\yii\grid\ActionColumn',
                'contentOptions' => ['style' => 'width: 90px;'],
                'template' => '{remove}',
//                'visibleButtons' => [
//                    'view' => false,
//                    'update' => false,
//                    'delete' => false,
//                    'remove' => true,
//                ],
                'buttons' => [
                    'remove' => function ($url, $model, $key) {
                        $url = '#';
                        return Html::a('Remove', $url, [
                            'class' => 'btn btn-sm btn-default btn-block btn-remove-category',
                            'category_id' => $model->id,
                        ]);
                    },
                ]
            ],
        ],
    ]); ?>
</div>