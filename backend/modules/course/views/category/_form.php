<?php

/**
 * @var $this \yii\web\View
 */
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\web\JsExpression;

use yii\helpers\Url;

use kyna\course\models\Category;

use common\widgets\upload\Upload;
use common\widgets\tinymce\TinyMce;
use common\widgets\autocomplete\AutoComplete;

$crudTitles = Yii::$app->params['crudTitles'];
?>

<div class="becategory-form">

    <?php $form = ActiveForm::begin([
        'options' => ['enctype' => 'multipart/form-data'],
    ]); ?>

    <?= $form->field($model, 'parent_id')->dropDownList(ArrayHelper::map($categories, 'id', 'name'), ['prompt' => $crudTitles['prompt']]) ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true, 'onchange' => "$('#cat-slug').val(convertToSlug($(this).val()))"]) ?>

    <?= $form->field($model, 'slug')->textInput(['maxlength' => true, 'id' => 'cat-slug']) ?>

    <?= $form->field($model, 'is_featured')->checkBox() ?>

    <div class="row">
        <?= $form->field($model, 'menu_icon',['options' => ['class' => 'col-xs-6']])->widget(Upload::className(), [
            'display' => 'image',
        ]); ?>

        <?= $form->field($model, 'home_icon',['options' => ['class' => 'col-xs-6']])->widget(Upload::className(), [
            'display' => 'image',
        ]); ?>

        <?= $form->field($model, 'images_thumb',['options' => ['class' => 'col-xs-12']])->widget(Upload::className(), [
            'display' => 'image',
        ]); ?>
    </div>


    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'order')->textInput() ?>

    <?= $form->field($model, 'status')->dropDownList(Category::listStatus(), ['prompt' => $crudTitles['prompt']]) ?>

    <?= $form->field($model, 'title')->textarea() ?>

    <?= $form->field($model, 'seo_description')->widget(TinyMce::className(), [
        'layout' => 'advanced',
    ])
    ?>
    <?= $form->field($model, 'redirect_url')->hint('Link Redirect khi khóa học bị ẩn, để trống nếu muốn sử dụng redirect link mặc định'); ?>

    <div class="panel panel-default">
        <div class="panel-heading"><b>Danh sách danh mục khóa học liên quan</b></div>
        <div class="form-group panel-body">
            <label for="course-search">Tên danh mục khóa học muốn thêm</label>
            <?= AutoComplete::widget([
                'id' => 'category-typeahead',
                'name' => 'category',
                'options' => [
                    'class' => 'form-control',
                    'placeholder' => 'Nhập tên danh mục khóa học',
                ],
                'clientOptions' => [
                    'minLength' => 2,
                    'highlight' => 'true',
                ],
                'dataset' => [
                    'displayKey' => 'name',
                    'limit' => 100
                ],
                'bloodhound' => [
                    'datumTokenizer' => new JsExpression('Bloodhound.tokenizers.obj.whitespace("name")'),
                    'queryTokenizer' => new JsExpression('Bloodhound.tokenizers.whitespace'),
                    'remote' => [
                        'url' => Url::to('/course/api/search-category') . '?q=%QUERY',
                        'wildcard' => '%QUERY',
                    ],
                ],
            ]) ?>
            <?= $form->field($model, 'related_categories_id')->hiddenInput(['id' => 'related-categories-id'])->label(false); ?>
        </div>
        <div class="form-group panel-body" id="categories-content"></div>
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? $crudTitles['create'] : $crudTitles['update'], ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::a($crudTitles['cancel'], !empty($model->parent) ? ['view', 'id' => $model->parent_id] : ['index'],['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<?= $this->render('//_partials/stringjs') ?>

<?php
    $script_related_courses = "
        var categories_id = $('#related-categories-id');
//        console.log(categories_id.val());

        $(document).ready(function() {
            updateRelatedCategoryList();
        });
        
        $('body').on('typeahead:select', '#category-typeahead', function(e, datum) {
            addCategory(datum.id); 
            $(this).typeahead('val', '');
        });
        
        $('body').on('click', '.btn-remove-category', function (e) {
            e.preventDefault();
            
            var id = $(this).attr('category_id');
            
            removeCategory(id);
        });
        
                
        function addCategory(id) {
            // add to categories_id array
            var itemArray = categories_id.val().split(',');
            
            itemArray.push(id);
            itemArray = _.uniq(itemArray);  // remove duplicate
            
            categories_id.val(itemArray.join(','));
            
            // re-render
            updateRelatedCategoryList();
        }
        
        function removeCategory(id) {
            // remove from categories_id array
            var itemArray = categories_id.val().split(',');
            
            var index = itemArray.indexOf(id);
            if (index >= 0) {
                itemArray.splice(index, 1);
                categories_id.val(itemArray.join(','));
                
                // re-render
                updateRelatedCategoryList();                
            } 
           
        }

        function updateRelatedCategoryList() {
            url = '/course/category/render';            
            $.post(
                url, 
                { 
                    \"categories_id\": categories_id.val()
                }, 
                function (response) {
                    $('#categories-content').html(response)
                }
            );
        };
    ";

    $css_related_courses = "
        #categories-content .grid-view .summary {
            display: none;
        }
    ";

    $this->registerJs($script_related_courses, \yii\web\View::POS_END);
    $this->registerCss($css_related_courses, ['position' => \yii\web\View::POS_END]);
?>