<?php

use yii\helpers\Html;

$this->title = Yii::$app->params['crudTitles']['create'];
$this->params['breadcrumbs'][] = ['label' => Yii::$app->controller->mainTitle, 'url' => ['/course/default/index']];
$this->params['breadcrumbs'][] = ['label' => 'Ý kiến học viên', 'url' => ['/course/view/opinion', 'id' => $model->course_id]];
$this->params['breadcrumbs'][] = 'Thêm ý kiến học viên'
?>
<div class="becategory-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
