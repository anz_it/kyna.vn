<?php

use yii\helpers\Html;

$this->title = Yii::$app->params['crudTitles']['update'] ;
$this->params['breadcrumbs'][] = ['label' => Yii::$app->controller->mainTitle, 'url' => ['/course/default/index']];
$this->params['breadcrumbs'][] = ['label' => 'Ý kiến học viên', 'url' => ['/course/view/opinion', 'id' => $model->course_id]];
$this->params['breadcrumbs'][] = 'Cập nhật'
?>
<div class="becategory-update">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
