<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kyna\course\models\Category;
use common\widgets\upload\Upload;
use common\widgets\tinymce\TinyMce;

$crudTitles = Yii::$app->params['crudTitles'];
?>

<div class="becategory-form">

    <?php $form = ActiveForm::begin([
        'options' => ['enctype' => 'multipart/form-data'],
    ]); ?>

    <?= $form->field($model, 'user_name')->textInput() ?>
    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'status')->dropDownList(Category::listStatus(), ['prompt' => $crudTitles['prompt']]) ?>

    <?= $form->field($model, 'avatar_url')->widget(Upload::className(), [
        'display' => 'image',
    ]); ?>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? $crudTitles['create'] : $crudTitles['update'], ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::a($crudTitles['cancel'], ['/course/view/opinion', 'id' => $model->course_id],['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<?= $this->render('//_partials/stringjs') ?>
