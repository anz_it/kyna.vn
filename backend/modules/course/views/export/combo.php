<?php

use yii\grid\GridView;
use yii\helpers\Url;
use yii\widgets\Pjax;

use kartik\export\ExportMenu;
use yii\helpers\ArrayHelper;

$crudTitles = Yii::$app->params['crudTitles'];
$crudButtonIcons = Yii::$app->params['crudButtonIcons'];

$this->title = 'Export Combo khóa học';
$this->params['breadcrumbs'][] = $this->title;
$formatter = Yii::$app->formatter;

$gridColumns = [
    'id',
    'created_time:datetime',
    'name',
    [
        'label' => 'Danh sách giảng viên',
        'format' => 'html',
        'value' => function($model) {
            return $model->teacherText;
        }
    ],
    [
        'label' => 'Giá bán',
        'value' => function($model) {
            return $model->sellPrice;
        }
    ],
    [
        'label' => 'Giá gốc',
        'value' => function($model) {
            return $model->oldPrice;
        }
    ],
    [
        'label' => 'Tỷ lệ chia sẻ',
        'value' => function($model) use ($formatter){
            if ($model->oldPrice > 0) {
                $percentage = ($model->oldPrice - $model->sellPrice) / $model->oldPrice;
                return $formatter->asPercent($percentage, 2);
            }
        }
    ],
    [
        'label' => 'Số học viên đăng ký',
        'value' => function($model) {
            return $model->getOrderDetails()->count();
        }
    ],
    [
        'label' => 'Số học viên thanh toán',
        'value' => function($model) {
            return $model->getCountComboPaid()->count();
        }
    ]
];
?>
<div class="row">
    <div class="col-xs-12">
        <div class="course-combo-index">
            <?= \yii\bootstrap\Nav::widget([
                'items' => $tabs,
                'options' => ['class' => 'nav-pills'],
            ]) ?>
            <nav class="navbar navbar-default">
                <?php
                echo ExportMenu::widget([
                    'dataProvider' => $dataProvider,
                    'columns' => ArrayHelper::merge($gridColumns,[
                        'object:html',
                        'benefit:html',
                        'image_url'
                    ]),
                    'fontAwesome' => true,
                    'container' => [
                        'class' => 'btn-group navbar-btn navbar-left'
                    ],
                    'columnSelectorOptions'=>[
                        'label' => 'Export Cols',
                    ],
                    'dropdownOptions' => [
                        'label' => 'Export All',
                        'class' => 'btn btn-default'
                    ],
                    'showConfirmAlert' => false,
                    'showColumnSelector' => false,
                    'target' => ExportMenu::TARGET_BLANK,
                    'filename' => 'combo_export_'.date('Y-m-d_H-i', time()),
                    'exportConfig' => [
                        ExportMenu::FORMAT_PDF => false,
                    ]
                ]);
                ?>
            </nav>

            <div class="box">
                <?php 
                Pjax::begin();
                $teacherDesc = empty($searchModel->teacher_id) ? '' : $searchModel->teacher->profile->name;
                ?>
                <?=
                GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'options' => [
                        'data' => [
                            'sortable-widget' => 1,
                            'sortable-url' => Url::toRoute(['/course/combo/sorting']),
                        ]
                    ],
                    'columns' => $gridColumns
                ]);
                ?>
                <?php Pjax::end(); ?>
            </div>
        </div>
    </div>
</div>