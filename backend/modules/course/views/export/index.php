<?php

use yii\helpers\Url;
use yii\widgets\Pjax;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use yii\web\JsExpression;
use kartik\export\ExportMenu;
use kartik\grid\GridView;

use common\helpers\RoleHelper;

$crudTitles = Yii::$app->params['crudTitles'];
$crudButtonIcons = Yii::$app->params['crudButtonIcons'];

$this->title = Yii::$app->controller->mainTitle;
$this->params['breadcrumbs'][] = $this->title;

$user = Yii::$app->user;
$formatter = Yii::$app->formatter;
$teacherDesc = empty($searchModel->teacher_id) ? '' : $searchModel->teacher->profile->name;
$gridColumns = [
    'id',
    'created_time:datetime',
    'name',
    'price',
    [
        'attribute' => 'teacher_id',
        'value' => function ($model) {
            return !empty($model->teacher) ? $model->teacher->profile->name : '';
        },
        'filter' => Select2::widget([
            'model' => $searchModel,
            'initValueText' => $teacherDesc,
            'attribute' => 'teacher_id',
            'options' => ['placeholder' => $crudTitles['prompt']],
            'pluginOptions' => [
                'allowClear' => true,
                'minimumInputLength' => 3,
                'language' => [
                    'errorLoading' => new JsExpression("function () { return 'Chờ nhận kết quả...'; }"),
                ],
                'ajax' => [
                    'url' => Url::toRoute(['/user/api/search', 'role' => RoleHelper::ROLE_TEACHER, 'selectNameOnly' => true]),
                    'dataType' => 'json',
                    'data' => new JsExpression('function(params) { return {q:params.term}; }'),
                ],
                'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                'templateResult' => new JsExpression('function(teacher) { return teacher.text; }'),
                'templateSelection' => new JsExpression('function (teacher) { return teacher.text; }'),
            ],
        ])
    ],
    [
        'label' => 'Tỷ lệ chia sẻ',
        'value' => function ($model) {
            return $model->commissionPercent;
        },
    ],
    [
        'label' => 'Danh mục cấp 1',
        'attribute' => 'category_id',
        'value' => function ($model) {
            return !empty($model->category) ? $model->category->name : '';
        },
    ],
    [
        'label' => 'Danh mục cấp 2',
        'attribute' => 'category_id',
        'value' => function ($model) {
            $category = $model->category;
            if ($category && $category->parent) {
                return $category->parent->name;
            }
            return '';
        },
    ],
    [
        'label' => 'Số học viên đăng ký',
        'value' => function ($model) {
            return $model->countUserCourse;
        },
    ],
    [
        'label' => 'Số học viên thanh toán',
        'value' => function ($model) {
            return $model->countUserCourse;
        },
    ],
    [
        'label' => 'Tỷ lệ hoàn thành trung bình (%)',
        'value' => function ($model) use ($formatter) {
            return $formatter->asPercent($model->graduatedPercentage, 2);
        },
    ],
];
?>
<div class="row">
    <div class="col-xs-12">
        <div class="becourse-index">
            <?= \yii\bootstrap\Nav::widget([
                'items' => $tabs,
                'options' => ['class' => 'nav-pills'],
            ]) ?>
            <nav class="navbar navbar-default">
                <?php
                echo ExportMenu::widget([
                    'dataProvider' => $dataProvider,
                    'columns' => ArrayHelper::merge($gridColumns,['image_url']),
                    'fontAwesome' => true,
                    'container' => [
                        'class' => 'btn-group navbar-btn navbar-left'
                    ],
                    'columnSelectorOptions'=>[
                        'label' => 'Export Cols',
                    ],
                    'dropdownOptions' => [
                        'label' => 'Export All',
                        'class' => 'btn btn-default'
                    ],
                    'showConfirmAlert' => false,
                    'showColumnSelector' => false,
                    'target' => ExportMenu::TARGET_BLANK,
                    'filename' => 'course_export_'.date('Y-m-d_H-i', time()),
                    'exportConfig' => [
                        ExportMenu::FORMAT_PDF => false,
                    ]
                ]);
                ?>
            </nav>

            <div class="box">
                <?php Pjax::begin() ?>
                <?=
                GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => $gridColumns
                ]);
                ?>
                <?php Pjax::end() ?>
            </div>
        </div>
    </div>
</div>