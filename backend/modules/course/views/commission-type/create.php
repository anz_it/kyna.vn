<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model kyna\course\models\CourseCommissionType */

$this->title = Yii::$app->params['crudTitles']['create'];
$this->params['breadcrumbs'][] = ['label' => Yii::$app->controller->mainTitle, 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="course-commission-type-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
