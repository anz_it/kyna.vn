<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\Select2;
use kyna\course\models\CourseCommissionType;

$crudTitles = Yii::$app->params['crudTitles'];
?>

<div class="course-commission-type-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'start_percent')->textInput() ?>

    <?= $form->field($model, 'end_percent')->textInput() ?>
    
    <?= $form->field($model, 'default_percent')->textInput() ?>

    <?= $form->field($model, 'is_required_expiration_date')->checkbox() ?>

    <?= $form->field($model, 'is_multiple_commissions')->checkbox() ?>

    <?= $form->field($model, 'status')->widget(Select2::classname(), [
                'data' => CourseCommissionType::listStatus(),
                'hideSearch' => true,
                'options' => ['placeholder' => $crudTitles['prompt']],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? $crudTitles['create'] : $crudTitles['update'], ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::a($crudTitles['cancel'], ['index'], ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
