<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use kartik\widgets\Select2;
use kyna\course\models\search\CourseCommissionSearch;

$crudTitles = Yii::$app->params['crudTitles'];

$this->title = Yii::$app->controller->mainTitle;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-xs-12">
        <div class="course-commission-type-index">
            <p>
                <?= Html::a($crudTitles['create'], ['create'], ['class' => 'btn btn-success']) ?>
            </p>

            <div class="box">
                <?php Pjax::begin(['timeout' => Yii::$app->params['timeOutPjax'], 'id' => 'idPjaxReload']) ?>
                <?=
                GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],
                        [
                            'attribute' => 'id',
                            'options' => ['class' => 'col-xs-1']
                        ],
                        'name',
                        'start_percent',
                        'end_percent',
                        'default_percent',
                        [
                            'attribute' => 'is_required_expiration_date',
                            'format' => 'boolean',
                            'filter' => Select2::widget([
                                'attribute' => 'is_required_expiration_date',
                                'model' => $searchModel,
                                'data' => CourseCommissionSearch::getBooleanOptions(),
                                'options' => ['placeholder' => $crudTitles['prompt']],
                                'pluginOptions' => [
                                    'allowClear' => true
                                ],
                                'hideSearch' => true,
                                
                            ])
                        ],
                        [
                            'attribute' => 'is_multiple_commissions',
                            'format' => 'boolean',
                            'filter' => Select2::widget([
                                'attribute' => 'is_multiple_commissions',
                                'model' => $searchModel,
                                'data' => CourseCommissionSearch::getBooleanOptions(),
                                'options' => ['placeholder' => $crudTitles['prompt']],
                                'pluginOptions' => [
                                    'allowClear' => true
                                ],
                                'hideSearch' => true,
                                
                            ])
                        ],
                        [
                            'attribute' => 'status',
                            'value' => function ($model)
                            {
                                return $model->statusButton;
                            },
                            'format' => 'raw',
                            'filter' => Select2::widget([
                                'model' => $searchModel,
                                'attribute' => 'status',
                                'data' => CourseCommissionSearch::listStatus(false),
                                'options' => ['placeholder' => $crudTitles['prompt']],
                                'pluginOptions' => [
                                    'allowClear' => true
                                ],
                                'hideSearch' => true,
                            ])
                        ],
                        ['class' => 'app\components\ActionColumnCustom'],
                    ],
                ]);
                ?>
                <?php Pjax::end() ?>
            </div>
        </div>
    </div>
</div>