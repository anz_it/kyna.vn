<?php

use yii\helpers\Html;
use yii\helpers\StringHelper;
use yii\bootstrap\Tabs;
use app\modules\course\controllers\DefaultController;
use app\modules\course\controllers\QuizQuestionController;
use kyna\course\models\QuizQuestion;

$this->title = Yii::$app->params['crudTitles']['update'];

$this->params['breadcrumbs'][] = ['label' => 'Khóa học', 'url' => ['/course/default/index']];
$this->params['breadcrumbs'][] = ['label' => StringHelper::truncateWords($model->course->name, 6), 'url' => ['/course/view/index', 'id' => $model->course->id]];
$this->params['breadcrumbs'][] = ['label' => $this->context->mainTitle, 'url' => ['/course/view/question', 'id' => $model->course->id]];
$this->params['breadcrumbs'][] = $this->title;

$tabItems = [
    [
        'label' => 'Thông tin câu hỏi',
        'content' => $this->render('_form', [ 'model' => $model]),
        'active' => $tab == QuizQuestionController::TAB_INFO ? true : false
    ]
];
if ($model->type == QuizQuestion::TYPE_ONE_CHOICE) {
    $tabItems[] = [
        'label' => 'Đáp án',
        'content' => $this->render('_answer', ['model' => $model, 'answerModel' => $answerModel]),
        'active' => $tab == QuizQuestionController::TAB_ANSWER ? true : false
    ];
}
if($model->type == QuizQuestion::TYPE_FILL_INTO_DOTS){
    $tabItems[] = [
        'label' => 'Đáp án',
        'content' => $this->render('_answer', ['model' => $model, 'answerModel' => $answerModel]),
        'active' => $tab == QuizQuestionController::TAB_ANSWER ? true : false
    ];
}
?>
<div class="course-question-update">
    <?= $this->render('_buttons', ['model' => $model]) ?>
    
    <?=
    Tabs::widget([
        'items' => $tabItems
    ]);
    ?>
</div>
