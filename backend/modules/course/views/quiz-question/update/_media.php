<?php
/**
 * Created by PhpStorm.
 * User: khanhphan
 * Date: 11/13/16
 * Time: 7:13 PM
 */

?>

<div class="form-group">
    <label>Hình ảnh </label>
    <div></div>
    <div style="margin-bottom: 20px" ng-if="!!data.imageUrl">
        <img ng-src="{{params.mediaLink + data.imageUrl}}" width="200px">
        <a class="btn btn-danger" ng-click="removeImage()">Xóa</a>
    </div>

    <button class="btn btn-info" ngf-select="uploadImage($files)" accept="image/*">Chọn file hình ảnh...</button>

    <div class="alert alert-error" ng-if="!!imageError">{{imageError}}</div>

    <span class="progress" ng-show="imageProcess >= 0">
        <div style="width:{{imageProcess}}%" ng-bind="imageProcess + '%'"></div>
    </span>


</div>

<div class="form-group">
    <label>Audio </label>
    <div></div>
    <div ng-if="!!data.audioUrl">
        <audio ng-src="{{params.mediaLink + data.audioUrl}}" controls src="{{params.mediaLink + data.audioUrl}}"></audio>
        <a class="btn btn-danger" ng-click="removeAudio()" style="margin-top:-25px">Xóa</a>
    </div>
    <button class="btn btn-info" ngf-select="uploadAudio($files)" accept="audio/*">Chọn file audio...
    </button>



    <div class="alert alert-error" ng-if="!!audioError">{{audioError}}</div>


    <span class="progress" ng-show="audioProcess >= 0">
        <div style="width:{{audioProcess}}%" ng-bind="audioProcess + '%'"></div>
    </span>

</div>


<div class="form-group">
    <label>Video embed </label>
    <textarea rows="6" ng-model="data.videoEmbed" class="form-control"></textarea>
</div>
<div class="form-group" ng-if="!!data.videoEmbed">
    <label>Embed preview </label> (<a style="cursor: default;" ng-click="toggleEmbed()">Hide/Show</a>)
    <div ng-bind-html="renderHtml(data.videoEmbed)" ng-show="showEmbed">

    </div>
</div>