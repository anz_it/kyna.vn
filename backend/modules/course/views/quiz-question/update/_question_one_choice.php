<?php
/**
 * Created by PhpStorm.
 * User: khanhphan
 * Date: 10/6/16
 * Time: 11:26 AM
 * @var $this \yii\web\View
 * @var $form ActiveForm
 *
 */

use yii\bootstrap\ActiveForm;
use kyna\course\models\QuizQuestion;

$this->registerCss("
.question-answer-detail .row {
    margin: 5px 0;
}
.question-answer-detail .answer-content:hover {
    background: lightblue;
}
.insert-answer {
    margin-top: 10px;
}
.question-answer-detail .deleted, .question-answer-detail  .deleted label {
    text-decoration: line-through;
    font-style: italic;
}
.undo-btn {
     -moz-transform: scaleX(-1);
     -webkit-transform: scaleX(-1);
     -o-transform: scaleX(-1);
     transform: scaleX(-1);
     color: gray;
}
.thumb {
    width: 24px;
    height: 24px;
    float: none;
    position: relative;
    top: 7px;
}
form .progress {
    line-height: 15px;
}
}
.progress {
    display: inline-block;
    width: 100px;
    border: 3px groove #CCC;
}
.progress div {
    font-size: smaller;
    background: orange;
    width: 0;
}
.answer-show-content, .answer-is-correct {
    display: inline-block;
    vertical-align: top;
}
.answer-show-content {
    width: 94%;
}
.answer-is-correct {
    width: 5%;
}
");

$form = ActiveForm::begin([
    'id' => 'question-update-form',
    'enableAjaxValidation' => false,
    'enableClientValidation' => false,
    'options' => [
        'onSubmit' => 'return false',
    ]
]);

echo $form->field($model, 'id')->hiddenInput(['ng-model' => 'data.id'])->label(false);
?>
<div class="row">
    <div class="col-md-6">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Câu hỏi</h3>
            </div>

            <div class="box-body">
                <div class="form-group">
                    <label>Nội dung câu hỏi</label>
                    <textarea ui-tinymce="tinymceNoImageOptions" ng-model="data.questionContent"
                              class="form-control"></textarea>
                </div>
                <?= $this->render('_media') ?>
                <div class="form-group">
                    <label>Giải thích đáp án</label>
                    <textarea ui-tinymce="tinymceOptions" ng-model="data.answerExplain" class="form-control">
                    </textarea>
                </div>

            </div>
        </div>

    </div>
    <div class="col-md-6">
        <div class="row">
            <div class="col-md-12 box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title">Các câu trả lời</h3>
                </div>
                <div class="box-body question-answer-detail" ng-class="data.error ? 'has-error' : ''">
                    <div class="row answer-content" ng-repeat="answer in data.answers">
                        <div class="col-md-{{!!answer.imageUrl ? 8 : 11}} answer-correct"
                             ng-class="answer.isDeleted ? 'deleted' : ''">
                            <div class="answer-is-correct">
                                <input
                                    type="checkbox" <?= $model->type == QuizQuestion::TYPE_ONE_CHOICE ? 'ng-change="onCorrectAnswerChange(answer)"' : '' ?>
                                    ng-model="answer.isCorrect" ng-disabled="answer.isDeleted"/>
                            </div>
                            <div class="answer-show-content" ng-bind-html="renderHtml(answer.content)"></div>
                        </div>
                        <div class="col-md-3" ng-if="!!answer.imageUrl">
                            <div style="margin-bottom: 20px">
                                <img ng-src="{{params.mediaLink + answer.imageUrl}}" width="100%">
                                <!--                            <a class="btn btn-danger" ng-click="removeAnswerImage(answer)">Xóa</a>-->
                            </div>
                        </div>
                        <div class="col-md-1">
                            <a ng-click="removeAnswer(answer)" ng-if="!answer.isDeleted">
                                <i class="glyphicon glyphicon-trash" title="mark as delete"></i>
                            </a>
                        </div>
                        <div class="col-md-1">
                            <a ng-click="unremovedAnswer(answer)" ng-if="!!answer.isDeleted">
                                <i class="glyphicon glyphicon-share-alt undo-btn" title="undo"></i>
                            </a>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="box box-success col-md-12">
                <div class="box-header with-border">
                    <h3 class="box-title">Thêm câu trả lời</h3>
                </div>
                <div class="form-group insert-answer">
                    <textarea ui-tinymce="tinymceNoImageOptions" ng-model="data.insertText"
                              class="form-control" placeholder="Thêm câu trả lời"></textarea>
                    <span class="help-block" ng-if="data.error">{{data.error}}</span>
                </div>

                <div class="form-group">
                    <label>Hình ảnh </label>
                    <div></div>
                    <div style="margin-bottom: 20px" ng-if="!!answerImageUrl">
                        <img ng-src="{{params.mediaLink + answerImageUrl}}" width="200px">
                        <a class="btn btn-danger" ng-click="removeTempAnswerImage()">Xóa</a>
                    </div>

                    <button class="btn btn-info" ngf-select="uploadAnswerImage($files)" accept="image/*">Chọn file hình
                        ảnh...
                    </button>

                    <div class="alert alert-error" ng-if="!!imageError">{{imageError}}</div>

                    <span class="progress" ng-show="answerImageProcess >= 0">
                         <div style="width:{{imageProcess}}%" ng-bind="answerImageProcess + '%'"></div>
                    </span>
                </div>

                <div class="form-group">
                    <a class="btn btn-success" ng-click="addMoreAnswer(data)">
                        <i class="glyphicon glyphicon-plus"></i>Thêm
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="well">
        <button type="button" ng-click="save()" class="btn btn-primary" ng-disabled="loading"><i
                class="fa fa-refresh fa-spin" ng-show="loading"></i> Cập nhật
        </button>
    </div>
</div>

<?php ActiveForm::end() ?>

