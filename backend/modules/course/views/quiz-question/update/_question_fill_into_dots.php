<?php
/**
 * Created by PhpStorm.
 * User: khanhphan
 * Date: 10/6/16
 * Time: 11:26 AM
 * @var $this \yii\web\View
 * @var $form ActiveForm
 *
 */

use yii\bootstrap\ActiveForm;

$this->registerCss("
.question-answer-detail .row {
    margin: 5px 0;
}
.question-answer-detail .answer-content:hover {
    background: lightblue;
}
.insert-answer {
    margin-top: 10px;
}
.question-answer-detail .deleted {
    text-decoration: line-through;
    font-style: italic;
}
.undo-btn {
     -moz-transform: scaleX(-1);
     -webkit-transform: scaleX(-1);
     -o-transform: scaleX(-1);
     transform: scaleX(-1);
     color: gray;
}
.thumb {
    width: 24px;
    height: 24px;
    float: none;
    position: relative;
    top: 7px;
}
form .progress {
    line-height: 15px;
}
}
.progress {
    display: inline-block;
    width: 100px;
    border: 3px groove #CCC;
}
.progress div {
    font-size: smaller;
    background: orange;
    width: 0;
}
");

$form = ActiveForm::begin([
    'id' => 'question-update-form',
    'enableAjaxValidation' => false,
    'enableClientValidation' => false,
    'options' => [
        'onSubmit' => 'return false',
    ]
]);

echo $form->field($model, 'id')->hiddenInput(['ng-model' => 'data.id'])->label(false);
?>
    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Câu hỏi</h3>
                </div>

                <div class="box-body">
                    <?php /*$form->field($model, 'content')->widget(TinyMce::className(), [
                        'options' => ['rows' => 6, 'ng-model' => "data.questionContent"],
                        'language' => 'vi',
                        'clientOptions' => [
                            'plugins' => [
                                "advlist autolink lists link charmap print preview anchor",
                                "searchreplace visualblocks code fullscreen",
                                "insertdatetime media table contextmenu paste"
                            ],
                            'toolbar' => "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
                        ]
                    ])*/ ?>
                    <div class="form-group">
                        <label>Nội dung câu hỏi</label>
                        <textarea ui-tinymce="tinymceNoImageOptions" ng-model="data.questionContent" class="form-control"></textarea>
                    </div>

                    <?=$this->render('_media') ?>

                    <div class="form-group">
                        <label>Giải thích đáp án</label>
                        <textarea ui-tinymce="tinymceOptions" ng-model="data.answerExplain" class="form-control">
                        </textarea>
                    </div>
                    <div class="form-group">

                        <button class="btn btn-success" type="button" ng-click="genAnswers()"> Điền câu trả lời </button>
                        <button class="btn btn-info" type="button" ng-click="insertEmpty()">Chèn thêm chỗ trống</button>
                    </div>

            </div>
        </div>

    </div>
    <div class="col-md-6">
        <div class="box box-success" ng-repeat="question in data.questions">
            <div class="box-header with-border">
                <h3 class="box-title">{{question.content}}</h3>

            </div>
            <div class="box-body question-answer-detail" ng-class="question.error ? 'has-error' : ''">
                <div class="row answer-content"  ng-repeat="answer in question.answers">
                    <div class="col-md-8"  ng-class="answer.isDeleted ? 'deleted' : ''">
                        {{answer.content}}
                    </div>
                    <div class="col-md-4">
                        <a ng-click="removeAnswer( answer)" ng-if="!answer.isDeleted">
                            <i class="glyphicon glyphicon-trash" title="mark as delete"></i>
                        </a>
                    </div>
                    <div class="col-md-4">
                        <a ng-click="unremovedAnswer(answer)" ng-if="!!answer.isDeleted">
                            <i class="glyphicon glyphicon-share-alt undo-btn" title="undo"></i>
                        </a>
                    </div>

                </div>
                <div class="row insert-answer">
                    <div class="col-md-8">
                        <input type="text" class="form-control" ng-model="question.insertText" ng-enter="addMoreAnswer(question)" placeholder="Thêm câu trả lời"/>
                        <span class="help-block" ng-if="question.error">{{question.error}}</span>
                    </div>
                    <div class="col-md-4">
                        <a class="btn btn-success" ng-click="addMoreAnswer(question)"><i class="glyphicon glyphicon-plus"></i></a>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="well">
        <button type="button" ng-click="save()" class="btn btn-primary" ng-disabled="loading"> <i class="fa fa-refresh fa-spin" ng-show="loading"></i> Cập nhật</button>
    </div>
</div>
<?php  ActiveForm::end() ?>

