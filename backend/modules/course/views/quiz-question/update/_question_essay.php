<?php
/**
 * Created by PhpStorm.
 * User: khanhphan
 * Date: 10/6/16
 * Time: 11:26 AM
 * @var $this \yii\web\View
 * @var $form ActiveForm
 *
 */

use yii\bootstrap\ActiveForm;

$this->registerCss("
.question-answer-detail .row {
    margin: 5px 0;
}
.question-answer-detail .answer-content:hover {
    background: lightblue;
}
.insert-answer {
    margin-top: 10px;
}
.question-answer-detail .deleted {
    text-decoration: line-through;
    font-style: italic;
}
.undo-btn {
     -moz-transform: scaleX(-1);
     -webkit-transform: scaleX(-1);
     -o-transform: scaleX(-1);
     transform: scaleX(-1);
     color: gray;
}
.thumb {
    width: 24px;
    height: 24px;
    float: none;
    position: relative;
    top: 7px;
}
form .progress {
    line-height: 15px;
}
}
.progress {
    display: inline-block;
    width: 100px;
    border: 3px groove #CCC;
}
.progress div {
    font-size: smaller;
    background: orange;
    width: 0;
}
");

$form = ActiveForm::begin([
    'id' => 'question-update-form',
    'enableAjaxValidation' => false,
    'enableClientValidation' => false,
    'options' => [
        'onSubmit' => 'return false',
    ]
]);

echo $form->field($model, 'id')->hiddenInput(['ng-model' => 'data.id'])->label(false);
?>
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Câu hỏi</h3>
            </div>

            <div class="box-body">
                <?php /*$form->field($model, 'content')->widget(TinyMce::className(), [
                        'options' => ['rows' => 6, 'ng-model' => "data.questionContent"],
                        'language' => 'vi',
                        'clientOptions' => [
                            'plugins' => [
                                "advlist autolink lists link charmap print preview anchor",
                                "searchreplace visualblocks code fullscreen",
                                "insertdatetime media table contextmenu paste"
                            ],
                            'toolbar' => "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
                        ]
                    ])*/ ?>
                <div class="form-group">
                    <lable>Nội dung câu hỏi (*)</lable>
                    <textarea ng-model="data.questionContent"
                              class="form-control"></textarea>
                </div>
                <?=$this->render('_media') ?>
                <div class="form-group">
                    <label>Giải thích đáp án</label>
                    <textarea ng-model="data.answerExplain" class="form-control">
                    </textarea>
                </div>


            </div>
        </div>

    </div>


    <div class="well">
        <button type="button" ng-click="save()" class="btn btn-primary" ng-disabled="loading"><i
                class="fa fa-refresh fa-spin" ng-show="loading"></i> Cập nhật
        </button>
    </div>
</div>
<?php ActiveForm::end() ?>

