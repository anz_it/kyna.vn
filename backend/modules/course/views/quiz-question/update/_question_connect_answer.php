<?php
/**
 * Created by PhpStorm.
 * User: khanhphan
 * Date: 10/6/16
 * Time: 11:26 AM
 * @var $this \yii\web\View
 * @var $form ActiveForm
 *
 */

use yii\bootstrap\ActiveForm;

$this->registerCss("
.question-answer-detail .row {
    margin: 5px 0;
}
.question-answer-detail .answer-content:hover {
    background: lightblue;
}
.insert-answer {
    margin-top: 10px;
}
.question-answer-detail .deleted {
    text-decoration: line-through;
    font-style: italic;
}
.undo-btn {
     -moz-transform: scaleX(-1);
     -webkit-transform: scaleX(-1);
     -o-transform: scaleX(-1);
     transform: scaleX(-1);
     color: gray;
     
     
}
.btn-rm {
   position: absolute;
    right: 23px;
    z-index: 4;
    margin-top: 5px;
}
.box-deleted {
    opacity: 0.5
}
.thumb {
    width: 24px;
    height: 24px;
    float: none;
    position: relative;
    top: 7px;
}
form .progress {
    line-height: 15px;
}
}
.progress {
    display: inline-block;
    width: 100px;
    border: 3px groove #CCC;
}
.progress div {
    font-size: smaller;
    background: orange;
    width: 0;
}
");

$form = ActiveForm::begin([
    'id' => 'question-update-form',
    'enableAjaxValidation' => false,
    'enableClientValidation' => false,
    'options' => [
        'onSubmit' => 'return false',
    ]
]);

echo $form->field($model, 'id')->hiddenInput(['ng-model' => 'data.id'])->label(false);
?>
<div class="row">
    <div class="col-md-12" ng-class="data.error ? 'has-error' : ''">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Câu hỏi</h3>
            </div>

            <div class="box-body">
                <?php /*$form->field($model, 'content')->widget(TinyMce::className(), [
                        'options' => ['rows' => 6, 'ng-model' => "data.questionContent"],
                        'language' => 'vi',
                        'clientOptions' => [
                            'plugins' => [
                                "advlist autolink lists link charmap print preview anchor",
                                "searchreplace visualblocks code fullscreen",
                                "insertdatetime media table contextmenu paste"
                            ],
                            'toolbar' => "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
                        ]
                    ])*/ ?>
                <div class="form-group">
                    <label>Nội dung câu hỏi (*) </label>
                    <textarea ui-tinymce="tinymceNoImageOptions" ng-model="data.questionContent" class="form-control"></textarea>
                    <span class="help-block" ng-if="data.error">{{data.error}}</span>
                </div>

                <?=$this->render('_media') ?>

                <div class="form-group">
                    <label>Giải thích đáp án</label>
                    <textarea ui-tinymce="tinymceOptions" ng-model="data.answerExplain" class="form-control">
                    </textarea>
                </div>

            </div>
        </div>
    </div>
</div>
<div class="row" ng-repeat="question  in data.questions">
    <button class="btn btn-danger btn-rm" ng-click="removeAnswer(question)" ng-if="!question.isDeleted">Xóa</button>
    <button class="btn btn-info btn-rm" ng-click="unremovedAnswer(question)" ng-if="!!question.isDeleted">Phục hồi</button>
    <div class="col-md-12">

        <div class="box" ng-class="!question.isDeleted ? ' box-success' : 'box-default box-deleted'">
            <div class="box-body" ng-class="question.question.error || question.answer.error ? 'has-error' : ''">
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Nội dung cột A </label>
                        <textarea ui-tinymce="tinymceNoImageOptions" ng-model="question.question.content" class="form-control"></textarea>
                        <span class="help-block" ng-if="question.question.error">{{question.question.error}}</span>
                    </div>
                    <div class="form-group">
                        <label>Hình ảnh </label>
                        <div></div>
                        <div style="margin-bottom: 20px" ng-if="!!question.question.imageUrl">
                            <img ng-src="{{params.mediaLink + question.question.imageUrl}}" width="200px">
                            <a class="btn btn-danger" ng-click="removeAnswerImage(question.question)">Xóa</a>
                        </div>

                        <button class="btn btn-info" ngf-select="uploadModelImage(question.question, $files)" accept="image/*">Chọn file hình ảnh...</button>

                        <div class="alert alert-error" ng-if="!!question.question.imageError">{{question.question.imageError}}</div>

                        <span class="progress" ng-show="question.question.imageProcess >= 0">
                             <div style="width:{{imageProcess}}%" ng-bind="question.question.imageProcess + '%'"></div>
                        </span>
                    </div>
                    <div class="form-group">
                        <label>Thứ tự</label>
                        <input type="number" class="form-control" ng-model="question.question.order" />
                    </div>
                </div>


                <div class="col-md-6">
                    <div class="form-group">
                        <label>Nội dung cột B </label>
                        <textarea ui-tinymce="tinymceNoImageOptions" ng-model="question.answer.content" class="form-control"></textarea>
                        <span class="help-block" ng-if="question.answer.error">{{question.answer.error}}</span>
                    </div>
                    <div class="form-group">
                        <label>Hình ảnh </label>
                        <div></div>
                        <div style="margin-bottom: 20px" ng-if="!!question.answer.imageUrl">
                            <img ng-src="{{params.mediaLink + question.answer.imageUrl}}" width="200px">
                            <a class="btn btn-danger" ng-click="removeAnswerImage(question.answer)">Xóa</a>
                        </div>

                        <button class="btn btn-info" ngf-select="uploadModelImage(question.answer, $files)" accept="image/*">Chọn file hình ảnh...</button>

                        <div class="alert alert-error" ng-if="!!question.answer.imageError">{{question.answer.imageError}}</div>

                        <span class="progress" ng-show="question.answer.imageProcess >= 0">
                             <div style="width:{{imageProcess}}%" ng-bind="question.answer.imageProcess + '%'"></div>
                        </span>
                    </div>
                    <div class="form-group">
                        <label>Thứ tự</label>
                        <input type="number" class="form-control" ng-model="question.answer.order"/>
                    </div>
                </div>
                <hr>
            </div>
        </div>


    </div>
</div>

    <div class="clearfix"></div>
    <div class="well">
        <button type="button" ng-click="save()" class="btn btn-primary" ng-disabled="loading"> <i class="fa fa-refresh fa-spin" ng-show="loading"></i> Cập nhật</button>
        <button type="button" ng-click="addMoreAnswerConnect()" class="btn btn-success" ng-disabled = "loading"> <i class="fa fa-plus"></i> Thêm câu trả lời</button>
    </div>

<?php  ActiveForm::end() ?>

