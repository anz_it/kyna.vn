<?php

use yii\helpers\Html;
use app\modules\course\controllers\DefaultController;
use yii\helpers\StringHelper;

$this->title = Yii::$app->params['crudTitles']['create'];

$this->params['breadcrumbs'][] = ['label' => 'Khóa học', 'url' => ['/course/default/index']];
$this->params['breadcrumbs'][] = ['label' => StringHelper::truncateWords($model->course->name, 6), 'url' => ['/course/view/index', 'id' => $model->course->id]];
$this->params['breadcrumbs'][] = ['label' => $this->context->mainTitle, 'url' => ['/course/view/question', 'id' => $model->course->id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="course-question-create">
    <?= $this->render('_buttons', ['model' => $model]) ?>
    
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
