<?php

use yii\helpers\Html;
use yii\grid\GridView;

$crudTitles = Yii::$app->params['crudTitles'];

$this->title = Yii::$app->controller->mainTitle;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-xs-12">
        <div class="course-question-index">
            <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

            <p>
                <?= Html::a($crudTitles['create'], ['create'], ['class' => 'btn btn-success']) ?>
            </p>

            <div class="box">
                <?=
                GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],
                        'id',
                        'course_id',
                        'type',
                        'content:ntext',
                        'image_url:ntext',
                        // 'status',
                        // 'is_deleted:boolean',
                        // 'created_time:datetime',
                        // 'updated_time:datetime',
                        ['class' => 'yii\grid\ActionColumn'],
                    ],
                ]);
                ?>
            </div>
        </div>
    </div>
</div>