<?php

use yii\helpers\Json;
use yii\web\View;

use common\helpers\StringHelper;
use kyna\course\models\QuizQuestion;

use app\modules\course\assets\UpdateAsset;
use backend\assets\BootboxAsset;

BootboxAsset::register($this);
UpdateAsset::register($this);

$this->title = 'Cập nhật #' . $model->id;

$this->params['breadcrumbs'][] = ['label' => 'Khóa học', 'url' => ['/course/default/index']];
$this->params['breadcrumbs'][] = ['label' => StringHelper::truncateWords($model->course->name, 6), 'url' => ['/course/view/index', 'id' => $model->course->id]];
$this->params['breadcrumbs'][] = ['label' => $this->context->mainTitle, 'url' => ['/course/view/question', 'id' => $model->course->id]];
$this->params['breadcrumbs'][] = $this->title;

/**
 * @var $this \yii\web\View
 */
$this->registerJs("window.questionData = " . Json::encode($jsonData), View::POS_HEAD);
$this->registerJs("window.appParams = " . Json::encode([
    'mediaLink' => Yii::$app->params['media_link']
]), View::POS_HEAD);
?>
<div ng-app="UpdateQuestionApp" ng-controller="UpdateQuestionController">

    <?= $this->render('_buttons', ['model' => $model]) ?>

    <?php
    switch ($model->type) {
        case QuizQuestion::TYPE_FILL_INTO_DOTS:
            echo $this->render('update/_question_fill_into_dots', ['model' => $model]);
            break;

        case QuizQuestion::TYPE_CONNECT_ANSWER:
            echo $this->render('update/_question_connect_answer', ['model' => $model]);
            break;

        case QuizQuestion::TYPE_ONE_CHOICE:
        case QuizQuestion::TYPE_MULTIPLE_CHOICE:
            echo $this->render('update/_question_one_choice', ['model' => $model]);
            break;

        case QuizQuestion::TYPE_WRITING:
            echo $this->render('update/_question_essay', ['model' => $model]);
            break;
    }
    ?>
</div>
