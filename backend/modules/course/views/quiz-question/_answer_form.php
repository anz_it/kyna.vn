<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\modules\course\controllers\QuestionController;
use common\widgets\tinymce\TinyMce;

$crudTitles = Yii::$app->params['crudTitles'];
?>

<div class="course-answer-form">
    <?php $form = ActiveForm::begin(); ?>
        <?php if($question_model->type == \kyna\course\models\QuizQuestion::TYPE_FILL_INTO_DOTS){ ?>
            <?php if($model->isNewRecord){ ?>
                <div class="alert-success alert">

                    Bạn đang nhập câu trả lời cho chỗ trống thứ <?php
                    $count = $question_model->number_of_dots - count($question_model->answers);
                    echo $count;
                    ?>
                </div>
            <?php } ?>
            <div class="alert">
                Câu hỏi hiện tại là: <b><?= $question_model->content; ?></b>
            </div>
            <hr />
            <?php if(!$question_model->answersValidate) { ?>
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    Hiện tại câu trả lời đã đủ với số lượng vị trí. Vui lòng tăng số lượng vị trí của câu hỏi.
                </div>
            <?php } ?>
        <?php } ?>
        <?php if($question_model->answersValidate) { ?>
            <?= $form->field($model, 'content') ?>
        <?php } ?>
        <?= $form->field($model, 'image_url') ?>
        <?php if($question_model->type != \kyna\course\models\QuizQuestion::TYPE_FILL_INTO_DOTS) { ?>
            <?= $form->field($model, 'is_correct')->checkbox() ?>
        <?php } ?>
    
        <?= $form->field($model, 'explanation')->widget(TinyMce::className()) ?>

        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? $crudTitles['create'] : $crudTitles['update'], ['class' => 'btn btn-primary']) ?>
            <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
        </div>

    <?php ActiveForm::end(); ?>

</div>
