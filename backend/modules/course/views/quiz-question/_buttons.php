<?php

use yii\helpers\Html;
use app\modules\course\controllers\DefaultController;

$crudTitles = Yii::$app->params['crudTitles'];
$crudButtonIcons = Yii::$app->params['crudButtonIcons'];

?>

<p class="pull-left">
    <?= Html::a(
        $crudButtonIcons['back'] . " " . $crudTitles['back'],
        ['/course/view/question', 'id' => $model->course_id]
        , ['class' => 'btn btn-info', 'icon' => 'glyphicon glyphicon-backward']) ?>
</p>
<div class="clearfix"></div>