<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use common\components\SortableColumn;
use kartik\widgets\Select2;
use kyna\course\models\search\QuizAnswerSearch;
use app\modules\course\controllers\QuizQuestionController;

$searchModel = new QuizAnswerSearch();
$searchModel->question_id = $model->id;
$dataProvider = $searchModel->search(Yii::$app->request->queryParams);

$crudTitles = Yii::$app->params['crudTitles'];
?>

<?php echo $this->render('_answer_form', ['model' => $answerModel, 'question_model' => $model]); ?>

<?=
GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'options' => [
        'data' => [
            'sortable-widget' => 1,
            'sortable-url' => Url::toRoute(['/course/answer/sorting']),
        ]
    ],
    'rowOptions' => function ($model, $key, $index, $grid) {
        return ['data-sortable-id' => $model->id];
    },
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        [
            'class' => SortableColumn::className(),
        ],
        [
            'attribute' => 'id',
            'options' => ['class' => 'col-xs-2']
        ],
        'content:html',
        [
            'attribute' => 'is_correct',
            'value' => function ($model) {
                return $model->isExactlyText;
            },
            'visible' => ($model->type != \kyna\course\models\QuizQuestion::TYPE_FILL_INTO_DOTS),
            'filter' => Select2::widget([
                'model' => $searchModel,
                'attribute' => 'is_correct',
                'data' => QuizAnswerSearch::getIsExactlyOptions(),
                'options' => ['placeholder' => $crudTitles['prompt']],
                'pluginOptions' => [
                    'allowClear' => true
                ],
                'hideSearch' => true,
            ])
        ],
        [
            'attribute' => 'image_url',
            'format' => 'html',
            'value' => function ($model) {
                return !empty($model->image_url) ? Html::img($model->image_url, ['width' => '100px']) : null;
            },
            'filter' => false,
            'options' => ['class' => 'col-xs-1']
        ],
        [
            'attribute' => 'status',
            'value' => function ($model) {
                return Html::a(
                        $model->statusHtml,
                        Url::toRoute([
                            '/course/answer/change-status',
                            'id' => $model->id,
                            'redirectUrl' => Url::toRoute(['/course/quiz-question/update', 'id' => $model->question_id, 'tab' => QuizQuestionController::TAB_ANSWER])
                        ]),
                        ['title' => 'Thay đổi tình trạng']
                    );
            },
            'format' => 'html',
            'filter' => Select2::widget([
                'model' => $searchModel,
                'attribute' => 'status',
                'data' => QuizAnswerSearch::listStatus(),
                'options' => ['placeholder' => $crudTitles['prompt']],
                'pluginOptions' => [
                    'allowClear' => true
                ],
                'hideSearch' => true,
            ])
        ],
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{update} {delete}',
            'controller' => 'quiz-question',
            'buttons' => [
                'update' => function ($url, $model, $key) {
                    $url = Url::toRoute(['/course/quiz-question/update',
                        'id' => $model->question_id,
                        'tab' => QuizQuestionController::TAB_ANSWER,
                        'answerId' => $model->id,
                        'quizId' => Yii::$app->request->get('quizId'),
                    ]);
                    
                    $options = [
                        'title' => Yii::t('yii', 'Update'),
                        'aria-label' => Yii::t('yii', 'Update'),
                        'data-pjax' => '0',
                    ];
                    return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, $options);
                },
                'delete' => function ($url, $model, $key) {
                    $url = Url::toRoute([
                        '/course/answer/delete', 
                        'id' => $model->id, 
                        'redirectUrl' => Url::toRoute([
                            '/course/quiz-question/update',
                            'id' => $model->question_id,
                            'tab' => QuizQuestionController::TAB_ANSWER,
                            'quizId' => Yii::$app->request->get('quizId'),
                        ])
                    ]);

                    $options = array_merge([
                        'title' => Yii::t('yii', 'Delete'),
                        'aria-label' => Yii::t('yii', 'Delete'),
                        'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
                        'data-method' => 'post',
                        'data-pjax' => '0',
                    ]);
                    return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, $options);
                },
            ]
        ],
    ],
]);
?>
