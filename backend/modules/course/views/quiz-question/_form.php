<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\Select2;
use dosamigos\tinymce\TinyMce;
use kyna\course\models\QuizQuestion;
use app\modules\course\controllers\DefaultController;

$crudTitles = Yii::$app->params['crudTitles'];

$mceClientOptions = [
    'plugins' => [
        "advlist autolink lists link charmap print preview anchor",
        "searchreplace visualblocks code fullscreen",
        "insertdatetime media table contextmenu paste"
    ],
    'toolbar' => "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
];
?>

<div class="course-question-form">
    <div class="row">
        <?php $form = ActiveForm::begin(); ?>
        <div class="col-md-12">

            <div class="col-md-6">
                <?= $form->field($model, 'type')->dropDownList(QuizQuestion::getTypes(),['class' => 'question-type form-control']); ?>

                <?= $form->field($model, 'number_of_dots')->textInput(['class' => 'number-of-dots form-control']); ?>
            </div>
            <div class="col-md-6">


                <?= $form->field($model, 'status')->widget(Select2::classname(), [
                    'data' => QuizQuestion::listStatus(),
                    'hideSearch' => true,
                    'options' => ['placeholder' => $crudTitles['prompt']],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]) ?>

                <?= $form->field($model, 'image_url')->textInput(['maxlength' => true]) ?>
            </div>
        </div>
        <div class="col-md-12">
            <?= $form->field($model, 'content')->widget(TinyMce::className(), [
                'options' => ['rows' => 6],
                'language' => 'vi',
                'clientOptions' => $mceClientOptions
            ]) ?>

            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? $crudTitles['create'] : $crudTitles['update'], ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                <?= Html::a($crudTitles['cancel'], ['/course/view/question', 'id' => $model->course_id], ['class' => 'btn btn-default']) ?>
            </div>

        </div>



        <?php ActiveForm::end(); ?>
    </div>


</div>
