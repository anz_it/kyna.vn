<?php

use yii\helpers\Html;

$this->title = Yii::$app->params['crudTitles']['create'];
$this->params['breadcrumbs'][] = ['label' => Yii::$app->controller->mainTitle, 'url' => ['/course/default/index']];
$this->params['breadcrumbs'][] = ['label' => 'Screenshot', 'url' => ['/course/view/screenshot', 'id' => $model->course_id]];
$this->params['breadcrumbs'][] = 'Thêm'
?>
<div class="becategory-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
