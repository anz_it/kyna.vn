<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\Select2;
use dosamigos\tinymce\TinyMce;
use kartik\tree\TreeViewInput;
use kyna\course\models\CourseSection;
use kyna\course\models\Quiz;
use app\modules\course\controllers\DefaultController;

$crudTitles = Yii::$app->params['crudTitles'];
?>

<div class="course-quiz-form">

    <?php $form = ActiveForm::begin(); ?>
    
    <?= $form->field($lesson, 'section_id')->widget(TreeViewInput::classname(), [
            'query' => CourseSection::find()->where(['course_id' => $lesson->course_id])->addOrderBy('root, lft'),
            'headingOptions' => ['label' => 'Cấu trúc khóa học'],
            'rootOptions' => ['label' => $lesson->course->name],
            'fontAwesome' => true,
            'asDropdown' => true,
            'multiple' => false,
            'options' => ['disabled' => false],
            'dropdownConfig' => [
                'input' => [
                    'placeholder' => $crudTitles['prompt']
                ],
            ],
        ]) ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'type')->dropDownList($model->types) ?>

    <?= $form->field($lesson, 'day_can_learn')->textInput() ?>

    <?= $form->field($model, 'image_url')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->widget(TinyMce::className(), [
                'options' => ['rows' => 6],
                'language' => 'vi',
            ]) ?>
    
    <?= $form->field($lesson, 'note')->widget(TinyMce::className(), [
                'options' => ['rows' => 6],
                'language' => 'vi',
            ]) ?>

    <?= $form->field($model, 'status')->widget(Select2::classname(), [
            'data' => Quiz::listStatus(),
            'hideSearch' => true,
            'options' => ['placeholder' => $crudTitles['prompt']],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? $crudTitles['create'] : $crudTitles['update'], ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::a($crudTitles['cancel'], ['/course/view/lesson', 'id' => $model->course_id, 'sectionId' => Yii::$app->request->get('sectionId')], ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
