<?php

use yii\helpers\StringHelper;
use yii\bootstrap\Tabs;

$this->title = StringHelper::truncateWords($model->name, 10);
$backUrl = ['/course/view/lesson', 'id' => $model->course_id, 'sectionId' => $lesson->section_id];

$this->params['breadcrumbs'][] = ['label' => 'Khóa học', 'url' => ['/couse/default/index']];
$this->params['breadcrumbs'][] = ['label' => StringHelper::truncateWords($model->course->name, 6), 'url' => ['/course/view/index', 'id' => $model->course_id]];
$this->params['breadcrumbs'][] = ['label' => 'Bài học', 'url' => ['/course/view/lesson', 'id' => $model->course_id]];
$this->params['breadcrumbs'][] = ['label' => $lesson->section->name, 'url' => $backUrl];
$this->params['breadcrumbs'][] = StringHelper::truncateWords($model->name, 6);

$crudTitles = Yii::$app->params['crudTitles'];
$crudButtonIcons = Yii::$app->params['crudButtonIcons'];
?>
<div class="course-quiz-update">
    <?= $this->render('_buttons', ['model' => $model, 'backUrl' => $backUrl]) ?>

    <div class="course-quiz-create">
        <?=
        Tabs::widget([
            'items' => $this->context->getTabItems($model, $lesson, $tab, $metaModels)
        ]);
        ?>
    </div>

</div>
