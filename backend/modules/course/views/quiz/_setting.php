<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;

use app\components\MetaFieldType;
use app\modules\course\controllers\DefaultController;

$crudTitles = Yii::$app->params['crudTitles'];
?>

<?php if ($model->isNewRecord) : ?>
    <div class="alert alert-warning" >
        Hãy nhập thông tin và lưu bài Quiz trước.
    </div>
<?php else: ?>

    <?php $form = ActiveForm::begin(); ?>

    <?php
    echo $form->field($lesson, 'coefficient')->textInput();

    echo $form->field($lesson, 'percent_can_pass')->textInput();

    foreach ($metaModels as $key => $metaModel) {
        echo MetaFieldType::render($form, $metaModel, "[{$key}]value", ['id' => "meta-{$key}"]);
    }
    ?>
    <div class="form-group">
        <?= Html::submitButton($crudTitles['update'], ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::a($crudTitles['cancel'], ['/course/view/lesson', 'id' => $model->course_id], ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

<?php endif; ?>
