<?php

use yii\bootstrap\Tabs;
use yii\helpers\StringHelper;

$backUrl = ['/course/view/lesson', 'id' => $model->course_id];

$this->title = Yii::$app->params['crudTitles']['create'] . ' Quiz';

$this->params['breadcrumbs'][] = ['label' => 'Khóa học', 'url' => ['/couse/default/index']];
$this->params['breadcrumbs'][] = ['label' => StringHelper::truncateWords($model->course->name, 6), 'url' => ['/course/view/index', 'id' => $model->course_id]];
$this->params['breadcrumbs'][] = ['label' => 'Bài học', 'url' => $backUrl];

if (!empty($lesson->section)) {
    $backUrl['sectionId'] = $lesson->section_id;
    $this->params['breadcrumbs'][] = ['label' => $lesson->section->name, 'url' => $backUrl];
}

$this->params['breadcrumbs'][] = $this->title;

?>
<div class="course-quiz-create">
    <?= $this->render('_buttons', ['model' => $model, 'backUrl' => $backUrl]) ?>
    
    <?=
    Tabs::widget([
        'items' => $this->context->getTabItems($model, $lesson, $tab)
    ]);
    ?>
</div>
