<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

$this->title = StringHelper::truncateWords($model->name, 10);
$this->params['breadcrumbs'][] = ['label' => Yii::$app->controller->mainTitle, 'url' => ['index']];
$this->params['breadcrumbs'][] = StringHelper::truncateWords($model->name, 6);

$crudTitles = Yii::$app->params['crudTitles'];
?>
<div class="course-quiz-view">

    <p>
        <?= Html::a("<span class='glyphicon glyphicon-backward'></span> " . $crudTitles['back'], ['index'], ['class' => 'btn btn-info', 'icon' => 'glyphicon glyphicon-backward']) ?>
        <?= Html::a($crudTitles['update'], ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a($crudTitles['delete'], ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'course_id',
            'name',
            'image_url:ntext',
            'description:ntext',
            'status',
            'created_time:datetime',
            'updated_time:datetime',
        ],
    ]) ?>

</div>
