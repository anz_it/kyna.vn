<?php

$crudTitles = Yii::$app->params['crudTitles'];
?>
<?php if ($model->isNewRecord) {?>
    <div class="alert alert-warning" >
        Hãy nhập thông tin và lưu bài Quiz trước.
    </div>
<?php }else{ ?>
    <div class="row">
        <div class="col-md-6">
            <?=$this->render('gridview/_quiz_question', ['model' => $model]); ?>
        </div>
        
        <div class="col-md-6">
            <?=$this->render('gridview/_course_question', ['model' => $model]); ?>
        </div>
    </div>
<?php } ?>
