<?php

use yii\helpers\Html;

$crudTitles = Yii::$app->params['crudTitles'];
$crudButtonIcons = Yii::$app->params['crudButtonIcons'];

?>

<p class="pull-right">
    <?php 
        echo Html::a(
        $crudButtonIcons['back'] . " " . $crudTitles['back'],
        $backUrl
        , ['class' => 'btn btn-info']);
    ?>
</p>