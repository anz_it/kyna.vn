<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\grid\GridView;
use kyna\course\models\search\QuizQuestionSearch;
use app\modules\course\controllers\QuizQuestionController;

$questionSearchModel = new QuizQuestionSearch();
$questionSearchModel->course_id = $model->course_id;
$questionSearchModel->status = QuizQuestionSearch::STATUS_ACTIVE;
$questionDataProvider = $questionSearchModel->search(Yii::$app->request->queryParams);
$questionDataProvider->query->andWhere(['or', 'parent_id is null', 'parent_id = 0']);

?>

<h2>Câu hỏi của toàn khóa học</h2>

<?=
GridView::widget([
    'dataProvider' => $questionDataProvider,
    'filterModel' => $questionSearchModel,
    'columns' => [
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{addToQuiz} {listQuestion} {update} {delete}',
            'controller' => 'quiz-question',
            'options' => ['class' => 'col-xs-2'],
             'visibleButtons' => [
                'listQuestion' => function ($model, $key, $index) {
                    return $model->type !== QuizQuestionSearch::TYPE_WRITING;
                },
            ],
            'buttons' => [
                'addToQuiz' => function ($url, $model, $key) {
                    return Html::a('<span class="fa fa-arrow-left"></span>', 
                        [
                            '/course/quiz-detail/create',
                            'questionId' => $model->id,
                            'quizId' => Yii::$app->request->get('id'),
                        ],
                        [
                            'title' => 'Thêm vào Quiz'
                        ]
                    );
                },
                'listQuestion' => function ($url, $model, $key) {
                    return Html::a('<span class="fa fa-plus"></span>', 
                        [
                            '/course/question/update', 
                            'id' => $model->id, 
                            'tab' => QuizQuestionController::TAB_ANSWER,
                            'quizId' => Yii::$app->request->get('id')
                        ],
                        [
                            'title' => 'Thêm câu trả lời'
                        ]
                    );
                }
            ]
        ],
        ['class' => 'yii\grid\SerialColumn'],
        [
            'attribute' => 'id',
            'options' => ['class' => 'col-xs-2']
        ],
        'content:html',
    ],
]);
?>