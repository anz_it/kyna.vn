<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\grid\GridView;
use common\components\SortableColumn;
use kyna\course\models\search\QuizDetailSearch;
use kyna\course\models\QuizQuestion;
use app\modules\course\controllers\QuizQuestionController;

$searchModel = new QuizDetailSearch;
$searchModel->quiz_id = $model->id;
$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
$dataProvider->query->andWhere(['or', 'quiz_details.parent_id is null', 'quiz_details.parent_id = 0']);
$dataProvider->query->joinWith('quizQuestion')->andWhere([QuizQuestion::tableName() . '.status' => QuizQuestion::STATUS_ACTIVE]);
?>

<h2>Câu hỏi hiện tại của Quiz</h2>

<?=
GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'options' => [
        'data' => [
            'sortable-widget' => 1,
            'sortable-url' => Url::toRoute(['/course/quiz-detail/sorting']),
        ]
    ],
    'rowOptions' => function ($model) {
        return ['data-sortable-id' => $model->id];
    },
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        [
            'class' => SortableColumn::className(),
        ],
        [
            'attribute' => 'quiz_question_id',
            'label' => 'Id câu hỏi',
            'options' => ['class' => 'col-xs-2']
        ],
        [
            'attribute' => 'questionContent',
            'format' => 'html',
            'value' => function ($model) {
                return $model->quizQuestion->content;
            }
        ],
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{listQuestion} {update} {delete}',
            'controller' => 'quiz-detail',
            'options' => ['class' => 'col-xs-2'],
             'visibleButtons' => [
                'listQuestion' => function ($model, $key, $index) {
                    return $model->quizQuestion->type !== QuizQuestion::TYPE_WRITING;
                },
            ],
            'buttons' => [
                'delete' => function ($url, $model, $key) {
                    $options = array_merge([
                        'title' => Yii::t('yii', 'Remove khỏi Quiz'),
                        'aria-label' => Yii::t('yii', 'Remove khỏi Quiz'),
                        'data-confirm' => Yii::t('yii', 'Bạn có chắc là xóa câu hỏi này khỏi bài Quiz?'),
                        'data-method' => 'post',
                        'data-pjax' => '0',
                    ]);
                    return Html::a('<span class="fa fa-arrow-right"></span>', $url, $options);
                },
                'update' => function ($url, $model, $key) {
                    $url = [
                        '/course/quiz-question/update', 
                        'id' => $model->quiz_question_id,
                        'quizId' => $model->quiz_id,
                    ];

                    $options = [
                        'title' => Yii::t('yii', 'Update'),
                        'aria-label' => Yii::t('yii', 'Update'),
                        'data-pjax' => '0',
                    ];

                    return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, $options);
                },
                'listQuestion' => function ($url, $model, $key) {
                    return Html::a('<span class="fa fa-plus"></span>', 
                        [
                            '/course/quiz-question/update',
                            'id' => $model->quiz_question_id,
                            'tab' => QuizQuestionController::TAB_ANSWER,
                            'quizId' => $model->quiz_id,
                        ],
                        [
                            'title' => 'Thêm câu trả lời'
                        ]
                    );
                }
            ]
        ],
    ],
]);
?>

