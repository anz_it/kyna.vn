<?= \common\widgets\upload\Upload::widget([
    'autoUploadUrl' => '/course/upload/do-upload',
    'lastUploadUrl' => '/course/upload/last-upload-files',
    'display' => 'image',
    'name' => 'demo-upload' // match $this->uploadParamName
]) ?>
