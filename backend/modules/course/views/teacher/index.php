<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Url;
use common\helpers\CDNHelper;
use kyna\user\models\UserFollowTeacher;
use kyna\user\models\User;
$crudTitles = Yii::$app->params['crudTitles'];
$crudButtonIcons = Yii::$app->params['crudButtonIcons'];

$this->title = Yii::$app->controller->mainTitle;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-xs-12">
        <div class="beuser-index">
            <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
            <nav class="navbar navbar-default">
                <?= Html::a($crudButtonIcons['create'] . ' ' . $crudTitles['create'], ['create'], ['class' => 'btn btn-success navbar-btn navbar-left']) ?><!-- Button assign -->
                <?php if (Yii::$app->user->can('RelationManager.All')) : ?>
                    <a class="btn btn-primary navbar-btn navbar-left" id="btn_assign" href="/course/teacher/assign">
                        <i class="fa fa-upload"></i> Add Relation
                    </a>
                <?php endif; ?>
            </nav>

            <div class="box">
                <?php Pjax::begin(['timeout' => Yii::$app->params['timeOutPjax'], 'id' => 'idPjaxReload']) ?>
                <?=
                GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],
                        [
                            'attribute' => 'id',
                            'options' => ['class' => 'col-xs-2']
                        ],
                        'username',
                        'email:email',
                        [
                            'attribute' => 'avatar',
                            'format' => 'html',
                            'value' => function ($model) {
                                return CDNHelper::image($model->avatar, [
                                    'size' => CDNHelper::IMG_SIZE_AVATAR,
                                    'resizeMode' => 'crop',
                                    'alt' => $model->profile->name,
                                ]);
                            },
                            'filter' => false,
                            'options' => ['class' => 'col-xs-1']
                        ],
                        [
                            'attribute' => 'profile.name',
                            'filter' => Html::activeInput('text', $searchModel, 'name', ['class' => 'form-control'])
                        ],
                        [
                            'attribute' => 'slug',
                            'filter' => Html::activeInput('text', $searchModel, 'slug', ['class' => 'form-control'])
                        ],
                        [
                            'attribute' => 'relation',
                            'label' => 'User Relation',
                            'value' => function ($model) {
                                /* @var $model \kyna\course\models\Teacher*/
                                $teacherId = $model->getId();
                                $userFollow = UserFollowTeacher::findOne(['user_teacher'=>$teacherId]);
                                if($userFollow){
                                    return User::findOne(['id'=>$userFollow->getUserFollow()])->email;
                                }
                                return 'NA';
                            },
                            'filter' => Html::activeInput('text', $searchModel, 'relation', ['class' => 'form-control'])
                        ],
                        [
                            'attribute' => 'created_at',
                            'label' => 'Ngày tham gia',
                            'format' => 'date',
                            'filter' => false
                        ],
                        [
                            'class' => 'app\components\ActionColumnCustom',
                            'template' => '{view} {update} {delete} {contract} {export-notify} {add-relation-teacher-income}',
                            'buttons' => [
                                'contract' => function ($url, $model, $key) {
                                    $url = Url::toRoute(['/course/teacher-contracts/index', 'user_id' => $model->id]);

                                    $options = [
                                        'title' => 'Hợp đồng',
                                        'data-pjax' => '0',
                                    ];
                                    return Html::a('<span class="glyphicon glyphicon-file"></span>', $url, $options);
                                },
                                'export-notify' => function ($url, $model, $key) {
                                    $options = [
                                        'title' => 'Export danh sách nhận thông báo',
                                        'data-pjax' => '0',
                                    ];
                                    return Html::a('<span class="glyphicon glyphicon-export"></span>', $url, $options);
                                },

                                'add-relation-teacher-income' => function ($url, $model, $key) {
                                    $url = Url::toRoute(['/course/relation-teacher-income/create', 'user_id' => $model->id]);
                                    $options = [
                                        'title' => 'Add Relation',
                                        'data-pjax' => '0',
                                    ];
                                    return Html::a('<span class="glyphicon glyphicon-plus"></span>', $url, $options);
                                },
                            ]
                        ],
                    ],
                ]);
                ?>
                <?php Pjax::end() ?>
            </div>
        </div>
    </div>
</div>

<?php
$script = "
    ;(function($, window, document, undefined){
        $(document).ready(function(){        
            $(\"body\").on(\"click\", \"#btn_assign\", function (e) {
                e.preventDefault();
                var modal = $(\"#modal\"),
                    url = this.href;
       
                modal.find(\".modal-content\").load(url, function (resp) {
                    modal.modal(\"show\");
                });
            });
        });
    })(window.jQuery || window.Zepto, window, document);";
$css = "
    .modal-content .modal-header {
        border-radius: 0;
    }
    ";
?>
<?php
$this->registerCss($css);
$this->registerJs($script, \yii\web\View::POS_END, 'export-code');
?>