<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\BEUser */

$this->title = Yii::$app->params['crudTitles']['create'];
$this->params['breadcrumbs'][] = ['label' => Yii::$app->controller->mainTitle, 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="beuser-create">

    <?= $this->render('_form', [
        'model' => $model,
        'profile' => $profile,
        'metaValueModels' => $metaValueModels
    ]) ?>

</div>
