<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\JsExpression;
use yii\helpers\Url;
use app\components\MetaFieldType;
use common\widgets\upload\Upload;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model app\models\BEUser */
/* @var $form yii\widgets\ActiveForm */

$crudTitles = Yii::$app->params['crudTitles'];
?>

<div class="beuser-form">

    <?php $form = ActiveForm::begin([
        'options' => ['enctype' => 'multipart/form-data', 'autocomplete' => 'off']
    ]); ?>

    <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($profile, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'avatar')->widget(Upload::className(), [
        'display' => 'image'
    ]) ?>

    <?php
    foreach ($metaValueModels as $key => $metaValueModel) {
        echo MetaFieldType::render($form, $metaValueModel, "[{$key}]value", ['id' => "meta-{$key}", 'autocomplete' => 'off']);
    }
    ?>

    <?php
    $model->tags_list = $model->tags;
    echo $form->field($model, 'tags_list')->widget(Select2::classname(), [
        'maintainOrder' => false,
        'options' => ['placeholder' => 'Separate tags with commas', 'multiple' => true],
        'pluginOptions' => [
            'allowClear' => false,
            'tags' => true,
            'tokenSeparators' => [','],
            'minimumInputLength' => 3,
            'language' => [
                'errorLoading' => new JsExpression("function () { return 'Chờ nhận kết quả...'; }"),
            ],
            'ajax' => [
                'url' => Url::toRoute(['/tag/api/search']),
                'dataType' => 'json',
                'data' => new JsExpression('function(params) { return {q:params.term}; }'),
            ],
            'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
            'templateResult' => new JsExpression('function(res) { return res.text; }'),
            'templateSelection' => new JsExpression('function (res) { return res.text; }'),
        ],
    ]);
    ?>

    <?= $form->field($profile, 'bio')->textarea(['rows' => 3]) ?>

    <?= $form->field($model, 'password')->passwordInput(['autocomplete' => 'off']) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? $crudTitles['create'] : $crudTitles['update'], ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::a($crudTitles['cancel'], ['index'], ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
