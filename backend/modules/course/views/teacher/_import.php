<?php

use yii\widgets\ActiveForm;
use yii\helpers\Url;
use kyna\taamkru\models\form\CodeImportForm;

//$model = new CodeImportForm();
?>
<?php $form = ActiveForm::begin([
    'id' => 'code_import_form',
    'options' => [
        'enctype' => 'multipart/form-data',
        'class' => ''
    ],
    //'action' => Url::toRoute(['import-excel'])
]) ?>
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <h4 class="modal-title">Vui lòng chọn file</h4>
</div>
<div class="modal-body">
    <?= $form->field($model, 'file', [
    ])->fileInput([
        'id' => 'code_upload',
        'class' => 'form-control',
    ]) ?>

    <p class="">(File mẫu: <a href="<?= Url::toRoute(['download-sample'])?>" class="navbar-link"><i class="fa fa-download"></i>Tải về</a>)</p>
</div>
<div class="modal-footer">
    <button type="submit" class="btn btn-default" >Add Relation</button>
</div>

<?php ActiveForm::end() ?>