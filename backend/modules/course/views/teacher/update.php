<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\BEUser */

$crudTitles = Yii::$app->params['crudTitles'];

$this->title = $crudTitles['update'] . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::$app->controller->mainTitle, 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = $crudTitles['update'];
?>
<div class="beuser-update">

    <?= $this->render('_form', [
        'model' => $model,
        'profile' => $profile,
        'metaValueModels' => $metaValueModels
    ]) ?>

</div>
