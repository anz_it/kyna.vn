<?php

use yii\helpers\Html;
use yii\grid\GridView;
use kartik\select2\Select2;
use yii\web\JsExpression;
use common\helpers\RoleHelper;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel kyna\course\models\search\TeacherContractsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$crudTitles = Yii::$app->params['crudTitles'];
$crudButtonIcons = Yii::$app->params['crudButtonIcons'];

$this->title = Yii::$app->controller->mainTitle;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row teacher-contracts-index">
    <div class="col-xs-12">
        <div class="beuser-index">
            <p>
                <?= Html::a($crudButtonIcons['create'] . ' ' . $crudTitles['create'], ['create', 'user_id' => $searchModel->user_id], ['class' => 'btn btn-success']) ?>
            </p>
            <div class="box">
                <?php
                $teacherDesc = empty($searchModel->user_id) ? '' : $searchModel->teacher->profile->name;
                ?>
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],

                        //'id',
                        'code',
                        'title',
                        'description',
                        [
                            'attribute' => 'user_id',
                            'value' => function ($model) {
                                return !empty($model->teacher) ? $model->teacher->profile->name : null;
                            },
                            'filter' => Select2::widget([
                                'model' => $searchModel,
                                'initValueText' => $teacherDesc,
                                'attribute' => 'user_id',
                                'options' => ['placeholder' => $crudTitles['prompt']],
                                'pluginOptions' => [
                                    'allowClear' => true,
                                    'minimumInputLength' => 3,
                                    'language' => [
                                        'errorLoading' => new JsExpression("function () { return 'Chờ nhận kết quả...'; }"),
                                    ],
                                    'ajax' => [
                                        'url' => Url::toRoute(['/user/api/search', 'role' => RoleHelper::ROLE_TEACHER, 'selectNameOnly' => true]),
                                        'dataType' => 'json',
                                        'data' => new JsExpression('function(params) { return {q:params.term}; }'),
                                    ],
                                    'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                                    'templateResult' => new JsExpression('function(teacher) { return teacher.text; }'),
                                    'templateSelection' => new JsExpression('function (teacher) { return teacher.text; }'),
                                ],
                            ])
                        ],
                        [
                            'attribute' => 'user_take_care',
                            'format' => 'raw',
                            'value' => function ($model) {
                                return $model->getUserTakeCareString();
                            }
                        ],
                        // 'created_time:datetime',
                        // 'updated_time:datetime',

                        ['class' => 'yii\grid\ActionColumn'],
                    ],
                ]); ?>
            </div>
        </div>
    </div>
</div>
