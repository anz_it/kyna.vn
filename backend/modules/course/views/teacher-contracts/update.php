<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model kyna\course\models\TeacherContracts */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Teacher Contracts',
]) . $model->code;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Teacher Contracts'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->code, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="teacher-contracts-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
