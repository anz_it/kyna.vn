<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model kyna\course\models\TeacherContracts */

$this->title = $model->code;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Teacher Contracts'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$crudTitles = Yii::$app->params['crudTitles'];
?>
<div class="teacher-contracts-view">

    <p>
        <?= Html::a("<span class='glyphicon glyphicon-backward'></span> " . $crudTitles['back'], ['index'], ['class' => 'btn btn-info', 'icon' => 'glyphicon glyphicon-backward']) ?>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'title',
            'description',
            'code',
            [
                'attribute' => 'user_id',
                'value' => !empty($model->teacher) ? $model->teacher->profile->name : null
            ],
            [
                'attribute' => 'user_take_care',
                'format' => 'raw',
                'value' => $model->getUserTakeCareString()
            ],
            'created_time:datetime',
            'updated_time:datetime',
        ],
    ]) ?>

</div>
