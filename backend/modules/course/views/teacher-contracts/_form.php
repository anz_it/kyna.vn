<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model kyna\course\models\TeacherContracts */
/* @var $form yii\widgets\ActiveForm */
$crudTitles = Yii::$app->params['crudTitles'];
$relations = [];
foreach (\kyna\user\models\User::getUsersByRole($this->context->relationRole) as $item) {
    $relations[$item->id] = $item->profile->name;
}
$teachers = [];
foreach (\kyna\user\models\User::getUsersByRole($this->context->teacherRole) as $item) {
    $teachers[$item->id] = $item->profile->name;
}
?>

<div class="teacher-contracts-form beuser-form">

    <?php $form = ActiveForm::begin([
        'options' => ['enctype' => 'multipart/form-data', 'autocomplete' => 'off']
    ]); ?>

    <?= $form->field($model, 'user_id')->widget(\kartik\select2\Select2::classname(), [
        'data' => $teachers,
        'hideSearch' => false,
        'options' => ['placeholder' => $crudTitles['prompt'], 'disabled' => ($model->isNewRecord)?$model->isDisableTeacher:true],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]) ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textarea(['maxlength' => true]) ?>

    <?= $form->field($model, 'code')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'user_take_care')->widget(\kartik\select2\Select2::classname(), [
        'data' => $relations,
        'hideSearch' => false,
        'options' => ['placeholder' => $crudTitles['prompt'], 'multiple' => true],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
