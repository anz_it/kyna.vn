<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model kyna\course\models\TeacherContracts */

$this->title = Yii::t('app', 'Create Teacher Contracts');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Teacher Contracts'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="teacher-contracts-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
