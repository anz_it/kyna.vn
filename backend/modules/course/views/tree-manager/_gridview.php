<?php

use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\web\View;

use kartik\widgets\Select2;
use kyna\course\models\search\CourseLessonSearch;
use common\components\SortableColumn;
use kyna\course\models\CourseLesson;

$sectionId = !empty($params['node']->id) ? $params['node']->id : 0;
$searchModel = new CourseLessonSearch();
$searchModel->section_id = $sectionId;
$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
$dataProvider->pagination  = false;
$dataProvider->sort = false;

$crudTitles = Yii::$app->params['crudTitles'];
$crudButtonIcons = Yii::$app->params['crudButtonIcons'];
?>

<div class="kv-detail-container">
    <div class="kv-detail-heading">
        <div class="pull-right">
            <?php
            if (\Yii::$app->user->can('Course.Create')) {
                echo Html::a('<i class="ion-android-add"></i> '.$crudTitles['create'].' bài tập', [
                    '/course/quiz/create',
                    'courseId' => $params['courseId'],
                    'sectionId' => $sectionId,
                ], ['class' => 'btn btn-default', 'data-original-title' => $crudTitles['create'].' bài tập']);
            }

            if (\Yii::$app->user->can('Course.Create')) {
                echo Html::a('<i class="ion-android-add"></i> '.$crudTitles['create'].' Bài giảng', [
                    '/course/lesson/create',
                    'courseId' => $params['courseId'],
                    'sectionId' => $sectionId,
                ], ['class' => 'btn btn-default', 'data-original-title' => $crudTitles['create'].' Bài giảng']);
            }
            ?>
        </div>
        <div class="kv-detail-crumbs">
            <span class="kv-crumb-active">Danh sách bài học</span>
        </div>
    </div>
    <?php Pjax::begin(['timeout' => Yii::$app->params['timeOutPjax'], 'id' => 'idPjaxReload']) ?>
        <?=
            GridView::widget([
                'dataProvider' => $dataProvider,
                //'filterModel' => $searchModel,
                'id' => 'lesson-grid',
                'options' => [
                    'data' => [
                        'sortable-widget' => 1,
                        'sortable-url' => Url::toRoute(['/course/lesson/sorting']),
                    ],
                ],
                'rowOptions' => function ($model, $key, $index, $grid) {
                    return ['data-sortable-id' => $model->id];
                },
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    [
                        'class' => SortableColumn::className(),
                    ],
                    [
                        'attribute' => 'id',
                        'options' => ['class' => 'col-xs-2'],
                    ],
                    [
                        'attribute' => 'type',
                        'value' => function ($model) {
                            return $model->typeText;
                        },
                    ],
                    [
                        'attribute' => 'name',
                        'value' => function ($model) {
                            if ($model->type === CourseLesson::TYPE_QUIZ) {
                                return $model->quiz->name;
                            } else {
                                return $model->name;
                            }
                        }
                    ],
                    [
                        'attribute' => 'status',
                        'value' => function ($model) {
                            return $model->getStatusButton(Url::to(['/course/lesson/change-status', 'id' => $model->id]));
                        },
                        'format' => 'raw',
                        'filter' => Select2::widget([
                            'model' => $searchModel,
                            'attribute' => 'status',
                            'data' => CourseLessonSearch::listStatus(),
                            'options' => ['placeholder' => $crudTitles['prompt']],
                            'pluginOptions' => [
                                'allowClear' => true,
                            ],
                            'hideSearch' => true,
                        ]),
                    ],
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'controller' => 'lesson',
                        'template' => '{view} {update} {delete}',
                        'buttons' => [
                            'delete' => function ($url, $model, $key) {
                                $url = Url::toRoute([
                                    '/course/lesson/delete',
                                    'id' => $model->id,
                                    'redirectUrl' => Url::toRoute(['/course/view/lesson', 'id' => $model->course_id, 'sectionId' => $model->section_id]),
                                ]);

                                $options = array_merge([
                                    'title' => Yii::t('yii', 'Delete'),
                                    'aria-label' => Yii::t('yii', 'Delete'),
                                    'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
                                    'data-method' => 'post',
                                ]);

                                return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, $options);
                            },
                            'update' => function ($url, $model, $key) {
                                if ($model->type === CourseLesson::TYPE_QUIZ) {
                                    $url = ['/course/quiz/update', 'id' => $model->quiz_id];
                                }

                                $options = [
                                    'title' => Yii::t('yii', 'Update'),
                                    'aria-label' => Yii::t('yii', 'Update'),
                                    'data-pjax' => '0',
                                ];

                                return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, $options);
                            },
                        ],
                    ],
                ],
        ]);
        ?>
    <?php Pjax::end() ?>
</div>
<?php
$script = "
    ;(function($, window, document, undefined){
        $(document).ready(function(){
            $('input.status-toggle').bootstrapToggle();
        });
    })(window.jQuery || window.Zepto, window, document);";
?>
<?php
$this->registerJs($script, View::POS_END, 'lesson-js');
?>