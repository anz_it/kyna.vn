<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\JsExpression;
use yii\helpers\Url;
use app\components\MetaFieldType;
use common\widgets\upload\Upload;
use kartik\select2\Select2;
use kyna\user\models\Profile;
use kyna\user\models\User;
/* @var $this yii\web\View */
/* @var $model  kyna\user\models\UserFollowTeacher */
/* @var $form yii\widgets\ActiveForm */

$crudTitles = Yii::$app->params['crudTitles'];
?>
<div class="beuser-form">
    <?php $form = ActiveForm::begin([
        'options' => ['enctype' => 'multipart/form-data', 'autocomplete' => 'off']
    ]); ?>

    <?php
    echo $form->field($model, 'user_follow')->widget(Select2::classname(), [
        'initValueText' =>  empty($model->getUserFollow()) ? '' : User::findOne(['id'=>$model->getUserFollow()])->email, // set the initial display text
        'maintainOrder' => false,
        'options' => ['placeholder' => 'Chọn người quản lý', 'multiple' => false],
        'pluginOptions' => [
            'allowClear' => false,
            'tags' => true,
            'tokenSeparators' => [','],
            'minimumInputLength' => 1,
            'language' => [
                'errorLoading' => new JsExpression("function () { return 'Chờ nhận kết quả...'; }"),
            ],
            'ajax' => [
                'url' => Url::toRoute(['/course/relation-teacher-income/get-user-relation']),
                'dataType' => 'json',
                'data' => new JsExpression('function(params) { return {q:params.term}; }'),
            ],
            'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
            'templateResult' => new JsExpression('function(res) { return res.text; }'),
            'templateSelection' => new JsExpression('function (res) { return res.text; }'),
        ],
    ]);
    ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? $crudTitles['create'] : $crudTitles['update'], ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::a('<< Back', ['/course/teacher/index'], ['class' => 'btn btn-primary']) ?>
        <?php if(!$model->isNewRecord):?>

            <?= Html::a(Yii::t('app', 'Delete'), ['/course/relation-teacher-income/delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                ],
            ]) ?>
        <?php endif;?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
