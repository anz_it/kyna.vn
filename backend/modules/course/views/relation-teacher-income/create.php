<?php

use yii\helpers\Html;
use \kyna\user\models\Profile;

/* @var $this yii\web\View */
/* @var $model kyna\user\models\UserFollowTeacher */

$this->title = 'Người quản lý cho giáo viên :' . Profile::findOne(['user_id'=>$model->getUserTeacher()])->getName();
$this->params['breadcrumbs'][] = ['label' => 'Teacher', 'url' => ['/course/teacher/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="beuser-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
