<?php

use yii\helpers\Html;
use yii\helpers\StringHelper;
use app\modules\course\controllers\DefaultController;

$this->title = Yii::$app->params['crudTitles']['create'] . " " . $this->context->mainTitle;

$backUrl = ['/course/view/lesson', 'id' => $model->course_id];

$this->params['breadcrumbs'][] = ['label' => 'Khóa học', 'url' => ['/couse/default/index']];
$this->params['breadcrumbs'][] = ['label' => StringHelper::truncateWords($model->course->name, 6), 'url' => ['/course/view/index','id' => $model->course_id]];
$this->params['breadcrumbs'][] = ['label' => $this->context->mainTitle, 'url' => $backUrl];

if (!empty($model->section)) {
    $backUrl = ['/course/view/lesson', 'id' => $model->course_id, 'sectionId' => $model->section_id];
    
    $this->params['breadcrumbs'][] = ['label' => $model->section->name, 'url' => $backUrl];
}

$this->params['breadcrumbs'][] = $this->title;
?>
<div class="course-lesson-create">
    <?= $this->render('_buttons', ['model' => $model, 'backUrl' => $backUrl]) ?>
    
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
