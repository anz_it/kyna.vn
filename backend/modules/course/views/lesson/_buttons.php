<?php

use yii\helpers\Html;

$crudTitles = Yii::$app->params['crudTitles'];
$crudButtonIcons = Yii::$app->params['crudButtonIcons'];

?>

<p class="pull-right">
    <?= 
        Html::a(
        $crudButtonIcons['back'] . " " . $crudTitles['back'],
        $backUrl
        , ['class' => 'btn btn-info', 'icon' => 'glyphicon glyphicon-backward']) 
    ?>
</p>
<div class="clearfix"></div>