<?php

use yii\helpers\StringHelper;

$crudTitles = Yii::$app->params['crudTitles'];

$this->title = $crudTitles['update'] . ' ' . StringHelper::truncateWords($model->name, 10);
$this->params['breadcrumbs'][] = ['label' => 'Khóa học', 'url' => ['/couse/default/index']];
$this->params['breadcrumbs'][] = ['label' => StringHelper::truncateWords($model->course->name, 6), 'url' => ['/course/view/index', 'id' => $model->course_id]];
$this->params['breadcrumbs'][] = ['label' => $this->context->mainTitle, 'url' => ['/course/view/lesson', 'id' => $model->course_id, 'sectionId' => $model->section_id]];
$this->params['breadcrumbs'][] = StringHelper::truncateWords($model->name, 6);
$this->params['breadcrumbs'][] = $crudTitles['update'];

$backUrl = ['/course/view/lesson', 'id' => $model->course_id, 'sectionId' => $model->section_id];
?>
<div class="course-lesson-update">
    <?= $this->render('_buttons', ['model' => $model, 'backUrl' => $backUrl]) ?>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
