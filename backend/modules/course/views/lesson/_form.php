<?php

use yii\web\View;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\Select2;
use kartik\tree\TreeViewInput;
use kyna\course\models\CourseLesson;
use kyna\course\models\CourseSection;
use kyna\videomanager\widgets\videochooser\VideoChooser;

use common\widgets\tinymce\TinyMce;

$crudTitles = Yii::$app->params['crudTitles'];

$types = CourseLesson::getTypes();
unset($types[CourseLesson::TYPE_QUIZ]);
?>

<div class="course-lesson-form">
    <?php $form = ActiveForm::begin(['enableClientValidation' => false]); ?>

    <?php /*
    <?= $form->field($model, 'section_id')->widget(TreeViewInput::classname(), [
        'query' => CourseSection::find()->where(['course_id' => $model->course_id])->addOrderBy('root, lft'),
        'headingOptions' => ['label' => 'Cấu trúc khóa học'],
        'rootOptions' => ['label' => $model->course->name],
        'fontAwesome' => true,
        'asDropdown' => true,
        'multiple' => false,
        'options' => ['disabled' => false],
        'dropdownConfig' => [
            'input' => [
                'placeholder' => $crudTitles['prompt'],
            ],
        ],
    ]) ?>
    */ ?>

    <div class="row">
        <div class="col-sm-9 col-md-10"><?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?></div>
        <div class="col-sm-3 col-md-2"><?= $form->field($model, 'day_can_learn')->input('number') ?></div>
    </div>

    <?= $form->field($model, 'type')->radioList($types)->label(false) ?>

    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'coefficient')->textInput() ?>
        </div>

        <div class="col-md-4">
            <?= $form->field($model, 'percent_can_pass')->textInput() ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'video_type')->widget(Select2::classname(), [
                'data' => CourseLesson::listVideoType(),
                'hideSearch' => true,
                'options' => ['placeholder' => $crudTitles['prompt']],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]) ?>
        </div>
    </div>


    <div class="type-option" id="type-<?= CourseLesson::TYPE_VIDEO ?>" style="display: <?= ($model->type == CourseLesson::TYPE_VIDEO ? 'block' : 'none') ?>">
        <?= $form->field($model, 'video_link')->widget(VideoChooser::className(), [
            'chooserUrl' => Url::to(['/course/video', 'f' => $model->course_id]),
            'previewUrl' => Url::to('/course/video/preview'),
            'lesson' => $model
        ]) ?>
        <!--<?= $form->field($model, 'video_link')->textInput() ?>-->
    </div>

    <div class="type-option" id="type-<?= CourseLesson::TYPE_CONTENT ?>" style="display: <?= ($model->type == CourseLesson::TYPE_CONTENT ? 'block' : 'none') ?>">
        <?= $form->field($model, 'content')->widget(TinyMce::className(), [
                'layout' => 'full',
            ]) ?>
    </div>

    <?= $form->field($model, 'note')->widget(TinyMce::className()) ?>

    <?= $form->field($model, 'status')->widget(Select2::classname(), [
            'data' => CourseLesson::listStatus(),
            'hideSearch' => true,
            'options' => ['placeholder' => $crudTitles['prompt']],
            'pluginOptions' => [
                'allowClear' => true,
            ],
        ]) ?>




    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? $crudTitles['create'] : $crudTitles['update'], ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::a($crudTitles['cancel'], ['/course/view/lesson',
                'id' => $model->course_id,
                'sectionId' => $model->section_id,
            ], ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>

<?php
$script = "
    ;(function($, window, document, undefined) {
        $(document).ready(function(){
            $('body').on('change', 'input[name=\'CourseLesson[type]\']', function () {
                $('.type-option').removeClass('show');
                $('.type-option').addClass('hide');

                var selectedEle = $('#type-' + $(this).val());
                selectedEle.removeClass('hide');
                selectedEle.addClass('show');
            });
        });
    })(window.jQuery || window.Zepto, window, document);
    ";

$this->registerJs($script, View::POS_END, 'lesson-type-change');
?>
