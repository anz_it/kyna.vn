<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\StringHelper;
use kyna\course\models\CourseLesson;

$this->title = StringHelper::truncateWords($model->name, 10);
$this->params['breadcrumbs'][] = ['label' => 'Khóa học', 'url' => ['/couse/default/index']];
$this->params['breadcrumbs'][] = ['label' => StringHelper::truncateWords($model->course->name, 6), 'url' => ['/course/view/index', 'id' => $model->course_id]];
$this->params['breadcrumbs'][] = ['label' => $this->context->mainTitle, 'url' => ['/course/view/lesson', 'id' => $model->course_id]];
$this->params['breadcrumbs'][] = StringHelper::truncateWords($this->title, 6);

$crudTitles = Yii::$app->params['crudTitles'];

$seperateCols = [
    'id',
    'course.name:text:Khóa học',
    'section.name:text:Section',
    'name',
    'day_can_learn',
];
switch ($model->type) {
    case CourseLesson::TYPE_CONTENT:
        $seperateCols[] = 'content:html';
        break;
    
    case CourseLesson::TYPE_QUIZ:
        $seperateCols[] = 'quiz.name:text:Tên Quiz';
        break;
    
    case CourseLesson::TYPE_VIDEO:
        $seperateCols[] = 'video_link';
        break;
}
$seperateCols = array_merge($seperateCols, [
    'note:html',
    'statusText:text:Trạng thái',
    'created_time:datetime',
    'updated_time:datetime'
]);
?>
<div class="course-lesson-view">

    <p>
        <?= Html::a("<span class='glyphicon glyphicon-backward'></span> " . $crudTitles['back'], ['/course/view/lesson', 'id' => $model->course_id, 'sectionId' => $model->section_id], ['class' => 'btn btn-info', 'icon' => 'glyphicon glyphicon-backward']) ?>
        <?= Html::a($crudTitles['update'], ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a($crudTitles['delete'], ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => $seperateCols,
    ]) ?>

</div>
