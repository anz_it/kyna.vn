<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model kyna\course\models\search\CourseLessonSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="course-lesson-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'course_id') ?>

    <?= $form->field($model, 'section_id') ?>

    <?= $form->field($model, 'name') ?>

    <?= $form->field($model, 'day_can_learn') ?>

    <?php // echo $form->field($model, 'note') ?>

    <?php // echo $form->field($model, 'content') ?>

    <?php // echo $form->field($model, 'quiz_id') ?>

    <?php // echo $form->field($model, 'prev_lession_id') ?>

    <?php // echo $form->field($model, 'order') ?>

    <?php // echo $form->field($model, 'status') ?>

    <?php // echo $form->field($model, 'is_deleted')->checkbox() ?>

    <?php // echo $form->field($model, 'created_time') ?>

    <?php // echo $form->field($model, 'updated_time') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
