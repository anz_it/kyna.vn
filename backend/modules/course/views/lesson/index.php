<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\components\SortableColumn;

$crudTitles = Yii::$app->params['crudTitles'];

/* @var $this yii\web\View */
/* @var $searchModel kyna\course\models\search\CourseLessonSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::$app->controller->mainTitle;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-xs-12">
        <div class="course-lesson-index">
            <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

            <p>
                <?= Html::a($crudTitles['create'], ['create'], ['class' => 'btn btn-success']) ?>
            </p>

            <div class="box">
                <?=
                GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'options' => [
                        'data' => [
                            'sortable-widget' => 1,
                            'sortable-url' => \yii\helpers\Url::toRoute(['sorting']),
                        ]
                    ],
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],
                        [
                            'class' => SortableColumn::className(),
                        ],
                        [
                            'attribute' => 'id',
                            'options' => ['class' => 'col-xs-2']
                        ],
                        'course_id',
                        'section_id',
                        'name',
                        'day_can_learn',
                        // 'note:ntext',
                        // 'content:ntext',
                        // 'quiz_id',
                        // 'prev_lession_id',
                        // 'order',
                        // 'status',
                        // 'is_deleted:boolean',
                        // 'created_time:datetime',
                        // 'updated_time:datetime',
                        ['class' => 'yii\grid\ActionColumn'],
                    ],
                ]);
                ?>
            </div>
        </div>
    </div>
</div>