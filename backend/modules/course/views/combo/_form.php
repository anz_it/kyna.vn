<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\Select2;
use yii\web\JsExpression;
use yii\helpers\Url;

use kyna\course_combo\models\CourseCombo;
use app\components\MetaFieldType;
use common\widgets\upload\Upload;

$crudTitles = Yii::$app->params['crudTitles'];
?>

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <div class="row">
        <?= $form->field($model, 'name', ['options' => ['class' => 'col-xs-6']])->textInput([
            'maxlength' => true,
            'onchange' => "$('#course-slug').val(convertToSlug($(this).val()))",
            ]) ?>

        <?= $form->field($model, 'short_name', ['options' => ['class' => 'col-xs-6']])->textInput(['maxlength' => true, 'onchange' => "$('#course-slug').val(convertToSlug($(this).val()))",]) ?>
    </div>

    <div class="row">
    <?= $form->field($model, 'slug', ['options' => ['class' => 'col-xs-6']])->textInput(['maxlength' => true, 'id' => 'course-slug']) ?>

    <?= $form->field($model, 'status', ['options' => ['class' => 'col-xs-6']])->widget(Select2::classname(), [
            'data' => CourseCombo::listStatus(),
            'hideSearch' => true,
            'options' => ['placeholder' => $crudTitles['prompt']],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]) ?>
    </div>

    <div class="row">
        <?= $form->field($model, 'image_url', ['options' => ['class' => 'col-xs-4']])->widget(Upload::className(), ['display' => 'image']) ?>
        <?= $form->field($model, 'video_cover_image_url', ['options' => ['class' => 'col-xs-4']])->widget(Upload::className(), ['display' => 'image']) ?>
        <?= $form->field($model, 'video_url', ['options' => ['class' => 'col-xs-4']])->textInput() ?>
    </div>

    <div class="row">
        <?= $form->field($model, 'is_hot', ['options' => ['class' => 'col-xs-1']])->checkbox() ?>

        <?= $form->field($model, 'is_new', ['options' => ['class' => 'col-xs-1']])->checkbox() ?>

        <?= $form->field($model, 'is_4kid', ['options' => ['class' => 'col-xs-2']])->checkbox() ?>

    </div>

    <?php
    foreach ($metaValueModels as $key => $metaValueModel) {
        echo MetaFieldType::render($form, $metaValueModel, "[{$key}]value", ['id' => "meta-{$key}"]);
    }
    ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? $crudTitles['create'] : $crudTitles['update'], ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::a($crudTitles['cancel'], ['index'], ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>


<?= $model->isNewRecord ? $this->render('//_partials/stringjs') : '' ?>
