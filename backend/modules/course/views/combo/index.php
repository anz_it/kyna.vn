<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\widgets\Pjax;
use kartik\widgets\Select2;

use common\components\SortableColumn;
use kyna\course_combo\models\CourseCombo;
use app\modules\course\controllers\ComboController;

$crudTitles = Yii::$app->params['crudTitles'];
$crudButtonIcons = Yii::$app->params['crudButtonIcons'];

$this->title = Yii::$app->controller->mainTitle;
$this->params['breadcrumbs'][] = $this->title;

$formatter = Yii::$app->formatter;

?>
<div class="row">
    <div class="col-xs-12">
        <div class="course-combo-index">
            <p>
                <?= Html::a($crudButtonIcons['create'] . ' ' . $crudTitles['create'], ['create'], ['class' => 'btn btn-success']) ?>
            </p>

            <div class="box">
                <?php
                $teacherDesc = empty($searchModel->teacher_id) ? '' : $searchModel->teacher->profile->name;
                Pjax::begin(['timeout' => Yii::$app->params['timeOutPjax'], 'id' => 'idPjaxReload']);
                ?>
                <?=
                GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'options' => [
                        'data' => [
                            'sortable-widget' => 1,
                            'sortable-url' => Url::toRoute(['/course/combo/sorting']),
                        ]
                    ],
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],
                        [
                            'class' => SortableColumn::className(),
                        ],
                        [
                            'attribute' => 'id',
                            'options' => ['class' => 'col-xs-1']
                        ],
                        'name',
                        [
                            'attribute' => 'teacher_id',
                            'value' => function ($model) {
                                return !empty($model->teacher) ? $model->teacher->profile->name : '';
                            },
                            'filter' => Select2::widget([
                                'model' => $searchModel,
                                'initValueText' => $teacherDesc,
                                'attribute' => 'teacher_id',
                                'options' => ['placeholder' => $crudTitles['prompt']],
                                'pluginOptions' => [
                                    'allowClear' => true,
                                    'minimumInputLength' => 3,
                                    'language' => [
                                        'errorLoading' => new JsExpression("function () { return 'Chờ nhận kết quả...'; }"),
                                    ],
                                    'ajax' => [
                                        'url' => yii\helpers\Url::toRoute(['/course/api/search-teacher']),
                                        'dataType' => 'json',
                                        'data' => new JsExpression('function(params) { return {q:params.term}; }'),
                                    ],
                                    'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                                    'templateResult' => new JsExpression('function(teacher) { return teacher.text; }'),
                                    'templateSelection' => new JsExpression('function (teacher) { return teacher.text; }'),
                                ],
                            ])
                        ],
                        [
                            'attribute' => 'image_url',
                            'format' => 'html',
                            'value' => function ($model) {
                                return !empty($model->image_url) ? Html::img($model->image_url, ['width' => '150px']) : '';
                            },
                            'filter' => false,
                            'options' => ['class' => 'col-xs-1']
                        ],
                        [
                            'attribute' => 'price',
                            'format' => 'raw',
                            'value' => function($model) use ($formatter){
                                $price = 0;
                                if(!empty($model->items)){
                                    foreach($model->items as $item){
                                        $price += $item->course->oldPrice;
                                    }
                                }
                                return $formatter->asCurrency($price);
                            }
                        ],
                        [
                            'attribute' => 'object',
                            'format' => 'html',
                            'filter' => false,
                        ],
                        [
                            'attribute' => 'benefit',
                            'format' => 'html',
                            'filter' => false,
                        ],
                        [
                            'attribute' => 'is_4kid',
                            'value' => function ($model)
                            {
                                return empty($model->is_4kid) ? 'No':'Yes';
                            },
                            'format' => 'raw',
                            'filter' => Select2::widget([
                                'model' => $searchModel,
                                'attribute' => 'is_4kid',
                                'data' => [0=>'No',1=>'Yes'],
                                'options' => ['placeholder' => $crudTitles['prompt']],
                                'pluginOptions' => [
                                    'allowClear' => true
                                ],
                                'hideSearch' => true,
                            ])
                        ],
                        [
                            'attribute' => 'status',
                            'value' => function ($model)
                            {
                                return $model->statusButton;
                            },
                            'format' => 'raw',
                            'filter' => Select2::widget([
                                'model' => $searchModel,
                                'attribute' => 'status',
                                'data' => CourseCombo::listStatus(false),
                                'options' => ['placeholder' => $crudTitles['prompt']],
                                'pluginOptions' => [
                                    'allowClear' => true
                                ],
                                'hideSearch' => true,
                            ])
                        ],
                        [
                            'class' => 'app\components\ActionColumnCustom',
                            'template' => '{view} {list-courses} {update}',
                            'buttons' => [
                                'list-courses' => function ($url, $model, $key) {
                                    $url = Url::toRoute(['/course/combo/update', 'id' => $model->id, 'tab' => ComboController::TAB_LIST_COURSES]);
                                    
                                    $options = [
                                        'title' => 'Danh sách khóa học',
                                        'data-pjax' => '0',
                                    ];
                                    
                                    return Html::a('<span class="fa fa-list"></span>', $url, $options);
                                }
                            ]
                        ],
                    ],
                ]);
                ?>
                <?php Pjax::end(); ?>
            </div>
        </div>
    </div>
</div>