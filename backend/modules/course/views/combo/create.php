<?php

use yii\helpers\Html;
use yii\bootstrap\Tabs;

$this->title = Yii::$app->params['crudTitles']['create'];
$this->params['breadcrumbs'][] = ['label' => Yii::$app->controller->mainTitle, 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="course-combo-create">

    <?=
    Tabs::widget([
        'items' => $this->context->getTabItems(['model' => $model, 'metaValueModels' => $metaValueModels], Yii::$app->request->get('tab'))
    ]);
    ?>

</div>
