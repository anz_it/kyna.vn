<?php

use yii\helpers\Html;
use yii\bootstrap\Tabs;

$crudTitles = Yii::$app->params['crudTitles'];

$this->title = $crudTitles['update'] . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::$app->controller->mainTitle, 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = $crudTitles['update'];
?>
<div class="course-combo-update">

    <?=
    Tabs::widget([
        'items' => $this->context->getTabItems(['model' => $model, 'itemModel' => $itemModel,  'metaValueModels' => $metaValueModels], Yii::$app->request->get('tab'))
    ]);
    ?>

</div>
