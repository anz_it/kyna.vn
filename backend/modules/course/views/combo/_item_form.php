<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\JsExpression;
use kartik\select2\Select2;
use kyna\course\models\Course;

$crudTitles = Yii::$app->params['crudTitles'];
?>

<div class="course-answer-form">
    <?php $form = ActiveForm::begin(); ?>
        <?php
            echo $form->field($model, 'course_id')->widget(Select2::classname(), [
                'initValueText' =>  empty($model->course_id) ? '' : $model->course->name, // set the initial display text
                'options' => ['placeholder' => $crudTitles['prompt']],
                'pluginOptions' => [
                    'allowClear' => true,
                    'minimumInputLength' => 3,
                    'language' => [
                        'errorLoading' => new JsExpression("function () { return 'Chờ nhận kết quả...'; }"),
                    ],
                    'ajax' => [
                        'url' => yii\helpers\Url::toRoute(['/course/api/search', 'auto' => true, 'include_hide' => true, 'type' => [Course::TYPE_VIDEO, Course::TYPE_SOFTWARE]]),
                        'dataType' => 'json',
                        'data' => new JsExpression('function(params) { return {q:params.term}; }'),
                    ],
                    'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                    'templateResult' => new JsExpression('function(teacher) { return teacher.text; }'),
                    'templateSelection' => new JsExpression('function (teacher) { return teacher.text; }'),
                ],
            ]);
        ?>

        <?= $form->field($model, 'price') ?>

        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? $crudTitles['create'] : $crudTitles['update'], ['class' => 'btn btn-primary']) ?>
            <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
        </div>

    <?php ActiveForm::end(); ?>

</div>
