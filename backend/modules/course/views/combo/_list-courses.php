<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;

use common\components\SortableColumn;
use kyna\course_combo\models\CourseComboItem;
use app\modules\course\controllers\ComboController;

$searchModel = new CourseComboItem();
$searchModel->course_combo_id = $model->id;
$dataProvider = $searchModel->search(Yii::$app->request->queryParams);

$crudTitles = Yii::$app->params['crudTitles'];
?>

<?php if ($model->isNewRecord) : ?>
    <div class="alert alert-warning" >
        Hãy nhập thông tin và lưu thông tin Combo khóa học trước.
    </div>
<?php else: ?>
    <?php echo $this->render('_item_form', ['model' => $itemModel, 'combo' => $model]); ?>
    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => false,
        'options' => [
            'data' => [
                'sortable-widget' => 1,
                'sortable-url' => Url::toRoute(['/course/combo/item-sorting']),
            ]
        ],
        'rowOptions' => function ($model, $key, $index, $grid) {
            return ['data-sortable-id' => $model->id];
        },
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'class' => SortableColumn::className(),
            ],
            [
                'attribute' => 'course_id',
                'value' => function ($model) {
                    return $model->course->name;
                }
            ],
            'course.price:currency',
            'price:currency',
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update} {delete}',
                'controller' => 'question',
                'buttons' => [
                    'update' => function ($url, $model, $key) {
                        $url = Url::toRoute(['/course/combo/update',
                            'id' => $model->course_combo_id,
                            'itemId' => $model->id,
                            'tab' => ComboController::TAB_LIST_COURSES
                        ]);

                        $options = [
                            'title' => Yii::t('yii', 'Update'),
                            'aria-label' => Yii::t('yii', 'Update'),
                            'data-pjax' => '0',
                        ];
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, $options);
                    },
                    'delete' => function ($url, $model, $key) {
                        $url = Url::toRoute([
                            '/course/combo/delete-item', 
                            'id' => $model->id
                        ]);

                        $options = array_merge([
                            'title' => Yii::t('yii', 'Delete'),
                            'aria-label' => Yii::t('yii', 'Delete'),
                            'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
                            'data-method' => 'post',
                            'data-pjax' => '0',
                        ]);
                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, $options);
                    },
                ]
            ],
        ],
    ]);
    ?>
<?php endif; ?>
