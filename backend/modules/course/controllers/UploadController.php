<?php

namespace app\modules\course\controllers;

class UploadController extends \common\widgets\upload\UploadController
{

    // created for testing purpose
    public $uploadParamName = 'demo-upload';

    public function actionDemo()
    {
        return $this->render('demo');
    }

    public function getFileName($file)
    {
        return md5($file->baseName . time()) . '.' . $file->extension;
    }

}
