<?php

namespace app\modules\course\controllers;

use kyna\course\models\CourseOpinions;
use kyna\course\models\search\CourseOpinionSearch;
use Yii;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

use kotchuprik\sortable\actions\Sorting;

use kyna\base\models\MetaField;
use kyna\course\models\CourseLesson;
use kyna\course\models\Quiz;
use kyna\course\models\QuizMeta;
use kyna\course\models\search\QuizSearch;
use yii\widgets\Pjax;
use yii\web\UploadedFile;
use app\modules\course\lib\ImageUpload;
use common\lib\CDNImage;
/**
 * QuizController implements the CRUD actions for Quiz model.
 */
class OpinionController extends \app\components\controllers\Controller
{
    public $mainTitle = 'Khóa học';


    /**
     * Creates a new Opinion model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($course_id)
    {
        $model = new CourseOpinions();
        $model->loadDefaultValues();
        $model->course_id = $course_id;
        $post = Yii::$app->request->post();
        if ($model->load($post)) {
            // name of quiz = name of lesson\
            $model->course_id = $course_id;
            if ($model->save()) {
                if ($image_url = $this->_uploadImage($model, 'avatar_url')) {
                    $model->avatar_url = $image_url;
                    $model->save();
                }
                return $this->redirect(['/course/view/opinion', 'id' => $course_id]);
            }
        }

        return $this->render('create', [
            'model' => $model,

        ]);

    }

    /*
     * Update OpinonCourse model
     */

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $post = Yii::$app->request->post();
        if ($model->load($post)) {
            if ($image_url = $this->_uploadImage($model, 'avatar_url')) {
                $model->avatar_url = $image_url;
            }
            if ($model->save()) {
                Yii::$app->session->setFlash('warning', 'Cập nhật thành công');
                return $this->redirect(['update', 'id' => $id]);
            }
        }
        return $this->render('update', [
            'model' => $model,

        ]);
    }

    /*
     * Delete OpinionCourse model
     *
     */
    public function actionDelete($id, $redirectUrl = false)
    {
        $model = $this->findModel($id);
        $course_id = $model->course_id;
        $model->delete();
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $ret = [
                'success' => true,
                'message' => 'Đã xóa thành công',
            ];
            return $ret;
        } else {
            Yii::$app->session->setFlash('warning', 'Đã xóa thành công');
            return $this->redirect(['/course/view/opinion', 'id' => $course_id]);
        }
    }

    protected function findModel($id)
    {
        if (($model = CourseOpinions::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    private function _uploadImage($model, $attribute) {
        $files = UploadedFile::getInstances($model, $attribute);
        if (!sizeof($files)) {
            $oldAttributes = $model->oldAttributes;

            return isset($oldAttributes[$attribute]) ? $oldAttributes[$attribute] : false;
        }

        $file = $files[0];

        $cdnImage = new CDNImage($model, $attribute);
        $result = $cdnImage->upload($file);
        return $result;
    }

    public function actions()
    {
        return [
            'sorting' => [
                'class' => \kotchuprik\sortable\actions\Sorting::className(),
                'query' => CourseOpinions::find(),
            ],
        ];
    }

}