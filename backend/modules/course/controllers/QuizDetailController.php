<?php

namespace app\modules\course\controllers;

use kyna\course\models\QuizQuestion;
use Yii;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use kotchuprik\sortable\actions\Sorting;
use kyna\course\models\QuizDetail;
use kyna\course\models\search\QuizDetailSearch;
use app\modules\course\controllers\QuizController;

/**
 * QuizQuestionController implements the CRUD actions for QuizQuestion model.
 */
class QuizDetailController extends \app\components\controllers\Controller
{

    public $mainTitle = 'Câu hỏi';
    
    public function actions()
    {
        $actions = parent::actions();
        
        $actions['sorting'] = [
            'class' => Sorting::className(),
            'query' => QuizDetail::find()
        ];
        
        return $actions;
    }

    /**
     * Creates a new QuizQuestion model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionCreate($quizId, $questionId)
    {
        $model = new QuizDetail();

        $question = QuizQuestion::findOne($questionId);
        if ($question == null) {
            throw new BadRequestHttpException("ID câu hỏi không tồn tại");
        }

        $model->quiz_question_id = $questionId;
        $model->quiz_id = $quizId;

        if ($model->save()) {

            foreach ($question->children as $child) {
                $childDetail = new QuizDetail();
                $childDetail->quiz_id = $quizId;
                $childDetail->quiz_question_id = $child->id;
                $childDetail->parent_id = $model->id;
                $childDetail->save();

            }

            Yii::$app->session->setFlash('success', 'Thêm câu hỏi vào Quiz thành công.');
            
            return $this->redirect([
                '/course/quiz/update', 
                'id' => $model->quiz_id, 
                'tab' => QuizController::TAB_QUESTION
            ]);
        }
    }

    /**
     * Updates an existing QuizQuestion model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        if ($model->quizQuestion != null && ($model->quizQuestion->type == QuizQuestion::TYPE_FILL_INTO_DOTS || $model->quizQuestion->type == QuizQuestion::TYPE_CONNECT_ANSWER)) {
            foreach ($model->quizQuestion->children as $child) {
                $quizDetail = QuizDetail::find()->where(['quiz_id' => $model->quiz_id, 'quiz_question_id' => $child->id])->all();
                foreach ($quizDetail as $detail)
                    $detail->delete();
            }
        }
        $model->delete();

        return $this->redirect(['/course/quiz/update', 'id' => $model->quiz_id, 'tab' => QuizController::TAB_QUESTION]);
    }

    protected function findModel($id)
    {
        if (($model = QuizDetail::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
