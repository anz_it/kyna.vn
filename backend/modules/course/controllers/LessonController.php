<?php

namespace app\modules\course\controllers;

use Yii;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use kyna\course\models\CourseLesson;
use kyna\course\models\search\CourseLessonSearch;
use kotchuprik\sortable\actions\Sorting;

/**
 * LessonController implements the CRUD actions for CourseLesson model.
 */
class LessonController extends \app\components\controllers\Controller
{

    public $mainTitle = 'Bài học';
    
    public function actions()
    {
        $actions = parent::actions();
        
        $actions['sorting'] = [
            'class' => Sorting::className(),
            'query' => CourseLesson::find()
        ];
        
        return $actions;
    }

    /**
     * Lists all CourseLesson models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CourseLessonSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new CourseLesson model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($courseId, $sectionId = 0)
    {
        $model = new CourseLesson(['type' => CourseLesson::TYPE_VIDEO]);

        $model->loadDefaultValues();
        $model->course_id = $courseId;
        if (!empty($sectionId)) {
            $model->section_id = $sectionId;
        }
        
        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                Yii::$app->session->setFlash('success', 'Thêm thành công');
                return $this->redirect(['/course/lesson/update', 'id' => $model->id]);
            }
        }
        
        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing CourseLesson model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                Yii::$app->session->setFlash('success', 'Cập nhật thành công');
            }
        }
        
        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Finds the CourseLesson model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CourseLesson the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CourseLesson::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
