<?php

namespace app\modules\course\controllers;

use Yii;
use kyna\course\models\TeacherContracts;
use kyna\course\models\search\TeacherContractsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TeacherContractsController implements the CRUD actions for TeacherContracts model.
 */
class TeacherContractsController extends \app\components\controllers\Controller
{
    public $mainTitle = 'Hợp đồng Giảng viên';
    public $role = 'Teacher';
    public $teacherRole = 'Teacher';
    public $relationRole = 'Relation';

    /**
     * Lists all TeacherContracts models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TeacherContractsSearch();

        $params = Yii::$app->request->queryParams;
        if (isset($params['user_id'])) {
            $searchModel->user_id = $params['user_id'];
        }
        $dataProvider = $searchModel->search($params);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single TeacherContracts model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new TeacherContracts model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new TeacherContracts();
        $params = Yii::$app->request->queryParams;
        if (isset($params['user_id']) && intval($params['user_id']) > 0) {
            $model->user_id = $params['user_id'];
            $model->isDisableTeacher = true;
        }
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            // update meta relations
            $data = Yii::$app->request->post('TeacherContracts');
            if (isset($data['user_take_care'])) {
                $model->updateMetaField('relation', $data['user_take_care']);
            }
            Yii::$app->session->setFlash('success', "Thêm thành công!");
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing TeacherContracts model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $params = Yii::$app->request->queryParams;
        if (isset($params['user_id']) && intval($params['user_id']) > 0) {
            $model->user_id = $params['user_id'];
            $model->isDisableTeacher = true;
        }
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            // update meta relations
            $data = Yii::$app->request->post('TeacherContracts');
            if (isset($data['user_take_care'])) {
                $model->updateMetaField('relation', $data['user_take_care']);
            }
            Yii::$app->session->setFlash('success', "Cập nhật thành công!");
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Finds the TeacherContracts model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return TeacherContracts the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = TeacherContracts::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
