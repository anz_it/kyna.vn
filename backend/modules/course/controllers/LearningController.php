<?php

namespace app\modules\course\controllers;

use yii\filters\VerbFilter;
use kyna\user\models\UserCourse;

class LearningController extends \app\components\controllers\Controller
{
    
    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'active' => ['post'],
                ],
            ],
        ]);
    }
    
    public function actionActive($id)
    {
        $mUserCourse = $this->findModel($id);
        
        if (UserCourse::active($mUserCourse->user_id, $mUserCourse->course_id)) {
            $this->redirect(['/course/view/student', 'id' => $mUserCourse->course_id]);
        }
    }

    public function actionQuick($id)
    {
        $mUserCourse = $this->findModel($id);

        if (UserCourse::quick($mUserCourse->user_id, $mUserCourse->course_id)) {
            $this->redirect(['/course/view/student', 'id' => $mUserCourse->course_id]);
        }
    }

    public function actionUnQuick($id)
    {
        $mUserCourse = $this->findModel($id);

        if (UserCourse::unQuick($mUserCourse->user_id, $mUserCourse->course_id)) {
            $this->redirect(['/course/view/student', 'id' => $mUserCourse->course_id]);
        }
    }
    
    public function actionGraduate($id)
    {
        $mUserCourse = $this->findModel($id);
        
        if (UserCourse::graduate($mUserCourse->user_id, $mUserCourse->course_id)) {
            $this->redirect(['/course/view/student', 'id' => $mUserCourse->course_id]);
        }
    }
    
    public function actionReset($id)
    {
        $mUserCourse = $this->findModel($id);
        
        if (UserCourse::reset($mUserCourse->user_id, $mUserCourse->course_id)) {
            $this->redirect(['/course/view/student', 'id' => $mUserCourse->course_id]);
        }
    }
    
    protected function findModel($id)
    {
        if (($model = UserCourse::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}

