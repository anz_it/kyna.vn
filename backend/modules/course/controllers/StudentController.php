<?php

namespace app\modules\course\controllers;

use Yii;
use yii\web\Controller;
use yii\web\UploadedFile;
use yii\web\NotFoundHttpException;
use yii\filters\AccessControl;
use yii\helpers\Html;

use kyna\user\models\User;
use kyna\user\models\UserCourse;
use kyna\user\models\search\UserCourseSearch;

use app\modules\course\models\forms\StudentImportForm;

/**
 * StudentController implements the CRUD actions for UserCourse model.
 */
class StudentController extends Controller
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->user->can('Student.View');
                        }
                    ],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $searchModel = new UserCourseSearch();
        $dataProvider = $searchModel->searchStudent(Yii::$app->request->queryParams);

        $importForm = new StudentImportForm();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'importForm' => $importForm
        ]);
    }

    public function actionCreate()
    {
        $model = new UserCourse();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    public function actionImport()
    {
        $model = new StudentImportForm();

        if ($model->load(Yii::$app->request->post())) {
            $model->file = UploadedFile::getInstance($model, 'file');
            if ($model->validate()) {
                $total = 0;
                $inputFilePath = $model->file->tempName;

                try {
                    $inputFileType = \PHPExcel_IOFactory::identify($inputFilePath);
                    $objReader = \PHPExcel_IOFactory::createReader($inputFileType);
                    $objExcel = $objReader->load($inputFilePath);
                } catch (Exception $ex) {
                    Yii::$app->session->setFlash('error', $ex->getMessage());
                    return $this->redirect(['index']);
                }

                $sheet = $objExcel->getSheet(0);
                $highestRow = $sheet->getHighestRow();
                $highestColumn = $sheet->getHighestColumn();
                $errors = [];
                $users = [];

                $rowData = $sheet->rangeToArray('A' . 1 . ':' . $highestColumn . 1, NULL, NULL);
                $courseIds = explode(',', $rowData[0][6]);

                for ($row = 2; $row <= $highestRow; $row++) {
                    $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, NULL);

                    $email = trim($rowData[0][2]);
                    $user = User::find()->where(['email' => $email])->one();

                    if (is_null($user)) {
                        if ($user = $this->_saveExcelRow($rowData, $row, $errors, $courseIds)) {
                            $total++;
                        }
                    } else {
                        $user->scenario = 'update';

                        $this->_updateExcelRow($user, $rowData, $row, $errors, $courseIds);
                        $total++;
                    }
                    if (!is_null($user) && $user) {
                        $users[] = $user;
                    }
                }

                if (!empty($users)) {
                    $this->_exportCsv($users);
                    exit();
                }

                if ($total) {
                    Yii::$app->session->setFlash('success', "Import thành công $total học viên.");
                } else {
                    Yii::$app->session->setFlash('warning', "Không import được học viên nào.");
                }

                if (!empty($errors)) {
                    $errorTexts = [];
                    foreach ($errors as $row => $rowErrors) {
                        $errorTexts[$row] = "Row $row: " . implode(', ', $rowErrors);
                    }
                    Yii::$app->session->setFlash('warning', "Lỗi dữ liệu: <br>" . Html::ul($errorTexts));
                }

                return $this->redirect(['index']);
            }
        }

        return $this->render('upload', [
            'model' => $model,
        ]);
    }

    public function actionImportUser()
    {
        $model = new StudentImportForm();

        if ($model->load(Yii::$app->request->post())) {
            $model->file = UploadedFile::getInstance($model, 'file');
            if ($model->validate()) {
                $total = 0;
                $inputFilePath = $model->file->tempName;

                try {
                    $inputFileType = \PHPExcel_IOFactory::identify($inputFilePath);
                    $objReader = \PHPExcel_IOFactory::createReader($inputFileType);
                    $objExcel = $objReader->load($inputFilePath);
                } catch (Exception $ex) {
                    Yii::$app->session->setFlash('error', $ex->getMessage());
                    return $this->redirect(['index']);
                }
                /*Sheet 1 get user info*/
                $sheet = $objExcel->getSheet(0);
                $highestRow = $sheet->getHighestRow();
                $highestColumn = $sheet->getHighestColumn();
                $errors = [];
                /*Sheet 2 get user courses*/
                $sheet2 = $objExcel->getSheet(1);
                $highestRow2 = $sheet2->getHighestRow();
                $highestColumn2 = $sheet2->getHighestColumn();
                $rowData2 = $sheet2->rangeToArray('A' . 2 . ':' . $highestColumn2 . $highestRow2, NULL, NULL);
                $userCourses = [];
                foreach ($rowData2 as $item)
                {
                    if(!empty($item[5])) {
                        $userCourse = ['course_id' => $item[4], 'email' => $item[3]];
                        $userCourses[] = $userCourse;
                    }
                }
                /*End get user courses*/
                $users = [];
                for ($row = 2; $row <= $highestRow; $row++) {
                    $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, NULL);
                    $email = trim($rowData[0][5]);
                    $user_name =  trim($rowData[0][4]);
                    $pass = trim($rowData[0][7]);
                    $user = User::find()->where(['email' => $email])->one();
                    $user_by_username = User::find()->where(['username' => $user_name])->one();
                    $courseIds = $this->getUserCourses($userCourses, $email);

                    //add email as user_name
                    if(empty($user) && !empty($user_by_username))
                    {
                        $rowData[0][4] = $email;
                        if ($user = $this->_saveUserRow($rowData, $row, $errors, $courseIds, $user_name)) {
                            $total++;
                        }
                    }

                    if(!empty($user))
                    {
                        $user->scenario = 'update';
                        $this->_updateUserRow($user, $rowData, $row, $errors, $courseIds, $user_name);
                        $total++;
                    }
                    if (empty($user) && empty($user_by_username)) {

                        if ($user = $this->_saveUserRow($rowData, $row, $errors, $courseIds, $user_name)) {
                            $total++;
                        }
                    }
                    if(!empty($user)) {
                        if (!is_null($user) && $user) {
                            if (empty($pass)) {
                                $pass = !empty($user_name) ? trim($user_name) . '123' : Yii::$app->params['defaultPassword'];
                            }
                            $users[] = array('user' => $user, 'password' => $pass);
                        }
                    }
                }

                if (!empty($users)) {
                    $this->_exportUserCsv($users);
                    exit();
                }

                if ($total) {
                    Yii::$app->session->setFlash('success', "Import thành công $total học viên.");
                } else {
                    Yii::$app->session->setFlash('warning', "Không import được học viên nào.");
                }

                if (!empty($errors)) {
                    $errorTexts = [];
                    foreach ($errors as $row => $rowErrors) {
                        $errorTexts[$row] = "Row $row: " . implode(', ', $rowErrors);
                    }
                    Yii::$app->session->setFlash('warning', "Lỗi dữ liệu: <br>" . Html::ul($errorTexts));
                }

                return $this->redirect(['index']);
            }
        }

        return $this->render('upload', [
            'model' => $model,
        ]);
    }
    private function getUserCourses($userCourses, $email)
    {
        $ids = [];
        foreach ($userCourses as $userCourse)
        {
            if(trim($userCourse['email']) == trim($email))
                $ids[] = $userCourse['course_id'];
        }
        return $ids;
    }

    private function _saveExcelRow($rowData, $row, &$errors = [], $courseIds = [])
    {
        $model = new User();

        $model->email = trim($rowData[0][2]);
        $model->username = trim($rowData[0][3]);
        $model->password = !empty($rowData[0][4]) ? trim($rowData[0][4]) : Yii::$app->params['defaultPassword'];

        if ($model->create(false)) {
            $profile = $model->profile;
            $profile->name = trim($rowData[0][1]);
            $profile->phone_number = trim(substr($rowData[0][5], 0, 12));

            $profile->save(false);

            UserCourse::active($model->id, $courseIds);

            return $model;
        } else {
            var_dump($model->errors);die('2');
            foreach ($model->errors as $error) {
                $errors[$row][] = implode(', ', $error);
            }
        }

        return false;
    }

    private function _saveUserRow($rowData, $row, &$errors = [], $courseIds = [], $user_name)
    {
        $model = new User();

        $model->email = trim($rowData[0][5]);
        $model->username = trim($rowData[0][4]);

        if(!empty($rowData[0][7])){
            $model->password = trim($rowData[0][7]);
        }
        else {
            $model->password = !empty($user_name) ? trim($user_name) . '123' : Yii::$app->params['defaultPassword'];
        }

        if ($model->create(false)) {
            if(!empty($model->profile)) {
                $profile = $model->profile;
                if (!empty($rowData[0][1])) {
                    $profile->name = trim($rowData[0][1]);
                }
                if (!empty($rowData[0][6])) {
                    $profile->phone_number = trim($rowData[0][6]);
                }
                $profile->save(false);
            }
            UserCourse::active($model->id, $courseIds);

            return $model;
        } else {
            var_dump($model->errors);die('2');
            foreach ($model->errors as $error) {
                $errors[$row][] = implode(', ', $error);
            }
        }

        return false;
    }

    private function _updateExcelRow($model, $rowData, $row, &$errors = [], $courseIds = [])
    {
        $model->password = !empty($rowData[0][4]) ? trim($rowData[0][4]) : 'vietel123456';

        if ($model->save()) {
            UserCourse::active($model->id, $courseIds);

            return $model;
        } else {
            foreach ($model->errors as $error) {
                $errors[$row][] = implode(', ', $error);
            }
        }

        return false;
    }

    private function _updateUserRow($model, $rowData, $row, &$errors = [], $courseIds = [], $user_name)
    {
        if(!empty($rowData[0][7])){
            if(empty($model->password)){
                $model->password = trim($rowData[0][7]);
            }
        }else {
            $model->password = !empty($user_name) ? trim($user_name) . '123' : Yii::$app->params['defaultPassword'];
        }

        if ($model->save()) {
            UserCourse::active($model->id, $courseIds);

            return $model;
        } else {
            foreach ($model->errors as $error) {
                $errors[$row][] = implode(', ', $error);
            }
        }

        return false;
    }
    private function _exportUserCsv($users)
    {
        ini_set('max_execution_time', 0);

        header('Content-Type: text/csv; charset=UTF-8');
        header('Content-Disposition: attachment; filename=exported_users.csv');

        $output = fopen('php://output', 'w');
        fputs($output, "\xEF\xBB\xBF");

        fputcsv($output, [
            'Họ tên người dùng',
            'Email',
            'Username',
            'Mật khẩu',
            'Số điện thoại'
        ]);

        foreach ($users as $u) {
            if (!is_null($u)) {
                $user = $u['user'];
                $password = $u['password'];
                fputcsv($output, [$user->profile->name, $user->email, $user->username, $password, $user->profile->phone_number]);
            }
        }

        fclose($output);
    }
    private function _exportCsv($users)
    {
        ini_set('max_execution_time', 0);

        header('Content-Type: text/csv; charset=UTF-8');
        header('Content-Disposition: attachment; filename=exported_users.csv');

        $output = fopen('php://output', 'w');
        fputs($output, "\xEF\xBB\xBF");

        fputcsv($output, [
            'Họ tên người dùng',
            'Email',
            'Username',
            'Mật khẩu',
            'Số điện thoại'
        ]);

        foreach ($users as $user) {
            if (!is_null($user)) {
                fputcsv($output, [$user->profile->name, $user->email, $user->username, '', $user->profile->phone_number]);
            }
        }

        fclose($output);
    }

    protected function findModel($id)
    {
        if (($model = UserCourse::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
