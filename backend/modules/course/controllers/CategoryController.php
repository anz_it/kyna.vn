<?php

namespace app\modules\course\controllers;

use common\lib\CDNImage;
use Yii;
use yii\data\ArrayDataProvider;
use yii\web\Response;
use yii\web\UploadedFile;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

use kyna\course\models\Category;
use kyna\course\models\search\CategorySearch;
use app\modules\course\lib\ImageUpload;
use yii\db\Query;

/**
 * CategoryController implements the CRUD actions for Category model.
 */
class CategoryController extends \app\components\controllers\Controller
{

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'render' => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'view'],
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->user->can('Course.Category.View');
                        }
                    ],
                    [
                        'allow' => true,
                        'actions' => ['create', 'render'],
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->user->can('Course.Category.Create');
                        }
                    ],
                    [
                        'allow' => true,
                        'actions' => ['update', 'change-status', 'render'],
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->user->can('Course.Category.Update');
                        }
                    ],
                    [
                        'allow' => true,
                        'actions' => ['delete'],
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->user->can('Course.Category.Delete');
                        }
                    ],
                    [
                        'allow' => true,
                        'actions' => ['category-list'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all Category models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CategorySearch();

        $searchModel->parent_id = 0;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Category model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $searchModel = new CategorySearch();
        $searchModel->parent_id = $id;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('view', [
            'model' => $this->findModel($id),
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new Category model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Category();
        $model->loadDefaultValues();

        $categories = Category::find(['status' => Category::STATUS_ACTIVE])->all();

        $get = Yii::$app->request->get();
        if (!empty($get['parent_id'])) {
            $parent = Category::findOne(['id' => $get['parent_id']]);
            if (empty($parent)) {
                throw new \yii\web\HttpException(404);
            }
            $model->parent_id = $parent->id;
        }

        $post = Yii::$app->request->post();
        if ($model->load($post)) {
            if (empty($model->parent_id)) {
                $model->parent_id = 0;
            }

            $model->is_featured = $post['Category']['is_featured'];

            if ($menuIcon = $this->_uploadImage($model, 'menu_icon')) {
                $model->menu_icon = $menuIcon;
            }

            if ($homeIcon = $this->_uploadImage($model, 'home_icon')) {
                $model->home_icon = $homeIcon;
            }

            if ($images_thumbn = $this->_uploadImage($model, 'images_thumb')) {
                $model->images_thumb = $images_thumbn;
            }

            if ($model->save()) {
                return $this->redirect(['view', 'id' => !empty($model->parent_id) ? $model->parent_id : $model->id]);
            }
        }
        return $this->render('create', [
            'model' => $model,
            'categories' => $categories
        ]);
    }

    /**
     * Updates an existing Category model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $categories = Category::findAllActive();

        $post = Yii::$app->request->post();
        if ($model->load($post)) {
            if (empty($model->parent_id)) {
                $model->parent_id = 0;
            }

            $model->is_featured = $post['Category']['is_featured'];

            if ($menuIcon = $this->_uploadImage($model, 'menu_icon')) {
                $model->menu_icon = $menuIcon;
            }

            if ($homeIcon = $this->_uploadImage($model, 'home_icon')) {
                $model->home_icon = $homeIcon;
            }
            if ($images_thumbn = $this->_uploadImage($model, 'images_thumb')) {
                $model->images_thumb = $images_thumbn;
            }
            if ($model->save()) {
                return $this->redirect(['update', 'id' => !empty($model->parent_id) ? $model->parent_id : $model->id]);
            }
        }
        return $this->render('update', [
            'model' => $model,
            'categories' => $categories
        ]);
    }

    /**
     * Deletes an existing Category model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id, $redirectUrl = false)
    {
        $model = $this->findModel($id);

        $model->delete();

        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $ret = [
                'success' => true,
                'message' => 'Đã xóa thành công',
            ];
            return $ret;
        } else {
            Yii::$app->session->setFlash('warning', 'Đã xóa');
            return $this->redirect(!empty($model->parent_id) ? ['view', 'id' => $model->parent_id] : ['index']);
        }
    }

    /**
     * Finds the Category model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Category the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Category::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    private function _uploadImage($model, $attribute) {
        $files = UploadedFile::getInstances($model, $attribute);
        if (!sizeof($files)) {
            $oldAttributes = $model->oldAttributes;
            return isset($oldAttributes[$attribute]) ? $oldAttributes[$attribute] : false;
        }
        $file = $files[0];
        $cdnImage = new CDNImage($model, $attribute);
        $result = $cdnImage->upload($file);
        return $result;
    }

    public function actionRender()
    {
        $params = Yii::$app->request->getBodyParams();

        if (empty($params['categories_id'])) {
            return false;
        }
        // prepare params

        $categories_id = mb_split(',', $params['categories_id']);
        array_splice($categories_id, 0, 1);

        $categories = [];
        foreach ($categories_id as $key => $category_id) {
            $category = Category::findOne([
                "id" => $category_id,
            ]);
            if (!empty($category)) {
                $categories[] = $category;
            }
        }

        $dataProvider = new ArrayDataProvider();
        $dataProvider->setModels($categories);

        return $this->renderPartial('_form_cat_list', [
            "dataProvider" => $dataProvider,
        ]);
  	}

    public function actionCategoryList($q = null, $id = null) {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];
        if (!is_null($q)) {
            $query = new Query;
            $query->select('id, name AS text')
                ->from('categories')
                ->where(['like', 'name', $q])
                ->andWhere(['categories.status'=>1])
                ->limit(20);
            $command = $query->createCommand();
            $data = $command->queryAll();
            $out['results'] = array_values($data);
        }
        elseif ($id > 0) {
            $out['results'] = ['id' => $id, 'text' => Category::find($id)->name];
        }
        return $out;
    }
}
