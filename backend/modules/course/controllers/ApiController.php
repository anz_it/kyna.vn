<?php

namespace app\modules\course\controllers;

use common\helpers\ArrayHelper;
use kyna\course\models\Category;
use kyna\settings\models\Setting;
use Yii;
use yii\web\Response;
use yii\db\Query;

use kyna\course\models\Course;
use kyna\user\models\User;
use kyna\user\models\AuthAssignment;
use kyna\user\models\Profile;
use kyna\course\models\CourseSection;
use kyna\course\models\CourseTeachingAssistant;
use kyna\course_combo\models\CourseComboItem;

/*
 * This is api controller class for module Course
 */
class ApiController extends \app\components\controllers\Controller
{
    
    public $roleTeacher = 'Teacher';

    /**
     * @desc function search courses

     * @param string $q
     * @param bool $auto
     *
     * @return json
     */
    public function actionSearch($q = null, $auto = false)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        /*
         * include hide status
         */
        $includeHide = false;
        
        if (is_null($q)) {
            return [];
        }

        $query = new Query();
        
        $type = Yii::$app->request->get('type');
        $courseType = Yii::$app->request->get('course_type');
        if (empty($type)) {
            if (!empty($courseType) && $courseType == 'all') {
                $type =  array_keys(Course::listTypes());
                $type[] = Course::TYPE_COMBO;
            } else {
                $type[] = Course::TYPE_VIDEO;
            }
        }
        if (!is_array($type)) {
            $typeTmp[] = $type;
            $type = $typeTmp;
        }
        
        if (($teacherId = Yii::$app->request->get('teacher_id')) && !empty($teacherId)) {
            $query->andWhere(['teacher_id' => $teacherId]);
        }

        $query->select('c.*, c.name as text')
                ->from(Course::tableName() . ' as c')
                ->andWhere(['c.is_deleted' => Course::BOOL_NO])
                ->andWhere(['c.type' => $type])
                ->andWhere('(name LIKE :q OR slug LIKE :q OR short_name LIKE :q)', [':q' => "%{$q}%"]);
             
        $params = Yii::$app->request->get();
        if (isset($params['include_hide'])) {
            $includeHide = Yii::$app->request->get('include_hide');
        }
               
        if (!$includeHide) {
            // check status
            $settings = ArrayHelper::map(Setting::find()->all(), 'key', 'value');
            $specialCourses = !empty($settings['special_courses']) ? $settings['special_courses'] : '';
            $specialCourses = explode(',', $specialCourses);
            $specialCourseTypes = [
                Course::TYPE_SOFTWARE
            ];

            foreach ($specialCourseTypes as $key => $courseType) {
                if (!in_array($courseType, $type)) {
                   unset($specialCourseTypes[$key]);
                }
            }

            $query->andFilterWhere([
                'OR',
                ['c.status' => Course::STATUS_ACTIVE],
                ['c.type' => $specialCourseTypes],
                ['c.id' => $specialCourses],
            ]);
        }
                
        if (Yii::$app->user->can('TeachingAssistant')) {
            $teachingAssistantTable = CourseTeachingAssistant::tableName();
            $query->leftJoin("$teachingAssistantTable as ta", 'ta.course_id = c.id');
            $query->andWhere(['ta.teaching_assistant_id' => Yii::$app->user->id]);
        }

        $command = $query->createCommand();
        $data = $command->queryAll();
        
        if ($auto) {
            $out['results'] = array_values($data);
            return $out;
        }

        return array_values($data);
    }
    
    public function actionGetComboItems($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        
        $items = CourseComboItem::find()->where([
            'course_combo_id' => $id,
        ])->all();
        
        $result = [];
        foreach ($items as $item) {
            $result[] = [
                'id' => $item->id,
                'course_id' => $item->course_id,
                'price' => $item->course->price,
                'discount_amount' => $item->course->price - $item->price
            ];
        }
        
        return $result;
    }

    /**
     * @desc ajax function for find teacher autocomplete
     *
     * @param string $q
     * @param int    $id
     *
     * @return json
     */
    public function actionSearchTeacher($q = null, $id = null)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $out = ['results' => []];
        if (!is_null($q)) {
            $query = new Query();

            $query->select('id, profile.name AS text')
                    ->from(User::tableName())
                    ->join('LEFT JOIN', Profile::tableName(), 'profile.user_id = user.id')
                    ->join('LEFT JOIN', AuthAssignment::tableName(), 'auth_assignment.user_id = user.id')
                    ->where('(email LIKE :q OR username LIKE :q OR profile.name LIKE :q)', [':q' => "%{$q}%"])
                    ->andFilterWhere(['auth_assignment.item_name' => $this->roleTeacher]);

            $command = $query->createCommand();
            $data = $command->queryAll();

            $out['results'] = array_values($data);
        }

        return $out;
    }
    
    /**
     * @desc ajax function for find section
     *
     * @param int    $courseId
     *
     * @return json
     */
    public function actionSearchSection($courseId)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        
        $out = ['results' => []];
        if (!is_null($courseId)) {
            $query = new Query();

            $query->select('id, name AS text')
                    ->from(CourseSection::tableName())
                    ->where(['course_id' => $courseId, 'type' => CourseSection::TYPE_SECTION]);
            
            $command = $query->createCommand();
            $data = $command->queryAll();

            $out['results'] = array_values($data);
        }

        return $out;
    }


    /**
     * @param string $q
     * @param bool $auto
     * @return json
     */
    public function actionSearchCategory($q = null, $auto = false) {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $out = [
            'results' => []
        ];

        if (empty($q)) {
            return [];
        }

        $query = new Query();
        $data = $query->select('c.*, c.name as text')
            ->from(Category::tableName() . ' as c')
            ->andWhere(['c.is_deleted' => Course::BOOL_NO])
            ->andWhere('(name LIKE :q OR slug LIKE :q OR description LIKE :q)', [':q' => "%{$q}%"])
            ->all();

        if ($auto) {
            $out['results'] = array_values($data);
            return $out;
        }

        return array_values($data);
	}
	
    public function actionCourseList($q = null, $id = null) {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];
        if (!is_null($q)) {
            $query = new Query;
            $query->select('id, name AS text')
                ->from('courses')
                ->where(['like', 'name', $q])
                ->andWhere(['courses.status'=>1])
                ->limit(20);
            $command = $query->createCommand();
            $data = $command->queryAll();
            $out['results'] = array_values($data);
        }
        elseif ($id > 0) {
            $out['results'] = ['id' => $id, 'text' => Course::find($id)->name];
        }
        return $out;
    }
}
