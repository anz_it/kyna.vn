<?php

namespace app\modules\course\controllers;

use Yii;
use yii\web\UploadedFile;
use yii\web\NotFoundHttpException;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Response;

use kyna\order\models\Order;
use kyna\order\models\OrderDetails;
use kyna\base\models\MetaField;
use kyna\course\models\Course;
use kyna\course\models\search\CourseSearch;
use kyna\course\models\CourseMeta;
use kyna\course\models\CourseCommission;
use kyna\course\models\CourseTeachingAssistant;
use app\modules\course\lib\ImageUpload;
use common\helpers\ImageHelper;
use common\lib\CDNImage;

/**
 * DefaultController implements the CRUD actions for Course model.
 */
class DefaultController extends \app\components\controllers\Controller
{

    const TAB_LESSON = 'lesson';
    const TAB_STUDENT = 'student';
    const TAB_QUIZ = 'quiz';
    const TAB_DOCUMENT = 'doc';
    const TAB_QUESTION = 'question';

    public $mainTitle = 'Khóa học';
    public $mainModel = 'Course';

    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'view'],
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->user->can('Course.View');
                        },
                    ],
                    [
                        'allow' => true,
                        'actions' => ['create'],
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->user->can('Course.Create');
                        },
                    ],
                    [
                        'allow' => true,
                        'actions' => ['update', 'change-status'],
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->user->can('Course.Update');
                        },
                    ],
                    [
                        'allow' => true,
                        'actions' => ['delete'],
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->user->can('Course.Delete');
                        },
                    ],
                ],
            ],
        ]);
    }

    public function actionIndex()
    {
        $searchModel = new CourseSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'queryParams' => Yii::$app->request->queryParams
        ]);
    }

    public function actionCreate()
    {
        $model = new Course();
        $model->affiliate_commission_percent = 50;
        $courseCommissionModel = new CourseCommission();
        $courseCommissionModel->is_default = CourseCommission::BOOL_YES;
        $courseCommissionModel->loadDefaultValues();
        $metaValueModels = $this->getMetaValueModels();

        $teachingAssistant = new CourseTeachingAssistant();

        if ($model->load(Yii::$app->request->post()))
        {
            $timeNumber = $model->total_time_number;
            $timeUnit = Yii::$app->request->post('total_time_unit');
            $model->total_time = $this->convertTotalTime($timeNumber, $timeUnit);

            $courseCommissionModel->load(Yii::$app->request->post());
            $teachingAssistant->load(Yii::$app->request->post());

            if ($model->save())
            {
                foreach ($metaValueModels as $key => $metaValueModel)
                {
                    if (isset($_POST['CourseMeta'][$key]['value']))
                    {
                        $metaValueModel->value = $_POST['CourseMeta'][$key]['value'];
                        $metaValueModel->course_id = $model->id;
                        $metaValueModel->save();
                    }
                }

                $flashData = 'Khóa học đã được tạo'.PHP_EOL;
                $alert = 'success';

                $flashData .= '<ul class="fa-ul">';
                if ($image_url = $this->_uploadImage($model, 'image_url')) {
                    $model->image_url = $image_url;
                    $flashData .= '<li><i class="fa fa-li fa-check"></i> Upload image_url thành công</li>';
                }

                if ($video_cover_image_url = $this->_uploadImage($model, 'video_cover_image_url')) {
                    $model->video_cover_image_url = $video_cover_image_url;
                    $flashData .= '<li><i class="fa fa-li fa-check"></i> Upload video_cover_image_url thành công</li>';
                }

                if ($video_cover_image_url OR $image_url) {
                    $model->save();
                }

                $teachingAssistant->course_id = $model->id;
                if ( ! ($teachingAssistant->isNewRecord && empty($teachingAssistant->teaching_assistant_id)))
                {
                    if ($teachingAssistant->save()) {
                        $flashData .= '<li><i class="fa fa-li fa-check"></i> Thông tin trợ giảng đã lưu thành công</li>';
                    }
                    else {
                        $flashData .= '<li><i class="fa fa-li fa-error"></i> Thông tin trợ giảng đã lưu KHÔNG thành công</li>';
                        $alert = 'warning';
                    }
                }

                $courseCommissionModel->course_id = $model->id;
                if ($courseCommissionModel->save()) {
                    $flashData .= '<li><i class="fa fa-li fa-check"></i> Công thức commission được lưu thành công</li>';
                }
                else {
                    $flashData .= '<li><i class="fa fa-li fa-error"></i> Công thức commission được lưu KHÔNG thành công</li>';
                    $alert = 'warning';
                }
                $flashData .= '</ul>';

                Yii::$app->session->setFlash($alert, $flashData);

                return $this->redirect(['/course/view/index', 'id' => $model->id]);
            }
        }

        return $this->render('create', [
            'model' => $model,
            'metaValueModels' => $metaValueModels,
            'courseCommissionModel' => $courseCommissionModel,
            'teachingAssistant' => $teachingAssistant
        ]);
    }

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model->total_time_number = $model->totalTimePartials['number'];

        $courseCommissionModel = $model->defaultCommission;
        if (empty($courseCommissionModel)) {
            $courseCommissionModel = new CourseCommission();
            $courseCommissionModel->course_id = $id;
            $courseCommissionModel->is_default = CourseCommission::BOOL_YES;
            $courseCommissionModel->loadDefaultValues();
        }
        if (!empty($courseCommissionModel->apply_to_date)) {
            $courseCommissionModel->apply_to_date = Yii::$app->formatter->asDate($courseCommissionModel->apply_to_date);
        }
        $metaValueModels = $this->getMetaValueModels($id);

        $teachingAssistant = CourseTeachingAssistant::find()->andWhere(['course_id' => $id])->one();
        if (is_null($teachingAssistant)) {
            $teachingAssistant = new CourseTeachingAssistant();
            $teachingAssistant->course_id = $id;
        }

        if ($model->load(Yii::$app->request->post())) {
            $timeNumber = $model->total_time_number;
            $timeUnit = Yii::$app->request->post('total_time_unit');
            $model->total_time = $this->convertTotalTime($timeNumber, $timeUnit);

            if ($image_url = $this->_uploadImage($model, 'image_url')) {
                $model->image_url = $image_url;
            }
            if ($video_cover_image_url = $this->_uploadImage($model, 'video_cover_image_url')) {
                $model->video_cover_image_url = $video_cover_image_url;
            }

            $courseCommissionModel->load(Yii::$app->request->post());
            $teachingAssistant->load(Yii::$app->request->post());

            if ($model->save() && $courseCommissionModel->save()) {
                Yii::$app->session->setFlash('success', 'Cập nhật thành công.');

                if (!($teachingAssistant->isNewRecord && empty($teachingAssistant->teaching_assistant_id))) {
                    $teachingAssistant->save();
                }
                $noError = true;

                if ($noError) {
                    return $this->redirect(['/course/view/index', 'id' => $model->id]);
                }
            }
        }

        return $this->render('update', [
            'model' => $model,
            'metaValueModels' => $metaValueModels,
            'courseCommissionModel' => $courseCommissionModel,
            'teachingAssistant' => $teachingAssistant
        ]);
    }

    protected function findModel($id)
    {
        if (($model = Course::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function getMetaValueModels($course_id = 0)
    {
        $metaFields = MetaField::find()->where([
                    'status' => MetaField::STATUS_ACTIVE,
                    'model' => MetaField::MODEL_COURSE, ])
                ->all();

        $metaValueModels = [];

        foreach ($metaFields as $metaField) {
            if (!empty($course_id)) {
                $metaValueModel = CourseMeta::find()->where([
                            'course_id' => $course_id,
                            'key' => $metaField->key,
                        ])->one();

                if (empty($metaValueModel)) {
                    $metaValueModel = new CourseMeta();
                    $metaValueModel->meta_field_id = $metaField->id;
                }
                $metaValueModel->key = $metaField->key;
            } else {
                $metaValueModel = new CourseMeta();
                $metaValueModel->meta_field_id = $metaField->id;
                $metaValueModel->key = $metaField->key;
            }
            if ($metaField->is_required) {
                $metaValueModel->scenario = 'required';
            }
            $metaValueModels[$metaField->key] = $metaValueModel;
        }

        return $metaValueModels;
    }

    private function _uploadImage($model, $attribute) {
        $files = UploadedFile::getInstances($model, $attribute);
        if (!sizeof($files)) {
            $oldAttributes = $model->oldAttributes;

            return isset($oldAttributes[$attribute]) ? $oldAttributes[$attribute] : false;
        }

        $file = $files[0];

        $cdnImage = new CDNImage($model, $attribute);
        $result = $cdnImage->upload($file);

        return $result;
    }

    public function convertTotalTime($number, $unit) {
        $timePerUnit = 0;
        switch ($unit) {
            case 'months':
                $timePerUnit = 3600 * 24 * 30;
                break;
            case 'weeks':
                $timePerUnit = 3600 * 24 * 7;
                break;
            case 'days':
                $timePerUnit = 3600 * 24;
                break;
            case 'hours':
                $timePerUnit = 3600;
                break;
            case 'minutes':
                $timePerUnit = 60;
                break;
            default:
                break;
        }
        return $number * $timePerUnit;
    }

    /**
     * @desc delete function for Backend CRUDs
     * @param type $id
     * @return redirect
     */
    public function actionDelete($id)
    {
        $course = $this->findModel($id);

        $redirectUrl = Yii::$app->request->get('redirectUrl');

        if (empty($course->comboItems)) {
            $orderIds = OrderDetails::find()->select('order_id')
                ->distinct()
                ->where(['course_id' => $course->id])->column();

            $order = Order::find()->where(['id' => $orderIds, 'status' => Order::getStatusNotDelete()])->count();
            if (empty($order)) {
                $course->delete();
                if (Yii::$app->request->isAjax) {
                    Yii::$app->response->format = Response::FORMAT_JSON;
                    $ret = [
                        'success' => true,
                        'message' => 'Đã xóa thành công',
                    ];
                    return $ret;
                } else {
                    Yii::$app->session->setFlash('warning', 'Đã xóa');
                    return $this->redirect(empty($redirectUrl) ? ['index'] : $redirectUrl);
                }
            }
        }

        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $ret = [
                'success' => false,
                'message' => 'Không thể xóa khóa học',
            ];
            return $ret;
        } else {
            Yii::$app->session->setFlash('warning', 'Không thể xóa khóa học');
            return $this->redirect(empty($redirectUrl) ? ['index'] : $redirectUrl);
        }
    }

}
