<?php

namespace app\modules\course\controllers;


use app\modules\course\models\forms\RelationTeacherImportForm;
use Yii;
use yii\web\UploadedFile;
use yii\web\NotFoundHttpException;

use kyna\course\models\Teacher;
use kyna\course\models\search\TeacherSearch;
use kyna\user\models\Profile;
use kyna\base\models\MetaField;
use kyna\user\models\UserMeta;
use kyna\settings\models\TeacherSetting;
use kyna\course\models\TeacherNotify;

use common\lib\CDNImage;
use common\components\ExportExcel;

/**
 * TeacherController implements the CRUD actions for User model.
 */
class TeacherController extends \app\components\controllers\Controller
{

    public $mainTitle = 'Giảng viên';
    public $role = 'Teacher';
    public $roleRelation = 'Relation';
    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TeacherSearch();
        $searchModel->role = $this->role;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Teacher(['scenario' => 'create']);
        $profile = new Profile(['scenario' => 'create']);
        $metaValueModels = $this->getMetaValueModels();

        if ($model->load(Yii::$app->request->post()) && $profile->load(Yii::$app->request->post())) {

            if ($model->save()) {
                if ($avatar = $this->_uploadImage($model, 'avatar')) {
                    $model->avatar = $avatar;
                    $model->save();
                }
                $authManager = Yii::$app->authManager;
                // assign role Teacher
                $authManager->assign($authManager->getItem($this->role), $model->id);

                $settingModel = new TeacherSetting();
                $settingModel->teacher_id = $model->id;
                $settingModel->key = TeacherSetting::KEY_RECEIVE_MAIL_DATE;
                $settingModel->value = Yii::$app->params['default_receive_qna_emails'];
                $settingModel->save();

                $userProfile = $model->profile;
                $userProfile->name = $profile->name;
                if ($userProfile->save(false)) {
                    $noError = true;
                    foreach ($metaValueModels as $key => $metaValueModel) {
                        if (isset($_POST['UserMeta'][$key]['value'])) {
                            $metaValueModel->value = $_POST['UserMeta'][$key]['value'];
                            $metaValueModel->user_id = $model->id;
                            if (!$metaValueModel->save()) {
                                $noError = false;
                            }
                        }
                    }
                    if ($noError) {
                        Yii::$app->session->setFlash('success', "Thêm thành công!");
                        return $this->redirect(['view', 'id' => $model->id]);
                    }
                }
            }
        }

        return $this->render('create', [
            'model' => $model,
            'profile' => $profile,
            'metaValueModels' => $metaValueModels
        ]);

    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model->scenario = 'update';
        $profile = $model->profile;

        $metaValueModels = $this->getMetaValueModels($id);

        if ($model->load(Yii::$app->request->post())) {
            $profile->load(Yii::$app->request->post());

            if ($avatar = $this->_uploadImage($model, 'avatar')) {
                $model->avatar = $avatar;
            }

            if ($model->save() && $profile->save()) {
                $noError = true;

                foreach ($metaValueModels as $key => &$metaValueModel) {
                    $metaValueModel->value = isset($_POST['UserMeta'][$key]['value']) ? $_POST['UserMeta'][$key]['value'] : '';
                    $metaValueModel->user_id = $model->id;
                    if (!$metaValueModel->save()) {
                        $noError = false;
                    }
                }
                if ($noError) {
                    Yii::$app->session->setFlash('success', "Cập nhật thành công!");
                    return $this->redirect(['update', 'id' => $model->id]);
                }
            }
        }
        return $this->render('update', [
            'model' => $model,
            'profile' => $profile,
            'metaValueModels' => $metaValueModels
        ]);

    }

    public function getMetaValueModels($user_id = 0)
    {
        $metaFields = MetaField::find()->where([
            'status' => MetaField::STATUS_ACTIVE,
            'model' => MetaField::MODEL_TEACHER])
            ->all();

        $metaValueModels = [];

        foreach ($metaFields as $metaField) {
            if (!empty($user_id)) {
                $metaValueModel = UserMeta::find()
                    ->where([
                        'user_id' => $user_id,
                        'key' => $metaField->key,
                    ])
                    ->orderBy(['id' => SORT_DESC])
                    ->one();

                if (empty($metaValueModel)) {
                    $metaValueModel = new UserMeta();
                    $metaValueModel->meta_field_id = $metaField->id;
                }
                $metaValueModel->key = $metaField->key;
            } else {
                $metaValueModel = new UserMeta();
                $metaValueModel->meta_field_id = $metaField->id;
                $metaValueModel->key = $metaField->key;
            }
            if ($metaField->is_required) {
                $metaValueModel->scenario = 'required';
            }
            if ($metaField->is_unique) {
                $metaValueModel->scenario = 'unique';
            }
            if ($metaField->is_unique && $metaField->is_required) {
                $metaValueModel->scenario = 'all';
            }
            $metaValueModels[$metaField->key] = $metaValueModel;
        }

        return $metaValueModels;
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Teacher::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    private function _uploadImage($model, $attribute)
    {
        $files = UploadedFile::getInstances($model, $attribute);

        if (!sizeof($files)) {
            $oldAttributes = $model->oldAttributes;

            return isset($oldAttributes[$attribute]) ? $oldAttributes[$attribute] : false;
        }

        $file = $files[0];

        $cdnImage = new CDNImage($model, $attribute);
        $result = $cdnImage->upload($file);

        return $result;
    }

    /**
     * Export List Notify
     * @param $id
     */
    public function actionExportNotify($id)
    {
        $model = $this->findModel($id);
        $fileName = 'notify_export_' . $model->slug . '.xls';

        // prepare data
        $data = $this->formatNotifyContent($model);

        // render Excel
        $excelRunner = new ExportExcel();
        $excelRunner->type = ExportExcel::TYPE_DOWNLOAD;
        $excelRunner->renderData($data, null, $fileName);

        Yii::$app->end();
    }

    /**
     * Format Notify Content Rows for Excel
     * @param $header
     * @param $model
     * @return array
     */
    private function formatNotifyContent($model)
    {
        $notifyData = TeacherNotify::find()->where([
            'teacher_id' => $model->id
        ])->all();
        $returnData['header'] = [
            'inc' => '#',
            'teacher_id' => 'Teacher ID',
            'teacher_name' => 'Teacher Name',
            'teacher_email' => 'Teacher Email',
            'user_id' => 'Student ID',
            'user_name' => 'Student Name',
            'user_email' => 'Student Email',
            'created_time' => 'Register Date'

        ];
        $returnData['content'] = [];
        if (count($notifyData) > 0) {
            $row = 2;
            foreach ($notifyData as $item) {
                $rowData = null;
                foreach ($returnData['header'] as $column => $label) {
                    $value = null;
                    switch ($column) {
                        case 'inc':
                            $value = $row - 1;
                            break;
                        case 'created_time';
                            $value = Yii::$app->formatter->asDatetime(!empty($item->created_time) ? $item->created_time : null);
                            break;
                        case 'teacher_name';
                            $teacher = $item->teacher;
                            if ($teacher && $teacher->profile) {
                                $value = $teacher->profile->name;
                            }
                            break;
                        case 'teacher_email':
                            $teacher = $item->teacher;
                            if (!empty($teacher)) {
                                $value = $teacher->email;
                            }
                            break;
                        case 'user_name';
                            $user = $item->user;
                            if ($user && $user->profile) {
                                $value = $user->profile->name;
                            }
                            break;
                        case 'user_email':
                            $user = $item->user;
                            if (!empty($user)) {
                                $value = $user->email;
                            }
                            break;
                        default:
                            if ($item->{$column}) {
                                $value = $item->{$column};
                            }
                            break;
                    }
                    $rowData[$column] = $value;
                }
                array_push($returnData['content'], $rowData);
                $row++;
            }
            unset($rowData);
        }

        return $returnData;
    }

    public function actionAssign()
    {
        $model = new RelationTeacherImportForm();
        if ($model->load(Yii::$app->request->post()) && $res = $model->assign()) {
            if($res == true && !is_array($res)){
                Yii::$app->session->setFlash('success', "Import user follow teacher thành công");
            }else{
                $res = json_encode($res);
                Yii::$app->session->setFlash('success', "Danh sách không import thành công $res");
            }
            return $this->redirect(['/course/teacher/index']);
        } else {
            return $this->renderAjax('_import', [
                'model' => $model,
            ]);
        }
    }
    public function actionDownloadSample()
    {
        $filePath = Yii::getAlias('@upload/samples/relation_template.xlsx');

        if (!is_file($filePath)) {
            throw new NotFoundHttpException;
        }

        return Yii::$app->response->sendFile($filePath);
    }
}
