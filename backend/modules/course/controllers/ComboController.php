<?php

namespace app\modules\course\controllers;

use common\lib\CDNImage;
use Yii;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

use kotchuprik\sortable\actions\Sorting;

use kyna\course_combo\models\CourseCombo;
use kyna\course\models\search\CourseComboSearch;
use kyna\course_combo\models\CourseComboItem;
use kyna\base\models\MetaField;
use kyna\course_combo\models\CourseComboMeta;
use kyna\course\models\CourseTeachingAssistant;
use app\modules\course\lib\ImageUpload;

/**
 * ComboController implements the CRUD actions for CourseCombo model.
 */
class ComboController extends \app\components\controllers\Controller
{

    const TAB_INFO = 'info';
    const TAB_LIST_COURSES = 'list-courses';

    public $mainTitle = 'Khóa học Combo';

    public function actions()
    {
        $actions = parent::actions();

        $actions['sorting'] = [
            'class' => Sorting::className(),
            'query' => CourseCombo::find(),
            'orderAttribute' => 'position'
        ];
        $actions['item-sorting'] = [
            'class' => Sorting::className(),
            'query' => CourseComboItem::find(),
            'orderAttribute' => 'position'
        ];

        return $actions;
    }

    /**
     * Lists all CourseCombo models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CourseComboSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new CourseCombo model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new CourseCombo();
        $model->type = CourseCombo::TYPE_COMBO;
        $model->loadDefaultValues();

        $metaValueModels = $this->getMetaValueModels();

        $teachingAssistant = new CourseTeachingAssistant();

        if ($model->load(Yii::$app->request->post())) {

            if ($model->save()) {

                if ($image_url = $this->_uploadImage($model, 'image_url')) {
                    $model->image_url = $image_url;
                    $model->save();
                }
                if ($video_cover_image_url = $this->_uploadImage($model, 'video_cover_image_url')) {
                    $model->video_cover_image_url = $video_cover_image_url;
                    $model->save();
                }

                if ($teachingAssistant->load(Yii::$app->request->post())) {
                    $teachingAssistant->course_id = $model->id;
                    $teachingAssistant->save();
                }

                $noError = true;

                foreach ($metaValueModels as $key => $metaValueModel) {
                    if (isset($_POST['CourseComboMeta'][$key]['value'])) {
                        $metaValueModel->value = $_POST['CourseComboMeta'][$key]['value'];
                        $metaValueModel->course_id = $model->id;
                        if (!$metaValueModel->save()) {
                            $noError = false;
                        }
                    }
                }

                if ($noError) {
                    Yii::$app->session->setFlash('success', 'Thêm thành công.');

                    return $this->redirect([
                        '/course/combo/update',
                        'id' => $model->id,
                        'tab' => self::TAB_LIST_COURSES,
                    ]);
                }
            }
        }

        return $this->render('create', [
            'model' => $model,
            'metaValueModels' => $metaValueModels
        ]);
    }

    /**
     * Updates an existing CourseCombo model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model->scenario = CourseCombo::SCENARIO_UPDATE;
        $metaValueModels = $this->getMetaValueModels($id);

        $itemModel = $this->findItemModel(Yii::$app->request->get('itemId'));
        $itemModel->course_combo_id = $model->id;

        if ($itemModel->load(Yii::$app->request->post())) {
            $action = $itemModel->isNewRecord ? 'Thêm' : 'Cập nhật';

            if ($itemModel->save()) {
                $itemModel = $this->findItemModel();

                Yii::$app->session->setFlash('success', $action . ' khóa học thành công.');
                return $this->redirect([
                    '/course/combo/update',
                    'id' => $model->id,
                    'tab' => self::TAB_LIST_COURSES,
                ]);
            }
        }

        if ($model->load(Yii::$app->request->post())) {
            if ($image_url = $this->_uploadImage($model, 'image_url')) {
                $model->image_url = $image_url;
            }
            if ($video_cover_image_url = $this->_uploadImage($model, 'video_cover_image_url')) {
                $model->video_cover_image_url = $video_cover_image_url;
            }
            if ($model->save()) {
                $noError = true;

                foreach ($metaValueModels as $key => $metaValueModel) {
                    $metaValueModel->value = isset($_POST['CourseComboMeta'][$key]['value']) ? $_POST['CourseComboMeta'][$key]['value'] : '';
                    $metaValueModel->course_id = $model->id;
                    if (!$metaValueModel->save()) {
                        $noError = false;
                    }
                }

                if ($noError) {
                    Yii::$app->session->setFlash('success', 'Cập nhật thành công.');

                    return $this->redirect(['update', 'id' => $model->id, 'tab' => Yii::$app->request->get('tab')]);
                }
            }
        }

        return $this->render('update', [
            'model' => $model,
            'itemModel' => $itemModel,
            'metaValueModels' => $metaValueModels
        ]);
    }

    /**
     * Finds the CourseCombo model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CourseCombo the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CourseCombo::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function findItemModel($id = 0)
    {
        $model = CourseComboItem::findOne($id);

        if (is_null($model)) {
            $model = new CourseComboItem();
            $model->setScenario('create');
        } else {
            $model->setScenario('update');
        }

        return $model;
    }

    public function getTabItems($data, $tab = self::TAB_INFO)
    {
        $tabItems = [
            [
                'label' => 'Thông tin',
                'content' => $this->renderPartial('_form', $data, true),
                'active' => $tab == self::TAB_INFO ? true : false
            ],
            [
                'label' => 'Danh sách khóa học',
                'content' => $this->renderPartial('_list-courses', $data, true),
                'active' => $tab == self::TAB_LIST_COURSES ? true : false
            ]
        ];

        return $tabItems;
    }

    public function actionDeleteItem($id)
    {
        $model = $this->findItemModel($id);

        if ($model->delete()) {
            Yii::$app->session->setFlash('warning', 'Đã xóa');
        }

        return $this->redirect(['/course/combo/update', 'id' => $model->course_combo_id, 'tab' => self::TAB_LIST_COURSES]);
    }

    public function getMetaValueModels($course_id = 0)
    {
        $metaFields = MetaField::find()->where([
                    'status' => MetaField::STATUS_ACTIVE,
                    'model' => MetaField::MODEL_COURSE_COMBO, ])
                ->all();

        $metaValueModels = [];

        foreach ($metaFields as $metaField) {
            if (!empty($course_id)) {
                $metaValueModel = CourseComboMeta::find()->where([
                            'course_id' => $course_id,
                            'key' => $metaField->key,
                        ])
                    ->orderBy('id DESC')
                    ->one();

                if (empty($metaValueModel)) {
                    $metaValueModel = new CourseComboMeta();
                    $metaValueModel->meta_field_id = $metaField->id;
                }
                $metaValueModel->key = $metaField->key;
            } else {
                $metaValueModel = new CourseComboMeta();
                $metaValueModel->meta_field_id = $metaField->id;
                $metaValueModel->key = $metaField->key;
            }
            if ($metaField->is_required) {
                $metaValueModel->scenario = 'required';
            }
            $metaValueModels[$metaField->key] = $metaValueModel;
        }

        return $metaValueModels;
    }

    private function _uploadImage($model, $attribute) {
        $files = UploadedFile::getInstances($model, $attribute);
        if (!sizeof($files)) {
            $oldAttributes = $model->oldAttributes;

            return isset($oldAttributes[$attribute]) ? $oldAttributes[$attribute] : false;
        }

        $file = $files[0];

        $cdnImage = new CDNImage($model, $attribute);
        $result = $cdnImage->upload($file);

        return $result;
    }
}
