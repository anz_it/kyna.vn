<?php

namespace app\modules\course\controllers;

use Yii;
use yii\web\View;
use yii\web\Response;
use yii\base\Event;

use app\modules\course\components\TreeView;
use kyna\course\models\CourseSection;

class TreeManagerController extends \kartik\tree\controllers\NodeController
{

    /**
     * View, create, or update a tree node via ajax
     *
     * @return string json encoded response
     */
    public function actionManage($courseId = 0)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        static::checkValidRequest();
        $parentKey = $action = null;
        $modelClass = '\kartik\tree\models\Tree';
        $isAdmin = $softDelete = $showFormButtons = $showIDAttribute = false;
        $currUrl = $nodeView = $formOptions = $formAction = $breadCrumbs = $nodeSelected = '';
        $iconsList = $nodeAddlViews = [];
        extract(static::getPostData());
        /**
         * @var Tree $modelClass
         * @var Tree $node
         */
        if (!isset($id) || empty($id)) {
            $node = new $modelClass;
            $node->initDefaults();
            $node->course_id = $courseId;
        } else {
            $node = $modelClass::findOne($id);
        }
        $module = TreeView::module();
        $params = $module->treeStructure + $module->dataStructure + [
                'node' => $node,
                'parentKey' => $parentKey,
                'action' => $formAction,
                'formOptions' => empty($formOptions) ? [] : $formOptions,
                'modelClass' => $modelClass,
                'currUrl' => $currUrl,
                'isAdmin' => $isAdmin,
                'iconsList' => $iconsList,
                'softDelete' => $softDelete,
                'showFormButtons' => $showFormButtons,
                'showIDAttribute' => $showIDAttribute,
                'nodeView' => $nodeView,
                'nodeAddlViews' => $nodeAddlViews,
                'nodeSelected' => $nodeSelected,
                'breadcrumbs' => empty($breadcrumbs) ? [] :$breadcrumbs,
            ];
        if (!empty($module->unsetAjaxBundles)) {
            Event::on(View::className(), View::EVENT_AFTER_RENDER, function ($e) use ($module) {
                foreach ($module->unsetAjaxBundles as $bundle) {
                    unset($e->sender->assetBundles[$bundle]);
                }
            });
        }
        $callback = function () use ($nodeView, $params) {
            return $this->renderAjax($nodeView, ['params' => $params]);
        };
        return self::process(
            $callback,
            Yii::t('kvtree', 'Lỗi xảy ra trong quá trình xem, vui lòng thử lại.'),
            null
        );
    }

    public function actionGridview($courseId = 0)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        extract(static::getPostData());

        if (!isset($id) || empty($id)) {
            $node = new CourseSection();
            $node->initDefaults();
            $node->course_id = $courseId;
        } else {
            $node = CourseSection::findOne($id);
        }

        $nodeView = '@app/modules/course/views/tree-manager/_gridview';

        $module = TreeView::module();
        $params = $module->treeStructure + $module->dataStructure + [
            'node' => $node,
            'courseId' => $node->course_id,
        ];

        $callback = function () use ($nodeView, $params, $node) {
            if ($node->isRoot()) {
                return '<div class="alert alert-info">Vui lòng tạo các Bài giảng và bài tập trong các đầu mục con</div>';
            }
            return $this->renderAjax($nodeView, ['params' => $params]);
        };
        return self::process(
            $callback,
            Yii::t('kvtree', 'Lỗi xảy ra trong quá trình xem, vui lòng thử lại.'),
            null
        );
    }
}
