<?php

namespace app\modules\course\controllers;

use Yii;
use yii\web\NotFoundHttpException;

use kyna\course\models\QuizSessionAnswer;
use kyna\course\models\QuizSession;
use app\components\controllers\Controller;
use app\modules\course\models\QuizSessionSearch;

class MarkController extends Controller
{

    public $mainTitle = 'Chấm điểm';

    public function actionIndex($id)
    {
        $searchModel = new QuizSessionSearch();
        $searchModel->quiz_id = $id;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionSession($id)
    {
        $model = $this->findModel($id);
        $model->scenario = QuizSession::SCENARIO_MARK;
        $answerModels = $model->essayAnswers;
        
        if (Yii::$app->request->post('QuizSessionAnswer')) {
            $data = Yii::$app->request->post('QuizSessionAnswer');
            $score = $model->total_score;
            
            foreach ($answerModels as $answerModel) {
                $score -= $answerModel->score;
            }
            
            foreach ($data['score'] as $id => $mark) {
                $score += $this->_calculateScore($id, $mark);
            }
            
            $model->status = QuizSession::STATUS_MARKED;
            $model->total_score = $score;
            $percent = $model->total_score * 100 / $model->total_quiz_score;
            
            if ($percent >= $model->quiz->percent_can_pass) {
                $model->is_passed = QuizSession::BOOL_YES;
            } else {
                $model->is_passed = QuizSession::BOOL_NO;
            }
            
            if ($model->save()) {
                $this->_sendAlertMarked($model);
                Yii::$app->session->setFlash('success', 'Đã chấm thành công. Hệ thống sẽ chuyển kết quả tới học viên.');
                return $this->redirect(['/course/view/mark', 'id' => $model->quiz->course_id]);
            } else {
                Yii::$app->session->setFlash('error', 'Có lỗi xảy ra.');
            }
        }
        
        return $this->render('update', [
            'model' => $model,
            'answerModels' => $answerModels
        ]);
    }

    /**
     * 
     * @param type $id Id of QuizSessionAnswer
     * @param type $mark mark / 10
     * @return float
     */
    private function _calculateScore($id, $mark)
    {
        $model = QuizSessionAnswer::findOne($id);
        $model->score = ($mark * $model->question->calculateScore() * $model->quizDetail->coefficient) / 10;

        if ($model->save()) {
            return $model->score;
        }
        
        return 0;
    }
    
    /**
     * Send email to user when his quiz is marked
     * @param QuizSession $model
     */
    private function _sendAlertMarked($model)
    {
        Yii::$app->mailer->compose()
            ->setHtmlBody($this->renderPartial('@common/mail/course/quiz_marked', [
                'model' => $model
            ]))
            ->setTo($model->user->email)
            ->setSubject('Bài làm của bạn vừa được giảng viên chấm tại Kyna.vn')
            ->send();
    }
    
    /**
     * Finds the Quiz model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Quiz the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = QuizSession::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}