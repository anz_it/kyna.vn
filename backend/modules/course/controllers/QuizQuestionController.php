<?php

namespace app\modules\course\controllers;

use app\modules\course\models\QuestionChoice;
use app\modules\course\models\QuestionConnect;
use app\modules\course\models\QuestionEssay;
use app\modules\course\models\QuestionFillIn;
use app\modules\course\models\QuizQuestionProcessInterface;
use Yii;
use yii\base\Response;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use kyna\course\models\QuizQuestion;
use kyna\course\models\QuizAnswer;
use kyna\course\models\search\QuizQuestionSearch;
use kyna\course\models\QuizDetail;

/**
 * QuestionController implements the CRUD actions for QuizQuestion model.
 */
class QuizQuestionController extends \app\components\controllers\Controller
{

    const TAB_INFO = 'info';
    const TAB_ANSWER = 'answer';

    public $mainTitle = 'Câu hỏi';

    /**
     * Lists all QuizQuestion models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new QuizQuestionSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new QuizQuestion model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate1($courseId = 0)
    {
        $model = new QuizQuestion();
        $model->loadDefaultValues();
        $model->course_id = $courseId;

        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                $quizId = Yii::$app->request->get('quizId');

                if (!empty($quizId)) {
                    $quizDetailModel = new QuizDetail();
                    $quizDetailModel->quiz_id = $quizId;
                    $quizDetailModel->quiz_question_id = $model->id;

                    $quizDetailModel->save(false);
                }

                Yii::$app->session->setFlash('success', 'Thêm thành công.');

                if ($model->type == QuizQuestion::TYPE_WRITING) {
                    return $this->redirect(['/course/view/question', 'id' => $model->course_id]);
                } else {
                    return $this->redirect([
                        'update',
                        'id' => $model->id,
                        'tab' => self::TAB_ANSWER,
                        'quizId' => $quizId
                    ]);
                }
            }
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * @param $courseId
     * @author khanh.phan@kyna.vn
     * @return \yii\web\Response | mixed
     */
    public function actionCreate($courseId)
    {
        $type = Yii::$app->request->post('type');
        if (!empty ($type)) {
            $model = new QuizQuestion();
            $model->course_id = $courseId;
            $model->type = $type;
            $model->content = "";

            if ($model->save(false)) {
                return $this->redirect(['update', 'id' => $model->id]);
            }
            Yii::$app->session->setFlash("error", "Không tạo được câu hỏi.");
        }
        $this->redirect(Url::to(['course/view/question', 'id' => $courseId]));

    }

    /**
     * @param $model
     * @param $data
     * @return QuizQuestionProcessInterface
     */
    protected function buildProcessor($model, $data)
    {
        switch ($model->type) {
            case QuizQuestion::TYPE_FILL_INTO_DOTS:
                return new QuestionFillIn(['data' => $data, 'model' => $model]);

            case QuizQuestion::TYPE_ONE_CHOICE:
            case QuizQuestion::TYPE_MULTIPLE_CHOICE;
                return new QuestionChoice(['data' => $data, 'model' => $model]);

            case QuizQuestion::TYPE_CONNECT_ANSWER:
                return new QuestionConnect(['data' => $data, 'model' => $model]);

            case QuizQuestion::TYPE_WRITING:
                return new QuestionEssay(['data' => $data, 'model' => $model]);

            default:
                return null;

        }
    }

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $data = file_get_contents('php://input');
        if (!empty ($data)) {
            $data = json_decode($data, true);
            $processor = $this->buildProcessor($model,$data);
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

            return $processor->process();
        }

        $data = json_decode($data, true);
        $processor = $this->buildProcessor($model,$data);
        $jsonData = $processor->buildData();

        return $this->render('update1', ['model' => $model, 'jsonData' => $jsonData]);

    }

    /**
     * Updates an existing QuizQuestion model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate1($id, $tab = self::TAB_INFO, $answerId = 0)
    {
        $model = $this->findModel($id);
        $quizId = Yii::$app->request->get('quizId');

        $answerModel = $this->findAnswerModel($answerId);
        $answerModel->question_id = $model->id;

        if ($answerModel->load(Yii::$app->request->post())) {
            $action = $answerModel->isNewRecord ? 'Thêm' : 'Cập nhật';

            if ($answerModel->save()) {
                $answerModel = $this->findAnswerModel();

                Yii::$app->session->setFlash('success', $action . ' câu trả lời thành công.');
                return $this->redirect([
                    '/course/quiz-question/update',
                    'id' => $model->id,
                    'tab' => self::TAB_ANSWER,
                    'quizId' => $quizId
                ]);
            }
        }

        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                if (!empty($quizId)) {
                    Yii::$app->session->setFlash('success', 'Cập nhật câu hỏi thành công.');

                    return $this->redirect([
                        '/course/quiz/update',
                        'id' => $quizId,
                        'tab' => QuizController::TAB_QUESTION,
                    ]);
                }
                Yii::$app->session->setFlash('success', 'Cập nhật thành công.');

                return $this->redirect(['/course/view/question', 'id' => $model->course_id]);
            }
        }

        return $this->render('update', [
            'model' => $model,
            'tab' => $tab,
            'answerModel' => $answerModel
        ]);
    }

    protected function findAnswerModel($id = 0)
    {
        $model = QuizAnswer::findOne($id);

        if (is_null($model)) {
            $model = new QuizAnswer();
        }

        return $model;
    }


    /**
     * Finds the QuizQuestion model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return QuizQuestion the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = QuizQuestion::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
