<?php

namespace app\modules\course\controllers;

use kyna\course\models\search\CourseComboSearch;
use Yii;
use yii\web\UploadedFile;
use yii\web\NotFoundHttpException;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\data\ActiveDataProvider;
use yii\grid\SerialColumn;
use kyna\user\models\UserCourse;
use kyna\user\models\User;
use kyna\base\models\MetaField;
use kyna\course\models\Course;
use kyna\course\models\search\CourseSearch;
use kyna\course\models\CourseMeta;
use kyna\course\models\CourseCommission;
use kyna\course\models\CourseTeachingAssistant;
use kyna\order\models\Order;
use kyna\order\models\OrderDetails;

/**
 * DefaultController implements the CRUD actions for Course model.
 */
class ExportController extends \app\components\controllers\Controller
{

    const TAB_LESSON = 'lesson';
    const TAB_STUDENT = 'student';
    const TAB_QUIZ = 'quiz';
    const TAB_DOCUMENT = 'doc';
    const TAB_QUESTION = 'question';

    public $mainTitle = 'Export Khóa học';
    public $mainModel = 'Course';

    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'combo'],
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->user->can('Course.Export');
                        },
                    ],
                ],
            ],
        ]);
    }

    public function actionIndex()
    {
        $searchModel = new CourseSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'tabs' => $this->loadTabs(),
        ]);
    }

    public function actionCombo()
    {
        $searchModel = new CourseComboSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('combo', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'tabs' => $this->loadTabs(),
        ]);
    }

    private function loadTabs()
    {
        $tabsItems = [
            [
                'label' => 'Khóa học',
                'url' => Url::toRoute(['/course/export/index']),
                'active' => $this->action->id == 'index',
                'encode' => false,
            ],
            [
                'label' => 'Khóa học Combo',
                'url' => Url::toRoute(['/course/export/combo']),
                'active' => $this->action->id == 'combo',
                'encode' => false,
            ],
        ];

        return $tabsItems;
    }

}
