<?php

namespace app\modules\course\controllers;

use yii\web\NotFoundHttpException;
use kotchuprik\sortable\actions\Sorting;
use kyna\course\models\QuizAnswer;

class AnswerController extends \app\components\controllers\Controller
{
    
    public function actions()
    {
        $actions = parent::actions();
        
        $actions['sorting'] = [
            'class' => Sorting::className(),
            'query' => QuizAnswer::find()
        ];
        
        return $actions;
    }
    
    protected function findModel($id)
    {
        if (($model = QuizAnswer::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}

