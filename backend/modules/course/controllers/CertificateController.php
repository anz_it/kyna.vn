<?php

namespace app\modules\course\controllers;

use Yii;
use kyna\course\traits\CertificateControllerTrait;

class CertificateController extends \app\components\controllers\Controller
{
    use CertificateControllerTrait;

    public function getGenerator($saveFile)
    {
        return Yii::createObject([
            'class' => '\kyna\course\lib\CertificateImage',
            'saveFile' => $saveFile,
            'overwrite' => true
        ]);
    }
}
