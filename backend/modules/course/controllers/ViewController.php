<?php

namespace app\modules\course\controllers;

use kyna\course\models\Course;
use kyna\course\models\search\CourseOpinionSearch;
use kyna\course\models\search\CourseScreenshotSearch;
use Yii;
use yii\data\ActiveDataProvider;
use yii\web\Response;
use yii\web\ServerErrorHttpException;
use yii\web\UploadedFile;
use yii\web\NotFoundHttpException;

use app\modules\course\models\DocumentUploadForm;
use app\modules\course\lib\ImageUpload;

use kyna\course\models\CourseSection;
use kyna\course\models\CourseDocument;
use kyna\course\models\CourseLearnerQna;
use kyna\course\models\CourseDiscussion;
use kyna\course\models\search\CourseQuestionSearch;
use app\modules\course\models\QuizSessionSearch;
use kyna\user\models\UserCourse;
use kyna\user\models\search\UserCourseSearch;

use common\helpers\ImageHelper;

class ViewController extends \app\components\controllers\Controller
{

    const TAB_INDEX = 'index';
    const TAB_MARK = 'mark';
    const TAB_LESSON = 'lesson';
    const TAB_STUDENT = 'student';
    const TAB_QUIZ = 'quiz';
    const TAB_DOCUMENT = 'doc';
    const TAB_QUESTION = 'question';
    const TAB_QNA = 'qna';
    const TAB_DISCUSS = 'discuss';
    const TAB_OPINION = 'opinion';
    const TAB_SCREENSHOT = 'screenshot';
    private $_view;
    private $_model;
    public $mainTitle = 'Khóa học';

    public function tabs()
    {
        $_tabs = [
            [
                'label' => 'Thông tin khóa học',
                'url' => ['/course/view/' . self::TAB_INDEX, 'id' => $this->_model->id],
                'active' => $this->action->id === self::TAB_INDEX,
            ],
            [
                'label' => 'Danh sách học viên',
                'url' => ['/course/view/' . self::TAB_STUDENT, 'id' => $this->_model->id],
                'active' => $this->action->id === self::TAB_STUDENT,
            ],
            [
                'label' => 'Bài giảng/bài tập',
                'url' => ['/course/view/' . self::TAB_LESSON, 'id' => $this->_model->id],
                'active' => $this->action->id === self::TAB_LESSON,
                //'type' => Course::TYPE_VIDEO,
            ],
            [
                'label' => 'Chấm điểm',
                'url' => ['/course/view/' . self::TAB_MARK, 'id' => $this->_model->id],
                'active' => $this->action->id === self::TAB_MARK,
                //'type' => Course::TYPE_VIDEO,
            ],
            [
                'label' => 'Ngân hàng câu hỏi',
                'url' => ['/course/view/' . self::TAB_QUESTION, 'id' => $this->_model->id],
                'active' => $this->action->id === self::TAB_QUESTION,
                //'type' => Course::TYPE_VIDEO,
            ],
            [
                'label' => 'Tài liệu',
                'url' => ['/course/view/' . self::TAB_DOCUMENT, 'id' => $this->_model->id],
                'active' => $this->action->id === self::TAB_DOCUMENT,
                //'type' => Course::TYPE_VIDEO,
            ],
            [
                'label' => 'Câu hỏi cho giảng viên',
                'url' => ['/course/view/' . self::TAB_QNA, 'id' => $this->_model->id],
                'active' => $this->action->id === self::TAB_QNA,
            ],
            [
                'label' => 'Thảo luận',
                'url' => ['/course/view/' . self::TAB_DISCUSS, 'id' => $this->_model->id],
                'active' => $this->action->id === self::TAB_DISCUSS,
            ],
            [
                'label' => 'Ý kiến học viên',
                'url' => ['/course/view/' . self::TAB_OPINION, 'id' => $this->_model->id],
                'active' => $this->action->id === self::TAB_OPINION,
            ],
            [
                'label' => 'Screenshot',
                'url' => ['/course/view/' . self::TAB_SCREENSHOT, 'id' => $this->_model->id],
                'active' => $this->action->id === self::TAB_SCREENSHOT,
                'type' => Course::TYPE_SOFTWARE,
            ],
        ];
        $result = [];
        foreach ($_tabs as $item) {
            if (!isset($item['type']) || (isset($item['type']) && $item['type'] == $this->_model->type)) {
                array_push($result, $item);
            }
        }


        return $result;
    }

    public function beforeAction($action)
    {
        if (!in_array($action->id, ['doc-download', 'qna-answer', 'delete-student', 'doc-delete'])) {
            $_id = Yii::$app->request->get('id');
            $this->_model = $this->findModel($_id);
            $this->_view = $action->id;
        }
        return parent::beforeAction($action);
    }

    /**
     *
     * @param $id
     * @param null $flag
     * @return string
     */
    public function actionIndex($id, $flag = null)
    {
        return $this->_render([
            'user' => Yii::$app->user
        ]);
    }

    public function actionStudent($id)
    {
        $searchModel = new UserCourseSearch();
        $searchModel->course_id = $id;

        $newModel = new UserCourse();
        $newModel->course_id = $id;
        if ($newModel->load(Yii::$app->request->post()) && $newModel->save()) {
            UserCourse::active($newModel->user_id, $newModel->course_id);
            Yii::$app->session->setFlash('success', 'ÄÃ£ thÃªm há»c viÃªn thÃ nh cÃ´ng.');

            $newModel = new UserCourse();
            $newModel->course_id = $id;
        }

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        if (Yii::$app->request->isAjax && Yii::$app->request->post('type', null) == 'export') {
            $sql = $dataProvider->query->createCommand()->getRawSql();
            $sql = str_replace('`', '', $sql);
            $email = empty(Yii::$app->request->post('email')) ? Yii::$app->user->identity->email : Yii::$app->request->post('email');
            // execute command in console
            $rootPath = \Yii::getAlias('@root');
            $exportLog = \Yii::getAlias('@console/runtime/export_course_student.log');
            $exportErrorLog = \Yii::getAlias('@console/runtime/export_course_student-error.log');
            // check log file exist
            self::_checkExistFile($exportLog);
            self::_checkExistFile($exportErrorLog);
            $command = "php {$rootPath}/yii export/run course-student \"{$sql}\" {$email}" . " > {$exportLog} 2>>{$exportErrorLog} &";
            exec($command);
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'result' => true,
            ];
        }

        return $this->_render([
            'newModel' => $newModel,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    private function _checkExistFile($file)
    {
        clearstatcache();
        if (!file_exists($file)) {
            touch($file);
        }
    }

    public function actionLesson($id)
    {
        $user = Yii::$app->user;
        $sectionId = Yii::$app->request->get('sectionId', 0);
        $sectionQuery = CourseSection::find()->where(['course_id' => $id])->addOrderBy('root, lft');

        return $this->_render([
            'sectionQuery' => $sectionQuery,
            'sectionId' => $sectionId,
        ]);
    }

    public function actionQuestion($id)
    {
        $searchModel = new CourseQuestionSearch();
        $searchModel->course_id = $id;

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->where(['and', $dataProvider->query->where, ['or', 'parent_id is null', 'parent_id = 0']]);

        return $this->_render([
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionQna($id)
    {
        $model = $this->_model;
        $formModel = new CourseLearnerQna();
        $formModel->course_id = $id;

        $query = CourseLearnerQna::find()
            ->where(['course_id' => $id])
            ->andWhere(['or', '`question_id`=0', '`question_id` is null'])
            ->orderBy(['posted_time' => SORT_DESC]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        return $this->_render([
            'dataProvider' => $dataProvider,
            'formModel' => $formModel,
        ]);
    }

    public function actionQnaAnswer()
    {
        Yii::$app->response->format = Response::FORMAT_RAW;
        $model = new CourseLearnerQna();
        $model->user_id = Yii::$app->user->getId();

        if (!$model->load(Yii::$app->request->post()) or !$model->save()) {
            var_dump($model->errors);
            return;
        }

        $student = $model->question->user;

        $subject = "[Kyna.vn] CÃ¢u há»i #{$model->question_id} cá»§a báº¡n Ä‘Ã£ Ä‘Æ°á»£c giáº£ng viÃªn tráº£ lá»i";

        Yii::$app->mailer->compose()
            ->setHtmlBody($this->renderPartial('@common/mail/qa/notify_answer', [
                'fullname' => $student->profile->name,
                'questionId' => $model->question_id,
                'questionContent' => strip_tags($model->question->content),
                'courseName' => $model->course->name,
                'answerContent' => strip_tags($model->content),
                'qaLink' => $model->question->qaLink,
            ]))
            ->setTo($student->email)
            ->setSubject($subject)
            ->send();

        return '<div class="media-left"><img src="https://randomuser.me/api/portraits/lego/5.jpg" width="50" alt="" /></div>' .
            '<div class="media-body">' .
            '<h4 class="media-heading">' . $model->user->profile->name . '</h4>' .
            '<footer class="text-muted">' .
            '<small>Tráº£ lá»i lÃºc <time>' . $model->postedTime . '</time></small>' .
            '</footer>' .
            '<div class="media-content">' . $model->content . '</div>' .
            '</div>';
    }

    public function actionDoc($id)
    {
        $model = $this->_model;
        $formModel = new DocumentUploadForm();
        $formModel->course_id = $id;

        if (Yii::$app->request->isPost) {
            $formModel->files = UploadedFile::getInstances($formModel, 'files');
            if ($formModel->upload()) {
                $this->refresh();
            } else {
                var_dump($formModel->errors);
            }
        }

        $query = CourseDocument::find()->where([
            'course_id' => $id,
        ])->orderBy(['id' => 'desc']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        return $this->_render([
            'dataProvider' => $dataProvider,
            'formModel' => $formModel,
        ]);
    }

    public function actionDocDownload($id)
    {
        $doc = CourseDocument::findOne($id);
        $docLink = $doc->docLink;
        $docName = $doc->title . '.' . $doc->file_ext;
        $header_response = get_headers($docLink, 1);
        if (strpos($header_response[0], "404") !== false) {
            throw new NotFoundHttpException();
        }
        $response = Yii::$app->response;
        if (!is_resource($response->stream = fopen($docLink, 'r'))) {
            throw new ServerErrorHttpException('file access failed: permission deny');
        }
        // Force download file
        $response->headers->set('Content-Type', $doc->mime_type);
        $response->headers->set('Content-Length', $doc->size);
        $response->headers->set('Content-Disposition', "attachment; filename='{$docName}'");
        return $response->send();
    }

    public function actionMark($id)
    {
        $searchModel = new QuizSessionSearch();
        $searchModel->courseId = $id;
        $searchModel->status = QuizSessionSearch::STATUS_WAITING_FOR_MARK;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->_render([
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionDiscuss($id)
    {
        $model = $this->_model;

        if (Yii::$app->request->isPost) {
            $mDiscuss = new CourseDiscussion();

            if (!$mDiscuss->load(Yii::$app->request->post()) or !$mDiscuss->save()) {
                var_dump($mDiscuss->errors);
                die;
                // TODO: Error message here
            }

            if (!empty($mDiscuss->parent_id)) {
                return $this->renderPartial('discuss/_reply-list', [
                    'model' => $mDiscuss->parent,
                ]);
            }
        }

        $query = CourseDiscussion::find()
            ->where(['course_id' => $id])
            ->andWhere(['or', '`parent_id`=0', '`parent_id` is null'])
            ->orderBy(['created_time' => SORT_DESC]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        return $this->_render([
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionOpinion($id)
    {
        $searchModel = new CourseOpinionSearch();
        $searchModel->course_id = $id;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);


        return $this->_render([
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionScreenshot($id)
    {
        $searchModel = new CourseScreenshotSearch();
        $searchModel->course_id = $id;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);


        return $this->_render([
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    private function _render($params = [])
    {
        $isChanged = false;
        foreach (['image_url', 'video_cover_image_url'] as $attribute) {
            //$model = $this->_model;
            $upload = new ImageUpload($this->_model, $attribute);
            $file = ImageHelper::saveModelImage($this->_model, $attribute, $upload->getUploadPath(false));

            if ($file) {
                $this->_model->$attribute = str_replace($upload->getUploadPath(false), $upload->getUploadUrl(false),
                    $file);

                if ($this->_model->isAttributeChanged($attribute) and !empty($this->_model->$attribute)) {
                    $isChanged = true;
                }
            }
        }

        if ($isChanged) {
            $this->_model->save();
        }

        $params += [
            'model' => $this->_model,
        ];

        return $this->render('index', [
            'view' => $this->_view,
            'tabs' => $this->tabs(),
            'viewParams' => $params,
        ]);
    }

    protected function findModel($id, $class = '\\kyna\\course\\models\\Course')
    {
        if (($model = $class::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionDeleteStudent($id)
    {
        $model = UserCourse::findOne($id);

        if (is_null($model)) {
            throw new NotFoundHttpException();
        }

        UserCourse::deleteAll(['id' => $id]);
        Yii::$app->session->setFlash('warning', 'Đã xóa học viên khỏi khóa học.');

        return $this->redirect(['student', 'id' => $model->course_id]);
    }

    public function actionDocDelete($id)
    {
        $doc = CourseDocument::findOne($id);
        if (!$doc) {
            throw new NotFoundHttpException();
        }

        if (file_exists($doc->save_path)) {
            unlink($doc->save_path);
        }

        if ($doc->delete()) {
            Yii::$app->session->setFlash('warning', 'Đã xóa');
        }

        return $this->redirect(['/course/view/doc', 'id' => $doc->course_id]);
    }


}
