<?php

namespace app\modules\course\controllers;

use Yii;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use kotchuprik\sortable\actions\Sorting;

use kyna\base\models\MetaField;
use kyna\course\models\CourseLesson;
use kyna\course\models\Quiz;
use kyna\course\models\QuizMeta;
use kyna\course\models\search\QuizSearch;
use yii\widgets\Pjax;

/**
 * QuizController implements the CRUD actions for Quiz model.
 */
class QuizController extends \app\components\controllers\Controller
{
    
    const TAB_INFO = 'info';
    const TAB_SETTING = 'setting';
    const TAB_QUESTION = 'question';

    public $mainTitle = 'Quiz';
    
    /**
     * Lists all Quiz models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new QuizSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new Quiz model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($courseId, $sectionId = 0, $tab = self::TAB_INFO)
    {
        $model = new Quiz();
        $model->loadDefaultValues();
        
        $lesson = new CourseLesson();
        $lesson->loadDefaultValues();
        
        $lesson->section_id = $sectionId;
        $lesson->type = CourseLesson::TYPE_QUIZ;
        
        $model->course_id = $lesson->course_id = $courseId;
        
        if ($lesson->load(Yii::$app->request->post()) && $model->load(Yii::$app->request->post())) {
            // name of quiz = name of lesson
            $lesson->name = $model->name;
            
            if ($lesson->validate() && $model->save()) {
                $lesson->quiz_id = $model->id;
                $lesson->save(false);
                return $this->redirect(['update', 'id' => $model->id, 'tab' => self::TAB_SETTING]);
            }
        }
        
        return $this->render('create', [
            'model' => $model,
            'lesson' => $lesson,
            'tab' => $tab
        ]);
    }

    /**
     * Updates an existing Quiz model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id, $tab = self::TAB_INFO)
    {
        $model = $this->findModel($id);
        $metaModels = $this->getMeta($id);
        $lesson = $model->lesson;
        
        // load and save quiz settings
        if (QuizMeta::loadMultiple($metaModels, Yii::$app->request->post())) {
            $tab = self::TAB_SETTING;
            $isSuccess = true;

            foreach ($metaModels as $metaModel) {
                if (empty($metaModel->course_id)) {
                    $metaModel->course_id = $model->course_id;
                }
                $isSuccess = $isSuccess && $metaModel->save();
            }

            if ($lesson->load(Yii::$app->request->post())) {
                $isSuccess = $isSuccess && $lesson->save();
            }
            
            if ($isSuccess) {
                Yii::$app->session->setFlash('success', 'Cập nhật cấu hình thành công.');
            }
        }

        // load and save basic quiz information
        if ($lesson->load(Yii::$app->request->post()) && $model->load(Yii::$app->request->post())) {
            $tab = self::TAB_INFO;
            
            if ($model->save() && $lesson->save()) {
                Yii::$app->session->setFlash('success', 'Cập nhật thành công.');
            }
        }

        return $this->render('update', [
            'model' => $model,
            'tab' => $tab,
            'lesson' => $lesson,
            'metaModels' => $metaModels
        ]);
    }

    /**
     * Finds the Quiz model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Quiz the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Quiz::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    /**
     * @desc prepare tabs to show on Create/Update Form
     * @param type $model
     * @param type $tab
     * @param type $metaModels
     * @return array
     */
    public function getTabItems($model, $lesson, $tab = self::TAB_INFO, $metaModels = [])
    {
        $tabItems = [
            [
                'label' => 'Thông tin bài Quiz',
                'content' => $this->renderPartial('_form', [ 'model' => $model, 'lesson' => $lesson], true),
                'active' => $tab == QuizController::TAB_INFO ? true : false
            ],
            [
                'label' => 'Thiết lập cấu hình',
                'content' => $this->renderPartial('_setting', [ 'model' => $model, 'metaModels' => $metaModels, 'lesson' => $lesson], true),
                'active' => $tab == QuizController::TAB_SETTING ? true : false
            ],
            [
                'label' => 'Danh sách câu hỏi',
                'content' => $this->renderPartial('_question', ['model' => $model], true),
                'active' => $tab == QuizController::TAB_QUESTION ? true : false
            ]
        ];
        
        return $tabItems;
    }
    
    /**
     * @desc get QuizMeta models
     * @param type $quiz_id
     * @return QuizMeta
     */
    public function getMeta($quiz_id = 0)
    {
        $metaFields = MetaField::find()->andWhere([
                    'status' => MetaField::STATUS_ACTIVE,
                    'model' => MetaField::MODEL_COURSE_QUIZ
            ])->all();

        $metaValueModels = [];

        foreach ($metaFields as $metaField) {
            if (!empty($quiz_id)) {
                $metaValueModel = QuizMeta::find()->andWhere([
                            'quiz_id' => $quiz_id,
                            'meta_field_id' => $metaField->id,
                        ])->one();

                if (empty($metaValueModel)) {
                    $metaValueModel = new QuizMeta();
                    $metaValueModel->meta_field_id = $metaField->id;
                }
                $metaValueModel->key = $metaField->key;
            } else {
                $metaValueModel = new QuizMeta();
                $metaValueModel->meta_field_id = $metaField->id;
                $metaValueModel->key = $metaField->key;
            }
            $metaValueModel->quiz_id = $quiz_id;
            
            if ($metaField->is_required) {
                $metaValueModel->scenario = 'required';
            }
            $metaValueModels[$metaField->key] = $metaValueModel;
        }

        return $metaValueModels;
    }
}
