<?php

namespace app\modules\course\lib;

use Yii;

class ImageUpload extends \common\widgets\upload\lib\Upload
{
    public $uploadRoot = '@upload';
    public $uploadRootUrl = '/uploads';

    public $model;
    public $attribute;
    public $tableName;

    public function __construct($model, $attribute)
    {
        parent::__construct();
        $this->model = $model;
        $this->attribute = $attribute;

        if (!$this->tableName) {
            $this->tableName = $model->tableSchema->name;
        }
    }

    public function getFileName($file)
    {
        return $this->attribute . '.' . $file->extension;
    }

    public function getUploadPath($relativeDir)
    {
        return Yii::getAlias($this->uploadRoot . '/' . $this->tableName . '/' . $this->model->id . '/img');
    }

    public function getUploadUrl($relativeDir)
    {
        return Yii::getAlias($this->uploadRootUrl . '/' . $this->tableName . '/' . $this->model->id . '/img');
    }
}
