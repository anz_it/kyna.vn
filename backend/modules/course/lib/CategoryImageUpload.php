<?php

namespace app\modules\course\lib;

use Yii;

class CategoryImageUpload extends \common\widgets\upload\lib\Upload {
    public $uploadRoot = '@upload';
    public $uploadRootUrl = '/uploads';

    public $model;
    public $attribute;

    public function __construct($model, $attribute) {
        $this->model = $model;
        $this->attribute = $attribute;
    }

    public function getFileName($file) {
        return $this->attribute.'.'.$file->extension;
    }

    public function getUploadPath($relativeDir) {
        return Yii::getAlias($this->uploadRoot.'/categories/'.$this->model->id.'/img');
    }

    public function getUploadUrl($relativeDir) {
        return Yii::getAlias($this->uploadRootUrl.'/categories/'.$this->model->id.'/img');
    }
}
