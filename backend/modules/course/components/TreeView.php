<?php

namespace app\modules\course\components;

use yii\bootstrap\Html;

/*
 * To change this template file, choose Tools | Templates
 */
class TreeView extends \kartik\tree\TreeView
{
    /**
     * @var the main course of current TreeView
     */
    public $courseId;

    /**
     * @var path to view for showing gridview
     */
    public $gridView;

    public $mainTemplate = <<< HTML
<div class="row">
    <div class="col-sm-3">
        {wrapper}
    </div>
    <div class="col-sm-9">
        <div class="row">
            {detail}
        </div>
        <div class="row">
            {gridview}
        </div>
    </div>
</div>
HTML;

    public function renderWidget()
    {
        //echo($this->renderWrapper());
        $content = strtr($this->mainTemplate, [
            '{wrapper}' => $this->renderWrapper(),
            '{detail}' => $this->renderDetail(),
            '{gridview}' => $this->renderGridview(),
        ]);

        return strtr($content, [
            '{heading}' => $this->renderHeading(),
            '{search}' => $this->renderSearch(),
            '{toolbar}' => $this->renderToolbar(),
        ])."\n".
        Html::textInput('kv-node-selected', $this->value, $this->options)."\n";
    }

    public function renderGridview()
    {
        $modelClass = $this->query->modelClass;
        $node = $modelClass::findOne($this->displayValue);

        $params = $this->_module->treeStructure + $this->_module->dataStructure + [
            'node' => $node,
            'courseId' => $this->courseId,
        ];

        $content = $this->render($this->gridView, ['params' => $params]);

        return Html::tag('div', $content, ['id' => 'course-treeview-grid', 'style' => 'margin-top: 20px;']);
    }
}
