<?php

namespace app\modules\teacher\models;

/**
 * Created by PhpStorm.
 * User: macintosh
 * Date: 4/28/17
 * Time: 9:51 AM
 */
class SettingForm extends \yii\base\Model
{

    const RECEIVE_TYPE_AFTER_APPROVE = 1;
    const RECEIVE_TYPE_ONE_DATE = 2;

    public $receiveMailType;
    public $receiveMailDate;

    public function rules()
    {
        return [
            ['receiveMailType', 'required'],
            ['receiveMailDate', 'string'],
            ['receiveMailDate', 'required',
                'when' => function ($model) {
                    $model->receiveMailType == self::RECEIVE_TYPE_ONE_DATE;
                },
                'whenClient' => "function (attribute, value) {
                    var checkedInput = $('input[name=\'SettingForm[receiveMailType]\']:checked');
                    return checkedInput.val() == '" . self::RECEIVE_TYPE_ONE_DATE . "';
                }"
            ],
        ];
    }

    public function attributeLabels()
    {
        return [
            'receiveMailType' => 'Chế độ nhận thông báo',
            'receiveMailDate' => 'Ngày nhận'
        ];
    }

    public static function getReceiveTypeOptions()
    {
        return [
            self::RECEIVE_TYPE_AFTER_APPROVE => 'Ngay khi câu hỏi được duyệt',
            self::RECEIVE_TYPE_ONE_DATE => 'Định kỳ: chọn ngày trong tuần',
        ];
    }

}