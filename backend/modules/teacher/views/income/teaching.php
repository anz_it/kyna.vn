<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Nav;

$this->title = 'Thu nhập giảng dạy';
$this->params['breadcrumbs'][] = $this->title;

$crudTitles = Yii::$app->params['crudTitles'];
?>
<style>
    .dl-horizontal dt {
        width: 210px;
        padding-right: 10px;
    }
</style>
<div class="commission-income-view">
    <div class="row">
        <div class="col-xs-12">
            <div class="commission-income-index">
                <nav class="navbar navbar-default">
                    <h4 class="navbar-text navbar-left" style="font-weight: bold">Thống kê học viên theo học</h4>
                    <?= $this->render('_filter', ['searchModel' => $searchModel]) ?>
                </nav>
                <div class="row">
                    <div class="col-xs-6">
                        <div class="box box-solid">
                            <div class="box-body">
                                <dl class="dl-horizontal">
                                    <dt>Số học viên theo bộ lọc</dt>
                                    <dd><?= $totalRegFilter ?> HV</dd>
                                    <dt>Tổng thu nhập theo bộ lọc</dt>
                                    <dd><?= Yii::$app->formatter->asCurrency($totalAmountFilter) ?></dd>
                                    <dt>Thực nhận học phí theo bộ lọc</dt>
                                    <dd><?= Yii::$app->formatter->asCurrency($totalRealIncomeFilter) ?></dd>
                                </dl>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="box box-solid">
                            <div class="box-body">
                                <dl class="dl-horizontal">
                                    <dt>Tổng số học viên</dt>
                                    <dd><?= $totalReg ?> HV</dd>
                                    <dt>Tổng thu nhập</dt>
                                    <dd><?= Yii::$app->formatter->asCurrency($totalAmount) ?></dd>
                                    <dt>Thực nhận học phí tích lũy</dt>
                                    <dd><?= Yii::$app->formatter->asCurrency($totalRealIncome) ?></dd>
                                </dl>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box">
                    <?php
                    $roles = implode(",",
                        array_map(function ($p) { return $p->item_name; }, \kyna\user\models\AuthAssignment::find()->where(['user_id' => Yii::$app->user->id])->all())
                    );

                    echo GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => false,
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],
                            'created_time:datetime',
                            [
                                'label' => 'Học viên',
                                'value' => function ($model, $key) use ($roles){
                                    $user = $model->order->user;
                                    if ($user == null)
                                        return "";
                                    if (strpos($roles, "Teacher") !== false) {
                                        return substr($user->email, 0, 5).'******'.substr($user->email, strlen($user->email) - 5, 5);
                                    }
                                    return $user->email;
                                }
                            ],
                            [
                                'attribute' => 'course_id',
                                'value' => function ($model) {return $model->course->name; },
                                'options' => ['class' => 'col-xs-3'],
                            ],
                            [
                                'attribute' => 'total_amount',
                                'visible' => !$isTeacher,
                                'format' => 'raw',
                                'label' => 'Thực nhận học phí',
                                'value' => function ($model) {
                                    return Html::a(Yii::$app->formatter->asCurrency($model->realIncome), '#', [
                                        'data-content' => $model->explainTotalText,
                                        'data-toggle' => 'popover',
                                    ]) . " &#63;";
                                },

                            ],
                            [
                                'attribute' => 'teacher_commission_percent',
                                'visible' => !$isTeacher,
                                'label' => 'Hoa hồng',
                                'value' => function ($model) {
                                    return $model->teacher_commission_percent . '%';
                                }
                            ],
                            [
                                'attribute' => 'teacher_commission_amount',
                                'format' => 'currency',
                                'visible' => !$isTeacher
                            ]
                        ],
                    ]);
                    ?>
                </div>
            </div>
        </div>
    </div>

</div>
<?php
$js = <<< SCRIPT
/* To initialize BS3 tooltips set this below */
$(function () { 
    $("[data-toggle='popover']").popover({ trigger: "hover", html: true }); 
});;
SCRIPT;

$this->registerJs($js);
?>
