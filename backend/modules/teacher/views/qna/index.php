<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\web\JsExpression;

use kartik\widgets\Select2;
use kartik\daterange\DateRangePicker as DatePicker;
use dosamigos\tinymce\TinyMceAsset;

use app\modules\community\assets\CommunityAsset;

CommunityAsset::register($this);
TinyMceAsset::register($this);

$this->title = 'Câu hỏi cho giảng viên';
$this->params['breadcrumbs'][] = $this->title;

$crudTitles = Yii::$app->params['crudTitles'];
?>
<div class="course-learner-qna-index">
    <?php Pjax::begin(['timeout' => Yii::$app->params['timeOutPjax'], 'id' => 'idPjaxReload']); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'id',
                'options' => ['class' => 'col-xs-1']
            ],
            [
                'attribute' => 'date_range',
                'label' => 'Thời gian hỏi',
                'filter' => DatePicker::widget([
                    'model' => $searchModel,
                    'attribute' => 'date_range',
                    'convertFormat' => true,
                    'pluginOptions' => [
                        'locale' => [
                            'format' => 'd/m/Y',
                            'separator' => ' - ',
                        ]
                    ]
                ]),
                'options' => ['class' => 'col-xs-1'],
                'value' => function ($model) {
                    return Yii::$app->formatter->asDatetime($model->posted_time, "php:d/m/Y H:i");
                }
            ],
            [
                'attribute' => 'content',
                'format' => 'html',
                'value' => function ($model) {
                    $commentDivStyle = !empty($model->question_id) ? 'style="margin-left:70px;"' : '';

                    $commentDisplay = '<div class="commnet-display" ' . $commentDivStyle . '><div class="media-left"><img src="' . $model->user->avatarImage . '" width="50" alt="" /></div>'.
                        '<div class="media-body">'.
                        '<h4 class="media-heading">' . $model->user->profile->name . ' - ' . $model->user->email . '</h4>'.
                        '<footer class="text-muted">'.
                        '<small>Trả lời lúc <time>' . $model->postedTime . ' </time></small>'.
                        '</footer>'.
                        '<div class="media-content">' . strip_tags(($model->content), "<p><a>") . '</div>'.
                        '</div></div>';

                    return $commentDisplay;
                },
                'options' => ['class' => 'col-xs-6'],
            ],
            [
                'attribute' => 'user_id',
                'value' => function ($model) {
                    return $model->user->profile->name;
                },
                'filter' => Select2::widget([
                    'model' => $searchModel,
                    'initValueText' => !empty($searchModel->user) ? $searchModel->user->profile->name : '',
                    'attribute' => 'user_id',
                    'options' => ['placeholder' => $crudTitles['prompt']],
                    'pluginOptions' => [
                        'allowClear' => true,
                        'minimumInputLength' => 3,
                        'language' => [
                            'errorLoading' => new JsExpression("function () { return 'Chờ nhận kết quả...'; }"),
                        ],
                        'ajax' => [
                            'url' => Url::toRoute(['/user/api/search', 'auto' => true]),
                            'dataType' => 'json',
                        ],
                    ],
                ]),
                'options' => ['class' => 'col-xs-1']
            ],
            [
                'attribute' => 'course_id',
                'value' => function ($model) {
                    return !empty($model->course) ? $model->course->name : null;
                },
                'filter' => Select2::widget([
                    'model' => $searchModel,
                    'initValueText' => !empty($searchModel->course) ? $searchModel->course->name : '',
                    'attribute' => 'course_id',
                    'options' => ['placeholder' => $crudTitles['prompt']],
                    'pluginOptions' => [
                        'allowClear' => true,
                        'minimumInputLength' => 3,
                        'language' => [
                            'errorLoading' => new JsExpression("function () { return 'Chờ nhận kết quả...'; }"),
                        ],
                        'ajax' => [
                            'url' => Url::toRoute(['/course/api/search', 'auto' => true, 'teacher_id' => Yii::$app->user->id]),
                            'dataType' => 'json',
                        ],
                    ],
                ]),
                'options' => ['class' => 'col-xs-2']
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{edit} {answer}',
                'options' => ['class' => 'col-xs-1'],
                'buttons' => [
                    'edit' => function ($url, $model, $key) {
                        $url = Url::toRoute(['update', 'id' => $model->id]);

                        return Html::a('<span class="fa fa-edit"></span> Edit', $url, [
                            'class' => 'btn btn-sm btn-default btn-block btn-reply',
                        ]);
                    },
                    'answer' => function ($url, $model, $key) {
                        $url = Url::toRoute(['answer', 'question_id' => $model->id, 'id' => !is_null($model->answer) ? $model->answer->id : null]);

                        return Html::a('<span class="fa fa-reply"></span> Trả lời', $url, [
                            'class' => 'btn btn-sm btn-default btn-block btn-reply',
                        ]);
                    },
                ],
                'visibleButtons' => [
                    'answer' => function ($model) {
                        return $model->question_id == null;
                    },
                    'edit' => function ($model) {
                        return $model->question_id != null && $model->user_id == Yii::$app->user->id;
                    }
                ],
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
