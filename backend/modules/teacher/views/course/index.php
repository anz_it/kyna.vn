<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\helpers\ArrayHelper;

use kartik\select2\Select2;
use kartik\grid\GridView;

use kyna\course\models\Course;
use kyna\course\models\Category;

$crudTitles = Yii::$app->params['crudTitles'];
$crudButtonIcons = Yii::$app->params['crudButtonIcons'];

$this->title = 'Danh sách khóa học';
$this->params['breadcrumbs'][] = $this->title;

$user = Yii::$app->user;
?>
<div class="row">
    <div class="col-xs-12">
        <div class="becourse-index">
            <nav class="navbar navbar-default">
                <?= $this->render('@app/modules/course/views/default/_search_custom', ['queryParams' => $queryParams]) ?>
            </nav>

            <div class="box">
                <?php
                $teacherDesc = empty($searchModel->teacher_id) ? '' : $searchModel->teacher->profile->name;
                ?>
                <?php Pjax::begin(['timeout' => Yii::$app->params['timeOutPjax'], 'id' => 'idPjaxReload']) ?>
                <?=
                GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],
                        [
                            'attribute' => 'id',
                            'options' => ['class' => 'col-xs-1']
                        ],
                        'name',
                        'slug',
                        [
                            'attribute' => 'price',
                            'format' => 'currency',
                            'options' => ['class' => 'col-xs-1']
                        ],
                        [
                            'attribute' => 'category_id',
                            'value' => function ($model) {
                                return !empty($model->category) ? $model->category->name : null;
                            },
                            'filter' => Select2::widget([
                                'model' => $searchModel,
                                'attribute' => 'category_id',
                                'data' => ArrayHelper::map(Category::findAllActive(), 'id', 'name'),
                                'options' => ['placeholder' => $crudTitles['prompt']],
                                'pluginOptions' => [
                                    'allowClear' => true
                                ],
                            ])
                        ],
                        [
                            'attribute' => 'image_url',
                            'format' => 'html',
                            'value' => function ($model) {
                                return !empty($model->image_url) ? Html::img($model->image_url, ['width' => '100px']) : null;
                            },
                            'filter' => false,
                            'options' => ['class' => 'col-xs-1']
                        ],
                        [
                            'attribute' => 'status',
                            'value' => function ($model)
                            {
                                return $model->statusText;
                            },
                            'format' => 'raw',
                            'filter' => Select2::widget([
                                'model' => $searchModel,
                                'attribute' => 'status',
                                'data' => Course::listStatus(false),
                                'options' => ['placeholder' => $crudTitles['prompt']],
                                'pluginOptions' => [
                                    'allowClear' => true
                                ],
                                'hideSearch' => true,
                            ])
                        ],
                        [
                            'class' => 'app\components\ActionColumnCustom',
                            'template' => '{student}',
                            'buttons' => [
                                'student' => function ($url, $model, $key) {
                                    $url = Url::toRoute(['/teacher/course/student', 'id' => $model->id]);

                                    $options = [
                                        'title' => 'Danh sách học viên',
                                        'data-pjax' => '0',
                                    ];
                                    return Html::a('<span class="glyphicon glyphicon-user"></span>', $url, $options);
                                },
                            ],
                        ],
                    ],
                ]);
                ?>
                <?php Pjax::end() ?>
            </div>
        </div>
    </div>
</div>