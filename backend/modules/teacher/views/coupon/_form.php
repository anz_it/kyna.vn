<?php

use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use kartik\widgets\DateTimePicker;
use kyna\course\models\Course;

$teacherId = Yii::$app->user->id;
?>

<?php $form = ActiveForm::begin([
    'method' => 'post',
]); ?>
    <div class="row">
        <?= $form->field($model, 'code', ['options' => ['class' => 'col-xs-4']]) ?>

        <?= $form->field($model, 'start_date', ['options' => ['class' => 'col-xs-4']])->widget(DateTimePicker::className(), [
            'pluginOptions' => [
                'autoclose' => true,
                'format' => 'dd/mm/yyyy hh:ii',
            ],
            'pluginEvents' => [
                "changeDate" => "function(selected) { 
                    var _userOffset = selected.date.getTimezoneOffset() * 60 * 1000;
                    var startDate = new Date(selected.date.valueOf() + _userOffset);                       
                    var endDate = new Date(startDate.getFullYear(), startDate.getMonth() + 1, startDate.getDate(), startDate.getHours(), startDate.getMinutes());
                    $('#promotion-end_date-datetime').datetimepicker('setStartDate', startDate);
                    $('#promotion-end_date-datetime').datetimepicker('setEndDate', endDate);
                    var toDate = $('#promotion-end_date-datetime').datetimepicker('getDate');
                    if (endDate.getTime() < toDate.getTime()) {
                       $('#promotion-end_date-datetime').datetimepicker('setDate', endDate) 
                    }
                }",
            ]
        ]) ?>

        <?= $form->field($model, 'end_date', ['options' => ['class' => 'col-xs-4']])->widget(DateTimePicker::className(), [
            'pluginOptions' => [
                'autoclose' => true,
                'format' => 'dd/mm/yyyy hh:ii',
            ]
        ]) ?>
    </div>

    <div class="row">
        <?= $form->field($model, 'value', ['options' => ['class' => 'col-xs-4']])->textInput()->label('Phần trăm giảm giá') ?>

        <?= $form->field($model, 'number_usage', ['options' => ['class' => 'col-xs-4']])->textInput() ?>



        <?= $form->field($model, 'user_number_usage', ['options' => ['class' => 'col-xs-4']])->textInput() ?>

    </div>
    <div class="row">
        <?= $form->field($model, 'course_id', ['options' => ['class' => 'col-xs-4']])->checkboxList(ArrayHelper::map(Course::find()
            ->andWhere(['teacher_id' => $teacherId])
            ->andWhere(['!=', 'type', Course::TYPE_COMBO])->andWhere(['!=', 'is_deleted', 1])->andWhere(['status' => 1])
            ->all(), 'id', 'name')
        ) ?>
    </div>

    <div class="form-group">
        <label for="promotion-number_usage" class="control-label"></label>
        <?= Html::submitButton('Thêm', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
        <?= Html::a('Hủy', ['index'], ['class' => 'btn btn-default']) ?>
    </div>
<?php ActiveForm::end(); ?>