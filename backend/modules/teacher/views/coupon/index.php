<?php

use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

use kyna\promotion\models\Promotion;

$crudTitles = Yii::$app->params['crudTitles'];
$crudButtonIcons = Yii::$app->params['crudButtonIcons'];

$formatter = Yii::$app->formatter;

$this->title = 'Mã giảm giá';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="commission-income-view">
    <nav class="navbar navbar-default">
        <?php
            echo Html::a($crudButtonIcons['create'] . ' ' . $crudTitles['create'], ['create'], ['class' => 'btn btn-success navbar-btn navbar-left']);
        ?>
    </nav>
    <div class="box">
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                [
                    'attribute' => 'id',
                    'options' => ['class' => 'col-xs-1']
                ],
                'code',
                [
                    'attribute' => 'value',
                    'format'    => 'raw',
                    'label' => 'Phần trăm giảm giá',
                    'value'     => function($model) use ($formatter){
                        if($model->discount_type == Promotion::TYPE_PERCENTAGE){
                            return "<b>" . $model->value . ' % </b>';
                        }else{
                            return "<b> " . $formatter->asCurrency($model->value) . "</b>";
                        }
                    }
                ],
                'number_usage:integer:Số lần sử dụng tối đa',
                'user_number_usage:integer:Số lần sử dụng của người dùng',
                'current_number_usage',
                [
                    'attribute' => 'start_date',
                    'value' => function ($model) {
                        return !empty($model->start_date) ? gmdate('d/m/Y H:i', $model->start_date) : 'N/A';
                    },
                    'filter' => false,
                ],
                [
                    'attribute' => 'end_date',
                    'value' => function ($model) {
                        return  !empty($model->end_date) ? gmdate('d/m/Y H:i', $model->end_date) : 'N/A';
                    },
                    'filter' => false,
                ],

                [
                    'label' => 'Các khóa học áp dụng',
                    'value' => function ($model) {
                        return implode(', ', ArrayHelper::map($model->promoCourses, 'id', 'course.name'));
                    },

                ],
                [
                    'header' => 'Có thể sử dụng',
                    'format' => 'html',
                    'value' => function ($model) {
                        if ($model->canUse == Promotion::BOOL_YES) {
                            return '<span class="label label-success">' . Promotion::BOOL_YES_TEXT . '</span>';
                        } else {
                            return '<span class="label label-danger">' . Promotion::BOOL_NO_TEXT . '</span>';
                        }
                    }
                ],
                [
                    'attribute' => 'status',
                    'format' => 'raw',
                    'value' => function ($model) {
                        return $model->statusButton;
                    },
                    'filter' => Html::activeDropDownList($searchModel, 'status', Promotion::listStatus(false), ['class' => 'form-control', 'prompt' => $crudTitles['prompt']]),
                ],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{delete} {view}' ,
                    'visibleButtons' => [
                        'view' => Yii::$app->user->can('Coupon.Delete') || Yii::$app->user->can('Teacher'),

                        'delete' => Yii::$app->user->can('Coupon.Delete') || Yii::$app->user->can('Teacher'),
                    ],
                ],
            ],
        ]); ?>
    </div>
</div>
