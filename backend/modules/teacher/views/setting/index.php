<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\Select2;
use app\modules\teacher\models\SettingForm;

$crudTitles = Yii::$app->params['crudTitles'];

$this->title = "Setting";
?>

<div class="group-discount-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'receiveMailType')->radioList($types) ?>
        </div>
    </div>

    <div class="row" id="div-date" style="display: <?= ($model->receiveMailType == SettingForm::RECEIVE_TYPE_ONE_DATE ? 'block' : 'none') ?>;">
        <div class="col-md-3">
            <?= $form->field($model, 'receiveMailDate')->widget(Select2::className(), [
                'data' => [
                    'Sunday' => 'Chủ nhật',
                    'Monday' => 'Thứ Hai',
                    'Tuesday' => 'Thứ Ba',
                    'Wednesday' => 'Thứ Tư',
                    'Thursday' => 'Thứ Năm',
                    'Friday' => 'Thứ Sáu',
                    'Saturday' => 'Thứ Bảy',
                ],
                'hideSearch' => true,
                'options' => ['placeholder' => $crudTitles['prompt']],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]) ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton($crudTitles['update'], ['class' => 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end(); ?>

</div>
<?php
/** @var $this \yii\web\View */
$this->registerJs("
 $('input[name=\'SettingForm[receiveMailType]\'').change(function (e) {
        var checkedInput = $('input[name=\'SettingForm[receiveMailType]\']:checked');
        
        if (checkedInput.val() == '" . SettingForm::RECEIVE_TYPE_ONE_DATE . "') {
            $('#div-date').show();
        } else {
            $('#div-date').hide();
        }
    });
");
?>
