<?php

namespace app\modules\teacher;

use yii\filters\AccessControl;
use kyna\base\BaseModule;

/**
 * teacher module definition class
 */
class Module extends BaseModule
{

    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\teacher\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }

    /**
     * @inheritdoc
     */

}
