<?php
/**
 * Created by PhpStorm.
 * User: macintosh
 * Date: 4/27/17
 * Time: 11:34 AM
 */

namespace app\modules\teacher\controllers;

use kyna\promotion\models\PromotionSearch;
use Yii;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;

use kyna\promotion\models\Promotion;

use app\components\controllers\Controller;

class CouponController extends Controller
{

    public $mainTitle = 'Teacher Vouchers';

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['Teacher'],
                    ],
                ],
            ],
        ];
    }


    public function actionIndex()
    {
        $id = Yii::$app->user->id;
        $searchModel = new PromotionSearch();
        $searchModel->issued_person = $id;

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCreate()
    {
        $id = Yii::$app->user->id;

        $model = $this->newModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if (!$model->canTeacherCreate()) {
                Yii::$app->session->setFlash('warning', 'Không thể tạo nhiều hơn 100 mã trong tháng.');
            } else {
                if ($model->save()) {
                    Yii::$app->session->setFlash('success', 'Thêm thành công.');
                    return $this->redirect(['index']);
                }
            }
        }

        return $this->render('create', ['model' => $model]);
    }

    public function newModel($teacher_id)
    {
        $model = new Promotion();
        $model->setScenario('backend-teacher');
        $model->issued_person = $teacher_id;
        $model->created_by = Yii::$app->user->id;
        $model->type = Promotion::KIND_COURSE_APPLY;
        $model->discount_type = Promotion::TYPE_PERCENTAGE;
        $model->status = Promotion::BOOL_YES;
        $model->apply_scope = Promotion::APPLY_SCOPE_ALL;
        $model->current_number_usage = 0;
        $model->user_number_usage = 1;
        $model->number_usage = 1;
        return $model;
    }

    protected function findModel($id)
    {
        if (($model = Promotion::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}