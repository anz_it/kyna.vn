<?php
/**
 * Created by PhpStorm.
 * User: macintosh
 * Date: 4/27/17
 * Time: 10:29 AM
 */

namespace app\modules\teacher\controllers;

use Yii;
use app\components\controllers\Controller;
use kyna\course\models\search\CourseSearch;
use kyna\user\models\search\UserCourseSearch;
use yii\filters\AccessControl;

class CourseController extends Controller
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['Teacher'],
                    ],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $searchModel = new CourseSearch();
        $searchModel->teacher_id = Yii::$app->user->id;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'queryParams' => Yii::$app->request->queryParams
        ]);
    }

    public function actionStudent($id)
    {
        $searchModel = new UserCourseSearch();
        $searchModel->course_id = $id;

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        if (Yii::$app->request->isAjax && Yii::$app->request->post('type', null) == 'export') {
            $sql = $dataProvider->query->createCommand()->getRawSql();
            $sql = str_replace('`', '', $sql);
            $email = empty(Yii::$app->request->post('email'))?Yii::$app->user->identity->email:Yii::$app->request->post('email');
            // execute command in console
            $rootPath = \Yii::getAlias('@root');
            $exportLog = \Yii::getAlias('@console/runtime/export_course_student.log');
            $exportErrorLog = \Yii::getAlias('@console/runtime/export_course_student-error.log');
            // check log file exist
            self::_checkExistFile($exportLog);
            self::_checkExistFile($exportErrorLog);
            $command = "php {$rootPath}/yii export/run course-student \"{$sql}\" {$email}" . " > {$exportLog} 2>>{$exportErrorLog} &";
            exec($command);
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'result' => true,
            ];
        }

        return $this->render('student', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
}