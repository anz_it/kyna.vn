<?php
/**
 * Created by PhpStorm.
 * User: macintosh
 * Date: 4/27/17
 * Time: 3:40 PM
 */

namespace app\modules\teacher\controllers;

use Yii;
use kyna\course\models\Teacher;
use kyna\settings\models\TeacherSetting;
use app\components\controllers\Controller;
use app\modules\teacher\models\SettingForm;
use yii\filters\AccessControl;

class SettingController extends Controller
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['Teacher'],
                    ],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $model = new SettingForm();

        $teacher = Teacher::findOne(Yii::$app->user->id);
        $receiveDate = $teacher->getReceiveQnaDate();
        if ($receiveDate != null) {
            $model->receiveMailType = SettingForm::RECEIVE_TYPE_ONE_DATE;
            $model->receiveMailDate = $receiveDate;
        } else {
            $model->receiveMailType = SettingForm::RECEIVE_TYPE_AFTER_APPROVE;
        }
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $receiveDate = $model->receiveMailDate;
            if ($model->receiveMailType == SettingForm::RECEIVE_TYPE_AFTER_APPROVE) {
                $receiveDate = null;
            }

            $settingModel = TeacherSetting::findOne([
                'teacher_id' => $teacher->id,
                'key' => TeacherSetting::KEY_RECEIVE_MAIL_DATE,
            ]);

            if ($settingModel == null) {
                $settingModel = new TeacherSetting();
                $settingModel->teacher_id = $teacher->id;
                $settingModel->key = TeacherSetting::KEY_RECEIVE_MAIL_DATE;
            }

            $settingModel->value = $receiveDate;

            if ($settingModel->save()) {
                Yii::$app->session->setFlash('success', 'Cập nhật thành công.');
            }
        }
        return $this->render('index', [
            'model' => $model,
            'types' => SettingForm::getReceiveTypeOptions()
        ]);
    }
}