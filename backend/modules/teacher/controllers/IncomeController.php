<?php

namespace app\modules\teacher\controllers;

use Yii;
use app\modules\commission\models\Order;
use kyna\commission\models\CommissionIncome;
use kyna\commission\models\search\CommissionIncomeSearch;
use yii\db\Expression;
use yii\filters\AccessControl;

/**
 * Created by PhpStorm.
 * User: macintosh
 * Date: 4/25/17
 * Time: 10:26 AM
 */
class IncomeController extends \app\components\controllers\Controller
{

    /**
     * Show income from teaching commission
     * @return view
     */

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['Teacher'],
                    ],
                ],
            ],
        ];
    }

    public function actionTeaching()
    {
        $id = Yii::$app->user->id;
        $searchModel = new CommissionIncomeSearch();

        $searchModel->teacher_id = $id;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        list($totalReg, $totalAmount, $totalRegFilter, $totalAmountFilter, $totalRealIncomeFilter, $totalRealIncome) = $this->extractSummaryVarriables($id, $searchModel->course_id, $searchModel->date_ranger);

        $roles = implode(",",
            array_map(function ($p) { return $p->item_name; }, \kyna\user\models\AuthAssignment::find()->where(['user_id' => Yii::$app->user->id])->all())
        );
        $isTeacher = false;
        //check is teacher
        if (strpos($roles, "Teacher") !== false) {
            $isTeacher = true;
        }

        return $this->render('teaching', [
            'isTeacher' => $isTeacher,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'totalReg' => $totalReg,
            'totalRegFilter' => $totalRegFilter,
            'totalAmount' => $totalAmount,
            'totalAmountFilter' => $totalAmountFilter,
            'totalRealIncomeFilter' => $totalRealIncomeFilter,
            'totalRealIncome' => $totalRealIncome
        ]);
    }

    private function extractSummaryVarriables($teacher_id, $course_id = 0, $date_ranger = '')
    {
        $query = CommissionIncome::find();
        $query->andWhere(['teacher_id' => $teacher_id]);
        $query->andWhere(['!=', 'teacher_commission_percent', 0]);
        $query->andWhere(['!=', 'course_id', 10]);

        $totalReg = $query->count('id');
        $totalAmount = $query->sum('teacher_commission_amount');
        
        //all commission
        $totalRealIncome = $query->sum('(total_amount - total_amount *  acp_with_teacher / 100)');


        $query->andFilterWhere(['course_id' => $course_id]);

        if (!empty($date_ranger)) {
            $dateRange = explode(' - ', $date_ranger);
            $beginDate = date_create_from_format('d/m/Y H:i:s', $dateRange[0] . ' 00:00:00');
            $endDate = date_create_from_format('d/m/Y H:i:s', $dateRange[1] . ' 23:59:59');

            $query->andWhere(['>=', 'created_time', $beginDate->getTimestamp()]);
            $query->andWhere(['<=', 'created_time', $endDate->getTimestamp()]);
        }

        $totalRegFilter = $query->count('id');
        $totalAmountFilter = $query->sum('teacher_commission_amount');

        //all commission filter
        $totalRealIncomeFilter = $query->sum('(total_amount - total_amount *  acp_with_teacher / 100)');


        return [
            $totalReg,
            $totalAmount,
            $totalRegFilter,
            $totalAmountFilter,
            $totalRealIncomeFilter,
            $totalRealIncome,
        ];
    }

}