<?php
/**
 * Created by PhpStorm.
 * User: macintosh
 * Date: 4/25/17
 * Time: 10:39 AM
 */

namespace app\modules\teacher\controllers;

use Yii;
use yii\web\Response;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;
use common\helpers\StringHelper;
use kyna\course\models\CourseLearnerQna;
use kyna\course\models\search\CourseLearnerQnaSearch;

class QnaController extends \app\components\controllers\Controller
{

    /**
     * Lists all CourseLearnerQna models.
     * @return mixed
     */


    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['Teacher','TeachingAssistant'],
                    ],
                ],
            ],
        ];
    }


    public function actionIndex()
    {
        $searchModel = new CourseLearnerQnaSearch();
        $searchModel->is_approved = CourseLearnerQnaSearch::BOOL_YES;
        $searchModel->teacherId = Yii::$app->user->id;
        $dataProvider = $searchModel->teacherSearch(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionAnswer($question_id)
    {
        $model = new CourseLearnerQna();
        $model->setScenario('answer');
        $model->question_id = $question_id;
        $model->course_id = $model->question->course_id;
        $model->is_approved = CourseLearnerQna::BOOL_YES;
        $model->is_send_to_teacher = CourseLearnerQna::BOOL_YES;
        $needSendMail = false;

        if (!$model->is_send_answered) {
            $needSendMail = true;
            $model->is_send_answered = CourseLearnerQna::BOOL_YES;
        }

        if ($model->load(Yii::$app->request->post())) {
            // check is_null answer
            $successMsg = 'Trả lời thành công';

            if ($model->save()) {
                if ($needSendMail) {
                    $subject = "[Kyna.vn] Câu hỏi #{$question_id} của bạn đã được giảng viên trả lời";

                    Yii::$app->mailer->compose()
                        ->setHtmlBody($this->renderPartial('@common/mail/qa/notify_answer', [
                            'questionId' => $question_id,
                            'fullname' => $model->question->user->profile->name,
                            'courseName' => $model->course->name,
                            'questionContent' => StringHelper::truncate(strip_tags($model->question->content), 30),
                            'answerContent' => strip_tags($model->content),
                            'qaLink' => $model->question->qaLink
                        ], true))
                        ->setTo($model->question->user->email)
                        ->setSubject($subject)
                        ->send();

                    $successMsg .= ' và đã gửi mail tới học viên ' . $model->question->user->email;
                }

            }

            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'success' => true,
                'message' => $successMsg,
            ];
        }

        return $this->renderAjax('_form', ['model' => $model]);
    }

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model->setScenario('update');

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            // check is_null answer
            $successMsg = 'Cập nhật thành công';

            Yii::$app->response->format = Response::FORMAT_JSON;
            $ret = [
                'success' => true,
                'message' => $successMsg,
            ];
            return $ret;
        }

        return $this->renderAjax('_form', ['model' => $model]);
    }

    protected function findModel($id)
    {
        if (($model = CourseLearnerQna::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Không tìm thấy.');
        }
    }
}