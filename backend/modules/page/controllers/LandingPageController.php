<?php

namespace app\modules\page\controllers;

use Yii;
use kyna\page\models\Page;
use kyna\page\models\search\PageSearch;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * LandingPageController implements the CRUD actions for Page model.
 */
class LandingPageController extends \app\components\controllers\Controller
{
    
    public $mainTitle = 'Quản lý Landing Page';
    
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'view'],
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->user->can('LandingPage.View');
                        }
                    ],
                    [
                        'allow' => true,
                        'actions' => ['create'],
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->user->can('LandingPage.Create');
                        }
                    ],
                    [
                        'allow' => true,
                        'actions' => ['update', 'change-status'],
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->user->can('LandingPage.Update');
                        }
                    ],
                    [
                        'allow' => true,
                        'actions' => ['delete'],
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->user->can('LandingPage.Delete');
                        }
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all Page models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PageSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCreate()
    {
        $model = new Page();
        
        $model->on(Page::EVENT_AFTER_INSERT, [$this, 'flushCache']);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', 'Thêm thành công.');
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }
    
    public function flushCache() {
        //$cache = new yii\caching\FileCache();
        //$cache->flush();
        
        // $model->on(Page::EVENT_AFTER_DELETE, [$this, 'flushCache']);
    }

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        
        $model->on(Page::EVENT_AFTER_UPDATE, [$this, 'flushCache']);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', 'Cập nhật thành công.');
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    protected function findModel($id)
    {
        if (($model = Page::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
