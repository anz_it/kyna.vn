<?php

namespace app\modules\page\controllers;

use Yii;
use yii\filters\AccessControl;

class FileManagerController extends \app\components\controllers\Controller
{
    
    public $layout = '@app/views/layouts/lte';
    
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->user->can('FileManager.View');
                        },
                    ],
                ],
            ]
        ];
    }
    
    public function actionIndex()
    {
        return $this->render('index');
    }
}
