<?php

use mihaildev\elfinder\ElFinder;
use yii\web\JsExpression;

$this->title = "File Manager";
?>

<div class="row">
    <div class="col-xs-12">
        <div class="promotion-schedule-index">
            <?= ElFinder::widget([
                'language'         => 'vi',
                'controller'       => 'elfinder',
                'filter'           => [],
                'callbackFunction' => new JsExpression('function(file, id){}'),
                'containerOptions' => [
                    'style' => 'height: 700px;'
                ],
            ]);
            ?>
        </div>
    </div>
</div>

