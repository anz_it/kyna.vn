<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Url;

use kyna\page\models\Page;

$this->title = 'Quản lý Landing Pages';
$this->params['breadcrumbs'][] = $this->title;

$crudTitles = Yii::$app->params['crudTitles'];
$crudButtonIcons = Yii::$app->params['crudButtonIcons'];

$user = Yii::$app->user;
?>
<div class="page-index">
    <p>
        <?php if ($user->can('LandingPage.Create')) : ?>
            <?= Html::a($crudButtonIcons['create'] . ' ' . $crudTitles['create'], ['create'], ['class' => 'btn btn-success']) ?>
        <?php endif; ?>
    </p>
<?php Pjax::begin(['timeout' => Yii::$app->params['timeOutPjax'], 'id' => 'idPjaxReload']); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'id',
                'contentOptions' => ['style' => 'width: 100px;']
            ],
            'title',
            'slug',
            [
                'attribute' => 'is_payment',
                'format' => 'raw',
                'value' => function ($model) {
                    return $model->getStatusButton(null, 'is_payment');
                },
                'filter' => Html::activeDropDownList($searchModel, 'is_payment', Page::listStatus(false), ['class' => 'form-control', 'prompt' => $crudTitles['prompt']]),
            ],
            [
                'attribute' => 'is_redirect',
                'format' => 'raw',
                'value' => function ($model) {
                    return $model->getStatusButton(null, 'is_redirect');
                },
                'filter' => Html::activeDropDownList($searchModel, 'is_redirect', Page::listStatus(false), ['class' => 'form-control', 'prompt' => $crudTitles['prompt']]),
            ],
            [
                'attribute' => 'status',
                'format' => 'raw',
                'value' => function ($model) {
                    return $model->statusButton;
                },
                'filter' => Html::activeDropDownList($searchModel, 'status', Page::listStatus(false), ['class' => 'form-control', 'prompt' => $crudTitles['prompt']]), 
            ],
            [
                'class' => 'app\components\ActionColumnCustom',
                'visibleButtons' => [
                    'update' => $user->can('LandingPage.Update'),
                    'view' => $user->can('LandingPage.View'),
                    'delete' => $user->can('LandingPage.Delete'),
                ],
            ],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
