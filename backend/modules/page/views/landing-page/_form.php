<?php

use yii\bootstrap\Html;
use yii\widgets\ActiveForm;
use common\widgets\codeeditor\CodeEditor;
use kyna\page\models\Page;

use kartik\widgets\Select2;

$crudTitles = Yii::$app->params['crudTitles'];
?>

<div class="becourse-form">
Page
    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <div class="row">
        <div class="col-lg-4">
            <?=
            $form->field($model, 'title')->textInput([
                'maxlength' => true,
                'onchange' => "$('#cat-slug').val(convertToSlug($(this).val()))",
            ])
            ?>
        </div>
        <div class="col-lg-4">
            <?= $form->field($model, 'slug')->textInput(['id' => 'cat-slug']) ?>
        </div>
        <div class="col-lg-4">
            <?=
            $form->field($model, 'status')->widget(Select2::classname(), [
                'data' => Page::listStatus(),
                'hideSearch' => true,
                'options' => ['placeholder' => $crudTitles['prompt']],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ])
            ?>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-2">
            <?=
            $form->field($model, 'is_payment')->widget(Select2::classname(), [
                'data' => Page::listStatus(),
                'hideSearch' => true,
                'options' => ['placeholder' => $crudTitles['prompt']],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ])
            ?>
        </div>
        <div class="col-lg-2">
            <?=
            $form->field($model, 'is_redirect')->widget(Select2::classname(), [
                'data' => Page::listStatus(),
                'hideSearch' => true,
                'options' => ['placeholder' => $crudTitles['prompt']],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ])
            ?>
        </div>
        <div class="col-lg-4">
            <?php
            echo $form->field($model, 'redirect_url')->hint('Link Redirect khi khóa học bị ẩn, để trống nếu muốn sử dụng redirect link mặc định');
            ?>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <b>SEO</b>
                </div>
                <div class="panel-body">
                    <?= $form->field($model, 'seo_is_sitemap')->checkbox() ?>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-6">
            <label class="control-label" for="external_script"><?php echo $model->getAttributeLabel('external_script'); ?></label>
            <?=
            CodeEditor::widget([
                'model' => $model,
                'attribute' => 'external_script'
            ]);
            ?>
        </div>
        <div class="col-lg-6">
            <label class="control-label" for="success_script"><?php echo $model->getAttributeLabel('success_script'); ?></label>
            <?=
            CodeEditor::widget([
                'model' => $model,
                'attribute' => 'success_script'
            ]);
            ?>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <label class="control-label" for="content"><?php echo $model->getAttributeLabel('content'); ?></label>
            <?=
            CodeEditor::widget([
                'model' => $model,
                'attribute' => 'content',
                'language' => 'htmlmixed',
            ]);
            ?>
        </div>
    </div>

    <div class="row" style="margin-top: 10px;">

        <div class="col-lg-3">
            <label class="control-label col-lg-12">&nbsp;</label>
            <?= Html::submitButton($model->isNewRecord ? $crudTitles['create'] : $crudTitles['update'], ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            <?= Html::a($crudTitles['cancel'], ['index'], ['class' => 'btn btn-default']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?= $model->isNewRecord ? $this->render('//_partials/stringjs') : '' ?>