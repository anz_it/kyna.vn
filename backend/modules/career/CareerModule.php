<?php

namespace app\modules\career;

/**
 * career module definition class
 */
class CareerModule extends \kyna\base\BaseModule
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\career\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
