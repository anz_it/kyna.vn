<?php

namespace app\modules\career\controllers;

use yii\web\Controller;

/**
 * Default controller for the `career` module
 */
class DefaultController extends \app\components\controllers\Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
}
