<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model kyna\career\models\Career */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Careers'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="becourse-view">

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'title',
            'short_title',
            'slug',
            [
                'attribute' => 'category_id',
                'value' => $model->getCategoryText()
            ],
            [
                'attribute' => 'language_id',
                'value' => $model->getLanguageText()
            ],
            'content:html',
            'quantity',
            'publish_date:date',
            'end_date:date',
            [
                'attribute' => 'status',
                'value' => $model->statusText
            ],
            'created_time:datetime',
            'updated_time:datetime',
        ],
    ]) ?>

</div>
