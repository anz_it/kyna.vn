<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use kartik\select2\Select2;
use kyna\career\models\Career;

/* @var $this yii\web\View */
/* @var $searchModel kyna\career\models\search\CareerSearch_ */
/* @var $dataProvider yii\data\ActiveDataProvider */

$crudTitles = Yii::$app->params['crudTitles'];
$crudButtonIcons = Yii::$app->params['crudButtonIcons'];

$this->title = Yii::$app->controller->mainTitle;
$this->params['breadcrumbs'][] = $this->title;

$user = Yii::$app->user;
?>
<div class="row career-index">
    <div class="col-xs-12">
        <div class="beuser-index">
            <p>
                <?= Html::a($crudButtonIcons['create'] . ' ' . $crudTitles['create'], ['create'], ['class' => 'btn btn-success']) ?>
            </p>
            <div class="box">
                <?php Pjax::begin(['timeout' => Yii::$app->params['timeOutPjax'], 'id' => 'idPjaxReload']) ?>
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],

                        //'id',
                        [
                            'attribute' => 'title',
                            'value' => function ($model) {
                                return Html::a($model->title, $model->getPreviewUrl(), ['title' => 'Preview', 'target' => '_blank']);
                            },
                            'format' => 'raw',
                        ],
                        'slug',
                        [
                            'attribute' => 'category_id',
                            'value' => function ($model) {
                                return $model->getCategoryText();
                            },
                            'format' => 'html',
                            'filter' => Select2::widget([
                                'model' => $searchModel,
                                'attribute' => 'category_id',
                                'data' => Career::$category,
                                'options' => ['placeholder' => $crudTitles['prompt']],
                                'pluginOptions' => [
                                    'allowClear' => true
                                ],
                                'hideSearch' => false,
                            ])
                        ],
                        'quantity',
                        [
                            'attribute' => 'end_date',
                            'label' => 'Hết hạn?',
                            'format' => 'html',
                            'value' => function ($model) {
                                if ($model->end_date < time()) {
                                    return '<span class="label label-default">Hết hạn</span>';
                                }
                                return '<span class="label label-success">Chưa hết hạn</span>';
                            },
                            'contentOptions' => [
                                'style' => 'min-width: 115px;'
                            ],
                            'filter' => Html::activeDropDownList($searchModel, 'end_date', Career::$date_status, ['class' => 'form-control', 'prompt' => $crudTitles['prompt']]),
                        ],
                        [
                            'attribute' => 'status',
                            'format' => 'raw',
                            'value' => function ($model) {
                                return $model->statusButton;
                            },
                            'contentOptions' => [
                                'style' => 'min-width: 115px;'
                            ],
                            'filter' => Html::activeDropDownList($searchModel, 'status', Career::listStatus(false), ['class' => 'form-control', 'prompt' => $crudTitles['prompt']]),
                        ],
                        [
                            'class' => 'app\components\ActionColumnCustom',
                            'visibleButtons' => [
                                'update' => $user->can('Career.Update'),
                                'view' => $user->can('Career.View'),
                                'delete' => $user->can('Career.Delete'),
                            ],
                        ],
                    ],
                ]); ?>
                <?php Pjax::end() ?>
            </div>
        </div>
    </div>
</div>
