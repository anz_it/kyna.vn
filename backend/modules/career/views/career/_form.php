<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use kyna\career\models\Career;
use kartik\datetime\DateTimePicker;
use kartik\time\TimePicker;
use common\widgets\tinymce\TinyMce;
use \kartik\widgets\DatePicker;

/* @var $this yii\web\View */
/* @var $model kyna\career\models\Career */
/* @var $form yii\widgets\ActiveForm */

$crudTitles = Yii::$app->params['crudTitles'];

$model->end_date = Yii::$app->formatter->asDatetime($model->end_date, Yii::$app->formatter->dateFormat);
$model->publish_date = Yii::$app->formatter->asDatetime($model->publish_date, Yii::$app->formatter->dateFormat);
?>

<div class="becourse-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <div class="row">
        <div class="col-lg-4">
            <?=
            $form->field($model, 'title')->textInput([
                'maxlength' => true
            ])
            ?>
        </div>
        <div class="col-lg-4">
            <?=
            $form->field($model, 'category_id')->widget(Select2::classname(), [
                'data' => Career::$category,
                'hideSearch' => true,
                'options' => ['placeholder' => $crudTitles['prompt']],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ])
            ?>
        </div>
        <div class="col-lg-4">
            <?=
            $form->field($model, 'language_id')->widget(Select2::classname(), [
                'data' => Career::$language,
                'hideSearch' => true,
                'options' => ['placeholder' => $crudTitles['prompt']],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-6">
            <?=
            $form->field($model, 'short_title')->textInput([
                'maxlength' => true,
                'onchange' => "$('#cat-slug').val(convertToSlug($(this).val()))",
            ])
            ?>
        </div>
        <div class="col-lg-6">
            <?= $form->field($model, 'slug')->textInput([
                'id' => 'cat-slug',
                'maxlength' => true,
            ]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-4">
            <?= $form->field($model, 'publish_date')->widget(DatePicker::className(), [
                'pluginOptions' => [
                    'autoclose' => true,
                    'format' => 'dd/mm/yyyy',
                    //'todayHighlight' => true,
                    'orientation' => "bottom left",
                ]
            ]) ?>
        </div>
        <div class="col-lg-4">
            <?= $form->field($model, 'end_date')->widget(DatePicker::className(), [
                'pluginOptions' => [
                    'autoclose' => true,
                    'format' => 'dd/mm/yyyy',
                    //'todayHighlight' => true,
                    'orientation' => "bottom left",
                ]
            ]) ?>
        </div>
        <div class="col-lg-2">
            <?= $form->field($model, 'quantity')->textInput([
                'type' => 'number'
            ]) ?>
        </div>
        <div class="col-lg-2">
            <?= $form->field($model, 'status')->widget(Select2::classname(), [
                'data' => Career::listStatus(),
                'hideSearch' => true,
                'options' => ['placeholder' => $crudTitles['prompt']],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ])
            ?>
        </div>
    </div>

    <?= $form->field($model, 'content')->widget(TinyMce::className(), [
        'layout' => 'full',
    ])
    ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<?= $this->render('//_partials/stringjs') ?>