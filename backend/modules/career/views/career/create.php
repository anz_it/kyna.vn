<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model kyna\career\models\Career */

$this->title = Yii::$app->params['crudTitles']['create'];
$this->params['breadcrumbs'][] = ['label' => Yii::$app->controller->mainTitle, 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="becourse-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
