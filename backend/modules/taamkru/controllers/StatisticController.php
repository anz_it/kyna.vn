<?php

namespace app\modules\taamkru\controllers;

use common\helpers\PDFHelper;
use kyna\taamkru\models\Code;
use kyna\taamkru\models\form\CreateCodeForm;
use kyna\taamkru\models\form\CreateOrderForm;
use kyna\taamkru\models\Retailer;
use kyna\taamkru\models\search\StatisticSearch;
use Yii;
use yii\data\ArrayDataProvider;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;
use yii\helpers\Url;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;

use kyna\user\models\Profile;
use kyna\order\models\Order;
use kyna\order\models\OrderDetails;
use kyna\user\models\User;
use kyna\order\models\actions\OrderAction;
use kyna\payment\models\PaymentMethod;

use common\helpers\StringHelper;

use kyna\taamkru\models\search\OrderSearch;
use app\modules\order\models\ActionForm;
use yii\web\Response;

/**
 * OrderController implements the CRUD actions for Order model.
 */
class StatisticController extends \yii\web\Controller
{

    public $layout;
    public $mainTitle = 'Thống kê';

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['retailer'],
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->user->isTaamkruRetailer;
                        }
                    ],
                ],
            ],
        ];
    }

    public function actionRetailer()
    {
        $searchModel = $this->loadSearchModel();
        $retailer = Retailer::findOne(['user_id' => Yii::$app->user->id]);
        $searchModel->retailer_id = $retailer->id;
        $searchModel->status = Order::ORDER_STATUS_COMPLETE;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $totalOrder = $dataProvider->query->count();
        $totalPrice = !is_null($dataProvider->query->sum('total')) ? $dataProvider->query->sum('total') : 0;
        $totalUser = $dataProvider->query->count('DISTINCT(user_id)');

        return $this->render('retailer', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'totalOrder' => $totalOrder,
            'totalPrice' => $totalPrice,
            'totalUser' => $totalUser
        ]);
    }

    private function _checkExistFile($file)
    {
        clearstatcache();
        if (!file_exists($file)) {
            touch($file);
        }
    }

    public function loadSearchModel()
    {
        $searchModel = new StatisticSearch();
        $searchModel->is_done_telesale_process = OrderSearch::BOOL_YES;

        return $searchModel;
    }

    protected function findModel($id)
    {
        if (($model = Order::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    private function _detectUser($string)
    {
        //$userQuery = User::find();
        $stringType = StringHelper::typeDetect($string);
        switch ($stringType) {
            case StringHelper::STRING_TYPE_EMAIL:
                $params['email'] = $string;
                if ($user = User::find()->where($params)->one()) {
                    return $user->id;
                }

                return false;
            case StringHelper::STRING_TYPE_PHONE_NUMBER:
                $params['phone_number'] = $string;
                if ($userAddress = Profile::find()->select('user_id')->where($params)->asArray()->all()) {
                    return ArrayHelper::getColumn($userAddress, 'user_id');
                }

                return false;
            default:
                return false;
        }
    }

}
