<?php

namespace app\modules\taamkru\controllers;

use common\helpers\PDFHelper;
use kyna\taamkru\models\Code;
use kyna\taamkru\models\form\CreateCodeForm;
use kyna\taamkru\models\form\CreateOrderForm;
use kyna\taamkru\models\Retailer;
use Yii;
use yii\data\ArrayDataProvider;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;
use yii\helpers\Url;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;

use kyna\user\models\Profile;
use kyna\order\models\Order;
use kyna\order\models\OrderDetails;
use kyna\user\models\User;
use kyna\order\models\actions\OrderAction;
use kyna\payment\models\PaymentMethod;

use common\helpers\StringHelper;

use kyna\taamkru\models\search\OrderSearch;
use app\modules\order\models\ActionForm;
use yii\web\Response;

/**
 * OrderController implements the CRUD actions for Order model.
 */
class OrderController extends \yii\web\Controller
{

    public $layout;
    public $mainTitle = 'Quản lý Đơn hàng';

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index'],
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->user->can('Taamkru.Order.View');
                        }
                    ],
                    [
                        'allow' => true,
                        'actions' => ['retailer'],
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->user->isTaamkruRetailer;
                        }
                    ],
                    [
                        'allow' => true,
                        'actions' => ['create-cod', 'create-email'],
                        'matchCallback' => function ($rule, $action) {
                            $retailer = Retailer::findOne(['user_id' => Yii::$app->user->id, 'status' => Retailer::STATUS_ACTIVE]);
                            return Yii::$app->user->isTaamkruRetailer && !empty($retailer);
                        }
                    ],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $this->loadParams($params);

        $searchModel = $this->loadSearchModel();
        $searchModel->attributes = $params;
        $dataProvider = $searchModel->search($params);
        $newOrderCount = $dataProvider->totalCount;

        if (Yii::$app->request->isAjax && Yii::$app->request->post('type', null) == 'export') {
            // init pagination for raw SQL
            $sql = $dataProvider->query->createCommand()->getRawSql();
            $sql = str_replace('`', '', $sql);
            $email = empty(Yii::$app->request->post('email'))?Yii::$app->user->identity->email:Yii::$app->request->post('email');
            // execute command in console
            $rootPath = \Yii::getAlias('@root');
            $exportLog = \Yii::getAlias('@console/runtime/export_taamkru_order.log');
            $exportErrorLog = \Yii::getAlias('@console/runtime/export_taamkru_order-error.log');
            // check log file exist
            self::_checkExistFile($exportLog);
            self::_checkExistFile($exportErrorLog);
            $command = "php {$rootPath}/yii export/run taamkru-order \"{$sql}\" {$email}" . " > {$exportLog} 2>>{$exportErrorLog} &";
            exec($command);
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'result' => true,
            ];
        }

        return $this->render('index', [
            'queryParams' => $params,
            'dataProvider' => $dataProvider,
            'statusLabelCss' => Order::getStatusLabelCss(),
            'callStatuses' => OrderAction::callStatuses(),
            'searchModel' => $searchModel
        ]);
    }

    public function actionRetailer()
    {
        $this->loadParams($params);

        $searchModel = $this->loadSearchModel();
        $searchModel->attributes = $params;
        $retailer = Retailer::findOne(['user_id' => Yii::$app->user->id]);
        $searchModel->retailer_id = $retailer->id;
        $searchModel->status = Order::ORDER_STATUS_COMPLETE;
        $dataProvider = $searchModel->search($params);
        $newOrderCount = $dataProvider->totalCount;

        if (Yii::$app->request->isAjax && Yii::$app->request->post('type', null) == 'export') {
            // init pagination for raw SQL
            $sql = $dataProvider->query->createCommand()->getRawSql();
            $sql = str_replace('`', '', $sql);
            $email = empty(Yii::$app->request->post('email'))?Yii::$app->user->identity->email:Yii::$app->request->post('email');
            // execute command in console
            $rootPath = \Yii::getAlias('@root');
            $exportLog = \Yii::getAlias('@console/runtime/export_taamkru_order.log');
            $exportErrorLog = \Yii::getAlias('@console/runtime/export_taamkru_order-error.log');
            // check log file exist
            self::_checkExistFile($exportLog);
            self::_checkExistFile($exportErrorLog);
            $command = "php {$rootPath}/yii export/run taamkru-order-retailer \"{$sql}\" {$email}" . " > {$exportLog} 2>>{$exportErrorLog} &";
            exec($command);
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'result' => true,
            ];
        }

        return $this->render('retailer', [
            'queryParams' => $params,
            'dataProvider' => $dataProvider,
            'statusLabelCss' => Order::getStatusLabelCss(),
            'callStatuses' => OrderAction::callStatuses(),
            'searchModel' => $searchModel
        ]);
    }

    private function _checkExistFile($file)
    {
        clearstatcache();
        if (!file_exists($file)) {
            touch($file);
        }
    }

    public function loadSearchModel()
    {
        $searchModel = new OrderSearch();
        $searchModel->is_done_telesale_process = OrderSearch::BOOL_YES;

        return $searchModel;
    }

    private function loadParams(&$params)
    {
        if ($status = Yii::$app->request->get('status')) {
            $params['status'] = Yii::$app->request->get('status');
        } else {
            if ($search = Yii::$app->request->get('s')) {
                $params['s'] = trim($search);

                if ($userId = $this->_detectUser($search)) {
                    $params['user_id'] = $userId;
                } elseif (StringHelper::typeDetect($search) === StringHelper::STRING_TYPE_PHONE_NUMBER) {
                    $params['phone_number'] = $search;
                } else {
                    $params['id'] = (int)$search;
                }
            }
        }

        if (Yii::$app->request->get('date-range')) {
            $dateRange = explode(' - ', Yii::$app->request->get('date-range'));
            if (Yii::$app->controller->action->id == 'print-cod' || Yii::$app->controller->action->id == 'update-cod') {
                $beginDate = date_create_from_format('d/m/Y H:i', $dateRange[0]);
                $endDate = date_create_from_format('d/m/Y H:i', $dateRange[1]);
            } else {
                $beginDate = date_create_from_format('d/m/Y H:i:s', $dateRange[0] . ' 00:00:00');
                $endDate = date_create_from_format('d/m/Y H:i:s', $dateRange[1] . ' 00:00:00');
            }

            $params['from_date'] = $beginDate->getTimestamp();
            $params['to_date'] = $endDate->getTimestamp();
            $params['date-range'] = Yii::$app->request->get('date-range');
        }

        if (Yii::$app->request->get('print_type')) {
            $params['print_type'] = Yii::$app->request->get('print_type');
        }

        if (Yii::$app->request->get('retailer_id')) {
            $params['retailer_id'] = Yii::$app->request->get('retailer_id');
        }

        $params['call_status'] = Yii::$app->request->get('call_status');
    }

    protected function findModel($id)
    {
        if (($model = Order::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    private function _detectUser($string)
    {
        //$userQuery = User::find();
        $stringType = StringHelper::typeDetect($string);
        switch ($stringType) {
            case StringHelper::STRING_TYPE_EMAIL:
                $params['email'] = $string;
                if ($user = User::find()->where($params)->one()) {
                    return $user->id;
                }

                return false;
            case StringHelper::STRING_TYPE_PHONE_NUMBER:
                $params['phone_number'] = $string;
                if ($userAddress = Profile::find()->select('user_id')->where($params)->asArray()->all()) {
                    return ArrayHelper::getColumn($userAddress, 'user_id');
                }

                return false;
            default:
                return false;
        }
    }

    public function actionCreateCod()
    {
        $model = new CreateOrderForm();

        $model->payment_method = 'cod';
        $model->setScenario($model::SCENARIO_COD);

        $model->operator_id = \Yii::$app->user->id;
        if ($model->load(\Yii::$app->request->post()) && $model->validate()) {
            if (intval($model->shipping_location_id) <= 0) {
                $model->addError('shipping_location_id', 'Vui lòng chọn địa điểm');
            } elseif ($order = $model->create()) {
                // completed order
                if (Order::complete($order->id, $order->total)) {
                    Yii::$app->session->setFlash('success', 'Thêm thành công');
                    $this->redirect(['retailer']);
                } else {
                    Yii::$app->session->setFlash('warning', 'Không thể hoàn thành, vui lòng kiểm tra lại thông tin');
                }
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionCreateEmail()
    {
        $model = new CreateOrderForm();

        $model->payment_method = 'auto';

        $model->operator_id = \Yii::$app->user->id;
        if ($model->load(\Yii::$app->request->post()) && $model->validate()) {
            if ($order = $model->create()) {
                // completed order
                if (Order::complete($order->id, $order->total)) {
                    Yii::$app->session->setFlash('success', 'Thêm thành công');
                    $this->redirect(['retailer']);
                } else {
                    Yii::$app->session->setFlash('warning', 'Không thể hoàn thành, vui lòng kiểm tra lại thông tin');
                }
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

}
