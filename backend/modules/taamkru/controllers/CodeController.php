<?php

namespace app\modules\taamkru\controllers;

use kyna\taamkru\models\form\CodeImportForm;
use kyna\taamkru\models\form\CreateCodeForm;
use kyna\taamkru\models\Retailer;
use Yii;
use kyna\taamkru\models\Code;
use kyna\taamkru\models\search\CodeSearch;
use app\components\controllers\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\Response;

/**
 * CodeController implements the CRUD actions for Code model.
 */
class CodeController extends Controller
{
    public $mainTitle = 'Quản lý thẻ';
    public $role = 'TaamkruRetailer';

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index'],
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->user->can('Taamkru.Code.View');
                        }
                    ],
                    [
                        'allow' => true,
                        'actions' => ['create', 'download-sample', 'assign'],
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->user->can('Taamkru.Code.Create');
                        }
                    ],
                    [
                        'allow' => true,
                        'actions' => ['update', 'change-status'],
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->user->can('Taamkru.Code.Update');
                        }
                    ],
                    [
                        'allow' => true,
                        'actions' => ['cancel'],
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->user->can('Taamkru.Code.Delete');
                        }
                    ],
                    [
                        'allow' => true,
                        'actions' => ['retailer'],
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->user->isTaamkruRetailer;
                        }
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all Code models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CodeSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        if (Yii::$app->request->isAjax && Yii::$app->request->post('type', null) == 'export') {
            // init pagination for raw SQL
            $sql = $dataProvider->query->createCommand()->getRawSql();
            $sql = str_replace('`', '', $sql);
            $email = empty(Yii::$app->request->post('email'))?Yii::$app->user->identity->email:Yii::$app->request->post('email');
            // execute command in console
            $rootPath = \Yii::getAlias('@root');
            $exportLog = \Yii::getAlias('@console/runtime/export_taamkru_code.log');
            $exportErrorLog = \Yii::getAlias('@console/runtime/export_taamkru_code-error.log');
            // check log file exist
            self::_checkExistFile($exportLog);
            self::_checkExistFile($exportErrorLog);
            $command = "php {$rootPath}/yii export/run taamkru-code \"{$sql}\" {$email}" . " > {$exportLog} 2>>{$exportErrorLog} &";
            exec($command);
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'result' => true,
            ];
        }

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    private function _checkExistFile($file)
    {
        clearstatcache();
        if (!file_exists($file)) {
            touch($file);
        }
    }

    /**
     * Displays a single Code model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Code model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new CreateCodeForm();
        $autoSerial = Yii::$app->request->get('auto_serial', null);
        $retailerID = Yii::$app->request->get('retailer_id', null);
        $model->retailer_id = $retailerID;
        if (!is_null($autoSerial) && intval($autoSerial) == Code::BOOL_YES) {
            $model->setScenario($model::SCENARIO_AUTO_SERIAL);
            $model->auto_serial = Code::BOOL_YES;
        } else {
            $model->setScenario($model::SCENARIO_SERIAL);
            $model->quantity = 1;
            $model->auto_serial = Code::BOOL_NO;
        }

        if ($model->load(Yii::$app->request->post()) && $model->create()) {
            Yii::$app->session->setFlash('success', "Thêm thành công!");
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Code model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Code model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionCancel($id)
    {
        $model = $this->findModel($id);
        $model->cancel();
        return $this->redirect(['index']);
    }

    /**
     * Finds the Code model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Code the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Code::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionDownloadSample()
    {
        $filePath = Yii::getAlias('@upload/samples/taamkru_template.xlsx');

        if (!is_file($filePath)) {
            throw new NotFoundHttpException;
        }

        return Yii::$app->response->sendFile($filePath);
    }

    public function actionAssign()
    {
        $model = new CodeImportForm();

        if ($model->load(Yii::$app->request->post()) && $model->assign()) {
            Yii::$app->session->setFlash('warning', "Kết quả gán thẻ sẽ được gửi vào email {$model->email} trong vài phút");
            return $this->redirect(['index']);
        } else {
            return $this->renderAjax('_import', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Lists all Code models.
     * @return mixed
     */
    public function actionRetailer()
    {
        $searchModel = new CodeSearch();
        $retailer = Retailer::findOne(['user_id' => Yii::$app->user->id]);
        $searchModel->retailer_id = $retailer->id;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        if (Yii::$app->request->isAjax && Yii::$app->request->post('type', null) == 'export') {
            // init pagination for raw SQL
            $sql = $dataProvider->query->createCommand()->getRawSql();
            $sql = str_replace('`', '', $sql);
            $email = empty(Yii::$app->request->post('email'))?Yii::$app->user->identity->email:Yii::$app->request->post('email');
            // execute command in console
            $rootPath = \Yii::getAlias('@root');
            $exportLog = \Yii::getAlias('@console/runtime/export_taamkru_code.log');
            $exportErrorLog = \Yii::getAlias('@console/runtime/export_taamkru_code-error.log');
            // check log file exist
            self::_checkExistFile($exportLog);
            self::_checkExistFile($exportErrorLog);
            $command = "php {$rootPath}/yii export/run taamkru-code-retailer \"{$sql}\" {$email}" . " > {$exportLog} 2>>{$exportErrorLog} &";
            exec($command);
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'result' => true,
            ];
        }

        return $this->render('retailer', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
}
