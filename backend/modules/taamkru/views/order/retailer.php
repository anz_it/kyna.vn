<?php

use yii\bootstrap\Html;
use kartik\grid\GridView;
use yii\bootstrap\Nav;
use yii\bootstrap\ButtonDropdown;
use yii\helpers\Url;
use yii\widgets\Pjax;

use app\modules\order\models\ActionForm;
use app\modules\order\assets\BackendAsset;

BackendAsset::Register($this);
$this->title = $this->context->mainTitle;

$this->params['breadcrumbs'][] = $this->title;

$formatter = Yii::$app->formatter;
$user = Yii::$app->user;
?>

<div class="order-index">
    <nav class="navbar navbar-default">
        <div class="btn-group navbar-btn navbar-left">
            <a class="btn btn-default" href="<?= Url::toRoute(['create-cod']) ?>">
                <i class="fa fa-plus"></i> Gửi thẻ qua COD
            </a>
        </div>
        <div class="btn-group navbar-btn navbar-left">
            <a class="btn btn-default" href="<?= Url::toRoute(['create-email']) ?>">
                <i class="fa fa-envelope"></i> Gửi mã qua email
            </a>
        </div>
    </nav>
    <nav class="navbar navbar-default">
        <?= $this->render('_search_retailer', ['queryParams' => $queryParams, 'searchModel' => $searchModel]) ?>
    </nav>
    <?php Pjax::begin(['id' => 'idPjaxReload']) ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            //'filterModel' => $searchModel,
            'columns' => [
                [
                    'header' => 'Đơn hàng',
                    'attribute' => 'order_date',
                    'value' => function ($model, $key, $index) use ($formatter, $statusLabelCss, $callStatuses) {
                        $html = '<strong>#' . $model->id . '</strong><br>';
                        $html .= '<time>' . $formatter->asDatetime(!empty($model->order_date) ? $model->order_date : null) . '</time><br>';
                        $html .= '<span class="label ' . $statusLabelCss[$model->status] . '">' . $model->statusLabel . '</span><br>';

                        if (array_key_exists($model->call_status, $callStatuses)) {
                            $html .= $callStatuses[$model->call_status];
                        }

                        return $html;
                    },
                    'format' => 'raw',
                ],
                [
                    'label' => 'Retailer',
                    'format' => 'raw',
                    'value' => function ($model) {
                        $code = \kyna\taamkru\models\Code::findOne(['serial' => $model->activation_code]);
                        return !empty($code) ? (!empty($code->retailer) ? $code->retailer->user->profile->name : '') : null;
                    }
                ],
                [
                    'header' => 'Thông tin người mua',
                    'format' => 'raw',
                    'value' => function ($model, $key, $index) use ($formatter) {
                        $user = $model->user;
                        if (empty($user)) {
                            return null;
                        }
                        if (!empty($user->userAddress)) {
                            $info = $user->userAddress->toString(false);
                        } else {
                            // get phone number from user register
                            $info = $user->profile->phone_number;
                        }
                        return $formatter->asEmail($user->email) . '<br>' . $info;
                    },
                ],
                [
                    'attribute' => 'details',
                    'label' => 'Sản phẩm',
                    'value' => 'detailsText',
                    'format' => 'raw',
                ],
                [
                    'attribute' => 'activation_code',
                    'label' => 'Serial Code',
                    'format' => 'raw',
                ],
                [
                    'header' => 'Giá bán',
                    'format' => 'raw',
                    'value' => function ($model) use ($formatter) {
                        $html = '<strong>' . $formatter->asCurrency($model->total) . '</strong>';

                        return $html;
                    },
                ],
                [
                    'attribute' => 'payment_method',
                    'label' => 'Hình thức thanh toán',
                    'format' => 'raw',
                    'value' => function ($model) {
                        return $model->isCod ? 'COD' : 'Online';
                    },
                ],
            ],
        ]); ?>
    <?php Pjax::end(); ?>
</div>
