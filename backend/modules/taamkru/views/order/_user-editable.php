<?php

use yii\helpers\Url;
use yii\web\View;
use common\widgets\locationfield\LocationField;

?>
<div class="row">
    <div class="col-sm-6">
        <?= $form->field($model, 'shipping_contact_name') ?>
    </div>
    <div class="col-sm-6">
        <?= $form->field($model, 'shipping_phone_number') ?>
    </div>
</div>
<div class="row">
    <div class="col-sm-6">
        <?= $form->field($model, 'email') ?>
    </div>
    <div class="col-sm-6">
        <?= $form->field($model, 'shipping_street_address') ?>
    </div>
</div>
<?= $form->field($model, 'shipping_location_id')->widget(LocationField::className())->label(false) ?>
