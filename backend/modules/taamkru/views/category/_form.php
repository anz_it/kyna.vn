<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use \kyna\taamkru\models\Category;
use yii\web\JsExpression;
use yii\helpers\Url;
use \kyna\course\models\Course;

/* @var $this yii\web\View */
/* @var $model kyna\taamkru\models\Category */
/* @var $form yii\widgets\ActiveForm */

$crudTitles = Yii::$app->params['crudTitles'];

?>

<div class="becourse-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <div class="row">
        <div class="col-lg-6">
            <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-lg-6">
            <?php
            $courseInitText = (!empty($model->course)) ? $model->course->name : '';

            echo $form->field($model, 'course_id')->widget(Select2::classname(), [
                'initValueText' => $courseInitText, // set the initial display text
                'options' => ['placeholder' => $crudTitles['prompt']],
                'pluginOptions' => [
                    'allowClear' => true,
                    'minimumInputLength' => 3,
                    'language' => [
                        'errorLoading' => new JsExpression("function () { return 'Chờ nhận kết quả...'; }"),
                    ],
                    'ajax' => [
                        'url' => Url::toRoute(['/course/api/search', 'auto' => true]),
                        'dataType' => 'json',
                        'data' => new JsExpression('function(params) { return {q: params.term, include_hide: true, type: ['.Course::TYPE_SOFTWARE.','.Course::TYPE_COMBO.']}; }'),
                    ],
                    'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                    'templateResult' => new JsExpression('function(res) { return res.text; }'),
                    'templateSelection' => new JsExpression('function (res) { return res.text; }'),
                ],
            ]);
            ?>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-4">
            <?= $form->field($model, 'key')->widget(Select2::classname(), [
                'data' => Category::$objectType,
                'hideSearch' => true,
                'options' => ['placeholder' => $crudTitles['prompt'], 'disabled' => true],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ])
            ?>
        </div>
        <div class="col-lg-4">
            <?= $form->field($model, 'value')->textInput([
                'type' => 'number'
            ]) ?>
        </div>
        <div class="col-lg-4">
            <?= $form->field($model, 'status')->widget(Select2::classname(), [
                'data' => Category::listStatus(),
                'hideSearch' => true,
                'options' => ['placeholder' => $crudTitles['prompt']],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ])
            ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>