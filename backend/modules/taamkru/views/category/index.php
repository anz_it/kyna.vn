<?php

use yii\helpers\Html;
use yii\grid\GridView;
use \kyna\taamkru\models\Category;
use yii\helpers\Url;
use \kartik\select2\Select2;
use \kyna\course\models\Course;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $searchModel kyna\taamkru\models\search\CategorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$crudTitles = Yii::$app->params['crudTitles'];
$crudButtonIcons = Yii::$app->params['crudButtonIcons'];
$this->title = Yii::$app->controller->mainTitle;
$this->params['breadcrumbs'][] = $this->title;
$user = Yii::$app->user;
$courseInitText = (!empty($searchModel->course)) ? $searchModel->course->name : '';
?>
<div class="row category-index">
    <div class="col-xs-12">
        <div class="beuser-index">
            <p>
                <?= Html::a($crudButtonIcons['create'] . ' ' . $crudTitles['create'], ['create'], ['class' => 'btn btn-success']) ?>
            </p>
            <div class="box">
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],

                        'title',
                        [
                            'attribute' => 'course_id',
                            'value' => function ($model) {
                                return !empty($model->course) ? $model->course->name : null;
                            },
                            'filter' => Select2::widget([
                                'model' => $searchModel,
                                'initValueText' => $courseInitText,
                                'attribute' => 'course_id',
                                'options' => ['placeholder' => $crudTitles['prompt']],
                                'pluginOptions' => [
                                    'allowClear' => true,
                                    'minimumInputLength' => 3,
                                    'language' => [
                                        'errorLoading' => new JsExpression("function () { return 'Chờ nhận kết quả...'; }"),
                                    ],
                                    'ajax' => [
                                        'url' => Url::toRoute(['/course/api/search', 'auto' => true]),
                                        'dataType' => 'json',
                                        'data' => new JsExpression('function(params) { return {q: params.term, include_hide: true, type: ['.Course::TYPE_SOFTWARE.','.Course::TYPE_COMBO.']};}'),
                                    ],
                                    'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                                    'templateResult' => new JsExpression('function(teacher) { return teacher.text; }'),
                                    'templateSelection' => new JsExpression('function (teacher) { return teacher.text; }'),
                                ],
                            ])
                        ],
                        [
                            'attribute' => 'key',
                            'format' => 'html',
                            'value' => function ($model) {
                                return $model->getTypeText();
                            },
                            'contentOptions' => [
                                'style' => 'min-width: 115px;'
                            ],
                            'filter' => Html::activeDropDownList($searchModel, 'key', Category::$objectType, ['class' => 'form-control', 'prompt' => $crudTitles['prompt']]),
                        ],
                        'value',
                        [
                            'attribute' => 'status',
                            'format' => 'raw',
                            'value' => function ($model) {
                                return $model->statusButton;
                            },
                            'contentOptions' => [
                                'style' => 'min-width: 115px;'
                            ],
                            'filter' => Html::activeDropDownList($searchModel, 'status', Category::listStatus(false), ['class' => 'form-control', 'prompt' => $crudTitles['prompt']]),
                        ],
                        [
                            'class' => 'yii\grid\ActionColumn',
                            'visibleButtons' => [
                                'update' => $user->can('Taamkru.Category.Update'),
                                'view' => $user->can('Taamkru.Category.View'),
                                'delete' => $user->can('Taamkru.Category.Delete'),
                            ],
                        ],
                    ],
                ]); ?>
            </div>
        </div>
    </div>
</div>