<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use \kartik\select2\Select2;
use yii\web\JsExpression;
use yii\helpers\Url;
use \kyna\taamkru\models\Code;
use \kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $searchModel kyna\taamkru\models\search\CodeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$crudTitles = Yii::$app->params['crudTitles'];
$crudButtonIcons = Yii::$app->params['crudButtonIcons'];
$this->title = Yii::$app->controller->mainTitle;
$this->params['breadcrumbs'][] = $this->title;
$user = Yii::$app->user;
$initText = !empty($searchModel->retailer) ? $searchModel->retailer->user->profile->name : '';
$retailerRole = $this->context->role;
?>
<div class="row code-index">
    <div class="col-xs-12">
        <div class="beuser-index">
            <nav class="navbar navbar-default">
                <?= Html::a($crudButtonIcons['create'] . ' Thêm thẻ' , ['create'], ['class' => 'btn btn-success navbar-btn navbar-left']) ?>
                <!-- Button export -->
                <a class="btn btn-info navbar-btn navbar-left" id="btn_export">
                    <i class="fa fa-share"></i> Export file
                </a>
                <!-- Button assign -->
                <?php if ($user->can('Taamkru.Code.Create')) : ?>
                    <a class="btn btn-primary navbar-btn navbar-left" id="btn_assign" href="/taamkru/code/assign">
                        <i class="fa fa-upload"></i> Gán thẻ
                    </a>
                <?php endif; ?>
            </nav>
            <div class="box">
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => [
                        //['class' => 'yii\grid\SerialColumn'],

                        'id',
                        'serial',
                        'code',
                        [
                            'attribute' => 'created_time',
                            'format' => 'datetime',
                            'filter' =>  DatePicker::widget([
                                'model' => $searchModel,
                                'attribute' => 'created_time'
                            ])
                        ],
                        [
                            'attribute' => 'retailer_id',
                            'value' => function ($model) {
                                if (empty($model->retailer) || empty($model->retailer->user)) {
                                    return null;
                                }
                                return !empty($model->retailer->user->profile) ? $model->retailer->user->profile->name : null;
                            },
                            'filter' => Select2::widget([
                                'model' => $searchModel,
                                'initValueText' => $initText,
                                'attribute' => 'retailer_id',
                                'options' => ['placeholder' => 'Nhập Email'],
                                'pluginOptions' => [
                                    'allowClear' => true,
                                    'minimumInputLength' => 3,
                                    'language' => [
                                        'errorLoading' => new JsExpression("function () { return 'Chờ nhận kết quả...'; }"),
                                    ],
                                    'ajax' => [
                                        'url' => Url::toRoute(['/taamkru/api/search-retailer']),
                                        'dataType' => 'json',
                                        'data' => new JsExpression('function(params) { return {q:params.term}; }'),
                                    ],
                                    'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                                    'templateResult' => new JsExpression('function(res) { return res.text; }'),
                                    'templateSelection' => new JsExpression('function (res) { return res.text; }'),
                                ],
                            ])
                        ],
                        [
                            'attribute' => 'category_id',
                            'value' => function ($codeModel) {
                                return (!empty($codeModel->category)) ? $codeModel->category->title : null;
                            }
                        ],
                        [
                            'attribute' => 'status',
                            'format' => 'html',
                            'value' => function ($model) {
                                return $model->statusHtml;
                            },
                            'contentOptions' => [
                                'style' => 'min-width: 115px;'
                            ],
                            'filter' => Html::activeDropDownList($searchModel, 'status', Code::listStatus(false), ['class' => 'form-control', 'prompt' => $crudTitles['prompt']]),
                        ],
                        [
                            'label' => 'Hình thức thanh toán',
                            'format' => 'html',
                            'value' => function ($model) {
                                return $model->paymentMethodText;
                            },
                            'contentOptions' => [
                                'style' => 'min-width: 115px;'
                            ],
                            'filter' => Html::activeDropDownList($searchModel, 'payment_method', Code::listPaymentMethod(false), ['class' => 'form-control', 'prompt' => $crudTitles['prompt']]),
                        ],
                        [
                            'template' => '{cancel}',
                            'class' => 'yii\grid\ActionColumn',
                            'contentOptions' => ['style' => 'width: 90px;'],
                            'visibleButtons' => [
                                'cancel' => function ($model, $key) use ($user) {
                                    return $user->can('Taamkru.Code.Delete') && $model->status == Code::CODE_STATUS_IN_STORE;
                                },
                            ],
                            'buttons' => [
                                'cancel' => function ($url, $model, $key) {
                                    return Html::a('Hủy', $url, [
                                        'class' => 'btn btn-sm btn-default btn-block',
                                        'data-confirm' => 'Bạn có chắc là sẽ hủy thẻ này không?',
                                        'data-method' => 'post',
                                    ]);
                                },
                            ],
                        ],
                    ],
                ]); ?>
            </div>
        </div>
    </div>
</div>
<?php
$script = "
    ;(function($, window, document, undefined){
        $(document).ready(function(){        
            $('body').on('click', '#btn_export', function (event) {            
                event.preventDefault();
                var exportDialog = new BootstrapDialog();
                exportDialog.setTitle('Vui lòng nhập email nhận file export');
                exportDialog.setMessage($('<input id=\"email_export\" class=\"form-control\" placeholder=\"Email\" value=\"".Yii::$app->user->identity->email."\"></input>'));
                exportDialog.setType(BootstrapDialog.TYPE_SUCCESS);
                exportDialog.setButtons([{
                    label: 'Export',
                    action: function(dialog) {                       
                        var email = $('#email_export').val(); 
                        if (email == '') {
                            alert('Vui lòng nhập email');
                        }
                        var data = {email: email, type: 'export'};
                        $.post('', data, function (response) {
                            if (response.result) {
                                var bootstrapDialog = new BootstrapDialog();
                                bootstrapDialog.setMessage('Vui lòng kiểm tra email '+email+'. File Export sẽ được gửi đến trong vài phút.');
                                bootstrapDialog.setType(BootstrapDialog.TYPE_SUCCESS);
                                bootstrapDialog.open(); 
                                exportDialog.close();          
                            } else {
                                
                            }
                        });
                    }
                }]);
                exportDialog.open(); 
            });
            $(\"body\").on(\"click\", \"#btn_assign\", function (e) {
                e.preventDefault();
                var modal = $(\"#modal\"),
                    url = this.href;
       
                modal.find(\".modal-content\").load(url, function (resp) {
                    modal.modal(\"show\");
                });
            });
        });
    })(window.jQuery || window.Zepto, window, document);";
$css = "
    .modal-content .modal-header {
        border-radius: 0;
    }
    ";
?>
<?php
$this->registerCss($css);
$this->registerJs($script, \yii\web\View::POS_END, 'export-code');
?>
