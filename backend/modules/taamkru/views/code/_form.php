<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use \kyna\taamkru\models\Code;
use \kartik\select2\Select2;
use yii\web\JsExpression;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model kyna\taamkru\models\Code */
/* @var $form yii\widgets\ActiveForm */
$crudTitles = Yii::$app->params['crudTitles'];
$retailerRole = $this->context->role;
?>
<div class="becourse-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <div class="row">
        <div class="col-lg-4">
            <?= $form->field($model, 'auto_serial')->checkbox() ?>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-4">
            <?= $form->field($model, 'serial')->textInput(['maxlength' => true, 'disabled' => intval($model->auto_serial) ? true : false]) ?>
        </div>
        <div class="col-lg-4">
            <?php
            $initText = empty($model->retailer) ? '' : $model->retailer->user->profile->name;
            echo $form->field($model, 'retailer_id')->widget(Select2::classname(), [
                'initValueText' => $initText, // set the initial display text
                'options' => ['placeholder' => $crudTitles['prompt']],
                'pluginOptions' => [
                    'allowClear' => true,
                    'minimumInputLength' => 3,
                    'language' => [
                        'errorLoading' => new JsExpression("function () { return 'Chờ nhận kết quả...'; }"),
                    ],
                    'ajax' => [
                        'url' => Url::toRoute(['/taamkru/api/search-retailer']),
                        'dataType' => 'json',
                        'data' => new JsExpression('function(params) { return {q:params.term}; }'),
                    ],
                    'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                    'templateResult' => new JsExpression('function(res) { return res.text; }'),
                    'templateSelection' => new JsExpression('function (res) { return res.text; }'),
                ],
            ]);
            ?>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-4">
            <?= $form->field($model, 'prefix')->textInput(['maxlength' => true, 'disabled' => !$model->auto_serial]) ?>
        </div>
        <div class="col-lg-4">
            <?= $form->field($model, 'quantity')->textInput([
                'type' => 'number',
                'disabled' => !$model->auto_serial
            ]) ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Thêm mới') : Yii::t('app', 'Cập nhật'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<?php
$script = "
    ;(function($, window, document, undefined){
        function addParameterToURL(param){
            _url = location.href;
            _url += (_url.split('?')[1] ? '&':'?') + param;
            return _url;
        }
        $(document).ready(function(){
            $('body').on('change', '#createcodeform-auto_serial', function (event) {
                var param = 'auto_serial=' + ($(this).prop('checked') ? '1' : '0');
                window.location.href = addParameterToURL(param);
            });           
        });
    })(window.jQuery || window.Zepto, window, document);";
?>
<?php
$this->registerJs($script, \yii\web\View::POS_END, 'create-code');
?>

