<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model kyna\taamkru\models\Code */

$this->title = 'Thêm thẻ';
$this->params['breadcrumbs'][] = ['label' => Yii::$app->controller->mainTitle, 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="code-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
