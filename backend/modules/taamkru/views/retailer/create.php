<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model kyna\taamkru\models\Retailer */

$this->title = Yii::t('app', 'Create Retailer');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Retailers'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="retailer-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
