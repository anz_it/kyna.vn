<?php

use yii\helpers\Url;
use yii\helpers\Html;
use kartik\form\ActiveForm;
use kartik\daterange\DateRangePicker;
use kartik\widgets\Select2;
use yii\web\JsExpression;

?>

<?php
$form = ActiveForm::begin([
    'options' => ['class' => 'navbar-form navbar-right'],
    'method' => 'get',
]);
?>

<?=
$form->field($searchModel, 'date_ranger', [
    'addon' => ['prepend' => ['content' => '<i class="glyphicon glyphicon-calendar"></i>']],
    'options' => ['class' => 'form-group'],
])->widget(DateRangePicker::classname(), [
    'useWithAddon' => true,
    'convertFormat' => true,
    'pluginOptions' => [
        'timePicker' => true,
        'locale' => [
            'format' => 'd/m/yy',
            'separator' => " - ", // after change this, must update in controller
        ],
    ]
])->label(false)->error(false)
?>

    <div class="form-group">
        <?= Html::submitButton('<i class="ion-android-search"></i> Lọc', ['class' => 'btn btn-default']) ?>
    </div>

<?php ActiveForm::end(); ?>