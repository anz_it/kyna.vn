<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Nav;

$this->title = Yii::$app->controller->mainTitle;
$this->params['breadcrumbs'][] = $this->title;

$crudTitles = Yii::$app->params['crudTitles'];
?>
<style>

</style>
<br>
<div class="commission-income-view">
    <div class="row">
        <div class="col-xs-12">
            <div class="commission-income-index">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="box box-solid">
                            <div class="box-body">
                                <div class="row">
                                    <div class="col-xs-6">
                                        <h4><strong>Thống kê chung</strong></h4>
                                    </div>
                                    <div class="col-xs-6"> <?= $this->render('_filter', ['searchModel' => $searchModel]) ?></div>
                                </div>
                                <br>
                                <div class="row text-center">
                                    <div class="col-xs-4">
                                        <dl class="dl-horizontal">
                                            <div class="text-uppercase"><h4><strong>Số lượng order</strong></h4></div>
                                            <div><h3><strong><?= $totalOrder ?></strong></h3></div>
                                        </dl>
                                    </div>
                                    <div class="col-xs-4">
                                        <dl class="dl-horizontal">
                                            <div class="text-uppercase"><h4><strong>Số tiền đã thu</strong></h4></div>
                                            <div><h3><strong><?= Yii::$app->formatter->asCurrency($totalPrice) ?></strong></h3></div>
                                        </dl>
                                    </div>
                                    <div class="col-xs-4">
                                        <dl class="dl-horizontal">
                                            <div class="text-uppercase"><h4><strong>Số user</strong></h4></div>
                                            <div><h3><strong><?= $totalUser ?></strong></h3></div>
                                        </dl>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
<?php
$js = <<< SCRIPT
/* To initialize BS3 tooltips set this below */
$(function () { 
    $("[data-toggle='popover']").popover({ trigger: "hover", html: true }); 
});;
SCRIPT;

$this->registerJs($js);
?>
