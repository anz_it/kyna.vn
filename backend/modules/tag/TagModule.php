<?php

namespace app\modules\tag;

/**
 * tag module definition class
 */
class TagModule extends \kyna\base\BaseModule
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\tag\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
