<?php

namespace app\modules\tag\controllers;

use kyna\taamkru\models\Retailer;
use kyna\tag\models\Tag;
use Yii;
use yii\web\Response;
use yii\db\Query;
use kyna\user\models\Profile;
use kyna\user\models\User;
use kyna\user\models\AuthAssignment;
use yii\db\Expression;
use app\components\controllers\Controller;

class ApiController extends Controller
{
    public function actionSearch($q = null)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        
        $out = ['results' => []];
        if (!is_null($q)) {
            $query = new Query();
            $query->select([
                't.tag AS id',
                't.tag AS text',
                "t.tag AS name"
            ])->from(Tag::tableName() . ' t')
                ->where(['t.status' => Tag::STATUS_ACTIVE])
                ->andWhere("(t.tag LIKE :q OR t.slug LIKE :q)", [':q' => "%{$q}%"]);
                
            $query->groupBy('id');
            
            $command = $query->createCommand();
            $data = $command->queryAll();

            $out['results'] = array_values($data);
        }

        return $out;
    }

    public function actionTagList($q = null, $id = null) {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];
        if (!is_null($q)) {
            $query = new Query;
            $query->select('id, tag AS text')
                ->from('tags')
                ->where(['like', 'tag', $q])
                ->andWhere(['tags.status'=>1])
                ->limit(20);
            $command = $query->createCommand();
            $data = $command->queryAll();
            $out['results'] = array_values($data);
        }
        elseif ($id > 0) {
            $out['results'] = ['id' => $id, 'text' => Tag::find($id)->name];
        }
        return $out;
    }
}
