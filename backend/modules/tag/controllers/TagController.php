<?php

namespace app\modules\tag\controllers;

use Yii;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\UploadedFile;
use yii\web\Response;
use kyna\base\ActiveRecord;
use kyna\tag\models\Tag;
use kyna\tag\models\search\TagSearch;
use common\lib\CDNImage;
use app\components\controllers\Controller;

/**
 * TagController implements the CRUD actions for Tag model.
 */
class TagController extends Controller
{
    public $mainTitle = 'Tags';
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'view'],
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->user->can('Tags.View');
                        }
                    ],
                    [
                        'allow' => true,
                        'actions' => ['create'],
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->user->can('Tags.Create');
                        }
                    ],
                    [
                        'allow' => true,
                        'actions' => ['update', 'change-status'],
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->user->can('Tags.Update');
                        }
                    ],
                    [
                        'allow' => true,
                        'actions' => ['delete'],
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->user->can('Tags.Delete');
                        }
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all Tag models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TagSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Tag model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Tag model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Tag();

        if ($model->load(Yii::$app->request->post())) {
            if ($model->is_mobile == Tag::BOOL_YES && empty($model->image_url)) {
                $model->image_url = '/';
            }
            if ($model->save()) {
                if ($image_url = $this->_uploadImage($model, 'image_url')) {
                    $model->image_url = $image_url;
                    $model->save();
                }
                Yii::$app->session->setFlash('success', "Thêm thành công!");
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Tag model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            if ($model->is_mobile == Tag::BOOL_YES && empty($model->image_url)) {
                $model->image_url = '/';
            }
            if ($image_url = $this->_uploadImage($model, 'image_url')) {
                $model->image_url = $image_url;
            }
            if ($model->save()) {
                Yii::$app->session->setFlash('success', "Cập nhật thành công!");
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }
        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Tag model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        Yii::$app->session->setFlash('success', "Xóa thành công!");

        return $this->redirect(['index']);
    }

    /**
     * Finds the Tag model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Tag the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Tag::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    private function _uploadImage($model, $attribute) {
        $files = UploadedFile::getInstances($model, $attribute);
        if (!sizeof($files)) {
            $oldAttributes = $model->oldAttributes;

            return isset($oldAttributes[$attribute]) ? $oldAttributes[$attribute] : false;
        }

        $file = $files[0];

        $cdnImage = new CDNImage($model, $attribute);
        $result = $cdnImage->upload($file);

        return $result;
    }

    /**
     * @desc function to change status to all active records
     * @param integer $id
     * @param boolean $redirectUrl
     * @param string $field
     * @return string
     */
    public function actionChangeStatus($id, $redirectUrl = false, $field = 'status')
    {
        $response = [
            'status' => false,
            'msg' => ''
        ];
        $model = $this->findModel($id);

        if (($field == 'is_mobile') && ($model->{$field} == ActiveRecord::STATUS_DEACTIVE) && empty($model->image_url)) {
            $response['msg'] = "Vui lòng cập nhật hình ảnh cho Tag này";
        } else {
            if ($model->{$field} == ActiveRecord::STATUS_ACTIVE) {
                $model->{$field} = ActiveRecord::STATUS_DEACTIVE;
            } else {
                $model->{$field} = ActiveRecord::STATUS_ACTIVE;
            }
            if ($model->save(false)) {
                $response['status'] = true;
                $response['msg'] = "Thay đổi thành công!";
            } else {
                $response['msg'] = "Thay đổi thất bại!";
            }
        }

        Yii::$app->response->format = Response::FORMAT_JSON;
        return $response;
    }

}
