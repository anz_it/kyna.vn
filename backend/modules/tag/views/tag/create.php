<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model kyna\tag\models\Tag */

$this->title = Yii::t('app', 'Tạo Tag');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Tags'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="becourse-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
