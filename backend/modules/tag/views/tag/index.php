<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use common\helpers\CDNHelper;
use kyna\tag\models\Tag;

/* @var $this yii\web\View */
/* @var $searchModel kyna\tag\models\search\TagSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$crudTitles = Yii::$app->params['crudTitles'];
$crudButtonIcons = Yii::$app->params['crudButtonIcons'];
$this->title = Yii::$app->controller->mainTitle;
$this->params['breadcrumbs'][] = $this->title;
$user = Yii::$app->user;
?>
<div class="row category-index">
    <div class="col-xs-12">
        <div class="beuser-index">
            <p>
                <?= Html::a($crudButtonIcons['create'] . ' ' . $crudTitles['create'], ['create'], ['class' => 'btn btn-success']) ?>
            </p>
            <div class="box">
                <?php Pjax::begin(['timeout' => Yii::$app->params['timeOutPjax'], 'id' => 'idPjaxReload']) ?>
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],

                        'id',
                        'tag',
                        'slug',
                        //'description',
                        [
                            'label' => 'Số khóa học',
                            'format' => 'raw',
                            'value' => function ($model) {
                                return $model->countCourse;
                            },
                            'contentOptions' => [
                                'style' => 'min-width: 115px;'
                            ],
                        ],
                        [
                            'attribute' => 'image_url',
                            'format' => 'html',
                            'value' => function ($model) {
                                return CDNHelper::image($model->image_url, [
                                    'size' => CDNHelper::IMG_SIZE_ORIGINAL,
                                    'alt' => $model->tag,
                                    'width' => '100px'
                                ]);
                            },
                            'filter' => false,
                            'options' => ['class' => 'col-xs-1']
                        ],
                        [
                            'attribute' => 'is_desktop',
                            'format' => 'raw',
                            'value' => function ($model) {
                                return $model->getStatusButton(null, 'is_desktop');
                            },
                            'contentOptions' => [
                                'style' => 'min-width: 115px;'
                            ],
                            'filter' => Html::activeDropDownList($searchModel, 'is_desktop', Tag::listStatus(false), ['class' => 'form-control', 'prompt' => $crudTitles['prompt']]),
                        ],
                        [
                            'attribute' => 'is_mobile',
                            'format' => 'raw',
                            'value' => function ($model) {
                                return $model->getStatusButton(null, 'is_mobile');
                            },
                            'contentOptions' => [
                                'style' => 'min-width: 115px;'
                            ],
                            'filter' => Html::activeDropDownList($searchModel, 'is_mobile', Tag::listStatus(false), ['class' => 'form-control', 'prompt' => $crudTitles['prompt']]),
                        ],
                        [
                            'attribute' => 'status',
                            'format' => 'raw',
                            'value' => function ($model) {
                                return $model->statusButton;
                            },
                            'contentOptions' => [
                                'style' => 'min-width: 115px;'
                            ],
                            'filter' => Html::activeDropDownList($searchModel, 'status', Tag::listStatus(false), ['class' => 'form-control', 'prompt' => $crudTitles['prompt']]),
                        ],
                        [
                            'class' => 'yii\grid\ActionColumn',
                            'visibleButtons' => [
                                'update' => $user->can('Tags.Update'),
                                'view' => $user->can('Tags.View'),
                                'delete' => $user->can('Tags.Delete'),
                            ],
                        ],
                    ],
                ]); ?>
                <?php Pjax::end() ?>
            </div>
        </div>
    </div>
</div>
