<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use \kyna\tag\models\Tag;
use common\widgets\upload\Upload;
use common\widgets\tinymce\TinyMce;

/* @var $this yii\web\View */
/* @var $model kyna\tag\models\Tag */
/* @var $form yii\widgets\ActiveForm */

$crudTitles = Yii::$app->params['crudTitles'];
?>

<div class="becourse-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <div class="row">
        <div class="col-lg-4">
            <?=
            $form->field($model, 'tag')->textInput([
                'maxlength' => true,
                'onchange' => "$('#slug').val(convertToSlug($(this).val()))",
            ])
            ?>
        </div>
        <div class="col-lg-4">
            <?= $form->field($model, 'slug')->textInput([
                'id' => 'slug',
                'maxlength' => true,
            ]) ?>
        </div>
        <div class="col-lg-4">
            <?= $form->field($model, 'status')->widget(Select2::classname(), [
                'data' => Tag::listStatus(),
                'hideSearch' => true,
                'options' => ['placeholder' => $crudTitles['prompt']],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ])
            ?>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-4">
            <?= $form->field($model, 'is_desktop')->widget(Select2::classname(), [
                'data' => Tag::listStatus(),
                'hideSearch' => true,
                'options' => ['placeholder' => $crudTitles['prompt']],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ])
            ?>
        </div>
        <div class="col-lg-4">
            <?= $form->field($model, 'is_mobile')->widget(Select2::classname(), [
                'data' => Tag::listStatus(),
                'hideSearch' => true,
                'options' => ['placeholder' => $crudTitles['prompt']],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ])
            ?>
        </div>
        <div class="col-lg-4">
            <?= $form->field($model, 'image_url')->widget(Upload::className(), ['display' => 'image']) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <?= $form->field($model, 'description')->widget(TinyMce::className(), [
                'layout' => 'advanced',
            ])
            ?>
        </div>
    </div>

    <h3>Thông tin SEO: </h3>

    <div class="row">
        <div class="col-lg-4">
            <?= $form->field($model, 'seo_title')->textInput() ?>
        </div>
        <div class="col-lg-4">
            <?= $form->field($model, 'seo_description')->textarea(['maxlength' => true]) ?>
        </div>
        <div class="col-lg-4">
            <?= $form->field($model, 'seo_keyword')->textarea() ?>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-3">
            <?= $form->field($model, 'seo_robot_index')->widget(Select2::classname(), [
                'data' => Tag::listRobotIndex(),
                'hideSearch' => true,
                'options' => ['placeholder' => $crudTitles['prompt']],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ])
            ?>
        </div>
        <div class="col-lg-3">
            <?= $form->field($model, 'seo_robot_follow')->widget(Select2::classname(), [
                'data' => Tag::listRobotFollow(),
                'hideSearch' => true,
                'options' => ['placeholder' => $crudTitles['prompt']],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ])
            ?>
        </div>
        <div class="col-lg-3">
            <?= $form->field($model, 'seo_sitemap')->widget(Select2::classname(), [
                'data' => Tag::listSitemap(),
                'hideSearch' => true,
                'options' => ['placeholder' => $crudTitles['prompt']],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ])
            ?>
        </div>
        <div class="col-lg-3">
            <?= $form->field($model, 'seo_canonical')->textInput() ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<?= $this->render('//_partials/stringjs') ?>
