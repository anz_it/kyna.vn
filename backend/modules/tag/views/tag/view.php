<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model kyna\tag\models\Tag */

$this->title = $model->tag;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Tags'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="becourse-view">

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'tag',
            'slug',
            'description',
            [
                'attribute' => 'status',
                'value' => $model->statusText
            ],
            'seo_title',
            'seo_description',
            'seo_keyword',
            'seo_robot_index',
            'seo_robot_follow',
            'seo_sitemap',
            'seo_canonical',
            'is_deleted',
            'created_time:datetime',
            'updated_time:datetime',
        ],
    ]) ?>

</div>
