<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model kyna\tag\models\Tag */

$this->title = Yii::t('app', 'Cập nhật {modelClass}: ', [
    'modelClass' => 'Tag',
]) . $model->tag;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Tags'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->tag, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Cập nhật');
?>
<div class="tag-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
