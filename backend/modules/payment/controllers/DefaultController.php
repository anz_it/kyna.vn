<?php

namespace app\modules\payment\controllers;

use Yii;
use kyna\payment\models\PaymentMethod;
use yii\data\ActiveDataProvider;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\helpers\Html;

use kyna\order\models\Order;

/**
 * PaymentMethodController implements the CRUD actions for PaymentMethod model.
 */
class DefaultController extends \app\components\controllers\Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all PaymentMethod models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => PaymentMethod::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'paymentTypes' => PaymentMethod::getPaymentTypes(),
        ]);
    }

    /**
     * Creates a new PaymentMethod model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new PaymentMethod();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'paymentTypes' => PaymentMethod::getPaymentTypes(),
            ]);
        }
    }

    /**
     * Updates an existing PaymentMethod model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'paymentTypes' => PaymentMethod::getPaymentTypes(),
            ]);
        }
    }

    /**
     * Finds the PaymentMethod model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return PaymentMethod the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = PaymentMethod::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }


    public function actionTest($id) {
        $model = $this->findModel($id);
        //$transId = 840 . time();
        $orderId = 29;
        $order = Order::findOne($orderId);
        //$model->returnUrl = Url::current([], true);

        $client = $model->client;
        $model->paymentParams = [
            'serial' => '12345678912',
            'pinCode' => '123456789',
            'serviceCode' => 'VNP',
        ];
        //var_dump($client);
        // $response = $client->pay($transId, $orderId, $amount, [
        //     'serial' => '12345678912',
        //     'pinCode' => '123456789',
        //     'serviceCode' => 'VNP',
        // ]);
        $response = $model->pay($order);
        var_dump($response);

/*
        $response = $client->query($transId);
        //var_dump($response);
        //echo Html::a('pay', $client->pay($transId, $orderId, $amount), [
        //    'target' => '_blank',
        //]);

        $response = \Yii::$app->request->get();
        unset($response['id']);
        $client->ipn($response);
        var_dump($client->ipn($response));
        return $client->ipnResponse;
        */
    }
}
