<?php

namespace app\modules\payment\controllers;

use Yii;
use yii\data\ActiveDataProvider;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

use kotchuprik\sortable\actions\Sorting;

use kyna\base\models\Location;
use kyna\payment\models\PaymentMethod;
use kyna\payment\models\PaymentTransaction;

/**
 * PaymentMethodController implements the CRUD actions for PaymentMethod model.
 */
class MethodController extends \app\components\controllers\Controller
{
    
    public function actions()
    {
        $actions = parent::actions();
        
        $actions['sorting'] = [
            'class' => Sorting::className(),
            'orderAttribute' => 'position',
            'query' => PaymentMethod::find()
        ];
        
        return $actions;
    }
    
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'view'],
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->user->can('PaymentMethod.View');
                        },
                    ],
                    [
                        'allow' => true,
                        'actions' => ['create'],
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->user->can('PaymentMethod.Action.Create');
                        },
                    ],
                    [
                        'allow' => true,
                        'actions' => ['update', 'settings', 'sorting', 'change-status'],
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->user->can('PaymentMethod.Action.Update');
                        },
                    ],
                    [
                        'allow' => true,
                        'actions' => ['delete'],
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->user->can('PaymentMethod.Action.Delete');
                        },
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all PaymentMethod models.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => PaymentMethod::find()->orderBy('position'),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'paymentTypes' => PaymentMethod::getPaymentTypes(),
        ]);
    }

    public function actionSettings($id)
    {
        $method = $this->findModel($id);
        $locationModels = Location::find()->where('parent_id > 0')->orderBy([
            'parent_id' => 'asc',
            'position' => 'asc',
        ])->all();

        $settings = $method->clientSettings;
        if ($post = Yii::$app->request->post($method->formName())) {
            $settings = $post['clientSettings'];
            $method->clientSettings = $settings;
        }

        return $this->render('settings', [
            'model' => $method,
            'locationModels' => $locationModels,
            'settings' => $settings,
        ]);
    }

    /**
     * Creates a new PaymentMethod model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     *
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new PaymentMethod();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', 'Thêm thành công');
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'paymentTypes' => PaymentMethod::getPaymentTypes(),
            ]);
        }
    }

    /**
     * Updates an existing PaymentMethod model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param int $id
     *
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', 'Cập nhật thành công');
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'paymentTypes' => PaymentMethod::getPaymentTypes(),
            ]);
        }
    }

    /**
     * Finds the PaymentMethod model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param int $id
     *
     * @return PaymentMethod the loaded model
     *
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = PaymentMethod::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionTest($class)
    {
        $transaction = PaymentTransaction::findOne(7);
        $transaction->response_string = 'OrderCode=157427345493&TotalFee=20900&SessionToken=H3t8dCNh4lqm%2B5PHvqTac1pFk5jBQD%2BUAxz3l02%2B7CRL1Lt55y2uIY0%2F4oE9Uz8WK4%2Bj5tX8dQ0jW10zBau5Vv9eh7gKB4H4%2FRDIY4ibB3pCwv17b0x28%2B30QV9vmCbW3yfDttlgEY2kysUs%3D';
        $transaction->save();
        /*
        $order = Order::findOne(13);
        $address = [
            'contact_name' => 'Dat Nguyen (Kyna-IT)',
            'phone_number' => '0903415493',
            'location_id' => '548',
            'city_id' => '31',
            'street_address' => 'Testing Pls cancel it',
        ];

        $method = PaymentMethod::loadModel($class, $address);
        $result = $method->pay($order);
        var_dump($result);
        */
    }
}
