<?php
/**
 * @link http://www.yiiframework.com/
 *
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\modules\payment\assets;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 *
 * @since 2.0
 */
class PaymentAsset extends \yii\web\AssetBundle
{
    //public $basePath = '@webroot';
    //public $baseUrl = '@web';
    public $sourcePath = '@bower/codemirror';
    public $js = [
        'lib/codemirror.js',
        'mode/htmlmixed/htmlmixed.js',
        'mode/css/css.js',
        'mode/xml/xml.js',
        'mode/javascript/javascript.js',
    ];
    public $css = [
        'lib/codemirror.css',
    ];
}
