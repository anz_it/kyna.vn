<?php

use yii\bootstrap\Html;
use yii\bootstrap\ActiveForm;
use common\widgets\codeeditor\CodeEditor;

use app\modules\payment\assets\PaymentAsset;
PaymentAsset::register($this);  // $this represents the view object

/* @var $this yii\web\View */
/* @var $model common\models\PaymentMethod */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="payment-method-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'class')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'payment_type')->radioList($paymentTypes) ?>

    <?= $form->field($model, 'content')->widget(CodeEditor::className(), [
        'language' => 'htmlmixed',
    ]) ?>

    <?= $form->field($model, 'status')->checkbox() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
