<?php
use yii\bootstrap\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Cài đặt thông số ' . $model->name;

$pickups = $settings['Pickups'];
$pickups = array_flip($pickups);

$services = $settings['Services'];
$services = array_flip($services);

$locationSection = $settings['Locations'];

$formName = $model->formName();
?>
<nav class="navbar navbar-default navbar-static-top">
    <?php /*$form = ActiveForm::begin([
        'action' => Url::toRoute([
            '/shared/vendor/shipping-test/',
            'method' => 'calculateFee',
            'id' => $model->id
        ]),
        'method' => 'get',
        'options' => [
            'class' => 'navbar-form navbar-left',
        ],
    ]) ; ?>
        <?= Html::activeHiddenInput($testModel, 'vendor_id') ?>
        <?php
        <div class="form-group">
            <?= Html::activeDropDownList($testModel, 'from', $pickups, [
                'class' => 'form-control'
            ]) ?>
        </div>
        <div class="form-group">
            <?= Html::activeTextInput($testModel, 'to', [
                'class' => 'form-control'
            ]) ?>
        </div>
        <div class="form-group">
            <?= Html::activeDropDownList($testModel, 'serviceId', $services, [
                'class' => 'form-control'
            ]) ?>
        </div>
        ?>
        <button type="submit" class="btn btn-default"><i class="ion-bug"></i> Test</button>
    <?php ActiveForm::end(); */ ?>
</nav>
<?php

$form = ActiveForm::begin([
    'id' => 'settings-form',
]);
echo Html::hiddenInput('', '', ['id' => 'add-item']);

foreach ($settings as $section => $config) :
    if ($section != 'Locations') : ?>
        <h3>
            <?= $section ?>
            <a href="#" class="btn btn-default btn-xs btn-add-item hide"><i class="ion-android-add"></i> Thêm mục cài đặt</a>
        </h3>
        <div class="row">
        <?php foreach ($config as $key => $value) : ?>
            <?= $form->field($model, "clientSettings[$section][$key]", [
                'options' => [
                    'class' => 'form-group col-sm-4',
                ],
            ])->label($key) ?>
        <?php endforeach; ?>
        </div>
    <?php endif; ?>
<?php endforeach; ?>

<button type="submit" class="btn btn-primary"><i class="ion-android-done"></i> Lưu cài đặt</button>

<?php
$section = 'Locations';
$heading = '';
?>
<h2><?= $section ?></h2>
<div class="row">
<?php foreach ($locationModels as $location) :
    if(!empty($location->parent)) {
        $parentName = $location->parent->name;
    }else{
        echo $location->id . ' ' . $location->name;
    }
    if ($heading !== $parentName) : $heading = $parentName ?>
        <div class="col-sm-12">
            <div class="row">
                <div class="form-group col-sm-2">
                    <strong class="text-uppercase"><?= Html::staticControl($heading) ?></strong>
                </div>
                <?= $form->field($model, "clientSettings[$section][{$location->parent->id}][code]", [
                    'options' => [
                        'class' => 'form-group col-sm-2',
                    ],
                ])->textInput(['placeholder' => 'Mã địa phương'])->label(false) ?>
                <?= $form->field($model, "clientSettings[$section][{$location->parent->id}][pickup]", [
                    'options' => [
                        'class' => 'form-group col-sm-2',
                    ],
                ])->dropDownList($pickups)->label(false) ?>
                <?= $form->field($model, "clientSettings[$section][{$location->parent->id}][service]", [
                    'options' => [
                        'class' => 'form-group col-sm-2',
                    ],
                ])->dropDownList($services)->label(false) ?>
            </div>
        </div>
    <?php endif; ?>

    <div class="col-sm-4">
        <div class="row">
            <div class="form-group col-sm-6">
                <?= Html::staticControl(' → '.$location->name) ?>
            </div>
            <div class="col-sm-6">
                <?= $form->field($model, "clientSettings[$section][{$location->id}][code]", [
                    'options' => [
                        'class' => 'form-group',
                    ],
                ])->textInput(['placeholder' => 'Mã địa phương'])->label(false) ?>
                <?php if(!empty($model->clientSettings[$section][$location->id]['service'])) { ?>
                    <?= $form->field($model, "clientSettings[$section][{$location->id}][service]", [
                        'options' => [
                            'class' => 'form-group',
                        ],
                    ])->dropDownList($services)->label(false) ?>
                <?php }else { ?>
                    <?= $form->field($model, "clientSettings[$section][{$location->id}][service]", [
                        'options' => [
                            'class' => 'form-group',
                        ],
                    ])->dropDownList($services, [
                        'options' => [
                            $model->clientSettings[$section][$location->parent->id]['service'] => [
                                'selected' => true
                            ]
                        ]
                    ])->label(false) ?>
                <?php } ?>
            </div>
        </div>
    </div>
<?php endforeach; ?>
</div>
<button type="submit" class="btn btn-primary"><i class="ion-android-done"></i> Lưu cài đặt</button>
<?php ActiveForm::end(); ?>

<?php
$js = <<<JS
;(function($, _){
    $("h2, h3").hover(function () {
        $(this).find(".btn-add-item").removeClass("hide");
    }, function () {
        $(this).find(".btn-add-item").addClass("hide");
    });

    $(".btn-add-item").on("click", function(e) {
        e.preventDefault();

        var value = prompt($(this).text());
        if (value != null) {
            var name = $(this).data('target');
            $("#add-item").attr("name", name + "[" + value + "]").val("");
            //console.log(name, value); return;
            $("#settings-form").unbind("submit").submit();
        }
    });
})(jQuery, _);
JS;
$this->registerJs($js);
