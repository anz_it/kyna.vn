<?php

use yii\helpers\Url;
use yii\bootstrap\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use common\components\SortableColumn;

$this->title = 'Payment Methods';
$this->params['breadcrumbs'][] = $this->title;

$crudTitles = Yii::$app->params['crudTitles'];
$crudButtonIcons = Yii::$app->params['crudButtonIcons'];

$user = Yii::$app->user;
?>
<div class="payment-method-index">
    <?php if ($user->can('PaymentMethod.Action.Create')) : ?>
        <p>
            <?= Html::a($crudButtonIcons['create'] . ' ' . $crudTitles['create'], ['create'], ['class' => 'btn btn-success']) ?>
        </p>
    <?php endif; ?>
    <?php Pjax::begin(['id' => 'idPjaxReload']) ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'options' => [
            'data' => [
                'sortable-widget' => 1,
                'sortable-url' => Url::toRoute(['sorting']),
            ]
        ],
        'rowOptions' => function ($model, $key, $index, $grid) {
            return ['data-sortable-id' => $model->id];
        },
        'columns' => [
            [
                'class' => SortableColumn::className(),
            ],
            'name',
            'class',
            [
                'attribute' => 'payment_type',
                'value' => function ($model) use ($paymentTypes) {
                    return $paymentTypes[$model->payment_type];
                },
            ],
            [
                'attribute' => 'status',
                'value' => function ($model)
                {
                    return $model->statusButton;
                },
                'format' => 'raw',
            ],
            [
                'class' => 'app\components\ActionColumnCustom',
                'visibleButtons' => [
                    'view' => $user->can('PaymentMethod.View'),
                    'update' => $user->can('PaymentMethod.Action.Update'),
                    'delete' => $user->can('PaymentMethod.Action.Delete'),
                ]
            ],
        ],
    ]); ?>
    <?php Pjax::end() ?>
</div>
