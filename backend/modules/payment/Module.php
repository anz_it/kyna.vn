<?php

namespace app\modules\payment;

class Module extends \kyna\base\BaseModule
{
    public $controllerNamespace = 'app\modules\payment\controllers';

    public function init()
    {
        parent::init();
        // custom initialization code goes here
    }
}
