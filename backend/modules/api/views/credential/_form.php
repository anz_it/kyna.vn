<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kyna\api\models\ApiCredential;

/* @var $this yii\web\View */
/* @var $model kyna\api\models\ApiCredential */
/* @var $form yii\widgets\ActiveForm */

$crudTitles = Yii::$app->params['crudTitles'];
?>

<div class="api-credential-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'password')->passwordInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'status')->dropDownList(ApiCredential::listStatus(), ['prompt' => $crudTitles['prompt']]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
