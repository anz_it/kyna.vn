<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model kyna\api\models\ApiCredential */

$this->title = 'Update Api Credential: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Api Credentials', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="api-credential-update">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
