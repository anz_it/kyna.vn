<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use kyna\api\models\ApiCredential;


/* @var $this yii\web\View */
/* @var $searchModel kyna\api\models\search\ApiCredentialSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Api Credentials';
$this->params['breadcrumbs'][] = $this->title;

$crudTitles = Yii::$app->params['crudTitles'];
$crudButtonIcons = Yii::$app->params['crudButtonIcons'];

$user = Yii::$app->user;
?>
<div class="api-credential-index">
    <p>
        <?php
        if ($user->can('Api.Credential.Create')) {
            echo Html::a($crudButtonIcons['create'] . ' ' . $crudTitles['create'], ['create'], ['class' => 'btn btn-success']);
        }
        ?>
    </p>
    <?php Pjax::begin(['timeout' => Yii::$app->params['timeOutPjax'], 'id' => 'idPjaxReload']) ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'id',
                'contentOptions' => ['style' => 'width: 100px;']
            ],
            'username',
            'auth_key:ntext',
            'access_token:ntext',
            [
                'attribute' => 'status',
                'format' => 'raw',
                'value' => function ($model) {
                    return $model->statusButton;
                },
                'filter' => Html::activeDropDownList($searchModel, 'status', ApiCredential::listStatus(false), ['class' => 'form-control', 'prompt' => $crudTitles['prompt']]),
            ],

            ['class' => 'app\components\ActionColumnCustom'],
        ],
    ]); ?>
    <?php Pjax::end() ?>
</div>
