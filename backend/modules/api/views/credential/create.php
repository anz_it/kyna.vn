<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model kyna\api\models\ApiCredential */

$this->title = 'Create Api Credential';
$this->params['breadcrumbs'][] = ['label' => 'Api Credentials', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="api-credential-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
