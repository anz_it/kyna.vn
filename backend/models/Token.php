<?php

namespace app\models;

use Yii;

/**
 * Created by PhpStorm.
 * User: macintosh
 * Date: 8/4/17
 * Time: 2:22 PM
 */
class Token extends \kyna\user\models\Token
{

    public function beforeSave($insert)
    {
        if ($this->isNewRecord) {
            static::deleteAll(['user_id' => $this->user_id, 'type' => $this->type]);
            $this->created_at = time();
            $this->setAttribute('code', Yii::$app->security->generateRandomString());
        }
        return true;
    }
}