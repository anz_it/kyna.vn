<?php
/* @var $this \yii\web\View */
/* @var $content string */

use backend\assets\AdminLteAsset;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;

$bundle = AdminLteAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="icon" href="/favicon.png">
        
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>

        <?php $this->head() ?>
    </head>
    <body class="hold-transition skin-blue sidebar-mini <?= Url::home() !== Yii::$app->request->url ? 'sidebar-collapse' : '' ?>">
        <?php $this->beginBody() ?>
        <div id="loader"></div>
        <div class="wrapper">
            <?= $this->render('lte/_header', ['bundle' => $bundle]) ?>

            <?= $this->render('lte/_left_sidebar', ['bundle' => $bundle]) ?>

            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                       <?= Html::encode($this->title) ?>
                    </h1>
                    <?= Breadcrumbs::widget([
                        'homeLink' => [
                            'label' => '<i class="fa fa-dashboard"></i> Home',
                            'url' => yii\helpers\Url::toRoute(['/']),
                            'encode' => false
                        ],
                        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                    ]) ?>
                </section>
                <!-- Main content -->
                <section class="content">
                    <?= Alert::widget() ?>

                    <?= $content ?>
                </section>
            </div><!-- /.content-wrapper -->

            <?= $this->render('lte/_footer') ?>

            <?= $this->render('lte/_control_sidebar') ?>
        </div>

        <?php $this->endBody() ?>
        <!-- Modal -->
        <div class="modal fade" id="modal" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content"></div>
            </div>
        </div>

        <!-- Sub Modal -->
        <div class="modal fade" id="sub-modal" role="dialog" aria-labelledby="mySubModalLabel" >
            <div class="modal-dialog" role="document">
                <div class="modal-content"></div>
            </div>
        </div>

    </body>
</html>
<?php $this->endPage() ?>
