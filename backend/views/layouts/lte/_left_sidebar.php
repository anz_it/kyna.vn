<?php

use yii\helpers\Url;
use kyna\base\models\MetaField;
use kyna\commission\models\AffiliateUser;
use kyna\setting\models\Banner;

$moduleId = Yii::$app->controller->module->id;
$controllerId = $this->context->id;
$actionId = $this->context->action->id;
$user = \Yii::$app->user;
?>

<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <ul class="sidebar-menu">
            <!-- <li class="header">MAIN NAVIGATION</li> -->
            <!-- <li class="treeview <?php if ($moduleId == 'app-backend') {echo 'active';} ?>">
                <a href="/">
                    <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                </a>
            </li> -->
            <?php if ($user->can('Course.View') || $user->can('Course.Category.View') || $user->can('Course.Combo.View') || $user->can('Teacher.View') || $user->can('Student.View')) : ?>
            <li class="treeview <?php if ($moduleId == 'course') {echo 'active';} ?>">
                <a href="#">
                    <i class="fa fa-graduation-cap"></i>
                    <span>Khóa học</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <?php if ($user->can('Course.View')) : ?>
                        <li><a href="<?= Url::toRoute(['/course/default/index']) ?>">Danh sách khóa học</a></li>
                    <?php endif; ?>
                    <?php if ($user->can('Course.Category.View')) : ?>
                        <li><a href="<?= Url::toRoute(['/course/category']) ?>">Danh mục khóa học</a></li>
                    <?php endif; ?>
                    <?php if ($user->can('Course.Combo.View')) : ?>
                        <li><a href="<?= Url::toRoute(['/course/combo/index']) ?>">Khóa học Combo</a></li>
                    <?php endif; ?>
                    <?php if ($user->can('Teacher.View')) : ?>
                        <li><a href="<?= Url::toRoute(['/course/teacher/index']) ?>"> Giảng viên</a></li>
                    <?php endif; ?>
                    <?php if ($user->can('Student.View')) : ?>
                        <li><a href="<?= Url::toRoute(['/course/student/index']) ?>"> Danh sách học viên</a></li>
                    <?php endif; ?>
                    <?php if ($user->can('Course.Setting')) : ?>
                        <li><a href="<?= Url::toRoute(['/course/commission-type/index']) ?>"> Loại hoa hồng</a></li>
                        <li><a href="<?= Url::toRoute(['/course/meta-field/index', 'model' => MetaField::MODEL_COURSE]) ?>"> Course Meta Field</a></li>
                        <li><a href="<?= Url::toRoute(['/course/meta-field/index', 'model' => MetaField::MODEL_COURSE_COMBO]) ?>"> Combo Meta Field</a></li>
                        <li><a href="<?= Url::toRoute(['/course/meta-field/index', 'model' => MetaField::MODEL_TEACHER]) ?>"> Teacher Meta Field</a></li>
                        <li><a href="<?= Url::toRoute(['/course/meta-field/index', 'model' => MetaField::MODEL_COURSE_QUIZ]) ?>"> Quiz Meta Field</a></li>
                    <?php endif; ?>
                    <?php if ($user->can('Course.Export')) : ?>
                        <li><a href="<?= Url::toRoute(['/course/export/index']) ?>"> Exports</a></li>
                    <?php endif; ?>
                    <?php if ($user->can('Tags.View')) : ?>
                        <li><a href="<?= Url::toRoute(['/tag/tag/index']) ?>"> Tags</a></li>
                    <?php endif; ?>
                </ul>
            </li>
            <?php endif; ?>
            <?php if ($user->can('Order.View') || $user->can('PaymentMethod.View') || $user->can('Order.Export')) : ?>
            <li class="treeview <?php if ($moduleId == 'order' || $moduleId == 'payment') {echo 'active';} ?>">
                <a href="#">
                    <i class="fa fa-shopping-cart"></i>
                    <span>Quản lý giao dịch</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <?php if ($user->can('Order.View')) : ?>
                        <li><a href="<?= Url::toRoute(['/order/default/wait-process']) ?>">Đơn hàng</a></li>
                    <?php endif; ?>
                    <?php if ($user->can('Order.Action.Print')) : ?>
                        <li><a href="<?= Url::toRoute(['/order/default/print-cod']) ?>">In Ấn</a></li>
                    <?php endif; ?>
                    <?php if ($user->can('PaymentMethod.View')) : ?>
                        <li><a href="<?= Url::toRoute(['/payment/method/index']) ?>">Phương thức thanh toán</a></li>
                    <?php endif; ?>
                    <?php if ($user->can('Order.Export')) : ?>
                        <li><a href="<?= Url::toRoute(['/order/export/index']) ?>">Báo cáo</a></li>
                        <li><a href="<?= Url::toRoute(['/order/export/learner']) ?>">Danh sách học viên (theo đơn hàng)</a></li>
                        <li><a href="<?= Url::toRoute(['/export/registration/index']) ?>">Đơn đăng ký(User care + Order)</a></li>
                    <?php endif; ?>
                    <?php if ($user->can('Order.Revenue')) : ?>
                        <li><a href="<?= Url::toRoute(['/order/revenue/index']) ?>">Doanh thu</a></li>
                    <?php endif; ?>
                    <?php if ($user->can('Order.TransactionLog')) : ?>
                        <li><a href="<?= Url::toRoute(['/order/banking-transfer/index']) ?>">Lịch sử thanh toán</a></li>
                    <?php endif; ?>
                </ul>
            </li>
            <?php endif; ?>
            <?php if ($user->can('Coupon.View') || $user->can('Voucher.View') || $user->can('GroupDiscount.View') || $user->can('PromotionSchedule.View')) : ?>
                <li class="treeview <?php if ($moduleId == 'promo') {echo 'active';} ?>">
                    <a href="#">
                        <i class="fa fa-gift"></i>
                        <span>Marketing</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">

                        <?php if ($user->can('Voucher.View')) : ?>
                            <li><a href="<?= Url::toRoute(['/promotion/voucher/index']) ?>">Vouchers</a></li>
                        <?php endif; ?>
                        <?php if ($user->can('GroupDiscount.View')) : ?>
                            <li><a href="<?= Url::toRoute(['/promo/group-discount/index']) ?>">Group Discount</a></li>
                        <?php endif; ?>
                        <?php if ($user->can('PromotionSchedule.View')) : ?>
                            <li><a href="<?= Url::toRoute(['/promo/schedule/index']) ?>">Promotion Schedules</a></li>
                        <?php endif; ?>
                    </ul>
                </li>
            <?php endif; ?>
            <?php if ($user->can('Affiliate.Dashboard.View') || $user->can('Affiliate.Category.View') || $user->can('Affiliate.User.View') || $user->can('Commission.Calculation.View') || $user->can('Teacher.Income.View') || $user->can('Affiliate.Category.Course.View')) : ?>
                <li class="treeview <?php if ($moduleId == 'commission') {echo 'active';} ?>">
                    <a href="#">
                        <i class="fa fa-dollar"></i>
                        <span>Commission Management</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <?php if ($user->can('Affiliate.Dashboard.View')) : ?>
                            <li><a href="<?= Url::toRoute(['/commission/affiliate-income/index']) ?>"> Affiliate Dashboard</a></li>
                        <?php endif; ?>
                        <?php if ($user->can('Affiliate.User.View')) : ?>
                            <li><a href="<?= Url::toRoute(['/commission/affiliate-user/index']) ?>"> Affiliate Users</a></li>
                        <?php endif; ?>
                        <?php if ($user->can('Affiliate.Category.View')) : ?>
                            <li><a href="<?= Url::toRoute(['/commission/group/index']) ?>"> Affiliate Groups</a></li>
                        <?php endif; ?>
                        <?php if ($user->can('Affiliate.Category.View')) : ?>
                            <li><a href="<?= Url::toRoute(['/commission/affiliate-category/index']) ?>"> Affiliate Categories</a></li>
                        <?php endif; ?>
                        <?php if ($user->can('Affiliate.Category.Course.View')) : ?>
                            <li><a href="<?= Url::toRoute(['/commission/course/index']) ?>"> Affiliate Courses</a></li>
                        <?php endif; ?>
                        <?php if ($user->can('Teacher.Income.View')) : ?>
                            <li><a href="<?= Url::toRoute(['/commission/teacher-income/index']) ?>"> Teacher Incomes</a></li>
                        <?php endif; ?>
                        <?php if ($user->can('Commission.Calculation.View')) : ?>
                            <li><a href="<?= Url::toRoute(['/commission/calculation/index']) ?>"> Commission Calculations</a></li>
                        <?php endif; ?>
                        <?php if ($user->can('Affiliate.Export')) : ?>
                            <li><a href="<?= Url::toRoute(['/commission/export/index']) ?>"> Exports</a></li>
                        <?php endif; ?>
                    </ul>
                </li>
            <?php endif; ?>
            <?php
            $isAffiliate = AffiliateUser::find()->andWhere(['user_id' => $user->id])->exists();
            ?>
            <?php if ($isAffiliate && !$user->can('Teacher')) : ?>
                <li class="treeview <?php if ($controllerId == 'affiliate-income' || $controllerId == 'affiliate-income-manager') {echo 'active';} ?>">
                <a href="#">
                    <i class="fa fa-gg"></i>
                    <span>Thống kê thu nhập</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <?php if ($user->isAffiliateManager) : ?>
                        <li class="<?= $controllerId == 'affiliate-income-manager' ? 'active' : '' ?>"><a href="<?= Url::toRoute(['/commission/affiliate-income-manager/index']) ?>"> Quản lý đại lý cấp 2</a></li>
                    <?php endif; ?>
                    <?php if ($isAffiliate) : ?>
                        <li class="<?= $controllerId == 'affiliate-income' ? 'active' : '' ?>"><a href="<?= Url::toRoute(['/commission/affiliate-income/view']) ?>"> Thu nhập giới thiệu</a></li>
                    <?php endif; ?>
                </ul>
            </li>
            <?php endif; ?>
            <?php if ($user->can('User.View') || $user->can('TimeSlot.View') || $user->can('UserTelesale.View') || $user->can('User.Setting')) : ?>
            <li class="treeview <?php if ($moduleId == 'user') {echo 'active';} ?>">
                <a href="#">
                    <i class="fa fa-users"></i>
                    <span>User management</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <?php if ($user->can('User.View')) : ?>
                        <li><a href="<?= Url::toRoute(['/user/admin/index']) ?>"> Thành viên</a></li>
                    <?php endif; ?>
                    <?php if ($user->can('TimeSlot.View')) : ?>
                        <li><a href="<?= Url::toRoute(['/user/time-sheet']) ?>"> Phân công ca trực</a></li>
                    <?php endif; ?>
                    <?php if ($user->can('UserTelesale.View')) : ?>
                        <li><a href="<?= Url::toRoute(['/user/user-telesale/index']) ?>"> User Care Management</a></li>
                    <?php endif; ?>
                    <?php if ($user->can('User.Setting')) : ?>
                        <li><a href="<?= Url::toRoute(['/user/meta-field/index']) ?>"> Meta Field Management</a></li>
                    <?php endif; ?>
                    <?php if ($user->can('User.Statistic')) : ?>
                    <li><a href="<?= Url::toRoute(['/user/statistic/by-year']) ?>"> Thống kê học viên</a></li>
                    <?php endif; ?>
                </ul>
            </li>
            <?php endif; ?>
            <?php if ($user->can('Course.Qna') || $user->can('Course.Comment')) : ?>
            <li class="treeview <?php if ($moduleId == 'community') {echo 'active';} ?>">
                <a href="#">
                    <i class="fa fa-comments"></i>
                    <span>Cộng đồng</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <?php if ($user->can('Course.Qna')) : ?>
                        <li><a href="<?= Url::toRoute(['/community/qna/index']) ?>"> Câu hỏi cho giảng viên</a></li>
                    <?php endif; ?>
                    <?php if ($user->can('Course.Comment')) : ?>
                        <li><a href="<?= Url::toRoute(['/community/discussion/index']) ?>"> Thảo luận</a></li>
                    <?php endif; ?>
                    <li><a href="<?= Url::toRoute(['/community/rating/index']) ?>"> Đánh giá</a></li>
                </ul>
            </li>
            <?php endif; ?>
            <?php if ($user->can('TeachingAssistant')) : ?>
                <li class="<?php if ($moduleId == 'teacher' && $controllerId == 'qna') {echo 'active';} ?>">
                    <a href="<?= Url::toRoute(['/teacher/qna/index']) ?>">
                        <i class="fa fa-question-circle"></i>
                        <span>Câu hỏi</span>
                    </a>
                </li>
            <?php endif; ?>

            <?php if ($user->can('Teacher')) : ?>
                <li class="<?php if ($moduleId == 'teacher' && $controllerId == 'course') {echo 'active';} ?>">
                    <a href="<?= Url::toRoute(['/teacher/course/index']) ?>">
                        <i class="fa fa-graduation-cap"></i>
                        <span>Danh sách khóa học</span>
                    </a>
                </li>
                <li class="treeview <?php if (($moduleId == 'teacher' && $controllerId == 'income') || $moduleId == 'commission') {echo 'active';} ?>">
                    <a href="#">
                        <i class="fa fa-gg"></i>
                        <span>Thống kê thu nhập</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li class="<?php if ($actionId == 'teaching') {echo 'active';} ?>"><a href="<?= Url::toRoute(['/teacher/income/teaching']) ?>"> Thu nhập giảng dạy</a></li>
                        <?php if ($isAffiliate) : ?>
                            <li class="<?php if ($actionId == 'affiliate') {echo 'active';} ?>"><a href="<?= Url::toRoute(['/commission/affiliate-income/view']) ?>"> Thu nhập giới thiệu</a></li>
                        <?php endif; ?>
                    </ul>
                </li>
                <li class="<?php if ($moduleId == 'teacher' && $controllerId == 'coupon') {echo 'active';} ?>">
                    <a href="<?= Url::toRoute(['/teacher/coupon/index']) ?>">
                        <i class="fa fa-gift"></i>
                        <span>Voucher</span>
                    </a>
                </li>
                <li class="<?php if ($moduleId == 'teacher' && $controllerId == 'qna') {echo 'active';} ?>">
                    <a href="<?= Url::toRoute(['/teacher/qna/index']) ?>">
                        <i class="fa fa-question-circle"></i>
                        <span>Câu hỏi</span>
                    </a>
                </li>
                <li class="<?php if ($moduleId == 'teacher' && $controllerId == 'setting') {echo 'active';} ?>">
                    <a href="<?= Url::toRoute(['/teacher/setting/index']) ?>">
                        <i class="fa fa-cogs"></i>
                        <span>Cấu hình</span>
                    </a>
                </li>

            <?php endif; ?>


            <?php if ($user->can('Mana.Education.View') || $user->can('Mana.Certificate.View')
                    || $user->can('Mana.Organization.View') || $user->can('Mana.Subject.View')
                    || $user->can('Mana.Teacher.View') || $user->can('Mana.Course.View') || $user->can('Mana.Contact.View')) : ?>
                <li class="treeview <?php if ($moduleId == 'mana') {echo 'active';} ?>">
                    <a href="#">
                        <i class="fa fa-maxcdn"></i>
                        <span>Mana System</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <?php if ($user->can('Mana.Education.View')) : ?>
                            <li><a href="<?= Url::toRoute(['/mana/education/index']) ?>"> Hệ đào tạo</a></li>
                        <?php endif; ?>
                        <?php if ($user->can('Mana.Certificate.View')) : ?>
                            <li><a href="<?= Url::toRoute(['/mana/certificate/index']) ?>"> Chứng chỉ</a></li>
                        <?php endif; ?>
                        <?php if ($user->can('Mana.Organization.View')) : ?>
                            <li><a href="<?= Url::toRoute(['/mana/organization/index']) ?>"> Tổ chức cấp chứng chỉ</a></li>
                        <?php endif; ?>
                        <?php if ($user->can('Mana.Subject.View')) : ?>
                            <li><a href="<?= Url::toRoute(['/mana/subject/index']) ?>"> Chuyên ngành</a></li>
                        <?php endif; ?>
                        <?php if ($user->can('Mana.Teacher.View')) : ?>
                            <li><a href="<?= Url::toRoute(['/mana/teacher/index']) ?>"> Giảng viên</a></li>
                        <?php endif; ?>
                        <?php if ($user->can('Mana.Course.View')) : ?>
                            <li><a href="<?= Url::toRoute(['/mana/course/index']) ?>"> Khóa học</a></li>
                        <?php endif; ?>
                        <?php if ($user->can('Mana.Contact.View')) : ?>
                            <li><a href="<?= Url::toRoute(['/mana/contact/index']) ?>"> Học viên đăng ký</a></li>
                        <?php endif; ?>
                    </ul>
                </li>
            <?php endif; ?>
            <?php if ($user->can('LandingPage.View') || $user->can('FileManager.View')) : ?>
            <li class="treeview <?php if ($controllerId == 'landing-page') {echo 'active';} ?>">
                <a href="#">
                    <i class="fa fa-folder"></i>
                    <span>Landing Pages</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <?php if ($user->can('LandingPage.View')) : ?>
                        <li><a href="<?= Url::toRoute(['/page/landing-page/index']) ?>"> Quản lý Landing Pages</a> </li>
                    <?php endif; ?>
                    <?php if ($user->can('FileManager.View')) : ?>
                        <li><a href="<?= Yii::$app->params['static_link'] ?>/filemanager" target="_blank"> File Manager</a> </li>
                    <?php endif; ?>
                </ul>
            </li>
            <?php endif; ?>
            <?php if ($user->can('Career.View')) : ?>
            <li class="treeview <?php if ($moduleId == 'career') { echo 'active'; } ?>">
                <a href="<?= Url::toRoute(['/career/career/index']) ?>">
                    <i class="fa fa-envelope"></i>
                    <span>Tuyển dụng</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
            </li>
            <?php endif; ?>
            <?php if ($user->can('Partner.Partner.View') || $user->can('Partner.Category.View') || $user->can('Partner.Retailer.View') || $user->can('Partner.Order') || $user->can('Partner.Transaction') || $user->isPartnerRetailer) : ?>
                <li class="treeview <?php if ($controllerId == 'category') {echo 'active';} ?>">
                    <a href="#">
                        <i class="fa fa-universal-access"></i>
                        <span>Đối tác</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <?php if ($user->can('Partner.Partner.View')) : ?>
                            <li><a href="<?= Url::toRoute(['/partner/partner/index']) ?>"> Quản lý đối tác</a> </li>
                        <?php endif; ?>
                        <?php if ($user->can('Partner.Category.View')) : ?>
                            <li><a href="<?= Url::toRoute(['/partner/category/index']) ?>"> Danh mục</a> </li>
                        <?php endif; ?>
                        <?php if ($user->can('Partner.Retailer.View')) : ?>
                            <li><a href="<?= Url::toRoute(['/partner/retailer/index']) ?>"> Retailers</a> </li>
                        <?php endif; ?>
                        <?php if ($user->can('Partner.Code.View')) : ?>
                            <li><a href="<?= Url::toRoute(['/partner/code/index']) ?>"> Quản lý thẻ</a> </li>
                        <?php endif; ?>
                        <?php if ($user->can('Partner.Order')) : ?>
                            <li><a href="<?= Url::toRoute(['/partner/order/index']) ?>"> Quản lý đơn hàng</a> </li>
                        <?php endif; ?>
                        <?php if ($user->can('Partner.Transaction')) : ?>
                            <li><a href="<?= Url::toRoute(['/partner/transaction/index']) ?>"> Transaction</a> </li>
                        <?php endif; ?>
                        <?php if ($user->isPartnerRetailer) : ?>
                            <li><a href="<?= Url::toRoute(['/partner/order/retailer']) ?>"> Đơn hàng</a> </li>
                            <li><a href="<?= Url::toRoute(['/partner/code/retailer']) ?>"> Danh sách thẻ</a> </li>
                            <li><a href="<?= Url::toRoute(['/partner/statistic/retailer']) ?>"> Thống kê</a> </li>
                        <?php endif; ?>
                    </ul>
                </li>
            <?php endif; ?>
            <?php if ($user->can('Setting.Common') || $user->can('Setting.Seo') || $user->can('Setting.Location') || $user->can('Setting.Shipping') || $user->can('Setting.MetaField')) : ?>
            <li class="treeview <?php if ($moduleId == 'setting') { echo 'active'; } ?>">
                <a href="#">
                    <i class="fa fa-cogs"></i>
                    <span>Settings</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <?php if ($user->can('Setting.Common')) : ?>
                        <li><a href="<?= Url::toRoute(['/setting/setting/index']) ?>"> Common Setting</a> </li>
                    <?php endif; ?>
                    <?php if ($user->can('Setting.Location')) : ?>
                        <li><a href="<?= Url::toRoute(['/setting/location/index']) ?>"> Location</a></li>
                    <?php endif; ?>
                    <?php if ($user->can('Setting.Shipping')) : ?>
                        <li><a href="<?= Url::toRoute(['/setting/shipping/index']) ?>"> Shipping</a></li>
                    <?php endif; ?>
                    <?php if ($user->can('Setting.MetaField')) : ?>
                        <li><a href="<?= Url::toRoute(['/setting/meta-field/index?model=setting']) ?>"> Settings Meta Fields</a> </li>
                    <?php endif; ?>

                    <?php if ($user->can('Setting.Seo')) : ?>
                        <li><a href="<?= Url::toRoute(['/setting/seo-setting/index']) ?>">SEO Settings</a> </li>
                    <?php endif; ?>

                    <?php if ($user->can('Setting.Sitemap')) : ?>
                        <li><a href="<?= Url::toRoute(['/setting/sitemap/index']) ?>">Sitemap</a> </li>
                    <?php endif; ?>
                </ul>
            </li>
            <?php endif; ?>
            <?php if ($user->can('Banner.View')) : ?>
                <li class="treeview <?php if ($moduleId == 'banner') { echo 'active'; } ?>">
                    <a href="#">
                        <i class="fa fa-photo"></i>
                        <span>Banners</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="<?= Url::toRoute(['/banner/top/index']) ?>"> Top Banner</a> </li>
                        <li><a href="<?= Url::toRoute(['/banner/slider-banner/index']) ?>"> Slider Banner</a> </li>
                        <li><a href="<?= Url::toRoute(['/banner/campaign-category/index']) ?>"> Campaign Category</a> </li>
                        <li><a href="<?= Url::toRoute(['/banner/cross-product/index']) ?>"> Cross Product</a> </li>
                        <li><a href="<?= Url::toRoute(['/banner/slider/index']) ?>"> Banner Group</a> </li>
                        <li><a href="<?= Url::toRoute(['/banner/checkout/index']) ?>"> Banner Checkout</a> </li>
                        <li><a href="<?= Url::toRoute(['/banner/popup/index']) ?>"> Popup</a> </li>
                        <li><a href="<?= Url::toRoute(['/banner/app-home/index']) ?>"> App Home Banner</a> </li>
                        <li><a href="<?= Url::toRoute(['/banner/tag-banner/index']) ?>"> Tag Banner</a> </li>
                    </ul>
                </li>
            <?php endif; ?>
            <?php if ($user->can('Api.Credential.View')) : ?>
                <li class="treeview <?php if ($moduleId == 'api') { echo 'active'; } ?>">
                    <a href="#">
                        <i class="fa fa-code"></i>
                        <span>Api</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <?php if ($user->can('Api.Credential.View')) : ?>
                            <li><a href="<?= Url::toRoute(['/api/credential/index']) ?>"> Credentials</a> </li>
                        <?php endif; ?>
                    </ul>
                </li>
            <?php endif; ?>
            <?php if ($user->can('Notification.View')): ?>
                <li class="treeview <?php if ($moduleId == 'notification') { echo 'active'; } ?>">
                    <a href="#">
                        <i class="fa fa-bell"></i>
                        <span>Notificaion</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <?php if ($user->can('Notification.View')): ?>
                            <li><a href="<?= Url::toRoute((['/notification/default/index']))?>"> Danh sách Notifications</a></li>
                        <?php endif; ?>
                    </ul>
                </li>
            <?php endif; ?>
            <?php if ($user->can('Gamification.Mission.View') || $user->can('Gamification.Gift.View')) : ?>
                <li class="treeview <?php if ($controllerId == 'mission' || $controllerId == 'gift') {echo 'active';} ?>">
                    <a href="#">
                        <i class="fa fa-gamepad"></i>
                        <span>Gamification</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <?php if ($user->can('Gamification.Mission.View')) : ?>
                            <li><a href="<?= Url::toRoute(['/gamification/mission/index']) ?>"> Nhiệm vụ</a> </li>
                        <?php endif; ?>
                        <?php if ($user->can('Gamification.Gift.View')) : ?>
                            <li><a href="<?= Url::toRoute(['/gamification/gift/index']) ?>"> Quà tặng</a> </li>
                        <?php endif; ?>
                        <?php if ($user->can('Gamification.User.View')) : ?>
                            <li><a href="<?= Url::toRoute(['/gamification/user/index']) ?>"> Bảng xếp hạng</a> </li>
                        <?php endif; ?>
                    </ul>
                </li>
            <?php endif; ?>
            <?php if ($user->can('Faq.View')) : ?>
                <li class="treeview <?php if ($controllerId == 'faq' || $controllerId == 'faq-category') {echo 'active';} ?>">
                    <a href="#">
                        <i class="fa fa-question-circle"></i>
                        <span>FAQ</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <?php if ($user->can('Faq.Category.View')) : ?>
                            <li><a href="<?= Url::toRoute(['/faq/faq-category/index']) ?>"> Danh Mục</a> </li>
                        <?php endif; ?>
                        <?php if ($user->can('Faq.View')) : ?>
                            <li><a href="<?= Url::toRoute(['/faq/faq/index']) ?>"> Nội dung</a> </li>
                        <?php endif; ?>
                    </ul>
                </li>
            <?php endif; ?>

            <?php if ($user->can('Otp.Transaction')): ?>
                <li class="treeview <?php if ($moduleId == 'otp') { echo 'active'; } ?>">
                    <a href="#">
                        <i class="fa fa-mobile"></i>
                        <span>Otp</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <?php if ($user->can('Otp.Transaction')): ?>
                            <li><a href="<?= Url::toRoute((['/otp/otp-transaction/index']))?>">Otp Transaction</a></li>
                        <?php endif; ?>
                    </ul>
                </li>
            <?php endif; ?>

            <?php if ($user->can('Home.Teacher.View')) : ?>
                <li class="treeview <?php if ($moduleId == 'home') {echo 'active';} ?>">
                    <a href="#">
                        <i class="fa fa-home"></i>
                        <span>Home Setting</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <?php if ($user->can('Home.Teacher.View')) : ?>
                            <li><a href="<?= Url::toRoute(['/home/teacher/index']) ?>"> Teacher</a> </li>
                        <?php endif; ?>
                    </ul>
                </li>
            <?php endif; ?>

            <?php if ($user->can('Report.Run')) : ?>
                <li class="<?php if ($moduleId == 'report' && $controllerId == 'report') {echo 'active';} ?>">
                    <a href="<?= Url::toRoute(['/report/report/index']) ?>">
                        <i class="fa fa-question-circle"></i>
                        <span>Report</span>
                    </a>
                </li>
            <?php endif; ?>



        </ul>
    </section>
    <!-- /.sidebar -->
</aside>
