<?php
$script = "
    function convertToSlug(str)
    {
        return S(str).slugify().s;
    }
    ";
        
$this->registerJsFile(Yii::getAlias('@web/js/string.min.js'), ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJs($script, yii\web\View::POS_END, 'my-options');
