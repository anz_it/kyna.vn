<?php

namespace app\components;

use kyna\commission\models\AffiliateUser;
use kyna\partner\models\Retailer;

class WebUser extends \yii\web\User
{
    
    public function getIsAffiliate()
    {
        return AffiliateUser::find()->andWhere(['user_id' => $this->id])->exists();
    }

    public function getAffiliate()
    {
        return AffiliateUser::find()->andWhere(['user_id' => $this->id])->one();
    }

    
    public function getAffiliateGroup()
    {
        $affUser = AffiliateUser::find()->andWhere(['user_id' => $this->id])->one();
        if (!is_null($affUser) && !is_null($affUser->affiliateCategory)) {
            return $affUser->affiliateCategory->affiliate_group_id;
        }
        
        return null;
    }

    public function getIsPartnerRetailer()
    {
        return Retailer::find()->andWhere([
            'user_id' => $this->id,
            'status' => Retailer::STATUS_ACTIVE
        ])->exists();
    }

    public function getPartnerRetailer()
    {
        return Retailer::find()->andWhere([
            'user_id' => $this->id,
            'status' => Retailer::STATUS_ACTIVE
        ])->all();
    }

    public function getIsAffiliateManager()
    {
        return AffiliateUser::find()
            ->alias('a')
            ->andWhere(['a.user_id' => $this->id, 'a.is_manager' => AffiliateUser::STATUS_ACTIVE])->exists();
    }
}
