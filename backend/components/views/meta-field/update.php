<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\BEMetaField */

$crudTitles = Yii::$app->params['crudTitles'];

$this->title = $crudTitles['update'] . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::$app->controller->mainTitle, 'url' => ['index', 'model' => $this->context->modelType]];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id, 'model' => $this->context->modelType]];
$this->params['breadcrumbs'][] = $crudTitles['update'];
?>
<div class="becustom-field-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
