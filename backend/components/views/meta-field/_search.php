<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\search\BEMetaFieldSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="becustom-field-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'name') ?>

    <?= $form->field($model, 'type') ?>

    <?= $form->field($model, 'model') ?>

    <?= $form->field($model, 'extra_validate') ?>

    <?php // echo $form->field($model, 'data_set') ?>

    <?php // echo $form->field($model, 'is_required')->checkbox() ?>

    <?php // echo $form->field($model, 'is_index_es')->checkbox() ?>

    <?php // echo $form->field($model, 'is_readonly')->checkbox() ?>

    <?php // echo $form->field($model, 'note') ?>

    <?php // echo $form->field($model, 'status') ?>

    <?php // echo $form->field($model, 'created_time') ?>

    <?php // echo $form->field($model, 'updated_time') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
