<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\BEMetaField */

$this->title = Yii::$app->params['crudTitles']['create'];
$this->params['breadcrumbs'][] = ['label' => Yii::$app->controller->mainTitle, 'url' => ['index', 'model' => $this->context->modelType]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="becustom-field-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
