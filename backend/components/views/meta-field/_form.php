<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

use kyna\base\models\MetaField;
use app\components\MetaFieldType;

$crudTitles = Yii::$app->params['crudTitles'];
?>

<div class="becustom-field-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'key')->textInput(['maxlength' => true]) ?>
    
    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
    
    <?= $form->field($model, 'type')->dropDownList(MetaFieldType::getList(), ['prompt' => $crudTitles['prompt'], 'onchange' => "
            var type = $(this).val();
            if (type == '" . MetaFieldType::TYPE_CHECKBOX_LIST . "' || type == '" . MetaFieldType::TYPE_DROPDOWN_LIST . "' || type == '" . MetaFieldType::TYPE_RADIO_BUTTON_LIST . "') {
                $('#meta-data-set').show();
            } else {
                $('#meta-data-set').hide();
            }
        "]) ?>
    
    <div id="meta-data-set" style="display: <?= (array_key_exists($model->type, MetaFieldType::getListTypeHasDataSet()) ? 'block' : 'none') ?>">
        <?= $form->field($model, 'data_set')->textarea(['rows' => 6])->hint('Nhập dưới dạng json theo mẫu sau: [value1,value2,value3]') ?>
    </div>

    <?= $form->field($model, 'is_required')->checkbox() ?>

    <?= $form->field($model, 'is_index_es')->checkbox() ?>

    <?= $form->field($model, 'is_unique')->checkbox() ?>

    <?= $form->field($model, 'is_readonly')->checkbox() ?>

    <?= $form->field($model, 'note')->textarea(['rows' => 6]) ?>
    
    <?= $form->field($model, 'extra_validate')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'status')->dropDownList(MetaField::listStatus(), ['prompt' => $crudTitles['prompt']]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? $crudTitles['create'] : $crudTitles['update'], ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::a($crudTitles['cancel'], ['index', 'model' => $this->context->modelType], ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
