<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\grid\GridView;
use app\components\MetaFieldType;
use kyna\base\models\MetaField;

$crudTitles = Yii::$app->params['crudTitles'];
$crudButtonIcons = Yii::$app->params['crudButtonIcons'];

$this->title = Yii::$app->controller->mainTitle;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-xs-12">
        <div class="becustom-field-index">
            <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

            <p>
                <?= Html::a($crudButtonIcons['create'] . ' ' . $crudTitles['create'], ['create', 'model' => $this->context->modelType], ['class' => 'btn btn-success']) ?>
            </p>

            <div class="box">
                <?php Pjax::begin(['timeout' => Yii::$app->params['timeOutPjax'], 'id' => 'idPjaxReload']) ?>
                <?=
                GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],
                        [
                            'attribute' => 'id',
                            'options' => ['class' => 'col-xs-2']
                        ],
                        'key',
                        'name',
                        [
                            'attribute' => 'type',
                            'value' => function ($model) {
                                return $model->typeText;
                            },
                            'filter' => Html::activeDropDownList($searchModel, 'type', MetaFieldType::getList(), ['class' => 'form-control', 'prompt' => $crudTitles['prompt']]), 
                        ],
                        [
                            'attribute' => 'is_required',
                            'format' => 'boolean',
                            'filter' => Html::activeDropDownList($searchModel, 'is_required', MetaField::getBooleanOptions(), ['class' => 'form-control', 'prompt' => $crudTitles['prompt']]), 
                        ],
                        [
                            'attribute' => 'is_index_es',
                            'format' => 'boolean',
                            'filter' => Html::activeDropDownList($searchModel, 'is_index_es', MetaField::getBooleanOptions(), ['class' => 'form-control', 'prompt' => $crudTitles['prompt']]), 
                        ],
                        [
                            'attribute' => 'is_unique',
                            'format' => 'boolean',
                            'filter' => Html::activeDropDownList($searchModel, 'is_unique', MetaField::getBooleanOptions(), ['class' => 'form-control', 'prompt' => $crudTitles['prompt']]),
                        ],
                        [
                            'attribute' => 'is_readonly',
                            'format' => 'boolean',
                            'filter' => Html::activeDropDownList($searchModel, 'is_readonly', MetaField::getBooleanOptions(), ['class' => 'form-control', 'prompt' => $crudTitles['prompt']]), 
                        ],
                        [
                            'attribute' => 'status',
                            'format' => 'raw',
                            'value' => function ($model)
                            {
                                return $model->statusButton;
                            },
                            'filter' => Html::activeDropDownList($searchModel, 'status', MetaField::listStatus(false), ['class' => 'form-control', 'prompt' => $crudTitles['prompt']]), 
                        ],
                        ['class' => 'app\components\ActionColumnCustom'],
                    ],
                ]);
                ?>
                <?php Pjax::end() ?>
            </div>
        </div>
    </div>
</div>