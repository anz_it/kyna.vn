<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\BEMetaField */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::$app->controller->mainTitle, 'url' => ['index', 'model' => $this->context->modelType]];
$this->params['breadcrumbs'][] = $this->title;

$crudTitles = Yii::$app->params['crudTitles'];
?>
<div class="becustom-field-view">

    <p>
        <?= Html::a("<span class='glyphicon glyphicon-backward'></span> " . $crudTitles['back'], ['index', 'model' => $this->context->modelType], ['class' => 'btn btn-info', 'icon' => 'glyphicon glyphicon-backward']) ?>
        <?= Html::a($crudTitles['update'], ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a($crudTitles['delete'], ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            [
                'attribute' => 'type',
                'value' => $model->typeText
            ],
            'model',
            'extra_validate:ntext',
            'data_set:ntext',
            'is_required:boolean',
            'is_index_es:boolean',
            'is_unique:boolean',
            'is_readonly:boolean',
            'note:ntext',
            [
                'attribute' => 'status',
                'value' => $model->statusText
            ],
            'created_time:datetime',
            'updated_time:datetime',
        ],
    ]) ?>

</div>
