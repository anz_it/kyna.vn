<?php

namespace app\components;

use yii\data\ActiveDataProvider as BaseActiveDataProvider;
use yii\base\InvalidConfigException;
use yii\db\QueryInterface;

class ActiveDataProvider extends BaseActiveDataProvider
{

    public function prepareModels()
    {
        if (!$this->query instanceof QueryInterface) {
            throw new InvalidConfigException('The "query" property must be an instance of a class that implements the QueryInterface e.g. yii\db\Query or its subclasses.');
        }
        $query = clone $this->query;
        if (($pagination = $this->getPagination()) !== false) {
            if (empty($pagination->totalCount)) {
                $pagination->totalCount = $this->getTotalCount();
            }
            $query->limit($pagination->getLimit())->offset($pagination->getOffset());
        }
        if (($sort = $this->getSort()) !== false) {
            $query->addOrderBy($sort->getOrders());
        }

        return $query->all($this->db);
    }

    protected function prepareTotalCount()
    {
        if (!$this->query instanceof QueryInterface) {
            throw new InvalidConfigException('The "query" property must be an instance of a class that implements the QueryInterface e.g. yii\db\Query or its subclasses.');
        }

        $pagination = $this->getPagination();
        if ($pagination !== false && !empty($pagination->totalCount)) {
            return $pagination->totalCount;
        } else {
            $query = clone $this->query;

            return (int) $query->limit(-1)->offset(-1)->orderBy([])->count('*', $this->db);
        }
    }
}