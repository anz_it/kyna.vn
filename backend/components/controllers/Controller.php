<?php

namespace app\components\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use kyna\base\ActiveRecord;

/*
 * This is override class for Yii core Controller
 */
class Controller extends \yii\web\Controller
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionView($id)
    {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * @desc function to change status to all active records
     * @param integer $id
     */
    public function actionChangeStatus($id, $redirectUrl = false, $field = 'status')
    {
        $model = $this->findModel($id);

        if ($model->{$field} == ActiveRecord::STATUS_ACTIVE) {
            $model->{$field} = ActiveRecord::STATUS_DEACTIVE;
        } else {
            if ($model->{$field} == ActiveRecord::STATUS_DEACTIVE) {
                $model->{$field} = ActiveRecord::STATUS_ACTIVE;
            }
        }
        if ($model->save(false)) {
            return "Thay đổi thành công!";
        }
        else
            return "Thay đổi thất bại!";
    }



    /**
     * @desc delete function for Backend CRUDs
     * @param type $id
     * @return JSON mix or redirect URL
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            $ret = [
                'success' => true,
                'message' => 'Đã xóa thành công!',
            ];
            return $ret;
        } else {
            $redirectUrl = Yii::$app->request->get('redirectUrl');
            Yii::$app->session->setFlash('warning', 'Đã xóa thành công!');
            return $this->redirect(empty($redirectUrl) ? ['index'] : $redirectUrl);
        }
    }

}
