<?php

namespace app\components\controllers;

use Yii;
use yii\web\Response;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use kyna\base\models\MetaField;
use kyna\base\models\search\MetaFieldSearch;

/**
 * MetaFieldController implements the CRUD actions for MetaField model.
 */
class MetaFieldController extends Controller
{

    public $mainTitle = 'Meta Fields';
    public $modelType = '';

    public function init()
    {
        $ret = parent::init();

        $get = Yii::$app->request->get();
        if (isset($get['model'])) {
            if (in_array($get['model'], MetaField::getModelTypes())) {
                $this->modelType = $get['model'];
            }
        }

        return $ret;
    }

    public function beforeAction($action)
    {
        $ret = parent::beforeAction($action);

        if (in_array($action->id, ['index', 'create']) && empty($this->modelType)) {
            throw new NotFoundHttpException;
        }

        return $ret;
    }

    public function getViewPath()
    {
        return Yii::getAlias('@app/components/views/' . $this->id);
    }

    /**
     * Lists all MetaField models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MetaFieldSearch();
        $searchModel->model = $this->modelType;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single MetaField model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        $this->modelType = $model->model;

        return $this->render('view', [
            'model' => $model,
        ]);
    }

    /**
     * Creates a new MetaField model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new MetaField();
        $model->model = $this->modelType;
        $model->status = MetaField::STATUS_ACTIVE;

        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                Yii::$app->session->setFlash('success', 'Thêm thành công.');
                return $this->redirect(['view', 'id' => $model->id, 'model' => $this->modelType]);
            }
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing MetaField model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $this->modelType = $model->model;

        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                Yii::$app->session->setFlash('success', 'Cập nhật thành công.');
                return $this->redirect(['view', 'id' => $model->id, 'model' => $this->modelType]);
            }
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing MetaField model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id, $redirectUrl = false)
    {
        $model = $this->findModel($id);
        $this->modelType = $model->model;

        $model->delete();

        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $ret = [
                'message' => 'Đã xóa thành công',
                'success' => true,
            ];
            return $ret;
        } else {
            $redirectUrl = Yii::$app->request->get('redirectUrl');
            Yii::$app->session->setFlash('warning', 'Đã xóa');
            return $this->redirect(empty($redirectUrl) ? ['index', 'model' => $this->modelType] : $redirectUrl);
        }
    }

    /**
     * Finds the MetaField model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MetaField the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = MetaField::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
