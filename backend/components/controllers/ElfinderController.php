<?php

namespace app\components\controllers;

use Yii;

class ElfinderController extends \mihaildev\elfinder\PathController
{
    
    public function init()
    {
        $ret = parent::init();
        
        if (!Yii::$app->user->can('FileManager.Create')) {
            $this->disabledCommands[] = 'upload';
        }
        if (!Yii::$app->user->can('FileManager.Delete')) {
            $this->disabledCommands[] = 'rm';
        }
        if (!Yii::$app->user->can('FileManager.Update')) {
            $this->disabledCommands = array_merge($this->disabledCommands, [
                'edit', 
                'rename', 
                'move', 
                'copy', 
                'cut', 
                'paste', 
                'duplicate',
                'archive',
                'mkdir',
                'mkfile'
            ]);
        }
        
        return $ret;
    }
    
    public function actionManager()
    {
        return $this->renderFile("@app/modules/page/views/file-manager/manager.php", ['options' => $this->getManagerOptions()]);
    }

}
