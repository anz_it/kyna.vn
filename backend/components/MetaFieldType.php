<?php

namespace app\components;

use \yii\helpers\Json;
use kartik\select2\Select2;
use kartik\datetime\DateTimePicker;
use kartik\time\TimePicker;
use common\widgets\tinymce\TinyMce;
use common\widgets\codeeditor\CodeEditor;
use common\widgets\upload\Upload;

/*
 * This is class for proccess with Custom Field Type.
 */
class MetaFieldType
{

    const TYPE_TEXT = 1;
    const TYPE_TEXT_AREA = 2;
    const TYPE_NUMBER = 3;
    const TYPE_CHECKBOX = 4;
    const TYPE_CHECKBOX_LIST = 5;
    const TYPE_RADIO_BUTTON_LIST = 6;
    const TYPE_DROPDOWN_LIST = 7;
    const TYPE_TIME = 8;
    const TYPE_DATETIME = 9;
    const TYPE_EDITOR = 10;
    const TYPE_EDITOR_BASIC = 12;
    const TYPE_EDITOR_FULL = 15;
    const TYPE_JS = 11;
    const TYPE_IMG = 13;
    const TYPE_HTML = 14;

    /**
     * @desc return list type
     * @return array
     */
    public static function getList()
    {
        return [
            self::TYPE_TEXT => 'Text',
            self::TYPE_TEXT_AREA => 'Text Area',
            self::TYPE_NUMBER => 'Number',
            self::TYPE_CHECKBOX => 'Checkbox',
            self::TYPE_CHECKBOX_LIST => 'Checkbox List',
            self::TYPE_RADIO_BUTTON_LIST => 'Radio Button List',
            self::TYPE_DROPDOWN_LIST => 'Dropdown List',
            self::TYPE_TIME => 'Time',
            self::TYPE_DATETIME => 'Datetime',
            self::TYPE_EDITOR => 'Editor',
            self::TYPE_JS => 'Javascript',
            self::TYPE_HTML => 'HTML',
            self::TYPE_IMG => 'Uploaded Image',
            self::TYPE_EDITOR_FULL => 'Editor Full',
        ];
    }

    /**
     * @desc get the text of type
     * @param number $type
     * @return text or null
     */
    public static function getTypeText($type)
    {
        $list = self::getList();

        return isset($list[$type]) ? $list[$type] : null;
    }

    /**
     * @desc return list type has data set, ex: dropdown list, radio button list and checkbox list
     * @return array
     */
    public static function getListTypeHasDataSet()
    {
        return [
            self::TYPE_CHECKBOX_LIST => 'Checkbox List',
            self::TYPE_RADIO_BUTTON_LIST => 'Radio Button List',
            self::TYPE_DROPDOWN_LIST => 'Dropdown List',
        ];
    }

    public static function render($form, $model, $field, $options)
    {
        if (array_key_exists($model->metaField->type, self::getListTypeHasDataSet())) {
            $crudTitles = \Yii::$app->params['crudTitles'];
            $prompt = $crudTitles['prompt'];

            $values = Json::decode($model->metaField->data_set);
            if (is_array($values)) {
                $data = array_combine($values, $values);
            } else {
                $data = [];
            }
        }

        $isMultipleValues = in_array($model->metaField->type, [
            self::TYPE_CHECKBOX_LIST,
        ]);

        $label = htmlentities($model->metaField->name);
        $attribute = $field . ($isMultipleValues ? '[]' : '');
        $formField = $form->field($model, $attribute)->label($label);

        switch ($model->metaField->type) {

            case self::TYPE_NUMBER:
                return $formField->input('number', $options);

            case self::TYPE_TEXT_AREA :
                return $formField->textArea($options);

            case self::TYPE_CHECKBOX :
                return $formField->checkBox(array_merge($options, ['label' => $label]))->label(false);

            case self::TYPE_CHECKBOX_LIST :
                return $formField->checkboxList($data, array_merge($options, ['placeholder' => $prompt]));

            case self::TYPE_RADIO_BUTTON_LIST :
                return $formField->radioList($data, array_merge($options, ['placeholder' => $prompt]));

            case self::TYPE_DROPDOWN_LIST :
                return $formField->widget(Select2::classname(), [
                    'data' => $data,
                    'hideSearch' => true,
                    'options' => array_merge($options, ['placeholder' => $prompt]),
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);

            case self::TYPE_TIME :
                return $formField->widget(TimePicker::classname(), []);

            case self::TYPE_DATETIME :
                return $formField->widget(DateTimePicker::classname(), [
                        'options' => $options,
                        'pluginOptions' => [
                            'autoclose' => true
                        ]
                ]);

            case self::TYPE_EDITOR :
                return $formField->widget(TinyMce::className(), [
                    'layout' => 'advanced',
                ]);
            case self::TYPE_EDITOR_FULL :
                return $formField->widget(TinyMce::className(), [
                    'layout' => 'full',
                ]);

            case self::TYPE_EDITOR_BASIC:
                return $formField->widget(TinyMce::className(), [
                    'layout' => 'basic',
                ]);

            case self::TYPE_JS:
                return $formField->widget(CodeEditor::className(), [
                    'language' => 'javascript',
                ]);

            case self::TYPE_HTML:
                return $formField->widget(CodeEditor::className(), [
                    'language' => 'htmlmixed',
                ]);

            case self::TYPE_IMG:
                return $formField->widget(Upload::className(), ['display' => 'image']);

            case self::TYPE_TEXT:
            default:
                return $formField->textInput($options);
        }
    }

    public static function renderByModel($form, $model, $field, $options, $objectModel)
    {
        if (array_key_exists($model->metaField->type, self::getListTypeHasDataSet())) {
            $crudTitles = \Yii::$app->params['crudTitles'];
            $prompt = $crudTitles['prompt'];

            $values = Json::decode($model->metaField->data_set);
            if (is_array($values)) {
                $data = array_combine($values, $values);
            } else {
                $data = [];
            }
        }

        $isMultipleValues = in_array($model->metaField->type, [
            self::TYPE_CHECKBOX_LIST,
        ]);

        $label = htmlentities($model->metaField->name);
        $attribute = $field . ($isMultipleValues ? '[]' : '');
        $formField = $form->field($objectModel, $field)->label($label);

        switch ($model->metaField->type) {

            case self::TYPE_NUMBER:
                return $formField->input('number', $options);

            case self::TYPE_TEXT_AREA :
                return $formField->textArea($options);

            case self::TYPE_CHECKBOX :
                return $formField->checkBox(array_merge($options, ['label' => $label]))->label(false);

            case self::TYPE_CHECKBOX_LIST :
                return $formField->checkboxList($data, array_merge($options, ['placeholder' => $prompt]));

            case self::TYPE_RADIO_BUTTON_LIST :
                return $formField->radioList($data, array_merge($options, ['placeholder' => $prompt]));

            case self::TYPE_DROPDOWN_LIST :
                return $formField->widget(Select2::classname(), [
                    'data' => $data,
                    'hideSearch' => true,
                    'options' => array_merge($options, ['placeholder' => $prompt]),
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);

            case self::TYPE_TIME :
                return $formField->widget(TimePicker::classname(), []);

            case self::TYPE_DATETIME :
                return $formField->widget(DateTimePicker::classname(), [
                    'options' => $options,
                    'pluginOptions' => [
                        'autoclose' => true
                    ]
                ]);

            case self::TYPE_EDITOR :
                return $formField->widget(TinyMce::className(), [
                    'layout' => 'advanced',
                ]);

            case self::TYPE_EDITOR_FULL :
                return $formField->widget(TinyMce::className(), [
                    'layout' => 'full',
                ]);

            case self::TYPE_EDITOR_BASIC:
                return $formField->widget(TinyMce::className(), [
                    'layout' => 'basic',
                ]);

            case self::TYPE_JS:
                return $formField->widget(CodeEditor::className(), [
                    'language' => 'javascript',
                ]);

            case self::TYPE_HTML:
                return $formField->widget(CodeEditor::className(), [
                    'language' => 'htmlmixed',
                ]);

            case self::TYPE_IMG:
                return $formField->widget(Upload::className(), ['display' => 'image']);

            case self::TYPE_TEXT:
            default:
                return $formField->textInput($options);
        }
    }
}
