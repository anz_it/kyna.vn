<?php

namespace backend\controllers;

use app\components\WebErrorHandler;
use common\helpers\StringHelper;
use common\lib\CDNImage;
use common\widgets\upload\Upload;
use kyna\course\models\QuizQuestion;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use modules\user\models\LoginForm;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\web\UploadedFile;

/**
 * Site controller.
 */
class SiteController extends \yii\web\Controller
{
    public $layout = 'lte';
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index', 'upload'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
               // 'class' => 'yii\web\ErrorAction',
                'class' => 'app\components\WebErrorHandler',
            ],
        ];
    }





    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
//            if ($action->id == 'error' && \Yii::$app->errorHandler->exception->statusCode == 404) {
//                $this->layout = '404';
//            }
            return true;
        } else {
            return false;
        }
    }

    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionLogin()
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionUpload()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $result = [
            'success' => true,
        ];
        $fileInstance = UploadedFile::getInstanceByName("file[0]");
        if ($fileInstance != null) {
            if (isset($_POST['type'])) {
                if ($_POST['type'] == "image" && strpos($fileInstance->type, $_POST['type']) === false) {
                    $result['success'] = false;
                    $result['errorMsg'] = "File upload không phải là hình ảnh.";
                } else if ($_POST['type'] == "audio" && !in_array($fileInstance->extension, ['mp3', 'wav'])) {
                    $result['success'] = false;
                    $result['errorMsg'] = "Chỉ chấp nhận file mp3 và wav";
                }
            }
            if (empty($_POST['question_id']) || empty($quizQuestion = QuizQuestion::findOne($_POST['question_id']))) {
                $result['success'] = false;
                $result['errorMsg'] = "Câu hỏi không tồn tại";
            }
            if ($result['success'] === false)
                return $result;

            $cdnImage = new CDNImage($quizQuestion, $_POST['media_type'], $_POST['type']);
            if ($fileUploadedUrl = $cdnImage->upload($fileInstance)) {
                $result['url'] = $fileUploadedUrl;
            } else {
                $result['success'] = false;
                $result['errorMsg'] = 'Lưu file không thành công.'. $fileInstance->error;
            }
            return $result;
        } else {
            var_dump($fileInstance, $_FILES);
        }
    }
}
