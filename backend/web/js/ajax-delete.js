/**
 * Created by neiK on 2/7/2017.
 */

;(function($) {
    'use strict';

    $('body').on('click' ,'a.btn-delete', function(e){
        var confirm_massage = $(this).attr('confirm-message');
        if (confirm(confirm_massage) == false) {
            return false;
        }
        var href = $(this).attr('href');
        $.ajax({
            type:"POST",
            url:href,
            success: function (res){
                $.notifyDefaults({
                    type: 'warning',
                    allow_dismiss: true
                });
                $.notify(res.message);
            },
            error: function (res){
                $.notifyDefaults({
                    type: 'danger',
                    allow_dismiss: true
                });
                $.notify('Xóa thất bại!');
            },
            complete: function () {
                $.pjax.reload({container: '#idPjaxReload', timeout: 15000});
            }
        });
        return false;
    });

})(jQuery);
