;(function ($, _) {

    $('body').on('click', '.btn-popup',function (e) {
        e.preventDefault();
        var modal = $('#modal'),
            url = this.href;

        modal.find(".modal-content").load(url, function (resp) {
            modal.modal("show");
        });
    });

    // load content for submodal
    $('body').on('click', '.btn-popup-sub-modal',function (e) {
        e.preventDefault();
        var subModal = $('#sub-modal'),
            url = this.href;

        subModal.find(".modal-content").load(url, function (resp) {
            subModal.modal("show");
        });
    });

    // auto close modal when submodal closed
    // $('body').on('hidden.bs.modal', '#sub-modal', function (e) {
    //     // alert ('sub modal close');
    //     $('#modal').modal('toggle');
    // });

    $('body').on('pjax:beforeSend', function () {
        // alert ("hello");
        $('#loader').addClass('loading');
    });

    $('body').on('pjax:success', function() {
        // alert ("hihi");
        $('#loader').removeClass('loading');
    });

    $('body').on('beforeSubmit', '.form-data-ajax', function (e) {
        e.preventDefault();
        $('#loader').addClass('loading');
        $("#modal").modal("hide");
        var formData = $(this).serialize(),
            parseResp = function (resp) {
                if (resp.success === true || resp.success === 'true') {
                    $('#loader').removeClass('loading');
                    $.pjax.reload({container:'#idPjaxReload',timeout: 15000});
                    $.notify(resp.message,{
                        type: 'success',
                        allow_dismiss: true
                    });
                } else {
                    var message = (resp.message != null)? resp.message : 'Xử lý thất bại';
                    $('#loader').removeClass('loading');
                    $.pjax.reload({container:'#idPjaxReload',timeout: 15000});
                    $.notify({
                        message: message,
                    },{
                        type: 'danger',
                        allow_dismiss: true
                    });
                    console.log(resp);
                }
                return false;
            };

        if (this.method === 'post') {
            $.post(this.action, formData, parseResp).fail(function() {
                $('#loader').removeClass('loading');
                $.notify('Xử lý thất bại!',{
                    type: 'danger',
                    allow_dismiss: true
                });
            });
        }
        else {
            $.get(this.action, formData, parseResp);
        }

        return false;
    });

        $('#idPjaxReload').on('click', 'li.ajax-button a', function (e) {
            e.preventDefault();
            // alert("hello");
            $('#loader').addClass('loading');
            $("#modal").modal("hide");
            $.ajax({
                type: 'POST',
                url: $(this).attr('href'),
                success: function (res) {
                    data = JSON.parse(res);
                    if (data.success) {
                        $("#modal").modal("hide");
                        $('#loader').removeClass('loading');
                        $.pjax.reload({container:'#idPjaxReload',timeout: 15000});
                        $.notify("Hoàn tất cập nhật tình trạng giao vận",{
                            type: 'success',
                            allow_dismiss: true
                        });
                    } else {
                        $('#modal').modal("hide");
                        $('#loader').removeClass('loading');
                        $.pjax.reload({container:'#idPjaxReload',timeout: 15000});
                        $.notify('Xử lý thất bại!',{
                            type: 'danger',
                            allow_dismiss: true
                        });
                        return false;
                    }
                }
            });
            return false;
        });

})(jQuery, _);