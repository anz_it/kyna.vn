;(function($) {
    'use strict';

    $('body').on('click' ,'ul.pagination li', function(e){
        $('html, body').animate({ scrollTop: 0 }, "slow");
    });

})(jQuery);
