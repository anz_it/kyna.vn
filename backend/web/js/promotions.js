/**
 * Created by hongta on 6/21/16.
 */

$(document).ready(function(){
    $('.generate-code').change(function(){
        var checked = $('.generate-code').prop("checked");
        if(checked == true){
            //console.log('checked');
            $('.prefix-input').removeAttr('disabled');
            $('.field-coupon-code').hide();
            $('.quantity').removeAttr('disabled');
        }else {
            //console.log('Unchecked');
            $('.prefix-input').attr('disabled','');
            $('.field-coupon-code').show();
            $('.quantity').attr('disables', '');
        }
    });
});
