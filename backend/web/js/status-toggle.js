/**
 * Created by neiK on 12/27/2016.
 */

;(function($) {
    'use strict';

    $('body').on('change' ,'input.status-toggle', function(e){
        $('#loader').addClass('loading');
        var href = $(this).attr('data-href');
        var thisInput = $(this);

        $.ajax({
            type:"POST",
            url:href,
            success: function (res){
                if (res.status === false) {
                    $.notifyDefaults({
                        type: 'warning',
                        allow_dismiss: true
                    });

                    thisInput.data("bs.toggle").off(true);
                } else {
                    $.notifyDefaults({
                        type: 'success',
                        allow_dismiss: true
                    });
                }
                if (res.msg && res.msg != '') {
                    $.notify(res.msg);
                } else {
                    $.notify('Thay đổi thành công!');
                }
            },
            error: function (res){
                $.notifyDefaults({
                    type: 'danger',
                    allow_dismiss: true
                });
                $.notify('Thay đổi thất bại!');
            },
            complete: function () {
                $('#loader').removeClass('loading');
                if ($('#idPjaxReload').length > 0) {
                    $.pjax.reload({container: '#idPjaxReload', timeout: 15000});
                }
            }
        });
    });
    $('body').on('pjax:success', function() {
        $(function() {
            $('input.status-toggle').bootstrapToggle();
        });
    });
})(jQuery);