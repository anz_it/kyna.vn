/**
 * Created by khanhphan on 10/6/16.
 */
var app = angular.module('UpdateQuestionApp', ['ui.tinymce','ngFileUpload' ]);
app.controller('UpdateQuestionController', function ($scope, $http, $sce, Upload, $timeout) {
    $scope.init = function () {
        $scope.data = window.questionData;
        $scope.params = window.appParams;
        $http.defaults.headers.post['Content-Type'] = 'application/json;charset=utf-8';
        $http.defaults.headers.post['X-CSRF-Token'] = $('meta[name="csrf-token"]').attr("content");
        $scope.loading = false;
        $scope.showEmbed = false;
        $scope.answerImageUrl = false;
    };

    $scope.toggleEmbed = function () {
        $scope.showEmbed = !$scope.showEmbed;
    };

    $scope.uploadImage = function (files) {
        $scope.imageFile = files;
        if (files && files.length) {
            Upload.upload({
                url: '/site/upload',
                async: false,
                data: {
                    file: files,
                    type: 'image',
                    question_id: $scope.data.id,
                    media_type: 'question'
                },
                headers : {
                    'X-CSRF-Token' : $http.defaults.headers.post['X-CSRF-Token']

                }
            }).then(function (response) {
                $timeout(function () {
                    if (response.data.success == false) {
                        $scope.imageError = response.data.errorMsg;
                    } else {
                        $scope.data.imageUrl = response.data.url;
                        console.log($scope.data);
                    }
                });
            }, function (response) {

            }, function (evt) {
                $scope.imageProcess =
                    Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
            });
        }
    };

    $scope.uploadModelImage = function (model, files) {
        $scope.imageFile = files;
        if (files && files.length) {
            Upload.upload({
                url: '/site/upload',
                data: {
                    file: files,
                    type: 'image',
                    question_id: $scope.data.id,
                    media_type: 'question'
                },
                headers : {
                    'X-CSRF-Token' : $http.defaults.headers.post['X-CSRF-Token']

                }
            }).then(function (response) {
                $timeout(function () {
                    if (response.data.success == false) {
                        model.imageError = response.data.errorMsg;
                    } else {
                        model.imageUrl = response.data.url;
                    }
                });
            }, function (response) {

            }, function (evt) {
                model.imageProcess =
                    Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
            });
        }
    };

    $scope.uploadAnswerImage = function (files) {
        $scope.imageFile = files;
        if (files && files.length) {
            Upload.upload({
                url: '/site/upload',
                data: {
                    file: files,
                    type: 'image',
                    question_id: $scope.data.id,
                    media_type: 'answer'
                },
                headers : {
                    'X-CSRF-Token' : $http.defaults.headers.post['X-CSRF-Token']

                }
            }).then(function (response) {
                $timeout(function () {
                    if (response.data.success == false) {
                        $scope.imageError = response.data.errorMsg;
                    } else {
                        $scope.answerImageUrl = response.data.url;
                        console.log($scope.data);
                    }
                });
            }, function (response) {

            }, function (evt) {
                $scope.answerImageProcess =
                    Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
            });
        }
    };

    $scope.uploadAudio = function (files) {
        $scope.audioFile = files;
        if (files && files.length) {
            Upload.upload({
                url: '/site/upload',
                data: {
                    file: files,
                    type: 'audio',
                    question_id: $scope.data.id,
                    media_type: 'question'
                },
                headers : {
                    'X-CSRF-Token' : $http.defaults.headers.post['X-CSRF-Token']

                }
            }).then(function (response) {
                $timeout(function () {
                    if (response.data.success == false) {
                        $scope.audioError = response.data.errorMsg;
                    } else {
                        $scope.data.audioUrl = response.data.url;
                        console.log($scope.data);
                    }
                });
            }, function (response) {

            }, function (evt) {
                $scope.audioProcess =
                    Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
            });
        }
    };

    $scope.removeAudio = function () {
        $scope.data.audioUrl = "";
    }

    $scope.removeImage = function () {
        $scope.data.imageUrl = "";
        $scope.imageProcess = 0;
    }

    $scope.removeTempAnswerImage = function (){
        $scope.answerImageUrl = "";
        $scope.answerImageProcess = 0;
    }

    $scope.removeAnswerImage = function (answer){
        answer.imageUrl = "";
        answer.imageProcess = -1;
        answer.errorImage = "";
    }

    $scope.renderHtml = function(html_code) {
        return $sce.trustAsHtml(html_code);
    };

    $scope.pattern = /_+\(\d+\)_+/g;

    $scope.addMoreAnswer = function (question) {
        if (!question.answers) {
            question.answers = [];
        }
        if ((question.insertText == undefined || question.insertText.trim().length == 0) && ($scope.answerImageUrl.length == undefined || $scope.answerImageUrl.length <= 0)) {
            bootbox.alert("Bạn vui lòng không để trống câu trả lời.");
            //TODO: add error class here
            return false;
        }
        var data = {content: question.insertText};
        if ($scope.answerImageUrl.length > 0) {
            data.imageUrl = $scope.answerImageUrl;
        }
        question.answers.push(data);
        question.insertText = "";
        $scope.answerImageUrl = "";
        $scope.answerImageProcess = 0;
    }

    $scope.addMoreAnswerConnect = function()
    {
        $scope.data.questions.push({
           question : { content : ""},
           answer : {content : "" }
        });
    }

    $scope.removeAnswer = function (answer) {
        answer.isDeleted = true;
    }

    $scope.unremovedAnswer = function (answer) {
        answer.isDeleted = false;
    }

    $scope.genAnswers = function () {
        var matchQuestions = $scope.data.questionContent.match($scope.pattern);

        for (var index in matchQuestions) {
            if (!$scope._isQuestionNameExist(matchQuestions[index])) {
                var questionObject = {
                    content: matchQuestions[index]
                }
                $scope.data.questions.push(questionObject);
            }

        }
    }
    $scope.insertEmpty = function () {
        var match =  $scope.data.questionContent.match($scope.pattern);
        var maxNum = 0;
        for (var index in match) {
            var num = parseInt(match[index].replace(/[_\(\)]+/g, ''));
            if (num > maxNum)
                maxNum = num;

        }
        //tinyMCE.activeEditor.execCommand('mceInsertContent', false, " _("+(maxNum+1)+")_____ ");
        $scope.data.questionContent += " _("+(maxNum+1)+")_____ ";

    }

    $scope.tinymceOptions = {
        menubar: false,
        statusbar: false,
        height: "150px",
        plugins: 'link image code textcolor',
        toolbar: "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image forecolor backcolor"
    };

    $scope.tinymceNoImageOptions = {
        menubar: false,
        statusbar: false,
        height: "150px",
        plugins: 'link image code textcolor',
        toolbar: "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link forecolor backcolor"
    };

    $scope.tinymceOptionSimple = {
        menubar: false,
        statusbar: false,
        height: "180px",

        plugins: 'link image',
        toolbar: "link image"
    };

    $scope._isQuestionNameExist = function (name) {
        for (var index in $scope.data.questions) {
            if ($scope.data.questions[index].content == name)
                return true;
        }
        return false;
    }

    $scope.onCorrectAnswerChange = function (answer) {
        if (answer.isCorrect) {
            for (var index in $scope.data.answers) {
                $scope.data.answers[index].isCorrect = false;
            }
            answer.isCorrect = true;
        }

    }

    $scope.save = function () {
        $scope.data._csrf = $('input[name="_csrf"]').val();
        $scope.loading = true;


        $http({
            url : "/course/quiz-question/update?id="+$scope.data.id,
            method: 'POST',
            data: $scope.data

        }).then(function success(response){
            $scope.data = response.data.data;
            if (response.data.success) {
                bootbox.alert({
                    message: "Cập nhật thành công!",
                    size: 'small'
                });
            }
            $scope.loading = false;
        }, function error() {
            $scope.loading = false;
        });
        return false;
    }

    $scope.init();

}).directive('ngEnter', function () {
    return function (scope, element, attrs) {
        element.bind("keydown keypress", function (event) {
            if (event.which === 13) {
                scope.$apply(function () {
                    scope.$eval(attrs.ngEnter);
                });

                event.preventDefault();
            }
        });
    };
});