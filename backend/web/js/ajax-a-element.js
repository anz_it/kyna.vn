/**
 * Created by 40801 on 1/20/2017.
 */
 ;(function($) {
    'use strict';

    $('body').on('click', 'a.a-ajax', function (e) {
        if ($(this).attr('confirm-message')) {
            var confirm_massage = $(this).attr('confirm-message');
            if (confirm(confirm_massage) == false) {
                return false;
            }
        }
        $('#loader').addClass('loading');
        var href = $(this).attr('href');
        $.ajax({
            type: "POST",
            url: href,
            success: function (res){
                $.notifyDefaults({
                    type: 'success',
                    allow_dismiss: true
                });
                $.notify(res.massage);
            },
            error: function (res){
                $.notifyDefaults({
                    type: 'danger',
                    allow_dismiss: true
                });
                $.notify('Xử lý bị lỗi!');
            },
            complete: function() {
                $('#loader').removeClass('loading');
                $.pjax.reload({container: '#idPjaxReload', timeout: 15000});
            }
        });
        return false;
    });

})(jQuery);
