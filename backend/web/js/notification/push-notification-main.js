/**
 * Created by ngunp on 7/31/2017.
 */
function push_notification_main() {
    if (!'serviceWorker' in navigator) {
        console.log("Browser not support service worker!");
        return;
    }
    navigator.serviceWorker.register('/js/notification/push-notification-sw.js')
        .then(function (reg) {
            console.log(reg);
        })

    switch (Notification.permission) {
        case 'granted':
            console.log('Permission granted!!');
            break;
        case 'denied':
            console.log('Permission denied');
            break;
        case 'default':
            //no break
        default:
            console.log('get permission')
            Notification.requestPermission(function(status) {
                console.log('Notification permission status:', status);
            });
            break;
    }
}

push_notification_main();