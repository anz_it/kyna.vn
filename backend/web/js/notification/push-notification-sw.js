/**
 * Created by ngunp on 7/31/2017.
 */

// Listen for install event, set callback
self.addEventListener('install', function(event) {
    // Perform some task
    console.log('install');
    // self.registration.showNotification("install");
});

self.addEventListener('activate', function(event) {
    // Perform some task
    console.log('activate');
});

self.addEventListener('notificationclick', function(event) {
    var notification = event.notification;
    // var primarykey = notification.data.primaryKey;
    var action = event.action;
    // console.log(action);
    console.log(event);

    if (notification.data != null) {
        // var data = JSON.parse(notification.data);
        var data = notification.data;
        // console.log(data);
    }
    switch (action) {
        // case 'close':
        //     console.log("acion close");
        //     notification.close();
        //     break;
        // case 'explore':
        //     console.log("action explore");
        //     clients.openWindow("http://kyna.frontend.local");
        //     notification.close();
        //     break;
        default:
            console.log("unrecognized action");
            if (data != null && data.url != null)
                clients.openWindow(data.url);
            // clients.openWindow("https://kyna.vn");
            notification.close();
    }
});
