/**
 * Created by neiK on 2/7/2017.
 */

;(function($) {
    'use strict';

    $('body').on('click' ,'div[data-toggle="toggle"]', function(e){
        var data_confirm = $(this).find('input').attr('confirm-message');
        if (confirm(data_confirm) == false) {
            return false;
        } else {
            return true;
        }
    });

})(jQuery);