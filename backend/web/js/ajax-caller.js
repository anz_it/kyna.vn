;(function($) {
    'use strict';

    window.ajaxCaller = window.ajaxCaller || {};
    window.ajaxCaller.doAjax = function (el, url, submitData) {
        var data = $(el).data(),
            title = el.title || $(el).text() || url,
            targetSelector = $(el).data("target"),
            $target = $(targetSelector);

        if (!$(el).is("form")) {
            $target.removeData("bs.modal").removeData("modal");

            if (data.pushState !== "false" && data.pushState !== false) {
                window.history.pushState(data, title, url);
            }

            if (data.toggle === "popup") {
                $target.one("shown.bs.modal", function(e) {
                    $(this).find('.modal-content').load(url, submitData);
                });
                $target.one("hidden.bs.modal", function(e) {
                    $(e.target).removeData("bs.modal").removeData("modal").find(".modal-content").empty();

                    if (data.pushState !== "false" && data.pushState !== false) {
                        window.history.back();
                    }
                });
                $target.modal("show");
                return;
            }
            $target.load(url, submitData);
        }
        else {
            if (el.method !== 'get' && el.method !== 'GET') {
                $.post(url, submitData, function(resp) {
                    if (resp.success === true || resp.success === 'true') {
                        $target.modal("hide");
                        $target.closest('#modal').modal('hide');
                        $.pjax.reload({container:'#idPjaxReload',timeout: 15000});
                        $.notify(resp.message,{
                            type: 'success',
                            allow_dismiss: true
                        });
                    } else {
                        $target.html(resp);
                    }
                });
            }
            else {
                $.get(url, submitData, function(resp) {
                    $target.html(resp);
                });
            }
        }

    }

    $("body")
        .on("click", "a[data-ajax]", function (e) {
            e.preventDefault();
            window.ajaxCaller.doAjax(this, this.href, $(this).data())
        })
        .on("submit", "form[data-ajax]", function(e) {
            e.preventDefault();
            window.ajaxCaller.doAjax(this, this.action, $(this).serialize())
        });
})(jQuery);
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
