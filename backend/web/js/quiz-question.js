/**
 * Created by hongta on 6/30/16.
 */

$(document).ready(function(){
    var type_fill_dots = 3;
    showInput();
    $(".question-type").change(function(e){
        showInput();
    });
    function showInput(){
        var ques_type_val  = $('.question-type').val();
        //console.log(ques_type_val);
        if(ques_type_val == type_fill_dots){
            $('.number-of-dots').removeAttr('disabled');
        }else {
            $('.number-of-dots').attr('disabled', '');
        }
    }
});
