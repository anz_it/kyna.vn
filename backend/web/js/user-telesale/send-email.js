/**
 * Created by ngunp on 7/21/2017.
 */
;(function ($, _) {

    $('body').on('click', '.submit-button', function(e) {
        $('#usertelesale-action_value').val($(this).attr('name'));
    });

    // click modal's quick-send button when click sub-modal's quick-send button
    $('body').on('click', '#sub-modal button[name="quick-send"]', function(e) {
        $('#sub-modal').modal('hide');
        $('#modal input[type="submit"]').click();
    });

    // $('body').on('hide.bs.modal', '#sub-modal', function (e) {
    //     $('#modal input').prop("disabled", false);
    // });
    //
    // $('body').on('show.bs.modal', '#sub-modal', function (e) {
    //     // disable button on #modal
    //     $('#modal input').prop("disabled", true);
    // })
})(jQuery, _);