<?php
/**
 * Created by PhpStorm.
 * User: Tan
 * Date: 12/14/2018
 * Time: 5:18 PM
 */

namespace kid\models;


use yii\base\Model;

class KidPackage extends Model
{
    public $package_subscription_id;
    public $package_name;
    public $subscription_name;
    public $total_amount;
    public $affiliate_percent;
    public $description;

    public function rules()
    {
        return [
            [['package_subscription_id', 'total_amount', 'affiliate_percent'], 'integer'],
            [['package_subscription_id', 'total_amount', 'affiliate_percent'], 'required'],
            [['description', 'package_name', 'subscription_name'], 'string']
        ];
    }
}