<?php
/**
 * Created by PhpStorm.
 * User: Tan
 * Date: 11/6/2018
 * Time: 4:25 PM
 */

namespace kid\models;


use kyna\commission\models\AffiliateUser;

class KidAffiliateIncome extends \kyna\commission\models\KidAffiliateIncome
{
    public static function addCommission(
        $orderId,
        $invoiceCode,
        $affiliateId,
        $category,
        $orderStatusId,
        $orderStatus,
        $userName,
        $email,
        $phoneNumber,
        $total,
        $affiliateCommissionAmount,
        $description
    )
    {
        $commission = self::find()->andWhere(['order_id' => $orderId])->one();
        if (empty($commission)) {
            $commission = new self();
        }
        $commission->order_id = $orderId;
        $commission->invoice_code = $invoiceCode;
        $commission->affiliate_id = $affiliateId;
        $commission->register_name = $userName;
        $commission->register_email = $email;
        $commission->category = $category;
        $commission->order_status_id = $orderStatusId;
        $commission->order_status = $orderStatus;
        $commission->register_phone_number = $phoneNumber;
        $commission->total_amount = $total;
        $commission->description = $description;
        $commission->affiliate_commission_amount = $affiliateCommissionAmount;


        if ($commission->save()) {

            $email = null;
            $affCategory = null;
            $affiliateUser = AffiliateUser::find()->andWhere(['user_id' => $affiliateId, 'status' => AffiliateUser::STATUS_ACTIVE])->one();

            if (!empty($affiliateUser)) {
                $user = $affiliateUser->user;
                if (!empty($user)) {
                    $email = $user->email;
                    $categoryAff = $affiliateUser->affiliateCategory;
                    if (!empty($categoryAff)) {
                        $affCategory = $categoryAff->name;
                    }
                }
            }
            return ['order_id' => $orderId, 'affiliate_email' => $email, 'affiliate_category' => $affCategory];
        }
        return false;
    }
}