<?php
namespace kid\models;

use kyna\course\models\Course;
use yii\data\ActiveDataProvider;

class CourseDetail extends Course
{

    public function init()
    {
        $this->on(self::EVENT_AFTER_FIND, [$this, 'fillProperty']);
    }
    public function fields()
    {
        $fields = ['id', 'name', 'type','courses'];
        return $fields;
    }
    public function fillProperty()
    {

    }
    public function getCourses()
    {
        return $this->hasMany(CourseComboItem::className(), ['course_combo_id' => 'id']);
    }

}