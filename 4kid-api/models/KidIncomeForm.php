<?php
/**
 * Created by PhpStorm.
 * User: Tan
 * Date: 11/6/2018
 * Time: 4:37 PM
 */

namespace kid\models;

use kyna\commission\models\AffiliateUser;
use yii\base\Model;
use Yii;

class KidIncomeForm extends Model
{
    public $order_id;
    public $invoice_code;
    public $order_status_id;
    public $order_status;
    public $status;
    public $category;
    public $user_name;
    public $phone_number;
    public $email;
    public $affiliate_id;
    public $total_amount;
    public $affiliate_commission_amount;
    public $description;


    public function rules()
    {
        return [
            [['order_id', 'affiliate_id', 'order_status_id', 'total_amount', 'affiliate_commission_amount'], 'integer'],
            [['order_id', 'affiliate_id'], 'required'],
            [['email', 'user_name', 'phone_number', 'order_status', 'category', 'description', 'invoice_code'], 'string'],
            ['affiliate_id', 'checkIsAffiliate'],

        ];
    }

    public function checkIsAffiliate($attribute)
    {
        /* @var AffiliateUser $affiliateUser */
        $affiliateUser = AffiliateUser::find()->andWhere(['user_id' => $this->affiliate_id, 'status' => AffiliateUser::STATUS_ACTIVE])->one();
        if (empty($affiliateUser)) {
            $this->addError($attribute, 'Affiliate không đúng');
            return false;
        }


    }

    public function addCommission()
    {
        if ($this->validate()) {

            return KidAffiliateIncome::addCommission(
                $this->order_id,
                $this->invoice_code,
                $this->affiliate_id,
                $this->category,
                $this->order_status_id,
                $this->order_status,
                $this->user_name,
                $this->email,
                $this->phone_number,
                $this->total_amount,
                $this->affiliate_commission_amount,
                $this->description

            );
        }
        return $this;
    }
}