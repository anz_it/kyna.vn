<?php


namespace kid\models;


class CourseComboItem extends  \kyna\course_combo\models\CourseComboItem
{
    public function fields()
    {
        $fields = ['id','course_id','course'];
        return $fields;
    }

    public function getCourse()
    {
        //return $this->course_id;
        return CourseDetail::findOne(['id' => $this->course_id]);
    }
}