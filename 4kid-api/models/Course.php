<?php
/**
 * Created by PhpStorm.
 * User: truongn
 * Date: 11/3/17
 * Time: 5:09 PM
 */

namespace kid\models;


use yii\data\ActiveDataProvider;

class Course extends CourseDetail
{
    public function fields()
    {
        $fields = ['id', 'name'];
        return $fields;
    }

}