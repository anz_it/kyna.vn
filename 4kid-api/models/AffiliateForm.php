<?php
/**
 * Created by PhpStorm.
 * User: Tan
 * Date: 11/8/2018
 * Time: 1:40 PM
 */

namespace kid\models;


use common\helpers\ArrayHelper;
use kyna\commission\models\AffiliateUser;
use kyna\settings\models\Setting;
use yii\base\Model;
use Yii;

class AffiliateForm extends Model
{

    public $current_affiliate_id;
    public $new_affiliate_id;

    public function rules()
    {
        return [
            ['new_affiliate_id', 'required'],
            [['current_affiliate_id', 'new_affiliate_id'], 'integer'],
        ];
    }

    public function checkAffiliate()
    {
        if (!$this->validate()) {
            return $this;
        }
        $settings = ArrayHelper::map(Setting::find()->all(), 'key', 'value');
        if ($this->current_affiliate_id != $this->new_affiliate_id) {

            /* @var AffiliateUser $affiliateUser */
            $affiliateUser = AffiliateUser::find()->andWhere(['user_id' => $this->new_affiliate_id,
                'status' => AffiliateUser::STATUS_ACTIVE])->one();
            if(empty($affiliateUser)){
                return false;
            }

            /* @var AffiliateUser $affiliateUser */
            $currentAffUser = AffiliateUser::find()->andWhere(['user_id' => $this->current_affiliate_id,
                'status' => AffiliateUser::STATUS_ACTIVE])->one();

            if (!empty($currentAffUser)) {
                if ($affiliateUser->affiliate_category_id == Yii::$app->params['idOfOtherDisplayAffCategory']) {
                    if (isset($settings['priority_aff_ids'])) {
                        $priorityAffIds = explode(',', $settings['priority_aff_ids']);
                        if (!empty($priorityAffIds) && in_array($this->current_affiliate_id, $priorityAffIds)) {
                            return false;
                        }
                    }
                    if ($currentAffUser->affiliateCategory->affiliate_group_id == Yii::$app->params['id_aff_group_third_party']) {
                        return false;
                    }
                }

                // validate rule override affiliate category
                if (in_array($affiliateUser->affiliate_category_id,
                    $currentAffUser->affiliateCategory->category_can_not_override)) {
                    return false;
                }

                // aff has prefix 'Kyna'(in setting kyna_aff_cat_ids) cannot overwrite special affs in setting id_aff_can_not_overwrite_by_kyna
                $kynaAffIds = explode(',', $settings['kyna_aff_cat_ids']);
                $specialAffIds = explode(',', $settings['id_aff_can_not_overwrite_by_kyna']);
                if (in_array($this->current_affiliate_id, $specialAffIds) && in_array($affiliateUser->affiliate_category_id,
                        $kynaAffIds)) {
                    return false;
                }
            }
            return true;
        }
        return false;

    }
}