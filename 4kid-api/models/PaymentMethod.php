<?php
/**
 * Created by PhpStorm.
 * User: Tan
 * Date: 4/6/2018
 * Time: 9:19 AM
 */

namespace kid\models;


use kyna\order\models\Order;
use kyna\payment\events\TransactionEvent;

class PaymentMethod extends \kyna\payment\models\PaymentMethod
{
    public function init()
    {
        $ret = parent::init();
/*
        TransactionEvent::on(PaymentMethod::className(), PaymentMethod::EVENT_PAYMENT_SUCCESS, [$this, 'completeOrder']);
        return $ret*/;
        $this->on(self::EVENT_PAYMENT_SUCCESS, [$this, 'completeOrder']);

    }
    public function completeOrder($transactionEvent)
    {
        $transData = $transactionEvent->transData;

        Order::complete($transData['orderId'], $transData['transactionCode']);

        \Yii::$app->session->setFlash('need-tracking', true);
    }

}