<?php
/**
 * Created by PhpStorm.
 * User: khanhphan
 * Date: 9/26/17
 * Time: 10:57 AM
 */

$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'), require(__DIR__ . '/../../common/config/params-local.php'), require(__DIR__ . '/params.php'), require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'kid',
    'language' => '',
    'basePath' => dirname(__DIR__),
    'bootstrap' => [
        'log',
    ],
    'controllerNamespace' => 'kid\controllers',

    'components' => [

        'urlManager' => [
            'enablePrettyUrl' => true,
            'enableStrictParsing' => true,
            'showScriptName' => false,
            'rules' => [
                'v1/test/<action>' => 'v1/test/<action>',
                'v1/kid/<action>' => 'v1/kid/<action>',
                'v1/affiliate/<action>' => 'v1/affiliate/<action>',
            ],
        ],
        'request' => [
            'parsers' => [
                'application/json' => 'yii\web\JsonParser',
            ],
            'enableCsrfValidation' => true,
            'cookieValidationKey' => 'fM2hT237NcmHV8A4yNvPBa8J8s0iMLa8',
            'csrfCookie' => [
                'name' => '_csrf',
                'path' => '/',
                'domain' => ".4kid-api-kyna.local",
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],


        'response' => [
            'class' => 'yii\web\Response',
            'on beforeSend' => function ($event) {
                $response = $event->sender;
                    $response->data = [
                        'success' => $response->isSuccessful,
                        'data' => $response->data,
                    ];
                    //$response->statusCode = 200;

            },
        ],


        'cart' => [
            'class' => 'kid\modules\cart\components\ShoppingCart',
            'cartId' => 'kyna-cart',
        ],

        'assetManager' => [
            //'baseUrl' => '@cdn/assets',
            'linkAssets' => false,
            'bundles' => [
                'yii\web\JqueryAsset' => [
                    'sourcePath' => '@bower',
                    'js' => [
                        'jquery/dist/jquery.min.js',
                    ]
                ],
                'yii\bootstrap\BootstrapAsset' => [
                    'css' => [],
                ],
            ],
        ],
    ],
    'modules' => [
        'v1' => [
            'class' => 'kid\modules\v1\V1Module',
        ],
        'user' => [
            'class' => 'dektrium\user\Module',
        ],
        'treemanager' =>  [
            'class' => '\kartik\tree\Module',
        ],
        'cart'=>[
            'class' => 'kid\modules\cart\CartModule',
        ]
    ],
    'params' => $params
];