<?php

namespace kid\modules\v1\controllers;


use kid\components\RestController;
use Yii;

class TestController extends  RestController
{



    public function actionTest1()
    {

        if (Yii::$app->user->isGuest) {
            return ['message' => 'Toi la guest11'];
        }
        return ['message' => 'User ID của tôi là '.Yii::$app->user->id];
    }

    public function actionTest2()
    {
        if (Yii::$app->user->isGuest) {
            return ['message' => 'Toi la guest'];
        }
        return ['message' => 'User ID của tôi là '.Yii::$app->user->id];
    }
}