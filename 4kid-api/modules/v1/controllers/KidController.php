<?php
namespace kid\modules\v1\controllers;
use kid\models\Course;
use yii\data\ActiveDataProvider;
use kid\components\RestController;
use Yii;
use kid\models\CourseDetail;
use kid\models\Product;
use kyna\page\models\Page;
use kyna\user\models\UserTelesale;
use kyna\order\models\Order;
use kyna\user\models\TimeSlot;
use common\helpers\RoleHelper;
use kyna\user\models\User;
use yii\helpers\Url;
use kyna\payment\events\TransactionEvent;
use  kid\models\PaymentMethod;

class KidController extends  RestController
{

   /* public function init()
    {
        $ret = parent::init();

        TransactionEvent::on(PaymentMethod::className(), PaymentMethod::EVENT_PAYMENT_SUCCESS, [$this, 'completeOrder']);

        return $ret;
    }*/

    public function actionGetKidCourses($search = '',$exclude_ids = '', $limit = 10)
    {
        $query = CourseDetail::find()->where(['is_4kid' => 1,'status'=>1 ]);
        $exclude_ids = explode(",", $exclude_ids);
        if(is_array($exclude_ids))
         $query->andWhere(['NOT IN','id', $exclude_ids]);
        $query->andWhere(['like', 'name', $search]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'attributes' => ['id'],
                'defaultOrder' => [
                    'id' => SORT_DESC
                ]
            ]
        ]);
        $dataProvider->pagination->pageSize = $limit;

        return array('total_count'=>$dataProvider->getTotalCount(),'list_course'=>$dataProvider->getModels());;

    }
    public function actionGetKidIds($exclude_ids = '')
    {
        $query = CourseDetail::find()->where(['is_4kid' => 1,'status'=>1 ]);
        $exclude_ids = explode(",", $exclude_ids);
        if(is_array($exclude_ids))
            $query->andWhere(['NOT IN','id', $exclude_ids]);

        $query->select('id');

        $results = $query->asArray()->all();
        $ids = array_column($results,'id');
        return $ids;

    }
    public function actionCheckCourseForkid($course_id){
        return CourseDetail::find()->where(['is_4kid' => 1,'status'=>1, 'id'=>$course_id])->exists();
    }
    public function actionRegister()
    {
        $postData = Yii::$app->getRequest()->getBodyParams();
        $client_domain =  !empty($postData['domain']) ? $postData['domain'] : Yii::$app->request->getHostInfo();
        /* Tùy chỉnh các thông số */
        list($user_type, $plan_id, $course_id, $combo_id, $list_combo_ids, $cod_money, $bonus, $combo_tuong_tac,
            $combo_khuyen_mai, $advice_name, $filter_advice, $khoa_hoc_quan_tam, $alias_name, $email,
            $phonenumber, $fullname, $address, $city, $district, $note, $cong_viec, $han_che,
            $noi_lam_viec, $full_url, $page_slug, $landing_type, $list_course_ids, $slug, $payment_type,
            $affiliate_id, $price, $is_non_check_email) = $this->getParamsValue($postData);


        if(empty($email) && !empty($is_non_check_email))
        {
            $email = "hotro" . time() . '@kyna.vn';
        }

        $res = $this->checkPhoneEmail($phonenumber, $email);
        if (isset($res['status']) && !$res['status']) {
            return $res;
        }
        $user_type = UserTelesale::TYPE_SALES_4KID;
        $sell_price = 0;
        $course_ids = array();
        if(!empty($list_combo_ids))
        {
            $combo_ids = explode(",", $list_combo_ids);
            if(!empty($combo_ids))
                $course_ids = $combo_ids;
        }
        $course_name = '';
        if ($course_id > 0) {
            $user_type = UserTelesale::TYPE_SALES_4KID;
            // Tính Cod money
            $course = CourseDetail::findOne($course_id);
            if ($course) {
                $course_name .= $course->name."<br>";
                $sell_price = $course->sellPrice;
            }
        }
        if($combo_id > 0) {
            $user_type = UserTelesale::TYPE_SALES_4KID;
            $course = CourseDetail::findOne($combo_id);
            $sell_price = $course->getSellPrice();
            $course_name .= $course->name."<br>";
            if ($alias_name != '') {
                $advice_name = $alias_name."<br>";
            }
        }
        if (!empty($list_course_ids)) {
            $user_type = UserTelesale::TYPE_SALES_4KID;
            $courseIDs = explode(',', $list_course_ids);
            $course_ids = array_merge($course_ids, $courseIDs);
        }

        foreach ($course_ids as $courseID)
        {
            $course = CourseDetail::findOne($courseID);
            if(!empty($course))
            {
                $course_name .= $course->name . "<br/>";
                $sell_price += $course->sellPrice;
            }
        }
        if(empty($advice_name)){
            $advice_name = $course_name;
        }


        $model = Page::getUserTelesale($email, $user_type, $advice_name);
        $existOrder = false;
        if (!empty($course) || !empty($course_ids)) {
            $courseIDs = !empty($course) ? $course->id : explode(',', $course_ids);
            $existOrder = Page::getIsExistOrder($email, $courseIDs);
        }

        if ($existOrder) {
            if ($model) {
                if ($model->affiliate_id > 0 && $model->affiliate_id != $affiliate_id && $affiliate_id > 0) {
                    $old_affiliate_id = $model->affiliate_id;;
                    $model->affiliate_id = $affiliate_id;
                    $model->old_affiliate_id = $old_affiliate_id;
                    $model->updated_time = time();
                } else if ($model->affiliate_id == 0 && $affiliate_id > 0) {
                    $model->affiliate_id = $affiliate_id;
                }
                $model->save(false);
            }
            $res['status'] = 0;
            $res['errors'] = [];
            if (in_array($payment_type, [Page::PAYMENT_TYPE_ONEPAY_CC, Page::PAYMENT_TYPE_ONEPAY_ATM])) {
                $protocol = Yii::$app->request->isSecureConnection ? 'https://' : 'http://';
                $myCourseLink =  $client_domain . Yii::$app->urlManager->createUrl(['trang-ca-nhan/khoa-hoc']);
                $res['msg'] = "Bạn đã đăng ký khóa học này! <br> Trong trường hợp bạn đã thanh toán online thành công hoặc đã được giao mã kích hoạt, vui lòng đăng nhập Kyna.vn để bắt đầu học <a href='{$myCourseLink}'>{$myCourseLink}</a> <br> Trong trường hợp bạn đã đăng ký nhưng gặp thất bại khi thanh toán, bạn có thể kiểm tra email để nhận được hướng dẫn thanh toán lại. Hoặc vui lòng liên hệ số hotline hoặc email bên dưới cũng như khung chat ở góc phải để nhận được hỗ trợ.";
            } else {
                $res['msg'] = 'Bạn đã đăng ký khóa học này! Vui lòng chọn khóa học khác. <br> Hoặc có thể liên hệ với Kyna qua khung chat bên dưới hoặc thông tin hỗ trợ';
            }
        } else {

            if ($model) {
                // update old user telesale + note
                $model->setScenario(UserTelesale::SCENARIO_REREGISTER);
                if ($payment_type != '') {
                    $model->reregister_message = $model->getAttributeLabel('payment_type') . ' : ' . $payment_type;
                }
                if ($model->phone_number) {
                    $model->reregister_message .= (($payment_type != '') ? ', ' : '') . $model->getAttributeLabel('phone_number') . ' cũ : ' . $model->phone_number;
                }
                $model->reregister_message .= ($model->reregister_message ? ', ' : '') . 'Ngày đăng ký trước đó: ' . Yii::$app->formatter->asDatetime($model->created_time);
                $model->created_time = time();
            } else {
                $model = new UserTelesale();
                $model->setScenario(UserTelesale::SCENARIO_LANDING_PAGE);
                $model->tel_id = TimeSlot::getOperatorsInShift(RoleHelper::ROLE_TELESALE, $user_type);
                if ($payment_type != '') {
                    $model->payment_type = $model->getAttributeLabel('payment_type') . ' : ' . $payment_type;
                }
            }

            //list($filter_advice, $note) = $this->getFilterAdvice($advice_name, $note, $filter_advice, $khoa_hoc_quan_tam, $cong_viec, $han_che, $noi_lam_viec);

            $model->email = $email;
            $model->full_name = $fullname;
            $model->phone_number = $phonenumber;
            $model->type = $user_type;
            $model->amount = $sell_price;
            if (!empty($course_id)) {
                $model->course_id = $course_id;
            }

            if (!empty($course_ids)) {
                $model->list_course_ids = implode(",", $course_ids);
            }
            $model->bonus = $bonus;

            $model->affiliate_id = $affiliate_id;
            $model->street_address = $address;
            $model->form_name = $advice_name; // chưa làm cái này
            $model->location_id = $district;
            $model->note = $note;

            $url = $client_domain . Yii::$app->urlManager->createUrl('dang-ky-thanh-cong');
            $url_success_page = $url;
            $res['succespage'] = $url_success_page;
            if (!empty($model->list_course_ids)) {
                $res['product_ids'] =  $model->list_course_ids;
            }
            if(!empty($model->course_id)){
                $res['product_id'] = $model->course_id;
            }
            $res['price'] = $model->amount;

            //Save User Telesale ony for COD
            if($payment_type == Page::PAYMENT_TYPE_COD) {
                if ($model->save()) {
                    Yii::$app->session->setFlash('lead', $model->attributes);

                    $post['saved_id'] = $model->id;
                    Yii::$app->session['landing_page_register_id'] = $model->id;

                    $res['status'] = 1;
                    $res['errors'] = [];
                    if ($model->type == UserTelesale::TYPE_ADVICE) {
                        $res['msg'] = 'Bạn đã đăng ký nhận tư vấn thành công. <br> Kyna.vn sẽ sớm liên hệ với bạn để tư vấn thêm về chương trình học!';
                    } else {
                        $res['msg'] = 'Cảm ơn bạn đã đăng ký thành công khóa học tại Kyna For Kids!<br> Nhân viên CSKH Kyna For Kids sẽ sớm liên hệ với bạn trong vòng 24 giờ.';
                    }

                } else {
                    $res['status'] = 0;
                    $res['errors'] = $model->errors;
                    $res['msg'] = 'Quá trình gởi dữ liệu về hệ thống bị lỗi. Vui lòng thử lại sau. <br> Hoặc có thể liên hệ với Kyna qua khung chat bên dưới hoặc thông tin hỗ trợ';
                }
            }

            /* do payment if page can */
            if (in_array($payment_type, [Page::PAYMENT_TYPE_ONEPAY_CC, Page::PAYMENT_TYPE_ONEPAY_ATM])) {
                $order = $this->createOrder($model, $payment_type, $slug);

                // update status from payment online
                if (!$order) {
                    $res['status'] = 0;
                    $res['errors'] = "Tạo đơn hàng không thành công";
                    $res['msg'] = 'Quá trình gởi dữ liệu về hệ thống bị lỗi. Vui lòng thử lại sau. <br> Hoặc có thể liên hệ với Kyna qua khung chat bên dưới hoặc thông tin hỡ trợ';
                } else {
                    // update note when create order
                   /* $model->scenario = UserTelesale::SCENARIO_CREATE_ORDER;
                    $model->note = (($model->reregister_message) ? $model->reregister_message : $model->payment_type) . ", Đơn hàng: #{$order->id}";
                    $model->user_id = $order->user->id;
                    $model->save(false);*/

                    // process payment
                    $response = self::doPayment($order,  $client_domain, $page_slug);
                    if (!$response['status'] || empty($response['redirectUrl'])) {
                        Order::makeFailedPayment($order->id);
                        $res['status'] = 0;
                        $res['msg'] = $response['msg'];
                    } else {
                        $res['status'] = 1;
                        $res['msg'] = '';
                        $res['redirectUrl'] = $response['redirectUrl'];
                    }
                }
            }

           /* if($payment_type == Page::PAYMENT_TYPE_COD)
            {
                $order = $this->createOrder($model, $payment_type, $slug);
                if (!$order) {
                    $res['status'] = 0;
                    $res['errors'] = "Tạo đơn hàng không thành công";
                    $res['msg'] = 'Quá trình gởi dữ liệu về hệ thống bị lỗi. Vui lòng thử lại sau. <br> Hoặc có thể liên hệ với Kyna qua khung chat bên dưới hoặc thông tin hỡ trợ';
                }
                else{
                    $res['status'] = 1;
                    $res['msg'] = 'Tạo đơn hàng COD thành công';
                }
            }*/
        }

        return $res;
    }
    public function actionGetResponse($method)
    {
        $paymentMethod = PaymentMethod::loadModel($method);
        $get = Yii::$app->request->get();

        $result = $paymentMethod->ipn($get);

        if (empty($result['error'])) {
            $order = Order::findOne($result['orderId']);
                if ($order) {

                   return $result;
            } else {
                throw new NotFoundHttpException('Order or Page not found');
            }
        } else {
            if (!empty($get['vpc_OrderInfo'])) {
                Order::makeFailedPayment($get['vpc_OrderInfo']);
                $order = Order::findOne($get['vpc_OrderInfo']);
                if ($order ) {

                } else {
                    throw new NotFoundHttpException('Order or Page not found');
                }
            } else {
                throw new NotFoundHttpException('Payment failure');
            }
        }
        return $result;
    }

    private function getParamsValue($post)
    {
        $user_type = isset($post['user_type']) ? $post['user_type'] : UserTelesale::TYPE_ADVICE;
        $plan_id = isset($post['plan_id']) ? (int)$post['plan_id'] : 0;
        $course_id = isset($post['course_id']) ? (int)$post['course_id'] : 0;
        $combo_id = isset($post['combo_id']) ? (int)$post['combo_id'] : 0;
        $list_combo_ids = isset($post['list_combo_ids']) ? $post['list_combo_ids'] : 0;
        $cod_money = 0;
        $bonus = isset($post['bonus']) ? (int)$post['bonus'] : 2;
        $combo_tuong_tac = isset($post['combo_tuong_tac']) ? $post['combo_tuong_tac'] : '';
        $combo_khuyen_mai = isset($post['combo_khuyen_mai']) ? $post['combo_khuyen_mai'] : '';
        $advice_name = isset($post['advice_name']) ? $post['advice_name'] : '';
        $filter_advice = isset($post['filter_advice']) ? $post['filter_advice'] : '';
        $khoa_hoc_quan_tam = isset($post['khoa_hoc_quan_tam']) ? $post['khoa_hoc_quan_tam'] : '';
        $alias_name = isset($post['alias_name']) ? $post['alias_name'] : '';
        $email = isset($post['email']) ? $post['email'] : '';
        $phonenumber = isset($post['phonenumber']) ? $post['phonenumber'] : '';
        $fullname = isset($post['fullname']) ? $post['fullname'] : '';
        $address = isset($post['address']) ? $post['address'] : '';
        $city = isset($post['city']) ? (int)$post['city'] : 0;
        $district = isset($post['district']) ? (int)$post['district'] : 0;
        $note = '';
        $cong_viec = isset($post['cong_viec']) ? $post['cong_viec'] : '';
        $han_che = isset($post['han_che']) ? $post['han_che'] : '';
        $noi_lam_viec = isset($post['noi_lam_viec']) ? $post['noi_lam_viec'] : '';
        $full_url = isset($post['full_url']) ? $post['full_url'] : '';
        $page_slug = isset($post['page_slug']) ? $post['page_slug'] : '';
        $landing_type = isset($post['landing_type']) ? $post['landing_type'] : '';
        $list_course_ids = isset($post['list_course_ids']) ? $post['list_course_ids'] : [];
        $slug = isset($post['slug']) ? $post['slug'] : '';
        $payment_type = isset($post['payment_type']) ? $post['payment_type'] : '';
        $affiliate_id = isset($post['affiliate_id']) ? $post['affiliate_id'] : '';
        $price =  isset($post['price']) ? $post['price'] : 0;
        $is_non_check_email =  isset($post['is_non_check_email']) ? $post['is_non_check_email'] : 0;
        return [
            $user_type,
            $plan_id,
            $course_id,
            $combo_id,
            $list_combo_ids,
            $cod_money,
            $bonus,
            $combo_tuong_tac,
            $combo_khuyen_mai,
            $advice_name,
            $filter_advice,
            $khoa_hoc_quan_tam,
            $alias_name,
            $email,
            $phonenumber,
            $fullname,
            $address,
            $city,
            $district,
            $note,
            $cong_viec,
            $han_che,
            $noi_lam_viec,
            $full_url,
            $page_slug,
            $landing_type,
            $list_course_ids,
            $slug,
            $payment_type,
            $affiliate_id,
            $price,
            $is_non_check_email
        ];
    }

    private function checkPhoneEmail($phonenumber, $email)
    {
        $res = [];
        $phonenumber = preg_replace('/[^0-9]/', '', $phonenumber);
        if (strlen($phonenumber) > 12 || strlen($phonenumber) < 10) {
            $res['status'] = 0;
            $res['msg'] = 'Bạn vui lòng điền đúng số điện thoại';
        }

        if ($email == '' || !filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $res['status'] = 0;
            $res['msg'] = 'Bạn vui lòng điền đúng email';
        }
        return $res;
    }

    public function createOrder($model, $paymentType, $slug)
    {
        $orderDetails = [];
        $courseIDs = !empty($model->course_id) ? [$model->course_id] : array() ;
        if(!empty($model->list_course_ids)) {
            $list_course_ids = explode(',', $model->list_course_ids);
            $courseIDs = array_merge($courseIDs, $list_course_ids );
        }

        foreach ($courseIDs as $courseID) {
            $course = CourseDetail::findOne($courseID);
            if (!is_null($course)) {
                if ($course->type == Product::TYPE_COMBO) {
                    foreach ($course->comboItems as $comboItem) {
                        $pItem = Product::findOne($comboItem->course_id);
                        $discount = $pItem->price - $comboItem->price;
                        $pItem->setDiscountAmount($discount);
                        $pItem->setCombo($course);
                        $orderDetails[] = $pItem;
                    }
                } else {
                    $pItem = Product::findOne($course->id);
                    $orderDetails[] = $pItem;
                }
            }
        }
        if (!empty($orderDetails)) {
            $order = Order::createEmpty(Order::SCENARIO_LANDING_PAGE);
            $order->is_4kid = 1;

            $order->status = Order::ORDER_STATUS_PENDING_CONTACT;

            $order->point_of_sale = Order::POINT_OF_SALE_LANDING_PAGE;

            $order->payment_method = $paymentType;
            $order->page_id =  0;

            $order->affiliate_id = $model->affiliate_id;
            $order->operator_id = $model->tel_id;
            $order->is_done_telesale_process = Order::BOOL_YES;

            $user = $this->findUser($model);

            $order->user_id = $user !== false ? $user->id : null;

            $order->addDetails($orderDetails);

            if ($order->save()) {
                return $order;
            }
        }
        return false;
    }
    private function findUser($userTelesale)
    {
        $user = User::find()->where(['email' => $userTelesale->email])->one();

        if (!$user) {
            $user = new User([
                'scenario' => 'create',
            ]);
            $user->load(['User' => [
                'email' => $userTelesale->email,
                'username' => $userTelesale->email,
            ]]);
            // update meta fields
            $user = $this->updateMeta($user, $userTelesale);
            if ($user->create(false)) {
                // trigger job send welcome email
                //$user->sendWelcomeEmailConsole();
                // register profile
                $this->afterCreateUser($user, $userTelesale);
            }
        }

        return $user;
    }
    private function updateMeta($user, $userTelesale)
    {
        if ($userTelesale) {
            $user->phone = $userTelesale->phone_number;
            $user->address = $userTelesale->street_address;
            $user->location_id = $userTelesale->location_id;
        }
        return $user;
    }
    private function afterCreateUser($user, $userTelesale)
    {
        if ($userTelesale) {
            $profile = $user->profile;
            $profile->public_email = $userTelesale->email;
            $profile->name = $userTelesale->full_name;
            $profile->phone_number = $userTelesale->phone_number;
            $profile->location = $userTelesale->location_id;
            $profile->save(false);
        }
    }


    public static function doPayment(Order $order, $client_domain, $page_slug)
    {
        $result = [
            'status' => false,
            'msg' => "",
            'redirectUrl' => ''
        ];
        $paymentMethodModel = PaymentMethod::loadModel($order->payment_method);
        if (!empty($paymentMethodModel) && $paymentMethodModel->isPayable() && $paymentMethodModel->payment_type === PaymentMethod::PAYMENT_TYPE_REDIRECT) {
            $paymentMethodModel->returnUrl = Url::to($client_domain."/page/api/response?method=$order->payment_method&slug=$page_slug", true);
            try {
                $response = $paymentMethodModel->pay($order);
                $result['status'] = true;
                $result['redirectUrl'] = $response;
            } catch (Exception $e) {
                $result['msg'] = 'Quá trình gởi dữ liệu về hệ thống bị lỗi. Vui lòng thử lại sau. <br> Hoặc có thể liên hệ với Kyna qua khung chat bên dưới hoặc thông tin hỡ trợ';
            }
        } else {
            $result['msg'] = "Phương thức thanh toán {$order->payment_method} không khả dụng";
        }
        return $result;
    }

    public function completeOrder($transactionEvent)
    {
        $transData = $transactionEvent->transData;

        Order::complete($transData['orderId'], $transData['transactionCode']);

        \Yii::$app->session->setFlash('need-tracking', true);
    }

}