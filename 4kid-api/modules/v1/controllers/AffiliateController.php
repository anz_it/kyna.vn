<?php
/**
 * Created by PhpStorm.
 * User: Tan
 * Date: 11/6/2018
 * Time: 4:14 PM
 */

namespace kid\modules\v1\controllers;


use kid\components\RestController;
use kid\models\AffiliateForm;
use kid\models\KidIncomeForm;
use Yii;

class AffiliateController extends RestController
{
    public function verbs()
    {
        return [
            'check-affiliate' => ['POST'],
            'add-commission' => ['POST']
        ];
    }

    public function actionCheckAffiliate(){
        $affForm = new AffiliateForm();
        $affForm->load(Yii::$app->getRequest()->getBodyParams(), '');
        return $affForm->checkAffiliate();
    }

    public function actionAddCommission()
    {
        $commissionForm = new KidIncomeForm();
        $commissionForm->load(Yii::$app->getRequest()->getBodyParams(), '');

        return $commissionForm->addCommission();
    }
}