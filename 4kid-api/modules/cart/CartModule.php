<?php

namespace kid\modules\cart;

/**
 * cart module definition class
 */
class CartModule extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'kid\modules\cart\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
