<?php

namespace mobile\modules\cart\components;

use kyna\promotion\models\Promotion;
use Yii;

use yii\web\NotFoundHttpException;
use yz\shoppingcart\CartPositionInterface;
use yz\shoppingcart\CostCalculationEvent;
use yz\shoppingcart\ShoppingCart as ShoppingCartBase;
use yz\shoppingcart\CartActionEvent;

use kyna\order\models\Order;

use yii\web\BadRequestHttpException;
use mobile\helpers\SecurityHelper;
use mobile\models\User;
/* 
 * Shopping cart component class.
 */

class ShoppingCart extends ShoppingCartBase
{

    public $storeInCache = true;
    private $_email;
    private $_orderId;
    private $_discountAmount;
    private $_shippingAmount;
    private $_promotionCode;
    private $_userInfo = [];

    /**
     * @desc Override init function to load cart from Cache
     */
    public function init()
    {
        Yii::$app->session->open();

        if (Yii::$app->user->isGuest) {
            $this->cartId = Yii::$app->session->id;
        } else {
            $userId = Yii::$app->user->id;

            $this->cartId = "User_{$userId}";
        }
        if (Yii::$app->controller != null && Yii::$app->controller->id == 'mobile-checkout') {
            $this->cartId = Yii::$app->request->get('mobile-id');
        }

        $this->load();

    }

    /**
     * Override Session with Cache
     */
    public function load($oldCart = null)
    {
        if ($this->storeInCache) {
            // load cart stored in cache
            if ($this->cartId != Yii::$app->session->id) {
                $sessionCart = Yii::$app->cache->get(Yii::$app->session->id);
                if (!empty($sessionCart)) {
                    $this->setSerialized($sessionCart['positions']);
                    $this->_orderId = $sessionCart['order_id'];
                    if (!empty($sessionCart['email'])) {
                        $this->_email = $sessionCart['email'];
                    }
                    if (!empty($sessionCart['discountAmount'])) {
                        $this->_discountAmount = $sessionCart['discountAmount'];
                    }
                    if (!empty($sessionCart['shippingAmount'])) {
                        $this->_discountAmount = $sessionCart['shippingAmount'];
                    }
                    if (!empty($sessionCart['promotionCode'])) {
                        $this->_promotionCode = $sessionCart['promotionCode'];
                    }
                    if (!empty($sessionCart['userInfo'])) {
                        $this->_userInfo = $sessionCart['userInfo'];
                    }

                    $this->saveCart();
                    Yii::$app->cache->set(Yii::$app->session->id, FALSE);
                    return;
                }
            }

            $user = $this->getUserMobilePayment();
            if(!empty($user))
            {
                $data = Yii::$app->cache->get('order_user_id_'.$user->id);
            }

        } else {
            // get cart stored in session
            $data = Yii::$app->session->get($this->cartId);
        }

        // load cart
        if (!empty($data)) {
            $this->setSerialized($data['positions']);
            $this->_orderId = $data['order_id'];
            if (!empty($data['email'])) {
                $this->_email = $data['email'];
            }
            if (!empty($data['discountAmount'])) {
                $this->_discountAmount = $data['discountAmount'];
            }
            if (!empty($data['shippingAmount'])) {
                $this->_discountAmount = $data['shippingAmount'];
            }
            if (!empty($data['promotionCode'])) {
                $this->_promotionCode = $data['promotionCode'];
            }
            if (!empty($data['userInfo'])) {
                $this->_userInfo = $data['userInfo'];
            }
        }
    }

    /**
     * Override save cart from Session to Cache
     */
    public function saveCart()
    {
        $key = $this->cartId;
        $value = [
            'positions' => $this->getSerialized(),
            'order_id' => $this->_orderId,
            'email' => $this->_email,
            'promotionCode' => $this->_promotionCode,
            'discountAmount' => $this->_discountAmount,
            'shippingAmount' => $this->_shippingAmount,
            'userInfo' => $this->_userInfo,
        ];

        if ($this->storeInCache) {
            $user = $this->getUserMobilePayment();
            if(!empty($user))
            {
                Yii::$app->cache->set('order_user_id_'.$user->id, $value, Yii::$app->params['expiredCartTime']);
            }
           /* else
            Yii::$app->cache->set($key, $value, Yii::$app->params['expiredCartTime']);*/
        } else {
            Yii::$app->session->set($key, $value);
        }
    }

    /**
     * @desc Override put function to just keep maximum quantity is only 1
     * @param CartPositionInterface $position
     * @param int $quantity
     */
    public function put($position, $quantity = 1, $discount = 0, $promotionCode = null)
    {
        if (isset($this->_positions[$position->getId()])) {
            return $this->update($position, $quantity, $discount, $promotionCode);
        }

        $position->setQuantity($quantity);
        $position->setDiscountAmount($discount);
        $position->setPromotionCode($promotionCode);
        $this->_positions[$position->getId()] = $position;

        $this->trigger(self::EVENT_POSITION_PUT, new CartActionEvent([
            'action' => CartActionEvent::ACTION_POSITION_PUT,
            'position' => $this->_positions[$position->getId()],
        ]));
        $this->trigger(self::EVENT_CART_CHANGE, new CartActionEvent([
            'action' => CartActionEvent::ACTION_POSITION_PUT,
            'position' => $this->_positions[$position->getId()],
        ]));

        $this->saveCart();
    }

    /**
     * @param CartPositionInterface $position
     * @param int $quantity
     */
    public function update($position, $quantity = 1, $discount = 0, $promotionCode = null)
    {
        if ($quantity <= 0) {
            $this->remove($position);
            return;
        }

        if (!isset($this->_positions[$position->getId()])) {
            return $this->put($position, $quantity, $discount, $promotionCode);
        }

        $this->_positions[$position->getId()]->setQuantity($quantity);
        $this->_positions[$position->getId()]->setDiscountAmount($discount);
        $this->_positions[$position->getId()]->setPromotionCode($promotionCode);

        $this->trigger(self::EVENT_POSITION_UPDATE, new CartActionEvent([
            'action' => CartActionEvent::ACTION_UPDATE,
            'position' => $this->_positions[$position->getId()],
        ]));
        $this->trigger(self::EVENT_CART_CHANGE, new CartActionEvent([
            'action' => CartActionEvent::ACTION_UPDATE,
            'position' => $this->_positions[$position->getId()],
        ]));

        $this->saveCart();
    }

    public function removeAll()
    {
        $this->_positions = [];
        $this->trigger(self::EVENT_CART_CHANGE, new CartActionEvent([
            'action' => CartActionEvent::ACTION_REMOVE_ALL,
        ]));

        $this->saveCart();
    }

    public function setPositions($positions)
    {
        $this->_positions = array_filter($positions, function (CartPositionInterface $position) {
            return $position->quantity > 0;
        });
        $this->trigger(self::EVENT_CART_CHANGE, new CartActionEvent([
            'action' => CartActionEvent::ACTION_SET_POSITIONS,
        ]));

        $this->saveCart();
    }

    public function setOrderId($orderId)
    {
        $this->_orderId = $orderId;

        $this->saveCart();
    }

    public function getOrderId()
    {
        return $this->_orderId;
    }

    public function getOrder($id = null, $status = [
        Order::ORDER_STATUS_PAYMENT_FAILED,
        Order::ORDER_STATUS_REQUESTING_PAYMENT,
        Order::ORDER_STATUS_NEW,
        Order::ORDER_STATUS_PENDING_PAYMENT
    ])
    {
        $orderId = $this->_orderId;
        if ($id != null) {
            $orderId = $id;
        }
        $order = Order::find()->where(['id' => $orderId]);
        if (!empty($status)) {
            $order->andWhere(['status' => $status]);
        }
        $order = $order->one();
        if ($order == null) {
            throw new NotFoundHttpException();
        }

        return $order;
    }

    public function setPromotionCode($promotionCode)
    {
        $this->_promotionCode = $promotionCode;

        $this->saveCart();
    }

    public function getPromotionCode()
    {
        return $this->_promotionCode;
    }

    public function setShippingAmount($shippingAmount)
    {
        $this->_shippingAmount = $shippingAmount;

        $this->saveCart();
    }

    public function getShippingAmount()
    {
        return $this->_shippingAmount;
    }

    public function setEmail($email)
    {
        $this->_email = $email;

        $this->saveCart();
    }

    public function getEmail()
    {
        return $this->_email;
    }

    public function emptyCart($orderId = null)
    {
        if ($orderId == null || $this->_orderId == $orderId) {
            $this->_positions = [];
            $this->_orderId = null;
            $this->_email = null;
            $this->_discountAmount = null;
            $this->_promotionCode = null;

            $this->saveCart();
        }
    }

    public function setDiscountAmount($discountAmount)
    {
        $this->_discountAmount = $discountAmount;

        $this->saveCart();
    }

    public function getDiscountAmount()
    {
        return $this->_discountAmount;
    }

    public function getCost($withDiscount = false, $orderID = null)
    {
        $cost = 0;
        if ($orderID) {
            $order = $this->getOrder($orderID);
            $cost = $order->sub_total;
            if ($withDiscount) {
                $cost = max(0, $cost - $order->total_discount);
            }
        } else {
            foreach ($this->_positions as $position) {
                $cost += $position->getCost($withDiscount);
            }

            $costEvent = new CostCalculationEvent([
                'baseCost' => $cost,
                'discountValue' => $this->getDiscountAmount()
            ]);
            $this->trigger(self::EVENT_COST_CALCULATION, $costEvent);
            if ($withDiscount)
                $cost = max(0, $cost - $costEvent->discountValue);
        }
        return $cost;
    }

    /**
     * Override remove cart methods.
     *
     */
    public function remove($position)
    {
        $this->removeById($position->getId());
    }

    /**
     * Removes position from the cart by ID
     * @param string $id
     */
    public function removeById($id)
    {
        $this->trigger(self::EVENT_BEFORE_POSITION_REMOVE, new CartActionEvent([
            'action' => CartActionEvent::ACTION_BEFORE_REMOVE,
            'position' => $this->_positions[$id],
        ]));
        $this->trigger(self::EVENT_CART_CHANGE, new CartActionEvent([
            'action' => CartActionEvent::ACTION_BEFORE_REMOVE,
            'position' => $this->_positions[$id],
        ]));
        unset($this->_positions[$id]);

        $items = $this->_positions;

        if (count($items) == 0) {
            $this->_promotionCode = null;
            $this->_discountAmount = null;
        } else {
            // re-validate promotion code
            $this->updatePromotion();

            $arr_code = null;
            foreach ($items as $item) {
                if (!empty($item->promotionCode)) {
                    $arr_code[] = $item->promotionCode;
                }
            }
            if (empty($arr_code)) {
                $this->_promotionCode = null;
                $this->_discountAmount = null;
            }
        }

        $this->saveCart();
    }

    public function getUserInfo()
    {
        return $this->_userInfo;
    }

    public function setUserInfo($info)
    {
        $this->_userInfo = $info;
        $this->saveCart();
    }

    public function getIsFreeOrder()
    {
        $items = $this->_positions;

        if (count($items) > 0 && $this->getCost(true) === 0) {
            return true;
        }

        return false;
    }

    public function removePromotion($orderID)
    {
        if ($this->_orderId == $orderID) {
            // remove from cart
            foreach ($this->_positions as $position) {
                $this->update($position, 1, 0);
            }
            $this->_promotionCode = null;
            $this->_discountAmount = null;
            $this->saveCart();
        } else {
            // remove from order
            if ($order = Order::findOne($orderID)) {
                Order::removePromotion($order->id);
            }
        }
    }

    public function updatePromotion()
    {
        $cartCost = $this->getCost(false);
        $promotion = Promotion::find()->andWhere([
            'code' => $this->_promotionCode
        ])->one();
        if ($promotion && $cartCost >= $promotion->value) {
            // cart total must be larger than min promotion code amount
            if (!empty($promotion->min_amount) && $promotion->min_amount > $cartCost) {
                $this->removePromotion($this->_orderId);
            }
        }
    }
    public function getUserMobilePayment()
    {
        $token = Yii::$app->request->get('token');
        $user = null;
        if (!empty($token)) {
            $signature = Yii::$app->request->get('signature');

            if (!SecurityHelper::checkSignature($signature, $token)) {
                throw new BadRequestHttpException("Bad signature");
            }
            $user = \mobile\models\User::findIdentityByAccessToken($token);
            if(!empty($user)) {
                Yii::$app->user->login($user);
            }
        }
        return $user;
    }
}
