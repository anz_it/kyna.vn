<?php

namespace mana\components;

use Yii;
use yii\web\Cookie;
use kyna\commission\models\AffiliateUser;
use yii\web\Response;
use kyna\user\models\User;
use yii\web\View;

/*
 * This is override core Controller class for Frontend usage
 */

class Controller extends \yii\web\Controller
{

    public $layout = '@app/views/layouts/main';
    public $rootCats = [];
    public $bodyClass = '';

    public function init()
    {
        $ret = parent::init();

        return $ret;
    }

    public function beforeAction($action)
    {
        $ret = parent::beforeAction($action);
        
//        if (!Yii::$app->request->get('debug') && $debugModule = Yii::$app->getModule('debug')) {
//            $debugModule->instance->allowedIPs = [];
//        }

        $this->detectAffiliate();

        return $ret;
    }

    protected function detectAffiliate()
    {
        if (Yii::$app->session->hasFlash('affiliate_id')) {
            $affiliate_id = Yii::$app->session->getFlash('affiliate_id');

            $script = "
                (function ($, window, document, undefined) {
                    // trigger event when user mouse out from kyna
                    $(document).ready(function () {
                        var data = {
                            affiliate_id: " . $affiliate_id . "
                        };
                        
                        var csrfPram = $('meta[name=\'csrf-param\']').attr('content');
                        var csrfToken = $('meta[name=\'csrf-token\']').attr('content');
                        
                        data[csrfPram] = csrfToken;
                        $.ajax({
                            url: '/site/detect-affiliate',
                            type: 'POST',
                            data: data
                        });
                    });
                })(window.jQuery, window, document);
            ";

            $this->view->registerJs($script, View::POS_END, 'detect-affiliate-js');
        }
    }

    public function actionDetectAffiliate()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $affiliate_id = Yii::$app->request->post('affiliate_id', 0);

        $user = User::findOne($affiliate_id);
        if (is_null($user)) {
            return [
                'result' => false,
                'message' => 'Không tìm thấy user',
            ];
        }
        $affiliate_id = $user->id;

        $cookies = Yii::$app->request->cookies;
        $currentId = $cookies->getValue('affiliate_id');

        // check if not exist or update affiliate_id
        if (empty($currentId) || $currentId != $affiliate_id) {
            $affiliateUser = AffiliateUser::find()->where(['user_id' => $affiliate_id, 'status' => AffiliateUser::STATUS_ACTIVE])->one();
            if (empty($affiliateUser)) {
                return [
                    'result' => false,
                    'message' => 'Không tìm thấy affiliate',
                ];
            }

            $cookies = Yii::$app->response->cookies;

            // add affiliate cookie to the response
            $cookies->add(new Cookie([
                'name' => 'affiliate_id',
                'value' => $affiliate_id,
                'expire' => $affiliateUser->cookieExpireTimestamp
            ]));

            return [
                'result' => true,
                'message' => 'Đã cập nhật affiliate',
            ];
        }

        return [
            'result' => false,
            'message' => 'Affiliate không hợp lệ hoặc trùng thông tin',
        ];
    }

}
