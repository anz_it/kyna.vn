<div class="cat-box-1 box">                            
    <h4 class="title">
        {if empty($catModel)}
            Tìm theo danh mục
        {else}
            <span class="color-green">{$catModel.name}</span>
        {/if}
    </h4>
    <ul class="cat-menu cat-box-1">
        {if empty($catId)}
            <li>
                {use class="\yii\helpers\Url"}
                <a href="{Url::toRoute(['/course/default/index'])}" class="hover-color-green">
                    <span class="icon color-green-n"><i class="fa fa-archive"></i></span>Tất cả khóa học
                </a>
                <span class="float-right"><i class="fa fa-angle-right"></i></span>
            </li>
            <li>
                <a href="{Url::toRoute(['/course/combo/index'])}" class="hover-color-green">
                    <span class="icon color-green-n"><i class="fa fa-archive"></i></span>Khóa học Combo
                </a>
                <span class="float-right"><i class="fa fa-angle-right"></i></span>
            </li>
        {/if}
        {foreach from=$rootCats item=$rootCat}
            <li>
                <a href="{$rootCat->url}" class="hover-color-green">
                    <span class="icon color-green-n"><i class="fa fa-archive"></i></span>{$rootCat.name}
                </a>
                {if !empty($rootCat.children)}
                <span class="float-right"><i class="fa fa-angle-right"></i></span>
                {/if}
            </li>
        {/foreach}
    </ul>
</div><!--end cat-box-1-->