{ActiveForm assign='form' id='facet-filter-form' action="{$action}" method='get'}
    <div class="cat-box-2 box code-disable">
        <h4 class="title">Tìm theo đặc điểm khóa học</h4>
        <div class="form">
            <div class="checkbox">
                <input id="checkbox1" type="checkbox" name="type" value="1">
                <label for="checkbox1"><span><span></span></span>Khóa học combo</label>
                <span class="cat-count float-right">100</span>
            </div>
            <div class="checkbox">
                <input id="checkbox2" type="checkbox" name="checkbox" value="2">
                <label for="checkbox2"><span><span></span></span>Đang khuyến mãi</label>
                <span class="cat-count float-right">100</span>
            </div>
            <div class="checkbox">
                <input id="checkbox3" type="checkbox" name="checkbox" value="3">
                <label for="checkbox3"><span><span></span></span>Miễn phí</label>
                <span class="cat-count float-right">100</span>
            </div>                                
        </div>
    </div><!--end .cat-box-2-->

    {if !empty($refiners['total_time'])}
    <div class="cat-box-3 box">
        <h4 class="title">Tìm theo thời lượng</h4>
        <div class="form">
            {foreach from=$refiners['total_time'] item=$item}
                {if !empty($item.active)}
                    <div class="checkbox">
                        <input id="facet-course-time-range-{$item.id}" type="checkbox" name="total_time[{$item.id}]" value="{$item.id}" {if !empty($get['total_time']) && in_array($item.id, $get['total_time'])}checked="checked"{/if}>
                        <label for="facet-course-time-range-{$item.id}"><span><span></span></span>{$item.title}</label>
                        <span class="cat-count float-right">{$item.active}</span>
                    </div>
                {/if}
            {/foreach}
        </div>
    </div><!--end .cat-box-3-->
    {/if}

    {if !empty($refiners['level'])}
    <div class="cat-box-4 box">
        <h4 class="title">Tìm theo trình độ yêu cầu</h4>
        <div class="form">
            {foreach from=$refiners['level'] item=$item}
                {if !empty($item.active)}
                    <div class="checkbox">
                        <input id="facet-course-level-{$item.id}" type="checkbox" name="level[{$item.id}]" value="{$item.id}" {if !empty($get['level']) && in_array($item.id, $get['level'])}checked="checked"{/if}>
                        <label for="facet-course-level-{$item.id}"><span><span></span></span>{$listLevel[$item.id]}</label>
                        <span class="cat-count float-right">{$item.active}</span>
                    </div>
                {/if}
            {/foreach}
        </div>
    </div><!--end .cat-box-4-->
    {/if}

    <div class="cat-box-5 box code-disable">
        <h4 class="title">Chủ đề đang hot</h4>
        <ul class="main-tags">
            <li><a href="#">Facebook Marketing</a></li>
            <li><a href="#">Tiếng hoa</a></li>
            <li><a href="#">Tiếng nhật</a></li>
            <li><a href="#">Seo</a></li>
        </ul>
    </div><!--end .cat-box-5-->
    {if !empty(Yii::$app->request->get('q'))}
        {$q = Yii::$app->request->get('q')}
        <input type="hidden" name="q" value="{$q}"/>
    {/if}
{/ActiveForm}

{use class="yii\web\View"}
{$script = "
            $('#facet-filter-form input[type=\'checkbox\']').on('change', function() {
                $('#facet-filter-form').submit();
            });
"}
        
{$this->registerJs($script, View::POS_END, 'my-options')|void}