<?php

namespace app\components\widgets;

use app\models\Category;

/* 
 * This is Category widget to get categories and display on Frontend
 */
class CategoryWidget extends \yii\base\Widget
{
    
    public function run()
    {
        $get = \Yii::$app->request->get();
        $catId = 0;
        $catModel = null;
        $parentModel = null;
        
        if (!empty($get['catId'])) {
            $catId = $get['catId'];
            $catModel = Category::findOne($catId);
        }
        
        $rootCats = Category::getList($catId, ['id', 'name', 'slug']);
        
        if (empty($rootCats) && !empty($catModel)) {
            $rootCats = Category::getList($catModel->parent_id, ['id', 'name', 'slug']);
            $catModel = $catModel->parent;
        }
        
        return $this->render('category.tpl', [
            'rootCats' => $rootCats,
            'catId' => $catId,
            'catModel' => $catModel,
        ]);
    }
}
