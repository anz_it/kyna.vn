<?php

namespace app\components\widgets;

use Yii;
use yii\helpers\Url;
use kyna\course\models\Course;

/* 
 * This is KFacet widget to render list facets of course data
 */
class FacetWidget extends yii\base\Widget
{
    
    public function run()
    {
        $get = \Yii::$app->request->get();
        
        $refiners = Yii::$app->controller->refinerSet->getRefinerValues();
//        var_dump($refiners);die;
        $listLevel = Course::listLevel();
        
        return $this->render('facet.tpl', [
            'refiners' => $refiners,
            'listLevel' => $listLevel,
            'get' => $get,
            'action' => $this->getAction($get)
        ]);
    }
    
    private function getAction($get)
    {
        $params = ['/course/default/index'];
        if (isset($get['catId'])) {
            $params['catId'] = $get['catId'];
        }
        if (isset($get['slug'])) {
            $params['slug'] = $get['slug'];
        }
        if (isset($get['page'])) {
            $params['page'] = $get['page'];
        }
        
        return Url::toRoute($params);
    }
}
