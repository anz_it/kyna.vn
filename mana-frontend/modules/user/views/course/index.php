<?php
$this->title = "Mana.edu.vn - Khóa học của tôi";
?>
<div class="wrapper my-course">
    <div class="container">
        <div class="col-md-3">
            <div class="side-bar-menu">
                <ul>
                    <li><a href="/user/home?process=learning">Đang học</a></li>
                    <li><a href="/user/home?process=completed">Đã hoàn thành</a></li>
                    <li><a href="/user/home?process=not-learning">Chưa bắt đầu</a></li>
                </ul>
            </div>
        </div>
        <div class="col-md-9">
            <div class="categories-courses-main">
                <ul class="row breadcrumb">
                    <li><a href="#">Trang chủ</a></li>
                    <li class="active">Vào học</li>
                </ul>

                <div class="row courses-list">
                    <?php
                    foreach($dataProvider->models as $course) {
                        ?>
                        <div class="course-item">
                            <div class="col-md-3 text-right image-holder">
                                <img src="<?=$course->course->image_url?>" style="width: 100%;">
                            </div>
                            <div class="col-md-9">
                                <div class="quick-description">
                                    <h3><a href="<?=\yii\helpers\Url::toRoute(['/course/default/detail', 'slug' =>$course->course->slug])?>"><?=$course->course->name?></a></h3>
                                </div>
                                <div class="created-by">
                                    <div class="col-md-1 col-xs-2 avatar">
                                        <img src="#">
                                    </div>
                                    <div class="col-md-6 col-xs-10 name">
                                        <a href="#">org_name</a>
                                    </div>
                                    <div class="col-md-5 text-right">
                                        <a class="btn btn-see-all btn-study" data-hover="<?= !empty($course->started_date)?'Tiếp tục học':'Bắt đầu học'?>" href="<?=\yii\helpers\Url::toRoute(['/course/learning', 'id' => $course->course_id])?>"><span>Hoàn thành 80%</span></a>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>