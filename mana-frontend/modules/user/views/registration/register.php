<?php

use yii\helpers\Url;
use yii\web\View;
use yii\bootstrap\ActiveForm;

$helper = Yii::$app->facebook->getRedirectLoginHelper();

?>
<div class="modal-body">
    <button type="button" class="close close-popup" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <ul class="top">
        <?php if (!$model->fbId) : ?>
            <li>
                <a href="<?= $helper->getLoginUrl(Url::toRoute(['/user/auth/fb']), ['email']) ?>" class="button-facebook background-blue hover-bg-blue">
                    <i class="fa fa-facebook"></i> Đăng nhập bằng facebook
                </a>
            </li>
            <li>- Hoặc đăng ký tài khoản Kyna -</li>
        <?php else : ?>
            <li>Để hoàn tất đăng ký trên Kyna.vn,<br> bạn cần điền các thông tin sau</li>
        <?php endif; ?>
    </ul>
    <?php $form = ActiveForm::begin([
        'id' => 'facebook-register-form',
        'enableAjaxValidation' => true,
    ]) ?>
        <?= $form->errorSummary($model, ['class' => 'error-summary alert alert-warning']) ?>
    
        <?= $form->field($model, 'fbId')->hiddenInput()->label(false) ?>
        
        <?= $form->field($model, 'email', [
            'template' => '{beginWrapper}<span class="icon"><i class="fa fa-envelope"></i></span>{input}'.PHP_EOL.'{error}{endWrapper}',
            'inputOptions' => [
                'placeholder' => 'Email của bạn'
            ]
        ])->input('email')->label(false)->error(false); ?>

        <?= $form->field($model, 'password', [
            'template' => '{beginWrapper}<span class="icon"><i class="fa fa-lock"></i></span>{input}'.PHP_EOL.'{error}{endWrapper}',
            'inputOptions' => [
                'placeholder' => 'Mật khẩu'
            ]
        ])->passwordInput()->label(false)->error(false); ?>

        <?= $form->field($model, 'name', [
            'template' => '{beginWrapper}<span class="icon"><i class="fa fa-user"></i></span>{input}'.PHP_EOL.'{error}{endWrapper}',
            'inputOptions' => [
                'placeholder' => 'Họ tên'
            ]
        ])->textInput()->label(false)->error(false); ?>

        <?= $form->field($model, 'phonenumber', [
            'template' => '{beginWrapper}<span class="icon"><i class="fa fa-phone"></i></span>{input}'.PHP_EOL.'{error}{endWrapper}',
            'inputOptions' => [
                'placeholder' => 'Số điện thoại'
            ]
        ])->textInput()->label(false)->error(false); ?>

        <div class="button-submit">
            <button type="submit" class="btn btn-default background-green hover-bg-green">Đăng ký</button>
        </div><!--end .button-popup-->
    <?php ActiveForm::end() ?>
    <ul class="bottom">
        <li>Nếu đã có tài khoản</li>
        <li><a href="<?= Url::toRoute(['/user/registration/login']) ?>" data-target="#popup-login" data-toggle="modal">Đăng nhập</a></li>
    </ul>
</div>