<?php
namespace mana\modules\user\models;
use kyna\user\models\UserCourse;
use yii\data\ActiveDataProvider;

/**
 * Created by PhpStorm.
 * User: Hien Nguyen
 * Date: 7/25/2016
 * Time: 4:33 PM
 */
class UserCourseSearch extends \kyna\user\models\search\UserCourseSearch
{

    public function search($params)
    {
        $query = UserCourse::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'course_id' => $this->course_id,
            'activation_date' => $this->activation_date,
            'is_activated' => $this->is_activated,
            'is_started' => $this->is_started,
            'expiration_date' => $this->expiration_date,
            'is_graduated' => $this->is_graduated,
            'method' => $this->method,
            'status' => $this->status,
            'created_time' => $this->created_time,
            'updated_time' => $this->updated_time,
        ]);

        if (!empty($this->userInfo)) {
            $query->alias('t');
            $query->join('LEFT JOIN', '{{%user}}', 'user.id = t.user_id');
            $query->join('LEFT JOIN', '{{%profile}}', 'profile.user_id = t.user_id');
            $query->join('LEFT JOIN', '{{%user_meta}}', 'user_meta.user_id = t.user_id AND user_meta.key = \'phone\'');
            $query->andWhere('user.email LIKE :userInfo OR profile.name LIKE :userInfo OR user_meta.value LIKE :userInfo', [
                ':userInfo' => "%{$this->userInfo}%"
            ]);

        }

        $query->join('RIGHT JOIN', '{{%mana_courses}}', 'mana_courses.course_id = user_courses.course_id');

        return $dataProvider;
    }
}