<?php
/**
 * @author: Hong Ta
 * @desc: Order form
 */
namespace app\modules\user\models\forms;


use common\helpers\ArrayHelper;
use kyna\order\models\Order;
use kyna\user\models\UserCourse;
//use kyna\order\models\traits\OrderTrait;
use Yii;

class OrderForm extends \yii\base\Model{

//    use OrderTrait;

    protected $_order;

    const SCENARIO_ACTIVE_COD = 'active_cod';

    public $activation_code;

    public function scenarios()
    {
        return [
            self::SCENARIO_ACTIVE_COD => ['activation_code']
        ]; // TODO: Change the autogenerated stub
    }

    public function rules()
    {
        return [
            [['activation_code'], 'string', 'on' => [self::SCENARIO_ACTIVE_COD]],
            [['activation_code'], 'required', 'on' => [self::SCENARIO_ACTIVE_COD]],
            [['activation_code'], 'checkExist', 'on' => [self::SCENARIO_ACTIVE_COD]]
        ]; // TODO: Change the autogenerated stub
    }

    public function attributeLabels()
    {
        return [
            'activation_code' => 'Nhập mã kích hoạt'
        ]; // TODO: Change the autogenerated stub
    }

    public function checkExist(){
        $result = Order::getOrderByActivationCode($this->activation_code, Order::ORDER_STATUS_DELIVERING);

        if(empty($result)){
            $this->addError('activation_code', 'Mã kích hoạt không hợp lệ.');
        }
    }

    public function complete(){
//        var_dump($this->_order);die;
        if(empty($this->_order)){
            $this->_order = $this->getOrder();
        }
        $order = Order::complete($this->_order->id, $this->_order->payment_receipt);
        if ($order !== false) {
            // active courses
            $courseIds = ArrayHelper::map($order->details, 'id', 'course_id');
            $ret = UserCourse::active(Yii::$app->user->id, $courseIds);
        }
        return $ret;
    }

    public function getOrder(){
        if(empty($this->_order)){
            return $this->_order = Order::getOrderByActivationCode($this->activation_code, Order::ORDER_STATUS_DELIVERING);
        }
        return $this->_order;
    }

}