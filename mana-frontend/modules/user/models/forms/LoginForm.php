<?php

namespace app\modules\user\models\forms;

use Yii;

class LoginForm extends \dektrium\user\models\LoginForm
{
    
    /** @inheritdoc */
    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'login'      => 'Username hoặc Email',
            'password'   => 'Mật khẩu',
            'rememberMe' => 'Ghi nhớ đăng nhập',
        ]);
    }
    
}