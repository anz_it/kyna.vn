<?php

namespace app\modules\user\models\forms;

use kyna\user\models\User;
use dektrium\user\models\RegistrationForm as BaseRegistrationForm;

class RegistrationForm extends BaseRegistrationForm
{

    public $name;
    public $fbId;
    public $phonenumber;

    public function rules()
    {
        $user = $this->module->modelMap['User'];
        
        return array_merge(parent::rules(), [
            'nameRequired' => ['name', 'required'],
            'nameLength' => ['name', 'string', 'min' => 3, 'max' => 50],
            'fbId' => ['fbId', 'safe'],
            'emailUnique'   => [
                'email',
                'unique',
                'targetClass' => $user,
                'message' => 'Email đã được sử dụng'
            ],
            'phonenumberRequired' => ['phonenumber', 'required'],
        ]);
    }

    public function beforeValidate()
    {
        $this->username = $this->email;
        return parent::beforeValidate();
    }
    
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'email'    => 'Email',
            'username' => 'Username',
            'password' => 'Mật khẩu',
            'phonenumber' => 'Điện thoại'
        ];
    }

}
