<?php

namespace mana\modules\user\controllers;

use Yii;
use kyna\mana\models\Contact;
use yii\filters\AccessControl;

/**
 * Default controller for the `user` module
 */
class ContactController extends \mana\modules\user\components\Controller
{
    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index']
                    ],
                ],
            ],
        ]);
    }
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        $model = new Contact();
        $model->loadDefaultValues();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', 'Đăng ký thành công');
            return $this->redirect(['index']);
        } else {
            return $this->render('index', [
                'model' => $model,
            ]);
        }
    }
}
