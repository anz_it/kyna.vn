<?php

namespace mana\modules\user\controllers;

use Yii;

/**
 * Default controller for the `user` module
 */
class DefaultController extends \mana\modules\user\components\Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
}
