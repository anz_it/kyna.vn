<?php

namespace mana\modules\user\controllers;

use Yii;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\widgets\ActiveForm;

use kyna\user\models\UserCourse;
use dektrium\user\models\LoginForm;

/* 
 * Class SecurityController override dektrium Controller.
 */
class SecurityController extends \dektrium\user\controllers\SecurityController
{
    
    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'login' => ['post'],
                ],
            ],
        ]);
    }
    
    public function init()
    {
        $ret = parent::init();
        
        $this->on(self::EVENT_AFTER_LOGIN, [$this, 'afterLogin']);
        
        return $ret;
    }
    
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            $this->goHome();
        }
        
        /** @var LoginForm $model */
        $model = Yii::createObject(LoginForm::className());
        $event = $this->getFormEvent($model);

        $this->trigger(self::EVENT_BEFORE_LOGIN, $event);

        Yii::$app->response->format = Response::FORMAT_JSON;
        
        if ($model->load(Yii::$app->getRequest()->post()) && $model->login()) {
            $this->trigger(self::EVENT_AFTER_LOGIN, $event);
            
            $defaultUrl = Yii::$app->request->post('currentUrl'); 
            
            return [
                'result' => true,
                'redirectUrl' => Yii::$app->user->getReturnUrl($defaultUrl),
            ];
        } else {
            return [
                'result' => false,
                'errors' => $model->errors
            ];
        }
        
        Yii::$app->end();
    }
    
    public function afterLogin()
    {
//        $cart = Yii::$app->cart;
//        $positions = $cart->getPositions();
//
//        $removeCourses = [];
//        foreach ($positions as $position) {
//            $alreadyInCourse = UserCourse::find()->where([
//                'user_id' => Yii::$app->user->id,
//                'course_id' => $position->id
//            ])->exists();
//
//            if ($alreadyInCourse) {
//                // user has already registerd this course => remove cart item
//                $cart->remove($position);
//                $removeCourses[] = $position->name;
//            }
//        }
//
//        if (!empty($removeCourses)) {
//            $message = "Đã bỏ các khóa học sau ra khỏi giỏ hàng của bạn bởi vì bạn đã tham gia các khóa học này: <b>" . implode(',', $removeCourses)  . "</b>.";
//            Yii::$app->session->setFlash('warning', $message);
//            if (empty($cart->getPositions())) {
//                // cart is empty => redirect to cart index with message
//                Yii::$app->user->setReturnUrl(['/cart/default/index']);
//            }
//        }
    }
}

