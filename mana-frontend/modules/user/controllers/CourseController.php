<?php

namespace mana\modules\user\controllers;

use mana\modules\user\models\UserCourseSearch;
use Yii;

/**
 * CourseController
 */
class CourseController extends \mana\modules\user\components\Controller
{
 
    public function actionIndex()
    {
        $searchModel = new UserCourseSearch();
        $searchModel->user_id = Yii::$app->user->id;
        $searchModel->is_activated = UserCourseSearch::BOOL_YES;
        $dataProvider = $searchModel->search(Yii::$app->request->get());
        
        if (Yii::$app->request->isAjax) {
            return $this->renderPartial('index', [
                'dataProvider' => $dataProvider
            ]);
        }
        
        return $this->render('index', [
            'dataProvider' => $dataProvider
        ]);
    }
}