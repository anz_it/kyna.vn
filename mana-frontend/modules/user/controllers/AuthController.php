<?php

namespace mana\modules\user\controllers;

use dektrium\user\models\Profile;
use kyna\user\models\User;
use kyna\user\models\UserMeta;
use yii\helpers\Url;

/**
 * CourseController.
 */
class AuthController extends \mana\components\Controller
{

    public function beforeAction($action)
    {
        \Yii::$app->session->open();
        return parent::beforeAction($action);
    }

    public function actionFb()
    {
        $fbUser = \Yii::$app->session->get('fbUser');
        if (!$fbUser) {
            $helper = \Yii::$app->facebook->getRedirectLoginHelper();
            try {
                // Returns a `Facebook\FacebookResponse` object
                $accessToken = $helper->getAccessToken();
                $response = \Yii::$app->facebook->get('/me?fields=id,name,email', $accessToken);
            } catch (\Facebook\Exceptions\FacebookSDKException $e) {
                echo 'Facebook SDK returned an error: ' . $e->getMessage();
                exit;
            }

            $fbUser = $response->getGraphUser();
            \Yii::$app->session->set('fbUser', $fbUser);
        }

        if ($user = $this->_matchingFb($fbUser['id'])) {
            \Yii::$app->user->login($user);
            $this->goHome();
        } elseif ($user = $this->_matchingEmail($fbUser['email'])) {
            $user->loadMeta([
                'fb_id' => $fbUser['id'],
            ]);
            \Yii::$app->user->login($user);
            $this->goHome();
        }

        // $user = new User();
        // $user->setScenario('create');
        // $user->username = $fbUser['email'];
        // $user->email = $fbUser['email'];
        //
        // if ($user->create()) {
        //     $user->loadMeta([
        //         'fb_id' => $fbUser['id'],
        //     ]);
        //     $user->save();
        //
        //     $profile = $user->profile;
        //     $profile->user_id = $user->id;
        //     $profile->name = $fbUser['name'];
        //     $profile->save();
        //
        //     \Yii::$app->user->login($user);
        //     $this->redirect(Url::home().'#register');
        // }
        $this->goHome();
    }

    private function _matchingFb($fbId)
    {
        return User::find()->leftJoin(UserMeta::tableName() . ' m', User::tableName() . '.id = m.user_id')
                        ->where([
                            'm.key' => 'fb_id',
                            'm.value' => $fbId,
                        ])->one();
    }

    private function _matchingEmail($email)
    {
        return User::find()->where(['email' => $email])->one();
    }

}
