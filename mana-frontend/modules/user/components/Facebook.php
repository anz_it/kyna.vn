<?php

namespace mana\modules\user\components;

class Facebook extends \Facebook\Facebook {
    public $app_id = '1077261602350453';
    public $app_secret = '91a1ad3ccc08eaa5abc36e9e2d50b9cb';
    public $default_graph_version = \Facebook\Facebook::DEFAULT_GRAPH_VERSION;
    public $persistent_data_handler = false;

    public function __construct()
    {
        parent::__construct([
            'app_id' => $this->app_id,
            'app_secret' => $this->app_secret,
            'default_graph_version' => $this->default_graph_version,
            'persistent_data_handler' => $this->persistent_data_handler,
        ]);
    }
}
