<?php

namespace mana\modules\user\components;

use Yii;
use yii\filters\AccessControl;

/**
 * Base Controller for module User
 */
class Controller extends \mana\components\Controller
{

//    public $layout = '@app/modules/user/views/layouts/user';
    
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function getViewPath()
    {
        return Yii::getAlias('@mana/modules/user/views/' . $this->id);
    }
    
}
