<?php
use kyna\course\models\Quiz;
use yii\bootstrap\Html;
use yii\helpers\Url;
use kyna\course\models\QuizSession;

?>
<!--<div class="tab-pane clearfix active lesson-content-quiz">-->
<!--    <div class="lesson-wrap-quiz" id="quiz-content">-->
        <div class="quiz-start">
            <h2><?= $quiz->name ?></h2>
            <?= $quiz->description ?>
            <ul>
                <?php if ($quiz->type == Quiz::TYPE_ONE_CHOICE) { ?>
                        <li class="col-md-4 col-xs-12"><img src="/img/lesson/icon-start.png" alt="" class="img-responsive media-object"/></li>
                        <li class="col-md-8 col-xs-12">
                            <p>Số phần trăm yêu cầu: <?= $quiz->percent_can_pass; ?> %</p>
                            <p>Tổng số câu hỏi: <b><?= count($quiz->detailQuestionIds); ?></b></p>
                            <p>Bạn đạt: <b> <?= $session->scores; ?> %</b></p>
                            <p>Bạn đạt: <b> <?= $session->rightAnswers; ?> / <?= count($quiz->detailQuestionIds); ?></b></p>
                            <p>Kết quả:
                                <b>
                                    <?php if($session->scores >= $quiz->percent_can_pass) { ?>
                                        Đạt yêu cầu.
                                    <?php }else{ ?>
                                        Không đạt yêu cầu.
                                    <?php } ?>
                                </b>
                            </p>
                        </li>
                    <?php if ($session->isPassed) { ?>

                        <li class="col-md-2 col-xs-12">
                            <?php if ($quiz->display_type !== 'single') { ?>
                                <button type="submit" class="btn btn-default">Nộp bài</button>
                            <?php } ?>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                                    aria-hidden="true">Làm lại</span></button>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                                    aria-hidden="true">Tiếp tục khóa học</span></button>
                        </li>
                    <?php } ?>
                <?php } else { ?>
                    <h3>Bạn đã hoàn thành bài tập và đã nộp bài, vui lòng chờ giảng viên chấm điểm</h3>
                <?php } ?>
            </ul>
            <div class="redo-container">
                <?= Html::a('Làm lại', Url::toRoute(['/course/quiz/', 'id' => $quiz->id, 'status' => QuizSession::STATUS_REDO]), [
                    'class' => 'next-quiz btn-redo',
                    'data-ajax' => true,
                    'data-target' => '#quiz-content',
                    'data-push-state' => 'false',
                ]) ?>
            </div>
        </div>
<!--    </div>-->
<!--</div>-->
