<?php

use yii\widgets\ListView;
use yii\bootstrap\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;

$display_class = $quiz->display_type === 'single' ? 'quiz-slide' : 'quiz-slide';

?>
<?php $form = ActiveForm::begin([
    'options' => [
        'data-ajax' => true,
        'data-target' => '#quiz-content',
        'data-push-state' => 'false',
    ]
]) ?>
    <div class="quiz-header">
        <div class="time-remaining" time-val="<?= $session->time_remaining; ?>">
            <?= gmdate('H:i:s', $session->time_remaining); ?>
        </div>
    </div>
    <div class="<?= $display_class ?>">
        <?= ListView::widget([
            'dataProvider' => $dataProvider,
            'layout' => "{items}\n<div class='pagination'>{pager}</div>",
            'itemView' => function ($model, $key, $index) use ($form, $quiz, $lastAnswer , $next) {
                return $this->render('_single-question', [
                    'model' => $model,
                    'index' => $index,
                    'form' => $form,
                    'quiz' => $quiz,
                    'next'  => $next,
                    'lastAnswer' => $lastAnswer,
                ]);
            },
            'options' => [
                'tag' => 'ul',
                'class' => 'lesson-content-quiz media-list',
            ],
            'itemOptions' => [
                'tag' => false,
            ]
        ]) ?>

    </div>
    <div class="quiz-footer">
        <?php if($quiz->display_type === 'single' && $quiz->show_answer == 1 && $lastAnswer) { ?>

            <?= $this->render('_single-answer',[
                'lastAnswer' => $lastAnswer,
                'next'  => !empty($next) ? $next : false,
                'quiz'  => $quiz,
                'session' => $session
            ]); ?>
        <?php }else{ ?>
            <ul class="list-buttons">
                <?php if ($quiz->display_type === 'single') { ?>
                    <?php if ($prev) { ?>
                        <li class="pre">
                            <?= Html::a("<i class='fa fa-chevron-left'>Câu trước</i>", Url::toRoute(['/course/quiz/', 'id' => $quiz->id, 'position' => $prev]), [
                                'class' => 'prev-quiz btn',
                                'data-ajax' => true,
                                'data-target' => '#quiz-content',
                                'data-push-state' => 'false',
                            ]) ?>
                        </li>
                    <?php } ?>
                    <?php if ($next) { ?>
                        <li class="next">
                            <?= Html::a('<i class="fa fa-chevron-right">Câu tiếp</i>', Url::toRoute(['/course/quiz/', 'id' => $quiz->id, 'position' => $next]), [
                                'class' => 'next-quiz btn',
                                'data-ajax' => true,
                                'data-target' => '#quiz-content',
                                'data-push-state' => 'false',
                            ]) ?>
                        </li>
                    <?php } ?>
                <?php } ?>
                <li class="btn-quiz"><button type="submit" class="btn button">Gửi trả lời</button></li>
            </ul>
        <?php } ?>


    </div>
<?php ActiveForm::end() ?>
