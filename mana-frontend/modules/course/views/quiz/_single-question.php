<?php
use app\components\widgets\ListView;
use kyna\course\models\QuizQuestion;
use yii\bootstrap\Html;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;

$question = $model->question;
$answers = $model->answers;
$answerSet = ArrayHelper::map($answers, 'id', 'content');
$session = $model->session;

switch ($question->type) {
    case QuizQuestion::TYPE_WRITING:
        $instruction = '(Nhập câu trả lời vào ô bên dưới)';
        $field = $form->field($session, 'answersText['.$model->id.']')->textArea()->label(false);
        break;
    case QuizQuestion::TYPE_FILL_INTO_DOTS:
        $instruction = '(Điền vào chỗ trống)';
        $field = null;
        for($i = 0; $i < $model->question['number_of_dots']; $i++){
            $j = $i + 1;
            $field[$i] = $form->field($session, 'answersText['.$model->id.'][' . $j . ']')->textInput(['class' => 'col-sm3'])->label('(' . $j . ')');
        }
        break;
    default:
        $instruction = '(Chọn một câu trả lời bên dưới)';
        $field = $form->field($session, 'answers['.$model->id.']', ['template' => '{input}'])->radioList($answerSet, [
            'item' => function ($index, $label, $name, $checked, $value) use ($answers) {
//                $html = null;
                $imageUrl = (isset($answers[$value]) and $answers[$value]->image_url) ? $answers[$value]->image_url : false;
                if($checked) {
                    $selected = 'selected';

                    if($answers[$value]->is_correct == \kyna\course\models\QuizAnswer::BOOL_YES){
                        $correct = 'fa fa-check-circle-o';
                        $color = 'green';
                    }else{
                        $correct = 'fa fa-times-circle-o';
                        $color = 'red';
                    }
                    $html = '<label class="col-md-6 col-xs-12 '. $color . '">'.
                        '<div class="squared"><label class="quiz-squared-label ' . $selected . '"><i class="fa fa-check"></i>'
                        .Html::radio($name, $checked, ['value' => $value]).'</label></div><div class="media-left">'.
                        (($imageUrl) ? '<img class="thumbnail" src="'.$imageUrl.'" /><i class="' .$correct . '"></i>' : '<i class="' .$correct . ' no-image"></i>').
                        '</div><div class="media-body ' . $color . '">'.$label.'</div>'.
                        '</label>';

                }else{

                    $html = '<label class="col-md-6 col-xs-12">'.
                        '<div class="squared"><label class="quiz-squared-label"><i class="fa fa-check"></i>'
                        .Html::radio($name, $checked, ['value' => $value]).'</label></div>'.
                        (($imageUrl) ? '<div class="media-left"><img class="thumbnail" src="'.$imageUrl.'" /></div>' : '').
                        '<div class="media-body">'.$label.'</div>'.
                        '</label>';
                }
                return $html;
            }
        ]);
}

?>
<li class="list col-sm-12">
    <div class="title">
        <ul class="lesson-nav-header">
            <li><h4>Câu hỏi <?= $model->position ?></h4></li>
            <li>
                <span class="active"><?= $model->position ?></span>/<?= count($quiz->details) ?> câu
<!--                <span class="time">20:49</span>-->
            </li>
        </ul>
        <?= $question->content ?>
        <?php if ($question->image_url) { ?>
            <img src="<?= $question->image_url ?>" alt="<?= $question->content ?>" class="img-responsive col-md-4"/>
        <?php } ?>
        <p>
            <span class="bold italic"><?= $instruction ?></span>
        </p>
    </div>
    <?php if(count($field) > 1) { ?>
        <?php foreach ($field as $item) { ?>
            <?= $item; ?>
        <?php } ?>
    <?php }else{ ?>
        <?= $field; ?>
    <?php } ?>

</li>
