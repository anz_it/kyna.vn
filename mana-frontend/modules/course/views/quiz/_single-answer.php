<?php
use yii\helpers\Html;
use yii\helpers\Url;
?>

<div class="lesson-quiz-result col-xs-12 col-md-12">
    <div class="col-xs-9 col-md-9">
        <p><strong>Đáp án:</strong> <?= $lastAnswer->correctAnswer->content; ?></p>
        <?php if($lastAnswer->correctAnswer->explanation) { ?>
        <div><strong>Giải thích:</strong> <?= $lastAnswer->correctAnswer->explanation; ?></div>
        <?php } ?>

    </div>
    <div class="col-xs-3 col-md-3">
        <?= Html::a('Tiếp tục', Url::toRoute(['/course/quiz/', 'id' => $quiz->id, 'position' => $next]), [
            'class' => 'continue-quiz btn btn-default',
            'data-ajax' => true,
            'data-target' => '#quiz-content',
            'data-push-state' => 'false',
        ]) ?>
    </div>
</div>
