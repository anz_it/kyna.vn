<?php
use yii\widgets\ListView;

?>
<?= ListView::widget([
    'dataProvider' => $noteDataProvider,
    'layout' => '{items}',
    'itemView' => '_note-item',
    'itemOptions' => [
        'tag' => 'li',
    ],
    'options' => [
        'tag' => 'ul',
    ]
]) ?>
