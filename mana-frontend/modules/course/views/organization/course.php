<?php
$this->title = "Mana.edu.vn - Danh sách khóa học";
$numberCourse = 0;
?>
<?php if ($dataProvider->totalCount) {
    foreach ($dataProvider->getModels() as $course) {
        if ($course->kynaCourse && $course->organization) {
            $numberCourse++;
        }
    }
}
?>

<?php if ($category) { ?>
    <header>
        <div class="header">
            <div class="container">
                <div class="row categories-courses-header">
                    <div class="col-md-2 text-right image-holder">
                        <img src="<?= $category->image_url ?>">
                    </div>
                    <div class="col-md-10">
                        <h3><?= $category->name ?></h3>
                        <div class="">
                            <?= $category->description ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
<?php } ?>
<div class="wrapper categories">
    <div class="container">
        <div class="col-md-3">
            <div class="side-bar-menu" style="margin: 30px 0;">
                <ul>
                    <li><a href="/chuyen-nganh">Chuyên ngành</a></li>
                    <li><a href="/he-dao-tao">Hệ đào tạo</a></li>
                    <li><a href="/chung-chi">Chứng chỉ</a></li>
                    <li class="active"><a href="javascript:void(0)">Tổ chức cấp chứng chỉ</a></li>
                </ul>
            </div>
        </div>
        <div class="col-md-9">
            <div class="categories-main">

                <div class="courses-list">
                    <div class="panel-group" id="accordion">

                        <?php
                        if ($listOrganizations) {
                            foreach ($listOrganizations as $item) {
                                if ($item->slug == $category->slug) {
                                    continue;
                                }
                                ?>
                                <div class="panel panel-default panel-categories categories-item">
                                    <div class="panel-heading">
                                        <div class="row">
                                            <div class="col-md-3 text-right image-holder">
                                                <img src="<?= $item->image_url ?>" width="150px">
                                            </div>
                                            <div class="col-md-9">
                                                <div class="row">
                                                    <div class="col-md-6 col-sm-6">
                                                        <h4 class="panel-title">
                                                            <a class="" data-toggle="collapse"
                                                               data-parent="#accordion" href="#collapseTwo">
                                                                <?= $item->name ?>
                                                            </a>
                                                        </h4>
                                                    </div>
                                                    <?php if (count($item->courses) > 0) { ?>
                                                        <div class="col-md-offset-3 col-md-3 text-right col-sm-6">
                                                            <a class="btn btn-see-all"
                                                               href="/to-chuc-cap-chung-chi/danh-sach-khoa-hoc/<?= $item->slug ?>"
                                                               data-hover="Xem tất cả"><span><?= count($item->courses) ?>
                                                                    khóa học</span></a>
                                                        </div>
                                                    <?php } ?>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12 intro"><?= $item->description ?></div>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>

                                        </div>
                                    </div>

                                </div>
                                <?php
                            }
                        }
                        ?>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?= \mana\widgets\RegisterWidget::widget(['forHome' => false]) ?>
<script type="application/javascript">
    var categorySlug = "chuyen-nganh";
    var filterUrl = "<?=$filterUrl?>";
</script>