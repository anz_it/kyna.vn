<?php

use mana\models\Setting;
$dir_name = dirname(dirname(dirname(__FILE__)));
?>
<!DOCTYPE html>
<html lang="vi">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
	<meta property="fb:app_id" content="790788601060712">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="Bao gồm những kiến thức, kỹ năng cốt lõi theo tiêu chuẩn quốc tế giúp bạn trở thành người quản lý tài năng trong môi trường làm việc ngày càng cạnh tranh. Bộ sách còn giúp người đọc hình thành tư duy quản lý kinh doanh hiệu quả trong thời đại mới. Bộ sách được biên soạn dựa trên học liệu gốc của Hiệp hội IBTA, phân phối độc quyền bởi MANA.">
    <meta name="author" content="">
    <!--<link rel="icon" href="">-->
    <title>Bộ 5 sách kỹ năng trong kinh doanh - Mana.edu.vn</title>

	<meta name="robots" content="index,follow">
    <meta property="og:type" content="website" />
    <meta property='og:url' content='https://mana.edu.vn/cbp/combo-5-sach-ky-nang-kinh-doanh' />
    <meta property='og:image:url' content='https://mana.edu.vn/mana/cbp/combo-5-sach-ky-nang-kinh-doanh/img/thumb.png' />
    <meta property='og:title' content='Bộ 5 sách kỹ năng trong kinh doanh - Mana.edu.vn' />
    <meta property='og:description' content='Bao gồm những kiến thức, kỹ năng cốt lõi theo tiêu chuẩn quốc tế giúp bạn trở thành người quản lý tài năng trong môi trường làm việc ngày càng cạnh tranh. Bộ sách còn giúp người đọc hình thành tư duy quản lý kinh doanh hiệu quả trong thời đại mới. Bộ sách được biên soạn dựa trên học liệu gốc của Hiệp hội IBTA, phân phối độc quyền bởi MANA.' />

    <!-- Bootstrap core CSS -->
    <link href="/mana/cbp/combo-5-sach-ky-nang-kinh-doanh/css/main.css" rel="stylesheet" />

    <script src="/mana/cbp/combo-5-sach-ky-nang-kinh-doanh/js/jquery.min.js"></script>
    <script src="/mana/cbp/combo-5-sach-ky-nang-kinh-doanh/js/jquery.marquee.min.js"></script>
    <script src="/mana/cbp/combo-5-sach-ky-nang-kinh-doanh/js/tether.min.js"></script>
      <?php echo \common\helpers\Html::csrfMetaTags() ?>

</head>
<body>
<?php $this->beginBody()?>


    <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-M92WKP');</script>

    <!-- End Google Tag Manager -->
	<?php
	// get các tham số cần thiết */

	$getParams = $_GET;
	$utm_source = '';
	$utm_medium = '';
	$utm_campaign = '';

	if (isset($getParams['utm_source']) && trim($getParams['utm_source']) != '') {
		$utm_source = trim($getParams['utm_source']);
	}
	if (isset($getParams['utm_medium']) && trim($getParams['utm_medium']) != '') {
		$utm_medium = trim($getParams['utm_medium']);
	}
	if (isset($getParams['utm_campaign']) && trim($getParams['utm_campaign']) != '') {
		$utm_campaign = trim($getParams['utm_campaign']);
	}

	?>
    <section id="menu">
        <div class="container">
            <a class="navbar-brand" href="/">
                <img src="/mana/cbp/combo-5-sach-ky-nang-kinh-doanh/img/mana_white.png" alt="" />
            </a>
            <div class="contact">
                <button class="btn btn-reg">ĐĂNG KÝ NGAY</button>
            </div>
            <nav class="navbar navbar-light bg-faded" role="navigation">
                <button class="navbar-toggler btn-toggler hidden-xs-up collapsed" type="button" data-toggle="collapse" data-target="#collapsing-navbar">
                    <img src="/mana/cbp/combo-5-sach-ky-nang-kinh-doanh/img/icon-col.png" alt="" />
                </button>
                <div class="collapse navbar-toggleable-sm" id="collapsing-navbar">
                    <ul class="nav navbar-nav">
                        <li class="nav-item"><a href="#content" class="nav-link">NỘI DUNG SÁCH</a></li>
                    </ul>
                </div>
            </nav>
        </div>
    </section>
    <section id="banner">
        <div class="container">
            <div class="inner-banner">
                <div class="title-1">Bộ 5 sách</div>
                <div class="title-2">KỸ NĂNG TRONG
                    <br>KINH DOANH</div>
                <div class="des-1">PHÂN PHỐI ĐỘC QUYỀN BỞI</div>
                <div class="des-2">MANA ONLINE BUSINESS SCHOOL</div>
                <div class="btn-group">
                    <button class="btn ticket btn-register">ĐĂNG KÝ NGAY</button>
                    <br>
                    <button class="btn btn-info">TÌM HIỂU THÊM</button>
                </div>
            </div>
        </div>
    </section>
    <section id="intro">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-xs-12">
                    <img src="/mana/cbp/combo-5-sach-ky-nang-kinh-doanh/img/img-intro-1.png" alt="">
                </div>
                <div class="col-lg-6 col-xs-12">
                    <div class="title">Lời mở đầu</div>
                    <div class="des">
                        <p>Trong một thế giới ngày càng phẳng và môi trường kinh doanh nhiều biến động như hiện nay, ngừng học hỏi và thay đổi sẽ khiến bạn đi sau với tốc độ phát triển của xã hội; thường xuyên cập nhật các kiến thức mới, kĩ năng mới chuẩn mực quốc tế sẽ giúp bạn tự gia tăng cơ hội, lợi thế cạnh tranh của bản thân.</p>
                        <p>Mana Online Business School hân hạnh giới thiệu đến bạn Bộ 5 sách “Kỹ năng trong kinh doanh” (Top 5 Business Skills) là bí kíp bỏ túi và chìa khóa vạn năng cho bạn trong môi trường kinh doanh hiện nay. </p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6 col-xs-12">
                    <ul>
                        <li>Bộ sách chứa đầy đủ những bài học giá trị, mang tính thực tế cao, theo chuẩn mực quốc tế. </li>
                        <li>Bộ sách được Mana Online Business School biên soạn dựa trên học liệu gốc của Hiệp hội Đào tạo Kinh doanh Quốc tế (International Business Training Association) tại Hoa Kỳ, được công nhận bởi các tập đoàn hàng đầu thế giới.</li>
                        <li>Bộ sách bao gồm phiên bản tiếng Việt và tiếng Anh.</li>
                    </ul>
                </div>
                <div class="col-lg-6 col-xs-12">
                    <img src="/mana/cbp/combo-5-sach-ky-nang-kinh-doanh/img/img-intro-2.png" alt="">
                </div>
            </div>
        </div>
    </section>
    <section id="skill">
        <div class="container">
            <div class="row">
                <div class="box-title">
                    <div class="title">5 Kỹ năng cốt lõi & quan trọng bậc nhất trong kinh doanh đều được thể hiện trong bộ sách này</div>
                </div>
                <div class="box-book-store">
                    <ul>
                        <li class="clicked"><span>Leadership</span> – Nghệ thuật lãnh đạo hiệu quả</li>
                        <li><span>Business Communication</span> – Giao tiếp trong kinh doanh</li>
                        <li><span>Selling Skills</span> – Kỹ năng bán hàng chuyên nghiệp</li>
                        <li><span>Customer Service</span> – Kỹ năng chăm sóc khách hàng</li>
                        <li><span>Business Etiquette</span> – Nghi thức giao tiếp trong kinh doanh</li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <section id="content">
        <div class="container">
            <div class="title">NỘI DUNG CHI TIẾT BỘ 5 SÁCH “KỸ NĂNG TRONG KINH DOANH</div>
            <div id="box-content" class="box-content">
                <div class="item clearfix">
                    <div class="image-book"><img src="/mana/cbp/combo-5-sach-ky-nang-kinh-doanh/img/img-book-1.png" alt=""></div>
                    <div class="detail">
                        <div class="name"><b>Leadership</b> - Nghệ thuật lãnh đạo</div>
                        <div class="line"></div>
                        <div class="des">
                            <ul>
                                <li>Những kĩ năng và tố chất cần có của một nhà lãnh đạo tài ba (Skills and Characteristics)</li>
                                <li>Phong cách lãnh đạo theo từng tình huống cụ thể trong quản lý kinh doanh và khi đối diện với sự thay đổi (Leadership styles)</li>
                                <li>Xây dựng tầm nhìn và chiến lược cho công ty (Vision and mission)</li>
                                <li>Hướng dẫn phương pháp tạo động lực, gắn kết tinh thần đội nhóm (Motivating, training and connecting employees).</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="item clearfix">
                    <div class="image-book"><img src="/mana/cbp/combo-5-sach-ky-nang-kinh-doanh/img/img-book-2.png" alt=""></div>
                    <div class="detail">
                        <div class="name"><b>Business Communication</b> - Giao tiếp trong kinh doanh</div>
                        <div class="line"></div>
                        <div class="des">
                            <ul>
                                <li>Những yếu tố ảnh hưởng đến quá trình giao tiếp, kết quả và mối quan hệ của bạn (business communication model)</li>
                                <li>Bí quyết giao tiếp chuyên nghiệp ngôn từ và phi ngôn từ (verbal and non-verbal communication)</li>
                                <li>Hình thành phong thái viết chuyên nghiệp trong kinh doanh, các mẫu email báo cáo theo chuẩn mực quốc tế (business writing)</li>
                                <li>Phát triển phong thái thuyết trình tự tin lôi cuốn (presentation skill)</li>
                                <li>Cách giải quyết những mâu thuẫn trong giao tiếp kinh doanh (Handling conflicts)</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="item clearfix">
                    <div class="image-book"><img src="/mana/cbp/combo-5-sach-ky-nang-kinh-doanh/img/img-book-3.png" alt=""></div>
                    <div class="detail">
                        <div class="name"><b>Selling Skills</b> - Kỹ năng bán hàng chuyên nghiệp</div>
                        <div class="line"></div>
                        <div class="des">
                            <ul>
                                <li>Quy trình và các bước bán hàng tiêu chuẩn (sales flow)</li>
                                <li>Phương pháp tạo sự hào hứng trong quá trình bán hàng (self-motivation)</li>
                                <li>Phương pháp và quy trình tìm kiếm khách hàng tiềm năng (prospecting stage)</li>
                                <li>Chiến lược tiếp cận khách hàng tiềm năng (approaching stage)</li>
                                <li>Phương pháp xử lý tình huống từ chối của khách hàng</li>
                                <li>Chiến lược chốt sales thành công</li>
                                <li>Xây dựng quy trình bán hàng và chăm sóc khách hàng sau khi sales</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="item clearfix">
                    <div class="image-book"><img src="/mana/cbp/combo-5-sach-ky-nang-kinh-doanh/img/img-book-4.png" alt=""></div>
                    <div class="detail">
                        <div class="name"><b>Customer Service</b> - Chăm sóc khách hàng hiệu quả</div>
                        <div class="line"></div>
                        <div class="des">
                            <ul>
                                <li>Giới thiệu về dịch vụ chăm sóc khách hàng và chuẩn mực CSKH</li>
                                <li>Kỹ năng giao tiếp với khách hàng và làm dịu khách hàng khó tính</li>
                                <li>Chăm sóc khách hàng qua điện thoại và qua Internet</li>
                                <li>Chiến lược quản lý thời gian và sự căng thẳng khi CSKH</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="item clearfix">
                    <div class="image-book"><img src="/mana/cbp/combo-5-sach-ky-nang-kinh-doanh/img/img-book-5.png" alt=""></div>
                    <div class="detail">
                        <div class="name"><b>Business Etiquette</b> - Nghi thức giao tiếp trong kinh doanh</div>
                        <div class="line"></div>
                        <div class="des">
                            <ul>
                                <li>Nghi thức và phong thái giao tiếp trong môi trường kinh doanh quốc tế</li>
                                <li>Nghi thức trong các buổi hội họp, gặp gỡ</li>
                                <li>Nghi thức trong kinh doanh khi chào hỏi, làm quen</li>
                                <li>Đạo đức kinh doanh là một trong những chuẩn mực nghi thức</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="register">
        <div class="container">
            <div class="box-register">
                <div class="header">Mua trọn bộ 5 sách - Nhận ngay ưu đãi giá sốc 24%</div>
                <div class="select-book clearfix">
                    <div>
                        <img src="/mana/cbp/combo-5-sach-ky-nang-kinh-doanh/img/img-register.png" alt="">
                    </div>
                    <div class="selected-book">
                        <ul>
                            <li bookname="Leadership - Nghệ thuật lãnh đạo" class="icon-circle-thin selected">Leadership – Nghệ thuật lãnh đạo</li>
                            <li bookname="Business Communication - Giao tiếp trong kinh doanh" class="icon-circle-thin">Business Communication – Giao tiếp trong kinh doanh</li>
                            <li bookname="Selling Skills - Kỹ năng bán hàng hiệu quả" class="icon-circle-thin">Selling Skills – Kỹ năng bán hàng chuyên nghiệp</li>
                            <li bookname="Customer Serivce - Kỹ năng chăm sóc khách hàng" class="icon-circle-thin">Customer Service – Kỹ năng chăm sóc khách hàng</li>
                            <li bookname="Business Etiquette - Nghi thức giao tiếp trong kinh doanh" class="icon-circle-thin">Business Etiquette – Nghi thức giao tiếp trong kinh doanh</li>
                            <li class="line">
                                <div></div>
                            </li>
                            <li bookname="Combo 3 quyển" class="icon-circle-thin"><span class="red">Combo 3 quyển sách</span></li>
                            <li bookname="Combo 5 quyển" class="icon-circle-thin"><span class="red">Combo 5 quyển sách</span></li>
                        </ul>
                    </div>
                </div>
                <form action="/course/page/submit" name="landing-page-id" class="form-horizontal" id="form_advice"  accept-charset="utf-8">
                    <input type="hidden" value="Leadership - Nghệ thuật lãnh đạo" id="bookname">
                    <input type="hidden" id="advice_name" name="advice_name" value="Mana - Bộ 5 sách kỹ năng trong kinh doanh - Leadership - Nghệ thuật lãnh đạo" /> 
                    <input type="hidden" id="csrf" name="_csrf" />
                    <div class="col-lg-4 col-sm-6 col-xs-12">
                        <div class="input-group">
                            <span class="input-group-addon" id="icon-name"><i class="icon-user"></i></span>
                            <input type="text" class="form-control" id="fullname" name="fullname" aria-describedby="icon-name" placeholder="Nhập họ và tên *" required>
                        </div>
                    </div>
                    <div class="col-lg-4 col-sm-6 col-xs-12">
                        <div class="input-group">
                            <span class="input-group-addon" id="icon-email"><i class="icon-mail"></i></span>
                            <input type="text" class="form-control" id="email" name="email" aria-describedby="icon-email" placeholder="Nhập email *" required>
                        </div>
                    </div>
                    <div class="col-lg-4 col-sm-6 col-xs-12">
                        <div class="input-group">
                            <span class="input-group-addon" id="icon-phone"><i class="icon-phone"></i></span>
                            <input type="text" class="form-control" id="phonenumber" name="phonenumber" aria-describedby="icon-phone" placeholder="Nhập số điện thoại *" required>
                        </div>
                    </div>
                    <div class="col-lg-4 col-sm-6 col-xs-12">
                        <div class="input-group">
                            <span class="input-group-addon" id="icon-address"><i class="icon-location"></i></span>
                            <input type="text" class="form-control" id="address" name="address" aria-describedby="icon-address" placeholder="Nhập địa chỉ *" required>
                        </div>
                    </div>
                    <div class="col-lg-4 col-sm-6 col-xs-12">
                        <select class="form-control" name="city" id="city" required>
                            <?php
									if(!empty($city)){
										$cities = $city;
										foreach($cities as $index => $city){
											if($city['id'] == 0){
												?>
													<option value="<?php echo $city['id']; ?>" selected="selected"><?php echo $city['name']; ?></option>
												<?php
											}
											else {
												?>
													<option value="<?php echo $city['id']; ?>" ><?php echo $city['name']; ?></option>
												<?php
											}

										}
									}
									else {
										?>
										<option value="" selected="selected">Chọn Tỉnh/Thành Phố</option>
										<?php
									}
								?>
                        </select>
                    </div>
                    <div class="col-lg-4 col-sm-6 col-xs-12">
                        <select name="district" class="form-control" id="district" required>
                            <option value="0" selected="selected">Chọn Quận/Huyện</option>
                        </select>
                    </div>
                    <center>
                        <button id="dang_ky_form" class="btn btn-register btn_box_register button-regis">Đăng Ký Ngay</button>
                        <p>*Bộ phận tư vấn sẽ liên hệ xác nhận trong 24h, bạn vui lòng giữ máy
                            <br>*Miễn phí vận chuyển trên toàn quốc</p>
                    </center>
                </form>
            </div>
        </div>
    </section>
    <footer>
		<section id="hoi-dap">
			<div class="container">
				<div class="fb-comments fb_iframe_widget fb_iframe_widget_fluid"
					data-href="https://mana.edu.vn/cbp/combo-5-sach-ky-nang-kinh-doanh"
					data-width="100%" data-numposts="10"
					data-colorscheme="light" fb-xfbml-state="rendered">
				 </div>
			</div><!--end .container-->
		</section>
		<div class="container footer">
			<div class="row">
				<div class="col-md-3 logo">
					<div class="footer-logo">
						<ul>
							<li>
								<a href="#">
									<img src="/mana/cbp/combo-5-sach-ky-nang-kinh-doanh/img/1_Logo.png" class="img-responsive"/>
								</a>
							</li>
							<li>
								<a href="#">
									<img src="/mana/cbp/combo-5-sach-ky-nang-kinh-doanh/img/18_KynaLogo.png" class="img-responsive"/>
								</a>
							</li>
						</ul>

					</div>
				</div>
				<div class="col-md-6">
					<div class="footer-text-center">
						<h4>Công ty Cổ phần  Dream Việt Education</h4>
						<p> <b class="company-address"> Trụ sở chính:</b>  Lầu 6 Tòa nhà Thịnh Phát - 178/8 Đường D1, Phường 25, Quận Bình Thạnh, TP Hồ Chí Minh</p>
						<p> <b class="company-address"> Văn phòng Hà Nội:</b>  Tầng 6, Tòa Nhà Viện Công Nghệ - 25 Vũ Ngọc Phan, Phường Láng Hạ, Quận Đống Đa, TP Hà Nội </p>
						<p style="padding-top: 20px">Giấy phép ĐKKD số 0313589030 do Sở Kế hoạch và Đầu tư TPHCM cấp</p>
					</div>
				</div>
				<div class="col-md-3">
					<div class="footer-hot-line">
						<p><b class="company-address">Hotline:</b>  1900 6364 09</p>
            <p><b class="company-address">Email: </b>hotro@kyna.vn</p>
            <div class="fb-page" data-href="https://www.facebook.com/mana.edu.vn/" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/mana.edu.vn/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/mana.edu.vn/">MANA.edu.vn</a></blockquote></div>
					</div>
				</div>
			</div>
		</div>
	</footer>
	<div class="modal fade" id="modal">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-body">
					<div class="popup_body">
						<div class="alert alert-success" role="alert">Cám ơn bạn đã để lại thông tin. Kyna.vn sẽ sớm liên hệ với bạn để tư vấn thêm về chương trình học!</div>
					</div>
				</div>
			</div>
		</div>
	</div>
    <script src="/mana/cbp/combo-5-sach-ky-nang-kinh-doanh/js/bootstrap.js"></script>
    <script src="/mana/cbp/combo-5-sach-ky-nang-kinh-doanh/js/slick.min.js"></script>
    <script src="/mana/cbp/combo-5-sach-ky-nang-kinh-doanh/js/main.js"></script>

	<!--End of Zopim Live Chat Script-->
    <!--Start of Zopim Live Chat Script-->
<script type="text/javascript">
    window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
        d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
    _.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
        $.src="//v2.zopim.com/?1bMWnWprkt76mXJXRb6xBuP2dAD7uxPq";z.t=+new Date;$.
            type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
</script>
<!--End of Zopim Live Chat Script-->

	<div id="fb-root"></div>
	<script type="text/javascript">
		(function(d, s, id) {
			var js, fjs = d.getElementsByTagName(s)[0];
			if (d.getElementById(id)) return;
			js = d.createElement(s); js.id = id;
			js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.0&appId=790788601060712";
			fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));
	</script>

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-M92WKP"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<?php $this->endBody()?>
</body>
</html>
