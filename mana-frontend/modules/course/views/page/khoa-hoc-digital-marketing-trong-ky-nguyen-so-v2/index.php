<?php
use mana\models\Setting;
$dir_name = dirname(dirname(dirname(__FILE__)));
?>
<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from mana.edu.vn/cbp/khoa-hoc-ky-nang-giao-tiep/ by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 27 Apr 2016 13:29:41 GMT -->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<meta property="fb:app_id" content="790788601060712">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Đào tạo Digital Marketing chứng nhận quốc tế - Mana.edu.vn</title>
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,300,500,700' rel='stylesheet' type='text/css'>
    <!-- Bootstrap -->
    <link href="/mana/cs/khoa-hoc-digital-marketing-trong-ky-nguyen-so-v2/css/bootstrap.min.css" rel="stylesheet"/>
    <!-- <link href="/mana/cs/khoa-hoc-digital-marketing-trong-ky-nguyen-so-v2/css/site.css" rel="stylesheet"/> -->
    <link href="/mana/cs/khoa-hoc-digital-marketing-trong-ky-nguyen-so-v2/css/style.css" rel="stylesheet"/>
    <link href="/mana/cs/khoa-hoc-digital-marketing-trong-ky-nguyen-so-v2/css/jquery.fullPage.css" rel="stylesheet"/>
    <link href="/mana/cs/khoa-hoc-digital-marketing-trong-ky-nguyen-so-v2/css/font-awesome.min.css" rel="stylesheet"/>
    <link href="/mana/cs/khoa-hoc-digital-marketing-trong-ky-nguyen-so-v2/css/owl.carousel.css" rel="stylesheet"/>
    <link href="/mana/cs/khoa-hoc-digital-marketing-trong-ky-nguyen-so-v2/css/owl.theme.css" rel="stylesheet"/>
    <link href="/mana/cs/khoa-hoc-digital-marketing-trong-ky-nguyen-so-v2/css/owl.transitions.css" rel="stylesheet"/>
    <link rel="icon" href="/mana/images/manaFavicon.png">
    <?php echo \common\helpers\Html::csrfMetaTags() ?>
</head>
<body>
<?php $this->beginBody()?>

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-M92WKP');</script>
<!-- End Google Tag Manager -->
<?php
// get các tham số cần thiết */
$getParams = $_GET;
$utm_source = '';
$utm_medium = '';
$utm_campaign = '';

if (isset($getParams['utm_source']) && trim($getParams['utm_source']) != '') {
    $utm_source = trim($getParams['utm_source']);
}
if (isset($getParams['utm_medium']) && trim($getParams['utm_medium']) != '') {
    $utm_medium = trim($getParams['utm_medium']);
}
if (isset($getParams['utm_campaign']) && trim($getParams['utm_campaign']) != '') {
    $utm_campaign = trim($getParams['utm_campaign']);
}
?>


<nav class="navbar navbar-default" id="wrap-header" data-sticky_column data-spy="scroll" data-target="#scrollspy-course" data-offset-top="100" class="is_stuck">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#scrollspy-course" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <!-- <a href="https://kyna.vn/" target="_blank" class="navbar-brand kynavn-logo">
        <img src="https://kyna.vn/media/landing_theme/images/global/logo-green.svg" alt="Kyna.vn" class="img-responsive">
      </a> -->
      <a href="https://mana.edu.vn" class="mana-logo"><img src="/mana/cs/khoa-hoc-hoach-dinh-du-an-v2/images/mana.png" alt="Mana" ></a>
    </div>

    <div class="collapse navbar-collapse" id="scrollspy-course">
      <ul class="nav navbar-nav">
        <li><a href="#banner">GIỚI THIỆU</a></li>
        <li><a href="#khoa-hoc">NỘI DUNG</a></li>
        <li><a href="#giang-vien">GIẢNG VIÊN</a></li>
        <li><a href="#loi-ich">LỢI ÍCH</a></li>
        <li><a href="#don-vi-cap-bang">ĐƠN VỊ CẤP BẰNG</a></li>
        <li><a href="#form-dang-ky">ĐĂNG KÝ</a></li>
      </ul>
    </div>
    <!-- /.navbar-collapse -->
  </div>
  <!-- /.container -->
</nav>

<div id="fullpage">

  <div id='banner' class="section" >
    <div class="container">
      <div class="row">
        <div class="col-md-7 col-md-offset-5 text-content">
          <div class="text-content-title">
            <h3>Khoá học</h3>
            <h1>Marketing trong <br>thời đại số</h1>
            <hr>
            <p>
              Khóa học giúp bạn bắt kịp xu hướng Marketing mới nhất trong cuộc chạy đua
              kỹ thuật số đang diễn ra trên toàn thế giới
            </p>
            <p>
              Chương trình được phát triển dựa trên giáo trình, chương trình đào tạo
              <b>iMBA của ĐH Illinois (Hoa Kỳ).</b>
            </p>
          </div>
        </div>
      </div>
    </div>
    <i class="fa fa-angle-down"></i>
  </div>
  <!-- /banner -->

  <div id="loi-ich" class="section">
    <div class="container">
      <div class="row">
        <div class="col-md-6">
          <h3>Lợi ích của khóa học</h3>
          <ul>
            <li><p>
              Xây dựng góc nhìn toàn diện về Digital Marketing.
            </p></li>
            <li><p>
              <span>Đón đầu các xu hướng</span> Marketing mới nhất.
            </p></li>
            <li><p>
              Nhận ra sự thay đổi của hành vi khách hàng khi mua sắm trong thời đại số hóa;
            </p></li>
            <li><p>
              Hiểu được <span>sự tác động của các xu hướng Digital Marketing</span> trong quá trình phát triển sản phẩm mới,
                định giá sản phẩm và truyền thông thương hiệu, ...
            </p></li>
            <li><p>
              Thực hành các khái niệm mới, xu hướng mới vào những <span>bài tập
                tình huống thực tế</span> do chương trình đưa ra.
            </p></li>
          </ul>
        </div>
        <!-- /col-md-6 -->
      </div>
      <!-- row -->
      <i class="fa fa-angle-down"></i>
    </div>
    <!-- container -->
  </div>
  <!-- /loi-ich -->

  <div id="khoa-hoc" class="section">
    <div class="container">
      <div class="row">
        <h3>Khóa học diễn ra trong 4 tuần</h3>
        <div class="col-md-4 col-md-offset-4 list-tuan-hoc">
          <ul>
            <li><a class="active" data-target="tuan1">Tuần 1</a></li>
            <li><a data-target="tuan2">Tuần 2</a></li>
            <li><a data-target="tuan3">Tuần 3</a></li>
            <li><a data-target="tuan4">Tuần 4</a></li>
          </ul>
        </div>

      </div>
      <div class="row tuan tuan1">
        <a class="xs-title">Tuần 1</a>
        <div class="col-md-4 left">
          <h4>Tổng quan khóa học, xu hướng phát triển và sáng tạo sản phẩm trong Digital Marketing</h4>
          <div class="line"></div>
          <p>
            Bạn sẽ được làm quen với khóa học trên nền tảng học trực tuyến,
            tiếp xúc với người hướng dẫn và bạn học thông qua Facebook Group.
            Ngoài ra, bạn sẽ được tiếp cận với định nghĩa về xu hướng mới trong Marketing.
          </p>
          <p>
            <b>CASE STUDY</b>: Local Motors
          </p>
          <a href="#form-dang-ky" class="btn-dang-ky-hoc">Đăng ký học</a>
        </div>
        <!-- left -->
        <div class="col-md-4 middle">
          <div class="item-box">
            <b>Định nghĩa về Marketing và những xu hướng mới trong Marketing:</b>
            <ul>
              <li>Marketing là gì?</li>
              <li>Định nghĩa về Marketing.</li>
              <li>Marketing 4P.</li>
              <li>Marketing 4Ps và thương hiệu.</li>
            </ul>
          </div>
          <div class="item-box">
            <b>Xu hướng mới trong Digital Marketing: Khách hàng đồng sáng tạo:</b>
            <ul>
              <li>Vai trò của khách hàng</li>
              <li>Khái niệm khách hàng đồng sáng tạo</li>
              <li>3 vấn đề cốt lõi trong khách hàng đồng sáng tạo</li>
              <li>Những thí nghiệm liên quan đến khách hàng đồng sáng tạo</li>
              <li>Cách thức để thực hiện chiến lược “khách hàng đồng sáng tạo” hiệu quả</li>
            </ul>
          </div>
        </div>
        <!-- middle -->
        <div class="col-md-4 right">
          <div class="item-box">
            <b>Khái niệm về sản phẩm (Product) trong Marketing Mix:</b>
            <ul>
              <li>Khái niệm cơ bản về sản phẩm</li>
              <li>Định vị sản phẩm</li>
              <li>Phát triển sản phẩm</li>
              <li>Quản lý thương hiệu</li>
            </ul>
          </div>
          <div class="item-box">
            <b>Khái niệm thương hiệu Doppelgänger trong Digital Marketing:</b>
            <ul>
              <li>Khái niệm thương hiệu Doppelgänger trong Digital Marketing</li>
              <li>Tầm quan trọng của phát triển thương hiệu</li>
              <li>Định nghĩa thương hiệu Doppelgänger</li>
              <li>Ba vấn đề cốt lõi trong thương hiệu sánh đôi</li>
              <li>Tác động của thương hiệu sánh đôi</li>
              <li>Giải pháp</li>
            </ul>
          </div>
        </div>
        <!-- right -->
      </div>
      <!-- /row tuan1 -->

      <div class="row tuan tuan2">
        <a class="xs-title">Tuần 2</a>
        <div class="col-md-4 left">
          <h4>Những xu hướng mới trong Digital Marketing để thuyết phục khách hàng mua sắm</h4>
          <div class="line"></div>
          <p>
            Làm cách nào để quảng bá sản phẩm trong kỷ nguyên công nghệ?
            Trong module này, bạn sẽ được tiếp xúc với các khái niệm mới về Marketing và các CASE STUDY điển hình. Bên cạnh đó, rất nhiều bài thực hành đang đợi bạn tham gia trải nghiệm.
          </p>
          <p>
            <b>CASE STUDY</b>: Pepsi Refresh
          </p>
          <a href="#form-dang-ky" class="btn-dang-ky-hoc">Đăng ký học</a>
        </div>
        <!-- left -->
        <div class="col-md-4 middle">
          <div class="item-box">
            <b>Khái niệm về quảng bá (Promotion) trong Marketing Mix:</b>
            <ul>
              <li>Khái niệm về quảng bá</li>
              <li>Tìm hiểu về quảng bá trong Marketing Mix</li>
              <li>Các công cụ thường dùng trong quảng bá</li>
              <li>Cách thức sử dụng 5 công cụ quảng bá</li>
              <li>Nhận thức về truyền thông Marketing tích hợp</li>
              <li>Quảng cáo: Kênh tiếp thị trực quan sinh động và hiệu quả</li>
              <li>Yếu tố thuyết phục trong quảng cáo.</li>
            </ul>
          </div>
          <div class="item-box">
            <b>Xu hướng mới trong Digital Marketing: Nội dung do người dùng tạo ra</b>
          </div>
        </div>
        <!-- middle -->
        <div class="col-md-4 right">
          <div class="item-box">
            <b>Khái niệm về tính xác thực trong Digital Marketing:</b>
            <ul>
              <li>Tính xác thực trong Marketing</li>
              <li>Những tình huống thực tế mô tả tính xác thực trong Marketing</li>
              <li>Định nghĩa về tính xác thực trong Digital Marketing</li>
              <li>3 vấn đề cốt lõi của tính xác thực trong Digital Marketing</li>
              <li>Nghiên cứu về tính xác thực trong Digital Marketing</li>
              <li>Nghiên cứu về tính xác thực trong Digital Marketing</li>
            </ul>
          </div>

        </div>
        <!-- right -->
      </div>
      <!-- /row tuan2 -->

      <div class="row tuan tuan3">
        <a class="xs-title">Tuần 3</a>
        <div class="col-md-4 left">
          <h4>Những xu hướng mới trong Digital Marketing để phân phối sản phẩm hiệu quả</h4>
          <div class="line"></div>
          <p>
            Làm thế nào sản phẩm được trưng bày và phân phối trong thời đại số hóa?.
            Trong module này, bạn sẽ biết vì sao các công cụ kỹ thuật số có thể làm thay đổi việc phân phối
            các sản phẩm và tạo nên một cuộc cách mạng bán lẻ trên toàn thế giới.
          </p>
          <p>
            <b>CASE STUDY</b>: Threadless
          </p>
          <a href="#form-dang-ky" class="btn-dang-ky-hoc">Đăng ký học</a>
        </div>
        <!-- left -->
        <div class="col-md-4 middle">
          <div class="item-box">
            <b>Khái niệm về phân phối (Place) trong Marketing Mix</b>
            <ul>
              <li>Định nghĩa phân phối trong Marketing Mix</li>
              <li>Tìm hiểu về trung gian phân phối</li>
              <li>Những thay đổi trong phân phối sản phẩm</li>
              <li>Những thay đổi trong phân phối sản phẩm</li>
              <li>Tìm hiểu về bán lẻ</li>
              <li>Bán lẻ trong kỷ nguyên số</li>
              <li>Công nghệ in 3D và hình thức phân phối mới</li>
            </ul>
          </div>
          <div class="item-box">
            <b>Xu hướng mới trong Digital Marketing: sử dụng công nghệ in 3D</b>
            <ul>
              <li>Những sản phẩm đến từ công nghệ in ấn</li>
              <li>Đồng sáng tạo trong công nghệ in 3D</li>
              <li>Phân phối trong công nghệ in 3D.</li>
            </ul>
          </div>
        </div>
        <!-- middle -->
        <div class="col-md-4 right">
          <div class="item-box">
            <b>Khái niệm về bán lẻ hiện đại trong Digital Marketing</b>
            <ul>
              <li>Thương mại điện tử lên ngôi</li>
              <li>Hình thức kết hợp Online và Offline</li>
              <li>Những đặc tính quan trọng trong thị trường bán lẻ</li>
              <li>Những nghiên cứu về bán lẻ hiện đại</li>
              <li>Giải pháp để hoạt động hiệu quả trong thị trường bán lẻ hiện đại</li>
            </ul>
          </div>
        </div>
        <!-- right -->
      </div>
      <!-- /row tuan3 -->

      <div class="row tuan tuan4">
        <a class="xs-title">Tuần 4</a>
        <div class="col-md-4 left">
          <h4>Những xu hướng mới trong Digital Marketing để xác định giá bán sản phẩm</h4>
          <div class="line"></div>
          <p>
            Module này sẽ giúp bạn biết cách nâng cao giá trị và giá thành sản phẩm của mình trong thời đại số.
          </p>
          <p>
            <b>CASE STUDY</b>: Radiohead
          </p>
          <a href="#form-dang-ky" class="btn-dang-ky-hoc">Đăng ký học</a>
        </div>
        <!-- left -->
        <div class="col-md-4 middle">
          <div class="item-box">
            <b>Khái niệm về giá cả (Price) trong Marketing Mix</b><br><br>

          </div>
          <div class="item-box">

            <b>Xu hướng mới trong Digital Marketing: Sử dụng công cụ so sánh giá:</b><br>
            <ul>
              <li>Lý do hình thành công cụ so sánh giá</li>
              <li>Lợi ích của khách hàng đến từ công cụ so sánh giá</li>
              <li>Định nghĩa về công cụ so sánh giá</li>
              <li>Đặc điểm của công cụ so sánh giá</li>
              <li>Nghiên cứu về công cụ so sánh giá</li>
              <li>Cách thức sử dụng công cụ so sánh giá hiệu quả</li>
            </ul>
            <br>
          </div>
        </div>
        <!-- middle -->
        <div class="col-md-4 right">
          <div class="item-box">
            <b>Khái niệm về “Trả bao nhiêu bạn muốn” trong Digital Marketing</b><br><br>

          </div>
          <div class="item-box">
            <b>Tổng kết khóa học Marketing in a Digital World</b><br><br>
            <!-- <b>Xu hướng mới trong Digital Marketing: Nội dung do người dùng tạo ra</b> -->
          </div>
        </div>
        <!-- /right -->
      </div>
      <!-- /row tuan4 -->

    </div>
    <!-- container -->
    <i class="fa fa-angle-down"></i>
  </div>
  <!-- /khoa hoc -->

  <div id="don-vi-cap-bang" class="section" >
    <div class="container">
      <div class="col-md-5 left">
        <img src="/mana/cs/khoa-hoc-digital-marketing-trong-ky-nguyen-so-v2/images/illinois.png" alt="Illinois" class="img-responsive"/>
      </div>
      <div class="col-md-7 right">
        <h4>Chứng chỉ được cấp bởi</h4>
        <h3>Đại học Illinois - Urbana <br>
          champaign (Uiuc - Hoa Kỳ)
        </h3>
        <div class="line">

        </div>
        <ul>
          <li><i class="fa fa-star"></i>
          <p>
            UIUC là một trong ba cơ sở của Đại học Illinois.
          </p></li>
          <li><i class="fa fa-star"></i>
          <p>
            Top 50 trường <b>Đại học tốt nhất nước Mỹ.</b>
          </p></li>
          <li><i class="fa fa-star"></i>
          <p>
            UIUC là một trong các trường ĐH có môi trường đào tạo và nghiên cứu hàng đầu thế giới.
          </p></li>
          <li><i class="fa fa-star"></i>
          <p>
            Chứng chỉ được cấp bởi ĐH UIUC <b>được công nhận trên toàn thế giới.</b>
          </p></li>
        </ul>
      </div>
      <!-- right -->

    </div>
    <!-- container -->
    <i class="fa fa-angle-down"></i>
  </div>

  <div id="giang-vien" class="section">
    <div class="container">
      <h3>Giảng viên</h3>
      <div class="row sm-row">
        <div class="col-md-6 left">
          <img src="/mana/cs/khoa-hoc-digital-marketing-trong-ky-nguyen-so-v2/images/gv1.png" alt="" class="img-responsive sm-gv1"/>
          <img src="/mana/cs/khoa-hoc-digital-marketing-trong-ky-nguyen-so-v2/images/bw1.png" alt="" class="img-responsive md-gv" data-target="gv1" data-img-bw="bw1"/>
        </div>
        <div class="col-md-6 sm-gv1">
          <p>
            Giảng viên đến từ Đại học UIUC
          </p>
          <b>Giáo sư Aric Rindfleisch</b>
          <ul>
            <li><p>
              GĐ Điều hành của UIUC.
            </p></li>
            <li>
              <p>
                Giáo sư giảng dạy Marketing tại John M.Jones Professor.
              </p></li>
            <li><p>
              Tốt nghiệp Tiến sĩ trường ĐH Wisconsin tại Madison.
            </p></li>
          </ul>
        </div>
        <div class="col-md-6 right">
          <img src="/mana/cs/khoa-hoc-digital-marketing-trong-ky-nguyen-so-v2/images/gv2.png" alt="" class="img-responsive sm-gv2"/>
          <img src="/mana/cs/khoa-hoc-digital-marketing-trong-ky-nguyen-so-v2/images/bw2.png" alt="" class="img-responsive md-gv" data-target="gv2" data-img-bw="bw2"/>
        </div>
        <div class="col-md-6 sm-gv2">
          <p>
            Giảng viên Việt Nam
          </p>
          <b>Th.S Trần Khánh Tùng</b>
          <ul>
            <li><p>
              Nguyên Giám đốc Marketing tại IMC Group.
            </p></li>
            <li><p>
              Hiện là Co-founder của Your Fashion.
            </p></li>
            <li><p>
              Ông từng làm việc với sinh viên MBA ĐH Harvard trong dự án nghiên cứu hành vi cũng như xu hướng giải trí của giới trẻ Việt Nam.
            </p></li>
            <li><p>
              Tốt nghiệp ĐH Sorbonne Paris.
            </p></li>
          </ul>
        </div>

        <div class="col-md-6 tt-gv gv1">
          <p>
            Giảng viên đến từ Đại học UIUC
          </p>
          <b>Giáo sư Aric Rindfleisch</b>
          <ul>
            <li><p>
              GĐ Điều hành của UIUC.
            </p></li>
            <li>
              <p>
                Giáo sư giảng dạy Marketing tại John M.Jones Professor.
              </p></li>
            <li><p>
              Tốt nghiệp Tiến sĩ trường ĐH Wisconsin tại Madison.
            </p></li>
          </ul>
        </div>
        <div class="col-md-6 tt-gv gv2">
          <p>
            Giảng viên Việt Nam
          </p>
          <b>Th.S Trần Khánh Tùng</b>
          <ul>
            <li><p>
              Nguyên Giám đốc Marketing tại IMC Group.
            </p></li>
            <li><p>
              Hiện là Co-founder của Your Fashion.
            </p></li>
            <li><p>
              Ông từng làm việc với sinh viên MBA ĐH Harvard trong dự án nghiên cứu hành vi cũng như xu hướng giải trí của giới trẻ Việt Nam.
            </p></li>
            <li><p>
              Tốt nghiệp ĐH Sorbonne Paris.
            </p></li>
          </ul>
        </div>

      </div>
      <!-- row -->

    </div>
    <i class="fa fa-angle-down"></i>
  </div>

  <div id="cam-nhan" class="section">
    <div class="container">
      <h3>Học viên nói gì về chương trình học</h3>
      <div class="col-md-6 left">
        <img src="/mana/cs/khoa-hoc-digital-marketing-trong-ky-nguyen-so-v2/images/quote.png" alt="quote" />
        <p>
          Nhịp độ công việc của tôi khá cao nhưng tốc độ thay đổi của Marketing hiện đại còn nhanh hơn gấp trăm lần.
          Chương trình học này cho phép tôi vẫn đảm bảo công việc hằng ngày nhưng vẫn có thể tiếp thu với
          nguồn kiến thức mới một cách chủ động và tự tin.
          Đáng ngạc nhiên là việc áp dụng các kiến thức chuyên sâu và nắm bắt các xu hướng Marketing
          đã khiến Sếp nhìn tôi với con mắt khác.
        </p>
        <div class="hoc-vien">
          <img src="/mana/cs/khoa-hoc-digital-marketing-trong-ky-nguyen-so-v2/images/hv1.png" alt="" />
          <h4>Trần Thanh Tâm</h4>
          <p>
            GĐ Marketing Công ty Nội thất Thanh Tâm
          </p>
        </div>
      </div>
      <div class="col-md-6 right">
        <img src="/mana/cs/khoa-hoc-digital-marketing-trong-ky-nguyen-so-v2/images/quote.png" alt="quote" />
        <p>
          Thực ra, chị chẳng quan tâm mấy đến chứng chỉ của các trường ĐH nổi tiếng trên thế giới.
          Điều chị quan tâm là nguồn kiến thức được học có thực sự hữu dụng không.
          Nhưng thực tế sau khi học khóa Digital Marketing tại MANA chị phải công nhận hệ thống bài giảng,
          nội dung và phương pháp học tại đây thực sự rất tuyệt. Cơ hội thực hành, nắm bắt xu hướng Marketing mới,
          tiếp xúc với những nguồn kiến thức có tính thực tế cao làm chị rất hứng thú học hỏi.
        </p>
        <div class="hoc-vien">
          <img src="/mana/cs/khoa-hoc-digital-marketing-trong-ky-nguyen-so-v2/images/hv2.png" alt="" />
          <h4>Phượng Hoàng</h4>
          <p>
            Lead team Content Công ty Du lịch
          </p>
        </div>
      </div>

    </div>
    <i class="fa fa-angle-down"></i>
  </div>
  <!-- cam-nhan -->

  <div id="gioi-thieu" class="section">
    <div class="container">
      <div class="row text-intro">
        <h3>"Xin chào, chúng tôi là MANA <br>
           Viện đào tạo Quản trị Kinh doanh trực tuyến hàng đầu Việt Nam. <br>
           <span>
             Chúng tôi mang đến một hệ thống các chương trình đào tạo kỹ năng quản trị kinh doanh có tính ứng dụng cao."
           </span>
         </h3>
         <p>
           Tại MANA, các bạn sẽ được học các chương trình
           <span> đào tạo song ngữ với đội ngũ cố vấn học tập</span> 24/7.
           <br>Hình thức và phương pháp học khoa học mang lại cho bạn những trải nghiệm học tập mới mẻ và hữu ích:
         </p>

      </div>
      <div class="row list-item">
        <div class="col-md-2 col-md-offset-1">
          <img src="/mana/cs/khoa-hoc-digital-marketing-trong-ky-nguyen-so-v2/images/video.png" alt="" />
          <p><span>1. </span>
            Học trực tuyến các bài giảng Video
          </p>
        </div>
        <!-- item 1 -->
        <div class="col-md-2">
          <img src="/mana/cs/khoa-hoc-digital-marketing-trong-ky-nguyen-so-v2/images/thuchanh.png" alt="" />
          <p><span>2. </span>
            Thực hành hàng tuần
          </p>
        </div>
        <!-- item 2 -->

        <div class="col-md-2">
          <img src="/mana/cs/khoa-hoc-digital-marketing-trong-ky-nguyen-so-v2/images/fb.png" alt="" />
          <p><span>3. </span>
            Thảo luận trên Facebook Group
          </p>
        </div>
        <!-- item 3 -->

        <div class="col-md-2">
          <img src="/mana/cs/khoa-hoc-digital-marketing-trong-ky-nguyen-so-v2/images/kiemtra.png" alt="" />
          <p><span>4. </span>
            Kiểm tra tiểu luận cuối khóa
          </p>
        </div>
        <!-- item 4 -->

        <div class="col-md-2">
          <img src="/mana/cs/khoa-hoc-digital-marketing-trong-ky-nguyen-so-v2/images/nophoso.png" alt="" />
          <p><span>5. </span>
            Nộp hồ sơ và nhận bằng Quốc tế
          </p>
        </div>
        <!-- item 5 -->
      </div>

    </div>
    <i class="fa fa-angle-down"></i>
  </div>
  <!-- /gioi-thieu -->

  <div id="hoc-online" class="section">
    <div class="container">
      <div class="row text-header">
        <h4>Học online cùng Mana online business school</h4>
        <h3>Nhận chứng chỉ quốc tế</h3>
      </div>
      <!-- /text header -->
      <div class="row item-list">
        <div class="col-md-3 item">
          <i class="fa fa-clock-o"></i>
            <p>
              Học Online mọi lúc mọi nơi
            </p>
        </div>
        <!-- /item -->
        <div class="col-md-3 item">
          <i class="fa fa-globe"></i>
            <p>
              Chương trình học <b>song ngữ</b> Anh - Việt
            </p>
        </div>
        <!-- /item -->
        <div class="col-md-3 item">
          <i class="fa fa-graduation-cap"></i>
            <p>
              Học cùng chuyên gia - thực nghiệm cùng chuyên gia
            </p>
        </div>
        <!-- /item -->
        <div class="col-md-3 item">
          <i class="fa fa-certificate"></i>
            <p>
              Học tại Việt Nam nhận <b>chứng chỉ Quốc tế</b>
            </p>
        </div>
        <!-- /item -->
      </div>
      <div class="row" id="form-dang-ky">
        <h4>Đăng ký nhận tư vấn miễn phí</h4>
        <form name="landing-page-id" class="form-inline" id="form_advice" action="/course/page/submit" method="post" accept-charset="utf-8">
          <input type="hidden" id="advice_name" name="advice_name" value="Mana - Marketing trong kỷ nguyên số v2" /> 
          <input type="hidden" id="csrf" name="_csrf" />
          <div class="form-group">
            <input type="text" class="form-control" id="fullname" name="fullname" placeholder="Họ và tên">
          </div>
          <div class="form-group">
            <input type="text" class="form-control" id="phonenumber" name="phonenumber" placeholder="Số điện thoại">
          </div>
          <div class="form-group">
            <input type="email" class="form-control" id="email" name="email" placeholder="Email">
          </div>
          <div class="clearfix">

          </div>
          <div class="form-action">
            <button type="submit" id="dang_ky_form" class="btn-dang-ky btn_box_register">Đăng ký
              <span>
              <div class="kyna-click-form"></div>
              <div class="kyna-click-form-fill"></div>
              <div class="kyna-click-form-img"></div>
          </span>
            </button>
          </div>
          <p>
            Bộ phận CSKH của MANA sẽ sớm liên hệ và tư vấn miễn phí về khóa học cho bạn.
          </p>

        </form>
        <div class="clearfix"></div>

      </div>
    </div>
  </div>
  <!-- /hoc-online -->

  <!-- <div id="hoi-dap" style="background: #f7f7f7;">
    <div class="container">
        <div class="fb-comments fb_iframe_widget fb_iframe_widget_fluid" data-href="https://kyna.vn/nhom-khoa-hoc/digital-marketing-danh-cho-bat-dong-san" data-width="100%" data-numposts="10" data-colorscheme="light" fb-xfbml-state="rendered"><span style="height: 173px;"><iframe id="f2c0e6ad3b0b2dc" name="f2b21dbf4d78c" scrolling="no" title="Facebook Social Plugin" class="fb_ltr fb_iframe_widget_lift" src="https://www.facebook.com/plugins/comments.php?api_key=191634267692814&amp;channel_url=https%3A%2F%2Fstaticxx.facebook.com%2Fconnect%2Fxd_arbiter.php%3Fversion%3D42%23cb%3Df18e273cb35ff2c%26domain%3Dkyna.vn%26origin%3Dhttps%253A%252F%252Fkyna.vn%252Ff3e7e80d0e3a294%26relation%3Dparent.parent&amp;colorscheme=light&amp;href=https%3A%2F%2Fkyna.vn%2Fnhom-khoa-hoc%2Fdigital-marketing-danh-cho-bat-dong-san&amp;locale=vi_VN&amp;numposts=10&amp;sdk=joey&amp;skin=light&amp;version=v2.0&amp;width=100%25" style="border: none; overflow: hidden; height: 173px; width: 100%;"></iframe></span></div>
    </div>

  </div> -->
  <!-- hoi-dap -->
</div>
<!-- footer -->
<footer id="footer">
    <div class="container footer">
        <div class="row">
            <div class="col-sm-3">
                <div class="footer-logo">
                    <ul>
                        <li>
                            <a href="https://mana.edu.vn">
                                <img src="/mana/cs/khoa-hoc-digital-marketing-trong-ky-nguyen-so/imgs/mana.png" class="img-responsive"/>
                            </a>
                        </li>
                        <li>
                            <a href="https://kyna.vn">
                                <img src="/mana/cs/khoa-hoc-digital-marketing-trong-ky-nguyen-so/imgs/18_KynaLogo.png" class="img-responsive"/>
                            </a>
                        </li>
                    </ul>

                </div>
            </div>
            <div class="col-sm-6">
                <div class="footer-text-center">
                    <h4>Công ty Cổ phần  Dream Việt Education</h4>
                    <p> <b class="company-address"> Trụ sở chính:</b> Lầu 6 Tòa nhà Thịnh Phát - 178/8 Đường D1, Phường 25, Quận Bình Thạnh, TP Hồ Chí Minh</p>
                    <p> <b class="company-address"> Văn phòng Hà Nội:</b> Tầng 6, Tòa Nhà Viện Công Nghệ - 25 Vũ Ngọc Phan, Phường Láng Hạ, Quận Đống Đa, TP Hà Nội </p>
                    <p style="padding-top: 20px">Giấy phép ĐKKD số 0313589030 do Sở Kế hoạch và Đầu tư TPHCM cấp</p>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="footer-hot-line">
                    <p><b class="company-address">Hotline:</b>  <?=Setting::HOTLINE ?></p>
                    <!-- <p><b class="company-address">Hotline:</b>  1900 6364 09</p>                     -->
                    <p><b class="company-address">Email: </b>hotro@kyna.vn</p>
                    <div class="fb-page" data-href="https://www.facebook.com/mana.edu.vn/" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/mana.edu.vn/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/mana.edu.vn/">MANA.edu.vn</a></blockquote></div>
                </div>
            </div>
        </div>
    </div>
</footer>
<div class="modal fade" id="modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <div class="popup_body">
                    <div class="alert alert-success" role="alert">Cám ơn bạn đã để lại thông tin. Kyna.vn sẽ sớm liên hệ với bạn để tư vấn thêm về chương trình học!</div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="/mana/cs/khoa-hoc-digital-marketing-trong-ky-nguyen-so-v2/js/jquery-1.11.2.min.js"></script>
<script src="/mana/cs/khoa-hoc-digital-marketing-trong-ky-nguyen-so-v2/js/bootstrap.min.js"></script>
<script src="/mana/cs/khoa-hoc-digital-marketing-trong-ky-nguyen-so-v2/js/jquery.fullPage.min.js"></script>
<script src="/mana/cs/khoa-hoc-digital-marketing-trong-ky-nguyen-so-v2/js/jquery.scrollme.min.js"></script>
<script src="/mana/cs/khoa-hoc-digital-marketing-trong-ky-nguyen-so-v2/js/owl.carousel.min.js"></script>

<!-- Nhan them vao -->
<script>
    $(document).ready(function(){

        $(window).on("scroll", function(e) {
          var nextScroll = $(window).scrollTop();
          if ($(window).width() > 768) {
            if(nextScroll > 100) {
                $('#wrap-header').hide();
            } else {
              $('#wrap-header').css({height: "100px", backgroundColor: "transparent"});
              $('#wrap-header .mana-logo img').css("margin-top", "25px");
              $('#wrap-header .navbar-nav').css("margin-top", "30px");
              $('#wrap-header').show();
            }
          }
          currentScroll = nextScroll;
      });
        if ($(window).width() > 768) {
          $('#fullpage').fullpage({
            scrollBar: true,
            // srollOverflow: true,
            fixedElements: '#wrap-header',
            // normalScrollElements: '#hoi-dap, #footer',
            fitToSection: false,
            controlArrows: true,
          });
        }
        $(".list-tuan-hoc a").on('click', function (e) {
          $this = $(this);
          $this.parents('#khoa-hoc').find('.tuan:visible').css("display", "none");
          $('.' + $this.attr('data-target')).css("display", "flex");
          $this.parents('ul').find('.active').removeClass('active');
          $this.addClass('active');
        });
        $('#giang-vien .md-gv').on('mouseenter', function (e) {
          $this = $(this);
          if ($('.tt-gv:visible').length == 0) {
            $this.attr('src', '/mana/cs/khoa-hoc-digital-marketing-trong-ky-nguyen-so-v2/images/' + $this.attr('data-target') + '.png');
            $('.' + $this.attr('data-target')).show();
          }

        });
        $('#giang-vien .md-gv').on('mouseleave', function (e) {
          $this = $(this);
          if (!$(e.toElement).hasClass('tt-gv')) {
            $this.attr('src', '/mana/cs/khoa-hoc-digital-marketing-trong-ky-nguyen-so-v2/images/' + $this.attr('data-img-bw') + '.png');
            $('.' + $this.attr('data-target')).hide();
          }
        });
        $('#giang-vien .sm-row').on('mouseleave', function (e) {
          $('.left .md-gv').attr('src', '/mana/cs/khoa-hoc-digital-marketing-trong-ky-nguyen-so-v2/images/bw1.png');
          $('.right .md-gv').attr('src', '/mana/cs/khoa-hoc-digital-marketing-trong-ky-nguyen-so-v2/images/bw2.png');
          $('.tt-gv:visible').hide();
        });

        $('a[href*=#]:not([href=#])').click(function() {
          if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {

            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
              if($(window).width() > 768){
                  if (target.length) {
                      $('html,body').animate({
                        scrollTop: target.offset().top - 80
                      }, 1000);
                      return false;
                  }
              } else {
                  if (target.length) {
                      $('html,body').animate({
                        scrollTop: target.offset().top - 80
                      }, 1000);
                      return false;
                  }
              }
          }
        });



    });


</script>
<!--End of Zopim Live Chat Script-->
    <!--Start of Zopim Live Chat Script-->
<script type="text/javascript">
    window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
        d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
    _.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
        $.src="//v2.zopim.com/?1bMWnWprkt76mXJXRb6xBuP2dAD7uxPq";z.t=+new Date;$.
            type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
</script>
<!--End of Zopim Live Chat Script-->
<div id="fb-root"></div>
<script type="text/javascript">
  (function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.0&appId=790788601060712";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));
</script>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-M92WKP"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<?php $this->endBody()?>
</body>

</html>
