<?php
use mana\models\Setting;

$dir_name = dirname(dirname(dirname(__FILE__)));
?>
<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from mana.edu.vn/cbp/khoa-hoc-ky-nang-giao-tiep/ by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 27 Apr 2016 13:29:41 GMT -->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta property="fb:app_id" content="790788601060712">
    <meta name="robots" content="index,follow">
    <meta property="og:type" content="website"/>
    <meta property='og:url' content='https://mana.edu.vn/cbp/khoa-hoc-ky-nang-ban-hang-chuyen-nghiep-v2'/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta property='og:title' content='Đào tạo nghi thức giao tiếp trong kinh doanh - Mana.edu.vn'/>
    <meta property='og:description'
          content='Nghi thức giao tiếp trong kinh doanh là một dạng nghi thức xã giao vô cùng quan trọng. Nắm vững nghi thức giao tiếp kinh doanh có thể giúp bạn mang về cho doanh nghiệp một khoản lợi nhuận kếch xù. Bên cạnh đó còn tạo được phong cách kinh doanh cho cá nhân bạn.'/>
    <meta property='og:image:url' content='/mana/cbp/khoa-hoc-nghi-thuc-giao-tiep-v2/img/intro.jpg'/>

    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Đào tạo nghi thức giao tiếp trong kinh doanh - Mana.edu.vn</title>
    <link href="/mana/cbp/khoa-hoc-nghi-thuc-giao-tiep-v2/css/bootstrap.min.css" rel="stylesheet"/>
    <link href="/mana/cbp/khoa-hoc-nghi-thuc-giao-tiep-v2/css/style.css" rel="stylesheet"/>
    <link href="/mana/cbp/khoa-hoc-nghi-thuc-giao-tiep-v2/css/font-awesome.min.css" rel="stylesheet"/>
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700" rel="stylesheet">
    <script type="text/javascript" src="/mana/cbp/khoa-hoc-nghi-thuc-giao-tiep-v2/js/jquery-1.11.2.min.js"></script>
    <script type="text/javascript" src="/mana/cbp/khoa-hoc-nghi-thuc-giao-tiep-v2/js/bootstrap.min.js"></script>
    <link rel="icon" href="/mana/images/manaFavicon.png">
      <?php echo \common\helpers\Html::csrfMetaTags() ?>
</head>
<body>
<?php $this->beginBody() ?>

<!-- Google Tag Manager -->
<script>(function (w, d, s, l, i) {
        w[l] = w[l] || [];
        w[l].push({
            'gtm.start': new Date().getTime(), event: 'gtm.js'
        });
        var f = d.getElementsByTagName(s)[0],
            j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';
        j.async = true;
        j.src =
            'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
        f.parentNode.insertBefore(j, f);
    })(window, document, 'script', 'dataLayer', 'GTM-M92WKP');</script>
<!-- End Google Tag Manager -->
<?php
// get các tham số cần thiết */
$getParams = $_GET;
$utm_source = '';
$utm_medium = '';
$utm_campaign = '';

if (isset($getParams['utm_source']) && trim($getParams['utm_source']) != '') {
    $utm_source = trim($getParams['utm_source']);
}
if (isset($getParams['utm_medium']) && trim($getParams['utm_medium']) != '') {
    $utm_medium = trim($getParams['utm_medium']);
}
if (isset($getParams['utm_campaign']) && trim($getParams['utm_campaign']) != '') {
    $utm_campaign = trim($getParams['utm_campaign']);
}
?>
<header>
    <nav class="navbar navbar-fixed-top" id="wrap-header">
        <div class="container">
            <div class="navbar-header">
                <a href="#register" class="button-register scroll mb">Đăng ký</a>
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                        data-target="#menu-collapse" aria-expanded="false">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a href="/" target="_blank" class="navbar-brand kyna-logo">
                    <img src="/mana/cbp/khoa-hoc-nghi-thuc-giao-tiep-v2/img/mana.png" alt="Kyna.vn"
                         class="img-responsive img-logo">
                </a>
            </div>

            <div class="collapse navbar-collapse" id="menu-collapse">
                <ul class="nav navbar-nav">
                    <li><a href="#intro">GIỚI THIỆU</a></li>
                    <li><a href="#etiquette-course">NỘI DUNG</a></li>
                    <li><a href="#author">GIẢNG VIÊN</a></li>
                    <li><a href="#route">LỢI ÍCH</a></li>
                    <li><a href="#register" class="scroll button-scroll">ĐĂNG KÝ</a></li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>
</header>
<main>
    <section id="banner">
        <div class="container">
            <h1>Nghi thức giao tiếp<br/>trong kinh doanh</h1>
            <p class="text-title">Theo tiêu chuẩn Quốc tế (*)</p>
            <div class="button">
                <a href="#intro">Tìm hiểu thêm</a>
            </div>
            <p class="text">(*) Hoàn thành khóa học bạn sẽ được cấp chứng chỉ bởi Hiệp hội IBTA (Hiệp hội Đào tạo Kinh
                doanh Quốc tế.)</p>
        </div><!--end .container-->
    </section>

    <section id="intro">
        <div class="container">
            <div class="top">
                <span><img src="/mana/cbp/khoa-hoc-nghi-thuc-giao-tiep-v2/img/icon-top.png" alt=""></span>
                <h2>Hiểu lễ nghĩa đi khắp thiên hạ.</br>Vô lễ một bước cũng khó khăn.</h2>
                <span><img src="/mana/cbp/khoa-hoc-nghi-thuc-giao-tiep-v2/img/icon-bottom.png" alt=""></span>
                <p class="text-title">(Theo triết lý của người phương Đông)</p>
                <p class="text-bottom">Lễ nghĩa trong kinh doanh chính là chữ tín, là các nghi thức giao tiếp mà người
                    làm kinh doanh cần phải tuân theo.</p>
            </div><!--end .top-->
            <div class="wrap-box">
                <div class="col-sm-6 col-xs-12 img">
                    <img src="/mana/cbp/khoa-hoc-nghi-thuc-giao-tiep-v2/img/intro-left.png"
                         alt="Thành công trong kinh doanh" class="img-responsive">
                </div><!--end .img-->
                <div class="col-sm-6 col-xs-12 text">
                    <h3>Thành công trong kinh doanh</h3>
                    <span class="line"><span></span></span>
                    <ul>
                        <li>
                            <span class="first">30%</span>
                            <span class="last">dựa vào kỹ xảo</span>
                        </li>

                        <li>
                            <span class="first">70%</span>
                            <span class="last">dựa vào thu phục lòng người</span>
                        </li>
                    </ul>
                    <p>Kỹ xảo kinh doanh dựa trên khả năng và kinh nghiệm thương trường của bạn. Thu phục được lòng
                        người hay không phụ thuộc vào kỹ năng và sự hiểu biết của bạn. Vậy nên, việc nắm vững các nghi
                        thức giao tiếp trong kinh doanh là vô cùng quan trọng.</p>
                </div><!--end .text-->
            </div><!--end .wrap-box-->
        </div><!--end .container-->
    </section>

    <section id="etiquette-course">
        <div class="container">
            <h2>Khóa học nghi thức giao tiếp trong kinh doanh<br/> CBP Business Etiquette</h2>
            <span class="line"><span></span></span>
            <ul>
                <li>
                    <img src="/mana/cbp/khoa-hoc-nghi-thuc-giao-tiep-v2/img/icon-img-1.png" alt=""
                         class="img-responsive">
                    <span class="title">12</span>
                    <p>bài học</p>
                </li>

                <li>
                    <img src="/mana/cbp/khoa-hoc-nghi-thuc-giao-tiep-v2/img/icon-img-2.png" alt=""
                         class="img-responsive">
                    <span class="title">10</span>
                    <p>giờ học</p>
                </li>

                <li>
                    <img src="/mana/cbp/khoa-hoc-nghi-thuc-giao-tiep-v2/img/icon-img-3.png" alt=""
                         class="img-responsive">
                    <span class="title">học online</span>
                    <p>Kết nối toàn cầu</p>
                </li>

                <li>
                    <img src="/mana/cbp/khoa-hoc-nghi-thuc-giao-tiep-v2/img/icon-img-4.png" alt=""
                         class="img-responsive">
                    <span class="title">1 kèm 1</span>
                    <p>với trợ giảng cá nhân</p>
                </li>
            </ul>

            <div class="wrap-box">
                <div class="col-md-4 col-sm-6 col-xs-12 wrap">
                    <div class="box">
                        <div class="media">
                            <div class="media-left">
                                <img src="/mana/cbp/khoa-hoc-nghi-thuc-giao-tiep-v2/img/icon-etiquette-course-1.png"
                                     alt="" class="img-responsive">
                            </div>
                            <div class="media-body">
                                Tầm quan trọng của nghi thức giao tiếp trong kinh doanh
                            </div>
                        </div>
                    </div><!--end .box-->
                </div><!-- .box-->

                <div class="col-md-4 col-sm-6 col-xs-12 wrap">
                    <div class="box">
                        <div class="media">
                            <div class="media-left">
                                <img src="/mana/cbp/khoa-hoc-nghi-thuc-giao-tiep-v2/img/icon-etiquette-course-2.png"
                                     alt="" class="img-responsive">
                            </div>
                            <div class="media-body">
                                Chào hỏi và giới thiệu
                            </div>
                        </div>
                    </div><!--end .box-->
                </div><!-- .box-->

                <div class="col-md-4 col-sm-6 col-xs-12 wrap">
                    <div class="box">
                        <div class="media">
                            <div class="media-left">
                                <img src="/mana/cbp/khoa-hoc-nghi-thuc-giao-tiep-v2/img/icon-etiquette-course-3.png"
                                     alt="" class="img-responsive">
                            </div>
                            <div class="media-body">
                                Tổ chức và tiến hành một buổi họp
                            </div>
                        </div>
                    </div><!--end .box-->
                </div><!-- .box-->

                <div class="col-md-4 col-sm-6 col-xs-12 wrap">
                    <div class="box">
                        <div class="media">
                            <div class="media-left">
                                <img src="/mana/cbp/khoa-hoc-nghi-thuc-giao-tiep-v2/img/icon-etiquette-course-4.png"
                                     alt="" class="img-responsive">
                            </div>
                            <div class="media-body">
                                Đạo đức kinh doanh
                            </div>
                        </div>
                    </div><!--end .box-->
                </div><!-- .box-->

                <div class="col-md-4 col-sm-6 col-xs-12 wrap">
                    <div class="box">
                        <div class="media">
                            <div class="media-left">
                                <img src="/mana/cbp/khoa-hoc-nghi-thuc-giao-tiep-v2/img/icon-etiquette-course-5.png"
                                     alt="" class="img-responsive">
                            </div>
                            <div class="media-body">
                                Nghi thức giao tiếp khi chiêu đãi đối tác
                            </div>
                        </div>
                    </div><!--end .box-->
                </div><!-- .box-->

                <div class="col-md-4 col-sm-6 col-xs-12 wrap">
                    <div class="box">
                        <div class="media">
                            <div class="media-left">
                                <img src="/mana/cbp/khoa-hoc-nghi-thuc-giao-tiep-v2/img/icon-etiquette-course-6.png"
                                     alt="" class="img-responsive">
                            </div>
                            <div class="media-body">
                                Quy tắc ăn uống tại các quốc gia
                            </div>
                        </div>
                    </div><!--end .box-->
                </div><!-- .box-->

                <div class="col-md-4 col-sm-6 col-xs-12 wrap">
                    <div class="box">
                        <div class="media">
                            <div class="media-left">
                                <img src="/mana/cbp/khoa-hoc-nghi-thuc-giao-tiep-v2/img/icon-etiquette-course-7.png"
                                     alt="" class="img-responsive">
                            </div>
                            <div class="media-body">
                                Nghi thức giao tiếp qua điện thoại
                            </div>
                        </div>
                    </div><!--end .box-->
                </div><!-- .box-->

                <div class="col-md-4 col-sm-6 col-xs-12 wrap">
                    <div class="box">
                        <div class="media">
                            <div class="media-left">
                                <img src="/mana/cbp/khoa-hoc-nghi-thuc-giao-tiep-v2/img/icon-etiquette-course-8.png"
                                     alt="" class="img-responsive">
                            </div>
                            <div class="media-body">
                                Quy tắc ứng xử với khách hàng qua điện thoại
                            </div>
                        </div>
                    </div><!--end .box-->
                </div><!-- .box-->

                <div class="col-md-4 col-sm-6 col-xs-12 wrap">
                    <div class="box">
                        <div class="media">
                            <div class="media-left">
                                <img src="/mana/cbp/khoa-hoc-nghi-thuc-giao-tiep-v2/img/icon-etiquette-course-9.png"
                                     alt="" class="img-responsive">
                            </div>
                            <div class="media-body">
                                Quy tắc sử dụng Internet và thư điện tử
                            </div>
                        </div>
                    </div><!--end .box-->
                </div><!-- .box-->

                <div class="col-md-4 col-sm-6 col-xs-12 wrap">
                    <div class="box">
                        <div class="media">
                            <div class="media-left">
                                <img src="/mana/cbp/khoa-hoc-nghi-thuc-giao-tiep-v2/img/icon-etiquette-course-10.png"
                                     alt="" class="img-responsive">
                            </div>
                            <div class="media-body">
                                Cách ăn mặc và tác phong kinh doanh chuyên nghiệp
                            </div>
                        </div>
                    </div><!--end .box-->
                </div><!-- .box-->

                <div class="col-md-4 col-sm-6 col-xs-12 wrap">
                    <div class="box">
                        <div class="media">
                            <div class="media-left">
                                <img src="/mana/cbp/khoa-hoc-nghi-thuc-giao-tiep-v2/img/icon-etiquette-course-11.png"
                                     alt="" class="img-responsive">
                            </div>
                            <div class="media-body">
                                Quy tắc ứng xử với người khuyết tật
                            </div>
                        </div>
                    </div><!--end .box-->
                </div><!-- .box-->

                <div class="col-md-4 col-sm-6 col-xs-12 wrap">
                    <div class="box">
                        <div class="media">
                            <div class="media-left">
                                <img src="/mana/cbp/khoa-hoc-nghi-thuc-giao-tiep-v2/img/icon-etiquette-course-12.png"
                                     alt="" class="img-responsive">
                            </div>
                            <div class="media-body">
                                Môi trường kinh doanh đa văn hóa
                            </div>
                        </div>
                    </div><!--end .box-->
                </div><!-- .box-->

            </div><!--end .wrap-box-->
            <div class="wrap-button">
                <a href="#register" class="scroll button-scroll">Đăng ký nhận học bổng</a>
            </div><!--end .wrap-button-->
        </div><!--end .container-->
    </section>

    <section id="author">
        <div class="top">
            <div class="container">
                <div class="col-sm-5 col-xs-12 img">
                    <img src="/mana/cbp/khoa-hoc-nghi-thuc-giao-tiep-v2/img/nguyenkientri.png" alt="Nguyễn Kiên Trì"
                         class="img-responsive">
                </div><!--end .img-->
                <div class="col-sm-7 col-xs-12 text">
                    <h2>Khóa học được dẫn dắt bởi <br/>Thạc sỹ Nguyễn Kiên Trì</h2>
                    <span class="line"><span></span></span>
                    <ul>
                        <li>MBA Tư vấn Quản lý Quốc tế - Thụy Sỹ</li>
                        <li>Expert Economic Certified Đại học California</li>
                        <li>Nhiều năm kinh nghiệm trong việc quản trị và giảng dạy, hiện đang nắm giữ các vị trí cấp cao
                            tại các tập đoàn lớn.
                        </li>
                    </ul>
                </div><!--end .text-->
            </div><!--end .container-->
        </div><!--end .top-->
        <div class="bottom">
            <div class="container">
                <h2>Chứng chỉ CBP Business Etiquette <br/> Được cấp bởi tổ chức IBTA</h2>
                <span class="line"><span></span></span>
                <div class="col-sm-4 col-xs-12 img">
                    <img src="/mana/cbp/khoa-hoc-nghi-thuc-giao-tiep-v2/img/ibta.png" alt="IBTA" class="img-responsive">
                </div><!--end .img-->
                <div class="col-sm-8 col-xs-12 text">
                    <p>Chứng chỉ CBP của IBTA là một trong những chứng chỉ uy tín nhất về kỹ năng kinh doanh chuyên
                        nghiệp và kỹ năng quản lý.</p>
                    <p>CBP được công nhận trên toàn cầu từ Mỹ, Canada, Caribe, châu Phi, Trung Đông, Trung Quốc, Ấn Độ,
                        đến Singapore và vùng Viễn Đông.</p>
                    <p class="bold">Trên thế giới, các tập đoàn lớn như HONDA, Nokia, Mitsubishi, Intel, Ricoh, adidas,…
                        đều công nhận tiêu chuẩn đánh giá về kỹ năng kinh doanh của IBTA được mô tả qua chứng chỉ
                        CBP.</p>
                    <p><span>Chứng chỉ CBP Business Etiquette<span> chứng nhận cho kỹ năng giao tiếp trong kinh doanh chuẩn quốc tế và được thừa nhận bởi các công ty lớn, tập đoàn đa quốc gia.
                    </p>
                    <div class="wrap-button">
                        <a href="#register" class="scroll button-scroll">Đăng ký nhận học bổng</a>
                    </div><!--end .wrap-button-->
                </div><!--end .text-->
            </div><!--end .container-->
        </div><!--end .bottom-->
    </section>

    <section id="route">
        <div class="container">
            <h2>Lộ trình học và giá trị của chứng chỉ CBP</h2>
            <span class="line"><span></span></span>
            <img src="/mana/cbp/khoa-hoc-nghi-thuc-giao-tiep-v2/img/title.png" alt="" class="img-responsive line">
            <ul class="mb-lotrinh">
                <li>
                    <div class="media">
                        <div class="media-left">
                            <span class="wrap">1</span>
                        </div>
                        <div class="media-body">
                            Tham gia lớp học trực tuyến
                        </div>
                    </div>
                </li>
                <li>
                    <div class="media">
                        <div class="media-left">
                            <span class="wrap">2</span>
                        </div>
                        <div class="media-body">
                            Thảo luận trực tuyến
                        </div>
                    </div>
                </li>
                <li>
                    <div class="media">
                        <div class="media-left">
                            <span class="wrap">3</span>
                        </div>
                        <div class="media-body">
                            Ôn luyện tối đa trong 6 tháng
                        </div>
                    </div>
                </li>
                <li>
                    <div class="media">
                        <div class="media-left">
                            <span class="wrap">4</span>
                        </div>
                        <div class="media-body">
                            Thi trực tuyến qua hệ thống khảo thí Prometric
                        </div>
                    </div>
                </li>
                <li>
                    <div class="media">
                        <div class="media-left">
                            <span class="wrap">5</span>
                        </div>
                        <div class="media-body">
                            Đợi xét duyệt bởi Hiệp hội IBTA
                        </div>
                    </div>
                </li>
                <li>
                    <div class="media">
                        <div class="media-left">
                            <span class="wrap">6</span>
                        </div>
                        <div class="media-body">
                            Nhận chứng chỉ CBP Business Etiquette
                        </div>
                    </div>
                </li>
            </ul>
            <div class="wrap">
                <div class="col-sm-6 col-xs-12 text">
                    <h3>Sở hữu chứng chỉ CBP<br/>Business Etiquette</h3>
                    <ul>
                        <li>
                            <div class="media">
                                <div class="media-left">
                                    <i class="fa fa-check" aria-hidden="true"></i>
                                </div>
                                <div class="media-body">
                                    Bạn sẽ tự tin ứng tuyển vào các công ty, tập đoàn đa quốc gia, tập đoàn nước ngoài.
                                </div>
                            </div>
                        </li>

                        <li>
                            <div class="media">
                                <div class="media-left">
                                    <i class="fa fa-check" aria-hidden="true"></i>
                                </div>
                                <div class="media-body">
                                    Bạn sẽ trở nên chuyên nghiệp khi tham gia vào các buổi tiệc, buổi ký kết với đối
                                    tác.
                                </div>
                            </div>
                        </li>

                        <li>
                            <div class="media">
                                <div class="media-left">
                                    <i class="fa fa-check" aria-hidden="true"></i>
                                </div>
                                <div class="media-body">
                                    Là chứng nhận cho kỹ năng kinh doanh chuyên nghiệp của bạn.
                                </div>
                            </div>
                        </li>

                        <li>
                            <div class="media">
                                <div class="media-left">
                                    <i class="fa fa-check" aria-hidden="true"></i>
                                </div>
                                <div class="media-body">
                                    Là chứng chỉ có giá trị trên toàn cầu.
                                </div>
                            </div>
                        </li>

                        <li>
                            <div class="media">
                                <div class="media-left">
                                    <i class="fa fa-check" aria-hidden="true"></i>
                                </div>
                                <div class="media-body">
                                    Tăng cơ hội thăng tiến trong công việc.
                                </div>
                            </div>
                        </li>
                    </ul>
                </div><!--end .text-->
                <div class="col-sm-6 col-xs-12 img">
                    <img src="/mana/cbp/khoa-hoc-nghi-thuc-giao-tiep-v2/img/route-wrap.png" alt=""
                         class="img-responsive">
                </div><!--end .img-->
            </div><!--end .wrap-->

        </div><!--end .container-->
    </section>

    <section id="register">
        <div class="container">
            <form action="/course/page/submit" name="landing-page-id">
                <input type="hidden" id="advice_name" name="advice_name" value="Mana - Khóa học nghi thức giao tiếp v2" /> 
                <input type="hidden" id="csrf" name="_csrf" />
                <h2><span class="text-transform">Đăng ký nhận tư vấn</span><br/> Nhận ngay <span
                        class="text-transform bold">Học bổng trị giá 1.500.000Đ</span></h2>
                <div class="form-s">
                    <div class="form-group col-md-4 col-xs-12">
                        <div class="input">
                            <label>Họ tên (*)</label>
                            <input type="text" name="fullname" id="fullname" class="input-group form-control" required
                                   placeholder="Họ Và Tên"/>
                        </div>
                    </div>

                    <div class="form-group col-md-4 col-xs-12">
                        <div class="input">
                            <label>Số điện thoại (*)</label>
                            <input type="text" name="phonenumber" id="phonenumber" class="input-group form-control" required
                                   placeholder="Số Điện Thoại"/>
                        </div>
                    </div>
                    <div class="form-group col-md-4 col-xs-12">
                        <div class="input">
                            <label>Email (*)</label>
                            <input type="email" name="email" id="email" class="input-group form-control" required
                                   placeholder="Email"/>
                        </div>
                    </div>
                </div>
                <div class="wrap-button">
                    <button class="scroll btn_box_register button-regis" id="dang_ky_form">Đăng ký nhận học bổng</button>
                </div>
                <p>Mọi thông tin của bạn sẽ được bảo mật hoàn toàn. Sau khi hoàn tất đăng ký, bộ phận CSKH sẽ gọi điện
                    xác nhận và tư vấn kỹ hơn về khóa học với bạn.</p>
            </form>
        </div><!--end .container-->
    </section>

    <section id="about">
        <div class="container">
            <h2>Xin chào, chúng tôi là MANA,<br/> Viện đào tạo Quản trị Kinh doanh trực tuyến hàng đầu Việt Nam.</h2>
            <div class="col-sm-6 col-xs-12 text">
                <ul>
                    <li>MANA mang đến một hệ thống các chương trình đào tạo kỹ năng quản trị kinh doanh có tính ứng dụng
                        cao.
                    </li>
                    <li>Tại MANA, học viên sẽ được học trên các chương trình đào tạo song ngữ cùng đội ngũ cố vấn học
                        tập 24/7.
                    </li>
                    <li>Hình thức và phương pháp học khoa học mang lại cho học viên những trải nghiệm học tập mới mẻ và
                        thú vị.
                    </li>
                    <li>Vào 16.6.2016, Dream Viet Education Corporation (công ty chủ quản của MANA OBS) đã trở thành đối
                        tác chính thức và độc quyền của Hiệp hội IBTA tại Việt Nam. Điều này xác định, MANA là đại diện
                        hợp pháp trong việc phân phối các chương trình Đào tạo kỹ năng Kinh doanh chuẩn quốc tế trên
                        toàn Việt Nam trong 5 năm 2016 - 2021.
                    </li>
                </ul>
            </div><!--end .text-->
            <div class="col-sm-6 col-xs-12 img">
                <img src="/mana/cbp/khoa-hoc-nghi-thuc-giao-tiep-v2/img/about.png" alt="" class="img-responsive">
            </div><!--end .img-->
        </div><!--end .container-->
    </section>

    <section id="partner-logo">
        <div class="container">
            <h2>Báo chí nói về chúng tôi</h2>
            <div class="col-lg-10 col-xs-12 wrap">
                <div class="col-sm-4 col-xs-12 img">
                    <img src="/mana/cbp/khoa-hoc-nghi-thuc-giao-tiep-v2/img/doanhnhan.png" alt="Doanh Nhân"
                         class="img-responsive">
                </div><!--end .img-->

                <div class="col-sm-4 col-xs-12 img">
                    <img src="/mana/cbp/khoa-hoc-nghi-thuc-giao-tiep-v2/img/dantri.png" alt="Dân Trí"
                         class="img-responsive">
                </div><!--end .img-->

                <div class="col-sm-4 col-xs-12 img">
                    <img src="/mana/cbp/khoa-hoc-nghi-thuc-giao-tiep-v2/img/cafebiz.png" alt="" class="img-responsive">
                </div><!--end .img-->
            </div>
        </div><!--end .container-->
    </section>

    <section id="hoi-dap">
        <div class="container">
            <div class="fb-comments fb_iframe_widget fb_iframe_widget_fluid"
                 data-href="https://mana.edu.vn/cbp/khoa-hoc-nghi-thuc-giao-tiep-v2"
                 data-width="100%" data-numposts="10"
                 data-colorscheme="light" fb-xfbml-state="rendered">
            </div>
        </div><!--end .container-->
    </section>
</main>
<!-- footer -->
<footer>
    <div class="container">
        <div class="col-md-4 col-sm-4 col-xs-12 left">
            <a href="https://mana.edu.vn" target="_blank">
                <img src="/mana/cbp/khoa-hoc-nghi-thuc-giao-tiep-v2/img/mana.png" alt="Mana" class="img-responsive">
            </a>
            <a href="https://kyna.vn" target="_blank">
                <img src="/mana/cbp/khoa-hoc-nghi-thuc-giao-tiep-v2/img/kyna.png" alt="kyna" class="img-responsive">
            </a>
        </div>
        <div class="col-md-5 col-sm-5 col-xs-12 mid">
            <p class="comp-name">Công ty Cổ phần Dream Việt Education</p>
            <p><b><u>Trụ sở chính: </u></b>Lầu 6 Tòa nhà Thịnh Phát - 178/8 Đường D1, Phường 25, Quận Bình Thạnh, TP Hồ
                Chí Minh</p>
            <p><b><u>Văn phòng Hà Nội: </u></b>Tầng 6 Tòa nhà Viện Công Nghệ - 25 Vũ Ngọc Phan, Phường Láng Hạ, Quận
                Đống Đa, TP Hà Nội</p>
            <p>
                Giấy phép ĐKKD số 0313589030 do Sở Kế hoạch và Đầu tư TPHCM cấp
            </p>
        </div>
        <div class="col-md-3 col-sm-3 col-xs-12 right">
          <p><b class="hotline">Hotline:</b> 1900 6364 09</p>
          <p><b class="email">Email: </b>hotro@kyna.vn</p>
          <div class="fb-page" data-href="https://www.facebook.com/mana.edu.vn/" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/mana.edu.vn/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/mana.edu.vn/">MANA.edu.vn</a></blockquote></div>
        </div>
    </div>
</footer>


<div class="modal fade" id="modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <div class="popup_body">
                    <div class="alert alert-success" role="alert">Cám ơn bạn đã để lại thông tin. Bộ phận Tư vấn sẽ sớm
                        liên hệ với bạn để tư vấn thêm về chương trình học!
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>


    $(function () {
        $('a[href*=#]:not([href=#])').click(function () {
            if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
                var target = $(this.hash);
                target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                if ($(window).width() > 768) {
                    if (target.length) {
                        $('html,body').animate({
                            scrollTop: target.offset().top - 50
                        }, 1000);
                        return false;
                    }
                } else {
                    if (target.length) {
                        $('html,body').animate({
                            scrollTop: target.offset().top - 50
                        }, 1000);
                        return false;
                    }
                }
            }
        });


        if ($(window).width() > 760) {
            $(window).on('scroll', function () {
                if ($(window).scrollTop() > 50) {
                    $('#wrap-header').addClass('scrolled');
                    $('.navbar').addClass('scrolled');
                    $('.img-logo').addClass('scrolled');
                    $('.navbar-nav').addClass('scrolled');
                } else {
                    $('#wrap-header').removeClass('scrolled');
                    $('.navbar').removeClass('scrolled');
                    $('.img-logo').removeClass('scrolled');
                    $('.navbar-nav').removeClass('scrolled');
                }
            });
        }
    });
</script>

<div id="fb-root"></div>
<script type="text/javascript">
    (function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s);
        js.id = id;
        js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.0&appId=790788601060712";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
</script>

<script type="text/javascript">
    window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
        d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
    _.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
        $.src="//v2.zopim.com/?1bMWnWprkt76mXJXRb6xBuP2dAD7uxPq";z.t=+new Date;$.
            type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
</script>
<!--End of Zopim Live Chat Script-->

<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-M92WKP"
            height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->
<?php $this->endBody() ?>
</body>

<!-- Mirrored from mana.edu.vn/cbp/khoa-hoc-ky-nang-giao-tiep/ by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 27 Apr 2016 13:31:05 GMT -->
</html>
