<?php
use mana\models\Setting;
$dir_name = dirname(dirname(dirname(__FILE__)));
?>
<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from mana.edu.vn/cbp/khoa-hoc-ky-nang-giao-tiep/ by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 27 Apr 2016 13:29:41 GMT -->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Đào tạo Project Management chứng nhận quốc tế - Mana.edu.vn</title>
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,300,500,700' rel='stylesheet' type='text/css'>
    <!-- Bootstrap --> 
    <link href="/mana/cs/khoa-hoc-hoach-dinh-du-an/css/bootstrap.min.css" rel="stylesheet"/>
    <link href="/mana/cs/khoa-hoc-hoach-dinh-du-an/css/site.css" rel="stylesheet"/>
    <link href="/mana/cs/khoa-hoc-hoach-dinh-du-an/css/font-awesome.min.css" rel="stylesheet"/>
    <link href="/mana/cs/khoa-hoc-hoach-dinh-du-an/css/owl.carousel.css" rel="stylesheet"/>
    <link href="/mana/cs/khoa-hoc-hoach-dinh-du-an/css/owl.theme.css" rel="stylesheet"/>
    <link href="/mana/cs/khoa-hoc-hoach-dinh-du-an/css/owl.transitions.css" rel="stylesheet"/>
    <link rel="icon" href="/mana/images/manaFavicon.png">
</head>
<body>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-M92WKP');</script>
<!-- End Google Tag Manager -->

<?php
// get các tham số cần thiết */
$getParams = $_GET;
$utm_source = '';
$utm_medium = '';
$utm_campaign = '';

if (isset($getParams['utm_source']) && trim($getParams['utm_source']) != '') {
    $utm_source = trim($getParams['utm_source']);
}
if (isset($getParams['utm_medium']) && trim($getParams['utm_medium']) != '') {
    $utm_medium = trim($getParams['utm_medium']);
}
if (isset($getParams['utm_campaign']) && trim($getParams['utm_campaign']) != '') {
    $utm_campaign = trim($getParams['utm_campaign']);
}
?>
<nav class="navbar navbar-inverse header-menu navbar-fixed-top scroll-link">
    <div class="container">
        <div class="col-sm-3">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                        data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">
                    <img class="logo-brand" src="/mana/cs/khoa-hoc-hoach-dinh-du-an/imgs/mana.png"/>
                </a>
                <a href="#regis" class="button mb">ĐĂNG KÝ NGAY</a>
            </div>
        </div>
        <div class="col-sm-9 col-xs-12">
            <div id="navbar" class="navbar-collapse collapse">
                <ul class="nav navbar-nav main-menu">
                    <li class="active"><a href="#intro">GIỚI THIỆU</a></li>
                    <li><a href="#ucirvine">ĐƠN VỊ CẤP BẰNG</a></li>
                    <li><a href="#accord">ĐỐI TƯỢNG</a></li>
                    <li><a href="#part">HỌC PHẦN</a></li>
                    <li><a href="#regis">ĐĂNG KÝ</a></li>
                </ul>
            </div>
        </div>
    </div>
</nav>

<section id="banner">
    <div class="wrapper-slider scroll-link">
        <ul>
            <li>
                <div class="item">
                    <img class="img-slider img-responsive" src="/mana/cs/khoa-hoc-hoach-dinh-du-an/imgs/slide/img-1.jpg" alt="First slide"/>
                    <div class="container">
                        <div class="carousel-caption">
                            <h2><img class="logo-slider img-responsive" src="/mana/cs/khoa-hoc-hoach-dinh-du-an/imgs/mana.png"/></h2>
                            <h4>
                                <span class="bold text-transform">"Hoạch định dự án hiệu quả trong 6 tuần"</span><br />Đại học California cấp bằng có <span class="bold">giá trị trên toàn cầu</span></h4>
                            <a class="btn btn-lg btn-primary btn-regis-slider" href="#regis" role="button">ĐĂNG KÝ</a>
                        </div>
                    </div>
                </div><!--end .item-->
            </li>

            <li>
                <div class="item">
                    <img class="img-slider img-responsive" src="/mana/cs/khoa-hoc-hoach-dinh-du-an/imgs/slide/img-2.jpg" alt="Second slide"/>
                    <div class="container">
                        <div class="carousel-caption">
                            <h2><img class="logo-slider img-responsive" src="/mana/cs/khoa-hoc-hoach-dinh-du-an/imgs/mana.png"/></h2>
                            <h4>Đơn vị cấp bằng: <span class="bold text-transform">Đại học California, Irvine Extension</span><br />(top <span class="bold big">50</span> trường Đại học tốt nhất nước Mỹ)</h4>
                            <a class="btn btn-lg btn-primary btn-regis-slider" href="#regis" role="button">ĐĂNG KÝ</a>
                        </div>
                    </div>
                </div><!--end .item-->
            </li>

            <li>
                <div class="item">
                    <img class="img-slider img-responsive" src="/mana/cs/khoa-hoc-hoach-dinh-du-an/imgs/slide/img-3.jpg" alt="First slide"/>
                    <div class="container">
                        <div class="carousel-caption">
                            <h2><img class="logo-slider img-responsive" src="/mana/cs/khoa-hoc-hoach-dinh-du-an/imgs/mana.png"/></h2>
                            <h4>Khóa học <span class="bold">Hoạch định dự án </span>cơ bản phù hợp với nhiều đối tượng. <br />Khóa học được phát triển dựa trên <span class="bold">giáo án chuẩn</span> của Đại học California</h4>
                            <a class="btn btn-lg btn-primary btn-regis-slider" href="#regis" role="button">ĐĂNG KÝ</a>
                        </div>
                    </div>
                </div><!--end .item-->
            </li>

            <li>
                <div class="item">
                    <img class="img-slider img-responsive" src="/mana/cs/khoa-hoc-hoach-dinh-du-an/imgs/slide/img-4.jpg" alt="First slide"/>
                    <div class="container">
                        <div class="carousel-caption">
                            <h2><img class="logo-slider img-responsive" src="/mana/cs/khoa-hoc-hoach-dinh-du-an/imgs/mana.png"/></h2>
                            <h4>Chương trình được phát triển bởi <span class="bold text-transform">Mana Online Business School</span><br />Viện Đào tạo Quản trị Kinh doanh hàng đầu Việt Nam</h4>
                            <a class="btn btn-lg btn-primary btn-regis-slider" href="#regis" role="button">ĐĂNG KÝ</a>
                        </div>
                    </div>
                </div><!--end .item-->
            </li>
        </ul>
    </div><!--end .wrapper-slider-->
</section><!--end #banner-->

<section class="container" id="intro">

    <ul class="wrap-intro-heading">
        <li>
            <div class="line"><hr/></div>
        </li>
        <li>
            <h3 class="intro-heading"> GIỚI THIỆU </h3>
        </li>
        <li>
            <div class="line"><hr/></div>
        </li>
    </ul>

    <div class="row">
        <div class='text-intro'>
            <p>Chương trình <span class="bold">"HOẠCH ĐỊNH DỰ ÁN"</span> (Project Management) được xây dựng dựa trên <span class="bold">giáo trình quốc tế</span> của Đại học California (Mỹ) và <span class="bold">chuẩn giáo trình</span> của Viện Quản lý Dự án (Project Management Institution).</p>
        </div>
    </div>

    <div class="row">
        <div class="text-details clearfix">
            <div class="col-sm-5 col-xs-12">
                <img src="/mana/cs/khoa-hoc-hoach-dinh-du-an/imgs/intro.jpg" class="img-responsive"/>
            </div>
            <div class="col-sm-7 col-xs-12">
                <div class="text-details-content">
                    <p class="text">Chương trình cung cấp kiến thức cùng cách thức <span class="bold">khởi tạo và lên kế hoạch dự án.</span> Nắm rõ cách lên <span class="bold">một bản kế hoạch hoàn chỉnh</span> bao gồm chi tiết cấu trúc dự án, tiến độ công việc, quyền hạn và mối liên hệ giữa các bên liên quan (Stakeholder). Từ đó, giúp người quản lý <span class="bold">kiểm soát được rủi ro</span> và <span class="bold">tăng khả năng thành công</span> của dự án.</p>
                    <ul>
                        <li><p><span class="blue bold">1. </span>Chương trình học <span class="bold">"Quản lý dự án"</span> chuyên sâu và thực tiễn với các bài tập <span class="bold">thực hành xuyên suốt.</span></p></li>
                        <li><p><span class="blue bold">2. </span>Chương trình học <span class="bold">song ngữ Anh – Việt</span> giúp người học theo kịp tiến trình học mà không gặp trở ngại gì.</p></li>
                        <li><p><span class="blue bold">3. </span>Học cùng <span class="bold">chuyên gia từ Đại học California và chuyên gia về Quản lý dự án người Việt.</span></p></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section><!--end #intro-->

<section id="road">
    <div class="container">

        <ul class="wrap-intro-heading">
            <li>
                <div class="line"><hr/></div>
            </li>
            <li>
                <h3 class="intro-heading"> LỘ TRÌNH HỌC TẬP</h3>
            </li>
            <li>
                <div class="line"><hr/></div>
            </li>
        </ul>

        <div class="row">
            <ul>
                <li>
                    <img src="/mana/cs/khoa-hoc-hoach-dinh-du-an/imgs/icon-road-1.png" alt="" class="img-responsive"/>
                    <p>Học bài giảng video</p>
                </li>
                <li>
                    <img src="/mana/cs/khoa-hoc-hoach-dinh-du-an/imgs/icon-road-2.png" alt="" class="img-responsive"/>
                    <p>Bài tập thực hành hàng tuần</p>
                </li>
                <li>
                    <img src="/mana/cs/khoa-hoc-hoach-dinh-du-an/imgs/icon-road-3.png" alt="" class="img-responsive"/>
                    <p>Thảo luận nhóm với các bạn học</p>
                </li>
                <li>
                    <img src="/mana/cs/khoa-hoc-hoach-dinh-du-an/imgs/icon-road-4.png" alt="" class="img-responsive"/>
                    <p>Kiểm tra tiểu luận cuối khóa</p>
                </li>
                <li>
                    <img src="/mana/cs/khoa-hoc-hoach-dinh-du-an/imgs/icon-road-5.png" alt="" class="img-responsive"/>
                    <p>Nộp hồ sơ và nhận bằng quốc tế</p>
                </li>
            </ul>
        </div>
    </div><!--end .container-->
</section><!--end .road -->

<section id="ucirvine">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-sm-5 col-xs-12 img">
                <img src="/mana/cs/khoa-hoc-hoach-dinh-du-an/imgs/ucirvine.png" alt="Ucirvine" class="img-responsive"/>
            </div><!--end .img-->
            <div class="col-md-8 col-sm-7 col-xs-12 text">
                <h4>Bằng và chứng chỉ Quốc tế được cấp bởi</h4>
                <h2 class="text-transform">Đại học California, Irvine extension</h2>
                <p>Top <span class="big">50</span> trường Đại học <span class="big">tốt nhất</span> nước Mỹ</p>
            </div><!--end .text-->
        </div>
    </div><!--end .container-->
</section>

<section id="accord">
    <div class="container">

        <ul class="wrap-intro-heading">
            <li>
                <div class="line"><hr/></div>
            </li>
            <li>
                <h3 class="intro-heading"> KHÓA HỌC NÀY PHÙ HỢP VỚI</h3>
            </li>
            <li>
                <div class="line"><hr/></div>
            </li>
        </ul>

        <div class="row">
            <div class="col-md-4 col-xs-6 box">
                <div class="wrap-img">
                    <img src="/mana/cs/khoa-hoc-hoach-dinh-du-an/imgs/accord-1.png" alt="" class="img-responsive"/>
                    <div class="background"></div>
                </div><!--end .wrap-img-->
                <h4>Sinh viên khối ngành Kinh tế, khối ngành Công nghệ thông tin</h4>
            </div><!--end .box-->

            <div class="col-md-4 col-xs-6 box">
                <div class="wrap-img">
                    <img src="/mana/cs/khoa-hoc-hoach-dinh-du-an/imgs/accord-2.png" alt="" class="img-responsive"/>
                    <div class="background"></div>
                </div><!--end .wrap-img-->
                <h4>Nhân viên Kinh doanh – Marketing – PR</h4>
            </div><!--end .box-->

            <div class="col-md-4 col-xs-6 box">
                <div class="wrap-img">
                    <img src="/mana/cs/khoa-hoc-hoach-dinh-du-an/imgs/accord-3.png" alt="" class="img-responsive"/>
                    <div class="background"></div>
                </div><!--end .wrap-img-->
                <h4>Kỹ sư Công nghệ thông tin</h4>
            </div><!--end .box-->

            <div class="col-md-4 col-xs-6 box">
                <div class="wrap-img">
                    <img src="/mana/cs/khoa-hoc-hoach-dinh-du-an/imgs/accord-4.png" alt="" class="img-responsive"/>
                    <div class="background"></div>
                </div><!--end .wrap-img-->
                <h4>Trưởng/phó phòng Kinh doanh – Marketing – PR</h4>
            </div><!--end .box-->

            <div class="col-md-4 col-xs-6 box">
                <div class="wrap-img">
                    <img src="/mana/cs/khoa-hoc-hoach-dinh-du-an/imgs/accord-5.png" alt="" class="img-responsive"/>
                    <div class="background"></div>
                </div><!--end .wrap-img-->
                <h4>Cấp Giám đốc</h4>
            </div><!--end .box-->

            <div class="col-md-4 col-xs-6 box">
                <div class="wrap-img">
                    <img src="/mana/cs/khoa-hoc-hoach-dinh-du-an/imgs/accord-6.png" alt="" class="img-responsive"/>
                    <div class="background"></div>
                </div><!--end .wrap-img-->
                <h4>Những cá nhân muốn phát triển khả năng quản trị dự án</h4>
            </div><!--end .box-->

        </div><!--end .row-->
    </div><!--end .container-->
</section>


<section class="container study-heading" id="part">

    <ul class="wrap-intro-heading">
        <li>
            <div class="line"><hr/></div>
        </li>
        <li>
            <h3 class="intro-heading"> HỌC PHẦN</h3>
        </li>
        <li>
            <div class="line"><hr/></div>
        </li>
    </ul>

    <div class="row">
        <div class="header-intro-text">
            <div class="col-sm-3">
            </div>
            <div class="col-sm-6">
                <p>QUẢN LÝ DỰ ÁN – HOẠCH ĐỊNH DỰ ÁN HIỆU QUẢ TRONG 6 TUẦN</p>
            </div>
            <div class="col-sm-3">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="content-collapse">

            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingOne">
                        <h4 class="panel-title">
                            <span class="circle-bullet">1</span>
                            <span class="title"><span class="mb">1. </span>KHỞI ĐỘNG</span>
                            <a class="accordion-toggle" role="button" data-toggle="collapse" data-parent="#accordion"
                               href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                            </a>
                        </h4>
                    </div>
                    <div id="collapseOne" class="panel-collapse collapse" role="tabpanel"
                         aria-labelledby="headingOne">
                        <div class="panel-body">
                            <ul>
                                <li>Tìm hiểu tổng quan về chương trình học, các tài liệu cần thiết và tham gia khảo sát đầu khoá học.</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingTwo">
                        <h4 class="panel-title">
                            <span class="circle-bullet">2</span>
                            <span class="title"><span class="mb">2. </span>THẾ NÀO LÀ MỘT DỰ ÁN</span>
                            <a class="accordion-toggle collapsed" role="button" data-toggle="collapse"
                               data-parent="#accordion" href="#collapseTwo" aria-expanded="false"
                               aria-controls="collapseTwo">
                            </a>
                        </h4>
                    </div>
                    <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                        <div class="panel-body">
                            <ul>
                                <li>Tìm hiểu các đặc tính của một dự án, các ràng buộc trong một dự án. Tìm hiểu định nghĩa vai trò, trách nhiệm của người quản trị dự án và cấu trúc tổ chức dự án trong doanh nghiệp/tổ chức.</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingThree">
                        <h4 class="panel-title">
                            <span class="circle-bullet">3</span>
                            <span class="title"><span class="mb">3. </span>TÌM HIỂU VỀ CÁC BÊN LIÊN QUAN (Stakeholders)</span>
                            <a class="accordion-toggle collapsed" role="button" data-toggle="collapse"
                               data-parent="#accordion" href="#collapseThree" aria-expanded="false"
                               aria-controls="collapseThree">
                            </a>
                        </h4>
                    </div>
                    <div id="collapseThree" class="panel-collapse collapse" role="tabpanel"
                         aria-labelledby="headingThree">
                        <div class="panel-body">
                            <ul>
                                <li>Tìm hiểu định nghĩa về các bên liên quan trong một dự án: họ là ai, họ cần thông tin gì, làm sao để quản lý mối quan hệ của họ với dự án.</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingFour">
                        <h4 class="panel-title">
                            <span class="circle-bullet">4</span>
                            <span class="title"><span class="mb">4. </span>QUẢN LÝ PHẠM VI (Scope Management)</span>
                            <a class="accordion-toggle collapsed" role="button" data-toggle="collapse"
                               data-parent="#accordion" href="#collapseFour" aria-expanded="false"
                               aria-controls="collapseFour">
                            </a>
                        </h4>
                    </div>
                    <div id="collapseFour" class="panel-collapse collapse" role="tabpanel"
                         aria-labelledby="headingFou">
                        <div class="panel-body">
                            <ul>
                                <li>Tìm hiểu mục đích của bản tuyên bố dự án (Project charter), tóm tắt các yếu tố chính của một bản kế hoạch dự án. Xác định phạm vi dự án và phương pháp tiếp cận cấu trúc phân chia công việc (WBS – Work Breakdown Structure).</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingFive">
                        <h4 class="panel-title">
                            <span class="circle-bullet">5</span>
                            <span class="title"><span class="mb">5. </span>HOẠCH ĐỊNH VÀ QUẢN LÝ NHÂN SỰ TRONG DỰ ÁN (Human Resource Management)</span>
                            <a class="accordion-toggle collapsed" role="button" data-toggle="collapse"
                               data-parent="#accordion" href="#collapseFive" aria-expanded="false"
                               aria-controls="collapseFive">
                            </a>
                        </h4>
                    </div>
                    <div id="collapseFive" class="panel-collapse collapse" role="tabpanel"
                         aria-labelledby="headingFive">
                        <div class="panel-body">
                            <ul>
                                <li>Hoạch định vai trò, trách nhiệm, quyền hạn của mỗi nhân sự trong dự án và phương án điều hành, quản lý đội thực thi dự án.</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingSix">
                        <h4 class="panel-title">
                            <span class="circle-bullet">6</span>
                            <span class="title"><span class="mb">6. </span>TỔNG KẾT</span>
                            <a class="accordion-toggle collapsed" role="button" data-toggle="collapse"
                               data-parent="#accordion" href="#collapseSix" aria-expanded="false"
                               aria-controls="collapseSix">
                            </a>
                        </h4>
                    </div>
                    <div id="collapseSix" class="panel-collapse collapse" role="tabpanel"
                         aria-labelledby="headingSix">
                        <div class="panel-body">
                            <ul>
                                <li>Tổng kết khoá học bằng cách thảo luận với những chuyên gia quản lý dự án, thi cuối khoá và tham dự vào survey sau khoá học.</li>
                            </ul>
                        </div>
                    </div>
                </div><!--end .panel panel-default-->

            </div><!--end #accordion-->

        </div>
    </div>
</section>

<section id="teacher">
    <div class="container">

        <ul class="wrap-intro-heading">
            <li>
                <div class="line"><hr/></div>
            </li>
            <li>
                <h3 class="intro-heading"> GIẢNG VIÊN</h3>
            </li>
            <li>
                <div class="line"><hr/></div>
            </li>
        </ul>

        <div class="row">
            <div class="col-sm-10 col-sm-offset-1 col-xs-12 wrapper-author">
                <div class="col-md-6 col-xs-12 box first">
                    <img src="/mana/cs/khoa-hoc-hoach-dinh-du-an/imgs/le_van_tien_si.png" alt="" class="img-responsive"/>
                    <div class="wrap-hover">
                        <div class="wrap pc">
                            <h4>Thạc sĩ Lê Văn Tiến Sĩ, PMP</h4>
                            <ul>
                                <li>
                                    <div class="media">
                                      <div class="media-left">
                                        <i class="fa fa-circle" aria-hidden="true"></i>
                                      </div>
                                      <div class="media-body">
                                        Giám Đốc Sản Xuất, Giám Đốc Chất Lượng, Giám Đốc Đào Tạo tại Success Software Services (SSS).
                                      </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="media">
                                      <div class="media-left">
                                        <i class="fa fa-circle" aria-hidden="true"></i>
                                      </div>
                                      <div class="media-body">
                                        Chuyên gia đào tạo quản lý dự án và kỹ năng mềm tại TÜV Rheinland Vietnam.
                                      </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="media">
                                      <div class="media-left">
                                        <i class="fa fa-circle" aria-hidden="true"></i>
                                      </div>
                                      <div class="media-body">
                                        Hơn 15 năm kinh nghiệm trong lĩnh vực quản lý, Thạc sĩ từng là Giám Đốc Điều Hành công ty gia công phần mềm của Úc và giám đốc khu vực Đông Dương và trưởng văn phòng đại diện của NIIT Ấn Độ tại Việt Nam.
                                      </div>
                                    </div>
                                </li>
                            </ul>
                        </div><!--end .wrap-->

                        <div class="wrap mb">
                            <h4>Margaret Meloni</h4>
                            <ul>
                                <li>
                                    <div class="media">
                                      <div class="media-left">
                                        <i class="fa fa-circle" aria-hidden="true"></i>
                                      </div>
                                      <div class="media-body">
                                        Giảng viên Quản lý dự án của UCI Irvine Extension và UCLA Extension là những trường đại học hàng đầu thế giới.
                                      </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="media">
                                      <div class="media-left">
                                        <i class="fa fa-circle" aria-hidden="true"></i>
                                      </div>
                                      <div class="media-body">
                                        Chủ tịch tại Meloni Coaching Solutions chuyên về quản trị dự án.
                                      </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="media">
                                      <div class="media-left">
                                        <i class="fa fa-circle" aria-hidden="true"></i>
                                      </div>
                                      <div class="media-body">
                                        Hơn 18 năm kinh nghiệm trong vai trò quản lý dự án tại nhiều công ty lớn trong đó có Fortune 500.
                                      </div>
                                    </div>
                                </li>
                            </ul>

                        </div><!--end .wrap-->
                        <span class="icon-left"><img src="/mana/cs/khoa-hoc-hoach-dinh-du-an/imgs/icon-box-gv.png" class="img-responsive"/></span>
                    </div><!--end .wrap-hover-->
                </div><!--end .box-->

                <div class="col-md-6 col-xs-12 box last">
                    <img src="/mana/cs/khoa-hoc-hoach-dinh-du-an/imgs/Maraget_Meloni.png" alt="" class="img-responsive"/>
                    <div class="wrap-hover">
                        <div class="wrap pc">
                            <h4>Margaret Meloni, PMP</h4>
                            <ul>
                                <li>
                                    <div class="media">
                                      <div class="media-left">
                                        <i class="fa fa-circle" aria-hidden="true"></i>
                                      </div>
                                      <div class="media-body">
                                        Giảng viên Quản lý dự án của UCI Irvine Extension và UCLA Extension là những trường đại học hàng đầu thế giới.
                                      </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="media">
                                      <div class="media-left">
                                        <i class="fa fa-circle" aria-hidden="true"></i>
                                      </div>
                                      <div class="media-body">
                                        Chủ tịch tại Meloni Coaching Solutions chuyên về quản trị dự án.
                                      </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="media">
                                      <div class="media-left">
                                        <i class="fa fa-circle" aria-hidden="true"></i>
                                      </div>
                                      <div class="media-body">
                                        Hơn 18 năm kinh nghiệm trong vai trò quản lý dự án tại nhiều công ty lớn trong đó có Fortune 500.
                                      </div>
                                    </div>
                                </li>
                            </ul>

                        </div><!--end .wrap-->

                        <div class="wrap mb">
                            <h4>Thạc sĩ Lê Văn Tiến Sĩ, PMP</h4>
                            <ul>
                                <li>
                                    <div class="media">
                                      <div class="media-left">
                                        <i class="fa fa-circle" aria-hidden="true"></i>
                                      </div>
                                      <div class="media-body">
                                        Giám Đốc Sản Xuất, Giám Đốc Chất Lượng, Giám Đốc Đào Tạo tại Success Software Services (SSS).
                                      </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="media">
                                      <div class="media-left">
                                        <i class="fa fa-circle" aria-hidden="true"></i>
                                      </div>
                                      <div class="media-body">
                                        Chuyên gia đào tạo quản lý dự án và kỹ năng mềm tại TÜV Rheinland Vietnam.
                                      </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="media">
                                      <div class="media-left">
                                        <i class="fa fa-circle" aria-hidden="true"></i>
                                      </div>
                                      <div class="media-body">
                                        Hơn 15 năm kinh nghiệm trong lĩnh vực quản lý, Thạc sĩ từng là Giám Đốc Điều Hành công ty gia công phần mềm của Úc và giám đốc khu vực Đông Dương và trưởng văn phòng đại diện của NIIT Ấn Độ tại Việt Nam.
                                      </div>
                                    </div>
                                </li>
                            </ul>
                        </div><!--end .wrap-->
                        <span class="icon-right"><img src="/mana/cs/khoa-hoc-hoach-dinh-du-an/imgs/icon-box-gv.png" class="img-responsive"/></span>
                    </div><!--end .wrap-hover-->
                </div><!--end .box-->
            </div>

        </div><!--end .row-->
    </div><!--end .container-->
</section>

<section id="online">
    <div class="container">

        <ul class="wrap-intro-heading">
            <li>
                <div class="line"><hr/></div>
            </li>
            <li>
                <h3 class="intro-heading"> HỌC ONLINE CÙNG MANA OBS NHẬN CHỨNG CHỈ QUỐC TẾ</h3>
            </li>
            <li>
                <div class="line"><hr/></div>
            </li>
        </ul>

        <div class="row">
            <div class="col-md-4 col-sm-5 col-xs-12 img">
                <img src="/mana/cs/khoa-hoc-hoach-dinh-du-an/imgs/mana-big.png" alt="MANA" class="img-responsive"/>
            </div><!--end .img-->

            <div class="col-md-8 col-sm-7 col-xs-12 text">
                <ul>
                    <li><i class="fa fa-clock-o" aria-hidden="true"></i> Học Online mọi lúc mọi</li>
                    <li><i class="fa fa-globe" aria-hidden="true"></i> Chương trình học song ngữ Anh - Việt</li>
                    <li><i class="fa fa-graduation-cap" aria-hidden="true"></i> Học cùng chuyên gia - thực nghiệm cùng chuyên gia</li>
                    <li><i class="fa fa-certificate" aria-hidden="true"></i> Học tại Việt Nam nhận chứng chỉ Quốc tế</li>
                </ul>
            </div><!--end .text-->
        </div><!--end .row-->
    </div><!--end .container-->
</section>
<section class="registration" id="regis">
    <div class="container resgistration-border">

        <ul class="wrap-intro-heading">
            <li>
                <div class="line"><hr/></div>
            </li>
            <li>
                <h3 class="intro-heading"> ĐĂNG KÝ NHẬN TƯ VẤN</h3>
            </li>
            <li>
                <div class="line"><hr/></div>
            </li>
        </ul>

        <form action="#" class="form-horizontal" id="form_advice" method="post" accept-charset="utf-8">
            <div class="row">

                <div class="text-intro-regist">
                    <p>Bộ phận CSKH của MANA sẽ sớm liên hệ và tư vấn miễn phí về khóa học cho bạn.</p>
                </div>
                <div class="regist-form clearfix">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <input type="text" id="name" class="input-group form-control" required  placeholder="Họ Và Tên"/>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <input type="text" id="phone" class="input-group form-control" required placeholder="Số Điện Thoại"/>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <input type="text" id="email" class="input-group form-control" required  placeholder="Email"/>
                        </div>
                    </div>
                </div>
                <div class="btn-wrap">
                    <div class="button-regist">
                        <button type="submit"  id="dang_ky_form" class="btn btn_box_register">ĐĂNG KÝ</button>
                            <span>
                                <div class="kyna-click-form"></div>
                                <div class="kyna-click-form-fill"></div>
                                <div class="kyna-click-form-img"></div>
                            </span>
                        </button>
                    </div>
                </div><!--end .btn-wrap-->

                <div class="information-text hidden">
                    <p class="text-transform">Giảm ngay <span class="bold blue price big">500.000đ</span> khi đăng ký trước ngày 20/06/2016<br />
                    và tặng ngay học bổng <span class="bold blue price big">500.000đ</span> để trải nghiệm hàng trăm khóa học thú vị trên Kyna.vn</p>
                </div>

            </div>
        </form>
    </div>
</section>
<footer>
    <div class="container footer">
        <div class="row">
            <div class="col-sm-3">
                <div class="footer-logo">
                    <ul>
                        <li>
                            <a href="#">
                                <img src="/mana/cs/khoa-hoc-hoach-dinh-du-an/imgs/mana.png" class="img-responsive"/>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <img src="/mana/cs/khoa-hoc-hoach-dinh-du-an/imgs/18_KynaLogo.png" class="img-responsive"/>
                            </a>
                        </li>
                    </ul>

                </div>
            </div>
            <div class="col-sm-6">
                <div class="footer-text-center">
                    <h4>Công ty Cổ phần  Dream Việt Education</h4>
                    <p> <b class="company-address"> Trụ sở chính:</b> Lầu 6 Tòa nhà Thịnh Phát - 178/8 Đường D1, Phường 25, Quận Bình Thạnh, TP Hồ Chí Minh</p>
                    <p> <b class="company-address"> Văn phòng Hà Nội:</b> Tầng 6, Tòa Nhà Viện Công Nghệ - 25 Vũ Ngọc Phan, Phường Láng Hạ, Quận Đống Đa, TP Hà Nội </p>
                    <p style="padding-top: 20px">Giấy phép ĐKKD số 0313589030 do Sở Kế hoạch và Đầu tư TPHCM cấp</p>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="footer-hot-line">
                    <p><b class="company-address">Hotline:</b>  <?=Setting::HOTLINE ?></p>
                    <p>Thứ 2 – thứ 6: từ 08h30 – 21h00</p>
                    <p>Thứ 7: 08h30 – 17h00</p>
                    <p><b class="company-address">Email:</b> hotro@kyna.vn</p>
                </div>
            </div>
        </div>
    </div>
</footer>
<div class="modal fade" id="modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <div class="popup_body">
                    <div class="alert alert-success" role="alert">Cám ơn bạn đã để lại thông tin. Kyna.vn sẽ sớm liên hệ với bạn để tư vấn thêm về chương trình học!</div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="/mana/cs/khoa-hoc-hoach-dinh-du-an/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/mana/cs/khoa-hoc-hoach-dinh-du-an/js/owl.carousel.min.js"></script>
<script>
$(function(){
  $('.logo-brand').data('size','big');
});

$(window).scroll(function(){
  if($(document).scrollTop() > 0)
{
    if($('.logo-brand').data('size') == 'big')
    {
        $('.logo-brand').data('size','small');
        $('.logo-brand').stop().animate({
            height:'40px'
        },600);
        $('.navbar').addClass('menufix');
    }
}
else
  {
    if($('.logo-brand').data('size') == 'small')
      {
        $('.logo-brand').data('size','big');
        $('.logo-brand').stop().animate({
            height:'50px'
        },600);

        $('.navbar').removeClass('menufix');
      }
  }
});
</script>
<!-- Nhan them vao -->
<script>
    $(document).ready(function(){
        $("#dang_ky_form").bind('click', function(e){

           if($.trim($("#name").val()) != '' && $.trim($("#email").val()) != '' && $.trim($("#phone").val()) != '' ){
                e.preventDefault();
                var url = 'https://docs.google.com/forms/d/1myIiCpaYI4aS9cpNMy1Ff6YBdbsEnI5Yjey71-syuh8/formResponse';
               var data = {'entry.1051362568': $("#name").val(),
                   'entry.2075734835': $("#email").val(),
                   'entry.332608401': $("#phone").val(),
                   'entry.1984477227': '<?php echo $utm_source; ?>',
                   'entry.187100342':'<?php echo $utm_medium;  ?>',
                   'entry.927267113':'<?php echo $utm_campaign;  ?>'
               };
                $.ajax({
                    'url': url,
                    'method': 'POST',
                    'dataType': 'XML',
                    'data': data,
                    'statusCode': {
                        0: function () {
                            $("#name").val('');
                            $("#email").val('');
                            $("#phone").val('');
                            $("#modal").modal();
                        },
                        200: function () {
                            $("#name").val('');
                            $("#email").val('');
                            $("#phone").val('');
                            $("#modal").modal();
                        }
                    }

                });
           }

        });

        $(".wrapper-slider ul").owlCarousel({
    		autoPlay: true,
    		items : 1,
    		itemsDesktop : [1199,1],
    		itemsDesktopSmall:	[979,1],
    		itemsTablet:	[768,1],
    		itemsMobile:	[479,1],
    		mouseDrag : true,
            autoPlay: 40000,
    		navigation: false,
    		pagination : true,
    	});
    });


    $(function() {
    	  $('.scroll-link a[href*=#]:not([href=#])').click(function() {
    	    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {

    	      var target = $(this.hash);
    	      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
				if($(window).width() > 768){
					if (target.length) {
						$('html,body').animate({
						  scrollTop: target.offset().top - 70
						}, 1000);
						return false;
					}
				} else {
					if (target.length) {
						$('html,body').animate({
						  scrollTop: target.offset().top
						}, 1000);
						return false;
					}
				}
            }
    	  });
    	});

        /* RESPONSIVE EQUAL HEIGHT BLOCKS */
    ;( function( $, window, document, undefined )
    {
        'use strict';

        var $list       = $( '#accord' ),
            $items      = $list.find( '.box' ),
            setHeights  = function()
            {
                $items.css( 'height', 'auto' );

                var perRow = Math.floor( $list.width() / $items.width() );
                if( perRow == null || perRow < 2 ) return true;

                for( var i = 0, j = $items.length; i < j; i += perRow )
                {
                    var maxHeight   = 0,
                        $row        = $items.slice( i, i + perRow );

                    $row.each( function()
                    {
                        var itemHeight = parseInt( $( this ).outerHeight() );
                        if ( itemHeight > maxHeight ) maxHeight = itemHeight;
                    });
                    $row.css( 'height', maxHeight );
                }
            };

        setHeights();
        $( window ).on( 'resize', setHeights );
        $list.find( 'img' ).on( 'load', setHeights );

    })( jQuery, window, document );


    $( "#accord .wrap-img" ).hover(
          function() {
            $( this ).addClass( "hover" );
          }, function() {
            $( this ).removeClass( "hover" );
          }
    );

    $( "#teacher .box.first" ).hover(
          function() {
            $( "#teacher .box.last" ).addClass( "hover" );
          }, function() {
            $( "#teacher .box.last" ).removeClass( "hover" );
          }
    );

    $( "#teacher .box.last" ).hover(
          function() {
            $( "#teacher .box.first" ).addClass( "hover" );
          }, function() {
            $( "#teacher .box.first" ).removeClass( "hover" );
          }
    );

</script>
<!--End of Zopim Live Chat Script-->
<!--Start of Zopim Live Chat Script-->
<script type="text/javascript">
    window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
        d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
    _.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
        $.src="//v2.zopim.com/?1bMWnWprkt76mXJXRb6xBuP2dAD7uxPq";z.t=+new Date;$.
            type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
</script>
<!--End of Zopim Live Chat Script-->

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-M92WKP"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
</body>

</html>
