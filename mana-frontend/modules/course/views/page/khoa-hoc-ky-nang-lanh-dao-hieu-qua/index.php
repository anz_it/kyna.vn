<?php
use mana\models\Setting;
$dir_name = dirname(dirname(dirname(__FILE__)));
?>
<!DOCTYPE html>
<html lang="en">

    <!-- Mirrored from mana.edu.vn/cbp/khoa-hoc-ky-nang-giao-tiep/ by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 27 Apr 2016 13:29:41 GMT -->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Đào tạo kỹ năng lãnh đạo hiệu quả - Mana.edu.vn</title>
        <link href='https://fonts.googleapis.com/css?family=Roboto:400,300,500,700' rel='stylesheet' type='text/css'>
        <!-- Bootstrap -->
        <link href="/mana/cbp/khoa-hoc-ky-nang-lanh-dao-hieu-qua/css/bootstrap.min.css" rel="stylesheet"/>
        <link href="/mana/cbp/khoa-hoc-ky-nang-lanh-dao-hieu-qua/css/site.css" rel="stylesheet"/>
        <link href="/mana/cbp/khoa-hoc-ky-nang-lanh-dao-hieu-qua/css/owl.carousel.css" rel="stylesheet"/>
        <link href="/mana/cbp/khoa-hoc-ky-nang-lanh-dao-hieu-qua/css/owl.theme.css" rel="stylesheet"/>
        <link href="/mana/cbp/khoa-hoc-ky-nang-lanh-dao-hieu-qua/css/owl.transitions.css" rel="stylesheet"/>
        <link rel="icon" href="/mana/images/manaFavicon.png">

    </head>
    <body>
        <!-- Google Tag Manager -->
        <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-M92WKP');</script>
<!-- End Google Tag Manager -->
        <!-- End Google Tag Manager -->
        <?php
        // get các tham số cần thiết */
        $getParams = $_GET;
        $utm_source = '';
        $utm_medium = '';
        $utm_campaign = '';

        if (isset($getParams['utm_source']) && trim($getParams['utm_source']) != '') {
            $utm_source = trim($getParams['utm_source']);
        }
        if (isset($getParams['utm_medium']) && trim($getParams['utm_medium']) != '') {
            $utm_medium = trim($getParams['utm_medium']);
        }
        if (isset($getParams['utm_campaign']) && trim($getParams['utm_campaign']) != '') {
            $utm_campaign = trim($getParams['utm_campaign']);
        }
        ?>
        <nav class="navbar navbar-inverse header-menu navbar-fixed-top scroll-link">
            <div class="container">
                <div class="col-sm-3">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                                data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="#">
                            <img class="logo-brand" src="/mana/cbp/khoa-hoc-ky-nang-lanh-dao-hieu-qua/imgs/1_Logo.png"/>
                        </a>
                        <a href="#regis" class="button mb">ĐĂNG KÝ NGAY</a>
                    </div>
                </div>
                <div class="col-sm-9 col-xs-12">
                    <div id="navbar" class="navbar-collapse collapse">
                        <ul class="nav navbar-nav main-menu">
                            <li class="active"><a href="#intro">GIỚI THIỆU</a></li>
                            <li><a href="#part">HỌC PHẦN</a></li>
                            <li><a href="#method">HÌNH THỨC HỌC</a></li>
                            <li><a href="#teacher">GIẢNG VIÊN</a></li>
                            <li><a href="#regis">ĐĂNG KÝ</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </nav>

        <div id="banner">
            <div class="wrapper-slider scroll-link">
                <ul>
                    <li>
                        <div class="item">
                            <img class="img-slider img-responsive" src="/mana/cbp/khoa-hoc-ky-nang-lanh-dao-hieu-qua/imgs/1_BG/1_BG1.png" alt="First slide"/>
                            <div class="container">
                                <div class="carousel-caption">
                                    <img class="logo-slider img-responsive" src="/mana/cbp/khoa-hoc-ky-nang-lanh-dao-hieu-qua/imgs/1_Logo.png"/>
                                    <p>Chương trình <span class="bold">"Đào tạo kỹ năng lãnh đạo hiệu quả"</span><br /> do Hiệp hội doanh nhân Quốc Tế cấp chứng chỉ</p>
                                    <a class="btn btn-lg btn-primary btn-regis-slider" href="#regis" role="button">ĐĂNG KÝ</a>
                                </div>
                            </div>
                        </div><!--end .item-->
                    </li>

                    <li>
                        <div class="item">
                            <img class="img-slider img-responsive" src="/mana/cbp/khoa-hoc-ky-nang-lanh-dao-hieu-qua/imgs/1_BG/4_Slide2.png" alt="Second slide"/>
                            <div class="container">
                                <div class="carousel-caption">
                                    <img class="logo-slider img-responsive" src="/mana/cbp/khoa-hoc-ky-nang-lanh-dao-hieu-qua/imgs/1_Logo.png"/>
                                    <p>Nhận bằng CBP Leadership do Hiệp hội<br /> doanh nhân Quốc Tế cấp – Công nhận quốc tế</p>
                                    <a class="btn btn-lg btn-primary btn-regis-slider" href="#regis" role="button">ĐĂNG KÝ</a>
                                </div>
                            </div>
                        </div><!--end .item-->
                    </li>

                    <li>
                        <div class="item">
                            <img class="img-slider img-responsive" src="/mana/cbp/khoa-hoc-ky-nang-lanh-dao-hieu-qua/imgs/1_BG/5_Slide3.png" alt="Third slide"/>
                            <div class="container">
                                <div class="carousel-caption">
                                    <img class="logo-slider img-responsive" src="/mana/cbp/khoa-hoc-ky-nang-lanh-dao-hieu-qua/imgs/1_Logo.png"/>
                                    <p>Học online mọi lúc mọi nơi cùng doanh nhân hàng đầu</p>
                                    <a class="btn btn-lg btn-primary btn-regis-slider" href="#regis" role="button">ĐĂNG KÝ</a>
                                </div>
                            </div>
                        </div><!--end .item-->
                    </li>


                    <li>
                        <div class="item">
                            <img class="img-slider img-responsive" src="/mana/cbp/khoa-hoc-ky-nang-lanh-dao-hieu-qua/imgs/1_BG/6_Slide4.png" alt="Third slide"/>
                            <div class="container">
                                <div class="carousel-caption">
                                    <img class="logo-slider img-responsive" src="/mana/cbp/khoa-hoc-ky-nang-lanh-dao-hieu-qua/imgs/1_Logo.png"/>
                                    <p>MANA – Học viện đào tạo quản trị kinh doanh trực tuyến<br /> hàng đầu Việt Nam</p>
                                    <a class="btn btn-lg btn-primary btn-regis-slider" href="#regis" role="button">ĐĂNG KÝ</a>
                                </div>
                            </div>
                        </div><!--item-->
                    </li>
                </ul>
            </div><!--end .wrapper-slider-->
        </div><!--end #banner-->

        <div class="container" id="intro">
            <div class="row">
                <div class="col-md-4 col-sm-3"></div>
                <div class="col-md-1 col-sm-1 line"><hr/></div>
                <div class="col-md-2 col-sm-4"><h3 class="intro-heading"> GIỚI THIỆU </h3></div>
                <div class="col-md-1 col-sm-1 line"><hr/></div>
                <div class="col-md-4 col-sm-3"></div>

            </div>

            <div class="row">
                <div class='text-intro'>
                    <p>Chương trình <b class="main-color">“Đào tạo kỹ năng lãnh đạo hiệu quả”</b> được thiết kế, giảng dạy
                        bởi các chuyên gia tại Học viện đào tạo quản trị kinh doanh trực tuyến MANA (MANA business school) và được
                        Hiệp Hội Đào tạo Kinh doanh Quốc Tế (International Business Training Association) công nhận. Sau khi hoàn
                        thành đầy đủ các học phần và bài kiểm tra trực tuyến, học viên sẽ được cấp chứng chỉ CBP Leadership, được công nhận toàn cầu.</p>
                </div>
            </div>

            <div class="row circle-content">
                <div class="col-lg-4 col-sm-6 col-xs-12">
                    <div class="circle circle-first">
                        <div class="circle-text">
                            <p>KHÓA HỌC ĐÀO <br />TẠO KỸ NĂNG <br/> LÃNH ĐẠO <br />HIỆU QUẢ</p>
                        </div>
                    </div>
                    <div class="arrow-circle">
                    </div>
                </div><!--end .col-sm-4-->

                <div class="col-lg-3 col-sm-6 col-xs-12">
                    <div class="circle circle-middle">
                        <div class="circle-text">
                            <p>THI TRỰC TUYẾN</p>
                        </div>
                    </div>
                </div><!--end .col-sm-3-->

                <div class="col-lg-1 col-xs-12">
                    <div class="arrow-circle arrow-circle-second"></div>
                </div><!--end .col-sm-1-->

                <div class="col-lg-4 col-sm-12 col-xs-12">
                    <div class="circle circle-last">
                        <div class="circle-text">
                            <p>CBP<br />LEADERSHIP</p>
                        </div>
                    </div>
                </div><!--end .col-sm-4-->

            </div>
            <div class="row">
                <div class="text-details clearfix">
                    <div class="col-sm-7">
                        <img src="/mana/cbp/khoa-hoc-ky-nang-lanh-dao-hieu-qua/imgs/1_BG/2_BG2.png" class="img-responsive"/>
                    </div>
                    <div class="col-sm-5">
                        <div class="text-details-content">
                            <p class="text">Chương trình “Đào tạo kỹ năng lãnh đạo hiệu quả” của MANA business school sẽ cung cấp và rèn luyện cho bạn đầy đủ các <strong>kiến thức và kỹ năng tổ chức, xây dựng đội nhóm</strong>.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div><!--end #intro-->

        <div class="section-desc" id="term">
            <div class="section-desc-text-inner">
                <p>
                    Chương trình phù hợp với: Những chuyên viên, nhân viên văn phòng có nhu cầu rèn luyện kỹ năng và chuyên môn để trở thành trưởng nhóm. Đặc biệt phù hợp với những bạn đang làm việc trong lĩnh vực sales, marketing, phát triển kinh doanh.
                </p>
            </div><!--end .section-desc-text-inner-->
        </div><!--end .section-desc-->

        <div class="container study-heading" id="part">
            <div class="row">
                <div class="col-md-4 col-sm-3"></div>
                <div class="col-md-1 col-sm-1 line"><hr/></div>
                <div class="col-md-2 col-sm-4"><h3 class="intro-heading"> HỌC PHẦN </h3></div>
                <div class="col-md-1 col-sm-1 line"><hr/></div>
                <div class="col-md-4 col-sm-3"></div>
            </div>
            <div class="row">
                <div class="header-intro-text">
                    <div class="col-sm-3">
                    </div>
                    <div class="col-sm-6">
                        <p>Để trở thành một nhà lãnh đạo tài năng, bạn cần rèn luyện các kiến thức và kỹ năng sau:</p>
                    </div>
                    <div class="col-sm-3">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="content-collapse">
                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingOne">
                                <h4 class="panel-title">
                                    <span class="circle-bullet">1</span>
                                    <span class="title">Giới thiệu về lãnh đạo hiệu quả</span>
                                    <a class="accordion-toggle" role="button" data-toggle="collapse" data-parent="#accordion"
                                       href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel"
                                 aria-labelledby="headingOne">
                                <div class="panel-body">
                                    <ul>
                                        <li>Định nghĩa về lãnh đạo</li>
                                        <li>Định nghĩa về nhà lãnh đạo</li>
                                        <li>Định nghĩa về người theo sau</li>
                                        <li>Lãnh đạo hiệu quả</li>
                                        <li>Kỹ năng, tài năng được phát triển hay khả năng</li>
                                        <li>Trách nhịêm của một nhà lãnh đạo</li>
                                        <li>Phát triển tầm nhìn</li>
                                        <li>Phát triển sứ mệnh và mục đích</li>
                                        <li>Làm việc hướng đến việc đạt được mục đích và mục tiêu</li>
                                        <li>Xây dựng một đội ngũ gắn kết</li>
                                        <li>Xác định nhu cầu và đạt nhu cầu của đội nhóm</li>
                                        <li>Đo lường mức độ hoàn thành công việc của đội nhóm</li>
                                        <li>Giao trách nhiệm cho thành viên trong đội</li>
                                        <li>Khuyến khích các thành viên trong đội</li>
                                        <li>Tiềm năng lãnh đạo</li>
                                        <li>Ai cũng có thể là một nhà lãnh đạo</li>
                                        <li>Các tình huống tạo nên và hình thành các nhà lãnh đạo</li>
                                        <li>Những nhà lãnh đạo nắm giữ trách nhiệm</li>
                                        <li>Những điều cần phải có để trở thành một nhà lãnh đạo hiệu quả?</li>
                                        <li>Các mục đích rõ ràng</li>
                                        <li>Đào tạo</li>
                                        <li>Sự khác biệt giữa việc lãnh đạo và quản lý</li>
                                        <li>Sự khác biệt giữa lãnh đạo và quản lý</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingTwo">
                                <h4 class="panel-title">
                                    <span class="circle-bullet">2</span>
                                    <span class="title">Chọn phong cách lãnh đạo thích hợp</span>
                                    <a class="accordion-toggle collapsed" role="button" data-toggle="collapse"
                                       data-parent="#accordion" href="#collapseTwo" aria-expanded="false"
                                       aria-controls="collapseTwo">
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                <div class="panel-body">
                                    <ul>
                                        <li>Bản chất quá độ của lãnh đạo</li>
                                        <li>Điều gì sẽ xảy ra khi đạt được các mục tiêu chính?</li>
                                        <li>Những tình huống có thể thay đổi</li>
                                        <li>Các phong cách lãnh đạo</li>
                                        <li>Hỗ trợ liên quan</li>
                                        <li>Hỗ trợ chức năng</li>
                                        <li>Người theo sau</li>
                                        <li>Lãnh đạo tình huống</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingThree">
                                <h4 class="panel-title">
                                    <span class="circle-bullet">3</span>
                                    <span class="title">Phát triển tầm nhìn và sứ mệnh</span>
                                    <a class="accordion-toggle collapsed" role="button" data-toggle="collapse"
                                       data-parent="#accordion" href="#collapseThree" aria-expanded="false"
                                       aria-controls="collapseThree">
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseThree" class="panel-collapse collapse" role="tabpanel"
                                 aria-labelledby="headingThree">
                                <div class="panel-body">
                                    <ul>
                                        <li>Tầm nhìn</li>
                                        <li>Định nghĩa</li>
                                        <li>Mục đích</li>
                                        <li>Phương hướng và đích đến</li>
                                        <li>Niềm đam mê</li>
                                        <li>Các giá trị</li>
                                        <li>Các nguyên tắc chỉ đạo và tiêu chuẩn</li>
                                        <li>Kế họach tầm nhìn</li>
                                        <li>Sứ mệnh</li>
                                        <li>Mục đích</li>
                                        <li>Mục tiêu</li>
                                        <li>Kế hoạch</li>
                                        <li>Kế hoạch sứ mệnh</li>
                                        <li>Nguyên tắc chỉ đạo phát triển 1 kế họach sứ mệnh</li>
                                        <li>Truyền thông và tầm nhìn</li>
                                        <li>Phát triển một tầm nhìn rõ ràng</li>
                                        <li>Văn hóa tầm nhìn</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingFour">
                                <h4 class="panel-title">
                                    <span class="circle-bullet">4</span>
                                    <span class="title">Việc ra quyết định hiệu quả</span>
                                    <a class="accordion-toggle collapsed" role="button" data-toggle="collapse"
                                       data-parent="#accordion" href="#collapseFour" aria-expanded="false"
                                       aria-controls="collapseFour">
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseFour" class="panel-collapse collapse" role="tabpanel"
                                 aria-labelledby="headingFour">
                                <div class="panel-body">
                                    <ul>
                                        <li>Ra quyết định hiệu quả</li>
                                        <li>Thiết lập tiêu chuẩn</li>
                                        <li>Đánh giá tiêu chuẩn</li>
                                        <li>Xác định vấn đề và phân tích</li>
                                        <li>Giải quyết vấn đề</li>
                                        <li>Nhìn nhận về việc giải quyết một vấn đề</li>
                                        <li>Giải quyết vấn đề</li>
                                        <li>Thực hiện</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingFive">
                                <h4 class="panel-title">
                                    <span class="circle-bullet">5</span>
                                    <span class="title">Xây dựng đội ngũ cho những nhà lãnh đạo</span>
                                    <a class="accordion-toggle collapsed" role="button" data-toggle="collapse"
                                       data-parent="#accordion" href="#collapseFive" aria-expanded="false"
                                       aria-controls="collapseFive">
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseFive" class="panel-collapse collapse" role="tabpanel"
                                 aria-labelledby="headingFive">
                                <div class="panel-body">
                                    <ul>
                                        <li>Xây dựng đội ngũ</li>
                                        <li>Sự khác biệt giữa nhóm và đội ngũ</li>
                                        <li>Lãnh đạo một đội ngũ những nhà lãnh đạo</li>
                                        <li>Trách nhiệm của trưởng nhóm</li>
                                        <li>Sứ mệnh, mục đích và mục tiêu</li>
                                        <li>Tiêu chuẩn lựa chọn thành viên</li>
                                        <li>Truyền đạt trách nhiệm của các thành viên trong đội</li>
                                        <li>Đạt nhu cầu của đội ngũ</li>
                                        <li>Động lực</li>
                                        <li>Trách nhiệm</li>
                                        <li>Lợi ích của việc xây dựng đội ngũ</li>
                                        <li>Sở hữu</li>
                                        <li>Sự ủy quyền</li>
                                        <li>Sự công nhận</li>
                                        <li>Tiếp cận thành công và thất bại lựa chọn đội ngũ</li>
                                        <li>Chức năng các thành viên trong đội năng lực</li>
                                        <li>Sự cam kết</li>
                                        <li>Xác định các nhu cầu đào tạo</li>
                                        <li>Lợi ích của sự đa dạng hóa</li>
                                        <li>Quy mô</li>
                                        <li>Thông tin trong đội ngũ</li>
                                        <li>Các mục đích được thể hiện rõ ràng thúc đẩy đội ngũ</li>
                                        <li>Tạo ra môi trường học tập – 4 nhu cầu cơ bản</li>
                                        <li>Sự tương thích</li>
                                        <li>Sự tự quản</li>
                                        <li>Sự an toàn</li>
                                        <li>Sự gắn kết</li>
                                        <li>Hướng dẫn các đội ngũ</li>
                                        <li>Định nghĩa của việc huấn luyện</li>
                                        <li>Phát triển một quy trình huấn luyện</li>
                                        <li>Phát triển một quy trình huấn luyện như một nhà lãnh đạo</li>
                                        <li>Bạn muốn trở thành người như thế nào và ở đâu?</li>
                                        <li>Vì sao điều này quan trọng?</li>
                                        <li>Các hành động để bạn đạt đến đó là gì</li>
                                        <li>Bạn vẫn đang theo đúng lộ trình?</li>
                                        <li>Những thay đổi cần thiết để tiếp tục theo đúng lộ trình</li>
                                        <li>Hãy hỗ trợ lẫn nhau</li>
                                        <li>Một quy trình liên tục</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingSix">
                                <h4 class="panel-title">
                                    <span class="circle-bullet">6</span>
                                    <span class="title">Động lực</span>
                                    <a class="accordion-toggle collapsed" role="button" data-toggle="collapse"
                                       data-parent="#accordion" href="#collapseSix" aria-expanded="false"
                                       aria-controls="collapseSix">
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseSix" class="panel-collapse collapse" role="tabpanel"
                                 aria-labelledby="headingSix">
                                <div class="panel-body">
                                    <ul>
                                        <li>Động lực</li>
                                        <li>Định nghĩa động lực</li>
                                        <li>Chuyển sang hành động</li>
                                        <li>Mong muốn và nhu cầu</li>
                                        <li>Khuyến khích hoàn thành</li>
                                        <li>Tinh thần</li>
                                        <li>Cải thiện tinh thần</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="teacher" id="teacher">
            <div class="container">
                <div class="row title">
                    <div class="col-md-4 col-sm-3"></div>
                    <div class="col-md-1 col-sm-1 line"><hr/></div>
                    <div class="col-md-2 col-sm-4"><h3 class="intro-heading"> GIẢNG VIÊN </h3></div>
                    <div class="col-md-1 col-sm-1 line"><hr/></div>
                    <div class="col-md-4 col-sm-3"></div>
                </div>
                <div class="wrap-content">
                    <div class="col-sm-4 col-xs-12 img">
                        <img src="/mana/cbp/khoa-hoc-ky-nang-lanh-dao-hieu-qua/imgs/hothithanhvan.png" alt="Thạc sỹ Hồ Thị Thanh Vân" class="img-responsive">
                    </div><!--end .video-->
                    <div class="col-sm-8 col-xs-12 content">
                        <h3>Thạc sỹ <span>Hồ Thị Thanh Vân (Vân Hồ)</span></h3>
                        <ul>
                            <li><span>&#45;</span> Được đào tạo Cao học chuyên ngành Marketing Bán hàng và Dịch vụ (MMSS) tại trường Đại học Paris 1 Sorbonne (IAE) phối hợp với trường Quản trị Châu Âu (ESCP - EAP European School Management).</li>
                            <li><span>&#45;</span> 20 năm kinh nghiệm trong lĩnh vực Sales và Marketing tại các công ty hàng đầu thế giới: Giám đốc Ngành hàng kiêm Lãnh đạo thị trường Cambodia/Laos - Novartis Pharma, Giám đốc Điều hành Tiếp thị - AstraZeneca, Giám đốc Marketing - Sanofi, Giám đốc Thương hiệu - Abbott, Giám đốc Kinh doanh - GlaxoSmithKline.</li>
                            <li><span>&#45;</span> Dành nhiều giải thưởng danh giá:
                                Giải thưởng của Phó Chủ tịch tập đoàn Khu vực Châu Á, Giải thưởng Khu vực International cho thành tích xuất sắc trong việc giới thiệu thành công sản phẩm mới ra thị trường (AstraZeneca); Giải thưởng Quán quân Marketing cho Chương trình Marketing xuất sắc (Abbott); Giải thưởng Cầu vồng dành cho Lãnh đạo xuất sắc và Giải thưởng Thành tích Kinh doanh xuất sắc trong 4 năm liên tiếp (GlaxoSmithKline).</li>
                        </ul>
                    </div><!--end .content-->
                </div><!--end .content-->
            </div><!--end .container-->
        </div>
        <div class="study-method" id="method">
            <div class="container">
                <div class="row">
                    <div class="col-sm-3">
                    </div>
                    <div class="col-sm-1 line">
                        <hr/>
                    </div>
                    <div class="col-sm-4">
                        <h3 class="intro-heading"> HÌNH THỨC HỌC </h3>
                    </div>
                    <div class="col-sm-1 line">
                        <hr/>
                    </div>
                    <div class="col-sm-3">
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-3">
                        <div class="circle-method circle-calendar"><i class="icon-calendar"></i> </div>
                        <div class="study-method-text">
                            <h4>Học mọi lúc mọi nơi</h4>
                            <p>Học qua video bài giảng được biên tập chuyên nghiệp.  Tài liệu bao gồm sách và bài thuyết trình, tư liệu tham khảo</p>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="circle-method circle-question"><i class="icon-question"></i></div>
                        <div class="study-method-text">
                            <h4>Hỏi đáp cùng chuyên gia</h4>
                            <p>Cố vấn 1-2-1 (1 kèm 1) trong suốt quá trình học và luyện thi. Hỏi đáp cùng chuyên gia, doanh nhân có kinh nghiệm</p>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="circle-method circle-pencil"><i class="icon-pencil"></i></div>
                        <div class="study-method-text">
                            <h4>Hỏi đáp và luyện thi</h4>
                            <p>Học đi đôi với hành trong suốt quá trình học. Rèn luyện cùng hơn 100 bài thi thử</p>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="circle-method circle-house"><i class="icon-house"></i></div>
                        <div class="study-method-text">
                            <h4>Thi trực tuyến và nhận bằng quốc tế</h4>
                            <p>Thi trực tuyến mọi lúc mọi nơi. 6 tháng để rèn luyện thoải mái trước khi thi</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="registration" id="regis">
            <div class="container resgistration-border">
                <div class="row">
                    <div class="col-sm-3">
                    </div>
                    <div class="col-sm-1 line">
                        <hr/>
                    </div>
                    <div class="col-sm-4">
                        <h3 class="intro-heading"> ĐĂNG KÝ NHẬN TƯ VẤN </h3>
                    </div>
                    <div class="col-sm-1 line">
                        <hr/>
                    </div>
                    <div class="col-sm-3">
                    </div>
                </div>
                <form action="" class="form-horizontal" id="form_advice" accept-charset="utf-8">
                    <div class="row">

                        <div class="text-intro-regist">
                            <br/>
                            <p class="text">HỌC PHÍ: 4.700.000Đ (Bao gồm học liệu Online, sách và chi phí dự thi CBP)</p>
                            <p class="text-last"><span>HỌC BỔNG DÀNH CHO 99 NGƯỜI ĐĂNG KÝ ĐẦU TIÊN: 1.000.000Đ</span></p>
                        </div>
                        <div class="regist-form">
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <input type="text" name="name" id="name" class="input-group form-control" required  placeholder="Họ Và Tên"/>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <input type="text" name="phone" id="phone" class="input-group form-control" required placeholder="Số Điện Thoại"/>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <input type="email" name="email" id="email" class="input-group form-control" required  placeholder="Email"/>
                                </div>
                            </div>
                        </div>
                        <div class="button-regist">
                            <button id="dang_ky_form"  class="btn btn-primary btn-regist button-regis">ĐĂNG KÝ</button>
                        </div>
                        <div class="information-text">
                            <p>Bộ phận chăm sóc khách hàng của MANA Business School sẽ liên lạc sớm với bạn để tư vấn về khóa học (mức phí tư vấn là 0đ).</p>
                        </div>

                    </div>
                </form>
            </div>
        </div>
        <footer>
            <div class="container footer">
                <div class="row">
                    <div class="col-sm-3">
                        <div class="footer-logo">
                            <ul>
                                <li>
                                    <a href="#">
                                        <img src="/mana/cbp/khoa-hoc-ky-nang-lanh-dao-hieu-qua/imgs/1_Logo.png" class="img-responsive"/>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <img src="/mana/cbp/khoa-hoc-ky-nang-lanh-dao-hieu-qua/imgs/18_KynaLogo.png" class="img-responsive"/>
                                    </a>
                                </li>
                            </ul>

                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="footer-text-center">
                            <h4>Công ty Cổ phần  Dream Việt Education</h4>
                            <p> <b class="company-address"> Địa chỉ ĐKKD:</b>  <?php echo Setting::DIA_CHI_DKKD ?></p>
                            <p> <b class="company-address"> Văn phòng Hồ Chí Minh:</b>  <?php echo Setting::DIA_CHI_VAN_PHONG ?></p>
                            <p> <b class="company-address"> Văn phòng Hà Nội:</b>  <?php echo Setting::DIA_CHI_VAN_PHONG_HA_NOI ?> </p>
                            <p style="padding-top: 20px">Giấy phép ĐKKD số 0313589030 do Sở Kế hoạch và Đầu tư TPHCM cấp</p>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="footer-hot-line">
                            <p><b class="company-address">Hotline:</b>  <?=Setting::HOTLINE ?></p>
                            <p>Thứ 2 – thứ 6: từ 08h30 – 21h00</p>
                            <p>Thứ 7: 08h30 – 17h00</p>
                            <p><b class="company-address">Email:</b></p> hotro@kyna.vn
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <div class="modal fade" id="modal">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="popup_body">
                            <div class="alert alert-success" role="alert">Cám ơn bạn đã để lại thông tin. Kyna.vn sẽ sớm liên hệ với bạn để tư vấn thêm về chương trình học!</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="/mana/cbp/khoa-hoc-ky-nang-lanh-dao-hieu-qua/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="/mana/cbp/khoa-hoc-ky-nang-lanh-dao-hieu-qua/js/owl.carousel.min.js"></script>
        <script>
            $(function () {
                $('.logo-brand').data('size', 'big');
            });

            $(window).scroll(function () {
                if ($(document).scrollTop() > 0)
                {
                    if ($('.logo-brand').data('size') == 'big')
                    {
                        $('.logo-brand').data('size', 'small');
                        $('.logo-brand').stop().animate({
                            height: '40px'
                        }, 600);
                        $('.navbar').addClass('menufix');
                    }
                } else
                {
                    if ($('.logo-brand').data('size') == 'small')
                    {
                        $('.logo-brand').data('size', 'big');
                        $('.logo-brand').stop().animate({
                            height: '50px'
                        }, 600);

                        $('.navbar').removeClass('menufix');
                    }
                }
            });
        </script>
        <!-- Nhan them vao -->
        <script>
            $(document).ready(function () {
                $("#dang_ky_form").bind('click', function (e) {

                    if ($.trim($("#name").val()) != '' && $.trim($("#email").val()) != '' && $.trim($("#phone").val()) != '') {
                        e.preventDefault();
                        var url = 'https://docs.google.com/forms/d/e/1FAIpQLSdoE_XDl7u06D_89bwAptcMV6vKQb0n_S-t_BGrLcLY1NsStw/formResponse';
                        var data = {
                            'entry.1618386042': $("#name").val(),
                            'entry.1550829392': $("#email").val(),
                            'entry.1725316625': $("#phone").val(),
                            'entry.953023567': '<?php echo $utm_source; ?>',
                            'entry.823420574':'<?php echo $utm_medium;  ?>',
                            'entry.1198803187':'<?php echo $utm_campaign;  ?>'
                            };
                        $.ajax({
                            'url': url,
                            'method': 'POST',
                            'dataType': 'XML',
                            'data': data,
                            'statusCode': {
                                0: function () {
                                    $("#name").val('');
                                    $("#email").val('');
                                    $("#phone").val('');
                                    $("#modal").modal();
                                },
                                200: function () {
                                    $("#name").val('');
                                    $("#email").val('');
                                    $("#phone").val('');
                                    $("#modal").modal();
                                }
                            }

                        });
                    }

                });

                $(".wrapper-slider ul").owlCarousel({
                    autoPlay: true,
                    items: 1,
                    itemsDesktop: [1199, 1],
                    itemsDesktopSmall: [979, 1],
                    itemsTablet: [768, 1],
                    itemsMobile: [479, 1],
                    mouseDrag: true,
                    autoPlay: 40000,
                            navigation: false,
                    pagination: true,
                });
            });


            $(function () {
                $('.scroll-link a[href*=#]:not([href=#])').click(function () {
                    if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {

                        var target = $(this.hash);
                        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                        if ($(window).width() > 768) {
                            if (target.length) {
                                $('html,body').animate({
                                    scrollTop: target.offset().top - 70
                                }, 1000);
                                return false;
                            }
                        } else {
                            if (target.length) {
                                $('html,body').animate({
                                    scrollTop: target.offset().top
                                }, 1000);
                                return false;
                            }
                        }
                    }
                });
            });

        </script>
         <!--End of Zopim Live Chat Script-->
        <!--Start of Zopim Live Chat Script-->
<script type="text/javascript">
    window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
        d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
    _.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
        $.src="//v2.zopim.com/?1bMWnWprkt76mXJXRb6xBuP2dAD7uxPq";z.t=+new Date;$.
            type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
</script>
<!--End of Zopim Live Chat Script-->


<!-- Google Tag Manager (noscript) --> 
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-M92WKP"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

    </body>

    <!-- Mirrored from mana.edu.vn/cbp/khoa-hoc-ky-nang-giao-tiep/ by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 27 Apr 2016 13:31:05 GMT -->
</html>
