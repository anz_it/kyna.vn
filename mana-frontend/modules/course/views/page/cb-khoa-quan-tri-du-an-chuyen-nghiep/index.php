<?php

use common\helpers\Html;

/**
 * @var $this \yii\web\View
 */
$dir_name = dirname(dirname(dirname(__FILE__)));
?>
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Quản trị dự án chuyên nghiệp trong 12 tuần</title>

        <meta property='og:title' content='Quản trị dự án chuyên nghiệp trong 12 tuần' />
        <meta property='og:description' content='Đạt đủ điều kiện 35 PDUs theo tiêu chuẩn PMP. Khóa học bao gồm những kiến thức đầy đủ nhất về Project Management.' />

        <link href="/mana/cs/cb-quan-tri-du-an-chuyen-nghiep/css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="/mana/cs/cb-quan-tri-du-an-chuyen-nghiep/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="/mana/cs/cb-quan-tri-du-an-chuyen-nghiep/css/jquery.fullPage.css" rel="stylesheet" type="text/css"/>
        <link href="/mana/cs/cb-quan-tri-du-an-chuyen-nghiep/css/style.css" rel="stylesheet" type="text/css"/>
        <link href="/mana/cs/cb-quan-tri-du-an-chuyen-nghiep/css/iconfont.css" rel="stylesheet" type="text/css"/>
        <link href="/mana/cs/cb-quan-tri-du-an-chuyen-nghiep/css/fonts.css" rel="stylesheet" type="text/css"/>
        <link rel="icon" href="/mana/images/manaFavicon.png">
        <script src="/mana/cs/cb-quan-tri-du-an-chuyen-nghiep/js/jquery.min.js"></script>
        <?php echo Html::csrfMetaTags() ?> 
    </head>
    <body>
    <?php $this->beginBody()?>
        <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-M92WKP');</script>
        <!-- End Google Tag Manager -->

        <?php
        // get các tham số cần thiết */
        $getParams = $_GET;
        $utm_source = '';
        $utm_medium = '';
        $utm_content = '';

        if (isset($getParams['utm_source']) && trim($getParams['utm_source']) != '') {
            $utm_source = trim($getParams['utm_source']);
        }
        if (isset($getParams['utm_medium']) && trim($getParams['utm_medium']) != '') {
            $utm_medium = trim($getParams['utm_medium']);
        }
        if (isset($getParams['utm_content']) && trim($getParams['utm_content']) != '') {
            $utm_content = trim($getParams['utm_content']);
        }
        ?>

        <div id="fullpage">
            <section id='header'>
                <div class="container">
                    <a class="navbar-brand" href="/">
                        <img src="/mana/cs/cb-quan-tri-du-an-chuyen-nghiep/img/logo.png" alt="" />
                    </a>
                    <nav class="navbar navbar-light bg-faded" role="navigation">
                        <button class="navbar-toggler hidden-lg-up" type="button" data-toggle="collapse" data-target="#collapsing-navbar">
                            &#9776;
                        </button>

                        <div class="collapse navbar-toggleable-md" id="collapsing-navbar">
                            <ul class="nav navbar-nav">
                                <li class="nav-item"><a href="#about" class="nav-link active">GIỚI THIỆU</a></li>
                                <li class="nav-item"><a href="#courses" class="nav-link">NỘI DUNG</a></li>
                                <li class="nav-item"><a href="#courses-detail-author" class="nav-link">GIẢNG VIÊN</a></li>
                                <li class="nav-item"><a href="#object" class="nav-link">LỢI ÍCH</a></li>
                                <li class="nav-item"><a href="#license" class="nav-link">ĐƠN VỊ CẤP BẰNG</a></li>
                                <li class="nav-item"><a href="#register" class="nav-link button-scroll">ĐĂNG KÝ</a></li>
                            </ul>
                        </div>
                    </nav>
                </div>
            </section>

            <section id="banner" class="section">
                <img src="/mana/cs/cb-quan-tri-du-an-chuyen-nghiep/img/bg-banner.png" alt="" width="100%" />
                                <div class="box-banner">
                    <div class="title">
                        <div class="container">
                            <div class="tit-2">QUẢN TRỊ DỰ ÁN CHUYÊN NGHIỆP</div>
                            <div class="tit-3">VỚI KHÓA HỌC PROJECT MANAGEMENT</div>
                        </div>
                    </div>
                    <div class="container">
                        <div class="tit-4">Theo tiêu chuẩn PMP (tương đương 35 PDUs) (*)</div>
                        <p style="margin-bottom: 0">
                            <span>Chứng chỉ PMP</span> được cấp bởi <span>Viện Quản lý Dự án Hoa Kỳ (PMI)</span>, cấp và được công nhận trên toàn cầu. Chứng chỉ PMP được xem là thước đo, <span>là chứng chỉ cốt lõi của mọi nhà quản lý dự án trên toàn cầu.</span>
                        </p>
                        <p>Để tham gia kỳ thi cấp chứng chỉ PMP, người dự thi cần tối thiểu 35 giờ học (35 PDUs) chuyên sâu về quản lý dự án.</p>
                        <p class="text-bottom">(*): PMP - Project Management Professional là chứng chỉ về quản trị dự án chuyên nghiệp.</p>
                        <div class="icon-angle-down" data-url="#about"></div>
                    </div>
                </div>
            </section>
            <section id="about" path='scrolling' class="section">
                <div class="container">
                    <div class="courses-detail col-md-7">
                        <h3>KHÓA HỌC CUNG CẤP KIẾN THỨC VỀ</h3>
                        <ul class="courses-detail-list">
                            <li><span>Hoạch định dự án và ngân sách một cách hiệu quả.</span></li>
                            <li><span>Phân công nhiệm vụ cho các thành viên.</span></li>
                            <li><span>Cách lên lịch trình quản lý và vận hành dự án.</span></li>
                            <li><span>Biết cách phối hợp và đạt được hiệu quả cao nhất trong từng hoạt động.</span></li>
                            <li><span>Quản lý rủi ro, xác định và xử lý rủi ro.</span></li>
                            <li><span>Hoạch định giải pháp truyền thông.</span></li>
                        </ul>
                                                <ul class="button-about">
                            <li><button class="btn btn-register btn-tuvan button-scroll">NHẬN TƯ VẤN VỀ LỊCH HỌC</button></li>
                        </ul>
                    </div>
                    <div class="courses-info col-md-5">
                        <h3>Thông tin khóa học</h3>
                        <ul class="courses-info-list">
                            <li class="courses-info-icon-1">12 tuần</li>
                            <li class="courses-info-icon-2">Biên soạn, thiết kế dựa trên giáo trình chuẩn quốc tế của Đại học California, Mỹ (UCI).</li>
                            <li class="courses-info-icon-3">Chuyên gia từ ĐH California, Mỹ (UCI) kết hợp với giảng viên người Việt giàu kinh nghiệm.</li>
                            <li class="courses-info-icon-4">Song ngữ tiếng Anh – tiếng Việt.</li>
                        </ul>
                    </div>
                </div>
                <div class="icon-angle-down" data-url="#courses"></div>
            </section>
            <section id="courses" path='scrolling' class="section">
                <div class="container">
                    <div class="mana-vi">QUẢN TRỊ DỰ ÁN</div>
                    <div class="mana-en">PROJECT MANAGEMENT</div>
                    <div class="box-mana">
                        <div class="mana-detail col-md-4" data-url="#courses-detail-time">
                            <strong>12</strong>
                            <br />Tuần học
                        </div>
                        <div class="mana-detail col-md-4" data-url="#courses-detail-courses">
                            <strong>4</strong>
                            <br />Khóa học
                        </div>
                        <div class="mana-detail col-md-4" data-url="#courses-detail-author">
                            <strong>2</strong>
                            <br />Chuyên gia
                        </div>
                    </div>
                </div>
                                <div class="icon-angle-down" data-url="#courses-detail-time"></div>
            </section>
            <section id="courses-detail-time" class="courses-detail-time section">
                <div class="container">
                    <h3>12 TUẦN HỌC</h3>
                    <div class="box-time">
                        <div class="row">
                            <div class="cell-time">
                                <div class="img-course-time time-1"></div>
                                <div class="text-courses"><span>1.</span> Học trực tuyến các bài giảng Video</div>
                            </div>
                            <div class="cell-time">
                                <div class="img-course-time time-2"></div>
                                <div class="text-courses"><span>2.</span> Thực hành hàng tuần</div>
                            </div>
                            <div class="cell-time">
                                <div class="img-course-time time-3"></div>
                                <div class="text-courses"><span>3.</span> Thảo luận trên Facebook Group</div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="cell-time">
                                <div class="img-course-time time-4"></div>
                                <div class="text-courses"><span>4.</span> Kiểm tra tiểu luận cuối khóa</div>
                            </div>
                            <div class="cell-time">
                                <div class="img-course-time time-5"></div>
                                <div class="text-courses"><span>5.</span> Nộp hồ sơ và nhận bằng Quốc tế</div>
                            </div>
                        </div>
                    </div>
                </div>
                                <div class="icon-angle-down" data-url="#courses-detail-courses"></div>
            </section>
            <section id="courses-detail-courses" class="courses-detail-courses section">
                <div class="container">

                        <h2>4 KHÓA HỌC</h2>
                        <ul class="wrap-list">
                            <li class="list col-lg-3 col-sm-6 col-xs-12">
                                <h4><span></span>1</h4>
                                <div class="box">
                                    <div class="content">
                                        <h3>Hoạch định dự án hiệu quả trong 6 tuần</h3>
                                        <ul class="wrap-list-box">
                                            <li>- Tổng quan về hoạch định dự án</li>
                                            <li>- Quản lý các bên liên quan và phạm vi dự án</li>
                                            <li>- Quản lý nguồn nhân lực</li>
                                        </ul>
                                    </div>
                                </div><!--end .box-->
                            </li>

                            <li class="list col-lg-3 col-sm-6 col-xs-12">
                                <h4><span></span>2</h4>
                                <div class="box">

                                    <div class="content">
                                        <h3>Hoạch định ngân sách và lên lịch trình dự án</h3>
                                        <ul class="wrap-list-box">
                                            <li>- Nhu cầu nguồn lực và quản lý chất lượng</li>
                                            <li>- Ước lượng và quản lý chi phí dự án</li>
                                            <li>- Lên ngân sách và lịch trình cho dự án</li>
                                        </ul>
                                    </div>
                                </div><!--end .box-->
                            </li>

                            <li class="list col-lg-3 col-sm-6 col-xs-12">
                                <h4><span></span>3</h4>
                                <div class="box">

                                    <div class="content">
                                        <h3>Hoạch định giải pháp truyền thông và xử lý rủi ro trong dự án</h3>
                                        <ul class="wrap-list-box">
                                            <li>- Quản lý truyền thông</li>
                                            <li>- Quản lý rủi ro trong dự án</li>
                                            <li>- Quản lý sự thay đổi và tích hợp dự án</li>
                                        </ul>
                                    </div>
                                </div><!--end .box-->
                            </li>

                            <li class="list col-lg-3 col-sm-6 col-xs-12">
                                <h4><span></span>4</h4>
                                <div class="box">
                                    <div class="content">
                                        <h3>Kiểm tra khả năng quản lý dự án</h3>
                                        <ul class="wrap-list-box">
                                            <li>- Kiểm tra kiến thức</li>
                                            <li>- Đánh giá năng lực cuối khóa.</li>
                                        </ul>
                                    </div>
                                </div><!--end .box-->
                            </li>
                        </ul>
                        <div class="icon-angle-down" data-url="#courses-detail-author"></div>

                </div>
            </section>
            <section id="courses-detail-author" class="section" path="scrolling">
                <div class="container">
                    <h3>2 CHUYÊN GIA</h3>
                    <div class="row sm-row">
                        <div class="col-md-6 left">
                            <img src="/mana/cs/cb-quan-tri-du-an-chuyen-nghiep/img/gv1.png" alt="" class="img-responsive sm-gv1" />
                            <img src="/mana/cs/cb-quan-tri-du-an-chuyen-nghiep/img/bw1.png" alt="" class="img-responsive md-gv" data-target="gv1" data-img-bw="bw1" />
                        </div>
                        <div class="col-md-6 sm-gv1">
                            <p>
                                Giảng viên đến từ Đại học UCI
                            </p>
                            <b>Margaret Meloni, MBA, PMP</b>
                            <ul>
                                <li>
                                    <p>
                                        Giảng viên Quản lý dự án của UCI Irvine Extension và UCLA Extension là những Đại học hàng đầu thế giới.
                                    </p>
                                </li>
                                <li>
                                    <p>
                                        Chủ tịch tại Meloni Coaching Solutions chuyên về quản trị dự án.
                                    </p>
                                </li>
                                <li>
                                    <p>
                                        Hơn 18 năm kinh nghiệm trong vai trò quản lý dự án tại nhiều công ty lớn như Fortune 500.
                                    </p>
                                </li>
                            </ul>
                        </div>
                        <div class="col-md-6 right">
                            <img src="/mana/cs/cb-quan-tri-du-an-chuyen-nghiep/img/gv2.png" alt="" class="img-responsive sm-gv2" />
                            <img src="/mana/cs/cb-quan-tri-du-an-chuyen-nghiep/img/bw2.png" alt="" class="img-responsive md-gv" data-target="gv2" data-img-bw="bw2" />
                        </div>
                        <div class="col-md-6 sm-gv2">
                            <p>
                                Giảng viên Việt Nam
                            </p>
                            <b>Thạc sĩ Lê Văn Tiến Sĩ, PMP</b>
                            <ul>
                                <li>
                                    <p>
                                        Giám đốc sản xuất, giám đốc chất lượng, giám đốc đào tạo tại Success Software Service (SSS).
                                    </p>
                                </li>
                                <li>
                                    <p>
                                        Chuyên gia đào tạo quản lý dự án và kỹ năng mềm tại TUV Rheinland Việt Nam.
                                    </p>
                                </li>
                                <li>
                                    <p>
                                        Hơn 15 năm kinh nghiệm trong lĩnh vực quản lý.
                                    </p>
                                </li>
                                <li>
                                    <p>
                                        Từng là Giám đốc Điều hành công ty Gia công phần mềm của Úc,
                                        Giám đốc khu vực Đông Dương và trưởng phòng đại diện của NIII Ấn Độ tại Việt Nam.
                                    </p>
                                </li>
                            </ul>
                        </div>

                        <div class="col-md-6 tt-gv gv1" data-target="gv1" data-img-bw="bw1">
                            <p>
                                Giảng viên đến từ Đại học UCI
                            </p>
                            <b>Margaret Meloni, MBA, PMP</b>
                            <ul>
                                <li>
                                    <p>
                                        Giảng viên Quản lý dự án của UCI Irvine Extension và UCLA Extension là những Đại học hàng đầu thế giới.
                                    </p>
                                </li>
                                <li>
                                    <p>
                                        Chủ tịch tại Meloni Coaching Solutions chuyên về quản trị dự án.
                                    </p>
                                </li>
                                <li>
                                    <p>
                                        Hơn 18 năm kinh nghiệm trong vai trò quản lý dự án tại nhiều công ty lớn như Fortune 500.
                                    </p>
                                </li>
                            </ul>
                        </div>
                        <div class="col-md-6 tt-gv gv2" data-target="gv2" data-img-bw="bw2">
                            <p>
                                Giảng viên Việt Nam
                            </p>
                            <b>Thạc sĩ Lê Văn Tiến Sĩ, PMP</b>
                            <ul>
                                <li>
                                    <p>
                                        Giám đốc sản xuất, giám đốc chất lượng, giám đốc đào tạo tại Success Software Service (SSS).
                                    </p>
                                </li>
                                <li>
                                    <p>
                                        Chuyên gia đào tạo quản lý dự án và kỹ năng mềm tại TUV Rheinland Việt Nam.
                                    </p>
                                </li>
                                <li>
                                    <p>
                                        Hơn 15 năm kinh nghiệm trong lĩnh vực quản lý.
                                    </p>
                                </li>
                                <li>
                                    <p>
                                        Từng là Giám đốc Điều hành công ty Gia công phần mềm của Úc,
                                        Giám đốc khu vực Đông Dương và trưởng phòng đại diện của NIII Ấn Độ tại Việt Nam.
                                    </p>
                                </li>
                            </ul>
                        </div>

                    </div>

                </div>
            </section>
            <section id="object" path='scrolling' class="section">
                <div class="container">
                    <div class="content-object col-lg-7">
                        <h2>MỤC TIÊU KHÓA HỌC</h2>
                        <ul>
                            <li><span>Dễ dàng xác định phạm vi và quản lý dự án.</span></li>
                            <li><span>Phân tích và hoạch định được nhu cầu nguồn nhân lực, tài chính và thời gian.</span></li>
                            <li><span>Doanh nghiệp và cá nhân khẳng định lợi thế cạnh tranh trên thị trường.</span></li>
                            <li><span>Hiểu rõ và có khả năng đối mặt, giải quyết những phát sinh bất ngờ của dự án.</span></li>
                            <li><span><b>Đạt điều kiện “35 giờ học quản lý dự án chính thức” tương đương 35 PDUs.</b></span></li>
                            <li><span>Các ứng viên thi chứng chỉ PMP ® có thể tham gia các khóa học do các đối tác đào tạo của PMI tổ chức.</span></li>
                        </ul>
                        <ul class="button-main">
                            <li><button class="btn btn-see-intro" data-toggle="modal" data-target="#video-intro"><img src="/mana/cs/cb-quan-tri-du-an-chuyen-nghiep/img/intro.png" alt="">INTRO</button></li>
                            <li><button class="btn btn-register btn-tuvan button-scroll">NHẬN TƯ VẤN MIỄN PHÍ</button></li>
                        </ul>
                    </div>
                    <div class="image-object col-lg-5">
                        <div class="box-img">
                            <img class="img-1" src="/mana/cs/cb-quan-tri-du-an-chuyen-nghiep/img/object-1.png" />
                            <img class="img-2" src="/mana/cs/cb-quan-tri-du-an-chuyen-nghiep/img/object-2.png" />
                        </div>
                    </div>
                </div>
            </section>
            <section id="service" class="section">
                <div class="container">
                    <div class="box-service col-md-6">
                        <h2>THAM GIA KHÓA HỌC PROJECT MANAGEMENT</h2>
                        <h4>Tại MANA ONLINE BUSINESS SCHOOL</h4>
                        <ul>
                            <li><img src="/mana/cs/cb-quan-tri-du-an-chuyen-nghiep/img/box-service-icon-1.png"/><span>Học ONLINE 100%.</span></li>
                            <li><img src="/mana/cs/cb-quan-tri-du-an-chuyen-nghiep/img/box-service-icon-2.png"/><span>Chủ động không gian và thời gian học tập.</span></li>
                            <li><img src="/mana/cs/cb-quan-tri-du-an-chuyen-nghiep/img/box-service-icon-3.png"/><span>Học trên nền tảng học trực tuyến cao cấp.</span></li>
                            <li><img src="/mana/cs/cb-quan-tri-du-an-chuyen-nghiep/img/box-service-icon-4.png"/><span>Học Online song ngữ Anh - Việt.</span></li>
                            <li><img src="/mana/cs/cb-quan-tri-du-an-chuyen-nghiep/img/box-service-icon-5.png"/><span>Đội ngũ cố vấn học tập 24/7.</span></li>
                        </ul>
                    </div>
                    <div class="col-md-6 col-xs-12 img">
                       <img src="/mana/cs/cb-quan-tri-du-an-chuyen-nghiep/img/img-service.jpg" alt="" class="img-responsive">
                    </div><!--end .img-->
                    <div class="icon-angle-down" data-url="#license"></div>
                </div>
            </section>
            <section id="license" path='scrolling' class="section">
                <div class="container">
                    <div class="image-license col-md-5">
                        <img src="/mana/cs/cb-quan-tri-du-an-chuyen-nghiep/img/img-license.png" />
                    </div>
                    <div class="content-license col-md-7">
                        <h3>CHỨNG CHỈ ĐƯỢC CẤP BỞI</h3>
                        <h2>CALIFORNIA – IRVINE EXTENSION</h2>
                        <h2>(ĐH UCI – HOA KỲ)</h2>
                        <div class="line-license"></div>
                        <ul>
                            <li class="icon-star">UC Irvine Extension thuộc hệ thống đại học California tại thành phố Irvine, California.</li>
                            <li class="icon-star">Top 50 trường Đại học tốt nhất nước Mỹ.</li>
                            <li class="icon-star">Là một trong những trường ĐH hàng đầu về đào tạo và nghiên cứu trên thế giới.</li>
                            <li class="icon-star">Chứng chỉ được cấp bởi ĐH UCI được công nhận trên toàn thế giới.</li>
                        </ul>
                    </div>
                    <div class="icon-angle-down" data-url="#welcome"></div>
                </div>
            </section>
            <section id="pmp" path='scrolling' class="section">
                <div class="container">
                    <h2>DÀNH CHO CÁC HỌC VIÊN MUỐN THAM GIA KỲ THI PMP</h2>
                    <div class="col-sm-7 col-xs-12 text">
                        <ul>
                            <li>Hoàn thành khóa học này, học viên sẽ <span>đạt được điều kiện 35 giờ học</span> (35 PDUs) của kỳ thi cấp chứng chỉ PMP.</li>
<li>Có được nền tảng kiến thức và kỹ năng về quản lý dự án theo tiêu chuẩn quốc tế.</li>
<li><span>Sở hữu chứng nhận uy tín cho trình độ về quản lý dự án</span> theo tiêu chuẩn quốc tế.</li>
                        </ul>
                    </div>
                    <div class="image-license col-md-5">
                        <img src="/mana/cs/cb-quan-tri-du-an-chuyen-nghiep/img/logo-pmp.png" />
                    </div>
                    <img src="/mana/cs/cb-quan-tri-du-an-chuyen-nghiep/img/img-bottom-pmp.png" class="img-responsive bottom">
                    <div class="icon-angle-down" data-url="#register"></div>
                </div>
            </section>
            <section id="welcome" class="section">
                <div class="container">
                    <h2>“Xin chào, chúng tôi là MANA,<br />Viện đào tạo Quản trị Kinh doanh trực tuyến hàng đầu Việt Nam.</h2>
                    <h5>Chúng tôi mang đến một hệ thống các chương trình đào tạo kỹ năng quản trị kinh doanh có tính ứng dụng cao.”</h5>
                    <h4>Tại MANA, các bạn sẽ được học các chương trình <span>đào tạo song ngữ với đội ngũ cố vấn học tập</span> 24/7. Hình thức và phương pháp học khoa học mang lại cho bạn những trải nghiệm học tập mới mẻ và hữu ích.</h4>
                    <div class="image-welcome">
                        <img src="/mana/cs/cb-quan-tri-du-an-chuyen-nghiep/img/img-welcome.png" />
                    </div>
                    <div class="icon-angle-down" data-url="#register"></div>
                </div>
            </section>
            <section id="register" path='scrolling' class="section">
                <div class="container">
                    <form class="form-register container" name="form-submit" action="/course/page/submit" method="post" accept-charset="utf-8" id="landing-page-id">
                        <input type="hidden" id="advice_name" name="advice_name" value="Mana - Quản trị dự án chuyên nghiệp" />
                        <input type="hidden" id="csrf" name="_csrf" />

                        <div class="title-register">ĐĂNG KÝ NHẬN TƯ VẤN MIỄN PHÍ <br/> CÙNG CƠ HỘI NHẬN HỌC BỔNG TRỊ GIÁ 2.500.000Đ</div>
                        <div class="form-group col-md-4">
                            <input type="text" class="form-control" id="fullname" name="fullname" value="" placeholder="Họ và tên"  required>
                        </div>
                        <div class="form-group col-md-4">
                            <input type="email" id="email" class="form-control" name="email" value="" placeholder="Email" required>
                        </div>
                        <div class="form-group col-md-4">
                            <input type="number" id="phonenumber" class="form-control" name="phonenumber" value="" placeholder="Số điện thoại" required>
                        </div>
                        <center><button class="btn btn-register btn_box_register button-regis" type="submit">ĐĂNG KÝ</button></center>
                        <center><p>Bộ phận CSKH của MANA sẽ sớm liên hệ và tư vấn miễn phí về khóa học cho bạn. Chúc bạn một ngày làm việc vui vẻ.</p></center>
                    </form>
                </div>
            </section>
            <section id="footer">
                <div class="container">
                    <div class="img-footer col-md-4">
                        <img src="/mana/cs/cb-quan-tri-du-an-chuyen-nghiep/img/branch.png" />
                    </div>
                    <div class="address-footer col-md-5">
                        <p><b>Công ty Cổ phần  Dream Việt Education</b></p>
                        <p><b><u>Trụ sở chính:</u></b> Lầu 6 Tòa nhà Thịnh Phát - 178/8 Đường D1, Phường 25, Quận Bình Thạnh, TP Hồ Chí Minh</p>
                        <p><b><u>Văn phòng Hà Nội:</u></b> Tầng 6, Tòa Nhà Viện Công Nghệ - 25 Vũ Ngọc Phan, Phường Láng Hạ, Quận Đống Đa, TP Hà Nội</p>
                        <p class="page-license">Giấy phép ĐKKD số 0313589030 do Sở Kế hoạch và Đầu tư TPHCM cấp</p>
                    </div>
                    <div class="info-footer col-md-3">
                        <p><b><u>Hotline:</u></b> 1900 6364 09</p>
                        <p>Thứ 2 – thứ 6: từ 08h30 – 21h00</p>
                        <p>Thứ 7: 08h30 – 17h00</p>
                        <p><b><u>Email:</u></b> hotro@kyna.vn</p>
                    </div>
                </div>
            </section>
            <!--</footer>-->
        </div>


        <div class="modal fade" id="modal">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="popup_body">
                            <div class="alert alert-success" role="alert">Cám ơn bạn đã để lại thông tin. Kyna.vn sẽ sớm liên hệ với bạn để tư vấn thêm về chương trình học!</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="video-intro">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="videoWrapper"></div>
                        <button class="btn btn-close" data-dismiss="modal" aria-label="Close">Đóng</button>
                    </div>
                </div>
            </div>
        </div>

        <script src="/mana/cs/cb-quan-tri-du-an-chuyen-nghiep/js/tether.min.js"></script>
        <script src="/mana/cs/cb-quan-tri-du-an-chuyen-nghiep/js/bootstrap.min.js"></script>
        <script src="/mana/cs/cb-quan-tri-du-an-chuyen-nghiep/js/jquery.fullPage.min.js"></script>
        <script src="/mana/cs/cb-quan-tri-du-an-chuyen-nghiep/js/jquery.scrollme.min.js"></script>
        <script src="/mana/cs/cb-quan-tri-du-an-chuyen-nghiep/js/main.js"></script>



        <script>
            $(".btn-see-intro").click(function () {
                $("#video-intro .modal-body .videoWrapper").html('<iframe width="560" height="315" src="https://www.youtube.com/embed/LCHNlkFJGZw" frameborder="0" allowfullscreen></iframe>');
            });

            $("#video-intro .btn-close").click(function () {
                $("#video-intro .modal-body .videoWrapper").html('');
            });

            $('#video-intro').on('hidden.bs.modal', function () {
                $("#video-intro .modal-body .videoWrapper").html('');
            })
        </script>
        <!--End of Zopim Live Chat Script-->
        <!--Start of Zopim Live Chat Script-->
<script type="text/javascript">
    window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
        d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
    _.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
        $.src="//v2.zopim.com/?1bMWnWprkt76mXJXRb6xBuP2dAD7uxPq";z.t=+new Date;$.
            type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
</script>
<!--End of Zopim Live Chat Script-->


<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-M92WKP"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<?php $this->endBody()?>
    </body>

</html>
