<?php
use mana\models\Setting;
$dir_name = dirname(dirname(dirname(__FILE__)));
?>
<!DOCTYPE html>
<html lang="en">

    <!-- Mirrored from mana.edu.vn/cbp/khoa-hoc-ky-nang-giao-tiep/ by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 27 Apr 2016 13:29:41 GMT -->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Đào tạo Kỹ năng giao tiếp trong kinh doanh - Mana.edu.vn</title>
        <link href='https://fonts.googleapis.com/css?family=Roboto:400,300,500,700' rel='stylesheet' type='text/css'>
        <!-- Bootstrap -->
        <link href="/mana/cbp/khoa-hoc-ky-nang-giao-tiep/css/bootstrap.min.css" rel="stylesheet"/>
        <link href="/mana/cbp/khoa-hoc-ky-nang-giao-tiep/css/site.css" rel="stylesheet"/>
        <link href="/mana/cbp/khoa-hoc-ky-nang-giao-tiep/css/owl.carousel.css" rel="stylesheet"/>
        <link href="/mana/cbp/khoa-hoc-ky-nang-giao-tiep/css/owl.theme.css" rel="stylesheet"/>
        <link href="/mana/cbp/khoa-hoc-ky-nang-giao-tiep/css/owl.transitions.css" rel="stylesheet"/>
        <link rel="icon" href="/mana/images/manaFavicon.png">


    </head>
    <body>

    <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-M92WKP');</script>
<!-- End Google Tag Manager -->

        <?php
        // get các tham số cần thiết */
        $getParams = $_GET;
        $utm_source = '';
        $utm_medium = '';
        $utm_campaign = '';

        if (isset($getParams['utm_source']) && trim($getParams['utm_source']) != '') {
            $utm_source = trim($getParams['utm_source']);
        }
        if (isset($getParams['utm_medium']) && trim($getParams['utm_medium']) != '') {
            $utm_medium = trim($getParams['utm_medium']);
        }
        if (isset($getParams['utm_campaign']) && trim($getParams['utm_campaign']) != '') {
            $utm_campaign = trim($getParams['utm_campaign']);
        }
        ?>
        <nav class="navbar navbar-inverse header-menu navbar-fixed-top scroll-link">
            <div class="container">
                <div class="col-sm-3">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                                data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="#">
                            <img class="logo-brand" src="/mana/cbp/khoa-hoc-ky-nang-giao-tiep/imgs/1_Logo.png"/>
                        </a>
                        <a href="#regis" class="button mb">ĐĂNG KÝ NGAY</a>
                    </div>
                </div>
                <div class="col-sm-9 col-xs-12">
                    <div id="navbar" class="navbar-collapse collapse">
                        <ul class="nav navbar-nav main-menu">
                            <li class="active"><a href="#intro">GIỚI THIỆU</a></li>
                            <li><a href="#part">HỌC PHẦN</a></li>
                            <li><a href="#method">HÌNH THỨC HỌC</a></li>
                            <li><a href="#teacher">GIẢNG VIÊN</a></li>
                            <li><a href="#regis">ĐĂNG KÝ</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </nav>

        <div id="banner">
            <div class="wrapper-slider scroll-link">
                <ul>
                    <li>
                        <div class="item">
                            <img class="img-slider img-responsive" src="/mana/cbp/khoa-hoc-ky-nang-giao-tiep/imgs/1_BG/1_BG1.png" alt="First slide"/>
                            <div class="container">
                                <div class="carousel-caption">
                                    <img class="logo-slider img-responsive" src="/mana/cbp/khoa-hoc-ky-nang-giao-tiep/imgs/1_Logo.png"/>
                                    <p>Chương trình <span class="bold">"Đào tạo Kỹ năng giao tiếp trong kinh doanh"</span><br /> do Hiệp hội doanh nhân Quốc Tế cấp chứng chỉ</p>
                                    <a class="btn btn-lg btn-primary btn-regis-slider" href="#regis" role="button">ĐĂNG KÝ</a>
                                </div>
                            </div>
                        </div><!--end .item-->
                    </li>

                    <li>
                        <div class="item">
                            <img class="img-slider img-responsive" src="/mana/cbp/khoa-hoc-ky-nang-giao-tiep/imgs/1_BG/4_Slide2.png" alt="Second slide"/>
                            <div class="container">
                                <div class="carousel-caption">
                                    <img class="logo-slider img-responsive" src="/mana/cbp/khoa-hoc-ky-nang-giao-tiep/imgs/1_Logo.png"/>
                                    <p>Nhận bằng CBP Business Communication do Hiệp hội<br /> doanh nhân Quốc Tế cấp – Công nhận quốc tế</p>
                                    <a class="btn btn-lg btn-primary btn-regis-slider" href="#regis" role="button">ĐĂNG KÝ</a>
                                </div>
                            </div>
                        </div><!--end .item-->
                    </li>

                    <li>
                        <div class="item">
                            <img class="img-slider img-responsive" src="/mana/cbp/khoa-hoc-ky-nang-giao-tiep/imgs/1_BG/5_Slide3.png" alt="Third slide"/>
                            <div class="container">
                                <div class="carousel-caption">
                                    <img class="logo-slider img-responsive" src="/mana/cbp/khoa-hoc-ky-nang-giao-tiep/imgs/1_Logo.png"/>
                                    <p>Học online mọi lúc mọi nơi cùng doanh nhân hàng đầu</p>
                                    <a class="btn btn-lg btn-primary btn-regis-slider" href="#regis" role="button">ĐĂNG KÝ</a>
                                </div>
                            </div>
                        </div><!--end .item-->
                    </li>


                    <li>
                        <div class="item">
                            <img class="img-slider img-responsive" src="/mana/cbp/khoa-hoc-ky-nang-giao-tiep/imgs/1_BG/6_Slide4.png" alt="Third slide"/>
                            <div class="container">
                                <div class="carousel-caption">
                                    <img class="logo-slider img-responsive" src="/mana/cbp/khoa-hoc-ky-nang-giao-tiep/imgs/1_Logo.png"/>
                                    <p>MANA – Học viện đào tạo quản trị kinh doanh trực tuyến<br /> hàng đầu Việt Nam</p>
                                    <a class="btn btn-lg btn-primary btn-regis-slider" href="#regis" role="button">ĐĂNG KÝ</a>
                                </div>
                            </div>
                        </div><!--item-->
                    </li>
                </ul>
            </div><!--end .wrapper-slider-->
        </div><!--end #banner-->

        <div class="container" id="intro">
            <div class="row">
                <div class="col-md-4 col-sm-3"></div>
                <div class="col-md-1 col-sm-1 line"><hr/></div>
                <div class="col-md-2 col-sm-4"><h3 class="intro-heading"> GIỚI THIỆU </h3></div>
                <div class="col-md-1 col-sm-1 line"><hr/></div>
                <div class="col-md-4 col-sm-3"></div>

            </div>

            <div class="row">
                <div class='text-intro'>
                    <p>Chương trình <b class="main-color">“Đào tạo Kỹ năng giao tiếp trong kinh doanh”</b> được thiết kế, giảng dạy
                        bởi các chuyên gia tại Học viện đào tạo quản trị kinh doanh trực tuyến MANA (MANA business school) và được
                        Hiệp Hội Đào tạo Kinh doanh Quốc Tế (International Business Training Association) công nhận. Sau khi hoàn
                        thành đầy đủ các học phần và bài kiểm tra trực tuyến, học viên sẽ được cấp chứng chỉ CBP Business
                        Communication, được công nhận toàn cầu.</p>
                </div>
            </div>

            <div class="row circle-content">
                <div class="col-lg-4 col-sm-6 col-xs-12">
                    <div class="circle circle-first">
                        <div class="circle-text">
                            <p>KHÓA HỌC ĐÀO <br />TẠO KỸ NĂNG <br/> GIAO TIẾP TRONG <br />KINH DOANH</p>
                        </div>
                    </div>
                    <div class="arrow-circle">
                    </div>
                </div><!--end .col-sm-4-->

                <div class="col-lg-3 col-sm-6 col-xs-12">
                    <div class="circle circle-middle">
                        <div class="circle-text">
                            <p>THI TRỰC TUYẾN</p>
                        </div>
                    </div>
                </div><!--end .col-sm-3-->

                <div class="col-lg-1 col-xs-12">
                    <div class="arrow-circle arrow-circle-second"></div>
                </div><!--end .col-sm-1-->

                <div class="col-lg-4 col-sm-12 col-xs-12">
                    <div class="circle circle-last">
                        <div class="circle-text">
                            <p>CBP BUSINESS <br />COMMUNICATION</p>
                        </div>
                    </div>
                </div><!--end .col-sm-4-->

            </div>
            <div class="row">
                <div class="text-details clearfix">
                    <div class="col-sm-7">
                        <img src="/mana/cbp/khoa-hoc-ky-nang-giao-tiep/imgs/1_BG/2_BG2.png" class="img-responsive"/>
                    </div>
                    <div class="col-sm-5">
                        <div class="text-details-content">
                            <p class="text">Chương trình “Đào tạo Kỹ năng giao tiếp trong kinh doanh” của MANA business school cung cấp:</p>
                            <ul>
                                <li><p><span>1. </span>Kiến thức chuẩn, các kỹ năng quan trọng nhất trong giao tiếp kinh doanh.</p></li>
                                <li><p><span>2. </span>Nguyên tắc để vận dụng chuyên nghiệp trong từng hình thức giao tiếp.</p></li>
                                <li><p><span>3. </span>Xử lý bất kỳ mâu thuẫn nào phát sinh trong quá trình giao tiếp.</p></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div><!--end #intro-->

        <div class="section-desc" id="term">
            <div class="section-desc-text-inner">
                <p>Chương trình phù hợp với những nhà lãnh đạo trong kinh doanh, các nhân viên cấp cao, quản lý, giám sát, những người làm việc tại các vị trí giao tiếp trực tiếp bên ngoài và những ai muốn chuyên sâu vào lĩnh vực giao tiếp trong kinh doanh.</p>
            </div><!--end .section-desc-text-inner-->
        </div><!--end .section-desc-->

        <div class="container study-heading" id="part">
            <div class="row">
                <div class="col-md-4 col-sm-3"></div>
                <div class="col-md-1 col-sm-1 line"><hr/></div>
                <div class="col-md-2 col-sm-4"><h3 class="intro-heading"> HỌC PHẦN </h3></div>
                <div class="col-md-1 col-sm-1 line"><hr/></div>
                <div class="col-md-4 col-sm-3"></div>
            </div>
            <div class="row">
                <div class="header-intro-text">
                    <div class="col-sm-3">
                    </div>
                    <div class="col-sm-6">
                        <p>Kỹ năng giao tiếp trong kinh doanh gồm những yếu tố:</p>
                    </div>
                    <div class="col-sm-3">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="content-collapse">
                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingOne">
                                <h4 class="panel-title">
                                    <span class="circle-bullet">1</span>
                                    <span class="title">Giới thiệu giao tiếp trong kinh doanh</span>
                                    <a class="accordion-toggle" role="button" data-toggle="collapse" data-parent="#accordion"
                                       href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel"
                                 aria-labelledby="headingOne">
                                <div class="panel-body">
                                    <ul>
                                        <li>Giao tiếp trong kinh doanh là gì</li>
                                        <li>Một mô hình giao tiếp trong kinh doanh</li>
                                        <li>Trách nhiệm của người mã hóa/ người giải mã</li>
                                        <li>Sự khác biệt giữa phương tiện truyền đạt và kênh truyền đạt</li>
                                        <li>Các rào cản của giao tiếp</li>
                                        <li>Chiến lược để vượt qua rào cản</li>
                                        <li>Phản hồi</li>
                                        <li>Một số câu hỏi sau cùng</li>
                                        <li>Sự khác biệt giữa giao tiếp bằng lời và giao tiếp không bằng lời</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingTwo">
                                <h4 class="panel-title">
                                    <span class="circle-bullet">2</span>
                                    <span class="title">CẤU TRÚC GIAO TIẾP TRONG KINH DOANH</span>
                                    <a class="accordion-toggle collapsed" role="button" data-toggle="collapse"
                                       data-parent="#accordion" href="#collapseTwo" aria-expanded="false"
                                       aria-controls="collapseTwo">
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                <div class="panel-body">
                                    <ul>
                                        <li>Các nền tảng trong giao tiếp</li>
                                        <li>Xác định thông điệp của bạn</li>
                                        <li>Phân tích người nghe của bạn</li>
                                        <li>Cấu trúc hóa thông điệp của bạn</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingThree">
                                <h4 class="panel-title">
                                    <span class="circle-bullet">3</span>
                                    <span class="title">PHÁT TRIỂN PHONG CÁCH VIẾT TRONG KINH DOANH</span>
                                    <a class="accordion-toggle collapsed" role="button" data-toggle="collapse"
                                       data-parent="#accordion" href="#collapseThree" aria-expanded="false"
                                       aria-controls="collapseThree">
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseThree" class="panel-collapse collapse" role="tabpanel"
                                 aria-labelledby="headingThree">
                                <div class="panel-body">
                                    <ul>
                                        <li>
                                            Vai trò của giao tiếp bằng văn bản </li>
                                        <li>Giao tiếp bằng văn bản hiệu quả</li>
                                        <li>Danh sách liệt kê những mục cần kiểm tra trong giao tiếp</li>
                                        <li>Phát triển phong cách giao tiếp bằng văn bản hiệu quả</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingFour">
                                <h4 class="panel-title">
                                    <span class="circle-bullet">4</span>
                                    <span class="title">Các hình thức viết trong kinh doanh</span>
                                    <a class="accordion-toggle collapsed" role="button" data-toggle="collapse"
                                       data-parent="#accordion" href="#collapseFour" aria-expanded="false"
                                       aria-controls="collapseFour">
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseFour" class="panel-collapse collapse" role="tabpanel"
                                 aria-labelledby="headingFou">
                                <div class="panel-body">
                                    <ul>
                                        <li>Các mẫu thư từ và ghi nhớ</li>
                                        <li>Thư từ trong kinh doanh</li>
                                        <li>Các hình thức thư từ mẫu </li>
                                        <li>Ghi nhớ trong kinh doanh</li>
                                        <li>Tin tốt và giao tiếp qua lại bằng thư có tính thuyết phục </li>
                                        <li>Các thông điệp có nội dung tích cực </li>
                                        <li>Các thông điệp có nội dung thuyết phục</li>
                                        <li>Quản lý việc viết báo cáo</li>
                                        <li>Các thành phần của một báo cáo</li>
                                        <li>Giao tiếp bằng thư điện tử</li>
                                        <li>Gửi thư điện tử</li>
                                        <li>Gửi chuyển tiếp thư điện tử</li>
                                        <li>Các cơ bản của thư điện tử</li>
                                        <li>Sử dụng ngôn ngữ giác quan</li>
                                        <li>Điều khoản bảo mật và tác quyền</li>
                                        <li>Nghi thức xã giao trên internet</li>
                                        <li>Giao tiếp trực tuyến </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingFive">
                                <h4 class="panel-title">
                                    <span class="circle-bullet">5</span>
                                    <span class="title">Giao tiếp bằng văn bản trong các trường hợp đặc biệt</span>
                                    <a class="accordion-toggle collapsed" role="button" data-toggle="collapse"
                                       data-parent="#accordion" href="#collapseFive" aria-expanded="false"
                                       aria-controls="collapseFive">
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseFive" class="panel-collapse collapse" role="tabpanel"
                                 aria-labelledby="headingFive">
                                <div class="panel-body">
                                    <ul>
                                        <li>Giao tiếp bằng văn bản lịch thiệp là gì</li>
                                        <li>Các nguyên tắc của giao tiếp bằng văn bản lịch thiệp</li>
                                        <li>Viết một lá thư tồi</li>
                                        <li>Vì sao cần phải viết thuyết phục</li>
                                        <li>Viết một lá thư có tính thuyết phục</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingSix">
                                <h4 class="panel-title">
                                    <span class="circle-bullet">6</span>
                                    <span class="title">Phát triển các kỹ năng giao tiếp bằng lời</span>
                                    <a class="accordion-toggle collapsed" role="button" data-toggle="collapse"
                                       data-parent="#accordion" href="#collapseSix" aria-expanded="false"
                                       aria-controls="collapseSix">
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseSix" class="panel-collapse collapse" role="tabpanel"
                                 aria-labelledby="headingSix">
                                <div class="panel-body">
                                    <ul>
                                        <li>Các hướng dẫn giao tiếp bằng lời hiệu quả</li>
                                        <li>Lên kế hoạch</li>
                                        <li>Các thành phần chính để nâng cao kỹ năng giao tiếp bằng lời </li>
                                        <li>Các yếu tố của giao tiếp giao bằng lời hiệu quả</li>
                                        <li>Nguyên tắc của những bài nói chuyện hiệu quả</li>
                                        <li>Các phong cách nói chuyện hay những hình thức chuyển tải bài nói chuyện</li>
                                        <li>Sự lắng nghe và quan sát tích cực   </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingSeven">
                                <h4 class="panel-title">
                                    <span class="circle-bullet">7</span>
                                    <span class="title">Giao tiếp chuyên nghiệp qua điện thoại</span>
                                    <a class="accordion-toggle collapsed" role="button" data-toggle="collapse"
                                       data-parent="#accordion" href="#collapseSeven" aria-expanded="false"
                                       aria-controls="collapseSeven">
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseSeven" class="panel-collapse collapse" role="tabpanel"
                                 aria-labelledby="headingSeven">
                                <div class="panel-body">
                                    <ul>
                                        <li>Nghi thức xã giao qua điện thoại</li>
                                        <li>Trả lời điện thoại một cách nhã nhặn</li>
                                        <li>Những điều sẽ nói với người gọi điện thoại là gì</li>
                                        <li>Thực hiện các nguyên tắc khi nói chuyện với những gọi điện thoại kém kiên nhẫn</li>
                                        <li>Chọn lọc những cuộc gọi</li>
                                        <li>Ghi chú các thông điệp </li>
                                        <li>Nền tảng của giao tiếp qua điện thoại </li>
                                        <li>Kết thúc cuộc nói chuyện một cách khéo léo </li>
                                        <li>Kiểm tra các thông điệp và gọi điện thoại lại </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingEight">
                                <h4 class="panel-title">
                                    <span class="circle-bullet">8</span>
                                    <span class="title">Giao tiếp không bằng lời </span>
                                    <a class="accordion-toggle collapsed" role="button" data-toggle="collapse"
                                       data-parent="#accordion" href="#collapseEight" aria-expanded="false"
                                       aria-controls="collapseEight">
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseEight" class="panel-collapse collapse" role="tabpanel"
                                 aria-labelledby="headingEight">
                                <div class="panel-body">
                                    <ul>
                                        <li>Tầm quan trọng của việc giao tiếp không lằng lời trong kinh doanh</li>
                                        <li>Ngôn ngữ cử chỉ </li>
                                        <li>Giao tiếp thực tế </li>
                                        <li>Khoảng cách trong giao tiếp</li>
                                        <li>Thể hiện một hình ảnh chuyên nghiệp</li>
                                        <li>Môi trường kinh doanh ảnh hưởng đến giao tiếp như thế nào</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingNine">
                                <h4 class="panel-title">
                                    <span class="circle-bullet">9</span>
                                    <span class="title">Phát triển kỹ năng trình bày hiệu quả</span>
                                    <a class="accordion-toggle collapsed" role="button" data-toggle="collapse"
                                       data-parent="#accordion" href="#collapseNine" aria-expanded="false"
                                       aria-controls="collapseNine">
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseNine" class="panel-collapse collapse" role="tabpanel"
                                 aria-labelledby="headingNine">
                                <div class="panel-body">
                                    <ul>
                                        <li>Các hình thức thuyết trình khác nhau</li>
                                        <li>Bài thuyết trình mang tính chất cung cấp thông tin</li>
                                        <li>Bài thuyết trình có tính thuyết phục </li>
                                        <li>Bài thuyết trình thể hiện sự thiện chí </li>
                                        <li>Những điều lo lắng khi thuyết trình </li>
                                        <li>Trang phục thích hợp khi thuyết trình</li>
                                        <li>Ngữ cảnh và văn hóa trong khi thuyết trình</li>
                                        <li>Những điều quan trọng khi chuẩn bị thuyết trình</li>
                                        <li>Các kỹ thuật đơn giản khi sử dụng hỗ trợ minh họa cho thuyết trình</li>
                                        <li>Minh họa hình ảnh là gì </li>
                                        <li>Một số bí quyết khi chuẩn bị và sử dụng hình ảnh minh họa </li>
                                        <li>Tầm quan trọng của việc sử dụng kỹ thuật trong một bài thuyết trình</li>
                                        <li>Tầm quan trọng của danh sách các mục cần kiểm tra trong thuyết trình</li>
                                        <li>Cần bao gồm những điểm gì trong danh sách các mục cần kiểm tra</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingTen">
                                <h4 class="panel-title">
                                    <span class="circle-bullet">10</span>
                                    <span class="title">Mâu thuẫn và bất đồng trong giao tiếp kinh doanh</span>
                                    <a class="accordion-toggle collapsed" role="button" data-toggle="collapse"
                                       data-parent="#accordion" href="#collapseTen" aria-expanded="false"
                                       aria-controls="collapseTen">
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseTen" class="panel-collapse collapse" role="tabpanel"
                                 aria-labelledby="headingTen">
                                <div class="panel-body">
                                    <ul>
                                        <li>Nhận biết về mâu thuẫn</li>
                                        <li>Vai trò của các giá trị</li>
                                        <li>Các giá trị giải quyết mâu thuẫn</li>
                                        <li>Các cách thức giải quyết mâu thuẫn </li>
                                        <li>Chọn cách thức giải quyết mâu thuẫn </li>
                                        <li>Các chiến lược giải quyết mâu thuẫn </li>
                                        <li>Lắng nghe hiệu quả </li>
                                        <li>Các bí quyết lắng nghe hiệu quả</li>
                                        <li>Trước khi lắng nghe</li>
                                        <li>Trong khi lắng nghe</li>
                                        <li>Trách nhiệm hòa giải của người quản lý</li>
                                        <li>Giao tiếp các nền văn hóa khác nhau </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="teacher" id="teacher">
            <div class="container">
                <div class="row title">
                    <div class="col-md-4 col-sm-3"></div>
                    <div class="col-md-1 col-sm-1 line"><hr/></div>
                    <div class="col-md-2 col-sm-4"><h3 class="intro-heading"> GIẢNG VIÊN </h3></div>
                    <div class="col-md-1 col-sm-1 line"><hr/></div>
                    <div class="col-md-4 col-sm-3"></div>
                </div>
                <div class="wrap-content">
                    <div class="col-sm-4 col-xs-12 img">
                        <img src="/mana/cbp/khoa-hoc-ky-nang-giao-tiep/imgs/hothithanhvan.png" alt="Thạc sỹ Hồ Thị Thanh Vân (Vân Hồ)" class="img-responsive">
                    </div><!--end .video-->
                    <div class="col-sm-8 col-xs-12 content">
                        <h3>Thạc sỹ <span>Hồ Thị Thanh Vân (Vân Hồ)</span></h3>
                        <ul>
                            <li><span>&#45;</span> Được đào tạo Cao học chuyên ngành Marketing Bán hàng và Dịch vụ (MMSS) tại trường Đại học Paris 1 Sorbonne (IAE) phối hợp với trường Quản trị Châu Âu (ESCP - EAP European School Management).</li>
                            <li><span>&#45;</span> 20 năm kinh nghiệm trong lĩnh vực Sales và Marketing tại các công ty hàng đầu thế giới: Giám đốc Ngành hàng kiêm Lãnh đạo thị trường Cambodia/Laos - Novartis Pharma, Giám đốc Điều hành Tiếp thị - AstraZeneca, Giám đốc Marketing - Sanofi, Giám đốc Thương hiệu - Abbott, Giám đốc Kinh doanh - GlaxoSmithKline.</li>
                            <li><span>&#45;</span> Dành nhiều giải thưởng danh giá:
                                Giải thưởng của Phó Chủ tịch tập đoàn Khu vực Châu Á, Giải thưởng Khu vực International cho thành tích xuất sắc trong việc giới thiệu thành công sản phẩm mới ra thị trường (AstraZeneca); Giải thưởng Quán quân Marketing cho Chương trình Marketing xuất sắc (Abbott); Giải thưởng Cầu vồng dành cho Lãnh đạo xuất sắc và Giải thưởng Thành tích Kinh doanh xuất sắc trong 4 năm liên tiếp (GlaxoSmithKline).</li>
                        </ul>

                    </div><!--end .content-->
                </div><!--end .content-->
            </div><!--end .container-->
        </div>
        <div class="study-method" id="method">
            <div class="container">
                <div class="row">
                    <div class="col-sm-3">
                    </div>
                    <div class="col-sm-1 line">
                        <hr/>
                    </div>
                    <div class="col-sm-4">
                        <h3 class="intro-heading"> HÌNH THỨC HỌC </h3>
                    </div>
                    <div class="col-sm-1 line">
                        <hr/>
                    </div>
                    <div class="col-sm-3">
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-3">
                        <div class="circle-method circle-calendar"><i class="icon-calendar"></i> </div>
                        <div class="study-method-text">
                            <h4>Học mọi lúc mọi nơi</h4>
                            <p>Học qua video bài giảng được biên tập chuyên nghiệp.  Tài liệu bao gồm sách và bài thuyết trình, tư liệu tham khảo</p>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="circle-method circle-question"><i class="icon-question"></i></div>
                        <div class="study-method-text">
                            <h4>Hỏi đáp cùng chuyên gia</h4>
                            <p>Cố vấn 1-2-1 (1 kèm 1) trong suốt quá trình học và luyện thi. Hỏi đáp cùng chuyên gia, doanh nhân có kinh nghiệm</p>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="circle-method circle-pencil"><i class="icon-pencil"></i></div>
                        <div class="study-method-text">
                            <h4>Hỏi đáp và luyện thi</h4>
                            <p>Học đi đôi với hành trong suốt quá trình học. Rèn luyện cùng hơn 100 bài thi thử</p>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="circle-method circle-house"><i class="icon-house"></i></div>
                        <div class="study-method-text">
                            <h4>Thi trực tuyến và nhận bằng quốc tế</h4>
                            <p>Thi trực tuyến mọi lúc mọi nơi. 6 tháng để rèn luyện thoải mái trước khi thi</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="registration" id="regis">
            <div class="container resgistration-border">
                <div class="row">
                    <div class="col-sm-3">
                    </div>
                    <div class="col-sm-1 line">
                        <hr/>
                    </div>
                    <div class="col-sm-4">
                        <h3 class="intro-heading"> ĐĂNG KÝ NHẬN TƯ VẤN </h3>
                    </div>
                    <div class="col-sm-1 line">
                        <hr/>
                    </div>
                    <div class="col-sm-3">
                    </div>
                </div>
                <form action="" class="form-horizontal" id="form_advice"  accept-charset="utf-8">
                    <div class="row">

                        <div class="text-intro-regist">
                            <br/>
                            <!--<p class="text">HỌC PHÍ: 3.480.000Đ (Bao gồm học liệu Online, sách và chi phí dự thi CBP)</p>-->
                            <p class="text-last"><span>NHẬN NGAY HỌC BỔNG DÀNH CHO 99 NGƯỜI ĐĂNG KÝ ĐẦU TIÊN: 1.000.000Đ</span></p>
                        </div>
                        <div class="regist-form">
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <input type="text" name="name" id="name" class="input-group form-control" required  placeholder="Họ Và Tên"/>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <input type="text" name="phone" id="phone" class="input-group form-control" required placeholder="Số Điện Thoại"/>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <input type="email" name="email" id="email" class="input-group form-control" required  placeholder="Email"/>
                                </div>
                            </div>
                        </div>
                        <div class="button-regist">
                            <button id="dang_ky_form" class="btn btn-primary btn-regist button-regis">ĐĂNG KÝ</button>
                        </div>
                        <div class="information-text">
                            <p>Bộ phận chăm sóc khách hàng của MANA Business School sẽ liên lạc sớm với bạn để tư vấn về khóa học (mức phí tư vấn là 0đ).</p>
                        </div>

                    </div>
                </form>
            </div>
        </div>
        <footer>
            <div class="container footer">
                <div class="row">
                    <div class="col-sm-3">
                        <div class="footer-logo">
                            <ul>
                                <li>
                                    <a href="#">
                                        <img src="/mana/cbp/khoa-hoc-ky-nang-giao-tiep/imgs/1_Logo.png" class="img-responsive"/>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <img src="/mana/cbp/khoa-hoc-ky-nang-giao-tiep/imgs/18_KynaLogo.png" class="img-responsive"/>
                                    </a>
                                </li>
                            </ul>

                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="footer-text-center">
                            <h4>Công ty Cổ phần  Dream Việt Education</h4>
                            <p> <b class="company-address"> Địa chỉ ĐKKD:</b>  <?php echo Setting::DIA_CHI_DKKD ?></p>
                            <p> <b class="company-address"> Văn phòng Hồ Chí Minh:</b>  <?php echo Setting::DIA_CHI_VAN_PHONG ?></p>
                            <p> <b class="company-address"> Văn phòng Hà Nội:</b>  <?php echo Setting::DIA_CHI_VAN_PHONG_HA_NOI ?> </p>
                            <p style="padding-top: 20px">Giấy phép ĐKKD số 0313589030 do Sở Kế hoạch và Đầu tư TPHCM cấp</p>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="footer-hot-line">
                            <p><b class="company-address">Hotline:</b>  <?=Setting::HOTLINE ?></p>
                            <p>Thứ 2 – thứ 6: từ 08h30 – 21h00</p>
                            <p>Thứ 7: 08h30 – 17h00</p>
                            <p><b class="company-address">Email:</b></p> hotro@kyna.vn
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <div class="modal fade" id="modal">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="popup_body">
                            <div class="alert alert-success" role="alert">Cám ơn bạn đã để lại thông tin. Kyna.vn sẽ sớm liên hệ với bạn để tư vấn thêm về chương trình học!</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="/mana/cbp/khoa-hoc-ky-nang-giao-tiep/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="/mana/cbp/khoa-hoc-ky-nang-giao-tiep/js/owl.carousel.min.js"></script>
        <script>
            $(function () {
                $('.logo-brand').data('size', 'big');
            });

            $(window).scroll(function () {
                if ($(document).scrollTop() > 0)
                {
                    if ($('.logo-brand').data('size') == 'big')
                    {
                        $('.logo-brand').data('size', 'small');
                        $('.logo-brand').stop().animate({
                            height: '40px'
                        }, 600);
                        $('.navbar').addClass('menufix');
                    }
                } else
                {
                    if ($('.logo-brand').data('size') == 'small')
                    {
                        $('.logo-brand').data('size', 'big');
                        $('.logo-brand').stop().animate({
                            height: '50px'
                        }, 600);

                        $('.navbar').removeClass('menufix');
                    }
                }
            });
        </script>
        <!-- Nhan them vao -->
        <script>
            $(document).ready(function () {
                $("#dang_ky_form").bind('click', function (e) {

                    if ($.trim($("#name").val()) != '' && $.trim($("#email").val()) != '' && $.trim($("#phone").val()) != '') {
                        e.preventDefault();
                        var url = 'https://docs.google.com/forms/d/e/1FAIpQLSe2jnSuhzJNNTpqJCU5-aJCKdT8KDesVUr19312OAOx1xDVTg/formResponse';
                        var data = {
                            'entry.1618386042': $("#name").val(),
                            'entry.1550829392': $("#email").val(),
                            'entry.1725316625': $("#phone").val(),
                            'entry.953023567': '<?php echo $utm_source; ?>',
                            'entry.823420574':'<?php echo $utm_medium;  ?>',
                            'entry.1198803187':'<?php echo $utm_campaign;  ?>'
                            };
                        $.ajax({
                            'url': url,
                            'method': 'POST',
                            'dataType': 'XML',
                            'data': data,
                            'statusCode': {
                                0: function () {
                                    $("#name").val('');
                                    $("#email").val('');
                                    $("#phone").val('');
                                    $("#modal").modal();
                                },
                                200: function () {
                                    $("#name").val('');
                                    $("#email").val('');
                                    $("#phone").val('');
                                    $("#modal").modal();
                                }
                            }

                        });
                    }

                });

                $(".wrapper-slider ul").owlCarousel({
                    autoPlay: true,
                    items: 1,
                    itemsDesktop: [1199, 1],
                    itemsDesktopSmall: [979, 1],
                    itemsTablet: [768, 1],
                    itemsMobile: [479, 1],
                    mouseDrag: true,
                    autoPlay: 40000,
                            navigation: false,
                    pagination: true,
                });
            });


            $(function () {
                $('.scroll-link a[href*=#]:not([href=#])').click(function () {
                    if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {

                        var target = $(this.hash);
                        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                        if ($(window).width() > 768) {
                            if (target.length) {
                                $('html,body').animate({
                                    scrollTop: target.offset().top - 70
                                }, 1000);
                                return false;
                            }
                        } else {
                            if (target.length) {
                                $('html,body').animate({
                                    scrollTop: target.offset().top
                                }, 1000);
                                return false;
                            }
                        }
                    }
                });
            });

        </script>
        <!--End of Zopim Live Chat Script-->
        <!--Start of Zopim Live Chat Script-->
<script type="text/javascript">
    window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
        d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
    _.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
        $.src="//v2.zopim.com/?1bMWnWprkt76mXJXRb6xBuP2dAD7uxPq";z.t=+new Date;$.
            type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
</script>
<!--End of Zopim Live Chat Script-->


<!-- Google Tag Manager (noscript) --> 
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-M92WKP"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

    </body>

    <!-- Mirrored from mana.edu.vn/cbp/khoa-hoc-ky-nang-giao-tiep/ by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 27 Apr 2016 13:31:05 GMT -->
</html>
