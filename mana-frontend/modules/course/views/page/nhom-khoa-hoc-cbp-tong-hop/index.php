<?php

use mana\models\Setting;

$dir_name = dirname(dirname(dirname(__FILE__)));
?>
<!DOCTYPE html>
<html lang="en">

    <!-- Mirrored from mana.edu.vn/cbp/khoa-hoc-ky-nang-giao-tiep/ by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 27 Apr 2016 13:29:41 GMT -->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Đào tạo Chứng chỉ Kinh doanh Chuyên nghiệp - Mana.edu.vn</title>
        <meta name="description" content="Chương trình được thiết kế bởi Hiệp hội Đào tạo Kinh doanh Quốc tế (IBTA) – Hoa Kỳ. Đăng ký chương trình học tại MANA, cơ hội chinh phục mọi đỉnh cao nghề nghiệp trong tầm tay.">
        <link href='https://fonts.googleapis.com/css?family=Roboto:400,300,500,700' rel='stylesheet' type='text/css'>
        <!-- Bootstrap -->
        <link href="/mana/cbp/nhom-khoa-hoc-cbp-tong-hop/css/bootstrap.min.css" rel="stylesheet"/>
        <link href="/mana/cbp/nhom-khoa-hoc-cbp-tong-hop/css/site.css" rel="stylesheet"/>
        <link href="/mana/cbp/nhom-khoa-hoc-cbp-tong-hop/css/owl.carousel.css" rel="stylesheet"/>
        <link href="/mana/cbp/nhom-khoa-hoc-cbp-tong-hop/css/owl.theme.css" rel="stylesheet"/>
        <link href="/mana/cbp/nhom-khoa-hoc-cbp-tong-hop/css/owl.transitions.css" rel="stylesheet"/>
        <link href="/mana/cbp/nhom-khoa-hoc-cbp-tong-hop/css/font-awesome.min.css" rel="stylesheet"/>
        <link rel="icon" href="/mana/images/manaFavicon.png">
    </head>
    <body>
        <!-- Google Tag Manager -->
        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-M92WKP');</script>
        <!-- End Google Tag Manager -->
        <?php
        // get các tham số cần thiết */
        $getParams = $_GET;
        $utm_source = '';
        $utm_medium = '';
        $utm_campaign = '';

        if (isset($getParams['utm_source']) && trim($getParams['utm_source']) != '') {
            $utm_source = trim($getParams['utm_source']);
        }
        if (isset($getParams['utm_medium']) && trim($getParams['utm_medium']) != '') {
            $utm_medium = trim($getParams['utm_medium']);
        }
        if (isset($getParams['utm_campaign']) && trim($getParams['utm_campaign']) != '') {
            $utm_campaign = trim($getParams['utm_campaign']);
        }
        ?>
        <nav class="navbar navbar-inverse header-menu navbar-fixed-top scroll-link">
            <div class="container">
                <div class="col-sm-3">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                                data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="https://mana.edu.vn/">
                            <img class="logo-brand" src="/mana/cbp/nhom-khoa-hoc-cbp-tong-hop/imgs/1_Logo.png"/>
                        </a>
                        <a href="#register" class="button mb">ĐĂNG KÝ NGAY</a>
                    </div>
                </div>
                <div class="col-sm-9 col-xs-12">
                    <div id="navbar" class="navbar-collapse collapse">
                        <ul class="nav navbar-nav main-menu">
                            <li><a href="#about">GIỚI THIỆU</a></li>
                            <li><a href="#courses">HỌC PHẦN</a></li>
                            <li><a href="#route">HÌNH THỨC HỌC</a></li>
                            <li><a href="#register">ĐĂNG KÝ</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </nav>

        <section id="banner">
            <div class="wrapper-slider scroll-link">
                <ul>
                    <li>
                        <div class="item">
                            <div class="container">
                                <div class="carousel-caption">
                                    <h3>Chinh phục mọi đỉnh cao nghề nghiệp</h3>
                                    <h5>Với chứng chỉ CBP (*)</h5>
                                    <a class="btn btn-lg btn-primary btn-regis-slider" href="#courses" role="button">XEM KHÓA</a>
                                    <p>(*): Chứng chỉ CBP (Certified Business Professional) là chứng chỉ Kinh doanh chuyên nghiệp được cấp bởi <br/>Hiệp hội đào tạo kinh doanh quốc tế IBTA (International Business Training Association).<br/>Chứng chỉ CBP được công nhận trên toàn cầu.</p>
                                </div>
                            </div>
                        </div><!--end .item-->
                    </li>
                </ul>
            </div><!--end .wrapper-slider-->
        </section><!--end #banner-->

        <section id="about">
            <div class="container">
                <div class="col-sm-7 col-xs-12 text">
                    <ul class="first">
                        <li>- Chứng chỉ CBP là <span class="bold">một trong 8 chứng chỉ kinh doanh</span> được số đông sinh viên Kinh tế và nhân viên Kinh doanh <span class="bold">tại Mỹ theo đuổi.</span></li>
                        <li><span class="bold">- Honda, Nokia, Rocoh, Adidas, Motorola, Ford, Intel, PriceWaterHouseCooper,</span>… là những thương hiệu đã công nhận chứng chỉ CBP như một tiêu chuẩn đánh giá nhân sự.</li>
                        <li>- Chứng chỉ CBP cung cấp cho học viên những <span class="bold">kiến thức và kỹ năng tốt nhất/ mới nhất</span> trong lĩnh vực kinh doanh từ nền tảng đến cao cấp:
                            <div class="wrap">
                                <div class="col-sm-6 col-xs-12 box">
                                    <ul>
                                        <li>CBP Leadership</li>
                                        <li>CBP Customer Service</li>
                                        <li>CBP Sales</li>
                                    </ul>
                                </div><!--end .box-->
                                <div class="col-sm-6 col-xs-12 box">
                                    <ul>
                                        <li>CBP Business Etiquette</li>
                                        <li>CBP Business Communication</li>
                                        <li>CBP Executive</li>
                                    </ul>
                                </div><!--end .box-->
                            </div><!--end .wrap-->
                        </li>
                        <li>- Các công ty đa quốc gia, tập đoàn lớn trên thế giới sử dụng chương trình CBP như một <span class="bold">chương trình đào tạo nhân sự nội bộ</span>, đồng thời lấy tiêu chuẩn CBP để đánh giá nhân sự đầu vào.</li>
                    </ul>
                </div><!--end .text-->
                <div class="col-sm-5 col-xs-12 img">
                    <ul>
                        <li><img src="/mana/cbp/nhom-khoa-hoc-cbp-tong-hop/imgs/about1.png" alt="" class="img-responsive"></li>
                        <li><img src="/mana/cbp/nhom-khoa-hoc-cbp-tong-hop/imgs/about2.png" alt="" class="img-responsive"></li>
                    </ul>
                </div><!--end .img-->
            </div><!--end .container-->
        </section>

        <section id="courses">
            <div class="container">
                <div class="row title-main">
                    <div class="col-md-2 col-sm-1"></div>
                    <div class="col-md-1 col-sm-1 line"><hr/></div>
                    <div class="col-md-6 col-sm-8">
                        <h3 class="intro-heading"> CÁC KHÓA ĐÀO TẠO CẤP CHỨNG CHỈ CBP<br/>
                            TẠI MANA ONLINE BUSINESS SCHOOL </h3></div>
                    <div class="col-md-1 col-sm-1 line"><hr/></div>
                    <div class="col-md-2 col-sm-1"></div>
                </div>
                <div class="wrap-content">
                    <ul class="wrap-list nav nav-tabs" role="tablist">
                        <li class="list">
                            <div class="title">
                                <h2><span class="bold">01</span> CBP<span class="sub">TM</span> Leadership - Kỹ năng lãnh đạo hiệu quả</h2>
                            </div><!--end .title-->
                            <div class="col-sm-4 col-xs-12 img">
                                <img src="/mana/cbp/nhom-khoa-hoc-cbp-tong-hop/imgs/hothithanhvan.png" alt="Thạc sỹ Hồ Thị Thanh Vân" class="img-responsive">
                                <h3>Thạc sỹ <span>Hồ Thị Thanh Vân</span></h3>
                                <ul>
                                    <li>Chuyên ngành Marketing Bán hàng và Dịch vụ (MMSS) tại trường Đại học Paris 1 Sorbonne (IAE) phối hợp với trường Quản trị Châu Âu (ESCP - EAP European School Management).</li>
                                    <li>20 năm kinh nghiệm trong lĩnh vực Sales và Marketing tại các công ty hàng đầu thế giới.</li>
                                </ul>
                            </div><!--end .video-->
                            <div class="col-sm-8 col-xs-12 content">
                                <p class="text">Cung cấp đầy đủ các kiến thức và kỹ năng tổ chức, xây dựng đội nhóm.</p>
                                <h4>Nội dung</h4>
                                <ul>
                                    <li>Hiểu một nhà lãnh đạo thực sự là như thế nào</li>
                                    <li>Vai trò và trách nhiệm của một người lãnh đạo</li>
                                    <li>Tạo được phong cách lãnh đạo cho bản thân</li>
                                    <li>Biết cách ra được quyết định hiệu quả</li>
                                    <li>Biết cách xây dựng đội nhóm thực hiệu quả</li>
                                    <li>Hiểu và biết cách động viên, khích lệ tinh thần nhóm</li>
                                </ul>
                                <div class="price">
                                    <p>Học phí: <span class="color-price">4.700.000Đ</span> (Bao gồm học liệu Online, sách và chi phí dự thi CBP)</p>
                                    <h4>Ưu đãi giảm: 1.000.000Đ chỉ còn <span class="color-price">3.700.000Đ</span></h4>
                                    <span class="button-register" for="#CBP1"><a href="#">Đăng ký giữ ưu đãi</a></span>
                                </div><!--end .price-->
                            </div><!--end .content-->
                        </li>

                        <li class="list">
                            <div class="title">
                                <h2><span class="bold">02</span> CBP<span class="sub">TM</span> Customer Service – Kỹ năng chăm sóc và thấu hiểu khách hàng</h2>
                            </div><!--end .title-->
                            <div class="col-sm-4 col-xs-12 img">
                                <img src="/mana/cbp/nhom-khoa-hoc-cbp-tong-hop/imgs/nguyenkientri.png" alt="Thạc sỹ Nguyễn Kiên Trì" class="img-responsive">
                                <h3>Thạc sỹ <span>Nguyễn Kiên Trì</span></h3>
                                <ul>
                                    <li>MBA Tư vấn Quản lý Quốc tế - Thuỵ Sỹ, Expert Economic Certified Đại học California.</li>
                                    <li>Nhiều năm kinh nghiệm trong việc quản trị và giảng dạy, hiện đang nắm giữ các vị trí cấp cao tại các tập đoàn lớn.</li>
                                </ul>
                            </div><!--end .video-->
                            <div class="col-sm-8 col-xs-12 content">
                                <p class="text">Cung cấp kiến thức nền tảng kiến thức và tập trung vào việc xây dựng mối quan hệ khách hàng lâu dài.<br/>Chương trình gồm nhiều tình huống tương tác và thực nghiệm thực tế</p>
                                <h4>Nội dung</h4>
                                <ul>
                                    <li>Tổng quan về dịch vụ chăm sóc khách hàng</li>
                                    <li>Kỹ năng giao tiếp với khách hàng</li>
                                    <li>Phân tích khách hàng và giải quyết xung đột với khách hàng</li>
                                    <li>Chăm sóc khách hàng qua điện thoại/ internet</li>
                                    <li>Chiến lược quản lý thời gian</li>
                                    <li>Chiến lược quản lý sự căng thẳng</li>
                                </ul>
                                <div class="price">
                                    <p>Học phí: <span class="color-price">3.800.000Đ</span> (Bao gồm học liệu Online, sách và chi phí dự thi CBP)</p>
                                    <h4>Ưu đãi giảm: 1.000.000Đ chỉ còn <span class="color-price">2.800.000Đ</span></h4>
                                    <span class="button-register see-promotion" for="#CBP2"><a href="#">Đăng ký giữ ưu đãi</a></span>
                                </div><!--end .price-->
                            </div><!--end .content-->
                        </li>

                        <li class="list">
                            <div class="title">
                                <h2><span class="bold">03</span> CBP<span class="sub">TM</span> Selling Skills – Kỹ năng bán hàng chuyên nghiệp</h2>
                            </div><!--end .title-->
                            <div class="col-sm-4 col-xs-12 img">
                                <img src="/mana/cbp/nhom-khoa-hoc-cbp-tong-hop/imgs/nguyenngoan.png" alt="Thạc sỹ Nguyễn Ngoan" class="img-responsive">
                                <h3>Thạc sỹ <span>Nguyễn Ngoan</span></h3>
                                <ul>
                                    <li>Phó tổng Giám đốc – Star Travel International.</li>
                                    <li>Chủ tịch MANDA MIND Corporation.</li>
                                </ul>
                            </div><!--end .video-->
                            <div class="col-sm-8 col-xs-12 content">
                                <p class="text">Cung cấp phương pháp tìm kiếm khách hàng tiềm năng và xây dựng được chiến lược bán hàng thành công.</p>
                                <h4>Nội dung</h4>
                                <ul>
                                    <li>Nền tảng của việc bán hàng và những cách bán hàng phổ biến</li>
                                    <li>Phương pháp tìm kiếm khách hàng tiềm năng</li>
                                    <li>Các chiến lược hiệu quả trong lần đầu tiếp xúc với khách hàng (First Contact Stage)</li>
                                    <li>Các chiến lược hiệu quả trong giai đoạn đánh giá chất lượng khách hàng (Qualification stage)</li>
                                    <li>Các chiến lược hiệu quả trong giai đoạn thuyết trình bán hàng (Presentation Stage)</li>
                                    <li>Phương pháp giải quyết sự từ chối của khách hàng (Objection Resolution Stage)</li>
                                    <li>Chiến lược chốt Sales thành công (Closing stage)</li>
                                    <li>Chiến lược kết thúc và theo dõi sau bán hàng (Wrap – up và Follow – up Stage)</li>
                                </ul>
                                <div class="price">
                                    <p>Học phí: <span class="color-price">4.700.000Đ</span> (Bao gồm học liệu Online, sách và chi phí dự thi CBP)</p>
                                    <h4>Ưu đãi giảm: 1.000.000Đ chỉ còn <span class="color-price">3.700.000Đ</span></h4>
                                    <span class="button-register" for="#CBP3"><a href="#">Đăng ký giữ ưu đãi</a></span>
                                </div><!--end .price-->
                            </div><!--end .content-->
                        </li>

                        <li class="list">
                            <div class="title">
                                <h2><span class="bold">04</span> CBP<span class="sub">TM</span> Business Communication – Kỹ năng giao tiếp trong kinh doanh</h2>
                            </div><!--end .title-->
                            <div class="col-sm-4 col-xs-12 img">
                                <img src="/mana/cbp/nhom-khoa-hoc-cbp-tong-hop/imgs/hothithanhvan.png" alt="Thạc sỹ Hồ Thị Thanh Vân" class="img-responsive">
                                <h3>Thạc sỹ <span>Hồ Thị Thanh Vân</span></h3>
                                <ul>
                                    <li>Chuyên ngành Marketing Bán hàng và Dịch vụ (MMSS) tại trường Đại học Paris 1 Sorbonne (IAE) phối hợp với trường Quản trị Châu  u (ESCP - EAP European School Management).</li>
                                    <li>20 năm kinh nghiệm trong lĩnh vực Sales và Marketing tại các công ty hàng đầu thế giới.</li>
                                </ul>
                            </div><!--end .video-->
                            <div class="col-sm-8 col-xs-12 content">
                                <p class="text">Cung cấp các nguyên tắc để vận dụng chuyên nghiệp trong từng hình thức giao tiếp và cách xử lý các mâu thuẫn phát sinh trong quá trình giao tiếp.</p>
                                <h4>Nội dung</h4>
                                <ul>
                                    <li>Làm quen với cấu trúc giao tiếp trong kinh doanh</li>
                                    <li>Phát triển phong cách viết trong kinh doanh </li>
                                    <li>Phát triển kỹ năng giao tiếp ngôn từ</li>
                                    <li>Giao tiếp và trao đổi bằng điện thoại trong kinh doanh</li>
                                    <li>Giao tiếp phi ngôn từ và kỹ năng thuyết trình hiệu quả</li>
                                    <li>Mâu thuẫn xung đột và những bất đồng trong giao tiếp kinh doanh</li>
                                </ul>
                                <div class="price">
                                    <p>Học phí: <span class="color-price">3.480.000Đ</span> (Bao gồm học liệu Online, sách và chi phí dự thi CBP)</p>
                                    <h4>Ưu đãi giảm: 1.000.000Đ chỉ còn <span class="color-price">2.480.000Đ</span></h4>
                                    <span class="button-register" for="#CBP4"><a href="#">Đăng ký giữ ưu đãi</a></span>
                                </div><!--end .price-->
                            </div><!--end .content-->
                        </li>

                        <li class="list">
                            <div class="title">
                                <h2><span class="bold">05</span> CBP<span class="sub">TM</span> Business Etiquette – Nghi thức giao tiếp trong kinh doanh</h2>
                            </div><!--end .title-->
                            <div class="col-sm-4 col-xs-12 img">
                                <img src="/mana/cbp/nhom-khoa-hoc-cbp-tong-hop/imgs/nguyenkientri.png" alt="Thạc sỹ Nguyễn Kiên Trì" class="img-responsive">
                                <h3>Thạc sỹ <span>Nguyễn Kiên Trì</span></h3>
                                <ul>
                                    <li>MBA Tư vấn Quản lý Quốc tế - Thuỵ Sỹ, Expert Economic Certified Đại học California.</li>
                                    <li>Nhiều năm kinh nghiệm trong việc quản trị và giảng dạy, hiện đang nắm giữ các vị trí cấp cao tại các tập đoàn lớn.</li>
                                </ul>
                            </div><!--end .video-->
                            <div class="col-sm-8 col-xs-12 content">
                                <p class="text">Trang bị những kỹ năng giao tiếp cần thiết để làm chủ tình huống và đạt thành công trong công việc.</p>
                                <h4>Nội dung</h4>
                                <ul>
                                    <li>Hội họp và các nghi thức xã giao trong phòng họp</li>
                                    <li>Các nguyên tắc đạo đức trong kinh doanh</li>
                                    <li>Nghi thức giao tiếp khi chiêu đãi đối tác và quy tắc ăn uống tại các quốc gia</li>
                                    <li>Nghi thức giao tiếp và quy tắc ứng xử với khách hàng qua điện thoại</li>
                                    <li>Quy tắc sử dụng mạng và thư điện tử</li>
                                    <li>Trang phục trong kinh doanh và tính chuyên nghiệp</li>
                                    <li>Quy tắc ứng xử với người khuyết tật</li>
                                    <li>Thách thức trong môi trường đa văn hóa</li>
                                </ul>
                                <div class="price">
                                    <p>Học phí: <span class="color-price">3.480.000Đ</span> (Bao gồm học liệu Online, sách và chi phí dự thi CBP)</p>
                                    <h4>Ưu đãi giảm: 1.000.000Đ chỉ còn <span class="color-price">2.480.000Đ</span></h4>
                                    <span class="button-register" for="#CBP5"><a href="#">Đăng ký giữ ưu đãi</a></span>
                                </div><!--end .price-->
                            </div><!--end .content-->
                        </li>

                        <li class="list">
                            <div class="title">
                                <h2><span class="bold">06</span> CBP Executive – Đào tạo quản lý cấp trung</h2>
                            </div><!--end .title-->
                            <div class="col-sm-4 col-xs-12 img">
                                <ul class="wrap-list-img-first">
                                    <li class="img-list-sub">
                                        <img src="/mana/cbp/nhom-khoa-hoc-cbp-tong-hop/imgs/nguyenkientri.png" alt="Thạc sỹ Nguyễn Kiên Trì" class="img-responsive">
                                        <h3>Thạc sỹ <span>Nguyễn Kiên Trì</span></h3>
                                        <ul>
                                            <li>MBA Tư vấn Quản lý Quốc tế - Thuỵ Sỹ, Expert Economic Certified Đại học California.</li>
                                            <li>Nhiều năm kinh nghiệm trong việc quản trị và giảng dạy, hiện đang nắm giữ các vị trí cấp cao tại các tập đoàn lớn.</li>
                                        </ul>
                                    </li>
                                    <li class="img-list-sub">
                                        <img src="/mana/cbp/nhom-khoa-hoc-cbp-tong-hop/imgs/hothithanhvan.png" alt="Thạc sỹ Hồ Thị Thanh Vân" class="img-responsive">
                                        <h3>Thạc sỹ <span>Hồ Thị Thanh Vân</span></h3>
                                        <ul>
                                            <li>Chuyên ngành Marketing Bán hàng và Dịch vụ (MMSS) tại trường Đại học Paris 1 Sorbonne (IAE) phối hợp với trường Quản trị Châu Âu (ESCP - EAP European School Management).</li>
                                            <li>20 năm kinh nghiệm trong lĩnh vực Sales và Marketing tại các công ty hàng đầu thế giới.</li>
                                        </ul>
                                    </li>
                                    <li class="img-list-sub">
                                        <img src="/mana/cbp/nhom-khoa-hoc-cbp-tong-hop/imgs/nguyenngoan.png" alt="Thạc sỹ Nguyễn Ngoan" class="img-responsive">
                                        <h3>Thạc sỹ <span>Nguyễn Ngoan</span></h3>
                                        <ul>
                                            <li>Phó tổng Giám đốc – Star Travel International.</li>
                                            <li>Chủ tịch MANDA MIND Corporation.</li>
                                        </ul>
                                    </li>
                                </ul>

                            </div><!--end .video-->
                            <div class="col-sm-8 col-xs-12 content">
                                <p class="text">Để đạt được CBP Executive, người học cần đạt được 5 chứng chỉ CBPe như trên bao gồm: CBPe Leadership, Customer Service, Sales, Business Communication, Business Etiquette.</p>
                                <h4>Nội dung</h4>
                                <ul>
                                    <li>Kỹ năng lãnh đạo chuyên nghiệp</li>
                                    <li>Kỹ năng thấu hiểu và chăm sóc khách hàng</li>
                                    <li>Kỹ năng sales và thuyết phục khách hàng</li>
                                    <li>Kỹ năng giao tiếp trong kinh doanh</li>
                                    <li>Các nghi thức xã giao trong kinh doanh</li>
                                </ul>
                                <div class="price">
                                    <p>Học phí: <span class="color-price">3.480.000Đ</span> (Bao gồm học liệu Online, sách và chi phí dự thi CBP)</p>
                                    <h4>Ưu đãi giảm: 1.000.000Đ chỉ còn <span class="color-price">2.480.000Đ</span></h4>
                                    <span class="button-register" for="#CBP6"><a href="#">Đăng ký giữ ưu đãi</a></span>
                                </div><!--end .price-->
                            </div><!--end .content-->
                        </li>
                    </ul>

                </div><!--end .content-->
            </div><!--end .container-->
        </section>

        <section id="conquer">
            <div class="container">
                <div class="col-md-4 col-sm-2 col-xs-12">
                </div>
                <div class="col-md-8 col-sm-12 col-xs-12 text">
                    <h2>CHINH PHỤC MỌI ĐỈNH CAO NGHỀ NGHIỆP</h2>
                    <h4>Với chứng chỉ CBP</h4>
                    <ul>
                        <li>
                            <div class="media">
                                <div class="media-left">
                                    <i class="fa fa-check" aria-hidden="true"></i>
                                </div>
                                <div class="media-body">
                                    Là chứng chỉ kinh doanh chuyên nghiệp do Hiệp hội IBTA cấp.
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="media">
                                <div class="media-left">
                                    <i class="fa fa-check" aria-hidden="true"></i>
                                </div>
                                <div class="media-body">
                                    Giáo trình gốc theo bản quyền của IBTA.
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="media">
                                <div class="media-left">
                                    <i class="fa fa-check" aria-hidden="true"></i>
                                </div>
                                <div class="media-body">
                                    Có cố vấn học tập 1 kèm 1.
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="media">
                                <div class="media-left">
                                    <i class="fa fa-check" aria-hidden="true"></i>
                                </div>
                                <div class="media-body">
                                    Thi trực tuyến trên hệ thống khảo thí Prometric.
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="media">
                                <div class="media-left">
                                    <i class="fa fa-check" aria-hidden="true"></i>
                                </div>
                                <div class="media-body">
                                    Người học được quyền chuẩn bị và ôn luyện trước khi thi tối đa 6 tháng.
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="media">
                                <div class="media-left">
                                    <i class="fa fa-check" aria-hidden="true"></i>
                                </div>
                                <div class="media-body">
                                    Học phần bao gồm: học liệu Online, sách và tài liệu, chi phí dự thi CBP.
                                </div>
                            </div>
                        </li>
                    </ul>


                </div><!--end .text-->
            </div><!--end .container-->
        </section>

        <section id="route">
            <div class="container">
                <h2>LỘ TRÌNH HỌC ONLINE TẠI MANA</h2>
                <ul>
                    <li>
                        <div class="col-md-8 col-xs-12 img">
                            <img src="/mana/cbp/nhom-khoa-hoc-cbp-tong-hop/imgs/route-line-1.png" alt="Học trực tuyến các bài giảng Video" class="img-responsive">
                        </div><!--end .img-->
                        <div class="col-md-4 col-xs-12 text">Học trực tuyến các bài giảng Video </div>
                    </li>
                    <li>
                        <div class="col-md-8 col-xs-12 img">
                            <img src="/mana/cbp/nhom-khoa-hoc-cbp-tong-hop/imgs/route-line-2.png" alt="Thực hành hàng tuần" class="img-responsive">
                        </div><!--end .img-->
                        <div class="col-md-4 col-xs-12 text">Thực hành hàng tuần</div>
                    </li>
                    <li>
                        <div class="col-md-8 col-xs-12 img">
                            <img src="/mana/cbp/nhom-khoa-hoc-cbp-tong-hop/imgs/route-line-3.png" alt="Thảo luận trên Facebook Group" class="img-responsive">
                        </div><!--end .img-->
                        <div class="col-md-4 col-xs-12 text">Thảo luận trên Facebook Group</div>
                    </li>
                    <li>
                        <div class="col-md-8 col-xs-12 img">
                            <img src="/mana/cbp/nhom-khoa-hoc-cbp-tong-hop/imgs/route-line-4.png" alt="Thi trực tuyến trên hệ thống khảo thí Prometric" class="img-responsive">
                        </div><!--end .img-->
                        <div class="col-md-4 col-xs-12 text">Thi trực tuyến trên hệ thống khảo thí Prometric</div>
                    </li>
                    <li>
                        <div class="col-md-8 col-xs-12 img">
                            <img src="/mana/cbp/nhom-khoa-hoc-cbp-tong-hop/imgs/route-line-5.png" alt="Nhận bằng Quốc tế" class="img-responsive">
                        </div><!--end .img-->
                        <div class="col-md-4 col-xs-12 text">Nhận bằng Quốc tế</div>
                    </li>
                </ul>
            </div><!--end .container-->
        </section>

        <section id="register">
            <div class="container">
                <div class="wrap-register">
                    <div class="row">
                        <div class="col-sm-3">
                        </div>
                        <div class="col-sm-1 line">
                            <hr/>
                        </div>
                        <div class="col-sm-4">
                            <h3 class="intro-heading"> ĐĂNG KÝ NHẬN TƯ VẤN </h3>
                        </div>
                        <div class="col-sm-1 line">
                            <hr/>
                        </div>
                        <div class="col-sm-3">
                        </div>
                    </div>

                    <div>
                        <!-- Nav tabs -->
                        <form action="#" class="form-horizontal form-cbp" id="form_cbp" method="" accept-charset="utf-8">
                            <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation" class="active" data-value="Khóa 1: Leadership - Kỹ năng lãnh đạo hiệu quả"><a href="#CBP1" aria-controls="home" role="tab" data-toggle="tab">Khóa 1. CBP<span class="sub">TM</span> Leadership – Kỹ năng lãnh đạo hiệu quả</a></li>
                                <li role="presentation" data-value="Khóa 2: Customer Service - Kỹ năng chăm sóc khách hàng"><a href="#CBP2" aria-controls="profile" role="tab" data-toggle="tab">Khóa 2. CBP<span class="sub">TM</span> Customer Service – Kỹ năng chăm sóc và thấu hiểu khách</a></li>
                                <li role="presentation" data-value="Khóa 3: Selling Skills - Kỹ năng bán hàng"><a href="#CBP3" aria-controls="messages" role="tab" data-toggle="tab">Khóa 3. CBP<span class="sub">TM</span> Sales – Kỹ năng bán hàng chuyên nghiệp</a></li>
                                <li role="presentation" data-value="Khóa 4: Business Etiquette - Nghi thức giao tiếp trong kinh doanh"><a href="#CBP4" aria-controls="settings" role="tab" data-toggle="tab">Khóa 4. CBP<span class="sub">TM</span> Business Communication – Kỹ năng giao tiếp trong kinh doanh</a></li>
                                <li role="presentation" data-value="Khóa 5: Business Communication - Kỹ năng giao tiếp trong kinh doanh"><a href="#CBP5" aria-controls="settings" role="tab" data-toggle="tab">Khóa 5. CBP<span class="sub">TM</span> Business Etiquette – Nghi thức giao tiếp trong kinh doanh</a></li>
                                <li role="presentation" data-value="Khóa 6: CBP Executive - Quản lý cấp trung"><a href="#CBP6" aria-controls="settings" role="tab" data-toggle="tab">Khóa 6. CBP Executive – Đào tạo quản lý cấp trung</a></li>
                            </ul>

                            <div class="regist-form">
                                <ul>
                                    <li>
                                        <div class="form-group">
                                            <input type="text" id="name" class="input-group form-control" required  placeholder="Họ Và Tên"/>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="form-group">
                                            <input type="text" id="phone" class="input-group form-control" required placeholder="Số Điện Thoại"/>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="form-group">
                                            <input type="text" id="email" class="input-group form-control" required  placeholder="Email"/>
                                        </div>
                                    </li>
                                </ul>
                            </div>

                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane active" id="CBP1">

                                    <div class="row">
                                        <div class="text-intro-regist">
                                            <p class="text">HỌC PHÍ: <span>4.700.000Đ</span> (Bao gồm học liệu Online, sách và chi phí dự thi CBP)</p>
                                            <p class="text-last">ƯU ĐÃI CHỈ CÒN <span>3.700.000Đ</span></p>
                                        </div>
                                    </div>
                                </div><!-- end #CBP1 -->

                                <div role="tabpanel" class="tab-pane" id="CBP2">

                                    <div class="row">
                                        <div class="text-intro-regist">
                                            <p class="text">HỌC PHÍ: <span>3.800.000Đ</span> (Bao gồm học liệu Online, sách và chi phí dự thi CBP)</p>
                                            <p class="text-last">ƯU ĐÃI CHỈ CÒN <span>2.800.000Đ</span></p>
                                        </div>
                                    </div>
                                </div>

                                <div role="tabpanel" class="tab-pane" id="CBP3">

                                    <div class="row">
                                        <div class="text-intro-regist">
                                            <p class="text">HỌC PHÍ: <span>4.700.000Đ</span> (Bao gồm học liệu Online, sách và chi phí dự thi CBP)</p>
                                            <p class="text-last">ƯU ĐÃI CHỈ CÒN <span>3.700.000Đ</span></p>
                                        </div>
                                    </div>
                                </div>

                                <div role="tabpanel" class="tab-pane" id="CBP4">

                                    <div class="row">
                                        <div class="text-intro-regist">
                                            <p class="text">HỌC PHÍ: <span>3.480.000Đ</span> (Bao gồm học liệu Online, sách và chi phí dự thi CBP)</p>
                                            <p class="text-last">ƯU ĐÃI CHỈ CÒN <span>2.480.000Đ</span></p>
                                        </div>
                                    </div>
                                </div>

                                <div role="tabpanel" class="tab-pane" id="CBP5">

                                    <div class="row">
                                        <div class="text-intro-regist">
                                            <p class="text">HỌC PHÍ: <span>3.480.000Đ</span> (Bao gồm học liệu Online, sách và chi phí dự thi CBP)</p>
                                            <p class="text-last">ƯU ĐÃI CHỈ CÒN <span>2.480.000Đ</span></p>
                                        </div>
                                    </div>
                                </div>

                                <div role="tabpanel" class="tab-pane" id="CBP6">

                                    <div class="row">
                                        <div class="text-intro-regist">
                                            <p class="text">HỌC PHÍ: <span>15.000.000Đ</span> (Bao gồm học liệu Online, sách và chi phí dự thi CBP)</p>
                                            <p class="text-last">ƯU ĐÃI CHỈ CÒN <span>12.500.000Đ</span></p>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div class="button-regist">
                                <button type="submit"  id="form_cbp_button" class="btn btn-primary btn-regist button-regis">ĐĂNG KÝ</button>
                            </div>
                        </form>
                    </div>
                    <p class="text-form-bottom">Mọi thông tin đăng ký của bạn tại đây sẽ được bảo mật hoàn toàn. Bộ phận CSKH của MANA sẽ sớm liên hệ với bạn để tư vấn chi tiết hơn về khóa học.</p>
                </div><!--end .wrap-register-->

            </div><!--end .container-->
        </section>

        <section id="about-mana">
            <div class="container">
                <div class="col-sm-4 col-xs-12 img">
                    <img src="/mana/cbp/nhom-khoa-hoc-cbp-tong-hop/imgs/logo-mana.png" alt="Mana" class="img-responsive">
                </div><!--end .img-->
                <div class="col-sm-8 col-xs-12 text">
                    <ul>
                        <li>MANA Online Business School là một sản phẩm trực thuộc hệ sinh thái giáo dục trực tuyến (e - learning) KYNA GROUP.</li>
                        <li>MANA chuyên đào tạo các kỹ năng kinh doanh chuyên nghiệp. Trong đó, nội dung học được MANA biên soạn hoặc chuyển ngữ từ các chương trình học của các tổ chức, đại học quốc tế nổi tiếng.</li>
                        <li>MANA tự hào khi trở thành Viện đào tạo Quản trị Kinh doanh trực tuyến hàng đầu Việt Nam hiện nay. MANA mang đến các chương trình đào tạo kỹ năng quản trị kinh doanh có tính ứng dụng cao.</li>
                        <li>Tại MANA, học viên sẽ được học các chương trình đào tạo song ngữ với đội ngũ cố vấn học tập 24/7.</li>
                        <li>Hình thức và phương pháp học khoa học tại MANA sẽ mang lại cho học viên những trải nghiệm học tập mới mẻ và hữu ích.</li>
                        <li>MANA Online Business School – Nâng tầm sự nghiệp của bạn.</li>
                    </ul>
                </div><!--end .text-->
            </div><!--end .container-->
        </section>

        <footer>
            <div class="container footer">
                <div class="row">
                    <div class="col-sm-3">
                        <div class="footer-logo">
                            <ul>
                                <li>
                                    <a href="https://mana.edu.vn/">
                                        <img src="/mana/cbp/nhom-khoa-hoc-cbp-tong-hop/imgs/1_Logo.png" class="img-responsive"/>
                                    </a>
                                </li>
                                <li>
                                    <a href="https://kyna.vn/">
                                        <img src="/mana/cbp/nhom-khoa-hoc-cbp-tong-hop/imgs/18_KynaLogo.png" class="img-responsive"/>
                                    </a>
                                </li>
                            </ul>

                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="footer-text-center">
                            <h4>Công ty Cổ phần  Dream Việt Education</h4>
                            <p> <b class="company-address"> Trụ sở chính:</b>  Lầu 6 Tòa nhà Thịnh Phát - 178/8 Đường D1, Phường 25, Quận Bình Thạnh, TP Hồ Chí Minh</p>
                            <p> <b class="company-address"> Văn phòng Hà Nội:</b>  Phòng 604 tháp A, Hà Thành Plaza 102 Thái Thịnh, Đống Đa, TP Hà Nội </p>
                            <p style="padding-top: 20px">Giấy phép ĐKKD số 0313589030 do Sở Kế hoạch và Đầu tư TPHCM cấp</p>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="footer-hot-line">
                            <p><b class="company-address">Hotline:</b>  1900 6364 09</p>
                            <p>Thứ 2 – thứ 6: từ 08h30 – 21h00</p>
                            <p>Thứ 7: 08h30 – 17h00</p>
                            <p><b class="company-address">Email:</b></p> hotro@kyna.vn
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <div class="modal fade" id="modal">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="popup_body">
                            <div class="alert alert-success" role="alert">Cám ơn bạn đã để lại thông tin. Kyna.vn sẽ sớm liên hệ với bạn để tư vấn thêm về chương trình học!</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="/mana/cbp/nhom-khoa-hoc-cbp-tong-hop/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="/mana/cbp/nhom-khoa-hoc-cbp-tong-hop/js/owl.carousel.min.js"></script>
        <script>
            $(function () {
                $('.logo-brand').data('size', 'big');
            });

            $(window).scroll(function () {
                if ($(document).scrollTop() > 0)
                {
                    if ($('.logo-brand').data('size') == 'big')
                    {
                        $('.logo-brand').data('size', 'small');
                        $('.logo-brand').stop().animate({
                            height: '40px'
                        }, 600);
                        $('.navbar').addClass('menufix');
                    }
                } else
                {
                    if ($('.logo-brand').data('size') == 'small')
                    {
                        $('.logo-brand').data('size', 'big');
                        $('.logo-brand').stop().animate({
                            height: '50px'
                        }, 600);

                        $('.navbar').removeClass('menufix');
                    }
                }
            });
        </script>
        <!-- Nhan them vao -->
        <script>
            $(document).ready(function () {
                $("#form_cbp_button").bind('click', function (e) {

                    if ($.trim($("#name").val()) != '' && $.trim($("#email").val()) != '' && $.trim($("#phone").val()) != '') {
                        e.preventDefault();
                        var url = 'https://docs.google.com/forms/d/e/1FAIpQLSes-YFn5zOMGk4GZpGAFEc3LB5WOKUeW42Rp95JDezuLybB5Q/formResponse';

                        var data = {
                            'entry.1618386042': $("#name").val(),
                            'entry.1550829392': $("#email").val(),
                            'entry.1725316625': $("#phone").val(),
                            'entry.397215100': $("#form_cbp ul li.active").attr("data-value"),
                            'entry.953023567': '<?php echo $utm_source; ?>',
                            'entry.823420574': '<?php echo $utm_medium; ?>',
                            'entry.1198803187': '<?php echo $utm_campaign; ?>'
                        };
                        $.ajax({
                            'url': url,
                            'method': 'POST',
                            'dataType': 'XML',
                            'data': data,
                            'statusCode': {
                                0: function () {
                                    $("#name").val('');
                                    $("#email").val('');
                                    $("#phone").val('');
                                    $("#modal").modal();
                                },
                                200: function () {
                                    $("#name").val('');
                                    $("#email").val('');
                                    $("#phone").val('');
                                    $("#modal").modal();
                                }
                            }

                        });
                    }
                });


                $('#tabs-button-2').click(function (e) {
                    e.preventDefault();
                    $(this).parents('body').find('#CBP2').tab('show');
                });

                $("#courses .wrap-list").owlCarousel({
                    autoPlay: true,
                    items: 1,
                    itemsDesktop: [1199, 1],
                    itemsDesktopSmall: [979, 1],
                    itemsTablet: [768, 1],
                    itemsMobile: [479, 1],
                    mouseDrag: true,
                    autoPlay: 40000,
                            navigation: false,
                    pagination: true,
                });

                $(".wrap-list-img-first").owlCarousel({
                    autoPlay: true,
                    items: 1,
                    itemsDesktop: [1199, 1],
                    itemsDesktopSmall: [979, 1],
                    itemsTablet: [768, 1],
                    itemsMobile: [479, 1],
                    mouseDrag: true,
                    autoPlay: 40000,
                            navigation: false,
                    pagination: true,
                });

            });


            $(function () {
                $('.scroll-link a[href*=#]:not([href=#])').click(function () {
                    if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {

                        var target = $(this.hash);
                        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                        if ($(window).width() > 768) {
                            if (target.length) {
                                $('html,body').animate({
                                    scrollTop: target.offset().top - 70
                                }, 1000);
                                return false;
                            }
                        } else {
                            if (target.length) {
                                $('html,body').animate({
                                    scrollTop: target.offset().top
                                }, 1000);
                                return false;
                            }
                        }
                    }
                });
            });
            $(".button-register").click(function (e) {
                e.preventDefault();
                var value = $(this).attr("for");
                $("a[href='" + value + "']").click();
                $('html,body').animate({
                    scrollTop: $("#register").offset().top
                }, 1000);
            });
        </script>
        <!--End of Zopim Live Chat Script-->
        <!--Start of Zopim Live Chat Script-->
<script type="text/javascript">
    window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
        d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
    _.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
        $.src="//v2.zopim.com/?1bMWnWprkt76mXJXRb6xBuP2dAD7uxPq";z.t=+new Date;$.
            type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
</script>
<!--End of Zopim Live Chat Script-->


<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-M92WKP"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

    </body>

    <!-- Mirrored from mana.edu.vn/cbp/khoa-hoc-ky-nang-giao-tiep/ by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 27 Apr 2016 13:31:05 GMT -->
</html>
