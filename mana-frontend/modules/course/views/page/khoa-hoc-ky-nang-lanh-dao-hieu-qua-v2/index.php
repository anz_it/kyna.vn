<?php
use mana\models\Setting;
$dir_name = dirname(dirname(dirname(__FILE__)));
?>
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
		<meta property="fb:app_id" content="790788601060712">
        <title>Đào tạo kỹ năng lãnh đạo hiệu quả - Mana.edu.vn</title>
		<meta name="description" content="Bao gồm những bài học về kỹ năng và tư duy lãnh đạo, khóa học Leadership của MANA đã thu hút hàng nghìn bạn trẻ tham gia.">

        <link rel="icon" href="/mana/images/manaFavicon.png">
        <meta name="robots" content="index,follow">
		<meta property='og:url' content='https://mana.edu.vn/cbp/khoa-hoc-ky-nang-lanh-dao-hieu-qua-v2' />
        <meta property="og:type" content="website" />
        <meta property='og:title' content='Đào tạo kỹ năng lãnh đạo hiệu quả - Mana.edu.vn' />
        <meta property='og:description' content='Bao gồm những bài học về kỹ năng và tư duy lãnh đạo, khóa học Leadership của MANA đã thu hút hàng nghìn bạn trẻ tham gia.' />
        <meta property='og:image:url' content='/mana/cbp/khoa-hoc-ky-nang-lanh-dao-hieu-qua-v2/img/fb-share.jpg' />

        <link href="/mana/cbp/khoa-hoc-ky-nang-lanh-dao-hieu-qua-v2/css/bootstrap.min.css" rel="stylesheet"/>
        <link href="/mana/cbp/khoa-hoc-ky-nang-lanh-dao-hieu-qua-v2/css/font-awesome.min.css" rel="stylesheet"/>
        <link href="/mana/cbp/khoa-hoc-ky-nang-lanh-dao-hieu-qua-v2/css/animate.css" rel="stylesheet"/>
        <link href="/mana/cbp/khoa-hoc-ky-nang-lanh-dao-hieu-qua-v2/css/style.css" rel="stylesheet"/>

        <script type="text/javascript" src="/mana/cbp/khoa-hoc-ky-nang-lanh-dao-hieu-qua-v2/js/jquery-1.11.2.min.js"></script>
        <?php $this->render('@app/modules/course/views/page/facebook_pixel'); ?>
        <?php echo \common\helpers\Html::csrfMetaTags() ?>
    </head>
    <body>
    <?php $this->beginBody()?>

        <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-M92WKP');</script>

    <!-- End Google Tag Manager -->
        <?php
        // get các tham số cần thiết */
        $getParams = $_GET;
        $utm_source = '';
        $utm_medium = '';
        $utm_campaign = '';

        if (isset($getParams['utm_source']) && trim($getParams['utm_source']) != '') {
            $utm_source = trim($getParams['utm_source']);
        }
        if (isset($getParams['utm_medium']) && trim($getParams['utm_medium']) != '') {
            $utm_medium = trim($getParams['utm_medium']);
        }
        if (isset($getParams['utm_campaign']) && trim($getParams['utm_campaign']) != '') {
            $utm_campaign = trim($getParams['utm_campaign']);
        }
        ?>
        <div class="navbar-fixed-top">
          <nav class="navbar" id="wrap-header">

              <div class="container">
                  <div class="navbar-header">
                      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#menu-collapse" aria-expanded="false">
                          <span class="icon-bar"></span>
                          <span class="icon-bar"></span>
                      </button>
                      <a href="/" target="_blank" class="navbar-brand kyna-logo">
                          <img src="/mana/cbp/khoa-hoc-ky-nang-lanh-dao-hieu-qua-v2/img/mana.png" alt="Mana.edu.vn" class="img-responsive img-logo">
                      </a>
                  </div>

                  <div class="collapse navbar-collapse" id="menu-collapse">
                      <ul class="nav navbar-nav">
                          <li><a href="#intro">GIỚI THIỆU</a></li>
                          <li><a href="#course">NỘI DUNG</a></li>
                          <li><a href="#organization">ĐƠN VỊ CẤP BẰNG</a></li>
                          <li><a href="#lecture">GIẢNG VIÊN</a></li>
                          <li><a href="#advantage">LỢI ÍCH</a></li>
                          <li><a href="#register">ĐĂNG KÝ</a></li>
                      </ul>
                  </div>
                  <!-- /.navbar-collapse -->
              </div>
              <!-- /.container -->
          </nav>
        </div>

        <article>
            <section id="header">
                <div class="container">
                  <div class="col-md-7 col-sm-7 wow slideInRight">
                    <h1>
                        Đào tạo kỹ năng <br>
                        lãnh đạo hiệu quả <br>
                        trong kinh doanh
                    </h1>
                    <p>
                       <b>
                        Theo tiêu chuẩn CBP (*)
                       </b>
                    </p>
                    <a href="#intro" class="">Tìm hiểu ngay</a>
                    <p class="explanation">
                        (*): CBP - Certified Business Professsional, là chứng chỉ Kinh doanh chuyên nghiệp.
                        Đây là một chứng chỉ uy tín về kỹ năng kinh doanh chuyên nghiệp và kỹ năng quản lý. Chứng chỉ CBP được
                        công nhận trên toàn cầu.
                    </p>
                  </div>
                </div>
            </section>
            <!-- end header -->
            <section id="intro">
                <div class="container">
                  <div class="col-md-6 col-sm-8 wow bounceInLeft pull-right">
                      <h2>Sự thật về tầm quan trọng của kỹ năng lãnh đạo hiệu quả</h2>
                      <div class="list-item">
                          <div class="item">
                              <span>60%</span>
                              <p>công ty tại Việt Nam đang đối mặt với việc thiếu hụt tài năng lãnh đạo khiến hiệu suất công việc toàn công ty giảm sút.</p>
                          </div>
                          <div class="item">
                              <span>75%</span>
                              <p>nhân viên được khảo sát, thừa nhận lý do nghỉ việc là vì thiếu tin tưởng vào sếp.</p>
                          </div>
                          <div class="item">
                              <span>43%</span>
                              <p>nhân viên cho rằng sự phản hồi thường xuyên và chân thành của sếp là vô cùng quan trọng.</p>
                          </div>
                          <p class="conclusion">
                              Vì vậy, <b>kỹ năng lãnh đạo hiệu quả trong kinh doanh</b> chính là <b>kỹ năng quan trọng số 1</b> để xây dựng nền tảng phát triển bền vững cho một công ty.
                          </p>
                      </div>
                  </div>
                </div>
            </section>
            <!-- /intro -->
            <section id="intro-more">
                <div class="container wow zoomIn">
                   <h2>
                       Nhà lãnh đạo hiệu quả trong kinh doanh <br>
                       <span>Theo tiêu chuẩn Quốc tế là:</span>
                   </h2>
                    <div class="item-container">
                        <div class="item">
                            <img src="/mana/cbp/khoa-hoc-ky-nang-lanh-dao-hieu-qua-v2/img/i1.png" alt="" class="img-responsive">
                            <p>
                                <b>Động viên</b><br>
                                nhân viên thường xuyên để tạo động lực cho tập thể
                            </p>
                        </div>
                        <div class="item">
                            <img src="/mana/cbp/khoa-hoc-ky-nang-lanh-dao-hieu-qua-v2/img/conversation.png" alt="" class="img-responsive">
                            <p>
                                <b>Giao tiếp</b><br>
                                tốt và xây dựng được phong thái giao tiếp riêng
                            </p>
                        </div>
                        <div class="item">
                            <img src="/mana/cbp/khoa-hoc-ky-nang-lanh-dao-hieu-qua-v2/img/i3.png" alt="" class="img-responsive">
                            <p>
                                <b>Gương mẫu</b><br>
                                theo đúng chuẩn mực cho phép
                            </p>
                        </div>
                        <div class="item">
                            <img src="/mana/cbp/khoa-hoc-ky-nang-lanh-dao-hieu-qua-v2/img/group.png" alt="" class="img-responsive">
                            <p>
                                <b>Hợp tác</b><br>
                                và luôn khuyến khích sự hợp tác
                            </p>
                        </div>
                        <div class="item">
                            <img src="/mana/cbp/khoa-hoc-ky-nang-lanh-dao-hieu-qua-v2/img/group2.png" alt="" class="img-responsive">
                            <p>
                                <b>Tầm nhìn</b><br>
                                xa để nhìn rõ mọi đường đi của dự án, con người
                            </p>
                        </div>
                        <div class="item">
                            <img src="/mana/cbp/khoa-hoc-ky-nang-lanh-dao-hieu-qua-v2/img/king.png" alt="" class="img-responsive">
                            <p>
                                <b>Hiểu biết</b><br>
                                rộng và sẵn sàng chia sẻ sự hiểu biết, kinh nghiệm
                            </p>
                        </div>
                        <div class="item">
                            <img src="/mana/cbp/khoa-hoc-ky-nang-lanh-dao-hieu-qua-v2/img/group3.png" alt="" class="img-responsive">
                            <p>
                                <b>Thay đổi</b><br>
                                khi cần thiết
                            </p>
                        </div>
                    </div>
                </div>
            </section>
            <!-- /intro-more-->
            <section id="course">
                <div class="container wow bounceInUp">
                    <h2>
                        Khóa học leadership bao gồm 6 bài học
                    </h2>
                    <div class="col-md-4 col-sm-6 col-xs-12 item">
                        <div class="item-header">
                            <div class="circle-number">
                                1
                            </div>
                            <div class="dotted-line"></div>
                        </div>
                        <p class="lesson-name">Vai trò và trách nhiệm của nhà lãnh đạo</p>
                        <ul>
                            <li><b>Tầm nhìn</b> của nhà lãnh đạo</li>
                            <li><b>Tạo động lực</b> thúc đẩy cho các thành viên</li>
                            <li>Bài học tình huống</li>
                        </ul>
                    </div>
                    <div class="col-md-4 col-sm-6 col-xs-12 item">
                        <div class="item-header">
                            <div class="circle-number">
                                2
                            </div>
                            <div class="dotted-line"></div>
                        </div>
                        <p class="lesson-name">Phong cách lãnh đạo</p>
                        <ul>
                            <li>Sự biến đổi của nhà lãnh đạo trong thời đại mới</li>
                            <li><b>4 phong cách lãnh đạo</b> theo tình huống</li>
                            <li>Hộ trợ mối quan hệ và hỗ trợ chức năng</li>
                        </ul>
                    </div>
                    <div class="col-md-4 col-sm-6 col-xs-12 item">
                        <div class="item-header">
                            <div class="circle-number">
                                3
                            </div>
                            <div class="dotted-line"></div>
                        </div>
                        <p class="lesson-name">Tầm nhìn và sứ mệnh của nhà lãnh đạo</p>
                        <ul>
                            <li><b>4 hướng xây dựng tầm nhìn </b>của một nhà lãnh đạo</li>
                            <li>Truyền đạt tầm nhìn cho tập thể</li>
                            <li><b>Xây dựng văn hóa nội bộ</b></li>
                        </ul>
                    </div>
                    <div class="col-md-4 col-sm-6 col-xs-12 item">
                        <div class="item-header">
                            <div class="circle-number">
                                4
                            </div>
                            <div class="dotted-line"></div>
                        </div>
                        <p class="lesson-name">Ra quyết định một cách hiệu quả</p>
                        <ul>
                            <li>Các tiêu chuẩn đánh giá một quyết định</li>
                            <li><b>Phân tích rủi ro, chi phí và cách giải quyết</b></li>
                            <li>Phong cách và cách thức ra quyết định</li>
                        </ul>
                    </div>
                    <div class="col-md-4 col-sm-6 col-xs-12 item">
                        <div class="item-header">
                            <div class="circle-number">
                                5
                            </div>
                            <div class="dotted-line"></div>
                        </div>
                        <p class="lesson-name">Xây dựng đội nhóm - team building</p>
                        <ul>
                            <li>Động viên và đáp ứng nhu cầu của các thành viên </li>
                            <li><b>Xây dựng văn hóa và tinh thần đồng đội</b></li>
                            <li>Giải quyết các xung đột trong đội nhóm</li>
                        </ul>
                    </div>
                    <div class="col-md-4 col-sm-6 col-xs-12 item">
                        <div class="item-header">
                            <div class="circle-number">
                                6
                            </div>
                            <div class="dotted-line"></div>
                        </div>
                        <p class="lesson-name">Động viên và khích lệ</p>
                        <ul>
                            <li>Xác định nhu cầu và mong muốn của nhân viên</li>
                            <li>Nâng cao tinh thần đội nhóm </li>
                            <li><b>Xây dựng lòng trung thành và kết nối các thành viên với nhau</b></li>
                        </ul>
                    </div>
                    <div class="regist-now" data-wow-delay="2s">
                        <!--<a href="#register" data-xmas="false">Đăng ký ngay <br>nhận học bổng 1.000.000Đ</a>-->
                        <a href="#register" data-xmas='true'>Học bổng 1.500.000Đ</a>
                    </div>
                </div>
            </section>

            <section id="organization">
                <div class="container wow slideInDown">
                    <h2>Giáo trình gốc theo bản quyền <br>của tổ chức IBTA</h2>
                    <div class="col-md-7 col-sm-7 left">
                        <p>
                           <b>
                            IBTA (International Business Training Association) là Hiệp hội Đào tạo Kinh doanh Quốc tế, có trụ sở tại Hoa Kỳ.
                            </b>
                        </p>
                        <img src="/mana/cbp/khoa-hoc-ky-nang-lanh-dao-hieu-qua-v2/img/ibta.png" alt="" class="img-responsive">
                        <p>
                            Trên thế giới, các tập đoàn lớn như HONDA, Nokia, Mitsubishi, Intel, Ricoh, adidas,... đều công nhận tiêu chuẩn đánh giá về kỹ năng kinh doanh của IBTA được mô tả qua chứng chỉ CBP.
                        </p>
                    </div>
                    <div class="col-md-5 col-sm-5 right">
                        <img src="/mana/cbp/khoa-hoc-ky-nang-lanh-dao-hieu-qua-v2/img/ibta.png" alt="" class="img-responsive">
                    </div>
                </div>
            </section>

            <section id="lecture">
                <div class="container wow slideInUp">
                    <div class="mg-image">
                      <img src="/mana/cbp/khoa-hoc-ky-nang-lanh-dao-hieu-qua-v2/img/chuyengia.jpg" alt="Thac si Ho Thi Thanh Van">
                    </div>
                    <h2>
                        Học trực tuyến cùng chuyên gia <br>
                        <b>Thạc sỹ Hồ Thị Thanh Vân (Vân Hồ)</b>
                    </h2>
                    <p class="brief-info">
                        Được đào tạo Cao học chuyên ngành Marketing Bán hàng và Dịch vụ (MMSS) tại trường Đại học Paris Sorbonne(IAE) phối hợp với trường Quản trị Châu Âu (ESCP -EAP European School Management).
                    </p>
                    <div class="exp">
                        <p>
                            <b>
                                20 năm kinh nghiệm trong ngành Sales &amp; Marketing tại các công ty hàng đầu thế giới:
                            </b>
                        </p>
                        <ul>
                            <li><p>Giám đốc Ngành hàng tại Việt Nam kiêm Lãnh đạo thị trường Cambodia/Laos của Novartis Pharma.</p></li>
                            <li><p>Nguyên giám đốc Điều hành Tiếp thị - AstraZeneca.</p></li>
                            <li><p>Nguyên Giám đốc Marketing - Sanofi.</p></li>
                            <li><p>Nguyên Giám đốc Thương hiệu Abbott.</p></li>
                            <li><p>Nguyên Giám đốc Kinh doanh - GlaxoSmithKline.</p></li>
                        </ul>
                    </div>
                    <div class="achievements">
                        <p><b>Dành nhiều giải thưởng danh giá:</b></p>
                        <ul>
                            <li><p>Giải thưởng của Phó Chủ tịch tập đoàn Khu vực Châu Á.</p></li>
                            <li><p>Giải thưởng Khu vực International cho thành tích xuất sắc trong việc giới thiệu thành công sản phẩm mới ra thị trường (AstraZenca).</p></li>
                            <li><p>Giải thưởng Quán quân Marketing cho Chương trình Marketing xuất sắc (Abbott).</p></li>
                            <li><p>Giải thưởng Cầu vồng dành cho Lãnh đạo xuất sắc.</p></li>
                            <li><p>Giải thưởng Thành tích Kinh doanh xuất sắc trong 4 năm liên tiếp (GlaxoSmithKline).</p></li>
                        </ul>
                    </div>
                    <div class="video-wrapper">
                        <iframe src="https://www.youtube.com/embed/XGrww3hKWuE" frameborder="0" allowfullscreen></iframe>
                    </div>
                </div>
            </section>

            <section id="advantage-top">
                <div class="container ">
                    <div class="item wow bounceInLeft">
                        <i class="fa fa-globe"></i>
                        <p class="top">Học mọi lúc mọi nơi</p>
                        <p class="bottom">Học qua video bài giảng được biên tập chuyên nghiệp. Tài liệu bao gồm sách và bài thuyết trình, tư liệu tham khảo.</p>
                    </div>
                    <div class="item wow bounceInLeft">
                        <i class="fa fa-comments"></i>
                        <p class="top">Học kèm 1 - 1</p>
                        <p class="bottom">Cố vấn 1 kèm 1, trong suốt quá trình học và luyện thi. Hỏi đáp cùng chuyên gia, doanh nhân giàu kinh nghiệm.</p>
                    </div>
                    <div class="item wow bounceInRight">
                        <i class="fa fa-question-circle"></i>
                        <p class="top">Hỏi đáp và luyện thi</p>
                        <p class="bottom">Chương trình học được đan xen giữa bài học lý thuyết và thực hành, cùng hơn 100 bài thi thử.</p>
                    </div>
                    <div class="item wow bounceInRight">
                        <i class="fa fa-certificate"></i>
                        <p class="top">Thi trực tuyến và nhận chứng chỉ quốc tế</p>
                        <p class="bottom">Thi trực tuyến trên hệ thống PROMETRIC với tối đa 12 tháng ôn luyện trước khi thi.</p>
                    </div>
                </div>
            </section>

            <section id="advantage">
                <div class="container">
                    <div class="col-md-6 col-sm-6 col-xs-12 child-container wow slideInRight pull-right">
                        <h2>Lợi thế khi sở hữu chứng <br>chỉ CBP Leadership</h2>
                        <ul>
                            <li>
                                <i class="fa fa-check-circle"></i>
                                <p>Đạt điều kiện cần để thi <b>lấy chứng chỉ CBP Executive</b> - chứng chỉ CBP Lãnh đạo cấp trung</p>
                            </li>
                            <li>
                                <i class="fa fa-check-circle"></i>
                                <p>Là chứng nhận cho <b>kỹ năng lãnh đạo hiệu quả, </b>có giá trị trên toàn cầu</p>
                            </li>
                            <li>
                                <i class="fa fa-check-circle"></i>
                                <p>Có tư duy và kỹ năng lãnh đạo trong doanh nghiệp</p>
                            </li>
                            <li>
                                <i class="fa fa-check-circle"></i>
                                <p><b>Là lợi thế khi ứng tuyển vào các tập đoàn đa quốc gia, tập đoàn nước ngoài</b></p>
                            </li>
                            <li>
                                <i class="fa fa-check-circle"></i>
                                <p><b>Tăng cơ hội thăng tiến trong công việc</b></p>
                            </li>
                        </ul>
                    </div>
                </div>
            </section>

            <section id="review">
                <div class="container">
                    <h2>Học viên của Mana online business school</h2>
                    <div class="col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1 col-xs-12">
                        <div id="review-carousel" class="carousel slide" data-ride="carousel" data-interval="8000">
                            <!-- Indicators -->
                            <ol class="carousel-indicators">
                                <li data-target="#review-carousel" data-slide-to="0" class="active"></li>
                                <li data-target="#review-carousel" data-slide-to="1"></li>
                            </ol>

                            <!-- Wrapper for slides -->
                            <div class="carousel-inner" role="listbox">

                                <div class="item active">
                                  <img src="/mana/cbp/khoa-hoc-ky-nang-lanh-dao-hieu-qua-v2/img/hv.png" alt="Thanh Son" class="img-responsive">
                                  <div class="text">
                                      <p class="review-text">
                                        Anh khá thỏa mãn ở 2 nội dung, đó là cách ra quyết định và cách xây dựng đội nhóm.
                                      </p>
                                      <p>Anh càng thấm thía trong việc xem những người đồng nghiệp của mình là anh em cùng chiến đấu và cách gọi tập thể đó là chúng ta. Hiểu được điều đó thì đơn giản thôi nhưng không phải dễ để thực sự nghĩ và làm việc như những người anh em. Anh đã tìm ra được giải pháp cho team của mình, nó khá là hiệu quả đấy.</p>
                                      <p class="person-info">
                                        <b>Anh Thanh Sơn - 32 tuổi</b>
                                      </p>
                                  </div>
                                </div>
                                <div class="item">
                                  <img src="/mana/cbp/khoa-hoc-ky-nang-lanh-dao-hieu-qua-v2/img/hv2.jpg" alt="" class="img-responsive">
                                  <div class="text">
                                      <p class="review-text">
                                        Vô tình học và vô tình tìm ra giải pháp cho mình cũng như đốc rút được những kinh nghiệm quý báu. Thực sự khi xem các bộ phim của Mỹ, tôi hay thần tượng những nhân vật lãnh đạo trong phim. Tôi cố học theo và tự tạo cho mình một phong thái lãnh đạo chuẩn mực. Cơ mà sao vẫn thấy sai sai. Khóa học này đã chỉ ra tôi sai ở đâu và cần thay đổi những gì.
                                      </p>
                                      <p>
                                        Trước khi biến một thứ gì đó thành kỹ năng thì chúng ta cần phải thay đổi tư duy. Và tư duy của tôi đã thay đổi!”
                                      </p>
                                      <p class="person-info">
                                        <b>Anh Quang Nguyễn - 28 tuổi</b>
                                      </p>
                                  </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section id="register">
                <div class="container">
                    <div class="form-container">
                      <div data-xmas='true'>
                        <h2 style="margin-bottom: 30px">ĐĂNG KÝ KHÓA HỌC<br>NHẬN NGAY HỌC BỔNG TRỊ GIÁ 1.500.000Đ</h2>
                        <h2 style="margin-bottom: 30px;text-transform:none">Học online cùng Mana Online Business School</h2>
                      </div>
                       <form action="/course/page/submit" name="landing-page-id" id="landing-page-id" class="form-inline">

                           <input type="hidden" id="advice_name" name="advice_name" value="Mana - Khóa học kỹ năng lãnh đạo hiệu quả v2" />
                            <input type="hidden" id="csrf" name="_csrf" />
                           <div class="col-md-4 col-sm-4 col-xs-12">
                            <input type="text" id="fullname" name="fullname" class="form-control" placeholder="Nhập họ và tên" required>
                          </div>
                          <div class="col-md-4 col-sm-4 col-xs-12">
                            <input type="text" id="phonenumber" name="phonenumber" class="form-control" placeholder="Số điện thoại" required>
                          </div>
                          <div class="col-md-4 col-sm-4 col-xs-12">
                            <input type="email" id="email" name="email" class="form-control" placeholder="Email" required>
                          </div>
                          <button type="submit" class="button-regis wow swing btn_box_register">
                            Đăng ký
                          </button>
                          <!--<div data-xmas="false">
                            <p>
                              Mọi thông tin của bạn đều được bảo mật hoàn toàn.
                            </p>
                            <p>
                              Bộ phận Tư vấn sẽ liên hệ sớm với bạn để tư vấn về khóa học và học bổng trị giá 1.000.000đ.
                            </p>
                          </div>-->
                          <div data-xmas="true">
                            <p>ĐĂNG KÝ NHẬN HỌC BỔNG</p>
                            <p>Bộ phận CSKH sẽ liên hệ với bạn sớm nhất có thể. Mọi thắc mắc về khóa học và học bổng
                            sẽ được giải đáp thông qua cuộc gọi này.</p>
                          </div>
                        </form>
                    </div>
                </div>
            </section>

            <section id="about-mana">
              <div class="container">
                <div class="row top">
                  <h3>Xin chào, chúng tôi là MANA, <br> Viện đào tạo Quản trị Kinh doanh trực tuyến hàng đầu Việt Nam.</h3>
                  <div class="col-md-6 col-sm-6 col-xs-12 left">
                    <p>MANA mang đến một hệ thống các chương trình đào tạo kỹ năng quản trị kinh doanh có tính ứng dụng cao.</p>
                    <p>Tại MANA, học viên sẽ được học trên các chương trình đào tạo song ngữ cùng đội ngũ cố vấn học tập 24/7.</p>
                    <p>Hình thức và phương pháp học khoa học mang lại cho học viên những trải nghiệm học tập mới mẻ và thú vị.</p>
                    <p>Vào 16.6.2016, Dream Viet Education Corporation (công ty chủ quản của MANA OBS) đã trở thành đối tác chính thức và
                    độc quyền của Hiệp hội IBTA tại Việt Nam. Điều này xác định, MANA là đại diện hợp pháp trong việc phân phối các chương trình
                  Đào tạo kỹ năng Kinh doanh chuẩn quốc tế trên toàn Việt Nam trong 5 năm 2016 - 2021.</p>
                  </div>
                  <div class="col-md-6 col-sm-6 col-xs-12 right">
                    <img src="/mana/cbp/khoa-hoc-ky-nang-lanh-dao-hieu-qua-v2/img/about-bg.jpg" alt="" class="img-responsive">
                  </div>
                </div>
                <div class="row bottom">
                  <h3>Báo chí nói về chúng tôi</h3>
                  <div class="item-container">
                    <div class="item">
                      <a href="http://www.doanhnhansaigon.vn/san-pham-moi/nang-cao-nang-luc-canh-tranh-qua-dao-tao-ky-nang-truc-tuyen/1095837/" target="_blank">
                        <img src="/mana/cbp/khoa-hoc-ky-nang-lanh-dao-hieu-qua-v2/img/doanhnhan.png" alt="" class="im-responsive">
                      </a>
                    </div>
                    <div class="item">
                      <a href="http://dantri.com.vn/khuyen-hoc/dang-sau-viec-ky-ket-cua-kynavn-va-international-business-training-association-ibta-20160627165026711.htm" target="_blank">
                        <img src="/mana/cbp/khoa-hoc-ky-nang-lanh-dao-hieu-qua-v2/img/dantri.png" alt="" class="im-responsive">
                      </a>
                    </div>
                    <div class="item">
                      <a href="http://cafebiz.vn/bi-mat-tang-truong-than-toc-cua-kyna-20160810133337045.chn" target="_blank">
                        <img src="/mana/cbp/khoa-hoc-ky-nang-lanh-dao-hieu-qua-v2/img/cafebiz.png" alt="" class="im-responsive">
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            </section>

        </article>
			<section id="hoi-dap">
				<div class="container">
					<div class="fb-comments fb_iframe_widget fb_iframe_widget_fluid"
						data-href="https://mana.edu.vn/cbp/khoa-hoc-ky-nang-lanh-dao-hieu-qua-v2"
						data-width="100%" data-numposts="10"
						data-colorscheme="light" fb-xfbml-state="rendered">
					 </div>
				</div><!--end .container-->
			</section>
        <!-- footer -->
        <footer>
            <div class="container">
              <div class="col-md-4 col-sm-4 col-xs-12 left">
                <a href="https://mana.edu.vn" target="_blank">
                  <img src="/mana/cbp/khoa-hoc-ky-nang-lanh-dao-hieu-qua-v2/img/mana.png" alt="Mana" class="img-responsive">
                </a>
                <a href="https://kyna.vn" target="_blank">
                  <img src="/mana/cbp/khoa-hoc-ky-nang-lanh-dao-hieu-qua-v2/img/kyna.png" alt="kyna" class="img-responsive">
                </a>
              </div>
              <div class="col-md-5 col-sm-5 col-xs-12 mid">
                <p class="comp-name">Công ty Cổ phần Dream Việt Education</p>
                <p><b><u>Trụ sở chính: </u></b>Lầu 6 Tòa nhà Thịnh Phát - 178/8 Đường D1,
                Phường 25, Quận Bình Thạnh, TP Hồ Chí Minh</p>
                <p><b><u>Văn phòng Hà Nội: </u></b>Tầng 6 Tòa nhà Viện Công Nghệ - 25 Vũ Ngọc Phan,
                Phường Láng Hạ, Quận Đống Đa, TP Hà Nội</p>
                <p>
                  Giấy phép ĐKKD số 0313589030 do Sở Kế hoạch và Đầu tư TPHCM cấp
                </p>
              </div>
              <div class="col-md-3 col-sm-3 col-xs-12 right">
                <p><b class="company-address">Hotline:</b> 1900 6364 09</p>
                <p><b class="company-address">Email: </b>hotro@kyna.vn</p>
                <div class="fb-page" data-href="https://www.facebook.com/mana.edu.vn/" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/mana.edu.vn/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/mana.edu.vn/">MANA.edu.vn</a></blockquote></div>
              </div>
            </div>
        </footer>

        <script type="text/javascript" src="/mana/cbp/khoa-hoc-ky-nang-lanh-dao-hieu-qua-v2/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="/mana/cbp/khoa-hoc-ky-nang-lanh-dao-hieu-qua-v2/js/wow.min.js"></script>

        <div class="modal fade" id="modal">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="popup_body">
                            <div class="alert alert-success" role="alert">Cám ơn bạn đã để lại thông tin. Bộ phận Tư vấn sẽ sớm liên hệ với bạn để tư vấn thêm về chương trình học!</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <script>
            $(document).ready(function () {
                $(window).width() > 1024 && new WOW().init();
                if ($(window).width() > 760) {
                  $(window).on('scroll', function() {
                      if ($(window).scrollTop() > 50) {
                          $('#wrap-header').addClass('scrolled');
                          $('.navbar').addClass('scrolled');
                          $('.img-logo').addClass('scrolled');
                          $('.navbar-nav').addClass('scrolled');
                      } else {
                        $('#wrap-header').removeClass('scrolled');
                        $('.navbar').removeClass('scrolled');
                        $('.img-logo').removeClass('scrolled');
                        $('.navbar-nav').removeClass('scrolled');
                      }
                  });
                }
                /* form submit handler */


            });

            /* smooth scroll handler */
            $(function() {
                $('a[href*=#]:not([href=#])').click(function() {
                    if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {

                        var target = $(this.hash);
                        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                        if ($(window).width() > 768) {
                            if (target.length) {
                                $('html,body').animate({
                                    scrollTop: target.offset().top - 80
                                }, 1000);
                                return false;
                            }
                        } else {
                            if (target.length) {
                                $('html,body').animate({
                                    scrollTop: target.offset().top - 80
                                }, 1000);
                                return false;
                            }
                        }
                    }
                });
            });
        </script>

        <!-- Bootstrap scrollspy handler-->
         <script>
            $(document).ready(function() {

                $('body').scrollspy({
                    target: '#menu-collapse',
                    offset: 100
                });
            });

        </script>
        <?php $this->render('@app/modules/course/views/page/ga_landing_page'); ?>
<!--Start of Zopim Live Chat Script-->
<script type="text/javascript">
    window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
        d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
    _.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
        $.src="//v2.zopim.com/?1bMWnWprkt76mXJXRb6xBuP2dAD7uxPq";z.t=+new Date;$.
            type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
</script>
<!--End of Zopim Live Chat Script-->
	<div id="fb-root"></div>
	<script type="text/javascript">
		(function(d, s, id) {
			var js, fjs = d.getElementsByTagName(s)[0];
			if (d.getElementById(id)) return;
			js = d.createElement(s); js.id = id;
			js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.0&appId=790788601060712";
			fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));
	</script>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-M92WKP"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
    <?php $this->endBody()?>
    </body>

    <!-- Mirrored from mana.edu.vn/cbp/khoa-hoc-ky-nang-giao-tiep/ by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 27 Apr 2016 13:31:05 GMT -->
</html>
