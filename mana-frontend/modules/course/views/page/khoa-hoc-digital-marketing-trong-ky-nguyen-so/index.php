<?php
use mana\models\Setting;

$dir_name = dirname(dirname(dirname(__FILE__)));
?>
<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from mana.edu.vn/cbp/khoa-hoc-ky-nang-giao-tiep/ by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 27 Apr 2016 13:29:41 GMT -->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Đào tạo Digital Marketing chứng nhận quốc tế - Mana.edu.vn</title>
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,300,500,700' rel='stylesheet' type='text/css'>
    <!-- Bootstrap -->
    <link href="/mana/cs/khoa-hoc-digital-marketing-trong-ky-nguyen-so/css/bootstrap.min.css" rel="stylesheet"/>
    <link href="/mana/cs/khoa-hoc-digital-marketing-trong-ky-nguyen-so/css/site.css" rel="stylesheet"/>
    <link href="/mana/cs/khoa-hoc-digital-marketing-trong-ky-nguyen-so/css/font-awesome.min.css" rel="stylesheet"/>
    <link href="/mana/cs/khoa-hoc-digital-marketing-trong-ky-nguyen-so/css/owl.carousel.css" rel="stylesheet"/>
    <link href="/mana/cs/khoa-hoc-digital-marketing-trong-ky-nguyen-so/css/owl.theme.css" rel="stylesheet"/>
    <link href="/mana/cs/khoa-hoc-digital-marketing-trong-ky-nguyen-so/css/owl.transitions.css" rel="stylesheet"/>
    <link rel="icon" href="/mana/images/manaFavicon.png">
</head>
<body>
    <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-M92WKP');</script>
<!-- End Google Tag Manager -->

<?php
// get các tham số cần thiết */
$getParams = $_GET;
$utm_source = '';
$utm_medium = '';
$utm_campaign = '';

if (isset($getParams['utm_source']) && trim($getParams['utm_source']) != '') {
    $utm_source = trim($getParams['utm_source']);
}
if (isset($getParams['utm_medium']) && trim($getParams['utm_medium']) != '') {
    $utm_medium = trim($getParams['utm_medium']);
}
if (isset($getParams['utm_campaign']) && trim($getParams['utm_campaign']) != '') {
    $utm_campaign = trim($getParams['utm_campaign']);
}
?>
<nav class="navbar navbar-inverse header-menu navbar-fixed-top scroll-link">
    <div class="container">
        <div class="col-sm-3">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                        data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">
                    <img class="logo-brand" src="/mana/cs/khoa-hoc-digital-marketing-trong-ky-nguyen-so/imgs/mana.png"/>
                </a>
                <a href="#regis" class="button mb">ĐĂNG KÝ NGAY</a>
            </div>
        </div>
        <div class="col-sm-9 col-xs-12">
            <div id="navbar" class="navbar-collapse collapse">
                <ul class="nav navbar-nav main-menu">
                    <li class="active"><a href="#intro">GIỚI THIỆU</a></li>
                    <li><a href="#ucirvine">ĐƠN VỊ CẤP BẰNG</a></li>
                    <li><a href="#accord">ĐỐI TƯỢNG</a></li>
                    <li><a href="#part">HỌC PHẦN</a></li>
                    <li><a href="#regis">ĐĂNG KÝ</a></li>
                </ul>
            </div>
        </div>
    </div>
</nav>

<section id="banner">
    <div class="wrapper-slider scroll-link">
        <ul>
            <li>
                <div class="item">
                    <img class="img-slider img-responsive" src="/mana/cs/khoa-hoc-digital-marketing-trong-ky-nguyen-so/imgs/slide/img-1.jpg" alt="First slide"/>
                    <div class="container">
                        <div class="carousel-caption">
                            <h2><img class="logo-slider img-responsive" src="/mana/cs/khoa-hoc-digital-marketing-trong-ky-nguyen-so/imgs/mana.png"/></h2>
                            <h4>
                                <span class="bold text-transform">"MARKETING TRONG KỶ NGUYÊN SỐ"</span><br />Đại học Illinois (Hoa Kỳ) cấp bằng có <span class="bold">giá trị trên toàn cầu</span></h4>
                            <a class="btn btn-lg btn-primary btn-regis-slider" href="#regis" role="button">ĐĂNG KÝ</a>
                        </div>
                    </div>
                </div><!--end .item-->
            </li>

            <li>
                <div class="item">
                    <img class="img-slider img-responsive" src="/mana/cs/khoa-hoc-digital-marketing-trong-ky-nguyen-so/imgs/slide/img-2.jpg" alt="Second slide"/>
                    <div class="container">
                        <div class="carousel-caption">
                            <h2><img class="logo-slider img-responsive" src="/mana/cs/khoa-hoc-digital-marketing-trong-ky-nguyen-so/imgs/mana.png"/></h2>
                            <h4>Đơn vị cấp bằng: <span class="bold text-transform">ĐẠI HỌC ILLINOIS – URBANA CHAMPAIGN</span><br />(top <span class="bold big">50</span> trường Đại học tốt nhất nước Mỹ)</h4>
                            <a class="btn btn-lg btn-primary btn-regis-slider" href="#regis" role="button">ĐĂNG KÝ</a>
                        </div>
                    </div>
                </div><!--end .item-->
            </li>

            <li>
                <div class="item">
                    <img class="img-slider img-responsive" src="/mana/cs/khoa-hoc-digital-marketing-trong-ky-nguyen-so/imgs/slide/img-3.jpg" alt="First slide"/>
                    <div class="container">
                        <div class="carousel-caption">
                            <h2><img class="logo-slider img-responsive" src="/mana/cs/khoa-hoc-digital-marketing-trong-ky-nguyen-so/imgs/mana.png"/></h2>
                            <h4>Khóa học Digital Marketing là <span class="bold">1 trong 10 khóa học trực tuyến chuyên nghiệp hot</span> nhất hiện nay.<br />Khóa học được phát triển dựa trên <span class="bold">giáo án chuẩn của Đại học Illinois (Mỹ).</span></h4>
                            <a class="btn btn-lg btn-primary btn-regis-slider" href="#regis" role="button">ĐĂNG KÝ</a>
                        </div>
                    </div>
                </div><!--end .item-->
            </li>

            <li>
                <div class="item">
                    <img class="img-slider img-responsive" src="/mana/cs/khoa-hoc-digital-marketing-trong-ky-nguyen-so/imgs/slide/img-4.jpg" alt="First slide"/>
                    <div class="container">
                        <div class="carousel-caption">
                            <h2><img class="logo-slider img-responsive" src="/mana/cs/khoa-hoc-digital-marketing-trong-ky-nguyen-so/imgs/mana.png"/></h2>
                            <h4>Chương trình được phát triển bởi <span class="bold text-transform">Mana Online Business School</span><br />Viện Đào tạo Quản trị Kinh doanh hàng đầu Việt Nam</h4>
                            <a class="btn btn-lg btn-primary btn-regis-slider" href="#regis" role="button">ĐĂNG KÝ</a>
                        </div>
                    </div>
                </div><!--end .item-->
            </li>
        </ul>
    </div><!--end .wrapper-slider-->
</section><!--end #banner-->

<section class="container" id="intro">

    <ul class="wrap-intro-heading">
        <li>
            <div class="line"><hr/></div>
        </li>
        <li>
            <h3 class="intro-heading"> GIỚI THIỆU </h3>
        </li>
        <li>
            <div class="line"><hr/></div>
        </li>
    </ul>

    <div class="row">
        <div class='text-intro'>
            <p>Chương trình <span class="bold">"MARKETING TRONG KỶ NGUYÊN SỐ"</span> (Digital Marketing) được xây dựng dựa trên <span class="bold">giáo trình quốc tế</span> của Đại học Illinois (Mỹ).</p>
        </div>
    </div>

    <div class="row">
        <div class="text-details clearfix">
            <div class="col-sm-5 col-xs-12">
                <img src="/mana/cs/khoa-hoc-digital-marketing-trong-ky-nguyen-so/imgs/intro.jpg" class="img-responsive"/>
            </div>
            <div class="col-sm-7 col-xs-12">
                <div class="text-details-content">
                    <p class="text">Khóa học cung cấp các khái niệm tổng quan về Chiến lược Tiếp thị và các công cụ để giải quyết truyền thông thương hiệu trong thời đại kỹ thuật số. Nắm vững cách ứng dụng Digital vào các hoạt động Marketing giúp <span class="bold">gia tăng doanh số bền vững</span> với ngân sách có hạn.</p>
                    <ul>
                        <li><p><span class="blue bold">1. </span>Chương trình học <span class="bold">"Marketing trong kỷ nguyên số"</span> cập nhật các xu hướng Marketing mới nhất với các bài tập <span class="bold">thực hành xuyên suốt.</span></p></li>
                        <li><p><span class="blue bold">2. </span>Chương trình học <span class="bold">song ngữ Anh – Việt</span> giúp người học theo kịp tiến trình học mà không gặp trở ngại gì.</p></li>
                        <li><p><span class="blue bold">3. </span>Học cùng <span class="bold">chuyên gia từ Đại học Illinois</span> và <span class="bold">chuyên gia về Digital Marketing người Việt.</span></p></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section><!--end #intro-->

<section id="road">
    <div class="container">

        <ul class="wrap-intro-heading">
            <li>
                <div class="line"><hr/></div>
            </li>
            <li>
                <h3 class="intro-heading"> LỘ TRÌNH HỌC TẬP</h3>
            </li>
            <li>
                <div class="line"><hr/></div>
            </li>
        </ul>

        <div class="row">
            <ul>
                <li>
                    <img src="/mana/cs/khoa-hoc-digital-marketing-trong-ky-nguyen-so/imgs/icon-road-1.png" alt="" class="img-responsive"/>
                    <p>Học bài giảng video</p>
                </li>
                <li>
                    <img src="/mana/cs/khoa-hoc-digital-marketing-trong-ky-nguyen-so/imgs/icon-road-2.png" alt="" class="img-responsive"/>
                    <p>Bài tập thực hành hàng tuần</p>
                </li>
                <li>
                    <img src="/mana/cs/khoa-hoc-digital-marketing-trong-ky-nguyen-so/imgs/icon-road-3.png" alt="" class="img-responsive"/>
                    <p>Thảo luận nhóm với các bạn học</p>
                </li>
                <li>
                    <img src="/mana/cs/khoa-hoc-digital-marketing-trong-ky-nguyen-so/imgs/icon-road-4.png" alt="" class="img-responsive"/>
                    <p>Kiểm tra tiểu luận cuối khóa</p>
                </li>
                <li>
                    <img src="/mana/cs/khoa-hoc-digital-marketing-trong-ky-nguyen-so/imgs/icon-road-5.png" alt="" class="img-responsive"/>
                    <p>Nộp hồ sơ và nhận bằng quốc tế</p>
                </li>
            </ul>
        </div>
    </div><!--end .container-->
</section><!--end .road -->

<section id="ucirvine">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-sm-5 col-xs-12 img">
                <img src="/mana/cs/khoa-hoc-digital-marketing-trong-ky-nguyen-so/imgs/ucirvine.png" alt="Ucirvine" class="img-responsive"/>
            </div><!--end .img-->
            <div class="col-md-8 col-sm-7 col-xs-12 text">
                <h4>Bằng và chứng chỉ Quốc tế được cấp bởi</h4>
                <h2 class="text-transform">ĐẠI HỌC ILLINOIS – URBANA CHAMPAIGN</h2>
                <p>Top <span class="big">50</span> trường Đại học <span class="big">tốt nhất</span> nước Mỹ</p>
            </div><!--end .text-->
        </div>
    </div><!--end .container-->
</section>

<section id="accord">
    <div class="container">

        <ul class="wrap-intro-heading">
            <li>
                <div class="line"><hr/></div>
            </li>
            <li>
                <h3 class="intro-heading"> KHÓA HỌC NÀY PHÙ HỢP VỚI</h3>
            </li>
            <li>
                <div class="line"><hr/></div>
            </li>
        </ul>

        <div class="row">
            <div class="col-md-4 col-xs-6 box">
                <div class="wrap-img">
                    <img src="/mana/cs/khoa-hoc-digital-marketing-trong-ky-nguyen-so/imgs/accord-1.png" alt="" class="img-responsive"/>
                    <div class="background"></div>
                </div><!--end .wrap-img-->
                <h4>Sinh viên khối ngành Kinh tế, khối ngành Công nghệ thông tin</h4>
            </div><!--end .box-->

            <div class="col-md-4 col-xs-6 box">
                <div class="wrap-img">
                    <img src="/mana/cs/khoa-hoc-digital-marketing-trong-ky-nguyen-so/imgs/accord-2.png" alt="" class="img-responsive"/>
                    <div class="background"></div>
                </div><!--end .wrap-img-->
                <h4>Các bạn CNTT muốn làm Marketing</h4>
            </div><!--end .box-->

            <div class="col-md-4 col-xs-6 box">
                <div class="wrap-img">
                    <img src="/mana/cs/khoa-hoc-digital-marketing-trong-ky-nguyen-so/imgs/accord-3.png" alt="" class="img-responsive"/>
                    <div class="background"></div>
                </div><!--end .wrap-img-->
                <h4>Nhân viên Kinh doanh</h4>
            </div><!--end .box-->

            <div class="col-md-4 col-xs-6 box">
                <div class="wrap-img">
                    <img src="/mana/cs/khoa-hoc-digital-marketing-trong-ky-nguyen-so/imgs/accord-4.png" alt="" class="img-responsive"/>
                    <div class="background"></div>
                </div><!--end .wrap-img-->
                <h4>Nhân viên Marketing/PR chuyên ngành Kinh doanh – Marketing</h4>
            </div><!--end .box-->

            <div class="col-md-4 col-xs-6 box">
                <div class="wrap-img">
                    <img src="/mana/cs/khoa-hoc-digital-marketing-trong-ky-nguyen-so/imgs/accord-5.png" alt="" class="img-responsive"/>
                    <div class="background"></div>
                </div><!--end .wrap-img-->
                <h4>Trưởng/phó phòng Kinh doanh – Marketing - PR</h4>
            </div><!--end .box-->

        </div><!--end .row-->
    </div><!--end .container-->
</section>


<section class="container study-heading" id="part">

    <ul class="wrap-intro-heading">
        <li>
            <div class="line"><hr/></div>
        </li>
        <li>
            <h3 class="intro-heading"> HỌC PHẦN</h3>
        </li>
        <li>
            <div class="line"><hr/></div>
        </li>
    </ul>

    <div class="row">
        <div class="header-intro-text">
            <div class="col-sm-3">
            </div>
            <div class="col-sm-6">
                <p>MARKETING TRONG KỶ NGUYÊN SỐ</p>
            </div>
            <div class="col-sm-3">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="content-collapse">

            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingOne">
                        <h4 class="panel-title">
                            <span class="circle-bullet">1</span>
                            <span class="title"><span class="mb">1. </span>TỔNG QUAN VÀ XU HƯỚNG PHÁT TRIỂN – SÁNG TẠO SẢN PHẨM TRONG DIGITAL MARKETING</span>
                            <a class="accordion-toggle" role="button" data-toggle="collapse" data-parent="#accordion"
                               href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                            </a>
                        </h4>
                    </div>
                    <div id="collapseOne" class="panel-collapse collapse" role="tabpanel"
                         aria-labelledby="headingOne">
                        <div class="panel-body">
                            <ul>
                                <li>Đầu tiên, bạn sẽ có một cái nhìn toàn cảnh về Marketing trong kỷ nguyên số, để giúp bạn nắm chắc được bản chất của Marketing.</li>
                                <li>Tìm hiểu sâu về Marketing và Marketing 4Ps, đặc biệt hơn học phần này còn chỉ ra cho bạn thấy những thay đổi của Marketing từ tác động của Digital Marketing.</li>
                                <li>Cuối cùng, bạn sẽ được tìm hiểu về xu hướng phát triển và sáng tạo sản phẩm trong Digital Marketing – mang khách hàng vào quy trình sáng tạo và sản xuất.</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingTwo">
                        <h4 class="panel-title">
                            <span class="circle-bullet">2</span>
                            <span class="title"><span class="mb">2. </span>NHỮNG XU HƯỚNG MỚI TRONG DIGITAL MARKETING ĐỂ THUYẾT PHỤC KHÁCH HÀNG MUA SẮM</span>
                            <a class="accordion-toggle collapsed" role="button" data-toggle="collapse"
                               data-parent="#accordion" href="#collapseTwo" aria-expanded="false"
                               aria-controls="collapseTwo">
                            </a>
                        </h4>
                    </div>
                    <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                        <div class="panel-body">
                            <ul>
                                <li>Một sản phẩm được quảng bá như thế nào trong môi trường số? Trong phần này, bạn sẽ được học cách các công cụ mới giúp khách hàng có vai trò chủ động hơn trong các hoạt động quảng bá và khuyến mãi của thương hiệu.</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingThree">
                        <h4 class="panel-title">
                            <span class="circle-bullet">3</span>
                            <span class="title"><span class="mb">3. </span>NHỮNG XU HƯỚNG MỚI TRONG DIGITAL MARKETING ĐỂ PHÂN PHỐI SẢN PHẨM HIỆU QUẢ</span>
                            <a class="accordion-toggle collapsed" role="button" data-toggle="collapse"
                               data-parent="#accordion" href="#collapseThree" aria-expanded="false"
                               aria-controls="collapseThree">
                            </a>
                        </h4>
                    </div>
                    <div id="collapseThree" class="panel-collapse collapse" role="tabpanel"
                         aria-labelledby="headingThree">
                        <div class="panel-body">
                            <ul>
                                <li>Một sản phẩm được phân phối như thế nào trong môi trường số?</li>
                                <li>Trong phần này, bạn sẽ được học cách các công cụ mới thay đổi việc phân phối sản phẩm và tạo nên một cuộc cách mạng trong ngành bán lẻ.</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingFour">
                        <h4 class="panel-title">
                            <span class="circle-bullet">4</span>
                            <span class="title"><span class="mb">4. </span>NHỮNG XU HƯỚNG MỚI TRONG DIGITAL MARKETING ĐỂ XÁC ĐỊNH GIÁ BÁN SẢN PHẨM</span>
                            <a class="accordion-toggle collapsed" role="button" data-toggle="collapse"
                               data-parent="#accordion" href="#collapseFour" aria-expanded="false"
                               aria-controls="collapseFour">
                            </a>
                        </h4>
                    </div>
                    <div id="collapseFour" class="panel-collapse collapse" role="tabpanel"
                         aria-labelledby="headingFou">
                        <div class="panel-body">
                            <ul>
                                <li>Một sản phẩm được định giá như thế nào trong môi trường số?</li>
                                <li>Trong phần này, bạn sẽ được học cách các công cụ mới cho phép khách hàng chủ động đánh giá và tự định giá cho các sản phẩm mà họ muốn mua.</li>
                            </ul>
                        </div>
                    </div>
                </div>

            </div><!--end #accordion-->

        </div>
    </div>
</section>

<section id="teacher">
    <div class="container">

        <ul class="wrap-intro-heading">
            <li>
                <div class="line"><hr/></div>
            </li>
            <li>
                <h3 class="intro-heading"> GIẢNG VIÊN</h3>
            </li>
            <li>
                <div class="line"><hr/></div>
            </li>
        </ul>

        <div class="row">
            <div class="col-sm-10 col-sm-offset-1 col-xs-12">
                <div class="col-md-6 col-xs-12 box first">
                    <img src="/mana/cs/khoa-hoc-digital-marketing-trong-ky-nguyen-so/imgs/gv1.png" alt="" class="img-responsive"/>
                    <div class="wrap-hover">
                        <div class="wrap pc">
                            <h4>Thạc sĩ Trần Khánh Tùng</h4>
                            <ul>
                                <li>
                                    <div class="media">
                                      <div class="media-left">
                                        <i class="fa fa-circle" aria-hidden="true"></i>
                                      </div>
                                      <div class="media-body">
                                          Thạc sỹ Trần Khánh Tùng Tốt nghiệp tại ĐH Sorbonne Paris.
                                      </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="media">
                                      <div class="media-left">
                                        <i class="fa fa-circle" aria-hidden="true"></i>
                                      </div>
                                      <div class="media-body">
                                          Thạc Sỹ Trần Khánh Tùng đã từng làm việc với sinh viên MBA ĐH Harvard trong dự án nghiên cứu hành vi cũng như xu hướng giải trí của giới trẻ Việt Nam.
                                      </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="media">
                                        <div class="media-left">
                                            <i class="fa fa-circle" aria-hidden="true"></i>
                                        </div>
                                        <div class="media-body">
                                            Nguyên Giám đốc Marketing của IMC Group.
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="media">
                                      <div class="media-left">
                                        <i class="fa fa-circle" aria-hidden="true"></i>
                                      </div>
                                      <div class="media-body">
                                          Hiện tại Ông đang là co-founder của Your Fashion<br />
                                          Giám đốc Marketing tại IMC Group.
                                      </div>
                                    </div>
                                </li>
                            </ul>

                        </div><!--end .wrap-->

                        <div class="wrap mb">
                            <h4>Giáo sư Aric Rindfleisch</h4>
                            <ul>
                                <li>
                                    <div class="media">
                                      <div class="media-left">
                                        <i class="fa fa-circle" aria-hidden="true"></i>
                                      </div>
                                      <div class="media-body">
                                        Giáo sư giảng dạy Marketing tại Trường John M. Jones Professor.
                                      </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="media">
                                      <div class="media-left">
                                        <i class="fa fa-circle" aria-hidden="true"></i>
                                      </div>
                                      <div class="media-body">
                                        Giám đốc điều hành của UIUC.
                                      </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="media">
                                      <div class="media-left">
                                        <i class="fa fa-circle" aria-hidden="true"></i>
                                      </div>
                                      <div class="media-body">
                                        Tốt nghiệp tiến sĩ trường Đại Học Wisconsin tại Madison.
                                      </div>
                                    </div>
                                </li>
                            </ul>
                        </div><!--end .wrap-->
                        <span class="icon-left"><img src="/mana/cs/khoa-hoc-digital-marketing-trong-ky-nguyen-so/imgs/icon-box-gv.png" class="img-responsive"/></span>
                    </div><!--end .wrap-hover-->
                </div><!--end .box-->

                <div class="col-md-6 col-xs-12 box last">
                    <img src="/mana/cs/khoa-hoc-digital-marketing-trong-ky-nguyen-so/imgs/gv2.png" alt="" class="img-responsive"/>
                    <div class="wrap-hover">
                        <div class="wrap pc">
                            <h4>Giáo sư Aric Rindfleisch</h4>
                            <ul>
                                <li>
                                    <div class="media">
                                      <div class="media-left">
                                        <i class="fa fa-circle" aria-hidden="true"></i>
                                      </div>
                                      <div class="media-body">
                                        Giáo sư giảng dạy Marketing tại Trường John M. Jones Professor.
                                      </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="media">
                                      <div class="media-left">
                                        <i class="fa fa-circle" aria-hidden="true"></i>
                                      </div>
                                      <div class="media-body">
                                        Giám đốc điều hành của UIUC.
                                      </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="media">
                                      <div class="media-left">
                                        <i class="fa fa-circle" aria-hidden="true"></i>
                                      </div>
                                      <div class="media-body">
                                        Tốt nghiệp tiến sĩ trường Đại Học Wisconsin tại Madison.
                                      </div>
                                    </div>
                                </li>
                            </ul>
                        </div><!--end .wrap-->

                        <div class="wrap mb">
                            <h4>Thạc sĩ Trần Khánh Tùng</h4>
                            <ul>
                                <li>
                                    <div class="media">
                                      <div class="media-left">
                                        <i class="fa fa-circle" aria-hidden="true"></i>
                                      </div>
                                      <div class="media-body">
                                        Giám đốc Marketing tại IMC Group.
                                      </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="media">
                                      <div class="media-left">
                                        <i class="fa fa-circle" aria-hidden="true"></i>
                                      </div>
                                      <div class="media-body">
                                        Hơn 12 năm kinh nghiệm về digital marketing tại các agency lớn tại Việt Nam.
                                      </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="media">
                                      <div class="media-left">
                                        <i class="fa fa-circle" aria-hidden="true"></i>
                                      </div>
                                      <div class="media-body">
                                        Tốt nghiệp MBA tại Đại học Sorbonne Paris.
                                      </div>
                                    </div>
                                </li>
                            </ul>

                        </div><!--end .wrap-->
                        <span class="icon-right"><img src="/mana/cs/khoa-hoc-digital-marketing-trong-ky-nguyen-so/imgs/icon-box-gv.png" class="img-responsive"/></span>
                    </div><!--end .wrap-hover-->

                </div><!--end .box-->
            </div>
        </div><!--end .row-->
    </div><!--end .container-->
</section>

<section id="online">
    <div class="container">

        <ul class="wrap-intro-heading">
            <li>
                <div class="line"><hr/></div>
            </li>
            <li>
                <h3 class="intro-heading"> HỌC ONLINE CÙNG MANA OBS NHẬN CHỨNG CHỈ QUỐC TẾ</h3>
            </li>
            <li>
                <div class="line"><hr/></div>
            </li>
        </ul>

        <div class="row">
            <div class="col-md-4 col-sm-5 col-xs-12 img">
                <img src="/mana/cs/khoa-hoc-digital-marketing-trong-ky-nguyen-so/imgs/mana-big.png" alt="MANA" class="img-responsive"/>
            </div><!--end .img-->

            <div class="col-md-8 col-sm-7 col-xs-12 text">
                <ul>
                    <li><i class="fa fa-clock-o" aria-hidden="true"></i> Học Online mọi lúc mọi</li>
                    <li><i class="fa fa-globe" aria-hidden="true"></i> Chương trình học song ngữ Anh - Việt</li>
                    <li><i class="fa fa-graduation-cap" aria-hidden="true"></i> Học cùng chuyên gia - thực nghiệm cùng chuyên gia</li>
                    <li><i class="fa fa-certificate" aria-hidden="true"></i> Học tại Việt Nam nhận chứng chỉ Quốc tế</li>
                </ul>
            </div><!--end .text-->
        </div><!--end .row-->
    </div><!--end .container-->
</section>
<section class="registration" id="regis">
    <div class="container resgistration-border">

        <ul class="wrap-intro-heading">
            <li>
                <div class="line"><hr/></div>
            </li>
            <li>
                <h3 class="intro-heading"> ĐĂNG KÝ NHẬN TƯ VẤN</h3>
            </li>
            <li>
                <div class="line"><hr/></div>
            </li>
        </ul>

        <form action="#" class="form-horizontal" id="form_advice" method="post" accept-charset="utf-8">
            <div class="row">

                <div class="text-intro-regist">
                    <p>Bộ phận CSKH của MANA sẽ sớm liên hệ và tư vấn miễn phí về khóa học cho bạn.</p>
                </div>
                <div class="regist-form clearfix">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <input type="text" id="name" class="input-group form-control" required  placeholder="Họ Và Tên"/>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <input type="text" id="phone" class="input-group form-control" required placeholder="Số Điện Thoại"/>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <input type="text" id="email" class="input-group form-control" required  placeholder="Email"/>
                        </div>
                    </div>
                </div>
                <div class="btn-wrap">
                    <div class="button-regist">
                        <button type="submit"  id="dang_ky_form" class="btn btn_box_register">ĐĂNG KÝ</button>
                            <span>
                                <div class="kyna-click-form"></div>
                                <div class="kyna-click-form-fill"></div>
                                <div class="kyna-click-form-img"></div>
                            </span>
                        </button>
                    </div>
                </div><!--end .btn-wrap-->

                <div class="information-text hidden">
                    <p class="text-transform">Giảm ngay <span class="bold blue price big">500.000đ</span> khi đăng ký trước ngày 20/06/2016<br />
                    và tặng ngay học bổng <span class="bold blue price big">500.000đ</span> để trải nghiệm hàng trăm khóa học thú vị trên Kyna.vn</p>
                </div>

            </div>
        </form>
    </div>
</section>
<footer>
    <div class="container footer">
        <div class="row">
            <div class="col-sm-3">
                <div class="footer-logo">
                    <ul>
                        <li>
                            <a href="#">
                                <img src="/mana/cs/khoa-hoc-digital-marketing-trong-ky-nguyen-so/imgs/mana.png" class="img-responsive"/>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <img src="/mana/cs/khoa-hoc-digital-marketing-trong-ky-nguyen-so/imgs/18_KynaLogo.png" class="img-responsive"/>
                            </a>
                        </li>
                    </ul>

                </div>
            </div>
            <div class="col-sm-6">
                <div class="footer-text-center">
                    <h4>Công ty Cổ phần  Dream Việt Education</h4>
                    <p> <b class="company-address"> Trụ sở chính:</b> Lầu 6 Tòa nhà Thịnh Phát - 178/8 Đường D1, Phường 25, Quận Bình Thạnh, TP Hồ Chí Minh</p>
                    <p> <b class="company-address"> Văn phòng Hà Nội:</b> Tầng 6, Tòa Nhà Viện Công Nghệ - 25 Vũ Ngọc Phan, Phường Láng Hạ, Quận Đống Đa, TP Hà Nội </p>
                    <p style="padding-top: 20px">Giấy phép ĐKKD số 0313589030 do Sở Kế hoạch và Đầu tư TPHCM cấp</p>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="footer-hot-line">
                    <p><b class="company-address">Hotline:</b>  <?=Setting::HOTLINE ?></p>
                    <p>Thứ 2 – thứ 6: từ 08h30 – 21h00</p>
                    <p>Thứ 7: 08h30 – 17h00</p>
                    <p><b class="company-address">Email:</b> hotro@kyna.vn</p>
                </div>
            </div>
        </div>
    </div>
</footer>
<div class="modal fade" id="modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <div class="popup_body">
                    <div class="alert alert-success" role="alert">Cám ơn bạn đã để lại thông tin. Kyna.vn sẽ sớm liên hệ với bạn để tư vấn thêm về chương trình học!</div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="/mana/cs/khoa-hoc-digital-marketing-trong-ky-nguyen-so/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/mana/cs/khoa-hoc-digital-marketing-trong-ky-nguyen-so/js/owl.carousel.min.js"></script>
<script>
$(function(){
  $('.logo-brand').data('size','big');
});

$(window).scroll(function(){
  if($(document).scrollTop() > 0)
{
    if($('.logo-brand').data('size') == 'big')
    {
        $('.logo-brand').data('size','small');
        $('.logo-brand').stop().animate({
            height:'40px'
        },600);
        $('.navbar').addClass('menufix');
    }
}
else
  {
    if($('.logo-brand').data('size') == 'small')
      {
        $('.logo-brand').data('size','big');
        $('.logo-brand').stop().animate({
            height:'50px'
        },600);

        $('.navbar').removeClass('menufix');
      }
  }
});
</script>
<!-- Nhan them vao -->
<script>
    $(document).ready(function(){
        $("#dang_ky_form").bind('click', function(e){

           if($.trim($("#name").val()) != '' && $.trim($("#email").val()) != '' && $.trim($("#phone").val()) != '' ){
                e.preventDefault();
                var url = 'https://docs.google.com/forms/d/1Dc326ATbw52RL8RjiDhZeUvW8T8p5L50MMulA2ymQZk/formResponse';
               var data = {'entry.1550829392': $("#name").val(),
                   'entry.1618386042': $("#email").val(),
                   'entry.1725316625': $("#phone").val(),
                   'entry.953023567': '<?php echo $utm_source; ?>',
                   'entry.823420574':'<?php echo $utm_medium;  ?>',
                   'entry.1198803187':'<?php echo $utm_campaign;  ?>'
               };
                $.ajax({
                    'url': url,
                    'method': 'POST',
                    'dataType': 'XML',
                    'data': data,
                    'statusCode': {
                        0: function () {
                            $("#name").val('');
                            $("#email").val('');
                            $("#phone").val('');
                            $("#modal").modal();
                        },
                        200: function () {
                            $("#name").val('');
                            $("#email").val('');
                            $("#phone").val('');
                            $("#modal").modal();
                        }
                    }

                });
           }

        });

        $(".wrapper-slider ul").owlCarousel({
                autoPlay: true,
                items : 1,
                itemsDesktop : [1199,1],
                itemsDesktopSmall:	[979,1],
                itemsTablet:	[768,1],
                itemsMobile:	[479,1],
                mouseDrag : true,
            autoPlay: 40000,
                navigation: false,
                pagination : true,
        });
    });


    $(function() {
          $('.scroll-link a[href*=#]:not([href=#])').click(function() {
            if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {

              var target = $(this.hash);
              target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
                                if($(window).width() > 768){
                                        if (target.length) {
                                                $('html,body').animate({
                                                  scrollTop: target.offset().top - 70
                                                }, 1000);
                                                return false;
                                        }
                                } else {
                                        if (target.length) {
                                                $('html,body').animate({
                                                  scrollTop: target.offset().top
                                                }, 1000);
                                                return false;
                                        }
                                }
            }
          });
        });

        /* RESPONSIVE EQUAL HEIGHT BLOCKS */
    ;( function( $, window, document, undefined )
    {
        'use strict';

        var $list       = $( '#accord' ),
            $items      = $list.find( '.box' ),
            setHeights  = function()
            {
                $items.css( 'height', 'auto' );

                var perRow = Math.floor( $list.width() / $items.width() );
                if( perRow == null || perRow < 2 ) return true;

                for( var i = 0, j = $items.length; i < j; i += perRow )
                {
                    var maxHeight   = 0,
                        $row        = $items.slice( i, i + perRow );

                    $row.each( function()
                    {
                        var itemHeight = parseInt( $( this ).outerHeight() );
                        if ( itemHeight > maxHeight ) maxHeight = itemHeight;
                    });
                    $row.css( 'height', maxHeight );
                }
            };

        setHeights();
        $( window ).on( 'resize', setHeights );
        $list.find( 'img' ).on( 'load', setHeights );

    })( jQuery, window, document );


    $( "#accord .wrap-img" ).hover(
          function() {
            $( this ).addClass( "hover" );
          }, function() {
            $( this ).removeClass( "hover" );
          }
    );

    $( "#teacher .box.first" ).hover(
          function() {
            $( "#teacher .box.last" ).addClass( "hover" );
          }, function() {
            $( "#teacher .box.last" ).removeClass( "hover" );
          }
    );

    $( "#teacher .box.last" ).hover(
          function() {
            $( "#teacher .box.first" ).addClass( "hover" );
          }, function() {
            $( "#teacher .box.first" ).removeClass( "hover" );
          }
    );

</script>
    <!--End of Zopim Live Chat Script-->
    <!--Start of Zopim Live Chat Script-->
<script type="text/javascript">
    window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
        d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
    _.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
        $.src="//v2.zopim.com/?1bMWnWprkt76mXJXRb6xBuP2dAD7uxPq";z.t=+new Date;$.
            type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
</script>
<!--End of Zopim Live Chat Script-->

 
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-M92WKP"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->


</body>

</html>
