<?php

use mana\models\Setting;
$dir_name = dirname(dirname(dirname(__FILE__)));
?>
<!DOCTYPE html>
<html lang="vi">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
	  <meta property="fb:app_id" content="790788601060712">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="Học bổng lên đế 3.000.000đ. + Đăng ký một lần học mãi mãi + Học theo chương trình học quốc tế + Bước đầu cho hành trình trở thành lãnh đạo tài năng.">
    <meta name="author" content="">
    <!--<link rel="icon" href="">-->
    <title>Trở thành chiến binh Sales đẳng cấp quốc tế - Mana.edu.vn</title>

	  <meta name="robots" content="index,follow">
    <meta property="og:type" content="website" />
    <meta property='og:url' content='https://mana.edu.vn/cbp-combo/combo-selling-customer-service' />
    <meta property='og:image:url' content='https://mana.edu.vn/mana/cbp-combo/combo-selling-customer-service/img/thumb.jpg' />
    <meta property='og:title' content='Trở thành chiến binh Sales đẳng cấp quốc tế - Mana.edu.vn' />
    <meta property='og:description' content='Học bổng lên đế 3.000.000đ. + Đăng ký một lần học mãi mãi + Học theo chương trình học quốc tế + Bước đầu cho hành trình trở thành lãnh đạo tài năng.' />

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="/mana/cbp-combo/combo-selling-customer-service/css/bootstrap.min.css">
  	<link rel="stylesheet" href="/mana/cbp-combo/combo-selling-customer-service/css/slick.css">
  	<link rel="stylesheet" href="/mana/cbp-combo/combo-selling-customer-service/css/style.css">
  	<link rel="stylesheet" href="/mana/cbp-combo/combo-selling-customer-service/css/font-awesome.min.css">


    <?php echo \common\helpers\Html::csrfMetaTags() ?>

</head>
<body>
<?php $this->beginBody()?>


    <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-M92WKP');</script>

    <!-- End Google Tag Manager -->
	<?php
	// get các tham số cần thiết */

	$getParams = $_GET;
	$utm_source = '';
	$utm_medium = '';
	$utm_campaign = '';

	if (isset($getParams['utm_source']) && trim($getParams['utm_source']) != '') {
		$utm_source = trim($getParams['utm_source']);
	}
	if (isset($getParams['utm_medium']) && trim($getParams['utm_medium']) != '') {
		$utm_medium = trim($getParams['utm_medium']);
	}
	if (isset($getParams['utm_campaign']) && trim($getParams['utm_campaign']) != '') {
		$utm_campaign = trim($getParams['utm_campaign']);
	}

	?>
  <section id="header">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<div id="topMenu">
						<div class="logo">
							<a class="page-scroll" href="#header"><img src="/mana/cbp-combo/combo-selling-customer-service/images/logo.png" alt=""></a>
						</div>
						<ul class="topMenu navbar-nav">
							<li><a class="page-scroll" href="#about">Giới thiệu</a></li>
							<li><a class="page-scroll" href="#courses">Nội dung học</a></li>
							<li><a class="page-scroll" href="#benefits">Quyền lợi</a></li>
							<li><a class="page-scroll" href="#testimonial">cảm nhận học viên</a></li>
							<li><a class="page-scroll button-scroll topReg" href="#adsBotReg">Đăng kí học</a></li>
						</ul>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12">
					<div id="banner">
						<div class="tagline">
							<h1>
								 TRỞ THÀNH <strong>CHIẾN BINH SALES</strong>
							</h1>
							<p>Với bộ khóa học <strong>Kỹ năng bán hàng (Selling skills)</strong> và <strong>Chăm sóc khách hàng (Customer Service) theo tiêu chuẩn Quốc tế</strong> (*)</p>
						</div>
						<a href="#about" class="bt-CTA topCTA page-scroll">TÌM HIỂU THÊM</a>
						<p class="note">(*) Hoàn thành khóa học, bạn sẽ được cấp chứng chỉ CBP được xét duyệt bởi Hiệp hội Đào tạo Kinh doanh Quốc tế IBTA, chứng chỉ được công nhận trên toàn cầu.</p>
					</div>
				</div>
			</div>
		</div>
		<div class="man-top"></div>
	</section>
	<section id="about" class="animationEffect">
		<div class="container">
			<div class="row">
				<div class="col-md-3">
					<div class="bgEffect" >
						<img src="/mana/cbp-combo/combo-selling-customer-service/images/img-about.jpg" alt="">
					</div>
				</div>
				<div class="col-md-8 col-md-offset-1">
					<div class="textAbout">
						<h2 class="mainTitle" data-text="Chân dung một Salesman thành công là:">
							<span>Chân dung một Salesman thành công là:</span>
						</h2>
						<ul>
							<li><strong>Thấu hiểu khách hàng</strong>, và đem lại giá trị cho khách hàng</li>
							<li><strong>Mở rộng được mạng lưới khách hàng</strong> tiềm năng cho doanh nghiệp</li>
							<li><strong>Tạo được sự gắn kết</strong> giữa thương hiệu và khách hàng</li>
							<li><strong>Gia tăng được giá trị thương hiệu</strong> cho doanh nghiệp </li>
							<li>Thu nhập từ hoa hồng <strong>lớn và bền vững</strong></li>
						</ul>
						<p>Do vậy, để trở thành một chiến binh sales thực thụ, bạn cần quan tâm đến <strong>kỹ năng bán hàng</strong> và <strong>chiến lược chăm sóc khách hàng</strong> để duy trì mối quan hệ lâu dài</p>
						<p>Đây cũng là hai <strong>kỹ năng tối thiểu</strong> để trở thành Giám đốc kinh doanh hay Giám đốc điều hành của một doanh nghiệp.</p>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section id="stats">
		<div class="container">
			<div class="row">
				<div class="iconBlock">
					<img src="/mana/cbp-combo/combo-selling-customer-service/images/icon-1.png" alt="">
					<p><strong>02</strong> khóa học toàn diện</p>
				</div>
				<div class="iconBlock">
					<img src="/mana/cbp-combo/combo-selling-customer-service/images/icon-2.png" alt="">
					<p><strong>20+</strong> nội dung về bán hàng và CSKH</p>
				</div>
				<div class="iconBlock">
					<img src="/mana/cbp-combo/combo-selling-customer-service/images/icon-3.png" alt="">
					<p><strong>02</strong> chuyên gia CBP Instructor</p>
				</div>
				<div class="iconBlock">
					<img src="/mana/cbp-combo/combo-selling-customer-service/images/icon-4.png" alt="">
					<p>Học trực tuyến <strong>mọi lúc mọi nơi</strong></p>
				</div>
				<div class="iconBlock">
					<img src="/mana/cbp-combo/combo-selling-customer-service/images/icon-5.png" alt="">
					<p>Trợ giảng <strong>1 kèm 1</strong></p>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
	</section>
	<section id="courses">
		<div class="container">
			<div class="mobileTabs">
				<h2 class="mainTitle mainTitle2 titleCenter" data-text="Nội dung chương trình học">
					<span><strong>Nội dung chương trình học</strong></span>
				</h2>
				<ul>
					<li class="active"><a href="">Khóa học 1</a></li>
					<li><a href="">Khóa học 2</a></li>
				</ul>
			</div>
			<div class="expertSlider">
				<div class="sliderPost">
					<div class="col-md-3 textLeft">
						<h2 class="mainTitle mainTitle2" data-text="Nội dung chương trình học">
							<span>Nội dung chương trình học</span>
						</h2>
						<p class="hidden768">Khóa học 1: <strong>Kỹ năng bán hàng chuyên nghiệp (CBP Selling skills)</strong></p>
						<p>Giảng viên giảng dạy: <strong>Thạc sỹ Nguyễn Ngoan (CBP Certified Instructor)</strong></p>
						<ul>
							<li>Phó tổng Giám đốc - Star Travel International</li>
							<li>Chủ tịch MANDA MIND Corporation</li>
							<li>Nhà sáng lập thương hiệu “Đặc sản 3 miền”</li>
							<li>Diễn giả Doanh nhân & Chuyên gia tư vấn chiến lược, thương hiệu</li>
						</ul>
					</div>
					<div class="col-md-8 col-md-offset-1">
						<div class="detailCourse">
							<div class="detailText">
								<h3>
									<span>1</span>
									<p>Khóa học 1: <strong>Kỹ năng bán hàng chuyên nghiệp (CBP Selling skills)</strong></p>
									<div class="clearfix"></div>
								</h3>
								<ul>
									<li>Quy trình bán hàng và các giai đoạn bán hàng</li>
									<li>PP tạo sự hào hứng tự nhiên trong quá trình bán hàng</li>
									<li>PP tìm kiếm khách hàng tiềm năng thành công</li>
									<li>Chiến lược tiếp cận khách hàng theo từng giai đoạn</li>
									<li>PP giải quyết tình huống khi khách hàng từ chối</li>
									<li>Chiến lược chốt sales thành công</li>
									<li>Chiến lược kết thúc quá trình bán hàng và xây dựng hệ thống khách hàng sau khi bán</li>
								</ul>
							</div>
							<div class="detailExpert">
								<div class="imgExpert">
									<img src="/mana/cbp-combo/combo-selling-customer-service/images/expert-1.png" alt="">
								</div>
								<div class="mobileOnly">
									<p>Giảng viên giảng dạy: <strong>Thạc sỹ Nguyễn Ngoan (CBP Certified Instructor)</strong></p>
									<ul>
										<li>Phó tổng Giám đốc - Star Travel International</li>
										<li>Chủ tịch MANDA MIND Corporation</li>
										<li>Nhà sáng lập thương hiệu “Đặc sản 3 miền”</li>
										<li>Diễn giả Doanh nhân & Chuyên gia tư vấn chiến lược, thương hiệu</li>
									</ul>
								</div>
								<div class="clearfix"></div>
							</div>
						</div>
					</div>

				</div>
				<div class="sliderPost">
					<div class="col-md-3 textLeft">
						<h2 class="mainTitle mainTitle2" data-text="Nội dung chương trình học">
							<span>Nội dung chương trình học</span>
						</h2>
						<p class="hidden768">Khóa học 2: <strong>Kỹ năng chăm sóc khách hàng hiệu quả (CBP Customer Service)</strong></p>
						<p>Giảng viên giảng dạy: <strong>Thạc sỹ Nguyễn Kiên Trì - CBP Certified Instructor</strong></p>
						<ul>
							<li>MBA Tư vấn Quản lý Quốc tế - Thụy Sỹ</li>
							<li>Expert Economic Certified - Đại học California</li>
							<li>Giám đốc Sales & Marketing Hạ Long</li>
							<li>Giám đốc Sales Marketing Saigontourist - Liberty Group</li>
						</ul>
					</div>
					<div class="col-md-8 col-md-offset-1">
						<div class="detailCourse">
							<div class="detailText">
								<h3>
									<span>2</span>
									<p>Khóa học 2: <strong>Kỹ năng chăm sóc khách hàng hiệu quả (CBP Customer Service)</strong></p>
									<div class="clearfix"></div>
								</h3>
								<ul>
									<li>Kỹ năng giao tiếp trong dịch vụ chăm sóc khách hàng</li>
									<li>Phân tích tâm lý và thấu hiểu tâm lý khách hàng</li>
									<li>Cách ứng xử tốt nhất khi gặp khách hàng khó tính</li>
									<li>Chăm sóc khách hàng qua điện thoại</li>
									<li>Chăm sóc khách hàng qua Internet</li>
									<li>Chiến lược quản lý thời gian</li>
									<li>Phương pháp tạm biệt sự căng thẳng</li>
								</ul>
							</div>
							<div class="detailExpert">
								<div class="imgExpert">
									<img src="/mana/cbp-combo/combo-selling-customer-service/images/expert-2.png" alt="">
								</div>
								<div class="mobileOnly">
									<p>Giảng viên giảng dạy: <strong>Thạc sỹ Nguyễn Kiên Trì - CBP Certified Instructor</strong></p>
									<ul>
										<li>MBA Tư vấn Quản lý Quốc tế - Thụy Sỹ</li>
										<li>Expert Economic Certified - Đại học California</li>
										<li>Giám đốc Sales & Marketing Hạ Long</li>
										<li>Giám đốc Sales Marketing Saigontourist - Liberty Group</li>
									</ul>
								</div>
								<div class="clearfix"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section id="benefits">
		<div class="container">
			<div class="row">
				<h2 class="mainTitle titleCenter" data-text="Học online 1 kèm 1 với cố vấn học tập">
					<span><strong>Học online 1 kèm 1 với cố vấn học tập</strong></span>
				</h2>
				<div class="rowTop">
					<div class="itemBlock">
						<strong>Học online mọi lúc mọi nơi:</strong>
						Học qua video bài giảng được biên tập chuyên nghiệp. Tài liệu bao gồm sách và bài thuyết trình, tư liệu tham khảo.Hỏi đáp cùng trợ giảng 1 kèm 1
					</div>
					<div class="itemBlock">
						<strong>Học kèm 1-1:</strong>
						Cố vấn 1 kèm 1, trong suốt quá trình học và luyện thi. Hỏi đáp cùng chuyên gia, doanh nhân giàu kinh nghiệm
					</div>
					<div class="itemBlock">
						<strong>Học kết hợp cùng thực hành và luyện thi:</strong>
						Chương trình học được đan xen giữa bài học lý thuyết và thực hành, cùng hơn 100 câu thi thử
					</div>
					<div class="itemBlock">
						<strong>Thi trực tuyến tại nhà và nhận chứng chỉ quốc tế:</strong>
						Thi trực tuyến trên hệ thống PROMETRIC với tối đa 6 tháng ôn luyện trước khi thi
					</div>
				</div>
			</div>
			<div class="row">
				<div class="rowBot">
					<h2 class="mainTitle titleCenter" data-text="Chương trình học online theo tiêu chuẩn quốc tế">
						<span><strong>Chương trình học online theo tiêu chuẩn quốc tế</strong></span>
					</h2>
					<ul>
						<li>
							Khóa học được phát triển dựa trên kiến thức và kinh nghiệm của hàng trăm Salesman thành công từ nhiều nơi trên thế giới
						</li>
						<li>
							Được bảo trợ nội dung bởi tổ chức học thuật uy tín - Hiệp hội Đào tạo Kinh doanh Quốc tế IBTA
						</li>
						<li>
							Được nhận chứng chỉ CBP (chứng chỉ Kinh doanh Chuyên nghiệp) cấp bởi Hiệp hội IBTA, được công nhận trên toàn cầu
						</li>
						<li>
							Sở hữu video trọn đời và toàn bộ các cập nhật về kiến thức, nghề nghiệp từ Mana Online Business School hoàn toàn miễn phí
						</li>
					</ul>
				</div>
			</div>
			<div class="adsBot">
				<strong>Nhận ngay học bổng 3.000.000 đồng từ MANA OBS</strong>
				<a href="#adsBotReg"  class="bt-CTA page-scroll">Đăng kí ngay</a>
			</div>
		</div>
	</section>
	<section id="cerfiticate">
		<div class="row">
			<div class="col-md-6 leftSide">
				<h2 class="mainTitle mainTitle2" data-text="Cơ hội sở hữu chứng chỉ CBP">
					<span>Cơ hội sở hữu chứng chỉ CBP</span>
				</h2>
				<h3>Chứng Chỉ Kinh Doanh Được Toàn Thế Giới Công Nhận</h3>
				<ul>
					<li><i class="fa fa-check-circle" aria-hidden="true"></i> Chứng chỉ CBP của IBTA là một trong những chứng chỉ uy tín nhất về kỹ năng kinh doanh chuyên nghiệp và kỹ năng quản lý</li>
					<li><i class="fa fa-check-circle" aria-hidden="true"></i> CBP được công nhận trên toàn cầu từ Mỹ, Canada, Caribe, châu Phi, Trung Đông, Trung Quốc, Ấn Độ, đến Singapore và vùng Viễn Đông</li>
				</ul>
				<strong>
					Trên thế giới, các tập đoàn lớn như HONDA, Nokia, Mitsubishi, Intel, Ricoh, Adidas,… đều công nhận tiêu chuẩn đánh giá về kỹ năng kinh doanh của IBTA được mô tả qua chứng chỉ CBP
				</strong>
			</div>
			<div class="col-md-6">
				<img src="/mana/cbp-combo/combo-selling-customer-service/images/cerfiticate.jpg" alt="">
			</div>
		</div>
	</section>
	<section id="reg">
		<div class="container">
			<div class="row">
				<div class="regNTesti">
					<div id="testimonial">
						<div class="sliderTesti">
							<div class="itemTesti">
								“Làm việc trong môi trường là một tập đoàn bán lẻ của nước ngoài rất cạnh tranh, đòi hỏi cần có các kỹ năng theo tiêu chuẩn quốc tế cũng như bằng cấp tương đương, do vậy, mình đã tham gia chương trình của Mana Online Business School. Sau chương trình, điều mình nhận được không chỉ là chứng chỉ CBP mà còn nhiều kiến thức bổ ích từ hai chuyên gia của chương trình. Rất cảm ơn các giảng viên và Mana OBS” - <strong>Chị Thu Thảo</strong> <i>(Distribution Channel Executive)</i>
							</div>
							<div class="itemTesti">
								“Chương trình của Mana OBS đem lại cho mình nhiều kiến thức bổ ích, các chuẩn mực bán hàng và chăm sóc khách hàng theo tiêu chuẩn quốc tế. Sau khi tham gia chương trình, mình đã có thể xây dựng được mối quan hệ bền vững với khách hàng ” - <strong>Anh Cao Qúy</strong> <i>Sales Team Leader</i>
							</div>
							<div class="itemTesti">
								“Làm việc trong môi trường là một tập đoàn bán lẻ của nước ngoài rất cạnh tranh, đòi hỏi cần có các kỹ năng theo tiêu chuẩn quốc tế cũng như bằng cấp tương đương, do vậy, mình đã tham gia chương trình của Mana Online Business School. Sau chương trình, điều mình nhận được không chỉ là chứng chỉ CBP mà còn nhiều kiến thức bổ ích từ hai chuyên gia của chương trình. Rất cảm ơn các giảng viên và Mana OBS” - <strong>Chị Thu Thảo</strong> <i>(Distribution Channel Executive)</i>
							</div>
							<div class="itemTesti">
								“Chương trình của Mana OBS đem lại cho mình nhiều kiến thức bổ ích, các chuẩn mực bán hàng và chăm sóc khách hàng theo tiêu chuẩn quốc tế. Sau khi tham gia chương trình, mình đã có thể xây dựng được mối quan hệ bền vững với khách hàng ” - <strong>Anh Cao Qúy</strong> <i>Sales Team Leader</i>
							</div>
						</div>
						<div class="sliderTestiNav">
							<div class="sliderTestiNavItem"><img src="/mana/cbp-combo/combo-selling-customer-service/images/testi-1.png" alt=""></div>
							<div class="sliderTestiNavItem"><img src="/mana/cbp-combo/combo-selling-customer-service/images/testi-2.png" alt=""></div>
							<div class="sliderTestiNavItem"><img src="/mana/cbp-combo/combo-selling-customer-service/images/testi-1.png" alt=""></div>
							<div class="sliderTestiNavItem"><img src="/mana/cbp-combo/combo-selling-customer-service/images/testi-2.png" alt=""></div>
						</div>
					</div>
					<div class="adsBotReg" id="adsBotReg">
						<h4>Đăng ký chương trình TRỞ THÀNH CHIẾN BINH SALES <strong>THEO TIÊU CHUẨN QUỐC TẾ</strong></h4>
						<h3><strong>NHẬN NGAY HỌC BỔNG 3.000.000 ĐỒNG TỪ MANA OBS</strong></h3>
            <h4 style="margin-bottom: 20px">Học online cùng Mana Online Business School</h4>
					</div>
          <form action="/course/page/submit" id="landing-page-id">
             <input type="hidden" id="csrf" name="_csrf" />
             <input type="hidden" id="combo_id" name="combo_id" value="669" />
             <input type="hidden" id="advice_name" name="advice_name" value="Mana / Mana - Combo Selling Skills + Customer Services">
             <input type="hidden" id="page_slug" name="page_slug" value="cbp-combo/combo-selling-customer-service">

						<div class="col-md-4">
							<label for="">Họ và tên*</label>
							<div class="txtGR">
								<i class="fa fa-user" aria-hidden="true"></i>
								<input type="text" id="fullname" name="fullname" placeholder="Nhập họ và tên" required>
							</div>
						</div>
						<div class="col-md-4">
							<label for="">Số điện thoại*</label>
							<div class="txtGR">
								<i class="fa fa-phone" aria-hidden="true"></i>
								<input type="text" id="phonenumber" name="phonenumber" placeholder="Nhập số điện thoại" required>
							</div>
						</div>
						<div class="col-md-4">
							<label for="">Email*</label>
							<div class="txtGR">
								<i class="fa fa-envelope" aria-hidden="true"></i>
								<input type="email" id="email" name="email" placeholder="Nhập email" required>
							</div>
						</div>
						<div class="clearfix"></div>
						<button type="submit" class="bt-CTA topCTA">Đăng kí ngay</button>
						<p class="note">Bộ phận Tư vấn Tuyển sinh sẽ liên hệ với bạn để tư vấn về chương trình. Bạn vui lòng giữ máy.</p>
					</form>
				</div>
			</div>
		</div>
	</section>

  <section id="hoi-dap">
    <div class="container">
      <div class="fb-comments fb_iframe_widget fb_iframe_widget_fluid"
        data-href="https://mana.edu.vn/cbp-combo/combo-selling-customer-service"
        data-width="100%" data-numposts="10"
        data-colorscheme="light" fb-xfbml-state="rendered">
       </div>
    </div>
    <!--end .container-->
  </section>

  <footer>
    <div class="container">
      <div class="col-md-4 col-sm-4 col-xs-12 left">
        <a href="https://mana.edu.vn" target="_blank">
          <img src="/mana/cbp-combo/combo-selling-customer-service/images/mana.png" alt="Mana" class="img-responsive">
        </a>
        <a href="https://kyna.vn" target="_blank">
          <img src="/mana/cbp-combo/combo-selling-customer-service/images/kyna.png" alt="kyna" class="img-responsive">
        </a>
      </div>
      <div class="col-md-5 col-sm-5 col-xs-12 mid">
        <p class="comp-name">Công ty Cổ phần Dream Việt Education</p>
        <p><b><u>Trụ sở chính: </u></b>Lầu 6 Tòa nhà Thịnh Phát - 178/8 Đường D1,
        Phường 25, Quận Bình Thạnh, TP Hồ Chí Minh</p>
        <p><b><u>Văn phòng Hà Nội: </u></b>Tầng 6 Tòa nhà Viện Công Nghệ - 25 Vũ Ngọc Phan,
        Phường Láng Hạ, Quận Đống Đa, TP Hà Nội</p>
        <p>
          Giấy phép ĐKKD số 0313589030 do Sở Kế hoạch và Đầu tư TPHCM cấp
        </p>
      </div>
      <div class="col-md-3 col-sm-3 col-xs-12 right">
        <p><b class="company-address">Hotline:</b> <a href="tel:1900 6364 09">1900 6364 09</a></p>
        <p><b class="company-address">Email: </b><a href="mailto:hotro@kyna.vn">hotro@kyna.vn</a></p>
        <div class="fb-page" data-href="https://www.facebook.com/mana.edu.vn/" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/mana.edu.vn/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/mana.edu.vn/">MANA.edu.vn</a></blockquote></div>
      </div>
    </div>
	</footer>
  <!-- end footer -->

  <script type="text/javascript" src="/mana/cbp-combo/combo-selling-customer-service/js/jquery-1.9.1.js"></script>
	<script type="text/javascript" src="/mana/cbp-combo/combo-selling-customer-service/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="/mana/cbp-combo/combo-selling-customer-service/js/slick.min.js"></script>
	<script type="text/javascript" src="/mana/cbp-combo/combo-selling-customer-service/js/main.js"></script>

	<div class="modal fade" id="modal">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-body">
					<div class="popup_body">
						<div class="alert alert-success" role="alert">Cám ơn bạn đã để lại thông tin. Kyna.vn sẽ sớm liên hệ với bạn để tư vấn thêm về chương trình học!</div>
					</div>
				</div>
			</div>
		</div>
	</div>


	<!--End of Zopim Live Chat Script-->
    <!--Start of Zopim Live Chat Script-->
<script type="text/javascript">
    window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
        d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
    _.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
        $.src="//v2.zopim.com/?1bMWnWprkt76mXJXRb6xBuP2dAD7uxPq";z.t=+new Date;$.
            type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
</script>
<!--End of Zopim Live Chat Script-->

	<div id="fb-root"></div>
	<script type="text/javascript">
		(function(d, s, id) {
			var js, fjs = d.getElementsByTagName(s)[0];
			if (d.getElementById(id)) return;
			js = d.createElement(s); js.id = id;
			js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.0&appId=790788601060712";
			fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));
	</script>

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-M92WKP"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<?php $this->endBody()?>
</body>
</html>
