<?php

use mana\models\Setting;
$dir_name = dirname(dirname(dirname(__FILE__)));
?>
<!DOCTYPE html>
<html lang="vi">
<head>
  <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
  	  <meta property="fb:app_id" content="790788601060712">
      <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

      <meta name="description" content="Phát triển phong cách và kỹ năng giao tiếp chuyên nghiệp. Học bổng 3.000.000đ khi đăng ký hôm nay!">
      <meta name="author" content="">
      <!--<link rel="icon" href="">-->
      <title>Trở thành bậc thầy trong giao tiếp kinh doanh - Mana.edu.vn</title>

  	  <meta name="robots" content="index,follow">
      <meta property="og:type" content="website" />
      <meta property='og:url' content='https://mana.edu.vn/cbp-combo/combo-ky-nang-giao-tiep' />
      <meta property='og:image:url' content='https://mana.edu.vn/mana/cbp-combo/combo-ky-nang-giao-tiep/img/thumb.png' />
      <meta property='og:title' content='Trở thành bậc thầy trong giao tiếp kinh doanh - Mana.edu.vn' />
      <meta property='og:description' content='Phát triển phong cách và kỹ năng giao tiếp chuyên nghiệp. Học bổng 3.000.000đ khi đăng ký hôm nay!' />

  <link href="/mana/cbp-combo/combo-ky-nang-giao-tiep/css/bootstrap.css" rel="stylesheet"/>
  <link href="/mana/cbp-combo/combo-ky-nang-giao-tiep/css/style.css" rel="stylesheet"/>
  <link href="/mana/cbp-combo/combo-ky-nang-giao-tiep/css/font-awesome.min.css" rel="stylesheet"/>

  <link rel="stylesheet" type="text/css" href="/mana/cbp-combo/combo-ky-nang-giao-tiep/css/slick.css"/>
  <link rel="stylesheet" type="text/css" href="/mana/cbp-combo/combo-ky-nang-giao-tiep/css/slick-theme.css"/>

  <!-- FontAwesome -->
  <script type="text/javascript" src="/mana/cbp-combo/combo-ky-nang-giao-tiep/js/jquery-1.11.2.min.js"></script>
  <script type="text/javascript" src="/mana/cbp-combo/combo-ky-nang-giao-tiep/js/bootstrap.min.js"></script>

  <!-- LINK FONT -->
  <script type="text/javascript">
  $('body').scrollspy({ target: '#scrollspy-course' })
  </script>

      <?php echo \common\helpers\Html::csrfMetaTags() ?>
</head>
<body data-spy="scroll" data-target="#scrollspy-course" data-sticky_parent>
<?php $this->beginBody()?>

    <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-M92WKP');</script>

    <!-- End Google Tag Manager -->
	<?php
	// get các tham số cần thiết */

	$getParams = $_GET;
	$utm_source = '';
	$utm_medium = '';
	$utm_campaign = '';

	if (isset($getParams['utm_source']) && trim($getParams['utm_source']) != '') {
		$utm_source = trim($getParams['utm_source']);
	}
	if (isset($getParams['utm_medium']) && trim($getParams['utm_medium']) != '') {
		$utm_medium = trim($getParams['utm_medium']);
	}
	if (isset($getParams['utm_campaign']) && trim($getParams['utm_campaign']) != '') {
		$utm_campaign = trim($getParams['utm_campaign']);
	}

	?>
  <header id="wrapper-header">
		<div id="wrap-header">
			<div class="container">
				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-6 logo-left">
					<h2><a href="https://mana.edu.vn/" target="_blank">
            <img src="/mana/cbp-combo/combo-ky-nang-giao-tiep/img/mana.png" alt="mana.edu.vn" class="img-responsive main"/>
            <img src="/mana/cbp-combo/combo-ky-nang-giao-tiep/img/mana-white.png" alt="Kyna.vn" class="img-responsive scroll"/>
          </a></h2>
				</div><!--end .col-sm-6 col-xs-6 logo-left-->
				<div class="col-lg-8 col-md-8 col-sm-6 col-xs-12 center" data-spy="scroll" data-target="#scrollspy-course" data-offset-top="100">
          <div id="scrollspy-course" class="scrollspy-course">
    				<ul>
              <li class="active"><a href="javascript:void(0)" data-link="#content-top" class="scroll nav-link">Nội dung</a></li>
              <li><a data-link="#expert" href="javascript:void(0)" class="scroll nav-link">Giảng viên</a></li>
              <li><a data-link="#opinion" href="javascript:void(0)" class="scroll nav-link">Ý kiến</a></li>
              <li><a data-link="#register" href="javascript:void(0)" class="button-scroll scroll nav-link">Đăng ký ngay <img src="/mana/cbp-combo/combo-ky-nang-giao-tiep/img/icon-click.png" alt="Đăng ký ngay"></a></li>
    				</ul>
          </div>
				</div><!--end .center-->

				<!-- <div class="col-lg-1 col-md-1 col-sm-8 col-xs-6 hotline-right pc font-roboto">
				</div> end .col-sm-6 col-xs-6 hotline-right-->
        <div class="col-sm-8 col-xs-6 scroll-form-style mobile">
					<a target="register" class="button scroll click-scroll-form-study button-scroll">ĐĂNG KÝ NGAY</a>
				</div><!--end .col-xs-6 scroll-form-style mobile -->
			</div><!--end .container-->
		</div>
	</header><!--end #wrap-header-->

  <main>
    <section id="banner">
      <div class="container">
        <h2><strong>TRỞ THÀNH BẬC THẦY GIAO TIẾP</strong></h2>
        <h3>Trong kinh doanh</h3>
        <h4>Với bộ khóa học <span>Kỹ năng giao tiếp (Business Communication)</span> và <span>Nghi thức giao tiếp (Business Etiquette)</span> theo tiêu chuẩn Kinh doanh Quốc tế <span>(*)</span></h4>
        <div class="line"><span></span></div>
        <p>(*) Hoàn thành khóa học, bạn sẽ được cấp chứng chỉ CBP, xét duyệt bởi Hiệp hội Đào tạo Kinh doanh Quốc tế IBTA, chứng chỉ được công nhận trên toàn cầu.</p>
        <div class="button"><a href="#content-center" class="scroll button-click">Tìm hiểu thêm</a></div>
      </div><!--end .container-->
    </section>

    <section id="content-top" data-name='scrolling'>
      <div class="container">
        <div class="col-md-6 col-xs-12 text">
          <h2>85% doanh nhân thất bại</h2>
          <h3>VÌ KHÔNG CÓ KỸ NĂNG GIAO TIẾP TRONG KINH DOANH</h3>
          <div class="line"><span></span></div>
          <p>Theo một nghiên cứu xã hội của Hiệp hội Đào tạo Kinh doanh Quốc tế IBTA:<br/>
<span>85%</span> sự thành công trong kinh doanh đến từ kỹ năng giao tiếp điêu luyện. <span>15%</span> còn lại dựa vào năng lực của bản thân.</p>
<p class="last">Nếu bạn nghĩ rằng Kỹ năng và Nghi thức giao tiếp trong kinh doanh chỉ đơn giản là cách bạn nói chuyện với đồng nghiệp, sếp hay đối tác. <span>Thì bạn đã nhầm rồi!</span></p>
        </div><!--end .text-->
        <div class="col-md-6 col-xs-12 img">
          <img src="/mana/cbp-combo/combo-ky-nang-giao-tiep/img/vi-khong-co-ky-nang-giao-tiep-trong-kinh-doanh.jpg" alt="vi-khong-co-ky-nang-giao-tiep-trong-kinh-doanh" class="img-responsive">
          <p>Bạn có thể học và rèn luyện tất cả kỹ năng giao tiếp tại Mana</p>
        </div><!--end .img-->
        <div class="col-xs-12">
          <div class="bottom">
            <h3>Để phát triển phong cách và nghi thức giao tiếp trong kinh doanh chuyên nghiệp, cần có:</h3>
            <ul>
              <li>
                <div class="media">
                  <div class="media-left">
                    <img src="/mana/cbp-combo/combo-ky-nang-giao-tiep/img/icon-content-bottom.png" alt="">
                  </div>
                  <div class="media-body">
                    Giao tiếp ngôn từ khéo léo, ngôn ngữ cơ thể sống động
                  </div>
                </div>
              </li>

              <li>
                <div class="media">
                  <div class="media-left">
                    <img src="/mana/cbp-combo/combo-ky-nang-giao-tiep/img/icon-content-bottom.png" alt="">
                  </div>
                  <div class="media-body">
                    Kỹ năng viết trong kinh doanh chuyên nghiệp
                  </div>
                </div>
              </li>

              <li>
                <div class="media">
                  <div class="media-left">
                    <img src="/mana/cbp-combo/combo-ky-nang-giao-tiep/img/icon-content-bottom.png" alt="">
                  </div>
                  <div class="media-body">
                    Kỹ năng thuyết trình ấn tượng, tạo sức hút
                  </div>
                </div>
              </li>

              <li>
                <div class="media">
                  <div class="media-left">
                    <img src="/mana/cbp-combo/combo-ky-nang-giao-tiep/img/icon-content-bottom.png" alt="">
                  </div>
                  <div class="media-body">
                    Thương thuyết, thuyết phục và đàm phán hiệu quả
                  </div>
                </div>
              </li>

              <li>
                <div class="media">
                  <div class="media-left">
                    <img src="/mana/cbp-combo/combo-ky-nang-giao-tiep/img/icon-content-bottom.png" alt="">
                  </div>
                  <div class="media-body">
                    Chuẩn mực, nghi thức giao tiếp theo chuẩn quốc tế và đa văn hóa
                  </div>
                </div>
              </li>

              <li>
                <div class="media">
                  <div class="media-left">
                    <img src="/mana/cbp-combo/combo-ky-nang-giao-tiep/img/icon-content-bottom.png" alt="">
                  </div>
                  <div class="media-body">
                    Trang phục và thao tác, hành xử chuyên nghiệp, đúng mực
                  </div>
                </div>
              </li>

            </ul>
          </div><!--end .bottom-->
        </div><!--end .col-xs-12-->
      </div><!--end .container-->
    </section>

    <section id="content-center" data-name='scrolling'>
      <div class="container">
          <div class="col-md-12">
            <div class="top">
              <div class="col-md-4 col-sm-12 col-xs-12 box">
                <h2>Khóa học <span>trở thành bậc thầy giao tiếp trong kinh doanh</span></h2>
              </div><!--end .box-->
              <div class="col-md-4 col-sm-6 col-xs-12 box">
                <img src="/mana/cbp-combo/combo-ky-nang-giao-tiep/img/khoa-hoc-toan-dien.png" alt="2 Khóa học toàn diện" class="img-responsive">
                <p><span>2</span> khóa học toàn diện</p>
              </div><!--end .box-->

              <div class="col-md-4 col-sm-6 col-xs-12 box">
                <img src="/mana/cbp-combo/combo-ky-nang-giao-tiep/img/noi-dung-ve-ban-hang-va-cskh.png" alt="20h+ nội dung về kỹ năng và nghi thức giao tiếp" class="img-responsive">
                <p><span>20h+</span> nội dung về kỹ năng và nghi thức giao tiếp</p>
              </div><!--end .box-->
            </div><!--end .top-->
            <div class="bottom">
              <div class="col-md-4 col-sm-4 col-xs-12 box">
                <img src="/mana/cbp-combo/combo-ky-nang-giao-tiep/img/chuyen-gia-cbp-instructor.png" alt="2 chuyên gia CBP Instructor" class="img-responsive">
                <p><span>2</span> chuyên gia CBP Instructor</p>
              </div><!--end .box-->

              <div class="col-md-4 col-sm-4 col-xs-12 box">
                <img src="/mana/cbp-combo/combo-ky-nang-giao-tiep/img/hoc-truc-tuyen-moi-luc-moi-noi.png" alt="Học trực tuyến mọi lúc mọi nơi" class="img-responsive">
                <p>Học trực tuyến <span>mọi lúc mọi nơi</span></p>
              </div><!--end .box-->

              <div class="col-md-4 col-sm-4 col-xs-12 box">
                <img src="/mana/cbp-combo/combo-ky-nang-giao-tiep/img/tro-giang-1-kem-1.png" alt="Trợ giảng 1 kèm 1" class="img-responsive">
                <p>Trợ giảng <span>1 kèm 1</span></p>
              </div><!--end .box-->
            </div><!--end .bottom-->
          </div>
      </div><!--end .container-->
    </section>

    <section id="content-bottom">
      <div class="container">
        <div class="col-xs-12">
          <div class="wrap content-bottom-pc">

              <div class="col-md-5 col-xs-12 img">
                <h2>Nội dung chương trình</h2>
                <ul class="" role="tablist">
                   <li role="presentation" class="active"><a href="#content-bottom-first" role="tab" data-toggle="tab"><span>1. Khóa học 1:</span> Kỹ năng giao tiếp trong kinh doanh (CBP Business Communication)</a></li>
                   <li role="presentation"><a href="#content-bottom-last" role="tab" data-toggle="tab"><span>2. Khóa học 2:</span> Nghi thức giao tiếp trong kinh doanh (CBP Business Etiquette)</a></li>
                 </ul>
              </div><!--end .img-->
              <div class="col-md-7 col-xs-12 text">


                <!-- Tab panes -->
                <div class="tab-content">
                  <div role="tabpanel" class="tab-pane active" id="content-bottom-first">
                    <ul class="list-click active">
                      <li>
                        <div class="media">
                          <div class="media-left">
                            <img src="/mana/cbp-combo/combo-ky-nang-giao-tiep/img/icon-content-bottom.png" alt="">
                          </div>
                          <div class="media-body">
                            Kỹ năng giao tiếp có vai trò như thế nào trong kinh doanh
                          </div>
                        </div>
                      </li>

                      <li>
                        <div class="media">
                          <div class="media-left">
                            <img src="/mana/cbp-combo/combo-ky-nang-giao-tiep/img/icon-content-bottom.png" alt="">
                          </div>
                          <div class="media-body">
                            Cấu trúc giao tiếp trong kinh doanh và các yếu tố ảnh hưởng đến quá trình giao tiếp thường ngày
                          </div>
                        </div>
                      </li>

                      <li>
                        <div class="media">
                          <div class="media-left">
                            <img src="/mana/cbp-combo/combo-ky-nang-giao-tiep/img/icon-content-bottom.png" alt="">
                          </div>
                          <div class="media-body">
                            Phát triển phong cách viết trong kinh doanh chuyên nghiệp
                          </div>
                        </div>
                      </li>

                      <li>
                        <div class="media">
                          <div class="media-left">
                            <img src="/mana/cbp-combo/combo-ky-nang-giao-tiep/img/icon-content-bottom.png" alt="">
                          </div>
                          <div class="media-body">
                            Kỹ năng viết các văn bản thường gặp trong kinh doanh bao gồm bản ghi nhớ, báo cáo, email v.v
                          </div>
                        </div>
                      </li>

                      <li>
                        <div class="media">
                          <div class="media-left">
                            <img src="/mana/cbp-combo/combo-ky-nang-giao-tiep/img/icon-content-bottom.png" alt="">
                          </div>
                          <div class="media-body">
                            Phát triển kỹ năng giao tiếp ngôn từ lôi cuốn, tự tin
                          </div>
                        </div>
                      </li>

                      <li>
                        <div class="media">
                          <div class="media-left">
                            <img src="/mana/cbp-combo/combo-ky-nang-giao-tiep/img/icon-content-bottom.png" alt="">
                          </div>
                          <div class="media-body">
                            Giao tiếp và trao đổi bằng điện thoại trong kinh doanh
                          </div>
                        </div>
                      </li>

                      <li>
                        <div class="media">
                          <div class="media-left">
                            <img src="/mana/cbp-combo/combo-ky-nang-giao-tiep/img/icon-content-bottom.png" alt="">
                          </div>
                          <div class="media-body">
                            Ngôn ngữ cơ thể - công cụ giao tiếp quyền lực
                          </div>
                        </div>
                      </li>

                      <li>
                        <div class="media">
                          <div class="media-left">
                            <img src="/mana/cbp-combo/combo-ky-nang-giao-tiep/img/icon-content-bottom.png" alt="">
                          </div>
                          <div class="media-body">
                            Phát triển kỹ năng thuyết trình hiệu quả và tự tin
                          </div>
                        </div>
                      </li>

                      <li>
                        <div class="media">
                          <div class="media-left">
                            <img src="/mana/cbp-combo/combo-ky-nang-giao-tiep/img/icon-content-bottom.png" alt="">
                          </div>
                          <div class="media-body">
                            Giải quyết mâu thuẫn xung đột và những bất đồng trong giao tiếp kinh doanh
                          </div>
                        </div>
                      </li>
                    </ul>

                  </div>
                  <div role="tabpanel" class="tab-pane" id="content-bottom-last">
                    <ul class="list-click">
                      <li>
                        <div class="media">
                          <div class="media-left">
                            <img src="/mana/cbp-combo/combo-ky-nang-giao-tiep/img/icon-content-bottom.png" alt="">
                          </div>
                          <div class="media-body">
                            Tầm quan trọng của nghi thức giao tiếp trong kinh doanh
                          </div>
                        </div>
                      </li>

                      <li>
                        <div class="media">
                          <div class="media-left">
                            <img src="/mana/cbp-combo/combo-ky-nang-giao-tiep/img/icon-content-bottom.png" alt="">
                          </div>
                          <div class="media-body">
                            Chào hỏi và giới thiệu bản thân
                          </div>
                        </div>
                      </li>

                      <li>
                        <div class="media">
                          <div class="media-left">
                            <img src="/mana/cbp-combo/combo-ky-nang-giao-tiep/img/icon-content-bottom.png" alt="">
                          </div>
                          <div class="media-body">
                            Cách tạo ấn tượng đầu tiên tốt đẹp và cách xây dựng đội ngũ Tiếp tân chuyên nghiệp
                          </div>
                        </div>
                      </li>

                      <li>
                        <div class="media">
                          <div class="media-left">
                            <img src="/mana/cbp-combo/combo-ky-nang-giao-tiep/img/icon-content-bottom.png" alt="">
                          </div>
                          <div class="media-body">
                            Tổ chức và tiến hành một buổi họp
                          </div>
                        </div>
                      </li>

                      <li>
                        <div class="media">
                          <div class="media-left">
                            <img src="/mana/cbp-combo/combo-ky-nang-giao-tiep/img/icon-content-bottom.png" alt="">
                          </div>
                          <div class="media-body">
                            Đạo đức kinh doanh
                          </div>
                        </div>
                      </li>

                      <li>
                        <div class="media">
                          <div class="media-left">
                            <img src="/mana/cbp-combo/combo-ky-nang-giao-tiep/img/icon-content-bottom.png" alt="">
                          </div>
                          <div class="media-body">
                            Nghi thức giao tiếp khi chiêu đãi đối tác
                          </div>
                        </div>
                      </li>

                      <li>
                        <div class="media">
                          <div class="media-left">
                            <img src="/mana/cbp-combo/combo-ky-nang-giao-tiep/img/icon-content-bottom.png" alt="">
                          </div>
                          <div class="media-body">
                            Quy tắc ăn uống tại các quốc gia
                          </div>
                        </div>
                      </li>

                      <li>
                        <div class="media">
                          <div class="media-left">
                            <img src="/mana/cbp-combo/combo-ky-nang-giao-tiep/img/icon-content-bottom.png" alt="">
                          </div>
                          <div class="media-body">
                            Nghi thức giao tiếp qua điện thoại, Internet và thư điện tử
                          </div>
                        </div>
                      </li>

                      <li>
                        <div class="media">
                          <div class="media-left">
                            <img src="/mana/cbp-combo/combo-ky-nang-giao-tiep/img/icon-content-bottom.png" alt="">
                          </div>
                          <div class="media-body">
                            Cách ăn mặc và tác phong kinh doanh chuyên nghiệp
                          </div>
                        </div>
                      </li>

                      <li>
                        <div class="media">
                          <div class="media-left">
                            <img src="/mana/cbp-combo/combo-ky-nang-giao-tiep/img/icon-content-bottom.png" alt="">
                          </div>
                          <div class="media-body">
                            Quy tắc ứng xử với người khuyết tật
                          </div>
                        </div>
                      </li>

                      <li>
                        <div class="media">
                          <div class="media-left">
                            <img src="/mana/cbp-combo/combo-ky-nang-giao-tiep/img/icon-content-bottom.png" alt="">
                          </div>
                          <div class="media-body">
                            Môi trường kinh doanh đa văn hóa
                          </div>
                        </div>
                      </li>
                    </ul>
                  </div>

                </div>


              </div><!--end .text-->
            </div>


            <div class="panel-group content-bottom-mb" id="accordion" role="tablist" aria-multiselectable="true">

              <h2>NỘI DUNG CHƯƠNG TRÌNH</h2>
               <div class="panel panel-default first">
                 <div class="panel-heading" role="tab" id="headingOne">
                   <h4 class="panel-title">
                     <a class="accordion-toggle" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true">
                       <span>1. Khóa học 1:</span> Kỹ năng giao tiếp trong kinh doanh (CBP Business Communication)
                     </a>
                   </h4>
                 </div>
                 <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                   <div class="panel-body">
                     <ul>
                       <li>Kỹ năng giao tiếp có vai trò như thế nào trong kinh doanh</li>
                       <li>Cấu trúc giao tiếp trong kinh doanh và các yếu tố ảnh hưởng đến quá trình giao tiếp thường ngày</li>
                       <li>Phát triển phong cách viết trong kinh doanh chuyên nghiệp</li>
                       <li>Kỹ năng viết các văn bản thường gặp trong kinh doanh bao gồm bản ghi nhớ, báo cáo, email v.v</li>
                       <li>Phát triển kỹ năng giao tiếp ngôn từ lôi cuốn, tự tin</li>
                       <li>Giao tiếp và trao đổi bằng điện thoại trong kinh doanh</li>
                       <li>Ngôn ngữ cơ thể - công cụ giao tiếp quyền lực</li>
                       <li>Phát triển kỹ năng thuyết trình hiệu quả và tự tin</li>
                       <li>Giải quyết mâu thuẫn xung đột và những bất đồng trong giao tiếp kinh doanh</li>
                     </ul>
                   </div>
                 </div>
               </div>

               <div class="panel panel-default last">
                 <div class="panel-heading" role="tab" id="headingTwo">
                   <h4 class="panel-title">
                     <a class="collapsed accordion-toggle" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false">
                       <span>2. Khóa học 2:</span> Nghi thức giao tiếp trong kinh doanh (CBP Business Etiquette)
                     </a>
                   </h4>
                 </div>
                 <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                   <div class="panel-body">
                     <ul>
                       <li>Tầm quan trọng của nghi thức giao tiếp trong kinh doanh</li>
                       <li>Chào hỏi và giới thiệu bản thân</li>
                       <li>Cách tạo ấn tượng đầu tiên tốt đẹp và cách xây dựng đội ngũ Tiếp tân chuyên nghiệp</li>
                       <li>Tổ chức và tiến hành một buổi họp</li>
                       <li>Đạo đức kinh doanh</li>
                       <li>Nghi thức giao tiếp khi chiêu đãi đối tác</li>
                       <li>Quy tắc ăn uống tại các quốc gia</li>
                       <li>Nghi thức giao tiếp qua điện thoại, Internet và thư điện tử</li>
                       <li>Cách ăn mặc và tác phong kinh doanh chuyên nghiệp</li>
                       <li>Quy tắc ứng xử với người khuyết tật</li>
                       <li>Môi trường kinh doanh đa văn hóa</li>
                     </ul>
                   </div>
                 </div>
               </div>

             </div>



          </div>
      </div><!--end .container-->
    </section>

    <section id="expert" data-name='scrolling'>
      <div class="container">
        <h2>CHUYÊN GIA HÀNG ĐẦU</h2>
        <div class="col-md-10 col-md-offset-1 col-xs-12 main expert-slider slick-slider">
          <div class="wrap-content">
            <div class="col-md-4 col-xs-12 img"><img src="/mana/cbp-combo/combo-ky-nang-giao-tiep/img/ho-thi-thanh-van.png" alt="Thạc sỹ Hồ Thị Thanh Vân" class="img-responsive"></div>
            <div class="col-md-8 col-xs-12 text">
              <div class="wrap">
                <h3>Thạc sỹ Hồ Thị Thanh Vân (Vân Hồ)</h3>
                <h4>CBP Certified Instructor - Giảng viên khóa Kỹ năng giao tiếp trong kinh doanh</h4>
                <ul>
                  <li>
                    <div class="media">
                      <div class="media-left">
                        <img src="/mana/cbp-combo/combo-ky-nang-giao-tiep/img/icon-expert.png" alt="">
                      </div>
                      <div class="media-body">
                        <p>Thạc sỹ chuyên ngành Marketing Bán hàng và Dịch vụ (MMSS) tại trường Đại học Paris 1 Sorbonne (IAE) phối hợp với trường Quản trị Châu Âu (ESCP - EAP European School Management).</p>
                      </div>
                    </div>
                  </li>

                  <li>
                    <div class="media">
                      <div class="media-left">
                        <img src="/mana/cbp-combo/combo-ky-nang-giao-tiep/img/icon-expert.png" alt="">
                      </div>
                      <div class="media-body">
                        <p>20 năm kinh nghiệm trong lĩnh vực Sales và Marketing tại các công ty hàng đầu thế giới.</p>
                      </div>
                    </div>
                  </li>

                  <li>
                    <div class="media">
                      <div class="media-left">
                        <img src="/mana/cbp-combo/combo-ky-nang-giao-tiep/img/icon-expert.png" alt="">
                      </div>
                      <div class="media-body">
                        <p>Dành nhiều giải thưởng danh giá trong lĩnh vực Sales &amp; Marketing.</p>
                      </div>
                    </div>
                  </li>
                </ul>
              </div>
            </div>
          </div>

          <div class="wrap-content">
            <div class="col-md-4 col-xs-12 img"><img src="/mana/cbp-combo/combo-ky-nang-giao-tiep/img/nguyenkientri.png" alt="Nguyễn Kiên Trì" class="img-responsive"></div>
            <div class="col-md-8 col-xs-12 text">
              <div class="wrap">
                <h3>Tiến sĩ Nguyễn Kiên Trì</h3>
                <h4>CBP Certified Instructor - Giảng viên khóa Nghi thức giao tiếp trong kinh doanh</h4>
                <ul>
                  <li>
                    <div class="media">
                      <div class="media-left">
                        <img src="/mana/cbp-combo/combo-ky-nang-giao-tiep/img/icon-expert.png" alt="">
                      </div>
                      <div class="media-body">
                        <p>MBA Tư vấn Quản lý Quốc tế - Thuỵ Sỹ</p>
                      </div>
                    </div>
                  </li>

                  <li>
                    <div class="media">
                      <div class="media-left">
                        <img src="/mana/cbp-combo/combo-ky-nang-giao-tiep/img/icon-expert.png" alt="">
                      </div>
                      <div class="media-body">
                        <p>Expert Economic Certified Đại học California</p>
                      </div>
                    </div>
                  </li>

                  <li>
                    <div class="media">
                      <div class="media-left">
                        <img src="/mana/cbp-combo/combo-ky-nang-giao-tiep/img/icon-expert.png" alt="">
                      </div>
                      <div class="media-body">
                        <p>Nhiều năm kinh nghiệm trong việc quản trị và giảng dạy, hiện đang nắm giữ các vị trí cấp cao tại các tập đoàn lớn.</p>
                      </div>
                    </div>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div><!--end .container-->
    </section>

    <section id="online">

      <div class="container">
        <div class="col-md-3 col-xs-12 img">

        </div><!--end .img-->
        <div class="col-md-9 col-xs-12 text clearfix">
          <h2>Học online "1 kèm 1" với cố vấn học tập</h2>
          <div class="col-md-6 col-xs-12 left">
            <ul>
              <li>
                <h3><img src="/mana/cbp-combo/combo-ky-nang-giao-tiep/img/hoc-online-moi-luc-moi-noi.png" alt="Học online mọi lúc mọi nơi"> Học online mọi lúc mọi nơi</h3>
                <p>Học qua video bài giảng được biên tập chuyên nghiệp. Tài liệu bao gồm sách và bài thuyết trình, tư liệu tham khảo.Hỏi đáp cùng trợ giảng 1 kèm 1</p>
              </li>
              <li>
                <h3><img src="/mana/cbp-combo/combo-ky-nang-giao-tiep/img/hoc-ket-hop-cung-thuc-hanh-va-luyen-thi.png" alt="Học kết hợp cùng thực hành và luyện thi"> Học kết hợp cùng thực hành và luyện thi: </h3>
                <p>Chương trình học được đan xen giữa bài học lý thuyết và thực hành, cùng hơn 100 câu thi thử.</p>
              </li>
            </ul>
          </div><!--end .left-->
          <div class="col-md-6 col-xs-12 right">
            <ul>
              <li>
                <h3><img src="/mana/cbp-combo/combo-ky-nang-giao-tiep/img/hoc-kem.png" alt="Học kèm 1-1"> Học kèm 1-1</h3>
                <p>Cố vấn 1 kèm 1, trong suốt quá trình học và luyện thi. Hỏi đáp cùng chuyên gia, doanh nhân giàu kinh nghiệm.</p>
              </li>
              <li>
                <h3><img src="/mana/cbp-combo/combo-ky-nang-giao-tiep/img/thi-truc-tuyen-tai-nha-va-nhan-chung-chi-quoc-te.png" alt="Thi trực tuyến tại nhà và nhân chứng chỉ quốc tế"> Thi trực tuyến tại nhà và nhận chứng chỉ quốc tế:</h3>
                <p>Thi trực tuyến trên hệ thống PROMETRIC với tối đa 6 tháng ôn luyện trước khi thi.</p>
              </li>
            </ul>
          </div><!--end .right-->
        </div><!--end .text-->
      </div>
    </section>

    <section id="online-study">
      <h2>Chương trình học online theo tiêu chuẩn quốc tế</h2>
      <div class="container">
        <div class="col-xs-12">
          <div class="wrap">
          <div class="col-md-3 col-sm-6 col-xs-12 wrap-box">
            <div class="box">
              <p>Khóa học được phát triển dựa trên kiến thức và kinh nghiệm của các doanh nhân thành công trên thế giới.</p>
              <img src="/mana/cbp-combo/combo-ky-nang-giao-tiep/img/online-study-1.png" alt="" class="img-responsive">
            </div>
          </div><!--end .box-->

          <div class="col-md-3 col-sm-6 col-xs-12 wrap-box">
            <div class="box">
              <p>Được nhận chứng chỉ CBP (chứng chỉ Kinh doanh Chuyên nghiệp) cấp bởi Hiệp hội IBTA, được công nhận trên toàn cầu.</p>
              <img src="/mana/cbp-combo/combo-ky-nang-giao-tiep/img/online-study-3.png" alt="" class="img-responsive">
            </div>
          </div><!--end .box-->

          <div class="col-md-3 col-sm-6 col-xs-12 wrap-box">
            <div class="box">
              <p>Được bảo trợ nội dung bởi tổ chức học thuật uy tín - Hiệp hội Đào tạo Kinh doanh Quốc tế IBTA.</p>
              <img src="/mana/cbp-combo/combo-ky-nang-giao-tiep/img/online-study-2.png" alt="" class="img-responsive">
            </div>
          </div><!--end .box-->

          <div class="col-md-3 col-sm-6 col-xs-12 wrap-box">
            <div class="box">
              <p>Sở hữu video trọn đời và toàn bộ các cập nhật về kiến thức, nghề nghiệp từ Mana Online Business School hoàn toàn miễn phí.</p>
              <img src="/mana/cbp-combo/combo-ky-nang-giao-tiep/img/online-study-4.png" alt="" class="img-responsive">
            </div>
          </div><!--end .box-->
        </div>
        </div>
      </div>
    </section>

    <section id="register-text">
      <div class="container">
        <h2>Đăng ký học và nhận học bổng trị giá <span>3.000.000 ĐỒNG</span></h2>
      </div><!--end .container-->
    </section>

    <section id="cbp-certificate">
      <div class="container">
        <div class="col-xs-12 wrapper">
          <div class="wrap clearfix">
            <div class="col-md-4 col-xs-12 img">
              <img src="/mana/cbp-combo/combo-ky-nang-giao-tiep/img/trong-tran.png" alt="Trọng Trần" class="img-responsive">
            </div><!--end .img-->
            <div class="col-md-8 col-xs-12 text">
              <h2>SỞ HỮU CHỨNG CHỈ CBP</h2>
              <h5>Chứng Chỉ Kinh Doanh Được Toàn Thế Giới Công Nhận</h5>
              <p>Chứng chỉ CBP của IBTA là một trong những chứng chỉ uy tín nhất về kỹ năng kinh doanh chuyên nghiệp và kỹ năng quản lý.</p>
              <p>CBP được công nhận trên toàn cầu từ Mỹ, Canada, Caribe, châu Phi, Trung Đông, Trung Quốc, Ấn Độ, đến Singapore và vùng Viễn Đông.</p>
              <p class="italic"><span>Trên thế giới, các tập đoàn lớn như HONDA, Nokia, Mitsubishi, Intel, Ricoh, adidas,… đều công nhận tiêu chuẩn đánh giá về kỹ năng kinh doanh của IBTA được mô tả qua chứng chỉ CBP.</span></p>
            </div><!--end .img-->
          </div><!--end .wrap-->
        </div>
        <img src="/mana/cbp-combo/combo-ky-nang-giao-tiep/img/line-content-bottom.png" alt="" class="img-responsive line">
      </div><!--end .container-->
    </section>

    <section id="opinion" data-name="scrolling">
      <div class="container">
        <div class="col-xs-12">
          <div class="col-lg-1 col-xs-12 icon">
            <img src="/mana/cbp-combo/combo-ky-nang-giao-tiep/img/icon.png" alt="" class="img-responsive">
          </div>
          <div class="col-lg-11 col-xs-12 img">
            <ul>
              <li>
                <img src="/mana/cbp-combo/combo-ky-nang-giao-tiep/img/thanh-minh.jpg" alt="Nguyễn Thanh Minh" class="img-responsive">
                <div class="title">
                  <div>
                    <h4>Nguyễn Thanh Minh</h4>
                    <span><i class="fa fa-chevron-down" aria-hidden="true"></i></span>
                    <span><i class="fa fa-chevron-down" aria-hidden="true"></i></span>
                  </div>
                </div>
                <div class="text">
                  <p>Trải qua sự bối rối khi đi tiếp đối tác là người Nhật, tôi mới thấy được các nghi thức xã giao là vô cùng quan trọng. Tôi và đối tác chỉ là những người gặp nhau vì làm ăn, không ai kịp hiểu đối phương là gì nên việc có thái độ đúng mực và đúng chuẩn lại chính là mấu chốt của mối quan hệ. Khóa học này đủ để tôi lấy làm kinh nghiệm cả đời</p>
                </div>

              </li>

              <li>
                <img src="/mana/cbp-combo/combo-ky-nang-giao-tiep/img/trang-nha.jpg" alt="Trang Nhã" class="img-responsive">
                <div class="title">
                  <div>
                    <h4>Trang Nhã</h4>
                    <span><i class="fa fa-chevron-down" aria-hidden="true"></i></span>
                    <span><i class="fa fa-chevron-down" aria-hidden="true"></i></span>
                  </div>
                </div>
                <div class="text">
                  <p>Đi làm rồi mới biết kỹ năng giao tiếp là kỹ năng cần thiết nhất ấy. Một người giỏi làm mà không giỏi nói thì sẽ khó mà được trọng dụng. Mà chị thấy riêng việc giao tiếp tốt với đồng nghiệp thôi thì công việc đã dễ thở hơn gấp ngàn lần rồi. Học cái này không thừa, không hề thừa một chút nào luôn.</p>
                </div>


              </li>
            </ul>
          </div>
        </div>
      </div><!--end .container-->
    </section>

    <section id="register" data-name="scrolling">
      <div class="container">
        <div class="col-xs-12">
          <div class="wrap">
            <h2>Đăng ký chương trình</h2>
            <div class="wrap-line">
              <div class="line">
              <h3>Trở thành bậc thầy giao tiếp trong kinh doanh</h3>
              <h3><span>Nhận ngay học bổng 3.000.000 đồng từ mana obs</span></h3>
              </div>
            </div><!--end .wrap-line-->
            <h2 style="margin-bottom: 50px">Học online cùng Mana Online Business School</h2>
            <form action="/course/page/submit" id="landing-page-id">
      					<div class="form-s">
                  <input type="hidden" id="csrf" name="_csrf" />
                  <input type="hidden" id="combo_id" name="combo_id" value="676" />
                  <input type="hidden" id="advice_name" name="advice_name" value="Mana / Mana - Combo nghĩ thức + kỹ năng giao tiếp">
                  <input type="hidden" id="page_slug" name="page_slug" value="cbp-combo/combo-ky-nang-giao-tiep">
      	          <div class="form-group col-md-4 col-xs-12">
      	              <div class="input">
      										<label>Họ và tên (*)</label>
                          <div>
                            <span><i class="fa fa-user" aria-hidden="true"></i></span>
        	                  <input type="text" class="form-control" id="fullname" name="fullname" value="" required="" data-errormessage="Họ và tên" placeholder="Nhập họ và tên">
                          </div>
                      </div>
      	          </div>

      	          <div class="form-group col-md-4 col-xs-12">
      	              <div class="input">
      										<label>Số điện thoại (*)</label>
                          <div>
                            <span><i class="fa fa-phone" aria-hidden="true"></i></span>
        	                  <input type="text" class="form-control" name="phonenumber" id="phonenumber" value="" maxlength="50" required="" data-errormessage="Cung cấp sdt để được hỗ trợ tốt hơn trong quá trình thanh toán, tham gia học" placeholder="Nhập số điện thoại">
                          </div>
                      </div>
      	          </div>

                  <div class="form-group col-md-4 col-xs-12">
      	              <div class="input">
      										<label>Email (*)</label>
                          <div>
                            <span><i class="fa fa-envelope" aria-hidden="true"></i></span>
        	                  <input type="email" id="email" name="email" value="" required="" data-errormessage="Nhập chính xác email của bạn.." class="form-control" placeholder="Nhập email">
                          </div>
                      </div>
      	          </div>

      	      </div>
      				<div class="btn-submit">
                  <button type="submit" class="btn_box_register button-regis bt-CTA topCTA">Đăng ký</button>
      						<p>Bộ phận Tư vấn Tuyển sinh sẽ liên hệ với bạn để tư vấn về chương trình. Bạn vui lòng giữ máy.</p>
              </div>
            </form>
            </div><!--end .wrap-->
          </div>
      </div><!--end .container-->
    </section>

   <div id="comment">
      <div class="container">
        <div class="col-xs-12">
          <div class="fb-comments fb_iframe_widget fb_iframe_widget_fluid"
            data-href="https://mana.edu.vn/cbp-combo/combo-ky-nang-giao-tiep"
            data-width="100%" data-numposts="10" data-colorscheme="light">
          </div>
        </div>
      </div>
    </div>

  </main>

  <footer>
            <div class="container">
              <div class="col-md-4 col-sm-4 col-xs-12 left">
                <a href="https://mana.edu.vn" target="_blank">
                  <img src="/mana/cbp-combo/combo-ky-nang-giao-tiep/img/mana-white.png" alt="Mana" class="img-responsive">
                </a>
                <a href="https://kyna.vn" target="_blank">
                  <img src="/mana/cbp-combo/combo-ky-nang-giao-tiep/img/kyna.png" alt="KynA" class="img-responsive">
                </a>
              </div>
              <div class="col-md-5 col-sm-5 col-xs-12 mid">
                <p class="comp-name">Công ty Cổ phần Dream Việt Education</p>
                <p><b><u>Trụ sở chính: </u></b>Lầu 6 Tòa nhà Thịnh Phát - 178/8 Đường D1,
                Phường 25, Quận Bình Thạnh, TP Hồ Chí Minh</p>
                <p><b><u>Văn phòng Hà Nội: </u></b>Tầng 6 Tòa nhà Viện Công Nghệ - 25 Vũ Ngọc Phan,
                Phường Láng Hạ, Quận Đống Đa, TP Hà Nội</p>
                <p>
                  Giấy phép ĐKKD số 0313589030 do Sở Kế hoạch và Đầu tư TPHCM cấp
                </p>
              </div>
              <div class="col-md-3 col-sm-3 col-xs-12 right">
                <p><b class="company-address">Hotline:</b> 1900 6364 09</p>
                <p><b class="company-address">Email: </b>hotro@kyna.vn</p>
                <div class="fb-page" data-href="https://www.facebook.com/mana.edu.vn/" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/mana.edu.vn/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/mana.edu.vn/">MANA.edu.vn</a></blockquote></div>
              </div>
            </div>
        </footer>


        	<div class="modal fade" id="modal">
        		<div class="modal-dialog">
        			<div class="modal-content">
        				<div class="modal-body">
        					<div class="popup_body">
        						<div class="alert alert-success" role="alert">Cám ơn bạn đã để lại thông tin. Kyna.vn sẽ sớm liên hệ với bạn để tư vấn thêm về chương trình học!</div>
        					</div>
        				</div>
        			</div>
        		</div>
        	</div>


        	<!--End of Zopim Live Chat Script-->
            <!--Start of Zopim Live Chat Script-->
        <script type="text/javascript">
            window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
                d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
            _.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
                $.src="//v2.zopim.com/?1bMWnWprkt76mXJXRb6xBuP2dAD7uxPq";z.t=+new Date;$.
                    type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
        </script>
        <!--End of Zopim Live Chat Script-->

        	<div id="fb-root"></div>
        	<script type="text/javascript">
        		(function(d, s, id) {
        			var js, fjs = d.getElementsByTagName(s)[0];
        			if (d.getElementById(id)) return;
        			js = d.createElement(s); js.id = id;
        			js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.0&appId=790788601060712";
        			fjs.parentNode.insertBefore(js, fjs);
        		}(document, 'script', 'facebook-jssdk'));
        	</script>

        <!-- Google Tag Manager (noscript) -->
        <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-M92WKP"
        height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
        <!-- End Google Tag Manager (noscript) -->



    <script src="/mana/cbp-combo/combo-ky-nang-giao-tiep/js/slick.min.js" type="text/javascript" charset="utf-8"></script>
<script type="text/javascript" src="/mana/cbp-combo/combo-ky-nang-giao-tiep/js/main.js"></script>
<?php $this->endBody()?>
</body>
</html>
