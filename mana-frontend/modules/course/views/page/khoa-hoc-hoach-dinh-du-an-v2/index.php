<?php

use mana\models\Setting;

$dir_name = dirname(dirname(dirname(__FILE__)));
?>
<!DOCTYPE html>
<html lang="en">

    <!-- Mirrored from mana.edu.vn/cbp/khoa-hoc-ky-nang-giao-tiep/ by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 27 Apr 2016 13:29:41 GMT -->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta property="fb:app_id" content="790788601060712">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Đào tạo Project Management chứng nhận quốc tế - Mana.edu.vn</title>
        <link href='https://fonts.googleapis.com/css?family=Roboto:400,300,500,700' rel='stylesheet' type='text/css'>
        <!-- Bootstrap -->
        <link href="/mana/cs/khoa-hoc-hoach-dinh-du-an-v2/css/bootstrap.min.css" rel="stylesheet"/>
        <!-- <link href="/mana/cs/khoa-hoc-hoach-dinh-du-an-v2/css/site.css" rel="stylesheet"/> -->
        <link href="/mana/cs/khoa-hoc-hoach-dinh-du-an-v2/css/style.css" rel="stylesheet"/>
        <link href="/mana/cs/khoa-hoc-hoach-dinh-du-an-v2/css/jquery.fullPage.css" rel="stylesheet"/>
        <link href="/mana/cs/khoa-hoc-hoach-dinh-du-an-v2/css/font-awesome.min.css" rel="stylesheet"/>
        <link href="/mana/cs/khoa-hoc-hoach-dinh-du-an-v2/css/owl.carousel.css" rel="stylesheet"/>
        <link href="/mana/cs/khoa-hoc-hoach-dinh-du-an-v2/css/owl.theme.css" rel="stylesheet"/>
        <link href="/mana/cs/khoa-hoc-hoach-dinh-du-an-v2/css/owl.transitions.css" rel="stylesheet"/>
        <link rel="icon" href="/mana/images/manaFavicon.png">
          <?php echo \common\helpers\Html::csrfMetaTags() ?>

    </head>
    <body>
    <?php $this->beginBody()?>

    <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-M92WKP');</script>
    <!-- End Google Tag Manager -->
        <?php
// get các tham số cần thiết */
        $getParams = $_GET;
        $utm_source = '';
        $utm_medium = '';
        $utm_campaign = '';

        if (isset($getParams['utm_source']) && trim($getParams['utm_source']) != '') {
            $utm_source = trim($getParams['utm_source']);
        }
        if (isset($getParams['utm_medium']) && trim($getParams['utm_medium']) != '') {
            $utm_medium = trim($getParams['utm_medium']);
        }
        if (isset($getParams['utm_campaign']) && trim($getParams['utm_campaign']) != '') {
            $utm_campaign = trim($getParams['utm_campaign']);
        }
        ?>


        <nav class="navbar navbar-default" id="wrap-header" data-sticky_column data-spy="scroll" data-target="#scrollspy-course" data-offset-top="100" class="is_stuck">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#scrollspy-course" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a href="https://mana.edu.vn" class="mana-logo"><img src="/mana/cs/khoa-hoc-hoach-dinh-du-an-v2/images/mana.png" alt="Mana" ></a>
                </div>

                <div class="collapse navbar-collapse" id="scrollspy-course">
                    <ul class="nav navbar-nav">
                        <li><a href="#banner">GIỚI THIỆU</a></li>
                        <li><a href="#khoa-hoc">NỘI DUNG</a></li>
                        <li><a href="#giang-vien">GIẢNG VIÊN</a></li>
                        <li><a href="#loi-ich">LỢI ÍCH</a></li>
                        <li><a href="#don-vi-cap-bang">ĐƠN VỊ CẤP BẰNG</a></li>
                        <li><a href="#form-dang-ky">ĐĂNG KÝ</a></li>
                    </ul>
                </div>
                <!-- /.navbar-collapse -->
            </div>
            <!-- /.container -->
        </nav>

        <div id="fullpage">

            <div id='banner' class="section" >
                <div class="container">
                    <div class="row">
                        <div class="col-md-7 col-md-offset-5 text-content">
                            <div class="text-content-title">
                                <h3>Khoá học</h3>
                                <h1>HOẠCH ĐỊNH DỰ ÁN HIỆU QUẢ</h1>
                                <hr>
                                <p>
                                    Khóa học giúp bạn dễ dàng xác định phạm vi và quản lý dự án. Từ đó, xây dựng được cấu trúc phân chia công việc,
                                    sử dụng nguồn nhân lực một cách hợp lý, cách lên kế hoạch dự án để phát triển dự án một cách tốt nhất.
                                </p>
                                <p>
                                    Chương trình học được phát triển dựa trên giáo trình của
                                    <b>Đại học California (Hoa Kỳ).</b>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <i class="fa fa-angle-down"></i>
            </div>
            <!-- /banner -->

            <div id="loi-ich" class="section">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6">
                            <h3>Lợi ích của khóa học</h3>
                            <ul>
                                <li><p>
                                        Kiến thức cơ bản về hoạch định và quản lý dự án.
                                    </p></li>
                                <li><p>
                                        Kiến thức chuyên ngành vững chắc.
                                    </p></li>
                                <li><p>
                                        Áp dụng được ngay lặp tức các kiến thức trong khóa học.
                                    </p></li>
                                <li><p>
                                        Có tư duy chiến lược về ngân sách và thời gian.
                                    </p></li>
                                <li><p>
                                        Hiểu vai trò và trách nhiệm của người quản lý dự án và nhóm dự án.
                                    </p></li>
                                <li><p>
                                        Có cơ hội thực hành liên tục.
                                    </p></li>
                            </ul>
                        </div>
                        <!-- /col-md-6 -->
                    </div>
                    <!-- row -->
                    <i class="fa fa-angle-down"></i>
                </div>
                <!-- container -->
            </div>
            <!-- /loi-ich -->

            <div id="khoa-hoc" class="section">
                <div class="container">
                    <div class="row">
                        <h3>Khóa học diễn ra trong 6 tuần</h3>
                        <div class="col-md-7 list-tuan-hoc">
                            <ul>
                                <li><a class="active" data-target="tuan1">Tuần 1</a></li>
                                <li><a data-target="tuan2">Tuần 2</a></li>
                                <li><a data-target="tuan3">Tuần 3</a></li>
                                <li><a data-target="tuan4">Tuần 4</a></li>
                                <li><a data-target="tuan5">Tuần 5</a></li>
                                <li><a data-target="tuan6">Tuần 6</a></li>
                            </ul>
                        </div>

                    </div>
                    <div class="row tuan tuan1">
                        <a class="xs-title">Tuần 1</a>
                        <div class="text-content">
                            <p>
                                Tìm hiểu tổng quan về chương trình học,
                                các tài liệu liên quan và tham gia khảo sát đầu khóa học.
                            </p>
                            <a href="#form-dang-ky" class="btn-dang-ky-hoc">Đăng ký học</a>
                        </div>

                    </div>
                    <!-- /row tuan1 -->

                    <div class="row tuan tuan2">
                        <a class="xs-title">Tuần 2</a>
                        <div class="col-md-4 left">
                            <h4>GIỚI THIỆU VỀ DỰ ÁN</h4>
                            <div class="line"></div>
                            <p>
                                Trong phần này, học viên sẽ xác định:
                            </p>
                            <ul>
                                <li>Những đặc tính quan trọng của một dự án</li>
                                <li>Những ràng buộc chính của dự án</li>
                                <li>Vai trò - trách nhiệm của quản lý dự án</li>
                                <li>Xác định cấu trúc của dự án</li>
                            </ul>
                            <!-- <p>
                              <b>CASE STUDY</b>: Pepsi Refresh
                            </p> -->
                            <a href="#form-dang-ky" class="btn-dang-ky-hoc">Đăng ký học</a>
                        </div>
                        <!-- left -->
                        <div class="col-md-4 middle">
                            <div class="item-box">
                                <b>Học viên sẽ tìm hiểu những ý tưởng cơ bản của quản lý dự án.
                                </b><br><br>
                                <b>Trong bài này chỉ dừng ở mức độ tìm hiểu tổng quan, những vấn đề chi tiết sẽ được trình bày
                                    trong các bài học tiếp theo.</b>
                            </div>
                            <!-- <div class="item-box">
                              <b>Xu hướng mới trong Digital Marketing: Nội dung do người dùng tạo ra</b>
                            </div> -->
                        </div>
                        <!-- middle -->
                        <div class="col-md-4 right">
                            <div class="item-box">

                                <ul>
                                    <li>Giới thiệu PMBOK</li>
                                    <li>Chứng chỉ PMP</li>
                                    <li>Dự án và chương trình (projects and programs)</li>
                                    <li>03 ràng buộc cơ bản của dự án</li>
                                    <li>Người quản lý dự án</li>
                                    <li>Ảnh hưởng của cấu trúc tổ chức</li>
                                    <li>Những người liên quan trong dự án</li>
                                    <li>Vòng đời dự án</li>
                                    <li>Quy trình quản lý dự án</li>
                                </ul>
                            </div>

                        </div>
                        <!-- right -->
                    </div>
                    <!-- /row tuan2 -->

                    <div class="row tuan tuan3">
                        <a class="xs-title">Tuần 3</a>
                        <div class="col-md-4 left">
                            <h4>QUẢN LÝ CÁC BÊN LIÊN QUAN</h4>
                            <div class="line"></div>
                            <p>
                                Trong buổi học này, học viên sẽ hiểu được:
                            </p>
                            <ul>
                                <li>Định nghĩa của cụm từ bên liên quan dự án (Project Stakeholder)</li>
                                <li>Xác định các bên liên quan là ai</li>
                                <li>Xác định những nhu cầu thông tin của họ</li>
                                <li>Xác định trách nhiệm cho việc quản lý các bên liên quan</li>
                                <li>Kiểm soát sự can dự của họ</li>
                            </ul>
                            <!-- <p>
                              <b>CASE STUDY</b>: Threadless
                            </p> -->
                            <a href="#form-dang-ky" class="btn-dang-ky-hoc">Đăng ký học</a>
                        </div>
                        <!-- left -->
                        <div class="col-md-4 middle">
                            <div class="item-box">
                                <b>Quản lý các bên liên quan</b><br><br>
                                <b>Quy trình khởi tạo quản lý các bên liên quan</b>
                                <ul>
                                    <li>Xác định các bên liên quan</li>
                                </ul>


                            </div>
                            <div class="item-box">
                                <b>Quy trình lên kế hoạch quản lý các bên liên quan</b>
                                <ul>
                                    <li>Bản kế hoạch quản lý các bên liên quan</li>
                                </ul>

                            </div>
                        </div>
                        <!-- middle -->
                        <div class="col-md-4 right">
                            <div class="item-box">
                                <b>Quy trình thực thi quản lý các bên liên quan</b>
                                <ul>
                                    <li>Quản lý sự can dự của các bên liên quan (Manage Stakeholder Engagement)</li>
                                </ul>
                            </div>
                            <div class="item-box">
                                <b>Quy trình theo dõi và kiểm soát các bên liên quan</b>
                                <ul>
                                    <li>Kiểm soát sự can dự của các bên liên quan (Control Stakeholder Engagement)</li>
                                </ul>
                            </div>

                        </div>
                        <!-- right -->
                    </div>
                    <!-- /row tuan3 -->

                    <div class="row tuan tuan4">
                        <a class="xs-title">Tuần 4</a>
                        <div class="col-md-4 left">
                            <h4>QUẢN LÝ PHẠM VI</h4>
                            <div class="line"></div>
                            <p>
                                Trong buổi học này, chúng ta sẽ xác định:

                            </p>
                            <ul>
                                <li>Mục đích của việc sử dụng bản tuyên bố dự án (Project Charter)
                                </li>
                                <li>Tóm tắt các yếu tố chính của một kế hoạch dự án
                                </li>
                                <li>Thảo luận phát biểu phạm vi (Scope Statement)
                                </li>
                                <li>Cấu trúc phân rã công việc (Work Breakdown Structure - WBS)</li>
                            </ul>
                            <!-- <p>
                              <b>CASE STUDY</b>: Radiohead
                            </p> -->
                            <a href="#form-dang-ky" class="btn-dang-ky-hoc">Đăng ký học</a>
                        </div>
                        <!-- left -->
                        <div class="col-md-4 middle">
                            <div class="item-box">
                                <b>Quản lý tích hợp dự án</b><br><br>

                            </div>
                            <div class="item-box">


                                <b>Nhóm các quy trình khởi tạo dự án</b>
                                <ul>
                                    <li>Phát triển bản tuyên bố dự án (Project Charter)</li>
                                    <li>Xác định các bên liên quan (Identify Stakeholder)</li>
                                </ul>
                                <br>
                            </div>
                            <div class="item-box">
                                <b>Nhóm các quy trình lên kế hoạch dự án</b>
                                <ul>
                                    <li>Thiết kế kế hoạch quản lý dự án</li>
                                </ul>
                            </div>
                        </div>
                        <!-- middle -->
                        <div class="col-md-4 right">
                            <div class="item-box">
                                <b>Quản lý phạm vi dự án</b><br><br>
                                <b>Nhóm các quy trình lên kế hoạch dự án (tiếp theo)</b>
                                <ul>
                                    <li>Lên kế hoạch quản lý phạm vi dự án</li>
                                    <li>Ghi nhận yêu cầu</li>
                                    <li>Xác định phạm vi</li>
                                    <li>Tạo cấu trúc phân rã công việc (WBS-Work Breakdown Structure)</li>
                                </ul>
                            </div>
                            <div class="item-box">
                                <b> Nhóm quy trình theo dõi và kiểm soát</b>
                                <ul>
                                    <li>Phê chuẩn phạm vi</li>
                                    <li>Kiểm soát phạm vi</li>
                                </ul>
                            </div>
                        </div>
                        <!-- /right -->
                    </div>
                    <!-- /row tuan4 -->
                    <div class="row tuan tuan5">
                        <a class="xs-title">Tuần 5</a>
                        <div class="col-md-4 left">
                            <h4>QUẢN LÝ NGUỒN NHÂN LỰC</h4>
                            <div class="line"></div>
                            <p>
                                - Hoạch định và quản lý nhân sự trong dự án. <br><br>
                                - Hoạch định vai trò, trách nhiệm, quyền hạn của mỗi nhân sự trong dự án và phương án điều hành, quản lý đội thực thi dự án.
                            </p>

          <!-- <p>
            <b>CASE STUDY</b>: Pepsi Refresh
          </p> -->
                            <a href="#form-dang-ky" class="btn-dang-ky-hoc">Đăng ký học</a>
                        </div>
                        <!-- left -->
                        <div class="col-md-4 middle">
                            <div class="item-box">
                                <b>Quản lý nguồn nhân lực</b><br><br>
                                <b>Các quy trình lên kế hoạch quản lý nguồn nhân lực</b><br><br>

                            </div>
                            <div class="item-box">
                                <b>Kế hoạch quản lý nguồn nhân lực</b><br><br>
                                <b>Nhóm các quy trình lên kế hoạch dự án</b><br><br>
                                <b>Thiết kế kế hoạch quản lý dự án</b><br><br>
                            </div>
                        </div>
                        <!-- middle -->
                        <div class="col-md-4 right">
                            <div class="item-box">

                                <b>Các quy trình thực thi quản lý nguồn nhân lực</b><br><br>
                            </div>
                            <div class="item-box">
                                <b>Tìm kiếm nhân lực dự án</b><br><br>
                                <b>Phát triển đội dự án</b><br><br>
                                <b>Quản lý đội dự án</b>
                            </div>

                        </div>
                        <!-- right -->
                    </div>
                    <!-- /row tuan5 -->
                    <div class="row tuan tuan6">
                        <a class="xs-title">Tuần 6</a>
                        <div class="text-content">
                            <p>
                                TỔNG KẾT
                            </p>
                            <p>
                                Tổng kết khóa học bằng cách thảo luận với các chuyên gia dự án,
                                thi cuối khóa và tham dự vào khảo sát khóa học.
                            </p>
                            <a href="#form-dang-ky" class="btn-dang-ky-hoc">Đăng ký học</a>
                        </div>

                    </div>
                    <!-- /row tuan6 -->

                </div>
                <!-- container -->
                <i class="fa fa-angle-down"></i>
            </div>
            <!-- /khoa hoc -->

            <div id="don-vi-cap-bang" class="section" >
                <div class="container">
                    <div class="col-md-5 left">
                        <img src="/mana/cs/khoa-hoc-hoach-dinh-du-an-v2/images/illinois.png" alt="Illinois" class="img-responsive"/>
                    </div>
                    <div class="col-md-7 right">
                        <h4>Chứng chỉ được cấp bởi</h4>
                        <h3>ĐẠI HỌC CALIFORNIA – IRVINE EXTENSION (ĐH UCI – HOA KỲ)
                        </h3>
                        <div class="line">

                        </div>
                        <ul>
                            <li><i class="fa fa-star"></i>
                                <p>
                                    UC Irvine Extension thuộc hệ thống đại học California tại thành phố Irvine, California.
                                </p></li>
                            <li><i class="fa fa-star"></i>
                                <p>
                                    Top 50 trường Đại học tốt nhất nước Mỹ.
                                </p></li>
                            <li><i class="fa fa-star"></i>
                                <p>
                                    Là một trong những trường ĐH hàng đầu về đào tạo và nghiên cứu trên thế giới.
                                </p></li>
                            <li><i class="fa fa-star"></i>
                                <p>
                                    Chứng chỉ được cấp bởi ĐH UIUC được công nhận trên toàn thế giới.
                                </p></li>
                        </ul>
                    </div>
                    <!-- right -->

                </div>
                <!-- container -->
                <i class="fa fa-angle-down"></i>
            </div>

            <div id="giang-vien" class="section">
                <div class="container">
                    <h3>Giảng viên</h3>
                    <div class="row sm-row">
                        <div class="col-md-6 left">
                            <img src="/mana/cs/khoa-hoc-hoach-dinh-du-an-v2/images/gv1.png" alt="" class="img-responsive sm-gv1"/>
                            <img src="/mana/cs/khoa-hoc-hoach-dinh-du-an-v2/images/bw1.png" alt="" class="img-responsive md-gv" data-target="gv1" data-img-bw="bw1"/>
                        </div>
                        <div class="col-md-6 sm-gv1">
                            <p>
                                Giảng viên đến từ Đại học UCI
                            </p>
                            <b>Margaret Meloni, MBA, PMP</b>
                            <ul>
                                <li><p>
                                        Giảng viên Quản lý dự án của UCI Irvine Extension và UCLA Extension là những Đại học hàng đầu thế giới.
                                    </p></li>
                                <li>
                                    <p>
                                        Chủ tịch tại Meloni Coaching Solutions chuyên về quản trị dự án.
                                    </p></li>
                                <li><p>
                                        Hơn 18 năm kinh nghiệm trong vai trò quản lý dự án tại nhiều công ty lớn như Fortune 500.
                                    </p></li>
                            </ul>
                        </div>
                        <div class="col-md-6 right">
                            <img src="/mana/cs/khoa-hoc-hoach-dinh-du-an-v2/images/gv2.png" alt="" class="img-responsive sm-gv2"/>
                            <img src="/mana/cs/khoa-hoc-hoach-dinh-du-an-v2/images/bw2.png" alt="" class="img-responsive md-gv" data-target="gv2" data-img-bw="bw2"/>
                        </div>
                        <div class="col-md-6 sm-gv2">
                            <p>
                                Giảng viên Việt Nam
                            </p>
                            <b>Thạc sĩ Lê Văn Tiến Sĩ, PMP</b>
                            <ul>
                                <li><p>
                                        Giám đốc sản xuất, giám đốc chất lượng, giám đốc đào tạo tại Success Software Service (SSS).
                                    </p></li>
                                <li><p>
                                        Chuyên gia đào tạo quản lý dự án và kỹ năng mềm tại TUV Rheinland Việt Nam.
                                    </p></li>
                                <li><p>
                                        Hơn 15 năm kinh nghiệm trong lĩnh vực quản lý.
                                    </p></li>
                                <li><p>
                                        Từng là Giám đốc Điều hành công ty Gia công phần mềm của Úc,
                                        Giám đốc khu vực Đông Dương và trưởng phòng đại diện của NIII Ấn Độ tại Việt Nam.
                                    </p></li>
                            </ul>
                        </div>

                        <div class="col-md-6 tt-gv gv1">
                            <p>
                                Giảng viên đến từ Đại học UCI
                            </p>
                            <b>Margaret Meloni, MBA, PMP</b>
                            <ul>
                                <li><p>
                                        Giảng viên Quản lý dự án của UCI Irvine Extension và UCLA Extension là những Đại học hàng đầu thế giới.
                                    </p></li>
                                <li>
                                    <p>
                                        Chủ tịch tại Meloni Coaching Solutions chuyên về quản trị dự án.
                                    </p></li>
                                <li><p>
                                        Hơn 18 năm kinh nghiệm trong vai trò quản lý dự án tại nhiều công ty lớn như Fortune 500.
                                    </p></li>
                            </ul>
                        </div>
                        <div class="col-md-6 tt-gv gv2">
                            <p>
                                Giảng viên Việt Nam
                            </p>
                            <b>Thạc sĩ Lê Văn Tiến Sĩ, PMP</b>
                            <ul>
                                <li><p>
                                        Giám đốc sản xuất, giám đốc chất lượng, giám đốc đào tạo tại Success Software Service (SSS).
                                    </p></li>
                                <li><p>
                                        Chuyên gia đào tạo quản lý dự án và kỹ năng mềm tại TUV Rheinland Việt Nam.
                                    </p></li>
                                <li><p>
                                        Hơn 15 năm kinh nghiệm trong lĩnh vực quản lý.
                                    </p></li>
                                <li><p>
                                        Từng là Giám đốc Điều hành công ty Gia công phần mềm của Úc,
                                        Giám đốc khu vực Đông Dương và trưởng phòng đại diện của NIII Ấn Độ tại Việt Nam.
                                    </p></li>
                            </ul>
                        </div>

                    </div>
                    <!-- row -->

                </div>
                <i class="fa fa-angle-down"></i>
            </div>

            <div id="cam-nhan" class="section">
                <div class="container">
                    <h3>Học viên nói gì về chương trình học</h3>
                    <div class="col-md-6 left">
                        <img src="/mana/cs/khoa-hoc-hoach-dinh-du-an-v2/images/quote.png" alt="quote" />
                        <p>
                            Đối với công ty Outsourcing, sự thành công của công ty được đo lường bằng mức độ hài lòng của khách hàng.
                            Để tạo được sự hài lòng cho khách hàng, tôi phải quan lý được việc giao phần mềm đúng tiêu chuẩn kỹ thuật,
                            kịp tiến độ thời gian trong mức kinh phí cho phép. Một dự án chỉ được xem là hiệu quả khi làm tốt những điều trên và
                            lúc đó công ty tôi mới được trả tiền và có thêm nhiều khách hàng. Điều đó đặt ra cho tôi một thử thách lớn về kỹ năng quản lý
                            và những thử thách đó được giải quyết bởi những kiến thức nền tảng hữu ích có trong khóa Project Management tại MANA
                        </p>
                        <div class="hoc-vien">
                            <img src="/mana/cs/khoa-hoc-hoach-dinh-du-an-v2/images/hv1.png" alt="" />
                            <h4>Chị Phan Thị Thanh Thủy </h4>
                            <p>
                                Công ty Outsourcing IT
                            </p>
                        </div>
                    </div>
                    <div class="col-md-6 right">
                        <img src="/mana/cs/khoa-hoc-hoach-dinh-du-an-v2/images/quote.png" alt="quote" />
                        <p>
                            Những dự án mà anh triển khai thường xuyên bị chậm tiến độ và gặp vấn đề về việc điều phối nhân lực.
                            Khóa Project Management đã giúp anh nhận ra những điểm sai lầm mà anh từng mắc phải. Khóa học giúp anh rắc lại
                            nền tảng kiến thức, giúp anh xác định rõ ràng hơn về việc xây dựng cấu trúc công việc, sử dụng nguồn nhân lực,
                            quản lý các rủi ro. Hiện anh đang học tiếp các khóa khác liên quan đến Project Management tại MANA.
                        </p>
                        <div class="hoc-vien">
                            <img src="/mana/cs/khoa-hoc-hoach-dinh-du-an-v2/images/hv2.png" alt="" />
                            <h4>Anh Công Vinh </h4>
                            <p>
                                Tập đoàn xây dựng Long Việt Hải
                            </p>
                        </div>
                    </div>
                    <div class="button-main">
                        <button class="btn btn-see-intro" data-toggle="modal" data-target="#video-intro" style="
                                width: 280px;
                                "><img src="/mana/cs/khoa-hoc-hoach-dinh-du-an-v2/images/intro.png" alt="">XEM INTRO</button>
                    </div>

                </div>
                <i class="fa fa-angle-down"></i>
            </div>
            <!-- cam-nhan -->

            <div id="gioi-thieu" class="section">
                <div class="container">
                    <div class="row text-intro">
                        <h3>"Xin chào, chúng tôi là MANA <br>
                            Viện đào tạo Quản trị Kinh doanh trực tuyến hàng đầu Việt Nam. <br>
                            <span>
                                Chúng tôi mang đến một hệ thống các chương trình đào tạo kỹ năng quản trị kinh doanh có tính ứng dụng cao."
                            </span>
                        </h3>
                        <p>
                            Tại MANA, các bạn sẽ được học các chương trình
                            <span> đào tạo song ngữ với đội ngũ cố vấn học tập</span> 24/7.
                            <br>Hình thức và phương pháp học khoa học mang lại cho bạn những trải nghiệm học tập mới mẻ và hữu ích:
                        </p>

                    </div>
                    <div class="row list-item">
                        <div class="col-md-2 col-md-offset-1">
                            <img src="/mana/cs/khoa-hoc-hoach-dinh-du-an-v2/images/video.png" alt="" />
                            <p><span>1. </span>
                                Học trực tuyến các bài giảng Video
                            </p>
                        </div>
                        <!-- item 1 -->
                        <div class="col-md-2">
                            <img src="/mana/cs/khoa-hoc-hoach-dinh-du-an-v2/images/thuchanh.png" alt="" />
                            <p><span>2. </span>
                                Thực hành hàng tuần
                            </p>
                        </div>
                        <!-- item 2 -->

                        <div class="col-md-2">
                            <img src="/mana/cs/khoa-hoc-hoach-dinh-du-an-v2/images/fb.png" alt="" />
                            <p><span>3. </span>
                                Thảo luận trên Facebook Group
                            </p>
                        </div>
                        <!-- item 3 -->

                        <div class="col-md-2">
                            <img src="/mana/cs/khoa-hoc-hoach-dinh-du-an-v2/images/kiemtra.png" alt="" />
                            <p><span>4. </span>
                                Kiểm tra tiểu luận cuối khóa
                            </p>
                        </div>
                        <!-- item 4 -->

                        <div class="col-md-2">
                            <img src="/mana/cs/khoa-hoc-hoach-dinh-du-an-v2/images/nophoso.png" alt="" />
                            <p><span>5. </span>
                                Nộp hồ sơ và nhận bằng Quốc tế
                            </p>
                        </div>
                        <!-- item 5 -->
                    </div>

                </div>
                <i class="fa fa-angle-down"></i>
            </div>
            <!-- /gioi-thieu -->

            <div id="hoc-online" class="section">
                <div class="container">
                    <div class="row text-header">
                        <h4>Học online cùng Mana online business school</h4>
                        <h3>Nhận chứng chỉ quốc tế</h3>
                    </div>
                    <!-- /text header -->
                    <div class="row item-list">
                        <div class="col-md-3 item">
                            <i class="fa fa-clock-o"></i>
                            <p>
                                Học Online mọi lúc mọi nơi
                            </p>
                        </div>
                        <!-- /item -->
                        <div class="col-md-3 item">
                            <i class="fa fa-globe"></i>
                            <p>
                                Chương trình học <b>song ngữ</b> Anh - Việt
                            </p>
                        </div>
                        <!-- /item -->
                        <div class="col-md-3 item">
                            <i class="fa fa-graduation-cap"></i>
                            <p>
                                Học cùng chuyên gia - thực nghiệm cùng chuyên gia
                            </p>
                        </div>
                        <!-- /item -->
                        <div class="col-md-3 item">
                            <i class="fa fa-certificate"></i>
                            <p>
                                Học tại Việt Nam nhận <b>chứng chỉ Quốc tế</b>
                            </p>
                        </div>
                        <!-- /item -->
                    </div>
                    <div class="row" id="form-dang-ky">
                        <h4>Đăng ký nhận tư vấn miễn phí</h4>
                        <form class="form-inline" id="form_advice" name="landing-page-id" action="/course/page/submit" method="post" accept-charset="utf-8">
                            <input type="hidden" id="advice_name" name="advice_name" value="Mana - Hoạch định dự án hiệu quả -v2" /> 
                            <input type="hidden" id="csrf" name="_csrf" />
                            <div class="form-group">
                                <input type="text" class="form-control" id="fullname" name="fullname" placeholder="Họ và tên" required>
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" id="phonenumber" name="phonenumber" placeholder="Số điện thoại" required>
                            </div>
                            <div class="form-group">
                                <input type="email" class="form-control" id="email" name="email" placeholder="Email" required>
                            </div>
                            <div class="clearfix">

                            </div>
                            <div class="form-action">
                                <button type="submit" id="dang_ky_form" class="btn-dang-ky btn_box_register">Đăng ký
                                    <span>
                                <div class="kyna-click-form"></div>
                                <div class="kyna-click-form-fill"></div>
                                <div class="kyna-click-form-img"></div>
                            </span>
                                </button>
                            </div>

                        </form>

                        <p>
                            Bộ phận CSKH của MANA sẽ sớm liên hệ và tư vấn miễn phí về khóa học cho bạn.
                        </p>
                    </div>
                </div>
            </div>
            <!-- /hoc-online -->

            <!-- <div id="hoi-dap" style="background: #f7f7f7;">
              <div class="container">
                  <div class="fb-comments fb_iframe_widget fb_iframe_widget_fluid" data-href="https://kyna.vn/nhom-khoa-hoc/digital-marketing-danh-cho-bat-dong-san" data-width="100%" data-numposts="10" data-colorscheme="light" fb-xfbml-state="rendered"><span style="height: 173px;"><iframe id="f2c0e6ad3b0b2dc" name="f2b21dbf4d78c" scrolling="no" title="Facebook Social Plugin" class="fb_ltr fb_iframe_widget_lift" src="https://www.facebook.com/plugins/comments.php?api_key=191634267692814&amp;channel_url=https%3A%2F%2Fstaticxx.facebook.com%2Fconnect%2Fxd_arbiter.php%3Fversion%3D42%23cb%3Df18e273cb35ff2c%26domain%3Dkyna.vn%26origin%3Dhttps%253A%252F%252Fkyna.vn%252Ff3e7e80d0e3a294%26relation%3Dparent.parent&amp;colorscheme=light&amp;href=https%3A%2F%2Fkyna.vn%2Fnhom-khoa-hoc%2Fdigital-marketing-danh-cho-bat-dong-san&amp;locale=vi_VN&amp;numposts=10&amp;sdk=joey&amp;skin=light&amp;version=v2.0&amp;width=100%25" style="border: none; overflow: hidden; height: 173px; width: 100%;"></iframe></span></div>
              </div>

            </div> -->
            <!-- hoi-dap -->
        </div>

        <footer id="footer">
            <div class="container footer">
                <div class="row">
                    <div class="col-sm-3">
                        <div class="footer-logo">
                            <ul>
                                <li>
                                    <a href="https://mana.edu.vn/">
                                        <img src="/mana/cs/khoa-hoc-digital-marketing-trong-ky-nguyen-so/imgs/mana.png" class="img-responsive"/>
                                    </a>
                                </li>
                                <li>
                                    <a href="https://kyna.vn">
                                        <img src="/mana/cs/khoa-hoc-digital-marketing-trong-ky-nguyen-so/imgs/18_KynaLogo.png" class="img-responsive"/>
                                    </a>
                                </li>
                            </ul>

                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="footer-text-center">
                            <h4>Công ty Cổ phần  Dream Việt Education</h4>
                            <p> <b class="company-address"> Trụ sở chính:</b> Lầu 6 Tòa nhà Thịnh Phát - 178/8 Đường D1, Phường 25, Quận Bình Thạnh, TP Hồ Chí Minh</p>
                            <p> <b class="company-address"> Văn phòng Hà Nội:</b> Tầng 6, Tòa Nhà Viện Công Nghệ - 25 Vũ Ngọc Phan, Phường Láng Hạ, Quận Đống Đa, TP Hà Nội </p>
                            <p style="padding-top: 20px">Giấy phép ĐKKD số 0313589030 do Sở Kế hoạch và Đầu tư TPHCM cấp</p>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="footer-hot-line">
                            <p><b class="company-address">Hotline:</b>  <?= Setting::HOTLINE ?></p>
                            <!-- <p><b class="company-address">Hotline:</b>  1900 6364 09</p>                     -->
                            <p><b class="company-address">Email: </b>hotro@kyna.vn</p>
                            <div class="fb-page" data-href="https://www.facebook.com/mana.edu.vn/" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/mana.edu.vn/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/mana.edu.vn/">MANA.edu.vn</a></blockquote></div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <div class="modal fade" id="modal">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="popup_body">
                            <div class="alert alert-success" role="alert">Cám ơn bạn đã để lại thông tin. Kyna.vn sẽ sớm liên hệ với bạn để tư vấn thêm về chương trình học!</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="video-intro">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="videoWrapper"></div>
                        <button class="btn btn-close" data-dismiss="modal" aria-label="Close">Đóng</button>
                    </div>
                </div>
            </div>
        </div>

        <script src="/mana/cs/khoa-hoc-hoach-dinh-du-an-v2/js/jquery-1.11.2.min.js"></script>
        <script src="/mana/cs/khoa-hoc-hoach-dinh-du-an-v2/js/bootstrap.min.js"></script>
        <script src="/mana/cs/khoa-hoc-hoach-dinh-du-an-v2/js/jquery.fullPage.min.js"></script>
        <script src="/mana/cs/khoa-hoc-hoach-dinh-du-an-v2/js/jquery.scrollme.min.js"></script>
        <script type="text/javascript" src="/mana/cs/khoa-hoc-hoach-dinh-du-an-v2/js/owl.carousel.min.js"></script>

        <!-- Nhan them vao -->
        <script>
            $(document).ready(function () {

                $(window).on("scroll", function (e) {
                    var nextScroll = $(window).scrollTop();
                    if ($(window).width() > 768) {
                        if (nextScroll > 100) {
                            $('#wrap-header').hide();
                        } else {
                            $('#wrap-header').css({height: "100px", backgroundColor: "transparent"});
                            $('#wrap-header .mana-logo img').css("margin-top", "25px");
                            $('#wrap-header .navbar-nav').css("margin-top", "30px");
                            $('#wrap-header').show();
                        }
                    }
                    currentScroll = nextScroll;
                });
                if ($(window).width() > 768) {
                    $('#fullpage').fullpage({
                        scrollBar: true,
                        // srollOverflow: true,
                        fixedElements: '#wrap-header',
                        // normalScrollElements: '#hoi-dap, #footer',
                        fitToSection: false,
                        controlArrows: true,
                    });
                }
                $(".list-tuan-hoc a").on('click', function (e) {
                    $this = $(this);
                    $this.parents('#khoa-hoc').find('.tuan:visible').css("display", "none");
                    $('.' + $this.attr('data-target')).css("display", "flex");
                    $this.parents('ul').find('.active').removeClass('active');
                    $this.addClass('active');
                });
                $('#giang-vien .md-gv').on('mouseenter', function (e) {
                    $this = $(this);
                    if ($('.tt-gv:visible').length == 0) {
                        $this.attr('src', '/mana/cs/khoa-hoc-hoach-dinh-du-an-v2/images/' + $this.attr('data-target') + '.png');
                        $('.' + $this.attr('data-target')).show();
                    }

                });
                $('#giang-vien .md-gv').on('mouseleave', function (e) {
                    $this = $(this);
                    if (!$(e.toElement).hasClass('tt-gv')) {
                        $this.attr('src', '/mana/cs/khoa-hoc-hoach-dinh-du-an-v2/images/' + $this.attr('data-img-bw') + '.png');
                        $('.' + $this.attr('data-target')).hide();
                    }
                });
                $('#giang-vien .sm-row').on('mouseleave', function (e) {
                    $('.left .md-gv').attr('src', '/mana/cs/khoa-hoc-hoach-dinh-du-an-v2/images/bw1.png');
                    $('.right .md-gv').attr('src', '/mana/cs/khoa-hoc-hoach-dinh-du-an-v2/images/bw2.png');
                    $('.tt-gv:visible').hide();
                });

                $('a[href*=#]:not([href=#])').click(function () {
                    if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {

                        var target = $(this.hash);
                        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                        if ($(window).width() > 768) {
                            if (target.length) {
                                $('html,body').animate({
                                    scrollTop: target.offset().top - 80
                                }, 1000);
                                return false;
                            }
                        } else {
                            if (target.length) {
                                $('html,body').animate({
                                    scrollTop: target.offset().top - 80
                                }, 1000);
                                return false;
                            }
                        }
                    }
                });



            });

        </script>

        <script>
            $(".btn-see-intro").click(function () {
                $("#video-intro .modal-body .videoWrapper").html('<iframe width="560" height="315" src="https://www.youtube.com/embed/LCHNlkFJGZw" frameborder="0" allowfullscreen></iframe>');
            });

            $("#video-intro .btn-close").click(function () {
                $("#video-intro .modal-body .videoWrapper").html('');
            });

            $('#video-intro').on('hidden.bs.modal', function () {
                $("#video-intro .modal-body .videoWrapper").html('');
            })
        </script>
        <!--End of Zopim Live Chat Script-->
       <!--Start of Zopim Live Chat Script-->
<script type="text/javascript">
    window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
        d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
    _.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
        $.src="//v2.zopim.com/?1bMWnWprkt76mXJXRb6xBuP2dAD7uxPq";z.t=+new Date;$.
            type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
</script>
<!--End of Zopim Live Chat Script-->
<div id="fb-root"></div>
<script type="text/javascript">
  (function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.0&appId=790788601060712";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));
</script>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-M92WKP"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
    <?php $this->endBody()?>
    </body>

</html>
