<?php
/**
 * Created by IntelliJ IDEA.
 * User: tn
 * Date: 11/16/2016
 * Time: 12:01 PM
 */
?>
<!-- Modal -->
<div id="thankyou_modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="my-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="md-body">
                <img src="<?= ($status)?'https://kyna.vn/img/landing-page/thankyou-icon.png':'https://kyna.vn/img/landing-page/thankyou-fail-icon.png' ?>" alt="thank you">
                <?php if ($status && !$isAdvice) : ?>
                    <h3>Đăng ký thành công!</h3>
                <?php endif; ?>
                <p class="message">
                    <?= $msg ?>
                </p>
                <p>
                    Hotline: <b>1900.6364.09 </b>- Email: <b>hotro@kyna.vn</b>
                </p>
            </div>
            <?php if (!empty($relatedCourses)) : ?>
                <div class="md-footer">
                    <h4>Có thể bạn quan tâm</h4>
                    <ul>
                        <?php $i = 1; ?>
                        <?php foreach ($relatedCourses as $key => $course) : ?>
                            <li>
                                <span><?= $i ?></span>
                                <a href="<?= $key ?>" target="_blank"><?= $course ?></a>
                            </li>
                            <?php $i ++; ?>
                        <?php endforeach; ?>
                    </ul>
                </div>
            <?php endif; ?>
        </div>
    </div>
    <style>
        #thankyou_modal .modal-content{
            padding: 15px 15px 50px;
        }
        #thankyou_modal img{
            display: block;
            width: 150px;
            margin: 30px auto;
        }
        #thankyou_modal h3{
            text-transform: uppercase;
            color: #50ad4e;
            font-size: 24px;
            font-weight: bold;
            text-align: center;
            margin: 40px auto 20px;
        }
        #thankyou_modal p{
            font-size: 14px !important;
            color: #000;
            text-align: center;
            margin: 10px auto;
        }
        #thankyou_modal .md-footer{
            padding-left: 0;
            padding-right: 0;
            margin-left: 0;
            margin-right: 0;
        }
        #thankyou_modal .md-footer h4{
            text-transform: uppercase;
            color: #000;
            font-weight: bold;
            text-align: center;
            margin: 50px auto 30px;
        }
        #thankyou_modal .md-footer ul{
            text-align: center;
            padding-left: 0;
        }
        #thankyou_modal .md-footer li{
            text-align: left;
            display: -webkit-inline-flex;
            display: -moz-inline-flex;
            display: -ms-inline-flex;
            display: inline-flex;
            align-items: center;
            width: 30%;
        }
        #thankyou_modal .md-footer span{
            display: inline-block;
            background: #50ad4e;
            width: 30px;
            height: 30px;
            padding: 5px 10px;
            font-weight: bold;
            color: #fff;
            border-radius: 50%;
            margin-right: 3px;
        }
        #thankyou_modal .md-footer a{
            color: #333;
            font-size: 14px;
            /*border-bottom: 1px dashed black;*/
            text-decoration: underline !important;
        }
    </style>
</div>