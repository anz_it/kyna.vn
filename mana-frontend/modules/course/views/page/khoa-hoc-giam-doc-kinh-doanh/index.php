<?php

use mana\models\Setting;
$dir_name = dirname(dirname(dirname(__FILE__)));
?>
<!DOCTYPE html>
<html lang="vi">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />

    <title>Đào tạo Giám Đốc Kinh Doanh chuẩn Quốc Tế - Mana.edu.vn</title>

    <meta name="robots" content="index,follow">
    <meta name="title" content="Đào tạo Giám Đốc Kinh Doanh chuẩn Quốc Tế - Mana.edu.vn">
    <meta name="description" content="Đồng hành cùng chuyên gia trở thành Giám Đốc Kinh Doanh chuyên nghiệp theo giáo trình quốc tế ✔️ Học bổng lên tới 3.220.000đ ✔️ ĐỘC QUYỀN tại MANA ✔️ Học ngay!">
    <meta property="fb:app_id" content="790788601060712">
    <meta property="og:type" content="website" />
    <meta property="og:title" content="Đào tạo Giám Đốc Kinh Doanh chuẩn Quốc Tế - Mana.edu.vn">
    <meta property="og:description" content="Đồng hành cùng chuyên gia trở thành Giám Đốc Kinh Doanh chuyên nghiệp theo giáo trình quốc tế ✔️ Học bổng lên tới 3.220.000đ ✔️ ĐỘC QUYỀN tại MANA ✔️ Học ngay!">
    <meta property='og:url' content='https://mana.edu.vn/cbp/khoa-hoc-giam-doc-kinh-doanh' />
    <meta property='og:image:url' content='https://mana.edu.vn/mana/cbp/khoa-hoc-giam-doc-kinh-doanh/img/thumb.jpg' />

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

    <link rel="stylesheet" href="/mana/cbp/khoa-hoc-giam-doc-kinh-doanh/css/style.css">
    <link rel="stylesheet" href="/mana/cbp/khoa-hoc-giam-doc-kinh-doanh/css/slick.css">
    <link rel="stylesheet" href="/mana/cbp/khoa-hoc-giam-doc-kinh-doanh/css/slick-theme.css">


    <?php echo \common\helpers\Html::csrfMetaTags() ?>

</head>
<body>
  <?php $this->beginBody()?>

  <!-- Google Tag Manager -->
  <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
  new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
  j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
  'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
  })(window,document,'script','dataLayer','GTM-M92WKP');</script>
  <!-- End Google Tag Manager -->
	<?php

  	$getParams = $_GET;
  	$utm_source = '';
  	$utm_medium = '';
  	$utm_campaign = '';

  	if (isset($getParams['utm_source']) && trim($getParams['utm_source']) != '') {
  		$utm_source = trim($getParams['utm_source']);
  	}
  	if (isset($getParams['utm_medium']) && trim($getParams['utm_medium']) != '') {
  		$utm_medium = trim($getParams['utm_medium']);
  	}
  	if (isset($getParams['utm_campaign']) && trim($getParams['utm_campaign']) != '') {
  		$utm_campaign = trim($getParams['utm_campaign']);
  	}
  ?>

  <div id="topbar" class="container-fluid">
    <a href="#form-wrapper" class="button-scroll">
      Học bổng trị giá <b>3.220.000<sup>đ</sup></b> và nhiều phần quà hấp dẫn khi đăng ký hôm nay!
      <!-- <span>Đăng ký ngay!</span> -->
    </a>
  </div>

  <nav class="navbar navbar-default navbar-fixed-top" id="navbar-wrap" data-spy="scroll" data-target="#scrollspy-course" data-offset-top="100">
    <div class="container">
      <div class="navbar-header">
        <a type="button" class="navbar-toggle button-scroll mb-btn-register" href="#form-wrapper">ĐĂNG KÝ</a>

        <a href="/"  class="logo">
          <img src="/mana/cbp/khoa-hoc-giam-doc-kinh-doanh/img/logo.png" alt="mana.edu.vn" class="img-responsive">
        </a>
      </div>

      <div class="collapse navbar-collapse" id="scrollspy-course">
        <ul class="nav navbar-nav">
          <li><a href="#program">Nội dung</a></li>
          <li><a href="#lecture">Giảng viên</a></li>
          <li><a href="#learn-online">Lợi ích</a></li>
          <li><a href="#review">Ý kiến</a></li>
          <li>
            <a href="#form-wrapper" class="button-scroll">
              ĐĂNG KÝ NGAY
            </a>
          </li>
        </ul>
      </div>
      <!-- /.navbar-collapse -->
    </div>
    <!-- /.container -->
  </nav>

  <main>

    <section id="banner">
      <div class="container">
        <div class="row">
          <div class="col-xs-12 col-sm-8 text-intro">
            <p>Đào tạo</p>
            <h1>Giám đốc kinh doanh</h1>
            <h2>Chuyên nghiệp chuẩn quốc tế</h2>
            <h3>Trọn bộ 5 khóa học được công nhận trên toàn cầu (*)</h3>
            <div class="line"></div>
            <div class="explaination">
              <i>(*) Hoàn thành khóa học, bạn sẽ được cấp chứng chỉ CBP
              (Certified Business Professional), xét duyệt bởi Hiệp hội Đào tạo Kinh doanh
              Quốc tế IBTA. Chứng chỉ được công nhận trên toàn cầu.</i>
            </div>
            <div class="button-wrap">
              <a href="#intro" class="find-out">Tìm hiểu thêm</a>
            </div>
          </div>
          <div class="col-sm-4">
            <img src="/mana/cbp/khoa-hoc-giam-doc-kinh-doanh/img/bannerMan.jpg" alt="" class="right-img">
          </div>
        </div>

      </div>
    </section>
    <!-- end banner -->

    <section id="intro">
      <div class="container">
        <img src="/mana/cbp/khoa-hoc-giam-doc-kinh-doanh/img/introMan.png" alt="" class="img-left">
        <div class="col-xs-12 col-sm-8 col-sm-offset-4 right">
          <b>Giám đốc kinh doanh</b> - một vị trí có thể thay đổi cả vận mệnh
          của doanh nghiệp, được coi là nhân vật có quyền lực xếp thứ 2 chỉ sau CEO.
          Vì thế, đảm nhận vai trò này không phải là một chuyện đơn giản. Bên cạnh
          việc lăn lộn, trải nghiệm trên thương trường, Giám Đốc Kinh Doanh rất cần có
          những kiến thức được hệ thống hóa bài bản cũng như những chia sẻ từ các "tiền bối"
          và sự kết nối với cộng đồng các doanh nhân.
          <div class="line"></div>
          <h3>Để trở thành một Giám Đốc Kinh Doanh thành công, bạn cần: </h3>
          <ul>
            <li>
              <img src="/mana/cbp/khoa-hoc-giam-doc-kinh-doanh/img/iconShield.png" />
              <p>Mở rộng được mạng lưới khách hàng tiềm năng cho doanh nghiệp</p>
            </li>
            <li>
              <img src="/mana/cbp/khoa-hoc-giam-doc-kinh-doanh/img/iconShield.png" />
              <p>Ứng xử thông minh, giao tiếp chuyên nghiệp với cả nhân viên và khách hàng.</p>
            </li>
            <li>
              <img src="/mana/cbp/khoa-hoc-giam-doc-kinh-doanh/img/iconShield.png" />
              <p>Gia tăng giá trị thương hiệu, chữ "Tín" trong mắt khách hàng.</p>
            </li>
            <li>
              <img src="/mana/cbp/khoa-hoc-giam-doc-kinh-doanh/img/iconShield.png" />
              <p>Thấu hiểu được khó khăn của nhân viên. Biết xây dựng văn hóa đồng đội và tạo động lực cho nhân viên.</p>
            </li>
            <li>
              <img src="/mana/cbp/khoa-hoc-giam-doc-kinh-doanh/img/iconShield.png" />
              <p>Có tầm nhìn xa, quyết đoán, phong thái lãnh đạo chuyên nghiệp.</p>
            </li>
          </ul>
        </div>

      </div>
    </section>
    <!-- end #intro -->

    <section id="advantage">
      <div class="container-fluid">
        <h2>Chương trình Đào tạo <b>Giám đốc kinh doanh chuyên nghiệp</b> <br>chuẩn quốc tế
        được ra đời nhằm hỗ trợ đắc lực cho bạn trên con đường thành công</h2>
        <ul>
          <li>
            <img src="/mana/cbp/khoa-hoc-giam-doc-kinh-doanh/img/board.png" alt="">
            <p class="bold">5 khóa học</p>
            <p>chuẩn quốc tế</p>
          </li>
          <li>
            <img src="/mana/cbp/khoa-hoc-giam-doc-kinh-doanh/img/trophy.png" alt="">
            <p class="bold">3 chuyên gia</p>
            <p>hàng đầu</p>
          </li>
          <li>
            <img src="/mana/cbp/khoa-hoc-giam-doc-kinh-doanh/img/clock.png" alt="">
            <p class="bold">70h+</p>
            <p>nội dung</p>
          </li>
          <li>
            <img src="/mana/cbp/khoa-hoc-giam-doc-kinh-doanh/img/pencil.png" alt="">
            <p>Học trực tuyến</p>
            <p class="bold">Mọi lúc mọi nơi</p>
          </li>
          <li>
            <img src="/mana/cbp/khoa-hoc-giam-doc-kinh-doanh/img/book.png" alt="">
            <p>Trợ giảng</p>
            <p class="bold">1 kèm 1</p>
          </li>
        </ul>
        <img src="/mana/cbp/khoa-hoc-giam-doc-kinh-doanh/img/programImg.png" alt="" class="lg-img">
      </div>
    </section>
    <!-- end #advantage  -->

    <section id="program">
      <div class="container">
        <h2>Nội dung chương trình</h2>
        <div class="col-xs-12 col-sm-8">
          <div id="control-buttons"></div>
          <div id="course-slide">
            <div class="item" data-slide-number="1">
              <div class="header">
                <b>Khóa học 1: </b> kỹ năng bán hàng chuyên nghiệp <br>
                (CBP <sup>TM</sup> Selling Skills)
                <div class="line"></div>
              </div>
              <div class="list">
                <ul>
                  <li><b>1.</b> Quy trình bán hàng và các giai đoạn bán hàng</li>
                  <li><b>2.</b> Phương pháp tạo sự hào hứng tự nhiên trong quá trình bán hàng</li>
                  <li><b>3.</b> Phương pháp tìm kiếm khách hàng tiềm năng thành công</li>
                  <li><b>4.</b> Chiến lược tiếp cận khách hàng theo từng giai đoạn</li>
                  <li><b>5.</b> Phương pháp giải quyết tình huống khi khách hàng từ chối</li>
                  <li><b>6.</b> Chiến lược chốt sales thành công</li>
                  <li><b>7.</b> Chiến lược kết thúc quá trình bán hàng và xây dựng hệ thống khách hàng sau khi bán</li>
                </ul>
              </div>
            </div>
            <!-- item1 -->
            <div class="item" data-slide-number="2">
              <div class="header">
                <b>Khóa học 2: </b> kỹ năng chăm sóc và thấu hiểu khách hàng <br>
                (CBP <sup>TM</sup> Customer Service)
                <div class="line"></div>
              </div>
              <div class="list">
                <ul>
                  <li><b>1.</b> Kỹ năng giao tiếp trong dịch vụ chăm sóc khách hàng</li>
                  <li><b>2.</b> Phân tích tâm lý và thấu hiểu tâm lý khách hàng</li>
                  <li><b>3.</b> Cách ứng xử tốt nhất khi gặp khách hàng khó tính</li>
                  <li><b>4.</b> Chăm sóc khách hàng qua điện thoại</li>
                  <li><b>5.</b> Chăm sóc khách hàng qua Internet</li>
                  <li><b>6.</b> Chiến lược quản lý thời gian</li>
                  <li><b>7.</b> Chiến lược kết thúc quá trình bán hàng và xây dựng hệ thống khách hàng sau khi bán</li>
                </ul>
              </div>
            </div>
            <!-- item2 -->
            <div class="item" data-slide-number="3">
              <div class="header">
                <b>Khóa học 3: </b> kỹ năng giao tiếp trong kinh doanh <br>
                (CBP <sup>TM</sup> Business Communication)
                <div class="line"></div>
              </div>
              <div class="list">
                <ul>
                  <li><b>1.</b> Kỹ năng giao tiếp có vai trò như thế nào trong kinh doanh</li>
                  <li><b>2.</b> Cấu trúc giao tiếp trong kinh doanh và các yếu tố ảnh hưởng đến quá trình giao tiếp thường ngày</li>
                  <li><b>3.</b> Phát triển phong cách viết trong kinh doanh chuyên nghiệp</li>
                  <li><b>4.</b> Kỹ năng viết các văn bản thường gặp trong kinh doanh bao gồm bản ghi nhớ, báo cáo, email v.v</li>
                  <li><b>5.</b> Phát triển kỹ năng giao tiếp ngôn từ lôi cuốn, tự tin</li>
                  <li><b>6.</b> Giao tiếp và trao đổi bằng điện thoại trong kinh doanh</li>
                  <li><b>7.</b> Ngôn ngữ cơ thể - công cụ giao tiếp quyền lực</li>
                  <li><b>8.</b> Phát triển kỹ năng thuyết trình hiệu quả và tự tin</li>
                  <li><b>9.</b> Giải quyết mâu thuẫn xung đột và những bất đồng trong giao tiếp kinh doanh</li>
                </ul>
              </div>
            </div>
            <!-- item3 -->

            <div class="item" data-slide-number="4">
              <div class="header">
                <b>Khóa học 4: </b> nghi thức giao tiếp trong kinh doanh <br>
                (CBP <sup>TM</sup> Business Etiquette)
                <div class="line"></div>
              </div>
              <div class="list">
                <ul>
                  <li><b>1.</b> Tầm quan trọng của nghi thức giao tiếp trong kinh doanh</li>
                  <li><b>2.</b> Chào hỏi và giới thiệu bản thân</li>
                  <li><b>3.</b> Cách tạo ấn tượng đầu tiên tốt đẹp và cách xây dựng đội ngũ Tiếp tân chuyên nghiệp</li>
                  <li><b>4.</b> Tổ chức và tiến hành một buổi họp</li>
                  <li><b>5.</b> Đạo đức kinh doanh</li>
                  <li><b>6.</b> Nghi thức giao tiếp khi chiêu đãi đối tác</li>
                  <li><b>7.</b> Quy tắc ăn uống tại các quốc gia</li>
                  <li><b>8.</b> Nghi thức giao tiếp qua điện thoại, Internet và thư điện tử</li>
                  <li><b>9.</b> Cách ăn mặc và tác phong kinh doanh chuyên nghiệp</li>
                  <li><b>10.</b> Quy tắc ứng xử với người khuyết tật</li>
                  <li><b>11.</b> Môi trường kinh doanh đa văn hóa</li>
                </ul>
              </div>
            </div>
            <!-- item4 -->
            <div class="item" data-slide-number="5">
              <div class="header">
                <b>Khóa học 5: </b>Nghệ thuật lãnh đạo hiệu quả <br>
                (CBP <sup>TM</sup> Leadership)
                <div class="line"></div>
              </div>
              <div class="list">
                <ul>
                  <li><b>1.</b> Vai trò và trách nhiệm của nhà lãnh đạo</li>
                  <li><b>2.</b> Xây dựng phong cách lãnh đạo</li>
                  <li><b>3.</b> Kiến tạo tầm nhìn và sứ mệnh của nhà lãnh đạo</li>
                  <li><b>4.</b> Kỹ năng ra quyết định một cách hiệu quả</li>
                  <li><b>5.</b> Xây dựng đội nhóm – team building</li>
                  <li><b>6.</b> Nghệ thuật động viên và khích lệ nhân viên</li>
                </ul>
              </div>
            </div>
          </div>
          <div id="arrow-control">
            <span></span>
          </div>
        </div>
      </div>
    </section>
    <!-- end #program -->

    <section id="sologan">
      <div class="container">
        <h2>
          Đăng ký tư vấn và nhận học bổng <br>
          <img src="/mana/cbp/khoa-hoc-giam-doc-kinh-doanh/img/textInset.png" alt="Đăng ký tư vấn và nhận học bổng">
        </h2>
        <a href="#form-wrapper" class="button-scroll">Đăng ký ngay</a>
      </div>
    </section>

    <section id="lecture">
      <div class="container">

        <div class="lecture-box">
          <img src="/mana/cbp/khoa-hoc-giam-doc-kinh-doanh/img/httv.jpg" alt="Thạc sỹ Hồ Thị Thanh Vân">
          <div class="info">
            <div class="lecture-name">
              Thạc sỹ Hồ Thị Thanh Vân (Vân Hồ) <br>
              CBP Certified Instructor
            </div>
            <div class="line"></div>
            <ul>
              <li>
                <i class="fa fa-circle"></i>
                <p>
                  Thạc sỹ chuyên ngành Marketing Bán hàng và Dịch vụ (MMSS) tại trường Đại học Paris 1 Sorbonne (IAE) phối hợp với trường Quản trị Châu  u (ESCP - EAP European School Management).
                </p>
              </li>
              <li>
                <i class="fa fa-circle"></i>
                <p>20 năm kinh nghiệm, giữ vị trí lãnh đạo chủ chốt trong lĩnh vực Sales và Marketing tại các công ty hàng đầu thế giới.</p>
              </li>
              <li>
                <i class="fa fa-circle"></i>
                <p>
                  Dành nhiều giải thưởng danh giá trong lĩnh vực Sales &amp; Marketing.
                </p>
              </li>
            </ul>
          </div>
        </div>

        <div class="lecture-box">
          <img src="/mana/cbp/khoa-hoc-giam-doc-kinh-doanh/img/nkt.jpg" alt="Thạc sỹ Nguyễn Kiên Trì">
          <div class="info">
            <div class="lecture-name">
              Thạc sỹ Nguyễn Kiên Trì <br>
              CBP Certified Instructor
            </div>
            <div class="line"></div>
            <ul>
              <li>
                <i class="fa fa-circle"></i>
                <p>MBA Tư vấn Quản lý Quốc tế - Thuỵ Sỹ</p>
              </li>
              <li>
                <i class="fa fa-circle"></i>
                <p>Expert Economic Certified Đại học California</p>
              </li>
              <li>
                <i class="fa fa-circle"></i>
                <p>Nhiều năm kinh nghiệm trong việc quản trị và giảng dạy, hiện đang nắm giữ các vị trí cấp cao tại các tập đoàn lớn.</p>
              </li>
            </ul>
          </div>
        </div>

        <div class="lecture-box">
          <img src="/mana/cbp/khoa-hoc-giam-doc-kinh-doanh/img/nvn.jpg" alt="Thạc sỹ Nguyên Văn Ngoan">
          <div class="info">
            <div class="lecture-name">
              Thạc sỹ Nguyễn Văn Ngoan <br>
              CBP Certified Instructor
            </div>
            <div class="line"></div>
            <ul>
              <li>
                <i class="fa fa-circle"></i>
                <p>Phó tổng Giám đốc - Star Travel International</p>
              </li>
              <li>
                <i class="fa fa-circle"></i>
                <p>Chủ tịch MANDA MIND Corporation</p>
              </li>
              <li>
                <i class="fa fa-circle"></i>
                <p>Nhà sáng lập chuỗi cửa hàng "Đặc sản 3 miền"</p>
              </li>
              <li>
                <i class="fa fa-circle"></i>
                <p>Diễn giả Doanh nhân &amp; Chuyên gia tư vấn chiến lược, thương hiệu.</p>
              </li>
            </ul>
          </div>
        </div>

      </div>
    </section>
    <!-- end lecture -->

    <section id="learn-online">
      <div class="container">
        <h2>Học online "1 kèm 1" với cố vấn học tập</h2>
        <div class="col-xs-12 col-md-10 col-md-offset-1">
          <div class="box">
            <img src="/mana/cbp/khoa-hoc-giam-doc-kinh-doanh/img/icon1.png" alt="">
            <p><b>Học mọi lúc mọi nơi:</b> Học qua video bài giảng được biên tập chuyên nghiệp.</p>
          </div>
          <div class="box">
            <img src="/mana/cbp/khoa-hoc-giam-doc-kinh-doanh/img/icon2.png" alt="">
            <p><b>Học 1 kèm 1: </b>Cố vấn 1 kèm 1, trong suốt quá trình học và luyện
            thi. Hỏi đáp cùng chuyên gia, doanh nhân giàu kinh nghiệm.</p>
          </div>
          <div class="box">
            <img src="/mana/cbp/khoa-hoc-giam-doc-kinh-doanh/img/icon3.png" alt="">
            <p><b>Học kết hợp thực hành và luyện thi: </b>
            Chương trình học được đan xen giữa bài học lý thuyết và thực hành, cùng hơn 100 bài thi thử. </p>
          </div>
          <div class="box">
            <img src="/mana/cbp/khoa-hoc-giam-doc-kinh-doanh/img/icon4.png" alt="">
            <p><b>Thi trực truyến tại nhà và nhận chứng chỉ quốc tế: </b>
            Thi trực tuyến trên hệ thống PROMETRIC với tối đa 12 tháng ôn luyện trước khi thi.</p>
          </div>
        </div>
      </div>
    </section>
    <!-- end learn-online -->

    <section id="cert">
      <div class="container">
        <div class="row">
          <div class="col-xs-12 col-sm-3 col-sm-offset-1">
            <img src="/mana/cbp/khoa-hoc-giam-doc-kinh-doanh/img/certImg.jpg" alt="Chứng chỉ CBP">
          </div>
          <div class="col-xs-12 col-sm-7 right">
            <h3>SỞ HỮU CHỨNG CHỈ CBP</h3>
            <h3>Chứng chỉ Kinh Doanh Được Toàn Thế Giới Công Nhận</h3>
            <div class="line"></div>
            <ul>
              <li>
                <img src="/mana/cbp/khoa-hoc-giam-doc-kinh-doanh/img/checkIcon.png" alt="">
                <p>
                  Chứng chỉ CBP của IBTA là một trong những chứng chỉ uy tín nhất về kỹ năng kinh doanh chuyên nghiệp và kỹ năng quản lý.
                </p>
              </li>
              <li>
                <img src="/mana/cbp/khoa-hoc-giam-doc-kinh-doanh/img/checkIcon.png" alt="">
                <p>CBP được công nhận trên toàn cầu từ Mỹ, Canada, Caribe, Châu Phi, Trung Đông, Trung Quốc, Ấn Độ, đến
                Singapore và vùng Viễn Đông.</p>
              </li>
              <li>
                <img src="/mana/cbp/khoa-hoc-giam-doc-kinh-doanh/img/checkIcon.png" alt="">
                <p>Trên thế giới, các tập đoàn lớn như HONDA, Nokia, Mitsubishi, Intel,
                Ricoh, adidas,... đều công nhận tiêu chuẩn đánh giá về kỹ năng kinh doanh
              của IBTA được mô tả qua chứng chỉ CBP.</p>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </section>
    <!-- end #cert -->

    <section id="review">
      <div class="container">
        <h2>Học viên của Mana Online Business School </h2>
        <div class="col-md-10 col-md-offset-1 wrapper">
          <div id="review-slide" class="row">
            <div class="review-item">
              <p>
                "Anh cảm thấy công việc của mình nhẹ hẳn, đỡ stress và ít vất vả hơn lúc trước
                do mình học thêm được nhiều kiến thức mới hỗ trợ khá tốt trong công việc, biết hệ thống
                lại các đầu việc, quản lý thời gian một cách khoa học. Hơn nữa, còn mở rộng được mạng lưới
                khách hàng của công ty. Mọi việc trở nên dễ dàng hơn. Ảnh cảm thấy vui vì đạt được
                nhiều thành quả tốt hơn, không bỏ phí công sức và thời gian để đầu tư vào khóa học."
              </p>
              <img src="/mana/cbp/khoa-hoc-giam-doc-kinh-doanh/img/hv1.png" alt="">
              <div class="user">
                <b>Anh Việt Dũng - 35 tuổi</b>
                Giám đốc kinh doanh công ty công nghệ phần mềm
              </div>
              <span>1/3</span>
            </div>
            <div class="review-item">
              <p>
                "Học xong mới biết mình bị hỗng kiến thức nhiều quá, ai dè công việc nó cứ dậm chân tại chỗ, không tiến triển được. Nhân viên thì chán nản, nhiều người xin nghỉ việc. Sau khi học xong thì chị đã rút ra được khá nhiều bài học và kinh nghiệm từ các giảng viên. Xây dựng lại được văn hóa, tinh thần của các anh chị em trong phòng ban, nhờ vậy mà mọi người phấn chấn hơn. Kết quả kinh doanh cũng cải thiện hơn lúc trước nhiều. Gỡ được khá nhiều nút thắc trong lòng!"
              </p>
              <img src="/mana/cbp/khoa-hoc-giam-doc-kinh-doanh/img/hv2.jpg" alt="">
              <div class="user">
                <b>Chị Thanh Hương - 32 tuổi</b>
                Quản lý bán hàng chuỗi thương hiệu thời trang
              </div>
              <span>2/3</span>
            </div>
            <div class="review-item">
              <p>
                "Tôi là một người rất bận rộn, không có thời gian mà đi học các lớp đào tạo kỹ năng đâu, mặc dù rất muốn. Nhưng may, đây là thời đại Internet, tôi biết đến các lớp học online. Vì học online chủ động được thời gian nên tôi cũng học thử xem sao. Không ngờ kết quả hơn cả mong đợi, doanh thu team của tôi tăng đáng kể, có nhiều thời gian cho gia đình hơn, stress ít hẳn đi và đặc biệt là khả năng ứng xử với khách hàng và các bạn trong team tốt hơn hẳn."
              </p>
              <img src="/mana/cbp/khoa-hoc-giam-doc-kinh-doanh/img/hv3.jpg" alt="">
              <div class="user">
                <b>Anh Tùng Linh – 29 tuổi</b>
                Sale Team Leader công ty bất động sản
              </div>
              <span>3/3</span>
            </div>
          </div>

          <div id="review-arrow-control"></div>
        </div>
        <img src="/mana/cbp/khoa-hoc-giam-doc-kinh-doanh/img/reviewItemBg.png" alt="" class="abs-img">
      </div>
    </section>
    <!-- end #review -->

    <section id="register">
      <div class="container">
        <div class="col-xs-12 col-sm-7 left">
          <div class="row">
            <h2>Tham gia chương trình học <br> theo tiêu chuẩn quốc tế</h2>
            <div class="line"></div>
            <ul>
              <li>
                <img src="/mana/cbp/khoa-hoc-giam-doc-kinh-doanh/img/checkIcon.png" alt="">
                <p>Khóa học phát triển dựa trên kiến thức, kinh nghiệm của hàng trăm <b>chuyên gia đào tạo kinh doanh hàng đầu</b> từ nhiều nơi trên thế giới.</p>
              </li>
              <li>
                <img src="/mana/cbp/khoa-hoc-giam-doc-kinh-doanh/img/checkIcon.png" alt="">
                <p>
                  Giáo trình <b>DUY NHẤT và ĐỘC QUYỀN</b> từ tổ chức học thuật uy tín - Hiệp hội Đào tạo kinh doanh Quốc tế IBTA.
                </p>
              </li>
              <li>
                <img src="/mana/cbp/khoa-hoc-giam-doc-kinh-doanh/img/checkIcon.png" alt="">
                <p>
                  Được nhận <b>chứng chỉ Kinh doanh Chuyên nghiệp</b> (CBP Executive) cấp bởi Hiệp hội IBTA, được công nhận trên toàn cầu.
                </p>
              </li>
              <li>
                <img src="/mana/cbp/khoa-hoc-giam-doc-kinh-doanh/img/checkIcon.png" alt="">
                <p>
                  Học online <b>mọi lúc mọi nơi, </b>kết nối cùng Cộng đồng CBP Professionals.
                </p>
              </li>
              <li>
                <img src="/mana/cbp/khoa-hoc-giam-doc-kinh-doanh/img/checkIcon.png" alt="">
                <p>
                  Sở hữu video <b>trọn đời</b> và toàn bộ các cập nhật về kiến thức, nghề nghiệp  từ Mana Online Business School <b>hoàn toàn miễn phí.</b>
                </p>
              </li>
            </ul>
          </div>
        </div>
        <div class="col-xs-12 col-sm-5 form-wrapper" id="form-wrapper">
          <p class="top">Đăng ký chương trình</p>
          <h3>Đào tạo giám đốc kinh doanh <br>chuyên nghiệp chuẩn quốc tế</h3>
          <div class="line"></div>
          <div class="two-line">
            <div class="white-line"></div>
            <span>Nhận ngay học bổng</span>
            <div class="white-line"></div>
          </div>
          <div class="border-box">
            <b>3,220,000 đồng từ mana obs</b>
          </div>
          <h3 style="margin-top: 25px;margin-bottom: 0px; text-transform: none;">Học online cùng Mana Online Business School</h3>
          <!-- end .border-box -->
          <form id="landing-page-id" class="row" action="/course/page/submit">
             <input type="hidden" id="csrf" name="_csrf" />
             <!-- <input type="hidden" id="combo_id" name="combo_id" value="" /> -->
             <input type="hidden" id="advice_name" name="advice_name" value="Mana - Giám đốc kinh doanh">
             <input type="hidden" id="page_slug" name="page_slug" value="cbp/khoa-hoc-giam-doc-kinh-doanh">

            <div class="col-xs-12">
              <div class="form-group">
                <p>Họ và tên</p>
                <input type="text" name="fullname" id="fullname" class="form-control" required>
              </div>
            </div>
            <!-- name -->

            <div class="col-xs-12">
              <div class="form-group">
                <p>Email</p>
                <input type="email" name="email" id="email" class="form-control" required>
              </div>
            </div>
            <!-- email -->

            <div class="col-xs-12">
              <div class="form-group">
                <p>Số điện thoại</p>
                <input type="text" name="phonenumber" id="phonenumber" class="form-control" required>
              </div>
            </div>
            <!-- phone -->

            <div class="button-wrap">
              <button type="submit" class="button-regis">ĐĂNG KÝ NGAY</button>
            </div>
          </form>
          <p class="bottom">Bộ phận Tư vấn Tuyển sinh sẽ liên hệ với bạn để tư vấn về chương trình. Bạn vui lòng giữ máy</p>
        </div>
        <!-- end .form-wrapper -->
      </div>
    </section>
    <!-- end register -->

    <section id="about">
      <div class="container-fluid top">
        <img src="/mana/cbp/khoa-hoc-giam-doc-kinh-doanh/img/aboutLeft.jpg" alt="" class="left-img">
        <img src="/mana/cbp/khoa-hoc-giam-doc-kinh-doanh/img/aboutRight.png" alt="" class="right-img">
        <div class="container">
          <div class="col-md-10 col-md-offset-1">
            <h2>Xin chào, chúng tôi là mana, viện đào tạo quản trị kinh doanh trực tuyến hàng đầu Việt Nam</h2>
            <p>MANA mang đến một hệ thống các chương trình đào tạo kỹ năng quản trị kinh doanh có tính ứng dụng cao.</p>
            <p>Tại MANA, học viên sẽ được học trên các chương trình đào tạo song ngữ cùng đội ngũ cố vấn học tập 24/7. Hình thức và phương pháp học khoa học mang lại cho học viên những trải nghiệm học tập mới mẻ và thú vị.</p>
            <p>Vào 16.6.2016, Dream Viet Education Corporation (công ty chủ quản của MANA OBS) đã trở thành đối tác chính thức và độc quyền của Hiệp hội IBTA tại Việt Nam. Điều này xác định, MANA là đại diện hợp pháp trong việc phân phối các chương trình Đào tạo kỹ năng Kinh doanh chuẩn quốc tế trên toàn Việt Nam trong 5 năm 2016 - 2021.</p>
          </div>
        </div>
      </div>
      <div class="container bottom">
        <h3>Báo chí nói về chúng tôi</h3>
        <div class="col-xs-12 col-sm-6 col-md-3">
          <a href="http://dantri.com.vn/khuyen-hoc/dang-sau-viec-ky-ket-cua-kynavn-va-international-business-training-association-ibta-20160627165026711.htm">
            <img src="/mana/cbp/khoa-hoc-giam-doc-kinh-doanh/img/dantri.jpg" alt="">
          </a>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-3">
          <a href="https://www.youtube.com/watch?v=hlYgT9QVOMQ">
            <img src="/mana/cbp/khoa-hoc-giam-doc-kinh-doanh/img/fbnc.jpg" alt="">
          </a>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-3">
          <a href="//cafebiz.vn/bi-mat-tang-truong-than-toc-cua-kyna-20160810133337045.chn">
            <img src="/mana/cbp/khoa-hoc-giam-doc-kinh-doanh/img/cafebiz.jpg" alt="">
          </a>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-3">
          <a href="//www.doanhnhansaigon.vn/san-pham-moi/nang-cao-nang-luc-canh-tranh-qua-dao-tao-ky-nang-truc-tuyen/1095837/">
            <img src="/mana/cbp/khoa-hoc-giam-doc-kinh-doanh/img/doanhnhan.jpg" alt="">
          </a>
        </div>
      </div>
    </section>


    <!-- <section id="comments-facebook">

    </section> -->

  </main>

  <section id="comment-plugin">
    <div class="container">
      <div class="fb-comments fb_iframe_widget fb_iframe_widget_fluid"
        data-href=""
        data-width="100%" data-numposts="10"
        data-colorscheme="light" fb-xfbml-state="rendered">
       </div>
    </div>
    <!--end .container-->
  </section>

  <footer>
    <div class="container">
      <div class="col-md-4 col-sm-4 col-xs-12 left">
        <a href="https://mana.edu.vn" >
          <img src="/mana/cbp/khoa-hoc-giam-doc-kinh-doanh/img/mana.png" alt="Mana" class="img-responsive">
        </a>
        <a href="https://kyna.vn" >
          <img src="/mana/cbp/khoa-hoc-giam-doc-kinh-doanh/img/kyna.png" alt="kyna" class="img-responsive">
        </a>
      </div>
      <div class="col-md-5 col-sm-5 col-xs-12 mid">
        <p>Công ty Cổ phần Dream Việt Education</p>
        <p>
          <b><u>Trụ sở chính: </u></b>Lầu 6 Tòa nhà Thịnh Phát - 178/8 Đường D1,
          Phường 25, Quận Bình Thạnh, TP Hồ Chí Minh
        </p>
        <p>
          <b><u>Văn phòng Hà Nội: </u></b>Tầng 6 Tòa nhà Viện Công Nghệ - 25 Vũ Ngọc Phan,
          Phường Láng Hạ, Quận Đống Đa, TP Hà Nội
        </p>
        <p>
          Giấy phép ĐKKD số 0313589030 do Sở Kế hoạch và Đầu tư TPHCM cấp
        </p>
      </div>
      <div class="col-md-3 col-sm-3 col-xs-12 right">
        <p>
          <b>Hotline:</b>
          <a href="tel:1900 6364 09">1900 6364 09</a>
        </p>
        <p>
          <b>Email: </b>
          <a href="mailto:hotro@kyna.vn">hotro@kyna.vn</a>
        </p>
        <div class="fb-page" data-href="https://www.facebook.com/mana.edu.vn/" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/mana.edu.vn/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/mana.edu.vn/">MANA.edu.vn</a></blockquote></div>
      </div>
    </div>
	</footer>
  <!-- end footer -->

  <!-- jQuery first, then Bootstrap JS. -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <!-- <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-smoove/0.2.9/jquery.smoove.min.js"></script> -->
  <script src="/mana/cbp/khoa-hoc-giam-doc-kinh-doanh/js/slick.js"></script>
  <script src="/mana/cbp/khoa-hoc-giam-doc-kinh-doanh/js/main.js"></script>

  <div id="fb-root"></div>
	<script type="text/javascript">
		(function(d, s, id) {
			var js, fjs = d.getElementsByTagName(s)[0];
			if (d.getElementById(id)) return;
			js = d.createElement(s); js.id = id;
			js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.8&appId=790788601060712";
			fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));
	</script>

  <!--Start of Zopim Live Chat Script-->
  <script type="text/javascript">
      window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
          d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
      _.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
          $.src="//v2.zopim.com/?1bMWnWprkt76mXJXRb6xBuP2dAD7uxPq";z.t=+new Date;$.
              type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
  </script>
  <!--End of Zopim Live Chat Script-->

  <!-- Google Tag Manager (noscript) -->
  <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-M92WKP"
  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
  <!-- End Google Tag Manager (noscript) -->

<?php $this->endBody()?>
</body>
</html>
