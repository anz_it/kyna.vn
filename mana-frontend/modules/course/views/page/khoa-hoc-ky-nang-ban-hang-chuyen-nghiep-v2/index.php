<?php
use mana\models\Setting;
$dir_name = dirname(dirname(dirname(__FILE__)));
?>
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta property="fb:app_id" content="790788601060712">
        <meta name="robots" content="index,follow">
        <meta property="og:type" content="website" />
		<meta property='og:url' content='https://mana.edu.vn/cbp/khoa-hoc-ky-nang-ban-hang-chuyen-nghiep-v2' />
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta property='og:title' content='Kỹ năng bán hàng theo tiêu chuẩn quốc tế - Mana.edu.vn' />
        <meta property='og:description' content='Kỹ năng bán hàng chuyên nghiệp theo tiêu chuẩn quốc tế. 7 nội dung học và những kinh nghiệm bán hàng trên toàn cầu được tổng hợp trong khóa học này.' />
        <meta property='og:image:url' content='/mana/cbp/khoa-hoc-ky-nang-ban-hang-chuyen-nghiep-v2/img/intro.jpg' />


        <title>Kỹ năng bán hàng theo tiêu chuẩn quốc tế - Mana.edu.vn</title>
		<meta name="description" content="Kỹ năng bán hàng chuyên nghiệp theo tiêu chuẩn quốc tế. 7 nội dung học và những kinh nghiệm bán hàng trên toàn cầu được tổng hợp trong khóa học này.">


        <link href="/mana/cbp/khoa-hoc-ky-nang-ban-hang-chuyen-nghiep-v2/css/bootstrap.min.css" rel="stylesheet"/>
        <link href="/mana/cbp/khoa-hoc-ky-nang-ban-hang-chuyen-nghiep-v2/css/style.css" rel="stylesheet"/>
        <link href="/mana/cbp/khoa-hoc-ky-nang-ban-hang-chuyen-nghiep-v2/css/animate.css" rel="stylesheet"/>
        <link href="/mana/cbp/khoa-hoc-ky-nang-ban-hang-chuyen-nghiep-v2/css/font-awesome.min.css" rel="stylesheet"/>

        <script type="text/javascript" src="/mana/cbp/khoa-hoc-ky-nang-ban-hang-chuyen-nghiep-v2/js/jquery-1.11.2.min.js"></script>

         <link rel="icon" href="/mana/images/manaFavicon.png">
          <?php echo \common\helpers\Html::csrfMetaTags() ?>

    </head>
    <body data-sticky_parent>
    <?php $this->beginBody()?>

    <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-M92WKP');</script>

    <!-- End Google Tag Manager -->
        <?php
        // get các tham số cần thiết */
        $getParams = $_GET;
        $utm_source = '';
        $utm_medium = '';
        $utm_campaign = '';

        if (isset($getParams['utm_source']) && trim($getParams['utm_source']) != '') {
            $utm_source = trim($getParams['utm_source']);
        }
        if (isset($getParams['utm_medium']) && trim($getParams['utm_medium']) != '') {
            $utm_medium = trim($getParams['utm_medium']);
        }
        if (isset($getParams['utm_campaign']) && trim($getParams['utm_campaign']) != '') {
            $utm_campaign = trim($getParams['utm_campaign']);
        }
        ?>
        <nav class="navbar navbar-fixed-top" id="wrap-header">

            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#menu-collapse" aria-expanded="false">
                      <span class="icon-bar"></span>
                      <span class="icon-bar"></span>
                  </button>
                    <a href="/" target="_blank" class="navbar-brand kyna-logo">
                        <img src="/mana/cbp/khoa-hoc-ky-nang-ban-hang-chuyen-nghiep-v2/img/mana.png" alt="Kyna.vn" class="img-responsive img-logo">
                    </a>
                </div>

                <div class="collapse navbar-collapse" id="menu-collapse">
                    <ul class="nav navbar-nav">
                        <li><a href="#intro">GIỚI THIỆU</a></li>
                        <li><a href="#course">NỘI DUNG</a></li>
                        <li><a href="#organization">ĐƠN VỊ CẤP BẰNG</a></li>
                        <li><a href="#lecture">GIẢNG VIÊN</a></li>
                        <li><a href="#advantage">LỢI ÍCH</a></li>
                        <li><a href="#register">ĐĂNG KÝ</a></li>
                    </ul>
                </div>
                <!-- /.navbar-collapse -->
            </div>
            <!-- /.container -->
        </nav>
    <article>
        <section id="header">
            <div class="container">
                <h1>
                    Đào tạo kỹ năng <br>
                    <span>Bán hàng chuyên nghiệp</span>
                </h1>
                <p>
                    <b>
                    Theo tiêu chuẩn CBP (*)
                   </b>
                </p>
                <a href="#intro" class="">Tìm hiểu ngay</a>
                <p class="col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1 col-xs-12  explanation">
                    (*): CBP - Certified Business Professsional, là chứng chỉ Kinh doanh chuyên nghiệp. Đây là một chứng chỉ uy tín về kỹ năng kinh doanh chuyên nghiệp và kỹ năng quản lý. Chứng chỉ CBP được công nhận trên toàn cầu.
                </p>
            </div>
        </section>
        <!-- end header -->
        <section id="intro">
            <div class="container">
                <div class="col-md-6 col-sm-6 left wow bounceInLeft">
                    <h2>
                      Không chỉ salesman <br>
                      Mới cần rèn luyện kỹ năng
                      Bán hàng chuyên nghiệp
                    </h2>
                    <img src="/mana/cbp/khoa-hoc-ky-nang-ban-hang-chuyen-nghiep-v2/img/intro.jpg" alt="" class="img-responsive">
                    <p>Dù chúng ta đang tồn tại với nhiều chức danh khác nhau, nhưng bất cứ ai trong chúng ta - những con người đang làm việc hăng say, đều là một <b>Salesman.</b></p>
                    <p>Bởi vì: </p>
                    <ul>
                        <li>
                            <p>Mọi doanh nghiệp đều tồn tại với <b>mục tiêu doanh thu.</b></p>
                        </li>
                        <li>
                            <p>
                                Dù bạn đảm nhiệm vai trò gì trong doanh nghiệp, bạn đều phải đóng góp vào quá trình vận hành và phát triển của công ty.
                            </p>
                        </li>
                        <li>
                            <p>
                                <b>Kỹ năng giao tiếp, đàm phán, nắm bắt vấn đề và giải quyết sự từ chối </b>là kỹ năng không phải chỉ dành riêng cho các salesman.
                            </p>
                        </li>
                        <li>
                            <p>
                                Kỹ năng bán hàng chuyên nghiệp chính là <b>năng lực cạnh tranh nổi bật,</b> nó thể hiện rất rõ ngay trong lần phỏng vấn đầu tiên.
                            </p>
                        </li>
                    </ul>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12 right wow bounceInRight">
                    <img src="/mana/cbp/khoa-hoc-ky-nang-ban-hang-chuyen-nghiep-v2/img/intro.jpg" alt="" class="img-responsive">
                </div>
            </div>
        </section>
        <!-- /intro -->
        <section id="intro-more">
            <img src="/mana/cbp/khoa-hoc-ky-nang-ban-hang-chuyen-nghiep-v2/img/intro2.jpg" alt="" class="img-fixed">
            <div class="container wow zoomIn">
                <div class="col-md-7 col-sm-6 col-xs-12 left"><img src="/mana/cbp/khoa-hoc-ky-nang-ban-hang-chuyen-nghiep-v2/img/intro2.jpg" alt="" class="img-responsive"></div>
                <div class="col-md-5 col-sm-6 col-xs-12 right">
                    <h2>
                       Kỹ năng bán hàng <br>
                       chuyên nghiệp<br>
                       <span>Theo tiêu chuẩn Quốc tế là</span>
                   </h2>

                    <ul>
                        <li>
                            <p>Hoàn thành điểm cuối của <b>hành trình bán hàng </b>một cách hoàn hảo</p>
                        </li>
                        <li>
                            <p>Tạo được hình ảnh thân thiện cho doanh nghiệp</p>
                        </li>
                        <li>
                            <p>
                                <b>Xử lý sự từ chối của khách hàng</b> một cách khôn ngoan và lo-gic
                            </p>
                        </li>
                        <li>
                            <p>
                                Có <b>chiến lược bán hàng </b>theo từng giai đoạn
                            </p>
                        </li>
                        <li>
                            <p>
                                Chăm sóc và <b>xây dựng được nhóm khách hàng thân thiết</b>
                            </p>
                        </li>
                    </ul>
                    <img src="/mana/cbp/khoa-hoc-ky-nang-ban-hang-chuyen-nghiep-v2/img/intro2.jpg" alt="" class="img-responsive">
                </div>

            </div>
        </section>
        <!-- /intro-more-->
        <section id="course">
            <div class="container wow bounceInUp">
                <h2>
                    Nội dung khóa học selling skills
                </h2>

                <div class="item-container">
                    <div class="item">
                        <p class="number">1</p>
                        <p>
                            Quy trình bán hàng và các giai đoạn bán hàng
                        </p>
                    </div>
                    <div class="item">
                        <p class="number">2</p>
                        <p>
                            Phương pháp <b>tạo sự hào hứng tự nhiên </b>trong quá trình bán hàng
                        </p>
                    </div>
                    <div class="item">
                        <p class="number">3</p>
                        <p>
                            Phương pháp <b>tìm kiếm khách hàng tiềm năng thành công</b>
                        </p>
                    </div>
                    <div class="item">
                        <p class="number">4</p>
                        <p>
                            Chiến lược <b>tiếp cận khách hàng hiệu quả </b>theo từng giai đoạn
                        </p>
                    </div>
                    <div class="item">
                        <p class="number">5</p>
                        <p>
                            Phương pháp <b>giải quyết tình huống khi khách hàng từ chối</b>
                        </p>
                    </div>
                    <div class="item">
                        <p class="number">6</p>
                        <p>
                            <b>Chiến lược chốt sales thành công</b>
                        </p>
                    </div>
                    <div class="item">
                        <p class="number">7</p>
                        <p>
                            Chiến lược kết thúc quá trình bán hàng và <b>xây dựng hệ thống khách hàng </b>sau khi bán
                        </p>
                    </div>
                </div>

                <div class="regist-now" data-wow-delay="2s">
                    <!--<a href="#register" data-xmas="false">Đăng ký nhận học bổng 1.000.000Đ</a>-->
                    <a href="#register" data-xmas='true'>Học bổng 1.500.000Đ</a>
                </div>
            </div>
        </section>

        <section id="organization">
            <div class="container wow slideInDown">
                <div class="col-md-5 col-sm-5 left">
                    <img src="/mana/cbp/khoa-hoc-ky-nang-ban-hang-chuyen-nghiep-v2/img/ibta.png" alt="" class="img-responsive">
                </div>
                <div class="col-md-7 col-sm-7 right">
                  <h2>Giáo trình gốc theo bản quyền <br>của tổ chức IBTA</h2>
                  <img src="/mana/cbp/khoa-hoc-ky-nang-ban-hang-chuyen-nghiep-v2/img/ibta.png" alt="" class="img-responsive">
                    <p>
                        <b>
                        IBTA (International Business Training Association) là Hiệp hội Đào tạo Kinh doanh Quốc tế, có trụ sở tại Hoa Kỳ.
                        </b>
                    </p>
                    <p>
                        Trên thế giới, các tập đoàn lớn như HONDA, Nokia, Mitsubishi, Intel, Ricoh, adidas,... đều công nhận tiêu chuẩn đánh giá về kỹ năng kinh doanh của IBTA được mô tả qua chứng chỉ CBP.
                    </p>
                </div>
            </div>
        </section>

        <section id="lecture">
            <div class="container wow slideInUp">
                <div class="col-md-5 col-sm-5 col-xs-12 left">
                    <img src="/mana/cbp/khoa-hoc-ky-nang-ban-hang-chuyen-nghiep-v2/img/chuyengia.jpg" alt="Thac sy Nguyen Ngoan">
                </div>
                <div class="col-md-7 col-sm-7 col-xs-12 right">
                    <h2>
                    Học trực tuyến cùng chuyên gia <br>
                    <span>Thạc sỹ Nguyễn Ngoan</span>
                    </h2>
                    <img src="/mana/cbp/khoa-hoc-ky-nang-ban-hang-chuyen-nghiep-v2/img/chuyengia.jpg" alt="Thac sy Nguyen Ngoan">
                    <ul>
                        <li>
                            <p>
                                <b>Phó tổng Giám đốc - Star Travel International</b>
                            </p>
                        </li>
                        <li>
                            <p>
                                Chủ tịch MANDA MIND Corporation
                            </p>
                        </li>
                        <li>
                            <p>
                                Nhà sáng lập ww.dacsan3mien.com
                            </p>
                        </li>
                        <li>
                            <p>
                                Diễn giả Doanh nhân &amp; Chuyên gia tư vấn chiến lược, thương hiệu
                            </p>
                        </li>
                    </ul>
                </div>
            </div>
        </section>

        <section id="advantage-top">
            <div class="container ">
                <div class="item wow bounceInLeft">
                    <i class="fa fa-globe"></i>
                    <p class="top">Học mọi lúc mọi nơi</p>
                    <p class="bottom">Học qua video bài giảng được biên tập chuyên nghiệp. Tài liệu bao gồm sách và bài thuyết trình, tư liệu tham khảo.</p>
                </div>
                <div class="item wow bounceInLeft">
                    <i class="fa fa-comments"></i>
                    <p class="top">Học kèm 1 - 1</p>
                    <p class="bottom">Cố vấn 1 kèm 1, trong suốt quá trình học và luyện thi. Hỏi đáp cùng chuyên gia, doanh nhân giàu kinh nghiệm.</p>
                </div>
                <div class="item wow bounceInRight">
                    <i class="fa fa-question-circle"></i>
                    <p class="top">Hỏi đáp và luyện thi</p>
                    <p class="bottom">Chương trình học được đan xen giữa bài học lý thuyết và thực hành, cùng hơn 100 bài thi thử.</p>
                </div>
                <div class="item wow bounceInRight">
                    <i class="fa fa-certificate"></i>
                    <p class="top">Thi trực tuyến và nhận chứng chỉ quốc tế</p>
                    <p class="bottom">Thi trực tuyến trên hệ thống PROMETRIC với tối đa 12 tháng ôn luyện trước khi thi.</p>
                </div>
            </div>
        </section>

        <section id="advantage">
            <div class="container">
                <div class="col-md-6 col-sm-6 col-xs-12 child-container wow slideInRight">
                    <h2>Lợi thế khi sở hữu chứng <br>chỉ CBP Selling skills</h2>
                    <ul>
                        <li>
                            <i class="fa fa-check-circle"></i>
                            <p>Đạt điều kiện cần để thi <b>lấy chứng chỉ CBP Executive</b> - chứng chỉ CBP Lãnh đạo cấp trung</p>
                        </li>
                        <li>
                            <i class="fa fa-check-circle"></i>
                            <p>Là chứng nhận cho <b>kỹ năng bán hàng chuyên nghiệp, </b>có giá trị trên toàn cầu</p>
                        </li>
                        <li>
                            <i class="fa fa-check-circle"></i>
                            <p>Có tư duy và kỹ năng bán hàng hiệu quả</p>
                        </li>
                        <li>
                            <i class="fa fa-check-circle"></i>
                            <p><b>Là lợi thế khi ứng tuyển vào các tập đoàn đa quốc gia, tập đoàn nước ngoài</b></p>
                        </li>
                        <li>
                            <i class="fa fa-check-circle"></i>
                            <p><b>Tăng cơ hội thăng tiến trong công việc</b></p>
                        </li>
                    </ul>
                </div>
            </div>
        </section>

        <section id="review">
            <div class="container">
                <h2>Học viên của Mana online business school</h2>
                <div class="col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1 col-xs-12">
                    <div id="review-carousel" class="carousel slide" data-ride="carousel" data-interval="8000">
                        <!-- Indicators -->
                        <ol class="carousel-indicators">
                            <li data-target="#review-carousel" data-slide-to="0" class="active"></li>
                            <li data-target="#review-carousel" data-slide-to="1"></li>
                        </ol>

                        <!-- Wrapper for slides -->
                        <div class="carousel-inner" role="listbox">

                            <div class="item active">
                                <img src="/mana/cbp/khoa-hoc-ky-nang-ban-hang-chuyen-nghiep-v2/img/hv1.jpg" alt="Thanh Son" class="img-responsive">
                                <div class="text">
                                    <p class="review-text">
                                        Bán hàng là một công việc vô cùng thú vị khi mà bạn có thể được tiếp xúc với nhiều kiểu người, nhiều loại giao tiếp và trải nghiệm rất nhiều cung bậc cảm xúc. Điều mà tôi mong muốn truyền đạt cho các em mới chập chững vào nghề sales luôn là đức tính kiên nhẫn. Khóa học này với sự dẫn dắt của anh Nguyễn Ngoan với rất nhiều kiến thức và kinh nghiệm được chia sẻ khiến tôi càng mở rộng vốn nghề được nhiều.
                                    </p>
                                    <p class="person-info">
                                        <b>Anh Nguyễn Văn Hà - Giám sát bán hàng</b>
                                    </p>
                                </div>
                            </div>
                            <div class="item">
                                <img src="/mana/cbp/khoa-hoc-ky-nang-ban-hang-chuyen-nghiep-v2/img/hv2.jpg" alt="" class="img-responsive">
                                <div class="text">
                                    <p class="review-text">
                                        Công ty tôi đòi hỏi tôi phải có chứng chỉ Selling Skills để được thăng chức và tăng lương. Tôi học chỉ để đạt đúng điều kiện đó, nhưng khóa học này khiến tôi thấy mình mở rộng được nhiều điều và đã hiểu vì sao công ty bắt buộc tôi hoàn thành khóa học nếu muốn thăng chức. Những kiến thức của khóa học không hề xáo rỗng mà rất thực tế và dễ áp dụng. Kinh nghiệm làm sale của tôi chỉ đủ để giúp tôi an toàn trong nghề. Còn kiến thức trong khóa học giúp tôi hình thành được tư duy bán hàng và bán hàng có chiến lược dài lâu.
                                    </p>
                                    <p class="person-info">
                                        <b>Anh Trần Quang Hiềng - Salesman 5 năm kinh nghiệm</b>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section id="register">
            <div class="container">
                <div class="form-container">
                   <div data-xmas="true">
                     <h2 style="margin-bottom: 30px">ĐĂNG KÝ KHÓA HỌC<br>NHẬN NGAY HỌC BỔNG TRỊ GIÁ 1.500.000Đ</h2>
                     <h2 style="margin-bottom: 30px;text-transform:none">Học online cùng Mana Online Business School</h2>
                   </div>
                    <form action="/course/page/submit" id="landing-page-id" name="landing-page-id" class="form-inline">
                        <input type="hidden" id="advice_name" name="advice_name" value="Mana - Khóa học kỹ năng bán hàng chuyên nghiệp V2" /> 
                        <input type="hidden" id="csrf" name="_csrf" />
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <input type="text" id="fullname" name="fullname" class="form-control" placeholder="Nhập họ và tên" required>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <input type="text" id="phonenumber" name="phonenumber" class="form-control" placeholder="Số điện thoại" required>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <input type="email" id="email" name="email" class="form-control" placeholder="Email" required>
                        </div>
                        <button type="submit" class="button-regis wow swing btn_box_register">
                        Đăng ký
                      </button>
                      <!--<div data-xmas="false">
                        <p>
                          Mọi thông tin của bạn đều được bảo mật hoàn toàn.
                        </p>
                        <p>
                          Bộ phận Tư vấn sẽ liên hệ sớm với bạn để tư vấn về khóa học và học bổng trị giá 1.000.000đ.
                        </p>
                      </div>-->
                        <div data-xmas="true">
                          <p>ĐĂNG KÝ NHẬN HỌC BỔNG</p>
                          <p>Bộ phận CSKH sẽ liên hệ với bạn sớm nhất có thể. Mọi thắc mắc về khóa học và học bổng
                          sẽ được giải đáp thông qua cuộc gọi này.</p>
                        </div>
                    </form>
                </div>
            </div>
        </section>

        <section id="about-mana">
            <div class="container">
                <div class="row top">
                    <h3>Xin chào, chúng tôi là MANA, <br> Viện đào tạo Quản trị Kinh doanh trực tuyến hàng đầu Việt Nam.</h3>
                    <div class="col-md-6 col-sm-6 col-xs-12 left">
                        <p>MANA mang đến một hệ thống các chương trình đào tạo kỹ năng quản trị kinh doanh có tính ứng dụng cao.</p>
                        <p>Tại MANA, học viên sẽ được học trên các chương trình đào tạo song ngữ cùng đội ngũ cố vấn học tập 24/7.</p>
                        <p>Hình thức và phương pháp học khoa học mang lại cho học viên những trải nghiệm học tập mới mẻ và thú vị.</p>
                        <p>Vào 16.6.2016, Dream Viet Education Corporation (công ty chủ quản của MANA OBS) đã trở thành đối tác chính thức và độc quyền của Hiệp hội IBTA tại Việt Nam. Điều này xác định, MANA là đại diện hợp pháp trong việc phân phối các chương trình Đào tạo kỹ năng Kinh doanh chuẩn quốc tế trên toàn Việt Nam trong 5 năm 2016 - 2021.</p>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12 right">
                        <img src="/mana/cbp/khoa-hoc-ky-nang-ban-hang-chuyen-nghiep-v2/img/about-bg.jpg" alt="" class="img-responsive">
                    </div>
                </div>
                <div class="row bottom">
                    <h3>Báo chí nói về chúng tôi</h3>
                    <div class="item-container">
                        <div class="item">
                            <a href="http://www.doanhnhansaigon.vn/san-pham-moi/nang-cao-nang-luc-canh-tranh-qua-dao-tao-ky-nang-truc-tuyen/1095837/" target="_blank">
                                <img src="/mana/cbp/khoa-hoc-ky-nang-ban-hang-chuyen-nghiep-v2/img/doanhnhan.png" alt="" class="im-responsive">
                            </a>
                        </div>
                        <div class="item">
                            <a href="http://dantri.com.vn/khuyen-hoc/dang-sau-viec-ky-ket-cua-kynavn-va-international-business-training-association-ibta-20160627165026711.htm" target="_blank">
                                <img src="/mana/cbp/khoa-hoc-ky-nang-ban-hang-chuyen-nghiep-v2/img/dantri.png" alt="" class="im-responsive">
                            </a>
                        </div>
                        <div class="item">
                            <a href="http://cafebiz.vn/bi-mat-tang-truong-than-toc-cua-kyna-20160810133337045.chn" target="_blank">
                                <img src="/mana/cbp/khoa-hoc-ky-nang-ban-hang-chuyen-nghiep-v2/img/cafebiz.png" alt="" class="im-responsive">
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </section>

    </article>
		<section id="hoi-dap">
			<div class="container">
				<div class="fb-comments fb_iframe_widget fb_iframe_widget_fluid"
					data-href="https://mana.edu.vn/cbp/khoa-hoc-ky-nang-ban-hang-chuyen-nghiep-v2"
					data-width="100%" data-numposts="10"
					data-colorscheme="light" fb-xfbml-state="rendered">
				 </div>
			</div><!--end .container-->
		</section>
    <!-- footer -->
    <footer>
        <div class="container">
            <div class="col-md-4 col-sm-4 col-xs-12 left">
                <a href="https://mana.edu.vn" target="_blank">
                    <img src="/mana/cbp/khoa-hoc-ky-nang-ban-hang-chuyen-nghiep-v2/img/mana.png" alt="Mana" class="img-responsive">
                </a>
                <a href="https://kyna.vn" target="_blank">
                    <img src="/mana/cbp/khoa-hoc-ky-nang-ban-hang-chuyen-nghiep-v2/img/kyna.png" alt="kyna" class="img-responsive">
                </a>
            </div>
            <div class="col-md-5 col-sm-5 col-xs-12 mid">
                <p class="comp-name">Công ty Cổ phần Dream Việt Education</p>
                <p><b><u>Trụ sở chính: </u></b>Lầu 6 Tòa nhà Thịnh Phát - 178/8 Đường D1, Phường 25, Quận Bình Thạnh, TP Hồ Chí Minh</p>
                <p><b><u>Văn phòng Hà Nội: </u></b>Tầng 6 Tòa nhà Viện Công Nghệ - 25 Vũ Ngọc Phan, Phường Láng Hạ, Quận Đống Đa, TP Hà Nội</p>
                <p>
                    Giấy phép ĐKKD số 0313589030 do Sở Kế hoạch và Đầu tư TPHCM cấp
                </p>
            </div>
            <div class="col-md-3 col-sm-3 col-xs-12 right">
                <p><b class="hotline">Hotline:</b> 1900 6364 09</p>
                <p><b class="email">Email: </b>hotro@kyna.vn</p>
                <div class="fb-page" data-href="https://www.facebook.com/mana.edu.vn/" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/mana.edu.vn/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/mana.edu.vn/">MANA.edu.vn</a></blockquote></div>
            </div>
        </div>
    </footer>

    <script type="text/javascript" src="/mana/cbp/khoa-hoc-ky-nang-ban-hang-chuyen-nghiep-v2/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="/mana/cbp/khoa-hoc-ky-nang-ban-hang-chuyen-nghiep-v2/js/wow.min.js"></script>

        <div class="modal fade" id="modal">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="popup_body">
                            <div class="alert alert-success" role="alert">Cám ơn bạn đã để lại thông tin. Bộ phận Tư vấn sẽ sớm liên hệ với bạn để tư vấn thêm về chương trình học!</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <script>
            $(window).width() > 1024 && new WOW().init();
            if ($(window).width() > 760) {
                $(window).on('scroll', function() {
                    if ($(window).scrollTop() > 50) {
                        $('#wrap-header').addClass('scrolled');
                        $('.navbar').addClass('scrolled');
                        $('.img-logo').addClass('scrolled');
                        $('.navbar-nav').addClass('scrolled');
                    } else {
                        $('#wrap-header').removeClass('scrolled');
                        $('.navbar').removeClass('scrolled');
                        $('.img-logo').removeClass('scrolled');
                        $('.navbar-nav').removeClass('scrolled');
                    }
                });
            }
            $(function() {
                $('a[href*=#]:not([href=#])').click(function() {
                    if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {

                        var target = $(this.hash);
                        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                        if ($(window).width() > 768) {
                            if (target.length) {
                                $('html,body').animate({
                                    scrollTop: target.offset().top - 80
                                }, 1000);
                                return false;
                            }
                        } else {
                            if (target.length) {
                                $('html,body').animate({
                                    scrollTop: target.offset().top - 80
                                }, 1000);
                                return false;
                            }
                        }
                    }
                });
            });

        </script>

        <script>
            $(document).ready(function() {

                $('body').scrollspy({
                    target: '#menu-collapse',
                    offset: 100
                });
            });

        </script>

        <script>
            $(document).ready(function () {
                $("#landing-page-id").bind('click', function (e) {

                    if ($.trim($("#name").val()) != '' && $.trim($("#email").val()) != '' && $.trim($("#phone").val()) != '') {
                        e.preventDefault();
                        var url = 'https://docs.google.com/forms/d/e/1FAIpQLSeuoywN74YXvloUgxeAePFWgCsiQPuf0fd1wFlKBtdZM7M5pQ/formResponse';
                        var data = {
                            'entry.1618386042': $("#name").val(),
                            'entry.1550829392': $("#email").val(),
                            'entry.1725316625': $("#phone").val(),
                            'entry.953023567': '<?php echo $utm_source; ?>',
                            'entry.823420574':'<?php echo $utm_medium;  ?>',
                            'entry.1198803187':'<?php echo $utm_campaign;  ?>'
                        };
                        $.ajax({
                            'url': url,
                            'method': 'POST',
                            'dataType': 'XML',
                            'data': data,
                            'statusCode': {
                                0: function () {
                                    $("#name").val('');
                                    $("#email").val('');
                                    $("#phone").val('');
                                    $("#modal").modal();
                                },
                                200: function () {
                                    $("#name").val('');
                                    $("#email").val('');
                                    $("#phone").val('');
                                    $("#modal").modal();
                                }
                            }

                        });
                    }

                });

            });




        </script>

	<div id="fb-root"></div>
	<script type="text/javascript">
		(function(d, s, id) {
			var js, fjs = d.getElementsByTagName(s)[0];
			if (d.getElementById(id)) return;
			js = d.createElement(s); js.id = id;
			js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.0&appId=790788601060712";
			fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));
	</script>
<!-- Google Tag Manager (noscript) -->

    <!--Start of Zopim Live Chat Script-->
<script type="text/javascript">
    window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
        d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
    _.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
        $.src="//v2.zopim.com/?1bMWnWprkt76mXJXRb6xBuP2dAD7uxPq";z.t=+new Date;$.
            type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
</script>
<!--End of Zopim Live Chat Script-->

<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-M92WKP"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
    <?php $this->endBody()?>
    </body>

    <!-- Mirrored from mana.edu.vn/cbp/khoa-hoc-ky-nang-giao-tiep/ by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 27 Apr 2016 13:31:05 GMT -->
</html>
