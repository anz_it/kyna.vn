<?php

use mana\models\Setting;
$dir_name = dirname(dirname(dirname(__FILE__)));
?>
<!DOCTYPE html>
<html lang="vi">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
	<meta property="fb:app_id" content="790788601060712">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="Bao gồm các kỹ năng và kiến thức chuyên sâu nhất về kỹ năng giao tiếp trong kinh doanh. Khóa học không chỉ tạo nên một phong thái kinh doanh chuyên nghiệp mà còn giúp tạo lập các mối quan hệ kinh doanh bền vững. Khóa học kỹ năng giao tiếp trong kinh doanh theo tiêu chuẩn quốc tế.">
    <meta name="author" content="">
    <!--<link rel="icon" href="">-->
    <title>Kỹ năng giao tiếp trong kinh doanh - Mana.edu.vn</title>

	<meta name="robots" content="index,follow">
    <meta property="og:type" content="website" />
    <meta property='og:url' content='https://mana.edu.vn/cbp/khoa-hoc-ky-nang-giao-tiep-v2' />
    <meta property='og:image:url' content='https://mana.edu.vn/mana/cbp/khoa-hoc-ky-nang-giao-tiep-v2/img/thumb-giaotiep.png' />
    <meta property='og:title' content='Kỹ năng giao tiếp trong kinh doanh - Mana.edu.vn' />
    <meta property='og:description' content='Bao gồm các kỹ năng và kiến thức chuyên sâu nhất về kỹ năng giao tiếp trong kinh doanh. Khóa học không chỉ tạo nên một phong thái kinh doanh chuyên nghiệp mà còn giúp tạo lập các mối quan hệ kinh doanh bền vững. Khóa học kỹ năng giao tiếp trong kinh doanh theo tiêu chuẩn quốc tế.' />

    <!-- Bootstrap core CSS -->

    <link href="/mana/cbp/khoa-hoc-ky-nang-giao-tiep-v2/dist/css/bootstrap.css" rel="stylesheet" />
    <link href="/mana/cbp/khoa-hoc-ky-nang-giao-tiep-v2/dist/css/bootstrap.min.css" rel="stylesheet" />
    <link href="/mana/cbp/khoa-hoc-ky-nang-giao-tiep-v2/css/fonts.css" rel="stylesheet" />
    <link href="/mana/cbp/khoa-hoc-ky-nang-giao-tiep-v2/css/style.css" rel="stylesheet" />
    <link href="/mana/cbp/khoa-hoc-ky-nang-giao-tiep-v2/css/iconfont.css" rel="stylesheet" />
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="icon" href="/mana/images/manaFavicon.png">
    <script src="/mana/cbp/khoa-hoc-ky-nang-giao-tiep-v2/dist/js/jquery.min.js"></script>
    <script src="/mana/cbp/khoa-hoc-ky-nang-giao-tiep-v2/js/jquery.marquee.min.js"></script>
    <script src="/mana/cbp/khoa-hoc-ky-nang-giao-tiep-v2/dist/js/tether.min.js"></script>
    <script src="/mana/cbp/khoa-hoc-ky-nang-giao-tiep-v2/dist/js/bootstrap.js"></script>
      <?php echo \common\helpers\Html::csrfMetaTags() ?>
</head>
<body>
<?php $this->beginBody()?>

    <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-M92WKP');</script>

    <!-- End Google Tag Manager -->
	<?php
	// get các tham số cần thiết */

	$getParams = $_GET;
	$utm_source = '';
	$utm_medium = '';
	$utm_campaign = '';

	if (isset($getParams['utm_source']) && trim($getParams['utm_source']) != '') {
		$utm_source = trim($getParams['utm_source']);
	}
	if (isset($getParams['utm_medium']) && trim($getParams['utm_medium']) != '') {
		$utm_medium = trim($getParams['utm_medium']);
	}
	if (isset($getParams['utm_campaign']) && trim($getParams['utm_campaign']) != '') {
		$utm_campaign = trim($getParams['utm_campaign']);
	}

	?>
    <section id="menu" class="menu">
      <div id="topbar">
        <span class="white">Nâng cao kỹ năng giao tiếp kinh doanh cùng chuyên gia &nbsp;<i class="fa fa-chevron-circle-right"></i> &nbsp;</span>
        <span class="yellow">Học bổng 1.500.000đ &nbsp;<i class="fa fa-arrow-down"></i></span>
      </div>
        <div class="container">
            <a class="navbar-brand" href="/">
                <img src="/mana/cbp/khoa-hoc-ky-nang-giao-tiep-v2/img/logo.png" alt="" />
            </a>
            <div class="contact">
                <button class="btn btn-register"><div class="icon-turn-right"></div>Đăng ký ngay!</button>
            </div>
            <nav class="navbar navbar-light bg-faded hidden-md-down" role="navigation">
                <button class="navbar-toggler btn-toggler hidden-lg-up collapsed" type="button" data-toggle="collapse" data-target="#collapsing-navbar">
                    <img src="/mana/cbp/khoa-hoc-ky-nang-giao-tiep-v2/img/icon-col.png" alt="" />
                </button>
                <div class="collapse navbar-toggleable-md" id="collapsing-navbar">
                    <ul class="nav navbar-nav">
                        <li class="nav-item"><a href="#info" class="nav-link active">Giới thiệu</a></li>
                        <li class="nav-item"><a href="#intro" class="nav-link">Nội dung</a></li>
                        <li class="nav-item"><a href="#teacher" class="nav-link">Giảng viên</a></li>
                        <li class="nav-item"><a href="#feedback" class="nav-link">Ý kiến học viên</a></li>
                    </ul>
                </div>
            </nav>
        </div>
    </section>
    <section id="banner" class="banner">
        <div class="container">
            <div class="title-main">
                <div class="title-header hidden-md-down">ĐÀO TẠO KỸ NĂNG GIAO TIẾP TRONG KINH DOANH</div>
                <div class="title-header hidden-lg-up">ĐÀO TẠO<br>KỸ NĂNG GIAO TIẾP<br>TRONG KINH DOANH</div>
                <div class="text">Theo tiêu chuẩn CBP (*)</div>
                <div class="box-register">
                    <button class="btn btn-register"><div class="icon-turn-right"></div>Đăng ký ngay!</button>
                </div>
            </div>
            <div class="note hidden-md-down">(*) CBP – Certified Business Professional,<br>là chứng chỉ Kinh doanh chuyên nghiệp.<br>Đây là một chứng chỉ uy tín về <b><i>kỹ năng kinh doanh</i></b><br>chuyên nghiệp và <b><i>kỹ năng quản lý.</i></b></div>
            <div class="note hidden-lg-up">(*) CBP – Certified Business Professional, là chứng chỉ Kinh doanh chuyên nghiệp. Đây là một chứng chỉ uy tín về <b><i>kỹ năng kinh doanh</i></b> chuyên nghiệp và <b><i>kỹ năng quản lý.</i></b></div>
        </div>

    </section>
    <section id="info" class="info" path="scrolling">
        <div class="container">
            <div class="title-info"><b>DUY NHẤT</b><br>và <b>ĐỘC QUYỀN</b></div>
            <div class="text-info">Tại MANA Online Business School</div>
            <div class="row box-info">
                <div class="item-info col-md-6 clearfix">
                    <div class="img">
                        <img src="/mana/cbp/khoa-hoc-ky-nang-giao-tiep-v2/img/icon-1.png" class="img-responsive" alt="">
                    </div>
                    <div class="content">
                        Giáo trình gốc theo bản quyền của IBTA, Hoa Kỳ
                    </div>
                </div>
                <div class="item-info col-md-6 clearfix">
                    <div class="img">
                        <img src="/mana/cbp/khoa-hoc-ky-nang-giao-tiep-v2/img/icon-2.png" class="img-responsive" alt="">
                    </div>
                    <div class="content">
                        Làm chủ mọi tình huống trong môi trường giao tiếp kinh doanh chuẩn mực
                    </div>
                </div>
                <div class="item-info col-md-6 clearfix">
                    <div class="img">
                        <img src="/mana/cbp/khoa-hoc-ky-nang-giao-tiep-v2/img/icon-3.png" class="img-responsive" alt="">
                    </div>
                    <div class="content">
                        Cung cấp hệ thống kiến thức chuyên sâu và các case study thực tế
                    </div>
                </div>
                <div class="item-info col-md-6 clearfix">
                    <div class="img">
                        <img src="/mana/cbp/khoa-hoc-ky-nang-giao-tiep-v2/img/icon-4.png" class="img-responsive" alt="">
                    </div>
                    <div class="content">
                        Tạo điều kiện phấn đấu, phát triển kỹ năng để trở thành một nhà kinh doanh chuyên nghiệp
                    </div>
                </div>
            </div>
            <div id="giaotrinh">
                <div class="title-giaotrinh">GIÁO TRÌNH GỐC THEO BẢN QUYỀN<br><b>CỦA TỔ CHỨC IBTA</b></div>
                <div class="row row-giaotrinh">
                    <div class="col-lg-8">
                            <p><b>IBTA (International Business Training Association) là Hiệp hội Đào tạo Kinh doanh Quốc tế, có trụ sở tại Hoa Kỳ. </b><br></p>
                            <p>IBTA là một tổ chức kiến thức chuyên đào tạo và cấp chứng chỉ CBP (Certified Business Professional) ở nhiều lĩnh vực kinh doanh khác nhau theo chuẩn quốc tế được công nhận trên toàn cầu.<br>
                            Chứng chỉ CBP của IBTA là một trong những chứng chỉ uy tín nhất về kỹ năng kinh doanh chuyên nghiệp và kỹ năng quản lý. CBP được công nhận trên toàn cầu từ Mỹ, Canada, Caribe, châu Phi, Trung Đông, Trung Quốc, Ấn Độ, đến Singapore và vùng Viễn Đông. </p>
                            <p><b><i>Trên thế giới, các tập đoàn lớn như HONDA, Nokia, Mitsubishi, Intel, Ricoh, adidas,… đều công nhận tiêu chuẩn đánh giá về kỹ năng kinh doanh của IBTA được mô tả qua chứng chỉ CBP.</i></b></p>
                    </div>
                    <div class="col-lg-4">
                        <img src="/mana/cbp/khoa-hoc-ky-nang-giao-tiep-v2/img/img-giaotrinh.png" class="img-fluid img-giaotrinh" alt="">
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="process">
        <div class="container">
            <div class="title-process"><b>CHƯƠNG TRÌNH ĐÀO TẠO</b><br><b>KỸ NĂNG GIAO TIẾP TRONG KINH DOANH</b><br>Theo tiêu chuẩn CBP</div>
            <div class="row-process clearfix">
                <div class="item-process first"><h4>4 kỹ năng</h4>(giao tiếp ngôn từ, giao tiếp phi ngôn từ, giao tiếp bằng văn bản, thuyết trình)</div>
                <div class="item-process down"><h4>20h</h4>nội dung</div>
                <div class="item-process center"><h4>49</h4>bài học và thực hành</div>
                <div class="item-process down"><h4>Trực tuyến</h4>mọi lúc mọi nơi</div>
                <div class="item-process last">Trợ giảng<h4>1 kèm 1</h4></div>
            </div>
        </div>
    </section>
    <section id="intro" path="scrolling">
        <div class="container">
            <div class="title-intro">NỘI DUNG KHÓA HỌC</div>
            <div class="row-intro clearfix">
                <div class="content col-lg-7">
                    <ul>
                        <li><span>Kỹ năng giao tiếp có vai trò như thế nào trong kinh doanh.</span></li>
                        <li><span>Cấu trúc giao tiếp trong kinh doanh và các yếu tố ảnh hưởng đến quá trình giao tiếp thường ngày.</span></li>
                        <li><span>Phát triển phong cách viết trong kinh doanh chuyên nghiệp.</span></li>
                        <li><span>Kỹ năng viết các văn bản thường gặp trong kinh doanh bao gồm bản ghi nhớ, báo cáo, email v.v</span></li>
                        <li><span>Phát triển kỹ năng giao tiếp ngôn từ lôi cuốn, tự tin.</span></li>
                        <li><span>Giao tiếp và trao đổi bằng điện thoại trong kinh doanh.</span></li>
                        <li><span>Ngôn ngữ cơ thể - công cụ giao tiếp quyền lực.</span></li>
                        <li><span>Phát triển kỹ năng thuyết trình hiệu quả và tự tin.</span></li>
                        <li><span>Giải quyết mâu thuẫn xung đột và bất đồng trong giao tiếp kinh doanh.</span></li>
                    </ul>
                </div>
                <div class="video col-md-5">
                    <img src="/mana/cbp/khoa-hoc-ky-nang-giao-tiep-v2/img/video-intro.png" alt="" data-toggle="modal" data-target="#video-intro">
                </div>
            </div>
            <div class="box-register">
                <!--<center data-xmas="false">
                  <button class="btn btn-register size-larger">
                    <div class="icon-turn-right"></div>ĐĂNG KÝ NHẬN<br>HỌC BỔNG 1.000.000 đ
                  </button>
                </center>-->
                <center data-xmas="true">
                  <button class="btn btn-register size-larger">
                    <div class="icon-turn-right"></div>ĐĂNG KÝ NHẬN<br> HỌC BỔNG 1.500.000Đ
                  </button>
                </center>
            </div>
        </div>
    </section>
    <section id="teacher" path="scrolling">
        <div class="container">
            <div class="row row-teacher">
                <div class="images-teacher col-md-4">
                    <center><div class="title-teacher hidden-md-up">HỌC TRỰC TUYẾN CÙNG CHUYÊN GIA</div></center>
                    <img class="hidden-sm-down" src="/mana/cbp/khoa-hoc-ky-nang-giao-tiep-v2/img/img-teacher1.png" alt="">
                    <img class="hidden-md-up" src="/mana/cbp/khoa-hoc-ky-nang-giao-tiep-v2/img/img-teacher1-mb.png" alt="">
                </div>
                <div class="info-teacher col-md-8 hidden-sm-down">
                    <div class="title-teacher">HỌC TRỰC TUYẾN CÙNG CHUYÊN GIA</div>
                    <div class="box-intro-teacher">
                        <div class="author">Thạc sỹ Hồ Thị Thanh Vân (Vân Hồ)</div>
                        <ul class="list-skill">
                            <li><i class="icon-caret-right"></i> Được đào tạo Cao học chuyên ngành Marketing Bán hàng và Dịch vụ (MMSS) tại trường Đại học Paris 1 Sorbonne (IAE) phối hợp với trường Quản trị Châu Âu (ESCP - EAP European School Management).</li>
                            <li><i class="icon-caret-right"></i> 20 năm kinh nghiệm trong ngành Sales &amp; Marketing tại các công ty hàng đầu thế giới:
                                <ul>
                                    <li>Giám đốc Ngành hàng tại Việt Nam kiêm Lãnh đạo thị trường Cambodia/Laos của Novartis Pharma.</li>
                                    <li>Nguyên Giám đốc Điều hành Tiếp thị - AstraZeneca.</li>
                                    <li>Nguyên Giám đốc Marketing - Sanofi.</li>
                                    <li>Nguyên Giám đốc Thương hiệu - Abbott.</li>
                                    <li>Nguyên Giám đốc Kinh doanh - GlaxoSmithKline.</li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="info-teacher inbox col-xs-12">
                    <div class="box-intro-teacher hidden-md-up">
                        <div class="author">Thạc sỹ Hồ Thị Thanh Vân (Vân Hồ)</div>
                        <ul class="list-skill">
                            <li><i class="icon-caret-right"></i> Được đào tạo Cao học chuyên ngành Marketing Bán hàng và Dịch vụ (MMSS) tại trường Đại học Paris 1 Sorbonne (IAE) phối hợp với trường Quản trị Châu Âu (ESCP - EAP European School Management).</li>
                            <li><i class="icon-caret-right"></i> 20 năm kinh nghiệm trong ngành Sales &amp; Marketing tại các công ty hàng đầu thế giới:
                                <ul>
                                    <li>Giám đốc Ngành hàng tại Việt Nam kiêm Lãnh đạo thị trường Cambodia/Laos của Novartis Pharma.</li>
                                    <li>Nguyên Giám đốc Điều hành Tiếp thị - AstraZeneca.</li>
                                    <li>Nguyên Giám đốc Marketing - Sanofi.</li>
                                    <li>Nguyên Giám đốc Thương hiệu - Abbott.</li>
                                    <li>Nguyên Giám đốc Kinh doanh - GlaxoSmithKline.</li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <ul class="more-info">
                        <li><i class="icon-caret-right"></i> Dành nhiều giải thưởng danh giá:</li>
                        <li>Giải thưởng của Phó Chủ tịch tập đoàn Khu vực Châu Á.</li>
                        <li>Giải thưởng Khu vực International cho thành tích xuất sắc trong việc giới thiệu thành công sản phẩm mới ra thị trường (AstraZeneca).</li>
                        <li>Giải thưởng Quán quân Marketing cho Chương trình Marketing xuất sắc (Abbott).</li>
                        <li>Giải thưởng Cầu vồng dành cho Lãnh đạo xuất sắc.</li>
                        <li>Giải thưởng Thành tích Kinh doanh xuất sắc trong 4 năm liên tiếp (GlaxoSmithKline).</li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <section id="result">
        <div class="container">
            <div class="row row-result">
                <div class="left col-md-6 clearfix">
                    <ul>
                        <li>LỢI ÍCH HỌC TRÊN MANA</li>
                        <li><i class="icon-check"></i> <b>Học mọi lúc – mọi nơi:</b> Học qua video bài giảng được biên tập chuyên nghiệp. Tài liệu bao gồm sách và bài thuyết trình, tư liệu tham khảo.</li>
                        <li><i class="icon-check"></i> <b>Học kèm 1 – 1:</b> Cố vấn 1 kèm 1, trong suốt quá trình học và luyện thi. Hỏi đáp cùng chuyên gia, doanh nhân giàu kinh nghiệm.</li>
                        <li><i class="icon-check"></i> <b>Hỏi đáp và luyện thi:</b> Chương trình học được đan xen giữa bài học lý thuyết và thực hành, cùng hơn 100 bài thi thử.</li>
                        <li><i class="icon-check"></i> <b>Thi trực tuyến và nhận chứng chỉ quốc tế:</b> Thi trực tuyến trên hệ thống PROMETRIC với tối đa 6 tháng ôn luyện trước khi thi.</li>
                    </ul>
                </div>
                <div class="right col-md-6 clearfix">
                    <ul>
                        <li>LỢI THẾ KHI SỞ HỮU CBP</li>
                        <li><i class="icon-check"></i> Đạt điều kiện cần để thi lấy chứng chỉ CBP Executive – chứng chỉ CBP Lãnh đạo cấp trung.</li>
                        <li><i class="icon-check"></i> Là chứng nhận cho kỹ năng kinh doanh chuyên nghiệp.</li>
                        <li><i class="icon-check"></i> Sở hữu chứng chỉ có giá trị trên toàn cầu.</li>
                        <li><i class="icon-check"></i> Là lợi thế khi ứng tuyển vào các tập đoàn đa quốc gia, tập đoàn nước ngoài.</li>
                        <li><i class="icon-check"></i> Tăng cơ hội thăng tiến trong công việc.</li>
                    </ul>
                </div>
                <div class="col-xs-12"></div>
            </div>
        </div>
    </section>
    <section id="feedback" path="scrolling">
        <div class="container">
            <div class="row row-feedback">
                <div class="col-lg-4">
                    <div class="title-feedback">
                        <div>HỌC VIÊN CỦA MANA ONLINE BUSINESS SCHOOL</div>
                        <img alt="" src="/mana/cbp/khoa-hoc-ky-nang-giao-tiep-v2/img/icon-speaker.png">
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="content-feedback">
                        <p>“Khi được cử đi học khóa này, tôi vẫn nghĩ giao tiếp thì có gì mà học, giao tiếp trong kinh doanh chẳng phải là những buổi tiệc rượu hay sao.<br>Nhưng tôi đã nhận ra mình nông cạn quá. Việc giao tiếp thành công trong kinh doanh không chỉ là việc trao đổi trực tiếp mà còn gián tiếp, giao tiếp bằng ngôn ngữ viết và giao tiếp trực tuyến. Hơn hết, giao tiếp trong kinh doanh còn cần phải có chiến lược cụ thể và có phong cách riêng biệt.”</p>
                        <p class="name">Anh Hoài Nam – Trưởng phòng Kế hoạch và Đầu tư công ty cổ phần Việt Long</p>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="content-feedback">
                        <p>“Tôi vốn đã ngưỡng mộ chị Vân Hồ từ lâu. Không chỉ là vì chị có phong thái đĩnh đạc và tự tin mà còn vì những thành tựu mà chị đã đạt được. Tham gia khóa học này, tôi mới nghiệm ra được phong cách giao tiếp trong kinh doanh là một trong những bí quyết thành công của chị. Tôi muốn thử thách mình ở các công ty lớn đa quốc gia và chứng chỉ CBP là bước đầu để tôi chứng minh được giá trị của mình.”</p>
                        <p class="name">Chị Thanh Thảo – Senior Marketing Manager</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="line">
        <div class="img-line"></div>
    </section>
    <section id="register">
        <div class="container">
            <div class="title-register" data-xmas="false">Học online cùng Mana Online Business School</div>
    			<form action="/course/page/submit" name="landing-page-id" class="form-horizontal" id="form_advice"  accept-charset="utf-8">

                    <input type="hidden" id="advice_name" name="advice_name" value="Mana - Khóa học kỹ năng giao tiếp V2" /> 
                    <input type="hidden" id="csrf" name="_csrf" /><div class="box-register">
    					<div class="col-md-4 clearfix">
    						<label for="name">Họ và tên (*)</label>
    						<div class="input-group">
    							<span class="input-group-addon" id="icon-name"><i class="icon-user"></i></span>
    							<input type="text" class="form-control" id="fullname" name="fullname" aria-describedby="icon-name" placeholder="Nhập họ và tên" required>
    						</div>
    					</div>
    					<div class="col-md-4 clearfix">
    						<label for="email">Email (*)</label>
    						<div class="input-group">
    							<span class="input-group-addon" id="icon-email"><i class="icon-mail"></i></span>
    							<input type="text" class="form-control" id="email" name="email" aria-describedby="icon-email" placeholder="Nhập email" required>
    						</div>
    					</div>
    					<div class="col-md-4 clearfix">
    						<label for="phone">Số điện thoại (*)</label>
    						<div class="input-group">
    							<span class="input-group-addon" id="icon-phone"><i class="icon-phone"></i></span>
    							<input type="text" class="form-control" id="phonenumber" name="phonenumber" aria-describedby="icon-phone" placeholder="Nhập số điện thoại" required>
    						</div>
    					</div>
    					<div class="button-register">
    						<button class="btn btn-register btn_box_register size-medium  btn-regist button-regis" id="dang_ky_form"><div class="icon-turn-right"></div>Đăng ký ngay!</button>
    					</div>
              <!--<div data-xmas="false">
                <p class="text-button">Mọi thông tin của bạn đều được bảo mật hoàn toàn</p>
      					<div class="note col-md-8">
                  * Đăng ký khóa học và nhận ngay bộ tài liệu bao gồm các
                    <span>mẫu văn bản bằng tiếng Anh</span> phải dùng trong môi trường kinh doanh hiện nay.<br/>
                  * Đăng ký khóa học để có cơ hội nhận được <span>học bổng 1.000.000đ.</span></div>
              </div>-->
              <div data-xmas="true">
                <p class="text-button">
                  ĐĂNG KÝ NHẬN HỌC BỔNG <br>
                  Bộ phận CSKH sẽ liên hệ với bạn sớm nhất có thể. Mọi thắc mắc về khóa học và học bổng
                  sẽ được giải đáp thông qua cuộc gọi này.
                </p>
                <div class="note col-md-8">
                * Đăng ký khóa học và nhận ngay bộ tài liệu bao gồm các
                  <span>mẫu văn bản bằng tiếng Anh</span> phải dùng trong môi trường kinh doanh hiện nay.<br/>
                * Đăng ký khóa học để có cơ hội nhận được <span>học bổng 1.500.000đ.</span></div>
                </div>
              </div>
    				</div>
    			</form>
        </div>
    </section>
    <section id="about">
        <div class="container">
            <div class="logo"><img src="/mana/cbp/khoa-hoc-ky-nang-giao-tiep-v2/img/logo.png" alt=""></div>
            <div class="content">
                <p>Xin chào, chúng tôi là MANA – Viện đào tạo Quản trị Kinh doanh Trực tuyến hàng đầu Việt Nam.</p>
                <p>MANA mang đến một hệ thống các chương trình đào tạo kỹ năng quản trị kinh doanh có tính ứng dụng cao.</p>
                <p>Tại MANA, học viên sẽ được học trên các chương trình đào tạo song ngữ cùng đội ngũ cố vấn học tập 24/7.</p>
                <p>Hình thức và phương pháp học khoa học mang lại cho học viên những trải nghiệm học tập mới mẻ và thú vị.</p>
                <p>Vào 16.6.2016, Dream Viet Education Corporation (công ty chủ quản của MANA OBS) đã trở thành đối tác chính thức và độc quyền của Hiệp hội IBTA tại Việt Nam. Điều này xác định, MANA là đại diện hợp pháp trong việc phân phối các chương trình Đào tạo kỹ năng Kinh doanh chuẩn quốc tế trên toàn Việt Nam trong 5 năm 2016 – 2021.</p>
            </div>
        </div>
    </section>
	<footer>
	<section id="hoi-dap">
        <div class="container">
            <div class="fb-comments fb_iframe_widget fb_iframe_widget_fluid"
                data-href="https://mana.edu.vn/cbp/khoa-hoc-ky-nang-giao-tiep-v2"
                data-width="100%" data-numposts="10"
                data-colorscheme="light" fb-xfbml-state="rendered">
             </div>
        </div><!--end .container-->
    </section>
    <div class="container footer">
        <div class="row">
            <div class="col-md-3 logo">
                <div class="footer-logo">
                    <ul>
                        <li>
                            <a href="#">
                                <img src="/mana/cbp/khoa-hoc-ky-nang-giao-tiep-v2/img/1_Logo.png" class="img-responsive"/>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <img src="/mana/cbp/khoa-hoc-ky-nang-giao-tiep-v2/img/18_KynaLogo.png" class="img-responsive"/>
                            </a>
                        </li>
                    </ul>

                </div>
            </div>
            <div class="col-md-6">
                <div class="footer-text-center">
                    <h4>Công ty Cổ phần  Dream Việt Education</h4>
                    <p> <b class="company-address"> Trụ sở chính:</b>  Lầu 6 Tòa nhà Thịnh Phát - 178/8 Đường D1, Phường 25, Quận Bình Thạnh, TP Hồ Chí Minh</p>
                    <p> <b class="company-address"> Văn phòng Hà Nội:</b>  Tầng 6, Tòa Nhà Viện Công Nghệ - 25 Vũ Ngọc Phan, Phường Láng Hạ, Quận Đống Đa, TP Hà Nội </p>
                    <p style="padding-top: 20px">Giấy phép ĐKKD số 0313589030 do Sở Kế hoạch và Đầu tư TPHCM cấp</p>
                </div>
            </div>
            <div class="col-md-3">
                <div class="footer-hot-line">
                    <p><b class="company-address">Hotline:</b>  1900 6364 09</p>
                    <p><b class="company-address">Email: </b>hotro@kyna.vn</p>
                    <div class="fb-page" data-href="https://www.facebook.com/mana.edu.vn/" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/mana.edu.vn/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/mana.edu.vn/">MANA.edu.vn</a></blockquote></div>
                </div>
            </div>
        </div>
    </div>
</footer>
	<div class="modal fade" id="video-intro">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-body">
                    <div class="videoWrapper"></div>
                    <button class="btn btn-close" data-dismiss="modal" aria-label="Close">Xem thông tin</button>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade" id="modal">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-body">
					<div class="popup_body">
						<div class="alert alert-success" role="alert">Cám ơn bạn đã để lại thông tin. Kyna.vn sẽ sớm liên hệ với bạn để tư vấn thêm về chương trình học!</div>
					</div>
				</div>
			</div>
		</div>
	</div>
    <script src="/mana/cbp/khoa-hoc-ky-nang-giao-tiep-v2/js/main.js"></script>

	<!-- Nhan them vao -->
	<script>
		$(document).ready(function () {
          var today = new Date(),
              expiredDate = new Date('02/01/2017');
          if (today < expiredDate ){
            $('#menu').addClass('has-topbar');
            $('[data-xmas="false"]').each(function (idx, elm) {
              $(elm).hide();
            });
          } else{
            $('[data-xmas="true"]').each(function (idx, elm) {
              $(elm).remove();
              $('#noel-topbar').remove();
              $('#menu').removeClass('has-topbar');
            });
          }

		});
	</script>
	<!--End of Zopim Live Chat Script-->
    <!--Start of Zopim Live Chat Script-->
<script type="text/javascript">
    window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
        d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
    _.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
        $.src="//v2.zopim.com/?1bMWnWprkt76mXJXRb6xBuP2dAD7uxPq";z.t=+new Date;$.
            type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
</script>
<!--End of Zopim Live Chat Script-->

	<div id="fb-root"></div>
	<script type="text/javascript">
		(function(d, s, id) {
			var js, fjs = d.getElementsByTagName(s)[0];
			if (d.getElementById(id)) return;
			js = d.createElement(s); js.id = id;
			js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.0&appId=790788601060712";
			fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));
	</script>

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-M92WKP"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<?php $this->endBody()?>
</body>
</html>
