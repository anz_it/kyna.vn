<?php
use mana\models\Setting;
$dir_name = dirname(dirname(dirname(__FILE__)));
?>
<!DOCTYPE html>
<html lang="vi">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
	<meta property="fb:app_id" content="790788601060712">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="Ưu đãi lớn khi đăng ký sớm.+ Tiêu chuẩn đào tạo quốc tế + Được thừa nhận trên toàn cầu + Học với các chuyên gia hàng đầu + Học song ngữ Anh - Việt + Tăng mức lương với chứng chỉ quốc tế.">
    <meta name="author" content="">
    <!--<link rel="icon" href="">-->
    <title>Đào tạo kỹ năng chăm sóc và thấu hiểu khách hàng - Mana.edu.vn</title>

	<meta name="robots" content="index,follow">
    <meta property="og:type" content="website" />
    <meta property='og:url' content='https://mana.edu.vn/cbp/khoa-hoc-ky-nang-cham-soc-khach-hang-v2' />
    <meta property='og:image:url' content='https://mana.edu.vn/mana/cbp/khoa-hoc-ky-nang-cham-soc-khach-hang-v2/img/thumb.png' />
    <meta property='og:title' content='Đào tạo kỹ năng chăm sóc và thấu hiểu khách hàng - Mana.edu.vn' />
    <meta property='og:description' content='Ưu đãi lớn khi đăng ký sớm.+ Tiêu chuẩn đào tạo quốc tế + Được thừa nhận trên toàn cầu + Học với các chuyên gia hàng đầu + Học song ngữ Anh - Việt + Tăng mức lương với chứng chỉ quốc tế.' />
    <!-- Bootstrap core CSS -->
    <link href="/mana/cbp/khoa-hoc-ky-nang-cham-soc-khach-hang-v2/css/main.css" rel="stylesheet" />

    <script src="/mana/cbp/khoa-hoc-ky-nang-cham-soc-khach-hang-v2/js/jquery.min.js"></script>
    <script src="/mana/cbp/khoa-hoc-ky-nang-cham-soc-khach-hang-v2/js/jquery.marquee.min.js"></script>
    <script src="/mana/cbp/khoa-hoc-ky-nang-cham-soc-khach-hang-v2/js/tether.min.js"></script>
    <script src="/mana/cbp/khoa-hoc-ky-nang-cham-soc-khach-hang-v2/js/bootstrap.js"></script>
    <script src="/mana/cbp/khoa-hoc-ky-nang-cham-soc-khach-hang-v2/js/slick.min.js"></script>
      <?php echo \common\helpers\Html::csrfMetaTags() ?>

    </head>


<body>
<?php $this->beginBody()?>

    <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-M92WKP');</script>

    <!-- End Google Tag Manager -->
	<?php
	// get các tham số cần thiết */

	$getParams = $_GET;
	$utm_source = '';
	$utm_medium = '';
	$utm_campaign = '';

	if (isset($getParams['utm_source']) && trim($getParams['utm_source']) != '') {
		$utm_source = trim($getParams['utm_source']);
	}
	if (isset($getParams['utm_medium']) && trim($getParams['utm_medium']) != '') {
		$utm_medium = trim($getParams['utm_medium']);
	}
	if (isset($getParams['utm_campaign']) && trim($getParams['utm_campaign']) != '') {
		$utm_campaign = trim($getParams['utm_campaign']);
	}

	?>
    <section id="menu">
        <div class="container">
            <a class="navbar-brand" href="/">
                <img src="/mana/cbp/khoa-hoc-ky-nang-cham-soc-khach-hang-v2/img/logo.png" alt="" />
            </a>
            <div class="contact">
                <button class="btn btn-register shadow-invert">ĐĂNG KÝ</button>
            </div>
            <nav class="navbar navbar-light bg-faded" role="navigation">
                <button class="navbar-toggler btn-toggler hidden-xs-up collapsed" type="button" data-toggle="collapse" data-target="#collapsing-navbar">
                    <img src="/mana/cbp/khoa-hoc-ky-nang-cham-soc-khach-hang-v2/img/icon-col.png" alt="" />
                </button>
                <div class="collapse navbar-toggleable-md" id="collapsing-navbar">
                    <ul class="nav navbar-nav">
                        <li class="nav-item"><a href="#about" class="nav-link active">GIỚI THIỆU</a></li>
                        <li class="nav-item"><a href="#skill" class="nav-link">NỘI DUNG</a></li>
                        <li class="nav-item"><a href="#proccess" class="nav-link">GIẢNG VIÊN</a></li>
                    </ul>
                </div>
            </nav>
        </div>
    </section>
    <section id="banner">
        <div class="container">
            <div class="tit-1">Đào tạo kỹ năng</div>
            <div class="tit-2">CHĂM SÓC KHÁCH HÀNG</div>
            <div class="tit-3">Theo chuẩn quốc tế (*)</div>
            <div class="tit-4">*Theo tiêu chuẩn của Hiệp hội Đào tạo Kinh doanh Quốc tế IBTA. Hoàn thành khóa học này, bạn sẽ được
                <br>cấp chứng chỉ CBP được xét duyệt bởi Hiệp hội IBTA, chứng chỉ được công nhận trên toàn cầu.</div>
            <button class="btn btn-register shadow-invert current-about">TÌM HIỂU THÊM <i class="icon-chevron-circle-down"></i></button>
        </div>
    </section>
    <section id="about" path='scrolling'>
        <div class="container">
            <div class="col-left">
                <h3>Customer Service chính là:</h3>
                <ul>
                    <li class="icon-play-1">
                        <p class="title">Gương mặt thương hiệu của doanh nghiệp:</p>
                        <p>Đó phải là một bộ mặt thân thiện được thể hiện qua giọng nói, ngữ điệu.</p>
                    </li>
                    <li class="icon-play-1">
                        <p class="title">Người bạn của khách hàng:</p>
                        <p>Một người bạn nhẫn nại, biết lắng nghe và biết thấu hiểu.</p>
                    </li>
                    <li class="icon-play-1">
                        <p class="title">Yếu tố phát triển bền vững:</p>
                        <p>Dịch vụ CSKH tốt sẽ gia tăng thiện cảm từ khách hàng và là lợi thế cạnh tranh vô cùng lớn.</p>
                    </li>
                </ul>
            </div>
            <div class="col-bottom">
                <h3>Kết quả khảo sát 500 chuyên gia nhân sự tại Mỹ cho rằng:</h3>
                <div class="des"><i><span>“</span>Một người <font color="#06188d">Chăm sóc khách hàng giỏi</font> có tố chất để trở thành một nhà <font color="#06188d">Lãnh đạo giỏi</font>, vì họ có:</i></div>
                <div class="row row-feature">
                    <div class="col-md-4 col-xs-6">
                        <div class="item">
                            <img src="/mana/cbp/khoa-hoc-ky-nang-cham-soc-khach-hang-v2/img/icon-feature-1.png" alt="" />
                            <p>Tư duy về nhu cầu của
                                <br>khách hàng cực tốt</p>
                        </div>
                    </div>
                    <div class="col-md-4 col-xs-6">
                        <div class="item">
                            <img src="/mana/cbp/khoa-hoc-ky-nang-cham-soc-khach-hang-v2/img/icon-feature-2.png" alt="" />
                            <p>Khả năng sử dụng
                                <br>ngôn từ đỉnh cao</p>
                        </div>
                    </div>
                    <div class="col-md-4 col-xs-6">
                        <div class="item">
                            <img src="/mana/cbp/khoa-hoc-ky-nang-cham-soc-khach-hang-v2/img/icon-feature-3.png" alt="" />
                            <p>Khả năng nhìn xa
                                <br>trông rộng</p>
                        </div>
                    </div>
                    <div class="col-md-4 col-xs-6">
                        <div class="item">
                            <img src="/mana/cbp/khoa-hoc-ky-nang-cham-soc-khach-hang-v2/img/icon-feature-4.png" alt="" />
                            <p>Biết quản lý thời gian</p>
                        </div>
                    </div>
                    <div class="col-md-4 col-xs-6">
                        <div class="item">
                            <img src="/mana/cbp/khoa-hoc-ky-nang-cham-soc-khach-hang-v2/img/icon-feature-5.png" alt="" />
                            <p>Kỹ năng phân tích</p>
                        </div>
                    </div>
                    <div class="col-md-4 col-xs-6">
                        <div class="item">
                            <img src="/mana/cbp/khoa-hoc-ky-nang-cham-soc-khach-hang-v2/img/icon-feature-6.png" alt="" />
                            <p>Sự kiên trì</p>
                        </div>
                    </div>
                </div>
                <div class="note"><b>Theo Hiệp hội IBTA</b>, kỹ năng <b>Chăm sóc khách hàng</b> chính là
                    <br class="hidden-sm-down"><b>1 trong 5 kỹ năng phải có</b> để trở thành <b>một nhà quản lý thực sự.</b></div>
            </div>
        </div>
    </section>
    <section id="skill" path='scrolling'>
        <div class="container">
            <h2>Khoá học kỹ năng<br><span>CHĂM SÓC KHÁCH HÀNG</span><br>theo tiêu chuẩn quốc tế</h2>
            <div class="row row-menu">
                <div class="item">
                    <div class="content">
                        <div class="table">
                            <div class="table-cell">
                                <font color="#f73131"><u>10</u> GIỜ</font>
                                <br>nội dung
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="content">
                        <div class="table">
                            <div class="table-cell">
                                <font color="#f73131"><u>7</u></font>
                                <br>chuyên đề
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="content">
                        <div class="table">
                            <div class="table-cell">
                                Trực tuyến
                                <br><font color="#f73131">mọi lúc, <br>mọi nơi</font>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="content">
                        <div class="table">
                            <div class="table-cell">
                                Trợ giảng
                                <br><font color="#f73131">1 kèm 1</font>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <h3>Nội dung chi tiết khóa học:</h3>
            <div class="box-content" id="contentAccordion">
                <div class="panel panel-default">
                    <div class="collapsed" data-toggle="collapse" data-parent="#contentAccordion" data-target="#question1" path="scrolling">
                        <a href="#" class="ing">
                            <h6 class="panel-title icon-">
                                <span>1.</span> Kỹ năng giao tiếp trong dịch vụ chăm sóc khách hàng
                            </h6>
                        </a>
                    </div>
                    <div id="question1" class="panel-collapse collapse" style="height: 0px;">
                        <div class="panel-body">
                            <ul>
                                <li>Giao tiếp với khách hàng bằng lời</li>
                                <li>Giao tiếp không bằng lời</li>
                                <li>Khoảng cách thực trong giao tiếp</li>
                                <li>Cách để truyền đạt thông tin hiệu quả đến khách hàng</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="collapsed" data-toggle="collapse" data-parent="#contentAccordion" data-target="#question2" path="scrolling">
                        <a href="#" class="ing">
                            <h6 class="panel-title icon-">
                                <span>2.</span> Phân tích tâm lý và thấu hiểu tâm lý khách hàng
                            </h6>
                        </a>
                    </div>
                    <div id="question2" class="panel-collapse collapse" style="height: 0px;">
                        <div class="panel-body">
                            <ul>
                                <li>Tác phong làm việc và thái độ đúng chuẩn trong quá trình chăm sóc</li>
                                <li>Phân tích khách hàng</li>
                                <li>Xác định mức độ dịch vụ phù hợp</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="collapsed" data-toggle="collapse" data-parent="#contentAccordion" data-target="#question3" path="scrolling">
                        <a href="#" t class="ing">
                            <h6 class="panel-title icon-">
                                <span>3.</span> Cách ứng xử với khách hàng khó tính
                            </h6>
                        </a>
                    </div>
                    <div id="question3" class="panel-collapse collapse" style="height: 0px;">
                        <div class="panel-body">
                            <ul>
                                <li>Tìm hiểu nguyên nhân khiến khách hàng không vui/khó chịu</li>
                                <li>Cách tránh những điều khó chịu</li>
                                <li>5 bước làm dịu sự nóng tính/ khó tính của khách hàng</li>
                                <li>Những điều nên làm khi đến chính bạn cũng thấy khó chịu với sự khó chiều của khách hàng</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="collapsed" data-toggle="collapse" data-parent="#contentAccordion" data-target="#question4" path="scrolling">
                        <a href="#" class="ing">
                            <h6 class="panel-title icon-">
                                <span>4.</span> Chăm sóc khách hàng qua điện thoại
                            </h6>
                        </a>
                    </div>
                    <div id="question4" class="panel-collapse collapse" style="height: 0px;">
                        <div class="panel-body">
                            <ul>
                                <li>Kỹ năng sử dụng điện thoại</li>
                                <li>Kỹ năng chào hỏi chuyên nghiệp và lắng nghe tích cực</li>
                                <li>Tips & tricks trong quá trình chăm sóc khách hàng qua điện thoại</li>
                                <li>Cách kết thúc một cuộc gọi</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="collapsed" data-toggle="collapse" data-parent="#contentAccordion" data-target="#question5" path="scrolling">
                        <a href="#" class="ing">
                            <h6 class="panel-title icon-">
                                <span>5.</span> Chăm sóc khách hàng qua Internet
                            </h6>
                        </a>
                    </div>
                    <div id="question5" class="panel-collapse collapse" style="height: 0px;">
                        <div class="panel-body">
                            <ul>
                                <li>Hướng dẫn giao tiếp bằng thư điện tử</li>
                                <li>Kiến thức phải hiểu khi chăm sóc khách hàng qua Internet</li>
                                <li>Kịch bản chăm sóc khách hàng qua Internet</li>
                                <li>Hỗ trợ khách hàng trực tuyến như thế nào được xem là hiệu quả</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="collapsed" data-toggle="collapse" data-parent="#contentAccordion" data-target="#question6" path="scrolling">
                        <a href="#" class="ing">
                            <h6 class="panel-title icon-">
                                <span>6.</span> Chiến lược quản lý thời gian
                            </h6>
                        </a>
                    </div>
                    <div id="question6" class="panel-collapse collapse" style="height: 0px;">
                        <div class="panel-body">
                            <ul>
                                <li>Kiểm soát và phân tích thời gian, nhiệm vụ của bạn</li>
                                <li>Sắp xếp thứ tự ưu tiên của các nhiệm vụ</li>
                                <li>Các trường hợp gây lãng phí thời gian</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="collapsed" data-toggle="collapse" data-parent="#contentAccordion" data-target="#question7" path="scrolling">
                        <a href="#" class="ing">
                            <h6 class="panel-title icon-">
                                <span>7.</span> Phương pháp tạm biệt sự căng thẳng
                            </h6>
                        </a>
                    </div>
                    <div id="question7" class="panel-collapse collapse" style="height: 0px;">
                        <div class="panel-body">
                            <ul>
                                <li>Điều gì gây nên sự căng thẳng</li>
                                <li>Trách nhiệm, sự yêu thích và nghỉ ngơi</li>
                                <li>Suy nghĩ tích cực</li>
                                <li>Nói lời tạm biệt với sự căng thẳng và trở thành một người chăm sóc khách hàng chuyên nghiệp.</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <center>
                <button class="btn btn-register shadow-invert">ĐĂNG KÝ HỌC VÀ<br class="hidden-md-up"> NHẬN NGAY HỌC BỔNG</button>
            </center>
        </div>
    </section>
    <section id="license">
        <div class="container">
            <h2>Chứng chỉ<br><span>CBP CUSTOMER SERVICE</span><br>được cấp bởi tổ chức <img src="/mana/cbp/khoa-hoc-ky-nang-cham-soc-khach-hang-v2/img/icon-license.png" alt="" /></h2>
            <div class="row">
                <div class="col-lg-5">
                    <p>Chứng chỉ <b>CBP của IBTA</b> là một trong những chứng chỉ uy tín nhất về kỹ năng kinh doanh chuyên nghiệp và kỹ năng quản lý. <b>CBP</b> được công nhận trên toàn cầu từ <b>Mỹ, Canada, Caribe, châu Phi, Trung Đông, Trung Quốc, Ấn Độ, đến Singapore và vùng Viễn Đông.</b></p>
                    <br>
                    <p>Chứng chỉ CBP Customer Service chứng nhận cho kỹ năng chăm sóc khách hàng đạt chuẩn theo tiêu chuẩn quốc tế và được thừa nhận bởi các công ty lớn, tập đoàn đa quốc gia:</p>
                    <br>
                    <center>
                        <img src="/mana/cbp/khoa-hoc-ky-nang-cham-soc-khach-hang-v2/img/img-brand.png" alt="" />
                    </center>
                </div>
                <div class="col-lg-7">
                    <img src="/mana/cbp/khoa-hoc-ky-nang-cham-soc-khach-hang-v2/img/img-map.png" alt="" />
                </div>
            </div>
            <center>
                <button class="btn btn-register">ĐĂNG KÝ<br class="hidden-md-up"> NHẬN NGAY HỌC BỔNG</button>
            </center>
        </div>
    </section>
    <section id="proccess" path='scrolling'>
        <div class="container">
            <h2>Lộ trình học và giá trị của<br><span>CHỨNG CHỈ CBP</span></h2>
            <div class="row">
                <div class="col-lg-5">
                    <div class="item-left">
                        <img src="/mana/cbp/khoa-hoc-ky-nang-cham-soc-khach-hang-v2/img/img-process.png" alt="" />
                    </div>
                </div>
                <div class="col-lg-7">
                    <div class="item-right">
                        <h3>Sở hữu chứng chỉ <br>CBP CUSTOMER SERVICE</h3>
                        <ul>
                            <li class="icon-check-square-o">Bạn sẽ tự tin ứng tuyển vào các công ty, tập đoàn đa quốc gia, tập đoàn nước ngoài.</li>
                            <li class="icon-check-square-o">Bạn sẽ dễ dàng cho việc công tác tại các quốc gia khác ngoài Việt Nam.</li>
                            <li class="icon-check-square-o">Là chứng nhận cho kỹ năng kinh doanh chuyên nghiệp của bạn.</li>
                            <li class="icon-check-square-o">Là chứng chỉ có giá trị trên toàn cầu.</li>
                            <li class="icon-check-square-o">Tăng cơ hội thăng tiến trong công việc.</li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="item-bottom clearfix">
                        <div class="img">
                            <img alt="" src="/mana/cbp/khoa-hoc-ky-nang-cham-soc-khach-hang-v2/img/img-author.png" />
                        </div>
                        <div class="content">
                            <h3>Thạc sỹ NGUYỄN KIÊN TRÌ</h3>
                            <p>Là giảng viên sẽ đồng hành cùng bạn trong suốt khóa học này. Kinh nghiệm và kiến thức của Thạc sỹ trong lĩnh vực Sales và Chăm sóc khách hàng sẽ giúp bạn rất nhiều trong thời gian tới.</p>
                            <ul>
                                <li class="icon-square">MBA Tư vấn Quản lý Quốc tế - Thụy Sỹ</li>
                                <li class="icon-square">Expert Economic Certified - Đại học California</li>
                                <li class="icon-square">Giám Đốc Sales & Marketing Ha Long</li>
                                <li class="icon-square">Giám Đốc Sales Marketing Saigontourist - Liberty Group</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="register">
        <div class="container">
            <div class="box-title">
                <h2 class="title">Đăng ký khóa học
                    <br><span>NHẬN NGAY HỌC BỔNG TRỊ GIÁ 1.500.000Đ</span></h2>
            </div>
            <h2 style="margin-top: 30px;margin-bottom: 0;text-align: center">Học online cùng Mana Online Business School</h2>
            <form action="/course/page/submit" name="landing-page-id" id="landing-page-id" method="post" accept-charset="utf-8" class="row">
                <input type="hidden" id="advice_name" name="advice_name" value="Mana - Khóa học kỹ năng chăm sóc khách hàng V2" />
                 <input type="hidden" id="csrf" name="_csrf" />
                <div class="form-s clearfix">
                    <div class="form-group col-md-4 col-xs-12">
                        <div class="input">
                            <label>Họ tên</label>
                            <input type="text" class="form-control" id="fullname" name="fullname" value="" required="" data-errormessage="Họ tên" placeholder="Nhập họ và tên" >
                        </div>
                    </div>
                    <div class="form-group col-md-4 col-xs-12">
                        <div class="input">
                            <label>Email</label>
                            <input type="email" id="email" name="email" value="" required="" data-errormessage="Nhập chính xác email của bạn.." class="form-control" placeholder="Nhập email">
                        </div>
                    </div>
                    <div class="form-group col-md-4 col-xs-12">
                        <div class="input">
                            <label>Số ĐT</label>
                            <input type="text" class="form-control" name="phonenumber" id="phonenumber" value="" maxlength="50" required="" data-errormessage="Cung cấp sdt để được hỗ trợ tốt hơn trong quá trình thanh toán, tham gia học" placeholder="Nhập số điện thoại">
                        </div>
                    </div>
                </div>
                <center>
                    <button id="dang_ky_form" type="submit" class="btn shadow-invert btn_box_register button-regis">ĐĂNG KÝ<br class="hidden-md-up"> NHẬN NGAY HỌC BỔNG</button>
                    <p class="note">*Mọi thông tin của bạn sẽ được bảo mật hoàn toàn.
                        <br>Sau khi hoàn tất đăng ký, bộ phận CSKH sẽ gọi điện xác nhận và tư vấn kỹ hơn về khóa học với bạn.</p>
                </center>
            </form>
        </div>
    </section>
    <section id="hoi-dap" path="scrolling">
        <div class="container">
            <div class="fb-comments fb_iframe_widget fb_iframe_widget_fluid"
                data-href="https://mana.edu.vn/cbp/khoa-hoc-ky-nang-cham-soc-khach-hang-v2"
                data-width="100%" data-numposts="10"
                data-colorscheme="light" fb-xfbml-state="rendered">
             </div>
        </div>
    </section>
	<footer>
        <div class="container footer">
            <div class="row">
                <div class="col-md-3 logo">
                    <div class="footer-logo">
                        <ul>
                            <li>
                                <a href="#">
                                    <img src="/mana/cbp/khoa-hoc-ky-nang-cham-soc-khach-hang-v2/img/1_Logo.png" class="img-responsive"/>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <img src="/mana/cbp/khoa-hoc-ky-nang-cham-soc-khach-hang-v2/img/18_KynaLogo.png" class="img-responsive"/>
                                </a>
                            </li>
                        </ul>

                    </div>
                </div>
                <div class="col-md-6">
                    <div class="footer-text-center">
                        <h4>Công ty Cổ phần  Dream Việt Education</h4>
                        <p> <b class="company-address"> Trụ sở chính:</b>  Lầu 6 Tòa nhà Thịnh Phát - 178/8 Đường D1, Phường 25, Quận Bình Thạnh, TP Hồ Chí Minh</p>
                        <p> <b class="company-address"> Văn phòng Hà Nội:</b>  Tầng 6, Tòa Nhà Viện Công Nghệ - 25 Vũ Ngọc Phan, Phường Láng Hạ, Quận Đống Đa, TP Hà Nội </p>
                        <p style="padding-top: 20px">Giấy phép ĐKKD số 0313589030 do Sở Kế hoạch và Đầu tư TPHCM cấp</p>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="footer-hot-line">
                        <p><b class="company-address">Hotline:</b>  1900 6364 09</p>
                        <p><b class="company-address">Email: </b>hotro@kyna.vn</p>
                        <div class="fb-page" data-href="https://www.facebook.com/mana.edu.vn/" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/mana.edu.vn/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/mana.edu.vn/">MANA.edu.vn</a></blockquote></div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
	<div class="modal fade" id="modal">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-body">
					<div class="popup_body">
						<div class="alert alert-success" role="alert">Cám ơn bạn đã để lại thông tin. Mana.edu.vn sẽ sớm liên hệ với bạn để tư vấn thêm về chương trình học!</div>
					</div>
				</div>
			</div>
		</div>
	</div>
    <script src="/mana/cbp/khoa-hoc-ky-nang-cham-soc-khach-hang-v2/js/main.js"></script>

	<!-- Nhan them vao -->
	<script>
		$(document).ready(function () {
          $('#close-topbar').on('click', function () {
            $('#noel-topbar').remove();
            $('#menu').removeClass('has-topbar');
              $('#banner').css('marginTop', '0');
          });
          var today = new Date(),
              expiredDate = new Date('02/01/2017');
          if (today < expiredDate ){
            $('#menu').addClass('has-topbar');
            $('[data-xmas="false"]').each(function (idx, elm) {
              $(elm).hide();
            });
          } else{
            $('[data-xmas="true"]').each(function (idx, elm) {
              $(elm).remove();
              $('#noel-topbar').remove();
              $('#menu').removeClass('has-topbar');
            });
          }

		});
	</script>
	<!--End of Zopim Live Chat Script-->
    <!--Start of Zopim Live Chat Script-->
<script type="text/javascript">
    window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
        d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
    _.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
        $.src="//v2.zopim.com/?1bMWnWprkt76mXJXRb6xBuP2dAD7uxPq";z.t=+new Date;$.
            type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
</script>
<!--End of Zopim Live Chat Script-->

	<div id="fb-root"></div>
	<script type="text/javascript">
		(function(d, s, id) {
			var js, fjs = d.getElementsByTagName(s)[0];
			if (d.getElementById(id)) return;
			js = d.createElement(s); js.id = id;
			js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.0&appId=790788601060712";
			fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));
	</script>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-M92WKP"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<?php $this->endBody()?>
</body>
</html>
