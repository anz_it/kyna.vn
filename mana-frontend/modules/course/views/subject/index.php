<?php
$this->title = Yii::$app->params['siteTilePrefix'] . ' - Chuyên ngành';
?>
    <header>
        <div class="header">
            <div class="container">
                <div class="row categories-courses-header">
                    <div class="col-md-offset-3 col-md-9">
                        <h3>Chương trình học</h3>
                        <div>
                            Chuyên ngành
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </header>
    <div class="wrapper categories">
        <div class="container">
            <div class="col-md-3">
                <div class="side-bar-menu">
                    <ul>
                        <li class="active"><a href="javascript:void(0)">Chuyên ngành</a></li>
                        <li><a href="/he-dao-tao">Hệ đào tạo</a></li>
                        <li><a href="/chung-chi">Chứng chỉ</a></li>
                        <li><a href="/to-chuc-cap-chung-chi">Tổ chức cấp chứng chỉ</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-9">
                <div class="categories-main">

                    <div class="courses-list">
                        <div class="panel-group" id="accordion">

                            <?php

                            if ($listData) {
                                foreach ($listData as $item) {
                                    if (count($item->courseSubjects) > 0) {
                                        ?>
                                        <div class="panel panel-default panel-categories categories-item">
                                            <div class="panel-heading">
                                                <div class="row">
                                                    <div class="col-md-3 text-right image-holder">
                                                        <img src="<?= $item->image_url ?>" width="150px">
                                                    </div>
                                                    <div class="col-md-9">
                                                        <div class="row">
                                                            <div class="col-md-6 col-sm-6">
                                                                <h4 class="panel-title" data-id="<?=$item->id?>">
                                                                    <?php if ($item->children) { ?>
                                                                        <a class="accordion-toggle"
                                                                           data-toggle="collapse"
                                                                           data-parent="#accordion"
                                                                           href="#collapse<?= $item->id ?>">
                                                                            <?= $item->name ?>
                                                                        </a>
                                                                    <?php } else { ?>
                                                                        <a class="" data-toggle="collapse"
                                                                           data-parent="#accordion" href="#collapseTwo">
                                                                            <?= $item->name ?>
                                                                        </a>
                                                                    <?php } ?>
                                                                </h4>
                                                            </div>
                                                            <div
                                                                class="col-md-offset-3 col-md-3 text-right col-sm-6">
                                                                <a class="btn btn-see-all"
                                                                   href="/chuyen-nganh/danh-sach-khoa-hoc/<?= $item->slug ?>"
                                                                   data-hover="Xem tất cả"><span><?= count($item->courseSubjects) ?>
                                                                        khóa học</span></a>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-12 intro"><?= $item->description ?></div>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>

                                                </div>
                                            </div>
                                            <?php if ($item->children) { ?>
                                                <div id="collapse<?= $item->id ?>" class="panel-collapse collapse">
                                                    <div class="panel-body">
                                                        <ul class="list-item">
                                                            <?php foreach ($item->children as $child) { ?>
                                                                <li class="checkbox">
                                                                    <a href="/chuyen-nganh/danh-sach-khoa-hoc/<?= $child->slug ?>"><?= $child->name ?></a>
                                                                </li>
                                                            <?php } ?>
                                                        </ul>
                                                    </div>
                                                </div>
                                            <?php } ?>

                                        </div>
                                        <?php
                                    }
                                }
                            }
                            ?>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?= \mana\widgets\RegisterWidget::widget() ?>