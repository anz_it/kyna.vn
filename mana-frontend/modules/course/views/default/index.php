<?php
    $this->title = "Mana.edu.vn - Danh sách khóa học";
?>
<header>
    <div class="header">
        <div class="container">
            <div class="row categories-courses-header">
                <div class="col-md-2 text-right image-holder">
                    <img src="http://placehold.it/400x400">
                </div>
                <div class="col-md-10">
                    <h3>Tìm kiếm</h3>
                    <div class="">
                        Danh sách khóa học
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
<div class="wrapper categories-courses">
    <div class="container">
        <div class="col-md-4">
            <div class="side-bar-menu">
                <div class="panel-group" id="accordion">

                    <div class="panel panel-default panel-categories">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#subjects">
                                    Chuyên ngành
                                </a>
                            </h4>
                        </div>
                        <div id="subjects" class="panel-collapse collapse in">
                            <div class="panel-body">
                                <ul class="list-item">
                                    <?php

                                        if ($listSubjects) {
                                           ?>
                                            <?php foreach ($listSubjects as $item) { ?>
                                                <?php
                                                if ($item->children) {
                                                    ?>
                                                    <li class="checkbox">
                                                        <input id="box-subject-<?=$item->id?>" type="checkbox" class="checkbox-search" data-model="subjects"  value="<?=$item->id?>" />
                                                        <label id="parent-<?=$item->id?>"><?=$item->name?></label>
                                                        <ul>
                                                            <?php
                                                            foreach ($item->children as $itemChild) {
                                                                ?>
                                                                <li class="checkbox">
                                                                    <input id="box-subject-<?=$itemChild->id?>" type="checkbox" class="checkbox-search checkbox-child" data-parent-id="parent-<?=$item->id?>" data-model="subjects"  value="<?=$itemChild->id?>" />
                                                                    <label for="box-subject-<?=$itemChild->id?>"><?=$itemChild->name?></label>
                                                                </li>
                                                                <?php
                                                            }
                                                            ?>
                                                        </ul>
                                                    </li>
                                                    <?php
                                                } else {
                                                    ?>
                                                    <li class="checkbox">
                                                        <input id="box-subject-<?=$item->id?>" type="checkbox" class="checkbox-search" data-model="subjects"  value="<?=$item->id?>" />
                                                        <label for="box-subject-<?=$item->id?>"><?=$item->name?></label>
                                                    </li>
                                                    <?php
                                                }
                                                ?>

                                            <?php } ?>
                                    <?php
                                        }
                                    ?>

                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default panel-categories">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#educations">
                                    Hệ đào tạo
                                </a>
                            </h4>
                        </div>
                        <div id="educations" class="panel-collapse collapse">
                            <div class="panel-body">
                                <ul class="list-item">
                                    <?php
                                        if ($listEducations) {
                                            ?>
                                            <?php foreach ($listEducations as $item) { ?>
                                                <li class="checkbox">
                                                    <input id="box-education-<?=$item->id?>" type="checkbox"  class="checkbox-search" data-model="educations" value="<?=$item->id?>"  />
                                                    <label for="box-education-<?=$item->id?>"><?=$item->name?></label>
                                                </li>
                                            <?php } ?>
                                    <?php } ?>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default panel-categories">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#certificates">
                                    Chứng chỉ
                                </a>
                            </h4>
                        </div>
                        <div id="certificates" class="panel-collapse collapse">
                            <div class="panel-body">
                                <ul class="list-item">
                                    <?php
                                        if ($listCertificates) {
                                            ?>
                                            <?php foreach ($listCertificates as $item) { ?>
                                                <li class="checkbox">
                                                    <input id="box-certificate-<?=$item->id?>" type="checkbox"  class="checkbox-search" data-model="certificates" value="<?=$item->id?>" />
                                                    <label for="box-certificate-<?=$item->id?>"><?=$item->name?></label>
                                                </li>
                                            <?php } ?>
                                    <?php
                                        }
                                    ?>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default panel-categories">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#organizations">
                                    Tổ chức cấp chứng chỉ
                                </a>
                            </h4>
                        </div>
                        <div id="organizations" class="panel-collapse collapse">
                            <div class="panel-body">
                                <ul class="list-item">
                                    <?php
                                        if ($listOrganizations) {
                                            ?>
                                            <?php foreach ($listOrganizations as $item) { ?>
                                                <li class="checkbox">
                                                    <input id="box-organization-<?=$item->id?>" type="checkbox" class="checkbox-search" data-model="organizations" value="<?=$item->id?>" />
                                                    <label for="box-organization-<?=$item->id?>"><?=$item->name?></label>
                                                </li>
                                            <?php } ?>
                                    <?php
                                        }
                                    ?>
                                </ul>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="col-md-8">
            <div class="categories-courses-main">
                <div class="row header">
                    <div class="col-md-3 count text-left"><span class="number-count"><?=$dataProvider->totalCount?></span> khóa học</div>
                    <div class="col-md-9 text-right form-search">
                        <form class="form-inline">
                            <div class="row">
                                <label class="col-md-offset-3 col-md-3">Sắp xếp theo</label>
                                <div class="col-md-6 custom-select-box">
                                    <?= \yii\helpers\Html::dropDownList('list', $searchModel->orderByItem, \kyna\mana\models\Course::listOrders(), ['class'=>'form-control', 'id'=>'select_sortBy']) ?>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

                <div class="row courses-list">
                    <?php if ($dataProvider->totalCount) { ?>
                        <?php foreach ($dataProvider->getModels() as $course) { ?>
                            <?php if ($course->kynaCourse && $course->organization) { ?>
                                    <div class="course-item">
                                        <div class="col-md-3 text-right image-holder">
                                            <a href="/khoa-hoc/<?=$course->kynaCourse->slug?>">
                                                <img src="<?=$course->kynaCourse->image_url?>" style="width: 100%">
                                            </a>
                                        </div>
                                        <div class="col-md-9">
                                            <div class="quick-description">
                                                <h3><a href="/khoa-hoc/<?=$course->kynaCourse->slug?>"><?=$course->kynaCourse->name?></a></h3>
                                                <p>
                                                    <?=$course->kynaCourse->description?>
                                                </p>
                                            </div>
                                            <div class="created-by">
                                                <div class="col-md-1 col-xs-2 avatar">
                                                    <img src="<?=$course->organization->image_url?>" style="width: 100%">
                                                </div>
                                                <div class="col-md-9 col-xs-10 name">
                                                    <a href="#"><?=$course->organization->name?></a>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                <?php } ?>
                        <?php } ?>
                    <?php  } ?>


                </div>
            </div>
        </div>
    </div>
</div>
<?= \mana\widgets\RegisterWidget::widget(['forHome' => false]) ?>
<script type="application/javascript">
    var categorySlug = "chuyen-nganh";
</script>