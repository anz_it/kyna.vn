<?php
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;

$this->title = Yii::$app->params['siteTilePrefix'] . ' - ' . $course->kynaCourse->name;
?>
    <div class="wrapper course-detail">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <ul class="breadcrumb">
                        <?php if (isset($category) && !empty($category)) { ?>

                            <li><a href="#"><?= $category->parent_name ?></a></li>
                            <li><a href="#"><?= $category->name ?></a></li>
                        <?php } ?>
                        <li class="active"><?= $course->kynaCourse->name ?></li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col-md-8">
                    <!--                <div class="video-intro">-->
                    <!---->
                    <!--                </div>-->
                    <?php if ($course->kynaCourse->video_url) { ?>
                        <iframe style="width: 100%; height: 425px;" src="<?= $course->kynaCourse->youtubeEmbedUrl ?>"
                                frameborder="0" allowfullscreen></iframe>
                    <?php } else if ($course->kynaCourse->video_cover_image_url) { ?>
                        <div class="video-intro">
                            <img src="<?= $course->kynaCourse->video_cover_image_url ?>" class="img-responsive"
                                 style="width: 100%;">
                        </div>
                    <?php } ?>
                    <div class="detail">
                        <div class="title">
                            <h3>Tổng quan chương trình</h3>
                        </div>
                        <div class="description">
                            <div class="row text-intro">
                                <div class="col-md-4 col-sm-4">
                                    Tên chương trình
                                </div>
                                <div class="col-md-8 col-sm-8">
                                    <strong>
                                        <?= $course->kynaCourse->name ?>
                                    </strong>
                                </div>
                            </div>
                            <div class="row text-intro">
                                <div class="col-md-4 col-sm-4">
                                    Bằng/chứng chỉ
                                </div>
                                <div class="col-md-8 col-sm-8">
                                    <strong>
                                        <?= $course->certificate->name ?>
                                    </strong>
                                </div>
                            </div>
                            <div class="row text-intro">
                                <div class="col-md-4 col-sm-4">
                                    Đơn vị cung cấp
                                </div>
                                <div class="col-md-8 col-sm-8">
                                    <strong>
                                        <a href="/to-chuc-cap-chung-chi/danh-sach-khoa-hoc/<?= $course->organization->slug ?>"><?= $course->organization->name ?></a>
                                    </strong>
                                </div>
                            </div>
                            <div class="row text-intro">
                                <div class="col-md-4 col-sm-4">
                                    Thời lượng
                                </div>
                                <div class="col-md-8 col-sm-8">
                                    <strong>
                                        <?= $course->learning_time ?>
                                    </strong>
                                </div>
                            </div>

                            <div class="row text-intro hidden">
                                <div class="col-md-4 col-sm-4">
                                    Hình thức thanh toán
                                </div>
                                <div class="col-md-8 col-sm-8">
                                    <strong>
                                        Chuyển khoản
                                    </strong>
                                </div>
                            </div>

                            <div class="row text-intro">
                                <div class="col-md-4 col-sm-4">
                                    Thời gian
                                </div>
                                <div class="col-md-8 col-sm-8">
                                    <strong>
                                        <?= $course->learning_start_date ?>
                                    </strong>
                                </div>
                            </div>

                        </div>
                    </div>

                    <div class="detail">
                        <div class="title">
                            <h3>Bằng cấp</h3>
                        </div>
                        <div class="description">
                            <div class="row">
                                <div class="col-md-5 image-holder">
                                    <img src="<?= $course->certificate->image_url ?>">
                                </div>
                                <div class="col-md-7">
                                    <h3><?= $course->certificate->name ?></h3>
                                    <span><?= $course->certificate->description ?></span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="detail">
                        <div class="title">
                            <h3>Mục tiêu và nội dung</h3>
                        </div>
                        <div class="description">
                            <?= $course->kynaCourse->overview ?>
                        </div>
                        <!--                        --><?php
                        //                        echo "<pre>";var_dump($course->kynaCourse->sections);
                        if ($course->kynaCourse->sections) { ?>
                            <div class="description course-content">
                                <div class="header">
                                    <strong>Nội dung: </strong><?= $course->kynaCourse->name ?>
                                </div>
                                <div class="row">
                                    <div class="panel-group" id="accordion">


                                        <div class="panel panel-default panel-categories">
                                            <?php
                                            foreach ($course->kynaCourse->sections as $section) {

                                                if ($section->lvl == 0) {
                                                    ?>
                                                    <div class="panel-heading">
                                                        <h4 class="panel-title">
                                                            <a class="accordion-toggle" data-toggle="collapse"
                                                               data-parent="#accordion"
                                                               href="#collapse-section-<?= $section->id ?>">
                                                                <?= $section->name ?>
                                                            </a>
                                                        </h4>
                                                    </div>
                                                    <div id="collapse-section-<?= $section->id ?>"
                                                         class="panel-collapse collapse">
                                                        <div class="panel-body">
                                                            <ul class="list-item">
                                                                <?php
                                                                foreach ($course->kynaCourse->sections as $sectionChild) {
                                                                    if ($sectionChild->root == $section->id) {
                                                                        ?>
                                                                        <li>
                                                                            <a class="item" href="#">
                                                                                    <span class="col-md-10 col-xs-10">
                                                                                        <i class="fa fa-play-circle-o"
                                                                                           aria-hidden="true"></i> <?= $sectionChild->name ?>
                                                                                    </span>
                                                                                <span
                                                                                    class="col-md-2 text-right col-xs-2"></span>
                                                                                <span class="clearfix"></span>
                                                                            </a>
                                                                        </li>
                                                                        <?php
                                                                    }
                                                                }
                                                                ?>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                    <?php if ($course->getTeachers()->all()) { ?>
                        <div class="detail">
                            <div class="title">
                                <h3>Giảng viên</h3>
                            </div>
                            <div class="description teachers">

                                <?php foreach ($course->getTeachers()->all() as $teacher) { ?>
                                    <div class="row item">
                                        <div class="col-md-3">
                                            <img src="<?= $teacher->image_url ?>" class="img-responsive"/>
                                        </div>
                                        <div class="col-md-9">
                                            <h3><?= $teacher->name ?></h3>
                                            <h5 class="font_16px" style="font-style: italic"><?= $teacher->title ?></h5>
                                            <p><?= $teacher->description ?></p>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    <?php } ?>

                    <div class="detail">
                        <div class="title">
                            <h3>Khóa học phù hợp với</h3>
                        </div>
                        <div class="description">
                            <?= $course->description_for ?>
                        </div>
                    </div>

                    <div class="detail">
                        <div class="title">
                            <h3>Tiêu chuẩn đầu vào và tiêu chí tốt nghiệp</h3>
                        </div>
                        <div class="description">
                            <?= $course->condition ?>
                        </div>
                    </div>

                    <div class="detail">
                        <div class="title">
                            <h3>Hình thức thanh toán</h3>
                        </div>
                        <div class="description">
                            <h4>Thanh toán bằng hai cách:</h4>
                            <ul>
                                <li>Chuyển khoản qua số tài khoản <span class="text-bold">0531 0025 11245</span></li>
                                <li>
                                    Chủ tài khoản: Công ty cổ phần Dream Viet Education<br/>
                                    Ngân hàng: Ngân hàng Vietcombank, chi nhánh Đông Sài Gòn, TP.HCM<br/>
                                    <span class="text-bold">Ghi chú khi chuyển khoản:</span> Trong mục "Ghi chú" khi
                                    chuyển khoản, bạn vui lòng ghi rõ: Số điện thoại + Họ Tên + Email đăng ký học + Khóa
                                    học đăng ký
                                </li>

                                <li>
                                    Đăng ký trực tiếp tại văn phòng công ty cổ phần Dream Việt Education:
                                </li>

                                <li>
                                    <span class="text-bold">Trụ sở tại TP.HCM:</span><br/>
                                    Lầu G, toà nhà Thịnh Phát – 178/8 đường D1, Phường 25, Quận Bình Thạnh, TP.HCM
                                </li>

                                <li>
                                    <span class="text-bold">Trụ sở tại Hà Nội:</span><br/>
                                    Tầng 6, tòa nhà Viện Công nghệ - 25 Vũ Ngọc Phan, Phường Láng Hạ, TP. Hà Nội
                                </li>
                            </ul>


                        </div>
                    </div>
                    <?php if ($listCourses) { ?>

                        <div class="detail categories-courses-main">
                            <div class="title">
                                <h3>Khóa học liên quan</h3>
                            </div>
                            <div class="courses-list">
                                <?php
                                foreach ($listCourses as $item) {
                                    ?>
                                    <div class="course-item">
                                        <div class="col-md-3 text-right image-holder">
                                            <a href="/khoa-hoc/<?= $item->kynaCourse->name ?>">
                                                <img src="<?= $item->kynaCourse->image_url ?>" style="width: 100%">
                                            </a>
                                        </div>
                                        <div class="col-md-9">
                                            <div class="quick-description">
                                                <h3>
                                                    <a href="/khoa-hoc/<?= $item->kynaCourse->slug ?>"><?= $item->kynaCourse->name ?></a>
                                                </h3>
                                                <p>
                                                    <?= $item->kynaCourse->description ?>
                                                </p>
                                            </div>
                                            <div class="created-by">
                                                <div class="col-md-1 col-xs-2 avatar">
                                                    <img src="<?= $item->organization->image_url ?>"
                                                         style="width: 100%">
                                                </div>
                                                <div class="col-md-9 col-xs-10 name">
                                                    <a href="#"><?= $item->organization->name ?></a>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <?php
                                }

                                ?>
                            </div>
                        </div>
                    <?php } ?>
                </div>
                <div class="col-md-4 register-relative">
                    <div class="register-info">
                        <div class="col-md-12">
                            <div class="text-center title">Đăng ký nhận tư vấn</div>
                        </div>
                        <?php $form = ActiveForm::begin(['action' => \yii\helpers\Url::toRoute(['/user/contact/index'])]); ?>
                        <?= $form->field($model, 'name')->textInput(['maxlength' => true, 'placeholder' => 'Họ và tên'])->label(false) ?>

                        <?= $form->field($model, 'phone')->textInput(['maxlength' => true, 'placeholder' => $model->getAttributeLabel('phone')])->label(false) ?>

                        <?= $form->field($model, 'email')->textInput(['maxlength' => true, 'placeholder' => $model->getAttributeLabel('email')])->label(false) ?>

                        <div class="custom-select-box">

                            <?= $form->field($model, 'subject_id')->dropDownList(ArrayHelper::map($listSubjects, 'id', 'name'), ['prompt' => 'Chuyên ngành quan tâm'])->label(false) ?>

                        </div>

                        <div class="custom-select-box">
                            <?= $form->field($model, 'education_id')->dropDownList(ArrayHelper::map($listEducations, 'id', 'name'), ['prompt' => $model->getAttributeLabel('education_id')])->label(false) ?>
                        </div>

                        <div class="custom-select-box">
                            <?= $form->field($model, 'certificate_id')->dropDownList(ArrayHelper::map($listCertificates, 'id', 'name'), ['prompt' => $model->getAttributeLabel('certificate_id')])->label(false) ?>
                        </div>

                        <div class="col-md-12 text-center">
                            <?= \yii\helpers\Html::submitButton('Đăng ký', ['class' => 'btn btn-default btn-register btn-lg']) ?>
                        </div>

                        <div class="col-md-12 text-center hotline">
                            <span>Hotline <strong>1900 6364 09</strong></span>
                        </div>
                        <!--                        </div>-->
                        <?php ActiveForm::end(); ?>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>

    <script type='text/javascript'>
        $(window).bind('scroll', function () {

            if ($(window).scrollTop() > 280) {
                $('.register-relative').addClass('register-scroll');


            } else {
                $('.register-relative').removeClass('register-scroll');
            }

            if ($(window).scrollTop() > ($('.course-detail').height() - 400) && $(window).scrollTop() < ($('.course-detail').height() + 400)) {
                $('.register-relative').addClass('register-scroll-on-bottom');
            } else {
                $('.register-relative').removeClass('register-scroll-on-bottom');
            }
        });
        console.log($('.course-detail').height());
    </script>

    <script src="https://apis.google.com/js/platform.js" async defer>{
            lang: 'vi'
        }</script>
    <script type="text/javascript" src='/media/jwplayer/jwplayer_v611/jwplayer.js'></script>
    <!--    --><?php //if ($course->UrlVideo) { ?>
    <!--        <script type="text/javascript">-->
    <!--            $(document).ready(function() {-->
    <!--                jwplayer.key = "5XXb+w0txH2+cnkwOtAOWXU39zFQbZ6VT9mOA6R83tk="; //"N8zhkmYvvRwOhz4aTGkySoEri4x+9pQwR7GHIQ==";-->
    <!--                jwplayer("intro_player").setup({-->
    <!--                    file: "--><?php //echo $course->UrlVideo ?>//",
    //                    image: '<?php //echo $course->video_cover_image ?>//',
    //                    height: '100%', width: '100%'
    //                });
    //            });
    //        </script>
    //    <?php //  } ?>