<?php
$this->title = Yii::$app->params['siteTilePrefix']. " - Giới thiệu";
?>
<header>
    <div class="header">
        <div class="container">
            <div class="row about">
                <div class="col-md-12 text-center" style="top:119px; margin-bottom: 90px;height: 279px;">
                    <a href="#"><img src="/mana/images/mana_white.png" class="header-logo col-md-offset-3 col-md-6"></a>
                </div>
                <div class="col-md-12 text-center">
                    <!--                    <div class="header-text">-->
                    <!--                        Viện đào tạo quản trị kinh trực tuyến hàng đầu-->
                    <!--                    </div>-->
                    <div class="col-md-offset-2 col-md-8 text-intro">
                        <span class="text-bold">Mana Online Business School</span> là đơn vị đào tạo các chương trình Quốc tế tại Việt Nam. Với các chương trình đào tạo đa ngành trên nàng tảng trực tuyến, tập trung vào các nhóm ngành khoa học ứng dụng như : Marketing, Quản trị kinh doanh, Quản trị nhà hàng khách sạn, Quản trị dự án và vận hành, Tài chính ...và hàng trăm các khóa học kỹ năng mềm cần thiết giúp làm việc hiệu quả, thăng tiến sự nghiệp trong tương lai
                    </div>

                </div>

            </div>
        </div>
    </div>
    <div class="clearfix"></div>
</header>
<div class="about">
    <div class="wrap main">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-sm-6">
                    <img src="/mana/images/about_item_1.jpg" class="img-responsive" />
                </div>
                <div class="col-md-5 col-sm-6 text">
                    <h5>Học tại <span class="text-bold">Mana Online Business School:</span></h5>
                    <ul>
                        <li><span>1</span>Sẽ có cố vấn học tập 1-1</li>
                        <li><span>2</span>Cơ hội ứng dụng kiến thức đã học vào thực tế</li>
                        <li><span>3</span>Hoàn toàn có thể làm chủ thời gian và tốc độ học</li>
                        <li><span>4</span>Tiết kiệm chi phí</li>
                    </ul>

                    <div class="big-text">
                        Sứ mệnh của <span class="text-bold">Mana Online Business School</span> là sử dụng tri thức và giáo dục để xây dựng nguồn nhân lực mang tầm vóc quốc tế, sẵn sàng cho tốc độ hội nhập và phát triển nhanh chóng.
                    </div>
                </div>
            </div>

        </div>
    </div>

    <div class="wrap bottom">
        <div class="container">
            <div class="row">
                <div class="col-md-5 text">
                    Hiện nay <span class="text-bold">Mana Online Business School</span> đang triển khai các chương trình học nhận chứng chỉ, chứng nhận trong nước và quốc tế như : Chứng chỉ Kinh doanh Chuyên nghiệp của Hiệp hội Kinh doanh Quốc tế, Hoa Kỳ (IBTA - International Business Training Association),MBA ,Đại Học, Cao Đẳng, Chứng nhận đào tạo chuyên nghiệp được cấp bởi các trường, trung tâm đào tạo uy tín tại US & UK.
                </div>
                <div class="col-md-offset-1 col-md-6">
                    <div class="row">
                        <div class="col-md-4 item">
                            <img src="/mana/images/logo/ibta.png" class="img-responsive" />
                        </div>
                        <div class="col-md-4 item">
                            <img src="https://c2.staticflickr.com/8/7514/27569525734_1aede8ecae_o.png" class="img-responsive" />
                        </div>
                        <div class="col-md-4 item">
                            <img src="/mana/images/logo_v5.png" class="img-responsive" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>