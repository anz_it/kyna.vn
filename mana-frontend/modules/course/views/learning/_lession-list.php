<?php
use yii\widgets\ListView;
?>

<?= ListView::widget([
    'dataProvider' => $lessionDataProvider,
    'layout' => '{items}',
    'itemOptions' => [
        'tag' => false,
    ],
    'options' => [
        'class' => 'nav lesson-list-sub',
        'tag' => 'ul',
    ],
    'itemView' => function ($model, $key, $index, $widget) use ($finishedLessons, &$number) {
        if (in_array($model->id, $finishedLessons)) {
            $finished = true;
        }
        else {
            $finished = false;
        }
        if($model->type == 'video'){
            $number++;
        }
        return $this->render('_lession-list-item',[
            'model' => $model,
            'index' => $index,
            'number' => $number,
            'finished' => $finished
        ]);
    },
]) ?>
