<?php
use app\modules\course\components\TreeView;
use yii\helpers\Url;
?>
<?= TreeView::widget([
    'query' => $course->getSectionTreeQuery(),
    'linkTemplate' => Url::toRoute(['/course/learning/', 'id' => $course->id, 'section' => '{nodeId}']),
    'currentNodeId' => $sectionId,
    'wrapperOptions' => [
        'class' => 'tab-pane clearfix tab-menu-content',
        'role' => 'tabpanel'
    ],
    'treeOptions' => [
        'class' => 'nav-tabs nav-tabs-product clearfix',
        'role' => 'tablist'
    ],
]) ?>
