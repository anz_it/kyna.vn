<?php
use kyna\course\models\QuizSession;
use yii\bootstrap\Tabs;
use yii\bootstrap\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

$this->title = "Giáo trình khóa học - {$course->name}";

?>
<div class="header-nav">
    <div class="container">
        <ul class="wrap clearfix">
            <li class="title"><h4><?= $course->name ?></h4></li>
            <li class="feature">
                <div class="sub">
                    <div class="template">
                        <span>Xem kiểu</span>
                        <a href="#" class="first" data-template="template-col-two"><span></span><span></span></a>
                        <a href="#" class="last" data-template="template-full"><span></span></a>
                    </div>
                    <div class="note">                                                      
                       <div class="wrap">                              
                            <a href="#" class="hover-color-green dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Ghi chú</a>
                            <div class="dropdown-menu">                                            
                              <div class="wrap-input clearfix">
                                  <span>Nhập câu hỏi của bạn và nhấn Enter để gửi</span>
                                  <textarea class="form-control" rows="3"></textarea>
                              </div>                                                                                       
                            </div>                                                                                                                                                                                                                        
                        </div><!--end .wrap-->
                    </div><!--end .note-->
                    
                    <div class="question">                           
                        <div class="wrap">                              
                            <a href="#" class="hover-color-green dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-question-circle" aria-hidden="true"></i> Đặt câu hỏi</a>
                            <div class="dropdown-menu">                                            
                              <div class="wrap-input clearfix">
                                  <span>Nhập câu hỏi của bạn và nhấn Enter để gửi</span>
                                  <textarea class="form-control" rows="3"></textarea>
                              </div>                                                                                       
                            </div>                                                                                                                                                                                                                        
                        </div><!--end .wrap-->
                    </div><!--end .question-->
<!--                    <div class="switch-language">-->
<!--                        <a class="item" href="#"><img src="/mana/images/vi.png"></a>-->
<!--                        <a class="item" href="#"><img src="/mana/images/en.png"></a>-->
<!--                    </div>-->
                </div>
            </li>
        </ul>

    </div><!--end .container-->
</div><!--end .header-nav-->
<!--
       <ul class="wrap clearfix">
          <li class="title">
             <h4><?= $course->name ?></h4>
          </li>
          <li class="feature">
             <div class="sub">
                <div class="template">
                   <span>Xem kiểu</span>
                   <a href="#" class="first" data-template="template-col-two"><span></span><span></span></a>
                   <a href="#" class="last" data-template="template-full"><span></span></a>
                </div>
                <div class="note">                                                      
                   <div class="wrap">                              
                        <a href="#" class="hover-color-green dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Ghi chú</a>
                        <div class="dropdown-menu">                                            
                          <div class="wrap-input clearfix">
                              <span>Nhập câu hỏi của bạn và nhấn Enter để gửi</span>
                              <textarea class="form-control" rows="3"></textarea>
                          </div>                                                                                       
                        </div>                                                                                                                                                                                                                        
                    </div>
                </div>
                
                <div class="question">                           
                    <div class="wrap">                              
                        <a href="#" class="hover-color-green dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-question-circle" aria-hidden="true"></i> Đặt câu hỏi</a>
                        <div class="dropdown-menu">                                            
                          <div class="wrap-input clearfix">
                              <span>Nhập câu hỏi của bạn và nhấn Enter để gửi</span>
                              <textarea class="form-control" rows="3"></textarea>
                          </div>                                                                                       
                        </div>                                                                                                                                                                                                                        
                    </div>
                </div>
             </div>
             
            
          </li>
       </ul>               
    </div>

</div> 
-->      
<!--end .header-nav-->
<div class="wrap-video">
    <div class="container">

        <div class="col-md-4 col-sm-5 col-xs-12 left zoomIn animated">

          <!-- Nav tabs -->
          <ul class="nav nav-tabs" id="aside-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#lesson-tab-content" id="aside-section-tab" aria-controls="lesson-tab-content" role="tab" data-toggle="tab">Nội dung</a></li>
            <li role="presentation"><a href="#lesson-tab-notes" id="aside-note-tab" aria-controls="lesson-tab-notes" role="tab" data-toggle="tab">Ghi chú</a></li>
          </ul>

          <!-- Tab panes -->
          <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="lesson-tab-content">
                <?= $this->render('_sections', [
                    'course' => $course,
                    'sectionId' => $sectionId,
                ])?>
            </div><!--end #lesson-tab-content-->
            <div role="tabpanel" class="tab-pane" id="lesson-tab-notes" data-id="<?= $lession->id ?>">

            </div><!--end #lesson-tab-notes-->
            <div class="wrap-input">
                <?= Html::beginForm(Url::toRoute('/course/course-note/add'), 'post', [
                    'id' => 'add-note-form',
                ]) ?>
                    <?= Html::hiddenInput('id', $lession->id); ?>
                    <?= Html::hiddenInput('t', 0, [
                        'id' => 'playback-time'
                    ]); ?>
                    <?= Html::textarea('note', '', [
                        'id' => 'note-input',
                        'placeholder' => 'Nhập ghi chú và Enter để lưu lại',
                        'class' => 'form-control'
                    ]) ?>
                <?php Html::endForm(); ?>
            </div><!--end .wrap-input-->
          </div><!--end .tab-content-->

        </div><!--end .left-->

        <div class="tab-content tab-content-video clearfix col-md-8 col-sm-7 col-xs-12 right pd0 zoomIn animated">
            <div role="tabpanel" class="tab-pane clearfix tab-parent-content active" id="lesson-video-1">
                <div class="tab-content tab-content-sub active clearfix">
                    <?php $number = 0; ?>
                    <?= $this->render('_lession-list', [
                        'lessionDataProvider' => $lessionDataProvider,
                        'finishedLessons' => $finishedLessons,
                        'number' => $number
                    ])?>
                    <?php if($lession->getCanLearnByDate($user->id) || $lession->getIsQuick($user->id)) { ?>
                        <?php if ($lession->isQuiz) { ?>
                            <?= $this->render('_quiz', [
                                'session' => $lession->quiz->getSession($user->id),
                                'quiz' => $lession->quiz,
                            ]) ?>
                        <?php }else { ?>
                            <?= $this->render('_video', [
                                'lession' => $lession,
                            ]) ?>
                        <?php } ?>
                    <?php } else { ?>
                        <?= $this->render('_alert', [
                            'course' => $lession->course,
                            'lession'   => $lession,
                            'user_id' => $user->id
                        ]); ?>
                    <?php } ?>
                </div>
            </div><!--end #lesson-video-1-->

        </div><!--end .tab-content-wrap-->
    </div><!--end .container-->
</div><!--end .wrap-video-->

<div class="wrap-content">
    <div class="container">
        <div class="col-md-4 col-sm-5 col-xs-12 left">
            <div class="wrap-menu-content zoomIn animated none">
                <h3>Nội dung</h3>
                <div role="tabpanel" class="tab-pane clearfix tab-menu-content">
                    <?= $this->render('_sections', [
                        'course' => $course,
                        'sectionId' => $sectionId,
                    ])?>
                </div><!--end .tab-menu-content-->
            </div><!--end .wrap-menu-content-->
            <div class="wrap-support">
                <div class="wrap-call">
                    <ul>
                        <li><i class="fa fa-phone-square" aria-hidden="true"></i></li>
                        <li>
                            <h4>Gọi <span class="color-green">1900 6364 09</span></h4>
                            <p>Cần hỗ trợ, hướng dẫn khi học</p>
                        </li>
                    </ul>
                </div><!--end .wrap-call-->
                <span class="feedback"><a href="javascript:void(0)" data-toggle="modal" data-target="#lesson-feedback"><i class="fa fa-book" aria-hidden="true"></i> Góp ý cho khóa học</a></span>
                     <!-- Modal Popup Góp Ý -->
                     <div class="modal fade" id="lesson-feedback" tabindex="-1" role="dialog">
                        <div class="modal-dialog" role="document">
                           <div class="modal-content wrap-popup-tu-van">
                              <div class="modal-header">
                                 <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                                 <img src="/img/lesson/icon-popup.png" class="img-responsive"/>
                                 <h4 class="title font-roboto">Khảo sát sự hài lòng của học viên với khóa học</h4>
                              </div>
                              <!--end .modal-header-->
                              <div class="content">
                                 <p>Chào bạn, ý kiến đóng góp của bạn là nguồn nguyên liệu quý giá để Kyna.vn và các giảng viên hoàn thiện khóa học. Hãy giúp chúng tôi phát triển hơn nữa những khóa học chất lượng và bổ ích nhé.</p>
                                 <ul>
                                    <li>
                                       <h4>1. Đánh giá của bạn về chất lượng nội dung bài học</h4>
                                    </li>
                                    <li>
                                       <span>Rất không hữu ích</span>
                                       <span><a href="#">1</a></span>
                                       <span><a href="#" class="active">2</a></span>
                                       <span><a href="#">3</a></span>
                                       <span><a href="#">4</a></span>
                                       <span><a href="#">5</a></span>
                                       <span>Rất hứu ích</span>
                                    </li>
                                    <li>
                                       <h4>2. Đánh giá của bạn về chất lượng âm thanh của khóa học</h4>
                                    </li>
                                    <li>
                                       <span>Rất tệ</span>
                                       <span><a href="#">1</a></span>
                                       <span><a href="#" class="active">2</a></span>
                                       <span><a href="#">3</a></span>
                                       <span><a href="#">4</a></span>
                                       <span><a href="#">5</a></span>
                                       <span>Rất tốt</span>
                                    </li>
                                    <li>
                                       <h4>3. Đánh giá của bạn về hình ảnh minh họa, hiệu úng chữ, màu sắc</h4>
                                    </li>
                                    <li>
                                       <span>Rất không hài lòng</span>
                                       <span><a href="#">1</a></span>
                                       <span><a href="#">2</a></span>
                                       <span><a href="#">3</a></span>
                                       <span><a href="#" class="active">4</a></span>
                                       <span><a href="#">5</a></span>
                                       <span>Rất hài lòng</span>
                                    </li>
                                    <li>
                                       <h4>4. Nhìn chung, mức độ hài lòng của bạn về khóa học này như thế nào</h4>
                                    </li>
                                    <li>
                                       <span>Rất không hài lòng</span>
                                       <span><a href="#">1</a></span>
                                       <span><a href="#">2</a></span>
                                       <span><a href="#">3</a></span>
                                       <span><a href="#" class="active">4</a></span>
                                       <span><a href="#">5</a></span>
                                       <span>Rất hài lòng</span>
                                    </li>
                                    <li>
                                       <h4>5. Góp ý của bạn để khóa học hữu ích hơn?</h4>
                                    </li>
                                    <li>
                                       <form class="form-horizontal">
                                          <textarea class="form-control" rows="3" placeholder="Vui lòng góp ý"></textarea>
                                       </form>
                                       <button class="btn btn-default" type="submit">Gửi</button>
                                       <button class="btn-last" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">Hủy</span></button>
                                    </li>
                                 </ul>
                              </div>
                              <!--end .content-->
                           </div>
                        </div>
                        <!--end .modal-dialog-->
                     </div>
                     <!-- End Modal Popup -->

            </div><!--end .wrap-support-->
        </div><!--end .left-->
        <div class="col-md-8 col-sm-7 col-xs-12 right" id="main-desc">
            <!-- Nav tabs -->
            <?= Tabs::widget([
                'options' => [
                    'class' => 'nav nav-tabs menu-main'
                ],
                'linkOptions' => [
                    'role' => "tab",
                    'data-toggle' => "tab",
                ],
                'encodeLabels' => false,
                'renderTabContent' => false,
                'items' => $contentTabs,
            ]) ?>


              <!-- Tab panes -->
              <div class="tab-content">
                  <?php if ($course->content) : ?>
                    <div role="tabpanel" class="tab-pane active" id="lesson-about">
                        <?= $course->content ?>
                    </div><!--end .tab-pane-->
                <?php endif;?>
                <div role="tabpanel" class="tab-pane" id="lesson-document" data-remote="<?= Url::toRoute(['/course/learning/documents', 'id' => $course->id]) ?>">
                    <div class="ajax-content"></div>
                </div>
                <div role="tabpanel" class="tab-pane" id="lesson-question" data-remote="<?= Url::toRoute(['/course/learning/qna', 'id' => $course->id]) ?>">
                    <!--end .right-->
                    <div id="lesson-content-qa">
                        <div class="tab-content tab-content-sub clearfix">
                            <?= $this->render('_qna-form', [
                                'qnaModel' => $qnaModel,
                                'user' => $user,
                                'courseId' => $course->id,
                            ]) ?>
                            <div role="tabpanel" class="tab-pane clearfix active ajax-content" id="lesson-latest-qa">
                            </div>
                        </div>
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane" id="lesson-comment" data-remote="<?= Url::toRoute(['/course/learning/discuss', 'id' => $course->id]) ?>">
                    <?= $this->render('./tabs/discuss', ['course' => $course]) ?>
                </div>
                <div role="tabpanel" class="tab-pane" id="lesson-transcript">
                    <?= $lession->transcript ?>
                </div>
              </div>
        </div><!--end .right-->
    </div><!--end .container-->
</div><!--end .wrap-content-->
