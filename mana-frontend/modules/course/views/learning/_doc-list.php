<?php
use common\helpers\FontAwesomeHelper;
use yii\grid\GridView;
use yii\grid\ActionColumn;
use yii\helpers\Url;

?>
<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'tableOptions' => ['class' => 'table'],
    'showHeader' => false,
    'summary' => false,
    'columns' => [
        [
            'attribute' => 'title',
            'format' => 'html',
            'value' => function ($model) {
                return FontAwesomeHelper::file($model->mime_type, ['3x', 'pull-left']).
                    '<strong>'.$model->title.'</strong><br>'.
                    '<em class="text-muted">'.Yii::$app->formatter->asSize($model->size).'</em>';
            },
        ],
        [
            'class' => ActionColumn::className(),
            'template' => '{view} {download}',
            'contentOptions' => [
                'class' => 'text-right'
            ],
            'urlCreator' => function ($action, $model, $key, $index) {
                return 'learning/document/?d='.$model->id.'&action='.$action;
            },
            'buttons' => [
                'view' => function ($url, $model) {
                    return '<a href="'.$url.'" class="btn btn-default" target="_blank"><i class="fa fa-file-o"></i> Xem</a>';
                },
                'download' => function ($url, $model) {
                    return '<a href="'.$url.'" class="btn btn-default"><i class="fa fa-cloud-download"></i> Download</a>';
                }
            ]
        ]
    ],
]) ?>
