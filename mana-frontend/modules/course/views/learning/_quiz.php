<?php
use yii\bootstrap\Html;
use yii\helpers\Url;
use kyna\course\models\QuizSession;
use kyna\course\models\Quiz;

?>
<div class="tab-pane clearfix active lesson-content-quiz">
    <div class="lesson-wrap-quiz" id="quiz-content">
        <div class="quiz-start">
            <h2><?= $quiz->name ?></h2>
            <?= $quiz->description ?>
            <?php
                $array = [
                    QuizSession::STATUS_SUBMITTED,
                    QuizSession::STATUS_PASSED,
                    QuizSession::STATUS_FAILED
                ];
            ?>
            <?php if(in_array($session->status, $array)) { ?>
                <?php if($session->status == QuizSession::STATUS_SUBMITTED){ ?>
                    <?php if($session->quiz->type == Quiz::TYPE_ONE_CHOICE){ ?>
<!--                        <div class="col-xs-12">-->
                            <ul>
                                <li class="col-md-4 col-xs-12"><img src="/img/lesson/icon-start.png" alt="" class="img-responsive media-object"/></li>
                                <li class="col-md-8 col-xs-12">
                                    <p>Số phần trăm yêu cầu: <?= $quiz->percent_can_pass; ?> %</p>
                                    <p>Tổng số câu hỏi: <b><?= count($quiz->detailQuestionIds); ?></b></p>
                                    <p>Bạn đạt: <b> <?= $session->scores; ?> %</b></p>
                                    <p>Bạn đạt: <b> <?= $session->rightAnswers; ?> / <?= count($quiz->detailQuestionIds); ?></b></p>
                                    <p>Kết quả:
                                        <b>
                                            <?php if($session->scores >= $quiz->percent_can_pass) { ?>
                                                Đạt yêu cầu.
                                            <?php }else{ ?>
                                                Không đạt yêu cầu.
                                            <?php } ?>
                                        </b>
                                    </p>
                                </li>
                            </ul>


<!--                        </div>-->

                    <?php }else{ ?>
                        <h3> Bạn đã hoàn thành bài tập và đã nộp bài, vui lòng chờ giảng viên chấm điểm </h3>
                    <?php } ?>
                <?php }elseif($session->status == QuizSession::STATUS_PASSED){ ?>

                <?php }else{ ?>

                <?php } ?>
                <div class="redo-container">
                    <?= Html::a('Làm lại', Url::toRoute(['/course/quiz/', 'id' => $quiz->id, 'status' => QuizSession::STATUS_REDO]), [
                        'class' => 'next-quiz btn-redo',
                        'data-ajax' => true,
                        'data-target' => '#quiz-content',
                        'data-push-state' => 'false',
                    ]) ?>
                </div>
            <?php } else { ?>
                <ul>
                    <?php
                        $time = null;
                        if($session->time_remaining == $session->duration || !$session->time_remaining) {
                            $time = $session->duration;
                        }else{
                            $time = $session->time_remaining;
                        }
                    ?>
                    <li class="col-md-4 col-xs-12"><img src="/img/lesson/icon-start.png" alt="" class="img-responsive media-object"/></li>
                    <li class="col-md-8 col-xs-12">
                        <p>Bạn sẽ có <span class="bold"><?= gmdate('H:i:s', $time) ?></span> để hoàn tất bài tập này.</p>
                        <p>Bấm "Bắt đầu trả lời" khi đã sẵn sàng.</p>
                        <?= Html::a('Bắt đầu làm bài', Url::toRoute(['/course/quiz/', 'id' => $quiz->id]), [
                           'class' => 'next-quiz',
                           'data-ajax' => true,
                           'data-target' => '#quiz-content',
                           'data-push-state' => 'false',
                        ]) ?>
                    </li>
                </ul>
            <?php } ?>
        </div><!--end .quiz-start-->
   </div><!--end .lesson-wrap-quiz-->
</div>
