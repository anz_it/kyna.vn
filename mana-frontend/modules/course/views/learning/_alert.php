<?php
/**
 * @author: Hong Ta
 * @desc: alert for user day can learn or active quick learn package
 */
use yii\helpers\Html;
use yii\helpers\Url;
?>
<div class="tab-pane clearfix active lesson-content-quiz">
    <div class="lesson-wrap-quiz" id="quiz-content">
        <div class="quiz-start">
            <h2>Thông báo</h2>
            <br />
            <p>
                Bạn chưa thể xem được bài học này, hãy đợi đến ngày <b style="color:red"> <?= $lession->getDateCanLearn($user_id); ?> </b> bạn nhé!!.
            </p>
            <br />
            <p>Để có thể xem được toàn bộ các bài học trong khoá học này ngay lập tức,
                bạn có thể chuyển sang chế độ học cấp tốc bằng cách click vào nút "Chuyển sang học cấp tốc" dưới đây:
            </p>
            <?= Html::a('Chuyển sang học cấp tốc', Url::toRoute(['/course/learning/quick-learn', 'id' => $course->id]), [
                'class' => 'next-quiz btn-redo',
                'data-confirm' => 'Bạn có chắc muốn học cấp tốc??' ,
                'data-ajax' => true,
                'data-target' => '#quiz-content',
                'data-push-state' => 'false',
            ]) ?>
            <br />
            <br />
            <p>
                <b style="color:red;text-decoration: underline;">Ghi chú:</b> Khi chuyển sang học cấp tốc, việc học của bạn có thể sẽ không đạt hiệu quả như khi bạn học theo phương pháp chuẩn với tiến độ
                ban đầu mà giảng viên đưa ra (1 ngày chỉ được học một vài bài để có thời gian suy nghĩ, áp dụng trong thực tế), nên bạn sẽ <b style="color:red;">không được hoàn học phí</b>
                khoá học này nếu như khoá học không hiệu quả với bạn.
            </p>
        </div>
    </div>
</div>
