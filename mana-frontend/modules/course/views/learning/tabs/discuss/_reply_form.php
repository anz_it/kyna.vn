<?php

use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\helpers\Html;
use kyna\course\models\CourseDiscussion;

$user = Yii::$app->user->identity;

$mDiscuss = new CourseDiscussion();
$mDiscuss->course_id = $model->course_id;
$mDiscuss->user_id = $user->id;
$mDiscuss->parent_id = $model->id;
?>

<div class="wrap-reply lesson-form-reply " id="lesson-form-reply-<?= $model->id ?>" role="group"> 
    <div class="media">
        <div class="media-left"> <a href="#"> <img class="media-object" data-src="holder.js/64x64" alt="64x64" src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9InllcyI/PjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iNjQiIGhlaWdodD0iNjQiIHZpZXdCb3g9IjAgMCA2NCA2NCIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+PCEtLQpTb3VyY2UgVVJMOiBob2xkZXIuanMvNjR4NjQKQ3JlYXRlZCB3aXRoIEhvbGRlci5qcyAyLjYuMC4KTGVhcm4gbW9yZSBhdCBodHRwOi8vaG9sZGVyanMuY29tCihjKSAyMDEyLTIwMTUgSXZhbiBNYWxvcGluc2t5IC0gaHR0cDovL2ltc2t5LmNvCi0tPjxkZWZzPjxzdHlsZSB0eXBlPSJ0ZXh0L2NzcyI+PCFbQ0RBVEFbI2hvbGRlcl8xNTQ5OGY1Yzc3MSB0ZXh0IHsgZmlsbDojQUFBQUFBO2ZvbnQtd2VpZ2h0OmJvbGQ7Zm9udC1mYW1pbHk6QXJpYWwsIEhlbHZldGljYSwgT3BlbiBTYW5zLCBzYW5zLXNlcmlmLCBtb25vc3BhY2U7Zm9udC1zaXplOjEwcHQgfSBdXT48L3N0eWxlPjwvZGVmcz48ZyBpZD0iaG9sZGVyXzE1NDk4ZjVjNzcxIj48cmVjdCB3aWR0aD0iNjQiIGhlaWdodD0iNjQiIGZpbGw9IiNFRUVFRUUiLz48Zz48dGV4dCB4PSIxNC41IiB5PSIzNi41Ij42NHg2NDwvdGV4dD48L2c+PC9nPjwvc3ZnPg==" data-holder-rendered="true" style="width: 64px; height: 64px;"> </a> </div>
        <div class="media-body">
            <div class="content-sub">                                                                                                                                                                                                 
                <div class="box-comment">
                    <?php $form = ActiveForm::begin([
                        'id' => 'discuss-form',
                        'action' => Url::toRoute(['/course/learning/discuss', 'id' => $model->course_id]),
                        'options' => [
                            'data-ajax' => true,
                            'data-target' => '#listview-replies-' . $model->id
                        ],
                    ]) ?>                                        
                        <?= Html::activeHiddenInput($mDiscuss, 'course_id') ?>
                        <?= Html::activeHiddenInput($mDiscuss, 'user_id') ?>
                        <?= Html::activeHiddenInput($mDiscuss, 'parent_id') ?>
                        <?= Html::activeTextarea($mDiscuss, 'comment', [
                            'rows' => '3',
                            'placeholder' => 'Nhập trả lời của bạn ở đây',
                            'class' => 'form-control',
                        ]) ?>
                        <div class="wrap-button"> 
                            <p><?= $user->profile->name ?></p>     
                            <ul>
                                <li><button type="reset" class="btn btn-default close-form">Hủy</button></li>
                                <li><button type="submit" class="btn btn-default button-reply">Trả lời</button></li>                                    
                            </ul>                                                                                     
                        </div>
                    <?php ActiveForm::end() ?>
                </div>                                                                                           
            </div><!--end .content-sub--> 
        </div>
    </div><!--end .media-->                                                                                                                                                                                                                                                                                                                                                     
</div><!--end .wrap-reply-->  