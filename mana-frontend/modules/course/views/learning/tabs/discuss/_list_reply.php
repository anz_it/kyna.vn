<?php

use yii\widgets\ListView;
use yii\data\ActiveDataProvider;

?>

<?= ListView::widget([
    'dataProvider' => new ActiveDataProvider([ 'query' => $model->getComments()]),
    'layout' => '<div class="content-sub">{items}</div>',
    'id' => 'listview-replies-' . $model->id,
    'itemView' => '_reply_item',
    'emptyText' => false
]) ?>