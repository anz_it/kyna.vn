<?php

use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\helpers\Html;
use kyna\course\models\CourseDiscussion;

$user = Yii::$app->user->identity;

$mDiscuss = new CourseDiscussion();
$mDiscuss->course_id = $course->id;
$mDiscuss->user_id = $user->id;
?>

<div class="wrapper form">
    <div class="left">
        <span class="author"><img src="/img/64x64.svg" class="img-responsive" alt=""/></span>
    </div><!--end .left-->
    <div class="right">
        <div class="box-comment">
            <?php $form = ActiveForm::begin([
                'id' => 'discuss-form',
                'action' => Url::toRoute(['/course/learning/discuss', 'id' => $course->id]),
                'options' => [
                    'data-ajax' => true,
                    'data-target' => '#discuss-content'
                ],
            ]) ?>
                <?= Html::activeHiddenInput($mDiscuss, 'course_id') ?>
                <?= Html::activeHiddenInput($mDiscuss, 'user_id') ?>
                <?= Html::activeTextarea($mDiscuss, 'comment', [
                    'rows' => '3',
                    'placeholder' => 'Nhập thảo luận bạn ở đây',
                    'class' => 'form-control',
                ]) ?>
                <div class="wrap-button"> 
                    <p><?= $user->profile->name ?></p>                           
                    <button type="submit" class="btn button">Gửi thảo luận</button>                            
                </div>
            <?php ActiveForm::end() ?>
        </div><!--end .box-comment-->
    </div><!--end .right-->
</div><!--end .form-->
