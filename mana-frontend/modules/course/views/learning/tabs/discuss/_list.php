<?php

use yii\widgets\ListView;

?>

<?= ListView::widget([
    'dataProvider' => $dataProvider,
    'layout' => '{items}',
    'itemView' => '_item',
    'emptyText' => false,
    'options' => [
        'tag' => 'ul',
        'class' => 'media-list'
    ],
    'itemOptions' => [
        'tag' => 'li',
        'class' => 'media media-key'
    ],
]) ?> 