<div class="media-left"> 
    <a href="#"> 
        <img class="media-object" data-src="holder.js/64x64" alt="64x64" src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9InllcyI/PjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iNjQiIGhlaWdodD0iNjQiIHZpZXdCb3g9IjAgMCA2NCA2NCIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+PCEtLQpTb3VyY2UgVVJMOiBob2xkZXIuanMvNjR4NjQKQ3JlYXRlZCB3aXRoIEhvbGRlci5qcyAyLjYuMC4KTGVhcm4gbW9yZSBhdCBodHRwOi8vaG9sZGVyanMuY29tCihjKSAyMDEyLTIwMTUgSXZhbiBNYWxvcGluc2t5IC0gaHR0cDovL2ltc2t5LmNvCi0tPjxkZWZzPjxzdHlsZSB0eXBlPSJ0ZXh0L2NzcyI+PCFbQ0RBVEFbI2hvbGRlcl8xNTQ5OGY1Yzc3MSB0ZXh0IHsgZmlsbDojQUFBQUFBO2ZvbnQtd2VpZ2h0OmJvbGQ7Zm9udC1mYW1pbHk6QXJpYWwsIEhlbHZldGljYSwgT3BlbiBTYW5zLCBzYW5zLXNlcmlmLCBtb25vc3BhY2U7Zm9udC1zaXplOjEwcHQgfSBdXT48L3N0eWxlPjwvZGVmcz48ZyBpZD0iaG9sZGVyXzE1NDk4ZjVjNzcxIj48cmVjdCB3aWR0aD0iNjQiIGhlaWdodD0iNjQiIGZpbGw9IiNFRUVFRUUiLz48Zz48dGV4dCB4PSIxNC41IiB5PSIzNi41Ij42NHg2NDwvdGV4dD48L2c+PC9nPjwvc3ZnPg==" data-holder-rendered="true" style="width: 64px; height: 64px;">
    </a>
</div>
<div class="media-body">   
    <ul class="top">
        <li class="name"><?= $model->user->profile->name ?></li>
        <li class="icon">●</li>
        <li class="date"><?= $model->postedTime ?></li>
    </ul>          
    <p><?= $model->comment ?></p>
    <ul class="bottom">
<!--        <li>1 <a href="#"><i class="fa fa-thumbs-o-up" aria-hidden="true"></i></a></li>
        <li class="icon">●</li>  -->
        <li><a href="#lesson-form-reply-<?= $model->id ?>" class="btn-reply btn-reply-form"><i class="fa fa-comment-o" aria-hidden="true"></i> Trả lời</a></li>
<!--        <li class="icon">●</li>                                    
        <li><a href=""><i class="fa fa-bookmark-o" aria-hidden="true"></i> Theo dõi</a></li>                                                                                                                                         -->
    </ul>                                                            

    <?= $this->render('_list_reply', ['model' => $model]) ?>
    
    <?= $this->render('_reply_form', ['model' => $model]) ?>
</div><!--end .media-body-->                           