<?php
use yii\bootstrap\Html;
use yii\helpers\Url;

$urlParams = ['/course/learning/'];
$urlParams = array_merge($urlParams, \Yii::$app->request->get());
$urlParams['lession'] = $model->id;

$active = (\Yii::$app->request->get('lession') == $model->id) ? ' class="active"' : '';
$class = $finished ? 'complete' : 'default';
$color = null;
if($active){
    $color = '#fff';
}else{
    $color = '#f16e11';
}
if($model->type == 'video'){
    $text = $finished ?  $number . '<i class="fa fa-check fa-course-check"></i>' : $number;
}else{
    $text = $finished ? '<i class="fa fa-check"></i>' : '<i class="fa fa-question" style="color:' . $color .'"></i>';
}


?>
<li<?= $active ?>>
<?= Html::a($text, Url::toRoute($urlParams), [
    'class' => $class,
    'data-video-iframe' => Url::toRoute(['/course/learning/video-frame', 'v' => $urlParams]),
]) ?>
</li>
