<?php
use yii\bootstrap\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
?>
<div class="wrapper form">
    <div class="left">
        <span class="author"><img src="/img/my-courses/author.jpg" class="img-responsive" alt=""/></span>
    </div><!--end .left-->
    <div class="right">
        <div class="box-comment">
            <?php $form = ActiveForm::begin([
                'action' => Url::toRoute('/course/learning/qna/', ['id' => $courseId]),
                'id' => 'qna-post-form',
                'options' => [
                    'data-ajax' => true,
                    'data-target' => '#lesson-latest-qa',
                ],
            ]); ?>
                <?= Html::activeHiddenInput($qnaModel, 'course_id') ?>
                <?= Html::activeHiddenInput($qnaModel, 'user_id') ?>
                <?= Html::activeTextarea($qnaModel, 'content', [
                    'rows' => '3',
                    'placeholder' => 'Nhập trả lời của bạn ở đây',
                    'class' => 'form-control',
                ]) ?>
                <div class="wrap-button">
                    <p><?= $user->profile->name ?></p>
                    <button type="submit" class="btn button">Gửi trả lời</button>
                </div>
            <?php ActiveForm::end() ?>
        </div><!--end .box-comment-->
    </div><!--end .right-->
</div><!--end .form-->
