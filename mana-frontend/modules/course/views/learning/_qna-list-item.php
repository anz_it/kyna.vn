<?php
use yii\data\ActiveDataProvider;
use yii\widgets\ListView;
?>
<li class="wrapper">
    <div class="left">
       <ul>
          <li class="icon-question text-muted"><i class="fa fa-question"></i></li>
          <li class="author"><img src="img/my-courses/author.jpg" class="img-responsive" alt=""></li>
       </ul>
    </div>
    <!--end author-->
    <div class="right">
        <ul class="top">
            <li class="name"><?= $model->user->profile->name ?></li>
            <li class="icon">●</li>
            <li class="date"><?= $model->postedTime ?></li>
        </ul>
        <div class="text"><?= $model->content ?></div>
        <!--
        <ul class="bottom">
            <li>1 <a href="#"><i class="fa fa-thumbs-o-up" aria-hidden="true"></i></a></li>
            <li class="icon">●</li>
            <li><a href=""><i class="fa fa-bookmark-o" aria-hidden="true"></i> Theo dõi</a></li>
        </ul>
        -->
        <?= ListView::widget([
            'dataProvider' => new ActiveDataProvider([ 'query' => $model->getAnswers()]),
            'layout' => '{items}',
            'showOnEmpty' => false,
            'options' => [
                'class' => 'content-sub'
            ],
            'emptyText' => false,
            'itemView' => function($model, $key, $index, $widget) {
                return '<div class="wrap">
                    <img src="img/my-courses/icon-border-question.png" class="icon-border">
                    <div>'.
                        $model->content.
                    '</div>
                </div>';
            }
        ]) ?>

    </div>
</li>
