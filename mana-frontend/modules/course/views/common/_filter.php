<?php if ($dataProvider->totalCount) { ?>
    <?php foreach ($dataProvider->getModels() as $course) { ?>
        <?php if ($course->kynaCourse && $course->organization) { ?>
            <div class="course-item">
                <div class="col-md-3 text-right image-holder">
                    <a href="/khoa-hoc/<?= $course->kynaCourse->slug ?>">
                        <img src="<?= $course->kynaCourse->image_url ?>" style="width: 100%">
                    </a>
                </div>
                <div class="col-md-9">
                    <div class="quick-description">
                        <h3>
                            <a href="/khoa-hoc/<?= $course->kynaCourse->slug ?>"><?= $course->kynaCourse->name ?></a>
                        </h3>
                        <p>
                            <?= $course->kynaCourse->description ?>
                        </p>
                    </div>
                    <div class="created-by">
                        <div class="col-md-1 col-xs-2 avatar">
                            <img src="<?= $course->organization->image_url ?>" style="width: 100%">
                        </div>
                        <div class="col-md-9 col-xs-10 name">
                            <a href="#"><?= $course->organization->name ?></a>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        <?php } ?>
    <?php } ?>
<?php } ?>