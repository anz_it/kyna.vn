<?php

use mana\modules\course\assets\LearningAsset;
use yii\helpers\Html;

LearningAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body class="page-lesson">
<?php $this->beginBody() ?>
<?= \mana\widgets\HeaderWidget::widget(); ?>
<section id="lesson">
    <?= $content ?>
</section>
<?= \mana\widgets\FooterWidget::widget(); ?>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
