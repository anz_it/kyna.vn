<?php

namespace mana\modules\course\controllers;

use common\helpers\CaptchaHelper;
use common\helpers\RoleHelper;
use kyna\course\models\Course;
use kyna\user\models\TimeSlot;
use kyna\user\models\UserTelesale;
use Yii;
use kyna\base\models\Location;
use common\helpers\TreeHelper;
use yii\helpers\Json;
use yii\web\Response;
use yii\web\View;

/**
 * Default controller for the `course` module
 */
class PageController extends \mana\components\Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public $layout = "@app/views/layouts/pages";
    
    public function beforeAction($action)
    {
        if ($action->id == 'submit-query-district' // || $action->id== 'submit'
        ) {
            $this->enableCsrfValidation = false;
        }

        return parent::beforeAction($action);
    }
    
    public function actionIndex() {
        $slug = Yii::$app->request->get('slug');

        /* Process Affiliate */
        $affiliate_id = Yii::$app->request->get('affiliate_id');
        if ($affiliate_id) {
            Yii::$app->session->setFlash('affiliate_id', $affiliate_id);
            $get = Yii::$app->request->get();
            unset($get['slug']);
            unset($get['dir']);
            unset($get['affiliate_id']);
            $newURL = Yii::$app->request->getUrl();
            // remove affiliate from url
            $newURL = str_replace('/' . $affiliate_id, '', $newURL);
            if (!empty($get)) {
                $arrURL = http_build_query($get);
                $newURL .= '?' . $arrURL;
            }
            $this->redirect($newURL, 301);
        }

        /* Lấy thông tin quận huyện */
        $locationModels = Location::find()->all();
        $locations = TreeHelper::dataMapper($locationModels);

        $cityData = [
            ['id' => '', 'name' => 'Chọn Tỉnh/Thành phố']
        ];

        foreach ($locations as $key => $location) {
            if ($location['parent_id'] == 0) {
                $cityData[] = ['id' => $key, 'name' => $location['name']];
            }
        }
        $data['city'] = $cityData;



        return $this->render("@app/modules/course/views/page/{$slug}/index", $data);
    }
    
    
    public function actionSubmitQueryDistrict()
    {
        
        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();
            return $this->getDistrictByCity($post['city']);
        }

        Yii::$app->response->format = Response::FORMAT_JSON;
        return ['ok' => 'success'];
    }

    public function getDistrictByCity($city_id)
    {
        
        $locationModels = Location::find()->all();
        $locations = TreeHelper::dataMapper($locationModels);
        $locationTree = TreeHelper::createTree($locations);
        $currentCityDistricts = $locationTree[$city_id]['_children'];
        $result = [
            [
                'id' => '',
                'name' => 'Quận/Huyện'
            ]
        ];

        if ($currentCityDistricts) {
            foreach ($currentCityDistricts as $key => $value) {
                $result[] = ['id' => $value['id'], 'name' => $value['name']];
            }
        }
        Yii::$app->response->format = Response::FORMAT_JSON;
        return $result;
    }

    public function actionSubmit()
    {
        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();
            /* Tùy chỉnh các thông số */
            $model = new UserTelesale();
            $model->setScenario('landing-page');

            list($user_type, $plan_id, $course_id, $combo_id, $cod_money, $bonus, $combo_tuong_tac,
                $combo_khuyen_mai, $advice_name, $filter_advice, $khoa_hoc_quan_tam, $alias_name, $email,
                $phonenumber, $fullname, $address, $city, $district, $note, $cong_viec, $han_che,
                $noi_lam_viec, $full_url, $page_slug, $landing_type, $list_course_ids, $slug) = $this->getParamsValue($post);

            // verify re-captcha
            $clientSessionKey = 'mana_lp_client_session_'. $slug;
            CaptchaHelper::setClientSession($clientSessionKey);
            $this->_verifyReCaptcha($post, $clientSessionKey);

            $res = $this->checkPhoneEmail($phonenumber, $email);

            if (isset($res['status']) && !$res['status']) {
                return $this->responseJson($post, $res, $user_type);
            }

            if (empty ($user_type)) {
                $user_type = UserTelesale::TYPE_MANA;
            }

//

            $checkExist = UserTelesale::findOne(['email' => $email, 'type' => $user_type, 'form_name' => $advice_name]);
            $cookies = Yii::$app->request->cookies;
            if ($checkExist) { // Đã tồn tại
                $affiliate_id_cookie = (int)$cookies->getValue('affiliate_id');
                if ($checkExist->affiliate_id > 0 && $checkExist->affiliate_id != $affiliate_id_cookie && $affiliate_id_cookie > 0) {
                    $old_affiliate_id = $checkExist->affiliate_id;
                    $checkExist->affiliate_id = $affiliate_id_cookie;
                    $checkExist->old_affiliate_id = $old_affiliate_id;
                    $checkExist->updated_time = time();
                } else if ($checkExist->affiliate_id == 0 && $affiliate_id_cookie > 0) {
                    $checkExist->affiliate_id = $affiliate_id_cookie;
                }
                $checkExist->save(false);

                $res['status'] = 0;
                $res['errors'] = [];
                $res['msg'] = 'Bạn đã đăng ký landing page này! <br> mana.edu.vn sẽ sớm liên hệ với bạn để tư vấn thêm về chương trình học!';
            } else {
                list($filter_advice, $note) = $this->getFilterAdvice($advice_name, $note, $filter_advice, $khoa_hoc_quan_tam, $cong_viec, $han_che, $noi_lam_viec);

                // Trước khi insert
                $assign_id = TimeSlot::getOperatorsInShift(RoleHelper::ROLE_TELESALE, $user_type);
                //
                $model->email = $email;
                $model->full_name = $fullname;
                $model->phone_number = $phonenumber;
                $model->type = $user_type;
                $model->amount = $cod_money;
                if ($course_id > 0) {
                    $model->course_id = $course_id;
                }
                if ($combo_id > 0) {
                    $model->course_id = $combo_id;
                }
                if (!empty($list_course_ids)) {
                    $model->list_course_ids = $list_course_ids;
                    $model->type = UserTelesale::TYPE_SINGLE_COURSE;
                }
                $model->bonus = $bonus;

                $model->affiliate_id = $cookies->getValue('affiliate_id');
                $model->street_address = $address;
                $model->form_name = $advice_name;
                $model->location_id = $district;
                $model->tel_id = $assign_id;
                $model->note = $note;

                // Tiến hành save
                $url = Yii::$app->request->serverName . Yii::$app->urlManager->createUrl('dang-ky-thanh-cong');
                if (Yii::$app->request->isSecureConnection) {
                    $url_success_page = 'https://' . $url;
                } else {
                    $url_success_page = 'http://' . $url;
                }

                if ($model->save()) {
                    $res['status'] = 1;
                    $res['errors'] = [];
                    $res['succespage'] = $url_success_page;
                    if (!empty($list_course_ids)) {
                        $res['product_id'] = $model->list_course_ids;
                    } else {
                        $res['product_id'] = $model->course_id;
                    }
                    $res['price'] = $model->amount;
                    $res['msg'] = 'Cảm ơn bạn đã đăng ký thành công khóa học tại Mana.edu.vn! <br> Nhân viên CSKH Mana.edu.vn sẽ sớm liên hệ với bạn trong vòng 24 giờ.';
                } else {

                    $res['status'] = 0;
                    $res['errors'] = $model->errors;
                    $res['msg'] = 'Quá trình gởi dữ liệu về hệ thống bị lỗi. Vui lòng thử lại sau. <br> Hoặc có thể liên hệ với Mana.edu.vn qua khung chat bên dưới hoặc thông tin hỡ trợ';
                }
            }

            return $this->responseJson($post, $res, $user_type);
        }
    }

    private function responseJson($post, $res, $user_type = UserTelesale::TYPE_MANA)
    {
        $relatedCourses = [];
        if (isset($post['RelatedCourses']) && !empty($post['RelatedCourses'])) {
            $relatedCourses = $post['RelatedCourses'];
        }
        $res['html'] = $this->renderPartial('thankyou-modal', [
            'relatedCourses' => $relatedCourses,
            'isAdvice' => ($user_type == UserTelesale::TYPE_MANA) ? true : false,
            'status' => $res['status'],
            'msg' => $res['msg']
        ]);
        Yii::$app->response->format = Response::FORMAT_JSON;

        return $res;
    }

    public function checkPhoneEmail($phonenumber, $email)
    {
        $res = [];
        $phonenumber = preg_replace('/[^0-9]/', '', $phonenumber);
        if (strlen($phonenumber) > 12) {
            $res['status'] = 0;
            $res['msg'] = 'Bạn vui lòng điền đúng số điện thoại';
        }

        if ($email == '') {
            $res['status'] = 0;
            $res['msg'] = 'Bạn vui lòng điền đúng email';
        }
        return $res;
    }

    public function getParamsValue($post)
    {
        $user_type = UserTelesale::TYPE_MANA;
        $plan_id = isset($post['plan_id']) ? (int)$post['plan_id'] : 0;
        $course_id = isset($post['course_id']) ? (int)$post['course_id'] : 0;
        $combo_id = isset($post['combo_id']) ? (int)$post['combo_id'] : 0;
        $cod_money = 0;
        $bonus = isset($post['bonus']) ? (int)$post['bonus'] : 2;
        $combo_tuong_tac = isset($post['combo_tuong_tac']) ? $post['combo_tuong_tac'] : '';
        $combo_khuyen_mai = isset($post['combo_khuyen_mai']) ? $post['combo_khuyen_mai'] : '';
        $advice_name = isset($post['advice_name']) ? $post['advice_name'] : '';
        $filter_advice = isset($post['filter_advice']) ? $post['filter_advice'] : '';
        $khoa_hoc_quan_tam = isset($post['khoa_hoc_quan_tam']) ? $post['khoa_hoc_quan_tam'] : '';
        $alias_name = isset($post['alias_name']) ? $post['alias_name'] : '';
        $email = isset($post['email']) ? $post['email'] : '';
        $phonenumber = isset($post['phonenumber']) ? $post['phonenumber'] : '';
        $fullname = isset($post['fullname']) ? $post['fullname'] : '';
        $address = isset($post['address']) ? $post['address'] : '';
        $city = isset($post['city']) ? (int)$post['city'] : 0;
        $district = isset($post['district']) ? (int)$post['district'] : 0;
        $note = '';
        $cong_viec = isset($post['cong_viec']) ? $post['cong_viec'] : '';
        $han_che = isset($post['han_che']) ? $post['han_che'] : '';
        $noi_lam_viec = isset($post['noi_lam_viec']) ? $post['noi_lam_viec'] : '';
        $full_url = isset($post['full_url']) ? $post['full_url'] : '';
        $page_slug = isset($post['page_slug']) ? $post['page_slug'] : '';
        $landing_type = isset($post['landing_type']) ? $post['landing_type'] : '';
        $list_course_ids = isset($post['list_course_ids']) ? $post['list_course_ids'] : [];
        $slug = isset($post['slug']) ? $post['slug'] : '';

        return [
            $user_type,
            $plan_id,
            $course_id,
            $combo_id,
            $cod_money,
            $bonus,
            $combo_tuong_tac,
            $combo_khuyen_mai,
            $advice_name,
            $filter_advice,
            $khoa_hoc_quan_tam,
            $alias_name,
            $email,
            $phonenumber,
            $fullname,
            $address,
            $city,
            $district,
            $note,
            $cong_viec,
            $han_che,
            $noi_lam_viec,
            $full_url,
            $page_slug,
            $landing_type,
            $list_course_ids,
            $slug
        ];
    }

    public function getFilterAdvice($advice_name, $note, $filter_advice, $khoa_hoc_quan_tam, $cong_viec, $han_che, $noi_lam_viec)
    {

        if ($filter_advice == 'hoc_cung_chuyen_gia') {
            $advice_name = 'PR KYNA - VOUCHER 100K';
            $note = 'Yêu cầu liên hệ tặng voucher 100k cho học viên.<br/>';
            if ($khoa_hoc_quan_tam != '') {
                $note .= 'Khóa học quan tâm: ' . $khoa_hoc_quan_tam . ' <br/>';
            }
        }
        if ($filter_advice == 'hoc_khong_gioi_han') {
            $advice_name = 'CAMPAIGN HỌC KHÔNG GIỚI HẠN HV MỚI';
            $note = 'Yêu cầu liên hệ tặng voucher 100k cho học viên.<br/>';
            $note .= ($cong_viec != '' ? 'Công việc: ' . $cong_viec . ' <br/>' : '');
            $note .= ($han_che != '' ? 'Hạn chế: ' . $han_che . ' <br/>' : '');
            $note .= ($noi_lam_viec != '' ? 'Nơi làm việc: ' . $noi_lam_viec . ' <br/>' : '');
        }
        return [
            $advice_name,
            $note
        ];
    }

    private function _verifyReCaptcha($post, $key = 'lp_client_session') {
        $enableCaptcha = CaptchaHelper::getIsEnable($key);
        if ($enableCaptcha) {
            $response = CaptchaHelper::verifyReCaptcha($post);
            if ($response == false) {
                // render popup re-captcha
                $result['status'] = 0;
                $result['recaptcha'] = 1;
                $result['html'] = $this->renderPartial('recaptcha-modal', [
                    'data' => $post
                ]);
                echo Json::encode($result);
                Yii::$app->end();
            }
        }
    }
}