<?php

namespace mana\modules\course\controllers;

use kyna\mana\models\Contact;
use kyna\mana\models\Course;
use Yii;
use kyna\mana\models\Certificate;
use kyna\mana\models\Education;
use kyna\mana\models\Organization;
use kyna\mana\models\Subject;
use mana\modules\course\models\CourseSearch;
use yii\web\Controller;

/**
 * Default controller for the `course` module
 */
class DefaultController extends \mana\components\Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {

        $searchModel = new CourseSearch();
        $searchModel->q = Yii::$app->request->get('q');
        $searchModel->orderByItem = Yii::$app->request->get('sort');
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $listSubjects = Subject::find()->where(['status'=>Subject::STATUS_ACTIVE, 'parent_id' => 0])->all();
        $listEducations = Education::findAllActive();
        $listCertificates = Certificate::findAllActive();
        $listOrganizations = Organization::findAllActive();
        return $this->render('index',[
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'listSubjects' => $listSubjects,
            'listEducations' => $listEducations,
            'listCertificates' => $listCertificates,
            'listOrganizations' => $listOrganizations,
        ]);
    }

    public function actionDetail() {

        $course = Course::find()->joinWith(['kynaCourse' => function ($query) {
            $query->andWhere([\kyna\course\models\Course::tableName().'.slug' => Yii::$app->request->get('slug')]);
        }])->one();
        $model = new Contact();
        $listSubjects = Subject::findAllActive();
        $listCertificates = Certificate::findAllActive();
        $listEducations = Education::findAllActive();

        $listCourses = Course::find()->joinWith(['kynaCourse' => function ($query) use ($course) {
//            $query->andWhere(['!=','kc.id' ,$course->course_id]);
        }, 'organization'])->where(['!=','course_id' ,$course->course_id])->limit(5)->orderBy(['order' => SORT_DESC])->all();
//        echo "<pre>";
//        var_dump($course);die;
        return$this->render('detail', [
            'course' => $course,
            'listCourses' => $listCourses,
            'model' => $model,
            'listEducations' => $listEducations,
            'listCertificates' => $listCertificates,
            'listSubjects' => $listSubjects
        ]);
    }

    public function actionIntro() {
        return $this->render('intro');
    }
}
