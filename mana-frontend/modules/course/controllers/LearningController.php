<?php

namespace mana\modules\course\controllers;

use Yii;
use kyna\course\models\Course;
use kyna\course\models\CourseLesson;
use kyna\course\models\CourseDocument;
use kyna\course\models\CourseLearnerQna;
use kyna\course\models\CourseDiscussion;
use kyna\user\models\UserCourse;
use yii\base\InvalidRouteException;
use yii\web\Response;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
use yii\web\ServerErrorHttpException;

class LearningController extends \mana\components\Controller
{
    public $layout = 'lesson';

    public function getContentTabs() {
        /**
         * <ul class="nav nav-tabs menu-main" role="tablist">
         *    <li role="presentation" class="active"><a href="#lesson-about" aria-controls="lesson-about" role="tab" data-toggle="tab">Tổng quan</a></li>
         *    <li role="presentation"><a href="#lesson-document" aria-controls="lesson-document" role="tab" data-toggle="tab">Tài liệu</a></li>
         *    <li role="presentation"><a href="#lesson-question" aria-controls="lesson-question" role="tab" data-toggle="tab">Hỏi giảng viên</a></li>
         *    <li role="presentation"><a href="#lesson-comment" aria-controls="lesson-comment" role="tab" data-toggle="tab">Thảo luận</a></li>
         *    <li role="presentation"><a href="#lesson-transcript" aria-controls="lesson-transcript" role="tab" data-toggle="tab"><i class="fa fa-file-text-o" aria-hidden="true"></i> Xem Transcript</a></li>
         *  </ul>
         */
        return [
            [
                'label' => 'Tổng quan',
                'url' => '#lesson-about',
            ],
            [
                'label' => 'Tài liệu',
                'url' => '#lesson-document',
            ],
            [
                'label' => 'Hỏi giảng viên',
                'url' => '#lesson-question',
            ],
            [
                'label' => 'Thảo luận',
                'url' => '#lesson-comment',
            ],
            [
                'label' => '<i class="fa fa-file-text-o" aria-hidden="true"></i> Xem Transcript',
                'url' => '#lesson-transcript',
            ],
        ];
    }

    public function actionIndex($id)
    {
        $userId = Yii::$app->user->getId();
        if (!UserCourse::getLearning($id, $userId)) {
            throw new NotFoundHttpException();

        }

        $course = $this->findCourse($id);

        $sectionId = Yii::$app->request->get('section');
        if (!$sectionId) {
            $sectionId = $course->defaultSection->id;
        }

        $lessionId = Yii::$app->request->get('lession');
        if (!$lessionId) {
            $lessionId = CourseLesson::find()->where(['section_id' => $sectionId])->min('id');
            if ($lessionId) {
                $this->redirect(Url::toRoute([
                    '/course/learning',
                    'id' => $id,
                    'section' => $sectionId,
                    'lession' => $lessionId,
                ]));
            }
        }

        $lession = CourseLesson::findOne($lessionId);
        if (!$lession) {
            $lession = new CourseLesson();
        }

        $lessionDataProvider = new ActiveDataProvider([
            'query' => CourseLesson::find()->where(['section_id' => $sectionId]),
        ]);

        $user = Yii::$app->user->identity;

        $qnaModel = new CourseLearnerQna();
        $qnaModel->course_id = $id;
        $qnaModel->user_id = $user->id;

        return $this->render('index', [
            'course' => $course,
            'finishedLessons' => $course->getFinishedLessons($userId),
            'lessionDataProvider' => $lessionDataProvider,
            'lession' => $lession,
            'sectionId' => $sectionId,
            'qnaModel' => $qnaModel,
            'user' => $user,
            'contentTabs' => $this->contentTabs,
        ]);
    }

    public function actionEndLesson($id)
    {
        Yii::$app->response->format = Response::FORMAT_RAW;

        return UserCourse::finishLesson($id);
    }

    public function actionQna() {
        $courseLearnerQna = new CourseLearnerQna();

        if (Yii::$app->request->isPost) {
            $formData = Yii::$app->request->post($courseLearnerQna->formName());
            if (isset($formData['course_id'])) {
                $id = $formData['course_id'];
            }
        }
        else {
            $id = Yii::$app->request->get('id');
        }

        if (!isset($id) or !$id) {
            throw new InvalidRouteException();
        }

        if (Yii::$app->request->isPost) {
            $courseLearnerQna = new CourseLearnerQna();

            if (!$courseLearnerQna->load(Yii::$app->request->post()) or !$courseLearnerQna->save()) {
                //var_dump($courseLearnerQna->errors);
                die;
                // TODO: Error message here
            }
        }

        $course = $this->findCourse($id);

        $query = $course->getLearnerQuestions();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        return $this->renderPartial('_qna-list', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionDiscuss($id)
    {
        if (Yii::$app->request->isPost) {
            $mDiscuss = new CourseDiscussion();

            if (!$mDiscuss->load(Yii::$app->request->post()) or !$mDiscuss->save()) {
                var_dump($mDiscuss->errors);
                die;
                // TODO: Error message here
            }

            if (!empty($mDiscuss->parent_id)) {
                return $this->renderPartial('tabs/discuss/_list_reply', [
                    'model' => $mDiscuss->parent,
                ]);
            }
        }

        $course = $this->findCourse($id);
        $query = $course->getDiscussions();
        $query->orderBy('created_time DESC');
        $query->andFilterWhere(['parent_id' => 0]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        return $this->renderPartial('tabs/discuss/_list', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionDocuments($id)
    {
        $course = $this->findCourse($id);

        $query = $course->getDocuments();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        return $this->renderPartial('_doc-list', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionDocument($d, $action = 'view')
    {
        $doc = CourseDocument::findOne($d);
        if (!$doc or !file_exists($doc->save_path)) {
            throw new NotFoundHttpException();
        }

        $response = Yii::$app->response;
        if ($action === 'download') {
            $response->headers->remove('Pragma');
            $response->xSendFile($doc->save_path, $doc->title.'.'.$doc->file_ext, [
                'mimeType' => $doc->mime_type,
            ]);
        } else {
            $response->headers->set('Content-Type', $doc->mime_type);
            $response->format = Response::FORMAT_RAW;
            if (!is_resource($response->stream = fopen($doc->save_path, 'r'))) {
                throw new ServerErrorHttpException('file access failed: permission deny');
            }
        }

        return $response->send();
    }

    protected function findCourse($id)
    {
        $model = Course::findOne($id);
        if ($model !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionQuickLearn($id)
    {
        $userId = Yii::$app->user->getId();
        $course = $this->findCourse($id);

        $user_course = UserCourse::find()->where(['user_id' => $userId, 'course_id' => $course->id])->one();

        $sectionId = Yii::$app->request->get('section');

        if (!$sectionId) {
            $sectionId = $course->defaultSection->id;
        }

        $lessionId = Yii::$app->request->get('lession');
        if(!empty($user_course)){
            $user_course->is_quick = UserCourse::BOOL_YES;
            if($user_course->validate()){
                $user_course->save();
                if (!$lessionId) {
                    $lessionId = CourseLesson::find()->where(['section_id' => $sectionId])->min('id');
                }
                $this->redirect(Url::toRoute([
                    '/course/learning',
                    'id' => $id,
                    'section' => $sectionId,
                    'lession' => $lessionId,
                ]));
            }else{
                var_dump($user_course->getErrors());
            }
        }
    }
}
