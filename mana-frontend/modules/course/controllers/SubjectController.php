<?php
namespace mana\modules\course\controllers;

use kyna\mana\models\Certificate;
use kyna\mana\models\Education;
use kyna\mana\models\Organization;
use kyna\mana\models\Subject;
use mana\modules\course\models\CourseSearch;
use Yii;
use yii\helpers\Url;
use yii\web\Response;

/**
 * Created by PhpStorm.
 * User: Hien Nguyen
 * Date: 7/19/2016
 * Time: 4:54 PM
 */

class SubjectController extends \mana\components\Controller {
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        $listData = Subject::findAllActive();
        return $this->render('index', [
            'listData' => $listData
        ]);
    }

    public function actionCourse() {
        $slug = Yii::$app->request->get('slug');
        $category = Subject::findOne(['slug' => $slug, 'status' => Subject::STATUS_ACTIVE]);
        $searchModel = new CourseSearch();
        $searchModel->q = Yii::$app->request->get('q');
        $searchModel->orderByItem = Yii::$app->request->get('sort');

        if ($category) {
            $searchModel->subject_id = $category->id;
            $searchModel->subjectIds = Subject::find()->where(['status'=>Subject::STATUS_ACTIVE, 'parent_id' => $category->id])->select('id')->column();

        }

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $data = $this->renderPartial('@mana/modules/course/views/common/_filter',[
                'dataProvider' => $dataProvider,
            ]);
            return [
                'data' => $data,
                'listCount' => $dataProvider->totalCount
            ];
        }
        $listSubjects = Subject::find()->where(['status'=>Subject::STATUS_ACTIVE, 'parent_id' => 0])->all();
        $listEducations = Education::findAllActive();
        $listCertificates = Certificate::findAllActive();
        $listOrganizations = Organization::findAllActive();
        return $this->render('@mana/modules/course/views/common/course',[
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'listSubjects' => $listSubjects,
            'listEducations' => $listEducations,
            'listCertificates' => $listCertificates,
            'listOrganizations' => $listOrganizations,
            'category' => $category,
            'filterUrl' => Url::toRoute(['/course/subject/course', 'slug'=>$slug])
        ]);
    }

    public function actionFilter() {
        if (Yii::$app->request->isAjax) {
            return 1;
        }
        return 2;
    }
}