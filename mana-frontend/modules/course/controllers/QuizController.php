<?php

namespace mana\modules\course\controllers;

use common\helpers\ArrayHelper;
use kyna\course\models\Quiz;
use kyna\course\models\QuizAnswer;
use kyna\course\models\QuizQuestion;
use kyna\course\models\QuizSession;
use yii\data\ActiveDataProvider;
use yii\web\Response;

class QuizController extends \mana\components\Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }
    public function actionIndex($id)
    {
        $quiz = Quiz::findOne($id);

        $userId = \Yii::$app->user->getId();

        $position = \Yii::$app->request->get('position', false);

        $redo = \Yii::$app->request->get('status', false);
        if(!empty($redo)){
            if($redo == QuizSession::STATUS_REDO){
                $redo_session = QuizSession::find()->where([
                    'user_id' => $userId,
                    'quiz_id' => $id,
                    'status'  => QuizSession::STATUS_SUBMITTED
                ])->one();
                if(!empty($redo_session)){
                    $redo_session->status = $redo;
                    $redo_session->is_passed = QuizSession::BOOL_NO;
                    if($redo_session->validate()){
                        $redo_session->save();
                    }
                    unset($redo);
                }
            }
        }

        $currentPosition = false;

        $session = $quiz->getSession($userId);
        if($session->status == QuizSession::STATUS_SUBMITTED){
            if($quiz->type == Quiz::TYPE_ONE_CHOICE){
                $score = $session->scores;
//                var_dump($score);die;
            }
            return $this->renderPartial('_result', [
                'quiz' => $quiz,
                'session' => $session,
            ]);
        }
        $now = time();

        if($session->start_time == $now){
            $session->time_remaining = $session->duration;
        }else{
            $session->time_remaining = $session->duration - ($now - $session->start_time);
        }
        $session->last_interactive = $now;
        $session->status = QuizSession::STATUS_DOING;
        $session->is_passed = 0;
        $session->save();



        if ($session->load(\Yii::$app->request->post())) {

            $lastAnswer = $session->saveAnswers();



            if($quiz->show_answer == QuizAnswer::BOOL_YES){
                $query = $session->getUnansweredModels()->orderBy(['position' => SORT_ASC])->limit(1);

                $positionQuery = clone $query;

                $currentPosition = $positionQuery->select('position')->scalar();

                $position = $currentPosition - 1;
            }
            if(empty($session->unansweredModels)){
                $session->status = QuizSession::STATUS_SUBMITTED;
                $session->is_passed = 0;
                $session->save();
            }
        }
        if ($session->isSubmitted) {

            return $this->renderPartial('_result', [
                'quiz' => $quiz,
                'session' => $session,
            ]);

        }

        if ($session->isFinished and !$session->isSubmitted) {
            return $this->renderPartial('_result', [
                'quiz' => $quiz,
                'session' => $session,
            ]);
        }

        $count = count($quiz->details);

        if ($quiz->display_type === 'single') {
            if (!$position) {
                $query = $session->getUnansweredModels()->orderBy(['position' => SORT_ASC])->limit(1);

                $positionQuery = clone $query;
                $currentPosition = $positionQuery->select('position')->scalar();

                $position = $currentPosition;
            } else{
                $query = $session->getAnswerModels()->andWhere(['position' => $position]);
                $positionQuery = clone $query;
                $currentPosition = $positionQuery->select('position')->scalar();
//                $position = $currentPosition;
            }
        }else{
            $query = $session->getAnswerModels();
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => false,
        ]);

        $prev = ($position and ($position > 1)) ? $position - 1 : false;

        $next = ($position and $currentPosition and ($position <= $currentPosition)) ? $position + 1 : false;

        if($currentPosition && $currentPosition == $count){
            $next = false;
        }

        return $this->renderPartial('_questions', [
            'dataProvider' => $dataProvider,
            'quiz' => $quiz,
            'prev' => $prev,
            'next' => $next,
            'session' => $session,
            'lastAnswer' => !empty($lastAnswer) ? $lastAnswer : null,
        ]);
    }

    private function _isCorrectAnswer($answerId)
    {
        return QuizAnswer::find()->where([
            'id' => $answerId,
            'is_exactly' => true,
        ])->exists();
    }

    public function actionQuestion($id, $order)
    {
        $quiz = Quiz::findOne($id);
        $quizQuestion = $quiz->getQuizQuestion($order);

        $answerQuery = $quizQuestion->question->getAnswers();
        $answerDataProvider = new ActiveDataProvider([
            'query' => $answerQuery,
        ]);
        //$answers = ArrayHelper::map($quizQuestion->question->answers, 'id', 'content');

        return $this->renderPartial('_single-question', [
            'quizQuestion' => $quizQuestion,
            'quiz' => $quiz,
            'answerDataProvider' => $answerDataProvider,
        ]);
    }

    public function actionGetAnswer($answer_id)
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;
        $answer = QuizAnswer::findOne($answer_id);

        if (!$answer) {
            return [
                'isCorrect' => false,
            ];
        }

        return [
            'isCorrect' => $answer->is_exactly,
            'explaination' => $answer->question->explaination,
        ];
    }
}
