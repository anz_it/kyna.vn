<?php
namespace mana\modules\course\controllers;

use kyna\mana\models\Certificate;
use kyna\mana\models\Education;
use kyna\mana\models\Organization;
use kyna\mana\models\Subject;
use mana\modules\course\models\CourseSearch;
use Yii;
use yii\helpers\Url;
use yii\web\Response;

/**
 * Created by PhpStorm.
 * User: Hien Nguyen
 * Date: 7/19/2016
 * Time: 4:54 PM
 */

class OrganizationController extends \mana\components\Controller {
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        $listData = Organization::findAllActive();
        return $this->render('index', [
            'listData' => $listData
        ]);
    }

    public function actionCourse() {
        $slug = Yii::$app->request->get('slug');
        $category = Organization::findOne(['slug' => $slug, 'status' => Education::STATUS_ACTIVE]);
        $searchModel = new CourseSearch();
        $searchModel->q = Yii::$app->request->get('q');
        $searchModel->orderByItem = Yii::$app->request->get('sort');

        if ($category) {
            $searchModel->organization_id = $category->id;
        }

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $data = $this->renderPartial('@mana/modules/course/views/common/_filter',[
                'dataProvider' => $dataProvider,
            ]);
            return [
                'data' => $data,
                'listCount' => $dataProvider->totalCount
            ];
        }
        $listSubjects = Subject::find()->where(['status'=>Subject::STATUS_ACTIVE, 'parent_id' => 0])->all();
        $listEducations = Education::findAllActive();
        $listCertificates = Certificate::findAllActive();
        $listOrganizations = Organization::findAllActive();
        return $this->render('@mana/modules/course/views/common/course',[
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'listSubjects' => $listSubjects,
            'listEducations' => $listEducations,
            'listCertificates' => $listCertificates,
            'listOrganizations' => $listOrganizations,
            'category' => $category,
            'filterUrl' => Url::toRoute(['/course/organization/course', 'slug'=>$slug])
        ]);
    }

    public function actionView() {
        $slug = Yii::$app->request->get('slug');
        $category = Organization::findOne(['slug' => $slug, 'status' => Education::STATUS_ACTIVE]);
        $searchModel = new CourseSearch();
        $searchModel->q = Yii::$app->request->get('q');
        $searchModel->orderByItem = Yii::$app->request->get('sort');

        if ($category) {
            $searchModel->organization_id = $category->id;
        }

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $data = $this->renderPartial('@mana/modules/course/views/common/_filter',[
                'dataProvider' => $dataProvider,
            ]);
            return [
                'data' => $data,
                'listCount' => $dataProvider->totalCount
            ];
        }
        $listSubjects = Subject::find()->where(['status'=>Subject::STATUS_ACTIVE, 'parent_id' => 0])->all();
        $listEducations = Education::findAllActive();
        $listCertificates = Certificate::findAllActive();
        $listOrganizations = Organization::findAllActive();
        return $this->render('course',[
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'listSubjects' => $listSubjects,
            'listEducations' => $listEducations,
            'listCertificates' => $listCertificates,
            'listOrganizations' => $listOrganizations,
            'category' => $category,
            'filterUrl' => Url::toRoute(['/course/organization/course', 'slug'=>$slug])
        ]);
    }
}