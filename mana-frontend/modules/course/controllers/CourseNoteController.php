<?php

namespace mana\modules\course\controllers;

use kyna\course\models\CourseLesson;
use kyna\course\models\CourseLessionNote;
use kyna\user\models\UserCourse;
use yii\base\InvalidCallException;
use yii\data\ActiveDataProvider;
use yii\web\Response;

class CourseNoteController extends \mana\components\Controller
{

    public function actionGet($id)
    {
        $query = CourseLessionNote::find()->where([
                    'course_lession_id' => $id,
                    'user_id' => \Yii::$app->user->getId(),
                ])->orderBy(['id' => SORT_DESC])->limit(10);

        $noteDataProvider = new ActiveDataProvider([
            'query' => $query
        ]);

        return $this->renderPartial('_note', ['noteDataProvider' => $noteDataProvider]);
    }

    public function actionAdd()
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;
        $lessionNote = new CourseLessionNote();

        $id = \Yii::$app->request->post('id');
        $userId = \Yii::$app->user->getId();
        $runtime = \Yii::$app->request->post('t');
        $note = \Yii::$app->request->post('note');

        if (!$note) {
            return [
                'success' => false,
                    //'message' => 'Nhập nội dung ghi chú để lưu',
            ];
        }

        if (!$id) {
            throw new InvalidCallException('Cant perform this function without and `id`');
        }

        if (!$userId) {
            return [
                'success' => false,
                'message' => 'Bạn phải đăng nhập để lưu các ghi chú',
            ];
        }

        $lessionNote->attributes = [
            'course_lession_id' => $id,
            'user_id' => $userId,
            'current_run_time' => floor($runtime),
            'note' => $note,
        ];

        $result = $lessionNote->save();

        if (\Yii::$app->request->isAjax) {
            return [
                'success' => $result
            ];
        }
    }

}
