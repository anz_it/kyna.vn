<?php
return [
    'adminEmail' => 'admin@example.com',
    /**
     * Expiration Time for Cache Cart
     */
    'expiredCartTime' => 86400, // 1 day
    'transactionPrefix' => 'kyna.vn_',
    // site info
    'hotline' => '1900 6364 09',
    'siteTilePrefix' => 'Mana.edu.vn'
];
