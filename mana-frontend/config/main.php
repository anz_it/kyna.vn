<?php

$params = array_merge(
        require(__DIR__ . '/../../common/config/params.php'), require(__DIR__ . '/../../common/config/params-local.php'), require(__DIR__ . '/params.php'), require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'mana-frontend',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'mana\controllers',
    'modules' => [
        'user' => [
            'class' => 'dektrium\user\Module',
            'enableUnconfirmedLogin' => true,
            'enableConfirmation' => true,
            'enableRegistration' => true,
            'modelMap' => [
                'User' => 'kyna\user\models\User',
                'Profile' => 'kyna\user\models\Profile',
                'LoginForm' => 'dektrium\user\models\LoginForm',
                'RegistrationForm' => 'app\modules\user\models\forms\RegistrationForm',
            ],
            'controllerMap' => [
                'security' => 'mana\modules\user\controllers\SecurityController',
                'default' => 'mana\modules\user\controllers\DefaultController',
                'course' => 'mana\modules\user\controllers\CourseController',
//                'message' => 'app\modules\user\controllers\MessageController',
//                'question' => 'app\modules\user\controllers\QuestionController',
//                'transaction' => 'app\modules\user\controllers\TransactionController',
//                'document' => 'app\modules\user\controllers\DocumentController',
//                'order'     => 'app\modules\user\controllers\OrderController',
                'registration'  => 'mana\modules\user\controllers\RegistrationController',
                'auth'  => 'mana\modules\user\controllers\AuthController',
                'contact'  => 'mana\modules\user\controllers\ContactController',
            ],
        ],
        'treemanager' =>  [
            'class' => '\kartik\tree\Module',
        ],
        'course' => [
            'class' => 'mana\modules\course\CourseModule',
        ],
    ],
    'components' => [
        'user' => [
            'class' => 'app\components\WebUser',
            'identityClass' => 'kyna\user\models\User',
            'loginUrl' => ['/site/login'],
            'enableAutoLogin' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'categories' => ['ipn'],
                    'levels' => ['error', 'warning', 'info'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'rules' => [
                'gioi-thieu' => 'course/default/intro',
                // course
                'danh-sach-khoa-hoc/<slug:[\w-]+>-c<catId:\d+>' => 'course/default/index',
                'khoa-hoc/<slug:[\w-]+>' => 'course/default/detail',
                'danh-sach-khoa-hoc' => 'course/default/index',

                // subject
                'chuyen-nganh' => 'course/subject/index',
                'chuyen-nganh/danh-sach-khoa-hoc/<slug:[\w-]+>' => 'course/subject/course',

                // education
                'he-dao-tao' => 'course/education/index',
                'he-dao-tao/danh-sach-khoa-hoc/<slug:[\w-]+>' => 'course/education/course',

                // certificate
                'chung-chi' => 'course/certificate/index',
                'chung-chi/danh-sach-khoa-hoc/<slug:[\w-]+>' => 'course/certificate/course',

                // organization
                'to-chuc-cap-chung-chi' => 'course/organization/index',
                'to-chuc-cap-chung-chi/danh-sach-khoa-hoc/<slug:[\w-]+>' => 'course/organization/course',
                'to-chuc-cap-chung-chi/<slug:[\w-]+>' => 'course/organization/view',
                '<slug:[\w-]+>' => 'course/default/detail',
                'cbp/<slug:[\w-]+>/<affiliate_id:\d+>'=> "course/page/index",
                'cs/<slug:[\w-]+>/<affiliate_id:\d+>'=> "course/page/index",
                'cbp/<slug:[\w-]+>'=> "course/page/index",
                'cs/<slug:[\w-]+>'=> "course/page/index",
				'cbp-combo/<slug:[\w-]+>'=> "course/page/index",
				'cbp-combo/<slug:[\w-]+>/<affiliate_id:\d+>'=> "course/page/index"
            ],
        ],
        'view' => [
            'class' => 'yii\web\View',
            'renderers' => [
                'tpl' => [
                    'class' => 'yii\smarty\ViewRenderer',
                    //'cachePath' => '@runtime/Smarty/cache',
                    'widgets' => [
                        'blocks' => [
                            'ActiveForm' => '\yii\widgets\ActiveForm',
                        ],
                    ],
                ],
            ],
        ],
//        'cart' => [
//            'class' => 'app\modules\cart\components\ShoppingCart',
//            'cartId' => 'kyna-cart',
//        ],
        'facebook' => [
            'class' => 'mana\modules\user\components\Facebook',
        ],
    ],
    'params' => $params,
];
