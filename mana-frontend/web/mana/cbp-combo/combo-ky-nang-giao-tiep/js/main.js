
$(window).on("load scroll", function () {
  var top = $(this).scrollTop();
  if (top > 50) {
      if (!$("#wrap-header").hasClass("action")) {
          $("#wrap-header").addClass("action");
      }
  }
  else {
      $("#wrap-header").removeClass("action");
  }
});

function showHide(shID) {
   if (document.getElementById(shID)) {
      if (document.getElementById(shID+'-show').style.display != 'none') {
         document.getElementById(shID+'-show').style.display = 'none';
         document.getElementById(shID).style.display = 'block';
      }
      else {
         document.getElementById(shID+'-show').style.display = 'inline';
         document.getElementById(shID).style.display = 'none';
      }
   }
}

$(".scrollspy-course > ul > li").click(function(){
    var obj = $(this).parents("ul").find('li');
    for (var i = 0; i < $(obj).length; i++)
        $(obj).removeClass("active");
    $(this).addClass("active");
    var id = $(this).find('a').attr("data-link");
    goToByScroll(id);
});

$(".button-click").click(function() {
  $('html,body').animate({
    scrollTop: $("#content-center").offset().top - 70},
    'slow');
});

$(".mobile .button").click(function() {
  $('html,body').animate({
    scrollTop: $("#register").offset().top - 70},
    'slow');
});

function goToByScroll(id){
    var pos = $(id).offset().top - 70;
    $('html,body').animate({
        scrollTop: pos},
        'slow');
}

/* Set active For Menu when scrolling*/
$(window).scroll(function(){
    var top = $(this).scrollTop();
    var obj = $("[data-name='scrolling']");
    var current = $(obj[0]);
    for(var i = 1; i < $(obj).length; i++){
        if (top - ($(obj[i]).offset().top - 72) >= 0)
            current = $(obj[i]);
    }
    var link_current = $("#scrollspy-course li").find("a[data-link='#" + $(current).attr("id") + "']");
    if (!$(link_current).parent().hasClass("active"))
    {
        $("#scrollspy-course li").removeClass("active");
        $(link_current).parent().addClass("active");
    }
});

$(".expert-slider").slick({
  infinite: true,
  slidesToShow: 1,
  dots: true,
  responsive: [
    {
      breakpoint: 1199,
      settings: {
        dots: true,
        infinite: false,
      }
    }
  ]
});
