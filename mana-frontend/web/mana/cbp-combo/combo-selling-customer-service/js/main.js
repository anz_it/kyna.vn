$(function(){
	$('.sliderPost:eq(1)').hide();
	$('.mobileTabs ul li').click(function(){
		var curIndex = $(this).index();
		$('.mobileTabs ul li').removeClass('active');
		$('.sliderPost').fadeOut(100);
		$(this).addClass('active');
		$('.sliderPost:eq('+curIndex+')').fadeIn(100);
		return false;
	});
})
if($(window).width() > 640){
	$('.expertSlider').slick({
		infinite: true,
	    slidesToShow: 1,
	    slidesToScroll: 1,
	    speed: 500,
	    arrows: true,
	    dots: false,
	    responsive: [
	    {
	      breakpoint: 992,
	      settings: {
	        arrows: false,
			dots: true
	      }
	    },
	    {
	      breakpoint: 468,
	      settings: {
	      	arrows: false,
			dots: true,
	        adaptiveHeight: true
	      }
	    }
	  	]
	});

};
$('.sliderTesti').slick({
	infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    asNavFor: '.sliderTestiNav',
    speed: 500,
    arrows: true,
    dots: false,
    responsive:[{
	  	breakpoint: 640,
	  	settings: {
			arrows: false,
			adaptiveHeight: true
	  	}
	}]
});
$('.sliderTestiNav').slick({
	slidesToShow: 3,
	slidesToScroll: 1,
	asNavFor: '.sliderTesti',
	dots: false,
	arrows: false,
	touchMove: true,
	centerMode: true,
	centerPadding: '0',
	infinite: true,
	focusOnSelect: true
});
$(function() {
	$('a.page-scroll').click(function() {
		if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
			var target = $(this.hash);
			target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
			if (target.length) {
				$('html,body').animate({
					scrollTop: target.offset().top - 55
				}, 500);
				return false;
			}
		}
	});
});

$(function(){
	jQuery(window).scroll(function () {
		if (jQuery(window).scrollTop() > 100) {
			jQuery("#topMenu").addClass("scroll");
		} else {
			jQuery("#topMenu").removeClass("scroll");
		}
	});
});

var topMenu = $(".navbar-nav"),
    topMenuHeight = topMenu.outerHeight(),
    menuItems = topMenu.find("a"),
    scrollItems = menuItems.map(function() {
        var item = $($(this).attr("href"));
        if (item.length) {
            return item;
        }
    });

$(window).scroll(function() {
    var fromTop = $(this).scrollTop() + topMenuHeight;
    var cur = scrollItems.map(function() {
        if ($(this).offset().top < fromTop)
            return this;
    });
    cur = cur[cur.length - 1];
    var id = cur && cur.length ? cur[0].id : "";
    menuItems
        .removeClass("active")
        .filter("[href=#" + id + "]").addClass("active");
});