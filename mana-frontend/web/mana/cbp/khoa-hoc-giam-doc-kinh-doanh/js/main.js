(function () {
  'use strict';

  $('a[href*=#]:not([href=#])').on('click', hyperlinkClickHandler);

  // $('.play-embed').on('click', playVideoHandler);

  $(window).on('scroll', windowScrollHandler);

  $('body').scrollspy({
      target: '#scrollspy-course',
      offset: 100
  });

  function windowScrollHandler() {
    $(window).scrollTop() > 0 && $('#navbar-wrap').addClass('scrolled') || $('#navbar-wrap').removeClass('scrolled');
  }

  // function playVideoHandler() {
  //   $(this).css('display', 'none');
  //   $(this).next('.embed-responsive').css('display', 'block');
  //
  //   $(this).next().find('iframe').attr('src', 'https://www.youtube.com/embed/vKX_pBZZ21M?autoplay=1');
  // }

  function hyperlinkClickHandler() {
    if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {

        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
        if ($(window).width() > 768) {
            if (target.length) {
                $('html,body').animate({
                    scrollTop: target.offset().top - 80
                }, 1000);
                return false;
            }
        } else {
            if (target.length) {
                $('html,body').animate({
                    scrollTop: target.offset().top - 80
                }, 1000);
                return false;
            }
        }
    }
  }

  var courseLength = $('#course-slide').find('.item').length;

  function updateSlideNumber(current, length) {
    $('#arrow-control').find('span').html(current + '/' + length);
  }

  updateSlideNumber(1, courseLength);

  $('#course-slide').slick({
    // arrows: false,
    // autoplay: true,
    // autoplaySpeed: 5000,
    slidesToShow: 1,
    centerPadding: "0",
    arrows: true,
    centerMode: true,
    appendArrows: $('#arrow-control, #control-buttons'),
    prevArrow: '<i class="fa fa-angle-left"></i>',
    nextArrow: '<i class="fa fa-angle-right"></i>',
    responsive: [
      {
        breakpoint: 767,
        settings: {
          appendArrows: $('#arrow-control'),
          adaptiveHeight: true,
          dots: false,
        }
      }
    ]
  });

  $('#review-slide').slick({
    // arrows: false,
    // autoplay: true,
    // autoplaySpeed: 5000,
    // adaptiveHeight: false,
    slidesToShow: 1,
    centerPadding: "0",
    arrows: true,
    centerMode: true,
    appendArrows: $('#review-arrow-control'),
    prevArrow: '<i class="fa fa-angle-left"></i>',
    nextArrow: '<i class="fa fa-angle-right"></i>',
    dots: false,
    // responsive: [
    //   {
    //     breakpoint: 767,
    //     settings: {
    //
    //     }
    //   }
    // ]
  });

  $('#course-slide').on('afterChange', function (slick, currentSlide) {
    var items = $('#course-slide').find('.slick-slide');
    var current = $('#course-slide').find('.slick-current');
    updateSlideNumber($(current).attr('data-slide-number'), courseLength);
  });

  $('#control-buttons').find('.fa-angle-left').on('click', function () {
    triggerPrevSlide();
  });

  $('#control-buttons').find('.fa-angle-right').on('click', function () {
    triggerNextSlide();
  });

  function triggerPrevSlide() {
    $('#arrow-control').find('.fa-angle-left').trigger('click');
  }

  function triggerNextSlide() {
    $('#arrow-control').find('.fa-angle-right').trigger('click');
  }

})();
