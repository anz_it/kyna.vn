<?php
$dir_name = dirname(dirname(dirname(__FILE__)));
?>
<!DOCTYPE html>
<html lang="en">

    <!-- Mirrored from mana.edu.vn/cbp/khoa-hoc-ky-nang-giao-tiep/ by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 27 Apr 2016 13:29:41 GMT -->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Đào tạo quản lý cấp trung - Mana.edu.vn</title>
        <link href='https://fonts.googleapis.com/css?family=Roboto:400,300,500,700' rel='stylesheet' type='text/css'>
        <!-- Bootstrap --> 
        <link href="css/bootstrap.min.css" rel="stylesheet"/>
        <link href="css/site.css" rel="stylesheet"/>
        <link href="css/owl.carousel.css" rel="stylesheet"/>
        <link href="css/owl.theme.css" rel="stylesheet"/>
        <link href="css/owl.transitions.css" rel="stylesheet"/>
        <?php include $dir_name."/facebook_pixel.php"; ?>
    </head>
    <body>
        <?php
        // get các tham số cần thiết */
        $getParams = $_GET;
        $utm_source = '';
        $utm_medium = '';
        $utm_campaign = '';

        if (isset($getParams['utm_source']) && trim($getParams['utm_source']) != '') {
            $utm_source = trim($getParams['utm_source']);
        }
        if (isset($getParams['utm_medium']) && trim($getParams['utm_medium']) != '') {
            $utm_medium = trim($getParams['utm_medium']);
        }
        if (isset($getParams['utm_campaign']) && trim($getParams['utm_campaign']) != '') {
            $utm_campaign = trim($getParams['utm_campaign']);
        }
        ?>
        <nav class="navbar navbar-inverse header-menu navbar-fixed-top scroll-link">
            <div class="container">
                <div class="col-sm-3">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                                data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="#">
                            <img class="logo-brand" src="imgs/1_Logo.png"/>
                        </a>
                        <a href="#regis" class="button mb">ĐĂNG KÝ NGAY</a>
                    </div>
                </div>
                <div class="col-sm-9 col-xs-12">
                    <div id="navbar" class="navbar-collapse collapse">
                        <ul class="nav navbar-nav main-menu">
                            <li class="active"><a href="#intro">GIỚI THIỆU</a></li>
                            <li><a href="#part">HỌC PHẦN</a></li>
                            <li><a href="#method">HÌNH THỨC HỌC</a></li>
                            <li><a href="#regis">ĐĂNG KÝ</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </nav>

        <div id="banner">
            <div class="wrapper-slider scroll-link">
                <ul>
                    <li>
                        <div class="item">
                            <img class="img-slider img-responsive" src="imgs/1_BG/1_BG1.png" alt="First slide"/>
                            <div class="container">
                                <div class="carousel-caption">
                                    <img class="logo-slider img-responsive" src="imgs/1_Logo.png"/>
                                    <p>Chương trình <span class="bold">"Đào tạo quản lý cấp trung"</span><br /> do Hiệp hội doanh nhân Hoa Kỳ (IBTA) cấp chứng chỉ.</p>
                                    <a class="btn btn-lg btn-primary btn-regis-slider" href="#regis" role="button">ĐĂNG KÝ</a>
                                </div>
                            </div>
                        </div><!--end .item-->
                    </li>

                    <li>
                        <div class="item">
                            <img class="img-slider img-responsive" src="imgs/1_BG/4_Slide2.png" alt="Second slide"/>
                            <div class="container">
                                <div class="carousel-caption">
                                    <img class="logo-slider img-responsive" src="imgs/1_Logo.png"/>
                                    <p>Nhận bằng CBP Executive do Hiệp hội<br /> doanh nhân Hoa Kỳ cấp – Công nhận quốc tế</p>
                                    <a class="btn btn-lg btn-primary btn-regis-slider" href="#regis" role="button">ĐĂNG KÝ</a>
                                </div>
                            </div>
                        </div><!--end .item-->
                    </li>

                    <li>
                        <div class="item">
                            <img class="img-slider img-responsive" src="imgs/1_BG/5_Slide3.png" alt="Third slide"/>
                            <div class="container">
                                <div class="carousel-caption">
                                    <img class="logo-slider img-responsive" src="imgs/1_Logo.png"/>
                                    <p>Học online mọi lúc mọi nơi cùng doanh nhân hàng đầu</p>
                                    <a class="btn btn-lg btn-primary btn-regis-slider" href="#regis" role="button">ĐĂNG KÝ</a>
                                </div>
                            </div>
                        </div><!--end .item-->
                    </li>


                    <li>
                        <div class="item">
                            <img class="img-slider img-responsive" src="imgs/1_BG/6_Slide4.png" alt="Third slide"/>
                            <div class="container">
                                <div class="carousel-caption">
                                    <img class="logo-slider img-responsive" src="imgs/1_Logo.png"/>
                                    <p>MANA – Học viện đào tạo quản trị kinh doanh trực tuyến<br /> hàng đầu Việt Nam</p>
                                    <a class="btn btn-lg btn-primary btn-regis-slider" href="#regis" role="button">ĐĂNG KÝ</a>
                                </div>
                            </div>
                        </div><!--item-->
                    </li>
                </ul>
            </div><!--end .wrapper-slider-->
        </div><!--end #banner-->

        <div class="container" id="intro">
            <div class="row">
                <div class="col-md-4 col-sm-3"></div>
                <div class="col-md-1 col-sm-1 line"><hr/></div>
                <div class="col-md-2 col-sm-4"><h3 class="intro-heading"> GIỚI THIỆU </h3></div>
                <div class="col-md-1 col-sm-1 line"><hr/></div>
                <div class="col-md-4 col-sm-3"></div>

            </div>

            <div class="row">
                <div class='text-intro'>
                    <p>Chương trình <b class="main-color">“Đào tạo quản lý cấp trung”</b> được thiết kế, giảng dạy
                        bởi các chuyên gia tại Học viện đào tạo quản trị kinh doanh trực tuyến MANA (MANA business school) và được
                        Hiệp Hội Đào tạo Kinh doanh Hoa Kỳ (International Business Training Association) công nhận. Sau khi hoàn
                        thành đầy đủ các học phần và bài kiểm tra trực tuyến, học viên sẽ được cấp chứng chỉ CBP Executive, được công nhận toàn cầu.</p>
                </div>
            </div>

            <div class="row circle-content">
                <div class="col-lg-4 col-sm-6 col-xs-12">
                    <div class="circle circle-first">
                        <div class="circle-text">
                            <p><br />5 KHÓA HỌC<br/></p>
                        </div>
                    </div>
                    <div class="arrow-circle">
                    </div>
                </div><!--end .col-sm-4-->

                <div class="col-lg-3 col-sm-6 col-xs-12">
                    <div class="circle circle-middle">
                        <div class="circle-text">
                            <p>THI TRỰC TUYẾN</p>
                        </div>
                    </div>
                </div><!--end .col-sm-3-->

                <div class="col-lg-1 col-xs-12">
                    <div class="arrow-circle arrow-circle-second"></div>
                </div><!--end .col-sm-1-->

                <div class="col-lg-4 col-sm-12 col-xs-12">
                    <div class="circle circle-last">
                        <div class="circle-text">
                            <p>CBP<br />EXECUTIVE</p>
                        </div>
                    </div>
                </div><!--end .col-sm-4-->

            </div>
            <div class="row">
                <div class="text-details clearfix">
                    <div class="col-sm-7">
                        <img src="imgs/1_BG/2_BG2.png" class="img-responsive"/>
                    </div>
                    <div class="col-sm-5">
                        <div class="text-details-content">
                            <p class="text">Chương trình “Đào tạo quản lý cấp trung” của MANA business school sẽ cung cấp và rèn luyện cho bạn :</p>
                            <ul>
                                <li><p><span>1. </span>Đầy đủ các kiến thức quản lý, kinh doanh.</p></li>
                                <li><p><span>2. </span>-	Kỹ năng cần có khác như giao tiếp trong kinh doanh, đàm phán.</p></li>

                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div><!--end #intro-->

        <div class="section-desc" id="term">
            <div class="section-desc-text-inner">
                <p><br/>
                    Chương trình phù hợp với: Chuyên viên kinh doanh, Chuyên viên Marketing, Phát triển Kinh Doanh, Quản lý dự án.</p>
            </div><!--end .section-desc-text-inner-->
        </div><!--end .section-desc-->

        <div class="container study-heading" id="part">
            <div class="row">
                <div class="col-md-4 col-sm-3"></div>
                <div class="col-md-1 col-sm-1 line"><hr/></div>
                <div class="col-md-2 col-sm-4"><h3 class="intro-heading"> HỌC PHẦN </h3></div>
                <div class="col-md-1 col-sm-1 line"><hr/></div>
                <div class="col-md-4 col-sm-3"></div>
            </div>
            <div class="row">
                <div class="header-intro-text">
                    <div class="col-sm-3">
                    </div>
                    <div class="col-sm-6">
                        <p>Để trở thành một trưởng nhóm tài năng, bạn cần rèn luyện các kiến thức và kỹ năng sau:</p>
                    </div>
                    <div class="col-sm-3">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="content-collapse">
                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingOne">
                                <h4 class="panel-title">
                                    <span class="circle-bullet">1</span>
                                    <span class="title">Kỹ năng lãnh đạo chuyên nghiệp</span>
                                    <a class="accordion-toggle" role="button" data-toggle="collapse" data-parent="#accordion"
                                       href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel"
                                 aria-labelledby="headingOne">
                                <div class="panel-body">
                                    <ul>
                                        <li>Các chuẩn mực lãnh đạo hiệu quả</li>
                                        <li>Phong cách lãnh đạo phù hợp với doanh nghiệ</li>
                                        <li>Phát triển tầm nhìn và sứ mệnh</li>
                                        <li>Kỹ năng ra quyết định nhanh chóng</li>
                                        <li>Xây dựng đội ngũ</li>
                                        <li>Tạo động lực cho nhân sự</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingTwo">
                                <h4 class="panel-title">
                                    <span class="circle-bullet">2</span>
                                    <span class="title">Kỹ năng thấu hiểu và chăm sóc khách hàng</span>
                                    <a class="accordion-toggle collapsed" role="button" data-toggle="collapse"
                                       data-parent="#accordion" href="#collapseTwo" aria-expanded="false"
                                       aria-controls="collapseTwo">
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                <div class="panel-body">
                                    <ul>
                                        <li>Kỹ năng phân tích và thấu hiểu khách hàng</li>
                                        <li>Giao tiếp, thuyết phục và chăm sóc khách hàng</li>
                                        <li>Các phương tiện để chăm sóc khách hàng ứng dụng Internet</li>
                                        <li>Chiến lược quản lý thời gian và cảm xúc khi giao tiếp với khách hàng</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingThree">
                                <h4 class="panel-title">
                                    <span class="circle-bullet">3</span>
                                    <span class="title">Kỹ năng sales và thuyết phục khách hàng</span>
                                    <a class="accordion-toggle collapsed" role="button" data-toggle="collapse"
                                       data-parent="#accordion" href="#collapseThree" aria-expanded="false"
                                       aria-controls="collapseThree">
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseThree" class="panel-collapse collapse" role="tabpanel"
                                 aria-labelledby="headingThree">
                                <div class="panel-body">
                                    <ul>
                                        <li>Chiến lược phân tích nhu cầu khách hàng</li>
                                        <li>Thuyết trình sản phẩm hiệu quả</li>
                                        <li>Chiến lược chốt sales và xử lý từ chối</li>
                                        <li>Chiến lược xây dựng dịch vụ hậu mãi chất lượng</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingFour">
                                <h4 class="panel-title">
                                    <span class="circle-bullet">4</span>
                                    <span class="title">Kỹ năng giao tiếp trong kinh doanh</span>
                                    <a class="accordion-toggle collapsed" role="button" data-toggle="collapse"
                                       data-parent="#accordion" href="#collapseFour" aria-expanded="false"
                                       aria-controls="collapseFour">
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseFour" class="panel-collapse collapse" role="tabpanel"
                                 aria-labelledby="headingFour">
                                <div class="panel-body">
                                    <ul>
                                        <li>Giới thiệu về giao tiếp trong kinh doanh</li>
                                        <li>Quy trình giao tiếp trong kinh doanh</li>
                                        <li>Phát triển phong cách viết trong kinh doanh</li>
                                        <li>Phát triển kỹ năng giao tiếp bằng lời</li>
                                        <li>Phát triển kỹ năng giao tiếp phi ngôn ngữ</li>
                                        <li>Kỹ năng trình bày hiệu quả</li>
                                        <li>Giải quyết mâu thuẫn và bất đồng trong giao tiếp</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingFive">
                                <h4 class="panel-title">
                                    <span class="circle-bullet">5</span>
                                    <span class="title">Các nghi thức xã giao trong kinh doanh</span>
                                    <a class="accordion-toggle collapsed" role="button" data-toggle="collapse"
                                       data-parent="#accordion" href="#collapseFive" aria-expanded="false"
                                       aria-controls="collapseFive">
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseFive" class="panel-collapse collapse" role="tabpanel"
                                 aria-labelledby="headingFive">
                                <div class="panel-body">
                                    <ul>
                                        <li>Chào hỏi và giới thiệu</li>
                                        <li>Các nghi thức xã giao trong hội họp, chiêu đãi</li>
                                        <li>Các nghi thức xã giao trên điện thoại, Internet</li>
                                        <li>Nghi thức xã giao trong môi trường toàn cầu hóa</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="study-method" id="method">
            <div class="container">
                <div class="row">
                    <div class="col-sm-3">
                    </div>
                    <div class="col-sm-1 line">
                        <hr/>
                    </div>
                    <div class="col-sm-4">
                        <h3 class="intro-heading"> HÌNH THỨC HỌC </h3>
                    </div>
                    <div class="col-sm-1 line">
                        <hr/>
                    </div>
                    <div class="col-sm-3">
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-3">
                        <div class="circle-method circle-calendar"><i class="icon-calendar"></i> </div>
                        <div class="study-method-text">
                            <h4>Học mọi lúc mọi nơi</h4>
                            <p>Học qua video bài giảng được biên tập chuyên nghiệp.  Tài liệu bao gồm sách và bài thuyết trình, tư liệu tham khảo</p>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="circle-method circle-question"><i class="icon-question"></i></div>
                        <div class="study-method-text">
                            <h4>Hỏi đáp cùng chuyên gia</h4>
                            <p>Cố vấn 1-2-1 (1 kèm 1) trong suốt quá trình học và luyện thi. Hỏi đáp cùng chuyên gia, doanh nhân có kinh nghiệm</p>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="circle-method circle-pencil"><i class="icon-pencil"></i></div>
                        <div class="study-method-text">
                            <h4>Hỏi đáp và luyện thi</h4>
                            <p>Học đi đôi với hành trong suốt quá trình học. Rèn luyện cùng hơn 100 bài thi thử</p>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="circle-method circle-house"><i class="icon-house"></i></div>
                        <div class="study-method-text">
                            <h4>Thi trực tuyến và nhận bằng quốc tế</h4>
                            <p>Thi trực tuyến mọi lúc mọi nơi. 12 tháng để rèn luyện thoải mái trước khi thi</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="registration" id="regis">
            <div class="container resgistration-border">
                <div class="row">
                    <div class="col-sm-3">
                    </div>
                    <div class="col-sm-1 line">
                        <hr/>
                    </div>
                    <div class="col-sm-4">
                        <h3 class="intro-heading"> ĐĂNG KÝ NHẬN TƯ VẤN </h3>
                    </div>
                    <div class="col-sm-1 line">
                        <hr/>
                    </div>
                    <div class="col-sm-3">
                    </div>
                </div>
                <form action="#" class="form-horizontal" id="form_advice" method="post" accept-charset="utf-8">
                    <div class="row">

                        <div class="text-intro-regist">
                            <p>và <b>HỌC BỔNG 1,000,000 ĐỒNG</b> dành riêng cho bạn</p>
                        </div>
                        <div class="regist-form">
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <input type="text" id="name" class="input-group form-control" required  placeholder="Họ Và Tên"/>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <input type="text" id="phone" class="input-group form-control" required placeholder="Số Điện Thoại"/>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <input type="text" id="email" class="input-group form-control" required  placeholder="Email"/>
                                </div>
                            </div>
                        </div>
                        <div class="button-regist">
                            <button type="submit"  id="dang_ky_form" class="btn btn-primary btn-regist">ĐĂNG KÝ</button>
                        </div>
                        <div class="information-text">
                            <p>Bộ phận chăm sóc khách hàng của MANA Business School sẽ liên lạc sớm với bạn để tư vấn về khóa học (mức phí tư vấn là 0đ).</p>
                        </div>

                    </div>
                </form>
            </div>
        </div>
        <footer>
            <div class="container footer">
                <div class="row">
                    <div class="col-sm-3">
                        <div class="footer-logo">
                            <ul>
                                <li>
                                    <a href="#">
                                        <img src="imgs/1_Logo.png" class="img-responsive"/>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <img src="imgs/18_KynaLogo.png" class="img-responsive"/>
                                    </a>
                                </li>
                            </ul>

                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="footer-text-center">
                            <h4>Công ty Cổ phần  Dream Việt Education</h4>
                            <p> <b class="company-address"> Trụ sở chính:</b>  Lầu 6 Tòa nhà Thịnh Phát - 178/8 Đường D1, Phường 25, Quận Bình Thạnh, TP Hồ Chí Minh</p>
                            <p> <b class="company-address"> Văn phòng Hà Nội:</b>  Phòng 604 tháp A, Hà Thành Plaza 102 Thái Thịnh, Đống Đa, TP Hà Nội </p>
                            <p style="padding-top: 20px">Giấy phép ĐKKD số 0313589030 do Sở Kế hoạch và Đầu tư TPHCM cấp</p>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="footer-hot-line">
                            <p><b class="company-address">Hotline:</b>  1900 6364 09</p>
                            <p>Thứ 2 – thứ 6: từ 08h30 – 21h00</p>
                            <p>Thứ 7: 08h30 – 17h00</p>
                            <p><b class="company-address">Email:</b></p> hotro@kyna.vn
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <div class="modal fade" id="modal">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="popup_body">
                            <div class="alert alert-success" role="alert">Cám ơn bạn đã để lại thông tin. Kyna.vn sẽ sớm liên hệ với bạn để tư vấn thêm về chương trình học!</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="js/bootstrap.min.js"></script>
        <script type="text/javascript" src="js/owl.carousel.min.js"></script>
        <script>
            $(function () {
                $('.logo-brand').data('size', 'big');
            });

            $(window).scroll(function () {
                if ($(document).scrollTop() > 0)
                {
                    if ($('.logo-brand').data('size') == 'big')
                    {
                        $('.logo-brand').data('size', 'small');
                        $('.logo-brand').stop().animate({
                            height: '40px'
                        }, 600);
                        $('.navbar').addClass('menufix');
                    }
                } else
                {
                    if ($('.logo-brand').data('size') == 'small')
                    {
                        $('.logo-brand').data('size', 'big');
                        $('.logo-brand').stop().animate({
                            height: '50px'
                        }, 600);

                        $('.navbar').removeClass('menufix');
                    }
                }
            });
        </script>
        <!-- Nhan them vao -->
        <script>
            $(document).ready(function () {
                $("#dang_ky_form").bind('click', function (e) {

                    if ($.trim($("#name").val()) != '' && $.trim($("#email").val()) != '' && $.trim($("#phone").val()) != '') {
                        e.preventDefault();
                        var url = 'https://docs.google.com/forms/d/e/1FAIpQLScXh253MYnBsAV8iZFIkpDhpGj7pQgQgBEialmiKE7yxRVnBw/formResponse';
                        var data = {
                            'entry.1618386042': $("#name").val(),
                            'entry.1550829392': $("#email").val(),
                            'entry.1725316625': $("#phone").val(),
                            'entry.953023567': '<?php echo $utm_source; ?>',
                            'entry.823420574':'<?php echo $utm_medium;  ?>',
                            'entry.1198803187':'<?php echo $utm_campaign;  ?>'
                        };
                        $.ajax({
                            'url': url,
                            'method': 'POST',
                            'dataType': 'XML',
                            'data': data,
                            'statusCode': {
                                0: function () {
                                    $("#name").val('');
                                    $("#email").val('');
                                    $("#phone").val('');
                                    $("#modal").modal();
                                },
                                200: function () {
                                    $("#name").val('');
                                    $("#email").val('');
                                    $("#phone").val('');
                                    $("#modal").modal();
                                }
                            }

                        });
                    }

                });

                $(".wrapper-slider ul").owlCarousel({
                    autoPlay: true,
                    items: 1,
                    itemsDesktop: [1199, 1],
                    itemsDesktopSmall: [979, 1],
                    itemsTablet: [768, 1],
                    itemsMobile: [479, 1],
                    mouseDrag: true,
                    autoPlay: 40000,
                            navigation: false,
                    pagination: true,
                });
            });


            $(function () {
                $('.scroll-link a[href*=#]:not([href=#])').click(function () {
                    if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {

                        var target = $(this.hash);
                        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                        if ($(window).width() > 768) {
                            if (target.length) {
                                $('html,body').animate({
                                    scrollTop: target.offset().top - 70
                                }, 1000);
                                return false;
                            }
                        } else {
                            if (target.length) {
                                $('html,body').animate({
                                    scrollTop: target.offset().top
                                }, 1000);
                                return false;
                            }
                        }
                    }
                });
            });

        </script>
        <?php
        include $dir_name."/ga_landing_page.php"; ?>
    </body>

    <!-- Mirrored from mana.edu.vn/cbp/khoa-hoc-ky-nang-giao-tiep/ by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 27 Apr 2016 13:31:05 GMT -->
</html>
