ActionJs = {
    Init: function () {
        $("#collapsing-navbar ul.navbar-nav > li").click(function (event) {
            event.preventDefault();
            id = $(this).find("a").attr("href");
            $('html, body').animate({
                scrollTop: $(id).offset().top - 70
            }, 1000);
        });
        $(".btn-register").click(function () {
            var goTo = $("#register").offset().top + 46;
            if ($(this).hasClass("current-about"))
                goTo = $("#about").offset().top - 70;
            $('html, body').animate({
                scrollTop: goTo
            }, 1000);
        });
    }
}
ActionJs.Init();
$(window).on("load scroll", function () {
    var top = $(this).scrollTop();
    if (top > 50) {
        if (!$("#menu").hasClass("action")) {
            $("#menu").addClass("action");
        }
    }
    else {
        $("#menu").removeClass("action");
    }
    var obj = $("[path='scrolling']");
    var current = $(obj[0]);
    for (var i = 1; i < $(obj).length; i++) {
        if (top - ($(obj[i]).offset().top - 70) >= 0)
            current = $(obj[i]);
    }
    var link_current = $("#collapsing-navbar ul.navbar-nav > li").find("a[href='#" + $(current).attr("id") + "']");
    if (!$(link_current).hasClass("active")) {
        $("#collapsing-navbar ul.navbar-nav > li a").removeClass("active");
        $(link_current).addClass("active");
    }
})