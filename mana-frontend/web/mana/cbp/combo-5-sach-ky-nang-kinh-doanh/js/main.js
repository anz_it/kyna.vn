ActionJs = {
    Init: function () {
        $("#collapsing-navbar ul.navbar-nav > li").click(function (event) {
            event.preventDefault();
            id = $(this).find("a").attr("href");
            $('html, body').animate({
                scrollTop: $(id).offset().top - 70
            }, 1000);
        });
        $(".btn-register, .btn-reg").click(function () {
            var goTo = $("#register").offset().top;
            $('html, body').animate({
                scrollTop: goTo
            }, 1000);
        });
        $(".btn-info").click(function () {
            var goTo = $("#skill").offset().top;
            $('html, body').animate({
                scrollTop: goTo - 70
            }, 1000);
        });
        $('.box-book-store ul li').click(function(){
            var li_item = $("#box-content .item");
            $('.box-book-store ul li').removeClass("clicked");
            $(li_item).removeClass("selected");
            var num = $(this).addClass("clicked").index();
            var space = $(window).height() - $(li_item[num]).height() - 70;
            var goTo = $(li_item[num]).addClass("selected").offset().top;
            if (space > 0)
                goTo -= space/2;
            if (space > 0)
                goTo -= 70;
            $('html, body').animate({
                scrollTop: goTo
            }, 1000);
        });
        $('.selected-book ul li').click(function(){
            $('.selected-book ul li').removeClass('selected')
            $(this).addClass('selected');
			var valu = $(this).attr('bookname');
            $('#advice_name').val('Mana - Bộ 5 sách kỹ năng trong kinh doanh - ' + valu);
			$("#bookname").val(valu);
        });
        if (window.matchMedia('(max-width: 767px)').matches) {
            $('#box-content').slick({
                centerMode: true,
                arrows: false,
                centerPadding: '0',
                slidesToShow: 1,
                responsive: [
                {
                    breakpoint: 767,
                    settings: {
                        arrows: true,
                        centerMode: true,
                        centerPadding: '0',
                        slidesToShow: 1,
                        prevArrow: "<div class='btn left icon-angle-left'></div>",
                        nextArrow: "<div class='btn right icon-angle-right'></div>"
                    }
                }
                ]
            });
            var obj = $("#box-content .item .detail");
            var max = 0;
            $(obj).each(function(){
                var _h = $(this).find(".name").height() + $(this).find(".des").height();
                if (max < _h)
                    max = _h;
            }).height(max + 40);
        }
    }
}
ActionJs.Init();
$(window).on("load scroll", function () {
    var top = $(this).scrollTop();
    if (top > 50) {
        if (!$("#menu").hasClass("action")) {
            $("#menu").addClass("action");
        }
    }
    else {
        $("#menu").removeClass("action");
    }
    var obj = $("[path='scrolling']");
    var current = $(obj[0]);
    for (var i = 1; i < $(obj).length; i++) {
        if (top - ($(obj[i]).offset().top - 70) >= 0)
            current = $(obj[i]);
    }
    var link_current = $("#collapsing-navbar ul.navbar-nav > li").find("a[href='#" + $(current).attr("id") + "']");
    if (!$(link_current).hasClass("active")) {
        $("#collapsing-navbar ul.navbar-nav > li a").removeClass("active");
        $(link_current).addClass("active");
    }
})