/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$(document).ready(function () {
    // check if multi form
    if ($('form').length > 1) {
        changeCityMulti();
        //submitFormMulti();
        //initPublisherObjMulti();
    } else {
        changeCity();
        //submitForm();
        //initPublisherObj();
    }
});

function changeCityMulti() {
    $("select[name='city']").bind('change', function () {
        var city_id = $(this).val();
        if (city_id > 0) {
            var action_form = $(this).parents("form").attr('action');
            $("select[name='district']").html('<option value="" >--- Đang lấy dữ liệu ----</option>');
            var queryDistrictURL = '/page/default/submit-query-district';
            $.ajax({
                type: "POST",
                url: queryDistrictURL,
                data: {city: city_id},
                dataType: "JSON",
                success: function (data) {
                    addDistrictElementMulti(data);
                },
                error: function (er) {
                    console.log(er);
                }
            });
        } else {
            $("select[name='district']").html('<option value="" >Chọn Quận/Huyện</option>');
        }
    });
}
function changeCity() {
    $("#city").bind('change', function () {
        var city_id = $(this).val();
        if (city_id > 0) {
            var action_form = $(this).parents("form").attr('action');
            $("#district").html('<option value="" >--- Đang lấy dữ liệu ----</option>');
            var queryDistrictURL = '/course/page/submit-query-district';
            $.ajax({
                type: "POST",
                url: queryDistrictURL,
                data: {city: city_id},
                dataType: "JSON",
                success: function (data) {
                    addDistrictElement(data);
                },
                error: function (er) {
                    console.log(er);
                }

            });
        } else {
            $("#district").html('<option value="" >Chọn Quận/Huyện</option>');
        }


    });
}

function addDistrictElement(data) {
    if (data.length > 0) {
        var Str_HTML = '';
        $.each(data, function (index, value) {
            Str_HTML += '<option value="' + value.id + '" >' + value.name + '</option>';
        });
        $("#district").html(Str_HTML);
    }
}
function initPublisherObj() {
    /* kiểm tra */

    if (typeof publisherObj !== 'undefined') {
        var myForm = $("#landing-page-id");
        var HTML = '';
        $.each(publisherObj, function (index, obj) {
            HTML += "<input type='hidden'  name='publisher[" + obj.publisherName + "]' value='" + obj.publisherValue + "' />";
        });
        if (myForm.length == 1) {
            $(HTML).prependTo(myForm);
        } else {
            var multiForm = $("form[name='landing-page-id']");
            if (multiForm.length > 0) {
                $.each(multiForm, function (index, vForm) {
                    $(HTML).prependTo(vForm);
                });
            }
        }

    }



}