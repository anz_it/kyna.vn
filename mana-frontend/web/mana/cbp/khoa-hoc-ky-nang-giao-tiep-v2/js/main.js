ActionJs = {
    Init: function () {
        $("#collapsing-navbar ul.navbar-nav > li").click(function (event) {
            event.preventDefault();
            id = $(this).find("a").attr("href");
            $('html, body').animate({
                scrollTop: $(id).offset().top - 70
            }, 1000);
        });
        $("#menu button.btn-register, #banner button.btn-register, #intro button.btn-register, #topbar").click(function () {
            $('html, body').animate({
                scrollTop: $('#register').offset().top -70
            }, 1000);
        });
        ActionJs.OpenVideoIntro();
    },
    //<iframe width="560" height="315" src="https://www.youtube.com/embed/EtCHNU4ynR0" frameborder="0" allowfullscreen></iframe>
    OpenVideoIntro: function(){
        $("#intro .video img").click(function(){
            $("#video-intro .modal-body .videoWrapper").html('<iframe width="560" height="315" src="https://www.youtube.com/embed/iB89dwU7Sng" frameborder="0" allowfullscreen></iframe>');
        })

        $("#intro .btn-close").click(function(){
            $("#video-intro .modal-body .videoWrapper").html('');
        })
        $('#video-intro').on('hidden.bs.modal', function () {
            $("#video-intro .modal-body .videoWrapper").html('');
        })
    },
}
ActionJs.Init();
$(window).on("load scroll", function () {
    var top = $(this).scrollTop();
    if (top > 50) {
        if (!$("#menu").hasClass("action")) {
            $("#menu").addClass("action");
        }
    }
    else {
        $("#menu").removeClass("action");
    }
    var obj = $("[path='scrolling']");
    var current = $(obj[0]);
    for (var i = 1; i < $(obj).length; i++) {
        if (top - ($(obj[i]).offset().top - 70) >= 0)
            current = $(obj[i]);
    }
    var link_current = $("#collapsing-navbar ul.navbar-nav > li").find("a[href='#" + $(current).attr("id") + "']");
    if (!$(link_current).hasClass("active")) {
        $("#collapsing-navbar ul.navbar-nav > li a").removeClass("active");
        $(link_current).addClass("active");
    }
})
