<?php
$dir_name = dirname(dirname(dirname(__FILE__)));
?>
<!DOCTYPE html>
<html lang="en">

    <!-- Mirrored from mana.edu.vn/cbp/khoa-hoc-ky-nang-giao-tiep/ by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 27 Apr 2016 13:29:41 GMT -->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Kỹ năng bán hàng chuyên nghiệp - Mana.edu.vn</title>
        <link href='https://fonts.googleapis.com/css?family=Roboto:400,300,500,700' rel='stylesheet' type='text/css'>
        <!-- Bootstrap --> 
        <link href="/mana/cbp/khoa-hoc-ky-nang-ban-hang-chuyen-nghiep/css/bootstrap.min.css" rel="stylesheet"/>
        <link href="/mana/cbp/khoa-hoc-ky-nang-ban-hang-chuyen-nghiep/css/site.css" rel="stylesheet"/>
        <link href="/mana/cbp/khoa-hoc-ky-nang-ban-hang-chuyen-nghiep/css/owl.carousel.css" rel="stylesheet"/>
        <link href="/mana/cbp/khoa-hoc-ky-nang-ban-hang-chuyen-nghiep/css/owl.theme.css" rel="stylesheet"/>
        <link href="/mana/cbp/khoa-hoc-ky-nang-ban-hang-chuyen-nghiep/css/owl.transitions.css" rel="stylesheet"/>
        <?php include $dir_name."/facebook_pixel.php"; ?>
    </head>
    <body>
        <?php
        // get các tham số cần thiết */
        $getParams = $_GET;
        $utm_source = '';
        $utm_medium = '';
        $utm_campaign = '';

        if (isset($getParams['utm_source']) && trim($getParams['utm_source']) != '') {
            $utm_source = trim($getParams['utm_source']);
        }
        if (isset($getParams['utm_medium']) && trim($getParams['utm_medium']) != '') {
            $utm_medium = trim($getParams['utm_medium']);
        }
        if (isset($getParams['utm_campaign']) && trim($getParams['utm_campaign']) != '') {
            $utm_campaign = trim($getParams['utm_campaign']);
        }
        ?>
        <nav class="navbar navbar-inverse header-menu navbar-fixed-top scroll-link">
            <div class="container">
                <div class="col-sm-3">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                                data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="#">
                            <img class="logo-brand" src="/mana/cbp/khoa-hoc-ky-nang-ban-hang-chuyen-nghiep/imgs/1_Logo.png"/>
                        </a>
                        <a href="#regis" class="button mb">ĐĂNG KÝ NGAY</a>
                    </div>
                </div>
                <div class="col-sm-9 col-xs-12">
                    <div id="navbar" class="navbar-collapse collapse">
                        <ul class="nav navbar-nav main-menu">
                            <li class="active"><a href="#intro">GIỚI THIỆU</a></li>
                            <li><a href="#part">HỌC PHẦN</a></li>
                            <li><a href="#method">HÌNH THỨC HỌC</a></li>
                            <li><a href="#regis">ĐĂNG KÝ</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </nav>

        <div id="banner">
            <div class="wrapper-slider scroll-link">
                <ul>
                    <li>
                        <div class="item">
                            <img class="img-slider img-responsive" src="/mana/cbp/khoa-hoc-ky-nang-ban-hang-chuyen-nghiep/imgs/1_BG/1_BG1.png" alt="First slide"/>
                            <div class="container">
                                <div class="carousel-caption">
                                    <img class="logo-slider img-responsive" src="/mana/cbp/khoa-hoc-ky-nang-ban-hang-chuyen-nghiep/imgs/1_Logo.png"/>
                                    <p>Chương trình <span class="bold">"Đào tạo kỹ năng bán hàng chuyên nghiệp"</span> do Hiệp hội doanh nhân <span class="bold">Quốc tế</span> (IBTA) cấp chứng chỉ.</p>
                                    <a class="btn btn-lg btn-primary btn-regis-slider" href="#regis" role="button">ĐĂNG KÝ</a>
                                </div>
                            </div>
                        </div><!--end .item-->
                    </li>

                    <li>
                        <div class="item">
                            <img class="img-slider img-responsive" src="/mana/cbp/khoa-hoc-ky-nang-ban-hang-chuyen-nghiep/imgs/1_BG/4_Slide2.png" alt="Second slide"/>
                            <div class="container">
                                <div class="carousel-caption">
                                    <img class="logo-slider img-responsive" src="/mana/cbp/khoa-hoc-ky-nang-ban-hang-chuyen-nghiep/imgs/1_Logo.png"/>
                                    <p>Nhận bằng CBP Selling Skills do Hiệp hội doanh nhân <span class="bold">Quốc tế cấp</span> – <span class="bold">Công nhận trên toàn cầu</span></p>
                                    <a class="btn btn-lg btn-primary btn-regis-slider" href="#regis" role="button">ĐĂNG KÝ</a>
                                </div>
                            </div>
                        </div><!--end .item-->
                    </li>

                    <li>
                        <div class="item">
                            <img class="img-slider img-responsive" src="/mana/cbp/khoa-hoc-ky-nang-ban-hang-chuyen-nghiep/imgs/1_BG/5_Slide3.png" alt="Third slide"/>
                            <div class="container">
                                <div class="carousel-caption">
                                    <img class="logo-slider img-responsive" src="/mana/cbp/khoa-hoc-ky-nang-ban-hang-chuyen-nghiep/imgs/1_Logo.png"/>
                                    <p>Học online mọi lúc mọi nơi cùng doanh nhân hàng đầu</p>
                                    <a class="btn btn-lg btn-primary btn-regis-slider" href="#regis" role="button">ĐĂNG KÝ</a>
                                </div>
                            </div>
                        </div><!--end .item-->
                    </li>


                    <li>
                        <div class="item">
                            <img class="img-slider img-responsive" src="/mana/cbp/khoa-hoc-ky-nang-ban-hang-chuyen-nghiep/imgs/1_BG/6_Slide4.png" alt="Third slide"/>
                            <div class="container">
                                <div class="carousel-caption">
                                    <img class="logo-slider img-responsive" src="/mana/cbp/khoa-hoc-ky-nang-ban-hang-chuyen-nghiep/imgs/1_Logo.png"/>
                                    <p>MANA – Học viện đào tạo quản trị kinh doanh trực tuyến <br/>hàng đầu Việt Nam</p>
                                    <a class="btn btn-lg btn-primary btn-regis-slider" href="#regis" role="button">ĐĂNG KÝ</a>
                                </div>
                            </div>
                        </div><!--item-->
                    </li>
                </ul>
            </div><!--end .wrapper-slider-->
        </div><!--end #banner-->

        <div class="container" id="intro">
            <div class="row">
                <div class="col-md-4 col-sm-3"></div>
                <div class="col-md-1 col-sm-1 line"><hr/></div>
                <div class="col-md-2 col-sm-4"><h3 class="intro-heading"> GIỚI THIỆU </h3></div>
                <div class="col-md-1 col-sm-1 line"><hr/></div>
                <div class="col-md-4 col-sm-3"></div>

            </div>

            <div class="row">
                <div class='text-intro'>
                        <p>Chương trình <b class="main-color">"Đào tạo kỹ năng bán hàng chuyên nghiệp"</b> được thiết kế, giảng dạy bởi các chuyên gia tại Học viện đào tạo quản trị kinh doanh trực tuyến MANA (MANA business school) và được Hiệp hội Đào tạo Kinh doanh <span class="bold">Quốc tế</span> <span class="main-color">(International Business Training Association)</span> công nhận. Sau khi hoàn thành đầy đủ các học phần và bài kiểm tra trực tuyến, học viên sẽ được cấp chứng chỉ <span class="main-color">CBP Selling Skills</span>, được công nhận toàn cầu.</p>
                </div>
            </div>

            <div class="row circle-content">
                <div class="col-lg-4 col-sm-6 col-xs-12">
                    <div class="circle circle-first">
                        <div class="circle-text">
                            <p>KHÓA HỌC ĐÀO <br />TẠO KỸ NĂNG <br/> BÁN HÀNG CHUYÊN <br />NGHIỆP</p>
                        </div>
                    </div>
                    <div class="arrow-circle">
                    </div>
                </div><!--end .col-sm-4-->

                <div class="col-lg-3 col-sm-6 col-xs-12">
                    <div class="circle circle-middle">
                        <div class="circle-text">
                            <p>THI TRỰC TUYẾN</p>
                        </div>
                    </div>
                </div><!--end .col-sm-3-->

                <div class="col-lg-1 col-xs-12">
                    <div class="arrow-circle arrow-circle-second"></div>
                </div><!--end .col-sm-1-->

                <div class="col-lg-4 col-sm-12 col-xs-12">
                    <div class="circle circle-last">
                        <div class="circle-text">
                            <p>CBP SELLING <br />SKILLS</p>
                        </div>
                    </div>
                </div><!--end .col-sm-4-->

            </div>
            <div class="row">
                <div class="text-details clearfix">
                    <div class="col-sm-7">
                        <img src="/mana/cbp/khoa-hoc-ky-nang-ban-hang-chuyen-nghiep/imgs/1_BG/2_BG2.png" class="img-responsive"/>
                    </div>
                    <div class="col-sm-5">
                        <div class="text-details-content">
                            <p class="text">Chương trình “Đào tạo kỹ năng bán hàng chuyên nghiệp” của MANA business school sẽ cung cấp và rèn luyện cho bạn:</p>
                            <ul>
                                <li><p><span>1. </span>Kỹ năng về chiến lược, quy trình và động lực bán hàng.</p></li>
                                <li><p><span>2. </span>Chương trình “Đào tạo kỹ năng bán hàng chuyên nghiệp” sẽ là câu trả lời cho quá trình phấn đấu của bạn.</p></li>
                                <li><p><span>3. </span>Giúp bạn đánh giá bản thân và trang bị thêm những công cụ hiệu quả phục vụ cho công việc bán hàng.</p></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div><!--end #intro-->

        <div class="section-desc" id="term">
            <div class="section-desc-text-inner">
                <p>
                    Chương trình phù hợp với: <br/>Những chuyên viên, nhân viên văn phòng có nhu cầu rèn luyện kỹ năng và chuyên môn để trở thành trưởng nhóm. Đặc biệt phù hợp với những bạn đang làm việc trong lĩnh vực sales, marketing, phát triển kinh doanh.
                </p>
            </div><!--end .section-desc-text-inner-->
        </div><!--end .section-desc-->

        <div class="container study-heading" id="part">
            <div class="row">
                <div class="col-md-4 col-sm-3"></div>
                <div class="col-md-1 col-sm-1 line"><hr/></div>
                <div class="col-md-2 col-sm-4"><h3 class="intro-heading"> HỌC PHẦN </h3></div>
                <div class="col-md-1 col-sm-1 line"><hr/></div>
                <div class="col-md-4 col-sm-3"></div>
            </div>
            <div class="row">
                <div class="header-intro-text">
                    <div class="col-sm-3">
                    </div>
                    <div class="col-sm-6">
                        <p>Để trở thành một nhân viên chăm sóc khách hàng tài năng, bạn cần rèn luyện các kiến thức và kỹ năng sau:</p>
                    </div>
                    <div class="col-sm-3">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="content-collapse">
                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingOne">
                                <h4 class="panel-title">
                                    <span class="circle-bullet">1</span>
                                    <span class="title">Giới thiệu về bán hàng</span>
                                    <a class="accordion-toggle" role="button" data-toggle="collapse" data-parent="#accordion"
                                       href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel"
                                 aria-labelledby="headingOne">
                                <div class="panel-body">
                                    <ul>
                                        <li>Khái niệm bán hàng và mối liên hệ giữa người bán và người mua</li>
                                        <li>Nền tảng bán hàng: Các yếu tố cần có để trở thành người bán hàng chuyên nghiệp</li>
                                        <li>Những cách bán hàng phổ biến</li>
                                        <li>Giới thiệu quy trình bán hàng &amp; các giai đoạn bán hàng</li>
                                        <li>Tìm hiểu về sản phẩm của bạn</li>
                                        <li>Tầm quan trọng của thái độ bán hàng tích cực</li>
                                        <li>Phát triển sự hào hứng tự nhiên thông qua quá trình bán hàng</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingTwo">
                                <h4 class="panel-title">
                                    <span class="circle-bullet">2</span>
                                    <span class="title">TÌM KIẾM KHÁCH HÀNG TIỀM NĂNG (PROSPECTING STAGE)</span>
                                    <a class="accordion-toggle collapsed" role="button" data-toggle="collapse"
                                       data-parent="#accordion" href="#collapseTwo" aria-expanded="false"
                                       aria-controls="collapseTwo">
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                <div class="panel-body">
                                    <ul>
                                        <li>Định nghĩa giai đoạn tìm kiếm khách hàng tiềm năng. (Prospecting Stage)</li>
                                        <li>Khách hàng tiềm năng là gì?</li>
                                        <li>Hồ sơ khách hàng</li>
                                        <li>Phân loại các kênh tìm kiếm khách hàng tiềm năng (Channel Ratings)</li>
                                        <li>Các kênh tìm kiếm khách hàng đầu mối (Lead Channels)</li>
                                        <li>Tầm quan trọng của việc xác định những yếu tố ảnh hưởng mua sắm của KH tiềm năng</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingThree">
                                <h4 class="panel-title">
                                    <span class="circle-bullet">3</span>
                                    <span class="title">GIAI ĐOẠN LẦN ĐẦU TIÊN TIẾP XÚC KHÁCH HÀNG (FIRST CONTACT STAGE)</span>
                                    <a class="accordion-toggle collapsed" role="button" data-toggle="collapse"
                                       data-parent="#accordion" href="#collapseThree" aria-expanded="false"
                                       aria-controls="collapseThree">
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseThree" class="panel-collapse collapse" role="tabpanel"
                                 aria-labelledby="headingThree">
                                <div class="panel-body">
                                    <ul>
                                        <li>Giới thiệu về giai đoạn lần đầu tiên tiếp xúc khách hàng (First Contact Stage)</li>
                                        <li>Các chiến lược trong giai đoạn tiếp xúc lần đầu tiên</li>
                                        <li>Các chiến lược khác</li>
                                        <li>Bốn bước quan trọng trong giai đoạn lần đầu tiên tiếp xúc khách hàng</li>
                                        <li>Attention Grabbers - Những cách để gợi sự chú ý của người nghe</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingFour">
                                <h4 class="panel-title">
                                    <span class="circle-bullet">4</span>
                                    <span class="title">GIAI ĐOẠN ĐÁNH GIÁ CHẤT LƯỢNG KHÁCH HÀNG (QUALIFICATION STAGE)</span>
                                    <a class="accordion-toggle collapsed" role="button" data-toggle="collapse"
                                       data-parent="#accordion" href="#collapseFour" aria-expanded="false"
                                       aria-controls="collapseFour">
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseFour" class="panel-collapse collapse" role="tabpanel"
                                 aria-labelledby="headingFour">
                                <div class="panel-body">
                                    <ul>
                                        <li>Giới thiệu về giai đoạn đánh giá chất lượng khách hàng (Qualification Stage)</li>
                                        <li>Những yếu tố trong giai đoạn đánh giá chất lượng khách hàng</li>
                                        <li>Các bước quan trọng trong giai đoạn đánh giá chất lượng khách hàng</li>
                                        <li>Các câu hỏi giúp khai thác thông tin khách hàng</li>
                                        <li>Các chiến lược lắng nghe khách hàng hiệu quả</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingFive">
                                <h4 class="panel-title">
                                    <span class="circle-bullet">5</span>
                                    <span class="title">CHIẾN LƯỢC THUYẾT TRÌNH HIỆU QUẢ (PRESENTATION STAGE)</span>
                                    <a class="accordion-toggle collapsed" role="button" data-toggle="collapse"
                                       data-parent="#accordion" href="#collapseFive" aria-expanded="false"
                                       aria-controls="collapseFive">
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseFive" class="panel-collapse collapse" role="tabpanel"
                                 aria-labelledby="headingFive">
                                <div class="panel-body">
                                    <ul>
                                        <li>Mục tiêu của việc thuyết trình</li>
                                        <li>Truyền tải bài thuyết trình đến đúng khách hàng tiềm năng cụ thể</li>
                                        <li>Những động cơ mua sắm của người mua</li>
                                        <li>Những chiến lược bằng chứng thành công</li>
                                        <li>Đánh giá phản hồi của khách hàng</li>
                                        <li>Chìa khóa quan trọng để tạo một bài thuyết trình hiệu quả</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingSix">
                                <h4 class="panel-title">
                                    <span class="circle-bullet">6</span>
                                    <span class="title">GIAI ĐOẠN GIẢI QUYẾT SỰ TỪ CHỐI CỦA KHÁCH HÀNG (OBJECTION RESOLUTION STAGE)</span>
                                    <a class="accordion-toggle collapsed" role="button" data-toggle="collapse"
                                       data-parent="#accordion" href="#collapseSix" aria-expanded="false"
                                       aria-controls="collapseSix">
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseSix" class="panel-collapse collapse" role="tabpanel"
                                 aria-labelledby="headingSix">
                                <div class="panel-body">
                                    <ul>
                                        <li>Định nghĩa về sự từ chối của khách hàng và những nguyên nhân</li>
                                        <li>Những chiến lược giải quyết các từ chối</li>
                                        <li>Hãy tạo ra những phản hồi từ chối nhằm làm giảm xung đột</li>
                                        <li>Khám phá những từ chối tiềm ẩn</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingSeven">
                                <h4 class="panel-title">
                                    <span class="circle-bullet">7</span>
                                    <span class="title">NHỮNG CHIẾN LƯỢC CHỐT SALES THÀNH CÔNG (CLOSING STAGE)</span>
                                    <a class="accordion-toggle collapsed" role="button" data-toggle="collapse"
                                       data-parent="#accordion" href="#collapseSeven" aria-expanded="false"
                                       aria-controls="collapseSeven">
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseSeven" class="panel-collapse collapse" role="tabpanel"
                                 aria-labelledby="headingSeven">
                                <div class="panel-body">
                                    <ul>
                                        <li>Tìm hiểu về giai đoạn chốt sales</li>
                                        <li>Rào cản nỗi sợ hãi</li>
                                        <li>Nhận diện những dấu hiệu mua hàng</li>
                                        <li>Những chiến lược để chốt sales</li>
                                        <li>Bạn sẽ làm gì khi khách hàng nói KHÔNG?</li>
                                        <li>Bạn làm gì khi thương vụ bị mất</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingEight">
                                <h4 class="panel-title">
                                    <span class="circle-bullet">8</span>
                                    <span class="title">KẾT THÚC &amp; THEO DÕI SAU BÁN HÀNG (WRAP-UP &amp; FOLLOW-UP STAGES)</span>
                                    <a class="accordion-toggle collapsed" role="button" data-toggle="collapse"
                                       data-parent="#accordion" href="#collapseEight" aria-expanded="false"
                                       aria-controls="collapseEight">
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseEight" class="panel-collapse collapse" role="tabpanel"
                                 aria-labelledby="headingEight">
                                <div class="panel-body">
                                    <ul>
                                        <li>Kết thúc &amp; chăm sóc khách hàng sau bán hàng</li>
                                        <li>Lời giới thiệu trong bán hàng</li>
                                        <li>Chăm sóc khách hàng sau bán hàng và lặp lại bán hàng (cho khách hàng cũ)</li>
                                        <li>Những chiến lược tạo ra lặp lại bán hàng (repeat sales)</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="teacher">
            <div class="container">
                <div class="row title">
                    <div class="col-md-4 col-sm-3"></div>
                    <div class="col-md-1 col-sm-1 line"><hr/></div>
                    <div class="col-md-2 col-sm-4"><h3 class="intro-heading"> GIẢNG VIÊN </h3></div>
                    <div class="col-md-1 col-sm-1 line"><hr/></div>
                    <div class="col-md-4 col-sm-3"></div>
                </div>
                <div class="wrap-content">
                    <div class="col-sm-4 col-xs-12 img">
                        <img src="/mana/cbp/khoa-hoc-ky-nang-ban-hang-chuyen-nghiep/imgs/nguyenngoan.png" alt="Thạc sỹ Nguyễn Ngoan" class="img-responsive">
                    </div><!--end .video-->
                    <div class="col-sm-8 col-xs-12 content">
                        <h3>Thạc sỹ <span>Nguyễn Ngoan</span></h3>
                        <ul class="first">
                            <li><span>&#45;</span> Phó tổng Giám đốc – Star Travel International</li>
                            <li><span>&#45;</span> Chủ tịch MANDA MIND Corporation</li>
                            <li><span>&#45;</span> Nhà sáng lập <a href="www.dacsan3mien.com" class="bold">www.dacsan3mien.com</a></li>
                            <li><span>&#45;</span> Diễn giả Doanh nhân &amp; Chuyên gia tư vấn chiến lược, thương hiệu</li>
                        </ul>

                    </div><!--end .content-->
                </div><!--end .content-->
            </div><!--end .container-->
        </div>
        <div class="study-method" id="method">
            <div class="container">
                <div class="row">
                    <div class="col-sm-3">
                    </div>
                    <div class="col-sm-1 line">
                        <hr/>
                    </div>
                    <div class="col-sm-4">
                        <h3 class="intro-heading"> HÌNH THỨC HỌC </h3>
                    </div>
                    <div class="col-sm-1 line">
                        <hr/>
                    </div>
                    <div class="col-sm-3">
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-3">
                        <div class="circle-method circle-calendar"><i class="icon-calendar"></i> </div>
                        <div class="study-method-text">
                            <h4>Học mọi lúc mọi nơi</h4>
                            <p>Học qua video bài giảng được biên tập chuyên nghiệp.  Tài liệu bao gồm sách và bài thuyết trình, tư liệu tham khảo</p>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="circle-method circle-question"><i class="icon-question"></i></div>
                        <div class="study-method-text">
                            <h4>Hỏi đáp cùng chuyên gia</h4>
                            <p>Cố vấn 1-2-1 (1 kèm 1) trong suốt quá trình học và luyện thi. Hỏi đáp cùng chuyên gia, doanh nhân có kinh nghiệm</p>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="circle-method circle-pencil"><i class="icon-pencil"></i></div>
                        <div class="study-method-text">
                            <h4>Hỏi đáp và luyện thi</h4>
                            <p>Học đi đôi với hành trong suốt quá trình học. Rèn luyện cùng hơn 100 bài thi thử</p>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="circle-method circle-house"><i class="icon-house"></i></div>
                        <div class="study-method-text">
                            <h4>Thi trực tuyến và nhận bằng quốc tế</h4>
                            <p>Thi trực tuyến mọi lúc mọi nơi. 12 tháng để rèn luyện thoải mái trước khi thi</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="registration" id="regis">
            <div class="container resgistration-border">
                <div class="row">
                    <div class="col-sm-3">
                    </div>
                    <div class="col-sm-1 line">
                        <hr/>
                    </div>
                    <div class="col-sm-4">
                        <h3 class="intro-heading"> ĐĂNG KÝ NHẬN TƯ VẤN </h3>
                    </div>
                    <div class="col-sm-1 line">
                        <hr/>
                    </div>
                    <div class="col-sm-3">
                    </div>
                </div>
                <form action="#" class="form-horizontal" id="form_advice" method="post" accept-charset="utf-8">
                    <div class="row">

                        <div class="text-intro-regist">
                            <p>và <b>HỌC BỔNG 1,000,000 ĐỒNG</b> dành riêng cho bạn</p> <br/>
                            <p class="text">HỌC PHÍ: 4.700.000Đ (Bao gồm học liệu Online, sách và chi phí dự thi CBP)</p>
                            <p class="text-last"><span>HỌC BỔNG DÀNH CHO 99 NGƯỜI ĐĂNG KÝ ĐẦU TIÊN: 1.000.000Đ</span></p>
                        </div>
                        <div class="regist-form">
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <input type="text" id="name" class="input-group form-control" required  placeholder="Họ Và Tên"/>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <input type="text" id="phone" class="input-group form-control" required placeholder="Số Điện Thoại"/>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <input type="text" id="email" class="input-group form-control" required  placeholder="Email"/>
                                </div>
                            </div>
                        </div>
                        <div class="button-regist">
                            <button type="submit"  id="dang_ky_form" class="btn btn-primary btn-regist">ĐĂNG KÝ</button>
                        </div>
                        <div class="information-text">
                            <p>Bộ phận chăm sóc khách hàng của MANA Business School sẽ liên lạc sớm với bạn để tư vấn về khóa học (mức phí tư vấn là 0đ).</p>
                        </div>

                    </div>
                </form>
            </div>
        </div>
        <footer>
            <div class="container footer">
                <div class="row">
                    <div class="col-sm-3">
                        <div class="footer-logo">
                            <ul>
                                <li>
                                    <a href="#">
                                        <img src="/mana/cbp/khoa-hoc-ky-nang-ban-hang-chuyen-nghiep/imgs/1_Logo.png" class="img-responsive"/>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <img src="/mana/cbp/khoa-hoc-ky-nang-ban-hang-chuyen-nghiep/imgs/18_KynaLogo.png" class="img-responsive"/>
                                    </a>
                                </li>
                            </ul>

                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="footer-text-center">
                            <h4>Công ty Cổ phần  Dream Việt Education</h4>
                            <p> <b class="company-address"> Trụ sở chính:</b>  Lầu 6 Tòa nhà Thịnh Phát - 178/8 Đường D1, Phường 25, Quận Bình Thạnh, TP Hồ Chí Minh</p>
                            <p> <b class="company-address"> Văn phòng Hà Nội:</b>  Phòng 604 tháp A, Hà Thành Plaza 102 Thái Thịnh, Đống Đa, TP Hà Nội </p>
                            <p style="padding-top: 20px">Giấy phép ĐKKD số 0313589030 do Sở Kế hoạch và Đầu tư TPHCM cấp</p>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="footer-hot-line">
                            <p><b class="company-address">Hotline:</b>  1900 6364 09</p>
                            <p>Thứ 2 – thứ 6: từ 08h30 – 21h00</p>
                            <p>Thứ 7: 08h30 – 17h00</p>
                            <p><b class="company-address">Email:</b></p> hotro@kyna.vn
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <div class="modal fade" id="modal">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="popup_body">
                            <div class="alert alert-success" role="alert">Cám ơn bạn đã để lại thông tin. Kyna.vn sẽ sớm liên hệ với bạn để tư vấn thêm về chương trình học!</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="/mana/cbp/khoa-hoc-ky-nang-ban-hang-chuyen-nghiep/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="/mana/cbp/khoa-hoc-ky-nang-ban-hang-chuyen-nghiep/js/owl.carousel.min.js"></script>
        <script>
            $(function () {
                $('.logo-brand').data('size', 'big');
            });

            $(window).scroll(function () {
                if ($(document).scrollTop() > 0)
                {
                    if ($('.logo-brand').data('size') == 'big')
                    {
                        $('.logo-brand').data('size', 'small');
                        $('.logo-brand').stop().animate({
                            height: '40px'
                        }, 600);
                        $('.navbar').addClass('menufix');
                    }
                } else
                {
                    if ($('.logo-brand').data('size') == 'small')
                    {
                        $('.logo-brand').data('size', 'big');
                        $('.logo-brand').stop().animate({
                            height: '50px'
                        }, 600);

                        $('.navbar').removeClass('menufix');
                    }
                }
            });
        </script>
        <!-- Nhan them vao -->
        <script>
            $(document).ready(function () {
                $("#dang_ky_form").bind('click', function (e) {

                    if ($.trim($("#name").val()) != '' && $.trim($("#email").val()) != '' && $.trim($("#phone").val()) != '') {
                        e.preventDefault();
                        var url = 'https://docs.google.com/forms/d/e/1FAIpQLSeuoywN74YXvloUgxeAePFWgCsiQPuf0fd1wFlKBtdZM7M5pQ/formResponse';
                        var data = {
                            'entry.1618386042': $("#name").val(),
                            'entry.1550829392': $("#email").val(),
                            'entry.1725316625': $("#phone").val(),
                            'entry.953023567': '<?php echo $utm_source; ?>',
                            'entry.823420574':'<?php echo $utm_medium;  ?>',
                            'entry.1198803187':'<?php echo $utm_campaign;  ?>'
                        };
                        $.ajax({
                            'url': url,
                            'method': 'POST',
                            'dataType': 'XML',
                            'data': data,
                            'statusCode': {
                                0: function () {
                                    $("#name").val('');
                                    $("#email").val('');
                                    $("#phone").val('');
                                    $("#modal").modal();
                                },
                                200: function () {
                                    $("#name").val('');
                                    $("#email").val('');
                                    $("#phone").val('');
                                    $("#modal").modal();
                                }
                            }

                        });
                    }

                });

                $(".wrapper-slider ul").owlCarousel({
                    autoPlay: true,
                    items: 1,
                    itemsDesktop: [1199, 1],
                    itemsDesktopSmall: [979, 1],
                    itemsTablet: [768, 1],
                    itemsMobile: [479, 1],
                    mouseDrag: true,
                    autoPlay: 40000,
                            navigation: false,
                    pagination: true,
                });
            });


            $(function () {
                $('.scroll-link a[href*=#]:not([href=#])').click(function () {
                    if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {

                        var target = $(this.hash);
                        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                        if ($(window).width() > 768) {
                            if (target.length) {
                                $('html,body').animate({
                                    scrollTop: target.offset().top - 70
                                }, 1000);
                                return false;
                            }
                        } else {
                            if (target.length) {
                                $('html,body').animate({
                                    scrollTop: target.offset().top
                                }, 1000);
                                return false;
                            }
                        }
                    }
                });
            });

        </script>
        <?php
        include $dir_name."/ga_landing_page.php"; ?>
    </body>

    <!-- Mirrored from mana.edu.vn/cbp/khoa-hoc-ky-nang-giao-tiep/ by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 27 Apr 2016 13:31:05 GMT -->
</html>
