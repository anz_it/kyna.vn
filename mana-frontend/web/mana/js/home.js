$(document).ready(function() {
       $('ul.dropdown-menu a.expand-dropdown-menu').on('click', function(event){
           //The event won't be propagated to the document NODE and
           // therefore events delegated to document won't be fired
           // console.log(112);
           $('.collapse').collapse("hide");
           $(this).next().collapse("toggle");
           event.stopPropagation();
           event.preventDefault();
           // $('a.expand-dropdown-menu').parent().addClass('open');
           // console.log($('a.expand-dropdown-menu').parent().addClass('open'));
       });
        $('.menu .dropdown').hover(function() {
            $(this).addClass('open').find('.dropdown-menu').first().stop(true, true).show();
        }, function() {
            $(this).removeClass('open').find('.dropdown-menu').first().stop(true, true).hide();
        });

        function getParam(name) {
            var url = window.location.href;
            var results = new RegExp('[\\?&]' + name + '=([^&#]*)').exec(url);
            if (results != null) { return results[0]; }
            return null;
        }

        function getParamValue(name) {
            var url = window.location.href;
            var results = new RegExp('[\\?&]' + name + '=([^&#]*)').exec(url);
            if (results != null) { return results[1]; }
            return null;
        }

        function getCurPar(name, val){
            var cur_link = window.location.href,
                is_exist = getParam(name),
                par = name + '=' + val;
            if (is_exist != null){
                is_exist = is_exist.substr(1);
                return cur_link.replace(is_exist, par);
            }
            if (cur_link.indexOf('?')>0){
                return cur_link += '&'+ par;
            }
            return cur_link += '?'+ par;
        }

        function redirect_filter(name, val){
            var url = getCurPar(name,val);  window.location.href = url;
        }

        $(document).ready(function(){
            //$("#tab_category").BaseTab();
            var slug = window.location.pathname.split('/')[2];
            $(".link_sidebar").each(function(){
                var href = $(this).children("a").attr("href");
                if (href != null && href.indexOf(slug)>-1){
                    $(this).addClass("active");
                }
            });

            $("#course_list_cate").hide().delay(50).fadeIn();

            $("#select_sortBy").change(function(){
                var val = $(this).val(), name = 'sort';
                redirect_filter(name,val);
            });

            $(".pagination_link a").click(function(e){
                e.preventDefault();
                var name = 'page', val = $(this).attr('href').replace('?page=','');
                if (val=='') val=1;
                redirect_filter(name,val);
            });

            $(".filter-link").click(function(e){
                e.preventDefault();
                var name = $(this).attr('data-name'), val = $(this).attr('data-val');
                redirect_filter(name,val);
            });


        });

    $('.checkbox-search').change(function (e) {
        var sort = getParam('sort');
        var querySearch = getParam('q');
        var param = "";

        // Add sort param
        if (sort != null) {
            param += sort;
            param += '&categorySlug=' +categorySlug;
        } else {
            param += '?categorySlug=' +categorySlug;
        }

        $('.checkbox-search').each( function (index) {
            if (this.checked) {

                var model = $(this).attr('data-model');
                param = param + '&CourseSearch[' + model+ 'Ids][]=' + $(this).val();

            }
        });
        if (querySearch != null) {
            param += '&q=' + getParamValue('q');
        }

        // console.log(param);


        $.get(filterUrl + param, function(data, status){
            console.log(data);
            $('.courses-list').html(data.data);
            $('.categories-courses-main .count .number-count'). html(data.listCount);
        });

    });

    $('.checkbox-child').change(function (e) {
        var parentID = $(this).data('parent-id');
        // console.log($(this));
        var isCheck = 0;
        $('.checkbox-child').each( function (index) {
            if (this.checked && ($(this).data('parent-id') == parentID)) {
                isCheck++;
                console.log($(this).data('parent-id'));
            }
        });
        if (isCheck > 0) {
            $('#'+parentID).addClass('parent');
        } else {
            $('#'+parentID).removeClass('parent');
        }
    });

});

(function($, window, document, undefined){
    $(document).ready(function(){
        $("#popup-login").on("hidden.bs.modal", function() {
            $("#login-form .error-summary").hide();
            $("#user-login").val("");
            $("#user-password").val("");
        })

        $("body").on("submit", "#login-form", function (event) {
            event.preventDefault();

            var data = $(this).serializeArray();
            data.push({name: "currentUrl", value: window.location})

            $.post("/user/security/login", data, function (response) {
                if (response.result) {
// login success
                    if (response.redirectUrl) {
                        window.location.replace(response.redirectUrl);
                    } else {
                        window.location.reload();
                    }
                } else {
// login failed
                    var liHtml = "";
                    $.each(response.errors, function (key, value){
                        liHtml += "<li>" + value[0] + "</li>";
                    });

                    $("#login-form .error-summary ul").html(liHtml);
                    $("#login-form .error-summary").show();
                }

            });
        });
    });
})(window.jQuery || window.Zepto, window, document);