jQuery(document).ready(function($) {
  $('#close-topbar').on('click', function () {
    $('#noel-topbar').remove();
    $('#wrap-header').removeClass('has-topbar');
  });
  var today = new Date(),
      expiredDate = new Date('02/01/2017');
  if (today < expiredDate ){
    $('#wrap-header').addClass('has-topbar');
    $('[data-xmas="false"]').each(function (idx, elm) {
      $(elm).hide();
    });
  } else{
    $('[data-xmas="true"]').each(function (idx, elm) {
      $(elm).remove();
      $('#noel-topbar').remove();
      $('#wrap-header').removeClass('has-topbar');
    });
  }
});
