ActionTopBar = {
    Init: function () {
        $('#km-topbar .btn').click(function () {
            var goTo = 0;
            if ($("#register").length > 0)
                goTo = $("#register").offset().top - 70;
            $('html, body').animate({
                scrollTop: goTo
            }, 1000);
        })

        $(document).ready(function () {
            $('#close-topbar').on('click', function () {
                $('#km-topbar').remove();
                $('html').css('margin-top','0');
                $('#menu').css('top','0');
            });
        });

    }
}
ActionTopBar.Init();