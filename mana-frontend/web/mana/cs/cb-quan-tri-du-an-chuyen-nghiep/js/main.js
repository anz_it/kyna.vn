actionPage = {
    Init: function () {
        var obj_detail = $("#courses-detail-slide .carousel-inner .carousel-item .detail");
        var obj_title = $("#courses-detail-slide .carousel-inner .carousel-item .title");
        var max = 0;
        var arr_h_detail = [];
        $(obj_detail).each(function () {
            $(this).parent().show();
            arr_h_detail.push($(this).height());
            if ($(this).height() > max)
                max = $(this).height();
            $(this).parent().attr("style","");
        })
        if (window.matchMedia('(min-width: 768px)').matches) {
            $(obj_title).each(function () {
                $(this).height(max - 180);
            })
            for (var i = 0; i < $(obj_detail).length ; i++) {
                $(obj_detail[i]).attr("style","height: auto; padding-top: " + ((max + 20 - arr_h_detail[i]) / 2) + "px");
            }
        }
        else {
            $(obj_title).each(function () {
                $(this).height("auto");
            })
            var _max = 0;
            $(obj_detail).each(function () {
                $(this).parent().show();
                var ev_h = 0;
                var obj_row = $(this).find(".row");
                $(obj_row).each(function () {
                    ev_h += $(this).height();
                });
                if (ev_h > _max)
                    _max = ev_h;
                $(this).parent().attr("style", "");
            })

            $(obj_detail).each(function () {
                $(this).attr("style", "");
                $(this).height(_max);
            })
        }
        $("#object .image-object").height($("#object .content-object").height());
        if (window.matchMedia('(min-width: 768px)').matches) {
            $("#license .image-license").height($("#license .content-license").height());
        }
        else
            $("#license .image-license").height("auto");
        
        $(".icon-angle-down, #courses .box-mana .mana-detail").click(function () {
            id = $(this).attr("data-url")
            $('html, body').animate({
                scrollTop: $(id).offset().top
            }, 1000);
        });
        $(".btn-tuvan").click(function () {
            $('html, body').animate({
                scrollTop: $("#register").offset().top
            }, 1000);
        });
        $("#header ul.navbar-nav > li").click(function (event) {
            event.preventDefault();
            id = $(this).find("a").attr("href");
            $('html, body').animate({
                scrollTop: $(id).offset().top
            }, 1000);
        });
		actionPage.SetHeightItem();
    },
    AuthorChangeImage: function () {
        $('#courses-detail-author img.md-gv, #courses-detail-author .tt-gv').hover(
            function () {
                var $this = $(this);
                var target = $this.attr('data-target');
                $('#courses-detail-author img[data-target="'+ target +'"').attr('src', '/mana/cs/cb-quan-tri-du-an-chuyen-nghiep/img/' + target + '.png');
                $('.' + target).show();

            }, function () {
                var $this = $(this);
                var target = $this.attr('data-target');
                var img_bw = $this.attr('data-img-bw');
                var $cur_content = $(".tt-gv." + target);
                if ($cur_content.length > 0) {
                    $('#courses-detail-author img[data-target="' + target + '"').attr('src', '/mana/cs/cb-quan-tri-du-an-chuyen-nghiep/img/' + img_bw + '.png');
                    //$this.attr('src', 'img/author/' + img_bw + '.png');
                    $cur_content.hide();
            }
            });
        //$('#courses-detail-author .md-gv').on('mouseleave', function (e) {
        //    $this = $(this);
        //    if (!$(e.toElement).hasClass('tt-gv')) {
        //        $this.attr('src', 'img/author/' + $this.attr('data-img-bw') + '.png');
        //        $('.' + $this.attr('data-target')).hide();
        //    }
        //});
        $('#courses-detail-author .sm-row').hover(function () {
            $('.tt-gv:visible').show();
        }, function () {
            $('.tt-gv:visible').hide();
        });
    },
    Fullpage: function () {
        if (window.matchMedia('(min-width: 768px)').matches) {
            $('#fullpage').fullpage({
                scrollBar: true,
                fixedElements: '#wrap-header',
                normalScrollElements: '#register, #footer',
                fitToSection: false,
                controlArrows: true,
            });
        }
    },
	SetHeightItem: function(){
		var $list = $('#courses-detail-courses .wrap-list'),
			$items = $list.find('.list'),
			setHeights = function ()
			{
				$items.css('height', 'auto');

				var perRow = Math.floor($list.width() / $items.width());
				if (perRow == null || perRow < 2)
					return true;

				for (var i = 0, j = $items.length; i < j; i += perRow)
				{
					var maxHeight = 0,
							$row = $items.slice(i, i + perRow);

					$row.each(function ()
					{
						var itemHeight = parseInt($(this).outerHeight());
						if (itemHeight > maxHeight)
							maxHeight = itemHeight;
					});
					$row.css('height', maxHeight);
				}
			};
		setHeights();
        $(window).on('resize', setHeights);
	}
};

$(window).on("load resize", function () {
    actionPage.Init();
    actionPage.Fullpage();
});

actionPage.AuthorChangeImage();

$(window).on("load scroll", function () {
    var top = $(this).scrollTop();
    if (top > 150) {
        if (!$("#header").hasClass("action")) {
            $("#header").addClass("action");
        }
    }
    else {
        $("#header").removeClass("action");
    }
    var obj = $("[path='scrolling']");
    var current = $(obj[0]);
    for (var i = 1; i < $(obj).length; i++) {
        if (top - ($(obj[i]).offset().top - 70) >= 0)
            current = $(obj[i]);
    }
    var link_current = $("#header ul.navbar-nav > li").find("a[href='#" + $(current).attr("id") + "']");
    if (!$(link_current).hasClass("active")) {
        $("#header ul.navbar-nav > li a").removeClass("active");
        $(link_current).addClass("active");
    }
});