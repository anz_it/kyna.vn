/* HOME */
    
/* SLIDER CONTENT BOTTOM */     
$(document).ready(function() {    	
	$(".wrap-slider-content-bottom").owlCarousel({
		autoPlay: true,
		items : 1,
		itemsDesktop : [1199,1], 
		itemsDesktopSmall:	[979,1],  
		itemsTablet:	[768,1],
		itemsMobile:	[479,1],            
		mouseDrag : true,  
		navigationText : ["<i class='fa fa-play rotate-180 icon'></i>",
											"<i class='fa fa-play icon'></i>"],
		navigation: true,
		pagination : false,
	}); 	    
}); 
