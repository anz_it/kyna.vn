;(function ($, window, document, undefined) {
    $(document).ready(function(){
        $("body").on("submit", "#user-info-form", function (event) {
            event.preventDefault();

            var data = $(this).serializeArray();

            $.post("/cart/default/add-user-info", data, function (response) {
                if (response.result) {
                    $('.user-info-form').html('<div class="alert alert-success"><span>Cảm ơn bạn đã để lại thông tin, chúng tôi sẽ liên hệ sớm nhất có thể để tư vấn các khóa học cho bạn.</span></div>');
                } else {
                    // login failed
                    var liHtml = "";
                    $.each(response.errors, function (key, value){
                        liHtml += "<li>" + value[0] + "</li>";
                    });

                    $("#user-info-form .error-summary ul").html(liHtml);
                    $("#user-info-form .error-summary").show();
                }

            });
        });
    });
})(window.jQuery, window, document);