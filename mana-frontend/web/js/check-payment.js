;(function($, window, document, undefined){
    $(document).ready(function(){
        $('body').on('click', '.btn-payment', function (e) {
            var csrfToken = $('meta[name="csrf-token"]').attr("content");
            
            $.ajax({
                url: '/cart/default/check-payment',
                type: 'POST',
                data: {
                     _csrf : csrfToken
                },
                success: function (response){
                    if (response.result) {
                        window.location = response.paymentUrl;
                    } else {
                        // user not logged in
                        $('#popup-login').modal('toggle');
                    }
                }
            });
            
            e.preventDefault();
        });
    });
})(window.jQuery || window.Zepto, window, document);