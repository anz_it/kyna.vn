/* CAT CAP 1 */

/* SLIDER SEARCH LIST */
$(document).ready(function() {    	
	$(".detail-search-list .wrap").owlCarousel({
		autoPlay: false,
		items : 4,
		itemsDesktop : [1199,1], 
		itemsDesktopSmall:	[979,3],  
		itemsTablet:	[768,2],
		itemsMobile:	[479,1],            
		mouseDrag : true,  
		navigationText : ["<i class='fa fa-play rotate-180 icon'></i>",
											"<i class='fa fa-play icon'></i>"],
		navigation: true,
		pagination : false,
	}); 	    
});