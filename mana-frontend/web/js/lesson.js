;(function ($) {
    var flash_flag = (navigator.mimeTypes["application/x-shockwave-flash"] != undefined), popObj  = null;
    var player;
    var html = $('.video-html5-type');

    if(flash_flag) {
        html.remove();
        player = flowplayer('player', '/js/flowplayer/flowplayer.commercial-3.2.18.swf', {
            key: '#$9e934cdbe9535648374',
            canvas: {
                backgroundColor: "#272727",
                backgroundGradient: "none"
            },
            plugins: {
                f4m: {
                    url: "/js/flowplayer/flowplayer.f4m-3.2.10.swf"
                },
                httpstreaming: {
                    url: "/js/flowplayer/flowplayer.httpstreaming-3.2.11.swf"
                },
                controls: {
                    url: "/js/flowplayer/flowplayer.controls-3.2.16.swf",
                    backgroundColor: "#333333",
                    backgroundGradient: "none"
                }
            },
            clip: {
                url: "manifest.f4m",
                autoPlay: true,
                urlResolvers: ['f4m'],
                provider: 'httpstreaming',
                baseUrl: $("#player").data('base-url'),
                scaling:'fit',
                onFinish: finishVideo
            }
        });
        $("#player").attr("style", "height: 100%");
    }else{
        console.log('Not Support Flash');
        $.when(
            $.getScript( "/js/flowplayer.min.js" ),
            $.getScript( "/js/flowplayer/html5/flowplayer.hlsjs.js" ),
            $.getScript( "/js/flowplayer/html5/flowplayer.dashjs.min.js" ),

//                    $.getScript( "/mypath/myscript3.js" ),
            $.Deferred(function( deferred ){
                $( deferred.resolve );
            })
        ).done(function(){
            var src_mp4 = $('#player').attr('data-base-url');
            var src_mpd = $('.video-html5-type').attr('video-url-mpd');
            var src_hls = $('.video-html5-type').attr('video-url-hls');
            flowplayer('#player', {
                autoplay: true,
                embed:false,
                ratio: false,
                key: "$631977711768310",
//                        key: "#$9e934cdbe9535648374",
                clip: {
                    hlsjs: {
                        // let hlsjs plugin pick optimal quality before playback starts
                        startLevel: -1
                    },
                    sources: [
                        { type: "application/dash+xml", src: src_mpd },
                        { type: "application/x-mpegurl", src: src_hls },
                        { type: "video/mp4", src: src_mp4 }
                    ]
                }
            });
            //$('.source-video').attr('type', 'video/mp4');
            $("#player").attr("style", "height: 100%");
            $(".video-html5-type").attr("style", "height: 100%");
            //$('.video-html5-type').attr('src', src_mp4);
        });
    }



    $('#add-note-form').on("submit", function (e) {
        e.preventDefault();
        var $form = $(this),
            url = this.action,
            data = $form.serialize();

        $.post(url, data, function (response) {
            //console.log(response);
            if (response.success !== false) {
                $('#note-input').val("");
                updateNoteList(function () {
                    $('#aside-note-tab').tab('show');
                });
            }
        });
    });
    $('#note-input').keypress(function (e) {
        if (e.which == 13) {
            var playbackTime = player.getTime();
            $('#add-note-form').find("#playback-time").val(playbackTime);
            $('#add-note-form').submit();
            return false;
        }
        //console.log(x);
    });

    var $noteList = $("#lesson-tab-notes");

    function updateNoteList (callback) {
        $noteList.load('/course/course-note/get/?id=' + $noteList.data('id'), callback);
    }

    updateNoteList();

    function finishVideo () {
        $.get('/course/learning/end-lesson/?id=' + $("#player").data('lesson-id'), function (resp) {
            console.log(resp);
        });
    }

    $("#transcript").on("click", "p", function (e) {
        e.preventDefault();

        var sec = $(this).data('sec');
        player.seek(sec);
    });

    $("#main-desc").on("shown.bs.tab", "[data-toggle='tab']", function (e) {
        var pane = $(e.target.getAttribute("href")),
            url = pane.data('remote');
        if (url !== undefined) {
            pane.find(".ajax-content").load(url);
        }
    });
})(jQuery);
