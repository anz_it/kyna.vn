;
(function($) {
    var kynaFb = {
        appId: "1077261602350453",
        loginUri: "https://www.facebook.com/dialog/oauth",
        language: "vi_VN",
        scope: "email",
        authUri: "http://mana.v3.local/user/auth/fb"
    };
    $.ajaxSetup({
        cache: true
    });
    $.getScript("//connect.facebook.net/" + kynaFb.language + "/sdk.js", function() {
        FB.init({
            appId: kynaFb.appId,
            version: 'v2.5' // or v2.0, v2.1, v2.2, v2.3
        });

        var $btnFb = $(".button-facebook");
        $btnFb.on("click", function(e) {
            e.preventDefault();
            var that = this;
            FB.login(function(response) {
                if (response.authResponse) {
                    FB.api('/me?fields=id,name,email', function(response) {
                        window.ajaxCaller.doAjax(that, that.href, response);
                    });
                }
            }, {
                "scope": "email"
            });
        });
    });
})(jQuery);
