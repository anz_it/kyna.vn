$(document).ready(function() {
    /* CHECKOUT */
    ;( function( $, window, document, undefined )
    {
        'use strict';    
           var setHeights_3  = function()
            {
                if($(window).width() > 991 ){                
                    var GetHeightNull = $('.checkout').height(),
                        CheckoutHeaderHeight = $('.checkout-header').height() + 45,
                        CheckoutBodyHeight = $('.checkout').height(),
                        CheckoutWidthRight = $('.checkout .wrap-content-right').width(),
                        CheckoutHeight = $('.checkout-confirm-content').height();                                              
                    $('.background.right.null').attr('style', 'height: ' + GetHeightNull + 'px');                                                   
                    $('.wrap-content-right .wrap').css({"padding-top": + CheckoutHeaderHeight + "px","width": + CheckoutWidthRight + "px"});                     
                }
            }; 
        setHeights_3();
        $( window ).on( 'resize', setHeights_3 );         
    })( jQuery, window, document ); 
   
    /* HEIGHT BOX CHECKOUT */

    // init payment methods
    $("input[name=\'PaymentForm[method]\']:radio:checked").parent().find('.checkout-sub-list').show();
    
    var CheckoutIconHeightImg = $('.checkout-confirm-content .img').height(),
        CheckoutIconHeightBox = $('.checkout-confirm-content .box .text').height(); 
        $('.checkout-confirm-content .box .text').css({top:(CheckoutIconHeightImg - CheckoutIconHeightBox )/2});

    /* HIDE/SHOW BOX CACH THUC THANH TOAN */   
    $("input[name=\'PaymentForm[method]\']").click(function() {
        var valshow = $(this).val();
        $("div.checkout-sub-list").hide();
        $("#checkout-show-" + valshow).show();
    });

});