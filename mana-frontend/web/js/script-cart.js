$('document').ready(function() {
        $('#cart-list-main .wrap-list').owlCarousel({
    	autoPlay: true,
    	items : 4,
    	itemsDesktop : [1199,4], 
    	itemsDesktopSmall:	[979,3],  
    	itemsTablet:	[768,2],
    	itemsMobile:	[479,1],            
    	mouseDrag : true,  
    	navigationText : ['<i class="fa fa-play rotate-180 icon"></i>',
    										'<i class="fa fa-play icon"></i>'],
    	navigation: true,
    	pagination : false,
    }); 
});
              	
