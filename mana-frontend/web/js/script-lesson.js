/* JS CHUYEN DOI COT */
$('#lesson .header-nav .feature .sub .template a.last').click(function() {
    $('#lesson').removeClass("col-two").addClass("col-full");
    $('.wrap-video .left').addClass("none");
    $('.wrap-video .right').removeClass("col-md-8 col-sm-7 col-xs-12 right pd0");
    $('.wrap-support').addClass("none");
    $('.wrap-menu-content').removeClass("none");
});

$('#lesson .header-nav .feature .sub .template a.first').click(function() {
    $('#lesson').removeClass("col-full").addClass("col-two");
    $('.wrap-video .left').removeClass("none");
    $('.wrap-video .tab-content-video').addClass("col-md-8 col-sm-7 col-xs-12 right pd0");
    $('.wrap-support').removeClass("none");
    $('.wrap-menu-content').addClass("none");
});

/* JS TINH KHOANG CACH MENU + CHIEU CAO MAN HINH */
var heightHeaderVideo = $('.wrap-header').height();
var heightHeaderMenuVideo = $('.header-nav').height();
var winHeightVideo = $(window).height() - heightHeaderVideo - heightHeaderMenuVideo - 70;
var heightHeaderVideo = $('.page-lesson .wrap-header').height();
var heightHeaderMenuVideo = $('.page-lesson .header-nav').height();

var winHeightVideo = $(window).height() - heightHeaderVideo - heightHeaderMenuVideo - 70;
var winHeightVideoTab = $(window).height() - heightHeaderVideo - heightHeaderMenuVideo - 170;
var winHeightVideoMain = $(window).height() - heightHeaderVideo - heightHeaderMenuVideo - 70;

$('.wrap-video .video').css('height', winHeightVideo + 'px');
/*
$('.lesson-content-quiz .lesson-wrap-quiz .quiz-slide').css('height', winHeightVideo + 'px');
$('.lesson-content-quiz .lesson-wrap-quiz .quiz-start').css('height', winHeightVideo + 'px');
$('.lesson-content-quiz .lesson-wrap-quiz .quiz-scroll').css('height', winHeightVideo + 'px');
*/
$('#quiz-content').css('height', winHeightVideo + 'px');
//$('#quiz-content').css('overflow', 'auto');
$('.page-lesson .tab-menu-content').css('height', winHeightVideoTab + 'px');
$('.wrap-video .video .videolessonWrapper > div').css('height', winHeightVideoMain + 'px');

/* READ MORE */
var lesson_flaghidden = "show";
$("#lesson .click-read-more a").click(function() {
    if (lesson_flaghidden == "show") {
        $(this).parents(".wrap-reply").find(".content-sub .text").removeClass("max-height", "fast");
        $(this).addClass('rote').text("Thu gọn");
        $(this).parents("#lesson").find("#lesson .collapse-more").addClass('add');
        lesson_flaghidden = "hidden";
    } else if (lesson_flaghidden == "hidden") {
        $(this).parents(".wrap-reply").find(".content-sub .text").addClass("max-height", "fast");
        $(this).parents("#lesson").find("#lesson .click-read-more a").removeClass('rote').text("Xem thêm");
        $(this).parents("#lesson").find("#lesson .collapse-more").removeClass('add');
        lesson_flaghidden = "show";
    }
});

/* CLICK CLOSE FORM */

$('#lesson-form-reply .wrap-button .close-form').click(function() {
    $('#lesson-form-reply').hide();
});

$('.lesson-form-reply .wrap-button .close-form').click(function () {
    $('.lesson-form-reply').hide();
});

$("#lesson").bind('click', function(e){
    var obj = $(e.target);
    if(obj.hasClass('btn-reply-form')){
        var  obj_key = obj.parents('.media-key');
        if(obj_key.length > 0){
            var v_key = obj_key.attr('data-key');
            $(".form-reply-selected").removeClass('form-reply-selected');
            $("#lesson-form-reply-"+v_key).addClass("form-reply-selected");
            $('html,body').animate({
                scrollTop: $("#lesson-form-reply-"+v_key).offset().top - 120
            }, 1000);
        }
    }
    else if (obj.hasClass('close-form')) {
        var obj_key = obj.parents('.media-key');
        if (obj_key.length > 0) {
            var v_key = obj_key.attr('data-key');
            $("#lesson-form-reply-"+v_key).hide();
            $("#lesson-form-reply-"+v_key).find("#discuss-form")[0].reset();
        }
    }
});

/* CLICK LABEL QUIZ */
$("body").bind('click', function(e){
        var obj = $(e.target);
        if (obj.hasClass('quiz-squared-label')) {
            $(".selected").removeClass('selected');
            obj.addClass('selected');
        }
    });

