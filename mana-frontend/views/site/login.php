<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Mana.edu.vn - Đăng nhập';
$this->params['breadcrumbs'][] = $this->title;
?>
<header>
    <div class="header">
        <div class="container">
            <div class="row">
                <div class="col-md-offset-4 col-md-4 login col-xs-offset-1 col-xs-10 col-sm-offset-3 col-sm-6">
                    <div class="col-md-12 science-top" >
                        <div class=" text-center science-title ">Đăng nhập</div>
                    </div>
                    <div class="col-md-12 text-center line" style=""><img src="/mana/images/icon/2_Line.png"></div>
                    <div class="col-md-12">
                        <?php if (!empty($msg)) { ?>
                            <div class="alert alert-danger" role="alert">
                                <?=$msg?>
                            </div>
                        <?php } ?>
                        <?php $form = ActiveForm::begin(['id' => 'login-form', 'action'=> ['/user/security/login'], 'options' => ['class'=>'form-horizontal']]); ?>

                        <?= $form->field($model, 'login')->textInput(['autofocus' => true, 'placeholder' => 'Email đăng ký'])->label(false) ?>

                        <?= $form->field($model, 'password')->passwordInput(['placeholder'=>'Mật khẩu'])->label(false) ?>

                        <div class="form-group option-login">
                            <div class="col-md-6 col-xs-6 remember">
                                <div class="checkbox">
                                    <input id="box1" type="checkbox" name="login-form[rememberMe]" />
                                    <label for="box1">Ghi nhớ mật khẩu</label>
                                </div>
                            </div>
                            <div class="col-md-6 text-right col-xs-6">
                                <a href="#">Quên mật khẩu ?</a>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="form-group text-center">
                            <?= Html::submitButton('Đăng Nhập', ['class' => 'btn btn-default btn-register btn-lg', 'name' => 'login-button']) ?>
                        </div>

<!--                        <div class="login-or-type">-->
<!--                            <div class="text-or">Hoặc</div>-->
<!--                        </div>-->
<!--                        <a href="--><?php //echo (!empty($url)) ? $url: '/user/register' ?><!--">-->
<!--                            <div class="btn_lite btn_facebook_icon"><span class="ico_facebook"></span>Đăng nhập bằng Facebook</div>-->
<!--                        </a>-->
<!--                        <a href="--><?//= \yii\helpers\Url::toRoute(['/user/registration/register'])?><!--" class="button-facebook background-blue hover-bg-blue" data-push-state=false data-toggle="popup" data-target="#popup-register"><div class="btn_lite btn_facebook_icon">-->
<!--                                <span class="ico_facebook"></span> Đăng nhập bằng facebook</div>-->
<!--                        </a>-->

                        <?php ActiveForm::end(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
