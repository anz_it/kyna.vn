<link href="/css/404.css" rel="stylesheet"/> 
 
<section>
    <div class="get-height-header"></div>
    <div id="page-404">
        <div class="main-content">
            <div class="container">
                <div class="col-md-10 col-md-offset-1 col-xs-12 wrap">
                    <div class="col-md-6 col-xs-12 img content">
                        <img src="/img/404/404.png" class="img-responsive" alt="404"/>
                    </div><!--end .img-->
                    <div class="col-md-6 col-xs-12 text content">
                        <h2>Đừng lo lắng</h2>
                        <p>Trong cuộc sống chúng ta vẫn thường hay đi vào những ngã rẽ không đúng!<br />Đây không phải là lớp học Online bạn đang tìm!</p>
                        <p class="bold">Trở lại đúng hướng và xem các gợi ý tìm kiếm khóa học khác của chúng tôi ở bên dưới</p>
                        <ul>
                            <li><a href="#">Facebook Marketing</a></li>
                            <li><a href="#">Tiếng hoa</a></li>
                            <li><a href="#">Facebook Marketing</a></li>
                            <li><a href="#">Tiếng hoa</a></li>
                        </ul>
                    </div><!--end .text-->
                </div><!--end .wrap-->
            </div><!--end .container-->
        </div><!--end main-content-->
        
        <div class="main-list clearfix">
           
            <div class="container"> 
                <div><h3>Gợi ý khóa học dành cho bạn</h3></div>
                                  
                <ul class="wrap-list">
                    <li class="box-product">                            
                        <div class="wrap clearfix">
                            <div class="img">
                                <img src="/img/img.png" alt="" class="img-responsive"/>
                                <span class="time">40 phút</span>
                                <span class="background-detail">
                                    <span class="wrap-position">
                                        <a href="#" class="background-green hover-bg-green" data-toggle="modal" data-target="#popup-product">Xem chi tiết</a>
                                    </span>
                                </span>
                            </div><!--end .img-->
                            <div class="content">
                                <h4 class="title"><a href="#" class="color-green hover-color-green" data-toggle="modal" data-target="#popup-product">Khóa học 30 ngày để tự tin và giao tiếp hiệu quả</a></h4>
                                <span class="author color-xam">Dương Ngọc Dũng</span>
                                <span>Tiến sĩ Tôn giáo học, ĐH Boston</span>
                                <ul>
                                    <li>
                                        <span class="icon-star"> 
                                        <i class="fa fa-star color-yellow"></i> 
                                        <i class="fa fa-star color-yellow"></i> 
                                        <i class="fa fa-star-half-o color-yellow"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>                                         
                                        </span>
                                    </li>
                                    <!--
                                    <li>(1200 đánh giá)</li>
                                    -->
                                </ul>                        
                            </div><!--end .content -->
                            <div class="view-price">
                                <ul>     
                                    <li class="sale"><span>400.000đ</span></li>                           
                                    <li class="price"><span class="color-orange bold">300.000đ</span></li>
                                </ul>
                            </div><!--end .view-price-->        
                        </div><!--end .wrap-->                                                                                        
                    </li>
                    <li class="box-product">                            
                        <div class="wrap clearfix">
                            <div class="img">
                                <img src="/img/img.png" alt="" class="img-responsive"/>
                                <span class="time">40 phút</span>
                                <span class="background-detail">
                                    <span class="wrap-position">
                                        <a href="#" class="background-green hover-bg-green" data-toggle="modal" data-target="#popup-product">Xem chi tiết</a>
                                    </span>
                                </span>
                            </div><!--end .img-->
                            <div class="content">
                                <h4 class="title"><a href="#" class="color-green hover-color-green" data-toggle="modal" data-target="#popup-product">Khóa học 30 ngày để tự tin và giao tiếp hiệu quả</a></h4>
                                <span class="author color-xam">Dương Ngọc Dũng</span>
                                <span>Tiến sĩ Tôn giáo học, ĐH Boston</span>
                                <ul>
                                    <li>
                                        <span class="icon-star"> 
                                        <i class="fa fa-star color-yellow"></i> 
                                        <i class="fa fa-star color-yellow"></i> 
                                        <i class="fa fa-star-half-o color-yellow"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>                                         
                                        </span>
                                    </li>
                                    <!--
                                    <li>(1200 đánh giá)</li>
                                    -->
                                </ul>                        
                            </div><!--end .content -->
                            <div class="view-price">
                                <ul>     
                                    <li class="sale"><span>400.000đ</span></li>                           
                                    <li class="price"><span class="color-orange bold">300.000đ</span></li>
                                </ul>
                            </div><!--end .view-price-->        
                        </div><!--end .wrap-->                                                                                        
                    </li>
                    <li class="box-product">                            
                        <div class="wrap clearfix">
                            <div class="img">
                                <img src="/img/img.png" alt="" class="img-responsive"/>
                                <span class="time">40 phút</span>
                                <span class="background-detail">
                                    <span class="wrap-position">
                                        <a href="#" class="background-green hover-bg-green" data-toggle="modal" data-target="#popup-product">Xem chi tiết</a>
                                    </span>
                                </span>
                            </div><!--end .img-->
                            <div class="content">
                                <h4 class="title"><a href="#" class="color-green hover-color-green" data-toggle="modal" data-target="#popup-product">Khóa học 30 ngày để tự tin và giao tiếp hiệu quả</a></h4>
                                <span class="author color-xam">Dương Ngọc Dũng</span>
                                <span>Tiến sĩ Tôn giáo học, ĐH Boston</span>
                                <ul>
                                    <li>
                                        <span class="icon-star"> 
                                        <i class="fa fa-star color-yellow"></i> 
                                        <i class="fa fa-star color-yellow"></i> 
                                        <i class="fa fa-star-half-o color-yellow"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>                                         
                                        </span>
                                    </li>
                                    <!--
                                    <li>(1200 đánh giá)</li>
                                    -->
                                </ul>                        
                            </div><!--end .content -->
                            <div class="view-price">
                                <ul>     
                                    <li class="sale"><span>400.000đ</span></li>                           
                                    <li class="price"><span class="color-orange bold">300.000đ</span></li>
                                </ul>
                            </div><!--end .view-price-->        
                        </div><!--end .wrap-->                                                                                        
                    </li>
                    <li class="box-product">                            
                        <div class="wrap clearfix">
                            <div class="img">
                                <img src="/img/img.png" alt="" class="img-responsive"/>
                                <span class="time">40 phút</span>
                                <span class="background-detail">
                                    <span class="wrap-position">
                                        <a href="#" class="background-green hover-bg-green" data-toggle="modal" data-target="#popup-product">Xem chi tiết</a>
                                    </span>
                                </span>
                            </div><!--end .img-->
                            <div class="content">
                                <h4 class="title"><a href="#" class="color-green hover-color-green" data-toggle="modal" data-target="#popup-product">Khóa học 30 ngày để tự tin và giao tiếp hiệu quả</a></h4>
                                <span class="author color-xam">Dương Ngọc Dũng</span>
                                <span>Tiến sĩ Tôn giáo học, ĐH Boston</span>
                                <ul>
                                    <li>
                                        <span class="icon-star"> 
                                        <i class="fa fa-star color-yellow"></i> 
                                        <i class="fa fa-star color-yellow"></i> 
                                        <i class="fa fa-star-half-o color-yellow"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>                                         
                                        </span>
                                    </li>
                                    <!--
                                    <li>(1200 đánh giá)</li>
                                    -->
                                </ul>                        
                            </div><!--end .content -->
                            <div class="view-price">
                                <ul>     
                                    <li class="sale"><span>400.000đ</span></li>                           
                                    <li class="price"><span class="color-orange bold">300.000đ</span></li>
                                </ul>
                            </div><!--end .view-price-->        
                        </div><!--end .wrap-->                                                                                        
                    </li>
                    <li class="box-product">                            
                        <div class="wrap clearfix">
                            <div class="img">
                                <img src="/img/img.png" alt="" class="img-responsive"/>
                                <span class="time">40 phút</span>
                                <span class="background-detail">
                                    <span class="wrap-position">
                                        <a href="#" class="background-green hover-bg-green" data-toggle="modal" data-target="#popup-product">Xem chi tiết</a>
                                    </span>
                                </span>
                            </div><!--end .img-->
                            <div class="content">
                                <h4 class="title"><a href="#" class="color-green hover-color-green" data-toggle="modal" data-target="#popup-product">Khóa học 30 ngày để tự tin và giao tiếp hiệu quả</a></h4>
                                <span class="author color-xam">Dương Ngọc Dũng</span>
                                <span>Tiến sĩ Tôn giáo học, ĐH Boston</span>
                                <ul>
                                    <li>
                                        <span class="icon-star"> 
                                        <i class="fa fa-star color-yellow"></i> 
                                        <i class="fa fa-star color-yellow"></i> 
                                        <i class="fa fa-star-half-o color-yellow"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>                                         
                                        </span>
                                    </li>
                                    <!--
                                    <li>(1200 đánh giá)</li>
                                    -->
                                </ul>                        
                            </div><!--end .content -->
                            <div class="view-price">
                                <ul>     
                                    <li class="sale"><span>400.000đ</span></li>                           
                                    <li class="price"><span class="color-orange bold">300.000đ</span></li>
                                </ul>
                            </div><!--end .view-price-->        
                        </div><!--end .wrap-->                                                                                        
                    </li>
                </ul>
            </div><!--end .container-->
        </div><!--end .main-banner-->
    </div><!--end #page-404-->
</section>

<script type="text/javascript" src="/js/404.js"></script>