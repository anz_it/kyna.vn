<?php
use yii\helpers\Url;
use yii\widgets\ActiveForm;
$this->title = "Mana.edu.vn - Empowering Your Career";
?>
<header>

    <div class="header">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center" style="top:119px; margin-bottom: 90px;height: 279px;">
                    <a href="#"><img src="/mana/images/mana_white.png" class="header-logo col-md-offset-3 col-md-6"></a>
                </div>
                <div class="col-md-12 text-center">
                    <div class="header-text">
                        Viện đào tạo quản trị kinh doanh trực tuyến hàng đầu
                    </div>
                    <div class="col-md-offset-3 col-md-6">
                        <form action="/danh-sach-khoa-hoc" class="career-search-form" method="get">
                            <div class="form-group has-success has-feedback">
                                <input type="text" name="q" class="form-control form-search" id="inputSuccess2" aria-describedby="inputSuccess2Status" placeholder="Tìm kiếm theo chương trình nghề nghiệp ...">
                                <span class="glyphicon glyphicon-search form-control-feedback" aria-hidden="true"></span>
                                <span id="inputSuccess2Status" class="sr-only">(success)</span>
                            </div>
                        </form>
                    </div>

                </div>
                <div class="col-md-12 text-center header-slogan text-uppercase ">HỌC TỪ XA MỌI LÚC MỌI NƠI - NHẬN BẰNG QUỐC Tế SẴN SÀNG HỘI NHẬP</div>

            </div>
        </div>
    </div>
    <div class="clearfix"></div>
</header>
<div class="wapper">
    <div class="container" style="border-bottom: 1px solid #b2b2b2">
        <div class="row">
            <div class="col-md-12 science-top" >
                <div class="text-center science-title ">Tại sao chọn <span class="text-uppercase" style="font-weight: bold;">MANA</span>?</div>
            </div>

            <div class="col-md-12 text-center line" ><img src="/mana/images/icon/2_Line.png"></div>
        
            <div class="col-md-12 benefit">
                <div class="col-md-4 col-xs-12 col-sm-4 benefit-item">                
                    <div class="media">
                        <div class="media-left">
                            <div class=""><span class="fa fa-plane fa-3x" aria-hidden="true"></span></div>
                        </div>
                        <div class="media-body">    
                            <div class="text-uppercase text-bold font_16px">CHƯƠNG TRÌNH CHUẨN QUỐC TẾ</div>
                            <div>Từ các trường đại học, viện đào tạo quốc tế.</div>
                        </div>
                    </div>
                </div>

                <div class="col-md-4 col-xs-12 col-sm-4 benefit-item">                
                    <div class="media">
                        <div class="media-left">
                            <div class=""><span class="fa fa-calendar fa-3x" aria-hidden="true"></span></div>
                        </div>
                        <div class="media-body">
                            <div class="text-uppercase text-bold font_16px">HỌC MỌI LÚC MỌI NƠI</div>
                            <div>Qua các thiết bị có kết nối internet.</div>
                        </div>
                    </div>
                </div>

                <div class="col-md-4 col-xs-12 col-sm-4 benefit-item">                
                    <div class="media">
                        <div class="media-left">
                            <div class=""><span class="fa fa-certificate fa-3x" aria-hidden="true"></span></div>
                        </div>
                        <div class="media-body">
                            <div class="text-uppercase text-bold font_16px">BẰNG CẤP ĐƯỢC QUỐC TẾ CÔNG NHẬN</div>
                            <div>Qua các thiết bị có kết nối internet.</div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-12 benefit">
                <div class="col-md-4 col-xs-12 col-sm-4 benefit-item">                
                    <div class="media">
                        <div class="media-left">
                            <div class=""><span class="fa fa-3x" aria-hidden="true"><img src="/mana/images/icon/6_Bubble.png" style="margin-top: -15px;"></span></div>
                        </div>
                        <div class="media-body">
                            <div class="text-uppercase text-bold font_16px">cố vấn học tập 1 kèm 1</div>
                            <div>Hỗ trợ trong suốt quá trình học tập, làm đề tài.</div>
                        </div>
                    </div>
                </div>

                <div class="col-md-4 col-xs-12 col-sm-4 benefit-item">                                        
                    <div class="media">
                        <div class="media-left">
                            <div class=""><span class="fa fa-users fa-3x" aria-hidden="true"></span></div>
                        </div>
                        <div class="media-body">
                            <div class="text-uppercase text-bold font_16px">MỞ RỘNG QUAN HỆ</div>
                            <div>Hỏi đáp cùng diễn giả doanh nhân. Thảo luận cùng các học viên khác.</div>
                        </div>
                    </div>
                </div>

                <div class="col-md-4 col-xs-12 col-sm-4 benefit-item">                                    
                    <div class="media">
                        <div class="media-left">
                            <div class=""><span class="fa fa-book fa-3x" aria-hidden="true"></span></div>
                        </div>
                        <div class="media-body">
                            <div class="text-uppercase text-bold font_16px">KHO TÀI LIỆU PHONG PHÚ</div>
                            <div>Tài liệu học tập và tham khảo từ Harvard, Stanford được biên phiên dịch dễ hiểu.</div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<div class="wapper" style="margin-bottom: 49px;">
    <div class="container">
        <div class="row">
            <div class="col-md-12" >
                <div class=" text-center science-title ">Chuyên ngành</div>
            </div>

            <div class="col-md-12 text-center line" ><img src="/mana/images/icon/2_Line.png"></div>

            <div class="col-md-12 subject-items">

                <div class="row">
                    <?php if ($listSubjects) {
                        foreach ($listSubjects as $subject) {
                            ?>
                            <div class="col-md-3 col-sm-6">
                                <a href="/chuyen-nganh/danh-sach-khoa-hoc/<?=$subject->slug?>" class="subject-item">
                                    <img src="<?=$subject->image_url?>" style="width: 100%;">
                                    <div class="text-center subject-text"><?=$subject->name?></div>
                                </a>
                            </div>
                            <?php
                        }
                    }
                    ?>
                </div>
            </div>
            <div class="col-md-12 text-center">
                <a href="/chuyen-nganh" class="btn btn-default btn-see-all">Xem tất cả</a>
            </div>

        </div>
    </div>
</div>
<div class="partner">
    <div class="container">
        <div class="row">
            <div class="col-md-12 science-top" >
                <div class=" text-center science-title ">Đối tác</div>
            </div>
            <div class="col-md-12 text-center line" style=""><img src="/mana/images/icon/2_Line.png"></div>
            <!--            <div class="col-md-12 icon-logo" style="margin-bottom: 49px">-->
            <!--                <div class="col-md-offset-3 col-md-2 col-sm-4 col-xs-4"><a href="#" style="min-height: 50px;display: block;"><img src="https://c2.staticflickr.com/8/7514/27569525734_1aede8ecae_o.png" class="col-md-12 col-xs-12"></a></div>-->
            <!--                <div class="col-md-2 col-sm-4 col-xs-4"><a href="#" style="min-height: 50px;display: block;"><img src="/mana/images/logo/partners/11_ibta.png" class="col-md-12 col-xs-12"></a></div>-->
            <!--                <div class="col-md-2 col-sm-4 col-xs-4"><a href="#" style="min-height: 50px;display: block;"><img src="/media/images/introduction/logo_v5.png" class="col-md-12 col-xs-12"></a></div>-->
            <!--            </div>-->
            <div class="col-md-offset-3 col-md-6" style="margin-bottom: 49px">
                <ul class="bxslider">
                    <li>
                        <a href="/to-chuc-cap-chung-chi/danh-sach-khoa-hoc/athe-awards-for-training-and-higher-education">
                            <img src="/mana/images/logo/slider/athe.png" />
                        </a>
                    </li>
                    <li>
                        <a href="/to-chuc-cap-chung-chi/danh-sach-khoa-hoc/international-business-training-association-ibta">
                            <img src="/mana/images/logo/slider/IBTA.png" />
                        </a>
                    </li>
                    <li>
                        <a href="/to-chuc-cap-chung-chi/danh-sach-khoa-hoc/kyna">
                            <img src="/mana/images/logo/slider/KYNA.png" />
                        </a>
                    </li>
                </ul>
            </div>


            <div class="col-md-12 icon-logo hidden" style="margin-bottom: 49px">
                <div class="row">
                    <div class="col-md-2 col-sm-4 col-xs-4"><a href="#" style="min-height: 50px;display: block;"><img src="/mana/images/logo/partners/7_Northampton.png" class="col-md-12 col-xs-12"></a></div>
                    <div class="col-md-2 col-sm-4 col-xs-4"><a href="#" style="min-height: 50px;display: block;"><img src="/mana/images/logo/partners/8_Prince-Ed.png" class="col-md-12 col-xs-12"></a></div>
                    <div class="col-md-2 col-sm-4 col-xs-4"><a href="#" style="min-height: 50px;display: block;"><img src="/mana/images/logo/partners/9_Sunderland.png" class="col-md-12 col-xs-12"></a></div>
                    <div class="col-md-2 col-sm-4 col-xs-4"><a href="#" style="min-height: 50px;display: block;"><img src="/mana/images/logo/partners/10_Texas.png" class="col-md-12 col-xs-12"></a></div>
                    <div class="col-md-2 col-sm-4 col-xs-4"><a href="#" style="min-height: 50px;display: block;"><img src="/mana/images/logo/partners/11_ibta.png" class="col-md-12 col-xs-12"></a></div>
                    <div class="col-md-2 col-sm-4 col-xs-4 hidden"><a href="#" style="min-height: 50px;display: block;"><img src="/mana/images/logo/13_ibta.png" class="col-md-12 col-xs-12"></a></div>
                </div>
            </div>
        </div>
    </div>
</div>

<?= \mana\widgets\RegisterWidget::widget(['forHome' => true])?>
<script type="text/javascript">
    $(document).ready(function(){
        $('.bxslider').bxSlider({
//            slideWidth: 300,
            responsive: true,
            pager: false,
            prevText: '',
            nextText: '',
            auto: true
        });
    });
</script>
