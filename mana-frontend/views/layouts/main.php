<?php
use mana\assets\AppAsset;
use yii\helpers\Html;
use mana\widgets\HeaderWidget;
use mana\widgets\FooterWidget;
AppAsset::register($this);

$this->beginPage();
?>
<!DOCTYPE HTML>
<html  lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <link rel="icon" href="/mana/images/manaFavicon.png">
    <!-- LINK FONT -->
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,500,700,300' rel='stylesheet' type='text/css'>
    
    

</head>

<body <?php if(!empty($this->context->bodyClass)) { ?> class="<?= $this->context->bodyClass; ?>" <?php } ?> >
<!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-M92WKP');</script>
    <!-- End Google Tag Manager -->
<?php $this->beginBody()?>


    <?= HeaderWidget::widget(); ?>

        <?= $content; ?>

    <?= FooterWidget::widget(); ?>

<?php $this->endBody() ?>
    
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-M92WKP"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->

</body>
</html>
<?php $this->endPage() ?>
