<?php
/**
 * Created by PhpStorm.
 * User: khanhphan
 * Date: 2/24/17
 * Time: 9:46 AM
 *
 * @var $this \yii\web\View
 */
$this->beginPage();
$this->registerJsFile('/js/pages/init.js');
?>
<?= $content; ?>
<?php $this->endPage(); ?>