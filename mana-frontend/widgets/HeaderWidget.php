<?php
/**
 * @author: Hien Nguyen
 * @desc: Header Widget
 */
namespace mana\widgets;

use kyna\mana\models\Certificate;
use kyna\mana\models\Education;
use kyna\mana\models\Organization;
use kyna\mana\models\Subject;
use yii;
use common\widgets\base\BaseWidget;

class HeaderWidget extends BaseWidget{

    public function run()
    {
        return $this->getHeader();
    }

    public function getHeader()
    {
        $listSubjects = Subject::find()->where(['status'=>Subject::STATUS_ACTIVE, 'parent_id' => 0])->all();
        $listEducations = Education::findAllActive();
        $listCertificates = Certificate::findAllActive();
        $listOrganizations = Organization::findAllActive();
        return $this->render('header',[
            'listSubjects' => $listSubjects,
            'listEducations' => $listEducations,
            'listCertificates' => $listCertificates,
            'listOrganizations' => $listOrganizations,
        ]);
    }
}