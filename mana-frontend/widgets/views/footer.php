<footer>
    <div class="col-md-12">
        <div class="col-md-4 text-center logo-footer">
            <div style="margin-bottom: 29px"><a href="#"><img src="/mana/images/logo/14_Mana.png"></a></div>
            <div><a href="#"><img src="/mana/images/logo/14_Kyna.png"></a></div>
        </div>

        <div class="col-md-4 text-left address">
            <div style="font-weight: bold">Công ty Cổ phần  Dream Việt Education</div>
            <div><u>Địa chỉ ĐKKD:</u> Tầng 1, Tòa nhà Packsimex, 52 Đông Du, Phường Bến Nghé, Quận 1, Thành phố Hồ Chí Minh</div>
            <div><u>Văn phòng Hồ Chí Minh:</u> Lầu 6 Tòa nhà Thịnh Phát - 178/8 Đường D1, Phường 25, Quận Bình Thạnh, TP Hồ Chí Minh</div>
            <div><u>Văn phòng Hà Nội:</u> Tầng 6, Tòa Nhà Viện Công Nghệ - 25 Vũ Ngọc Phan, Phường Láng Hạ, Quận Đống Đa, TP Hà Nội</div>
            <div style="margin-top: 29px">Giấy phép ĐKKD số 0313589030 do Sở Kế hoạch và Đầu tư TPHCM cấp</div>
        </div>

        <div class="col-md-4 text-left address">
            <div><u style="font-weight: bold">Hotline:</u> 1900 6364 09</div>
            <div>Thứ 2 – thứ 6: từ 08h30 – 21h00</div>
            <div>Thứ 7: 08h30 – 17h00</div>
            <div><u style="font-weight: bold">Email:</u> hotro@kyna.vn</div>
        </div>
    </div>
    <div class="clearfix"></div>
    <!-- POPUP  -->
    <div class="modal fade popup-form-header" id="popup-register" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content popup-order">
            </div>
        </div>
    </div>
    <!-- END POPUP -->
</footer>