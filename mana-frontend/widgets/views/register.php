<?php
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
?>
<?php if ($forHome) { ?>
    <div class="wapper register-home" style="margin-bottom: 49px;">
        <div class="container">
            <div class="row">
                <div class="col-md-12" >
                    <div class="text-center science-title">Đăng ký nhận tư vấn</div>
                </div>

                <div class="col-md-12 text-center line" ><img src="/mana/images/icon/2_Line.png"></div>
                <div class="col-md-offset-4 col-md-4" style="z-index: 1;">


                    <?php if (Yii::$app->session->hasFlash('success')) { ?>
                        <div class='alert alert-success'>
                            <p><?=Yii::$app->session->getFlash('success')?></p>
                        </div>
                    <?php } ?>
                    <?php $form = ActiveForm::begin(['action' => \yii\helpers\Url::toRoute(['/user/contact/index'])]); ?>
                    <div class="row">
                        <div class="col-md-12">

                            <?= $form->field($model, 'name')->textInput(['maxlength' => true, 'placeholder' => 'Họ và tên'])->label(false) ?>

                        </div>
                        <div class="col-md-12">

                            <?= $form->field($model, 'phone')->textInput(['maxlength' => true, 'placeholder' => $model->getAttributeLabel('phone')])->label(false) ?>
                        </div>
                        <div class="col-md-12">

                            <?= $form->field($model, 'email')->textInput(['maxlength' => true, 'placeholder' => $model->getAttributeLabel('email')])->label(false) ?>

                        </div>
                        <div class="col-md-12 custom-select-box">

                            <?= $form->field($model, 'subject_id')->dropDownList(ArrayHelper::map($listSubjects, 'id', 'name'), ['prompt' => 'Chuyên ngành quan tâm'])->label(false) ?>

                        </div>

                        <div class="col-md-12 custom-select-box">
                            <?= $form->field($model, 'education_id')->dropDownList(ArrayHelper::map($listEducations, 'id', 'name'), ['prompt' => $model->getAttributeLabel('education_id')])->label(false) ?>
                        </div>

                        <div class="col-md-12 custom-select-box">
                            <?= $form->field($model, 'certificate_id')->dropDownList(ArrayHelper::map($listCertificates, 'id', 'name'), ['prompt' => $model->getAttributeLabel('certificate_id')])->label(false) ?>
                        </div>

                        <div class="col-md-12 text-center">
                            <?= \yii\helpers\Html::submitButton('Đăng ký', ['class' => 'btn btn-default btn-register btn-lg']) ?>
                        </div>
                    </div>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>
<?php } else { ?>
    <div class="wrapper register">
        <div class="container">
            <div class="row">
                <div class="col-md-12" >
                    <div class="text-center science-title">Đăng ký nhận tư vấn</div>
                </div>

                <div class="col-md-12">
                    <?php if (Yii::$app->session->hasFlash('success')) { ?>
                        <div class='alert alert-success'>
                            <p><?=Yii::$app->session->getFlash('success')?></p>
                        </div>
                    <?php } ?>
                    <?php $form = ActiveForm::begin(['action' => \yii\helpers\Url::toRoute(['/user/contact/index'])]); ?>
                    <div class="col-md-4">

                        <?= $form->field($model, 'name')->textInput(['maxlength' => true, 'placeholder' => 'Họ và tên'])->label(false) ?>

                    </div>
                    <div class="col-md-4">

                        <?= $form->field($model, 'phone')->textInput(['maxlength' => true, 'placeholder' => $model->getAttributeLabel('phone')])->label(false) ?>
                    </div>
                    <div class="col-md-4">

                        <?= $form->field($model, 'email')->textInput(['maxlength' => true, 'placeholder' => $model->getAttributeLabel('email')])->label(false) ?>

                    </div>
                    <div class="col-md-4 custom-select-box">

                        <?= $form->field($model, 'subject_id')->dropDownList(ArrayHelper::map($listSubjects, 'id', 'name'), ['prompt' => 'Chuyên ngành quan tâm'])->label(false) ?>

                    </div>

                    <div class="col-md-4 custom-select-box">
                        <?= $form->field($model, 'education_id')->dropDownList(ArrayHelper::map($listEducations, 'id', 'name'), ['prompt' => $model->getAttributeLabel('education_id')])->label(false) ?>
                    </div>

                    <div class="col-md-4 custom-select-box">
                        <?= $form->field($model, 'certificate_id')->dropDownList(ArrayHelper::map($listCertificates, 'id', 'name'), ['prompt' => $model->getAttributeLabel('certificate_id')])->label(false) ?>
                    </div>

                    <div class="col-md-12 text-center">
                        <?= \yii\helpers\Html::submitButton('Đăng ký', ['class' => 'btn btn-default btn-register btn-lg']) ?>
                    </div>
                    <div class="col-md-12 text-center">
                        <span class="text-center">Bộ phận chăm sóc khách hàng của MANA Business School sẽ liên lạc sớm với bạn để tư vấn về khóa học (mức phí tư vấn là 0đ).</span>
                    </div>
                    <?php ActiveForm::end(); ?>

                </div>


            </div>
        </div>
    </div>
<?php } ?>

