<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ActiveForm;
?>
<div class="menu-top">
    <!-- Static navbar -->
    <nav class="navbar navbar-default navbar-static-top banner">
        <div class="container">
            <div class="navbar-header">
                <a class="navbar-brand" href="<?=Url::toRoute(['/site/index'])?>">
                    <img src="/mana/images/mana-alternative-07.png" class="img_logo" />
                </a>
            </div>

            <div id="navbar" class="navbar-collapse collapse">
                <?php

                if (!Yii::$app->requestedRoute == "") {
                    ?>
                    <ul class="nav navbar-nav navbar-left col-md-4">
                        <li class="col-md-12">
                            <form action="/danh-sach-khoa-hoc" method="get" class="header-search-form">
                                <div class="form-group has-success has-feedback ">
                                    <input type="text" name="q" class="form-control form-search" aria-describedby="inputSuccess2Status" placeholder="Tìm kiếm...">
                                    <span class="glyphicon glyphicon-search form-control-feedback" aria-hidden="true"></span>
                                </div>
                            </form>
                        </li>
                    </ul>
                    <?php
                }
                ?>
                <ul class="nav navbar-nav navbar-right">
                    <li class="join-class text-right"><a href="<?=Url::toRoute(['/user/course/index'])?>"><i class="fa fa-bank"></i> Vào lớp học</a></li>
                    <?php if (!Yii::$app->user->isGuest) { ?>
                        <li class="dropdown">
                            <a href="#" class="avatar" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                <img src="/mana/images/AvaMana.png" />
                                <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu" aria-labelledby="drop3">
                                <li>
                                    <?=Html::a("Thông tin cá nhân", ['/site/index']) ?>
                                <li>
                                    <?=Html::a("Thoát", ['/site/logout'], ['data-method' => 'post']) ?>
                                </li>
                            </ul>
                        </li>
                        <?php } ?>
                    <li class="contact">
                        <a href="./"><i class="fa fa-phone"></i> 1900 6364 09</a>
                        <a href="./"><i class="fa fa-envelope-o"></i> hotro@kyna.vn</a>
                    </li>
                </ul>
            </div><!--/.nav-collapse -->
        </div>
    </nav>

    <nav class="navbar navbar-static-top menu">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar_menu" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
            <div id="navbar_menu" class="navbar-collapse collapse">
                <ul class="nav navbar-nav col-md-12 col-sm-12">
                    <li class="dropdown">
                        <a href="/chuyen-nganh">Chuyên ngành</a>
                        <ul class="dropdown-menu" role="menu">
                            <?php
                            if ($listSubjects) {
                                foreach ($listSubjects as $item) {
                                    ?>
                                    <li class="col-xs-12 col-md-4 menu-item">
                                        <?php if ($subjectChilds = $item->children) { ?>
                                            <a href='javascript:void(0)'><?=$item->name?></a>
                                            <ul class="collapse" role="menu">

                                                <?php foreach ($subjectChilds as $child) { ?>
                                                    <li>
                                                        <a href="/chuyen-nganh/danh-sach-khoa-hoc/<?=$child->slug?>" class="link"><?=$child->name?></a>
                                                    </li>
                                                <?php } ?>

                                            </ul>
                                        <?php } else { ?>

                                            <a href="/chuyen-nganh/danh-sach-khoa-hoc/<?=$item->slug?>" class="link">
                                                <?=$item->name?>
                                            </a>
                                        <?php } ?>
                                    </li>
                                    <?php
                                }
                            }

                            ?>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="/he-dao-tao">Hệ đào tạo</a>
                        <ul class="dropdown-menu" role="menu">
                            <?php
                                if ($listEducations) {
                                    foreach ($listEducations as $item) {
                                        ?>
                                        <li class="col-xs-12 col-md-4 menu-item">
                                            <a href="/he-dao-tao/danh-sach-khoa-hoc/<?=$item->slug?>" class="link">
                                                <?=$item->name?>
                                            </a>
                                        </li>
                                        <?php
                                    }
                                }

                            ?>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="/chung-chi">Chứng chỉ</a>
                        <ul class="dropdown-menu" role="menu">
                            <?php
                            if ($listCertificates) {
                                foreach ($listCertificates as $item) {
                                    ?>
                                    <li class="col-xs-12 col-md-4 menu-item">
                                        <a href="/chung-chi/danh-sach-khoa-hoc/<?=$item->slug?>" class="link">
                                            <?=$item->name?>
                                        </a>
                                    </li>
                                    <?php
                                }
                            }

                            ?>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="/to-chuc-cap-chung-chi">Tổ chức đào tạo / Tổ chức cấp chứng chỉ</a>
                        <ul class="dropdown-menu" role="menu">
                            <div class="col-md-6 left">
                                <li class="col-xs-12 col-md-12 text-left">
                                    <a href="#" class="link">
                                        TỔ CHỨC ĐÀO TẠO
                                    </a>
                                </li>
                                <li class="col-xs-12 col-md-4 menu-item">
                                    <a href="/gioi-thieu
                                    " class="link" target="_blank">
                                        <img src="/mana/images/mana-alternative-07.png" style="height:auto;max-width: 85%;">
                                    </a>
                                </li>
                                <li class="col-xs-12 col-md-4 menu-item">
                                    <a href="https://kyna.vn/p/kyna/gioi-thieu" class="link" target="_blank">
                                        <img src="/mana/images/logo_v5.png" style="height:auto;max-width: 100%;">
                                    </a>
                                </li>

                            </div>
                            <div class="col-md-6 right">
                                <li class="col-xs-12 col-md-12 text-left">
                                    <a href="#" class="link">
                                        TỔ CHỨC CẤP CHỨNG CHỈ
                                    </a>
                                </li>
                                <?php
                                if ($listOrganizations) {
                                    foreach ($listOrganizations as $item) {
                                        ?>
                                        <li class="col-xs-12 col-md-4 menu-item">
                                            <a href="<?= Url::toRoute(['/course/organization/view', 'slug' => $item->slug]) ?>"
                                               class="link">
                                                <?php if ($item->slug == "mana-online-bussiness-school") { ?>
                                                    <img src="<?= $item->image_url ?>" class="img-responsive"
                                                         style="height:auto;max-width: 85%;">
                                                <?php } else { ?>
                                                    <img src="<?= $item->image_url ?>" class="img-responsive">
                                                <?php } ?>

                                            </a>
                                        </li>
                                        <?php
                                    }
                                }
                                ?>
                            </div>
                        </ul>
                    </li>
                </ul>
            </div><!--/.nav-collapse -->
        </div>
    </nav>
</div>