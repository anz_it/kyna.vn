<?php
/**
 * @author: Hien Nguyen
 * @desc: Header Widget
 */
namespace mana\widgets;

use yii;
use common\widgets\base\BaseWidget;

class FooterWidget extends BaseWidget{

    public function run()
    {
        return $this->getFooter();
    }

    public function getFooter()
    {
        return $this->render('footer',[
        ]);
    }
}