<?php
/**
 * @author: Hien Nguyen
 * @desc: Header Widget
 */
namespace mana\widgets;

use kyna\mana\models\Certificate;
use kyna\mana\models\Contact;
use kyna\mana\models\Education;
use kyna\mana\models\Subject;
use yii;
use common\widgets\base\BaseWidget;

class RegisterWidget extends BaseWidget{

    public $forHome = false;
    public function run()
    {
        return $this->getRegister();
    }

    public function getRegister()
    {
        $model = new Contact();
        $listSubjects = Subject::findAllActive();
        $listCertificates = Certificate::findAllActive();
        $listEducations = Education::findAllActive();
        return $this->render('register',[
            'forHome' => $this->forHome,
            'model' => $model,
            'listEducations' => $listEducations,
            'listCertificates' => $listCertificates,
            'listSubjects' => $listSubjects
        ]);
    }
}