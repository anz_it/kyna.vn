<?php
/**
 * Created by PhpStorm.
 * User: Hien Nguyen
 * Date: 10/8/2016
 * Time: 8:38 AM
 */

namespace mana\models;


class Setting
{
    const HOTLINE = '1900 6364 09';
    const DIA_CHI_DKKD = 'Tầng 1, Tòa nhà Packsimex, 52 Đông Du, Phường Bến Nghé, Quận 1, Thành phố Hồ Chí Minh';
    const DIA_CHI_VAN_PHONG = 'Lầu 6 Tòa nhà Thịnh Phát - 178/8 Đường D1, Phường 25, Quận Bình Thạnh, TP Hồ Chí Minh';
    const DIA_CHI_VAN_PHONG_HA_NOI = 'Tầng 6, Tòa Nhà Viện Công Nghệ - 25 Vũ Ngọc Phan, Phường Láng Hạ, Quận Đống Đa, TP Hà Nội';
}