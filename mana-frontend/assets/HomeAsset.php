<?php

namespace mana\assets;

use yii\web\AssetBundle;
use \yii\web\View;

/**
 * This is class asset bunle for `home` layout
 */
class HomeAsset extends AssetBundle
{

    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        // FontAwesome
        "css/font-awesome.min.css",
        // Style main
        "css/style.css",
        "css/media.css",
        "css/owl.carousel.css",
        "css/owl.theme.css",
        "css/owl.transitions.css",
        // Style Menu Mobile
        "css/jquery.sidr.dark.css"
    ];
    public $js = [
        ["js/bootstrap.min.js", 'position' => View::POS_END],
        ["js/owl.carousel.min.js", 'position' => View::POS_END],
        // JS Menu Mobile
        ["js/jquery.sidr.min.js", 'position' => View::POS_END],

        ["js/script-main.js", 'position' => View::POS_END],
        ["js/script-home.js", 'position' => View::POS_END],

        // cart
        ["js/check-payment.js", 'position' => View::POS_END],
        // ajax
        ["js/ajax-caller.js", 'position' => View::POS_END],
        ["js/facebook.js", 'position' => View::POS_END],
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
    public $jsOptions = ['position' => View::POS_HEAD];
}
