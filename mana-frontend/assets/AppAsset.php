<?php
/**
 * @link http://www.yiiframework.com/
 *
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace mana\assets;

use yii\web\AssetBundle;
use yii\web\View;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 *
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        // FontAwesome
        'css/font-awesome.min.css',
        // Style main
        'mana/css/style.css',
            ];
    public $js = [
        ['js/bootstrap.min.js', 'position' => View::POS_END],
        ['mana/bxslider/jquery.bxslider.min.js', 'position' => View::POS_END],
        // JS Menu Mobile
        ['js/jquery.sidr.min.js', 'position' => View::POS_END],

        ['js/script-main.js', 'position' => View::POS_END],
        // cart
        ['mana/js/home.js', 'position' => View::POS_END],
        // ajax
        ['js/ajax-caller.js', 'position' => View::POS_END],
        ["js/facebook.js", 'position' => View::POS_END],
        ["js/user-info.js", 'position' => View::POS_END],
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
    public $jsOptions = ['position' => View::POS_HEAD];
}
