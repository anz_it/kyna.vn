<?php

namespace kyna\servicecaller;

abstract class BaseCaller
{
    protected static $defaultMethod = 'GET';

    public $errors = [];

    abstract protected function signIn();
    //abstract protected function callerOptions();

    protected function setToken($token)
    {
        $cacheKey = md5(get_class($this));

        \Yii::$app->cache->set($cacheKey, $token);
    }
    protected function getToken()
    {
        $cacheKey = md5(get_class($this));

        return \Yii::$app->cache->get($cacheKey);
    }
    protected function deleteToken()
    {
        $cacheKey = md5(get_class($this));
        \Yii::$app->cache->delete($cacheKey);
    }
}
