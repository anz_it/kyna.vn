<?php

namespace kyna\servicecaller\traits;

trait CurlTrait {
    protected function callerOptions() {
        return [];
    }

    protected function beforeCall(&$ch, &$data, $method = false)
    {
        // do nothing
        // TODO: init Curl Handler and preserve data for fucntion calling
    }
    protected function afterCall(&$response)
    {
        // do nothing
    }

    protected function call($url, $data = null, $method = false)
    {
        if (!$method) {
            $method = isset(self::$defaultMethod) ? self::$defaultMethod : 'GET';
        }

        try {
            $ch = curl_init();

            if (false === $ch) {
                throw new Exception('failed to initialize');
            }

            $this->beforeCall($ch, $data, $method);

            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            curl_setopt($ch, CURLOPT_TIMEOUT, 60);

            $options = $this->callerOptions();
            foreach ($options as $name => $value) {
                curl_setopt($ch, $name, $value);
            }
            $response = curl_exec($ch);

            $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

            if ($httpcode !== 200) {
                $logData = [
                    'method' => $method,
                    'url' => $url,
                    'data' => $data,
                    'http_code' => $httpcode,
                ];
                \Yii::error($logData, "curl");
            }

            if (false === $response) {
                throw new \Exception(curl_error($ch), curl_errno($ch));
            }

            $this->afterCall($response, $method);

            curl_close($ch);
        }
        catch(\Exception $e) {
            trigger_error(sprintf('Curl failed with error #%d: %s', $e->getCode(), $e->getMessage()), E_USER_ERROR);
        }

        return $response;
    }
}
