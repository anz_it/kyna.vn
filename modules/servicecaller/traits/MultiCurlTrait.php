<?php

namespace kyna\servicecaller\traits;

trait MultiCurlTrait
{
    protected function callerOptions()
    {
        return [];
    }

    protected function beforeCall(&$ch, &$data, $method = false)
    {
        // do nothing
        // TODO: init Curl Handler and preserve data for function calling
    }

    protected function afterCall(&$response)
    {
        // do nothing
    }

    protected function call($data = null)
    {
        try {
            $multiCurl = [];
            $result = [];
            $mch = curl_multi_init();

            if ($mch === false) {
                throw new \Exception('Failed to initialize multi curl');
            }

            $this->beforeCall($data);

            // init data, options for each curl
            foreach ($data as $index => $item) {
                $multiCurl[$index] = curl_init();
                if (!empty($item['header'])) {
                    curl_setopt($multiCurl[$index], CURLOPT_HTTPHEADER, $item['header']);
                }
                curl_setopt($multiCurl[$index], CURLOPT_RETURNTRANSFER, true);
                curl_setopt($multiCurl[$index], CURLOPT_URL, $item['url']);
                curl_setopt($multiCurl[$index], CURLOPT_CUSTOMREQUEST, $item['method']);
                curl_setopt($multiCurl[$index], CURLOPT_POSTFIELDS, $item['data']);
                $options = $this->callerOptions();
                if (!empty($options)) {
                    curl_setopt_array($multiCurl[$index], $options);
                }
                curl_multi_add_handle($mch, $multiCurl[$index]);
            }

            // execute curl
            $isRunning = null;
            do {
                curl_multi_exec($mch, $isRunning);
            } while ($isRunning > 0);

            // get content
            foreach($multiCurl as $index => $curl) {
                $result[$index] = curl_multi_getcontent($curl);
                curl_multi_remove_handle($mch, $curl);
            }

            $this->afterCall($result);

            // all done
            curl_multi_close($mch);

            return $result;
        } catch (\Exception $e) {
            \Yii::$app->logger->log($e->getMessage(), \common\components\GoogleStackDriverLog::LEVEL_CRITICAL, $e->getTraceAsString());
        }
    }
}
