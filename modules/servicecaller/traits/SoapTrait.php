<?php

namespace kyna\servicecaller\traits;

trait SoapTrait
{
    protected function callerOptions() {
        return [];
    }

    protected function beforeCall(&$ch, &$data)
    {
        // do nothing
        // TODO: init Curl Handler and preserve data for fucntion calling
    }
    protected function afterCall(&$response)
    {
        // do nothing
    }

    protected function call($command, $wsdl, ...$args)
    {
        $options = $this->callerOptions();
        $context = stream_context_create([
            'ssl' => [
                // set some SSL/TLS specific options
                'verify_peer' => false,
                'verify_peer_name' => false,
                'allow_self_signed' => true
            ]
        ]);
        $options['stream_context'] = $context;

        try {
            $client = new \SoapClient($wsdl, $options);

            $result = $client->$command(...$args);

            return $result;
        } catch (\SoapFault $fault) {
            trigger_error("SOAP Fault: (faultcode: {$fault->faultcode}, faultstring: {$fault->faultstring})", E_USER_ERROR);
        }
    }
}
