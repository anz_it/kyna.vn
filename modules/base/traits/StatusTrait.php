<?php

namespace kyna\base\traits;

use yii\helpers\Url;

trait StatusTrait
{

    /**
     * @desc return list status for renders
     * @return array
     */

    public static function listStatus()
    {
        $list = [
            self::STATUS_ACTIVE => 'Hiện',
            self::STATUS_DEACTIVE => 'Ẩn',
        ];

        return $list;
    }

    public static function listImportant()
    {
        $list = [
            self::STATUS_ACTIVE => 'Có',
            self::STATUS_DEACTIVE => 'Không',
        ];

        return $list;
    }

    /**
     * @desc convert and return status by text
     * @return text or null
     */
    public function getStatusText() // TODO: move to trait
    {
        $listStatus = static::listStatus();
        if (isset($listStatus[$this->status])) {
            return $listStatus[$this->status];
        }
        return null;
    }
    public function getImportantText() // TODO: move to trait
    {
        $listStatus = static::listImportant();
        if (isset($listStatus[$this->is_important])) {
            return $listStatus[$this->is_important];
        }
        return null;
    }

    public function getStatusHtml() // TODO: move to trait
    {
        switch ($this->status) {
            case self::STATUS_ACTIVE:
                $labelClass = 'label-success';
                break;

            case self::STATUS_DEACTIVE:
                $labelClass = 'label-warning';
                break;

            default:
                $labelClass = 'label-default';
                break;
        }

        return '<span class="label ' . $labelClass . '">' . $this->statusText . '</span>';
    }

    public function getStatusButton($url = null, $field = 'status')
    {
        $contentToggle = static::listStatus();
        switch ($this->{$field}) {
            case self::STATUS_ACTIVE:
                $isChecked = true;
                break;

            case self::STATUS_DEACTIVE:
                $isChecked = false;
                break;

            default:
                $isChecked = false;
                break;
        }

        if ($url == null){
            $controller = \Yii::$app->controller;
            $url = Url::to(['/' . $controller->module->id . '/' . $controller->id . '/change-status', 'id' => $this->id, 'field' => $field]);
        }

        return '<input type="checkbox" 
        class="status-toggle" ' . ($isChecked ? 'checked' : '') . ' 
        data-toggle="toggle" 
        data-on="' . $contentToggle[self::STATUS_ACTIVE] .  '" data-off="' . $contentToggle[self::STATUS_DEACTIVE] . '" 
        data-href="' . $url . '" />';
    }

}
