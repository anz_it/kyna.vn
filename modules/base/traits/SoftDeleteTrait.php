<?php

namespace kyna\base\traits;

use kyna\base\ActiveRecord;
use Yii;

trait SoftDeleteTrait
{
    protected static function softDelete()
    {
        return FALSE;
    }

    /**
     *
     * @return \yii\db\ActiveQuery the newly created [[ActiveQuery]] instance
     */
    public static function find()
    {
        if (!static::softDelete()) {
            return parent::find();
        }

        return parent::find()->where(['OR', [static::tableName() . '.`is_deleted`' => ActiveRecord::BOOL_NO], [static::tableName() . '.`is_deleted`' => NULL] ]);
    }

}
