<?php

namespace kyna\base\traits;

use kyna\base\models\MetaField;
use Yii;
use yii\helpers\Inflector;

trait MetaTrait
{

    private  $_metaCache = [];
    /**
     * A temporary list of metadata, only save to database afterSave().
     *
     * @var array
     */
    private $_meta = [];

    /**
     * Set meta key enable.
     *
     * @return bool|string Name of meta, false if not set
     */
    protected function enableMeta()
    {
        return false;
    }

    /**
     * Define a list of metadata used in the object.
     *
     * @return array A non-associative array of met akey
     */
    protected function metaKeys()
    {
        $model = $this->enableMeta();
        if (!$model)
            return [];
        if (isset($this->_metaCache['metaKeys'])) {
            return $this->_metaCache['metaKeys'];
        }
        $metaFields = MetaField::find()->where([
            'status' => MetaField::STATUS_ACTIVE,
            'model' => $model, ])
            ->select("key")->column();
        $this->_metaCache['metaKeys'] = $metaFields;
        return $metaFields;

    }

    public function __get($key)
    {
        if (in_array($key, $this->metaKeys())) {
            return $this->getMeta($key);
        }

        return parent::__get($key);
    }

    public function __set($key, $value)
    {
        if (in_array($key, $this->metaKeys()) and ! is_null($value)) {
            $this->setMeta($key, $value);

            return;
        }

        parent::__set($key, $value);
    }

    public function afterSave($insert, $changedAttributes)
    {
        $this->saveMeta();

        return parent::afterSave($insert, $changedAttributes);
    }

    public function hasAttribute($name)
    {
        if (in_array($name, $this->metaKeys())) {
            return true;
        }
        return parent::hasAttribute($name);
    }

    /**
     * Using Model::loadMeta() instead of Model::load() to load an array to meta attributes.
     *
     * @param array $metaArray an associative array to load
     *
     * @return bool false if no value is loaded
     */
    public function loadMeta($metaArray)
    {
        $metaKeys = $this->metaKeys();
        $meta = array_intersect_key($metaArray, array_flip($metaKeys));

        if (!$meta or ! sizeof($meta)) {
            return false;
        }

        foreach ($meta as $key => $value) {
            if (!empty($value)) {
                $this->_meta[$key] = $value;
            }
        }

        return true;
    }

    /**
     * Metadata setter, use in __set.
     *
     * @param string $key   The meta key which needed to set
     * @param mixed  $value The meta value be set to the key
     */
    public function setMeta($key, $value)
    {
        if (in_array($key, $this->metaKeys())) {
            $this->_meta[$key] = $value;
        }
    }

    /**
     * Metadata getter, use in __get().
     *
     * @param string $key The meta key which needed to get
     *
     * @return mixed The meta value of the key
     */
    public function getMeta($key = false)
    {
        $enableMeta = $this->enableMeta();
        if (is_string($enableMeta)) {
            $type = $enableMeta;
            $metaModelName = "\\kyna\\$type\\models\\" . Inflector::camelize($type) . 'Meta';
        } elseif (is_array($enableMeta)) {
            $type = $enableMeta[0];
            $metaModelName = $enableMeta[1];
        }

        $objectKey = $this->getObjectKey($type);
        $metaKeys = $this->metaKeys();
        //$meta = $this->_meta;

        if ($key and isset($this->_meta[$key])) {
            return $this->_meta[$key];
        }

        $metaModel = $this->hasMany($metaModelName, [$objectKey => 'id'])->andOnCondition(['key' => $this->metaKeys()])->indexBy('key')->all();

        if ($key and isset($metaModel[$key])) {
            return $metaModel[$key]->value;
        }

        foreach ($metaKeys as $mKey) {
            if (!isset($this->_meta[$mKey])) {
                $this->_meta[$mKey] = isset($metaModel[$mKey]) ? $metaModel[$mKey]->value : null;
            }
        }

        if (!$key) {
            return $this->_meta;
        }

        return $this->_meta[$key];
    }

    public function getObjectKey($type)
    {
        return $type . '_id';
    }

    /**
     * Save all loaded meta data.
     *
     * @return bool Whether metadata updated successful or not
     */
    public function saveMeta()
    {
        if (empty($this->metaKeys()) or empty($this->_meta)) {
            return false;
        }
        $enableMeta = $this->enableMeta();
        if (is_string($enableMeta)) {
            $type = $enableMeta;
            $metaModelName = "\\kyna\\$type\\models\\" . Inflector::camelize($type) . 'Meta';
        } elseif (is_array($enableMeta)) {
            $type = $enableMeta[0];
            $metaModelName = $enableMeta[1];
        }
        $objectKey = $this->getObjectKey($type);
        $metaKeys = $this->metaKeys();

        $transaction = Yii::$app->db->beginTransaction();
        try {
            $metaModels = $metaModelName::find()->where([
                $objectKey => $this->id,
                'key' => $metaKeys,
            ])->indexBy('key')->all();

            $command = Yii::$app->db->createCommand();

            $insertArray = [];
            foreach ($this->_meta as $key => $value) {
                // If a meta key is new, add it to $insertArray to do batchInsert later
                if (!array_key_exists($key, $metaModels)) {
                    $insertArray[] = [
                        $objectKey => $this->id,
                        'key' => $key,
                        'value' => $value,
                    ];
                }
                //or else, update it
                else {
                    $command->update($metaModelName::tableName(), ['value' => $value], [
                        $objectKey => $this->id,
                        'key' => $key,
                    ])->execute();
                }
            }

            /*
             * Insert new meta to database
             */
            if (!empty($insertArray)) {
                $command->batchInsert($metaModelName::tableName(), [$objectKey, 'key', 'value'], $insertArray)->execute();
            }

            $transaction->commit();

            return true;
        } catch (\Exception $e) {
            var_dump($e->getMessage());
            die;
            $transaction->rollback();
            return false;
        }
    }

}
