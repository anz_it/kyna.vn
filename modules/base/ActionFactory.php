<?php

namespace kyna\base;

use yii\helpers\Inflector;

class ActionFactory
{
    public static function get($type, $modelClassName)
    {
        $action = \Yii::createObject($modelClassName);
        $action->type = $type;

        return $action;
    }
}
