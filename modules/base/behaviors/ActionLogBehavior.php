<?php

namespace kyna\base\behaviors;

use kyna\base\ActionFactory;
use yii\data\ActiveDataProvider;
use yii\helpers\Inflector;

class ActionLogBehavior extends \yii\base\Behavior
{
    /**
     * Example: `order` will contstruct CourseAction data model for course_actions table.
     *
     * @var bool|string action table name. False if not set
     */
    public $enableAction = false;
    public $modelClassName = false;

    /**
     * Action model adapter.
     *
     * @var bool|OrderAction
     */
    public $action = false;

    public $events = [];
    public function attach($owner)
    {
        if (false === ($owner instanceof \yii\db\ActiveRecord)) {
            throw new \yii\base\InvalidParamException('ActionLog can only be attached to '.
                'instances of ActiveRecord');
        }

        if (!$this->modelClassName) {
            $reflectionClass = new \ReflectionClass($owner);
            $classFileName = Inflector::camelize($this->enableAction);

            $this->modelClassName = $reflectionClass->getNamespaceName().'\\actions\\'.$classFileName.'Action';
        }

        parent::attach($owner);
    }

    public function events()
    {
        $events = [\yii\db\ActiveRecord::EVENT_INIT => 'initAction'];

        return $events + array_fill_keys(array_keys($this->events), 'log');
    }

    public function initAction()
    {
        if ($this->enableAction) {
            if (!is_string($this->enableAction)) {
                throw new \yii\base\InvalidValueException('$enableAction must be a string');
            }

            $this->action = ActionFactory::get($this->enableAction, $this->modelClassName);
            $this->action->allowedActions = array_keys($this->events);
        }
    }

    public function doAction($name, $meta)
    {
        return $this->action->log($name, $this->owner->id, $meta);
    }

    public function log($event)
    {
        if (array_key_exists($event->name, $this->events)) {
            $actionSettings = $this->events[$event->name];

            if (!empty($actionSettings['onetimeAction']) and $actionSettings['onetimeAction'] === true) {
                $this->action->onetimeAction = true;
            } else {
                $this->action->onetimeAction = false;
            }

            return $this->doAction($event->name, $event->actionMeta);
        }
    }

    public function availableActions()
    {
        if ($this->enableAction and is_string($this->enableAction)) {
            $objectKey = $this->enableAction.'_id';
            if ($this->action) {
                $actionClassName = $this->action->className();

                $where = [$objectKey => $this->owner->id];

                return $actionClassName::find()->where($where)->select('name')->distinct()->indexBy('name')->column();
            }
        }

        return [];
    }

    public function actionHistory($actionName = false)
    {
        if ($this->enableAction and is_string($this->enableAction)) {
            $objectKey = $this->enableAction.'_id';
            if ($this->action) {
                $actionClassName = $this->action->className();

                $where = [$objectKey => $this->owner->id];
                if ($actionName and is_string($actionName)) {
                    $where['name'] = $actionName;
                }

                $query = $actionClassName::find()->where($where)->orderBy(['action_time' => SORT_DESC]);
                $dataProvider = new ActiveDataProvider([
                    'query' => $query,
                ]);

                return $dataProvider;
            }
        }

        return false;
    }

    public function actionHistoryTelesale($actionName = false, $exceptName = [])
    {
        if ($this->enableAction and is_string($this->enableAction)) {
            $objectKey = $this->enableAction.'_id';
            if ($this->action) {
                $actionClassName = $this->action->className();

                $where = [$objectKey => $this->owner->id];
                $andWhere = [];
                if ($actionName and is_string($actionName)) {
                    $where['name'] = $actionName;
                } elseif (!empty($exceptName)) {
                    $andWhere = ['not in', 'name', $exceptName];
                }

                $query = $actionClassName::find()
                    ->where($where)
                    ->andWhere($andWhere)
                    ->orderBy(['action_time' => SORT_DESC]);
                $dataProvider = new ActiveDataProvider([
                    'query' => $query,
                ]);

                return $dataProvider;
            }
        }

        return false;
    }

    public function getLastAction()
    {
        if ($this->enableAction and is_string($this->enableAction)) {
            $objectKey = $this->enableAction.'_id';

            if ($this->action) {
                $actionClassName = $this->action->className();
                $query = $actionClassName::find()->where([$objectKey => $this->owner->id])->orderBy(['action_time' => SORT_DESC]);

                return $query->one();
            }
        }

        return false;
    }
}
