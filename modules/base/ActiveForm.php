<?php

namespace kyna\base;
use yii\bootstrap\ActiveForm as BaseActiveForm;

class ActiveForm extends BaseActiveForm {
    public $fieldClass = '\kyna\base\ActiveField';
}
