<?php

namespace kyna\base;

use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

use dektrium\user\models\Token;
use dektrium\user\models\User;
use kyna\settings\models\Setting;

class Mailer extends \dektrium\user\Mailer
{

    /**
     * @desc override dektrium Mailer sendWelcomeMessage function
     * @param User $user
     * @param Token $token
     * @param type $showPassword
     * @return type
     */
    public function sendWelcomeMessage(User $user, Token $token = null, $showPassword = false)
    {
        if (is_null($token)) {
            $token = new Token();
            $token->user_id = $user->id;
            $token->type = Token::TYPE_RECOVERY;

            if (!$token->save(false)) {
                return false;
            }
        }

        $mailer = Yii::$app->mailer;
        $mailer->htmlLayout = false;

        if ($this->sender === null) {
            $this->sender = isset(Yii::$app->params['adminEmail']) ?
                Yii::$app->params['adminEmail']
                : 'no-reply@example.com';
        }

        return $mailer->compose('@common/mail/user/auto_create', [
            'fullName' => !empty($user->profile->name) ? $user->profile->name : $user->email,
            'email' => $user->email,
            'link' => str_replace('dashboard.', '', Url::toRoute(['/user/recovery/reset', 'id' => $user->id, 'code' => $token->code], true)),
            'settings' => ArrayHelper::map(Setting::find()->all(), 'key', 'value')
        ])
            ->setTo($user->email)
            ->setFrom($this->sender)
            ->setSubject('Thông báo về việc đặt mật khẩu mới tại Kyna.vn')
            ->send();
    }
    
    protected function sendMessage($to, $subject, $view, $params = [])
    {
        $mailer = Yii::$app->mailer;
        $mailer->htmlLayout = '@common/mail/layouts/main';

        if ($this->sender === null) {
            $this->sender = isset(Yii::$app->params['adminEmail']) ?
                Yii::$app->params['adminEmail']
                : 'no-reply@example.com';
        }

        return $mailer->compose($view, $params)
            ->setTo($to)
            ->setFrom($this->sender)
            ->setSubject($subject)
            ->send();
    }

}

