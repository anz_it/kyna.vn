<?php

namespace kyna\base\models;

use kyna\settings\models\ShippingSetting;

/**
 * This is the model class for table "locations".
 *
 * @property int $id
 * @property string $name
 * @property int $parent_id
 * @property int $type
 * @property int $status
 * @property int $created_time
 * @property int $updated_time
 */
class Location extends \kyna\base\ActiveRecord
{
    
    const TYPE_INTERNAL = 1;
    const TYPE_EXTERNAL = 2;
    
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'locations';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['parent_id', 'position', 'type'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'parent_id' => 'Parent ID',
            'position' => 'Thứ tự',
            'type' => 'Loại',
        ];
    }
    
    public function getParent()
    {
        return $this->hasOne(self::className(), ['id' => 'parent_id']);
    }
    
    public static function getTypes()
    {
        return [
            self::TYPE_EXTERNAL => 'Ngoại thành',
            self::TYPE_INTERNAL => 'Nội thành'
        ];
    }
    
    public static function calculateFee($location_id, $amount)
    {
        $model = self::findOne($location_id);
        
        if (is_null($model)) {
            throw new \yii\web\NotFoundHttpException;
        }

        $cityId = $model->parent_id;

        $shippingConfigs = ShippingSetting::find()->andWhere([
            'city_id' => $cityId,
            'type' => $model->type,
        ])->orderBy('min_amount DESC')->all();

        if (!empty($shippingConfigs)) {
            foreach ($shippingConfigs as $shippingConfig) {
                if ($shippingConfig->min_amount > $amount) {
                    return $shippingConfig->fee;
                }
            }
            
            return 0;
        } else {
            $shippingConfig = ShippingSetting::find()->andWhere([
                'city_id' => NULL,
                'type' => $model->type,
            ])->andWhere(['>', 'min_amount', $amount])->orderBy('min_amount DESC')->one();

            if (!empty($shippingConfig)) {
                return $shippingConfig->fee;
            }
        }

        return 0;
    }
    
    public static function getByDistrictText($districtText)
    {
        $refinedDistrict = trim(str_replace(['--- Quận/Huyện ---', 'Huyện', 'Quận', 'Thành phố', 'Thành Phố', 'Thị trấn', 'Thị Xã', 'Thị xã', 'Tp.'], '', $districtText));
        
        return self::find()->where(['>', 'parent_id', 0])->andWhere(['like', 'name', $refinedDistrict])->one();
    }
}
