<?php

namespace kyna\base\models;

use Yii;
use kyna\base\models\VendorSettings;

/**
 * This is the model class for table "vendors".
 *
 * @property integer $id
 * @property string $name
 * @property integer $api_account_id
 * @property integer $vendor_type
 * @property double $total_amount
 * @property integer $status
 * @property integer $created_time
 * @property integer $updated_time
 */
class Vendor extends \kyna\base\ActiveRecord
{
    const VENDOR_TYPE_PAYMENT = 1;
    const VENDOR_TYPE_LOGISTIC = 2;
    const VENDOR_TYPE_VIDEO = 3;
    const VENDOR_TYPE_CDN = 4;

    const STATUS_DISABLED = 0;
    const STATUS_ENABLED = 1;

    //public $settings;

    /**
     * @inheritdoc
     */

    public static function tableName()
    {
        return 'vendors';
    }

    public static function getStatuses() {
        return [
            self::STATUS_DISABLED => 'Ngừng hoạt động',
            self::STATUS_ENABLED => 'Đang hoạt động'
        ];
    }

    public static function getVendorTypes() {
        return [
            self::VENDOR_TYPE_PAYMENT => 'Cổng thanh toán',
            self::VENDOR_TYPE_LOGISTIC => 'Đơn vị giao vận',
            self::VENDOR_TYPE_VIDEO => 'Dịch vụ video streaming',
            self::VENDOR_TYPE_CDN => 'Content Delivery Network'
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['vendor_type', 'status', 'created_time', 'updated_time'], 'integer'],
            [['name', 'alias'], 'string', 'max' => 45],
            ['settings', 'safe'],
            //[['vendor_api'], 'default'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Tên',
            'alias' => 'Tên mã',
            'settings' => 'Vendor Settings',
            'vendor_type' => 'Phân loại',
            'status' => 'Tình trạng hoạt động',
            'created_time' => 'Created Time',
            'updated_time' => 'Updated Time',
        ];
    }

    public function getStatusLabel() {
        $statuses = $this->getStatuses();
        if (array_key_exists($this->status, $statuses)) {
            return $statuses[$this->status];
        }
        return 'N/A';
    }

    public function getVendorTypeLabel() {
        $types = $this->getVendorTypes();
        if (array_key_exists($this->vendor_type, $types)) {
            return $types[$this->vendor_type];
        }

        return 'N/A';
    }

    public function getSetting() {
        return (new VendorSettings($this))->config;
    }

    public function getAvailableMethods() {
        if (self::VENDOR_TYPE_PAYMENT == $this->vendor_type) {
            $settings = [];
            foreach($this->setting as $key => $value) {
                if (!empty($value['Enable'])) {
                    $settings[$key] = $value;
                }
            }
            return $settings;
        }
        if (self::VENDOR_TYPE_LOGISTIC == $this->vendor_type) {
            return [$this->alias => $this->attributes];
        }
        return false;
    }
}
