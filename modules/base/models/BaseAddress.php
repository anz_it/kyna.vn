<?php

namespace kyna\base\models;

use common\helpers\PhoneNumberHelper;


/**
 * This is the model class for table "user_addresses".
 *
 * @property int $id
 * @property int $user_id
 * @property string $contact_name
 * @property string $email
 * @property string $phone_number
 * @property string $street_address
 * @property int $location_id
 * @property int $status
 * @property int $created_time
 * @property int $updated_time
 */
class BaseAddress extends \kyna\base\ActiveRecord
{
    public $city_id;

    public function getLocations()
    {
        $locations = [];
        $getterId = $this->location_id;
        //var_dump($getterId);
        do {
            $loc = Location::findOne($getterId);

            if (empty($loc)) {
                break;
            }
            $locations[] = $loc;
            $getterId = $loc->parent_id;
        } while ($getterId);

        return $locations;
    }

    public function getLocation()
    {
        return $this->hasOne(Location::className(), ['id' => 'location_id']);
    }

    public function getPhoneNumber()
    {
        return $this->phone_number;
    }

    protected function visibleFields()
    {
        return ['contact_name', 'phoneNumber', 'street_address'];
    }

    public function toString($hasLabel = true)
    {
        $text = '';

        $labels = $this->attributeLabels();
        foreach ($this->visibleFields() as $key) {
            if (!empty($this->$key)) {
                if ($hasLabel) {
                    $text .= '<span class="text-muted">'.$labels[$key].':</span> ';
                }
                $text .= $this->$key.'<br>';
            }
        }

        $locations = $this->locations;
        if (!empty($locations)) {
            foreach ($locations as $location) {
                $text .= $location->name.', ';
            }
        }

        return trim($text, ', ');
    }

    public function getAddressText()
    {
        //$location = $this->location;
        $text = $this->street_address;
        if (!empty($this->location)) {
            $text .= ', '.$this->location->name;
            if (!empty($this->location->parent)) {
                $text .= ', '.$this->location->parent->name;
            }
        }

        return $text;
    }
}
