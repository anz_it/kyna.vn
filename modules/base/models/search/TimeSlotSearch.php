<?php

namespace kyna\base\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use kyna\base\models\TimeSlot;

/**
 * TimeSlotSearch represents the model behind the search form about `app\models\TimeSlot`.
 */
class TimeSlotSearch extends TimeSlot
{
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'related_id', 'status', 'created_time', 'updated_time'], 'integer'],
            [['start_time', 'end_time', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday', 'type', 'user_type'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TimeSlot::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        
        $query->andFilterWhere([
            'id' => $this->id,
            'start_time' => $this->start_time,
            'end_time' => $this->end_time,
            'related_id' => $this->related_id,
            'status' => $this->status,
            'created_time' => $this->created_time,
            'updated_time' => $this->updated_time,
        ]);

        $query->andFilterWhere(['like', 'monday', $this->monday])
            ->andFilterWhere(['like', 'tuesday', $this->tuesday])
            ->andFilterWhere(['like', 'wednesday', $this->wednesday])
            ->andFilterWhere(['like', 'thursday', $this->thursday])
            ->andFilterWhere(['like', 'friday', $this->friday])
            ->andFilterWhere(['like', 'saturday', $this->saturday])
            ->andFilterWhere(['like', 'sunday', $this->sunday])
            ->andFilterWhere(['like', 'type', $this->type])
            ->andFilterWhere(['like', 'user_type', $this->user_type]);

        return $dataProvider;
    }
}
