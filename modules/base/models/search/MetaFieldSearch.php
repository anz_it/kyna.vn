<?php

namespace kyna\base\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use kyna\base\models\MetaField;

/**
 * MetaFieldSearch represents the model behind the search form about `app\models\MetaField`.
 */
class MetaFieldSearch extends MetaField
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'type', 'status', 'created_time', 'updated_time'], 'integer'],
            [['name', 'model', 'extra_validate', 'data_set', 'note'], 'safe'],
            [['is_required', 'is_index_es', 'is_unique', 'is_readonly'], 'boolean'],
            [['name', 'id'], 'trim'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MetaField::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'type' => $this->type,
            'model' => $this->model,
            'is_required' => $this->is_required,
            'is_index_es' => $this->is_index_es,
            'is_unique' => $this->is_unique,
            'is_readonly' => $this->is_readonly,
            'status' => $this->status,
            'created_time' => $this->created_time,
            'updated_time' => $this->updated_time,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'extra_validate', $this->extra_validate])
            ->andFilterWhere(['like', 'data_set', $this->data_set])
            ->andFilterWhere(['like', 'note', $this->note]);

        return $dataProvider;
    }
}
