<?php

namespace kyna\base\models;

use Yii;
use kyna\base\models\Vendor;
use yii\base\Model;
use yii\base\InvalidConfigException;
use donatj\Ini\Builder;

class VendorSettings extends Model {
    private $_vendor;
    private $_configFile;

    public $config;
    public $configRoot;

    public function safeAttributes() {
        return ['config'];
    }

    public function __construct($vendor) {
        parent::__construct();

        $this->_vendor = $vendor;

        if (!$this->_vendor) {
            throw new InvalidConfigException('Can not find vendor with Id equals to ' . $vendorId);
        }

        $this->configRoot = Yii::getAlias('@kyna/order/lib');
        $this->_configFile = $this->configRoot . '/' . $this->_vendor->alias . '/' . $this->_vendor->alias . '.ini';

        if (!file_exists($this->_configFile)) {
            file_put_contents($this->_configFile, '');
            //throw new InvalidConfigException($this->_configFile . ' is not existed');
        }

        $this->config = parse_ini_file($this->_configFile, true);
    }

    public function addItem($itemName, $path) {

    }

    public function save() {
        
    }
}
