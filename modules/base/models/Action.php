<?php

namespace kyna\base\models;

use Yii;
use yii\helpers\Inflector;
use kyna\user\models\User;

/**
 * This is the model class for table "order_actions".
 *
 * @property int $id
 * @property int $ob
 * @property int $user_id
 * @property string $name
 * @property string $user_type
 * @property int $action_time
 */
abstract class Action extends \kyna\base\ActiveRecord
{
    public $type;
    public $allowedActions;
    public $onetimeAction = false;
    private $_meta = false;
    //public $meta; // how

    private function _actionTable()
    {
        return $this->type.'_actions';
    }

    private function _objectKey()
    {
        return $this->type.'_id';
    }

    private function _actionClassName()
    {
        return Inflector::camelize($this->type).'Action';
    }

    public function __construct()
    {
        parent::__construct();
    }

    public function rules()
    {
        $objectKey = $this->_objectKey();

        return [
            [['user_id', $objectKey, 'action_time'], 'integer'],
            [['name'], 'string', 'max' => 50],
        ];
    }

    public function init()
    {
        parent::init();
    }

    //ActionFactory::action('order')->log('create', $order_id);
    public function log($actionName, $objectId, $meta = false)
    {
        if (!in_array($actionName, $this->allowedActions)) {
            throw new \yii\base\NotSupportedException('Action `'.$actionName.'` is not supported.');
        }

        $objectKey = $this->_objectKey();
        //$metaKey = $this->_metaKey();
        $userId = isset(Yii::$app->user) && !is_null(Yii::$app->user->identity) ? Yii::$app->user->identity->id : 0;

        $actionData = [
            'user_id' => $userId,
            $objectKey => $objectId,
            'name' => $actionName,
            'action_time' => time(),
        ];

        if ($this->onetimeAction !== false) {
            $action = self::find()->where([
                'user_id' => $userId,
                $objectKey => $objectId,
                'name' => $actionName,
            ])->one();

            if ($action) {
                $this->id = $action->id;
                $this->setIsNewRecord(false);
            }
        }

        $this->attributes = $actionData;
        if ($meta) {
            $this->loadMeta($meta);
        }

        return $this->save();
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
