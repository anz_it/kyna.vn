<?php

namespace kyna\base\models;

use Yii;
use app\components\MetaFieldType;

/**
 * This is the model class for table "meta_fields".
 *
 * @property integer $id
 * @property string $name
 * @property integer $type
 * @property string $model
 * @property string $extra_validate
 * @property string $data_set
 * @property boolean $is_required
 * @property boolean $is_index_es
 * @property boolean $is_readonly
 * @property string $note
 * @property integer $status
 * @property integer $created_time
 * @property integer $updated_time
 */
class _MetaField extends \kyna\base\ActiveRecord
{

    const MODEL_COURSE = 'course';
    const MODEL_CATEGORY = 'category';
    const MODEL_COURSE_QUIZ = 'course_quiz';
    const MODEL_COURSE_COMBO = 'course_combo';
    const MODEL_USER = 'user';
    const MODEL_TEACHER = 'teacher';
    const MODEL_SETTING = 'setting';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'meta_fields';
    }

    protected static function softDelete()
    {
        return false;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['key', 'name', 'model', 'type'], 'required'],
            [['type', 'status', 'created_time', 'updated_time'], 'integer'],
            [['extra_validate', 'data_set', 'note'], 'string'],
            [['is_required', 'is_index_es', 'is_readonly'], 'boolean'],
            [['name'], 'string', 'max' => 255],
            [['model'], 'string', 'max' => 20],
            [['key'], 'string', 'max' => 50],
            ['data_set', 'required', 'when' => function($model) {
                return array_key_exists($model->type, MetaFieldType::getListTypeHasDataSet());
            }, 'whenClient' => "function (attribute, value) {
                var type = $('#becustomfield-type').val();
                return  (type == '" . MetaFieldType::TYPE_CHECKBOX_LIST . "' || type == '" . MetaFieldType::TYPE_DROPDOWN_LIST . "' || type == '" . MetaFieldType::TYPE_RADIO_BUTTON_LIST . "');
            }"]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'key' => 'Key',
            'name' => 'Tên',
            'type' => 'Loại',
            'model' => 'Model',
            'extra_validate' => 'Extra Validate',
            'data_set' => 'Data Set',
            'is_required' => 'Bắt buộc',
            'is_index_es' => 'Thêm vào Elastic',
            'is_readonly' => 'Chỉ đọc',
            'note' => 'Ghi chú',
            'status' => 'Trạng thái',
            'created_time' => 'Ngày tạo',
            'updated_time' => 'Cập nhật cuối',
        ];
    }

    public function getTypeText()
    {
        return MetaFieldType::getTypeText($this->type);
    }

    public static function getModelTypes()
    {
        return [
            self::MODEL_COURSE,
            self::MODEL_CATEGORY,
            self::MODEL_COURSE_QUIZ,
            self::MODEL_COURSE_COMBO,
            self::MODEL_TEACHER,
            self::MODEL_USER,
            self::MODEL_SETTING
        ];
    }
}
