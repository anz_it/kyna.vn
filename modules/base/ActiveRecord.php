<?php

namespace kyna\base;

use Yii;
use kyna\base\traits\MetaTrait;
use kyna\base\traits\StatusTrait;
use yii2tech\ar\softdelete\SoftDeleteBehavior;
use kyna\base\traits\SoftDeleteTrait;

/*
 * This is overrided class for Yii Core ActiveRecord.
 */
class ActiveRecord extends \yii\db\ActiveRecord
{
    use MetaTrait, StatusTrait, SoftDeleteTrait;

    /**
     * constant status for all models.
     */
    const STATUS_DEACTIVE = 0;
    const STATUS_ACTIVE = 1;

    /**
     * const values for boolean column (BIT columns in tables)
     */
    const BOOL_YES = 1;
    const BOOL_NO = 0;
    const BOOL_YES_TEXT = 'Yes';
    const BOOL_NO_TEXT = 'No';

    public static $readOnMaster;

    protected static function softDelete()
    {
        return false;
    }

    public static function listBoolean()
    {
        $list = [
            self::BOOL_YES => self::BOOL_YES_TEXT,
            self::BOOL_NO => self::BOOL_NO_TEXT,
        ];

        return $list;
    }

    public function behaviors()
    {
        $behaviors = parent::behaviors();

        if (static::softDelete()) {
            // Soft delete
            $behaviors['softDeleteBehavior'] = [
                'class' => SoftDeleteBehavior::className(),
                'softDeleteAttributeValues' => [
                    'is_deleted' => true,
                ],
                'restoreAttributeValues' => [
                    'is_deleted' => false,
                ],
                'replaceRegularDelete' => true,
            ];
        }

        return $behaviors;
    }

    public function __construct()
    {
        parent::__construct();
    }

    public static function getDb()
    {
        $db = parent::getDb();

        if (static::$readOnMaster === true) {
            return $db->useMaster(function ($db) {
                return $db;
            });
        }

        return $db;
    }

    public static function getBooleanOptions() // TODO: move to trait
    {
        return [
            self::BOOL_YES => self::BOOL_YES_TEXT,
            self::BOOL_NO => self::BOOL_NO_TEXT,
        ];
    }

    public function isRequired($attribute)
    {
        foreach ($this->getActiveValidators($attribute) as $validator) {
            if ($validator instanceof \yii\validators\RequiredValidator) {
                return true;
            }
        }

        return false;
    }

    public function beforeSave($insert)
    {
        $ret = parent::beforeSave($insert);

        $time = time();
        if ($insert && $this->hasAttribute('created_time') && empty($this->created_time)) {
            $this->created_time = $time;
        }

        if ($insert && $this->hasAttribute('created_user_id') && empty($this->created_user_id) && !empty(Yii::$app->user)) {
            $this->created_user_id = Yii::$app->user->id;
        }

        if ($this->hasAttribute('updated_user_id') && !empty(Yii::$app->user)) {
            $this->updated_user_id = Yii::$app->user->id;
        }

        if ($this->hasAttribute('updated_time')) {
            $this->updated_time = $time;
        }

        return $ret;
    }
}
