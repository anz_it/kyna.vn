<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace kyna\base\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class LocationFormAsset extends AssetBundle
{
    public $sourcePath = __DIR__;
    public $css = [
        'css/location-form.css',
    ];
    public $js = [
        'js/location.js',
    ];
    public $depends = [
        'app\assets\KynaAsset',
        'common\widgets\treeview\TreeViewAsset',
    ];
}
