;(function ($) {
    $('[data-control="daterangepicker"]').each(function (i) {
        var data = $(this).data();
        data.locale = {
            format: "DD/MM/YYYY"
        };
        console.log(data);
        $(this).daterangepicker(data);
    });
    //$('[data-control="daterangepicker"]').daterangepicker();
})(jQuery);
