$('.dd').nestable({
    noDragClass: "dd-nodrag",
    expandBtnHTML   : '<button data-action="expand" type="button"><i class="ion-android-add"></i></button>',
    collapseBtnHTML : '<button data-action="collapse" type="button"><i class="ion-android-remove"></i></button>'
});

$('.dd').on('change', function() {
    /* on change event */
    var data = $(this).nestable('serialize');
    console.log(data);
    // TODO: ajax post to save the tree
});
