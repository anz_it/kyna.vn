<?php

namespace kyna\base;
use yii\bootstrap\ActiveField as BaseAciveField;

/**
 * foreach (Models::metaFields as $metaKey => $metaFieldId) {
 *     $form->field($models, $metaKey)->renderMeta();
 * }
 * foreach (Model::metaGroups as $metaGroup) {
 *     $form->renderGroup($models, $metaGroup);
 * }
 */

class ActiveField extends BaseActiveField {

    public function getIsMeta() {
        return $this->metaFieldId !== false;
    }

    public function getMetaFieldId() {
        $this->model->hasMeta($this->attribute);
    }

    public function renderMeta() {

    }
}
