<?php

namespace kyna\base\events;

class ActionEvent extends \yii\base\Event {
    public $actionMeta = [];

    public function addMeta($meta) {
        $this->actionMeta = (array)$meta + $this->actionMeta;
    }
}
