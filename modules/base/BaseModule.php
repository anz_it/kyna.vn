<?php

namespace kyna\base;

use yii\filters\AccessControl;

/*
 * This is override Yii Core Module class for Backend usage.
 */
class BaseModule extends \yii\base\Module
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => false,
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public $layout = '@app/views/layouts/lte';

}
