<?php

namespace kyna\importmanager;
use yii\base\Component;
use PhpOffice\PhpSpreadsheet\Reader\Excel5;
use PhpOffice\PhpSpreadsheet\IOFactory;

abstract class BaseImporter extends Component implements ImporterInterface {
    public $file;

    protected $phpExcel;

    public function init() {
        parent::init();

        $this->load();
    }
    public function load() {
        $type = IOFactory::identify($this->file);
        $reader = IOFactory::createReader($type);
        $this->phpExcel = $reader->load($this->file);
    }
    public abstract function import();
}
