<?php

namespace kyna\importmanager;

interface ImporterInterface {
    public function import();
    public function openFile();
}
