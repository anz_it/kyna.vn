<?php

namespace kyna\importmanager;

class ImportManagerModule extends \yii\base\Module
{
    public $controllerNamespace = 'kyna\importmanager\controllers';
    public $uploadRoot = '@upload/import';
    public $cache = [
        'class' => '\yii\caching\FileCache'
    ];
    public $cacheKey = 'import-queue';
    public $importerNamespace = 'kyna\\importmanager\\importers';
    public $maxRetry = 3;

    protected $_cache;

    public function getCache() {
        return $this->_cache;
    }

    public function init()
    {
        parent::init();

        $this->_cache = Yii::createObject($this->cache);
    }
}
