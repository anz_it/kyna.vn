<?php

namespace kyna\importmanager\controllers;
use yii\console\Controller;

class ImportController extends Controller {
    const STATUS_FAILED     = -1;
    const STATUS_QUEUING    = 0;
    const STATUS_PROCESSING = 1;
    const STATUS_FINISH     = 99;

    const EVENT_START_IMPORT    = 'start';
    const EVENT_FINISH_IMPORT   = 'finish';
    const EVENT_FAILED_IMPORT   = 'failed';

    public $uploadRoot;
    public $finishedDir;
    public $failedDir;
    public $cache;
    public $cacheKey;
    public $importerNamespace;
    public $maxRetry;

    public function init() {
        parent::init();

        $this->uploadRoot           = Yii::getAlias($this->module->uploadRoot);
        $this->cache                = $this->module->getCache();
        $this->cacheKey             = $this->module->cacheKey;
        $this->importerNamespace    = $this->module->importerNamespace;
        $this->maxRetry             = $this->module->maxRetry;

        if ($this->maxRetry >= self::STATUS_FINISH) {
            $this->maxRetry = self::STATUS_FINISH - 1;
        }

        $this->finishedDir = $this->uploadRoot.'/finished';
        if (!file_exists($this->finishedDir) or !is_dir($this->finishedDir)) {
            mkdir($this->finishedDir, 777);
        }

        $this->failedDir = $this->uploadRoot.'/failed';
        if (!file_exists($this->failedDir) or !is_dir($this->failedDir)) {
            mkdir($this->failedDir, 777);
        }

        $this->_scanNewImports();
    }

    public function actionIndex() {
        $file = $this->_pickItem();

        if (!$this->_startImport($file)) {
            echo $file.' is not existed';
            return false;
        }

        echo PHP_EOL.'['.date('c').'] Importing '.$file.'...';
        $this->trigger(self::EVENT_START_IMPORT);
        $startTime = microtime(true);

        $importer = $this->_getImporter($file);
        if ($importer->import()) {
            $this->_finishImport($file);
            $endTime = microtime(true);
            echo 'FINISH ('.($endTime - $startTime).' secs)';
            $this->trigger(self::EVENT_FINISH_IMPORT);
        }
        elseif ($this->_getStatus($file) >= $this->maxRetry) {
            $this->_failedImport($file);
            echo 'FAILED';
            $this->trigger(self::EVENT_FAILED_IMPORT);
        }
    }

    public function getQueue() {
        return $queue = $this->cache->get($this->cacheKey);
    }

    public function setQueue($queue) {
        $this->cache->set($this->cacheKey, $queue);
    }

    private function _getImporter($file) {
        $fileParts = explode('_', $file);
        $importerName = $fileParts[1];
        $importerClass = $this->importerNamespace.'\\'.$importerName;

        $path = $this->uploadRoot.'/'.$file;
        $type = mime_content_type($path);

        return Yii::createObject([
            'class' => $importerClass,
            'file' => $path,
            'type' => $type,
        ]);
    }

    private function _pickItem() {
        $queue = $this->queue;
        return array_search(self::STATUS_QUEUING, $queue);
    }

    private function _addToQueue(&$queue, $file) {
        $path = $this->uploadRoot.'/'.$file;

        if (array_key_exists($file, $queue)) {
            continue;
        }

        if (file_exists($path) and mime_content_type($path)) {
            $queue[$file] = self::STATUS_QUEUING;
        }

        ksort($queue);
    }

    private function _getStatus($file) {
        $queue = $this->queue;
        if (!array_key_exists($file, $queue)) {
            return false;
        }

        return $queue[$file];
    }

    private function _updateQueueItem($file, $status) {
        $queue = $this->queue;
        if (!array_key_exists($file, $queue)) {
            return false;
        }

        if ($status === false) {
            unset($queue[$file]);
        }

        $queue[$file] = $status;

        $this->queue = $queue;
        return true;
    }

    private function _startImport($file) {
        $status = $this->_getStatus($file);
        if ($status >= self::STATUS_QUEUING and $status < $this->maxRetry) {
            $this->_updateQueueItem($file, $status + 1);
            return true;
        }

        return false;
    }

    // dequeue
    private function _failedImport($file) {
        $this->_updateQueueItem($file, self::STATUS_FAILED);

        $oldPath = $this->uploadRoot.'/'.$file;
        $newPath = $this->failedDir.'/'.$file;
        rename($oldPath, $newPath);

        $this->_updateQueueItem($file, false);
    }

    // dequeue
    private function _finishImport($file) {
        $this->_updateQueueItem($file, self::STATUS_FINISH);

        $oldPath = $this->uploadRoot.'/'.$file;
        $newPath = $this->finishedDir.'/'.$file;
        rename($oldPath, $newPath);

        $this->_updateQueueItem($file, false);
    }

    private function _scanNewImports() {
        $root = $this->uploadRoot;

        $files = array_diff(scandir($root), ['.', '..']);

        $queue = $this->queue;
        foreach ($files as $file) {
            $this->_addToQueue($queue, $file);
        }
        $this->queue = $queue;
    }
}
