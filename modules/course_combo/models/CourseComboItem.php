<?php

namespace kyna\course_combo\models;

use yii\data\ActiveDataProvider;
use kotchuprik\sortable\behaviors\Sortable;

use kyna\course\models\Course;

/**
 * This is the model class for table "course_combo_items".
 *
 * @property integer $id
 * @property integer $course_combo_id
 * @property integer $course_id
 * @property string $price
 * @property integer $position
 * @property integer $reg_total
 * @property integer $status
 * @property integer $created_time
 * @property integer $updated_time
 */
class CourseComboItem extends \kyna\base\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'course_combo_items';
    }
    
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        
        $behaviors['sortable'] = [
            'class' => Sortable::className(),
            'query' => self::find(),
            'orderAttribute' => 'position'
        ];
        
        return $behaviors;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['course_combo_id', 'course_id', 'price'], 'required', 'on' => ['create', 'update']],
            [['course_combo_id', 'course_id', 'position', 'reg_total', 'status', 'created_time', 'updated_time'], 'integer'],
            [['price'], 'integer', 'integerOnly' => true],
            [['course_combo_id', 'course_id', 'price', 'reg_total', 'status', 'created_time', 'updated_time'], 'safe', 'on' => 'search'],
            ['course_id', 'unique', 'targetAttribute' => ['course_id', 'course_combo_id'], 'message' => 'Khóa học này đã tồn tại trong Combo'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'course_combo_id' => 'Course Combo ID',
            'course_id' => 'Khóa học',
            'price' => 'Giá ưu đãi',
            'position' => 'Order',
            'reg_total' => 'Reg Total',
            'status' => 'Trạng thái',
            'created_time' => 'Created Time',
            'updated_time' => 'Updated Time',
        ];
    }
    
    public function getCombo()
    {
        return $this->hasOne(Course::className(), ['id' => 'course_combo_id']);
    }
    
    public function getCourse()
    {
        return $this->hasOne(Course::className(), ['id' => 'course_id']);
    }
    
    public function afterSave($insert, $changedAttributes)
    {
        $ret = parent::afterSave($insert, $changedAttributes);

        if ($insert || isset($changedAttributes['price'])) {
            CourseCombo::calculatePrice($this->course_combo_id, $insert ? 'add' : 'update', $this->price);
        }
        if ($insert || isset($changedAttributes['course_id'])) {
            CourseCombo::detectOnlyTeacher($this->course_combo_id, $insert ? 'add' : 'update', $this->course->teacher_id);
            CourseCombo::updateTags($this->course_combo_id, $insert ? 'add' : 'update');
        }
        
        return $ret;
    }
    
    public function afterDelete()
    {
        $ret = parent::afterDelete();
        
        CourseCombo::calculatePrice($this->course_combo_id, 'delete', $this->price);
        CourseCombo::detectOnlyTeacher($this->course_combo_id, 'delete', $this->course->teacher_id);
        CourseCombo::updateTags($this->course_combo_id, 'delete');
        
        return $ret;
    }
    
        /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = self::find();
        $this->setScenario('search');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);
        
        if (!$this->validate()) {
            var_dump($this->scenario);die;
            var_dump($this->errors);die;
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'course_combo_id' => $this->course_combo_id,
            'course_id' => $this->course_id,
            'price' => $this->price,
            'status' => $this->status,
            'updated_time' => $this->updated_time,
        ]);
        
        $query->orderBy('position ASC');

        return $dataProvider;
    }
    
    public function getDiscountAmount()
    {
        return $this->course->price - $this->price;
    }
}
