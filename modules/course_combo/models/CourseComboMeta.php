<?php

namespace kyna\course_combo\models;

use kyna\base\models\MetaField;
use kyna\course\models\CourseMeta;

class CourseComboMeta extends CourseMeta
{
    
    /**
     * @desc get relation MetaField
     * @return BEMetaField model
     */
    public function getMetaField()
    {
        return MetaField::find()->where(['model' => MetaField::MODEL_COURSE_COMBO, 'key' => $this->key])->one();
    }
}