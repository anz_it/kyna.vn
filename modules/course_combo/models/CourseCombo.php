<?php

namespace kyna\course_combo\models;

use Yii;

use kyna\order\models\Order;
use kyna\order\models\OrderDetails;
use kyna\base\models\MetaField;
use kyna\course\models\Course;
use kyna\tag\models\CourseTag;

use kotchuprik\sortable\behaviors\Sortable;
use common\helpers\StringHelper;

use common\widgets\upload\UploadRequiredValidator;

/**
 * This is the model class for table "course_combos".
 *
 * @property integer $id
 * @property integer $type
 * @property string $image_url
 * @property string $object
 * @property string $benefit
 * @property string $seo_title
 * @property string $seo_description
 * @property string $seo_keyword
 * @property integer $position
 */
class CourseCombo extends Course
{
    public $description;
    public $is_feature;

    public function enableMeta()
    {
        return MetaField::MODEL_COURSE_COMBO;
    }

    public function getObjectKey($type)
    {
        return 'course_id';
    }

    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['sortable'] = [
            'class' => Sortable::className(),
            'query' => self::find(),
            'orderAttribute' => 'position'
        ];

        return $behaviors;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'short_name', 'status'], 'required'],
            ['type', 'default', 'value' => 2],
            [['image_url'], UploadRequiredValidator::className(), 'on' => self::SCENARIO_UPDATE],
            [['image_url', 'video_cover_image_url'], 'image', 'maxSize' => '4194304', 'mimeTypes' => 'image/png,image/jpeg'], // 4MiB
            [['object', 'benefit', 'seo_description', 'seo_title', 'seo_keyword'], 'string'],
            [['teacher_id', 'position', 'type', 'status', 'created_time', 'updated_time'], 'integer'],
            [['is_deleted'], 'boolean'],
            [['is_hot', 'is_new','is_4kid'], 'integer', 'integerOnly' => true],
            [['name'], 'string', 'max' => 1024],
            [['short_name'], 'string', 'max' => 45],
            [['slug', 'video_url'], 'string'],
            [['object', 'benefit'], 'filter', 'filter' => [StringHelper::class, 'htmlContentFilter']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'price' => 'Tổng học phí',
            'object' => 'Đối tượng học',
            'benefit' => 'Lợi ích',
            'seo_title' => 'SEO Title',
            'seo_keyword' => 'SEO Keyword',
            'seo_description' => 'SEO Description',
            'teacher_id' => 'Combo do 1 giảng viên dạy?'
        ]);
    }

    public function updateCourseTags()
    {
        // TODO: overwrite parent
    }

    public static function calculatePrice($id, $action, $price)
    {
        $combo = self::findOne($id);

        if (empty($combo)) {
            return;
        }

        switch ($action) {
            case 'add':
            case 'delete':
            case 'update':
                $result = Yii::$app->db->createCommand('SELECT sum(price) AS total FROM ' . CourseComboItem::tableName() . ' WHERE course_combo_id = :id')
                    ->bindParam(':id', $id)
                    ->queryOne();

                $combo->price = $result['total'];

                break;

            default:
                return false;
        }

        return $combo->save(false);
    }

    public static function updateTags($id, $action)
    {
        $combo = self::findOne($id);

        if (empty($combo)) {
            return;
        }

        switch ($action) {
            case 'add':
            case 'delete':
            case 'update':
                CourseTag::updateComboCourseTags($id, false);
                break;
            default:
                return false;
        }

        return $combo->save(false);
    }

    public static function detectOnlyTeacher($id, $action, $teacher_id)
    {
        $combo = self::findOne($id);

        if (empty($combo)) {
            return;
        }

        switch ($action) {
            case 'add':
            case 'delete':
            case 'update':
                $items = $combo->comboItems;

                $teacher_id = 0;
                foreach ($items as $item) {
                    if ($teacher_id != $item->course->teacher_id) {
                        if (empty($teacher_id)) {
                            $teacher_id = $item->course->teacher_id;
                        } else {
                            $teacher_id = 0;
                            break;
                        }
                    }
                }

                $combo->teacher_id = $teacher_id;

                break;

            default:
                return false;
        }

        return $combo->save(false);
    }

    public static function find()
    {
        $query = parent::find();

        return $query->andWhere(['type' => self::TYPE_COMBO]);
    }

    public function getItems()
    {
        return parent::getComboItems();
    }

    public function getOrderDetails()
    {
        return OrderDetails::find()->alias('t')->where(['course_combo_id' => $this->id])->groupBy(['course_combo_id', 'order_id']);
    }
    public function getTotal_users_count()
    {
        $query = new \yii\db\Query();
        $count = $query->select('user_id')->from('orders o')
            ->leftJoin('order_details t', 'o.id = t.order_id')
            ->andWhere(['!=', 'o.user_id', 0])
            ->andWhere(['!=', 'o.status', Order::ORDER_STATUS_CANCELLED])
            ->andWhere(['t.course_combo_id' => $this->id])
            ->groupBy([ 'o.user_id'])->count();
        return $count;
    }
    public function getCountComboPaid()
    {
        return $this->getOrderDetails()->leftJoin(Order::tableName(), Order::tableName() . '.id = t.order_id')->andWhere(['is_paid' => Order::BOOL_YES]);
    }

    public function getTeacherText()
    {
        $comboItems = $this->items;
        $listTeacher = '';
        if ($comboItems) {
            $teacherIDs = [];
            foreach ($comboItems as $item) {
                if ($item->course && $item->course->teacher) {
                    if (!in_array($item->course->teacher_id, $teacherIDs)) {
                        $listTeacher .= $item->course->teacher->profile->name . ", ";
                        $teacherIDs[] = $item->course->teacher_id;
                    }

                }
            }
        }
        return substr($listTeacher, 0, strlen($listTeacher) - 2);
    }

    public function getTeacherCount()
    {
        $teachers = [];
        foreach ($this->items as $item) {
            if (!key_exists($item->course->teacher_id, $teachers)) {
                $teachers[$item->course->teacher_id] = $item->course->teacher_id;
            }
        }

        return count($teachers);
    }

    public function getDiscountPercent()
    {
        $salePrice = $this->sellPrice;
        if (!empty($this->oldPrice)) {
            $originalPrice = $this->oldPrice;
        } else {
            $originalPrice = $salePrice;
        }
        $discountMoney = $originalPrice - $salePrice;
        if ($originalPrice > 0) {
            $discountRate = $discountMoney/$originalPrice;
        } else {
            $discountRate = 0;
        }

        return round($discountRate, 2) * 100;
    }

}
