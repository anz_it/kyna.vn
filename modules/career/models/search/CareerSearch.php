<?php

namespace kyna\career\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use kyna\career\models\Career;

/**
 * CareerSearch represents the model behind the search form about `kyna\career\models\Career`.
 */
class CareerSearch extends Career
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'category_id', 'quantity', 'end_date', 'status', 'created_time', 'updated_time'], 'integer'],
            [['title', 'slug', 'content'], 'safe'],
            [['title', 'slug', 'quantity'], 'trim'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Career::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // search expired
        if (!is_null($this->end_date) && $this->end_date != "") {
            if (intval($this->end_date)) {
                $query->andWhere(['<', 'end_date', time()]);
            } else {
                $query->andWhere(['>=', 'end_date', time()]);
            }
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'category_id' => $this->category_id,
            'quantity' => $this->quantity,
            //'end_date' => $this->end_date,
            'status' => $this->status,
            'created_time' => $this->created_time,
            'updated_time' => $this->updated_time,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'slug', $this->slug])
            ->andFilterWhere(['like', 'content', $this->content]);

        return $dataProvider;
    }
}
