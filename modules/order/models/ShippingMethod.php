<?php

namespace kyna\order\models;

use Yii;
use yii\base\Model;
use kyna\base\models\Vendor;
use kyna\base\models\VendorSettings;
use yii\helpers\Inflector;

class ShippingMethod extends Model
{
    public static $readOnMaster = true;
    //public $order_id;

    public $serviceId;
    public $errors;

    //private $_serviceId = 53323;
    private $_client;
    private $_vendor;
    private $_orderShipping;
    private $_settings;
    private $_amount = 0;
    /*
    public $length = 0;
    public $width = 0;
    public $height = 0;
    public $weight = 0;
    */

    public function safeAttributes()
    {
        return ['order_id', 'vendor_id'];
    }

    public function calculateFee()
    {
        $orderShipping = $this->_orderShipping;
        $locationSettings = $this->_settings['Locations'];

        $districtId = $orderShipping->location_id;
        $cityId = $orderShipping->location->parent->id;

        $citySettings = $locationSettings[$cityId];
        $districtSettings = $locationSettings[$districtId];

        $this->serviceId = $citySettings['service'];

        $result = $this->_client->calculateFee($citySettings['code'], $districtSettings['code'], $citySettings['service']);

        if ($result['error']) {
            $this->errors = $result['error'];

            return false;
        }

        return $result['fee'];
    }

    public function createOrder()
    {
        $orderShipping = $this->_orderShipping;
        $locationSettings = $this->_settings['Locations'];

        $districtId = $orderShipping->location_id;
        $cityId = $orderShipping->location->parent->id;

        $citySettings = $locationSettings[$cityId];
        $districtSettings = $locationSettings[$districtId];
//var_dump($citySettings);
        $this->serviceId = $citySettings['service'];
        $result = $this->_client->createOrder($citySettings['pickup'], $districtSettings['code'], $orderShipping, $this->_amount, $citySettings['service']);

        if ($result['error']) {
            $this->errors = $result['error'];

            return false;
        }

        $orderShipping->fee = $result['fee'];
        $orderShipping->shipping_code = $result['orderCode'];

        if ($orderShipping->save()) {
            return $orderShipping;
        }

        return false;
    }

    public function getOrder()
    {
        $shippingCode = $this->_orderShipping->shipping_code;

        $result = $this->_client->getOrder($shippingCode);
        if ($result['error']) {
            $this->errors = $result['error'];

            return false;
        }

        return $result;
    }

    public function cancelOrder()
    {
        $shippingCode = $this->_orderShipping->shipping_code;

        $result = $this->_client->cancelOrder($shippingCode);

        if (!empty($result['error'])) {
            $this->errors = $result['error'];

            return false;
        }

        return true;
    }

    public function via($vendorId)
    {
        $this->_vendor = Vendor::findOne($vendorId);
        $this->_settings = (new VendorSettings($this->_vendor))->config;

        $className = 'kyna\\order\\lib\\' . $this->_vendor->alias . '\\' .Inflector::camelize($this->_vendor->alias);
        $this->_client = Yii::createObject($className);

        return $this;
    }

    public function withAmount($amount)
    {
        $this->_amount = $amount;

        return $this;
    }

    public function shipTo($orderShipping)
    {
        $this->_orderShipping = $orderShipping;

        return $this;
    }

    public static function getAvailable()
    {
        $vendors = Vendor::find()->where([
            'vendor_type' => Vendor::VENDOR_TYPE_LOGISTIC,
            'status' => true,
        ])->all();

        $availbleMethods = [];
        foreach ($vendors as $vendor) {
            $availbleMethods += $vendor->availableMethods;
        }

        return $availbleMethods;
    }
}
