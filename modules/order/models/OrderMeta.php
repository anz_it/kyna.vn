<?php

namespace kyna\order\models;

/**
 * This is the model class for table "order_meta".
 *
 * @property int $id
 * @property int $order_id
 * @property string $key
 * @property string $value
 * @property string $data_type
 */
class OrderMeta extends \kyna\base\ActiveRecord
{
    public static $readOnMaster = true;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'order_meta';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['order_id'], 'required'],
            [['order_id'], 'integer'],
            [['value'], 'string'],
            [['key'], 'string', 'max' => 50],
            [['data_type'], 'string', 'max' => 10],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order_id' => 'Order ID',
            'key' => 'Key',
            'value' => 'Value',
            'data_type' => 'Data Type',
        ];
    }
}
