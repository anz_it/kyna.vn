<?php

namespace kyna\order\models;

use app\modules\order\models\ActionForm;
use common\campaign\CampaignTet;
use kyna\promo\models\GroupDiscount;
use kyna\promotion\models\UserVoucherFree;
use Yii;
use yii\base\InvalidValueException;
use yii\helpers\Inflector;

use common\helpers\ArrayHelper;
use common\helpers\StringHelper;
use common\helpers\FbOfflineConversionHelper;

use kyna\order\models\actions\OrderAction;
use kyna\payment\request\LocationRequestModel;
use kyna\payment\models\PaymentTransaction;
use kyna\promotion\models\Promotion;
use kyna\base\models\BaseAddress;
use kyna\base\behaviors\ActionLogBehavior;
use kyna\base\events\ActionEvent;
use kyna\payment\models\PaymentMethod;
use kyna\user\models\User;
use kyna\user\models\UserCourse;
use kyna\user\models\Address;
use kyna\base\models\Location;
use kyna\course\models\Course;
use kyna\commission\models\AffiliateUser;
use kyna\commission\Commission;
use kyna\settings\models\Setting;
use kyna\partner\lib\monkey_junior\MonkeyJunior;
use kyna\commission\models\CommissionIncome;
use kyna\partner\models\Code;
use kyna\gamification\models\Mission;
use kyna\gamification\models\UserGift;
use kyna\user\models\UserTelesale;
use kyna\partner\models\form\CreateCodeForm;
/**
 * This is the model class for table "orders".
 *
 * @property int $id                        => yes, auto
 * @property string $order_number               => yes, {shipping_method}-{YYMMDD}-{number}
 * @property int $order_date                => yes, now
 * @property int $user_id                   => yes, userAddress->user_id
 * @property string $promotion_code                => promotion_code
 * @property string $activation_code
 * @property bool $is_paid                   => yes, false
 * @property int $payment_method            => not implemented yet
 * @property string $reference_id               => yes, logged in user
 * @property string $point_of_sale               => yes, order from?
 * @property string $affiliate_id               => yes, affiliate user
 * @property string $operator_id               => yes, operated user
 * @property int $status                    => yes, ORDER_STATUS_DELIVERING
 * @property int $created_time              => yes, auto
 * @property int $updated_time              => yes, auto
 * @property float $direct_discount_amount     => yes
 * @property float $group_discount_amount      => yes, auto, default = 0
 * @property decimal $sub_total      => yes, auto, default = 0
 * @property decimal $total_discount      => yes, auto, default = 0
 * @property decimal $shipping_fee      => yes, auto, default = 0
 * @property decimal $total      => yes, auto, default = 0
 * @property decimal $payment_fee      => yes, auto, default = 0
 * @property decimal $month      => yes, auto, default = 0
 * @property bool $is_done_telesale_process      => yes, default = 0
 * @property bool $is_activated      => yes, default = 0
 * @property integer $activation_date      => yes, default null
 * @property integer $sent_payment_request_time      => yes, default null
 * @property integer $page_id
 * @property integer $user_telesale_id
 * @property boolean $is_uploaded_fboc
 * @property boolean $is_purchased_fboc
 *  @property OrderShipping shipping_address
 * @property int to_shipper_time
 */
class Order extends \kyna\base\ActiveRecord implements \kyna\order\models\OrderInterface
{
    use traits\OrderTrait;

    public $actionParams = false;
    public $order_details = [];
    private $_shippingAddress = null;
    public $month;

    public static $readOnMaster = true;

    protected function enableMeta()
    {
        return 'order';
    }

    public function init()
    {
        parent::init();

        $this->on(self::EVENT_AFTER_INSERT, [$this, 'saveShippingAddress']);
        $this->on(self::EVENT_UPDATED_BACKEND, [$this, 'saveShippingAddress']);
        $this->on(self::EVENT_AFTER_INSERT, [$this, 'saveDetails']);
        $this->on(self::EVENT_ORDER_SENT_TO_SHIPPING, [$this, 'saveShippingAddress']);
        $this->on(self::EVENT_ORDER_UPDATED_SHIPPING_ADDRESS, [$this, 'saveShippingAddress']);
        $this->on(self::EVENT_ORDER_UPDATED_PARTNER_CODE, [$this, 'changeStatusPartnerCode']);
        $this->on(self::EVENT_ORDER_UPDATED, [$this, 'saveShippingAddress']);
        $this->on(self::EVENT_BEFORE_UPDATE, [$this, 'saveDetails']);
        $this->on(self::EVENT_ORDER_PLACED, [$this, 'saveShippingAddress']);
        $this->on(self::EVENT_ORDER_PLACED, [$this, 'checkMonkeyJunior']);
        $this->on(self::EVENT_CREATED_BACKEND, [$this, 'checkMonkeyJunior']);
        $this->on(self::EVENT_CREATED_BACKEND, [$this, 'generatePartnerCode']);
        $this->on(self::EVENT_ORDER_PLACED, [$this, 'generatePartnerCode']);
        $this->on(self::EVENT_ACTIVATED, [$this, 'afterActivate']);
        $this->on(self::EVENT_ORDER_COMPLETED, [$this, 'afterComplete']);
        $this->on(self::EVENT_ORDER_COMPLETED_COD, [$this, 'afterComplete'], ['send_mail' => false]);
        $this->on(self::EVENT_ORDER_CANCELLED, [$this, 'afterCancel']);
        $this->on(self::EVENT_ORDER_RETURN, [$this, 'afterReturn']);
        $this->on(self::EVENT_REQUESTED_PAYMENT, [$this, 'afterRequestPayment']);
        $this->on(self::EVENT_PAYMENT_FAILED, [$this, 'afterPaymentFailed']);
        $this->on(self::EVENT_ORDER_PLACED, [$this, 'uploadFbOc']);
        $this->on(self::EVENT_CREATED_BACKEND, [$this, 'uploadFbOc']);
        $this->on(self::EVENT_CREATED_RETAILER, [$this, 'uploadFbOc']);
        $this->on(self::EVENT_LANDING_PAGE, [$this, 'uploadFbOc']);
        $this->on(self::EVENT_LANDING_PAGE, [$this, 'registerSuccess']);
        $this->on(self::EVENT_ORDER_PLACED, [$this, 'registerSuccess']);
        $this->on(self::EVENT_CREATED_BACKEND, [$this, 'registerSuccess']);
        $this->on(self::EVENT_UPDATED_BACKEND, [$this, 'generatePartnerCode']);
        $this->on(self::EVENT_ORDER_UPDATED_SHIPPING_ADDRESS, [$this, 'generatePartnerCode']);
        $this->on(self::EVENT_ORDER_SENT_TO_SHIPPING, [$this, 'generatePartnerCode']);
        $this->on(self::EVENT_UPDATED_BACKEND, [$this, 'updatePromoUseTime']);
        $this->on(self::EVENT_CREATED_BACKEND, [$this, 'updateUserTeleSales']);
        return;
    }

    protected function metaKeys()
    {
        return [
            'call_status',
            'is_manual',
            'payment_receipt',
            'voucher_value'
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'orders';
    }

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors[] = [
            'class' => ActionLogBehavior::className(),
            'enableAction' => 'order',
            'modelClassName' => OrderAction::className(),
            'events' => [
                self::EVENT_ORDER_CREATED => [],
                self::EVENT_ORDER_UPDATED => [],
                self::EVENT_ORDER_CALL_ENDED => [],
                self::EVENT_ORDER_ADDED_NOTE => [],
                self::EVENT_ORDER_PLACED => [],
                self::EVENT_ORDER_SENT_TO_SHIPPING => [],
                self::EVENT_ORDER_UPDATED_SHIPPING_ADDRESS => [],
                self::EVENT_ORDER_UPDATED_PARTNER_CODE => [],
                self::EVENT_ORDER_UPDATED_PAYMENT_METHOD => [],
                self::EVENT_ORDER_GET_PAID => [],
                self::EVENT_ORDER_COMPLETED => [],
                self::EVENT_ORDER_COMPLETED_COD => [],
                self::EVENT_ORDER_CANCELLED => [],
                self::EVENT_ORDER_CANCELLED_SHIPPING => [],
                self::EVENT_ORDER_RETURN => [],
                self::EVENT_REQUESTED_PAYMENT => [],
                self::EVENT_PAYMENT_FAILED => [],
                self::EVENT_WAITING_RETURN => [],
                self::EVENT_UPDATED_BACKEND => [],
            ],
        ];

        return $behaviors;
    }

    public function scenarios()
    {
        $scenarios = [
            self::SCENARIO_DEFAULT => [
                'sub_total',
                'direct_discount_amount',
                'total_discount',
            ],
            // create order on backend
            self::SCENARIO_CREATE_BACKEND => [
                'user_id',
                'direct_discount_amount',
                'group_discount_amount',
                'order_details',
                'shippingAddress',
                'payment_method',
                'operator_id',
                'reference_id',
                'sub_total',
                'total_discount',
                'shipping_fee',
                'total',
                'payment_fee',
                'month',
                'point_of_sale',
                'is_done_telesale_process',
                'actionParams',
                'activation_code',
                'user_telesale_id',
                'is_uploaded_fboc',
                'is_purchased_fboc',
                'is_4kid'
            ],
            // update order on backend
            self::SCENARIO_UPDATE_BACKEND => [
                'user_id',
                'direct_discount_amount',
                'group_discount_amount',
                'order_details',
                'shippingAddress',
                'payment_method',
                'operator_id',
                'reference_id',
                'sub_total',
                'total_discount',
                'shipping_fee',
                'total',
                'payment_fee',
                'month',
                'point_of_sale',
                'is_done_telesale_process',
                'actionParams',
                'activation_code',
                'user_telesale_id',
                'is_uploaded_fboc'
            ],
            // create order on retailer dashboard
            self::SCENARIO_CREATE_RETAILER => [
                'user_id',
                'direct_discount_amount',
                'group_discount_amount',
                'order_details',
                'shippingAddress',
                'payment_method',
                'operator_id',
                'reference_id',
                'sub_total',
                'total_discount',
                'shipping_fee',
                'total',
                'payment_fee',
                'month',
                'point_of_sale',
                'is_done_telesale_process',
                'actionParams',
                'activation_code',
                'is_uploaded_fboc',
                'is_purchased_fboc'
            ],
            // create order on frontend
            self::SCENARIO_CREATE_FRONTEND => ['order_details', 'user_id', 'point_of_sale', 'is_uploaded_fboc', 'is_purchased_fboc'],
            // change status manually without doing an action (normally on backend)
            self::SCENARIO_COMPLETE => ['id', 'payment_receipt', 'actionParams', 'is_done_telesale_process', 'is_paid', 'is_purchased_fboc'],
            self::SCENARIO_COMPLETE_COD => ['id', 'payment_receipt', 'actionParams', 'is_done_telesale_process', 'is_paid', 'is_purchased_fboc'],
            self::SCENARIO_IN_COMPLETE => ['id', 'payment_receipt', 'actionParams'],
            self::SCENARIO_APPLY_PROMOTION => ['promotion_code', 'affiliate_id'],
            self::SCENARIO_REMOVE_PROMOTION => ['promotion_code'],
            self::SCENARIO_CANCEL => ['id', 'actionParams'],
            self::SCENARIO_CANCEL_SHIPPING => ['id', 'actionParams'],
            self::SCENARIO_RETURN => ['id', 'actionParams'],
            self::SCENARIO_MOVE_TO_CS => ['id', 'actionParams', 'is_done_telesale_process', 'affiliate_id'],
            self::SCENARIO_SEND_TO_SHIPPING => ['id', 'actionParams'],
            self::SCENARIO_UPDATE_SHIPPING_ADDRESS => ['id', 'actionParams', 'shippingAddress'],
            self::SCENARIO_UPDATE_PARTNER_CODE => ['id', 'actionParams'],
            self::SCENARIO_UPDATE_PAYMENT_METHOD => ['id', 'payment_method', 'actionParams', 'status', 'total', 'shipping_fee', 'payment_fee', 'is_purchased_fboc'],
            self::SCENARIO_CALL => ['id', 'actionParams'],
            self::SCENARIO_NOTE => ['id', 'actionParams'],
            // update order information without changing status
            self::SCENARIO_UPDATE => ['id', 'payment_method', 'order_details', 'sub_total', 'total_discount', 'shipping_fee', 'total', 'payment_fee'],
            self::SCENARIO_PLACE_ORDER => ['id', 'payment_method', 'shippingAddress', 'promotion_code', 'affiliate_id', 'is_purchased_fboc'],
            self::SCENARIO_LANDING_PAGE => ['order_details', 'user_id', 'point_of_sale', 'shippingAddress', 'is_purchased_fboc'],
            self::SCENARIO_ACTIVATE => ['is_activated', 'activation_date'],
            self::SCENARIO_WAITING_RETURN => ['status'],
            self::SCENARIO_PAYMENT_FAILED => ['status', 'sent_payment_request_time'],
            self::SCENARIO_REQUEST_PAYMENT => ['status', 'payment_method', 'affiliate_id', 'sent_payment_request_time', 'is_purchased_fboc'],
        ];

        $scenario = $this->getScenario();
        if (array_key_exists($scenario, $scenarios)) {
            $scenarios[$scenario] = array_merge($scenarios[$scenario], $this->metaKeys());
        }

        return $scenarios;
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            // global
            [['order_date', 'user_id', 'status', 'created_time', 'reference_id', 'affiliate_id', 'operator_id', 'updated_time', 'activation_date'], 'integer'],
            [['direct_discount_amount', 'group_discount_amount'], 'number'],
            [['direct_discount_amount', 'group_discount_amount'], 'default', 'value' => 0],
            [['is_paid', 'is_activated'], 'boolean'],
            [['order_number', 'payment_method'], 'string', 'max' => 20],
            [['promotion_code'], 'string', 'on' => self::SCENARIO_APPLY_PROMOTION],
            [['activation_code'], 'string', 'max' => 50],
            [['order_details'], function ($attribute) {
                return ArrayHelper::isAssoc($this->$attribute);
            }],
            [['_shippingAddress'], 'required', 'when' => function ($model) {
                return $model->isCod;
            }],
            // backend-create
            [['user_id', 'order_details'], 'required', 'on' => [self::SCENARIO_CREATE_BACKEND, self::SCENARIO_UPDATE_BACKEND, self::SCENARIO_CREATE_FRONTEND, self::SCENARIO_CREATE_RETAILER]],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order_number' => 'Số hóa đơn',
            'order_date' => 'Ngày lập hóa đơn',
            'user_id' => 'Học viên',
            'promotion_code' => 'Mã giảm giá',
            'subtotal' => 'Thành tiền',
            'shippingAddress' => 'Dịch vụ giao vận',
            'is_paid' => 'Đã thanh toán',
            'payment_method' => 'Hình thức thanh toán',
            'reference_id' => 'Người giới thiệu',
            'status' => 'Trạng thái',
            'activation_code' => 'Mã Kích Hoạt',
            'created_time' => 'Created Time',
            'updated_time' => 'Updated Time',
            'direct_discount_amount' => 'Giảm giá trực tiếp',
            'group_discount_amount' => 'Giảm giá theo nhóm',
            'group_discount' => 'Giảm giá theo nhóm',
            'order_details' => 'Khóa học',
            'is_activated' => 'Đã kích hoạt?',
            'activation_date' => 'Ngày kích hoạt',
        ];
    }

    public function beforeSave($insert)
    {
        $event = new ActionEvent();
        if ($this->actionParams) {
            $event->addMeta($this->actionParams);
        }
        $event->addMeta(['order' => $this->attributes]);

        if ($this->scenario ===self::SCENARIO_CREATE_FRONTEND  || $this->scenario === self::SCENARIO_CREATE_BACKEND or $this->scenario === self::SCENARIO_CREATE_RETAILER or $this->scenario === self::SCENARIO_CREATE_VIA_FORM or $this->scenario === self::SCENARIO_PLACE_ORDER or $this->scenario === self::SCENARIO_LANDING_PAGE) {
            $this->order_number = $this->generateOrderNumber();
            $this->order_date = time();
            $this->status = $this->isCod ? self::ORDER_STATUS_PENDING_CONTACT : self::ORDER_STATUS_PENDING_PAYMENT;

            if (empty($this->activation_code)) {
                $this->activation_code = self::generateActivationCode();
            }
        }

        if (empty($this->operator_id) && isset(Yii::$app->user)) {
            if (Yii::$app->user->id != $this->user_id) {
                // create from dashboard => then operator the user make action
                $this->operator_id = Yii::$app->user->id;
            } else {
                // user create order by himself => detect operator within timeslot then assign to order
                $this->operator_id = self::detectOperator($this);
            }
        }

        $this->is_paid = false;

        if ($this->scenario === self::SCENARIO_CANCEL) {
            $this->status = self::ORDER_STATUS_CANCELLED;
        } elseif ($this->scenario === self::SCENARIO_GET_PAID) {
            $this->is_paid = true;
        } elseif ($this->scenario === self::SCENARIO_COMPLETE) {
            $this->is_paid = true;
            $this->status = self::ORDER_STATUS_COMPLETE;
        } elseif ($this->scenario === self::SCENARIO_CREATE_FRONTEND) {
            $this->status = self::ORDER_STATUS_NEW;
        } elseif ($this->scenario === self::SCENARIO_SEND_TO_SHIPPING) {
            $this->status = self::ORDER_STATUS_DELIVERING;
            $this->trigger(self::EVENT_ORDER_BEFORE_SENT_TO_SHIPPING, $event);
        } elseif ($this->scenario === self::SCENARIO_IN_COMPLETE) {
            $this->status = self::ORDER_STATUS_IN_COMPLETE;
        } elseif ($this->scenario === self::SCENARIO_UPDATE) {
            $this->trigger(self::EVENT_BEFORE_UPDATE, $event);
        }

        if (empty($this->total)
            || $this->scenario === self::SCENARIO_UPDATE
            || $this->scenario === Order::SCENARIO_PLACE_ORDER
            || $this->scenario === Order::SCENARIO_APPLY_PROMOTION
            || $this->scenario === self::SCENARIO_UPDATE_BACKEND
        ) {
            $this->calculateTotal();

        }

        Promotion::updatePromotionUseTime($this);

        return parent::beforeSave($insert);
    }

    public function uploadFbOc()
    {
        if (empty($this->user_telesale_id) && ($this->is_uploaded_fboc == null || !$this->is_uploaded_fboc)) {
            $isAutoUpload = Setting::findOne([
                'key' => 'seo_fbofflineconversion_is_upload_on_lead_create'
            ]);

            if (isset($isAutoUpload) && $isAutoUpload->value) {
                $lead = [
                    'phone_number' => StringHelper::standardPhonumber($this->user->profile->phone_number),
                    'email' => $this->user->email,
                    'amount' => $this->total - $this->shipping_fee,
                    'created_time' => $this->order_date
                ];

                $params = [
                    'data' => [],
                ];
                $match_keys = [
                    'lead_id' => 'o_' . $this->id,
                    'country' => hash('sha256', 'VN'),
                ];
                if (!empty($lead['email']))
                    $match_keys['email'] = hash('sha256', $lead['email']);
                if (!empty($lead['phone_number']))
                    $match_keys['phone'] = hash('sha256', $lead['phone_number']);

                $details = $this->details;
                $courseIds = [];
                foreach ($details as $detail) {
                    $courseIds[] = $detail->course_id;
                }

                if (!empty($lead['amount'])) {
                    $entry = [
                        'match_keys' => $match_keys,
                        'event_name' => "Lead",
                        'event_time' => $lead['created_time'],
                        'content_type' => 'product',
                        'value' => empty($lead['amount']) ? 0 : $lead['amount'],
                        'currency' => 'VND',
                    ];
                    if (!empty($courseIds))
                        $entry['content_ids'] = $courseIds;
                    $data[] = $entry;

                    $params['data'] = $data;
                    $ret = FbOfflineConversionHelper::UploadEvents($params);

                    if ($ret['success']) {
                        self::updateAll([
                            'is_uploaded_fboc' => true
                        ], ['id' => $this->id]);
                    }
                }
            }
        }
    }

    // Tính giá thực sau khi giảm của order
    private function calculateTotal()
    {
        $subTotal = 0;
        $subDiscount = 0;
        $this->shipping_fee = 0;
        $promotion = $this->promotion;
        $items = $this->order_details;
        $combo_discount = array();
        $combo_discount_total = 0;


        foreach ($items as $item) {
            $subTotal += $item['unit_price'];
            $subDiscount += $item['discount_amount'];
          /*  if(!empty($item['course_combo_id'])){
                $combo_discount[$item['course_combo_id']] = $item['combo_discount_amount'];
            } */

        }




        if (empty($items)) {
            $items = $this->details;
            foreach ($items as $item) {
                $subTotal += $item->subTotal;
                $subDiscount += $item->discount_amount;
                if(!empty($item->course_combo_id)) {
                    $combo_discount[$item->course_combo_id] = $item->combo_discount_amount;
                }
            }
        }


       /* foreach ($combo_discount as $key => $value )
        {
            $combo_discount_total += $value;
        } */
        $voucherDiscountValue = 0;

        $this->sub_total = $subTotal ;
        $this->total_discount = $subDiscount + $this->direct_discount_amount ;


        /*  if ($promotion && $promotion->type === \kyna\promotion\models\Promotion::KIND_ORDER_APPLY) {
              $voucherDiscountValue = $promotion->calcOrderVoucherAmount($this->sub_total - $subDiscount, $items);
          }

          $this->total_discount = $subDiscount  + $this->direct_discount_amount + $voucherDiscountValue + $combo_discount_total;
          */

        $address = $this->_shippingAddress;
        if (!empty($address['location_id'])) {
            $this->shipping_fee = Location::calculateFee($address['location_id'], $this->sub_total - $this->total_discount);
        }
        $this->total = $this->sub_total - $this->total_discount + $this->shipping_fee;
        if ($this->total < 0) {
            $this->total = 0;
        }
        $this->payment_fee = $this->calPaymentFee();


    }

    private function removePromotionValue()
    {
        $subTotal = 0;
        $subDiscount = 0;

        $items = $this->details;
        foreach ($items as $item) {
            $item->discount_amount = 0;
            $item->save(false);
            $subTotal += $item->subTotal;
            $subDiscount += $item->discount_amount;
        }

        $this->sub_total = $subTotal;
        $this->total_discount = $subDiscount + $this->group_discount_amount + $this->direct_discount_amount;

        $address = $this->_shippingAddress;
        if (!empty($address['location_id'])) {
            $this->shipping_fee = Location::calculateFee($address['location_id'], $this->sub_total - $this->total_discount);
        }

        $this->total = $this->sub_total - $this->total_discount + $this->shipping_fee;
        if ($this->total < 0) {
            $this->total = 0;
        }

        $this->payment_fee = $this->calPaymentFee();
    }

    public function calPaymentFee()
    {
        $feeParams = Yii::$app->params['payment_fees'];
        $paymentMethod = $this->payment_method;
        $totalAmount = $this->total - $this->shipping_fee; //

        if (!isset($feeParams[$paymentMethod])) {
            return 0;
        }


        if($this->isCod  )
        {
            $province_id = $this->getLocationParentId();
            $arrFees = $feeParams[$paymentMethod];
            foreach ($arrFees as $locationId => $fee) {
                if ($locationId == $province_id) {
                    return $fee ;
                }
            }
            return $arrFees[-1];

        } else {
            //var_dump($paymentMethod);

            $feePercent = $feeParams[$paymentMethod];
            //var_dump($feePercent); die();
            return $totalAmount * $feePercent / 100;
        }


        /*  $feePercent = 0;
          if (is_array($feeParams[$paymentMethod])) {
              $arrFees = $feeParams[$paymentMethod];
              ksort($arrFees);

              foreach ($arrFees as $amount => $percent) {
                  if ($totalAmount <= $amount) {
                      $feePercent = $percent;
                      break;
                  }
              }
          } else {
              $feePercent = $feeParams[$paymentMethod];
          }

          return $totalAmount * $feePercent / 100; */


    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        $event = new ActionEvent();
        if ($this->actionParams) {
            $event->addMeta($this->actionParams);
        }
        $event->addMeta(['order' => $this->attributes]);

        if ($insert && $this->scenario == self::SCENARIO_CREATE_BACKEND) {
            $this->trigger(self::EVENT_ORDER_CREATED, $event);
        } else {
            if (is_array($changedAttributes)) {
                $event->addMeta($changedAttributes);
            }

            switch ($this->scenario) {
                case self::SCENARIO_UPDATE:
                    $this->trigger(self::EVENT_ORDER_UPDATED, $event);
                    break;

                case self::SCENARIO_COMPLETE:
                    $this->trigger(self::EVENT_ORDER_COMPLETED, $event);
                    break;

                case self::SCENARIO_COMPLETE_COD:
                    $this->trigger(self::EVENT_ORDER_COMPLETED_COD, $event);
                    break;

                case self::SCENARIO_CANCEL:
                    $this->trigger(self::EVENT_ORDER_CANCELLED, $event);
                    break;
                case self::SCENARIO_CANCEL_SHIPPING:
                    $this->trigger(self::EVENT_ORDER_CANCELLED_SHIPPING, $event);
                    break;
                case self::SCENARIO_RETURN:
                    $this->trigger(self::EVENT_ORDER_RETURN, $event);
                    break;

                case self::SCENARIO_PLACE_ORDER:
                    $this->trigger(self::EVENT_ORDER_PLACED, $event);

                    break;

                case self::SCENARIO_GET_PAID:
                    $this->trigger(self::EVENT_ORDER_GET_PAID, $event);
                    break;

                case self::SCENARIO_SEND_TO_SHIPPING:
                    $this->trigger(self::EVENT_ORDER_SENT_TO_SHIPPING, $event);
                    break;

                case self::SCENARIO_UPDATE_SHIPPING_ADDRESS:
                    $this->trigger(self::EVENT_ORDER_UPDATED_SHIPPING_ADDRESS, $event);
                    break;

                case self::SCENARIO_UPDATE_PARTNER_CODE:
                    $this->trigger(self::EVENT_ORDER_UPDATED_PARTNER_CODE, $event);
                    break;

                case self::SCENARIO_UPDATE_PAYMENT_METHOD:
                    $this->trigger(self::EVENT_ORDER_UPDATED_PAYMENT_METHOD, $event);
                    break;

                case self::SCENARIO_CALL:
                    $this->trigger(self::EVENT_ORDER_CALL_ENDED, $event);
                    break;

                case self::SCENARIO_APPLY_PROMOTION:
                    $this->trigger(self::EVENT_ORDER_APPLY_PROMOTION, $event);
                    break;

                case self::SCENARIO_REMOVE_PROMOTION:
                    $this->trigger(self::EVENT_ORDER_REMOVE_PROMOTION, $event);
                    break;

                case self::SCENARIO_NOTE:
                    $this->trigger(self::EVENT_ORDER_ADDED_NOTE, $event);
                    break;

                case self::SCENARIO_ACTIVATE:
                    $this->trigger(self::EVENT_ACTIVATED, $event);
                    break;

                case self::SCENARIO_PAYMENT_FAILED:
                    $this->trigger(self::EVENT_PAYMENT_FAILED, $event);
                    break;

                case self::SCENARIO_REQUEST_PAYMENT:
                    $this->trigger(self::EVENT_REQUESTED_PAYMENT, $event);
                    break;

                case self::SCENARIO_WAITING_RETURN:
                    $this->trigger(self::EVENT_WAITING_RETURN, $event);
                    break;

                default:
                    break;
            }
        }

        if ($this->scenario == self::SCENARIO_CREATE_BACKEND) {
            $this->trigger(self::EVENT_CREATED_BACKEND, $event);
        }

        if ($this->scenario == self::SCENARIO_UPDATE_BACKEND) {
            $this->trigger(self::EVENT_UPDATED_BACKEND, $event);
        }

        if ($this->scenario == self::SCENARIO_CREATE_RETAILER) {
            $this->trigger(self::EVENT_CREATED_RETAILER, $event);
        }

        if ($this->scenario == self::SCENARIO_LANDING_PAGE) {
            $this->trigger(self::EVENT_LANDING_PAGE, $event);
        }

        if ($this->payment_method == 'free' && $this->status != self::ORDER_STATUS_COMPLETE) {
            self::complete($this->id, null);
        }

        if ($this->status == self::ORDER_STATUS_PENDING_CONTACT && ($this->is_purchased_fboc == null || !$this->is_purchased_fboc)) {
            $this->postFbPurchasedEvent();
        }
    }

    /* IS COD */

    public function getIsCod()
    {
        $paymentMethod = $this->paymentMethod;
        return !empty($paymentMethod) ? $paymentMethod->isCod : false;
    }

    /* SHIPPING ADDRESS */

    /**
     * @return OrderShipping
     */
    public function getShippingAddress()
    {
        if (!$this->isCod) {
            return;
        }

        return $this->hasOne(OrderShipping::className(), [
            'order_id' => 'id',
        ]);
    }

    public function getOrderShipping()
    {
        return $this->hasOne(OrderShipping::className(), [
            'order_id' => 'id',
        ]);
    }

    public function setShippingAddress($address)
    {
        if ($address instanceof Address) {
            $this->_shippingAddress = $address->attributes;
        } elseif (ArrayHelper::isAssoc($address)) {
            $this->_shippingAddress = $address;
        }
    }

    public function saveShippingAddress()
    {
        $location_id = null;
        if (!$this->id) {
            throw new InvalidValueException('Order must be saved before its shipping address');
        }
        if (empty($this->_shippingAddress)) {
            if (!$this->isCod) {
                OrderShipping::deleteAll(['order_id' => $this->id]);
            }
            return false;
        }

        $address = $this->_shippingAddress;
        $address['order_id'] = $this->id;

        $orderShipping = OrderShipping::find()->where(['order_id' => $this->id])->one();
        if (!$orderShipping) {
            $orderShipping = new OrderShipping();
        }

        $orderShipping->attributes = $address;
        $orderShipping->fee = $this->shippingAmount;

        if (!$orderShipping->save()) {
            $this->addErrors(['shippingAddress' => array_values($orderShipping->getErrors())]);
            return false;
        }
        return true;
    }

    public function getShippingFee($location_id)
    {
        $location_model = new LocationRequestModel();
        $location_model->location_id = $location_id;
        return $location_model->shipFee['fee'];
    }

    public function getShippingAddressText()
    {
        if ($address = $this->shippingAddress and ($address instanceof BaseAddress)) {
            return $address->toString(false);
        }

        return 'N/A';
    }

    /** ORDER DETAILS * */
    public function getDetails()
    {
        return $this->hasMany(OrderDetails::className(), ['order_id' => 'id']);
    }

    public function getOrderMeta()
    {
        return $this->hasMany(OrderMeta::className(), ['order_id' => 'id']);
    }

    public function getDetailsText()
    {
        if (empty($this->details)) {
            return 'Invalid';
        }

        $details = $this->details;

        $data = [];
        foreach ($details as $detail) {
            if ($detail->course_combo_id) {
                $data[$detail->course_combo_id][] = $detail;
            } else {
                $data[$detail->course_id][] = $detail;
            }
        }

        $html = '<address>';
        foreach ($data as $items) {
            $detail = $items[0];
            $meta = json_decode($detail->course_meta, true);

            if (count($items) == 1) {
                // single course
                $price = Yii::$app->formatter->asCurrency($detail->unit_price - $detail->discount_amount);
                $name = !empty($meta['name']) ? $meta['name'] : (!empty($detail->course) ? $detail->course->name : '');

                $html .= $name . '. <strong>' . $price . '</strong>';
                $html .= '<br>';
            } else {
                // combo
                $totalOldPrice = 0;
                $totalDiscount = 0;
                $subName = '<ul>';

                foreach ($items as $subDetail) {
                    $totalOldPrice += $subDetail->unit_price;
                    $totalDiscount += $subDetail->discount_amount;

                    $subMeta = json_decode($subDetail->course_meta, true);
                    $subName .= "<li>{$subMeta['name']}</li>";
                }

                $subName .= '</ul>';

                if (isset($meta['combo_name'])) {
                    $comboName = $meta['combo_name'];
                } else {
                    $comboName = $detail->combo->name;
                }

                $price = Yii::$app->formatter->asCurrency($totalOldPrice - $totalDiscount);
                $html .= $comboName . '. <strong>' . $price . '</strong>' . $subName;
            }
        }
        $html .= '</address>';

        return $html;
    }

    public function getExportDetailsText()
    {
        if (empty($this->details)) {
            return 'Invalid';
        }

        $details = $this->details;

        $data = [];
        foreach ($details as $detail) {
            if ($detail->course_combo_id) {
                $data[$detail->course_combo_id][] = $detail;
            } else {
                $data[$detail->course_id][] = $detail;
            }
        }

        $html = '';
        foreach ($data as $items) {
            $detail = $items[0];
            $meta = json_decode($detail->course_meta, true);
            $html .= PHP_EOL;
            if (count($items) == 1) {
                // single course
                $price = Yii::$app->formatter->asCurrency($detail->unit_price - $detail->discount_amount);
                $name = !empty($meta['name']) ? $meta['name'] : (!empty($detail->course) ? $detail->course->name : '');
                $html .= $name . '. ' . $price;
            } else {
                // combo
                $totalOldPrice = 0;
                $totalDiscount = 0;
                $subName = '';
                foreach ($items as $subDetail) {
                    $subName .= PHP_EOL;
                    $totalOldPrice += $subDetail->unit_price;
                    $totalDiscount += $subDetail->discount_amount;
                    $subMeta = json_decode($subDetail->course_meta, true);
                    $subName .= "     - {$subMeta['name']}";
                }

                if (isset($meta['combo_name'])) {
                    $comboName = $meta['combo_name'];
                } else {
                    $comboName = $detail->combo->name;
                }

                $price = Yii::$app->formatter->asCurrency($totalOldPrice - $totalDiscount);
                $html .= $comboName . '. ' . $price . $subName;
            }
        }

        // replace first PHP_EOL
        $html = trim($html);

        return $html;
    }



    public function convertCartToOrder($cartItem)
    {

    }

    public function addDetails($products)
    {
        $oldDetails = $this->details;
        $oldDetailsIds = [];
        $oldDetailsAC = [];
        if (!empty($oldDetails)) {
            $oldDetailsIds = ArrayHelper::map($oldDetails, 'course_id', 'course_id');
            $oldDetailsAC = ArrayHelper::map($oldDetails, 'course_id', 'activation_code');
        }
        $this->group_discount_amount = 0;
        if(is_array($products))
        foreach ($products as $product) {
            $meta = json_decode(OrderDetails::extractCourseMeta($product), true);

            $combo_id = 0;
            if (!empty($product->combo)) {
                $combo_id = $product->combo->id;
                $meta['combo_name'] = $product->combo->name;
            }
            $discountGroupAmount = $product->getDiscountGroupAmount();
            $orderDetail = [
                'course_id' => $product->id,
                'course_combo_id' => $combo_id,
                'course_meta' => json_encode($meta),
                'unit_price' => $product->getOriginalPrice(),
                'discount_amount' =>   $product->discountAmount ,
                'is_delivered' => false,
                'activation_code' => '',
                'promo_code' => $product->getPromotionCode(),
                'combo_discount_amount' => $product->getComboDiscountAmount(),
            ];

            // assign old partner activation_code if any
            if (in_array($product->id, $oldDetailsIds)) {
                $orderDetail['activation_code'] = $oldDetailsAC[$product->id];
            }

            $this->order_details[] = $orderDetail;

            $this->group_discount_amount += $discountGroupAmount;
        }
    }

    public function setDetails($courses)
    {
        $this->order_details = [];
        $this->addDetails($courses);
    }

    protected function saveDetails()
    {
        if (!$this->id) {
            throw new InvalidValueException('Order must be saved before saving its details');
        }

        $result = false;
        if (!empty($this->order_details)) {
            $result = OrderDetails::saveMany($this->id, $this->order_details);
        }
        return $result;
    }

    /** STATUS * */
    public function getStatuses()
    {
        if ($this->isCod) {
            return self::getCommonStatues() + self::getCodStatuses();
        } else {
            return self::getCommonStatues() + self::getAutoStatuses();
        }
    }

    public static function getCommonStatues()
    {
        return [
            self::ORDER_STATUS_COMPLETE => 'Hoàn thành',
            self::ORDER_STATUS_CANCELLED => 'Bị hủy',
            self::ORDER_STATUS_IN_COMPLETE => 'Không hoàn thành',
            self::ORDER_STATUS_PAYMENT_FAILED => 'Thanh toán thất bại',
            self::ORDER_STATUS_REQUESTING_PAYMENT => 'Yêu cầu thanh toán',
        ];
    }

    public static function getCodStatuses()
    {
        return [
            self::ORDER_STATUS_COMPLETE => 'Hoàn thành',
            self::ORDER_STATUS_PENDING_CONTACT => 'Chờ liên hệ',
            self::ORDER_STATUS_DELIVERING => 'Đang giao hàng',
            self::ORDER_STATUS_RETURN => 'Đã trả về',
            self::ORDER_STATUS_WAITING_RETURN => 'Chờ trả về',
        ];
    }

    public static function getAutoStatuses()
    {
        return [
            self::ORDER_STATUS_NEW => 'Mới tạo',
            self::ORDER_STATUS_PENDING_PAYMENT => 'Chờ thanh toán',
        ];
    }

    public static function getAllStatuses()
    {
        return self::getAutoStatuses() + self::getCodStatuses() + self::getCommonStatues();
    }

    public function saveCall($callStatus, $meta = [])
    {
        $this->call_status = $callStatus;
        $this->actionParams = $meta;
        if ($this->save()) {
            return $this;
        }

        return false;
    }

    public function getSubtotal()
    {
        return $this->sub_total;
    }

    public function getTotalDiscount()
    {
        return $this->total_discount;
    }

    public function getTotalCourseDiscount()
    {
        $totalCourseDiscount = 0;
        // calculate commission
        foreach ($this->details as $detail) {
            $totalCourseDiscount += $detail->discountAmount;
        }

        return $totalCourseDiscount;
    }

    public function getShippingAmount()
    {
        return $this->shipping_fee;
    }

    public function getPaymentFee()
    {
        return $this->payment_fee;
    }

    public function getRealPayment()
    {
        return round(max($this->total, 0),2);
    }

    public function getRealIncome()
    {
        return $this->total - $this->shippingAmount - $this->paymentFee;
    }

    public function getStatusLabel()
    {
        $status = self::getStatuses();

        return isset($status[$this->status]) ? $status[$this->status] : null;
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function getUserAddress()
    {
        return $this->hasOne(Address::className(), ['user_id' => 'user_id']);
    }

    public function getOperator()
    {
        return $this->hasOne(User::className(), ['id' => 'operator_id']);
    }

    public function getOriginalAffiliateUser()
    {
        return $this->hasOne(AffiliateUser::className(), ['user_id' => 'affiliate_id']);
    }

    public function getOriginalAffiliate()
    {
        return $this->hasOne(User::className(), ['id' => 'affiliate_id']);
    }

    public function getAffiliateLastUser()
    {
        return $this->hasOne(AffiliateUser::className(), ['user_id' => 'operator_id']);
    }

    public function getCommissionIncomes()
    {
        return $this->hasMany(CommissionIncome::className(), ['order_id' => 'id']);
    }

    /**
     * @desc generate random unique code for activation
     *
     * @param int $codeLen
     *
     * @return string string
     */
    public static function generateActivationCode($codeLen = 6)
    {
        $code = StringHelper::random($codeLen);
        if (empty(self::checkUnique($code))) {
            return $code;
        }
        return StringHelper::random($codeLen);
    }

    /**
     * @param string $code
     * @return bool
     */
    public static function checkUnique($code, $code_len = 6)
    {
        $result = self::find()->where(['activation_code' => $code])->one();
        return $result;
    }

    /**
     * @desc Get payment method name
     *
     * @return string|null
     */
    public function getPaymentMethodName()
    {
        if ($this->payment_method == 'auto') {
            return 'Chuyển khoản Ngân hàng';
        }
        $paymentMethod = $this->paymentMethod;
        return !empty($paymentMethod) ? $paymentMethod->name : null;
    }

    public function getPaymentMethod()
    {
        return PaymentMethod::find()->where(['class' => $this->payment_method])->one();
    }

    public function generateOrderNumber()
    {
        return;
    }

    public function getPaymentTransactions()
    {
        return $this->hasMany(PaymentTransaction::className(), ['order_id' => 'id']);
    }

    public function getPaymentTransactionPayoo(){
        return PaymentTransaction::findOne([
            'order_id' => $this->id,
            'payment_method'=>'payoo'
            ]
        );
    }

    public function getPaidAmount()
    {
        return $this->getPaymentTransactions()->where(['is not', 'amount', null])->sum('amount');
    }

    public static function applyPromotion($id, $promotion_code)
    {
        $order = self::findOne($id);
        $order->setScenario(self::SCENARIO_APPLY_PROMOTION);
        $order->promotion_code = $promotion_code;

        $codeModel = $order->promotion;

        if (!empty($codeModel->partner_id)) {
            $order->affiliate_id = $codeModel->partner_id;
        }
        if (!empty($codeModel->issued_person)) {
            $isContentPartnerAff = AffiliateUser::find()->andWhere(['user_id' => $codeModel->issued_person])->exists();
            if ($isContentPartnerAff) {
                $order->affiliate_id = $codeModel->issued_person;
            }
        }

        if ($order->save()) {
            return $order;
        }

        return false;
    }

    public static function removePromotion($id)
    {
        $order = self::findOne($id);
        $order->setScenario(self::SCENARIO_REMOVE_PROMOTION);
        $order->promotion_code = null;
        if ($order->save()) {
            return $order;
        }
        return false;
    }

    /**
     * Update Promotion to used
     */
    public static function updatePromotion($order)
    {
        $promotion = Promotion::changeStatus($order);
        return $promotion;
    }

    public static function promotionInformation($code)
    {
        list($codeVoucherFree,$userId) = UserVoucherFree::isUserVoucherFree($code);
        if(!empty($codeVoucherFree) && !is_null($userId)){
            return Promotion::find()->andWhere(['code' => $codeVoucherFree])->one();
        }else{
            return Promotion::find()->andWhere(['code' => $code])->one();
        }
    }

    public function getPromotion()
    {
        return $this->hasOne(Promotion::className(), ['code' => 'promotion_code']);
    }

    public function getPromotionCode(){
        return $this->promotion_code;
    }

    public function setPromotion($promotionCode)
    {
        $this->promotion_code = $promotionCode;
        //$this->save(false);
    }

    public static function getOrderByAffiliateID($affiliate_id, $date_ranger = '')
    {
        $query = self::find()->where(['affiliate_id' => $affiliate_id]);
        if (!empty($date_ranger)) {
            $dateRange = explode(' - ', $date_ranger);
            $beginDate = date_create_from_format('d/m/Y H:i', $dateRange[0] . ' 00:00');
            $endDate = date_create_from_format('d/m/Y H:i', $dateRange[1] . ' 00:00');

            $query->andWhere(['>=', 'order_date', $beginDate->getTimestamp()]);
            $query->andWhere(['<=', 'order_date', $endDate->getTimestamp()]);
        }
        return $query;
    }

    public static function getOrderByAffiliateCategoryID($affiliate_category_id, $date_ranger = '')
    {
        $query = self::find();
        if (!empty($date_ranger)) {
            $dateRange = explode(' - ', $date_ranger);
            $beginDate = date_create_from_format('d/m/Y H:i', $dateRange[0] . ' 00:00');
            $endDate = date_create_from_format('d/m/Y H:i', $dateRange[1] . ' 00:00');

            $query->andWhere(['>=', 'order_date', $beginDate->getTimestamp()]);
            $query->andWhere(['<=', 'order_date', $endDate->getTimestamp()]);
        }
        $query->joinWith('originalAffiliateUser')->andWhere([AffiliateUser::tableName() . '.affiliate_category_id' => $affiliate_category_id]);
        return $query;
    }

    public function getPrintAddress()
    {
        if ($this->user->address) {
            return $this->user->address->addressText;
        }
        return 'N/A';
    }

    public function getRefund()
    {
        if (in_array($this->payment_method, ['epay', 'ipay'])) {
            $transactions = PaymentTransaction::find()->where([
                'order_id' => $this->id,
                'payment_method' => ['epay', 'ipay'],
                'status' => PaymentTransaction::STATUS_ACTIVE
            ])->andWhere([
                'NOT', ['amount' => null]
            ])->all();

            if (!empty($transactions)) {
                $amount = null;
                foreach ($transactions as $transaction) {
                    $amount += (int)$transaction->amount;
                }
                return Order::calculateRefund($amount - $this->total);
            }
        }
        return 0;
    }

    public static function getStatusNotDelete()
    {
        return [
            self::ORDER_STATUS_PENDING_CONTACT,
            self::ORDER_STATUS_DELIVERING,
            self::ORDER_STATUS_IN_COMPLETE,
            self::ORDER_STATUS_DRAFT,
            self::ORDER_STATUS_NEW,
            self::ORDER_STATUS_PENDING_PAYMENT
        ];
    }

    public function getTotalDetailIncome()
    {
        $totalDetailIncome = 0;

        foreach ($this->details as $oDetail) {
            $totalDetailIncome += ($oDetail->realIncome * $oDetail->affiliateCommissionPercent / 100);
        }

        return $totalDetailIncome;
    }

    public function checkMonkeyJunior()
    {
        $hasMonkey = false;

        foreach ($this->details as $detail) {
            if ($detail->course->isMonkeyJunior) {
                $hasMonkey = true;
                break;
            }
        }

        if ($hasMonkey) {
            $monkeyGuideCourseId = Yii::$app->params['id_of_monkey_junior_guide_course'];

            if (!UserCourse::find()->andWhere(['course_id' => $monkeyGuideCourseId, 'user_id' => $this->user_id])->exists()) {
                if (UserCourse::active($this->user_id, $monkeyGuideCourseId) !== false) {
                    // send mail
                    $mailer = Yii::$app->mailer;
                    $mailer->htmlLayout = false;
                    $mailer->compose('@common/mail/order/monkey_junior_guide')
                        ->setTo($this->user->email)
                        ->setSubject('Tặng ba mẹ khóa học "Hướng dẫn học Monkey Junior hiệu quả" tại Kyna.vn')
                        ->send();
                }
            }
        }
    }

    public function generatePartnerCode()
    {
        $order = self::findOne($this->id);
        if (!$order->isCod) {
            return false;
        }
        $details = $order->details;
        $partnerDetails = [];
        foreach ($details as $detail) {
            if (!empty($detail->activation_code)) {
                continue;
            }
            $course = $detail->course;
            if ($course && $course->isPartner && $partner = $course->partner) {
                if (!$partner->isServiceType) {
                    continue;
                }
                $combo = $detail->combo;
                if ($combo && $combo->isComboPartner) {
                    $itemId = $combo->id;
                } else {
                    $itemId = $course->id;
                }
                if (!isset($partnerDetails[$itemId])) {
                    if (empty($detail->activation_code)) {
                        // generate new partner code
                        $model = new CreateCodeForm();
                        $model->partner_id = $partner->id;
                        $code = $model->createAutoSingle(Code::CODE_STATUS_IN_ACTIVE);
                        if (!is_null($code)) {
                            $partnerDetails[$itemId] = $code->serial;
                        }
                    } else {
                        $partnerDetails[$itemId] = $detail->activation_code;
                    }
                }
                $detail->activation_code = $partnerDetails[$itemId];
                $detail->save(false);
            }
        }
    }

    /**
     * Check if active Course
     * @return bool
     */
    public function canActiveCourse()
    {
        if (in_array($this->status, array(
            self::ORDER_STATUS_CANCELLED,
            self::ORDER_STATUS_RETURN,
        ))) {
            return false;
        }
        return true;
    }

    public function getIsSpecial()
    {
        $oDetails = $this->details;
        if (empty($oDetails)) {
            return false;
        }
        // get setting special courses
        $settings = ArrayHelper::map(Setting::find()->all(), 'key', 'value');
        $specialCourses = !empty($settings['special_courses']) ? $settings['special_courses'] : '';
        $specialCourses = explode(',', $specialCourses);
        $specialCourseTypes = [
            Course::TYPE_SOFTWARE
        ];
        foreach ($oDetails as $oDetail) {
            if (!in_array($oDetail->course->type, $specialCourseTypes) && !in_array($oDetail->course->id, $specialCourses)) {
                return false;
            }
        }

        return true;
    }

    /**
     * Check order is Monkey Junior
     * @return bool
     */
    public function getHasMonkeyJunior()
    {
        $oDetails = $this->details;
        if (empty($oDetails)) {
            return false;
        }

        foreach ($oDetails as $oDetail) {
            if ($oDetail->course->isMonkeyJunior) {
                return true;
            }
        }

        return false;
    }

    public function afterComplete($event)
    {


        if (!$this->isCod || $this->isSpecial) {
            self::activate($this->id);

        }
        if(Yii::$app->params['campaign_birthday']) {
            $birthdayCodes = [];
            if (GroupDiscount::checkApplyBirthday($this->details)) {
                $birthdayCodes = Promotion::createBirthdayCode(100000, 1, 0, $this->user_id);
            }

        }
        if (!empty($this->promotion_code)) {
            $this->updateGiftStatus();
        }

        $eventData = $event->data;


        if (is_null($eventData) || (isset($eventData['send_mail']) && $eventData['send_mail'])) {
            if (!$this->isCod) {
                // create KynaLingo coupon for campaign t2
                if ($this->getHasGroupDiscountCourse()) {
                    $code = Promotion::createLingoPromotion($this->user->id);
                }
            }
            if($this->total > 0) {
                $mailer = Yii::$app->mailer;
                $mailer->htmlLayout = false;

                if ($this->hasOnlyPartner) {
                    // send mail partner if only has partner in order
                    // get setting cc email
                    $settings = ArrayHelper::map(Setting::find()->all(), 'key', 'value');
                    $ccEmails = !empty($settings['partner_email_cc']) ? $settings['partner_email_cc'] : 'hotro@kyna.vn';
                    $ccEmails = explode(',', $ccEmails);

                    $mailer->compose('partner/active', [
                        'order' => $this,
                        'settings' => ArrayHelper::map(Setting::find()->all(), 'key', 'value')
                    ])
                        ->setTo($this->user->email)
                        ->setCc($ccEmails)
                        ->setSubject("[Kyna for Kids] - MÃ KÍCH HOẠT SẢN PHẨM")
                        ->send();
                } else {
                    // send mail Kyna order success
                    $mailer->compose('order/order_success', [
                        'order' => $this,
                        'settings' => ArrayHelper::map(Setting::find()->all(), 'key', 'value'),
                        'code' => !empty($code) ? $code : null,
                        'codeBirthday' => !empty($birthdayCodes),
                    ])->setTo($this->user->email)
                        ->setSubject("Thanh toán đơn hàng #{$this->id} thành công.")
                        ->send();
                }
            }
        }


        // check if Monkey Junior + online payment -> just call api to partner Monkey Junior
        $onlinePayment = ['epay', 'onepay_atm', 'onepay_cc', 'momo'];
        if (isset(Yii::$app->params['monkey_junior']) && $configs = Yii::$app->params['monkey_junior']) {
            $onlinePayment = isset($configs['online_payment']) ? $configs['online_payment'] : $onlinePayment;
        }
        if ($this->hasMonkeyJunior && in_array($this->payment_method, $onlinePayment)) {
            $monkeyJuniorLib = new MonkeyJunior();
            $monkeyJuniorLib->active($this->id);
        }

        $this->findMissionsAndGetPoint();
        if (($this->is_uploaded_fboc || !empty($this->user_telesale_id)) && !$this->is_purchased_fboc) {
            $this->postFbPurchasedEvent();
        }

        if ($this->isMobileCardPayment() && !empty($this->refund)) {
            $this->refundCodes($this->refund);
        }

        if ($this->v2_id != null || $this->v2_cod_id != null) {
            // not calculate commission for v2 data
            return;
        }

        $details = $this->details;
        //tính commmission
        foreach ($details as $detail) {
            Commission::calculate(
                $this->id,
                $this->subTotal,
                $this->total,
                $this->totalCourseDiscount,
                $this->totalDiscount,
                $this->paymentFee,
                $detail->course_id,
                $detail->itemTotal,
                $this->affiliate_id,
                $detail->course_combo_id,
                $detail,
                $this->shipping_fee,
                $this->direct_discount_amount,
                $details
            );
        }
    }

    public function getRefundCodes($amount)
    {
        $codeAmounts = [
            50000 => 0,
            100000 => 0
        ];

        if ($amount <= 50000) {
            $codeAmounts[50000] = 1;
        } elseif ($amount <= 100000) {
            $codeAmounts[100000] = 1;
        } else {
            $extra = $amount % 100000;
            $codeAmounts[100000] = (int)($amount / 100000);

            if ($extra > 0) {
                if ($extra <= 50000) {
                    $codeAmounts[50000] = 1;
                } else {
                    $codeAmounts[100000] = $codeAmounts[100000] + 1;
                }
            }
        }

        return $codeAmounts;
    }

    public function refundCodes($amount)
    {
        $codeAmounts = $this->getRefundCodes($amount);

        $dateTimeFormat = str_replace('php:', '', Yii::$app->formatter->datetimeFormat);

        foreach ($codeAmounts as $key => $data) {
            $value = $key;
            $num = $data;

            for ($i = 1; $i <= $num; $i ++) {
                $code = new Promotion();
                $code->auto = true;
                $code->start_date = date($dateTimeFormat);
                $code->apply_all = Promotion::BOOL_NO;
                $code->apply_all_single_course = 1;
                $code->apply_all_single_course_double = 0;
                $code->apply_all_combo = 0;
                $code->number_usage = 1;
                $code->user_number_usage = 1;
                $code->current_number_usage = 0;
                $code->prefix = 'GIFT_O' . $this->id . '_';
                $code->value = $value;
                $code->type = Promotion::KIND_COURSE_APPLY;
                $code->discount_type = Promotion::TYPE_FEE;
                $code->created_by = Yii::$app->user->id;
                $code->user_id = $this->user_id;
                $code->end_date = date($dateTimeFormat, strtotime('+1 year'));
                $code->apply_scope = Promotion::APPLY_SCOPE_ALL;
                $code->status = Promotion::BOOL_YES;
                if (!$code->save()) {
                    var_dump($code->errors);die;
                }
            }
        }
    }

    public function isMobileCardPayment()
    {
        if (in_array($this->payment_method, ['epay', 'ipay'])) {
            return true;
        }

        return false;
    }

	public function postFbPurchasedEvent()
    {
        $isAutoUpload = Setting::findOne([
            'key' => 'seo_fbofflineconversion_is_upload_on_lead_create'
        ]);
        $amount = $this->total - $this->shipping_fee;

        if ($amount > 0 && isset($isAutoUpload) && $isAutoUpload->value) {
            if (!empty($this->user_telesale_id)) {
                $id = $this->user_telesale_id;
                $phoneNumber = $this->userTelesale->phone_number;
                $email = $this->userTelesale->email;
            } else {
                $phoneNumber = $this->user->profile->phone_number;
                $email = $this->user->email;
                $id = 'o_' . $this->id;
            }

            $lead = [
                'phone_number' => StringHelper::standardPhonumber($phoneNumber),
                'email' => $email,
                'amount' => $amount,
                'created_time' => $this->order_date
            ];

            $params = [
                'data' => [],
            ];
            $match_keys = [
                'lead_id' => $id,
                'country' => hash('sha256', 'VN'),
            ];
            if (!empty($lead['email']))
                $match_keys['email'] = hash('sha256', $lead['email']);
            if (!empty($lead['phone_number']))
                $match_keys['phone'] = hash('sha256', $lead['phone_number']);

            $details = $this->details;
            $courseIds = [];
            foreach ($details as $detail) {
                $courseIds[] = $detail->course_id;
            }

            if (!empty($lead['amount'])) {
                $entry = [
                    'match_keys' => $match_keys,
                    'event_name' => "Purchase",
                    'event_time' => $lead['created_time'],
                    'content_type' => 'product',
                    'value' => empty($lead['amount']) ? 0 : $lead['amount'],
                    'currency' => 'VND',
                ];
                if (!empty($courseIds))
                    $entry['content_ids'] = $courseIds;
                $data[] = $entry;

                $params['data'] = $data;
                $ret = FbOfflineConversionHelper::UploadEvents($params);

                if ($ret['success']) {
                    $updateAttrs = [];

                    if (($this->is_uploaded_fboc == null || !$this->is_uploaded_fboc)) {
                        $updateAttrs['is_uploaded_fboc'] = true;
                    }
                    if (($this->is_purchased_fboc == null || !$this->is_purchased_fboc)) {
                        $updateAttrs['is_purchased_fboc'] = true;
                    }
                    if (!empty($updateAttrs)) {
                        self::updateAll($updateAttrs, ['id' => $this->id]);
                    }
                }
            }
        }
    }

    public function updateGiftStatus()
    {
        $userGift = UserGift::find()->where(['code' => $this->promotion_code])->one();
        if ($userGift != null) {
            $userGift->used_date = time();
            $userGift->save();
        }
    }

    public function findMissionsAndGetPoint()
    {
        $missions = Mission::findByOrderComplete($this->total);

        foreach ($missions as $mission) {
            $point = $mission->k_point;

            Mission::addPointByMission($this->user_id, $point, $mission->id, $this->id);
        }
    }

    public function afterCancel()
    {
        // return current_number_usage + 1 if has promotion code
        $promotion = self::promotionInformation($this->promotion_code);
        if ($promotion && $promotion->current_number_usage > 0) {
            $promotion->current_number_usage -= 1;
            $promotion->save(false);
        }
        $this->returnPartnerCode();
    }

    public function afterReturn()
    {
        $this->returnPartnerCode();
    }

    public function returnPartnerCode($exceptCourseIds = [])
    {
        // return Partner Code if any
        foreach ($this->details as $detail) {
            if (in_array($detail->course_id, $exceptCourseIds)) {
                continue;
            }
            $partnerCode = $detail->partnerCode;
            if ($partnerCode) {
                $partnerCode->cancel(true);
            }
        }
    }

    public function afterActivate()
    {
        $details = $this->details;

        $courseIds = ArrayHelper::map($details, 'id', 'course_id');

        UserCourse::active($this->user_id, $courseIds);



        // calculate commission

    }

    public function afterRequestPayment()
    {
        // send mail
        $mailer = Yii::$app->mailer;
        $mailer->htmlLayout = '@common/mail/layouts/main';
        $mailer->compose('@common/mail/order/request_payment', [
            'order' => $this,
            'settings' => ArrayHelper::map(Setting::find()->all(), 'key', 'value')
        ])
            ->setTo($this->user->email)
            ->setSubject('Yêu cầu thanh toán đơn hàng #' . $this->id . ' tại Kyna.vn')
            ->send();
    }

    public function afterPaymentFailed()
    {
        $mailer = Yii::$app->mailer;
        $mailer->htmlLayout = false;

        return $mailer->compose('order/payment_failed', [
            'order' => $this,
            'settings' => ArrayHelper::map(Setting::find()->all(), 'key', 'value'),
        ])
            ->setTo($this->user->email)
            ->setSubject("Đăng ký đơn hàng #{$this->id} không thành công.")
            ->send();
    }

    public function getHasPartner()
    {
        $details = $this->details;
        foreach ($details as $detail) {
            $course = $detail->course;
            if (!empty($course) && $course->isPartner && !empty($course->partner) && $course->partner->isServiceType) {
                return true;
            }
        }
        return false;
    }

    public function getHasOnlyPartner()
    {
        $details = $this->details;
        foreach ($details as $detail) {
            $course = $detail->course;
            if (!empty($course) && $course->type == Course::TYPE_VIDEO) {
                return false;
            }
            if(!empty($course) && $course->type == Course::TYPE_SOFTWARE && !empty($detail->activation_code)){
                return true;
            }
        }
        return false;
    }

    public function getHasOnlyNormalPartner()
    {
        $details = $this->details;
        foreach ($details as $detail) {
            $course = $detail->course;
            if (empty($course) || !$course->isPartner || $course->partner->isServiceType) {
                return false;
            }
        }
        return true;
    }

    public function canInputPartnerCode()
    {
        if (!$this->isCod || !$this->hasPartner) {
            return false;
        }
        $details = $this->details;
        foreach ($details as $detail) {
            $course = $detail->course;
            if ($course && $course->isPartner && $course->partner->isServiceType && !empty($detail->activation_code)) {
                return false;
            }
        }
        return true;
    }

    public function changeStatusPartnerCode()
    {
        $details = $this->details;
        foreach ($details as $detail) {
            $course = $detail->course;
            if ($course && $course->isPartner && $partnerCode = $detail->partnerCode) {
                $partnerCode->status = Code::CODE_STATUS_IN_ACTIVE;
                $partnerCode->save(false);
            }
        }
    }

    public function getShippingOrderCode()
    {
        $shippingClassName = self::PAYMENT_METHOD_NAMESPACE . $this->shipping_method . '\\' . Inflector::camelize($this->shipping_method);
        $paymentTransaction = PaymentTransaction::find()->where(['order_id' => $this->id])->orderBy(['id' => SORT_DESC])->one();
        $orderCode = '';
        if (class_exists($shippingClassName)) {
            $orderCode = $shippingClassName::KYNA_TRANSACTION_PREFIX;
            if (!empty($paymentTransaction->{$shippingClassName::KYNA_TRANSACTION_FIELD})) {
                $orderCode .= $paymentTransaction->{$shippingClassName::KYNA_TRANSACTION_FIELD};
            }
        }
        return $orderCode;
    }

    public function getUserTelesale()
    {
        return $this->hasOne(UserTelesale::className(), ['id' => 'user_telesale_id']);
    }

    public function getHasGroupDiscountCourse()
    {
        if (empty($this->details)) {
            return false;
        }
        foreach ($this->details as $detail) {
            if ($detail->course && $detail->course->isCampaignGroupDiscount) {
                return true;
            }
        }
        return false;
    }

    public function registerSuccess()
    {
        // send register success if cod, auto, bank_transfer
        if ($this->isCod || in_array($this->payment_method, ['auto', 'bank-transfer','payoo'])) {
            $mailer = Yii::$app->mailer;
            $mailer->htmlLayout = false;

            $settings = ArrayHelper::map(Setting::find()->all(), 'key', 'value');
            $mailer->compose('order/register_success', [
                'order' => $this,
                'settings' => $settings,
                'code' => !empty($code) ? $code : null
            ])->setTo($this->user->email)
                ->setSubject("Đăng ký đơn hàng #{$this->id} thành công.")
                ->send();
        }
    }

    public static function countOrderByEmailUser($email){
        $order = self::find()
            ->leftJoin('user','user.id=orders.user_id')
            ->where(['=', 'user.email', $email])
            ->count();
        return $order;

    }

    public function isUpdateEmail(){
        if($this->payment_method == 'cod'){ // hinh thuc cod
            switch($this->status){
                case self::ORDER_STATUS_DELIVERING:
                    return false;
                case self::ORDER_STATUS_IN_COMPLETE:
                    return false;
                case self::ORDER_STATUS_CANCELLED:
                    return false;
                case self::ORDER_STATUS_COMPLETE:
                    return false;
            }
        }
        else if(($this->payment_method == 'onepay_atm')
            || ($this->payment_method == 'onepay_cc')
            || ($this->payment_method == 'epay')
            || ($this->payment_method == 'bank-transfer')
            || ($this->payment_method == 'ipay')
            || ($this->payment_method == 'momo')
            || ($this->payment_method == 'payoo')
        ){
            switch($this->status){
                case self::ORDER_STATUS_IN_COMPLETE:
                    return false;
                case self::ORDER_STATUS_COMPLETE:
                    return false;
                case self::ORDER_STATUS_CANCELLED:
                    return false;
            }
        }

        return true;
    }

    public function canUpdate()
    {
        $enableActions = ActionForm::getEnableActions($this->status, $this->payment_method, $this->shipping_method);
        return isset($enableActions['update']) ? true : false;
    }
    public function updatePromoUseTime()
    {
        Promotion::updateUseTime($this);
    }

    private function getLocationParentId()
    {
        $address = $this->getShippingAddress()->one();
        if (!empty($address->location_id)) {
            return Location::findOne(['id'=>$address->location_id])->parent_id;
        } else {
            return -1;
        }
    }

    public function updateUserTeleSales()
    {
        if(!empty($this->user_telesale_id)) {
            $userTeleSales = UserTelesale::findOne($this->user_telesale_id);
            $userTeleSales->order_id = $this->id;
            $userTeleSales->save(false);
        }
    }

    public function updateStatus($status){
        $this->status = $status;
        $this->save(false);
    }

    public function newOrderVoucherFreeRef($user_id,$course,$landingPageId=0){
        $this->user_id  =  $user_id;
        $this->setDetails(array($course));
        $this->page_id = $landingPageId;
        $this->order_date = time();
        $this->status = self::ORDER_STATUS_COMPLETE;
        $this->direct_discount_amount = $course->price;
        $this->is_done_telesale_process = 1;
        //save Order
        $this->save(false);
        // save detail order
        $this->saveDetails();

        Order::note($this->id, $this->status, ['note'=>'VOUCHER FREE']);
        // add userCourse
        UserCourse::addUserCourse($user_id,$course->id);

        return $this;
    }
}
