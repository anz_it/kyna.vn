<?php

namespace kyna\order\models\traits;

use Yii;
use yii\base\InvalidParamException;

use common\helpers\RoleHelper;
use kyna\order\models\Order;
use kyna\user\models\TimeSlot;
use kyna\partner\models\Code;
use kyna\base\events\ActionEvent;
use kyna\commission\models\AffiliateUser;

trait OrderTrait
{
    private static function _save($order)
    {
        try {
            if ($order->save()) {
                return $order;
            }
        } catch (Exception $ex ) {
            var_dump($ex->getMessage());die;
        }

        return false;
    }

    private static function _getOrder($scenario, $orderId = false, $status = null)
    {
        if (!$orderId) {
            $order = new Order();
        } else {
            $order = Order::find()->filterWhere([
                'id' => $orderId,
                'status' => $status,
            ])->one();

            if (!$order) {
                throw new InvalidParamException('Can\'t find an order with entered `orderId`');
            }
        }
        $order->setScenario($scenario);

        return $order;
    }

    public static function createEmpty($scenario)
    {
        return self::_getOrder($scenario);
    }

    public static function complete($orderId, $payment_receipt, $actionParams = false, $scenario = Order::SCENARIO_COMPLETE)
    {
        $order = self::_getOrder($scenario, $orderId, [
            Order::ORDER_STATUS_NEW,
            Order::ORDER_STATUS_PENDING_PAYMENT,
            Order::ORDER_STATUS_PENDING_CONTACT,
            Order::ORDER_STATUS_DELIVERING,
            Order::ORDER_STATUS_IN_COMPLETE
        ]);

        if (!Code::activeMultiPartner($order)) {
            return false;
        }

        $order->is_done_telesale_process = Order::BOOL_YES;
        $order->is_paid = Order::BOOL_YES;
        $order->status = Order::ORDER_STATUS_COMPLETE;
        $order->payment_receipt = $payment_receipt;

        if ($actionParams) {
            $order->actionParams = $actionParams;
        }

        return self::_save($order);
    }

    public static function Incomplete($orderId, $payment_receipt, $actionParams = false)
    {
        $order = self::_getOrder(Order::SCENARIO_IN_COMPLETE, $orderId, [
            Order::ORDER_STATUS_NEW,
            Order::ORDER_STATUS_PENDING_PAYMENT,
            Order::ORDER_STATUS_PENDING_CONTACT,
            Order::ORDER_STATUS_DELIVERING,
            Order::ORDER_STATUS_CANCELLED
        ]);

        $order->status = Order::ORDER_STATUS_IN_COMPLETE;
        $order->payment_receipt = $payment_receipt;

        if ($actionParams) {
            $order->actionParams = $actionParams;
        }

        return self::_save($order);
    }

    public static function cancel($orderId, $actionParams = false)
    {
        $order = self::_getOrder(Order::SCENARIO_CANCEL, $orderId);
        $order->is_done_telesale_process = Order::BOOL_YES;
        $order->status = Order::ORDER_STATUS_CANCELLED;

        if ($actionParams) {
            $order->actionParams = $actionParams;
        }
        return self::_save($order);
    }

    public static function cancelShipping($orderId, $actionParams = false)
    {
        $order = Order::_getOrder(Order::SCENARIO_CANCEL_SHIPPING, $orderId);
        if ($actionParams) {
            $order->actionParams = $actionParams;
        }
        return self::_save($order);
    }

    public static function codReturn($orderId, $actionParams = false)
    {
        $order = self::_getOrder(Order::SCENARIO_RETURN, $orderId);
        $order->status = Order::ORDER_STATUS_RETURN;

        if ($actionParams) {
            $order->actionParams = $actionParams;
        }

        return self::_save($order);
    }

    public static function moveToCs($orderId, $actionParams = false)
    {
        $order = self::_getOrder(Order::SCENARIO_MOVE_TO_CS, $orderId);
        $order->is_done_telesale_process = Order::BOOL_YES;

        if (empty($order->affiliate_id)) {
            $order->affiliate_id = Yii::$app->user->id;
        }

        if ($actionParams) {
            $order->actionParams = $actionParams;
        }

        return self::_save($order);
    }

    public static function createBackend($orderDetails, $userId, $shippingAddress = null, $directDiscountAmount = 0, $groupDiscount = false)
    {
        $order = self::_getOrder(Order::SCENARIO_CREATE_BACKEND);

        $order->attributes = [
            'details' => $orderDetails,
            'user_id' => $userId,
            'orderShipping' => $shippingAddress,
            'direct_discount_amount' => $directDiscountAmount,
            'group_discount' => $groupDiscount,
        ];

        return self::_save($order);
    }

    public static function create($orderDetails, $userId, $meta = false, $scenario = Order::SCENARIO_CREATE_FRONTEND, $promotion_code = null)
    {

        $order = self::_getOrder($scenario);
        $order->user_id = $userId;
        $order->promotion_code = $promotion_code;
        $order->details = $orderDetails;

      /*  foreach ($orderDetails as $position )
        {
            $tmp =  'quantity :' . $position->quantity . '- price : '  . $position->price . '- discount:' . $position->discountAmount .   '-voucher discount:' . $position->voucherDiscountAmount .   ' - promotion code : '. $position->promotionCode ;
            var_dump($tmp );
        }
        die(); */


        $order->status = Order::ORDER_STATUS_NEW;
        if ($meta) {
            $order->loadMeta($meta);
        }

        return self::_save($order);
    }

    /**
     * Determine order is waiting to pay or not.
     *
     * @param int $orderId [description]
     * @param string $paymentMethod [description]
     *
     * @return bool|Order [description]
     */
    public static function placeOrder($orderId, $paymentMethod, $details, $shippingAddress = null, $actionParams = false, $user_id = null, $promotionCode = null)
    {
        $order = self::_getOrder(Order::SCENARIO_PLACE_ORDER, $orderId);
        $order->payment_method = $paymentMethod;
        if($paymentMethod == 'payoo'){
            $order->status = Order::ORDER_STATUS_PAYMENT_FAILED;
        }
        $order->details = $details;

        if ($promotionCode != null && $order->promotion_code != $promotionCode) {
            $order->promotion_code = $promotionCode;

            $codeModel = $order->promotion;

            if (!empty($codeModel->partner_id)) {
                $order->affiliate_id = $codeModel->partner_id;
            }
            if (!empty($codeModel->issued_person)) {
                $isContentPartnerAff = AffiliateUser::find()->andWhere(['user_id' => $codeModel->issued_person])->exists();
                if ($isContentPartnerAff) {
                    $order->affiliate_id = $codeModel->issued_person;
                }
            }
            $event = new ActionEvent();
            if ($order->actionParams) {
                $event->addMeta($order->actionParams);
            }
            $event->addMeta(['order' => $order->attributes]);

            $order->trigger(self::EVENT_ORDER_APPLY_PROMOTION, $event);
        }

        if ($user_id !== null) {
            $order->user_id = $user_id;
        }

        if ($shippingAddress && $order->isCod) {
            $order->shippingAddress = $shippingAddress;
        }
        if ($actionParams) {
            $order->actionParams = $actionParams;
        }

        if (empty($order->affiliate_id)) {
            // detect exist affiliate
            $cookies = Yii::$app->request->cookies;
            if ($affiliateId = $cookies->getValue('affiliate_id')) {
                $order->affiliate_id = $affiliateId;
            }
        }

        return self::_save($order);
    }



    public static function updateDetails($orderId, $details, $totalAmount,$promotion_code = null)
    {

      /*  foreach ($details as $position )
        {
            $tmp =  'quantity :' . $position->quantity . '- price : '  . $position->price . '- discount:' . $position->discountAmount .   '-voucher discount:' . $position->voucherDiscountAmount .   ' - promotion code : '. $position->promotionCode ;
            var_dump($tmp );
        }
        die(); */





        $order = self::_getOrder(Order::SCENARIO_UPDATE, $orderId);
        $needToUpdate = false;

        if (count($order->details) !== count($details)) {
            $needToUpdate = true;
        }

        if($order->promotion_code != $promotion_code)
        {
            $order->promotion_code = $promotion_code;
            $needToUpdate = true;
        }
        foreach ($order->details as $orderDetail) {
            $checkDiff = true;

            foreach ($details as $detail) {
                if ($orderDetail->course_id == $detail->id && $orderDetail->discountAmount == $detail->discountAmount) {
                    $checkDiff = false;
                    break;
                }
            }

            if ($checkDiff) {
                $needToUpdate = true;
                break;
            }
        }

        if ($totalAmount != $order->total) {
            $needToUpdate = true;
        }



       // var_dump($needToUpdate. $promotion_code); die();
        if ($needToUpdate ) {

            $order->details = $details;

            return self::_save($order);
        }

        return $order;
    }

    public static function getPaid($orderId, $transactionCode)
    {
        $order = self::_getOrder(Order::SCENARIO_GET_PAID, $orderId);
        $order->attributes = [
            'transactionCode' => $transactionCode,
            'is_paid' => true,
        ];

        return self::_save($order);
    }

    public static function requestPayment($orderId, $paymentMethod = null)
    {
        $order = self::_getOrder(Order::SCENARIO_REQUEST_PAYMENT, $orderId);

        if ($paymentMethod != null) {
            $order->payment_method = $paymentMethod;
        }
        if (empty($order->affiliate_id)) {
            $order->affiliate_id = Yii::$app->user->id;
        }
        $order->status = self::ORDER_STATUS_REQUESTING_PAYMENT;
        $order->is_done_telesale_process = Order::BOOL_YES;
        $order->sent_payment_request_time = time();

        return self::_save($order);
    }

    public static function makeFailedPayment($orderId)
    {
        $order = self::_getOrder(Order::SCENARIO_PAYMENT_FAILED, $orderId);

        $order->status = self::ORDER_STATUS_PAYMENT_FAILED;
        $order->sent_payment_request_time = time();

        return self::_save($order);
    }

    public static function makeCodWaitingForReturn($orderId)
    {
        $order = self::_getOrder(Order::SCENARIO_WAITING_RETURN, $orderId, self::ORDER_STATUS_DELIVERING);

        $order->status = self::ORDER_STATUS_WAITING_RETURN;

        return self::_save($order);
    }

    public static function sendToShipping($orderId, $shippingAddress, $params = false)
    {
        $order = self::_getOrder(Order::SCENARIO_SEND_TO_SHIPPING, $orderId, [
            self::ORDER_STATUS_PENDING_CONTACT
        ]);

        $order->is_done_telesale_process = Order::BOOL_YES;

        $order->setAttributes([
            'status' => Order::ORDER_STATUS_DELIVERING,
            'payment_method' => $shippingAddress['payment_method'],
            'shipping_method' => $shippingAddress['shipping_method'],
            'activation_code' => $shippingAddress['activation_code']
        ], false);

        $order->shippingAddress = $shippingAddress;
        if ($params) {
            $order->actionParams = $params;
        }
        $order->payment_fee = $order->calPaymentFee();
        return self::_save($order);
    }

    public static function updateShippingAddress($order, $shippingAddress, $params = false)
    {
        $order->setScenario(Order::SCENARIO_UPDATE_SHIPPING_ADDRESS);

        $order->shippingAddress = $shippingAddress;

        if ($params) {
            $order->actionParams = $params;
        }

        return self::_save($order);
    }

    public static function updatePartnerCode(Order $order, $partnerCode, $params = false)
    {
        $order->setScenario(Order::SCENARIO_UPDATE_PARTNER_CODE);
        $order->is_done_telesale_process = Order::BOOL_YES;

        if ($params) {
            $params['partner_code'] = json_encode($params['partner_code']);
            $order->actionParams = $params;
        }

        return self::_save($order);
    }

    public static function updatePaymentMethod(Order $order, $method, $params = false)
    {
        $oldPaymentMethod = $order->getPaymentMethod();
        $order->payment_method = $method;
        $newPaymentMethod = $order->getPaymentMethod();
        
        if ($newPaymentMethod->isCod) {
            if (!in_array($order->status, [
                Order::ORDER_STATUS_DELIVERING,
                Order::ORDER_STATUS_PENDING_CONTACT,
                Order::ORDER_STATUS_RETURN,
                Order::ORDER_STATUS_WAITING_RETURN,
            ])) {
                $order->status = Order::ORDER_STATUS_PENDING_CONTACT;
            }
        } else {
            if ($oldPaymentMethod != null && $oldPaymentMethod->isCod && !empty($order->shipping_fee)) {
                $order->total -= $order->shipping_fee;
                $order->shipping_fee = 0;
            }
            if (in_array($order->status, [
                Order::ORDER_STATUS_DELIVERING,
                Order::ORDER_STATUS_PENDING_CONTACT,
                Order::ORDER_STATUS_RETURN,
                Order::ORDER_STATUS_WAITING_RETURN,
                Order::ORDER_STATUS_NEW,
            ])) {
                $order->status = Order::ORDER_STATUS_PENDING_PAYMENT;
            }
        }
        $order->payment_fee = $order->calPaymentFee();
        $order->setScenario(Order::SCENARIO_UPDATE_PAYMENT_METHOD);

        if ($params) {
            $params['note'] = $params['note'];
            $order->actionParams = $params;
        }

        return self::_save($order);
    }

    public static function endCall($orderId, $callStatus, $callMeta = [])
    {
        $callMeta['duration'] = time() - $callMeta['start'];
        $order = self::_getOrder(Order::SCENARIO_CALL, $orderId);

        return $order->saveCall($callStatus, $callMeta);
    }

    public static function note($orderId, $callStatus, $callMeta = [])
    {
        $order = self::_getOrder(Order::SCENARIO_NOTE, $orderId);

        return $order->saveCall($callStatus, $callMeta);
    }

    /**
     * @param string activation_code
     * @desc get order with activation code fields
     * @result object
     */
    public static function getOrderByActivationCode($activation_code, $status)
    {
        $order = Order::find()->where(['activation_code' => $activation_code, 'status' => $status])->one();
        return $order;
    }

    public static function detectOperator($order)
    {
        // get operators in current timeslot
        return TimeSlot::getOperatorsInShift(RoleHelper::ROLE_TELESALE);
    }

    public static function calculateRefund($amount)
    {
        //return round($amount / 10000) * 10000;
        if($amount < 5000){
            return 0;
        }
        return $amount;
    }

    public static function getStatusLabelCss()
    {
        return [
            Order::ORDER_STATUS_CANCELLED => 'label-default',
            // temporary on admin only
            Order::ORDER_STATUS_DRAFT => 'label-default',
            // newly created order (shopping cart)
            Order::ORDER_STATUS_NEW => 'label-warning',
            // when user decide to choose an online payment method,
            // wait for response from payment gateway
            Order::ORDER_STATUS_PENDING_PAYMENT => 'label-info', // Telesale only
            Order::ORDER_STATUS_PENDING_CONTACT => 'label-warning', // Telesale only
            Order::ORDER_STATUS_IN_COMPLETE => 'label-warning', // Telesale only
            // sent to logistic service already
            Order::ORDER_STATUS_DELIVERING => 'label-info',
            // payment completed & service delivered
            Order::ORDER_STATUS_COMPLETE => 'label-success',
            Order::ORDER_STATUS_REQUESTING_PAYMENT => 'label-default',
            Order::ORDER_STATUS_PAYMENT_FAILED => 'label-default',
            // cod return status
            Order::ORDER_STATUS_WAITING_RETURN => 'label-default',
            Order::ORDER_STATUS_RETURN => 'label-default',
        ];
    }

    public static function activate($orderId, $actionParams = false)
    {
        $order = self::_getOrder(Order::SCENARIO_ACTIVATE, $orderId, [
            Order::ORDER_STATUS_COMPLETE,
            Order::ORDER_STATUS_DELIVERING, // ghn is delivering but not call status to update order complete
        ]);

        $order->is_activated = Order::BOOL_YES;
        $order->activation_date = time();

        if ($actionParams) {
            $order->actionParams = $actionParams;
        }

        return self::_save($order);
    }
}
