<?php

namespace kyna\order\models\actions;

/**
 * This is the model class for table "order_action_meta".
 *
 * @property int $id
 * @property int $action_id
 * @property string $key
 * @property string $value
 * @property string $data_type
 */
class OrderActionMeta extends \kyna\base\ActiveRecord
{
    public static $readOnMaster = true;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'order_action_meta';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['order_action_id'], 'required'],
            [['order_action_id'], 'integer'],
            [['value'], 'string'],
            [['key'], 'string', 'max' => 50],
            [['data_type'], 'string', 'max' => 10],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order_action_id' => 'Action ID',
            'key' => 'Key',
            'value' => 'Value',
            'data_type' => 'Data Type',
        ];
    }
}
