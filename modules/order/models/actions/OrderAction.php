<?php

namespace kyna\order\models\actions;

use kyna\base\models\Action;

class OrderAction extends Action
{

    const CALL_STATUS_UNTOUCHABLE = 1;
    const CALL_STATUS_THINKING = 2;
    const CALL_STATUS_CONFIRM = 3;
    const CALL_STATUS_ABANDONED = 4;
    const CALL_STATUS_RETURN = 5;
    const CALL_STATUS_PAYMENT_CHANGE = 6;
    const CALL_STATUS_OTHER = 0;

    public static $readOnMaster = true;

    protected function enableMeta()
    {
        return [
            'order_action',
            '\\kyna\\order\\models\\actions\\OrderActionMeta'
        ];
    }

    public function metaKeys()
    {
        return [
            'status',
            'start',
            'duration',
            'note',
            'checksum',
            'payment_method',
            'activation_code',
            'confirm',
            'contact_name',
            'phone_number',
            'location_id',
            'street_address',
            'partner_code'
        ];
    }

    public static function callStatuses()
    {
        return [
            self::CALL_STATUS_UNTOUCHABLE => 'Chưa liên hệ được',
            self::CALL_STATUS_THINKING => 'Đang suy nghĩ',
            self::CALL_STATUS_CONFIRM => 'Xác nhận đăng ký',
            self::CALL_STATUS_ABANDONED => 'Từ chối',
            self::CALL_STATUS_RETURN => 'Không giao được',
            self::CALL_STATUS_PAYMENT_CHANGE => 'Chuyển cách thanh toán',
            self::CALL_STATUS_OTHER => 'Khác',
        ];
    }

    public static function tableName()
    {
        return 'order_actions';
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderActionMeta()
    {
        return $this->hasMany(OrderActionMeta::className(), ['order_action_id' => 'id']);
    }
}
