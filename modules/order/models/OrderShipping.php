<?php

namespace kyna\order\models;

use kyna\base\models\Location;
use common\helpers\StringHelper;
use kyna\base\models\Vendor;

/**
 * This is the model class for table "order_shipping".
 *
 * @property int $id
 * @property int $order_id
 * @property int $method_id
 * @property string $contact_name
 * @property string $phone_number
 * @property string $street_address
 * @property int $location_id
 * @property string $shipping_code
 * @property float $fee
 * @property string $note Ghi chu
 * @property int $status
 * @property int $created_time
 * @property int $updated_time
 */
class OrderShipping extends \kyna\base\models\BaseAddress
{
    public static $readOnMaster = true;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'order_shipping';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['order_id', 'method_id', 'location_id', 'status', 'created_time', 'updated_time', 'vendor_id'], 'integer'],
            [['fee'], 'number'],
            [['contact_name'], 'string', 'max' => 255],
            [['phone_number'], 'string', 'max' => 15],
            [['street_address'], 'string', 'max' => 255],
            [['shipping_code'], 'string', 'max' => 45],
            ['note', 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order_id' => 'Order ID',
            'method_id' => 'Mã phương thức',
            'contact_name' => 'Người nhận',
            'phone_number' => 'Số điện thoại',
            'street_address' => 'Địa chỉ',
            'location_id' => 'Quận/Huyện',
            'shipping_code' => 'Mã vận đơn',
            'fee' => 'Phí vận chuyển',
            'status' => 'Trạng thái',
            'created_time' => 'Created Time',
            'updated_time' => 'Updated Time',
            'vendor_id' => 'Đơn vị vận chuyển',
            'note' => 'Ghi chú'
        ];
    }

    public function getFullAddressText()
    {
        $district = !empty($this->location) ? ', ' . $this->location->name : '';
        $city = (!empty($this->location) && !empty($this->location->parent)) ? ', ' . $this->location->parent->name : '';
        return $this->street_address . $district . $city;
    }
    
    public function getLocation() {
        return $this->hasOne(Location::className(), ['id' => 'location_id']);
    }

    public function getVendor() {
        return $this->hasOne(Vendor::className(), ['id' => 'vendor_id']);
    }
}
