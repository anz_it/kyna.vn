<?php

namespace kyna\order\models;

interface OrderInterface
{

    /**
     * Function createOrder(SCENARIO_CREATE_BACKEND)
     * Set order's status to [ORDER_STATUS_PENDING_PAYMENT] if payment method is auto
     * Set order's status to [ORDER_STATUS_PENDING_CONTACT] if payment method is cod
     * Fire [EVENT_ORDER_CREATED]
     */
    const SCENARIO_CREATE_BACKEND = 'create-backend';

    const SCENARIO_UPDATE_BACKEND = 'update-backend';

    /**
     * Function createOrder(SCENARIO_CREATE_VIA_FORM)
     * Attach [SCENARIO_CREATE_BACKEND]
     * Attach [SCENARIO_SEND_TO_SHIPPING]
     */
    const SCENARIO_CREATE_VIA_FORM = 'create-via-form';

    /**
     * Function sendToShipping()
     * Set order's status to [ORDER_STATUS_DELIVERING]
     * Fire [EVENT_ORDER_SENT_TO_SHIPPING]
     */
    const SCENARIO_SEND_TO_SHIPPING = 'send-to-shipping';
    
    const SCENARIO_UPDATE_SHIPPING_ADDRESS = 'update-shipping';

    /**
     * Function cancelOrder()
     * Set order's status to [ORDER_STATUS_CANCELLED]
     * Fire [EVENT_ORDER_CANCELLED]
     */
    const SCENARIO_CANCEL = 'cancel';

    const SCENARIO_CANCEL_SHIPPING = 'cancel-shipping';
    
    const SCENARIO_MOVE_TO_CS = 'move-to-cs';

    /**
     * Function codReturn()
     * Set order's status to [ORDER_STATUS_RETURN]
     * Fire [EVENT_ORDER_RETURN]
     */
    const SCENARIO_RETURN = 'cod-return';

    /**
     * Function codWatingReturn()
     * Set order's status to [ORDER_STATUS_WAITING_RETURN]
     * Fire [EVENT_ORDER_WAITING_RETURN]
     */
    const SCENARIO_WAITING_RETURN = 'cod-waiting-return';

    /**
     * Function createOrder(SCENARIO_CREATE_FRONTEND)
     * Set order's status to [ORDER_STATUS_NEW]
     * Fire [EVENT_ORDER_CREATED]
     */
    const SCENARIO_CREATE_FRONTEND = 'create-frontend';

    /**
     * Function updateOrder()
     * Fire [EVENT_ORDER_UPDATED]
     */
    const SCENARIO_UPDATE = 'update';

    /**
     * Function placeOrder()
     * Set order's status to [ORDER_STATUS_PENDING_PAYMENT] if payment method is auto
     * Set order's status to [ORDER_STATUS_PENDING_CONTACT] if payment method is cod
     * Fire [EVENT_ORDER_PLACED]
     */
    const SCENARIO_PLACE_ORDER = 'place-order';

    /**
     * Function getPaid()
     * Set order's status to [EVENT_ORDER_GET_PAID]
     * Fire [EVENT_ORDER_PAID]
     */
    const SCENARIO_GET_PAID = 'get-paid';

    const SCENARIO_REQUEST_PAYMENT = 'request-payment';

    const SCENARIO_PAYMENT_FAILED = 'payment-failed';

    /**
     * Function complete()
     * Set order's status to [ORDER_STATUS_COMPLETED]
     * Fire [EVENT_ORDER_COMPLETED]
     */
    const SCENARIO_COMPLETE = 'complete';

    const SCENARIO_COMPLETE_COD = 'complete-cod';

    const SCENARIO_IN_COMPLETE = 'in-complete';

    const SCENARIO_ACTIVATE = 'activate';

    /**
     * Function call()
     * Fire [EVENT_ORDER_CALL_ENDED]
     */
    const SCENARIO_CALL = 'call';
    
    const SCENARIO_NOTE = 'note';

    const SCENARIO_APPLY_PROMOTION = 'apply-promotion';

    const SCENARIO_REMOVE_PROMOTION = 'remove-promotion';
    
    const SCENARIO_LANDING_PAGE = 'landing-page';

    const SCENARIO_CREATE_RETAILER = 'create-retailer';

    const SCENARIO_UPDATE_PARTNER_CODE = 'update-partner-code';

    const SCENARIO_UPDATE_PAYMENT_METHOD = 'update-payment-method';

    // removed, invalid or expired order
    const ORDER_STATUS_CANCELLED = -1;
    // currently unused
    const ORDER_STATUS_DRAFT = 0;
    // begin checkout, not select payment method yet
    const ORDER_STATUS_NEW = 1;
    // when user decide to choose an online payment method,
    // wait for response from payment gateway
    const ORDER_STATUS_PENDING_PAYMENT = 2; // Telesale only
    // order is sent to Cod team
    const ORDER_STATUS_PENDING_CONTACT = 3; // Cod only
    // sent to logistic service already
    const ORDER_STATUS_DELIVERING = 4;
    // payment completed & service delivered
    const ORDER_STATUS_COMPLETE = 5;
    //order payment incomplete
    const ORDER_STATUS_IN_COMPLETE = 6;
    // cod order is being return to kyna
    const ORDER_STATUS_WAITING_RETURN = 7;
    // cod order is return to kyna
    const ORDER_STATUS_RETURN = 8;
    // need to payment
    const ORDER_STATUS_REQUESTING_PAYMENT = 9;
    // payment failed
    const ORDER_STATUS_PAYMENT_FAILED = 10;

    // fire after an order is created
    const EVENT_ORDER_CREATED = 'create';
    // fire after an order update its details
    const EVENT_ORDER_UPDATED = 'update';
    // fire after order get shipping transaction_id
    const EVENT_ORDER_SENT_TO_SHIPPING = 'send-to-shipping';
    
    const EVENT_ORDER_UPDATED_SHIPPING_ADDRESS = 'update-shipping-address';
    const EVENT_ORDER_UPDATED_PARTNER_CODE = 'update-partner-code';
    const EVENT_ORDER_UPDATED_PAYMENT_METHOD = 'update-payment-method';

    // fire after operator made a call to customer to confirm the order
    const EVENT_ORDER_CALL_ENDED = 'call';
    // fire after operator added note to the order
    const EVENT_ORDER_ADDED_NOTE = 'note';
    // fire after customer decide to checkout
    const EVENT_ORDER_PLACED = 'place-order';
    // fire after order is_paid is set to true
    const EVENT_ORDER_GET_PAID = 'paid';
    // fire after all order's items were delivered/activated
    const EVENT_ORDER_COMPLETED = 'complete';
    // fire after all order's items were delivered/activated COD
    const EVENT_ORDER_COMPLETED_COD = 'complete-cod';
    // fire after all order's items were delivered/activated
    const EVENT_ORDER_CANCELLED = 'cancel';
    const EVENT_ORDER_CANCELLED_SHIPPING = 'cancel-shipping';
    // fire after order return
    const EVENT_ORDER_RETURN = 'cod-return';
    // fire after order waiting return
    const EVENT_WAITING_RETURN = 'cod-waiting-return';
    // Before send to shipping
    const EVENT_ORDER_BEFORE_SENT_TO_SHIPPING = 'before-send-to-shipping';
    //Event save promotion code
    const EVENT_ORDER_APPLY_PROMOTION = 'event-apply-promotion';
    //Event remove promotion code
    const EVENT_ORDER_REMOVE_PROMOTION = 'event-remove-promotion';
    //Event Order incomplete
    const EVENT_ORDER_IN_COMPLETE = 'in_complete';
    // Event when order has been created at backend
    const EVENT_CREATED_BACKEND = 'created_backend';
    // Event when order has been updated at backend
    const EVENT_UPDATED_BACKEND = 'updated_backend';
    // Event when order has been activated
    const EVENT_ACTIVATED = 'activated';
    // Event when order has been activated
    const EVENT_REQUESTED_PAYMENT = 'requested-payment';
    // Event when order has been activated
    const EVENT_PAYMENT_FAILED = 'payment-failed';
    // Event create order from landing page
    const EVENT_LANDING_PAGE = 'landing-page';
    // Event when order has been created at backend
    const EVENT_CREATED_RETAILER = 'created_retailer';

    //Event when promo code is apply for order created
    const EVENT_ORDER_APPLY_PROMO = 'apply-promo';
    
    const POINT_OF_SALE_LANDING_PAGE = 'landing-page';
    const POINT_OF_SALE_SHOPPING_CART = 'shopping-cart';

    const PAYMENT_METHOD_NAMESPACE = 'kyna\\payment\\lib\\';

}
