<?php

namespace kyna\order\models;

use app\modules\cart\models\Product;
use kyna\course\models\Course;
use kyna\commission\models\CommissionIncome;
use kyna\partner\models\Code;

/**
 * This is the model class for table "order_details".
 *
 * @property int $id
 * @property int $order_id
 * @property int $course_id
* @property int $course_combo_id
 * @property string $course_meta
 * @property float $unit_price
 * @property float $discount_amount
 * @property int $quantity
 * @property bool $is_delivered
 * @property string $activation_code
 */
class OrderDetails extends \kyna\base\ActiveRecord
{
    const SCENARIO_UPDATE_PARTNER = 'update-partner';

    public static $readOnMaster = true;

    public $isGift;

    public static function courseData()
    {
        return ['name', 'image_url', 'slug', 'combo_name'];
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'order_details';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['order_id', 'course_id'], 'required'],
            [['order_id', 'course_id', 'course_combo_id', 'quantity'], 'integer'],
            [['course_meta', 'activation_code', 'promo_code'], 'string'],
            [['unit_price', 'discount_amount', 'combo_discount_amount'], 'number'],
            [['is_delivered'], 'boolean'],
            ['activation_code', 'validSerial', 'skipOnEmpty' => false, 'on' => [self::SCENARIO_UPDATE_PARTNER]]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order_id' => 'Order ID',
            'course_id' => 'Course ID',
            'course_combo_id' => 'Combo',
            'course_meta' => 'Course Meta',
            'unit_price' => 'Unit Price',
            'discount_amount' => 'Discount Amount',
            'quantity' => 'Quantity',
            'is_delivered' => 'Is Delivered',
            'activation_code' => 'Mã kích hoạt'
        ];
    }

    public static function saveMany($orderId, $details)
    {
        $transaction = \Yii::$app->db->beginTransaction();
        try {
            OrderDetails::deleteAll(['order_id' => $orderId]);

            foreach ($details as &$detail) {
                $detail['order_id'] = $orderId;
            }

            \Yii::$app->db->createCommand()->batchInsert(self::tableName(), [
                'course_id',
                'course_combo_id',
                'course_meta',
                'unit_price',
                'discount_amount',
                'is_delivered',
                'activation_code',
                'promo_code',
                'combo_discount_amount',
                'order_id',

                    ], $details)->execute();
            $transaction->commit();

            return true;
        } catch (\Exception $e) {
            $transaction->rollback();
            echo "<pre>";
            var_dump($e->getMessage(), '123');
            return false;
        }
    }

    public static function extractCourseMeta($course)
    {
        $data = false;
        foreach (self::courseData() as $courseField) {
            if (isset($course->$courseField)) {
                $data[$courseField] = $course->$courseField;
            }
        }
        return json_encode($data);
    }

    public static function calculateDiscount($course)
    {
        return !empty($course->discount_amount) ? $course->discount_amount : (!empty($course->discount_percent) ? $course->price * $course->discount_percent / 100 : 0);
    }

    public function getActivateStatus()
    {
        //$course = UserCourse::find();
    }

    public function getCourseInfo()
    {
        return json_decode($this->course_meta);
    }

    public function getCombo()
    {
        return $this->hasOne(Course::className(), ['id' => 'course_combo_id']);
    }

    public function getCourse()
    {
        return $this->hasOne(Course::className(), ['id' => 'course_id']);
    }

    public function getSubTotal()
    {
        return $this->unit_price * $this->quantity;
    }

    public function getItemTotal()
    {
        return ($this->unit_price - $this->discount_amount) * $this->quantity;
    }
    public function getOriginTotal(){
        return ($this->unit_price - $this->getOriginalDiscountAmount()) * $this->quantity;
    }
    
    public function getDiscountAmount()
    {
        return $this->discount_amount;
    }

    public function getOriginalDiscountAmount()
    {
        return $this->course->discountAmount;
    }

    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }
    
    public function getRealIncome()
    {
        $order = $this->order;
        
        if ($order->status === Order::ORDER_STATUS_COMPLETE) {
            $commissionIncome = CommissionIncome::find()->where([
                'order_id' => $this->order_id,
                'course_id' => $this->course_id
            ])->one();

            if (!is_null($commissionIncome)) {
                return $commissionIncome->total_amount;
            }
        }
        
        if ($order->total == 0) {
            return 0;
        }
        
        $orderDiscount = $order->totalDiscount - $order->totalCourseDiscount;
        // get percent of course in order
        $percentOfCourseInOrder = 0;
        if ($order->total > 0) {
            $percentOfCourseInOrder = $this->itemTotal * 100 / ($order->subTotal - $order->totalCourseDiscount);
        }
        
        $realIncomeOfCourse = $this->itemTotal - (($order->shippingAmount + $order->paymentFee) * $percentOfCourseInOrder / 100);
        
        return $realIncomeOfCourse;
    }
    
    public function getAffiliateCommissionPercent()
    {
        $order = $this->order;
        
        if ($order->is_activated == Order::BOOL_YES) {
            $commissionIncome = CommissionIncome::find()->where([
                'order_id' => $this->order_id,
                'course_id' => $this->course_id
            ])->one();

            if (!is_null($commissionIncome)) {
                return $commissionIncome->affiliate_commission_percent;
            }
        }
        
        if (!empty($order->affiliate_id)) {
            $originalAff = $order->originalAffiliateUser;
            if (!is_null($originalAff)) {
                return $originalAff->getCommissionPercent($this->course_id);
            }
        }
        
        return null;
    }

    public function getPartnerCode()
    {
        return $this->hasOne(Code::className(), ['serial' => 'activation_code']);
    }

    public function validSerial($attribute, $params)
    {
        $course = $this->course;
        if ($course && $course->isPartner) {
            if (empty($this->activation_code)) {
                $this->addError($attribute, 'Serial Code không được để trống');
            } else {
                $code = Code::find()->where([
                    'serial' => $this->activation_code,
                    'status' => Code::CODE_STATUS_IN_STORE,
                    'partner_id' => $course->partner->id,
                ])->exists();
                if (!$code) {
                    $this->addError($attribute, 'Serial Code không hợp lệ');
                }
            }
        }
    }

    public function getOriginalProduct()
    {
        return Product::findOne($this->course_id);
    }



    public function getCostText()
    {
        $costText = '';
        $formatter = \Yii::$app->formatter;
        $cost = $this->getOriginTotal();
        $course = $this->course;
        if ($course->getIsCampaign11()) {
            if ($this->isGift) {
                $costText = 'Khóa tặng';
            } else {
                $costText = $formatter->asCurrency($course->price);
            }
        } elseif (!empty($cost)) {
            $costText = $formatter->asCurrency($cost);
        } else {
            $costText = 'Miễn phí';
        }
        return $costText;
    }
    public function calcRatioVoucherDiscount($details, $discount_on_order = 0){
        $total = 0;
        $total_on_discount = 0;
        foreach ($details as $detail){
            $course_sell_price =  $detail->unit_price - $detail->discount_amount;
            if(!empty($detail->promo_code)){
                $total_on_discount += $course_sell_price;
            }
            $total += $course_sell_price;
        }

        $total = $total - $discount_on_order;
        //Free courses
        if($total == 0){
            return 0;
        }
        if(!empty($this->promo_code)){
            $discount_voucher_remain = $total_on_discount - $discount_on_order;
           if( $discount_voucher_remain >= 0)
           {
               if($total_on_discount == 0){
                   return 0;
               }
               $avg_price = $discount_voucher_remain * ($this->unit_price - $this->discount_amount)/$total_on_discount;
               return $avg_price / $total;
           }
           else
               return 0;
        }

         return ($this->unit_price - $this->discount_amount) / $total;
    }
    public function calcPercentCourseInVoucher($details){
        $total_on_discount = 0;
        foreach ($details as $detail){
            $course_sell_price =  $detail->unit_price - $detail->discount_amount;
            if(!empty($detail->promo_code)){
                $total_on_discount += $course_sell_price;
            }
        }
        if($total_on_discount == 0){
            return 0;
        }
        if(!empty($this->promo_code)){
            return  ($this->unit_price - $this->discount_amount) / $total_on_discount;
        }

        return 0;
    }
}
