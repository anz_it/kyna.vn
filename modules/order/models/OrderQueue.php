<?php

namespace kyna\order\models;
use kyna\user\models\TimeSlot;

/**
 * This is the model class for table "{{%order_divisions}}".
 *
 * @property int $id
 * @property int $order_id
 * @property int $user_id
 * @property string $type
 */
class OrderQueue extends \kyna\base\ActiveRecord
{
    public static $readOnMaster = true;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%order_divisions}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['order_id', 'user_id', 'type'], 'required'],
            [['order_id', 'user_id'], 'integer'],
            [['type'], 'string', 'max' => 20],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order_id' => 'Order ID',
            'user_id' => 'User ID',
            'type' => 'Type',
        ];
    }

    public static function pushToQueue($order) {
        //$order = Order::findOne($orderId);
        if (empty($order)) {
            return false;
        }
        if (self::find()->where(['order_id' => $order->id])->exists())  {
            // do nothing
            return true;
        }

        if ($order->isCod) {
            $type = 'CodExecutive';
        }
        else {
            $type = 'CustomerService';
        }

        $operatorIds = TimeSlot::getOperatorsInShift($type);

        if (empty($operatorIds)) {
            return false;
        }

        $queue = new static();
        $queue->attributes = [
            'order_id' => $order->id,
            'user_id' => $operatorIds[rand(0, sizeof($operatorIds) - 1)],
            'type' => $type
        ];
        // assign order to COD members
        return $queue->save();

        return false;
    }

    public static function getOrderIds($userId) {
        return self::find()->where(['user_id' => $userId])->select('order_id')->column();
    }
}
