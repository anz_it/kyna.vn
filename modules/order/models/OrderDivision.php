<?php

namespace kyna\order\models;

use Yii;

/**
 * This is the model class for table "{{%order_divisions}}".
 *
 * @property integer $id
 * @property integer $order_id
 * @property integer $user_id
 * @property string $type
 */
class OrderDivision extends \kyna\base\ActiveRecord
{
    public static $readOnMaster = true;

    const TYPE_COD = 'cod';
    const TYPE_TELESALE = 'telesale';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%order_divisions}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_id', 'user_id', 'type'], 'required'],
            [['order_id', 'user_id'], 'integer'],
            [['type'], 'string', 'max' => 20],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order_id' => 'Order ID',
            'user_id' => 'User ID',
            'type' => 'Type',
        ];
    }

    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }

}
