<?php

namespace kyna\order\lib;

abstract class BaseShipping extends \kyna\servicecaller\BaseCaller
{
    abstract public function calculateFee($from, $to, $serviceId);
    abstract public function createOrder($from, $to, $orderShipping, $amount, $serviceId);
    abstract public function getOrder($shippingCode);
    abstract public function cancelOrder($shippingCode);
}
