<?php

use yii\db\Migration;

class m180320_035700_create_home_cms_structure extends Migration
{
    const HOME_TEACHER_TABLE = '{{%home_teacher}}';

    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        // create table banner
        $this->createTable(self::HOME_TEACHER_TABLE, [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->notNull()->comment('Tên giảng viên'),
            'title' => $this->string(255)->notNull()->comment('Chức danh của giảng viên'),
            'slug' => $this->string(255),
            'image_url' => $this->string(),
            'status' => $this->integer(1)->notNull()->defaultValue(0)->comment('0-hide,1-show'),
            'created_time' => $this->integer(),
            'updated_time' => $this->integer(),
        ], $tableOptions);
    }

    public function safeDown()
    {
        $this->dropTable(self::HOME_TEACHER_TABLE);
    }
}
