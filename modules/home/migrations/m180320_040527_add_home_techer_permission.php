<?php

use yii\db\Migration;

class m180320_040527_add_home_techer_permission extends Migration
{
    public function safeUp()
    {
        $auth = Yii::$app->authManager;

        //=============================================================================
        // add "Home.Teacher.View" permission
        $viewTeacher = $auth->createPermission('Home.Teacher.View');
        $viewTeacher->description = 'Home.Teacher View';
        $auth->add($viewTeacher);

        // add "Home.Teacher.Create" permission
        $createTeacher = $auth->createPermission('Home.Teacher.Create');
        $createTeacher->description = 'Home.Teacher Create';
        $auth->add($createTeacher);
        $auth->addChild($createTeacher, $viewTeacher);

        // add "Home.Teacher.Update" permission
        $updateTeacher = $auth->createPermission('Home.Teacher.Update');
        $updateTeacher->description = 'Home.Teacher Update';
        $auth->add($updateTeacher);
        $auth->addChild($updateTeacher, $viewTeacher);

        // add "Home.Teacher.Delete" permission
        $deleteTeacher = $auth->createPermission('Home.Teacher.Delete');
        $deleteTeacher->description = 'Home.Teacher Delete';
        $auth->add($deleteTeacher);
        $auth->addChild($deleteTeacher, $viewTeacher);
        $auth->addChild($deleteTeacher, $updateTeacher);

        // add "Home.Teacher.All" permission
        $allTeacher = $auth->createPermission('Home.Teacher.All');
        $allTeacher->description = 'Home.Teacher All';
        $auth->add($allTeacher);
        $auth->addChild($allTeacher, $viewTeacher);
        $auth->addChild($allTeacher, $createTeacher);
        $auth->addChild($allTeacher, $updateTeacher);
        $auth->addChild($allTeacher, $deleteTeacher);
        //=============================================================================

        // add "HomeAdmin" role
        $homeAdmin = $auth->createRole('HomeAdmin');
        $homeAdmin->description = 'The admin of a Home module';
        $auth->add($homeAdmin);
        $auth->addChild($homeAdmin, $allTeacher);

        // add new role for Admin role
        $admin = $auth->getRole('Admin');
        $auth->addChild($admin, $homeAdmin);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $auth = Yii::$app->authManager;

        $homeAdmin = $auth->getRole('HomeAdmin');
        $admin = $auth->getRole('Admin');
        $auth->removeChild($admin, $homeAdmin);
        $auth->remove($homeAdmin);

        $allTeacher = $auth->getPermission('Home.Teacher.All');
        $deleteTeacher = $auth->getPermission('Home.Teacher.Delete');
        $updateTeacher = $auth->getPermission('Home.Teacher.Update');
        $createTeacher = $auth->getPermission('Home.Teacher.Create');
        $viewTeacher = $auth->getPermission('Home.Teacher.View');
        $auth->remove($allTeacher);
        $auth->remove($deleteTeacher);
        $auth->remove($updateTeacher);
        $auth->remove($createTeacher);
        $auth->remove($viewTeacher);
    }
}
