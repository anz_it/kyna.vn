<?php

namespace kyna\home\controllers;

use kyna\base\ActiveRecord;
use Yii;
use kyna\home\models\HomeTeacher;
use kyna\home\models\HomeTeacherSearch;
use yii\filters\AccessControl;
use kyna\base\controllers\admin\BaseController as Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TeacherController implements the CRUD actions for HomeTeacher model.
 */
class TeacherController extends Controller
{
    public $modelClass = 'kyna\home\models\HomeTeacher';
    public $modelSearchClass = 'kyna\home\models\HomeTeacherSearch';
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'view'],
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->user->can('Home.Teacher.View');
                        }
                    ],
                    [
                        'allow' => true,
                        'actions' => ['create'],
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->user->can('Home.Teacher.Create');
                        }
                    ],
                    [
                        'allow' => true,
                        'actions' => ['update', 'change-status', 'sorting'],
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->user->can('Home.Teacher.Update');
                        }
                    ],
                    [
                        'allow' => true,
                        'actions' => ['delete'],
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->user->can('Home.Teacher.Delete');
                        }
                    ],
                ],
            ],
        ];
    }

    /**
     * Creates a new Banner model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new HomeTeacher();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if ($image_url = $this->uploadFile($model, 'image_url')) {
                $model->image_url = $image_url;
                $model->save(false);
            }
            Yii::$app->session->setFlash('success', Yii::t('app', 'Tạo mới thành công.'));
            return $this->redirect($this->getRedirectPage('create', $model));
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Banner model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $image_url = $this->uploadFile($model, 'image_url');
            if (!empty($image_url)) {
                $model->image_url = $image_url;
                $model->save(false);
            }
            Yii::$app->session->setFlash('success', Yii::t('app', 'Cập nhật thành công.'));
            return $this->redirect($this->getRedirectPage('update', $model));
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    protected function getRedirectPage($action, $model = null)
    {
        switch ($action) {
            case 'update':
                return ['update', 'id' => $model->id];
                break;
            case 'create':
                return ['update', 'id' => $model->id];
                break;
            default:
                return parent::getRedirectPage($action, $model);
        }
    }

    /**
     * @return array
     */
    public function actions()
    {
        return [
            'sorting' => [
                'class' => \kotchuprik\sortable\actions\Sorting::className(),
                'query' => \kyna\home\models\HomeTeacher::find(),
            ],
        ];
    }
}
