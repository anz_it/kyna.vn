<?php

namespace kyna\home;

use yii\filters\AccessControl;

/*
 * This is home module.
 */
class HomeModule extends \yii\base\Module
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => false,
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public $layout = '@app/views/layouts/lte';

}
