<?php

namespace kyna\home\models;

use common\helpers\CDNHelper;
use common\widgets\upload\UploadRequiredValidator;
use kotchuprik\sortable\behaviors\Sortable;
use Yii;
use yii\web\UploadedFile;

/**
 * This is the model class for table "{{%home_teacher}}".
 *
 * @property integer $id
 * @property string $name
 * @property string $title
 * @property string $slug
 * @property string $image_url
 * @property integer $status
 * @property integer $created_time
 * @property integer $updated_time
 * @property integer $order
 */
class HomeTeacher extends \kyna\base\ActiveRecord
{
    public $imageSize = [
        'cover' => [
            CDNHelper::IMG_SIZE_ORIGINAL,
            CDNHelper::IMG_SIZE_HOME_TEACHER
        ],
        'contain' => [],
        'crop' => []
    ];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%home_teacher}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'title'], 'required'],
            [['status', 'created_time', 'updated_time', 'order'], 'integer'],
            [['name', 'title', 'slug'], 'string', 'max' => 255],
            [['image_url'], 'validateImage', 'skipOnEmpty' => false],
        ];
    }

    public function validateImage($attribute, $params)
    {
        $image = UploadedFile::getInstance($this, $attribute);
        if (!$image && empty($this->oldAttributes[$attribute])) {
            $this->addError($attribute,Yii::t('app', 'Hình giảng viên không được để trống'));
            return false;
        }
        $oldAttributes = $this->oldAttributes;
        if (isset($oldAttributes[$attribute])) {
            $this->{$attribute} = $oldAttributes[$attribute];
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Tên giảng viên'),
            'title' => Yii::t('app', 'Chức danh'),
            'slug' => Yii::t('app', 'Slug'),
            'image_url' => Yii::t('app', 'Hình ảnh'),
            'status' => Yii::t('app', 'Trạng thái'),
            'created_time' => Yii::t('app', 'Ngày tạo'),
            'updated_time' => Yii::t('app', 'Ngày cập nhật'),
            'order' => Yii::t('app', 'Order'),
        ];
    }

    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['sortable'] = [
            'class' => Sortable::className(),
            'query' => self::find(),
        ];

        return $behaviors;
    }

    public static function topTeachers($limit)
    {
        return self::find()
            ->where(['status' => self::STATUS_ACTIVE])
            ->limit($limit)
            ->orderBy('order ASC')
            ->all();
    }
}
