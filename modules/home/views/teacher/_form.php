<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\widgets\BtnSaveWidget;
use common\widgets\Select2;
use kyna\home\models\HomeTeacher;
use common\widgets\upload\Upload;

/* @var $this yii\web\View */
/* @var $model kyna\home\models\HomeTeacher */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="teacher-form">

    <?php
    $form = ActiveForm::begin([
        'id' => 'teacher-form',
        'validateOnBlur' => false,
        'options' => ['enctype' => 'multipart/form-data']
    ])
    ?>

    <div class="row">
        <?= $form->field($model, 'name', ['options' => ['class' => 'col-xs-6']])->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'slug', ['options' => ['class' => 'col-xs-6']])->textInput(['maxlength' => true]) ?>
    </div>

    <div class="row">
        <?= $form->field($model, 'title', ['options' => ['class' => 'col-xs-6']])->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'image_url', ['options' => ['class' => 'col-xs-6']])->widget(Upload::className(), ['display' => 'image']) ?>
    </div>

    <div class="row">
        <?= $form->field($model, 'status', ['options' => ['class' => 'col-xs-6']])->widget(Select2::classname(), [
            'data' => HomeTeacher::listStatus(),
            'hideSearch' => true,
            'theme' => Select2::THEME_DEFAULT,
            'options' => [
                'placeholder' => Yii::t('app', '-- Chọn --'),
            ],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]) ?>
    </div>

    <div class="form-group pull-right">
        <?= Html::a(Yii::t('app', 'Hủy'), ['index'], ['class' => 'btn btn-default']) ?>

        <?= BtnSaveWidget::widget([
            'model' => $model
        ]) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>