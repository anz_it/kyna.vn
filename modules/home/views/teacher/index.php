<?php

use yii\helpers\Html;
use common\components\GridView;
use yii\widgets\Pjax;
use common\widgets\BtnAddWidget;
use kyna\home\models\HomeTeacher;
use common\components\SortableColumn;
use common\helpers\CDNHelper;

/* @var $this yii\web\View */
/* @var $searchModel kyna\home\models\HomeTeacherSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Home Teacher');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="">
    <div class="box">
        <?php if (Yii::$app->user->can('Home.Teacher.Create')) : ?>
            <div class="box-header">
                <?= BtnAddWidget::widget() ?>
            </div>
        <?php endif; ?>

        <div class="box-body">
            <?php Pjax::begin(['timeout' => Yii::$app->params['timeOutPjax'], 'id' => 'idPjaxReload']) ?>
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'rowOptions' => function ($model, $key, $index, $grid) {
                    return ['data-sortable-id' => $model->id];
                },
                'options' => [
                    'data' => [
                        'sortable-widget' => 1,
                        'sortable-url' => \yii\helpers\Url::toRoute(['sorting']),
                    ]
                ],
                'columns' => array_merge(
                    [
                        [
                            'class' => SortableColumn::className(),
                            'header' => Yii::t('app', 'Order')
                        ],
                    ],
                    [
                        'name',
                        'title',
                        'slug',
                        [
                            'attribute' => 'image_url',
                            'format' => 'html',
                            'value' => function ($model) {
                                return CDNHelper::image($model->image_url, [
                                    'size' => CDNHelper::IMG_SIZE_ORIGINAL,
                                    'forceCreate' => true,
                                    'width' => '50px'
                                ]);
                            },
                            'filter' => false,
                            'options' => ['class' => 'col-xs-1']
                        ],
                        [
                            'attribute' => 'status',
                            'format' => 'raw',
                            'value' => function ($model) {
                                return $model->statusButton;
                            },
                            'contentOptions' => [
                                'style' => 'min-width: 115px;'
                            ],
                            'filter' => Html::activeDropDownList($searchModel, 'status', HomeTeacher::listStatus(), ['class' => 'form-control', 'prompt' => Yii::t('app', '-- Select --')]),
                        ],
                    ],
                    [[
                        'class' => 'yii\grid\ActionColumn',
                        'template' => '{update} {delete}',
                        'buttons' => [
                            'update' => function ($url, $model) {
                                $title = Yii::t('app', 'Update');
                                $options = array_merge([
                                    'title' => $title,
                                    'aria-label' => $title,
                                    'data-pjax' => '0',
                                ]);
                                $icon = Html::tag('span', '', ['class' => "fa fa-pencil"]);
                                return Html::a($icon, $url, $options);
                            },
                        ],
                        'visibleButtons' => [
                            'update' => function () {
                                return Yii::$app->user->can('Home.Teacher.Update');
                            },
                            'delete' => function () {
                                return Yii::$app->user->can('Home.Teacher.Delete');
                            }
                        ],
                    ]]
                )
            ]); ?>
            <?php Pjax::end() ?>
        </div>
    </div>
</div>
