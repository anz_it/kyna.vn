<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model kyna\home\models\HomeTeacher */

$this->title = Yii::t('app', 'Thêm giảng viên');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Giảng viên'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="">
    <div class="box">
        <div class="box-body">
            <?= $this->render('_form', [
                'model' => $model,
            ]) ?>
        </div>
    </div>
</div>