<?php

namespace kyna\seo\helpers;
use Yii;
use common\helpers\ArrayHelper;
use yii\db\Query;
use yii\helpers\Url;

class SitemapXmlHelper {

    const SITEMAPS = [
        'categories',
        'courses',
        'tags',
        'pages',
        'user'
    ];

    const SITEMAPS_ALLIAS = [
        'categories' => 'categories',
        'courses' => 'courses',
        'tags' => 'tags',
        'pages' => 'landing_pages',
        'user' => 'teacher'
    ];

    const SPEC_SITEMAPS = [
//        'bai-viet' => 'bai-viet/sitemap_index.xml',
    ];

    const CHANGE_FREQS = [
        'always',
        'hourly',
        'daily',
        'weekly',
        'monthly',
        'yearly',
        'never',
    ];

    const TABLE_NAME = 'sitemaps';

    //private $_urlSet = [];
    private static $_sitemapDir = '@webroot/sitemap';
    private static $_sitemapUrl = '@web/sitemap';
    private static $_sitemapRoot = '@webroot/sitemap.xml';

    public static function generateFromModels($models, $lastmod = false, $changefreq = false, $priority = false) {
        if (sizeof($models) === 0) {
            return false;
        }

        $urlSet = [];
        foreach ($models as $model) {
            $loc = htmlspecialchars($model->getUrl(false, 'https'));

            $urlSet[$model->id] = ['loc' => $loc];
            if ($lastmod) {
                $urlSet[$model->id]['lastmod'] = $model->hasAttribute($lastmod) ? date('c', $model->$lastmod) : $lastmod;
            }
            if ($changefreq) {
                $urlSet[$model->id]['changefreq'] = $changefreq;
            }
            if ($priority) {
                $urlSet[$model->id]['priority'] = $priority;
            }
        }

        return self::generate($urlSet, self::SITEMAPS_ALLIAS[$model->tableSchema->name]);
    }

    public static function generate($urlSet, $name = '') {
        if (sizeof($urlSet) === 0) {
            return false;
        }

        $allowedKeys = [
            'loc' => '',
            'lastmod' => '',
            'changefreq' => '',
            'priority' => '',
        ];

        $xml = '<?xml version="1.0" encoding="UTF-8"?>'.PHP_EOL.
            '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">'.PHP_EOL;
        foreach ($urlSet as $url) {
            $url = array_intersect_key($url, $allowedKeys);
            $xml .= "\t".'<url>'.PHP_EOL;

            foreach ($url as $prop => $val) {
                $xml .= "\t\t".'<'.$prop.'>'.$val.'</'.$prop.'>'.PHP_EOL;
            }
            $xml .= "\t".'</url>'.PHP_EOL;
        }
        $xml .= '</urlset>';

        $dir = Yii::getAlias(self::$_sitemapDir);
        if (!file_exists($dir) OR !is_dir($dir)) {
            mkdir($dir, 777, true);
        }
        $file = $dir.'/'.$name.'.xml';
        file_put_contents($file, $xml);

        return $file;
    }

    public static function generateIndex($sitemaps) {
        if (sizeof($sitemaps) === 0) {
            return false;
        }

        if (in_array('all', $sitemaps)) {
            $sitemaps = array_values(self::SITEMAPS_ALLIAS);
        }

        $sitemapString = '';
        foreach ($sitemaps as $sitemap) {
            $file = Yii::getAlias(self::$_sitemapDir.'/'.$sitemap.'.xml');
            if (!file_exists($file) OR !is_dir($file)) {
                $loc = Yii::getAlias(self::$_sitemapUrl.'/'.$sitemap.'.xml');
                $lastmod = date('c', filemtime($file));
                $sitemapString .= "\t".'<sitemap>'.PHP_EOL;
                $sitemapString .= "\t\t".'<loc>'.$loc.'</loc>'.PHP_EOL;
                $sitemapString .= "\t\t".'<lastmod>'.$lastmod.'</lastmod>'.PHP_EOL;
                $sitemapString .= "\t".'</sitemap>'.PHP_EOL;
            }
        }

        foreach (self::SPEC_SITEMAPS as $key => $special_sitemap) {
            $file = Yii::getAlias('@web'.'/'.$special_sitemap);
//            echo ($file);die();
            if (!file_exists($file) OR !is_dir($file)) {
//                $loc = Yii::getAlias(self::$_sitemapUrl.'/'.$special_sitemap);
                $loc = Yii::getAlias('@web'.'/'.$special_sitemap);
//                $lastmod = date('c', filemtime($file));
                $lastmod = date('c');
                $sitemapString .= "\t".'<sitemap>'.PHP_EOL;
                $sitemapString .= "\t\t".'<loc>'.$loc.'</loc>'.PHP_EOL;
                $sitemapString .= "\t\t".'<lastmod>'.$lastmod.'</lastmod>'.PHP_EOL;
                $sitemapString .= "\t".'</sitemap>'.PHP_EOL;
            }
        }

        if ($sitemapString === '') {
            return false;
        }

        $xml = '<?xml version="1.0" encoding="UTF-8"?>'.PHP_EOL.
            '<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">'.PHP_EOL;
        $xml .= $sitemapString;
        $xml .= '</sitemapindex>';
        $file = Yii::getAlias(self::$_sitemapRoot);
        file_put_contents($file, $xml);
        return $file;
    }



    public static function getSiteMapSettings()
    {
        $query = new Query();
        $data = $query->select('*')
            ->from(self::TABLE_NAME)
            ->where([
                'is_enabled' => true,
                'is_deleted' => false,
            ])
            ->all();

        $result = [];
        foreach ($data as $key => $value) {
            $result[$value['name']] = $value;
        }
        return $result;
    }
}
