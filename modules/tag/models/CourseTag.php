<?php

namespace kyna\tag\models;

use common\helpers\ArrayHelper;
use kyna\course\models\Course;
use kyna\course_combo\models\CourseCombo;
use Yii;

/**
 * This is the model class for table "{{%course_tags}}".
 *
 * @property integer $course_id
 * @property integer $tag_id
 */
class CourseTag extends \kyna\base\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%course_tags}}';
    }

    protected static function softDelete()
    {
        return false;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['course_id', 'tag_id'], 'required'],
            [['course_id', 'tag_id'], 'integer'],
            [['course_id', 'tag_id'], 'unique', 'targetAttribute' => ['course_id', 'tag_id'], 'message' => 'The combination of Course ID and Tag ID has already been taken.'],
        ];
    }

    public function setCourse($course_id){
        $this->course_id = $course_id;
    }

    public function setTag($tag_id){
        $this->tag_id = $tag_id;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'course_id' => Yii::t('app', 'Course ID'),
            'tag_id' => Yii::t('app', 'Tag ID'),
        ];
    }

    public static function assignCourseTag($tags, $course_id, $remove = false)
    {
        if ($remove) {
            // first remove all assigned tag for course
            CourseTag::deleteAll([
                'course_id' => $course_id
            ]);
        }
        // then assign presented tag
        $tags = is_array($tags) ? $tags : [$tags];
        foreach ($tags as $tag) {
            $tagModel = Tag::getTagModel($tag);
            $courseModel = Course::findOne($course_id);
            if ($tagModel && $courseModel) {
                $coureTag = new self();
                $coureTag->tag_id = $tagModel->id;
                $coureTag->course_id = $courseModel->id;
                $coureTag->save();
            }
            unset($tagModel);
            unset($courseModel);
        }
    }

    /**
     * Update tags after create/update combo course (tags from combo items)
     * @param $comboID
     * @param bool $remove
     * @return bool
     */
    public static function updateComboCourseTags($comboID, $remove = false)
    {
        $comboCourse = CourseCombo::find()->where(['id' => $comboID])->one();
        if (empty($comboCourse)) {
            return false;
        }

        // first remove all assigned tag for combo course
        if ($remove) {
            CourseTag::deleteAll([
                'course_id' => $comboID
            ]);
        }

        // get tag from course items
        $courseIDs = ArrayHelper::getColumn($comboCourse->items, 'course_id');
        $tags = Tag::find()
            ->alias('t')
            ->join('INNER JOIN', self::tableName(), self::tableName() . '.tag_id = t.id')
            ->join('INNER JOIN', Course::tableName(), Course::tableName() . '.id = ' . self::tableName() . '.course_id')
            ->where([
                Course::tableName() . '.id' => $courseIDs
            ])
            ->groupBy('t.tag')
            ->all();

        // remove tags that not in course items
        $tagIDs = ArrayHelper::getColumn($tags, 'id');
        CourseTag::deleteAll([
            'AND',
            ['course_id' => $comboCourse->id],
            ['NOT IN', 'tag_id', $tagIDs]
        ]);

        // then assign presented tags
        if (!empty($tags)) {
            foreach ($tags as $tag) {
                $tagModel = Tag::getTagModel($tag->tag);
                if ($tagModel === false) {
                    continue;
                }
                $courseTag = self::findOne([
                    'tag_id' => $tagModel->id,
                    'course_id' => $comboCourse->id
                ]);
                if (empty($courseTag)) {
                    $courseTag = new self();
                    $courseTag->tag_id = $tagModel->id;
                    $courseTag->course_id = $comboCourse->id;
                    $courseTag->save();
                }
                unset($tagModel);
            }
        }
    }
}
