<?php

namespace kyna\tag\models;

use Yii;
use kyna\course\models\Teacher;

/**
 * This is the model class for table "{{%teacher_tags}}".
 *
 * @property integer $teacher_id
 * @property integer $tag_id
 */
class TeacherTag extends \kyna\base\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%teacher_tags}}';
    }

    protected static function softDelete()
    {
        return false;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['teacher_id', 'tag_id'], 'required'],
            [['teacher_id', 'tag_id'], 'integer'],
            [['teacher_id', 'tag_id'], 'unique', 'targetAttribute' => ['teacher_id', 'tag_id'], 'message' => 'The combination of Teacher ID and Tag ID has already been taken.'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'teacher_id' => Yii::t('app', 'Teacher ID'),
            'tag_id' => Yii::t('app', 'Tag ID'),
        ];
    }

    public static function assignTeacherTag($tags, $teacher_id, $remove = false)
    {
        if ($remove) {
            // first remove all assigned tag for teacher
            self::deleteAll([
                'teacher_id' => $teacher_id
            ]);
        }
        // then assign presented tag
        $tags = is_array($tags) ? $tags : [$tags];
        foreach ($tags as $tag) {
            $tagModel = Tag::getTagModel($tag);
            $teacherModel = Teacher::findOne($teacher_id);
            if ($tagModel && $teacherModel) {
                $teacherTag = new self();
                $teacherTag->tag_id = $tagModel->id;
                $teacherTag->teacher_id = $teacherModel->id;
                $teacherTag->save();
            }
            unset($tagModel);
            unset($courseModel);
        }
    }
}
