<?php

namespace kyna\tag\models;

use Yii;

/**
 * This is the model class for table "{{%course_tags_log}}".
 *
 * @property integer $course_id
 * @property string $tag_ids
 * @property string $add_ids
 * @property string $remove_ids
 */
class CourseTagLog extends \kyna\base\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%course_tags_log}}';
    }

    protected static function softDelete()
    {
        return false;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['course_id'], 'required'],
            [['course_id','created_by'], 'integer'],
            [['tag_ids', 'add_ids', 'remove_ids'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'course_id' => Yii::t('app', 'Course ID'),
            'tag_ids' => Yii::t('app', 'Tags ID'),
        ];
    }

}
