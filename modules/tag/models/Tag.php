<?php

namespace kyna\tag\models;

use kyna\settings\models\Banner;
use Yii;
use common\widgets\upload\UploadRequiredValidator;
use common\helpers\CDNHelper;
use common\helpers\StringHelper;
use kyna\course\models\Course;
use yii\helpers\Url;

/**
 * This is the model class for table "{{%tags}}".
 *
 * @property integer $id
 * @property string $tag
 * @property string $slug
 * @property string $description
 * @property integer $status
 * @property integer $is_deleted
 * @property integer $image_url
 * @property integer $is_desktop
 * @property integer $is_mobile
 * @property integer $created_time
 * @property integer $updated_time
 * @property string $seo_title
 * @property string $seo_description
 * @property string $seo_keyword
 * @property string $seo_robot_index
 * @property string $seo_robot_follow
 * @property integer $seo_sitemap
 * @property integer $seo_canonical
 * @property integer banner_id
 */
class Tag extends \kyna\base\ActiveRecord
{
    const TYPE_DESKTOP = 'desktop';
    const TYPE_MOBILE = 'mobile';
    const SEO_ROBOT_INDEX = 'index';
    const SEO_ROBOT_NOINDEX = 'noindex';
    const SEO_ROBOT_FOLLOW = 'follow';
    const SEO_ROBOT_NOFOLLOW = 'nofollow';

    public $imageSize = [
        'cover' => [
            CDNHelper::IMG_SIZE_ORIGINAL,
            //CDNHelper::IMG_SIZE_TAG_DESKTOP,
            //CDNHelper::IMG_SIZE_TAG_MOBILE
        ],
        'contain' => [],
        'crop' => []
    ];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%tags}}';
    }

    protected static function softDelete()
    {
        return false;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tag', 'slug'], 'required'],
            [['status', 'is_deleted', 'is_desktop', 'is_mobile', 'created_time', 'updated_time', 'banner_id'], 'integer'],
            [['tag', 'slug', 'image_url'], 'string', 'max' => 255],
            [['tag', 'slug'], 'unique'],
            [['image_url'], UploadRequiredValidator::className(), 'skipOnEmpty' => true],
            [['image_url'], 'image', 'skipOnEmpty' => true, 'maxSize' => '4194304', 'mimeTypes' => 'image/png,image/jpeg,image/svg+xml'],
            [['image_url'], 'required', 'when' => function (self $model) {
                return $model->is_mobile == Tag::BOOL_YES || $model->is_desktop == Tag::BOOL_YES;
            }, 'whenClient' => 'function (attribute, value) {
                var img_value = $(\'#tag-image_url\').attr(\'value\');
                return $(\'[name*="[is_mobile]"]\').val() == 1 && (typeof img_value == "undefined" || img_value == "");
            }', 'message' => 'Thông tin này bắt buộc nếu Tag hiển thị ở trang chủ'],
            [['seo_description'], 'string', 'max' => 165],
            [['seo_title', 'seo_description', 'seo_keyword', 'seo_robot_index', 'seo_robot_follow', 'seo_sitemap', 'seo_canonical'], 'safe'],
            ['description', 'validMinMax', 'skipOnEmpty' => true, 'params' => ['min' => 1000, 'max' => 1500]],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'tag' => Yii::t('app', 'Tên Tag'),
            'slug' => Yii::t('app', 'Slug'),
            'description' => Yii::t('app', 'Mô tả Tag'),
            'status' => Yii::t('app', 'Trạng thái'),
            'is_deleted' => Yii::t('app', 'Đã xóa'),
            'is_desktop' => 'Hiển thị Desktop',
            'is_mobile' => 'Hiển thị Mobile',
            'image_url' => 'Hình ảnh',
            'created_time' => Yii::t('app', 'Ngày tạo'),
            'updated_time' => Yii::t('app', 'Ngày cập nhật'),
            'seo_title' => 'Title Tag',
            'seo_description' => 'Meta Description',
            'seo_keyword' => 'Meta Keyword',
            'seo_robot_index' => 'Meta Robots Index',
            'seo_robot_follow' => 'Meta Robots Follow',
            'seo_sitemap' => 'Include in Sitemap?',
            'seo_canonical' => 'Canonical URL',
        ];
    }

    public static function getTagModel($tag)
    {
        if (empty($tag)) {
            return false;
        }
        $slug = StringHelper::slugify($tag);
        $tagModel = self::find()
            ->where(['slug' => $slug])
            ->one();
        if (empty($tagModel)) {
            $tagModel = new self();
            $tagModel->tag = $tag;
            $tagModel->slug = $slug;
            $tagModel->status = self::BOOL_YES;
            if (!$tagModel->save()) {
                return false;
            }
        }
        return $tagModel;
    }

    public function getCountCourse()
    {
        return Course::find()
            ->alias('c')
            ->join('INNER JOIN', CourseTag::tableName(), CourseTag::tableName() . '.course_id = c.id')
            ->join('INNER JOIN', Tag::tableName(), Tag::tableName() . '.id = ' . CourseTag::tableName() . '.tag_id')
            ->where([
                Tag::tableName() . '.tag' => $this->tag
            ])
            ->count();
    }

    public static function topTags($type = self::TYPE_DESKTOP, $limit = 10 )
    {
        $where = [];
        if ($type == self::TYPE_DESKTOP)
            $where['is_desktop'] = 1;
        else
            $where['is_mobile'] = 1;
        return Tag::find()->select(['t.id', 't.slug', 't.tag', 't.seo_title', 't.image_url', 'count(*) as num_course'])
            ->alias('t')
            ->join('left join', CourseTag::tableName()." ct", 't.id = ct.tag_id')
            ->where($where + ['t.status' => self::STATUS_ACTIVE, 't.is_deleted' => self::BOOL_NO])
            ->groupBy(['t.id', 't.slug', 't.tag', 't.seo_title', 't.image_url'])
            ->orderBy(['num_course' => SORT_DESC, 'is_desktop' => SORT_DESC])
            ->limit($limit)
            ->all();
    }

    /**
     * @deprecated This function is deprecated, use topTags instead
     * @description
     * Bad, very bad performance query, let try to run 2 query with the same result:
     * 1. SELECT `t`.*, (SELECT COUNT(ct.tag_id) FROM `course_tags` ct WHERE ct.tag_id = t.id) AS num_course FROM `tags` `t` WHERE `t`.`status`=1 ORDER BY `t`.`is_desktop` DESC, `num_course` DESC LIMIT 10
     *
     * 2. select t.id, t.tag, t.slug, count(*) as count_tag
        from tags t join `course_tags` ct on t.id=ct.tag_id
        where t.status = 1
        group by t.id, t.tag, t.slug
        order by t.is_desktop desc, count_tag desc
        limit 10
     *
     * Number 2 perform 200 times faster, this is a reason make the website is so slow.
     * @param null $limit
     * @return $this
     *
     */
    public static function getDesktopTags($limit = null)
    {
        throw new \BadFunctionCallException();
        //TODO: DON'T USE THIS METHOD UNLESS BAD QUERY IS FIXED.

//        $countHotTags = self::find()
//            ->where([
//                'is_desktop' => Tag::BOOL_YES,
//                'status' => Tag::STATUS_ACTIVE
//            ])
//            ->count();
//        if ($limit > 0 && $countHotTags >= $limit) {
//            $result = self::find()
//                ->alias('t')
//                ->select([
//                    't.*',
//                    '(SELECT COUNT(ct.tag_id) FROM '.CourseTag::tableName().' ct WHERE ct.tag_id = t.id) AS num_course'])
//                ->where([
//                    't.is_desktop' => Tag::BOOL_YES
//                ])
//                ->orderBy('num_course DESC');
//        } else {
//            $result = self::find()
//                ->alias('t')
//                ->select([
//                    't.*',
//                    '(SELECT COUNT(ct.tag_id) FROM '.CourseTag::tableName().' ct WHERE ct.tag_id = t.id) AS num_course'])
//                ->orderBy('t.is_desktop DESC, num_course DESC');
//        }
//        $result = $result->andWhere(['t.status' => Tag::BOOL_YES]);
//        if (!is_null($limit)) {
//            $result = $result->limit($limit);
//        }
//        $result = $result->all();
//        return $result;
    }

    /**
     * @deprecated
     * @param null $limit
     * @return $this
     */
    public static function getMobileTags($limit = null)
    {
        throw new \BadFunctionCallException();
        // TODO: DON'T USE THIS METHOD UNLESS BAD QUERY IS FIXED.
//        $result = self::find()
//            ->alias('t')
//            ->select([
//                't.*',
//                '(SELECT COUNT(ct.tag_id) FROM '.CourseTag::tableName().' ct WHERE ct.tag_id = t.id) AS num_course'])
//            ->where([
//                't.is_mobile' => Tag::BOOL_YES
//            ])
//            ->orderBy('num_course DESC');
//        $result = $result->andWhere(['t.status' => Tag::BOOL_YES]);
//        if (!is_null($limit)) {
//            $result = $result->limit($limit);
//        }
//        $result = $result->all();
//        return $result;
    }

    /**
     * Get list of SEO Robot Index Text
     * @return array
     */
    public static function listRobotIndex()
    {
        $list = [
            self::SEO_ROBOT_INDEX => 'Index',
            self::SEO_ROBOT_NOINDEX => 'NoIndex',
        ];

        return $list;
    }

    /**
     * Get list of SEO Robot Follow Text
     * @return array
     */
    public static function listRobotFollow()
    {
        $list = [
            self::SEO_ROBOT_FOLLOW => 'Follow',
            self::SEO_ROBOT_NOFOLLOW => 'NoFollow',
        ];

        return $list;
    }

    /**
     * Allow include Sitemap?
     * @return array
     */
    public static function listSitemap()
    {
        $list = [
            self::STATUS_ACTIVE => 'Always',
            self::STATUS_DEACTIVE => 'Never',
        ];

        return $list;
    }

    /**
     * Get SEO Title
     * @return string
     */
    public function getTitle()
    {
        return !empty($this->seo_title) ? $this->seo_title : $this->tag;
    }

    /**
     * Get array SEO info of Tag
     * @return array
     */
    public function getSeoMeta()
    {
        $title = $this->title;
        $description = $this->seo_description;
        $keyword = $this->seo_keyword;
        $robots = [];
        if (!empty($this->seo_robot_index)) {
            $robots['index'] = $this->seo_robot_index;
        }
        if (!empty($this->seo_robot_follow)) {
            $robots['follow'] = $this->seo_robot_follow;
        }
        $canonical = $this->seo_canonical;

        return [$title, $description, $keyword, $robots, $canonical];
    }

    public function getUrl()
    {
        return Url::to(['/tag/'.$this->slug]);

    }

    public function validMinMax($attribute, $params)
    {
        $stringValue = html_entity_decode(strip_tags($this->{$attribute}));
        $strLength = mb_strlen($stringValue, 'UTF-8');
        if (isset($params['min']) && $strLength < $params['min']) {
            $this->addError($attribute, "{$this->getAttributeLabel($attribute)} phải ít nhất {$params['min']} kí tự");
        }
        if (isset($params['max']) && $strLength > $params['max']) {
            $this->addError($attribute, "{$this->getAttributeLabel($attribute)} phải nhiều nhất {$params['max']} kí tự");
        }
    }

    public function getBanner(){
        $dateTimeStamp = time();
        $banner = Banner::find()
            ->andWhere(['id' => $this->banner_id, 'status' => self::STATUS_ACTIVE])
            ->andWhere("from_date_time is null or (from_date_time is not null and from_date_time <= '{$dateTimeStamp}')")
            ->andWhere("to_date_time is null or (to_date_time is not null and to_date_time >= '{$dateTimeStamp}')")
            ->one();
        return $banner;
    }
}
