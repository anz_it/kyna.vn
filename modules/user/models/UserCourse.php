<?php

namespace kyna\user\models;

use kyna\gamification\models\UserPointHistory;
use Yii;
use yii\web\NotFoundHttpException;

use common\helpers\ArrayHelper;
use common\helpers\TreeHelper;

use kyna\base\behaviors\ActionLogBehavior;
use kyna\base\events\ActionEvent;
use kyna\course\models\Course;
use kyna\course\models\CourseLesson;
use kyna\course\models\CourseSection;
use kyna\learning\models\UserCourseLesson;
use kyna\gamification\models\Mission;

/**
 * This is the model class for table "user_courses".
 *
 * @property int $id
 * @property int $user_id
 * @property int $course_id
 * @property int $process
 * @property int $activation_date
 * @property bool $is_activated
 * @property bool $is_started
 * @property bool $is_deleted
 * @property int $started_date
 * @property int $expiration_date
 * @property bool $is_graduated
 * @property int $method
 * @property int $current_section
 * @property int $current_lesson
 * @property int $last_passed_lesson_id
 * @property int $status
 * @property int $created_time
 * @property int $updated_time
 * @property int $finished
 * @property int $is_requested_rating
 * @property Course $course
 */
class UserCourse extends \kyna\base\ActiveRecord
{

    const SCENARIO_UPDATE_PROCESS = 'scenario-update-process';

    const EVENT_ACTIVE = 'active';
    const EVENT_LESSON_FINISH = 'finish-lesson';
    const EVENT_GRADUATE = 'graduate';
    const EVENT_GRADUATE_EXCELLENT = 'graduate-excellent';
    const EVENT_CERTIFICATE = 'certificate';
    const EVENT_RESET = 'reset';
    const EVENT_START = 'start';
    const EVENT_UPDATED_PROCESS = 'updated-process';

    public $totalCourse;

    public $certNumberPrefix = 'KYNA-';
    public $certNumberSuffix = '';


    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user_courses';
    }

    public function init()
    {
        $ret = parent::init();

        $this->on(self::EVENT_GRADUATE, [$this, 'afterGraduated']);
        $this->on(self::EVENT_GRADUATE_EXCELLENT, [$this, 'afterGraduatedExcellent']);
        $this->on(self::EVENT_UPDATED_PROCESS, [$this, 'afterUpdatedProcess']);

        return $ret;
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'course_id'], 'required'],
            [
                [
                    'user_id', 'course_id', 'activation_date', 'expiration_date',
                    'method', 'status', 'created_time', 'updated_time', 'started_date',
                    'last_passed_lesson_id',
                    'finished', 'is_requested_rating'
                ],
                'integer'
            ],
            [['is_activated', 'is_started', 'is_graduated', 'is_quick'], 'boolean'],
            [['user_id'], 'unique', 'comboNotUnique' => 'Học viên đã đăng ký khóa học này.', 'targetAttribute' => ['user_id', 'course_id']]
        ];
    }

    public function scenarios()
    {
        $scenarios = parent::scenarios();

        $scenarios[self::SCENARIO_UPDATE_PROCESS] = [];

        return $scenarios;
    }

    public static function softDelete()
    {
        return false;
    }

    public static function getResetActions()
    {
        return [
            self::EVENT_GRADUATE,
            self::EVENT_LESSON_FINISH,
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'Học viên',
            'course_id' => 'Khóa học',
            'activation_date' => 'Activation Date',
            'is_activated' => 'Đã activated',
            'is_started' => 'Đã bắt đầu',
            'expiration_date' => 'Ngày hết hạn',
            'is_graduated' => 'Đã tốt nghiệp',
            'is_quick' => 'Học cấp tốc',
            'is_requested_rating' => 'Hiển thị yêu cầu đánh giá',
            'method' => 'Method',
            'status' => 'Status',
            'created_time' => 'Ngày đăng kí',
            'updated_time' => 'Updated Time',
        ];
    }

    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['action'] = [
            'class' => ActionLogBehavior::className(),
            'enableAction' => 'user_course',
            'modelClassName' => __NAMESPACE__ . '\\actions\\UserCourseAction',
            'events' => [
                self::EVENT_LESSON_FINISH => [],
                self::EVENT_GRADUATE => [
                    'onetimeAction' => true,
                ],
                self::EVENT_ACTIVE => [],
                self::EVENT_RESET => [],
                self::EVENT_CERTIFICATE => [
                    'onetimeAction' => true,
                ],
                self::EVENT_START => [],
            ],
        ];

        return $behaviors;
    }


    /**
     * @desc register courses for an user
     *
     * @param int $userId
     * @param int courseId or an array integer $courseIds
     *
     * @return string/false
     */
    public static function register($userId, $courseIds, $active = self::BOOL_NO)
    {
        if (!is_array($courseIds)) {
            $courseIds = [$courseIds];
        }

        $result = [];
        foreach ($courseIds as $courseId) {
            $userCourseModel = self::find()->where(['course_id' => $courseId, 'user_id' => $userId])->one();
            if (empty($userCourseModel)) {
                $userCourseModel = new self();

                $userCourseModel->user_id = $userId;
                $userCourseModel->course_id = $courseId;
                $userCourseModel->is_activated = $active;
                if ($userCourseModel->save()) {
                    $result[] = $userCourseModel->id;
                }
            }
        }

        if (!empty($result)) {
            return $result;
        }

        return false;
    }

    /**
     * @desc activate courses with activation code for an user
     *
     * @param int $userId
     * @param string $activationCode
     * @return array integer activated user_course
     */
    public static function active($userId, $courseIds)
    {
        if (!is_array($courseIds)) {
            $courseIds = [$courseIds];
        }

        $result = [];

        foreach ($courseIds as $courseId) {
            $userCourseModel = self::find()->where([
                'user_id' => $userId,
                'course_id' => $courseId,
            ])->one();

            if (is_null($userCourseModel)) {
                $userCourseModel = new static;

                $userCourseModel->user_id = $userId;
                $userCourseModel->course_id = $courseId;
            }

            $userCourseModel->is_activated = self::BOOL_YES;
            $userCourseModel->activation_date = time();
            if ($userCourseModel->save()) {
                $result[] = $userCourseModel->id;

                $userCourseModel->trigger(self::EVENT_ACTIVE, new ActionEvent());
            }
        }

        if (empty($result)) {
            return false;
        }

        return $result;
    }

    /*
     * Make student graduated
     * @param user_id id of student will be graduated
     * @param course_id id of course which student will be graduated in
     * @return Boolean
     */
    public static function graduate($user_id, $course_id)
    {
        $userCourseModel = self::find()->where([
            'user_id' => $user_id,
            'course_id' => $course_id,
            'is_graduated' => self::BOOL_NO,
        ])->one();

        if (!empty($userCourseModel)) {
            $userCourseModel->is_graduated = self::BOOL_YES;
            if ($userCourseModel->is_started == self::BOOL_NO) {
                $userCourseModel->is_started = self::BOOL_YES;
            }
            if ($userCourseModel->save()) {
                $userCourseModel->trigger(self::EVENT_GRADUATE, new ActionEvent());

                return true;
            }
        }

        return false;
    }

    /*
     * Reset course learning for student
     * @param user_id id of student will be reset
     * @param course_id id of course which student will be reset
     * @return Boolean
     */
    public static function reset($user_id, $course_id)
    {
        $userCourseModel = self::find()->where([
            'user_id' => $user_id,
            'course_id' => $course_id,
            'is_started' => self::BOOL_YES,
        ])->one();

        if (!empty($userCourseModel)) {
            $userCourseModel->is_started = self::BOOL_NO;
            if ($userCourseModel->is_graduated == self::BOOL_YES) {
                $userCourseModel->is_graduated = self::BOOL_NO;
            }
            $userCourseModel->activation_date = time();
            if ($userCourseModel->save()) {
                $userCourseModel->afterReset();

                return true;
            }
        }

        return false;
    }

    public static function quick($user_id, $course_id)
    {
        $userCourseModel = self::find()->where([
            'user_id' => $user_id,
            'course_id' => $course_id,
            'is_quick' => self::BOOL_NO,
        ])->orWhere([
            'user_id' => $user_id,
            'course_id' => $course_id,
            'is_quick' => NULL,
        ])->one();

        if (!empty($userCourseModel)) {
            $userCourseModel->is_quick = self::BOOL_YES;

            if ($userCourseModel->save()) {
                return true;
            }
        }
        return false;
    }

    public static function unQuick($user_id, $course_id)
    {
        $userCourseModel = self::find()->where([
            'user_id' => $user_id,
            'course_id' => $course_id,
            'is_quick' => self::BOOL_YES,
        ])->one();

        if (!empty($userCourseModel)) {
            $userCourseModel->is_quick = self::BOOL_NO;

            if ($userCourseModel->save()) {
                return true;
            }
        }
        return false;
    }

    public function afterReset()
    {
        // TODOs: delete related things (lesson, mission, quiz...)

        $this->trigger(self::EVENT_RESET, new ActionEvent());
    }

    public static function finishLesson($id)
    {
        $lesson = CourseLesson::findOne($id);
        if (!$lesson) {
            return false;
        }
        $userCourse = self::find()->where([
            'course_id' => $lesson->course_id,
            'user_id' => Yii::$app->user->getId(),
        ])->one();

        if (!$userCourse) {
            return false;
        }

        $event = new ActionEvent();
        $event->addMeta([
            'progress' => 100,
            'lesson_id' => $id,
        ]);
        $userCourse->trigger(self::EVENT_LESSON_FINISH, $event);

        return true;
    }

    public static function getLearning($courseId, $userId = false)
    {
        if (!$userId) {
            $userId = Yii::$app->user->getId();
        }

        $model = self::find()->where([
            'course_id' => $courseId,
            'user_id' => $userId,
            'is_activated' => true,
        ])->one();

        if ($model == null) {
            throw new NotFoundHttpException();
        }

        return $model;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCourse()
    {
        return $this->hasOne(Course::className(), ['id' => 'course_id']);
    }

    public function getStudent()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * Compare day can learn with current date
     * @param $dayCanLearn
     * @return bool
     */
    public function compareDate($dayCanLearn = null)
    {
        // $condition = $this->started_date + ($day_can_learn * (60 * 60 *24));
        $dateDiff = strtotime(date('Y-m-d')) - strtotime(date('Y-m-d', $this->started_date));
        if ($dateDiff >= $dayCanLearn * 24 * 3600) {
            return true;
        }
        return false;
    }

    public function getIsGraduated()
    {
        if ($this->is_graduated == self::BOOL_YES) {
            return true;
        }

        return false;
    }

    public function getCertificateNumber()
    {
        return $this->certNumberPrefix . $this->id . $this->certNumberSuffix;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public static function find()
    {
        return parent::find()->andOnCondition(['<>', self::tableName() . '.is_deleted', self::BOOL_YES]);
    }

    /**
     * Override delete function
     */
    public function delete()
    {
        $this->is_deleted = self::BOOL_YES;
        $this->update();
        //return parent::delete(); // TODO: Change the autogenerated stub
    }

    public function calculateProgress()
    {
        $sections = CourseSection::find()->where(['course_id' => $this->course_id, 'status' => 1, 'active' => 1, 'visible' => 1])
            ->with("lessons")
            ->addOrderBy('root')
            ->addOrderBy('lft')
            ->all();

        $sections = TreeHelper::buildTree($sections);

        $userCourseLessons = ArrayHelper::map(UserCourseLesson::find()->where(['user_course_id' => $this->id])->all(), "course_lesson_id", "process");

        if (empty ($userCourseLessons))
            return false;

        $totalCoefficientCourse = 0;
        $totalPercentCourse = 0;
        foreach ($sections as &$section) {
            $totalPercentParent = 0;
            $totalCoefficientParent = 0;

            foreach ($section->children as &$child) {
                $totalCoefficient = 0;
                $totalPercent = 0;
                foreach ($child->lessons as $lesson) {
                    $totalCoefficient += $lesson->coefficient;
                    $totalPercent += $lesson->coefficient * (isset($userCourseLessons[$lesson->id]) ? $userCourseLessons[$lesson->id] : 0);
                }
                $childProcess = $totalCoefficient == 0 ? 0 : $totalPercent / $totalCoefficient;
                $totalCoefficientParent +=  $child->coefficient;
                $totalPercentParent += $child->coefficient * $childProcess;
            }
            $sectionProcess = $totalCoefficientParent == 0 ? 0 : $totalPercentParent / $totalCoefficientParent;
            $totalCoefficientCourse += $section->coefficient;
            $totalPercentCourse += $section->coefficient * $sectionProcess;
        }

        $oldProcess = $this->process;
        $this->process = $totalCoefficientCourse == 0 ? 0 :  round($totalPercentCourse / $totalCoefficientCourse,2);
        if ($oldProcess == $this->process) {
            return false;
        }
        $this->scenario = self::SCENARIO_UPDATE_PROCESS;

        if ($this->process >= $this->course->percent_can_pass) {
            $this->finished = 1;
            $this->is_graduated = self::BOOL_YES;
            if ($this->is_started == self::BOOL_NO) {
                $this->is_started = self::BOOL_YES;
            }
            if ($this->save(false)) {
                $this->trigger(self::EVENT_GRADUATE, new ActionEvent());

                return true;
            }
        } else {
            $this->save(false);

            return true;
        }

    }

    public function afterSave($insert, $changedAttributes)
    {
        $ret = parent::afterSave($insert, $changedAttributes);

        if (!$insert && isset($changedAttributes['process'])
            && $this->is_graduated
            && $changedAttributes['process'] < $this->course->percent_can_be_excellent
            && $this->process >= $this->course->percent_can_be_excellent) {
            // check to update certificate to be excellent
            $this->updateCertificate();

            $this->trigger(self::EVENT_GRADUATE_EXCELLENT, new ActionEvent());
        }

        switch ($this->scenario) {
            case self::SCENARIO_UPDATE_PROCESS:
                $this->trigger(self::EVENT_UPDATED_PROCESS);
                break;

            default:
                break;
        }

        return $ret;
    }

    public function updateCertificate()
    {
        $saveDir = Yii::getAlias('@upload/user/{user_id}/certificates');
        $saveDir = str_replace('{user_id}', $this->user_id, $saveDir);
        if (!file_exists($saveDir) && !is_dir($saveDir)) {
            mkdir($saveDir, 0755, true);
        }

        $saveFile = $saveDir . '/' . $this->course_id . '.jpg';

        $certificateGenerator = Yii::createObject([
            'class' => '\kyna\course\lib\CertificateImage',
            'saveFile' => $saveFile,
            'overwrite' => true
        ]);

        $temp = explode(' ', $this->student->birthday);
        $birthdate = false;
        if (!empty($temp[0])) {
            $date = date_create_from_format('Y-m-d', $temp[0]);

            if ($date === false) {
                $date = date_create_from_format('d-m-Y', $temp[0]);
            }

            if ($date !== false) {
                $birthdate = $date->format('d-m-Y');
            }
        }
        if ($birthdate == false) {
            return false;
        }

        $certificateGenerator->generate($this->certificateNumber, $this->student->profile->name, $birthdate, $this->course->name, 'Xuất sắc');
    }

    public function afterGraduated()
    {
        $missions = Mission::findMissionsByCourseGraduated($this->course_id);
        $courseTblName = Course::tableName();

        foreach ($missions as $mission) {
            /* @var $mission Mission */
            $finishedCoursesQuery = self::find()
                ->where([
                    'user_id' => $this->user_id,
                    'is_graduated' => self::BOOL_YES
                ]);

            switch ($mission->apply_course_type) {
                case Mission::APPLY_COURSE_TYPE_SOME:
                    $courseIds = $mission->getMissionCourses()->select(['course_id'])->column();

                    $finishedCoursesQuery->andWhere([
                        'course_id' => $courseIds
                    ]);
                    break;

                case Mission::APPLY_COURSE_TYPE_CATEGORY:
                    $categoryIds = $mission->getMissionCategories()->select(['category_id'])->column();
                    $finishedCoursesQuery->joinWith('courses ' . $courseTblName);
                    $finishedCoursesQuery->joinWith('categories', 'categories.id = ' . $courseTblName . '.category_id');

                    $finishedCoursesQuery->andWhere([
                        'OR',
                        [$courseTblName . '.category_id' => $categoryIds],
                        ['categories.parent_id' => $categoryIds],

                    ]);

                    break;

                default:
                    break;
            }
            $conditions = $mission->missionConditions;
            $validMission = true;

            $courseIds = [];

            /* @var $condition MissionCondition */
            foreach ($conditions as $condition) {
                $minValue = $condition->min_value;
                $isValid = false;

                if ($minValue > 1) {
                    $finishedCourses = $finishedCoursesQuery->select('course_id')->distinct()->column();

                    foreach ($finishedCourses as $fCourseId) {
                        $checkExist = UserPointHistory::find()
                            ->alias('h')
                            ->joinWith('historyReferences hr')
                            ->where([
                                'h.mission_id' => $mission->id,
                                'h.user_id' => $this->user_id
                            ])
                            ->andWhere(["hr.reference_id" => $fCourseId])
                            ->exists();

                        if ($checkExist) {
                            continue;
                        }

                        $courseIds[] = $fCourseId;
                    }

                    $total = count($courseIds);
                    if ($total > 0 && $total % $minValue == 0) {
                        $isValid = true;
                    }
                } else {
                    $checkExist = UserPointHistory::find()
                        ->alias('h')
                        ->joinWith('historyReferences hr')
                        ->where([
                            'h.mission_id' => $mission->id,
                            'h.user_id' => $this->user_id
                        ])
                        ->andWhere(["hr.reference_id" => $this->course_id])
                        ->exists();

                    if (!$checkExist) {
                        $courseIds[] = $this->course_id;
                        $isValid = true;
                    }
                }

                if ($mission->condition_operator == 'AND') {
                    $validMission = $validMission && $isValid;
                } elseif ($mission->condition_operator == 'OR') {
                    $validMission = $validMission || $isValid;
                    if ($validMission) {
                        break;
                    }
                }
            }

            if ($validMission) {
                $point = $mission->k_point;
                if ($mission->k_point_for_free_course != null && $this->course->sellPrice == 0) {
                    $point = $mission->k_point_for_free_course;
                }

                Mission::addPointByMission($this->user_id, $point, $mission->id, $courseIds);
            }
        }
    }

    public function afterGraduatedExcellent()
    {
        $missions = Mission::findMissionsByCourseGraduatedExcellent($this->course_id);
        $courseTblName = Course::tableName();

        foreach ($missions as $mission) {
            /* @var $mission Mission */
            $finishedCoursesQuery = self::find()
                ->joinWith('course ' . $courseTblName)
                ->where([
                    'user_id' => $this->user_id,
                    'is_graduated' => self::BOOL_YES
                ])
                ->andWhere(['is not', $courseTblName . '.percent_can_be_excellent', null])
                ->andWhere([
                    '>=', 'process', $courseTblName . '.percent_can_be_excellent'
                ]);

            switch ($mission->apply_course_type) {
                case Mission::APPLY_COURSE_TYPE_SOME:
                    $courseIds = $mission->getMissionCourses()->select(['course_id'])->column();

                    $finishedCoursesQuery->andWhere([
                        'course_id' => $courseIds
                    ]);
                    break;

                case Mission::APPLY_COURSE_TYPE_CATEGORY:
                    $categoryIds = $mission->getMissionCategories()->select(['category_id'])->column();
                    $finishedCoursesQuery->leftJoin('categories', 'categories.id = ' . $courseTblName . 'category_id');

                    $finishedCoursesQuery->andWhere([
                        'OR',
                        [$courseTblName . '.category_id' => $categoryIds],
                        ['categories.parent_id' => $categoryIds],
                    ]);

                    break;

                default:
                    break;
            }
            $conditions = $mission->missionConditions;
            $validMission = true;
            $courseIds = [];

            /* @var $condition MissionCondition */
            foreach ($conditions as $condition) {
                $minValue = $condition->min_value;
                $isValid = false;

                if ($minValue > 1) {
                    $finishedCourses = $finishedCoursesQuery->select('course_id')->distinct()->column();

                    foreach ($finishedCourses as $fCourseId) {
                        $checkExist = UserPointHistory::find()
                            ->alias('h')
                            ->joinWith('historyReferences hr')
                            ->where([
                                'h.mission_id' => $mission->id,
                                'h.user_id' => $this->user_id
                            ])
                            ->andWhere(["hr.reference_id" => $fCourseId])
                            ->exists();

                        if ($checkExist) {
                            continue;
                        }

                        $courseIds[] = $fCourseId;
                    }

                    $total = count($courseIds);
                    if ($total > 0 && $total % $minValue == 0) {
                        $isValid = true;
                    }
                } else {
                    $checkExist = UserPointHistory::find()
                        ->alias('h')
                        ->joinWith('historyReferences hr')
                        ->where([
                            'h.mission_id' => $mission->id,
                            'h.user_id' => $this->user_id
                        ])
                        ->andWhere(["hr.reference_id" => $this->course_id])
                        ->exists();

                    if (!$checkExist) {
                        $courseIds[] = $this->course_id;
                        $isValid = true;
                    }
                }

                if ($mission->condition_operator == 'AND') {
                    $validMission = $validMission && $isValid;
                } elseif ($mission->condition_operator == 'OR') {
                    $validMission = $validMission || $isValid;
                    if ($validMission) {
                        break;
                    }
                }
            }

            if ($validMission) {
                $point = $mission->k_point;
                if ($mission->k_point_for_free_course != null && $this->course->sellPrice == 0) {
                    $point = $mission->k_point_for_free_course;
                }

                Mission::addPointByMission($this->user_id, $point, $mission->id, $courseIds);
            }
        }
    }

    public function afterUpdatedProcess()
    {
        $missions = Mission::findMissionsByCourseUpdatedProcess($this->course_id, $this->process);

        foreach ($missions as $mission) {
            $point = $mission->k_point;
            if ($mission->k_point_for_free_course != null && $this->course->sellPrice == 0) {
                $point = $mission->k_point_for_free_course;
            }

            Mission::addPointByMission($this->user_id, $point, $mission->id, $this->course_id, $this->course_id);
        }
    }

    public static function addUserCourse($user_id, $course_id){
        $userCourse = UserCourse::findOne(['user_id'=>$user_id,'course_id'=>$course_id]);
        if(empty($userCourse)){
            $userCourse = new UserCourse();
            $userCourse->user_id = $user_id;
            $userCourse->course_id = $course_id;
            $userCourse->is_activated = UserCourse::BOOL_YES;
            $userCourse->activation_date = time();
            $userCourse->save(false);
            return true;
        }else{
            return false;
        }
    }
}
