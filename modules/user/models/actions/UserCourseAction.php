<?php

namespace kyna\user\models\actions;

/**
 * This is the model class for table "course_lesson_actions".
 *
 * @property int $id
 * @property int $user_id
 * @property int $user_course_id
 * @property string $name
 * @property int $action_time
 */
class UserCourseAction extends \kyna\base\models\Action
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user_course_actions';
    }

    protected function enableMeta()
    {
        return [
            'user_course_action',
            __NAMESPACE__.'\\UserCourseActionMeta',
        ];
    }

    public function metaKeys()
    {
        return [
            'playback_time',
            'progress',
            'lesson_id',
            'certificate_number'
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'user_course_id', 'action_time'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'user_course_id' => 'Course Lesson ID',
            'name' => 'Name',
            'action_time' => 'Action Time',
        ];
    }
    
    public function afterDelete()
    {
        $ret = parent::afterDelete();
        
        UserCourseActionMeta::deleteAll([
            'user_course_action_id' => $this->id,
        ]);
        
        return $ret;
    }
}
