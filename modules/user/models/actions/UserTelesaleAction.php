<?php

namespace kyna\user\models\actions;

use kyna\base\models\Action;

class UserTelesaleAction extends Action
{
    
    protected function enableMeta()
    {
        return [
            'user_telesale_action',
            '\\kyna\\user\\models\\actions\\UserTelesaleActionMeta'
        ];
    }

    public function metaKeys()
    {
        return [
            'status',
            'note',
            'recall_date',
            'coupon',
        ];
    }

    public static function tableName()
    {
        return '{{%user_telesale_actions}}';
    }

}
