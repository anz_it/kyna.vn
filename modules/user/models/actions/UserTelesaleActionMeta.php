<?php

namespace kyna\user\models\actions;

use Yii;

/**
 * This is the model class for table "{{%user_telesale_action_meta}}".
 *
 * @property integer $id
 * @property integer $user_telesale_action_id
 * @property string $key
 * @property string $value
 */
class UserTelesaleActionMeta extends \kyna\base\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user_telesale_action_meta}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_telesale_action_id', 'key'], 'required'],
            [['user_telesale_action_id'], 'integer'],
            [['value'], 'string'],
            [['key'], 'string', 'max' => 20],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_telesale_action_id' => 'User Telesale Action ID',
            'key' => 'Key',
            'value' => 'Value',
        ];
    }
}
