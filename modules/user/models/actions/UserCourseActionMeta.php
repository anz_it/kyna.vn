<?php

namespace kyna\user\models\actions;

use Yii;

/**
 * This is the model class for table "course_lesson_action_meta".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $user_course_action_id
 * @property string $key
 * @property string $value
 */
class UserCourseActionMeta extends \kyna\base\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_course_action_meta';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'user_course_action_id'], 'integer'],
            [['value'], 'string'],
            [['key'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'user_course_action_id' => 'Course Lesson Action ID',
            'key' => 'Key',
            'value' => 'Value',
        ];
    }
}
