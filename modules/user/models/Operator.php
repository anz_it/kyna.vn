<?php

namespace kyna\user\models;

use Yii;

class Operator extends \kyna\user\models\User
{

    public static $defaultRole = 'Operator';

    public static function find()
    {
        return self::findByRole(self::$defaultRole);
    }

    public static function findByRole($roleName)
    {
        $ids = Yii::$app->authManager->getUserIdsByRole($roleName);
        return parent::find()->where(['id' => $ids]);
    }

}
