<?php

namespace kyna\user\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use kyna\user\models\UserCourse as UserCourseModel;

/**
 * UserCourse represents the model behind the search form about `kyna\user\models\UserCourse`.
 */
class UserCourse extends UserCourseModel
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'course_id', 'process', 'activation_date', 'started_date', 'expiration_date', 'method', 'status', 'created_time', 'updated_time'], 'integer'],
            [['is_activated', 'is_started', 'is_graduated', 'is_deleted', 'is_quick'], 'boolean'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = UserCourseModel::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'course_id' => $this->course_id,
            'process' => $this->process,
            'activation_date' => $this->activation_date,
            'is_activated' => $this->is_activated,
            'is_started' => $this->is_started,
            'started_date' => $this->started_date,
            'expiration_date' => $this->expiration_date,
            'is_graduated' => $this->is_graduated,
            'method' => $this->method,
            'is_deleted' => $this->is_deleted,
            'status' => $this->status,
            'created_time' => $this->created_time,
            'updated_time' => $this->updated_time,
            'is_quick' => $this->is_quick,
        ]);
        
        $query->andWhere(['>', 'user_id', 0]);
        
        $query->groupBy('user_id');

        return $dataProvider;
    }
}
