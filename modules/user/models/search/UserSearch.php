<?php

namespace kyna\user\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use kyna\user\models\User;
use kyna\user\models\UserMeta;

/**
 * UserSearch represents the model behind the search form about `app\models\User`.
 */
class UserSearch extends User
{
    
    public $name;
    public $role;
    
    public function rules()
    {
        return [
            [['id', 'confirmed_at', 'blocked_at', 'created_at', 'updated_at', 'flags', 'fb_id'], 'integer'],
            [['username', 'email', 'password_hash', 'auth_key', 'unconfirmed_email', 'registration_ip', 'name', 'role'], 'safe'],
            [['username', 'id', 'email', 'name', 'registration_ip'], 'trim'],
        ];
    }

    public function scenarios()
    {
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = User::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => ['created_at' => SORT_DESC]
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'confirmed_at' => $this->confirmed_at,
            'blocked_at' => $this->blocked_at,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'flags' => $this->flags,
        ]);
        
        $query->andFilterWhere(['like', 'username', $this->username])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'password_hash', $this->password_hash])
            ->andFilterWhere(['like', 'auth_key', $this->auth_key])
            ->andFilterWhere(['like', 'unconfirmed_email', $this->unconfirmed_email])
            ->andFilterWhere(['like', 'registration_ip', $this->registration_ip]);
        
        $query->joinWith('profile');
        $query->andFilterWhere(['like', 'profile.name', $this->name]);

        if (!empty($this->fb_id)) {
            $query->andWhere(['is not', 'profile.facebook_id', null]);
        }
        
        if (!empty($this->role)) {
            $query->joinWith('authAssignments');
            $query->andFilterWhere(['auth_assignment.item_name' => $this->role]);
        }

        return $dataProvider;
    }
}
