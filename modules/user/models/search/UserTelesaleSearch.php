<?php

namespace kyna\user\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use kyna\user\models\UserTelesale;

/**
 * UserTelesaleSearch represents the model behind the search form about `kyna\user\models\UserTelesale`.
 */
class UserTelesaleSearch extends UserTelesale
{
    
    const FILTER_BY_CREATED_DATE = 'created_time';
    const FILTER_BY_LAST_CALL_DATE = 'last_call_date';
    
    public $date_ranger;
    public $filter_date_by_col;
    public $form_name_list;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'location_id', 'user_id', 'affiliate_id', 'old_affiliate_id', 'tel_id', 'course_id', 'bonus', 'status', 'created_time', 'updated_time', 'follow_num'], 'integer'],
            [['email', 'phone_number', 'full_name', 'street_address', 'type', 'form_name', 'note', 'date_ranger', 'filter_date_by_col', 'form_name_list'], 'safe'],
            [['amount'], 'number'],
            [['is_success', 'is_deleted'], 'boolean'],
            [['email', 'phone_number', 'full_name', 'follow_num'], 'trim'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {

        $query = UserTelesale::find()->with('teler', 'teler.profile');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        
        $oldTelId = $this->tel_id;

        $this->load($params);
        
        if (empty($this->tel_id)) {
            $this->tel_id = $oldTelId;
        }
        
        if (!empty($this->email) || !empty($this->full_name) || !empty($this->phone_number)) {
            $this->tel_id = NULL;
        }

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'location_id' => $this->location_id,
            'user_id' => $this->user_id,
            'affiliate_id' => $this->affiliate_id,
            'old_affiliate_id' => $this->old_affiliate_id,
            'tel_id' => $this->tel_id,
            'course_id' => $this->course_id,
            'bonus' => $this->bonus,
            'amount' => $this->amount,
            'status' => $this->status,
            'created_time' => $this->created_time,
            'email' => $this->email,
            'phone_number' => $this->phone_number,
            'full_name' => $this->full_name,
            'follow_num' => $this->follow_num,
            'type' => $this->type
        ]);
        
        $query->andFilterWhere(['like', 'street_address', $this->street_address])
            ->andFilterWhere(['like', 'note', $this->note]);
        
        if (isset($params['btn-recall'])) {
            $query->andWhere(['>', 'recall_date', strtotime('-11 days')]);
            $query->andWhere(['<=', 'recall_date', time()]);
            $query->orderBy('recall_date DESC');
        }
        
        if (!empty($this->date_ranger)) {
            $dateArr = explode(' - ', $this->date_ranger);
            if (count($dateArr) == 2) {
                $startTime = date_create_from_format('d/m/Y H:i', trim($dateArr[0]). ' 00:00')->getTimestamp();
                $endTime = date_create_from_format('d/m/Y H:i', trim($dateArr[1]). '23:59')->getTimestamp();
                
                $query->andFilterWhere(['>=', $this->filter_date_by_col, $startTime]);
                $query->andFilterWhere(['<=', $this->filter_date_by_col, $endTime]);
            }
        }

        if (!empty($this->form_name_list)) {
            $query->andWhere(['or like','form_name',$this->form_name_list]);
        }

        $query->orderBy((!empty($this->filter_date_by_col) ? $this->filter_date_by_col : 'created_time') . ' DESC');

        return $dataProvider;
    }
    
    public static function getFilterDateByOptions()
    {
        return [
            self::FILTER_BY_CREATED_DATE => 'Lọc theo ngày tạo',
            self::FILTER_BY_LAST_CALL_DATE => 'Lọc theo ngày gọi',
        ];
    }
}
