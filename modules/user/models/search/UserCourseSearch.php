<?php

namespace kyna\user\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;

use kyna\user\models\UserCourse;
use kyna\user\models\User;
use kyna\user\models\Profile;
use kyna\course\models\CourseDocument;
use kyna\course\models\Course;
use kyna\course\models\Category;

/**
 * UserCourseSearch represents the model behind the search form about `kyna\user\models\UserCourse`.
 */
class UserCourseSearch extends UserCourse
{

    public $userInfo;
    public $userFullName;
    public $userPhone;
    public $userEmail;
    public $courseName;
    public $categoryId;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'course_id', 'activation_date', 'expiration_date', 'method', 'status', 'created_time', 'updated_time'], 'integer'],
            [['is_activated', 'is_started', 'is_graduated'], 'boolean'],
            [['userInfo', 'userEmail', 'userFullName', 'userPhone', 'courseName'], 'string'],
            [['totalCourse', 'categoryId'], 'integer'],
            [['userFullName', 'userEmail', 'userPhone', 'user_id', 'userInfo'], 'trim'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = UserCourse::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'course_id' => $this->course_id,
            'activation_date' => $this->activation_date,
            'is_activated' => $this->is_activated,
            'is_started' => $this->is_started,
            'expiration_date' => $this->expiration_date,
            'is_graduated' => $this->is_graduated,
            'method' => $this->method,
            'status' => $this->status,
            'created_time' => $this->created_time,
            'updated_time' => $this->updated_time,
        ]);

        if (!empty($this->userInfo)) {
            $tableName = UserCourse::tableName();

            $query->join('LEFT JOIN', '{{%user}}', "user.id = $tableName.user_id");
            $query->join('LEFT JOIN', '{{%profile}}', "profile.user_id = $tableName.user_id");
            $query->andWhere('user.email LIKE :userInfo OR profile.name LIKE :userInfo OR profile.phone_number LIKE :userInfo', [
                ':userInfo' => "%{$this->userInfo}%"
            ]);
        }

        $query->orderBy('created_time DESC');

        return $dataProvider;
    }

    public function searchStudent($params)
    {
        $query = UserCourse::find();
        $query->joinWith(['student']);

        $tableName = UserCourse::tableName();

        $query->select(["$tableName.user_id", 'count(*) as totalCourse']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            $tableName . '.user_id' => $this->user_id,
            'course_id' => $this->course_id,
            'process' => $this->process,
            'activation_date' => $this->activation_date,
            'is_activated' => $this->is_activated,
            'is_started' => $this->is_started,
            'started_date' => $this->started_date,
            'expiration_date' => $this->expiration_date,
            'is_graduated' => $this->is_graduated,
            'method' => $this->method,
            $tableName . '.is_deleted' => $this->is_deleted,
            $tableName . '.status' => $this->status,
            'created_time' => $this->created_time,
            'updated_time' => $this->updated_time,
            'is_quick' => $this->is_quick,
        ]);

        if (!empty($this->userEmail) || !empty($this->userFullName) || !empty($this->userPhone)) {
            $query->joinWith(['student.profile']);
            $studentTable = User::tableName();
            $profileTable = Profile::tableName();

            if (!empty($this->userEmail)) {
                $query->andWhere(['LIKE', "$studentTable.email", $this->userEmail]);
            }

            if (!empty($this->userFullName)) {
                $query->andWhere(['LIKE', "$profileTable.name", $this->userFullName]);
            }

            if (!empty($this->userPhone)) {
                $query->andWhere(['LIKE', "$profileTable.phone_number", $this->userPhone]);
            }
        }

        $query->andWhere(['>', $tableName . '.user_id', 0]);

        $query->groupBy('user_id');

        return $dataProvider;
    }

    /**
     * Search course at Khóa học của tôi
     * @param null $params
     * @return ActiveDataProvider
     */
    public function searchCourse($params = null)
    {
        $query = UserCourse::find()->joinWith('course');

        // add conditions that should always apply here
        if ($params != null) {
            $this->load($params);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!$this->validate()) {
            $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'course_id' => $this->course_id,
            'is_activated' => $this->is_activated,
            'is_started' => $this->is_started,
        ]);

        if (!empty($this->courseName) || !empty($this->categoryId)) {
            $courseTblName = Course::tableName();
            $query->joinWith('course');

            if (!empty($this->courseName)) {
                $query->andWhere("$courseTblName.name LIKE :term OR short_name LIKE :term OR keyword LIKE :term", [':term' => '%' . $this->courseName . '%']);
            }
            if (!empty($this->categoryId)) {
                $catModel = Category::findOne($this->categoryId);
                if ($catModel != null && !empty($catModel->relatedIds)) {
                    $query->andFilterWhere(['in', "$courseTblName.category_id", $catModel->relatedIds]);
                }
            }
        }

        $query->orderBy('created_time DESC');

        return $dataProvider;
    }

    public function searchDocuments($params)
    {
        $query = UserCourse::find();
        $selfTable = self::tableName();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'user_id' => $this->user_id,
            'is_activated' => $this->is_activated
        ]);

        $courseDocTable = CourseDocument::tableName();
        $query->leftJoin($courseDocTable . ' d', 'd.course_id = ' . $selfTable . '.course_id');
        $query->andWhere(['is not', 'd.id', null]);

        $query->orderBy("$selfTable.created_time DESC");
        $query->groupBy("$selfTable.id");

        return $dataProvider;
    }
}
