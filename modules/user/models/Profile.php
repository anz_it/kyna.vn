<?php

namespace kyna\user\models;

use dektrium\user\models\Profile as BaseProfile;
class Profile extends BaseProfile
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'profile';
    }

    /**
     * @return int
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * @param int $user_id
     */
    public function setUserId($user_id)
    {
        $this->user_id = $user_id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getPublicEmail()
    {
        return $this->public_email;
    }

    /**
     * @param string $public_email
     */
    public function setPublicEmail($public_email)
    {
        $this->public_email = $public_email;
    }

    /**
     * @return string
     */
    public function getGravatarEmail()
    {
        return $this->gravatar_email;
    }

    /**
     * @param string $gravatar_email
     */
    public function setGravatarEmail($gravatar_email)
    {
        $this->gravatar_email = $gravatar_email;
    }

    /**
     * @return string
     */
    public function getGravatarId()
    {
        return $this->gravatar_id;
    }

    /**
     * @param string $gravatar_id
     */
    public function setGravatarId($gravatar_id)
    {
        $this->gravatar_id = $gravatar_id;
    }

    /**
     * @return string
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * @param string $location
     */
    public function setLocation($location)
    {
        $this->location = $location;
    }

    /**
     * @return string
     */
    public function getWebsite()
    {
        return $this->website;
    }

    /**
     * @param string $website
     */
    public function setWebsite($website)
    {
        $this->website = $website;
    }

    /**
     * @return string
     */
    public function getBio()
    {
        return $this->bio;
    }

    /**
     * @param string $bio
     */
    public function setBio($bio)
    {
        $this->bio = $bio;
    }

    public function setPhoneNumber($phone_number){
        $this->phone_number = $phone_number;
    }

    public function getPhoneNumber(){
        return $this->phone_number;
    }

    public function setOldPhoneNumber($oldPhoneNumber){
        $this->old_phone_number = $oldPhoneNumber;
    }

    public function getOldPhoneNumber(){
        return $this->old_phone_number;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required', 'on' => 'create'],
            'bioString'            => ['bio', 'string'],
            'publicEmailPattern'   => ['public_email', 'email'],
            'gravatarEmailPattern' => ['gravatar_email', 'email'],
            'websiteUrl'           => ['website', 'url'],
            'nameLength'           => ['name', 'string', 'max' => 255],
            'publicEmailLength'    => ['public_email', 'string', 'max' => 255],
            'gravatarEmailLength'  => ['gravatar_email', 'string', 'max' => 255],
            'locationLength'       => ['location', 'string', 'max' => 255],
            'websiteLength'        => ['website', 'string', 'max' => 255],
            [['phone_number'], 'match', 'pattern' => '/^0[0-9]{9,10}$/', 'message' => 'Vui lòng nhập đúng số điện thoại'],
            ['facebook_id', 'string', 'max' => 20],
            [['old_phone_number'],'safe']
        ];
    }

    public function updateNewPhoneNumber($oldPhoneNumber,$newPhoneNumber){
        $this->setOldPhoneNumber($oldPhoneNumber);
        $this->setPhoneNumber($newPhoneNumber);
        $this->save(false);
    }

}
