<?php

namespace kyna\user\models;

use Yii;
use yii2tech\ar\softdelete\SoftDeleteBehavior;

use dektrium\user\models\User as BaseUser;
use dektrium\user\helpers\Password;

use kyna\base\traits\SoftDeleteTrait;
use kyna\base\traits\MetaTrait;
use kyna\base\traits\StatusTrait;
use kyna\order\models\Order;
use kyna\base\Mailer;

use common\helpers\FileHelper;
use common\helpers\CDNHelper;

class User extends BaseUser
{
    use MetaTrait, StatusTrait, SoftDeleteTrait;

    const STATUS_DEACTIVE = 0;
    const STATUS_ACTIVE = 1;

    public $imageSize = [
        'cover' => [],
        'contain' => [],
        'crop' => [
            CDNHelper::IMG_SIZE_AVATAR,
            CDNHelper::IMG_SIZE_AVATAR_SMALL,
            CDNHelper::IMG_SIZE_AVATAR_LARGE
        ]
    ];

    public function behaviors()
    {
        $behaviors = parent::behaviors();

        if (static::softDelete()) {
            // Soft delete
            $behaviors['softDeleteBehavior'] = [
                'class' => SoftDeleteBehavior::className(),
                'softDeleteAttributeValues' => [
                    'is_deleted' => true,
                ],
                'restoreAttributeValues' => [
                    'is_deleted' => false,
                ],
                'replaceRegularDelete' => true
            ];
        }

        return $behaviors;
    }
    
    protected function getMailer()
    {
        return Yii::$container->get(Mailer::className());
    }

    protected function enableMeta()
    {
        return 'user';
    }

    protected function metaKeys()
    {
        return [
            'fb_id',
            'gender',
            'phone',
            'address',
            'birthday',
            'location_id',
            'is_receive_email_newsletter',
            'is_receive_email_new_course_created',
            'is_finished_tutorial_beginner',
            'avatar',
            'manager_id',
        ];
    }

    protected static function softDelete()
    {
        return TRUE;
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['password'], 'required', 'on' => 'create'],
            'usernameLength'   => ['username', 'string', 'min' => 2, 'max' => 255],
        ]);
    }

    public function getUserAddress()
    {
        return $this->hasOne(Address::className(), ['user_id' => 'id']);
    }

    /**
     * @desc get roles
     *
     * @return array
     */
    public function getAuthAssignments()
    {
        return $this->hasMany(AuthAssignment::className(), ['user_id' => 'id']);
    }

    /**
     * @desc get list users by role
     *
     * @param string $role
     *
     * @return array
     */
    public static function getUsersByRole($role, $with = ['profile'])
    {
        return self::find()->joinWith('authAssignments')->with($with)->where(['like', AuthAssignment::tableName().'.item_name', $role])->all();
    }
    
    public function beforeSave($insert)
    {
        $ret = parent::beforeSave($insert);
        
        $time = time();
        if ($insert && $this->hasAttribute('created_at') && empty($this->created_at)) {
            $this->created_at = $time;
        }
        
        if ($this->hasAttribute('updated_at')) {
            $this->updated_at = $time;
        }
        
        if (empty($this->username)) {
            $this->generateUsername();
        }
        
        return $ret;
    }
    
    public function getUserMeta()
    {
        return $this->hasMany(UserMeta::className(), ['user_id' => 'id']);
    }

    public function getOrders()
    {
        return $this->hasMany(Order::className(), ['user_id' => 'id']);
    }

    public function getTotalPriceOrders()
    {
        $total = 0;
        $orders = $this->orders;
        if ($orders) {
            foreach ($orders as $order) {
                $total += $order->total;
            }
        }
        return $total;
    }
    
    public function getAvatarImage()
    {
        if (!empty($this->avatar) && $this->avatar != '/media/images/no-avatar-150x150.gif') {
            return $this->avatar;
        } else {
            return '/src/img/default.png';
        }
    }
    /**
     * Relation with token model
     */
    public function getToken()
    {
        return $this->hasOne(Token::className(), ['user_id' => 'id']);
    }
    
    public function getLoginToken()
    {
        return $this->hasOne(Token::className(), ['user_id' => 'id'])->andWhere(['type' => Token::TYPE_LOGIN]);
    }

    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @param string $authKey
     * @return boolean if auth key is valid for current user
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }
    
    public function createLoginToken()
    {
        $oldToken = Token::find()->andWhere(['user_id' => $this->id, 'type' => Token::TYPE_LOGIN, 'code' => Yii::$app->session->id])->exists();
        $token = new Token();
        $token->user_id = $this->id;
        $token->code = Yii::$app->session->id;
        $token->type = Token::TYPE_LOGIN;
        if(!$oldToken){
           $token->save(false);
        }

    }
    
    public function create($sendMail = true)
    {
        if ($this->getIsNewRecord() == false) {
            throw new \RuntimeException('Calling "' . __CLASS__ . '::' . __METHOD__ . '" on existing user');
        }

        $transaction = $this->getDb()->beginTransaction();

        try {
            $this->confirmed_at = time();
            $this->password = $this->password == null ? Password::generate(8) : $this->password;

            $this->trigger(self::BEFORE_CREATE);

            if (!$this->save()) {
                $transaction->rollBack();
                return false;
            }

            if ($sendMail) {
                $this->mailer->sendWelcomeMessage($this, null, true);
            }
            $this->trigger(self::AFTER_CREATE);

            $transaction->commit();

            return true;
        } catch (\Exception $e) {
            var_dump($e->getMessage());die;
            $transaction->rollBack();
            return false;
        }
    }

    /**
     * This method is used to register new user account. If Module::enableConfirmation is set true, this method
     * will generate new confirmation token and use mailer to send it to the user.
     *
     * @return bool
     */
    public function register($sendMail = true)
    {
        if ($this->getIsNewRecord() == false) {
            throw new \RuntimeException('Calling "' . __CLASS__ . '::' . __METHOD__ . '" on existing user');
        }

        $transaction = $this->getDb()->beginTransaction();

        try {
            $this->confirmed_at = $this->module->enableConfirmation ? null : time();
            $this->password     = $this->module->enableGeneratingPassword ? Password::generate(8) : $this->password;

            $this->trigger(self::BEFORE_REGISTER);

            if (!$this->save()) {
                $transaction->rollBack();
                return false;
            }

            if ($this->module->enableConfirmation) {
                /** @var Token $token */
                $token = \Yii::createObject(['class' => Token::className(), 'type' => Token::TYPE_CONFIRMATION]);
                $token->link('user', $this);
            }
            if ($sendMail) {
                $this->mailer->sendWelcomeMessage($this, isset($token) ? $token : null);
            }
            $this->trigger(self::AFTER_REGISTER);

            $transaction->commit();

            return true;
        } catch (\Exception $e) {
            $transaction->rollBack();
            \Yii::warning($e->getMessage());
            throw $e;
        }
    }

    /**
     * Send mail welcome for new user in console
     */
    public function sendWelcomeEmailConsole()
    {
        $rootPath = \Yii::getAlias('@root');
        $log = \Yii::getAlias('@console/runtime/user-email-welcome.log');
        $errorLog = \Yii::getAlias('@console/runtime/user-email-welcome-error.log');
        // check log file exists
        FileHelper::createFile($log);
        FileHelper::createFile($errorLog);
        $command = "php {$rootPath}/yii user/mail/welcome {$this->id} > {$log} 2>>{$errorLog} &";
        exec($command);
    }

    public function hasRole($roleName) {
        $authManager = \Yii::$app->getAuthManager();
        return $authManager->getAssignment($roleName, $this->getId()) ? true : false;
    }

    public function updatePassword($password){
        return $this->resetPassword($password);
    }
}
