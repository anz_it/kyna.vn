<?php

namespace kyna\user\models;

use common\helpers\FbOfflineConversionHelper;
use common\helpers\StringHelper;
use kyna\commission\Affiliate;
use kyna\settings\models\Setting;
use Yii;
use kyna\course\models\Course;
use kyna\base\behaviors\ActionLogBehavior;
use kyna\user\models\actions\UserTelesaleAction;
use kyna\base\events\ActionEvent;
use kyna\user\models\TimeSlot;
use kyna\commission\models\AffiliateUser;

use common\helpers\RoleHelper;

/**
 * This is the model class for table "{{%user_telesales}}".
 *
 * @property integer $id
 * @property string $email
 * @property string $phone_number
 * @property string $full_name
 * @property string $street_address
 * @property integer $location_id
 * @property string $type
 * @property integer $user_id
 * @property integer $affiliate_id
 * @property integer $old_affiliate_id
 * @property integer $tel_id
 * @property string $form_name
 * @property string $note
 * @property integer $course_id
 * @property integer $bonus
 * @property double $amount
 * @property boolean $is_success
 * @property boolean $is_deleted
 * @property integer $last_call_date
 * @property integer $recall_date
 * @property integer $follow_num
 * @property integer $status
 * @property string $list_course_ids
 * @property integer $created_time
 * @property integer $updated_time
 * @property boolean $is_uploaded_fboc
 * @property string $page_slug
 * @property integer order_id
 */
class UserTelesale extends \kyna\base\ActiveRecord
{
    public $reregister_message;
    public $redirect_message;
    public $payment_type;

    const TYPE_ADVICE = 'advice';
    const TYPE_COD_ACTIVE = 'cod-active';
    const TYPE_COD_CONTACT = 'cod-lien-he';
    const TYPE_COD_THINKING = 'cod-suy-nghi';
    const TYPE_COD_PAY = 'cod-pay';
    const TYPE_COLD_CALL = 'cold-call';
    const TYPE_COMBO = 'combo';
    const TYPE_COMBO_INTERACTIVE = 'combo_tuong_tac';
    const TYPE_COMBO_REAL_ESTATE = 'combo_bds';
    const TYPE_CUSTOMER = 'customer';
    const TYPE_GIVE_DOC = 'give_docs';
    const TYPE_LIST_FACEBOOK = 'list-facebook';
    const TYPE_PRICING_PLAN = 'pricing_plan';
    const TYPE_SINGLE_COURSE = 'single_course';
    const TYPE_STUDENT_MANAGEMENT = 'student-management';
    const TYPE_USER_FREE = 'user-free';
    const TYPE_USER_REFUSE = 'user-tu-choi';
    const TYPE_MANA = 'mana';
    const TYPE_AUTO_CART = 'auto-cart';
    const TYPE_CONSULTANT_B2B = 'consultant-b2b';
    const TYPE_SALES_B2B = 'sales-b2b';
    const TYPE_SALES_4KID = 'sales-4kid';

    const STATUS_CANCELLED = -1;
    const STATUS_FOLLOW = 3;
    const STATUS_NO_ANSWER = 4;
    const STATUS_NO_RING = 5;

    
    const SCENARIO_NOTE = 'note';
    const SCENARIO_REREGISTER = 're-register';
    const SCENARIO_CREATE_ORDER = 'create-order';
    const SCENARIO_LANDING_PAGE = 'landing-page';
    const SCENARIO_SEND_EMAIL = 'send-email';
    const SCENARIO_REDIRECT_CHECKOUT = 'redirect-checkout';

    const EVENT_NOTED = 'note';
    const EVENT_REREGISTER = 're-register';
    const EVENT_CREATED_ORDER = 'created-order';
    const EVENT_LANDING_PAGE = 'landing-page';
    const EVENT_SEND_EMAIL = 'send-email';
    const EVENT_REDIRECT_CHECKOUT = 'redirect-checkout';

    const EMAIL_TYPE__KHONG_MUON_HOC_ONLINE = 0;
    const EMAIL_TYPE__QUAN_TAM_KHOA_HOC_KHAC = 1;
    const EMAIL_TYPE__CHI_PHI_KHONG_PHU_HOP = 2;
    const EMAIL_TYPE__HUONG_DAN_THANH_TOAN = 3;

    const SUB_EMAIL_TYPE__QUAN_TAM_KHOA_HOC_KHAC__VI_TINH = 0;
    const SUB_EMAIL_TYPE__QUAN_TAM_KHOA_HOC_KHAC__THIET_KE = 1;
    const SUB_EMAIL_TYPE__QUAN_TAM_KHOA_HOC_KHAC__PHU_NU = 2;
    const SUB_EMAIL_TYPE__QUAN_TAM_KHOA_HOC_KHAC__PTBT = 3;
    const SUB_EMAIL_TYPE__QUAN_TAM_KHOA_HOC_KHAC__NGOAI_NGU = 4;
    const SUB_EMAIL_TYPE__QUAN_TAM_KHOA_HOC_KHAC__NGHE_THUAT = 5;
    const SUB_EMAIL_TYPE__QUAN_TAM_KHOA_HOC_KHAC__NUOI_CON = 6;
    const SUB_EMAIL_TYPE__QUAN_TAM_KHOA_HOC_KHAC__MKT = 7;
    const SUB_EMAIL_TYPE__QUAN_TAM_KHOA_HOC_KHAC__KY_NANG = 8;
    const SUB_EMAIL_TYPE__QUAN_TAM_KHOA_HOC_KHAC__KE_TOAN = 9;
    const SUB_EMAIL_TYPE__QUAN_TAM_KHOA_HOC_KHAC__KD = 10;
    const SUB_EMAIL_TYPE__QUAN_TAM_KHOA_HOC_KHAC__ID = 11;
    const SUB_EMAIL_TYPE__QUAN_TAM_KHOA_HOC_KHAC__COMBO = 12;

    const EMAIL_TYPES = [
        self::EMAIL_TYPE__KHONG_MUON_HOC_ONLINE => [
            'name' => 'khong-thich-hoc-online',
            'text' => 'Khách hàng từ chối vì không thích học Online',
        ],
        self::EMAIL_TYPE__QUAN_TAM_KHOA_HOC_KHAC => [
            'name' => 'quan-tam-khoa-hoc-khac',
            'text' => 'Khách hàng từ chối vì quan tâm khóa học khác',
            'subType' => [
                self::SUB_EMAIL_TYPE__QUAN_TAM_KHOA_HOC_KHAC__VI_TINH => [
                    'name' => 'vitinh',
                    'text' => 'Vi tính văn phòng',
                ],
                self::SUB_EMAIL_TYPE__QUAN_TAM_KHOA_HOC_KHAC__THIET_KE => [
                    'name' => 'thietke',
                    'text' => 'Thiết kế',
                ],
                self::SUB_EMAIL_TYPE__QUAN_TAM_KHOA_HOC_KHAC__PHU_NU => [
                    'name' => 'phunu',
                    'text' => 'Phụ nữ',
                ],
                self::SUB_EMAIL_TYPE__QUAN_TAM_KHOA_HOC_KHAC__PTBT => [
                    'name' => 'PTBT',
                    'text' => 'Phát triển bản thân',
                ],
                self::SUB_EMAIL_TYPE__QUAN_TAM_KHOA_HOC_KHAC__NGOAI_NGU => [
                    'name' => 'ngoaingu',
                    'text' => 'Ngoại ngữ',
                ],
                self::SUB_EMAIL_TYPE__QUAN_TAM_KHOA_HOC_KHAC__NGHE_THUAT => [
                    'name' => 'nghethuat',
                    'text' => 'Nghệ thuật',
                ],
                self::SUB_EMAIL_TYPE__QUAN_TAM_KHOA_HOC_KHAC__NUOI_CON => [
                    'name' => 'nuoicon',
                    'text' => 'Nuôi con',
                ],
                self::SUB_EMAIL_TYPE__QUAN_TAM_KHOA_HOC_KHAC__MKT => [
                    'name' => 'marketing',
                    'text' => 'Marketing',
                ],
                self::SUB_EMAIL_TYPE__QUAN_TAM_KHOA_HOC_KHAC__KY_NANG => [
                    'name' => 'kynang',
                    'text' => 'Kỹ Năng',
                ],
                self::SUB_EMAIL_TYPE__QUAN_TAM_KHOA_HOC_KHAC__KE_TOAN => [
                    'name' => 'ketoan',
                    'text' => 'Kế toán'
                ],
                self::SUB_EMAIL_TYPE__QUAN_TAM_KHOA_HOC_KHAC__KD => [
                    'name' => 'kinhdoanh',
                    'text' => 'Kinh doanh'
                ],
                self::SUB_EMAIL_TYPE__QUAN_TAM_KHOA_HOC_KHAC__ID => [
                    'name' => 'IT',
                    'text' => 'IT',
                ],
//                self::SUB_EMAIL_TYPE__QUAN_TAM_KHOA_HOC_KHAC__COMBO => [
//                    'name' => 'Combo',
//                    'text' => 'Combo',
//                ],
            ],
        ],
        self::EMAIL_TYPE__CHI_PHI_KHONG_PHU_HOP => [
            'name' => 'chi-phi-khong-phu-hop',
            'text' => 'Khách hàng từ chối vì chi phí không phù hợp',
        ],
        self::EMAIL_TYPE__HUONG_DAN_THANH_TOAN => [
            'name' => 'huong-dan-thanh-toan',
            'text' => 'Hướng Dẫn thanh toán',
        ],
    ];

    public $email_type;
    public $email_sub_type;
    public $action_value;
    public $email_coupon;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user_telesales}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['location_id', 'user_id', 'affiliate_id', 'old_affiliate_id', 'tel_id', 'course_id', 'bonus', 'last_call_date', 'status', 'created_time', 'updated_time', 'follow_num'], 'integer'],
            [['note', 'list_course_ids', 'form_name','page_slug'], 'string'],
            [['amount'], 'number'],
            [['is_success', 'is_deleted'], 'boolean'],
            [['email', 'full_name'], 'string', 'max' => 100],
            [['phone_number'], 'string', 'max' => 20],
            [['street_address'], 'string', 'max' => 255],
            [['type'], 'string', 'max' => 45],
            [['email', 'location_id', 'street_address'], 'required', 'on' => 'create'],
            [['email', 'full_name', 'phone_number'], 'required', 'on' => self::SCENARIO_LANDING_PAGE],
            [['status', 'note'], 'required', 'on' => self::SCENARIO_NOTE],
            [['recall_date', 'reregister_message', 'payment_type', 'redirect_message'], 'safe'],
            [['email', 'full_name', 'phone_number', 'reregister_message'], 'required', 'on' => self::SCENARIO_REREGISTER],
            [['email_type'], 'required', 'on' => self::SCENARIO_SEND_EMAIL],
        ];
    }
    
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'email' => 'Email',
            'phone_number' => 'SĐT',
            'full_name' => 'Full Name',
            'street_address' => 'Street Address',
            'location_id' => 'Location ID',
            'type' => 'Type',
            'user_id' => 'User ID',
            'affiliate_id' => 'Affiliate ID',
            'old_affiliate_id' => 'Old Affiliate ID',
            'tel_id' => 'Telesale',
            'form_name' => 'Form Name',
            'note' => 'Ghi chú',
            'course_id' => 'Course ID',
            'bonus' => 'Bonus',
            'amount' => 'Amount',
            'is_success' => 'Is Success',
            'is_deleted' => 'Is Deleted',
            'last_call_date' => 'Last Call Date',
            'status' => 'Trạng thái',
            'created_time' => 'Created Time',
            'updated_time' => 'Updated Time',
            'recall_date' => 'Ngày sẽ gọi lại',
            'follow_num' => 'Số lần follow',
            'list_course_ids' => 'Danh sách khóa học/nhóm khóa học',
            'payment_type' => 'Hình thức thanh toán',
            'page_slug' => 'Slug Landing Page'
        ];
    }
    
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        
        $behaviors[] = [
            'class' => ActionLogBehavior::className(),
            'enableAction' => 'user_telesale',
            'modelClassName' => UserTelesaleAction::className(),
            'events' => [
                self::EVENT_NOTED => [],
                self::EVENT_CREATED_ORDER => [],
                self::EVENT_REREGISTER => [],
                self::EVENT_LANDING_PAGE => [],
                self::EVENT_SEND_EMAIL => [],
                self::EVENT_REDIRECT_CHECKOUT => [],
            ],
        ];

        return $behaviors;
    }
    
    public static function getTypes()
    {
        return [
            self::TYPE_ADVICE => 'Form tư vấn',
            self::TYPE_COD_ACTIVE => 'COD active',
            self::TYPE_COD_THINKING => 'COD suy nghĩ',
            self::TYPE_COD_CONTACT => 'COD liên hệ',
            self::TYPE_COD_PAY => 'COD pay',
            self::TYPE_COLD_CALL => 'Cold call',
            self::TYPE_COMBO => 'Combo khóa học',
            self::TYPE_COMBO_REAL_ESTATE => 'Combo BĐS',
            self::TYPE_COMBO_INTERACTIVE => 'Combo tương tác',
            self::TYPE_CUSTOMER => 'Old Customer',
            self::TYPE_GIVE_DOC => 'Nhận tài liệu',
            self::TYPE_LIST_FACEBOOK => 'List facebook',
            self::TYPE_PRICING_PLAN => 'Pricing plan',
            self::TYPE_SINGLE_COURSE => 'Single course',
            self::TYPE_STUDENT_MANAGEMENT => 'User from student management',
            self::TYPE_USER_FREE => 'User free',
            self::TYPE_USER_REFUSE => 'User từ chối',
            self::TYPE_MANA => 'Mana',
            self::TYPE_AUTO_CART => 'Shopping cart',
            self::TYPE_CONSULTANT_B2B => 'Consultant B2B',
            self::TYPE_SALES_B2B => 'Sales B2B',
            self::TYPE_SALES_4KID => 'Sales ForKid'
        ];
    }
    
    public function getTypeText()
    {
        $types = self::getTypes();
        
        return isset($types[$this->type]) ? $types[$this->type] : null;
    }
    
    public function getTeler()
    {
        return $this->hasOne(User::className(), ['id' => 'tel_id']);
    }
    
    public static function getBonusConfigs()
    {
        return [
            self::TYPE_ADVICE => 4,
            self::TYPE_USER_FREE => 3,
            self::TYPE_USER_REFUSE => 1,
            self::TYPE_CUSTOMER => 1,
            self::TYPE_COLD_CALL => 20,
            self::TYPE_COD_THINKING => 3,
            self::TYPE_COD_CONTACT  => 3,
            self::TYPE_LIST_FACEBOOK => 2,
            self::TYPE_COD_PAY => 5,
            self::TYPE_COD_ACTIVE => 5,
            self::TYPE_STUDENT_MANAGEMENT => 1,
            self::TYPE_GIVE_DOC => 10,
            self::TYPE_PRICING_PLAN => 2,
            self::TYPE_SINGLE_COURSE => 1,
            self::TYPE_COMBO => 2,
            self::TYPE_MANA => 1,
            self::TYPE_COMBO_INTERACTIVE => 1,
            self::TYPE_AUTO_CART => 0,
            self::TYPE_CONSULTANT_B2B => 2,
            self::TYPE_SALES_B2B => 2,
        ];
    }
    
    public function getCourse()
    {
        return $this->hasOne(Course::className(), ['id' => 'course_id']);
    }
    
    public static function listStatus()
    {
        return [
            self::STATUS_FOLLOW => 'Follow',
            self::STATUS_NO_ANSWER => 'Không nghe máy',
            self::STATUS_NO_RING => 'Không đổ chuông',
        ];
    }



    public static function listStatusForAffiliate()
    {
        return [
            self::STATUS_ACTIVE => 'Mới tạo',
            self::STATUS_FOLLOW => 'Telesales đang follow',
            self::STATUS_NO_ANSWER => 'Telesales đang follow',
            self::STATUS_NO_RING => 'Telesales đang follow',
            self::STATUS_CANCELLED => 'Bị hủy hoặc/và trả lại'
        ];
    }


    public static function getStatusLabelCss()
    {
        return [
            self::STATUS_ACTIVE => 'label-warning',
            self::STATUS_FOLLOW => 'label-warning',
            self::STATUS_NO_ANSWER => 'label-warning',
            self::STATUS_NO_RING => 'label-warning',
            self::STATUS_CANCELLED => 'label-default'

        ];
    }



    public static function getemailTypes()
    {
        return self::EMAIL_TYPES;
    }

    public function afterSave($insert, $changedAttributes)
    {
        $ret = parent::afterSave($insert, $changedAttributes);
        
        $event = new ActionEvent();
        
        if (!empty($this->recall_date)) {
            $event->addMeta(['recall_date' => $this->recall_date]);
        }

        if ($this->scenario === self::SCENARIO_LANDING_PAGE && $this->payment_type != '') {
            $event->addMeta(['note' => $this->payment_type]);
            $this->trigger(self::EVENT_LANDING_PAGE, $event);
        }
        
        if ($this->scenario === self::SCENARIO_NOTE) {
            $event->addMeta([
                'status' => $this->statusText,
                'note' => $this->note,
            ]);
            $this->trigger(self::EVENT_NOTED, $event);
        }

        if ($this->scenario === self::SCENARIO_REREGISTER) {
            $event->addMeta([
                'note' => $this->reregister_message,
            ]);
            $this->trigger(self::EVENT_REREGISTER, $event);
        }
        
        if ($this->scenario === self::SCENARIO_CREATE_ORDER) {
            $event->addMeta([
                'note' => $this->note,
            ]);

            $this->trigger(self::EVENT_CREATED_ORDER, $event);
        }

        if ($this->scenario === self::SCENARIO_REDIRECT_CHECKOUT) {
            $event->addMeta([
                'note' => $this->redirect_message,
            ]);

            $this->trigger(self::SCENARIO_REDIRECT_CHECKOUT, $event);
        }

        if ($this->scenario === self::SCENARIO_SEND_EMAIL) {
            $event->addMeta([
                'note' => 'Email - '. self::EMAIL_TYPES[$this->email_type]['text'],
            ]);
            if (isset($this->email_coupon)) {
                $event->addMeta([
                    'coupon' => $this->email_coupon,
                ]);
            }
            $this->trigger(self::EVENT_SEND_EMAIL, $event);
        }

        if (!$this->is_uploaded_fboc) {
            $isAutoUpload = Setting::findOne([
                'key' => 'seo_fbofflineconversion_is_upload_on_lead_create'
            ]);
            if (isset($isAutoUpload) && $isAutoUpload->value == true) {
                $lead = $this;
                // prepare params
                $lead = self::standardize($lead);
                $params = [
//            'upload_tag' => 'test_upload_tag',
//            'data_set_id' => '137414683575437',
                    'data' => [],
                ];
                $match_keys = [
                    'lead_id' => $lead['id'],
                    'country' => hash('sha256', 'VN'),
                ];
                if (!empty($lead['email']))
                    $match_keys['email'] = hash('sha256', $lead['email']);
                if (!empty($lead['phone_number']))
                    $match_keys['phone'] = hash('sha256', $lead['phone_number']);
                if (!empty($lead['full_name'])) {
                    // parse fullname
                    // Tên tùm lum quá có nên làm hay không?
                }
                if (!empty($lead['course_id']))
                    $content_ids = [$lead['course_id']];
                if (!empty($lead['list_course_ids'])) {
                    $content_ids = explode(',', $lead['list_course_ids']);
                }
                if (!empty($lead['amount'])) {
//                if (!empty($content_ids)) {
                    $entry = [
                        'match_keys' => $match_keys,
                        'event_name' => "Lead",
                        'event_time' => $lead['created_time'],
                        'content_type' => 'product',
//                        'content_ids' => $content_ids,
                        'value' => empty($lead['amount'])? 0 : $lead['amount'],
                        'currency' => 'VND',
                    ];
                    if (!empty($content_ids))
                        $entry['content_ids'] = $content_ids;
                    $data[] = $entry;

                    $params['data'] = $data;
                    $ret = FbOfflineConversionHelper::UploadEvents($params);

                    if ($ret['success']) {
                        self::updateAll([
                            'is_uploaded_fboc' => true
                        ], ['id' => $this->id]);
                    }
                }
            }
        }

        return $ret;
    }
    
    public function beforeSave($insert)
    {
        $ret = parent::beforeSave($insert);
        
        if (!empty($this->recall_date) && !is_int($this->recall_date)) {
            $dateTimeFormat = str_replace('php:', '', Yii::$app->formatter->datetimeFormat);
            
            $reCallDate = date_create_from_format($dateTimeFormat, trim($this->recall_date));
            if ($reCallDate !== false) {
                $this->recall_date = $reCallDate->getTimestamp();
            }
        }
        return $ret;
    }

    public function getAffiliateUser()
    {
        return $this->hasOne(AffiliateUser::className(), ['user_id' => 'affiliate_id']);
    }
    
    public function getAffiliate()
    {
        return $this->hasOne(User::className(), ['id' => 'affiliate_id']);
    }
    
    public function getOldAffiliate()
    {
        return $this->hasOne(User::className(), ['id' => 'old_affiliate_id']);
    }

    public function getOldAffiliateUser()
    {
        return $this->hasOne(AffiliateUser::className(), ['user_id' => 'old_affiliate_id']);
    }
    
    public function getCanAccess()
    {
        if (empty($this->tel_id) || Yii::$app->user->id === $this->tel_id) {
            return true;
        }

        $historyDataProvider = $this->actionHistoryTelesale(false, [
            self::EVENT_REREGISTER,
            self::EVENT_LANDING_PAGE,
            self::EVENT_REDIRECT_CHECKOUT
        ]);
        $logs = $historyDataProvider->getModels();

        return empty($logs);
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['email' => 'email']);
    }

    public function isValidEmail()
    {
        return filter_var($this->email, FILTER_VALIDATE_EMAIL);
    }

    public static function add($phone, $courseId = null, $userId = null)
    {
        $courseIds = [];
        if (is_array($courseId)) {
            $courseIds = $courseId;
        } else {
            $courseIds[] = $courseId;
        }

        $userTelesale = self::find()->where([
            'type' => self::TYPE_AUTO_CART,
            'phone_number' => $phone,
        ])
            ->andWhere(['>=', 'created_time', strtotime('today')])
            ->one();

        $needSave = false;
        if (is_null($userTelesale)) {
            $userTelesale = new self();

            $userTelesale->phone_number = $phone;
            $userTelesale->type = self::TYPE_AUTO_CART;
            $userTelesale->list_course_ids = implode(', ', $courseIds);
            $userTelesale->user_id = $userId;
            $userTelesale->tel_id = TimeSlot::getOperatorsInShift(RoleHelper::ROLE_TELESALE, self::TYPE_AUTO_CART);

            // detect exist affiliate
            $cookies = Yii::$app->request->cookies;
            if ($affiliateId = $cookies->getValue('affiliate_id')) {
                $userTelesale->affiliate_id = $affiliateId;
            }

            $needSave = true;
        } else {
            $oldCourses = explode(', ', $userTelesale->list_course_ids);
            $userTelesale->list_course_ids = implode(', ', array_unique(array_merge($oldCourses, $courseIds)));
            $userTelesale->created_time = time();

            $diff = array_merge(array_diff($oldCourses, $courseIds), array_diff($courseIds, $oldCourses));
            if (count($diff) > 0) {
                $needSave = true;
            }
        }

        if ($needSave) {
            $formNames = [];

            if (!empty($userTelesale->list_course_ids)) {
                $totalCourseIds = explode(', ', $userTelesale->list_course_ids);
                foreach ($totalCourseIds as $courseId) {
                    $course = Course::findOne($courseId);
                    if ($course != null) {
                        $formNames[] = $course->name;
                    }
                }

                $userTelesale->form_name = implode(', ', $formNames);
            }

            if (!$userTelesale->save(false)) {
                return false;
            }
        }

        return true;
    }

    public static function standardize($lead)
    {
        $ret = $lead;
        if (!empty($lead['phone_number'])) {
            $phone = $lead['phone_number'];

            $ret['phone_number'] = StringHelper::standardPhonumber($phone);
        }
        return $ret;
    }


}
