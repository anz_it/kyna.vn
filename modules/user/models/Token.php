<?php

namespace kyna\user\models;

use dektrium\user\models\Token as BaseToken;

class Token extends BaseToken
{
    const TYPE_LOGIN = 4;

    public function beforeSave($insert)
    {
        if ($this->isNewRecord) {
            static::deleteAll(['user_id' => $this->user_id, 'type' => $this->type]);
            $this->created_at = time();
        }
        return true;
    }
}
