<?php

namespace kyna\user\models;

use yii\data\ActiveDataProvider;
use dektrium\user\models\UserSearch as BaseUserSearch;

class UserSearch extends BaseUserSearch
{

    public $phone_number;
    public $name;

    public function safeAttributes()
    {
        return ['phone_number', 'name', 'email'];
    }

    /**
     * @param $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = $this->finder->getUserQuery();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
//        $this->attributes = $params;
        //var_dump($this->load(['UserSearch' => $params]));
        if (!($this->load(['UserSearch' => $params]) && $this->validate())) {
            //var_dump($this->validate());
            return $dataProvider;
        }

        if ($this->created_at !== null) {
            $date = strtotime($this->created_at);
            $query->andFilterWhere(['between', 'created_at', $date, $date + 3600 * 24]);
        }

        $query->andFilterWhere(['like', 'username', $this->username])
                ->andFilterWhere(['=', 'email', $this->email])
                ->andFilterWhere(['registration_ip' => $this->registration_ip]);

        return $dataProvider;
    }

}
