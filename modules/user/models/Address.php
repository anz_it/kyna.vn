<?php

namespace kyna\user\models;

use kyna\base\models\Location;
use common\helpers\StringHelper;

/**
 * This is the model class for table "user_addresses".
 *
 * @property int $id
 * @property int $user_id
 * @property string $contact_name
 * @property string $email
 * @property string $phone_number
 * @property string $street_address
 * @property int $location_id
 * @property int $status
 * @property int $created_time
 * @property int $updated_time
 */
class Address extends \kyna\base\models\BaseAddress
{
    //public $city_id;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user_addresses';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'contact_name', 'phone_number'], 'required'],
            [['user_id', 'location_id', 'status', 'created_time', 'updated_time'], 'integer'],
            [['contact_name', 'phone_number', 'street_address'], 'string', 'max' => 255],
            // pay cod
            [['street_addres', 'location_id'], 'required', 'on' => 'pay_cod'],
            // safe
            [['city_id', 'email'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'contact_name' => 'Tên liên hệ',
            'email' => 'Email',
            'phone_number' => 'Số điện thoại',
            'street_address' => 'Địa chỉ',
            'location_id' => 'Địa phương',
            'status' => 'Status',
            'created_time' => 'Created Time',
            'updated_time' => 'Updated Time',
        ];
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
