<?php

namespace kyna\user\models;

use Yii;
use kyna\base\models\MetaField;

/**
 * This is the model class for table "user_meta".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $meta_field_id
 * @property string $key
 * @property string $value
 */
class UserMeta extends \kyna\base\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_meta';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'meta_field_id'], 'required'],
            [['user_id', 'meta_field_id'], 'integer'],
            [['value'], 'required', 'on' => ['required', 'all'], 'message' => $this->metaField->name . ' không được để trống.'],
            [['value'], 'unique', 'targetAttribute' => ['key', 'value'], 'on' => ['unique', 'all'], 'comboNotUnique' => $this->metaField->name . ' không được trùng.'],
            [['value'], 'string'],
            [['key'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'meta_field_id' => 'Meta Field ID',
            'key' => 'Key',
            'value' => 'Value',
        ];
    }

    /**
     * @desc get relation MetaField
     * @return BEMetaField model
     */
    public function getMetaField()
    {
        return MetaField::find()->where(['model' => [MetaField::MODEL_USER, MetaField::MODEL_TEACHER], 'key' => $this->key])->one();
//        return $this->hasOne(MetaField::className(),['id' => 'meta_field_id']);
    }
}
