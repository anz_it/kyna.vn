<?php

namespace kyna\user\models;

use Yii;

/**
 * This is the model class for table "time_slot".
 *
 * @property int $id
 * @property string $start_time
 * @property string $end_time
 * @property string $monday
 * @property string $tuesday
 * @property string $wednesday
 * @property string $thursday
 * @property string $friday
 * @property string $saturday
 * @property string $sunday
 * @property string $type
 * @property string $user_type
 * @property int $related_id
 * @property int $status
 * @property int $created_time
 * @property int $updated_time
 */
class TimeSlot extends \kyna\base\ActiveRecord
{

    const USER_TYPE_DEFAULT = 'default';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'time_slot';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['start_time', 'end_time'], 'integer'],
            [['related_id', 'status', 'created_time', 'updated_time'], 'integer'],
            [['monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday', 'type'], 'string', 'max' => 2000],
            [['user_type', 'type'], 'string', 'max' => 45],
            [['startTime', 'endTime'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'startTime' => 'Bắt đầu',
            'endTime' => 'Kết thúc',
            'monday' => 'Thứ 2',
            'tuesday' => 'Thứ 3',
            'wednesday' => 'Thứ 4',
            'thursday' => 'Thứ 5',
            'friday' => 'Thứ 6',
            'saturday' => 'Thứ 7',
            'sunday' => 'Chủ nhật',
            'type' => 'Type',
            'user_type' => 'User Type',
            'related_id' => 'Related ID',
            'status' => 'Status',
            'created_time' => 'Created Time',
            'updated_time' => 'Updated Time',
        ];
    }

    public static function getUserTypes()
    {
        return [
            self::USER_TYPE_DEFAULT => 'Mặc định',
        ] + UserTelesale::getTypes();
    }

    public static function getDaysOfWeek()
    {
        return [
            'sunday' => 'Chủ nhật',
            'monday' => 'Thứ hai',
            'tuesday' => 'Thứ ba',
            'wednesday' => 'Thứ tư',
            'thursday' => 'Thứ năm',
            'friday' => 'Thứ sáu',
            'saturday' => 'Thứ bảy',
        ];
    }

    public function getStartTime()
    {
        $time = strtotime('today') + $this->start_time;
        return date('H:i', $time);
    }

    public function setStartTime($value)
    {
        $this->start_time = strtotime('today ' . $value) - strtotime('today');
    }

    public function getEndTime()
    {
        $time = strtotime('today') + $this->end_time + 1;
        return date('H:i', $time);
    }

    public function setEndTime($value)
    {
        $this->end_time = strtotime('today ' . $value) - strtotime('today') - 1;

        if ($this->end_time <= 0) {
            $this->end_time = 3600 * 24 - 1;
        }
    }

    public function beforeValidate()
    {
        $weekDays = array_keys(self::getDaysOfWeek());

        foreach ($weekDays as $day) {
            if (!empty($this->$day)) {
                $this->$day = implode(',', $this->$day);
            } else {
                $this->$day = null;
            }
        }

        return parent::beforeValidate();
    }

    public function afterFind()
    {
        $return = parent::afterFind();

        $weekDays = array_keys(self::getDaysOfWeek());

        foreach ($weekDays as $day) {
            $this->$day = explode(',', $this->$day);
            if (empty($this->$day)) {
                $this->$day = null;
            }
        }


        return $return;
    }

    public static function getOperatorsInShift($role = null, $user_type = 'default')
    {
        $now = time();
        $today = strtotime('today');

        $dowIndex = date('w', $now);
        $weekDays = array_keys(self::getDaysOfWeek());
        $dayOfWeek = $weekDays[$dowIndex];

        $timeslot = TimeSlot::find()
                        ->andWhere(['type' => $role])
                        ->andFilterWhere(['user_type' => $user_type])
                        ->andWhere('start_time <= :start')
                        ->andWhere('end_time >= :end')
                        ->addParams([
                            ':start' => $now - $today,
                            ':end' => $now - $today,
                        ])->select(['id', $dayOfWeek])->one();
        
        if (is_null($timeslot) && $user_type !== 'default') {
            $timeslot = TimeSlot::find()
                ->andWhere(['type' => $role])
                ->andFilterWhere(['user_type' => 'default'])
                ->andWhere('start_time <= :start')
                ->andWhere('end_time >= :end')
                ->addParams([
                    ':start' => $now - $today,
                    ':end' => $now - $today,
                ])->select(['id', $dayOfWeek])->one();
        }
        
        if (is_null($timeslot)) {
            return null;
        }
        
        $opertorIds = $timeslot->$dayOfWeek;
        
        $cacheKey = "TimeSlot_{$timeslot->id}_{$dayOfWeek}";
        $queue = Yii::$app->cache->get($cacheKey);
        
        if (!empty($queue)) {
            $diff = array_merge(array_diff($queue, $opertorIds), array_diff($opertorIds, $queue));
            if (count($diff) > 0) {
                $queue = $opertorIds;
            }
        } else {
            $queue = $opertorIds;
        }
        
        $first = array_shift($queue);
        array_push($queue, $first);
        
        Yii::$app->cache->set($cacheKey, $queue);
        
        return $first;
    }

    public function getOperators($day)
    {
        $operatorIds = !is_array($this->$day) ? explode(',', $this->$day) : $this->$day;
        
        return Operator::find()->with('profile')->where(['id' => $operatorIds])->select(['id', 'username', 'email'])->all();
    }

}
