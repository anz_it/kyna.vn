<?php

namespace kyna\faq\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "faq".
 *
 * @property integer $id
 * @property string $title
 * @property string $slug
 * @property integer $category_id
 * @property string $content
 * @property integer $order
 * @property integer $is_important
 * @property integer $is_deleted
 * @property integer $status
 * @property integer $created_time
 * @property integer $updated_time
 * @property mixed statusText
 * @property mixed category
 */
class Faq extends \kyna\base\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'faq';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'slug', 'is_important', 'category_id', 'content', 'order', 'status'], 'required'],
            [['category_id', 'order', 'is_important', 'is_deleted', 'status', 'created_time', 'updated_time'], 'integer'],
            [['content'], 'string'],
            [['title', 'slug'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'slug' => 'Slug',
            'category_id' => 'Danh mục',
            'content' => 'Nội dung',
            'order' => 'Order',
            'is_important' => 'Quan trọng',
            'is_deleted' => 'Is Deleted',
            'status' => 'Trạng thái',
            'created_time' => 'Created Time',
            'updated_time' => 'Updated Time',
        ];
    }
    public function getCategory()
    {
        return $this->hasOne(FaqCategory::className(), ['id' => 'category_id']);
    }

    protected static function softDelete()
    {
        return true;
    }
}
