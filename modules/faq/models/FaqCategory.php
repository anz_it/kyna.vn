<?php

namespace kyna\faq\models;

use common\helpers\CDNHelper;
use Yii;
use yii\web\UploadedFile;

/**
 * This is the model class for table "faq_categories".
 *
 * @property integer $id
 * @property string $title
 * @property string $image
 * @property integer $is_deleted
 * @property integer $status
 * @property integer $created_time
 * @property integer $updated_time
 * @property mixed statusText
 */
class FaqCategory extends \kyna\base\ActiveRecord
{
    public $imageSize = [
        'cover' => [],
        'contain' => [],
        'crop' => []
    ];
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'faq_categories';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'status'], 'required'],
            [['image'], 'required', 'on' => 'create'],
            [['is_deleted', 'status', 'created_time', 'updated_time'], 'integer'],
            [['title', 'image'], 'string', 'max' => 255],
            [['image'], 'validateImage', 'skipOnEmpty' => false],
        ];
    }

    public function validateImage($attribute, $params)
    {
        $image = UploadedFile::getInstance($this, $attribute);
        if (!$image && empty($this->oldAttributes[$attribute])) {
            $this->addError($attribute, 'Icon không được để trống');
            return false;
        }
        $oldAttributes = $this->oldAttributes;
        if (isset($oldAttributes[$attribute])) {
            $this->{$attribute} = $oldAttributes[$attribute];
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'image' => 'Icon',
            'is_deleted' => 'Is Deleted',
            'status' => 'Trạng Thái',
            'created_time' => 'Created Time',
            'updated_time' => 'Updated Time',
        ];
    }

    protected static function softDelete()
    {
        return true;
    }

    public static function findAllActive()
    {
        return self::find()->andWhere(['status' => self::STATUS_ACTIVE])->all();
    }

    public function getFaqs()
    {
        return $this->hasMany(Faq::className(), ['category_id' => 'id'])
            ->andWhere(['status' => Faq::STATUS_ACTIVE])
            ->orderBy('order ASC');
    }
}
