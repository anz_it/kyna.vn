<?php

namespace kyna\report\models;

use Yii;

/**
 * This is the model class for table "report_param".
 *
 * @property integer $id
 * @property integer $report_id
 * @property string $name
 * @property string $default_value
 * @property string $description
 */
class ReportParam extends \kyna\base\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'report_param';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['report_id', 'name'], 'required'],
            [['report_id'], 'integer'],
            [['name',], 'string', 'max' => 64],
            [['description','default_value'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'report_id' => 'Report ID',
            'name' => 'Name',
            'default_value' => 'Default Value',
            'description' => 'Description',
        ];
    }
}
