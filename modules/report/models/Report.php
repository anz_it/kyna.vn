<?php

namespace kyna\report\models;

use Yii;

/**
 * This is the model class for table "report".
 *
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property string $sql_command
 * @property integer $status
 * @property string $note
 * @property string $columns_name
 * @property integer $created_time
 * @property integer $updated_time
 */
class Report extends \kyna\base\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'report';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'sql_command','columns_name'], 'required'],
            [['status', 'created_time', 'updated_time'], 'integer'],
            [['name', 'columns_name'], 'string', 'max' => 256],
            [['sql_command'], 'string', 'max' => 5024],
            [['description',  'note'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'description' => 'Description',
            'sql_command' => 'Sql Command',
            'status' => 'Status',
            'note' => 'Note',
            'columns_name' => 'Columns name',
            'created_time' => 'Created Time',
            'updated_time' => 'Updated Time',
        ];
    }


    public function getReportParams()
    {
        return $this->hasMany(ReportParam::className(), ['report_id' => 'id']);
    }
}
