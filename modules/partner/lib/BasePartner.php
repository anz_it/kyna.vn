<?php

namespace kyna\partner\lib;

use kyna\servicecaller\BaseCaller;

abstract class BasePartner extends BaseCaller
{
    public $method;
    public $returnUrl;
    public $transNo;
    public $ipnResponse;
    public $errors;
    public $isCod = false;

    /**
     * Active Activation Code from partner
     * @param $orderId
     * @return mixed
     */
    abstract public function active($orderId);

    /**
     * Active Multi Activation Code from partner
     * @param $orderId
     * @param $partnerId
     * @param $data
     * @return mixed
     */
    abstract public function activeMulti($orderId, $partnerId, $data);

    /**
     * Query status of Activation Code
     * @param $transId
     * @return mixed
     */
    abstract public function query($transId);

    /**
     * Listen event changing status from partner side
     * @param $data
     * @return mixed
     */
    abstract public function ipn($data);
}
