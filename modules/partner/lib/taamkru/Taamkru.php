<?php
namespace kyna\partner\lib\taamkru;

use common\helpers\ArrayHelper;
use kyna\course\models\Course;
use kyna\order\models\Order;
use kyna\partner\lib\BasePartner;
use kyna\servicecaller\traits\CurlTrait;
use kyna\settings\models\Setting;
use kyna\partner\models\Category;
use kyna\partner\models\Code;
use kyna\partner\models\Transaction;
use Yii;

class Taamkru extends BasePartner
{

    use CurlTrait;

    private $_apiUrl = 'https://api.taamkru.com/kyna/1/activation';
    private $_apiSecretKey = 'vyuaboiYUTE!89009_)!UNOPDAP{=1[PLK:J!nmGF&^!@(>@!(*#_nmew0';
    public $_codeExpiration = '';
    public $_productExpiration = '';
    public $_num_activations = 3;

    protected static $defaultMethod = 'POST';

    public function signIn()
    {
        // TODO: Implement signIn() method.
    }

    public function ipn($data)
    {
        // TODO: Implement ipn() method.
    }

    protected function beforeCall(&$ch, &$data)
    {
        $headers = array();
        $headers[] = 'Content-Type: application/json';
        $data = json_encode($data);

        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    }

    public function __construct()
    {
        if (isset(Yii::$app->params['taamkru']) && $configs = Yii::$app->params['taamkru']) {
            $this->_apiUrl = $configs['_apiUrl'];
            $this->_apiSecretKey = $configs['_apiSecretKey'];
            $this->_codeExpiration = $configs['_code_expiration'];
            $this->_productExpiration = $configs['_product_expiration'];
            $this->_num_activations = $configs['_num_activations'];
        }
    }

    public function active($orderId)
    {
        $order = Order::findOne($orderId);
        $partnerCode = $order->partnerCode;
        $category = null;
        foreach ($order->details as $detail) {
            $courseId = ($detail->course_combo_id > 0) ? $detail->course_combo_id : $detail->course_id;
            $category = Category::findOne([
                'course_id' => $courseId
            ]);
            break;
        }
        if ($partnerCode == null || $category == null) {
            return false;
        }
        $data = [
            "code" => $partnerCode->code,
            "code_expiration" => $this->_codeExpiration,
            "product_expiration" => $this->_productExpiration,
            $category->key => $category->value,
            "num_activations" => $this->_num_activations
        ];
        $command = '';
        $params = [
            "secret" => $this->_apiSecretKey,
            "activations" => [
                $data
            ]
        ];
        $endpoint = $this->_buildUrl($command);
        $result = $this->call($endpoint, $params);
        return $this->_return($result, $orderId, $params['activations'], $partnerCode->partner->id);
    }

    public function activeMulti($orderId, $partnerId, $data)
    {
        $activations = [];
        foreach ($data as $courseId => $info) {
            $category = Category::findOne([
                'course_id' => $courseId
            ]);
            if ($category == null) {
                return false;
            }
            $activations[] = [
                "code" => $info['activation_code'],
                "code_expiration" => $this->_codeExpiration,
                "product_expiration" => $this->_productExpiration,
                $category->key => $category->value,
                "num_activations" => $this->_num_activations
            ];
        }
        $command = '';
        $params = [
            "secret" => $this->_apiSecretKey,
            "activations" => $activations
        ];
        $endpoint = $this->_buildUrl($command);
        $result = $this->call($endpoint, $params);
        return $this->_return($result, $orderId, $activations, $partnerId);
    }

    public function query($transId)
    {
        // TODO: Implement query() method.
        $transaction = Transaction::findOne($transId);
        if ($transaction) {
            $command = '/' . $transaction->code;
            $params = [
                "secret" => $this->_apiSecretKey,
            ];
            $endpoint = $this->_buildUrl($command, $params);
            $result = $this->call($endpoint, $params, 'GET');
            return json_decode($result, true);
        }
        return false;
    }

    private function _buildUrl($command, $params = false)
    {
        $url = $this->_apiUrl . $command;
        if ($params) {
            $url .= '?' . http_build_query($params);
        }

        return $url;
    }

    /**
     * @param $response (status, message)
     * @param $orderId
     * @param $activations
     * @param $partnerId
     * @return array
     *  0 - Success
    1 - Generic error. If you experience this, something is seriously wrong, so please contact us.
    2 - Cannot find the specified module_id or category_id
    3 - The provided activation code already exists
     */
    private function _return($response, $orderId, $activations, $partnerId)
    {
        $response = json_decode($response, true);
        // save transaction
        $status = isset($response['status']) ? intval($response['status']) : Transaction::STATUS_NOT_RESPONSE;
        foreach ($activations as $item) {
            $transaction = new Transaction();
            $data['Transaction'] = [
                'order_id' => $orderId,
                'code' => $item['code'],
                'category_id' => isset($item['category_id']) ? $item['category_id'] : $item['module_id'],
                'num_activations' => $item['num_activations'],
                'code_expiration' => $item['code_expiration'],
                'product_expiration' => $item['product_expiration'],
                'status' => $status,
                'message' => isset($response['message']) ? $response['message'] : '',
                'created_by' => (isset(Yii::$app->user) && !Yii::$app->user->getIsGuest()) ? Yii::$app->user->id : 0,
                'partner_id' => $partnerId
            ];
            if ($transaction->load($data) && $transaction->save()) {
                unset($transaction);
                unset($data);
            }
        }
        $result['status'] = ($status == Transaction::STATUS_COMPLETE) ? true : false;
        return $result;
    }
}