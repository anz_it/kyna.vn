<?php
namespace kyna\partner\lib\yoons;

use common\helpers\ArrayHelper;
use kyna\course\models\Course;
use kyna\order\models\Order;
use kyna\partner\lib\BasePartner;
use kyna\servicecaller\traits\CurlTrait;
use kyna\servicecaller\traits\MultiCurlTrait;
use kyna\settings\models\Setting;
use kyna\partner\models\Category;
use kyna\partner\models\Code;
use kyna\partner\models\Transaction;
use Yii;

class Yoons extends BasePartner
{
    const CODE_PREFIX = "KN";
    const SEND_MAIL = true;

    use MultiCurlTrait;

    private $_apiUrlArray = [];

    public function signIn()
    {
        // TODO: Implement signIn() method.
    }

    public function ipn($data)
    {
        // TODO: Implement ipn() method.

    }

    protected function callerOptions()
    {
        $headers = [
            'Content-Type: application/x-www-form-urlencoded; charset=utf-8'
        ];
        return [
            CURLOPT_HTTPHEADER => $headers
        ];
    }

    protected function beforeCall(&$data)
    {
        foreach ($data as $index => $item) {
            if (!$item['data'] or !is_array($item['data'])) {
                $data[$index]['data'] = [];
            }
            $data[$index]['data'] = http_build_query($data[$index]['data']);
        }
    }

    public function __construct()
    {
        if (empty(Yii::$app->params['yoons']) || empty(Yii::$app->params['yoons']['_apiUrlArray'])) {
            return false;
        }
        $configs = Yii::$app->params['yoons'];
        $this->_apiUrlArray = $configs['_apiUrlArray'];
    }

    public function active($orderId)
    {
        // TODO: Implement active() method.
    }

    public function activeMulti($orderId, $partnerId, $data)
    {
        $activations = [];
        $order = Order::findOne($orderId);
        $category = null;
        foreach ($data as $courseId => $info) {
            $category = Category::findOne([
                'course_id' => $courseId
            ]);
            if ($category == null || empty($order)) {
                return false;
            }
            $activations[] = [
                "codenum" => $info['activation_code'],
                "codename" => "Mã kích hoạt của Kyna",
                "agentcode" => 1,
                "agentname" => "Kyna.vn",
                "schoolcode" => 1
            ];
        }
        $command = '';
        $params = [
            "apiType" => "cert_code_create",
            "activation" => $activations
        ];
        $apiData = [];
        foreach ($this->_apiUrlArray as $apiUrl) {
            $apiData[] = [
                'url' => $this->_buildUrl($apiUrl, $command),
                'method' => 'POST',
                'data' => $params
            ];
        }
        $result = $this->call($apiData);
        return $this->_return($result, $orderId, $activations, $partnerId, $category);
    }

    public function query($transId)
    {
        // TODO: Implement query() method.
        return false;
    }

    private function _buildUrl($baseUrl, $command, $params = false)
    {
        $url = $baseUrl . $command;
        if ($params) {
            $url .= '?' . http_build_query($params);
        }

        return $url;
    }

    /**
     * @param $response (status, message)
     * @param $orderId
     * @param $activations
     * @param $partnerId
     * @return array
     *  0 - Success
    1 - Generic error. If you experience this, something is seriously wrong, so please contact us.
    2 - Cannot find the specified module_id or category_id
    3 - The provided activation code already exists
     */
    private function _return($response, $orderId, $activations, $partnerId, $category)
    {
        $result = $this->_formatResponse($response);
        // save transaction
        foreach ($activations as $item) {
            $transaction = new Transaction();
            $data['Transaction'] = [
                'order_id' => $orderId,
                'code' => $item['codenum'],
                'category_id' => $category->value,
                'num_activations' => 0,
                'code_expiration' => null,
                'product_expiration' => null,
                'status' => $result['status'],
                'message' => $result['message'],
                'created_by' => (isset(Yii::$app->user) && !Yii::$app->user->getIsGuest()) ? Yii::$app->user->id : 0,
                'partner_id' => $partnerId
            ];
            if ($transaction->load($data) && $transaction->save()) {
                unset($transaction);
                unset($data);
            }
        }
        return [
            'status' => ($result['status'] == Transaction::STATUS_COMPLETE) ? true : false
        ];
    }

    private function _formatResponse($response)
    {
        $message = '';
        $generalStatus = Transaction::STATUS_COMPLETE;
        if (empty($response)) {
            $generalStatus = Transaction::STATUS_NOT_RESPONSE;
        }
        foreach ($response as $index => $item) {
            $result = json_decode($item, true);
            $resultCode = isset($result['resultCode']) ? intval($result['resultCode']) : Transaction::STATUS_NOT_RESPONSE;
            $message .= "{$result['partner']} : ";
            switch ($resultCode) {
                case "0000":
                    $status = Transaction::STATUS_COMPLETE;
                    break;
                case "9001":
                case "9002":
                case "9003":
                    $status = Transaction::STATUS_NOT_AVAILABLE;
                    break;
                case "9004":
                    $status = Transaction::STATUS_EXISTS;
                    break;
                case "5000":
                case "5001":
                    $status = Transaction::STATUS_GENERIC_ERROR;
                    break;
                default:
                    $status = Transaction::STATUS_NOT_RESPONSE;
            }
            $message .= Transaction::listStatus()[$status] . " ({$resultCode})" . '<br>';
            $generalStatus = $generalStatus == Transaction::STATUS_COMPLETE ? $status: $generalStatus;
        }
        return [
            'message' => $message,
            'status' => $generalStatus
        ];
    }
}
