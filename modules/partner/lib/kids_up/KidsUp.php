<?php
namespace kyna\partner\lib\kids_up;

use common\helpers\ArrayHelper;
use kyna\course\models\Course;
use kyna\order\models\Order;
use kyna\partner\lib\BasePartner;
use kyna\servicecaller\traits\CurlTrait;
use kyna\settings\models\Setting;
use kyna\partner\models\Category;
use kyna\partner\models\Code;
use kyna\partner\models\Transaction;
use Yii;

class KidsUp extends BasePartner
{
    const CODE_PREFIX = "KN";
    const SEND_MAIL = true;

    use CurlTrait;

    private $_apiUrl = 'https://api.kidsup.net';
    private $_userName = 'kidsup@kyna.vn';
    private $_passWord = 'X8yfhXoudyHTAoaRyLksZTmiPVNhAE89MCxStSsrDcH7zZ63VU';

    protected static $defaultMethod = 'POST';

    public function signIn()
    {
        // TODO: Implement signIn() method.
    }

    public function ipn($data)
    {
        // TODO: Implement ipn() method.

    }

    protected function beforeCall(&$ch, &$data)
    {
        $headers = [
            'Content-Type:application/json',
            'Authorization: Basic '. base64_encode("{$this->_userName}:{$this->_passWord}")
        ];
        $data = json_encode($data);

        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    }

    public function __construct()
    {
        if (isset(Yii::$app->params['kids_up']) && $configs = Yii::$app->params['kids_up']) {
            $this->_apiUrl = $configs['_apiUrl'];
            $this->_userName = $configs['_apiUserName'];
            $this->_passWord = $configs['_apiPassWord'];
        }
    }

    public function active($orderId)
    {
        // TODO: Implement active() method.
    }

    public function activeMulti($orderId, $partnerId, $data)
    {
        $activations = [];
        $order = Order::findOne($orderId);
        foreach ($data as $courseId => $info) {
            $category = Category::findOne([
                'course_id' => $courseId
            ]);
            if ($category == null || empty($order)) {
                return false;
            }
            $activations[] = [
                "email" => $order->user->email,
                "activateCode" => $info['activation_code'],
                "phone" => "",
                "name" => "",
                "address" => ""
            ];
        }
        $command = '/partner/activate';
        $params = $activations;
        $endpoint = $this->_buildUrl($command);
        $result = $this->call($endpoint, $params);
        return $this->_return($result, $orderId, $activations, $partnerId, $category);
    }

    public function query($transId)
    {
        // TODO: Implement query() method.
        $transaction = Transaction::findOne($transId);
        if ($transaction) {
            $command = '/partner/activation/' . $transaction->code;
            $params = null;
            $endpoint = $this->_buildUrl($command, $params);
            $result = $this->call($endpoint, $params, 'GET');
            $result = json_decode($result, true);
            return !empty($result['response']) ? $result['response'] : [];
        }
        return false;
    }

    private function _buildUrl($command, $params = false)
    {
        $url = $this->_apiUrl . $command;
        if ($params) {
            $url .= '?' . http_build_query($params);
        }

        return $url;
    }

    /**
     * @param $response (status, message)
     * @param $orderId
     * @param $activations
     * @param $partnerId
     * @return array
     *  0 - Success
    1 - Generic error. If you experience this, something is seriously wrong, so please contact us.
    2 - Cannot find the specified module_id or category_id
    3 - The provided activation code already exists
     */
    private function _return($response, $orderId, $activations, $partnerId, $category)
    {
        $response = json_decode($response, true);
        $status = Transaction::STATUS_NOT_RESPONSE;
        $message = '';
        if (!empty($response['code'])) {
            $status = Transaction::STATUS_GENERIC_ERROR;
            $message = !empty($response['message']) ? $response['message'] : '';
        }
        // save transaction
        foreach ($activations as $item) {
            if (!empty($response['response'][$item['activateCode']])) {
                $responseArray = $response['response'][$item['activateCode']];
                switch ($responseArray['code']) {
                    case 0:
                        $status = Transaction::STATUS_GENERIC_ERROR;
                        break;
                    case 1:
                        $status = Transaction::STATUS_COMPLETE;
                        break;
                    case -1:
                        $status = Transaction::STATUS_GENERIC_ERROR;
                        break;
                }
                $message = !empty($responseArray['message']) ? $responseArray['message'] : $message;
            }
            $transaction = new Transaction();
            $data['Transaction'] = [
                'order_id' => $orderId,
                'code' => $item['activateCode'],
                'category_id' => $category->value,
                'num_activations' => 2,
                'status' => $status,
                'message' => $message,
                'created_by' => (isset(Yii::$app->user) && !Yii::$app->user->getIsGuest()) ? Yii::$app->user->id : 0,
                'partner_id' => $partnerId
            ];
            if ($transaction->load($data) && $transaction->save()) {
                unset($transaction);
                unset($data);
            }
        }
        $result['status'] = ($status == Transaction::STATUS_COMPLETE) ? true : false;
        return $result;
    }
}