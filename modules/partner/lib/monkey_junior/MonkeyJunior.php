<?php
namespace kyna\partner\lib\monkey_junior;

use common\helpers\ArrayHelper;
use kyna\course\models\Course;
use kyna\order\models\Order;
use kyna\partner\lib\BasePartner;
use kyna\servicecaller\traits\CurlTrait;
use kyna\settings\models\Setting;
use kyna\partner\models\Category;
use kyna\partner\models\Code;
use kyna\partner\models\Transaction;
use Yii;

class MonkeyJunior extends BasePartner
{

    use CurlTrait;

    private $_apiUrl = 'http://behocchu.com/api/service';
    private $_apiSecretKey = '312bd01a479f1bfbe07c8d0a084096d6';
    private $_categoryObject = [
        '204' => 'com.earlystart.us.full',
        '205' => 'com.earlystart.ltr.1year',
        '523' => 'com.earlystart.vn.full',
        '773' => 'com.earlystart.zh.full',
        '774' => 'com.earlystart.uk.full',
        '775' => 'com.earlystart.vnn.full',
        '776' => 'com.earlystart.fr.full',
        '777' => 'com.earlystart.sp.full',
        '858' => 'com.earlystart.stories.6month',
        '859' => 'com.earlystart.stories.1year',
        '860' => 'com.earlystart.stories.lifetime',
        '1130' => 'com.earlystart.ltr.2year',
        '1131' => 'com.earlystart.ltr.4year',
    ];

    const SERVICE_PACKAGE_QUANTITY = 1;

    protected static $defaultMethod = 'POST';

    public function signIn()
    {
        // TODO: Implement signIn() method.
    }

    public function ipn($data)
    {
        // TODO: Implement ipn() method.

    }

    protected function beforeCall(&$ch, &$data)
    {
        $headers = array();
        $headers[] = 'Content-Type: application/x-www-form-urlencoded; charset=utf-8';
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        if (!$data or !is_array($data)) {
            $data = [];
        }

        $data = http_build_query($data);
    }

    public function __construct()
    {
        if (isset(Yii::$app->params['monkey_junior']) && $configs = Yii::$app->params['monkey_junior']) {
            $this->_apiUrl = $configs['_apiUrl'];
            $this->_apiSecretKey = $configs['_apiSecretKey'];
            $this->_categoryObject = $configs['_categoryObject'];
        }
    }

    public function active($orderId)
    {
        $order = Order::findOne($orderId);
        $package = null;
        foreach ($order->details as $detail) {
            $courseId = $detail->course_id;
            if (array_key_exists($courseId, $this->_categoryObject)) {
                $package[$this->_categoryObject[$courseId]] = self::SERVICE_PACKAGE_QUANTITY;
            }
        }

        if ($package == null) {
            return false;
        }

        $command = 'createOrder';
        $params = [
            "monkey_secure_secret" => $this->_apiSecretKey,
            "email" => $order->user->email,
            "list_package" => json_encode($package),
            "phone" => $order->user->profile->phone_number,
            "address" => $order->user->userAddress ? $order->user->userAddress->addressText : '',
            "name" => $order->user->profile->name
        ];
        $endpoint = $this->_buildUrl($command);
        $result = $this->call($endpoint, $params);
        return $this->_return($result, $orderId, $params);
    }

    public function activeMulti($orderId, $partnerId, $data)
    {
        // TODO: Implement activeMulti() method.
    }

    public function query($transId)
    {
        // TODO: Implement query() method.
    }

    private function _buildUrl($command, $params = false)
    {
        $url = $this->_apiUrl . '/' . $command;
        if ($params) {
            $url .= '?' . http_build_query($params);
        }

        return $url;
    }

    /**
     * @param $response (status, message)
     * @param $orderId
     * @param $data
     * @return array
     *  0 - Success
    1 - Generic error. If you experience this, something is seriously wrong, so please contact us.
    2 - Cannot find the specified module_id or category_id
    3 - The provided activation code already exists
     */
    private function _return($response, $orderId, $data)
    {
        $response = json_decode($response, true);
        $result = [
            'status' => false,
            'orderId' => $orderId,
            'response' => $response,
            'data' => http_build_query($data)
        ];
        if (isset($response['status']) && $response['status'] == "success") {
            $result['status'] = true;
        }
        \Yii::info($result, 'monkey');
        return $result;
    }

}