<?php

namespace kyna\partner\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use kyna\partner\models\Code;
use kyna\order\models\Order;

/**
 * CodeSearch represents the model behind the search form about `kyna\partner\models\Code`.
 */
class CodeSearch extends Code
{
    public $payment_method;
    public $retailer_id_array = [];
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'retailer_id', 'status', 'is_deleted', 'updated_time', 'activation_date', 'partner_id'], 'integer'],
            [['created_time'], 'string'],
            [['serial', 'code', 'payment_method', 'retailer_id_array'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Code::find();
        $query->alias('c');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'c.id' => $this->id,
            'c.retailer_id' => $this->retailer_id,
            'c.status' => $this->status,
            'c.is_deleted' => $this->is_deleted,
            //'created_time' => $this->created_time,
            'c.updated_time' => $this->updated_time,
            'c.activation_date' => $this->activation_date,
            'partner_id' => $this->partner_id
        ]);

        $query->andFilterWhere(['like', 'c.serial', $this->serial])
            ->andFilterWhere(['like', 'c.code', $this->code]);

        if (!empty($this->created_time)) {
            $date = \DateTime::createFromFormat('d/m/Y', $this->created_time);
            $query->andFilterWhere([
                'date(from_unixtime(c.created_time))' => $date->format('Y-m-d')
            ]);
        }

        switch ($this->payment_method) {
            case Code::PAYMENT_METHOD_ONLINE:
                $query->join('INNER JOIN', Order::tableName(), Order::tableName() . '.activation_code = c.serial');
                $query->andFilterWhere([
                    Order::tableName() . '.payment_method' => 'auto',
                    Order::tableName() . '.status' => Order::ORDER_STATUS_COMPLETE,
                ]);
                break;
            case Code::PAYMENT_METHOD_COD:
                $query->join('LEFT JOIN', Order::tableName(), Order::tableName() . '.activation_code = c.serial');
                $query->andFilterWhere([
                    'OR',
                    ['NOT IN', Order::tableName() . '.payment_method', ['auto']],
                    ['c.status' => [Code::CODE_STATUS_IN_STORE, Code::CODE_STATUS_IN_ACTIVE]],
                ]);
                break;
            default:
                break;
        }

        $query->groupBy('c.code');

        if (!empty($this->retailer_id_array)) {
            $query->andFilterWhere(['IN', 'c.retailer_id', $this->retailer_id_array]);
        }

        $query->orderBy('c.id DESC');

        return $dataProvider;
    }
}
