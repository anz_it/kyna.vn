<?php

namespace kyna\partner\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use kyna\partner\models\Category;

/**
 * CategorySearch represents the model behind the search form about `kyna\partner\models\Category`.
 */
class CategorySearch extends Category
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'value', 'course_id', 'status', 'is_deleted', 'created_time', 'updated_time', 'partner_id'], 'integer'],
            [['title', 'key'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Category::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'key' => $this->key,
            'value' => $this->value,
            'course_id' => $this->course_id,
            'status' => $this->status,
            'is_deleted' => $this->is_deleted,
            'created_time' => $this->created_time,
            'updated_time' => $this->updated_time,
            'partner_id' => $this->partner_id
        ]);

        $query->andFilterWhere(['like', 'title', $this->title]);

        $query->orderBy('id DESC');

        return $dataProvider;
    }
}