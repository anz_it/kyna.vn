<?php

namespace kyna\partner\models\search;

use yii\data\ActiveDataProvider;
use common\validators\PhoneNumberValidator;
use kyna\course\models\Course;
use kyna\partner\models\Code;
use kyna\partner\models\Retailer;
use kyna\order\models\Order;
use kyna\order\models\OrderMeta;

/**
 * OrderSearch represents the model behind the search form about `common\models\Order`.
 */
class OrderSearch extends Order //\yii\base\Model
{
    public $id;
    public $search;
    public $from_date;
    public $to_date;
    public $operator_id;
    public $user_id;
    public $phone_number;
    public $payment_method;
    public $order_ids;
    public $call_status = false;
    public $print_type;
    public $location;

    public $forceEmpty = false;

    public $allowedStatus = false;

    public $retailer_id;

    public $partner_id;

    public $retailer_id_array = [];

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'point_of_sale'], 'safe'],
            ['status', 'validateStatus'],
            [['from_date', 'to_date', 'operator_id', 'print_type', 'location', 'retailer_id'], 'integer'],
            [['phone_number'], PhoneNumberValidator::className()],
            [['retailer_id_array', 'partner_id'], 'safe']
        ];
    }

    public function validateStatus($attribute, $params)
    {
        $allowedStatus = $this->allowedStatus;
        $status = $this->$attribute;

        if ($status == (int) $status) {
            if ($allowedStatus AND !in_array($status, $allowedStatus)) {
                $this->addError($attribute, 'Status must be a subset of `allowedStatus`');
            }
        }
        elseif (is_array($status)) {
            $status = filter_var($status, FILTER_VALIDATE_INT, array(
              'flags'   => FILTER_REQUIRE_ARRAY,
            ));
            if (array_filter($status, 'is_int') === $status) {
                if ($allowedStatus AND (count(array_intersect($status, $allowedStatus)) != count($status))) {
                    $this->addError($attribute, 'Status must be a subset of `allowedStatus`');
                }
            }
            else {
                $this->addError($attribute, 'Status must be integer or array of integer');
            }
        }
        else {
            $this->addError($attribute, 'Status must be integer or array of integer');
        }
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        return \yii\base\Model::scenarios();
    }

    public function safeAttributes()
    {
        //return parent::safeAttributes();
        return [
            'id',
            'search',
            'from_date',
            'to_date',
            'status',
            'operator_id',
            'phone_number',
            'user_id',
            'order_ids',
            'affiliate_id',
            'call_status',
            'print_type',
            'forceEmpty',
            'promotion_code',
            'retailer_id',
            'partner_id'
        ];
    }

    /**
     * Creates data provider instance with search query applied.
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Order::find();
        $query->alias('t');
        $query->andWhere(['t.is_done_telesale_process' => $this->is_done_telesale_process]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);
        if (!$this->validate() or $this->forceEmpty) {
            $query->where('0=1');

            return $dataProvider;
        }

        if ($this->id) {
            $query->andFilterWhere(['id' => $this->id]);
            return $dataProvider;
        }

        if ($this->phone_number) {
            $query->joinWith('orderShipping os')
                ->andFilterWhere(['os.phone_number' => $this->phone_number]);;
        }

        $query->innerJoin('order_details d','t.id = d.order_id')
            ->innerJoin('courses c', 'd.course_id = c.id')
            ->andFilterWhere(['c.type' => Course::TYPE_SOFTWARE]);
        $query->groupBy('t.id');

        if (!$this->to_date) {
            $this->to_date = strtotime('tomorrow 0:00');
        }
        if (!$this->from_date) {
            $this->from_date = $this->to_date - 24*7*3600;
        }

        if (\Yii::$app->controller->action->id == 'print-cod') {
            $dateSearchField = 't.order_date';
            $this->to_date += 1;
        }
        else {
            $dateSearchField = 't.created_time';
            $this->to_date += 24*3600;
        }

        $query->andFilterWhere(['between', $dateSearchField, $this->from_date, $this->to_date]);

        if ($status = $this->status) {
            if (is_array($this->status)) {
                $status = filter_var($this->status, FILTER_VALIDATE_INT, [
                  'flags'   => FILTER_REQUIRE_ARRAY,
                ]);
            }
        }
        
        if ($this->operator_id) {
            $query->andWhere(['or', '`operator_id`=:operatorId', '`reference_id`=:operatorId'], [
                ':operatorId' => $this->operator_id
            ]);
        }
        if (!empty($this->affiliate_id)) {
            $query->andWhere(['affiliate_id' => $this->affiliate_id]);
        }

        if (!empty($this->call_status)) {
            $orderMetaTable = OrderMeta::tableName();
            $query->joinWith('orderMeta');
            $query->andWhere([$orderMetaTable . '.value' => $this->call_status, '`key`' => 'call_status']);
        }

        /*
         * Check user_id as array
         */
        if (is_array($this->user_id)) {
            $query->andFilterWhere(['IN' , 't.user_id', $this->user_id]);
        } else {
            $query->andFilterWhere(['t.user_id' => $this->user_id]);
        }

        $query->join('INNER JOIN', Code::tableName(), Code::tableName() . '.serial = d.activation_code');
        if (!empty($this->partner_id) || !empty($this->retailer_id) || !empty($this->retailer_id_array)) {
            if (!empty($this->partner_id)) {
                $query->andFilterWhere([
                    Code::tableName() . '.partner_id' => $this->partner_id
                ]);
            }
            if (!empty($this->retailer_id)) {
                $query->andFilterWhere([
                    Code::tableName() . '.retailer_id' => $this->retailer_id
                ]);
            }
            if (!empty($this->retailer_id_array)) {
                $query->andFilterWhere(['IN', Code::tableName() . '.retailer_id', $this->retailer_id_array]);
            }
        }

        $query->andFilterWhere(['payment_method' => $this->payment_method])
            ->andFilterWhere(['t.status' => $status])
            ->andFilterWhere(['t.id' => $this->order_ids])
            ->andFilterWhere(['t.user_id' => $this->user_id])
            ->andFilterWhere(['t.promotion_code' => $this->promotion_code]);

        $query->orderBy('id DESC');

        return $dataProvider;
    }

    public function getRetailer()
    {
        return $this->hasOne(Retailer::className(), ['id' => 'retailer_id']);
    }
}