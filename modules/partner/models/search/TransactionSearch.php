<?php

namespace kyna\partner\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use kyna\partner\models\Transaction;
use kyna\partner\models\Category;

/**
 * TransactionSearch represents the model behind the search form about `kyna\partner\models\Transaction`.
 */
class TransactionSearch extends Transaction
{
    public $partner_category_id;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'order_id', 'category_id', 'partner_category_id', 'partner_id', 'num_activations', 'code_expiration', 'product_expiration', 'status', 'created_by', 'created_time', 'updated_time'], 'integer'],
            [['code', 'message'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Transaction::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // get partner from category
        $category = Category::findOne($this->partner_category_id);
        if ($category) {
            $this->category_id = $category->value;
            $this->partner_id = $category->partner->id;
            $query->andFilterWhere([
                'category_id' => $this->category_id
            ]);
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'order_id' => $this->order_id,
            'partner_id' => $this->partner_id,
            'num_activations' => $this->num_activations,
            'code_expiration' => $this->code_expiration,
            'product_expiration' => $this->product_expiration,
            'status' => $this->status,
            'created_by' => $this->created_by,
            'created_time' => $this->created_time,
            'updated_time' => $this->updated_time,
        ]);

        $query->andFilterWhere(['like', 'code', $this->code])
            ->andFilterWhere(['like', 'message', $this->message]);

        $query->orderBy('id DESC');

        return $dataProvider;
    }
}