<?php

namespace kyna\partner\models;

use Yii;
use kyna\order\models\Order;
use kyna\user\models\User as KynaUser;

/**
 * This is the model class for table "{{%partner_retailer}}".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $status
 * @property integer $is_deleted
 * @property integer $created_time
 * @property integer $updated_time
 * @property integer $partner_id
 */
class Retailer extends \kyna\base\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%partner_retailer}}';
    }

    public static function softDelete()
    {
        return false;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'partner_id'], 'required'],
            [['user_id', 'status', 'is_deleted', 'created_time', 'updated_time', 'partner_id'], 'integer'],
            [['user_id', 'partner_id'], 'unique', 'targetAttribute' => ['user_id', 'partner_id'], 'comboNotUnique' => 'User đã là Retailer cho đối tác này'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'status' => Yii::t('app', 'Trạng thái'),
            'is_deleted' => Yii::t('app', 'Đã xóa'),
            'created_time' => Yii::t('app', 'Ngày tạo'),
            'updated_time' => Yii::t('app', 'Ngày cập nhật'),
            'partner_id' => Yii::t('app', 'Partner'),
        ];
    }

    public function getUser()
    {
        return $this->hasOne(KynaUser::className(), ['id' => 'user_id']);
    }

    public function getPartner()
    {
        return $this->hasOne(Partner::className(), ['id' => 'partner_id']);
    }

    public static function listStatus()
    {
        $list = [
            self::STATUS_ACTIVE => 'Hoạt động',
            self::STATUS_DEACTIVE => 'Ngừng hoạt động',
        ];
        return $list;
    }

    public function totalSellCodCode()
    {
        return Code::find()
            ->alias('c')
            ->join('INNER JOIN', Order::tableName(), Order::tableName() . '.activation_code = c.serial')
            ->where([
                'c.retailer_id' => $this->id,
                'c.status' => Code::CODE_STATUS_ACTIVE,
                Order::tableName() . '.payment_method' => 'ghn'
            ])->count();
    }

    public function totalCodCode()
    {
        $totalCode = Code::find()->where(['retailer_id' => $this->id])->count();
        return $totalCode - $this->totalAutoOrder();
    }

    public function totalOrder()
    {
        return Order::find()
            ->alias('o')
            ->join('INNER JOIN', Code::tableName(), Code::tableName() . '.serial = o.activation_code')
            ->where([
                Code::tableName() . '.retailer_id' => $this->id,
                'o.status' => Order::ORDER_STATUS_COMPLETE
            ])->count();
    }

    public function totalAutoOrder()
    {
        return Order::find()
            ->alias('o')
            ->join('INNER JOIN', Code::tableName(), Code::tableName() . '.serial = o.activation_code')
            ->where([
                Code::tableName() . '.retailer_id' => $this->id,
                'o.status' => Order::ORDER_STATUS_COMPLETE,
                'o.payment_method' => 'auto'
            ])->count();
    }
}
