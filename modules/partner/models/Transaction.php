<?php

namespace kyna\partner\models;

use kyna\user\models\User;
use Yii;

/**
 * This is the model class for table "{{%partner_transaction}}".
 *
 * @property integer $id
 * @property integer $order_id
 * @property string $code
 * @property integer $category_id
 * @property integer $num_activations
 * @property integer $code_expiration
 * @property integer $product_expiration
 * @property integer $status
 * @property integer $created_by
 * @property string $message
 * @property integer $created_time
 * @property integer $updated_time
 * @property integer $num_used
 * @property integer $partner_id
 */
class Transaction extends \kyna\base\ActiveRecord
{
    const STATUS_COMPLETE = 0;
    const STATUS_GENERIC_ERROR = 1;
    const STATUS_NOT_AVAILABLE = 2;
    const STATUS_EXISTS = 3;
    const STATUS_NOT_RESPONSE = -1;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%partner_transaction}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_id', 'category_id', 'partner_id','num_activations', 'code_expiration', 'product_expiration', 'status', 'created_by', 'created_time', 'updated_time', 'num_used'], 'integer'],
            [['code', 'category_id', 'num_activations', 'created_by', 'partner_id'], 'required'],
            [['code'], 'string', 'max' => 50],
            [['message'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'order_id' => Yii::t('app', 'Đơn hàng số'),
            'code' => Yii::t('app', 'Code'),
            'category_id' => Yii::t('app', 'Sản phẩm'),
            'num_activations' => Yii::t('app', 'Số lần kích hoạt tối đa'),
            'num_used' => Yii::t('app', 'Số lần đã kích hoạt'),
            'code_expiration' => Yii::t('app', 'Code Expiration'),
            'product_expiration' => Yii::t('app', 'Product Expiration'),
            'status' => Yii::t('app', 'Trạng thái'),
            'created_by' => Yii::t('app', 'Tạo bởi'),
            'message' => Yii::t('app', 'Message'),
            'created_time' => Yii::t('app', 'Created Time'),
            'updated_time' => Yii::t('app', 'Updated Time'),
            'partner_id' => Yii::t('app', 'Partner'),
        ];
    }

    public static function listStatus()
    {
        $list = [
            self::STATUS_COMPLETE => 'Hoàn thành',
            self::STATUS_GENERIC_ERROR => 'Lỗi kĩ thuật',
            self::STATUS_NOT_AVAILABLE => 'Không hợp lệ',
            self::STATUS_EXISTS => 'Đã tồn tại',
            self::STATUS_NOT_RESPONSE => 'Không phản hồi',
        ];

        return $list;
    }

    public function getStatusHtml() // TODO: move to trait
    {
        switch ($this->status) {
            case self::STATUS_COMPLETE:
                $labelClass = 'label-success';
                break;
            case self::STATUS_GENERIC_ERROR:
                $labelClass = 'label-danger';
                break;
            case self::STATUS_NOT_AVAILABLE:
                $labelClass = 'label-warning';
                break;
            case self::STATUS_EXISTS:
                $labelClass = 'label-info';
                break;
            case self::STATUS_NOT_RESPONSE:
                $labelClass = 'label-default';
                break;
            default:
                $labelClass = 'label-default';
                break;
        }

        return '<span class="label ' . $labelClass . '">' . $this->statusText . '</span>';
    }

    public function getCategory() {
        return $this->hasOne(Category::className(), ['value' => 'category_id'])->andOnCondition(['partner_id' => $this->partner->id]);
    }

    public function getUser() {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    public function getPartner()
    {
        return $this->hasOne(Partner::className(), ['id' => 'partner_id']);
    }
}