<?php

namespace kyna\partner\models\form;

use Yii;
use kyna\partner\models\Code;
use kyna\partner\models\CodeInterface;
use kyna\partner\models\Retailer;
use kyna\user\models\User as KynaUser;

/**
 * This is the model class for table "{{%partner_code}}".
 *
 * @property string $serial
 * @property string $prefix
 * @property integer $retailer_id
 * @property integer $quantity
 * @property integer $auto_serial
 * @property integer $partner_id
 */
class CreateCodeForm extends \kyna\base\ActiveRecord implements CodeInterface
{
    public $serial;
    public $retailer_id;
    public $auto_serial;
    public $prefix;
    public $quantity;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%partner_code}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['serial', 'quantity', 'partner_id'], 'required', 'on' => self::SCENARIO_SERIAL],
            [['quantity', 'partner_id'], 'required', 'on' => self::SCENARIO_AUTO_SERIAL],
            [['retailer_id', 'auto_serial', 'quantity', 'partner_id'], 'integer'],
            [['serial'], 'string', 'max' => 10],
            [['prefix'], 'string', 'max' => 5],
            [['serial'], 'unique', 'message' => 'Serial đã sử dụng'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'serial' => Yii::t('app', 'Serial Code'),
            'retailer_id' => Yii::t('app', 'Retailer'),
            'auto_serial' => Yii::t('app', 'Tự động tạo serial code'),
            'prefix' => Yii::t('app', 'Prefix'),
            'quantity' => Yii::t('app', 'Số lượng'),
            'partner_id' => Yii::t('app', 'Partner'),
        ];
    }

    public function getUser()
    {
        return $this->hasOne(KynaUser::className(), ['id' => 'user_id']);
    }

    public static function listStatus()
    {
        $list = [
            self::CODE_STATUS_IN_STORE => 'Chưa bán',
            self::CODE_STATUS_IN_ACTIVE => 'Chưa kích hoạt',
            self::CODE_STATUS_ACTIVE => 'Đã kích hoạt',
        ];
        return $list;
    }

    public function create()
    {
        if ($this->validate()) {
            if (!$this->auto_serial) {
                $code = new Code();
                $code->attributes = $this->attributes;
                if (intval($this->retailer_id) > 0) {
                    $code->status = self::CODE_STATUS_IN_ACTIVE;

                } else {
                    $code->status = self::CODE_STATUS_IN_STORE;
                }
                $code->code = Code::generateActivationCode(self::CODE_LENGTH, $this->partner_id);
                $code->partner_id = $this->partner_id;
                $code->is_deleted = self::BOOL_NO;
                return $code->save();
            } else {
                for ($i = 0; $i < $this->quantity; $i ++) {
                    $code = new Code();
                    if (intval($this->retailer_id) > 0) {
                        $code->retailer_id = $this->retailer_id;
                        $code->status = self::CODE_STATUS_IN_ACTIVE;

                    } else {
                        $code->status = self::CODE_STATUS_IN_STORE;
                    }
                    $code->code = Code::generateActivationCode(self::CODE_LENGTH, $this->partner_id);
                    $code->serial = Code::generateSerial(self::SERIAL_LENGTH, $this->prefix);
                    $code->partner_id = $this->partner_id;
                    $code->save();
                    unset($code);
                }
                return true;
            }
        }
    }

    public function createAutoSingle($status, $isRetailer = false)
    {
        $code = new Code();
        $code->code = Code::generateActivationCode(self::CODE_LENGTH, $this->partner_id);
        $code->serial = Code::generateSerial(self::SERIAL_LENGTH, $this->prefix);
        $code->partner_id = $this->partner_id;
        $code->status = $status;
        // if create by retailer, then update
        $retailer = Retailer::findOne(['user_id' => Yii::$app->user->id, 'partner_id' => $this->partner_id]);
        if ($isRetailer && !empty($retailer)) {
            $code->retailer_id = $retailer->id;
        }
        if ($code->save()) {
            return $code;
        }
        return null;
    }

    public function getRetailer()
    {
        return $this->hasOne(Retailer::className(), ['id' => 'retailer_id']);
    }
}
