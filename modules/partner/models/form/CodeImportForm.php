<?php

namespace kyna\partner\models\form;

use Yii;
use yii\web\UploadedFile;
use common\helpers\StringHelper;

class CodeImportForm extends \yii\base\Model
{
    
    public $file;
    public $email;
    
    public function rules()
    {
        return [
            [['file'], 'file', 'skipOnEmpty' => false, 'extensions' => 'xls, xlsx'],
            [['email'], 'email', 'skipOnEmpty' => false],
        ];
    }
    
    public function attributeLabels()
    {
        return [
            'file' => 'File',
            'email' => 'Email'
        ];
    }

    public function assign()
    {
        $this->file = UploadedFile::getInstance($this, 'file');
        if ($this->validate()) {
            $dir = Yii::getAlias("@upload") . '/partner/';
            if (!file_exists($dir)) {
                mkdir($dir, 0777, true);
            }
            $name = strtolower(StringHelper::random(15)). "." . $this->file->extension;
            $filepath = $dir . $name;
            if ($this->file->saveAs($filepath)) {
                $inputFilePath = $filepath;
                // execute command in console
                $rootPath = \Yii::getAlias('@root');
                $exportLog = \Yii::getAlias('@console/runtime/import_partner_code.log');
                $exportErrorLog = \Yii::getAlias('@console/runtime/import_partner_code-error.log');
                // check log file exist
                self::_checkExistFile($exportLog);
                self::_checkExistFile($exportErrorLog);
                $command = "php {$rootPath}/yii import/run partner-code \"{$inputFilePath}\" {$this->email}" . " > {$exportLog} 2>>{$exportErrorLog} &";
                exec($command);
                return true;
            }
        }
        return false;
    }

    private function _checkExistFile($file)
    {
        clearstatcache();
        if (!file_exists($file)) {
            touch($file);
        }
    }
}
