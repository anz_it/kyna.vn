<?php

namespace kyna\partner\models\form;

use common\helpers\ArrayHelper;
use Yii;
use yii\base\InvalidValueException;
use app\modules\order\models\Product;
use kyna\partner\models\Category;
use kyna\partner\models\Code;
use kyna\partner\models\Retailer;
use kyna\course\models\Course;
use kyna\order\models\Order;
use kyna\order\models\OrderQueue;
use kyna\user\models\Address;
use kyna\user\models\UserCourse;
use kyna\commission\models\AffiliateUser;
use kyna\user\models\UserTelesale;
use kyna\user\models\User;

class CreateOrderForm extends \yii\base\Model
{
    
    public $email;
    public $payment_method;
    public $shipping_contact_name;
    public $shipping_phone_number;
    public $shipping_street_address;
    public $shipping_location_id;
    public $activation_code;
    public $note;
    public $category_id;
    public $course_list;
    public $operator_id;

    const SCENARIO_COD = 'ghn';
    const SCENARIO_EMAIL = 'auto';

    public function rules()
    {
        return [
            ['email', 'email'],
            [['category_id', 'shipping_location_id', 'operator_id'], 'integer'],
            //['activation_code', 'string', 'max' => 10],

            [['shipping_street_address', 'shipping_location_id', 'activation_code'], 'required', 'on' => self::SCENARIO_COD],
            [['note', 'shipping_street_address'], 'string'],
            [['shipping_contact_name', 'email', 'shipping_phone_number', 'category_id'], 'required'],
            ['activation_code', 'validSerial']
        ];
    }

    public function attributeLabels()
    {
        return [
            'email' => 'Email',
            'payment_method' => 'Hình thức thanh toán',
            'category_id' => 'Chọn gói',
            'shipping_contact_name' => 'Họ tên',
            'shipping_phone_number' => 'Số điện thoại',
            'activation_code' => 'Serial Code',
            'shipping_street_address' => 'Địa chỉ',
            'shipping_location_id' => 'Địa phương',
            'note' => 'Ghi chú',
        ];
    }

    /**
     * Create order from partner retailer
     * @return array|bool|Order|null|\yii\db\ActiveRecord
     * Note: only support one course/combo per order
     */
    public function create()
    {
        /* Validate user before create */
        if (!$this->email) {
            return false;
        }

        $user = $this->findUser($this->email);

        if (!$user) {
            throw new InvalidValueException("Can't create user with this email");
        } else {
            // check duplicate user courses
            $category = Category::findOne($this->category_id);
            $course = Course::findOne($category->course_id);
            if (empty($course)) {
                Yii::$app->session->setFlash('warning', "Khóa học không tồn tại");
                return false;
            } else {
                $this->course_list = "{$course->id}";
            }
        }

        /* Start creating order */
        $order = Order::createEmpty(Order::SCENARIO_CREATE_RETAILER);
        $order->on(Order::EVENT_ORDER_CREATED, [$this, 'pushToQueue']);
        
        $order->activation_code = $this->activation_code;
        $order->payment_method = $this->payment_method;
        $order->user_id = $user->id;
        $order->reference_id = $this->operator_id;
        $order->operator_id = $this->operator_id;
        $order->is_done_telesale_process = Order::BOOL_YES;

        if ($address = $this->updateUserAddress($user->id)) {
            $order->shippingAddress = !empty($this->note) ? ($address->attributes + ['note' => $this->note]) : $address;
        }
        
        // save order details
        $order->addDetails($this->prepareOrderItems());
        if ($order->save()) {
            $serial = $order->activation_code;
            if ($this->payment_method == 'auto') {
                // auto-generate partner code
                $model = new CreateCodeForm();
                $model->partner_id = $course->partner_id;
                $code = $model->createAutoSingle(Code::CODE_STATUS_IN_ACTIVE, true);
                if (!is_null($code)) {
                    $serial = $code->serial;
                }
            }
            // update order detail activation code
            foreach ($order->details as $orderDetail) {
                $orderDetail->activation_code = $serial;
                $orderDetail->save(false);
            }
            return $order;
        }

        return false;
    }
    
    protected function prepareOrderItems()
    {
        $courseIds = explode(',', $this->course_list);
        $courses = Course::find()->where(['id' => $courseIds])->all();
        
        $addItems = [];
        foreach ($courses as $key => $course) {
            if ($course->type == Course::TYPE_COMBO) {
                unset($courses[$key]);
                
                $comboItems = $course->comboItems;
                foreach($comboItems as $item) {
                    if (!array_key_exists($item->course_id, $courses)) {
                        $product = Product::findOne($item->course_id);
                        $product->setCombo($course);
                        $product->setDiscountAmount($product->price - $item->price);
                        
                        $addItems[$product->id] = $product;
                    }
                }
            } else {
                $addItems[$course->id] = $course;
            }
        }
        
        return $addItems;
    }

    protected function updateUserAddress($userId)
    {
        if (!$address = Address::find()->where(['user_id' => $userId])->one()) {
            $address = new Address();
            $address->user_id = $userId;
        }

        $address->attributes = [
            'email' => $this->email,
            'contact_name' => $this->shipping_contact_name,
            'phone_number' => $this->shipping_phone_number,
            'street_address' => $this->shipping_street_address,
            'location_id' => $this->shipping_location_id,
        ];

        if (!$address->save()) {
            $this->errors = $address->errors;
            return false;
        }

        return $address;
    }

    protected function findUser($email)
    {
        $user = User::find()->where(['email' => $email])->one();

        if (!$user) {
            $user = new User([
                'scenario' => 'create',
            ]);
            $user->load(['User' => [
                'email' => $email,
                'username' => $email,
            ]]);
            // update meta fields
            $user = $this->beforeCreateUser($user);
            if ($user->create()) {
                // register profile
                $this->afterCreateUser($user);
            }
        }

        return $user;
    }

    /**
     * Update meta fields
     * @param $user
     * @return mixed
     */
    public function beforeCreateUser($user)
    {
        $user->phone = $this->shipping_phone_number;
        $user->address = $this->shipping_street_address;
        $user->location_id = $this->shipping_location_id;
        return $user;
    }

    /**
     * Update profile if Telesale user
     * @param $user
     */
    public function afterCreateUser($user)
    {
        $profile = $user->profile;
        $profile->public_email = $this->email;
        $profile->name = $this->shipping_contact_name;
        $profile->phone_number = $this->shipping_phone_number;
        $profile->location = $this->shipping_location_id;
        $profile->save(false);
    }

    public function pushToQueue($actionEvent)
    {
        $order = $actionEvent->sender;
        OrderQueue::pushToQueue($order);
    }

    public function validSerial($attribute, $params)
    {
        $retailers = Yii::$app->user->getPartnerRetailer();
        $category = Category::findOne($this->category_id);
        $code = Code::find()->where([
            'serial' => $this->activation_code,
            'status' => Code::CODE_STATUS_IN_ACTIVE,
            'partner_id' => $category->partner_id,
            'retailer_id' => ArrayHelper::getColumn($retailers, 'id')
        ])->exists();
        if (!$code) {
            $this->addError($attribute, Yii::t('app', 'Serial Code không hợp lệ'));
        }
    }
}
