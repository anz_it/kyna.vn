<?php

namespace kyna\partner\models;

use Yii;
use common\helpers\ArrayHelper;
use common\lib\memoize\Memoize;
use common\helpers\StringHelper;
use kyna\course\models\Course;
use kyna\order\models\Order;
use kyna\order\models\OrderDetails;
use kyna\settings\models\Setting;
use kyna\taamkru\lib\Taamkru;
use kyna\user\models\User as KynaUser;
use kyna\partner\models\form\CreateCodeForm;

/**
 * This is the model class for table "{{%partner_code}}".
 *
 * @property integer $id
 * @property string $serial
 * @property string $code
 * @property integer $retailer_id
 * @property integer $category_id
 * @property integer $status
 * @property integer $is_deleted
 * @property integer $created_time
 * @property integer $updated_time
 * @property integer $activation_date
 * @property integer $partner_id
 */
class Code extends \kyna\base\ActiveRecord implements CodeInterface
{
    use Memoize;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%partner_code}}';
    }

    public static function softDelete()
    {
        return false;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['serial', 'code', 'partner_id'], 'required'],
            [['retailer_id', 'category_id', 'status', 'is_deleted', 'created_time', 'updated_time', 'activation_date', 'partner_id'], 'integer'],
            [['serial', 'code'], 'string', 'max' => 50],
            [['serial'], 'unique', 'message' => 'Serial đã sử dụng'],
            [['code'], 'unique', 'message' => 'Code đã sử dụng'],

        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'serial' => Yii::t('app', 'Serial Code'),
            'code' => Yii::t('app', 'Mã kích hoạt'),
            'retailer_id' => Yii::t('app', 'Retailer'),
            'category_id' => Yii::t('app', 'Sản phẩm'),
            'status' => Yii::t('app', 'Trạng thái'),
            'is_deleted' => Yii::t('app', 'Đã xóa'),
            'created_time' => Yii::t('app', 'Ngày tạo'),
            'updated_time' => Yii::t('app', 'Ngày cập nhật'),
            'activation_date' => Yii::t('app', 'Ngày kích hoạt'),
            'partner_id' => Yii::t('app', 'Partner'),
        ];
    }

    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'category_id']);
    }

    public function getRetailer()
    {
        return $this->hasOne(Retailer::className(), ['id' => 'retailer_id']);
    }

    public function getPartner()
    {
        return $this->hasOne(Partner::className(), ['id' => 'partner_id']);
    }

    public function getOrder()
    {
        $orderDetail = OrderDetails::findOne(['activation_code' => $this->serial]);
        if ($orderDetail) {
            return $orderDetail->order;
        } else {
            return Order::find()
                ->where(['activation_code' => $this->serial])
                ->andWhere([
                    'NOT IN', 'status', [
                        Order::ORDER_STATUS_CANCELLED,
                        Order::ORDER_STATUS_RETURN,
                        Order::ORDER_STATUS_IN_COMPLETE
                    ]
                ])
                ->one();
        }
    }

    public static function listStatus()
    {
        $list = [
            self::CODE_STATUS_IN_STORE => 'Chưa bán',
            self::CODE_STATUS_IN_ACTIVE => 'Chưa kích hoạt',
            self::CODE_STATUS_ACTIVE => 'Đã kích hoạt',
            self::CODE_STATUS_CANCEL => 'Hủy'
        ];
        return $list;
    }

    public static function listStatusRetailer()
    {
        $list = [
            self::CODE_STATUS_IN_ACTIVE => 'Chưa kích hoạt',
            self::CODE_STATUS_ACTIVE => 'Đã kích hoạt',
        ];
        return $list;
    }

    public static function listPaymentMethod()
    {
        $list = [
            self::PAYMENT_METHOD_COD => 'COD',
            self::PAYMENT_METHOD_ONLINE => 'Online',
        ];
        return $list;
    }

    public function getStatusHtml() // TODO: move to trait
    {
        $html = "";
        switch ($this->status) {
            case self::CODE_STATUS_IN_STORE:
                $html = '<span class="label label-default">Chưa bán</span>';
                break;
            case self::CODE_STATUS_IN_ACTIVE:
                $html = '<span class="label label-warning">Chưa kích hoạt</span>';
                break;
            case self::CODE_STATUS_ACTIVE:
                $html = '<span class="label label-primary">Đã kích hoạt</span>';
                break;
            case self::CODE_STATUS_CANCEL:
                $html = '<span class="label label-danger">Hủy</span>';
                break;
            default:
                break;
        }
        return $html;
    }

    public function getStatusText() // TODO: move to trait
    {
        $text = "";
        switch ($this->status) {
            case self::CODE_STATUS_IN_STORE:
                $text = 'Chưa bán';
                break;
            case self::CODE_STATUS_IN_ACTIVE:
                $text = 'Chưa kích hoạt';
                break;
            case self::CODE_STATUS_ACTIVE:
                $text = 'Đã kích hoạt';
                break;
            case self::CODE_STATUS_CANCEL:
                $text = 'Hủy';
                break;
            default:
                break;
        }
        return $text;
    }

    public function getPaymentMethodText() // TODO: move to trait
    {
        $text = "";
        switch ($this->paymentMethod) {
            case self::PAYMENT_METHOD_COD:
                $text = 'COD';
                break;
            case self::PAYMENT_METHOD_ONLINE:
                $text = 'Online';
                break;
            default:
                $text = 'Online';
                break;
        }
        return $text;
    }

    public function getPaymentMethod()
    {
        $order = $this->order;
        return !empty($order) ? $order->payment_method : self::PAYMENT_METHOD_COD;
    }

    /**
     * @desc generate random unique code for activation
     *
     * @param int $codeLen
     *
     * @return string string
     */
    public static function generateSerial($serialLen = 10, $prefix)
    {
        $prefix = strtoupper($prefix);
        $serial = $prefix . StringHelper::random($serialLen - strlen($prefix));
        if (self::checkSerialUnique($serial)) {
            return $serial;
        }
        return $prefix . StringHelper::random($serialLen);
    }

    /**
     * @param string $code
     * @return bool
     */
    public static function checkSerialUnique($serial)
    {
        return Code::find()->where(['serial' => $serial])->exists();
    }

    /**
     * @desc generate random unique code for activation
     *
     * @param int $codeLen
     *
     * @return string string
     */
    public static function generateActivationCode($codeLen = 6, $partnerId)
    {
        $partner = Partner::findOne($partnerId);
        $client = $partner->client;
        $class = get_class($client);
        $prefix = defined("{$class}::CODE_PREFIX") ? $class::CODE_PREFIX : '';
        $code = $prefix . StringHelper::random($codeLen);
        if (self::checkCodeUnique($code)) {
            return $code;
        }
        return $prefix . StringHelper::random($codeLen);
    }

    /**
     * @param string $code
     * @return bool
     */
    public static function checkCodeUnique($code)
    {
        return Code::find()->where(['code' => $code])->exists();
    }

    public function cancel($force = false)
    {
        if (($this->status == self::CODE_STATUS_IN_STORE) || ($force == true)) {
            $this->status = Code::CODE_STATUS_CANCEL;
            $this->save();
        }
    }

    public function active($category_id, $orderId)
    {
        if (in_array($this->status, [self::CODE_STATUS_IN_ACTIVE, self::CODE_STATUS_IN_STORE])) {
            $this->category_id = $category_id;
            $this->status = Code::CODE_STATUS_ACTIVE;
            $this->activation_date = time();
            if ($this->save()) {
                // send mail if payment_method is online
                // $this->sendMail($orderId);
            }
        }
    }

    public function activeByPartner()
    {
        $order = $this->order;
        $client = $this->partner->client;
        if (!$this->isActivated && $order && $client) {
            // call api active code to Partner
            $result = $client->active($order->id);
            if (isset($result['status']) && $result['status']) {
                $category = null;
                foreach ($order->details as $detail) {
                    $courseId = ($detail->course_combo_id > 0) ? $detail->course_combo_id : $detail->course_id;
                    $category = Category::findOne([
                        'course_id' => $courseId,
                        'partner_id' => $this->partner->id
                    ]);
                    break;
                }
                $this->active($category->id, $order->id);
                return true;
            }
        }
        // cancel order if order diff cod
        if (!$order->isCod) {
            Order::cancel($order->id, [
                'note' => 'Hủy order, lý do không thể active code from Partner'
            ]);
        }
        return false;
    }

    public function getIsActivated()
    {
        return $this->status == self::CODE_STATUS_ACTIVE;
    }

    public function returnCode()
    {
        if ($this->status == self::CODE_STATUS_IN_ACTIVE && is_null($this->retailer_id)) {
            $this->category_id = null;
            $this->status = Code::CODE_STATUS_IN_STORE;
            $this->save();
        }
    }

    public function sendMail($orderId)
    {
        $order = Order::findOne($orderId);
        $partner = $this->partner;
        $client = $partner->client;
        $class = get_class($client);
        $sendMail = defined("{$class}::SEND_MAIL") ? $class::SEND_MAIL : true;
        if ($order->isCod || $partner == null || !$sendMail) {
            return false;
        }
        $mailer = Yii::$app->mailer;
        $mailer->htmlLayout = false;

        // get setting cc email
        $settings = ArrayHelper::map(Setting::find()->all(), 'key', 'value');
        $ccEmails = !empty($settings['partner_email_cc']) ? $settings['partner_email_cc'] : 'hotro@kyna.vn';
        $ccEmails = explode(',', $ccEmails);

        $mailer->compose('partner/' . $partner->class, [
            'order' => $order,
            'code' => $this,
            'partner' => $partner,
            'settings' => ArrayHelper::map(Setting::find()->all(), 'key', 'value')
        ])
            ->setTo($order->user->email)
            ->setCc($ccEmails)
            ->setSubject("[Kyna for Kids] - MÃ KÍCH HOẠT SẢN PHẨM")
            ->send();
    }

    /**
     * @param Order $order
     * @return bool
     */
    public static function activeMultiPartner(Order $order)
    {
        // auto-generate partner code for order detail
        $partnerData = [];
        foreach ($order->details as $orderDetail) {
            $course = $orderDetail->course;
            $combo = $orderDetail->combo;
            $activationCode = null;
            if ($course && $course->isPartner && $course->partner->isServiceType) {
                // check if AC is activated
                $transaction = Transaction::find()
                    ->where([
                        'code' => $orderDetail->partnerCode ? $orderDetail->partnerCode->code : null,
                        'order_id' => $order->id,
                        'status' => Transaction::STATUS_COMPLETE
                    ])
                    ->exists();
                if ($transaction) {
                    continue;
                }
                $partner = $orderDetail->course->partner;
                if (!array_key_exists($partner->id, $partnerData)) {
                    $partnerData[$partner->id] = [];
                }
                if ($combo && $combo->isComboPartner && !array_key_exists($combo->id, $partnerData[$partner->id])) {
                    $activationCode = self::generateCode($orderDetail, $partner);
                    $code = self::findOne(['serial' => $activationCode]);
                    if ($code) {
                        $partnerData[$partner->id][$combo->id] = ['id' => $combo->id, 'serial' => $code->serial,'activation_code' => $code->code];
                    }
                } elseif ($combo && $combo->isComboPartner) {
                    $activationCode = $partnerData[$partner->id][$combo->id]['serial'];
                } else {
                    $activationCode = self::generateCode($orderDetail, $partner);
                    $code = self::findOne(['serial' => $activationCode]);
                    if ($code) {
                        $partnerData[$partner->id][$course->id] = ['id' => $course->id, 'serial' => $code->serial, 'activation_code' => $code->code];
                    }
                }
                // update order activation code
                if (!is_null($activationCode)) {
                    $orderDetail->activation_code = $activationCode;
                    $orderDetail->save(false);
                }
            }
        }
        // call api active partner code
        $partnerAPIStatus = true;
        foreach ($partnerData as $partnerID => $data) {
            $partner = Partner::findOne($partnerID);
            if ($partner && $partner->client) {
                // call api active code to Partner
                $result = $partner->client->activeMulti($order->id, $partnerID, $data);
                if (isset($result['status']) && $result['status']) {
                    $category = null;
                    foreach ($data as $courseID => $info) {
                        $category = Category::findOne([
                            'course_id' => $courseID,
                            'partner_id' => $partnerID
                        ]);
                        $code = self::findOne(['code' => $info['activation_code']]);
                        $code->active($category->id, $order->id);
                    }
                } else {
                    $partnerAPIStatus = false;
                }
            }
        }
        return $partnerAPIStatus;
    }

    /**
     * @param OrderDetails $orderDetail
     * @param Partner $partner
     * @return null|string
     */
    private static function generateCode(OrderDetails $orderDetail, Partner $partner)
    {
        $activationCode = null;
        if ($orderDetail->order->isCod) {
            $activationCode = !empty($orderDetail->activation_code) ? $orderDetail->activation_code : null;
        } else {
            if (empty($orderDetail->activation_code)) {
                $model = new CreateCodeForm();
                $model->partner_id = $partner->id;
                $code = $model->createAutoSingle(Code::CODE_STATUS_IN_ACTIVE);
                if (!is_null($code)) {
                    $activationCode = $code->serial;
                }
            } else {
                $activationCode = $orderDetail->activation_code;
            }
        }
        return $activationCode;
    }
}
