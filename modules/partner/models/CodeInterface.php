<?php
namespace kyna\partner\models;


interface CodeInterface {

    const SCENARIO_SERIAL = 'serial';
    const SCENARIO_AUTO_SERIAL = 'auto-serial';

    const CODE_STATUS_IN_STORE = 0;
    const CODE_STATUS_IN_ACTIVE = 1;
    const CODE_STATUS_ACTIVE = 2;
    const CODE_STATUS_CANCEL = 3;

    const CODE_LENGTH = 6;
    const SERIAL_LENGTH = 10;

    const PAYMENT_METHOD_COD = 'cod';
    const PAYMENT_METHOD_ONLINE = 'auto';

}