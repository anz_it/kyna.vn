<?php

namespace kyna\commission;

use Yii;
use yii\web\NotFoundHttpException;

use kyna\commission\models\AffiliateUser;
use kyna\course\models\Course;
use kyna\course_combo\models\CourseCombo;
use kyna\commission\models\CommissionIncome;
use kyna\commission\models\CommissionCalculation;

class Commission
{
    
    /**
     * Main calculator commission for affiliate, teacher and Kyna
     * @param integer $order_id
     * @param float $order_sub_total
     * @param float $order_total
     * @param float $total_course_discount
     * @param float $order_total_discount
     * @param float $fee
     * @param integer $course_id
     * @param float $course_total
     * @param integer $affiliate_id
     * @param integer $combo_id
     * @return boolean
     */
    public static function calculate($order_id, $order_sub_total, $order_total, $total_course_discount,
                                     $order_total_discount, $fee, $course_id,
                                     $course_total, $affiliate_id = 0, $combo_id = 0,
                                      $detail = null,$shipping_fee = 0,$direct_discount_amount =0,$details = null)
    {

        // get X: real income of course = course's total - order fee * percent of course

       // $realIncomeOfCourse = $course_total - (($fee + $order_discount) * $price_course_ratio) - $promo_discount;
        $realIncomeOfCourse = 0;
        $price_course_ratio = 0;
        if($order_total > 0)
        {
            $price_course_ratio = ($detail->unit_price - $detail->discount_amount ) / $order_total ; // tỉ lệ giá trên tổng đơn hang
            $realIncomeOfCourse = $detail->unit_price - $detail->discount_amount - $fee * $price_course_ratio;
        }

        $course = self::loadCourseModel($course_id);
        // get Y: teacher commission percent
        $teacherComPercent = $course->commissionPercent;

        // get Z: affiliate commission percent
        $affComPercent = 0;
        $affGroupId = null;

        $affiliateUser = AffiliateUser::find()->where(['user_id' => $affiliate_id])->one();
        $isAffiliateIsTeacher  = false;
        if ($affiliateUser != null) {
            /* @var $affiliateUser AffiliateUser */
            $affComPercent = $affiliateUser->getCommissionPercent($course_id);
            $affGroupId = !is_null($affiliateUser->affiliateCategory) ? $affiliateUser->affiliateCategory->affiliate_group_id : null;
            // check if the case: Teacher affiliate the course is not his, or course in combo is not his
            $idOfAffGroupTeacherOwner = Yii::$app->params['id_aff_group_teacher_owner'];
            $idOfAffGroupThirdParty = Yii::$app->params['id_aff_group_third_party'];

            if ($affGroupId == $idOfAffGroupTeacherOwner) {
                if ($affiliate_id != $course->teacher_id) {
                    // Third party group
                    $affGroupId = $idOfAffGroupThirdParty;
                    $affComPercent = 0; // get group affiliate percent
                } else {
                    // teacher affiliate his course, then check course if in combo
                    if (!empty($combo_id)) {
                        $combo = CourseCombo::find()->where([
                            'id' => $combo_id
                        ])->one();
                        if ($combo->teacher_id != $affiliate_id) {
                            // Third party group
                            $affGroupId = $idOfAffGroupThirdParty;
                            $affComPercent = 0; // get group affiliate percent
                        } else {
                            $affComPercent =  self::getAffliatteCommissionPercentWithTeacher($course_id);
                            $isAffiliateIsTeacher = true;
                        }
                    } else { // case single course, affliate is teacher, get from course config
                        $affComPercent =  self::getAffliatteCommissionPercentWithTeacher($course_id);
                        $isAffiliateIsTeacher = true;
                    }
                }
            }
        }


        // get commission calculation
        $comCalculation = self::getCalculation($affGroupId);
        
        $comIncome = CommissionIncome::find()->where([
            'order_id' => $order_id,
            'course_id' => $course_id,
        ])->one();
        if (is_null($comIncome)) {
            $comIncome = new CommissionIncome();

            // no update commission percent for both aff and teacher when update
            $comIncome->teacher_commission_percent = $teacherComPercent;
            if (empty($affComPercent) && !$isAffiliateIsTeacher && !($affiliateUser != null && $affiliateUser->is_override_commission == AffiliateUser::BOOL_YES)) {
                // if both affiliate user and affiliate category has not overrided affiliate's commission percent then use from Calculation
                $affComPercent = $comCalculation->affiliate_percent;
            }
            $comIncome->affiliate_commission_percent = $affComPercent;
        }

        /*
         * Vd giá giảm trực tiếp là 1.000.000 cho 2 khóa học giá cuối sau giảm lần lược là 400.000, và 800.000
         * Giá đơn hàng cuối cùng = 200.000 Đ + 30.000đ (phí giao vận đơn hàng dưới 200k) = 230.000
         * Phần trăm giá giảm cho khóa học 400.000đ = ((400.000 -0 )/ (Tổng giá trị đơn hàng 1.200.000 đ) ))
         * Phần trăm giá giảm cho khóa học 800.000đ = ((800.000 -0 )/ (Tổng giá trị đơn hàng 1.200.000 đ) ))
         *
         * Tiền giam trực tiếp cho khóa học = Phần trăm giá giảm * $order_total_discount(giá tiền giảm trực tiếp 1.000.000đ)
         * Giá khóa học realIncomeOfCourse = 400.000 - Giá khóa học giam trực tiếp cho khóa học - (free * Phần trăm giá giảm của từng khóa học)
         * */

        if(!empty($direct_discount_amount)){

            $totalDiscountCourse = 0;
            foreach ($details as $d){
                $totalDiscountCourse += $d->discount_amount;
            }
            // Tông tiền đơn hàng trước khi giảm trực tiếp
            $orderTotal = $order_sub_total - $totalDiscountCourse - $shipping_fee ;

            // Tỉ lẹ phần trăm
            $price_course_ratio =  (($detail->unit_price - $detail->discount_amount)/ ($orderTotal));

            //Tính phần giá giảm trực tiếp
            $priceDiscountOfCourseDirect = $price_course_ratio * $direct_discount_amount;

            $realIncomeOfCourse = ($detail->unit_price - $detail->discount_amount) - $priceDiscountOfCourseDirect - ($fee * $price_course_ratio);
        }

        $comIncome->user_id = $affiliate_id;
        $comIncome->order_id = $order_id;
        $comIncome->course_id = $course_id;
        $comIncome->teacher_id = $course->teacher_id;
        $comIncome->commission_caculation_id = $comCalculation->id;
        $comIncome->total_amount = floor($realIncomeOfCourse);
        $comIncome->fee = round($fee * $price_course_ratio);


        if($comIncome->affiliate_commission_percent == 0)
        {
            $comIncome->acp_with_teacher = 0;

        } else {
            $comIncome->acp_with_teacher = self::getAffliatteCommissionPercentWithTeacher($course_id);
        }

        // calculate instructor's commission: X * ratio * Y
       // $comIncome->teacher_commission_amount = floor($realIncomeOfCourse * $comCalculation->instructor_percent * $comIncome->teacher_commission_percent / 10000);

        $comIncome->teacher_commission_amount = floor($realIncomeOfCourse * (100 - $comIncome->acp_with_teacher ) * $comIncome->teacher_commission_percent / 10000);

        // calculate affiliate's commission: X * Z
        $comIncome->affiliate_commission_amount = floor($realIncomeOfCourse * $comIncome->affiliate_commission_percent / 100);

        if ($affiliateUser != null) {
            // calculate affiliate manager commission: 2% * affiliate_commission_amount
            $comIncome->affiliate_manager_percent = 2;
            $comIncome->affiliate_manager_id = $affiliateUser->manager_id;
            $comIncome->affiliate_manager_amount = floor($realIncomeOfCourse * 2 / 100);
        }

        // calculate Kyna's commission
        $comIncome->kyna_commission_amount = floor($realIncomeOfCourse - $comIncome->teacher_commission_amount - $comIncome->affiliate_commission_amount);
        return $comIncome->save();
    }

    /**
     * Find calculation to apply calculate commission
     * @param integer|null $affGroupId
     * @return type
     * @throws NotFoundHttpException
     */
    public static function getCalculation($affGroupId)
    {
        $calculation = CommissionCalculation::getCurrentAvailableByGroup($affGroupId);

        if (!empty($calculation)) {
            return $calculation;
        } else {
            throw new NotFoundHttpException('Commission Calculation could not found with group id: ' . $affGroupId);
        }
    }

    public static function loadCourseModel($course_id)
    {
        $model = Course::findOne($course_id);

        if (!empty($model)) {
            return $model;
        } else {
            throw new NotFoundHttpException();
        }
    }

    public static function getAffliatteCommissionPercentWithTeacher($course_id)
    {

        $course = Course::findOne(['id'=>$course_id]);
        if(!empty($course) && isset($course->affiliate_commission_percent))
        {
            return $course->affiliate_commission_percent;
        } else {
            return 50;
        }
    }
    
}