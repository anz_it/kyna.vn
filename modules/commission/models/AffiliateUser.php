<?php

namespace kyna\commission\models;

use common\helpers\ArrayHelper;
use kyna\order\models\Order;
use Yii;
use kyna\user\models\User;

/**
 * This is the model class for table "{{%affiliate_users}}".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $affiliate_category_id
 * @property boolean $is_override_commission
 * @property double $commission_percent
 * @property integer $cookie_day
 * @property boolean $is_deleted
 * @property integer $status
 * @property integer $created_time
 * @property integer $updated_time
 * @property integer $manager_id
 * @property integer $is_manager
 */
class AffiliateUser extends \kyna\base\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%affiliate_users}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['user_id', 'unique', 'message' => 'User này đã được thêm vào affiliate'],
            [['user_id', 'affiliate_category_id'], 'required'],
            [['user_id', 'affiliate_category_id', 'cookie_day', 'status', 'created_time', 'updated_time', 'manager_id', 'is_manager'], 'integer'],
            [['is_override_commission', 'is_deleted'], 'boolean'],
            [['commission_percent'], 'number', 'max' => 100],
            ['commission_percent', 'number', 'min' => 1, 'when' => function ($model) {
                    return $model->is_override_commission;
            }, 'whenClient' => "function (attribute, value) {
                return $('#affiliateuser-is_override_commission').is('checked');
            }"],
            ['manager_id', 'checkParent'],
        ];
    }

    public function checkParent($attribute)
    {
        $parentAff = $this->parentAffiliate;
        if ($parentAff && $parentAff->user_id == $this->user_id) {
            $this->addError('manager_id', 'Affiliate manager không thể trùng với Affiliate user');
        }
    }

    public function afterSave($insert, $changedAttributes)
    {
        if (!$this->is_manager) {
            self::updateAll(['manager_id' => null], "manager_id = {$this->user_id}");
        }
    }

    public function afterDelete()
    {
        self::updateAll(['manager_id' => null], "manager_id = {$this->user_id}");
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User',
            'affiliate_category_id' => 'Loại Affiliate',
            'is_override_commission' => 'Thiết lập hoa hồng riêng cho User',
            'commission_percent' => 'Phần trăm hoa hồng',
            'cookie_day' => 'Số ngày lưu trữ Cookie',
            'is_deleted' => 'Is Deleted',
            'status' => 'Trạng thái',
            'created_time' => 'Created Time',
            'updated_time' => 'Updated Time',
            'manager_id' => 'Affiliate manager',
            'is_manager' => 'Is Manager'
        ];
    }

    /**
     * @desc return list status for renders
     * @return array
     */
    public static function listIsManagerText()
    {
        $list = [
            self::STATUS_ACTIVE => 'Yes',
            self::STATUS_DEACTIVE => 'No',
        ];

        return $list;
    }
    
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function getParentAffiliate()
    {
        return $this->hasOne(self::className(), ['user_id' => 'manager_id']);
    }

    public function getAffiliateCategory()
    {
        return $this->hasOne(AffiliateCategory::className(), ['id' => 'affiliate_category_id']);
    }
    
    public function getCookieDay()
    {
        if (empty($this->cookie_day) && !is_null($this->affiliateCategory)) {
            return $this->affiliateCategory->default_cookie_day;
        }
        
        return $this->cookie_day;
    }
    
    public function getCookieExpireTimestamp()
    {
        return !empty($this->cookieDay) ? (time() + $this->cookieDay * 24 * 3600) : 0;
    }
    
    public function getCommissionPercent($course_id = null)
    {
        $affCategoryCourse = AffiliateCategoryCourse::findOne([
            'course_id' => $course_id,
            'affiliate_category_id' => $this->affiliate_category_id,
            'status' => AffiliateCategoryCourse::STATUS_ACTIVE
        ]);

        if ($affCategoryCourse != null) {
            return $affCategoryCourse->commission_percent;
        }

        if ($this->is_override_commission) {
            return $this->commission_percent;
        }
        
        return !is_null($this->affiliateCategory) ? $this->affiliateCategory->commissionPercent : 0;
    }

    public function getIsAffiliateManager()
    {
        return $this->is_manager ? true : false;
    }

    public function getChild()
    {
        return $this->hasMany(AffiliateUser::className(), [AffiliateUser::tableName() . '.manager_id' => 'user_id']);
    }

    public function getTotalManagerAmount($course_id = null, $date_ranger = null)
    {
        $childAffQuery = CommissionIncome::find()
            ->alias('i')
            ->andWhere(['i.affiliate_manager_id' => $this->user_id])
            ->andWhere(['!=', 'i.course_id', 10])
            ->andWhere(['is not', 'i.id', NULL])
            ->andWhere(['>', 'i.affiliate_manager_amount', '0']);
        if (!empty($course_id)) {
            $childAffQuery->andFilterWhere(['i.course_id' => $course_id]);
        }
        if (!empty($date_ranger)) {
            $dateRange = explode(' - ', $date_ranger);
            $beginDate = date_create_from_format('d/m/Y H:i:s', $dateRange[0] . ' 00:00:00');
            $endDate = date_create_from_format('d/m/Y H:i:s', $dateRange[1] . ' 23:59:59');

            $childAffQuery->andWhere(['>=', 'i.created_time', $beginDate->getTimestamp()]);
            $childAffQuery->andWhere(['<=', 'i.created_time', $endDate->getTimestamp()]);
        }

        return $childAffQuery->sum('i.affiliate_manager_amount');
    }

    public function getTotalChildAmount($course_id, $date_ranger)
    {
        $childAffQuery = CommissionIncome::find()
            ->alias('i')
            ->andWhere(['i.affiliate_manager_id' => $this->user_id])
            ->andWhere(['!=', 'i.course_id', 10])
            ->andWhere(['is not', 'i.id', NULL])
            ->andWhere(['>', 'i.affiliate_manager_amount', '0']);
        if (!empty($course_id)) {
            $childAffQuery->andFilterWhere(['i.course_id' => $course_id]);
        }
        if (!empty($date_ranger)) {
            $dateRange = explode(' - ', $date_ranger);
            $beginDate = date_create_from_format('d/m/Y H:i:s', $dateRange[0] . ' 00:00:00');
            $endDate = date_create_from_format('d/m/Y H:i:s', $dateRange[1] . ' 23:59:59');

            $childAffQuery->andWhere(['>=', 'i.created_time', $beginDate->getTimestamp()]);
            $childAffQuery->andWhere(['<=', 'i.created_time', $endDate->getTimestamp()]);
        }

        return $childAffQuery->sum('i.affiliate_commission_amount');
    }
}
