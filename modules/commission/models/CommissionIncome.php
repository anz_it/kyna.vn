<?php

namespace kyna\commission\models;

use kyna\commission\Commission;
use kyna\order\models\OrderDetails;
use Yii;
use kyna\user\models\User;
use kyna\course\models\Course;
use kyna\course\models\Teacher;
use kyna\order\models\Order;
use kyna\promotion\models\Promotion;

/**
 * This is the model class for table "{{%commission_incomes}}".
 *
 * @property integer $id
 * @property integer $order_id
 * @property integer $course_id
 * @property integer $user_id
 * @property float $total_amount
 * @property float $fee
 * @property double $affiliate_commission_percent
 * @property float $affiliate_commission_amount
 * @property integer $teacher_id
 * @property double $teacher_commission_percent
 * @property float $teacher_commission_amount
 * @property float $kyna_commission_amount
 * @property integer $commission_caculation_id
 * @property integer $created_time
 * @property float $affiliate_manager_percent
 * @property double $affiliate_manager_amount
 * @property integer $affiliate_manager_id
 * @property float $acp_with_teacher
 */
class CommissionIncome extends \kyna\base\ActiveRecord
{
    
    public $date_ranger;
    public $affiliate_total_amount;
    public $teacher_total_amount;
    public $total_reg_filter;
    public $affiliate_category_id;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%commission_incomes}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_id', 'course_id'], 'required'],
            [['order_id', 'course_id', 'user_id', 'teacher_id', 'commission_caculation_id', 'created_time', 'affiliate_manager_id'], 'integer'],
            [['total_amount', 'fee', 'affiliate_commission_amount', 'teacher_commission_amount', 'kyna_commission_amount', 'affiliate_manager_amount'], 'integer', 'integerOnly' => false],
            [['affiliate_commission_percent', 'teacher_commission_percent', 'affiliate_manager_percent'], 'number'],
            [['order_id', 'course_id'], 'unique', 'targetAttribute' => ['order_id', 'course_id'], 'message' => 'The combination of Order ID and Course ID has already been taken.'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order_id' => 'Order ID',
            'course_id' => 'Khóa học',
            'user_id' => 'Người giới thiệu',
            'total_amount' => 'Tổng thu nhập',
            'affiliate_commission_percent' => 'Phần trăm hoa hồng giới thiệu',
            'affiliate_commission_amount' => 'Thu nhập giới thiệu',
            'teacher_id' => 'Giảng viên',
            'teacher_commission_percent' => 'Phần trăm hoa hồng giảng dạy',
            'teacher_commission_amount' => 'Thu nhập giảng dạy',
            'kyna_commission_amount' => 'Thu nhập Kyna',
            'commission_caculation_id' => 'Công thức tính',
            'created_time' => 'Ngày thu nhập',
            'affiliate_total_amount' => 'Tổng thu nhập',
            'teacher_total_amount' => 'Tổng thu nhập',
            'affiliate_manager_id' => 'Affiliate Manager',
            'affiliate_manager_percent' => 'Phần trăm hoa hồng từ manager',
            'affiliate_manager_amount' => 'Thu nhập từ manager',
            '$acp_with_teacher' => 'Phần trăm hoa hồng giới thiệu - teacher'
        ];
    }
    
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
    
    public function getAffiliateUser()
    {
        return $this->hasOne(AffiliateUser::className(), ['user_id' => 'user_id']);
    }
    
    public function getCourse()
    {
        return $this->hasOne(Course::className(), ['id' => 'course_id']);
    }
    
    public function getTeacher()
    {
        return $this->hasOne(Teacher::className(), ['id' => 'teacher_id']);
    }
    
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }

    public function getCommissionIncomeByUserId() {
        return self::find()->where(['user_id' => $this->user_id]);
    }

    public function getAffiliateCommissionIncomeByTeacherId() {
        return self::find()->where(['user_id' => $this->teacher_id]);
    }

    public function getCommissionIncomeByAffiliateCategoryId() {
        $query = self::find();
        if ($this->affiliateUser != null) {
            $query->joinWith('affiliateUser');
            $query->andWhere([AffiliateUser::tableName() . '.affiliate_category_id' => $this->affiliateUser->affiliate_category_id]);
        }
        return $query;
    }

    /**
     * @desc get total affiliate commission amount by teacher_id
     * @return number|null
     */
    public function getTotalAffiliateCommissionAmount($date_range) {
        $query = $this->getAffiliateCommissionIncomeByTeacherId();
        if (!empty($date_range)) {
            $dateRange = explode(' - ', $date_range);
            $beginDate = date_create_from_format('d/m/Y H:i:s', $dateRange[0] . ' 00:00:00');
            $endDate = date_create_from_format('d/m/Y H:i:s', $dateRange[1] . ' 23:59:59');

            $query->andWhere(['>=', 'created_time', $beginDate->getTimestamp()]);
            $query->andWhere(['<=', 'created_time', $endDate->getTimestamp()]);
        }
        return $query->sum('affiliate_commission_amount');
    }

    /**
     * @desc get total user paid by teacher_id
     * @return number|null
     */
    public function getTotalUserPaid($date_range, $is_paid = false) {
        $query = self::find()->where(['teacher_id' => $this->teacher_id]);
        if (!empty($date_range)) {
            $dateRange = explode(' - ', $date_range);
            $beginDate = date_create_from_format('d/m/Y H:i:s', $dateRange[0] . ' 00:00:00');
            $endDate = date_create_from_format('d/m/Y H:i:s', $dateRange[1] . ' 23:59:59');

            $query->andWhere(['>=', 'created_time', $beginDate->getTimestamp()]);
            $query->andWhere(['<=', 'created_time', $endDate->getTimestamp()]);
        }
        if ($is_paid) {
            $query->andWhere(['!=', 'teacher_commission_amount', 0]);
        }
        return $query->count();
    }

    public function getTotalUserFree($date_range) {

        return $this->total_reg_filter - $this->getTotalUserPaid($date_range, true);
    }

    public function getRealIncome()
    {
        $realIncome = $this->total_amount - ($this->total_amount *  $this->acp_with_teacher / 100);
        return $realIncome;
    }
    
    public function getExplainTotalText()
    {
        $formatter = Yii::$app->formatter;
        $order = $this->order;
        $affiliateUser = '';
        
        $result = "Học phí thực nhận: {$formatter->asCurrency($this->total_amount)}<br><br>";
        
        if (!empty($order->affiliate_id)) {
            if ($order->affiliate_id == $this->teacher_id){
                $affiliateUser = 'Giảng viên';
                $isTeacher = true;
            }
            else {
                $affiliateUser = 'KYNA';
            }

            $affiliatePercent = $this->affiliate_commission_percent;


            //truong.nguyen@kyna.vn


            $affiliatePercent =  $this->acp_with_teacher;
           /* if (!empty($this->commissionCalculation)) {

               // $affiliatePercent = 100 - $this->commissionCalculation->instructor_percent;

            }*/
            $result .= "Người giới thiệu: $affiliateUser, được hưởng {$affiliatePercent}% x  {$formatter->asCurrency($this->total_amount)} = {$formatter->asCurrency($this->total_amount * $affiliatePercent / 100)}<br><br>";
        }
        
        $result .= "Tiền $affiliateUser thực nhận từ học viên: {$formatter->asCurrency($this->realIncome)}<br><br>";
        $result .= "Code sử dụng: ";
        if (!empty($order->promotion_code)) {
            $order_detail = OrderDetails::find()->andWhere(['order_id' => $order->id, 'course_id' => $this->course_id])->one();
            if(!empty($order_detail->promo_code)){
                $result .= $order->promotion_code;
                $promotion = $order->promotion;
                if (!is_null($promotion)) {
                    $type = $promotion->type == Promotion::KIND_COURSE_APPLY ? 'Coupon' : 'Voucher';

                    if ($promotion->discount_type == Promotion::TYPE_PERCENTAGE) {
                        $result .= " ($type, giảm {$promotion->value}%)";
                    } elseif ($promotion->discount_type == Promotion::TYPE_FEE) {
                        $result .= " ($type, giảm {$formatter->asCurrency($promotion->value)})";
                    } elseif ($promotion->type == Promotion::TYPE_PARITY) {
                        $result .= " ($type, đưa học phí về {$formatter->asCurrency($promotion->value)})";
                    }
                }
            }else{
                $result .= "Không có";
            }


        } else {
            $result .= "Không có";
        }
        
        return $result;
    }

    public function getAffiliateCategory()
    {
        return AffiliateCategory::findOne($this->affiliate_category_id);
    }

    public function getCommissionCalculation()
    {
        return $this->hasOne(CommissionCalculation::className(), ['id' => 'commission_caculation_id']);
    }

    public function getAfffilicationCommisionPercentByCourse()
    {

    }
}
