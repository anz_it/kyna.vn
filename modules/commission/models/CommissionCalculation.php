<?php

namespace kyna\commission\models;

use Yii;
use kyna\commission\Affiliate;

/**
 * This is the model class for table "{{%commission_calculations}}".
 *
 * @property integer $id
 * @property integer $affiliate_group_id
 * @property double $instructor_percent
 * @property double $affiliate_percent
 * @property integer $start_date
 * @property integer $end_date
 * @property boolean $is_deleted
 * @property integer $status
 * @property integer $created_time
 * @property integer $updated_time
 */
class CommissionCalculation extends \kyna\base\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%commission_calculations}}';
    }
    
    public static function softDelete()
    {
        return true;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['affiliate_group_id', 'instructor_percent', 'affiliate_percent'], 'required'],
            [['affiliate_group_id', 'status', 'created_time', 'updated_time'], 'integer'],
            [['instructor_percent', 'affiliate_percent'], 'number'],
            ['start_date', 'validateStartDate'],
            [['start_date', 'end_date'], 'safe'],
            [['is_deleted'], 'boolean'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'affiliate_group_id' => 'Group',
            'instructor_percent' => 'Instructor Percent',
            'affiliate_percent' => 'Affiliate Percent',
            'start_date' => 'Start Date',
            'end_date' => 'End Date',
            'is_deleted' => 'Is Deleted',
            'status' => 'Status',
            'created_time' => 'Created Time',
            'updated_time' => 'Updated Time',
        ];
    }

    public function getAffiliateGroup()
    {
        return $this->hasOne(AffiliateGroup::className(), ['id' => 'affiliate_group_id']);
    }
    
    public function validateStartDate($attribute)
    {
        if (!$this->hasErrors($attribute)) {
            $dateTimeFormat = str_replace('php:', '', Yii::$app->formatter->datetimeFormat);
            $startDate = date_create_from_format($dateTimeFormat, $this->start_date);
            $endDate = date_create_from_format($dateTimeFormat, $this->end_date);
            if ($startDate !== false && $endDate !== false && $startDate > $endDate) {
                $this->addError($attribute, 'Start Date phải trước End Date');
            }
        }
    }
    
    public function beforeSave($insert)
    {
        $ret = parent::beforeSave($insert);
        
        $dateTimeFormat = str_replace('php:', '', Yii::$app->formatter->datetimeFormat);
        if (!empty($this->start_date)) {
            $startDate = date_create_from_format($dateTimeFormat, $this->start_date);
            if ($startDate !== false) {
                $this->start_date = $startDate->getTimestamp();
            }
        }
        
        if (!empty($this->end_date)) {
            $endDate = date_create_from_format($dateTimeFormat, $this->end_date);
            if ($endDate !== false) {
                $this->end_date = $endDate->getTimestamp();
            }
        }
        
        return $ret;
    }
    
    public static function getCurrentAvailableByGroup($affGroupId = false)
    {
        if ($affGroupId == false) {
            $noneGroup = AffiliateGroup::find()->andWhere(['is_none' => self::BOOL_YES])->one();
            $affGroupId = $noneGroup != null ? $noneGroup->id : null;
        }

        $query = self::find()->andWhere(['affiliate_group_id' => $affGroupId, 'status' => self::STATUS_ACTIVE]);

        $now = time();
        $query->andWhere("start_date IS NULL OR (start_date IS NOT NULL AND start_date <= $now)");
        $query->andWhere("end_date IS NULL OR (end_date IS NOT NULL AND end_date >= $now)");

        return $query->one();
    }
    
}
