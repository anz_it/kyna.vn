<?php

namespace kyna\commission\models;

use kyna\course\models\Course;
use Yii;

/**
 * This is the model class for table "{{%affiliate_courses}}".
 *
 * @property integer $id
 * @property integer $course_id
 * @property integer $affiliate_category_id
 * @property double $commission_percent
 * @property integer $status
 * @property integer $created_time
 * @property integer $updated_time
 */
class AffiliateCategoryCourse extends \kyna\base\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%affiliate_category_courses}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['course_id', 'commission_percent', 'affiliate_category_id'], 'required'],
            [['course_id', 'affiliate_category_id', 'status', 'created_time', 'updated_time'], 'integer'],
            [['commission_percent'], 'number', 'min' => 0, 'max' => 100],
            [['affiliate_category_id'], 'unique', 'targetAttribute' => ['course_id', 'affiliate_category_id'], 'comboNotUnique' => 'Thiết lập giữa khóa học và loại affiliate này đã tồn tại'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'course_id' => 'Khóa học',
            'affiliate_category_id' => 'Loại Affiliate',
            'commission_percent' => 'Phần trăm hoa hồng',
            'status' => 'Trạng thái',
            'created_time' => 'Ngày tạo',
            'updated_time' => 'Ngày cập nhật',
        ];
    }

    public function getAffiliateCategory()
    {
        return $this->hasOne(AffiliateCategory::className(), ['id' => 'affiliate_category_id']);
    }

    public function getCourse()
    {
        return $this->hasOne(Course::className(), ['id' => 'course_id']);
    }
}
