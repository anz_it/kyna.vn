<?php

namespace kyna\commission\models;

use kyna\user\models\User;
use Yii;

/**
 * This is the model class for table "kid_affiliate_income".
 *
 * @property integer $id
 * @property integer $order_id
 * @property string $invoice_code
 * @property integer $order_status_id
 * @property string $order_status
 * @property string $register_email
 * @property string $register_name
 * @property string $register_phone_number
 * @property string $category
 * @property integer $affiliate_id
 * @property integer $total_amount
 * @property double $affiliate_commission_amount
 * @property string $description
 * @property integer $created_time
 * @property integer $updated_time
 */
class KidAffiliateIncome extends \kyna\base\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'kid_affiliate_income';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_id', 'affiliate_id', 'total_amount', 'created_time', 'updated_time', 'order_status_id'], 'integer'],
            [['affiliate_commission_amount'], 'number'],
            [['register_email', 'register_name', 'category', 'order_status', 'invoice_code'], 'string', 'max' => 255],
            [['register_phone_number'], 'string', 'max' => 20],
            [['description'], 'string', 'max' => 500],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order_id' => 'Order ID',
            'invoice_code' => 'Mã đơn hàng',
            'register_email' => 'Email đăng ký',
            'register_name' => 'Tên học viên',
            'register_phone_number' => 'Số điện thoại',
            'category' => 'Loại khóa học',
            'affiliate_id' => 'Affiliate ID',
            'total_amount' => 'Thực nhận học phí',
            'affiliate_commission_amount' => 'Hoa hồng',
            'description' => 'Mô tả',
            'created_time' => 'Ngày tạo',
            'updated_time' => 'Ngày cập nhật',
        ];
    }
    public function getAffiliateUser()
    {
        return $this->hasOne(AffiliateUser::className(), ['user_id' => 'affiliate_id']);
    }
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'affiliate_id']);
    }
}
