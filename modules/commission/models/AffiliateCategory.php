<?php

namespace kyna\commission\models;

use Yii;
use kyna\commission\Affiliate;

/**
 * This is the model class for table "{{%affiliate_categories}}".
 *
 * @property integer $id
 * @property string $name
 * @property integer $affiliate_group_id
 * @property boolean $is_override_commission
 * @property double $commission_percent
 * @property integer $default_cookie_day
 * @property boolean $is_deleted
 * @property integer $status
 * @property integer $created_time
 * @property integer $updated_time
 * @property string $category_can_not_override
 */
class AffiliateCategory extends \kyna\base\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%affiliate_categories}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'affiliate_group_id'], 'required'],
            [['affiliate_group_id', 'default_cookie_day', 'status', 'created_time', 'updated_time'], 'integer'],
            [['is_override_commission', 'is_deleted'], 'boolean'],
            [['commission_percent'], 'number', 'max' => 100],
            ['commission_percent', 'number', 'min' => 1, 'when' => function ($model) {
                    return $model->is_override_commission;
            }, 'whenClient' => "function (attribute, value) {
                return $('#affiliatecategory-is_override_commission').is('checked');
            }"],
            [['name'], 'string', 'max' => 50],
            [['category_can_not_override'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Tên',
            'affiliate_group_id' => 'Group',
            'is_override_commission' => 'Thiết lập phần trăm hoa hồng riêng cho loại Affiliate này',
            'commission_percent' => 'Phần trăm hoa hồng',
            'default_cookie_day' => 'Số ngày lưu Cookie',
            'is_deleted' => 'Is Deleted',
            'status' => 'Trạng thái',
            'created_time' => 'Created Time',
            'updated_time' => 'Updated Time',
            'category_can_not_override' => 'Danh sách loại affiliate không thể ghi đè',
        ];
    }

    public function beforeSave($insert)
    {
        if (is_array($this->category_can_not_override)) {
            $this->category_can_not_override = implode(',', $this->category_can_not_override);
        }
        return parent::beforeSave($insert);
    }

    public function afterFind()
    {
        if (!empty($this->category_can_not_override) && !is_array($this->category_can_not_override)) {
            $this->category_can_not_override = explode(',', $this->category_can_not_override);
        } else {
            $this->category_can_not_override = [];
        }
        parent::afterFind();
    }

    public function getAffiliateGroup()
    {
        return $this->hasOne(AffiliateGroup::className(), ['id' => 'affiliate_group_id']);
    }

    public function getCommissionPercent()
    {
        if ($this->is_override_commission) {
            return $this->commission_percent;
        }

        $comCalculation = CommissionCalculation::getCurrentAvailableByGroup($this->affiliate_group_id);
        return $comCalculation->affiliate_percent;
    }
}
