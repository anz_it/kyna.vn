<?php

namespace kyna\commission\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use kyna\commission\models\AffiliateUser;

/**
 * AffiliateUserSearch represents the model behind the search form about `kyna\affiliate\models\AffiliateUser`.
 */
class AffiliateUserSearch extends AffiliateUser
{
    public $email;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'affiliate_category_id', 'cookie_day', 'status', 'created_time', 'updated_time', 'manager_id', 'is_manager'], 'integer'],
            [['is_override_commission', 'is_deleted'], 'boolean'],
            [['commission_percent'], 'number'],
            [['commission_percent', 'cookie_day'], 'trim'],
            [['email'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = AffiliateUser::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'created_time' => SORT_DESC
                ]
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'affiliate_category_id' => $this->affiliate_category_id,
            'is_override_commission' => $this->is_override_commission,
            'commission_percent' => $this->commission_percent,
            'cookie_day' => $this->cookie_day,
            'is_deleted' => $this->is_deleted,
            'status' => $this->status,
            'created_time' => $this->created_time,
            'updated_time' => $this->updated_time,
            'manager_id' => $this->manager_id,
            'is_manager' => $this->is_manager
        ]);

        if (!empty ($this->email)) {
            $query->joinWith("user");
            $query->andFilterWhere(["like", "user.email" , $this->email]);
        }
        
        //$query->orderBy('created_time DESC');


        return $dataProvider;
    }
}
