<?php

namespace kyna\commission\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use kyna\commission\models\AffiliateCategory;

/**
 * AffiliateCategorySearch represents the model behind the search form about `kyna\affiliate\models\AffiliateCategory`.
 */
class AffiliateCategorySearch extends AffiliateCategory
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'name', 'affiliate_group_id', 'is_override_commission', 'commission_percent', 'default_cookie_day', 'status', 'created_time', 'updated_time'], 'integer'],
            [['is_deleted'], 'boolean'],
            [['id', 'default_cookie_day'], 'trim'],
            [['category_can_not_override'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = AffiliateCategory::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'name' => $this->name,
            'affiliate_group_id' => $this->affiliate_group_id,
            'is_override_commission' => $this->is_override_commission,
            'commission_percent' => $this->commission_percent,
            'default_cookie_day' => $this->default_cookie_day,
            'is_deleted' => $this->is_deleted,
            'status' => $this->status,
            'created_time' => $this->created_time,
            'updated_time' => $this->updated_time,
        ]);

        return $dataProvider;
    }
}
