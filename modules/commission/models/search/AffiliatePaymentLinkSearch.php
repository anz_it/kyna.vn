<?php

namespace kyna\commission\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use kyna\commission\models\AffiliatePaymentLink;

/**
 * AffiliatePaymentLinkSearch represents the model behind the search form about `kyna\commission\models\AffiliatePaymentLink`.
 */
class AffiliatePaymentLinkSearch extends AffiliatePaymentLink
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'affiliate_id', 'status', 'created_time', 'updated_time'], 'integer'],
            [['code', 'course_ids'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = AffiliatePaymentLink::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'affiliate_id' => $this->affiliate_id,
            'status' => $this->status,
            'created_time' => $this->created_time,
            'updated_time' => $this->updated_time,
        ]);

        $query->andFilterWhere(['like', 'code', $this->code])
            ->andFilterWhere(['like', 'course_ids', $this->course_ids]);

        return $dataProvider;
    }
}
