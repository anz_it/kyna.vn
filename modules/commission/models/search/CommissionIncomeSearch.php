<?php

namespace kyna\commission\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use kyna\commission\models\CommissionIncome;
use app\modules\commission\models\AffiliateUser;

/**
 * CommissionIncomeSearch represents the model behind the search form about `kyna\commission\models\CommissionIncome`.
 */
class CommissionIncomeSearch extends CommissionIncome
{
    
    public $affiliate_category_id;
    public $is_manager;
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'order_id', 'course_id', 'user_id', 'affiliate_commission_amount', 'teacher_id', 'teacher_commission_amount', 'kyna_commission_amount', 'commission_caculation_id', 'created_time'], 'integer'],
            [['affiliate_commission_percent', 'teacher_commission_percent', 'affiliate_category_id'], 'number'],
            [['date_ranger', 'is_manager'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = CommissionIncome::find();
        
        $query->alias('t');
        
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'attributes' => ['t.id', 'created_time', 'total_amount', 'affiliate_commission_amount'],
                'defaultOrder' => [
                    'created_time' => SORT_DESC,
                    't.id' => SORT_DESC
                ]
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }
        if (!empty($this->teacher_id)) {
            $query->andWhere(['!=', 't.teacher_commission_percent', 0]);
        } elseif (!empty($this->user_id)) {
            $query->andWhere(['>', 't.affiliate_commission_amount', 0]);
        }
        $query->andWhere(['!=', 't.course_id', 10]);

        $query->andFilterWhere([
            't.user_id' => $this->user_id,
            't.teacher_id' => $this->teacher_id,
            't.course_id' => $this->course_id,
        ]);
        
        if (!empty($this->date_ranger)) {
            $dateRange = explode(' - ', $this->date_ranger);
            $beginDate = date_create_from_format('d/m/Y H:i:s', $dateRange[0] . ' 00:00:00');
            $endDate = date_create_from_format('d/m/Y H:i:s', $dateRange[1] . ' 23:59:59');

            $query->andWhere(['>=', 't.created_time', $beginDate->getTimestamp()]);
            $query->andWhere(['<=', 't.created_time', $endDate->getTimestamp()]);
        }

        if (!empty($this->affiliate_manager_id)) {
            $query->andWhere(['t.affiliate_manager_id' => $this->affiliate_manager_id]);
        }
        
        return $dataProvider;
    }
    
    public function affiliateSearch($params)
    {
        $query = AffiliateUser::find();
        $query->alias('t');

        $tblCommissionIncome = self::tableName();
        if (!empty($this->affiliate_manager_id)) {
            $query->leftJoin($tblCommissionIncome . ' ci', 'ci.user_id = t.user_id AND ci.affiliate_manager_id = ' . $this->affiliate_manager_id);
        } else {
            $query->leftJoin($tblCommissionIncome . ' ci', 'ci.user_id = t.user_id');
        }


        $query->select('t.user_id, sum(ci.affiliate_commission_amount) as affiliate_total_amount');
        $query->orderBy('affiliate_total_amount DESC');
        
        $query->andWhere(['!=', 't.user_id', 0]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            't.user_id' => $this->user_id,
        ]);

        if (!empty($this->affiliate_manager_id)) {
            $query->andFilterWhere([
                't.manager_id' => $this->affiliate_manager_id,
            ]);
        }
        
        if (!empty($this->date_ranger)) {
            $dateRange = explode(' - ', $this->date_ranger);
            $beginDate = date_create_from_format('d/m/Y H:i:s', $dateRange[0] . ' 00:00:00');
            $endDate = date_create_from_format('d/m/Y H:i:s', $dateRange[1] . ' 23:59:59');

            $query->andWhere(['>=', 'ci.created_time', $beginDate->getTimestamp()]);
            $query->andWhere(['<=', 'ci.created_time', $endDate->getTimestamp()]);
        }
        
        if (!empty($this->affiliate_category_id)) {
            $query->andWhere(['t.affiliate_category_id' => $this->affiliate_category_id]);
        }

        if (!empty($this->is_manager)) {
            $query->andWhere(['t.is_manager' => $this->is_manager]);
        }
        
        $query->groupBy('t.user_id');

        return $dataProvider;
    }
    
    public function teacherSearch($params,$isSearchAllUserFollow = true)
    {
        $query = CommissionIncome::find();
        $query->with('order');
        
        $query->alias('t');
        $query->select('t.teacher_id, sum(t.teacher_commission_amount) as teacher_total_amount');
        $query->orderBy('teacher_total_amount DESC');
        
        $query->andWhere(['!=', 't.teacher_id', 0]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }
        
        $query->andFilterWhere([
            't.teacher_id' => $this->teacher_id,
        ]);

        if (!empty($this->date_ranger)) {
            $dateRange = explode(' - ', $this->date_ranger);
            $beginDate = date_create_from_format('d/m/Y H:i:s', $dateRange[0] . ' 00:00:00');
            $endDate = date_create_from_format('d/m/Y H:i:s', $dateRange[1] . ' 23:59:59');

            $query->andWhere(['>=', 't.created_time', $beginDate->getTimestamp()]);
            $query->andWhere(['<=', 't.created_time', $endDate->getTimestamp()]);
        }

        if(!$isSearchAllUserFollow){
            $query->rightJoin('user_follow_teacher a','t.teacher_id = a.user_teacher');
            $query->andWhere(['user_follow'=> \Yii::$app->user->getId()]);
        }
        $query->groupBy('t.teacher_id');

        return $dataProvider;
    }

    public function exportCategorySearch($params)
    {
        $query = CommissionIncome::find();

        $query->alias('t');
        $query->select('t.user_id, sum(t.affiliate_commission_amount) as affiliate_total_amount, count(t.id) as total_reg_filter');
        $query->orderBy('affiliate_total_amount DESC');

        $query->andWhere(['!=', 't.user_id', 0]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            't.user_id' => $this->user_id,
        ]);

        if (!empty($this->date_ranger)) {
            $dateRange = explode(' - ', $this->date_ranger);
            $beginDate = date_create_from_format('d/m/Y H:i:s', $dateRange[0] . ' 00:00:00');
            $endDate = date_create_from_format('d/m/Y H:i:s', $dateRange[1] . ' 23:59:59');

            $query->andWhere(['>=', 't.created_time', $beginDate->getTimestamp()]);
            $query->andWhere(['<=', 't.created_time', $endDate->getTimestamp()]);
        }

        if (!empty($this->affiliate_category_id)) {
            $query->joinWith('affiliateUser');
            $query->andWhere([AffiliateUser::tableName() . '.affiliate_category_id' => $this->affiliate_category_id]);
        }

        $query->groupBy('t.user_id');

        return $dataProvider;
    }

    public function exportSearch($params)
    {
        $query = CommissionIncome::find();
        $query->joinWith('affiliateUser');
        $tblAffUser = AffiliateUser::tableName();

        $query->alias('t');
        $query->select($tblAffUser . '.affiliate_category_id, sum(t.affiliate_commission_amount) as affiliate_total_amount, count(t.id) as total_reg_filter');
        $query->orderBy('affiliate_total_amount DESC');

        $query->andWhere(['!=', 't.user_id', 0]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            $query->where('0=1');
            return $dataProvider;
        }

        if (!empty($this->date_ranger)) {
            $dateRange = explode(' - ', $this->date_ranger);
            $beginDate = date_create_from_format('d/m/Y H:i:s', $dateRange[0] . ' 00:00:00');
            $endDate = date_create_from_format('d/m/Y H:i:s', $dateRange[1] . ' 23:59:59');

            $query->andWhere(['>=', 't.created_time', $beginDate->getTimestamp()]);
            $query->andWhere(['<=', 't.created_time', $endDate->getTimestamp()]);
        }

        $query->andWhere(['!=', $tblAffUser . '.affiliate_category_id', 0]);
        $query->groupBy($tblAffUser . '.affiliate_category_id');

        return $dataProvider;
    }

    public function exportTeacherSearch($params)
    {
        $query = CommissionIncome::find()->joinWith('teacher');

        $query->alias('t');
        $query->select('t.teacher_id, sum(t.teacher_commission_amount) as teacher_total_amount, count(t.id) as total_reg_filter');
        $query->orderBy('teacher_total_amount DESC');

        $query->andWhere(['!=', 't.teacher_id', 0]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            't.teacher_id' => $this->teacher_id,
        ]);

        if (!empty($this->date_ranger)) {
            $dateRange = explode(' - ', $this->date_ranger);
            $beginDate = date_create_from_format('d/m/Y H:i:s', $dateRange[0] . ' 00:00:00');
            $endDate = date_create_from_format('d/m/Y H:i:s', $dateRange[1] . ' 23:59:59');

            $query->andWhere(['>=', 't.created_time', $beginDate->getTimestamp()]);
            $query->andWhere(['<=', 't.created_time', $endDate->getTimestamp()]);
        }

        $query->groupBy('t.teacher_id');

        return $dataProvider;
    }
}
