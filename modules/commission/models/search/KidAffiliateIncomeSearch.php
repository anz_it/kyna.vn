<?php
/**
 * Created by PhpStorm.
 * User: Tan
 * Date: 11/7/2018
 * Time: 1:58 PM
 */

namespace kyna\commission\models\search;


use app\components\ActiveDataProvider;
use kyna\commission\models\KidAffiliateIncome;

class KidAffiliateIncomeSearch extends KidAffiliateIncome
{
    public function fields()
    {
        return ['invoice_code', 'created_time', 'register_email', 'register_name', 'register_phone_number', 'category', 'order_status_id', 'category'];
    }

    public function search($params)
    {
        $query = self::find();

        $query->alias('t');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'attributes' => ['t.id', 'created_time', 'total_amount', 'affiliate_commission_amount'],
                'defaultOrder' => [
                    'created_time' => SORT_DESC,
                    't.id' => SORT_DESC
                ]
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }
        if (!empty($this->order_status_id)) {
            $query->andWhere(['t.order_status_id' => $this->order_status_id]);
        }
        if (!empty($this->affiliate_id)) {
            $query->andWhere(['>', 't.affiliate_commission_amount', 0]);
        }
        if (!empty($this->invoice_code)) {
            $query->andFilterWhere(['like', 'invoice_code', $this->invoice_code]);
        }
        if (!empty($this->register_name)) {
            $query->andFilterWhere(['like', 'name', $this->register_name]);
        }
        if (!empty($this->register_email)) {
            $query->andFilterWhere(['like', 'register_email', $this->register_email]);
        }
        if (!empty($this->register_phone_number)) {
            $query->andFilterWhere(['like', 'register_phone_number', $this->register_phone_number]);
        }
        if (!empty($this->category)) {
            $query->andFilterWhere(['like', 'category', $this->category]);
        }

        $query->andFilterWhere([
            't.affiliate_id' => $this->affiliate_id,
        ]);

        if (!empty($this->date_ranger)) {
            $dateRange = explode(' - ', $this->date_ranger);
            $beginDate = date_create_from_format('d/m/Y H:i:s', $dateRange[0] . ' 00:00:00');
            $endDate = date_create_from_format('d/m/Y H:i:s', $dateRange[1] . ' 23:59:59');

            $query->andWhere(['>=', 't.created_time', $beginDate->getTimestamp()]);
            $query->andWhere(['<=', 't.created_time', $endDate->getTimestamp()]);
        }

        return $dataProvider;
    }
}