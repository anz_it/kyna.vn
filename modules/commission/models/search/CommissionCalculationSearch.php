<?php

namespace kyna\commission\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use kyna\commission\models\CommissionCalculation;

/**
 * CommissionCalculationSearch represents the model behind the search form about `kyna\affiliate\models\CommissionCalculation`.
 */
class CommissionCalculationSearch extends CommissionCalculation
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'affiliate_group_id', 'start_date', 'end_date', 'status', 'created_time', 'updated_time'], 'integer'],
            [['instructor_percent', 'affiliate_percent'], 'number'],
            [['is_deleted'], 'boolean'],
            [['id', 'instructor_percent', 'affiliate_percent'], 'trim'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CommissionCalculation::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'affiliate_group_id' => $this->affiliate_group_id,
            'instructor_percent' => $this->instructor_percent,
            'affiliate_percent' => $this->affiliate_percent,
            'start_date' => $this->start_date,
            'end_date' => $this->end_date,
            'is_deleted' => $this->is_deleted,
            'status' => $this->status,
            'created_time' => $this->created_time,
            'updated_time' => $this->updated_time,
        ]);

        return $dataProvider;
    }
}
