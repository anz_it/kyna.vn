<?php

namespace kyna\commission\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use kyna\commission\models\AffiliateCategoryCourse;

/**
 * AffiliateCategoryCourseSearch represents the model behind the search form about `kyna\commission\models\AffiliateCategoryCourse`.
 */
class AffiliateCategoryCourseSearch extends AffiliateCategoryCourse
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'course_id', 'affiliate_category_id', 'status', 'created_time', 'updated_time'], 'integer'],
            [['commission_percent'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = AffiliateCategoryCourse::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'course_id' => $this->course_id,
            'affiliate_category_id' => $this->affiliate_category_id,
            'commission_percent' => $this->commission_percent,
            'status' => $this->status,
            'created_time' => $this->created_time,
            'updated_time' => $this->updated_time,
        ]);

        return $dataProvider;
    }
}
