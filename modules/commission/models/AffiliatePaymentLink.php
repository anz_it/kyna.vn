<?php

namespace kyna\commission\models;

use Yii;
use common\helpers\DocumentHelper;
use yii\helpers\Url;

/**
 * This is the model class for table "{{%affiliate_payment_links}}".
 *
 * @property integer $id
 * @property integer $affiliate_id
 * @property string $code
 * @property string $course_ids
 * @property integer $status
 * @property integer $created_time
 * @property integer $updated_time
 */
class AffiliatePaymentLink extends \kyna\base\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%affiliate_payment_links}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['affiliate_id', 'course_ids'], 'required'],
            [['affiliate_id', 'status', 'created_time', 'updated_time'], 'integer'],
            [['code'], 'string', 'max' => 20],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'affiliate_id' => 'Affiliate',
            'code' => 'Mã giảm giá',
            'course_ids' => 'Khóa học',
            'status' => 'Trạng thái',
            'created_time' => 'Ngày tạo',
            'updated_time' => 'Ngày cập nhật',
        ];
    }

    public function getLink()
    {
        $courseIdString = implode(',', json_decode($this->course_ids, true));
        $queryParams = [
            'id' => $courseIdString,
            'aff_id' => $this->affiliate_id
        ];
        if (!empty($this->code)) {
            $queryParams['code'] = $this->code;
        }
        $hash = DocumentHelper::hashParams($queryParams);

        return Yii::$app->params['baseUrl'] . Url::to(['/cart/checkout/index',
            'id' => implode(',', json_decode($this->course_ids, true)),
            'code' => !empty($this->code) ? $this->code : null,
            'aff_id' => $this->affiliate_id,
            'hash' => $hash
        ]);
    }
}
