<?php

namespace kyna\commission\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "affiliate_groups".
 *
 * @property integer $id
 * @property string $name
 * @property boolean $is_none
 * @property boolean $is_deleted
 * @property integer $status
 * @property integer $created_time
 * @property integer $updated_time
 */
class AffiliateGroup extends \kyna\base\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'affiliate_groups';
    }

    public static function softDelete()
    {
        return true;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['is_deleted', 'is_none'], 'boolean'],
            [['status', 'created_time', 'updated_time'], 'integer'],
            [['name'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Tên group',
            'is_none' => 'Không có affiliate',
            'is_deleted' => 'Is Deleted',
            'status' => 'Trạng thái',
            'created_time' => 'Ngày tạo',
            'updated_time' => 'Ngày cập nhật',
        ];
    }

    public static function getHtmlSelectData()
    {
        return ArrayHelper::map(self::find()->andWhere(['status' => self::STATUS_ACTIVE])->all(), 'id', 'name');
    }
}
