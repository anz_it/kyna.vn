<?php

namespace kyna\commission\models;

use Yii;

/**
 * This is the model class for table "{{%affiliate_user_rangers}}".
 *
 * @property integer $id
 * @property integer $affiliate_user_id
 * @property double $commission_percent
 * @property integer $cookie_day
 * @property integer $start_date
 * @property integer $end_date
 * @property integer $status
 * @property integer $created_time
 * @property integer $updated_time
 */
class AffiliateUserRanger extends \kyna\base\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%affiliate_user_rangers}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['affiliate_user_id'], 'required'],
            [['affiliate_user_id', 'cookie_day', 'start_date', 'end_date', 'status', 'created_time', 'updated_time'], 'integer'],
            [['commission_percent'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'affiliate_user_id' => 'Affiliate User ID',
            'commission_percent' => 'Commission Percent',
            'cookie_day' => 'Cookie Day',
            'start_date' => 'Start Date',
            'end_date' => 'End Date',
            'status' => 'Status',
            'created_time' => 'Created Time',
            'updated_time' => 'Updated Time',
        ];
    }
}
