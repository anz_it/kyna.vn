<?php

namespace kyna\commission;

use Yii;

class Affiliate
{
    
    const GROUP_NONE = 0;
    const GROUP_TEACHER_OWNER = 1;
    const GROUP_INTERNAL = 2;
    const GROUP_THIRD_PARTY = 3;
    const GROUP_TEACHER_HTS = 4;
    
    public static function getGroupOptions()
    {
        return [
            self::GROUP_NONE => 'None - Học viên tự tìm kiếm khóa học',
            self::GROUP_TEACHER_OWNER => 'Giảng viên giới thiệu khóa học của mình',
            self::GROUP_INTERNAL => 'Nội bộ Kyna',
            self::GROUP_THIRD_PARTY => 'Third party + Giảng viên giới thiệu khóa học không phải của mình',
            self::GROUP_TEACHER_HTS => 'Giảng viên hành trang sống',
        ];
    }
    
}