<?php

use kyna\videomanager\widgets\videoplayer\VideoPlayer;

?>

<?= VideoPlayer::widget([
    'video' => $video,
    'enableHlsJs' => true,
]) ?>
