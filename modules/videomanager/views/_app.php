<?php
use yii\grid\GridView;
use yii\grid\SerialColumn;
use yii\grid\ActionColumn;
use yii\bootstrap\Html;
use yii\helpers\Url;

?>

<!--
<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'id' => 'chooser-grid-view',
    'summary' => '',
    'columns' => [
        [
            'class' => SerialColumn::className()
        ],
        [
            'attribute' => 'name',
            'label' => 'Name',
            'format' => 'raw',
            'value' => function ($model, $key) {
                return Html::textInput(null, '/'.$model['app'].'/'.$model['name'], [
                    'class' => 'form-control input-sm url-copy',
                    'readonly' => true,
                    'id' => 'copy-'.$key,
                ]);
            },
        ],
        'size:shortSize',
        'creationTime:datetime',
        [
            'class' => ActionColumn::className(),
            'template' => '<div class="btn-group" role="group">{preview}{select}</div>',
            'buttons' => [
                'preview' => function($url, $model, $key) {
                    $testUrl = Url::toRoute([
                        '/course/video/preview',
                        'video' => '/'.$model['app'].'/'.$model['name'],
                    ]);
                    return Html::a('<i class="fa fa-play"></i>', $testUrl, [
                        'class' => 'btn btn-default btn-sm',
                        'title' => 'Click to test video',
                        'target' => '_blank',
                    ]);
                },
                'select' => function($url, $model, $key) {
                    return Html::a('Chọn', "#", [
                        'class' => 'btn btn-success btn-sm btn-select',
                        'data-video' => '/'.$model['app'].'/'.$model['name'],
                    ]);
                }
            ],
        ]
    ],
]) ?>
-->
