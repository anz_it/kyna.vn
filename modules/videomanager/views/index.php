<?php
use yii\bootstrap\Html;
use yii\bootstrap\Tabs;
use yii\helpers\Url;
use kyna\videomanager\assets\VideoManagerAsset;

?>
<div class="modal-body">
    <?= Tabs::widget([
        'options' => ['class' => 'video-chooser-tabs'],
        'navType' => 'nav-tabs',
        'items' => $tabItems,
        'renderTabContent' => false,
    ]) ?>
    <input type="hidden" value="<?=$f?>" id="dataf" />
    <div class="tab-content">
        <table  class="table video-chooser-table"
                data-preview-url="<?= $previewUrl ?>"
                data-striped="true"
                data-toggle="table"
                data-url="<?= $dataUrl ?>"
                data-search="true"
                data-pagination="true"
                data-sort-name="creationTime"
                data-sort-order="desc"
                data-cache="false">
            <thead>
                <tr>
                    <th data-formatter="serialFormatter">STT</th>
                    <th data-field="name" data-sortable="true">Tên file</th>
                    <th data-field="size" data-align="right" data-formatter="sizeFormatter" data-sortable="true">Dung lượng</th>
                    <th data-field="creationTime" data-align="right" data-formatter="dateFormatter" data-sortable="true">Ngày upload</th>
                    <th data-field="name" data-formatter="actionFormatter"></th>
                </tr>
            </thead>
        </table>
    </div>
</div>
<style>

</style>
