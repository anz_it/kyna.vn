<?php
namespace kyna\videomanager\models;

use Yii;
use yii\helpers\Inflector;
use yii\base\NotSupportedException;
use yii\base\InvalidRouteException;
use yii\web\Response;

class Video extends \yii\base\Model
{
    const VIDEO_DIR = 'http://media1.kyna.com.vn/videomanager1';
    const ENGINE_MANAGER_URL = 'https://media.kyna.com.vn/enginemanager';
    const RTMP_SERVER = 'rmtp://media.kyna.com.vn:1935';
    const HLS_SERVER = 'https://media.kyna.com.vn:1443';
    const FLOWPLAYER_KEY = false;

    private static function _getData($url)
    {
        if (!($data = Yii::$app->cache->get($url))) {
            $json = file_get_contents($url);
            $data = json_decode($json, true);
            Yii::$app->cache->set($url, $data, 1800);
        }
        return $data;
    }

    public static function getApps()
    {
        return self::_getData(self::VIDEO_DIR);
    }

    public static function getMedia($app)
    {
        $url = self::VIDEO_DIR . '/' . $app . '.json';
        return self::_getData($url);
    }

    public static function getVideoIn($folder)
    {
        $url = self::VIDEO_DIR."?f={$folder}";
        $json = file_get_contents($url);
        $data = json_decode($json, true);

        return $data;

    }

    public static function getVideo($file)
    {
        $url = self::VIDEO_DIR . '/?video=/vod/' . $file;
        return self::_getData($url);
    }
}
