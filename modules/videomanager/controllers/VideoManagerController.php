<?php

namespace kyna\videomanager\controllers;

use Yii;
use yii\helpers\Url;
use yii\helpers\Inflector;
use yii\web\Response;
use kyna\videomanager\models\Video;

class VideoManagerController extends \app\components\controllers\Controller
{

    public $ajaxUrl = '/course/video/';
    public $previewUrl = '/course/video/preview/';

    public function init()
    {
        parent::init();
        $this->setViewPath('@kyna/videomanager/views');
    }

    public function actionIndex($f)
    {
        // $apps = Video::getApps();
        // $app OR $app = $apps[0];

        $apps = ['vod'];
        $app = 'vod';
        $tabItems = [];
        foreach ($apps as $_app) {
            $tabItems[] = [
                'label' => Inflector::titleize($_app),
                'url' => Url::toRoute([$this->ajaxUrl . 'folder/', 'name' => $f]),
                'active' => ($app === $_app),
            ];
        }
        return $this->renderAjax('index', [
            'tabItems' => $tabItems,
            'engineManagerUrl' => Video::ENGINE_MANAGER_URL,
            'app' => $app,
            'dataUrl' => Url::toRoute([$this->ajaxUrl . 'folder/', 'name' => $f]),
            'previewUrl' => $this->previewUrl,
            'f' => implode('/', str_split($f)),
        ]);
    }

    public function actionFolder($name)
    {
        $media = Video::getVideoIn($name);
        Yii::$app->response->format = Response::FORMAT_JSON;
        return $media;
    }

    public function actionApp($name, $search = false)
    {
        $media = Video::getMedia($name);
        foreach ($media as &$medium) {
            $medium['video'] = '/' . $medium['app'] . '/' . $medium['name'];
        }

        Yii::$app->response->format = Response::FORMAT_JSON;

        return $media;
    }

    public function actionPreview($video)
    {
        $js = ';(function($) { $(".flowplayer").flowplayer(); })(jQuery);';
        $this->view->registerJs($js);
        return $this->renderAjax('_preview', [
            'video' => $video,
        ]);
    }

}
