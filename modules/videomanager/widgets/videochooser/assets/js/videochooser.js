+function ($) {
    'use strict';

    var VideoChooser = function (element, options) {
        this.$element = $(element);
        this.$modal = this.$element.find(".modal");

        this.options = options;

        var $input = this.$element.find(".form-control"),
            $modal = this.$modal;

        this.$modal
            .on("show.bs.modal", function (_e) {
                var url = $(_e.relatedTarget).data("url"),
                    isPreview = $(_e.relatedTarget).hasClass("btn-video-preview");


            })
            .on("shown.bs.modal", function (_e) {
                var title = $(_e.relatedTarget).html(),
                    url = $(_e.relatedTarget).data("url"),
                    isPreview = $(_e.relatedTarget).hasClass("btn-video-preview");

                $.ajaxSetup({
                    cache: false
                });

                if (isPreview) {
                    url = url.split(/[?#]/)[0] + "?video=" + $input.val();
                }

                $(this).find(".modal-title").html(title);
                $(this).find(".modal-body").load(url, function () {
                    $(this).find('.video-chooser-table').bootstrapTable();
                });
            })
            .on("hidden.bs.modal", function () {
                $(this).find(".modal-title").html("");
                $(this).find(".modal-body").html("");
            })
            .on("click", ".btn-select", function (_e) {
                console.log(_e);
                _e.preventDefault();
                var video = $(_e.target).data("video");
                $input.val(video);

                $modal.modal("hide");
            })
            .on("click", ".video-chooser-tabs a", function (_e) {
                _e.preventDefault();

                $modal.find(".video-chooser-tabs li").removeClass("active")
                $(_e.target).parent().addClass("active");

                var url = _e.target.href;
                $modal.find('.video-chooser-table').bootstrapTable('refresh', {
                    "url": url
                });
            });
    }

    VideoChooser.VERSION = '1.0.0';

    VideoChooser.DEFAULTS = {}

    function Plugin(option, _relatedTarget, _callback) {
        return this.each(function () {
            var $this = $(this)
            var data = $this.data('k.videochooser')
            var options = $.extend({}, VideoChooser.DEFAULTS, $this.data(), typeof option == 'object' && option)

            if (!data) $this.data('k.videochooser', (data = new VideoChooser(this, options)))
            if (typeof option == 'string') data[option](_relatedTarget, _callback)
        })
    }

    var old = $.fn.videochooser

    $.fn.videochooser = Plugin
    $.fn.videochooser.Constructor = VideoChooser

    $.fn.videochooser.noConflict = function () {
        $.fn.videochooser = old
        return this
    }

    window.sizeFormatter = function (value) {
        if (value == 0) return '0 B';
        var k = 1000; // or 1024 for binary
        var dm = 2;
        var sizes = ['B', 'KiB', 'MiB', 'GiB', 'TiB', 'PiB', 'EiB', 'ZiB', 'YiB'];
        var i = Math.floor(Math.log(value) / Math.log(k));
        return parseFloat((value / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
    }
    window.dateFormatter = function (value) {
        var date = new Date(value * 1000);
        return date.toLocaleString("vi-VN");
    }
    window.serialFormatter = function (value, row, index) {
        return index + 1;
    }
    window.actionFormatter = function (value, row, index) {

        var $btnSelect = $("<a>").attr("href", "#").attr("data-video", dataf.value+'/'+value)

            .addClass("btn btn-success btn-xs btn-select").html("Chọn");
        // $buttonGroup = $("<div class=btn-group>").append($btnPreview).append($btnSelect);

        return $btnSelect.prop('outerHTML');
    }

    $(window).on("load", function () {
        $('[data-video-chooser]').each(function () {
            var $chooser = $(this)
            Plugin.call($chooser, $chooser.data());
        });
    });
}(jQuery);
