<?php
namespace kyna\videomanager\widgets\videochooser\assets;

use yii\web\AssetBundle;

class VideoChooserAsset extends AssetBundle
{
    public $sourcePath = __DIR__;
    public $js = [
        'js/videochooser.js',
    ];
    public $css = [
        'css/videochooser.css',
    ];
    public $depends = [
        'kyna\videomanager\widgets\videochooser\assets\BootstrapTableAsset',
    ];
}
