<?php
namespace kyna\videomanager\widgets\videochooser\assets;

use yii\web\AssetBundle;

class BootstrapTableAsset extends AssetBundle
{
    public $sourcePath = '@bower/bootstrap-table/dist';
    public $js = [
        'bootstrap-table.min.js',
    ];
    public $css = [
        'bootstrap-table.min.css',
    ];
    public $depends = [
        'yii\bootstrap\BootstrapPluginAsset',
    ];
}
