<?php
namespace kyna\videomanager\widgets\videochooser;

use yii\bootstrap\Html;
use kyna\videomanager\widgets\videochooser\assets\VideoChooserAsset;

class VideoChooser extends \yii\widgets\InputWidget
{
    public $chooserUrl;
    public $previewUrl;
    public $inputOptions = [];
    public $lesson;

    public function init()
    {
        parent::init();
        if (empty($this->inputOptions['class'])) {
            $this->inputOptions['class'] = 'form-control';
        }
        //$this->inputOptions['readonly'] = true;
        $this->inputOptions['id'] = $this->options['id'] . '-input';
    }

    protected function registerClientScript()
    {
        $view = $this->getView();
        $assetBundle = VideoChooserAsset::register($view);
    }

    public function run()
    {
        $this->registerClientScript();

        if ($this->hasModel()) {
            $input = Html::activeTextInput($this->model, $this->attribute, $this->inputOptions);
            $value = $this->model->{$this->attribute};
        } else {
            $input = Html::textInput($this->name, $this->value, $this->inputOptions);
            $value = $this->value;
        }

        return $this->render('form-control', [
            'id' => $this->options['id'],
            'input' => $input,
            'chooserUrl' => $this->chooserUrl,
            'previewUrl' => $this->previewUrl,
        ]);
    }
}
