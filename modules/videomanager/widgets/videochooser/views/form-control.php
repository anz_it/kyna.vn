<?php
use yii\bootstrap\Html;
?>
<div class="video-chooser" id="<?= $id ?>" data-video-chooser>
    <div class="input-group">
        <label for="<?= $id.'-input' ?>"><?= $input ?></label>
        <div class="input-group-btn">
            <button type="button" class="btn btn-default btn-video-chooser" data-url="<?= $chooserUrl ?>" data-remote="false" data-target="<?= '#'.$id.'-modal' ?>" data-toggle="modal">
                Chọn video&hellip;
            </button>
            <button type="button" class="btn btn-default btn-video-preview" data-url="<?= $previewUrl ?>" data-remote="false" data-target="<?= '#'.$id.'-modal' ?>" data-toggle="modal">
                <i class="fa fa-play fa-fw"></i> Preview
            </button>
        </div>
    </div>
    <div id="<?= $id.'-modal' ?>" class="modal fade">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <header class="modal-header">
                    <h4 class="modal-title">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    </h4>
                </header>
                <div class="modal-body">Getting data&hellip;</div>
            </div>
        </div>
    </div>
</div>
