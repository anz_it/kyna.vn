<?php
use yii\bootstrap\Html;
/* @var $this \yii\web\View */

$this->registerCss("
    .flowplayer-end-screen {
        color: white;
        z-index: 50;  
        position: inherit;
        width: 100%;
        height: 100%;
        -webkit-border-radius: 20px;
        -moz-border-radius: 20px;
        border-radius: 20px;
        font-size: 14px;
//        background-color: #777;
        background-color: #404040;
//        background-color: transparent;
        text-align: center;
 
        /* do not show endscreen by default - a simpler, non-animated alternative would be:
        * display: none; */
        display: none;
        filter: alpha(opacity=0.5);
        -webkit-transition: opacity .5s;
        -moz-transition: opacity .5s;
        transition: opacity .5s;    
        
        opacity: 0.9
    }
    
    .countdown {
        position: relative;
        margin: auto;
        height: 100px;
        width: 100px;
        text-align: center;
        top: 50%;
        left: 50%;
        margin: -120px 0 0 -50px;
    }

    .countdown-number {
        color: white;
        display: inline-block;
        line-height: 40px;
//        font-size: -webkit-xxx-large;
        font-size: 50px;
        text-align: center;
        margin: 30px -7px 0 0;
        z-index: 99999;
    }
    
    .countdown-cancel {
        margin-top: 15px;
    }

    svg {
        position: absolute;
        top: 0;
        right: 0;
        width: 100px;
        height: 100px;
        transform: rotateZ(270deg);
    }
    
    svg:hover {
        cursor: pointer; 
    }

    svg circle {
        stroke-dasharray: 226px;
        stroke-dashoffset: 0px;
        stroke-linecap: round;
        stroke-width: 5px;
        stroke: white;
        fill: none;
        animation: draw 5s linear;
        animation-play-state: paused; 
        background-color: black; 

    }
    
    .up-next-lesson {
        font-size: x-large;
    }
    
    @media (max-width: 575px) {
        .up-next-lesson {
            font-size: medium;
        }
        .countdown {
            margin: -90px 0 0 -50px;
        }
    }

    .end-screen-close-button {
        float: right;
        width: 40px;
        background-color: transparent;
    }
    
    @keyframes draw {
    from {
        stroke-dashoffset: 226px;
    }
    to {
        stroke-dashoffset: 0px;
    }
}
");

?>
<?= Html::beginTag('div', [
    'id' => 'flowplayerhtml5',
    'class' => 'flowplayer',
    'data-lesson-id' => $lessonId,
    'data-user-course-id' => $userCourseId,
    'data-course-id' => $courseId,
    'style' => ' position: relative; z-index: 2'
]) ?>

<div class="flowplayer-end-screen" >
    <button class="end-screen-close-button countdown-cancel" style="display: none">X</button>
    <div class="up-next-lesson"></div>
    <div id="svg" class="countdown">
        <div class="countdown-number" href="#">&#9658;</div>
        <svg>
            <circle r="36" cx="50" cy="50"></circle>
        </svg>
        <a class="btn btn-default countdown-cancel" style="font-weight: bolder;">HỦY</a>
    </div>
</div>

<?= Html::endTag('div'); ?>





