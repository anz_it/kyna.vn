<?php
use yii\bootstrap\Html;

?>
<?= Html::beginTag('div', [
    'id' => 'flowplayerflash',
    'class' => 'flowplayer',
    'data-lesson-id' => $lessonId,
    'data-user-course-id' => $userCourseId,
    'style' => 'height: 540px; position: relative; z-index: 2'


]) ?>

<?= Html::endTag('div'); ?>
