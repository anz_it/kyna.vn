<?php
use yii\bootstrap\Html;

?>
<?= Html::beginTag('div', [
    'class' => 'flowplayer',
    'data-rtmp' => $rtmpServer ? $rtmpServer . '/' . $video['app'] : false,
    'data-autoplay' => $autoplay ? 'true' : 'false',
    'data-key' => $key,
    'data-swf' => $swf,
    'data-swf-hls' => $swfHls,
    'data-audio' => $audio ? 'true' : false,
    'data-cover-image' => ($coverImage and $audio) ? $coverImage : false,
    'data-live' => $live,
    'data-embed' => $embed,
    'data-hlsjs' => true,
    'data-lesson-id' => $lesson_id,
    'data-user-course-id' => $user_course_id,
    'data-title' => $title,
    'data-analytics' => $analytics,


]) ?>
<video>
    <source type="application/x-mpegurl"

            src="<?= $hlsServer . '/vod1/_definst_/' . $video['prefix'] . $originVideo . '/playlist.m3u8' ?>">
    <?php if ($rtmpServer) : ?>
        <source type="video/flash" src="<?= $video['prefix'] . $video['name'] ?>">
    <?php endif; ?>
</video>
<?= Html::endTag('div'); ?>
