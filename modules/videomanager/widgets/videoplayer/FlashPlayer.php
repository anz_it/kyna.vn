<?php
/**
 * Created by PhpStorm.
 * User: khanhphan
 * Date: 3/13/17
 * Time: 2:14 PM
 */

namespace kyna\videomanager\widgets\videoplayer;

use kyna\videomanager\widgets\videoplayer\assets\FlashAsset;
use kyna\videomanager\widgets\videoplayer\assets\LearningAsset;
use yii\base\Widget;

class FlashPlayer extends Widget
{
    public $video;
    public $lessonId;
    public $userCourseId;
    public $title = '';
    public $key = false;    // false|string
    public $playerOptions = [];

    public function init()
    {
        parent::init();
        $this->registerClientScript();
    }

    protected function registerClientScript()
    {

        $view = $this->getView();
        $assetBundle = FlashAsset::register($view);

        $time = time();
        $script = <<<EOF
        window.player = flowplayer("flowplayerflash", "{$assetBundle->baseUrl}/flowplayer.commercial-3.2.18.swf", {
            debug: false,
            title: '{$this->title}',
            key: '#\$9e934cdbe9535648374',
            clip: {
                autoPlay: false,
                autoBuffering: true,
                url: '{$this->video}',
                scaling: 'fit',
                //provider: 'hddn'
                provider: 'rtmp',
                connectionProvider: 'secure',
                
                onFinish: function (player) {
                
                    window.video = null;
                    window.learning.finish();
                },
                onPause: function (p) {
                
                    window.video = null;
                    window.learning.pause();
                },
                onResume: function (p) {
                
                    window.video = null;
                    window.learning.resume();
                }
            },
          
            plugins: {
                // here is our rtmp plugin configuration
                rtmp: {
                    url: "{$assetBundle->baseUrl}/flowplayer.rtmp-3.2.13.swf",
                    netConnectionUrl: 'rtmp://media.kyna.com.vn:1935/vod2/_definst_'
                },
                secure: {
                    url: "{$assetBundle->baseUrl}/flowplayer.securestreaming-3.2.9.swf",
                    timestamp: $time,
                }
            },

            canvas: {
                backgroundGradient: 'none'
            }
        });
        window.video = window.player.getClip();
    
EOF;


        $view->registerJs($script);
        LearningAsset::register($view);
        return $assetBundle;
    }

    public function run()
    {
        if (empty($this->video)) {
            return ('Video link must be specified');
        }
        return $this->render('flash', [
                'key' => $this->key,
                'lessonId' => $this->lessonId,
                'userCourseId' => $this->userCourseId,
                'title' => $this->title,
                'originVideo' => $this->video
            ] + $this->playerOptions);
    }

}