<?php
namespace kyna\videomanager\widgets\videoplayer;

use kyna\videomanager\widgets\videoplayer\assets\FlowplayerAsset;
use kyna\videomanager\models\Video;
use yii\bootstrap\Html;
use yii\base\Widget;
use yii\base\InvalidConfigException;

class VideoPlayer extends \yii\base\Widget
{
    public $hlsServer;
    public $video;
    public $lesson_id;
    public $user_course_id;
    public $enableHlsJs = true;     // true|false
    public $rtmpServer = false;    // true|false
    public $qualitySelector = false;    // false. Not implemented yet
    public $title = '';
    public $analytics = '';

    public $key = false;    // false|string

    public $playerOptions = [];

    private $_videoInfo;

    // Default options
    private $_playerOptions = [
        'autoplay' => false, // true|false
        'audio' => false, // true|false
        'coverImage' => false, // false|string
    ];


    public function init()
    {
        if (empty($this->video)) {
            return false;
        }

        if (!isset($this->rtmpServer)) {
            $this->rtmpServer = Video::RTMP_SERVER;
        }
        if (!isset($this->hlsServer)) {
            $this->hlsServer = Video::HLS_SERVER;
        }

        parent::init();

        // overried default options if set
        $this->playerOptions = array_merge($this->_playerOptions, $this->playerOptions);

        $this->playerOptions['hlsjs'] = $this->enableHlsJs;

        $this->_videoInfo = Video::getVideo($this->video);
        if ($this->_videoInfo['forceAudio']) {
            $this->playerOptions['audio'] = true;
        }
        if ($this->playerOptions['audio'] === true) {
            $this->enableHlsJs = false;
            $this->playerOptions['hlsjs'] = 'false';
        }

        // run this after playerOptions was set
        $assetBundle = $this->registerClientScript();

        // readonly options
        $this->playerOptions['live'] = 'false';
        $this->playerOptions['embed'] = 'false';
        $this->playerOptions['swf'] = $assetBundle->baseUrl . '/flowplayer.swf';
        $this->playerOptions['swfHls'] = $assetBundle->baseUrl . '/flowplayerhls.swf';
    }

    protected function registerClientScript()
    {
        $view = $this->getView();
        $assetBundle = FlowplayerAsset::register($view);

        if ($this->enableHlsJs === true) {
            $assetBundle->js[] = 'flowplayer.hlsjs.min.js';
        }

        if ($this->playerOptions['audio'] !== false) {
            $assetBundle->js[] = 'flowplayer.audio.min.js';
            $assetBundle->css[] = 'flowplayer.audio.css';
        }
        $assetBundle->js[] = "flowplayer.quality-selector.min.js";
        $assetBundle->css[] = "flowplayer.quality-selector.css";
        $assetBundle->js[] = "flowplayer.drive-analytics.min.js";

        return $assetBundle;
    }

    public function run()
    {
        if (empty($this->video)) {
            return ('Video link must be specified');
        }
        return $this->render('player', [
                'rtmpServer' => $this->rtmpServer,
                'hlsServer' => $this->hlsServer,
                'key' => $this->key,
                'video' => $this->_videoInfo,
                'lesson_id' => $this->lesson_id,
                'user_course_id' => $this->user_course_id,
                'title' => $this->title,
                'analytics' => $this->analytics,
                'originVideo' => $this->video
            ] + $this->playerOptions);
    }
}
