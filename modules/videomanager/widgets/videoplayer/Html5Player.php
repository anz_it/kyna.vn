<?php
/**
 * Created by PhpStorm.
 * User: khanhphan
 * Date: 5/10/17
 * Time: 10:47 AM
 */

namespace kyna\videomanager\widgets\videoplayer;

use kyna\videomanager\widgets\videoplayer\assets\FlowplayerAsset;
use kyna\videomanager\widgets\videoplayer\assets\LearningAsset;
use yii\base\Widget;
use yii\web\View;

class Html5Player extends Widget
{
    public $video;
    public $lessonId;
    public $userCourseId;
    public $courseId;
    public $title = '';
    public $key = false;    // false|string
    public $playerOptions = [];
    public $containerId = 'flowplayerhtml5';

    public function init()
    {
        parent::init();
        $this->registerClientScript();
    }

    protected function registerClientScript()
    {
        $view = $this->getView();
        $assetBundle = FlowplayerAsset::register($view);

        //$assetBundle->js[] = "flowplayer.quality-selector.min.js";
        //$assetBundle->css[] = "flowplayer.quality-selector.css";

        LearningAsset::register($view);
        $videoServer = \Yii::$app->params['videoServer'];
        $script = <<<EOF
        var container = document.getElementById('{$this->containerId}');       
        window.player = flowplayer(container, {
            hlsjs: {
                // strict: true,
                // debug: true,
                maxBufferLength: 30,
                maxMaxBufferLength: 300,
                // maxBufferSize: 60*1000*1000
            },
            autoplay: true,
            embed : false,
            clip: {
                sources: [
                      { type: "application/x-mpegurl",
                        src:  "{$videoServer}/_definst_/{$this->video}/playlist.m3u8" }
                    
                ],
                onFinish: function (p) {
                    console.log(p);
                }
            },
            title: '{$this->title}',
            key: '\$631977711768310',
            debug: false,
        }).on("finish", function (e, api){ 
            window.video = window.player.video;
            window.learning.finish();
        }).on("pause", function (e, api) {
            window.video = window.player.video;
            window.learning.pause();
        }).on("resume", function (e, api) { 
            window.video = window.player.video;
            window.learning.resume();
        }).on("ready", function (e, api) {
            window.video = window.player.video;
            window.learning.ready();
        });
        window.video = window.player.video;
        
EOF;


        $view->registerJs($script, View::POS_END);
        return $assetBundle;
    }

    public function run()
    {
        if (empty($this->video)) {
            return ('Video link must be specified');
        }
        return $this->render('html5', [
                'key' => $this->key,
                'lessonId' => $this->lessonId,
                'userCourseId' => $this->userCourseId,
                'courseId' => $this->courseId,
                'title' => $this->title,
                'originVideo' => $this->video
            ] + $this->playerOptions);
    }

}
