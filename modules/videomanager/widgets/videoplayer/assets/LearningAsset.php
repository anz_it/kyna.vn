<?php
namespace kyna\videomanager\widgets\videoplayer\assets;

use yii\web\AssetBundle;
use yii\web\JqueryAsset;

class LearningAsset extends AssetBundle
{
    public $sourcePath = __DIR__."/learning";
    public $js = [
        'learning.js?v=1504066379',
    ];
    public $css = [

    ];
    public $depends = [
        'yii\web\JqueryAsset',
    ];
}
