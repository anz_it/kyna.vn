<?php
namespace kyna\videomanager\widgets\videoplayer\assets;

use yii\web\AssetBundle;
use yii\web\JqueryAsset;

class FlashAsset extends AssetBundle
{
    public $sourcePath = __DIR__.'/flowplayer/flash';
    public $js = [
        'flowplayer-3.2.13.min.js',
    ];
    public $css = [
    ];
    public $depends = [
        'yii\web\JqueryAsset',
    ];
}
