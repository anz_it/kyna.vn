/**
 * Created by khanhphan on 5/9/17.
 */
function getVideo()
{
    if (window.video == null || window.video.must_reset == 1) {
        window.video = {
            time: window.player.getTime(),
            must_reset: 1,
            duration: window.player.getClip().duration
        }
    }
}
window.learning = {
    interval: null,
    lessonId: null,
    userCourseId: null,
    courseId: null,
    pause: function () {
        window.clearInterval(this.interval);
    },
    resume: function () {
        window.clearInterval(this.interval);
        this.interval = window.setInterval(this.updateProcess, 1000);
    },
    finish: function () {
        window.clearInterval(this.interval);
        this.syncFinish();
        this.goToNextLesson();
    },
    ready: function () {
        window.clearInterval(this.interval);
        this.interval = window.setInterval(this.updateProcess, 1000);
    },
    seek: function () {

    },

    setInterval: function () {

        this.interval = window.setInterval(this.updateProcess, 1000);
    },
    updateProcess: function () {
        getVideo();
        var currentSec = parseFloat(localStorage.getItem("currentSec_" + window.learning.lessonId));
        var currentCount = parseInt(localStorage.getItem("currentCount_" + window.learning.lessonId));
        if (!currentCount)
            currentCount = 0;

        currentCount++;
        localStorage.setItem("currentCount_" + window.learning.lessonId, currentCount);

        if (!currentSec) {
            currentSec = 0;
        }
        if (currentSec < window.video.time) {
            if (currentCount % 10 == 0) {
                window.learning.syncPercent();
            }
            localStorage.setItem("currentSec_" + window.learning.lessonId, window.video.time);
        }
    },
    syncPercent: function () {
        getVideo();
        $.ajax({
            type: 'POST',
            url: '/course/learning/sync-percent',
            data: {duration: window.video.duration, time: window.video.time, lessonId: window.learning.lessonId, userCourseId: window.learning.userCourseId},
            success: function (res) {
                if (res.notify) {
                    $.notify({
                        title: res.title,
                        message: res.message,
                        allow_dismiss: true
                    }, {
                        type: "notify-kpoint",
                        animate: {
                            enter: 'animated bounceInDown',
                            exit: 'animated bounceOutUp'
                        },
                        placement: {
                            from: 'bottom',
                            align: 'right'
                        }
                    });

                    $.ajax({
                        url: '/course/mission/summary?course_id=' + window.learning.courseId,
                        method: 'GET',
                        success: function (res) {
                            $('#mission-tab-content').html(res);
                        }
                    });
                }
            }

        });
    },
    syncFinish: function () {
        getVideo();
        $.ajax({
            type: 'POST',
            url: '/course/learning/sync-finish',
            data: {duration: window.video.duration, time: window.video.time, lessonId: window.learning.lessonId, userCourseId: window.learning.userCourseId},
            success: function (res) {
                if (res.notify) {
                    $.notify({
                        title: res.title,
                        message: res.message,
                        allow_dismiss: true
                    }, {
                        type: "notify-kpoint",
                        animate: {
                            enter: 'animated bounceInDown',
                            exit: 'animated bounceOutUp'
                        },
                        placement: {
                            from: 'bottom',
                            align: 'right'
                        }
                    });

                    $.ajax({
                        url: '/course/mission/summary?course_id=' + window.learning.courseId,
                        method: 'GET',
                        success: function (res) {
                            $('#mission-tab-content').html(res);
                        }
                    });
                }
            }
        })
    },
    goToNextLesson: function () {
        getVideo();
        var upNextLesson =   window.learning.getNextLesson();
        if(upNextLesson.url == undefined)
        {
            return;

        }
        $('.up-next-lesson').html('Bài học kế tiếp<br>' + upNextLesson.title);
        $('.fp-ui').css('background', 'none');
        $('.flowplayer-end-screen').show();
        $('.flowplayer-end-screen svg circle').attr('style', 'animation-play-state: running;');
        $('.fp-ui .fp-control').attr('disabled', true);
        var nextLessonTimedout = setTimeout(function(){
           // e.preventDefault();
            $('.flowplayer-end-screen svg circle').attr('style', 'animation-play-state: paused;');
            window.location = upNextLesson.url;
        }, 5 * 1000);

        $(document).on('click', '.countdown-cancel', function(e) {
            e.preventDefault();
            clearTimeout(nextLessonTimedout);
            $('.flowplayer-end-screen').hide();
            $('.fp-ui').css('background', '');
        });

    },
    init: function (lessonId, userCourseId, courseId) {
        this.lessonId = lessonId;
        this.userCourseId = userCourseId;
        this.courseId = courseId;
    },
    getNextLesson:function () {
        var upNextLesson = {}
        if ($('#thelist .seeing').parent().next().find('a').html() !== undefined) {
            _href =  $('#thelist .seeing').parent().next().find('a').attr('href');
            _raw_params = _href.split('?')[1].split('&');
            $.each(_raw_params, function (index, value) {
                _params = value.split('=');
                if (_params[0] == 'title') {
                    upNextLesson = {
                        title: decodeURIComponent(_params[1]).replace(/\\+/g, ' '),
                        url: _href
                    }
                }
            });
        }
        // next section in same section group
        else if ($('.sub-part.see-ing').next().html()) {
            _href = $('.sub-part.see-ing').next().find('a').attr('href');
            _text = $('.sub-part.see-ing').next().find('a').text();
            upNextLesson = {
                title: _text,
                url: _href
            }
        }
        // first section in next group
        else if ($('.sub-part.see-ing').parent().next().find('li.sub-part').first()) {
            _href = $('.sub-part.see-ing').parent().next().find('li.sub-part').first().find('a').attr('href');
            _text = $('.sub-part.see-ing').parent().next().find('li.sub-part').first().find('a').text();
            upNextLesson = {
                title: _text,
                url: _href
            }
        }
        return upNextLesson;

    }
}
$(function () {
    window.learning.init($('.flowplayer').data('lesson-id'), $('.flowplayer').data('user-course-id'), $('.flowplayer').data('course-id'));
});
