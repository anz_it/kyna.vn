<?php
namespace kyna\videomanager\widgets\videoplayer\assets;

use yii\web\AssetBundle;
use yii\web\JqueryAsset;

class FlowplayerAsset extends AssetBundle
{
    public $sourcePath = __DIR__.'/flowplayer';
    public $js = [
        'flowplayer.min.js',
        'flowplayer.hlsjs.min.js',
        'flowplayer.drive-analytics.min.js',
    ];
    public $css = [
        'skin/functional.css',
    ];
    public $depends = [
        'yii\web\JqueryAsset',
    ];
}
