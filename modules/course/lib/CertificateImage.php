<?php

namespace kyna\course\lib;

use Yii;
use yii\base\Component;

class CertificateImage extends Component
{
    const TEXT_ALIGN_CENTER = 'center';
    const TEXT_ALIGN_LEFT = 'left';

    public $userFont = '@kyna/course/assets/certificate/fonts/YesevaOne-Regular.ttf';
    public $userFontSize = 78;

    public $mainFont = '@kyna/course/assets/certificate/fonts/Roboto-Bold.ttf';
    public $mainFontSize = 25;

    public $image = '@kyna/course/assets/certificate/img/certificate.png';

    public $saveFile;
    public $overwrite = false;

    public function generate($certNumber, $userName, $birthdate, $courseName, $type = null)
    {
        if (!$this->overwrite && file_exists($this->saveFile)) {
            return;
        }

        $image = $this->getBackground();

        $mainFont = Yii::getAlias($this->mainFont);
        $userFont = Yii::getAlias($this->userFont);

        $userColor = imagecolorallocate($image, 251, 106, 0);
        $mainColor = imagecolorallocate($image, 51, 51, 51);
        $labelColor = imagecolorallocate($image, 81, 171, 81);
        $normalColor = imagecolorallocate($image, 127, 127, 127);

        // put cert number
        $this->addTextToImage($image, $this->mainFontSize, 56, 388, $mainColor, $mainFont, $certNumber);
        // put user name
        $this->addTextToImage($image, $this->userFontSize, self::TEXT_ALIGN_CENTER, 684, $userColor, $userFont, $userName);
        $this->addTextToImage($image, $this->mainFontSize, self::TEXT_ALIGN_CENTER, 880, $normalColor, $mainFont, 'Đã hoàn thành khóa học:');
        // put course name
        $this->addTextToImage($image, $this->mainFontSize, self::TEXT_ALIGN_CENTER, 940, $mainColor, $mainFont, $courseName);

        if ($birthdate !== false) {
            /* put birthdate */
            $this->addTextToImage($image, $this->mainFontSize, 555, 800, $labelColor, $mainFont, 'Ngày sinh:');
            $this->addTextToImage($image, $this->mainFontSize, 715, 800, $mainColor, $mainFont, $birthdate);
        }
        if ($type !== null) {
            /* put birthdate */
            $this->addTextToImage($image, $this->mainFontSize, 635, 1030, $labelColor, $mainFont, 'Loại:');
            $this->addTextToImage($image, $this->mainFontSize, 715, 1030, $mainColor, $mainFont, $type);
        }
        //var_dump($this->saveFile);die;
        $this->saveImage($image, $this->saveFile);
    }

    protected function saveImage($image, $savePath)
    {
        /* finish work */
        if (file_exists($savePath)) {
            unlink($savePath);
        }
        imagesavealpha($image, true);
        imagejpeg($image, $savePath, 90);

        imagedestroy($image);
    }

    protected function getBackground()
    {
        //$text = 'The testing message';
        $path = Yii::getAlias($this->image);
        $image = imagecreatefrompng($path);
        $background = imagecolorallocatealpha($image, 255, 255, 255, 127);
        imagefill($image, 0, 0, $background);

        return $image;
    }

    protected function addTextToImage(&$image, $fontSize, $align, $y, $color, $font, $text)
    {
        $bbox = imagettfbbox($fontSize, 0, $font, $text);
        if ($align === self::TEXT_ALIGN_CENTER) {
            $x = (imagesx($image) / 2) - (($bbox[2] - $bbox[0]) / 2) - 8;
        } elseif ($align === self::TEXT_ALIGN_LEFT) {
            $x = 260;
        } else {
            // 260 to make sure all text can't be out of the box
            $x = 260 + $align;
        }
        imagettftext($image, $fontSize, 0, $x, $y, $color, $font, $text);
    }
}
