<?php
namespace kyna\course\lib;

use kyna\servicecaller\traits\CurlTrait;
use Yii;
use kyna\servicecaller\BaseCaller;

class H5p extends BaseCaller
{
    use CurlTrait;

    private $_apiUrl = 'https://h5p.kynaforkids.vn/api';

    protected static $defaultMethod = 'GET';

    protected function Signin()
    {
        // do nothing
    }

    protected function beforeCall(&$ch, &$data)
    {
        $headers = [
            'Content-Type:application/json',
        ];
        $data = json_encode($data);

        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    }

    public function getContent($id)
    {
        $command = '/get_h5p_content/';
        $params = ['id' => $id];
        $endpoint = $this->_buildUrl($command, $params);
        $result = $this->call($endpoint, $params, 'GET');
        $result = json_decode($result, true);
        return ($result['status'] == 'ok') ? $result['data'] : [];
    }

    private function _buildUrl($command, $params = false)
    {
        $url = $this->_apiUrl . $command;
        if ($params) {
            $url .= '?' . http_build_query($params);
        }

        return $url;
    }
}
