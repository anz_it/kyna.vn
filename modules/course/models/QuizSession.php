<?php

namespace kyna\course\models;

use Yii;
use yii\caching\TagDependency;
use common\helpers\ArrayHelper;
use common\helpers\CacheHelper;
use kyna\learning\models\UserCourseLesson;
use kyna\user\models\User;
use kyna\user\models\UserCourse;

/**
 * @duration: calculation with seconds.
 * This is the model class for table "quiz_sessions".
 *
 * @property int $id
 * @property int $user_id
 * @property int $quiz_id
 * @property int $start_time
 * @property int $duration
 * @property int $status
 * @property boolean $is_passed
 * @property float $total_score
 * @property float $total_quiz_score
 */
class QuizSession extends \kyna\base\ActiveRecord
{

    // scenarios
    const SCENARIO_MARK = 'scenario-mark';

    // events
    const EVENT_MARKED = 'event-marked';

    //Init
    const STATUS_DEFAULT = 0;
    //Is submited
    const STATUS_SUBMITTED = 1;
    //Is passed
    const STATUS_PASSED = 2;
    //Is marked
    const STATUS_MARKED = 3;
    //Is failed
    const STATUS_FAILED = -1;
    //Is Doing
    const STATUS_DOING = 5;
    //Is expired
    const STATUS_EXPIRED = 6;
    //Is Started
    const STATUS_PAUSING = 4;
    // Redoing
    const STATUS_REDO = 7;
    // Waiting for marking
    const STATUS_WAITING_FOR_MARK = 8;

    private $_answers = [];
    public $anwsersText;

    public function init()
    {
        $ret = parent::init();

        $this->on(self::EVENT_MARKED, [$this, 'afterMark']);

        return $ret;
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'quiz_sessions';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [[
                'user_id', 'quiz_id', 'start_time', 'duration',
                'status', 'is_passed', 'last_interactive',
                'submit_time', 'time_remaining'
            ], 'integer'],
            [['total_quiz_score', 'total_score'], 'integer', 'integerOnly' => false],
            [['answers'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'Học viên',
            'quiz_id' => 'Bài quiz',
            'start_time' => 'Ngày làm bài',
            'duration' => 'Duration',
            'is_passed' => 'Vượt qua',
            'status' => 'Trạng thái',
            'last_interactive' => 'Lần tương tác gần nhất',
            'submit_time' => 'Thời gian nộp bài',
            'time_remaining' => 'Thời gian còn lại',
            'total_score' => 'Tổng điểm',
        ];
    }

    public function scenarios()
    {
        $scenarios = parent::scenarios(); // TODO: Change the autogenerated stub

        $scenarios[self::SCENARIO_MARK] = [];

        return $scenarios;
    }

    public static function listStatus()
    {
        return [
            self::STATUS_DEFAULT => 'Khởi tạo',
            self::STATUS_PAUSING => 'Tạm dừng',
            self::STATUS_FAILED => 'Rớt',
            self::STATUS_SUBMITTED => 'Đã nộp bài',
            self::STATUS_MARKED => 'Đã chấm điểm',
            self::STATUS_DOING => 'Đang làm',
            self::STATUS_EXPIRED => 'Hết thời gian',
            self::STATUS_PASSED => 'Đậu',
            self::STATUS_REDO => 'Làm lại',
            self::STATUS_WAITING_FOR_MARK => 'Chờ chấm điểm',
        ];
    }

    public function getStatusesText()
    {
        $status = self::getStatuses();

        return isset($status[$this->status]) ? $status[$this->status] : null;
    }

    public function getAnswerModels()
    {
        return $this->hasMany(QuizSessionAnswer::className(), ['quiz_session_id' => 'id']);
    }

    public function getEssayAnswers()
    {
        $answerModelQuery = $this->getAnswerModels();
        $answerModelQuery->joinWith('question');
        $tblQuestion = QuizQuestion::tableName();

        return $answerModelQuery->andWhere([
            "$tblQuestion.type" => QuizQuestion::TYPE_WRITING
        ]);
    }

    public function getUnansweredModels()
    {
        return $this->getAnswerModels()->where(['answer' => null]);
    }

    public function getMaxPosition()
    {
        $answerQuery = $this->getAnswerModels();

        return $answerQuery->max('position');
    }

    public function getIsSubmitted()
    {
        return $this->status === self::STATUS_SUBMITTED;
    }

    public function getIsExpired()
    {
        return $this->start_time > 0 && $this->start_time + $this->duration < time();
    }

    public function getRightAnswers()
    {
        $score = $this->getAnswerModels()->andWhere(['is_correct' => self::BOOL_YES])->sum('is_correct');
        return !empty($score) ? $score : 0;
    }

    public function getQuiz()
    {
        return $this->hasOne(Quiz::className(), ['id' => 'quiz_id']);
    }

    public function getAnswers()
    {
        $callable = function () {
            return $this->answerModels;
        };
        $cacheTags = new TagDependency(['tags' => QuizSessionAnswer::tableName() . '-' . $this->quiz_id . '-' . $this->user_id]);
        $answers = CacheHelper::getQueryCacheByTag($callable, $cacheTags);

        return ArrayHelper::map($answers, 'id', 'answer_id');
    }

    public function setAnswers($value)
    {
        $sessionAnswerIds = array_keys($value);
        $sessionAnswers = QuizSessionAnswer::find()->where(['id' => $sessionAnswerIds])->indexBy('id')->all();

        foreach ($sessionAnswers as $sessionAnswer) {
            $answer = isset($value[$sessionAnswer->id]) ? $value[$sessionAnswer->id] : 0;
            $sessionAnswer->attributes = [
                'answer' => is_array($answer) ? implode(', ', $answer) : $answer,
            ];
        }

        $this->_answers = $sessionAnswers + $this->_answers;
    }

    /**
     * @return bool
     */
    public function calculateScore()
    {
        foreach ($this->_answers as $answer) {
            /**
             * return 1 for now
             * TODO: Need to calculate with coefficient
             */
            $score = $answer->calculateScore();
        }
    }

    public function getTotalQuestion()
    {
        $totalHasParent = \Yii::$app->db->createCommand("select count(*) from (select count(*) from quiz_session_answers where parent_id is not null and quiz_session_id = {$this->id} group by parent_id) a")
            ->queryScalar();
        $total = \Yii::$app->db->createCommand("select count(*) from quiz_session_answers where quiz_session_id = {$this->id}")
            ->queryScalar();
        return $total - $totalHasParent;
    }


    public static function initSession($quizId, $userId)
    {
        $session = self::getIsDone($userId, $quizId);

        if (!empty($session)) {
            return $session;
        }

        $quizSession = static::find()->andWhere([
            'quiz_id' => $quizId,
            'user_id' => $userId,
            'status' => [self::STATUS_DOING, self::STATUS_DEFAULT, self::STATUS_WAITING_FOR_MARK]
        ])->one();

        if ($quizSession == null) {
            $quiz = Quiz::findOne($quizId);

            $quizSession = new QuizSession();
            $quizSession->attributes = [
                'quiz_id' => $quizId,
                'user_id' => $userId,
                'status' => self::STATUS_DEFAULT,
                'start_time' => null,
                'duration' => (int)$quiz->duration,
            ];
            if (!$quizSession->save()) {
                return false;
            }

            $quizSession->initAnswers($quiz);
        } else {
            if ($quizSession->status == self::STATUS_DOING && $quizSession->isExpired) {
                $quizSession->finish();
                $quizSession->save();
            }
        }

        return $quizSession;
    }

    public static function getIsDone($user_id, $quizId)
    {
        return self::find()->where(['user_id' => $user_id, 'status' => self::STATUS_MARKED, 'quiz_id' => $quizId])->one();
    }

    /**
     * @param $quiz Quiz
     */
    public function initAnswers($quiz)
    {
        if ($quiz->is_random_question) {
            $orderBy = new \yii\db\Expression('rand()');
        } else {
            $orderBy = ['t.order' => SORT_ASC];
        }

        $columns = ['quiz_session_id', 'quiz_detail_id', 'quiz_question_id', 'parent_id'];
        $details = $quiz->getDetails()->alias('t')->select(['t.id', 't.quiz_question_id', 't.parent_id'])->joinWith('quizQuestion')->andWhere([QuizQuestion::tableName() . '.status' => QuizQuestion::STATUS_ACTIVE])->orderBy($orderBy)->indexBy('id')->asArray()->all();

        $answers = [];
        foreach ($details as $id => $detail) {
            $answers[] = [$this->id, $id, $detail['quiz_question_id'], $detail['parent_id']];
        }

        QuizSessionAnswer::insertMany($columns, $answers);
    }

    /**
     * @inheritdoc
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * Before save
     * @duration: calculate with seconds.
     */
    public function beforeSave($insert)
    {
        if ($this->isNewRecord) {
            if (!empty($this->duration)) {
                $this->duration = $this->duration * 60;
            } else {
                $this->duration = 3600;
            }
        }

        return parent::beforeSave($insert);
    }

    public function afterSave($insert, $changedAttributes)
    {
        $ret = parent::afterSave($insert, $changedAttributes);

        if (!$this->isNewRecord) {
            if ($this->status == self::STATUS_SUBMITTED || $this->status == self::STATUS_REDO) {
                $beforeTrash = self::find()->where([
                    'user_id' => $this->user_id,
                    'quiz_id' => $this->quiz_id,
                    'status' => [self::STATUS_REDO, self::STATUS_EXPIRED, self::STATUS_WAITING_FOR_MARK, self::STATUS_MARKED]
                ])->all();

                foreach ($beforeTrash as $trash) {
                    $result = $this->moveTrash($trash);
                    if ($result) {
                        $trash->delete();
                    }
                }
            }
        }
        switch ($this->scenario) {
            case self::SCENARIO_MARK:
                $this->trigger(self::EVENT_MARKED);
                break;

            default:
                break;
        }

        if ($this->isNewRecord) {
            // invalid cache by tag for session
            $cacheTags = QuizSessionAnswer::tableName() . '-' . $this->quiz_id . '-' . $this->user_id;
            CacheHelper::invalidCacheByTag($cacheTags);
        }

        return $ret;
    }

    public function moveTrash($object)
    {
        $sessionTrash = new QuizSessionTrash();
        $sessionTrash->id = $object->id;
        $sessionTrash->attributes = $object->attributes;
        $sessionTrash->is_passed = QuizSession::BOOL_NO;

        if ($sessionTrash->save()) {
            foreach ($object->answerModels as $answer) {
                $answerTrash = new QuizSessionAnswerTrash();
                $answerTrash->attributes = $answer->attributes;
                $answerTrash->quiz_session_trash_id = $sessionTrash->id;
                if (!$answerTrash->save()) {
                    var_dump($answerTrash->errors);
                    die;
                }

            }
            return true;
        } else {
            var_dump($sessionTrash->errors);
            die;
        }

        return false;
    }

    public function beforeDelete()
    {
        foreach ($this->answerModels as $answer) {
            $answer->delete();
        }
        return parent::beforeDelete();
    }

    public function calculatePercent()
    {
        return $this->total_quiz_score > 0 ? $this->total_score * 100 / $this->total_quiz_score : 0;
    }

    public function finish()
    {
        $hasEssay = false;

        $this->total_score = 0;
        $this->total_quiz_score = 0;

        foreach ($this->answerModels as $answer) {
            $this->total_score += $answer->score;

            if (!empty($answer->question) && !empty($answer->quizDetail)) {
                $this->total_quiz_score += $answer->question->calculateScore() * $answer->quizDetail->coefficient;
                if ($hasEssay == false && $answer->question->type == QuizQuestion::TYPE_WRITING && !empty($answer->answer)) {
                    $hasEssay = true;
                }
            }
        }

        if ($hasEssay) {
            $this->status = self::STATUS_WAITING_FOR_MARK;
        } else {
            $percent = $this->calculatePercent();

            if ($percent >= $this->quiz->percent_can_pass) {
                $this->is_passed = self::BOOL_YES;
            } else {
                $this->is_passed = self::BOOL_NO;
            }

            $this->status = self::STATUS_MARKED;
            $this->scenario = self::SCENARIO_MARK;
        }

        $this->time_remaining = 0;
        $this->submit_time = time();
    }

    public function getRemainingTime()
    {
        return $this->start_time > 0 ? ($this->start_time + $this->duration - time()) : $this->duration;
    }

    public function getCountOfRedo()
    {
        return QuizSessionTrash::find()->where([
            'quiz_id' => $this->quiz_id,
            'user_id' => $this->user_id
        ])->count();
    }

    public function afterMark()
    {
        $lesson = CourseLesson::find()->where(['quiz_id' => $this->quiz_id])->one();
        if ($lesson != null) {
            $userCourse = UserCourse::getLearning($lesson->course_id, $this->user_id);

            if ($userCourse) {
                /** @var UserCourseLesson $userCourseLesson */
                $userCourseLesson = UserCourseLesson::find()->where(['user_course_id' => $userCourse->id, 'course_lesson_id' => $lesson->id])->one();
                if ($userCourseLesson) {
                    $userCourseLesson->process = $this->calculatePercent();
                    $userCourseLesson->save(false);
                }
                $userCourse->calculateProgress();
                if ($this->is_passed)
                    UserCourseLesson::pass($userCourse->id, $lesson->id);
            }
        }

    }

}
