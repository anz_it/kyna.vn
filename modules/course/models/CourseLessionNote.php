<?php

namespace kyna\course\models;

use Yii;

/**
 * This is the model class for table "course_lession_notes".
 *
 * @property integer $id
 * @property integer $course_lession_id
 * @property integer $user_id
 * @property string $note
 * @property integer $current_run_time
 */
class CourseLessionNote extends \kyna\base\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'course_lesson_notes';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['course_lession_id', 'user_id', 'current_run_time'], 'integer'],
            [['note'], 'string', 'max' => 1023],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'course_lession_id' => 'Course Lession ID',
            'user_id' => 'User ID',
            'note' => 'Note',
            'current_run_time' => 'Current Run Time',
        ];
    }

    public function getLession() {
        return $this->hasOne(CourseLesson::className(), ['id' => 'course_lession_id']);
    }

    public function getPlaybackTime() {
        return gmdate("i:s", $this->current_run_time);
    }
}
