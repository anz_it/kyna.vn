<?php

namespace kyna\course\models;

use Yii;
use kyna\user\models\User;

/**
 * This is the model class for table "{{%teacher_notify}}".
 *
 * @property integer $teacher_id
 * @property integer $user_id
 * @property integer $created_time
 * @property integer $updated_time
 */
class TeacherNotify extends \kyna\base\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%teacher_notify}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['teacher_id', 'user_id'], 'required'],
            [['teacher_id', 'user_id', 'created_time', 'updated_time'], 'integer'],
            [['teacher_id', 'user_id'], 'unique', 'targetAttribute' => ['teacher_id', 'user_id'], 'message' => 'The combination of Teacher ID and User ID has already been taken.'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'teacher_id' => Yii::t('app', 'Teacher ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'created_time' => Yii::t('app', 'Created Time'),
            'updated_time' => Yii::t('app', 'Updated Time'),
        ];
    }

    public function getTeacher()
    {
        return $this->hasOne(Teacher::className(), ['id' => 'teacher_id']);
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
