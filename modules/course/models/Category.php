<?php

namespace kyna\course\models;

use common\helpers\CDNHelper;
use common\widgets\upload\UploadRequiredValidator;
use Yii;
use kyna\course\models\CategoryMeta;
/**
 * This is the model class for table "categories".
 *
 * @property integer $id
 * @property integer $parent_id
 * @property string $name
 * @property string $slug
 * @property integer $level
 * @property string $description
 * @property integer $order
 * @property integer $type
 * @property integer $status
 * @property integer $created_time
 * @property integer $updated_time
 * @property string $title
 * @property string $seo_description
 * @property string $redirect_url
 * @property string $menu_icon
 * @property string $home_icon
 */
class Category extends \kyna\base\ActiveRecord
{
    public $imageSize = [
        'cover' => [
            CDNHelper::IMG_SIZE_ORIGINAL,
            CDNHelper::IMG_SIZE_CATEGORY_HOME_ICON,
            CDNHelper::IMG_SIZE_CATEGORY_MENU_ICON_DESKTOP,
            CDNHelper::IMG_SIZE_CATEGORY_MENU_ICON_MOBILE,
        ],
        'contain' => [],
        'crop' => []
    ];
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'categories';
    }

    protected static function softDelete()
    {
        return TRUE;
    }

    public function enableMeta()
    {
        return [
            'category',
            CategoryMeta::className()
        ];
    }

    protected function metaKeys()
    {
        return [
            'image_url',
            'is_featured',
        ];
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['parent_id', 'level', 'order', 'type', 'status', 'created_time', 'updated_time'], 'integer'],
            [['name', 'slug'], 'required'],
            [['description', 'title', 'related_categories_id'], 'string'],
            [['name', 'slug'], 'string', 'max' => 255],
            ['seo_description', 'validMinMax', 'skipOnEmpty' => true, 'params' => ['min' => 1000, 'max' => 1500]],
            [['redirect_url'], 'url'],
            [['menu_icon', 'home_icon'], UploadRequiredValidator::className(), 'skipOnEmpty' => true],
            [['menu_icon', 'home_icon','images_thumb'], 'image', 'skipOnEmpty' => true, 'maxSize' => '4194304', 'mimeTypes' => 'image/png,image/jpeg,image/svg+xml'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'parent_id' => 'Danh mục cha',
            'name' => 'Tên',
            'slug' => 'Slug',
            'level' => 'Level',
            'description' => 'Mô tả',
            'order' => 'Thứ tự',
            'type' => 'Loại',
            'status' => 'Trạng thái',
            'created_time' => 'Ngày tạo',
            'updated_time' => 'Cập nhật cuối',
            'title' => 'SEO Title',
            'seo_description' => 'SEO Description',
            'redirect_url' => 'Redirect URL',
            'menu_icon' => 'Menu Icon',
            'home_icon' => 'Home Icon',
            'related_categories_id' => 'Khóa học liên quan',
            'images_thumb' => 'Hình đại diện',
        ];
    }

    /**
     * @desc get relation of parent category
     * @return self
     */
    public function getParent()
    {
        return $this->hasOne(self::className(), ['id' => 'parent_id']);
    }

    /**
     * @desc get relation of children categories
     * @return array of self
     */
    public function getChildren()
    {
        return $this->hasMany(self::className(), ['parent_id' => 'id']);
    }

    public function getRelatedIds()
    {
        $ids = [$this->id];

        foreach ($this->children as $child) {
            $ids = array_merge($ids, $child->relatedIds);
        }

        return $ids;
    }

    public function loadDefaultValues($skipIfSet = true)
    {
        $ret = parent::loadDefaultValues($skipIfSet);

        if ($this->hasAttribute('status') && is_null($this->status)) {
            $this->status = self::STATUS_ACTIVE;
        }

        return $ret;
    }

    // TODO: use scope
    public static function findAllActive()
    {
        return self::find()->andWhere(['status' => self::STATUS_ACTIVE])->all();
    }

    public function getCourses()
    {
        return $this->hasMany(Course::className(), ['category_id' => 'id']);
    }

    public function beforeDelete()
    {
        if (!empty($this->courses)) {
            Yii::$app->session->setFlash('error', "Không thể xóa danh mục <b>{$this->name}</b> bởi vì vẫn còn khóa học trong danh mục này.");

            return false;
        }

        return parent::beforeDelete();
    }

    public function validMinMax($attribute, $params)
    {
        $stringValue = html_entity_decode(strip_tags($this->{$attribute}));
        $strLength = mb_strlen($stringValue, 'UTF-8');
        if (isset($params['min']) && $strLength < $params['min']) {
            $this->addError($attribute, "{$this->getAttributeLabel($attribute)} phải ít nhất {$params['min']} kí tự");
        }
        if (isset($params['max']) && $strLength > $params['max']) {
            $this->addError($attribute, "{$this->getAttributeLabel($attribute)} phải nhiều nhất {$params['max']} kí tự");
        }
    }
}