<?php

namespace kyna\course\models;

use kotchuprik\sortable\behaviors\Sortable;
use kyna\user\models\UserCourse;

/**
 * This is the model class for table "course_lessons".
 *
 * @property int $id
 * @property int $course_id
 * @property int $section_id
 * @property string $name
 * @property int $day_can_learn
 * @property string $type
 * @property string $video_link
 * @property string $note
 * @property string $content
 * @property int $quiz_id
 * @property int $order
 * @property int $status
 * @property bool $is_deleted
 * @property int $created_time
 * @property int $updated_time
 * @property int $video_type
 */
class CourseLesson extends \kyna\base\ActiveRecord
{
    const TYPE_VIDEO = 'video';
    const TYPE_CONTENT = 'content';
    const TYPE_QUIZ = 'quiz';

    const VIDEO_WOWZA = 0;
    const VIDEO_UIZA = 1;
    const VIDEO_H5P = 2;

    protected function enableMeta()
    {
        return ['course_lession', 'kyna\course\models\CourseLessionMeta'];
    }

    protected function metaKeys()
    {
        return ['transcript'];
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'course_lessons';
    }

    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['sortable'] = [
            'class' => Sortable::className(),
            'query' => self::find(),
        ];

        return $behaviors;
    }

    protected static function softDelete()
    {
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            // required
            [['course_id', 'section_id', 'name', 'type'], 'required'],
            [
                ['video_link'],
                'required',
                'when' => function ($model) {
                    return $model->type == self::TYPE_VIDEO;
                },
            ],
            [
                ['content'],
                'required',
                'when' => function ($model) {
                    return $model->type == self::TYPE_CONTENT;
                },
            ],
            //
            [['course_id', 'section_id', 'day_can_learn', 'quiz_id', 'order', 'status', 'created_time', 'updated_time','video_type'], 'integer'],
            [['note', 'content'], 'string'],
            [['is_deleted'], 'boolean'],
            [['name'], 'string', 'max' => 255],
            [['score', 'percent_can_pass', 'coefficient'], 'number', 'min' => 1, 'max' => 100],
            [['coefficient', 'percent_can_pass'], 'required']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'course_id' => 'Khóa học',
            'section_id' => 'Section',
            'name' => 'Tên',
            'day_can_learn' => 'Ngày có thể học',
            'type' => 'Loại bài học',
            'video_link' => 'Link Video',
            'note' => 'Ghi chú',
            'content' => 'Nội dung',
            'quiz_id' => 'Quiz',
            'order' => 'Order',
            'status' => 'Trạng thái',
            'is_deleted' => 'Is Deleted',
            'created_time' => 'Ngày tạo',
            'updated_time' => 'Ngày cập nhật',
            'score' => 'Điểm',
            'coefficient' => 'Hệ số',
            'percent_can_pass' => 'Tỉ lệ hoàn thành để vượt qua',
            'video_type' => 'Loại Video'
        ];
    }

    public function getIsQuiz()
    {
        return $this->type === self::TYPE_QUIZ;
    }

    public function getQuiz()
    {
        return $this->hasOne(Quiz::className(), ['id' => 'quiz_id']);
    }

    public function getCourse()
    {
        return $this->hasOne(Course::className(), ['id' => 'course_id']);
    }

    public function getSection()
    {
        return $this->hasOne(CourseSection::className(), ['id' => 'section_id']);
    }

    public static function getTypes()
    {
        return [
            self::TYPE_VIDEO => 'Video',
            self::TYPE_QUIZ => 'Quiz',
            self::TYPE_CONTENT => 'Reading',
        ];
    }

    public function getTypeText()
    {
        $types = self::getTypes();

        return isset($types[$this->type]) ? $types[$this->type] : null;
    }

    public function getVideoLink()
    {
        $filename = basename($this->video_link);
        if (empty($filename)) {
            return false;
        }

        $link = 'https://568e78a36fc88.streamlock.net/vod/' . $filename . '/manifest.f4m';

        return $link;
    }

    /**
     * Get can learn
     */

    public function getCanLearnByDate($user_id = null)
    {
        $result = UserCourse::find()->where(['user_id' => $user_id, 'course_id' => $this->course_id])->one();

        $first_lesson = $this->firstLesson;

        if (!empty($first_lesson->day_can_learn) && $first_lesson->day_can_learn > 0) {
            $lessons = self::find()->where(['course_id' => $this->course->id])->all();

            foreach ($lessons as $lesson) {
                $lesson->day_can_learn = $lesson->day_can_learn - $first_lesson->day_can_learn;
                $lesson->save(false);
            }
        }

        if (!empty($result)) {
            if ($result->compareDate($this->day_can_learn)) {
                return true;
            }
        }
        return false;
    }

    public function getIsQuick($user_id = null)
    {
        $result = UserCourse::find()->where([
            'user_id' => $user_id,
            'course_id' => $this->course_id,
            'is_quick' => self::BOOL_YES
        ])->one();

        if (!empty($result)) {
            return true;
        }
        return false;
    }

    public function getDateCanLearn($user_id = null)
    {
        $result = UserCourse::find()->where(['user_id' => $user_id, 'course_id' => $this->course_id])->one();

        if (!empty($result)) {
            return date("d-m-Y", $result->started_date + ($this->day_can_learn * (60 * 60 * 24)));
        }
        return false;
    }

    public function getFirstLesson()
    {
        $course = $this->course_id;
        $first_lesson = self::find()->where(['course_id' => $course])->orderBy(['order' => SORT_ASC])->one();
        return $first_lesson;
    }

    public function getIsFirstLesson()
    {
        if ($this->id == $this->firstLesson->id) {
            return true;
        }
        return false;
    }

    public function processVideoLink()
    {
        return $this->video_link;
      /*  if (strpos($this->video_link, "http://") !== false || strpos($this->video_link, "https://") !== false)
            return $this->video_link;
        $name = basename($this->video_link);
        return implode("/", str_split($this->course_id)) . "/" . $name; */
    }

    public static function listVideoType()
    {
        // TODO: get from `settings` table
        return [
            0 => 'Wowza Video Link',
            //1 => 'Uiza',
            2 => 'H5P ID'
        ];
    }

    public function getVideoTypeTexxt()
    {
        $videoTypes = self::listVideoType();
        return isset($videoTypes[$this->video_type]) ? $videoTypes[$this->video_type] : null;
    }
}
