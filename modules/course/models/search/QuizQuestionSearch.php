<?php

namespace kyna\course\models\search;

use kyna\course\models\Quiz;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use kyna\course\models\QuizQuestion;
use kyna\course\models\QuizDetail;

/**
 * QuizQuestionSearch represents the model behind the search form about `kyna\course\models\QuizQuestion`.
 */
class QuizQuestionSearch extends QuizQuestion
{
    public $type_all;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'course_id', 'status', 'created_time', 'updated_time', 'type'], 'integer'],
            [['is_deleted'], 'boolean'],
            ['content', 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = QuizQuestion::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'course_id' => $this->course_id,
            'status' => $this->status,
            'type' => $this->type,
            'is_deleted' => $this->is_deleted,
            'created_time' => $this->created_time,
            'updated_time' => $this->updated_time,
        ]);

        $query->andFilterWhere(['LIKE', 'content', $this->content]);
        
        if (Yii::$app->controller->id === 'quiz') {
            $query->andWhere('id NOT IN (SELECT quiz_question_id FROM ' . QuizDetail::tableName() . ' cqq WHERE cqq.quiz_id = :quiz_id)', [':quiz_id' => $params['id']]);
        }
        
        return $dataProvider;
    }
}
