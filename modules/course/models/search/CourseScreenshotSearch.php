<?php

namespace kyna\course\models\search;

use kyna\course\models\CourseOpinions;
use kyna\course\models\CourseScreenshot;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use kyna\course\models\Category;

/**
 * CategorySearch represents the model behind the search form about `kyna\course\models\Category;`.
 */
class CourseScreenshotSearch extends CourseScreenshot
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'course_id', 'order','status'], 'integer'],
            [[ 'description'], 'safe'],
            [[ 'description'], 'trim'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CourseScreenshot::find();


        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder'=>['order'=>SORT_ASC]]
        ]);

        if (!empty($params['id'])) {
            $query->andWhere(['course_id' => $params['id']]);
        }

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'course_id' => $this->course_id,
            'order' => $this->order,
            'status'=>$this->status
        ]);

        $query->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'image_url', $this->image_url]);

        return $dataProvider;
    }
}
