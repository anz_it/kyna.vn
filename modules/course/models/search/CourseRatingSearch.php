<?php

namespace kyna\course\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use kyna\course\models\CourseRating;

/**
 * CourseRatingSearch represents the model behind the search form about `kyna\course\models\CourseRating`.
 */
class CourseRatingSearch extends CourseRating
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'course_id', 'user_id', 'is_cheat', 'status', 'created_time', 'updated_time'], 'integer'],
            [['score', 'score_of_content', 'score_of_video', 'score_of_teacher'], 'number'],
            [['review_content', 'cheat_name'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CourseRating::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'created_time' => SORT_DESC
                ]
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'course_id' => $this->course_id,
            'user_id' => $this->user_id,
            'score' => $this->score,
            'score_of_content' => $this->score_of_content,
            'score_of_video' => $this->score_of_video,
            'score_of_teacher' => $this->score_of_teacher,
            'is_cheat' => $this->is_cheat,
            'status' => $this->status,
            'created_time' => $this->created_time,
            'updated_time' => $this->updated_time,
        ]);

        $query->andFilterWhere(['like', 'review_content', $this->review_content])
            ->andFilterWhere(['like', 'cheat_name', $this->cheat_name]);

        return $dataProvider;
    }
}
