<?php

namespace kyna\course\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use kyna\course_combo\models\CourseCombo as CourseComboModel;

/**
 * CourseCombo represents the model behind the search form about `kyna\course_combo\models\CourseCombo`.
 */
class CourseComboSearch extends CourseComboModel
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'teacher_id', 'position', 'status', 'created_time', 'updated_time','is_4kid'], 'integer'],
            [['name', 'short_name', 'image_url', 'object', 'benefit'], 'safe'],
            [['is_deleted'], 'boolean'],
            [['name', 'id'], 'trim'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CourseComboModel::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => Yii::$app->params['pageSize'],
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'status' => $this->status,
            'teacher_id' => $this->teacher_id,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'short_name', $this->short_name])
            ->andFilterWhere(['like', 'image_url', $this->image_url])
            ->andFilterWhere(['=','is_4kid', $this->is_4kid]);
        
        $query->orderBy('created_time DESC');

        return $dataProvider;
    }
}
