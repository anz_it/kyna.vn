<?php

namespace kyna\course\models\search;

use kyna\user\models\UserFollowTeacher;
use kyna\user\models\UserMeta;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use kyna\course\models\Teacher;
use kyna\user\models\User;
class TeacherSearch extends \kyna\user\models\search\UserSearch
{
    public $name;
    public $role;
    public $role_relation;
    public $slug;
    public $relation;

    public function rules()
    {
        return [
            [['id', 'confirmed_at', 'blocked_at', 'created_at', 'updated_at', 'flags', 'fb_id'], 'integer'],
            [['username', 'email', 'password_hash', 'auth_key', 'unconfirmed_email', 'registration_ip', 'name', 'role', 'slug','role_relation'], 'safe'],
            [['username', 'id', 'email', 'name', 'registration_ip','relation'], 'trim'],
        ];
    }

    public function scenarios()
    {
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = Teacher::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => ['created_at' => SORT_DESC]
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'confirmed_at' => $this->confirmed_at,
            'blocked_at' => $this->blocked_at,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'flags' => $this->flags,
        ]);

        $query->andFilterWhere(['like', 'username', $this->username])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'password_hash', $this->password_hash])
            ->andFilterWhere(['like', 'auth_key', $this->auth_key])
            ->andFilterWhere(['like', 'unconfirmed_email', $this->unconfirmed_email])
            ->andFilterWhere(['like', 'registration_ip', $this->registration_ip]);

        $query->joinWith('profile');
        $query->andFilterWhere(['like', 'profile.name', $this->name]);

        if (!empty($this->fb_id)) {
            $query->andWhere(['is not', 'profile.facebook_id', null]);
        }

        if (!empty($this->role)) {
            $query->joinWith('authAssignments');
            $query->andFilterWhere(['auth_assignment.item_name' => $this->role]);
        }
        if (!empty($this->role_relation)) {
            $query->joinWith('authAssignments');
            $query->andFilterWhere(['auth_assignment.item_name' => $this->role_relation]);
        }

        if (!empty($this->slug)) {
            $query->joinWith('userMeta');
            $query->andWhere(["user_meta.key" => 'slug']);
            $query->andFilterWhere(['like', "user_meta.value", $this->slug]);
        }
        if (!empty($this->relation)) {
            $user = User::findOne(['email'=>$this->relation]);
            $userFollowTeacher = UserFollowTeacher::findAll(['user_follow'=>$user->getId()]);
            $ids = [];
            foreach ($userFollowTeacher as $value){
                $ids[] = $value->getUserTeacher();
            }
            $userTable = User::tableName();
            $query->andWhere(['IN', "$userTable.id", $ids]);
        }
        return $dataProvider;
    }
}

