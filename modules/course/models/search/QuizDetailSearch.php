<?php

namespace kyna\course\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use kyna\course\models\QuizDetail;
use kyna\course\models\QuizQuestion;

/**
 * CourseQuestionSearch represents the model behind the search form about `kyna\course\models\CourseQuestion`.
 */
class QuizDetailSearch extends QuizDetail
{
    
    public $questionContent;
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'quiz_id', 'quiz_question_id', 'status', 'created_time', 'updated_time'], 'integer'],
            [['is_deleted'], 'boolean'],
            ['questionContent', 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = QuizDetail::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'order' => SORT_ASC
                ]
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'quiz_id' => $this->quiz_id,
            'quiz_question_id' => $this->quiz_question_id,
            'status' => $this->status,
            'is_deleted' => $this->is_deleted,
            'created_time' => $this->created_time,
            'updated_time' => $this->updated_time,
        ]);
        
        $query->joinWith('quizQuestion');
        $query->andWhere([QuizQuestion::tableName() . '.is_deleted' => QuizQuestion::BOOL_NO]);
        
        if (!empty($this->questionContent)) {
            $query->andFilterWhere(['like', QuizQuestion::tableName() . '.content', $this->questionContent]);
        }
        
        return $dataProvider;
    }
}
