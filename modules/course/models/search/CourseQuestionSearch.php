<?php

namespace kyna\course\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use kyna\course\models\QuizQuestion;

/**
 * CourseQuestionSearch represents the model behind the search form about `kyna\course\models\CourseQuestion`.
 */
class CourseQuestionSearch extends QuizQuestion
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'course_id', 'type', 'status', 'created_time', 'updated_time'], 'integer'],
            [['content', 'image_url'], 'safe'],
            [['is_deleted'], 'boolean'],
            [['id', 'content'], 'trim'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = QuizQuestion::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'created_time' => SORT_DESC
                ]
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'course_id' => $this->course_id,
            'type' => $this->type,
            'status' => $this->status,
            'is_deleted' => $this->is_deleted,
            'created_time' => $this->created_time,
            'updated_time' => $this->updated_time,
        ]);

        $query->andFilterWhere(['like', 'content', $this->content])
            ->andFilterWhere(['like', 'image_url', $this->image_url]);
        
        if (!empty($params['id']) && Yii::$app->controller->id === 'quiz') {
            $query->andWhere('id NOT IN (SELECT course_question_id FROM course_quiz_questions cqq WHERE cqq.quiz_id = :quiz_id)', [':quiz_id' => $params['id']]);
        }

        return $dataProvider;
    }
}
