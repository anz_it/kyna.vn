<?php

namespace kyna\course\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use kyna\course\models\CourseCommissionType;

/**
 * CourseCommissionSearch represents the model behind the search form about `kyna\course\models\CourseCommissionType`.
 */
class CourseCommissionSearch extends CourseCommissionType
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'status', 'created_time', 'updated_time'], 'integer'],
            [['name'], 'safe'],
            [['start_percent', 'end_percent', 'default_percent'], 'number'],
            [['is_required_expiration_date', 'is_multiple_commissions'], 'boolean'],
            [['name', 'id'], 'trim'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CourseCommissionType::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'start_percent' => $this->start_percent,
            'end_percent' => $this->end_percent,
            'is_required_expiration_date' => $this->is_required_expiration_date,
            'is_multiple_commissions' => $this->is_multiple_commissions,
            'status' => $this->status,
            'created_time' => $this->created_time,
            'updated_time' => $this->updated_time,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name]);

        return $dataProvider;
    }
}
