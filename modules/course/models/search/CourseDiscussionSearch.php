<?php

namespace kyna\course\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use kyna\course\models\CourseDiscussion;

/**
 * CourseDiscussionSearch represents the model behind the search form about `kyna\course\models\CourseDiscussion`.
 */
class CourseDiscussionSearch extends CourseDiscussion
{
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'course_id', 'user_id', 'parent_id', 'status', 'created_time', 'updated_time'], 'integer'],
            [['comment'], 'safe'],
            [['comment', 'id'], 'trim'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CourseDiscussion::find();
        
        $tableName = CourseDiscussion::tableName();
        
        $query->select("$tableName.*, IF(parent_id = 0, ($tableName.id * 10 + 1 + (1 - $tableName.id / 1000000000)), (parent_id * 10 + 0 + (1 - $tableName.id / 1000000000))) as sort");

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'attributes' => ['sort'],
                'defaultOrder' => [
                    'sort' => SORT_DESC
                ]
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            $tableName . '.id' => $this->id,
            $tableName . '.course_id' => $this->course_id,
            'user_id' => $this->user_id,
            'parent_id' => $this->parent_id,
            $tableName . '.status' => $this->status,
            $tableName . '.created_time' => $this->created_time,
            $tableName . '.updated_time' => $this->updated_time,
        ]);

        $query->andFilterWhere(['like', 'comment', $this->comment]);
        
        return $dataProvider;
    }
}
