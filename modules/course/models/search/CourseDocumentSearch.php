<?php
namespace kyna\course\models\search;

use kyna\user\models\UserCourse;
use Yii;
use kyna\course\models\CourseDocument;
use yii\base\Model;
use yii\data\ActiveDataProvider;

class CourseDocumentSearch extends CourseDocument{

    public $user_id;
    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search()
    {
        $query = CourseDocument::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $query->alias('t');

        $query->join('INNER JOIN', ['c' => UserCourse::tableName()], "c.course_id = t.course_id");

        $query->andWhere([
            'c.user_id' => $this->user_id,
        ]);

        $query->groupBy('t.course_id');

        return $dataProvider;

    }
}