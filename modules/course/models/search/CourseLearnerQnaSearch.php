<?php

namespace kyna\course\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use kyna\course\models\CourseLearnerQna as CourseLearnerQnaModel;

/**
 * CourseLearnerQna represents the model behind the search form about `kyna\course\models\CourseLearnerQna`.
 */
class CourseLearnerQnaSearch extends CourseLearnerQnaModel
{

    public $date_range;
    public $teacherId;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'course_id', 'user_id', 'question_id', 'posted_time', 'is_approved', 'status', 'teacherId'], 'integer'],
            [['content'], 'safe'],
            [['content', 'id'], 'trim'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CourseLearnerQnaModel::find()
            ->alias('qa');

        $query->joinWith('course courses');

        $query->select("qa.*, IF(qa.question_id is null, (qa.id * 10 + 1 + (1 - qa.id / 1000000000)), (qa.question_id * 10 + 0 + (1 - qa.id / 1000000000))) as sort");

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'attributes' => ['sort'],
                'defaultOrder' => [
                    'sort' => SORT_DESC
                ]
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        if (!empty($params['CourseLearnerQnaSearch']['date_range'])) {
            $dateRange = explode(' - ', $params['CourseLearnerQnaSearch']['date_range']);
            if (is_array($dateRange)) {
                $beginDate = date_create_from_format('d/m/Y H:i:s', $dateRange[0] . ' 00:00:00');
                $endDate = date_create_from_format('d/m/Y H:i:s', $dateRange[1] . ' 23:59:59');
                if ($beginDate && $endDate) {
                    $postedTimeCondition = "posted_time BETWEEN '". $beginDate->getTimestamp() . "' AND '" . $endDate->getTimestamp() . "'";
                    $query->andWhere($postedTimeCondition);
                }
                $this->date_range = $params['CourseLearnerQnaSearch']['date_range'];
            }
        }

        $query->andFilterWhere([
            'qa.id' => $this->id,
            'qa.course_id' => $this->course_id,
            'user_id' => $this->user_id,
            'posted_time' => $this->posted_time,
            'qa.is_approved' => $this->is_approved,
            'qa.status' => $this->status,
            'courses.teacher_id' => $this->teacherId,
        ]);

        if (isset($params['course_status'])) {
            $query->andFilterWhere([
                'courses.status' => self::STATUS_ACTIVE,
                'courses.is_deleted' => self::BOOL_NO
            ]);
        }

        if (isset($params['only_question'])) {
            $query->joinWith('question questions');
            $query->andWhere("ISNULL(questions.id) or questions.is_approved = 1");
        }

        $query->andFilterWhere(['like', 'content', $this->content]);

        return $dataProvider;
    }

    public function teacherSearch($params)
    {
        $query = self::find()->alias('qa');

        if(Yii::$app->user->can('Teacher')){
            $query->joinWith('course courses');
            $query->andWhere([
                'courses.teacher_id' => $this->teacherId
            ]);
        } 
        $query->select("qa.*, IF(question_id is null, (qa.id * 10 + 1 + (1 - qa.id / 1000000000)), (question_id * 10 + 0 + (1 - qa.id / 1000000000))) as sort");

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'attributes' => ['sort'],
                'defaultOrder' => [
                    'sort' => SORT_DESC
                ]
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'qa.id' => $this->id,
            'qa.course_id' => $this->course_id,
            'user_id' => $this->user_id,
            'question_id' => $this->question_id,
            'qa.status' => $this->status,
            'qa.posted_time' => $this->posted_time,
            'qa.updated_time' => $this->updated_time,
        ]);

        $query->andFilterWhere(['like', 'content', $this->content]);

        $query->andWhere(['qa.is_approved' => self::BOOL_YES]);

        return $dataProvider;

    }
}
