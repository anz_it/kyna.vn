<?php

namespace kyna\course\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use kyna\course\models\Course;
use kyna\course\models\Category;

/**
 * CourseSearch represents the model behind the search form about `app\models\Course`.
 */
class CourseSearch extends Course
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'level', 'price', 'price_discount', 'category_id', 'course_commission_type_id', 'teacher_id', 'status', 'created_time', 'updated_time', 'type'], 'integer'],
            [['name', 'short_name', 'slug', 'image_url', 'video_url', 'video_cover_image_url'], 'safe'],
            [['percent_discount', 'total_time'], 'number'],
            [['slug', 'name', 'id'], 'trim'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Course::find()->with(['category', 'teacher']);
        $query->andWhere([Course::tableName() . '.type' => [self::TYPE_VIDEO, self::TYPE_SOFTWARE]]);
        
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if (isset($params['filter_price']) && !empty($params['filter_price'])) {
            $query->andWhere(Course::tableName() . '.price ' . $params['filter_price']);
        }

        if (isset($params['filter_price_not_free']) && !empty($params['filter_price_not_free'])) {
            $query->andWhere(Course::tableName() . '.price ' . $params['filter_price_not_free']);
        }


        $query->andFilterWhere([
            Course::tableName() . '.id' => $this->id,
            'level' => $this->level,
            'price' => $this->price,
            'type' => $this->type,
            'price_discount' => $this->price_discount,
            'percent_discount' => $this->percent_discount,
            'total_time' => $this->total_time,
            'category_id' => $this->category_id,
            'course_commission_type_id' => $this->course_commission_type_id,
            'teacher_id' => $this->teacher_id,
            Course::tableName() . '.status' => $this->status,
            Course::tableName() . '.created_time' => $this->created_time,
            Course::tableName() . '.updated_time' => $this->updated_time,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'short_name', $this->short_name])
            ->andFilterWhere(['like', 'slug', $this->slug])
            ->andFilterWhere(['like', 'image_url', $this->image_url])
            ->andFilterWhere(['like', 'video_url', $this->video_url])
            ->andFilterWhere(['like', 'video_cover_image_url', $this->video_cover_image_url]);
        
        $query->orderBy('created_time DESC');
        
        return $dataProvider;
    }
}
