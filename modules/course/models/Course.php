<?php

namespace kyna\course\models;

use common\recommendation\Recommendation;
use kyna\base\models\Campaign11;
use kyna\tag\models\CourseTagLog;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

use common\elastic\Course as ElasticCourse;
use common\helpers\StringHelper;
use common\widgets\upload\UploadRequiredValidator;
use common\helpers\CDNHelper;

use kyna\base\models\MetaField;
use kyna\base\traits\MetaTrait;
use kyna\settings\models\SeoSetting;
use kyna\learning\models\UserCourseLesson;
use kyna\tag\models\CourseTag;
use kyna\tag\models\Tag;
use kyna\order\models\Order;
use kyna\order\models\OrderDetails;
use kyna\user\models\UserCourse;
use kyna\promo\models\PromotionSchedule;
use kyna\course_combo\models\CourseComboItem;
use kyna\course_combo\models\CourseCombo;
use kyna\partner\models\Partner;
use kyna\gamification\models\Mission;
use kyna\gamification\models\MissionCategory;
use kyna\gamification\models\MissionCourse;
use kyna\gamification\models\UserPointHistory;
use kyna\gamification\models\MissionCondition;
use kyna\promo\models\GroupDiscount;
use kyna\promo\models\GroupDiscountCourse;

/**
 * This is the model class for table "courses".
 *
 * @property integer $id
 * @property string $name
 * @property string $short_name
 * @property integer $level
 * @property integer $price
 * @property integer $price_discount
 * @property double $percent_discount
 * @property string $slug
 * @property string $image_url
 * @property string $video_url
 * @property string $video_cover_image_url
 * @property double $total_time
 * @property integer $category_id
 * @property integer $course_commission_type_id
 * @property integer $teacher_id
 * @property integer $status
 * @property integer $created_time
 * @property integer $updated_time
 * @property Teacher $teacher
 * @property float $percent_can_pass
 * @property float $percent_can_be_excellent
 * @property integer $is_hot
 * @property integer $is_new
 * @property integer $partner_id
 * @property integer $is_disable_seeding
 * @property float $rating
 * @property integer $rating_users_count
 * @property integer $total_users_count
 * @property string $redirect_url
 * @property integer $type
 * @property integer $purchase_type
 * @property string $what_you_learn
 * @property string $mac_app_link
 * @property string $window_app_link
 * @property string $ios_app_link
 * @property string $android_app_link
 * @property integer $max_device
 * @property float $affiliate_commission_percent
 * @property integer $is_4kid
 */
class Course extends \kyna\base\ActiveRecord
{
    use MetaTrait;

    const TYPE_VIDEO = 1;
    const TYPE_COMBO = 2;
    const TYPE_SOFTWARE = 3;

    const SCENARIO_UPDATE = 'update';
    const SCENARIO_UPDATE_RATING = 'update-rating';
    public $total_time_number;

    public $tags_list = [];
    public $old_tag_ids = [];

    private $_cache = [];

    public $imageSize = [
        'cover' => [
            CDNHelper::IMG_SIZE_THUMBNAIL,
            CDNHelper::IMG_SIZE_THUMBNAIL_SMALL
        ],
        'contain' => [],
        'crop' => [
            CDNHelper::IMG_SIZE_THUMBNAIL_YOUTUBE,
            CDNHelper::IMG_SIZE_THUMBNAIL_YOUTUBE_LARGE
        ]
    ];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'courses';
    }

    public function init()
    {
        parent::init();

        //$this->on(self::EVENT_AFTER_FIND, [$this, 'getCourseTags']);
        $this->on(self::EVENT_AFTER_INSERT, [$this, 'updateCourseTags']);
        $this->on(self::EVENT_AFTER_UPDATE, [$this, 'updateCourseTags']);

        return;
    }

    protected static function softDelete()
    {
        return true;
    }

    public function enableMeta()
    {
        return MetaField::MODEL_COURSE;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'type', 'slug', 'teacher_id', 'level', 'category_id', 'course_commission_type_id', 'price', 'percent_can_pass', 'purchase_type', 'what_you_learn'], 'required'],
            [['level', 'category_id', 'course_commission_type_id', 'type', 'teacher_id', 'status', 'created_time', 'updated_time', 'is_feature', 'is_disabled_qna', 'partner_id', 'max_device', 'is_4kid'], 'integer'],
            [['percent_discount', 'total_time'], 'number'],
            [['is_hot', 'is_new', 'is_disable_seeding', 'rating_users_count'], 'integer', 'integerOnly' => true],
            [['rating'], 'integer', 'integerOnly' => false, 'min' => 0, 'max' => 5],
            [['percent_can_pass', 'percent_can_be_excellent'], 'number', 'min' => 1, 'max' => 100],
            [['price', 'price_discount'], 'integer', 'min' => 0, 'max' => 100000000],
            [['total_time_number'], 'integer', 'min' => 0, 'max' => 100000000],
            [['slug', 'video_url', 'description', 'keyword'], 'string'],
            [['image_url'], UploadRequiredValidator::className(), 'skipOnEmpty' => true],
            [['image_url', 'video_cover_image_url'], 'image', 'skipOnEmpty' => true, 'maxSize' => '4194304', 'mimeTypes' => 'image/png,image/jpeg'], // 4MiB
            [['name'], 'string', 'max' => 255],
            ['slug', 'unique'],
            [['short_name'], 'string', 'max' => 45],
            [['overview'], 'filter', 'filter' => [StringHelper::class, 'htmlContentFilterNofollow'],
                'when' => function ($model) {
                    return $model->type != self::TYPE_VIDEO;
                }],
            ['custom_content', 'string'],
            'validatePercent' => ['percent_discount', 'double', 'min' => 0, 'max' => 100],
            'validatePercent' => ['affiliate_commission_percent', 'double', 'min' => 0, 'max' => 100],
            'validatePriceDiscount' => [
                'price_discount',
                'compare',
                'type' => 'number',
                'compareAttribute' => 'price',
                'operator' => '<',
                'whenClient' => "function (attribute, value) {
                    return $('#course-price').val() < $('#course-price_discount').val();
                }",
                'when' => function ($model) {
                    return $model->price > 0;
                },
                'message' => 'Học phí giảm phải nhỏ hơn học phí gốc'],
            [$this->metaKeys(), 'safe'],
            [['tags_list'], 'safe'],
            [['redirect_url', 'ios_app_link', 'mac_app_link', 'window_app_link', 'android_app_link'], 'url'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Tên',
            'short_name' => 'Tên ngắn',
            'level' => 'Mức độ',
            'price' => 'Học phí gốc',
            'price_discount' => 'Học phí giảm giá',
            'percent_discount' => 'Phần trăm giảm giá',
            'slug' => 'Slug',
            'image_url' => 'Ảnh đại diện',
            'video_url' => 'Link video',
            'video_cover_image_url' => 'Link video cover image',
            'total_time' => 'Tổng thời lượng',
            'category_id' => 'Danh mục',
            'course_commission_type_id' => 'Loại hoa hồng',
            'teacher_id' => 'Giảng viên',
            'status' => 'Trạng thái',
            'type' => 'Loại khoá học',
            'created_time' => 'Ngày tạo',
            'updated_time' => 'Cập nhật cuối',
            'total_time_number' => 'Tổng thời lượng',
            'percent_can_be_excellent' => 'Điểm để đạt chứng chỉ loại Xuất sắc',
            'tags_list' => 'Danh sách Tags',
            'is_hot' => 'Hot',
            'is_new' => 'New',
            'partner_id' => 'Partner',
            'is_disable_seeding' => 'Không cheat đánh giá',
            'rating' => 'Điểm đánh giá',
            'rating_users_count' => 'Tổng số người đánh giá',
            'redirect_url' => 'Redirect URL',
            'purchase_type' => 'Thời gian sở hữu',
            'what_you_learn' => 'Bạn sẽ học được gì',
            'window_app_link' => 'Link ứng dụng Window',
            'mac_app_link' => 'Link ứng dụng Mac',
            'ios_app_link' => 'Link ứng dụng iOS',
            'android_app_link' => 'Link ứng dụng Android',
            'max_device' => 'Số thiết bị tối đa',
            'affiliate_commission_percent'=>'% Phí Affiliate',
            'is_4kid' => 'Khóa học Forkid'
        ];
    }

    public function beforeSave($insert)
    {
        $ret = parent::beforeSave($insert);
        $this->old_tag_ids = $this->getTagIds();
        $oldAttributes = $this->oldAttributes;
        if (isset($oldAttributes['is_disable_seeding']) && $oldAttributes['is_disable_seeding'] != $this->is_disable_seeding) {
            $this->attributes = self::calculateRating($this->id, $this->is_disable_seeding, true);
        }

        return $ret;
    }

    /**
     * @param $courseId
     * @return array|false
     */
    public static function calculateRating($courseId, $newSetting = null, $return = false)
    {
        $course = self::findOne($courseId);
        $courseRatingTblName = CourseRating::tableName();

        $sql = "select count(*) as totalUser, sum(score) as totalScore
          from $courseRatingTblName
          where course_id = $courseId and status = " . CourseRating::STATUS_ACTIVE;

        if (($newSetting != null && $newSetting) || ($newSetting == null && $course->is_disable_seeding)) {
            $sql .= ' AND is_cheat = ' . CourseRating::BOOL_NO;
        }

        $result = Yii::$app->db->createCommand($sql)->queryOne();

        $newAttributes = [
            'rating' => $result['totalUser'] > 0 ? round($result['totalScore'] / $result['totalUser'], 1) : 0,
            'rating_users_count' => $result['totalUser']
        ];
        if ($return) {
            return $newAttributes;
        } else {
            $course->rating = $newAttributes['rating'];
            $course->rating_users_count = $newAttributes['rating_users_count'];
            $course->setScenario(self::SCENARIO_UPDATE_RATING);
            return $course->save(false);
        }
    }

    public function getTags()
    {
        $tags = $this->getTagsObjects();
        return ArrayHelper::getColumn($tags, ['tag']);
    }

    public function updateCourseTags()
    {
        if($this->getScenario() != self::SCENARIO_UPDATE_RATING) {
            CourseTag::assignCourseTag($this->tags_list, $this->id, true);
        }
    }

    public function getTagsObjects()
    {
        if (isset($this->_cache['tagsObjects']))
            return $this->_cache['tagsObjects'];
        $this->_cache['tagsObjects'] = Tag::find()
            ->join('INNER JOIN', CourseTag::tableName(), CourseTag::tableName() . '.tag_id = ' . Tag::tableName() . '.id')
            ->join('INNER JOIN', Course::tableName(), Course::tableName() . '.id = ' . CourseTag::tableName() . '.course_id')
            ->where([
                Course::tableName() . '.id' => $this->id,
                Tag::tableName() . '.status' => Tag::STATUS_ACTIVE
            ])
            ->all();

        return $this->_cache['tagsObjects'];

    }
    public function getTagIds(){
        $tags = Tag::find()
            ->join('INNER JOIN', CourseTag::tableName(), CourseTag::tableName() . '.tag_id = ' . Tag::tableName() . '.id')
            ->join('INNER JOIN', Course::tableName(), Course::tableName() . '.id = ' . CourseTag::tableName() . '.course_id')
            ->where([
                Course::tableName() . '.id' => $this->id,
                Tag::tableName() . '.status' => Tag::STATUS_ACTIVE
            ])
            ->all();
        return ArrayHelper::getColumn($tags, ['id']);
    }

    public static function totalTimeUnits()
    {
        return [
            'hours' => 'giờ',
            'minutes' => 'phút',
            'days' => 'ngày',
            'weeks' => 'tuần',
            'months' => 'tháng'
        ];
    }

    /**
     * @desc get teacher model relation
     * @return BETeacher instance
     */
    public function getTeacher()
    {
        return $this->hasOne(Teacher::className(), ['id' => 'teacher_id']);
    }

    /**
     * @desc get category model relation
     * @return Category instance
     */
    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'category_id']);
    }

    public function getPromotionSchedules()
    {
        return $this->hasMany(PromotionSchedule::className(), ['course_id' => 'id']);
    }

    public function getAvailiablePromotionSchedules()
    {
        $now = time();
        $promotionScheduleQuery = $this->getPromotionSchedules();

        return $promotionScheduleQuery
            ->andWhere(['status' => PromotionSchedule::STATUS_ACTIVE])
            ->andWhere(['<=', 'start_time', $now])
            ->andWhere(['>=', 'end_time', $now])
            ->orderBy('created_time DESC')
            ->limit(1)
            ->one();
    }

    public static function listLevel()
    {
        // TODO: get from `settings` table
        return [
            1 => 'Mới bắt đầu',
            2 => 'Chuyên sâu',
            3 => 'Chuyên nghiệp',
        ];
    }


    public static function listPurchaseType()
    {
        // TODO: get from `settings` table
        return [
            -1 => 'Sở hữu mãi mãi',
            1 => 'Sở hữu 1 tháng',
            3 => 'Sở hữu 3 tháng',
            6 => 'Sở hữu 6 tháng',
            12 => 'Sở hữu 1 năm',
            24 => 'Sở hữu 2 năm',
            36 => 'Sở hữu 3 năm',
        ];
    }

    public function getTypeText()
    {
        $listType = self::listTypes();
        return isset($listType[$this->type]) ? $listType[$this->type] : null;
    }


    public function getLevelText()
    {
        $listLevels = self::listLevel();

        return isset($listLevels[$this->level]) ? $listLevels[$this->level] : null;
    }

    public function getPurchaseTypeText()
    {
        $purchaseTypes = self::listPurchaseType();
        return isset($purchaseTypes[$this->purchase_type]) ? $purchaseTypes[$this->purchase_type] : null;
    }

    public function getPurchaseTypeTextFrontend()
    {
        $purchaseTypes = [
            -1 => null,
            1 => '1 tháng',
            3 => '3 tháng',
            6 => '6 tháng',
            12 => '1 năm',
            24 => '2 năm',
            36 => '3 năm',
        ];
        return isset($purchaseTypes[$this->purchase_type]) ? $purchaseTypes[$this->purchase_type] : null;
    }

    /**
     * @desc get embed youtube url
     * @return string|null
     */
    public function getYoutubeEmbedUrl()
    {
        if (empty($this->video_url)) {
            return null;
        }

        if (strpos($this->video_url, '/embed/') !== false) {
            return $this->video_url;
        }

        $queryStr = parse_url($this->video_url, PHP_URL_QUERY);
        if (!empty($queryStr)) {
            parse_str($queryStr, $arr);
            if (!empty($arr['v'])) {
                return "https://www.youtube.com/embed/{$arr['v']}";
            }
        }

        $path = parse_url($this->video_url, PHP_URL_PATH);
        if (!empty($path)) {
            $path = trim($path, '/');
            return "https://www.youtube.com/embed/{$path}";
        }

        return null;
    }

    public function getTotalTime()
    {
        if ($this->type == Course::TYPE_COMBO) {
            $total = 0;
            foreach ($this->comboItems as $comboItem) {
                if ($comboItem->course->status == Course::STATUS_ACTIVE)
                    $total += $comboItem->course->total_time;
            }
            return $total;
        } else {
            return $this->total_time;
        }
    }

    public function getUrl($scheme = false)
    {
        return Url::to(['/course/default/view', 'id' => $this->id, 'slug' => $this->slug],$scheme);
    }

    public function getLearningUrl($sectionId = null, $lessonId = null, $scheme = false)
    {
        return Url::toRoute(['/course/learning/index', 'id' => $this->id, 'section' => $sectionId, 'lesson' => $lessonId], $scheme);
    }

    public function getDiscountAmount()
    {
        if ($this->type == self::TYPE_COMBO) {
            $query = CourseComboItem::find();
            $query->where(['course_combo_id' => $this->id]);

            return $this->oldPrice - $query->sum('price');
        }

        $discountAmount = !empty($this->price_discount) ? $this->price_discount : round($this->percent_discount * $this->oldPrice / 100);
        $schedule = $this->getAvailiablePromotionSchedules();
        if( $schedule || ($discountAmount == 0 && $schedule)){
            $discountAmount = $schedule->discount_amount;
        }
        return $discountAmount;
    }

    public function setGroupDiscount($groupDiscount)
    {
        $this->groupDiscount = $groupDiscount;
    }

    public function getDiscountGroupAmount()
    {
        if (!empty($this->groupDiscount)) {
            $sellPrice = $this->oldPrice - $this->discountAmount;
            $percentDiscount = $this->groupDiscount->percent_discount / 100;
            $discountAmount = $sellPrice * $percentDiscount;
            if (!empty($this->groupDiscount->max_discount_amount))
                return $discountAmount <= $this->groupDiscount->max_discount_amount ? $discountAmount : $this->groupDiscount->max_discount_amount;
            return $discountAmount;
        }
        return 0;

    }

    public function getDiscountPercent()
    {
        if ($this->oldPrice > 0) {
            return round(($this->oldPrice - $this->sellPrice) / $this->oldPrice, 2) * 100;
        }

        return 0;
    }

    public function getTotalTimePartials()
    {
        $time = $this->total_time;

        if ($time / (3600 * 24 * 30) > 1) {
            return [
                'number' => ceil($time / (3600 * 24 * 30)),
                'unit' => 'months'
            ];
        }
        if ($time / (3600 * 24 * 7) > 1) {
            return [
                'number' => ceil($time / (3600 * 24 * 7)),
                'unit' => 'weeks'
            ];
        }
        if ($time / (3600 * 24) > 1) {
            return [
                'number' => ceil($time / (3600 * 24)),
                'unit' => 'days'
            ];
        }
        if ($time / 3600 > 1) {
            return [
                'number' => ceil($time / 3600),
                'unit' => 'hours'
            ];
        }

        return [
            'number' => ceil($time / 60),
            'unit' => 'minutes'
        ];
    }

    public function getTotalTimeText($long = true)
    {
        $timeArray = $this->getTotalTimePartials();
        $timeUnits = static::totalTimeUnits();

        return $timeArray['number'] . ' ' . $timeUnits[$timeArray['unit']];
    }

    public static function getTotalTimeInfo($time)
    {
        if ($time / (3600 * 24 * 30) > 1) {
            return [
                'number' => ceil($time / (3600 * 24 * 30)),
                'unit' => 'tháng'
            ];
        }
        if ($time / (3600 * 24 * 7) > 1) {
            return [
                'number' => ceil($time / (3600 * 24 * 7)),
                'unit' => 'tuần'
            ];
        }
        if ($time / (3600 * 24) > 1) {
            return [
                'number' => ceil($time / (3600 * 24)),
                'unit' => 'ngày'
            ];
        }
        if ($time / 3600 > 1) {
            return [
                'number' => ceil($time / 3600),
                'unit' => 'giờ'
            ];
        }

        return [
            'number' => ceil($time / 60),
            'unit' => 'phút'
        ];
    }

    public function getSectionTreeQuery()
    {
        return CourseSection::find()->where(['course_id' => $this->id, 'visible' => self::BOOL_YES, 'active' => self::BOOL_YES])->orderBy('root, lft');
    }

    public function getSections()
    {
        return $this->hasMany(CourseSection::className(), ['course_id' => 'id'])->andOnCondition([
            'course_id' => $this->id,
            'visible' => self::BOOL_YES,
            'active' => self::BOOL_YES
        ])->orderBy('root, lft');
    }

    public function getDefaultSection()
    {
        $sectionTree = CourseSection::find()->where(['course_id' => $this->id,
            'active' => CourseSection::BOOL_YES ])->orderBy('root, lft')->all();
        foreach ($sectionTree as $node) {
            if ($node->isLeaf()) {
                return $node;
            }
        }
    }

    public function getLastSection()
    {
        $sectionTree = CourseSection::find()->where(['course_id' => $this->id])->addOrderBy('root, lft')->orderBy(['id' => SORT_DESC])->all();
        foreach ($sectionTree as $node) {
            if ($node->isLeaf()) {
                return $node;
            }
        }
    }

    public function getFinishedLessons($userId)
    {
        $userCourseTblName = UserCourse::tableName();

        return ArrayHelper::map(UserCourseLesson::find()->alias('t')->joinWith('userCourse')->andWhere([
            "$userCourseTblName.user_id" => $userId,
            "$userCourseTblName.course_id" => $this->id,
            't.is_passed' => UserCourseLesson::BOOL_YES
        ])->select(['t.id', 't.course_lesson_id'])->all(), 'id', 'course_lesson_id');
    }

    public function getDocuments()
    {
        return $this->hasMany(CourseDocument::className(), ['course_id' => 'id']);
    }

    public function getLearnerQuestions()
    {
        return $this->hasMany(CourseLearnerQna::className(), ['course_id' => 'id'])
            ->onCondition(['or', ['question_id' => 0], ['question_id' => NULL]]);
    }

    public function getDiscussions()
    {
        return $this->hasMany(CourseDiscussion::className(), ['course_id' => 'id']);
    }

    /*
     * @desc get high light courses to show at Home page
     */

    public static function getListHighLight($limit = 8)
    {
        $featuredIds = CourseMeta::find()
            ->select('course_id')
            ->distinct()
            ->where([
                'key' => 'is_feature',
                'value' => '1',
            ])->column();

        $query = self::find()->where([
            'id' => $featuredIds,
            'status' => self::STATUS_ACTIVE,
            'type' => [self::TYPE_VIDEO, self::TYPE_SOFTWARE],
        ])->orderBy(['created_time' => SORT_DESC])->limit($limit);
        return $query->all();
    }

    public function getComboItems()
    {
        return $this->hasMany(CourseComboItem::className(), ['course_combo_id' => 'id'])->orderBy('position ASC');
    }

    public function getOldPrice()
    {
        if ($this->type == self::TYPE_COMBO) {
            $query = CourseComboItem::find();
            $query->where(['course_combo_id' => $this->id]);
            $query->joinWith('course');

            return $query->sum('courses.price');
        }

        return $this->price;
    }

    public function getSellPrice()
    {
        return $this->oldPrice - $this->discountAmount;
    }

    public function getComboName()
    {
        $liItems = "";
        foreach ($this->comboItems as $item) {
            $liItems .= "<li><b>{$item->course->name}</b> - Giá gốc: {$item->course->price}, Giảm giá: {$item->discountAmount}, Thành tiền: {$item->price}</li>";
        }

        $htmlText = <<<HTML
<div>
    <span class="combo-name">{$this->name}</span>
    <ul>
        {$liItems}
    </ul>
</div>
HTML;

        return $htmlText;
    }

    public function getDefaultCommission()
    {
        return $this->hasOne(CourseCommission::className(), ['course_id' => 'id'])->where(['is_default' => CourseCommission::BOOL_YES]);
    }

    public function getCommissions()
    {
        return $this->hasMany(CourseCommission::className(), ['course_id' => 'id']);
    }

    public function getCommissionType()
    {
        return $this->hasOne(CourseCommissionType::className(), ['id' => 'course_commission_type_id']);
    }

    public function getCommissionPercent()
    {
        $commissionType = $this->commissionType;
        $defaultCommission = $this->defaultCommission;

        if ($defaultCommission == null) {
            return 0;
        }

        if ($commissionType->is_required_expiration_date && !empty($defaultCommission->apply_to_date)) {
            $toDay = date('Y-m-d');
            $expiredDate = date('Y-m-d', $defaultCommission->apply_to_date);

            if (strtotime($toDay) > strtotime($expiredDate)) {
                return 0;
            }
        }

        return $defaultCommission->commission_percent;
    }

    /**
     * @desc Count lesson by level 1 sections
     * @return int|string
     */
    public function getLessonCount()
    {
        return $this->getSections()->andWhere([
            '>=',
            'lvl',
            1
        ])->count();

        /*
        $sectionTblName = CourseSection::tableName();
        return CourseLesson::find()->joinWith('section')->andWhere([
            "$sectionTblName.course_id" => $this->id,
            "$sectionTblName.visible" => self::BOOL_YES,
            "$sectionTblName.active" => self::BOOL_YES
        ])->count();
        */
    }

    public function getTeachingAssistants()
    {
        return $this->hasMany(CourseTeachingAssistant::className(), ['course_id' => 'id']);
    }

    public function getRelatedCourses()
    {
        if (!isset($this->_cache['relatedCourses'])) {
            $result = self::find()->andWhere(['category_id' => $this->category_id])
                ->andWhere(['status' => self::STATUS_ACTIVE])
                ->andWhere(['!=', 'id', $this->id])
                ->andWhere(['>', 'price', 0])
                ->limit(8)
                ->orderBy('created_time DESC')->all();
            $category = Category::findOne($this->category_id);
            $result_parent = null;
            if (count($result) < 8 && $category->parent_id != 0) {
                $result_parent = self::find()->andWhere(['category_id' => $category->parent_id])
                    ->andWhere(['status' => self::STATUS_ACTIVE])
                    ->andWhere(['!=', 'id', $this->id])
                    ->andWhere(['>', 'price', 0])
                    ->limit(8 - count($result))
                    ->orderBy('created_time DESC')->all();
            }
            if ($result_parent)
                $this->_cache['relatedCourses'] = array_merge($result, $result_parent);
            else
                $this->_cache['relatedCourses'] = $result;
        }
        return $this->_cache['relatedCourses'];

    }

    public function getRelatedCoursesByTag()
    {
        if (isset($this->_cache['relatedCoursesByTag'])) {
            return $this->_cache['relatedCoursesByTag'];
        }
        $result = self::find()
            ->join('INNER JOIN', CourseTag::tableName(), CourseTag::tableName() . '.course_id = ' . self::tableName() . '.id')
            ->join('INNER JOIN', Tag::tableName(), Tag::tableName() . '.id = ' . CourseTag::tableName() . '.tag_id')
            ->andWhere([self::tableName() . '.status' => self::STATUS_ACTIVE])
            ->andWhere(['!=', self::tableName() . '.id', $this->id])
            ->andWhere(['IN', Tag::tableName() . '.tag', $this->tags])
            ->andWhere([Tag::tableName() . '.status' => Tag::STATUS_ACTIVE])
            ->limit(5)
            ->orderBy(Course::tableName() . '.created_time DESC');
        if ($this->type == self::TYPE_COMBO) {
            $result = $result->andWhere([self::tableName() . '.type' => self::TYPE_COMBO]);
        } else {
            $result = $result->andWhere(['!=', self::tableName() . '.type', self::TYPE_COMBO]);
        }
        $this->_cache['relatedCoursesByTag'] = $result->all();
        return $this->_cache['relatedCoursesByTag'];
    }

    public function getUserCourse()
    {
        return $this->hasMany(UserCourse::className(), ['course_id' => 'id']);
    }

    public function getUserCourses()
    {
        return $this->hasMany(UserCourse::className(), ['course_id' => 'id']);
    }

    public function getCountUserCourse()
    {
        return UserCourse::find()->where(['course_id' => $this->id])->count();
    }

    public function getGraduatedPercentage()
    {

        $userHadGraduatedCount = UserCourse::find()->where(['course_id' => $this->id, 'is_graduated' => UserCourse::BOOL_YES])->count();
        $totalUserCount = $this->countUserCourse;

        if ($totalUserCount) {
            return (float)$userHadGraduatedCount / $totalUserCount;
        }

        return 0;

    }

    public function getMetaModel()
    {
        return $this->hasMany(CourseMeta::className(), ['course_id' => 'id']);
    }

    public static function listTypes()
    {
        return [
            self::TYPE_VIDEO => 'Khoá học Video',
            self::TYPE_SOFTWARE => 'Phần mềm'
        ];
    }

    public function getIsComboPartner()
    {
        if ($this->type == self::TYPE_COMBO) {
            foreach ($this->comboItems as $item) {
                if (!$item->course->isPartner) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    public function getComboPartnerTitle()
    {
        $result = '';
        if ($this->isComboPartner) {
            foreach ($this->comboItems as $item) {
                if ($result != '') {
                    $result .= ', ';
                }
                $result .= $item->course->name;
            }
        }
        return $result;
    }

    public function getIsPartner()
    {
        return $this->type === self::TYPE_SOFTWARE;
    }

    public function getIsMonkeyJunior()
    {
        return $this->hasOne(Partner::className(), ['id' => 'partner_id'])->where([
            'partner_type' => Partner::PARTNER_TYPE_NORMAL,
            'class' => 'monkey_junior'
        ])->exists();
    }

    public function getPartner()
    {
        return $this->hasOne(Partner::className(), ['id' => 'partner_id']);
    }

    public function getSeoMeta()
    {

        $oldPrice = $this->getOldPrice();
        $sellPrice = $this->getSellPrice();
        $template = [
            "{course_name}" => $this->name,
            "{price}" => number_format($oldPrice, 0, '.', ',') . "đ",
            "{teacher_name}" => $this->teacher ? $this->teacher->profile->name : "",
            "{teacher_title}" => $this->teacher ? $this->teacher->title : "",
            "{video_time}" => $this->getTotalTimeText(),
            "{level}" => $this->getLevelText(),
            "{lesson_count}" => $this->getLessonCount(),
        ];

        if ($oldPrice != $sellPrice) {
            $template["{remaining_price}"] = "giảm còn " . number_format($sellPrice, 0, '.', ',') . 'đ';
            $template["{percent_discount}"] = "giảm " . number_format(($oldPrice - $sellPrice) / $oldPrice * 100) . '%';
        } else {
            $template["{remaining_price}"] = $template["{price}"];
            $template["{percent_discount}"] = "";
        }
        $template['{course_description}'] = $this->description;
        $keys = array_keys($template);
        $values = array_values($template);


        $title = $this->seo_title;

        if (empty($title)) {
            $titleTemplate = SeoSetting::get("seo_course_view_title");

            if (!empty ($titleTemplate)) {
                $title = str_replace($keys, $values, $titleTemplate);

            } else {
                $title = $this->name;
            }
        }

        $description = $this->seo_description;

        if (empty ($description)) {
            $descriptionTemplate = SeoSetting::get("seo_course_view_description");
            if (!empty ($descriptionTemplate)) {
                $description = str_replace($keys, $values, $descriptionTemplate);
            } else {
                $description = $this->description;
            }
        }

        $keyword = $this->seo_keyword;
        if (empty ($keyword)) {
            $keywordTemplate = SeoSetting::get("seo_course_view_keyword");
            if (!empty ($keywordTemplate)) {
                $keyword = str_replace($keys, $values, $keywordTemplate);

            } else {
                $keyword = "";
            }
        }

        return [$title, $description, $keyword];
    }

    public function getCourseComboItems()
    {
        return $this->hasMany(CourseComboItem::className(), ['course_id' => 'id']);
    }

    public function getListCombo()
    {
        return $this->hasMany(CourseCombo::className(), ['id' => 'course_combo_id'])->via('courseComboItems')->andWhere(['status' => self::STATUS_ACTIVE]);
    }

    public function checkHasManyCombo(){
        $data = Recommendation::checkCourseHasManyCombo($this->id);
        if(empty($data))
            return false;

        $is_many_combo = $data[0]['sellcount'];
        $course_combo_id = $data[0]['combo_id'];
        if($is_many_combo)
        {
            $combo = CourseCombo::findOne(['id'=>$course_combo_id]);
            return $combo;
        }else{
            return false;
        }
    }

    public function afterSave($insert, $changedAttributes)
    {
        $ret = parent::afterSave($insert, $changedAttributes);

        ElasticCourse::update($this);
        $this->logTag();
        return $ret;
    }

    private function logTag()
    {
        $tags = $this->getTagsObjects();
        $tag_ids = ArrayHelper::getColumn($tags, ['id']);
        $courseTagsLog = new CourseTagLog();
        $courseTagsLog->course_id = $this->id;
        $courseTagsLog->tag_ids = implode(",", $tag_ids);
        $prevLog = CourseTagLog::find()->andWhere(['course_id'=>$this->id])->orderBy(['id'=>SORT_DESC])->one();
        $prevTagIds = [];
        if(!empty($prevLog))
        {
            $prevTagIds = explode(",", $prevLog->tag_ids);
        }
        else
        {
            $prevTagIds = $this->old_tag_ids;
        }
        if(!empty($prevTagIds))
        {
            $add_diff_ids = array_diff($tag_ids, $prevTagIds);
            $remove_diff_ids = array_diff($prevTagIds, $tag_ids);
            if(!empty($add_diff_ids)) {
                $courseTagsLog->add_ids = implode(",", $add_diff_ids);
            }
            if(!empty($remove_diff_ids)) {
                $courseTagsLog->remove_ids = implode(",", $remove_diff_ids);
            }

        }
        if(!empty( $courseTagsLog->remove_ids) || !empty($courseTagsLog->add_ids)) {
            if(!empty(Yii::$app->user)) {
                $courseTagsLog->created_by = Yii::$app->user->id;
            }
            $courseTagsLog->save();
        }
    }

    public function getRatings()
    {
        return $this->hasMany(CourseRating::className(), ['course_id' => 'id']);
    }

    public function getOrderDetails()
    {
        return $this->hasMany(OrderDetails::className(), ['course_id' => 'id']);
    }

    public function getTotal_users_count()
    {
        $configCountUserCourse = Yii::$app->params['count_total_user_course'];
        $courseId = $this->id;
        $countUser = $this->getOrderDetails()
            ->select(['o.user_id'])
            ->joinWith('order o')
            ->where(['!=', 'o.user_id', 0])
            ->andWhere(['!=', 'o.status', Order::ORDER_STATUS_CANCELLED])
            ->groupBy('o.user_id')->count();
        if(!empty($configCountUserCourse)){
            foreach ($configCountUserCourse as $courseConfigId=>$count){
                if($courseId == $courseConfigId){
                    $countUser += $count;
                    return $countUser;
                }
            }
        }
        return $countUser;
    }

    public function getMissions()
    {
        $missionCourseTblName = MissionCourse::tableName();
        $missionCategoryTblName = MissionCategory::tableName();

        $missions = Mission::find()->alias('m')
            ->joinWith(['missionCourses', 'missionCategories'])
            ->where([
                'OR',
                ['m.apply_course_type' => Mission::APPLY_COURSE_TYPE_ALL],
                [
                    'm.apply_course_type' => Mission::APPLY_COURSE_TYPE_SOME,
                    $missionCourseTblName . '.course_id' => $this->id
                ],
                [
                    'm.apply_course_type' => Mission::APPLY_COURSE_TYPE_CATEGORY,
                    $missionCategoryTblName . '.category_id' => [$this->category_id, $this->category->parent_id]
                ],
            ])
            ->andWhere(['m.status' => Mission::STATUS_ACTIVE])
            ->select(['m.id', 'm.name', 'm.k_point', 'm.k_point_for_free_course'])
            ->groupBy(['m.id', 'm.name', 'm.k_point', 'm.k_point_for_free_course'])
            ->all();

        return $missions;
    }

    public function getSummaryMissionByUser($user_id)
    {
        $notFinishedMissions = [];
        $finishedMissions = [];

        foreach ($this->missions as $mission) {
            /* @var $mission Mission */
            list($totalFinished, $total) = $mission->getSummaryByCourse($this->id, $user_id);
            if ($total == 0) {
                continue;
            }

            $mission->summaryText = "$totalFinished/$total";
            if ($totalFinished > 0 && $totalFinished >= $total) {
                $finishedMissions[] = $mission;
            } else {
                $notFinishedMissions[] = $mission;
            }
        }

        return [$finishedMissions, $notFinishedMissions];
    }

    public function getIsCampaign11()
    {
        if (isset(Yii::$app->params['is_campaign_1_1']) && Yii::$app->params['is_campaign_1_1']) {
            return Campaign11::find()->where(['course_id' => $this->id])->exists();
        }
        return false;
    }

    public function getIsCampaignGroupDiscount()
    {
        if (isset(Yii::$app->params['campaign_discount_group']) && Yii::$app->params['campaign_discount_group'] && empty($this->combo)) {
            return GroupDiscountCourse::find()->where(['course_id' => $this->id])->exists();
        }
        return false;
    }

    public function getIsBirthdayDiscount()
    {
        if (isset(Yii::$app->params['campaign_birthday']) && Yii::$app->params['campaign_birthday'] && empty($this->combo)) {
            return GroupDiscountCourse::find()->where(['course_id' => $this->id])->exists();
        }
        return false;
    }

    public function getOpinions()
    {
        $opinionTbl = CourseOpinions::tableName();
        return $this->hasMany(CourseOpinions::className(), ['course_id' => 'id'])->andWhere(["{$opinionTbl}.status" => CourseOpinions::STATUS_ACTIVE])->orderBy("{$opinionTbl}.order ASC");
    }

    public function getScreenshots()
    {
        $screenshotTbl = CourseScreenshot::tableName();
        return $this->hasMany(CourseScreenshot::className(), ['course_id' => 'id'])->andWhere(["{$screenshotTbl}.status" => CourseScreenshot::STATUS_ACTIVE])->orderBy("{$screenshotTbl}.order ASC");
    }

    public function getDevicesText()
    {
        $result = '';

        if (!empty($this->window_app_link)) {
            $result .= 'Máy tính Windows';
        }
        if (!empty($this->mac_app_link)) {
            if (!empty($this->window_app_link)) {
                $result .= '/';
            }
            $result .= 'MacOS';
        }
        if (!empty($this->ios_app_link)) {
            $result .= !empty($result) ? ', ' : '';
            $result .= 'iOS';
        }
        if (!empty($this->android_app_link)) {
            $result .= !empty($result) ? ', ' : '';
            $result .= 'Android';
        }

        return $result;
    }
}
