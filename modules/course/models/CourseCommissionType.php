<?php

namespace kyna\course\models;

use Yii;

/**
 * This is the model class for table "{{%course_commission_types}}".
 *
 * @property integer $id
 * @property string $name
 * @property double $start_percent
 * @property double $end_percent
 * @property double $default_percent
 * @property boolean $is_required_expiration_date
 * @property boolean $is_multiple_commissions
 * @property integer $status
 * @property integer $created_time
 * @property integer $updated_time
 */
class CourseCommissionType extends \kyna\base\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%course_commission_types}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'default_percent'], 'required'],
            [['start_percent', 'end_percent', 'default_percent'], 'number', 'min' => 0, 'max' => 100],
            [['is_required_expiration_date', 'is_multiple_commissions'], 'boolean'],
            [['status', 'created_time', 'updated_time'], 'integer'],
            [['name'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'start_percent' => 'Hoa hồng từ',
            'end_percent' => 'đến',
            'default_percent' => 'Hoa hồng mặc định',
            'is_required_expiration_date' => 'Giới hạn ngày được nhận hoa hồng',
            'is_multiple_commissions' => 'Hoa hồng theo thời gian',
            'status' => 'Status',
            'created_time' => 'Created Time',
            'updated_time' => 'Updated Time',
        ];
    }
}
