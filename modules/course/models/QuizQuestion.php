<?php

namespace kyna\course\models;

use app\modules\course\models\ConnectQuestionProcessor;
use app\modules\course\models\QuestionProcessor;
use app\modules\course\models\SingleChoiceQuestionProcessor;
use app\modules\course\models\MultipleChoiceQuestionProcessor;
use app\modules\course\models\EssayQuestionProcessor;
use app\modules\course\models\FillInQuestionProcessor;

/**
 * This is the model class for table "course_questions".
 *
 * @property int $id
 * @property int $course_id
 * @property int $type
 * @property string $content
 * @property string $image_url
 * @property int $status
 * @property bool $is_deleted
 * @property int $created_time
 * @property int $updated_time
 * @property int $parent_id
 * @property QuizQuestion $parent
 * @property QuizQuestion[] $children
 * @property QuizAnswer[] $answers
 * @property int $order
 * @property QuestionProcessor $processor
 * @property string $answer_explain
 * @property string $sound_url
 * @property string $video_embed
 */
class QuizQuestion extends \kyna\base\ActiveRecord
{
    
    const TYPE_ONE_CHOICE = 1;
    const TYPE_WRITING = 2;
    const TYPE_FILL_INTO_DOTS = 3;
    const TYPE_READING = 4;
    const TYPE_CONNECT_ANSWER = 5;
    const TYPE_MULTIPLE_CHOICE = 6;
    
    private $_processor;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'quiz_questions';
    }

    protected static function softDelete()
    {
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['course_id', 'content'], 'required'],
            [['course_id', 'type', 'status', 'created_time', 'updated_time', 'number_of_dots', 'order'], 'integer'],
            [['content', 'image_url', 'answer_explain', 'sound_url', 'video_embed'], 'string'],
            [['is_deleted'], 'boolean'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'course_id' => 'Khóa học',
            'type' => 'Loại câu hỏi',
            'content' => 'Nội dung',
            'number_of_dots' => 'Số lượng vị trí',
            'image_url' => 'Hình',
            'status' => 'Trạng thái',
            'is_deleted' => 'Is Deleted',
            'created_time' => 'Created Time',
            'updated_time' => 'Updated Time',
        ];
    }
    
    public function getProcessor()
    {
        if ($this->_processor == null) {
            switch ($this->type) {
                case self::TYPE_MULTIPLE_CHOICE:
                    $this->_processor = new MultipleChoiceQuestionProcessor();
                    break;
                
                case self::TYPE_ONE_CHOICE:
                    $this->_processor = new SingleChoiceQuestionProcessor();
                    break;
                
                case self::TYPE_WRITING:
                    $this->_processor = new EssayQuestionProcessor();
                    break;
                
                case self::TYPE_FILL_INTO_DOTS:
                    $this->_processor = new FillInQuestionProcessor();
                    break;
                case self::TYPE_CONNECT_ANSWER:
                    $this->_processor = new ConnectQuestionProcessor();
                
            }
            
            $this->_processor->setQuestion($this);
        }
        
        return $this->_processor;
    }

    public function getCourse()
    {
        return $this->hasOne(Course::className(), ['id' => 'course_id']);
    }

    public static function getTypes()
    {
        return [
            self::TYPE_ONE_CHOICE => 'Trắc nghiệm (1 câu trả lời đúng)',
            self::TYPE_MULTIPLE_CHOICE => 'Trắc nghiệm (nhiều câu trả lời đúng)',
            self::TYPE_WRITING => 'Tự luận',
            self::TYPE_FILL_INTO_DOTS => 'Điền vào chỗ trống',
            static::TYPE_CONNECT_ANSWER => "Nối câu trả lời"

        ];
    }

    public function getTypeText()
    {
        $types = self::getTypes();

        return isset($types[$this->type]) ? $types[$this->type] : null;
    }

    public function getQuizQuestions()
    {
        return $this->hasMany(QuizDetail::className(), ['course_question_id' => 'id']);
    }

    public function getAnswers()
    {
        return $this->hasMany(QuizAnswer::className(), ['question_id' => 'id'])->indexBy('id');
    }

    public function getCorrectAnswers()
    {
        return $this->hasMany(QuizAnswer::className(), ['question_id' => 'id'])->andWhere(['is_correct' => self::BOOL_YES]);
    }

    public function getAnswersValidate()
    {
        $result = true;
        if($this->type == self::TYPE_FILL_INTO_DOTS && $this->number_of_dots == count($this->answers)){
            $result = false;
        }
        return $result;
    }

    public function getChildren()
    {
        return $this->hasMany(self::className(), ['parent_id' => 'id']);
    }

    public function getParent()
    {
        return $this->hasOne(self::className(), ['parent_id' => 'id']);
    }
    
    public function calculateScore()
    {
        // TODO: return score * coefficient
        if (empty ($this->children))
            return 1;
        return 0;
    }
}
