<?php

namespace kyna\course\models;

use Yii;

/**
 * This is the model class for table "{{%course_commissions}}".
 *
 * @property integer $id
 * @property integer $course_id
 * @property double $commission_percent
 * @property integer $apply_from_date
 * @property integer $apply_to_date
 * @property boolean $is_default
 * @property boolean $is_deleted
 * @property integer $status
 * @property integer $created_time
 * @property integer $updated_time
 */
class CourseCommission extends \kyna\base\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%course_commissions}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['course_id', 'commission_percent'], 'required'],
            [['course_id', 'status', 'created_time', 'updated_time'], 'integer'],
            [['apply_from_date', 'apply_to_date'], 'safe'],
            [['commission_percent'], 'number', 'max' => 100],
            [['is_default', 'is_deleted'], 'boolean'],
            [
                'apply_to_date', 
                'required',
                'when' => function ($model) {
                    return empty($model->course->commissionType) ? false : $model->course->commissionType->is_required_expiration_date == CourseCommissionType::BOOL_YES;
                },
                'whenClient' => "function(attribute, value) {
                    var isRequired = $('#course-course_commission_type_id').find('option:selected').attr('data-required-end-date');
                    return isRequired == 'true';
                }",
                'message' => 'Cần thiết lập ngày hết hạn hoa hồng tương ứng với loại hoa hồng hiện tại'
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'course_id' => 'Khóa học',
            'commission_percent' => 'Phần trăm hoa hồng',
            'apply_from_date' => 'Ngày bắt đầu hưởng hoa hồng',
            'apply_to_date' => 'Ngày hết hạn hưởng hoa hồng',
            'is_default' => 'Is Default',
            'is_deleted' => 'Is Deleted',
            'status' => 'Status',
            'created_time' => 'Created Time',
            'updated_time' => 'Updated Time',
        ];
    }
    
    public function beforeSave($insert)
    {
        $ret = parent::beforeSave($insert);
        
        $dateFormat = str_replace('php:', '', Yii::$app->formatter->dateFormat);

        if (!empty($this->apply_from_date)) {
            $fromDate = date_create_from_format($dateFormat, $this->apply_from_date);
            if ($fromDate !== false) {
                $this->apply_from_date = $fromDate->getTimestamp();
            }
        }
        
        if (!empty($this->apply_to_date)) {
            $endDate = date_create_from_format($dateFormat, $this->apply_to_date);
            if ($endDate !== false) {
                $this->apply_to_date = $endDate->getTimestamp();
            }
        }
        
        return $ret;
    }
    
    public function getCourse()
    {
        return $this->hasOne(Course::className(), ['id' => 'course_id']);
    }
}
