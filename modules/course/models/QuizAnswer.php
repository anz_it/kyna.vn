<?php

namespace kyna\course\models;

use Yii;
use kotchuprik\sortable\behaviors\Sortable;

/**
 * This is the model class for table "course_answers".
 *
 * @property integer $id
 * @property integer $question_id
 * @property string $content
 * @property string $image_url
 * @property boolean $is_correct
 * @property string $explanation
 * @property integer $order
 * @property integer $status
 * @property boolean $is_deleted
 * @property integer $created_time
 * @property integer $updated_time
 */
class QuizAnswer extends \kyna\base\ActiveRecord
{

    const IS_EXACTLY_YES = 1;
    const IS_EXACTLY_NO = 0;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'quiz_answers';
    }

    protected static function softDelete()
    {
        return true;
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['question_id', 'content'], 'required'],
            [['question_id', 'order', 'status', 'created_time', 'updated_time'], 'integer'],
            [['content', 'image_url', 'explanation'], 'string'],
            [['is_correct', 'is_deleted'], 'boolean'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'question_id' => 'Question ID',
            'content' => 'Content',
            'image_url' => 'Hình',
            'is_correct' => 'Is Exactly',
            'explanation' => 'Giải thích',
            'order' => 'Order',
            'status' => 'Status',
            'is_deleted' => 'Is Deleted',
            'created_time' => 'Created Time',
            'updated_time' => 'Updated Time',
        ];
    }

    public function getQuestion() {
        return $this->hasOne(CourseQuestion::className(), ['id' => 'question_id']);
    }

    public static function getIsExactlyOptions()
    {
        return [
            self::IS_EXACTLY_YES => 'Đúng',
            self::IS_EXACTLY_NO => 'Sai',
        ];
    }

    public function getIsExactlyText()
    {
        $options = self::getIsExactlyOptions();
        return isset($options[$this->is_correct]) ? $options[$this->is_correct] : null;
    }
}
