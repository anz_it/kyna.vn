<?php

namespace kyna\course\models;

use Yii;
use kyna\course\models\Teacher;

/**
 * This is the model class for table "course_teaching_assistants".
 *
 * @property integer $id
 * @property integer $course_id
 * @property integer $teaching_assistant_id
 * @property integer $type
 * @property integer $status
 * @property integer $created_time
 * @property integer $updated_time
 */
class CourseTeachingAssistant extends \kyna\base\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'course_teaching_assistants';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['course_id'], 'required'],
            [['course_id', 'teaching_assistant_id', 'type', 'status', 'created_time', 'updated_time'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'course_id' => 'Course ID',
            'teaching_assistant_id' => 'Trợ giảng',
            'type' => 'Type',
            'status' => 'Status',
            'created_time' => 'Created Time',
            'updated_time' => 'Updated Time',
        ];
    }
    
    public function getTeachingAssistant()
    {
        return $this->hasOne(Teacher::className(), ['id' => 'teaching_assistant_id']);
    }
}
