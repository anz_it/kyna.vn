<?php

namespace kyna\course\models;

use Yii;

/**
 * This is the model class for table "quiz_session_answers".
 *
 * @property integer $id
 * @property integer $quiz_session_id
 * @property integer $quiz_detail_id
 * @property string $answer
 * @property integer $is_correct
 * @property integer $score
 * @property QuizQuestion $question
 */
class QuizSessionAnswer extends \kyna\base\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'quiz_session_answers';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['score', 'required', 'on' => 'mark'],
            ['score', 'integer', 'integerOnly' => false, 'min' => 0, 'max' => 10, 'on' => 'mark'],
            [['quiz_session_id', 'quiz_detail_id', 'is_correct', 'quiz_question_id', 'parent_id'], 'integer'],
            [['score'], 'integer', 'integerOnly' => false],
            [['answer'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'quiz_session_id' => 'Quiz Session ID',
            'quiz_detail_id' => 'Quiz Detail ID',
            'answer' => 'Answer Text',
            'is_correct' => 'Is Correct',
            'score' => 'Score',
        ];
    }

    public function getQuestion()
    {
        if (empty($this->quiz_question_id)) {
            $quizDetail = QuizDetail::findOne($this->quiz_detail_id);
            if ($quizDetail) {

                $dt = self::findOne($this->id);
                $dt->quiz_question_id = $quizDetail->quiz_question_id;
                $dt->save(false);
            }
        }
        return $this->hasOne(QuizQuestion::className(), ['id' => 'quiz_question_id']);
    }
    
    public function getQuizDetail()
    {
        return $this->hasOne(QuizDetail::className(), ['id' => 'quiz_detail_id']);
    }

    public function getSession()
    {
        return $this->hasOne(QuizSession::className(), ['id' => 'quiz_session_id']);
    }

    public function getIsAnswered()
    {
        return $this->answer != null;
    }

    public static function insertMany($columns, $rows)
    {
        if (!isset($columns['position'])) {
            $columns[] = 'position';
            $position = 0;
            foreach ($rows as &$row) {
                if (empty($row[3]))
                    $position = $position+1;
                $row[] = $position;

            }
        }
        return \Yii::$app->db->createCommand()->batchInsert(self::tableName(), $columns, $rows)->execute();
    }

    public function calculateScore()
    {
        $this->score = $this->question->processor->calculateScore($this->answer);
        
        $this->is_correct = ($this->score > 0) ? self::BOOL_YES : self::BOOL_NO;
        
        $this->save();
        
        return $this->score;
    }
    
}
