<?php

namespace kyna\course\models;
 
use Yii;
use kartik\tree\TreeView;
 
class CourseSection extends \kyna\base\ActiveRecord
{
    
    use \kartik\tree\models\TreeTrait {
        isDisabled as parentIsDisabled; // note the alias
        rules as parentRules;
    }

    public $children = [];
    public $lesson_coefficient = [];

    /**
     * @var string the classname for the TreeQuery that implements the NestedSetQueryBehavior.
     * If not set this will default to `kartik	ree\models\TreeQuery`.
     */
    public static $treeQueryClass; // change if you need to set your own TreeQuery
 
    /**
     * @var bool whether to HTML encode the tree node names. Defaults to `true`.
     */
    public $encodeNodeNames = false;
 
    /**
     * @var bool whether to HTML purify the tree node icon content before saving.
     * Defaults to `true`.
     */
    public $purifyNodeIcons = true;
 
    /**
     * @var array activation errors for the node
     */
    public $nodeActivationErrors = [];
 
    /**
     * @var array node removal errors
     */
    public $nodeRemovalErrors = [];
 
    /**
     * @var bool attribute to cache the `active` state before a model update. Defaults to `true`.
     */
    public $activeOrig = true;
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'course_sections';
    }
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        $rules = self::parentRules();

        return array_merge($rules, [
            ['course_id', 'safe'],
            [['score', 'percent_can_pass', 'coefficient'], 'number', 'min' => 1, 'max' => 100],
            [['coefficient', 'percent_can_pass'], 'required']
        ]);
    }

    public function attributeLabels()
    {
        $labels = parent::attributeLabels(); // TODO: Change the autogenerated stub

        return array_merge($labels, [
            'score' => 'Điểm',
            'coefficient' => 'Hệ số',
            'percent_can_pass' => 'Phần trăm để xác nhận hoàn thành'
        ]);
    }

    /**
     * Removes a node
     *
     * @param bool $softDelete whether to soft delete or hard delete
     * @param bool $currNode whether to update the current node value also
     *
     * @return bool status of activation/inactivation
     */
    public function removeNode($softDelete = true, $currNode = true)
    {
        /**
         * @var Tree $this
         * @var Tree $child
         */
        if ($softDelete == true) {
            $this->nodeRemovalErrors = [];
            $module = TreeView::module();
            extract($module->treeStructure);
            if ($this->isRemovableAll()) {
                $children = $this->children()->all();
                foreach ($children as $child) {
                    $child->active = 1;
                    if (!$child->save()) {
                        /** @noinspection PhpUndefinedFieldInspection */
                        /** @noinspection PhpUndefinedVariableInspection */
                        $this->nodeRemovalErrors[] = [
                            'id' => $child->$idAttribute,
                            'name' => $child->$nameAttribute,
                            'error' => $child->getFirstErrors()
                        ];
                    }
                }
            }
            if ($currNode) {
                $this->active = 0;
                if (!$this->save()) {
                    /** @noinspection PhpUndefinedFieldInspection */
                    /** @noinspection PhpUndefinedVariableInspection */
                    $this->nodeRemovalErrors[] = [
                        'id' => $this->$idAttribute,
                        'name' => $this->$nameAttribute,
                        'error' => $this->getFirstErrors()
                    ];
                    return false;
                }
            }
            return true;
        } else {
            return $this->removable_all || $this->isRoot() && $this->children()->count() == 0 ?
                $this->deleteWithChildren() : $this->delete();
        }
    }

    public function getLessions()
    {
        return $this->hasMany(CourseLesson::className(), ['section_id' => 'id']);
    }

    public function getLessons()
    {
        return $this->hasMany(CourseLesson::className(), ['section_id' => 'id']);
    }
}