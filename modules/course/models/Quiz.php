<?php

namespace kyna\course\models;

use yii\db\Expression;
use yii\db\Query;

/**
 * This is the model class for table "quizes".
 *
 * @property int $id
 * @property int $course_id
 * @property int $type
 * @property string $name
 * @property string $image_url
 * @property string $description
 * @property int $status
 * @property boolean $is_deleted
 * @property int $created_time
 * @property int $updated_time
 */
class Quiz extends \kyna\base\ActiveRecord
{

    const TYPE_ONE_CHOICE = 1;
    const TYPE_WRITING  = 2;
    const TYPE_FILL_INTO_DOTS = 3;
    const TYPE_ALL = 0;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'quizes';
    }

    public function enableMeta()
    {
        return ['quiz', QuizMeta::className()];
    }

    protected function metaKeys()
    {
        return [
            'duration',
            'min_score',
            'style_show',
            'is_random_question',
            'is_end_quiz',
            'is_show_question_explaination',
            'is_show_result_when_done_a_question',
            'is_back_or_next',
            'max_of_redo'
        ];
    }

    protected static function softDelete()
    {
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['course_id', 'name'], 'required'],
            [['course_id', 'status', 'created_time', 'updated_time', 'type'], 'integer'],
            [['image_url', 'description'], 'string'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'course_id' => 'Khóa học',
            'name' => 'Tên Quiz',
            'type'  => 'Loại Quiz',
            'image_url' => 'Link hình',
            'description' => 'Mô tả',
            'status' => 'Status',
            'created_time' => 'Created Time',
            'updated_time' => 'Updated Time',
        ];
    }

    public static function getTypes()
    {
        return [
            self::TYPE_ONE_CHOICE => 'Trắc nghiệm',
            self::TYPE_WRITING => 'Tự luận',
            self::TYPE_FILL_INTO_DOTS => 'Điền vào chỗ trống',
            self::TYPE_ALL => 'Hỗn hợp (Bao gồm trắc nghiệm, tự luận ...',
        ];
    }

    public function getTypeText()
    {
        $types = self::getTypes();

        return isset($types[$this->type]) ? $types[$this->type] : null;
    }

    public function getSession($userId)
    {
        return QuizSession::initSession($this->id, $userId);
    }

    /**
     * @return Query
     */
    public function getDetails()
    {
        return $this->hasMany(QuizDetail::className(), ['quiz_id' => 'id']);
    }

    public function getDetailQuestionIds()
    {
        return $this->getDetails()->select('quiz_question_id')->column();
    }

    public function getCourse()
    {
        return $this->hasOne(Course::className(), ['id' => 'course_id']);
    }
    
    public function getLesson()
    {
        return $this->hasOne(CourseLesson::className(), ['quiz_id' => 'id']);
    }

    public function getPercent_can_pass()
    {
        $courseLesson = CourseLesson::find()->andWhere(['quiz_id' => $this->id])->select('percent_can_pass')->one();

        if ($courseLesson != null) {
            return $courseLesson->percent_can_pass;
        }

        return 0;
    }
}
