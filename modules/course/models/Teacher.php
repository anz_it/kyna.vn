<?php

namespace kyna\course\models;

use Yii;
use yii\helpers\Url;
use common\helpers\ArrayHelper;
use kyna\tag\models\Tag;
use kyna\tag\models\TeacherTag;
use kyna\settings\models\TeacherSetting;
use kyna\settings\models\SeoSetting;

/**
 * Class Teacher is an extends of User
 */
class Teacher extends \kyna\user\models\User
{
    public $tags_list = [];
    private $_cache = [];

    public function init()
    {
        parent::init();

        $this->on(self::EVENT_AFTER_INSERT, [$this, 'updateTeacherTags']);
        $this->on(self::EVENT_AFTER_UPDATE, [$this, 'updateTeacherTags']);

        return;
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['tags_list'], 'safe'],
        ]);
    }

    protected function metaKeys()
    {
        return array_merge(parent::metaKeys(), [
            'title',
            'introduction',
            'slug',
            'seo_title',
            'seo_description',
            'seo_keyword',
            'seo_robot_index',
            'seo_robot_follow',
            'seo_canonical',
            'seo_sitemap'
        ]);
    }
    
    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'title' => 'Chức danh',
            'slug' => 'Slug',
            'tags_list' => 'Danh sách Tags',
            'seo_title' => 'Seo Title',
            'seo_description' => 'Meta Description',
            'seo_keyword' => 'Meta Keyword',
            'seo_robot_index' => 'Meta Robots Index',
            'seo_robot_follow' => 'Meta Robots Follow',
            'seo_canonical' => 'Canonical URL',
            'seo_sitemap' => 'Include in Sitemap?'
        ]);
    }
    
    public function getCourses()
    {
        return $this->hasMany(Course::className(), ['teacher_id' => 'id']);
    }
    
    public function beforeDelete()
    {
        if (!empty($this->courses)) {
            $name = !empty($this->profile->name) ? $this->profile->name : $this->email;
            Yii::$app->session->setFlash('error', "Không thể xóa giảng viên <b>{$name}</b> bởi vì vẫn còn khóa học đang giảng dạy.");
            
            return false;
        }
        
        return parent::beforeDelete();
    }

    public function getReceiveQnaDate()
    {
        $settingModel = TeacherSetting::findOne([
            'key' => TeacherSetting::KEY_RECEIVE_MAIL_DATE,
            'teacher_id' => $this->id
        ]);

        return $settingModel != null ? $settingModel->value : null;
    }

    public function getTags()
    {
        $tags = $this->getTagsObjects();
        return ArrayHelper::getColumn($tags, ['tag']);
    }

    public function updateTeacherTags()
    {
        TeacherTag::assignTeacherTag($this->tags_list, $this->id, true);
    }

    public function getTagsObjects()
    {
        if (isset($this->_cache['tagsObjects']))
            return $this->_cache['tagsObjects'];
        $this->_cache['tagsObjects'] = Tag::find()
            ->join('INNER JOIN', TeacherTag::tableName(), TeacherTag::tableName() . '.tag_id = ' . Tag::tableName() . '.id')
            ->join('INNER JOIN', Teacher::tableName(), Teacher::tableName() . '.id = ' . TeacherTag::tableName() . '.teacher_id')
            ->where([
                Teacher::tableName() . '.id' => $this->id,
                Tag::tableName() . '.status' => Tag::STATUS_ACTIVE
            ])
            ->all();

        return $this->_cache['tagsObjects'];

    }

    public function getIsUserRegister()
    {
        return TeacherNotify::find()
            ->where([
                'teacher_id' => $this->id,
                'user_id' => Yii::$app->user->id
            ])
            ->exists();
    }

    /**
     * Get array SEO info of Teacher
     * @return array
     */
    public function getSeoMeta()
    {
        $template = [
            "{teacher_name}" => $this->profile->name,
            "{teacher_title}" => $this->title
        ];
        $keys = array_keys($template);
        $values = array_values($template);

        $title = $this->seo_title;
        $description = $this->seo_description;
        $keyword = $this->seo_keyword;
        $robots = [];
        if (!empty($this->seo_robot_index)) {
            $robots['index'] = $this->seo_robot_index;
        }
        if (!empty($this->seo_robot_follow)) {
            $robots['follow'] = $this->seo_robot_follow;
        }
        $canonical = $this->seo_canonical;

        if (empty($title)) {
            $title = SeoSetting::get("seo_teacher_title");
            if (empty($title)) {
                $title = 'Thông tin giàng viên {teacher_name}';
            }
        }
        $title  = str_replace($keys, $values, $title);

        if (empty($description)) {
            $description = SeoSetting::get("seo_teacher_description");
            if (empty($description)) {
                $description = 'Tổng hợp các khóa học của giảng viên {teacher_name}, {teacher_title}. Ưu đãi học phí khi học Online cùng {teacher_name}. Đăng kí ngay!';
            }
        }
        $description  = str_replace($keys, $values, $description);

        return [$title, $description, $keyword, $robots, $canonical];
    }

    public function getUrl()
    {
        return Url::toRoute(['/user/teacher/index', 'slug' => $this->slug]);

    }
}

