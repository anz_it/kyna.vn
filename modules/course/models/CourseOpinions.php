<?php

namespace kyna\course\models;

use Yii;
use common\helpers\DocumentHelper;
use common\helpers\CDNHelper;
use yii2tech\ar\softdelete\SoftDeleteBehavior;
use kotchuprik\sortable\behaviors\Sortable;
use common\widgets\upload\UploadRequiredValidator;

/**
 * This is the model class for table "course_documents".
 *
 * @property integer $id
 * @property string $user_name
 * @property string $description
 * @property integer $course_id
 * @property string $avatar_url
 * @property integer $status
 * @property integer $order
 */
class CourseOpinions extends \kyna\base\ActiveRecord
{
    /**
     * @inheritdoc
     */

    public $imageSize = [
        'cover' => [
            CDNHelper::IMG_SIZE_THUMBNAIL,
            CDNHelper::IMG_SIZE_THUMBNAIL_SMALL
        ],
        'contain' => [],
        'crop' => [
            CDNHelper::IMG_SIZE_THUMBNAIL_YOUTUBE,
            CDNHelper::IMG_SIZE_THUMBNAIL_YOUTUBE_LARGE
        ]
    ];

    public static function tableName()
    {
        return 'course_opinions';
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['course_id', 'description', 'user_name', 'status'], 'required'],
            [['avatar_url'], UploadRequiredValidator::className(), 'skipOnEmpty' => true],
            [['avatar_url'], 'image', 'skipOnEmpty' => true, 'maxSize' => '4194304', 'mimeTypes' => 'image/png,image/jpeg'], // 4MiB
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Id',
            'course_id' => 'Khoá ',
            'description' => 'Nhận xét',
            'user_name' => 'Tên Người ',
            'avatar_url' => 'Avatar (png,jpg)',

        ];
    }

    public function behaviors()
    {
        $behaviors = parent::behaviors();

        if (static::softDelete()) {
            // Soft delete
            $behaviors['softDeleteBehavior'] = [
                'class' => SoftDeleteBehavior::className(),
                'softDeleteAttributeValues' => [
                    'is_deleted' => true,
                ],
                'restoreAttributeValues' => [
                    'is_deleted' => false,
                ],
                'replaceRegularDelete' => true,
            ];
        }

        $behaviors['sortable'] = [
            'class' => Sortable::className(),
            'query' => self::find(),
        ];

        return $behaviors;
    }
}
