<?php

namespace kyna\course\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use kyna\course\models\CourseDiscussion;

/**
 * CourseDiscussionSearch represents the model behind the search form about `kyna\course\models\CourseDiscussion`.
 */
class CourseDiscussionSearch extends CourseDiscussion
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'course_id', 'user_id', 'parent_id', 'status', 'created_time', 'updated_time'], 'integer'],
            [['comment'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CourseDiscussion::find()->joinWith('course');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $query->with('comments');

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'course_id' => $this->course_id,
            'user_id' => $this->user_id,
            'parent_id' => $this->parent_id,
            'status' => $this->status,
            'created_time' => $this->created_time,
            'updated_time' => $this->updated_time,
        ]);

        $query->andFilterWhere(['like', 'comment', $this->comment]);
        
        $query->orderBy('updated_time DESC');

        return $dataProvider;
    }
}
