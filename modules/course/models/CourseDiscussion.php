<?php

namespace kyna\course\models;

use Yii;
use kyna\user\models\User;
use common\helpers\StringHelper;
use kyna\course\models\CourseTeachingAssistant;

/**
 * This is the model class for table "course_discussions".
 *
 * @property integer $id
 * @property integer $course_id
 * @property integer $user_id
 * @property integer $parent_id
 * @property string $comment
 * @property integer $comment_to_user_id
 * @property integer $status
 * @property integer $created_time
 * @property integer $updated_time
 */
class CourseDiscussion extends \kyna\base\ActiveRecord
{

    const STATUS_SPAM = 2;
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'course_discussions';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['course_id', 'user_id', 'comment'], 'required'],
            [['course_id', 'user_id', 'parent_id', 'comment_to_user_id', 'status', 'created_time', 'updated_time'], 'integer'],
            [['comment'], 'string'],
//            [['comment'], 'filter', 'filter' => [StringHelper::class, 'htmlContentFilter']]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'course_id' => 'Khóa học',
            'user_id' => 'Học viên',
            'comment' => 'Nội dung',
            'status' => 'Status',
            'created_time' => 'Posted time',
            'updated_time' => 'Updated Time',
        ];
    }

    public function getPostedTime()
    {
        return Yii::$app->formatter->asRelativeTime($this->created_time);
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function getComments()
    {
        return $this->hasMany(self::className(), ['parent_id' => 'id']);
    }
    
    public function getParent()
    {
        return $this->hasOne(self::className(), ['id' => 'parent_id']);
    }
    
    public function getCourse()
    {
        return $this->hasOne(Course::className(), ['id' => 'course_id']);
    }
    
    public function afterDelete()
    {
        $ret = parent::afterDelete();
        
        foreach ($this->comments as $comment) {
            $comment->delete();
        }
        
        return $ret;
    }
    
    public function getTeachingAssistants()
    {
        return $this->hasMany(CourseTeachingAssistant::className(), ['course_id' => 'id'])->via('course');
    }
    
    public static function find()
    {
        $query = parent::find();
        
        if (isset(Yii::$app->user) && Yii::$app->user->can('TeachingAssistant')) {
            $query->joinWith('teachingAssistants as ta');
            $query->andWhere(['ta.teaching_assistant_id' => Yii::$app->user->id]);
        }
        
        return $query;
    }
    
    public static function listStatus()
    {
        $list = [
            self::STATUS_SPAM => 'Yes',
            self::STATUS_ACTIVE => 'No',
        ];

        return $list;
    }
    
}
