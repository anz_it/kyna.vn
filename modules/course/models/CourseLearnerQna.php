<?php

namespace kyna\course\models;

use Yii;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;

use kyna\user\models\User;
use kyna\settings\models\Setting;
use kyna\settings\models\TeacherSetting;
use kyna\gamification\models\Mission;

/**
 * This is the model class for table "course_learner_qna".
 *
 * @property integer $id
 * @property integer $course_id
 * @property integer $user_id
 * @property string $content
 * @property integer $question_id
 * @property integer $posted_time
 * @property boolean $is_approved
 * @property boolean $is_send_approved
 * @property boolean $is_send_unapproved
 * @property boolean $is_send_to_teacher
 * @property boolean $is_send_moved
 * @property boolean $is_send_deleted
 * @property boolean $is_send_answered
 * @property integer $status
 * @property integer $updated_time
 * @property boolean $is_on_behalf_of_teacher
 */
class CourseLearnerQna extends \kyna\base\ActiveRecord
{

    const SCENARIO_APPROVE = 'scenario-approve';

    const EVENT_APPROVED = 'event-approved';

    public $question_content;
    public $answer_content;

    public function init()
    {
        $ret = parent::init();

        $this->on(self::EVENT_APPROVED, [$this, 'afterApprove']);

        return $ret;
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'course_learner_qna';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['content'], 'required'],
            [['is_approved', 'is_send_approved', 'is_send_unapproved', 'is_send_to_teacher', 'is_send_moved', 'is_send_deleted', 'is_send_answered', 'is_on_behalf_of_teacher'], 'boolean'],
            [['course_id', 'user_id', 'question_id', 'posted_time', 'status', 'updated_time'], 'integer'],
            [['content', 'question_content'], 'string'],
            ['content', 'required', 'on' => ['answer', 'update']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'course_id' => 'Khóa học',
            'user_id' => 'Học viên',
            'content' => 'Nội dung',
            'question_id' => 'Câu hỏi',
            'posted_time' => 'Thời gian hỏi',
            'is_approved' => 'Đã duyệt',
            'question_content' => 'Câu hỏi',
            'answer_content' => 'Trả lời',
            'is_on_behalf_of_teacher' => 'Thay mặt giảng viên'
        ];
    }

    public function scenarios()
    {
        $scenarios = parent::scenarios();

        $scenarios[self::SCENARIO_APPROVE] = ['is_approved', 'is_send_approved', 'is_send_unapproved'];

        return $scenarios;
    }
    
    public static function find()
    {
        $query = parent::find();
        
        if (isset(Yii::$app->user) && Yii::$app->user->can('TeachingAssistant')) {
            $query->joinWith('course.teachingAssistants as ta');
            $query->andWhere(['ta.teaching_assistant_id' => Yii::$app->user->id]);
        }
        
        return $query;
    }

    public function beforeSave($insert)
    {
        if ($insert && empty($this->posted_time)) {
            $this->posted_time = time();
        }
        if (empty($this->user_id)) {
            $this->user_id = Yii::$app->user->id;
        }

        return parent::beforeSave($insert);
    }

    public function afterSave($insert, $changedAttributes)
    {
        $ret = parent::afterSave($insert, $changedAttributes);

        switch ($this->scenario) {
            case self::SCENARIO_APPROVE:
                $this->trigger(self::EVENT_APPROVED);
                break;

            default:
                break;
        }

        return $ret;
    }

    public function getPostedTime()
    {
        return \Yii::$app->formatter->asRelativeTime($this->posted_time);
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
    
    public function getAnswer()
    {
        return $this->hasOne(self::className(), ['question_id' => 'id']);
    }

    public function getAnswers()
    {
        return $this->hasMany(self::className(), ['question_id' => 'id']);
    }

    public function getHasAnswer()
    {
        return $this->getAnswer()->exists();
    }
    
    public function getCourse()
    {
        return $this->hasOne(Course::className(), ['id' => 'course_id']);
    }

    public function getTeacher()
    {
        return $this->hasOne(Teacher::className(), ['id' => 'teacher_id'])->via('course');
    }

    public function getTeacherReceiveDateSetting()
    {
        return $this->hasOne(TeacherSetting::className(), ['teacher_id' => 'teacher_id'])->via('course')->andWhere([
            'st.`key`' => TeacherSetting::KEY_RECEIVE_MAIL_DATE
        ]);
    }
    
    public function getQuestion()
    {
        return $this->hasOne(self::className(), ['id' => 'question_id']);
    }
    
    public function getQaLink()
    {
        $frontendUrl = str_replace('dashboard.', '', Yii::$app->request->hostInfo);
        
        return $frontendUrl . Url::toRoute(['/course/learning/index', 'id' => $this->course_id, 'question_id' => $this->id]);
    }

    public function afterApprove()
    {
        if ($this->is_approved == self::BOOL_YES) {
            if ($this->is_send_to_teacher == self::BOOL_NO) {
                // approved and not send to teacher
                $teacher = Teacher::findOne($this->course->teacher_id);

                if ($teacher != null) {
                    $receiveDate = $teacher->getReceiveQnaDate();

                    if ($receiveDate == null) {
                        $template = '@common/mail/qa/email_teacher';
                        $subject = "[Kyna.vn] Câu hỏi #{$this->id} từ học viên";
                        // get setting cc email
                        $settings = ArrayHelper::map(Setting::find()->all(), 'key', 'value');
                        $fromEmail = !empty($settings['email_teacher_from']) ? $settings['email_teacher_from'] : 'hotro@kyna.vn';
                        $ccEmails = !empty($settings['email_teacher_cc']) ? $settings['email_teacher_cc'] : 'hotro@kyna.vn';
                        $ccEmails = explode(',', $ccEmails);

                        $sendResult = Yii::$app->mailer->compose()
                            ->setHtmlBody(Yii::$app->controller->renderPartial($template, [
                                'questionId' => $this->id,
                                'studentName' => $this->user->profile->name,
                                'courseName' => $this->course->name,
                                'questionContent' => strip_tags($this->content),
                                'total_question' => 1,
                                'gender' => 'Thầy/Cô',
                                'questionDate' => \Yii::$app->formatter->asDatetime($this->posted_time, "php:d-m-Y  H:i:s"),
                                'fullName' => $teacher->profile->name
                            ]))
                            ->setFrom($fromEmail)
                            ->setTo($teacher->email)
                            ->setCc($ccEmails)
                            ->setSubject($subject)
                            ->send();

                        if ($sendResult) {
                            self::updateAll(['is_send_to_teacher' => self::BOOL_YES], ['id' => $this->id]);
                        }
                    }
                }
            }

            // find missions and added point to user
            $missions = Mission::findMissionsByQnaApproved($this->course_id, $this->user_id);

            foreach ($missions as $mission) {
                /* @var $mission Mission */
                $approvedQnaQuery = self::find()
                    ->alias('qna')
                    ->where([
                        'qna.user_id' => $this->user_id,
                        'qna.status' => self::BOOL_YES
                    ])
                    ->groupBy('qna.id');

                switch ($mission->apply_course_type) {
                    case Mission::APPLY_COURSE_TYPE_SOME:
                        $courseIds = $mission->getMissionCourses()->select(['course_id'])->column();

                        $approvedQnaQuery->andWhere([
                            'qna.course_id' => $courseIds
                        ]);
                        break;

                    case Mission::APPLY_COURSE_TYPE_CATEGORY:
                        $categoryIds = $mission->getMissionCategories()->select(['category_id'])->column();

                        $approvedQnaQuery->joinWith('courses c');
                        $approvedQnaQuery->leftJoin('categories', 'categories.id = c.category_id');

                        $approvedQnaQuery->andWhere([
                            'OR',
                            ['c.category_id' => $categoryIds],
                            ['categories.parent_id' => $categoryIds],
                        ]);

                        break;

                    default:
                        break;
                }

                $conditions = $mission->missionConditions;
                $validMission = true;

                /* @var $condition MissionCondition */
                foreach ($conditions as $condition) {
                    $minValue = $condition->min_value;
                    $isValid = false;

                    if ($minValue > 1) {
                        $totalApprovedQna = $approvedQnaQuery->count();
                        if ($totalApprovedQna > 0 && $totalApprovedQna % $minValue == 0) {
                            $isValid = true;
                        }
                    } else {
                        $isValid = true;
                    }

                    if ($mission->condition_operator == 'AND') {
                        $validMission = $validMission && $isValid;
                    } elseif ($mission->condition_operator == 'OR') {
                        $validMission = $validMission || $isValid;
                        if ($validMission) {
                            break;
                        }
                    }
                }


                if ($validMission) {
                    $point = $mission->k_point;
                    if ($mission->k_point_for_free_course != null && $this->course->sellPrice == 0) {
                        $point = $mission->k_point_for_free_course;
                    }

                    Mission::addPointByMission($this->user_id, $point, $mission->id, $this->id, $this->course_id);
                }
            }
        }
    }

}
