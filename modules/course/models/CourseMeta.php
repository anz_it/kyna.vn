<?php

namespace kyna\course\models;

use Yii;
use kyna\base\models\MetaField;

/**
 * This is the model class for table "course_custom_field_values".
 *
 * @property integer $id
 * @property integer $course_id
 * @property integer $meta_field_id
 * @property string $value
 */
class CourseMeta extends \kyna\base\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'course_meta';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['meta_field_id'], 'required'],
            [['value'], 'required', 'on' => 'required', 'message' => $this->metaField->name . ' không được để trống.'],
            [['course_id', 'meta_field_id'], 'integer'],
            [['value', 'key'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'course_id' => 'Course ID',
            'meta_field_id' => 'Custom Field ID',
            'value' => 'Value',
            'key' => 'Key',
        ];
    }
    
    /**
     * @desc get relation MetaField
     * @return BEMetaField model
     */
    public function getMetaField()
    {
        return MetaField::find()->where(['model' => MetaField::MODEL_COURSE, 'key' => $this->key])->one();
//        return $this->hasOne(MetaField::className(),['id' => 'meta_field_id']);
    }
}
