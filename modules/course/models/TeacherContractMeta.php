<?php

namespace kyna\course\models;

use Yii;

/**
 * This is the model class for table "{{%teacher_contract_meta}}".
 *
 * @property integer $id
 * @property integer $teacher_contract_id
 * @property string $key
 * @property string $value
 *
 * @property TeacherContracts $contract
 */
class TeacherContractMeta extends \kyna\base\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%teacher_contract_meta}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['teacher_contract_id', 'value'], 'required'],
            [['teacher_contract_id'], 'integer'],
            [['value'], 'string'],
            [['key'], 'string', 'max' => 255],
            [['teacher_contract_id'], 'exist', 'skipOnError' => true, 'targetClass' => TeacherContracts::className(), 'targetAttribute' => ['teacher_contract_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'teacher_contract_id' => Yii::t('app', 'Contract ID'),
            'key' => Yii::t('app', 'Key'),
            'value' => Yii::t('app', 'Value'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContract()
    {
        return $this->hasOne(TeacherContracts::className(), ['id' => 'teacher_contract_id']);
    }
}
