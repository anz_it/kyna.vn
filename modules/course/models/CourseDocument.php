<?php

namespace kyna\course\models;

use Yii;
use common\helpers\DocumentHelper;
use common\helpers\CDNHelper;

/**
 * This is the model class for table "course_documents".
 *
 * @property integer $id
 * @property string $title
 * @property string $save_path
 * @property integer $course_id
 * @property string $file_ext
 * @property integer $size
 */
class CourseDocument extends \kyna\base\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'course_documents';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['course_id'], 'integer'],
            [['title', 'save_path'], 'string', 'max' => 255],
            [['file_ext'], 'string', 'max' => 3],
            [['size'], 'integer'],
            [['mime_type'], 'string', 'max' => 127],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'save_path' => 'Saved Path',
            'course_id' => 'Course',
            'file_ext' => 'Extension',
            'size' => 'File Size',
            'mime_type' => 'MIME Type',
        ];
    }

    public function getCourse()
    {
        return $this->hasOne(Course::className(), ['id' => 'course_id']);
    }

    public function validateHash($queryParams)
    {
        if (!isset($queryParams['hash'])) {
            return false;
        }
        $hash = $this->getHash($queryParams);
        return $hash == $queryParams['hash'];
    }

    public function getHash($queryParams)
    {
        $hash = DocumentHelper::hashParams($queryParams);
        return $hash;
    }

    public function getDocLink()
    {
        return CDNHelper::getMediaLink() . $this->save_path;
    }
}
