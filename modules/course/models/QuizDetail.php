<?php

namespace kyna\course\models;

use kotchuprik\sortable\behaviors\Sortable;

/**
 * This is the model class for table "course_quiz_questions".
 *
 * @property int $id
 * @property int $quiz_id
 * @property int $quiz_question_id
 * @property string $order
 * @property int $status
 * @property int $coefficient
 * @property bool $is_deleted
 * @property int $created_time
 * @property int $updated_time
 * @property QuizQuestion $quizQuestion
 * @property Quiz $quiz
 * @property int parent_id
 */
class QuizDetail extends \kyna\base\ActiveRecord
{
    public $isRandom = false;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'quiz_details';
    }

    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['sortable'] = [
            'class' => Sortable::className(),
            'query' => self::find(),
        ];

        return $behaviors;
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['quiz_id', 'quiz_question_id'], 'required'],
            [['quiz_id', 'quiz_question_id', 'status', 'created_time', 'updated_time', 'order', 'coefficient', 'parent_id'], 'integer'],
            [['is_deleted'], 'boolean'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'quiz_id' => 'Quiz',
            'quiz_question_id' => 'Câu hỏi',
            'order' => 'Order',
            'status' => 'Status',
            'is_deleted' => 'Is Deleted',
            'created_time' => 'Created Time',
            'updated_time' => 'Updated Time',
        ];
    }
    
    public function getQuizQuestion()
    {
        return $this->hasOne(QuizQuestion::className(), ['id' => 'quiz_question_id']);
    }
    
    public function getQuiz()
    {
        return $this->hasOne(Quiz::className(), ['id' => 'quiz_id']);
    }

    
/*
    public static function getQuestionIds($quizId)
    {
        $model = new static();
        $model->quiz_id = $quizId;

        return $model->getQuestions()->select('id')->column();
    }

*/
}
