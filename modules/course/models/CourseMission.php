<?php

namespace kyna\course\models;

use Yii;

/**
 * This is the model class for table "{{%course_missions}}".
 *
 * @property integer $id
 * @property integer $course_id
 * @property integer $section_id
 * @property integer $lesson_id
 * @property string $name
 * @property string $type
 * @property integer $day_can_do
 * @property integer $max_executing_day
 * @property integer $bonus_delta_xu
 * @property string $description
 * @property boolean $is_deleted
 * @property integer $status
 * @property integer $created_time
 * @property integer $updated_time
 */
class CourseMission extends \kyna\base\ActiveRecord
{
    
    const TYPE_QUIZ = 'Quiz';
    const TYPE_READING = 'Reading';
    const TYPE_GRADUATION = 'Graduation';
    const TYPE_SHARING = 'Sharing';
    const TYPE_OTHER = 'Other';
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%course_missions}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['course_id', 'name', 'type'], 'required'],
            [['course_id', 'section_id', 'lesson_id', 'day_can_do', 'max_executing_day', 'bonus_delta_xu', 'status', 'created_time', 'updated_time'], 'integer'],
            [['description'], 'string'],
            [['name'], 'string', 'max' => 255],
            [['type'], 'string', 'max' => 45],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'course_id' => 'Course ID',
            'section_id' => 'Section ID',
            'lesson_id' => 'Lesson ID',
            'name' => 'Name',
            'type' => 'Type',
            'day_can_do' => 'Day Can Do',
            'max_executing_day' => 'Max Executing Day',
            'bonus_delta_xu' => 'Bonus Delta Xu',
            'description' => 'Description',
            'status' => 'Status',
            'created_time' => 'Created Time',
            'updated_time' => 'Updated Time',
        ];
    }
}
