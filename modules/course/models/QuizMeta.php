<?php

namespace kyna\course\models;

use Yii;
use kyna\base\models\MetaField;

/**
 * This is the model class for table "course_quiz_meta".
 *
 * @property integer $id
 * @property integer $course_id
 * @property integer $quiz_id
 * @property integer $meta_field_id
 * @property string $key
 * @property string $value
 */
class QuizMeta extends \kyna\base\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'quiz_meta';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['course_id', 'quiz_id', 'key'], 'required'],
            [['course_id', 'quiz_id', 'meta_field_id'], 'integer'],
            [['value'], 'required', 'on' => 'required', 'message' => $this->metaField->name . ' không được để trống.'],
            ['value', 'safe'],
            [['key'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'course_id' => 'Course ID',
            'quiz_id' => 'Quiz ID',
            'meta_field_id' => 'Custom Field ID',
            'key' => 'Key',
            'value' => 'Value',
        ];
    }

    /**
     * @desc get relation MetaField
     * @return BEMetaField model
     */
    public function getMetaField()
    {
        return $this->hasOne(MetaField::className(),['id' => 'meta_field_id']);
    }
}
