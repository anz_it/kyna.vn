<?php

namespace kyna\course\traits;

use Yii;
use yii\web\NotFoundHttpException;
use yii\helpers\Url;
use kyna\user\models\UserCourse;
use common\helpers\DocumentHelper;

trait CertificateControllerTrait
{
    public $imageSaveDir = '@upload/user/{user_id}/certificates';
    public $imageBaseUrl = '/uploads';

    public function actionIndex($courseId, $userId)
    {
        /**
         * @var $model UserCourse
         */
        $model = $this->getLearner($courseId, $userId);

        $certNumber = $model->certificateNumber;

        $user = $model->student;
        $userName = $user->profile->name;

        $temp = explode(' ', $user->birthday);
        $birthdate = false;
        if (!empty($temp[0])) {
            $date = date_create_from_format('Y-m-d', $temp[0]);

            if ($date === false) {
                $date = date_create_from_format('d-m-Y', $temp[0]);
            }

            if ($date !== false) {
                $birthdate = $date->format('d-m-Y');
            }
        }
        if ($birthdate == false) {
            return false;
        }

        $course = $model->course;
        $courseName = $course->name;

        $saveFile = $this->getSaveFile($courseId, $userId);

        $type = null;
        if ($model->process >= $course->percent_can_be_excellent) {
            $type = 'Xuất sắc';
        }

        $generator = $this->getGenerator($saveFile);
        $generator->generate($certNumber, $userName, $birthdate, $courseName, $type);

        $queryParams = [
            'courseId' => $courseId,
            'userId' => $userId
        ];
        $hash = DocumentHelper::hashParams($queryParams);

        $url = str_replace(Yii::getAlias('@upload'), $this->imageBaseUrl, $saveFile);
        return $this->render('index', [
            'url' => Url::to($url, true),
            'certNumber' => $certNumber,
            'shareUrl' => Url::toRoute(['/course/certificate/index', 'courseId' => $courseId, 'userId' => $userId, 'hash' => $hash], true),
            'userName' => $userName,
            'courseName' => $courseName,
        ]);
    }

    protected function getGenerator($saveFile)
    {
        return Yii::createObject([
            'class' => '\kyna\course\lib\CertificateImage',
            'saveFile' => $saveFile,
        ]);
    }

    protected function getLearner($courseId, $userId)
    {
        $userCourse = UserCourse::find()->where([
            'user_id' => $userId,
            'course_id' => $courseId,
            'is_graduated' => true,
        ])->one();

        if ($userCourse == null) {
            throw new NotFoundHttpException();
        }

        return $userCourse;
    }

    protected function getSaveFile($courseId, $userId)
    {
        $saveDir = Yii::getAlias($this->imageSaveDir);
        $saveDir = str_replace('{user_id}', $userId, $saveDir);
        if (!file_exists($saveDir) && !is_dir($saveDir)) {
            mkdir($saveDir, 0755, true);
        }

        return $saveDir . '/' . $courseId . '.jpg';
    }

}
