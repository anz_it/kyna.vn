<?php

use yii\helpers\Html;
use common\components\GridView;
use yii\widgets\Pjax;
use kyna\otp\models\OtpTransaction;

/* @var $this yii\web\View */
/* @var $searchModel kyna\otp\models\OtpTransactionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Otp Transaction');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="">
    <div class="box">
        <div class="box-body">
            <?php Pjax::begin(['timeout' => Yii::$app->params['timeOutPjax'], 'id' => 'idPjaxReload']) ?>
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => array_merge(
                    [['class' => 'yii\grid\SerialColumn']],
                    [
                        'id',
                        [
                            'attribute' => 'activation_code',
                            'value' => function ($model) {
                                return !empty($model->otp) ? $model->otp->activation_code : null;
                            }
                        ],
                        [
                            'attribute' => 'otp_code',
                            'value' => function ($model) {
                                return !empty($model->otp) ? $model->otp->otp_code : null;
                            }
                        ],
                        'response',
                        [
                            'attribute' => 'status',
                            'format' => 'raw',
                            'value' => function ($model) {
                                return $model->statusHtml;
                            },
                            'contentOptions' => [
                                'style' => 'min-width: 115px;'
                            ],
                            'filter' => Html::activeDropDownList($searchModel, 'status', OtpTransaction::listStatus(), ['class' => 'form-control', 'prompt' => Yii::t('app', '-- Select --')]),
                        ],
                    ]
                )
            ]); ?>
            <?php Pjax::end() ?>
        </div>
    </div>
</div>
