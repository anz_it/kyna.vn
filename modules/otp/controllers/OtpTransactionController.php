<?php

namespace kyna\otp\controllers;

use Yii;
use kyna\otp\models\OtpTransaction;
use kyna\otp\models\OtpTransactionSearch;
use yii\filters\AccessControl;
use kyna\base\controllers\admin\BaseController as Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * OtpTransactionController implements the CRUD actions for OtpTransaction model.
 */
class OtpTransactionController extends Controller
{
    public $modelClass = 'kyna\otp\models\OtpTransaction';
    public $modelSearchClass = 'kyna\otp\models\OtpTransactionSearch';
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index'],
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->user->can('Otp.Transaction');
                        }
                    ]
                ],
            ],
        ];
    }

    protected function getRedirectPage($action, $model = null)
    {
        switch ($action) {
            case 'update':
                return ['update', 'id' => $model->id];
                break;
            case 'create':
                return ['update', 'id' => $model->id];
                break;
            default:
                return parent::getRedirectPage($action, $model);
        }
    }
}
