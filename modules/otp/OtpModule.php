<?php
/**
 * Created by IntelliJ IDEA.
 * User: trangnguyen
 * Date: 3/26/2018
 * Time: 11:22 AM
 */

namespace kyna\otp;

use yii\filters\AccessControl;

/*
 * This is Otp module.
 */
class OtpModule extends \yii\base\Module
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => false,
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public $layout = '@app/views/layouts/lte';

}