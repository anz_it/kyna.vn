<?php

use yii\db\Migration;

class m180327_024743_create_otp_structure extends Migration
{
    const OTP_TABLE = '{{%otp}}';
    const OTP_TRANSACTION_TABLE = '{{%otp_transaction}}';

    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable(self::OTP_TABLE, [
            'id' => $this->primaryKey(11),
            'otp_code' => $this->string(20)->unique(),
            'activation_code' => $this->string(50),
            'phone' => $this->string(20),
            'is_used' => $this->smallInteger(1)->defaultValue(0),
            'status' => $this->smallInteger(1)->defaultValue(0),
            'created_time' => $this->integer(11)->defaultValue(0),
            'updated_time' => $this->integer()->defaultValue(0),
        ], $tableOptions);

        $this->createTable(self::OTP_TRANSACTION_TABLE, [
            'id' => $this->primaryKey(11),
            'otp_id' => $this->integer(11)->notNull(),
            'response' => $this->text(),
            'status' => $this->smallInteger(2)->defaultValue(0),
            'created_time' => $this->integer(10)->defaultValue(0),
            'updated_time' => $this->integer(10)->defaultValue(0),
        ]);

        $this->createIndex('idx_otp_activation_code', self::OTP_TABLE, 'activation_code');
        $this->createIndex('idx_otp_id', self::OTP_TRANSACTION_TABLE, 'otp_id');

        // create permission
        $otpTransaction = Yii::$app->authManager->createPermission('Otp.Transaction');
        Yii::$app->authManager->add($otpTransaction);
        Yii::$app->authManager->addChild(Yii::$app->authManager->getRole('Dev'), $otpTransaction);
        Yii::$app->authManager->addChild(Yii::$app->authManager->getRole('Admin'), $otpTransaction);
    }

    public function safeDown()
    {
        // remove permission
        $otpTransaction = Yii::$app->authManager->getPermission('Otp.Transaction');
        Yii::$app->authManager->remove($otpTransaction);
        Yii::$app->authManager->removeChild(Yii::$app->authManager->getRole('Admin'), $otpTransaction);
        Yii::$app->authManager->removeChild(Yii::$app->authManager->getRole('Dev'), $otpTransaction);

        $this->dropTable('otp_transaction');
        $this->dropTable('otp');
    }
}
