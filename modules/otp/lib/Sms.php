<?php
/**
 * Created by IntelliJ IDEA.
 * User: trangnguyen
 * Date: 3/26/2018
 * Time: 11:23 AM
 */

namespace kyna\otp\lib;

use Yii;
use kyna\order\models\Order;
use kyna\servicecaller\traits\CurlTrait;
use kyna\servicecaller\BaseCaller;
use kyna\otp\models\OtpTransaction;

class Sms extends BaseCaller
{

    use CurlTrait;

    private $_apiUrl = 'https://cloudsms.vietguys.biz:4438/api/index.php';
    private $_apiUser = 'kyna.vn';
    private $_apiSecretKey = 'xuucr';
    private $_brandName = 'KYNA.VN';

    protected static $defaultMethod = 'POST';

    public function signIn()
    {
        // TODO: Implement signIn() method.
    }

    protected function beforeCall(&$ch, &$data)
    {
        $headers = array();
        $headers[] = 'Content-Type: application/x-www-form-urlencoded';
        $data = http_build_query($data);

        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    }

    public function __construct()
    {
        if (isset(Yii::$app->params['sms']) && $configs = Yii::$app->params['sms']) {
            $this->_apiUrl = $configs['_apiUrl'];
            $this->_apiUser = $configs['_apiUser'];
            $this->_apiSecretKey = $configs['_apiSecretKey'];
            $this->_brandName = $configs['_brandName'];
        }
    }

    public function send($phone, $content, $refId)
    {
        $data = [
            "u" => $this->_apiUser,
            "pwd" => $this->_apiSecretKey,
            "from" => $this->_brandName,
            "phone" => $phone,
            "sms" => $content
        ];
        $command = '';
        $endpoint = $this->_buildUrl($command);
        try {
            $result = $this->call($endpoint, $data);
        } catch (\Exception $e) {
            $result = OtpTransaction::STATUS_NOT_RESPONSE;
        };
        return $this->_return($result, $refId, $data);
    }

    private function _buildUrl($command, $params = false)
    {
        $url = $this->_apiUrl . $command;
        if ($params) {
            $url .= '?' . http_build_query($params);
        }

        return $url;
    }

    /**
     * @param $response (status, message)
     * @param $refId
     * @param $data
     * @return boolean
     */
    private function _return($response, $refId, $data)
    {
        // save transaction
        $return = false;
        $status = in_array(intval($response), array_keys(OtpTransaction::listStatus())) ? intval($response) : OtpTransaction::STATUS_COMPLETE;
        $transaction = new OtpTransaction();
        $data['OtpTransaction'] = [
            'otp_id' => $refId,
            'response' => $response,
            'status' => $status,
            'created_time' => time(),
            'updated_time' => time()
        ];
        if ($transaction->load($data) && $transaction->save(false)) {
            $return = $transaction->status == OtpTransaction::STATUS_COMPLETE ? true : false;
        }
        return $return;
    }
}