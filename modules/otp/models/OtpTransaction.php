<?php

namespace kyna\otp\models;

use Yii;

/**
 * This is the model class for table "{{%otp_transaction}}".
 *
 * @property integer $id
 * @property integer $otp_id
 * @property string $response
 * @property integer $status
 * @property integer $created_time
 * @property integer $updated_time
 */
class OtpTransaction extends \kyna\base\ActiveRecord
{
    const STATUS_NOT_RESPONSE = 0;
    const STATUS_INVALID_PARAMS = -1;
    const STATUS_SERVER_BUSY = -2;
    const STATUS_INVALID_USER = -3;
    const STATUS_BLOCKED_USER = -4;
    const STATUS_INVALID_PASS = -5;
    const STATUS_DISABLED_API = -6;
    const STATUS_LIMITED_IP = -7;
    const STATUS_INVALID_BRANDNAME = -8;
    const STATUS_OUT_OF_USER_CREDIT = -9;
    const STATUS_INVALID_PHONE = -10;
    const STATUS_REFUSED_PHONE = -11;
    const STATUS_OUT_OF_CREDIT = -12;
    const STATUS_EMPTY_BRANDNAME = -13;
    const STATUS_LIMITED_CHARACTER = -14;
    const STATUS_DUPLICATE_MINUTE = -16;
    const STATUS_COMPLETE = 1;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%otp_transaction}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['otp_id'], 'required'],
            [['otp_id', 'status', 'created_time', 'updated_time'], 'integer'],
            [['response'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'otp_id' => Yii::t('app', 'Otp ID'),
            'response' => Yii::t('app', 'Response'),
            'status' => Yii::t('app', 'Status'),
            'created_time' => Yii::t('app', 'Created Time'),
            'updated_time' => Yii::t('app', 'Updated Time'),
        ];
    }

    public function getOtp()
    {
        return $this->hasOne(Otp::className(), ['id' => 'otp_id']);
    }

    public static function listStatus()
    {
        $list = [
            self::STATUS_NOT_RESPONSE => 'Không phản hồi',
            self::STATUS_INVALID_PARAMS => 'Chưa truyền đầy đủ tham	số',
            self::STATUS_SERVER_BUSY => 'Máy chủ đang bận',
            self::STATUS_INVALID_USER => 'Không tìm thấy tài khoản người dùng',
            self::STATUS_BLOCKED_USER => 'Tài khoản bị xóa',
            self::STATUS_INVALID_PASS => 'Thông tin xác thực chưa chình xác',
            self::STATUS_DISABLED_API => 'Chưa kích hoạt tính năng gửi qua API',
            self::STATUS_LIMITED_IP => 'IP bị giới hạn truy cập',
            self::STATUS_INVALID_BRANDNAME => 'Tên thương hiện chưa khai báo',
            self::STATUS_OUT_OF_USER_CREDIT => 'Tài khoản hết credits gửi tin',
            self::STATUS_INVALID_PHONE => 'Số điện thoại chưa chính xác',
            self::STATUS_REFUSED_PHONE => 'Số điện thoại nằm trong danh sách từ chối nhận tin',
            self::STATUS_OUT_OF_CREDIT => 'Hết credits gửi tin',
            self::STATUS_EMPTY_BRANDNAME => 'Tên thương hiệu chưa khai báo',
            self::STATUS_LIMITED_CHARACTER => 'Số kí tự vượt quá 459 kí tự (lỗi tin nhắn dài)',
            self::STATUS_DUPLICATE_MINUTE => 'Gửi trùng số điện thoại, thương hiệu, nội dung trong 01 phút',
            self::STATUS_COMPLETE => 'Gửi thành công'
        ];

        return $list;
    }

    public function getStatusHtml() // TODO: move to trait
    {
        switch ($this->status) {
            case self::STATUS_COMPLETE:
                $labelClass = 'label-success';
                break;
            case self::STATUS_NOT_RESPONSE:
                $labelClass = 'label-default';
                break;
            default:
                $labelClass = 'label-danger';
                break;
        }

        return '<span class="label ' . $labelClass . '">' . $this->statusText . '</span>';
    }
}
