<?php

namespace kyna\otp\models;

use Yii;
use common\helpers\StringHelper;

/**
 * This is the model class for table "{{%otp}}".
 *
 * @property integer $id
 * @property string $otp_code
 * @property string $activation_code
 * @property string $phone
 * @property integer $is_used
 * @property integer $status
 * @property integer $created_time
 * @property integer $updated_time
 */
class Otp extends \kyna\base\ActiveRecord
{
    const OTP_CODE_LENGTH = 6;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%otp}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['is_used', 'status', 'created_time', 'updated_time'], 'integer'],
            [['otp_code', 'phone'], 'string', 'max' => 20],
            [['activation_code'], 'string', 'max' => 50],
            [['otp_code'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'otp_code' => Yii::t('app', 'Otp Code'),
            'activation_code' => Yii::t('app', 'Activation Code'),
            'phone' => Yii::t('app', 'Phone'),
            'is_used' => Yii::t('app', 'Is Used'),
            'status' => Yii::t('app', 'Status'),
            'created_time' => Yii::t('app', 'Created Time'),
            'updated_time' => Yii::t('app', 'Updated Time'),
        ];
    }

    /**
     * @param $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        if ($this->isNewRecord) {
            if (empty($this->otp_code)) {
                $this->otp_code = self::generateOtpCode(self::OTP_CODE_LENGTH);
            }
        }
        return parent::beforeSave($insert);
    }

    /**
     * @desc generate random unique code for otp
     *
     * @param int $codeLen
     *
     * @return string string
     */
    public static function generateOtpCode($codeLen = self::OTP_CODE_LENGTH)
    {
        $code = StringHelper::randomNumber($codeLen);
        if (empty(self::checkUnique($code))) {
            return $code;
        }
        return StringHelper::randomNumber($codeLen);
    }

    /**
     * @param string $code
     * @return bool
     */
    public static function checkUnique($code)
    {
        return self::find()->where(['otp_code' => $code])->exists();
    }
}
