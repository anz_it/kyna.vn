<?php

namespace kyna\otp\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use kyna\otp\models\OtpTransaction;

/**
 * OtpTransactionSearch represents the model behind the search form about `kyna\otp\models\OtpTransaction`.
 */
class OtpTransactionSearch extends OtpTransaction
{
    public $otp_code;
    public $activation_code;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'otp_id', 'status', 'created_time', 'updated_time'], 'integer'],
            [['response', 'otp_code', 'activation_code'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = OtpTransaction::find()->alias('ot');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'ot.id' => $this->id,
            'ot.otp_id' => $this->otp_id,
            'ot.status' => $this->status,
            'ot.created_time' => $this->created_time,
            'ot.updated_time' => $this->updated_time,
        ]);

        if (!empty($this->otp_code) || !empty($this->activation_code)) {
            $otpTblName = Otp::tableName();
            $query->join('INNER JOIN', $otpTblName, "{$otpTblName}.id = ot.otp_id");
            if (!empty($this->otp_code)) {
                $query->andFilterWhere(['like', "{$otpTblName}.otp_code", $this->otp_code]);
            }
            if (!empty($this->activation_code)) {
                $query->andFilterWhere(['like', "{$otpTblName}.activation_code", $this->activation_code]);
            }
        }

        $query->andFilterWhere(['like', 'ot.response', $this->response]);

        return $dataProvider;
    }
}
