<?php

namespace app\modules\telesale;

class TelesaleModule extends \app\components\BEModule
{
    public $controllerNamespace = 'app\modules\telesale\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
