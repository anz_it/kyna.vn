<?php

use yii\helpers\Html;
use yii\grid\GridView;
use kartik\select2\Select2;

$crudTitles = Yii::$app->params['crudTitles'];

$this->title = Yii::$app->controller->mainTitle;
$this->params['breadcrumbs'][] = $this->title;

$cols = [
    ['class' => 'yii\grid\SerialColumn'],
    'start_time',
    'end_time',
];

foreach ($weekDays as $day) {
    $col = [
        'attribute' => $day,
        'format' => 'html',
        'value' => function ($model, $key, $index, $column) {
            $result = '';
            
            foreach ($model->{$column->attribute} as $userId) {
                if (!empty($userId)) {
                    $result .= \common\models\User::find()->where(['id' => $userId])->with('profile')->one()->profile->name . '<br>';
                }
            }
            
            return $result;
        }
    ];
    
    $cols[] = $col;
}
$cols[] = 'user_type';
$cols[] = [
    'class' => 'yii\grid\ActionColumn',
    'template' => '{update}'
];
?>
<nav class="navbar navbar-default navbar-static-top">
        <div class="container-fluid">
            <?= $this->render('_search', ['searchModel' => $searchModel]) ?>
            <ul class="navbar-nav nav navbar-right">
                <li><?= Html::a('<i class="fa fa-plus"></i> ' . $crudTitles['create'] . ' Time slot', ['create']) ?></li>
            </ul>
        </div>

    </nav>
<div class="row">
    <div class="col-xs-12">
        <div class="betime-slot-index">
            <div class="box">
                <?=
                GridView::widget([
                    'dataProvider' => $dataProvider,
//                    'filterModel' => $searchModel,
                    'columns' => $cols,
                ]);
                ?>
            </div>
        </div>
    </div>
</div>