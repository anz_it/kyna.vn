<?php

use yii\helpers\Html;
use yii\helpers\Url;
use kartik\select2\Select2;
use common\models\User;

$crudTitles = Yii::$app->params['crudTitles'];
$users = User::getUsersByRole(Yii::$app->controller->roleTelesale);
?>

<div class="betime-slot-search">
    <?= Html::beginForm(Url::toRoute(['/telesale/time-slot']), 'get', [
                'class'  => 'navbar-form navbar-left',
                'role' => 'search',
                'id' => 'form-search'
            ]) ?>
            <div class="form-group">
                <?= Select2::widget([
                    'model' => $searchModel,
                    'attribute' => 'user_type',
                    'data' => \app\models\BETimeSlot::getUserTypes(),
                    'options' => [
                        'placeholder' => '--Chọn User Type--',
                        'onchange' => "$('#form-search').submit();",
                    ],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]) ?>
            </div>
        <?= Html::endForm(); ?>
</div>
