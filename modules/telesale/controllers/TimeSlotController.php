<?php

namespace app\modules\telesale\controllers;

use Yii;
use kyna\base\models\TimeSlot;
use kyna\base\models\search\TimeSlotSearch;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\models\User;

/**
 * TimeSlotController implements the CRUD actions for TimeSlot model.
 */
class TimeSlotController extends \app\components\controllers\Controller
{

    public $roleTelesale = 'Telesale';
    public $mainTitle = 'Time slot';
    
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all TimeSlot models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TimeSlotSearch();
        $searchModel->type = TimeSlot::TYPE_TELESALE;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $weekDays = TimeSlot::getDaysOfWeek();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'weekDays' => $weekDays,
        ]);
    }

    /**
     * Creates a new TimeSlot model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new TimeSlot();
        
        $model->type = TimeSlot::TYPE_TELESALE;
        
        $users = User::getUsersByRole($this->roleTelesale);
        $weekDays = TimeSlot::getDaysOfWeek();

        if ($model->load(Yii::$app->request->post())) {
            foreach ($weekDays as $day) {
                if (is_array($model->$day)) {
                    $model->$day = implode(',', $model->$day);
                }
            }
            if ($model->save()) {
                return $this->redirect(['index']);
            }
        } else {
            return $this->render('create', [
                'model' => $model,
                'users' => $users,
                'weekDays' => $weekDays
            ]);
        }
    }

    /**
     * Updates an existing TimeSlot model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $oldUserType = $model->user_type;
        $users = User::getUsersByRole($this->roleTelesale);
        $weekDays = TimeSlot::getDaysOfWeek();

        if ($model->load(Yii::$app->request->post())) {
            foreach ($weekDays as $day) {
                if (is_array($model->$day)) {
                    $model->$day = implode(',', $model->$day);
                }
            }
            if ($model->validate()) {
                if ($oldUserType == TimeSlot::USER_TYPE_DEFAULT && $model->user_type != $oldUserType) {
                    $newModel = new TimeSlot();
                    $newModel->attributes = $model->attributes;
                    $newModel->related_id = $model->id;
                    if ($newModel->save(false)) {
                        return $this->redirect(['index']);
                    }
                }
                if ($model->save(false)) {
                    return $this->redirect(['index']);
                }
            }
        } else {
            return $this->render('update', [
                'model' => $model,
                'users' => $users,
                'weekDays' => $weekDays
            ]);
        }
    }

    /**
     * Finds the TimeSlot model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return TimeSlot the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = TimeSlot::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
