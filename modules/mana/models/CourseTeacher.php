<?php

namespace kyna\mana\models;

use Yii;

/**
 * This is the model class for table "{{%mana_course_teachers}}".
 *
 * @property integer $id
 * @property string $course_id
 * @property string $teacher_id
 * @property integer $created_time
 * @property integer $updated_time
 */
class CourseTeacher extends \kyna\base\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%mana_course_teachers}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['course_id', 'teacher_id'], 'required'],
            [['course_id', 'teacher_id', 'created_time', 'updated_time', 'v2_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'course_id' => 'Course ID',
            'teacher_id' => 'Teacher ID',
            'created_time' => 'Created Time',
            'updated_time' => 'Updated Time',
        ];
    }
}
