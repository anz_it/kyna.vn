<?php

namespace kyna\mana\models;

use Yii;

/**
 * This is the model class for table "{{%mana_course_subjects}}".
 *
 * @property integer $id
 * @property string $course_id
 * @property string $subject_id
 * @property integer $created_time
 * @property integer $updated_time
 */
class CourseSubject extends \kyna\base\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%mana_course_subjects}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['course_id', 'subject_id'], 'required'],
            [['course_id', 'subject_id', 'created_time', 'updated_time', 'v2_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'course_id' => 'Course ID',
            'subject_id' => 'Subject ID',
            'created_time' => 'Created Time',
            'updated_time' => 'Updated Time',
        ];
    }
}
