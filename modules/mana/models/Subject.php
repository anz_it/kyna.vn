<?php

namespace kyna\mana\models;

use Yii;

/**
 * This is the model class for table "{{%mana_subjects}}".
 *
 * @property integer $id
 * @property string $parent_id
 * @property string $name
 * @property string $slug
 * @property string $description
 * @property string $image_url
 * @property integer $order
 * @property integer $status
 * @property integer $created_time
 * @property integer $updated_time
 */
class Subject extends \kyna\base\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%mana_subjects}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['parent_id', 'order', 'status', 'created_time', 'updated_time', 'v2_id'], 'integer'],
            [['name', 'slug'], 'required'],
            [['description', 'image_url'], 'string'],
            [['name', 'slug'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'parent_id' => 'Chuyên ngành',
            'name' => 'Tên',
            'slug' => 'Slug',
            'description' => 'Mô tả',
            'image_url' => 'Ảnh đại diện',
            'order' => 'Thứ tự',
            'status' => 'Trạng thái',
            'created_time' => 'Created Time',
            'updated_time' => 'Updated Time',
        ];
    }

    /**
     * @desc get relation of parent subject
     * @return self
     */
    public function getParent()
    {
        return $this->hasOne(self::className(), ['id' => 'parent_id']);
    }

    /**
     * @desc get relation of children subjects
     * @return array of self
     */
    public function getChildren()
    {
        return $this->hasMany(self::className(), ['parent_id' => 'id']);
    }

    public function loadDefaultValues($skipIfSet = true)
    {
        $ret = parent::loadDefaultValues($skipIfSet);

        if ($this->hasAttribute('status') && is_null($this->status)) {
            $this->status = self::STATUS_ACTIVE;
        }

        return $ret;
    }

    // TODO: use scope
    public static function findAllActive()
    {
        return self::find()->andWhere(['status' => self::STATUS_ACTIVE])->all();
    }

    public function getCourses() {
        return $this->hasMany(Course::className(), ['course_id' => 'id'])->via('courseSubjects');
    }

    public function getCourseSubjects() {
        return $this->hasMany(CourseSubject::className(), ['subject_id' => 'id']);
    }
}
