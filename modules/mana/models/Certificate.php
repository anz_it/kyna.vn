<?php

namespace kyna\mana\models;

use Yii;

/**
 * This is the model class for table "{{%mana_certificates}}".
 *
 * @property integer $id
 * @property string $name
 * @property string $slug
 * @property string $description
 * @property string $image_url
 * @property integer $order
 * @property integer $status
 * @property integer $created_time
 * @property integer $updated_time
 */
class Certificate extends \kyna\base\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%mana_certificates}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'slug'], 'required'],
            [['description', 'image_url'], 'string'],
            [['order', 'status', 'created_time', 'updated_time', 'v2_id'], 'integer'],
            [['name', 'slug'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Tên',
            'slug' => 'Slug',
            'description' => 'Mô tả',
            'image_url' => 'Ảnh đại diện',
            'order' => 'Thứ tự',
            'status' => 'Trạng thái',
            'created_time' => 'Created Time',
            'updated_time' => 'Updated Time',
        ];
    }

    public function loadDefaultValues($skipIfSet = true)
    {
        $ret = parent::loadDefaultValues($skipIfSet);

        if ($this->hasAttribute('status') && is_null($this->status)) {
            $this->status = self::STATUS_ACTIVE;
        }

        return $ret;
    }

    // TODO: use scope
    public static function findAllActive()
    {
        return self::find()->andWhere(['status' => self::STATUS_ACTIVE])->all();
    }

    public function getCourses() {
        return $this->hasMany(Course::className(), ['certificate_id' => 'id']);
    }
}
