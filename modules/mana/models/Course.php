<?php

namespace kyna\mana\models;

use Yii;
use kyna\course\models\Course as KynaCourse;

/**
 * This is the model class for table "{{%mana_courses}}".
 *
 * @property integer $id
 * @property string $course_id
 * @property string $education_id
 * @property string $certificate_id
 * @property string $organization_id
 * @property string $description_for
 * @property string $condition
 * @property string $learning_time
 * @property string $learning_method
 * @property string $learning_start_date
 * @property integer $order
 * @property integer $status
 * @property integer $created_time
 * @property integer $updated_time
 */
class Course extends \kyna\base\ActiveRecord
{
    const ORDER_BY_NEW = 1;
    const ORDER_BY_HOT = 2;
    const ORDER_BY_PROMOTION = 3;

    public $subjects;
    public $teachers;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%mana_courses}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['course_id', 'education_id', 'certificate_id', 'organization_id'], 'required'],
            [['course_id', 'course_ref_id', 'education_id', 'certificate_id', 'organization_id', 'order', 'status', 'created_time', 'updated_time', 'v2_id'], 'integer'],
            [['description_for', 'condition', 'learning_time', 'learning_method', 'learning_start_date'], 'string'],
            ['course_id', 'unique', 'message' => 'Khóa học này đã được thêm vào Hệ thống Mana']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'course_id' => 'Khóa học Tiếng Việt',
            'course_ref_id' => 'Khóa học Tiếng Anh',
            'education_id' => 'Hệ đào tạo',
            'certificate_id' => 'Chứng chỉ',
            'subjects' => 'Chuyên ngành',
            'teachers' => 'Giảng viên',
            'organization_id' => 'Tổ chức cấp chứng chỉ',
            'description_for' => 'Khóa học phù hợp với',
            'condition' => 'Tiêu chuẩn đầu vào - Tiêu chí tốt nghiệp',
            'learning_time' => 'Thời lượng',
            'learning_method' => 'Cách thức học',
            'learning_start_date' => 'Thời gian bắt đầu học',
            'order' => 'Thứ tự',
            'status' => 'Trạng thái',
            'created_time' => 'Ngày tạo',
            'updated_time' => 'Ngày cập nhật',
        ];
    }

    public function loadDefaultValues($skipIfSet = true)
    {
        $ret = parent::loadDefaultValues($skipIfSet);

        if ($this->hasAttribute('status') && is_null($this->status)) {
            $this->status = self::STATUS_ACTIVE;
        }

        return $ret;
    }

    // TODO: use scope
    public static function findAllActive()
    {
        return self::find()->andWhere(['status' => self::STATUS_ACTIVE])->all();
    }

    public function getEducation() {
        return $this->hasOne(Education::className(), ['id' => 'education_id'] );
    }

    public function getCertificate() {
        return $this->hasOne(Certificate::className(), ['id' => 'certificate_id'] );
    }

    public function getOrganization() {
        return $this->hasOne(Organization::className(), ['id' => 'organization_id'] );
    }


    public function getKynaCourse() {
        return $this->hasOne(KynaCourse::className(), ['id' => 'course_id'] );
    }

    public function getKynaCourseReference() {
        return $this->hasOne(KynaCourse::className(), ['id' => 'course_ref_id'] );
    }

    public static function listOrders()
    {
        // TODO: get from `settings` table
        return [
            self::ORDER_BY_NEW => 'Khóa học mới nhất',
            self::ORDER_BY_HOT => 'Khóa học nổi bật',
            self::ORDER_BY_PROMOTION => 'Khóa học khuyến mãi',
        ];
    }

    public function getCourseSubjects() {
        return $this->hasMany(CourseSubject::className(), ['course_id' => 'id']);
    }

    /**
     * @return $this
     */
    public function getSubjects() {
        return $this->hasMany(Subject::className(), ['id' => 'subject_id'])->via('courseSubjects');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCourseTeachers() {
        return $this->hasMany(CourseTeacher::className(), ['course_id' => 'id']);
    }

    /**
     * @return Teacher
     */
    public function getTeachers() {
        return $this->hasMany(Teacher::className(), ['id' => 'teacher_id'])->via('courseTeachers');
    }


    /**
     * @return Teacher
     */
//    public function getListTeachers() {
//        return $this->hasMany(Teacher::className(), ['id' => 'teacher_id'])->via('courseTeachers');
//    }

}
