<?php

namespace kyna\mana\models;

use Yii;

/**
 * This is the model class for table "{{%mana_contacts}}".
 *
 * @property integer $id
 * @property string $name
 * @property string $phone
 * @property string $email
 * @property string $subject_id
 * @property string $certificate_id
 * @property string $education_id
 * @property integer $status
 * @property integer $created_time
 * @property integer $updated_time
 */
class Contact extends \kyna\base\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%mana_contacts}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['subject_id', 'certificate_id', 'education_id', 'status', 'created_time', 'updated_time', 'v2_id'], 'integer'],
            [['name'], 'string', 'max' => 55],
            [['phone'], 'string', 'max' => 11, 'min' => 10],
            [['name', 'email', 'phone', 'subject_id', 'certificate_id', 'education_id'], 'required'],
            ['email', 'email'],
            ['phone', 'number']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Tên',
            'phone' => 'Số điện thoại',
            'email' => 'Email',
            'subject_id' => 'Chuyên ngành',
            'certificate_id' => 'Chứng chỉ',
            'education_id' => 'Hệ đào tạo',
            'status' => 'Trạng thái',
            'created_time' => 'Ngày đăng ký',
            'updated_time' => 'Updated Time',
        ];
    }

    public static function listStatus()
    {
        $list = [
            self::STATUS_ACTIVE => 'Hoàn thành',
            self::STATUS_DEACTIVE => 'Chưa liên hệ',
        ];

        return $list;
    }

    /**
     * @desc convert and return status by text
     * @return text or null
     */
    public function getStatusText() // TODO: move to trait
    {
        $listStatus = self::listStatus();
        if (isset($listStatus[$this->status])) {
            return $listStatus[$this->status];
        }
        return null;
    }

    public function getStatusHtml() // TODO: move to trait
    {
        switch ($this->status) {
            case self::STATUS_ACTIVE:
                $labelClass = 'label-success';
                break;

            case self::STATUS_DEACTIVE:
                $labelClass = 'label-warning';

                break;

            default:
                $labelClass = 'label-default';
                break;
        }

        return '<span class="label ' . $labelClass . '">' . $this->statusText . '</span>';
    }

    public function loadDefaultValues($skipIfSet = true)
    {
        $ret = parent::loadDefaultValues($skipIfSet);

        if ($this->hasAttribute('status') && is_null($this->status)) {
            $this->status = self::STATUS_DEACTIVE;
        }

        return $ret;
    }
}
