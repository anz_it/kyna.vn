<?php

namespace kyna\learning\models;

use kyna\course\models\CourseLesson;
use kyna\course\models\Quiz;
use kyna\gamification\models\MissionCondition;
use kyna\user\models\UserCourse;
use kyna\gamification\models\Mission;
use kyna\gamification\models\UserPointHistory;
use Yii;

/**
 * This is the model class for table "{{%user_course_lessons}}".
 *
 * @property integer $id
 * @property integer $user_course_id
 * @property integer $course_lesson_id
 * @property integer $start_time
 * @property boolean $is_passed
 * @property integer $day_can_learn
 * @property integer $passed_time
 * @property double $process
 * @property integer $created_time
 * @property integer $updated_time
 * @property integer $status
 */
class UserCourseLesson extends \kyna\base\ActiveRecord
{

    const SCENARIO_PASS = 'scenario-pass';
    const SCENARIO_PROCESS = 'scenario-process';

    const EVENT_PASSED = 'event-passed';
    const EVENT_PROCESS_UPDATED = 'event-provess-updated';

    public $userId;
    public $courseId;

    public function init()
    {
        $parent = parent::init();

        $this->on(self::EVENT_PASSED, [$this, 'afterPass']);
        $this->on(self::EVENT_PROCESS_UPDATED, [$this, 'afterProcessUpdated']);

        return $parent;
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user_course_lessons}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_course_id', 'course_lesson_id'], 'required'],
            [['user_course_id', 'course_lesson_id', 'start_time', 'day_can_learn', 'passed_time', 'created_time', 'updated_time', 'status', 'userId', 'courseId'], 'integer'],
            [['is_passed'], 'boolean'],
            [['process'], 'number'],
        ];
    }

    public function scenarios()
    {
        $scenarios = array_merge(parent::scenarios(), [
            // create order on frontend
            self::SCENARIO_PASS => ['start_time', 'is_passed', 'passed_time', 'process'],
        ]);

        $scenario = $this->getScenario();
        if (array_key_exists($scenario, $scenarios)) {
            $scenarios[$scenario] = array_merge($scenarios[$scenario], $this->metaKeys());
        }

        return $scenarios;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_course_id' => 'User Course ID',
            'course_lesson_id' => 'Course Lesson ID',
            'start_time' => 'Start Time',
            'is_passed' => 'Is Passed',
            'day_can_learn' => 'Day Can Learn',
            'passed_time' => 'Passed Time',
            'process' => 'Process',
            'created_time' => 'Created Time',
            'updated_time' => 'Updated Time',
            'status' => 'Status',
        ];
    }

    public function afterSave($insert, $changedAttributes)
    {
        $ret = parent::afterSave($insert, $changedAttributes);

        switch ($this->scenario) {
            case self::SCENARIO_PASS:
                $this->trigger(self::EVENT_PASSED);
                break;

            case self::SCENARIO_PROCESS:
                $this->trigger(self::EVENT_PROCESS_UPDATED);
                break;

            default:
                break;
        }

        return $ret;
    }

    public static function pass($user_course_id, $lesson_id)
    {
        $model = self::find()->where([
            'user_course_id' => $user_course_id,
            'course_lesson_id' => $lesson_id
        ])->one();

        $time = time();
        if ($model == null) {
            $model = new self();
            $model->user_course_id = $user_course_id;
            $model->course_lesson_id = $lesson_id;
            $model->start_time = $time;
        }

        $model->scenario = self::SCENARIO_PASS;

        $model->is_passed = self::BOOL_YES;
        $model->passed_time = $time;
        $model->process = 100; // TODO: implement process

        return $model->save();
    }

    public function afterPass()
    {
        /* @var $lesson CourseLesson */
        $lesson = $this->courseLesson;

        switch ($lesson->type) {
            case CourseLesson::TYPE_QUIZ:
                $this->applyQuizMission($lesson->quiz_id, $this->userCourse->course->sellPrice == 0 ? true : false);
                break;

            case CourseLesson::TYPE_CONTENT:
                $this->applyContentMission($lesson, $this->userCourse->course->sellPrice == 0 ? true : false);
                break;

            default:
                break;
        }
    }

    public function applyQuizMission($quiz_id, $is_free_course = false)
    {
        /** @var  $quiz Quiz */
        $quiz = Quiz::findOne($quiz_id);

        $courseLessonTblName = CourseLesson::tableName();
        $userCourseTblName = UserCourse::tableName();
        $quizTblName = Quiz::tableName();

        $finishedQuizeUserCourseLessonsQuery = self::find()
            ->alias('ucl')
            ->joinWith('courseLesson')
            ->joinWith('userCourse')
            ->leftJoin("$quizTblName q", "q.id = $courseLessonTblName.quiz_id")
            ->groupBy($courseLessonTblName . '.quiz_id')
            ->select($courseLessonTblName . '.quiz_id')
            ->where([
                $userCourseTblName . '.user_id' => $this->userCourse->user_id,
                $courseLessonTblName . '.type' => CourseLesson::TYPE_QUIZ,
                'ucl.is_passed' => self::BOOL_YES,
                $userCourseTblName . '.course_id' => $quiz->course_id
            ]);

        switch ($quiz->type) {
            case Quiz::TYPE_ONE_CHOICE:
                $missions = Mission::findByQuizMultiChoice($quiz->is_end_quiz, $quiz->course_id, $quiz->course->category_id);

                $finishedQuizeUserCourseLessonsQuery->andWhere([
                    'q.type' => Quiz::TYPE_ONE_CHOICE
                ]);
                break;

            default:
                $missions = Mission::findByQuizOther($quiz->is_end_quiz, $quiz->course_id, $quiz->course->category_id);

                $finishedQuizeUserCourseLessonsQuery->andWhere([
                    '!=', 'q.type', Quiz::TYPE_ONE_CHOICE
                ]);
                break;
        }

        foreach ($missions as $mission) {
            /* @var $mission Mission */
            $missionQuery = clone $finishedQuizeUserCourseLessonsQuery;

            $conditions = $mission->missionConditions;
            $validMission = true;
            $quizIds = [];

            /* @var $condition MissionCondition */
            foreach ($conditions as $condition) {
                $conditionQuery = clone $missionQuery;

                $minValue = $condition->min_value;
                $isValid = false;

                if (in_array($condition->type, [MissionCondition::TYPE_LESSON_QUIZ_PASS_LAST_OTHER, MissionCondition::TYPE_LESSON_QUIZ_PASS_MULTI_CHOICE_LAST])) {
                    $conditionQuery->leftJoin('quiz_meta qm', "qm.quiz_id = q.id")
                        ->andWhere([
                            'qm.key' => 'is_end_quiz',
                            'qm.value' => 1
                        ]);
                }

                if ($minValue > 1) {
                    $finishedQuizes = $conditionQuery->distinct()->column();

                    foreach ($finishedQuizes as $qId) {
                        $checkExist = UserPointHistory::find()
                            ->alias('h')
                            ->joinWith('historyReferences hr')
                            ->where([
                                'h.mission_id' => $mission->id,
                                'h.user_id' => $this->userCourse->user_id
                            ])
                            ->andWhere(["hr.reference_id" => $qId])
                            ->exists();

                        if ($checkExist) {
                            continue;
                        }

                        $quizIds[] = $qId;
                    }

                    $total = count($quizIds);
                    if ($total > 0 && $total % $minValue == 0) {
                        $isValid = true;
                    }
                } else {
                    $checkExist = UserPointHistory::find()
                        ->alias('h')
                        ->joinWith('historyReferences hr')
                        ->where([
                            'h.mission_id' => $mission->id,
                            'h.user_id' => $this->userCourse->user_id
                        ])
                        ->andWhere(["hr.reference_id" => $quiz->id])
                        ->exists();

                    if (!$checkExist) {
                        $quizIds = $quiz->id;
                        $isValid = true;
                    }
                }

                if ($mission->condition_operator == 'AND') {
                    $validMission = $validMission && $isValid;
                } elseif ($mission->condition_operator == 'OR') {
                    $validMission = $validMission || $isValid;
                    if ($validMission) {
                        break;
                    }
                }
            }

            if ($validMission) {
                $point = $mission->k_point;
                if ($mission->k_point_for_free_course != null && $is_free_course) {
                    $point = $mission->k_point_for_free_course;
                }

                Mission::addPointByMission($this->userCourse->user_id, $point, $mission->id, $quizIds, $quiz->course_id);
            }
        }
    }

    public function applyContentMission($lesson, $is_free_course = false)
    {
        $missions = Mission::findByLessonContentFinished($lesson->course_id, $lesson->course->category_id, $lesson->id);

        $courseLessonTblName = CourseLesson::tableName();
        $userCourseTblName = UserCourse::tableName();

        $finishedContentLessonsQuery = self::find()
            ->alias('ucl')
            ->joinWith('courseLesson')
            ->joinWith('userCourse')
            ->groupBy('ucl.course_lesson_id')
            ->select('ucl.course_lesson_id')
            ->where([
                $userCourseTblName . '.user_id' => $this->userCourse->user_id,
                $userCourseTblName . '.course_id' => $lesson->course_id,
                $courseLessonTblName . '.type' => CourseLesson::TYPE_CONTENT,
                'ucl.is_passed' => self::BOOL_YES
            ]);

        foreach ($missions as $mission) {
            $missionQuery = clone $finishedContentLessonsQuery;
            $validMission = true;
            $conditions = $mission->missionConditions;
            $lessonIds = [];

            /* @var $condition MissionCondition */
            foreach ($conditions as $condition) {
                $conditionQuery = clone $missionQuery;

                $minValue = $condition->min_value;
                $isValid = false;

                if ($minValue > 1) {
                    $finishedLessons = $conditionQuery->distinct()->column();

                    foreach ($finishedLessons as $lId) {
                        $checkExist = UserPointHistory::find()
                            ->alias('h')
                            ->joinWith('historyReferences hr')
                            ->where([
                                'h.mission_id' => $mission->id,
                                'h.user_id' => $this->userCourse->user_id
                            ])
                            ->andWhere(['hr.reference_id' => $lId])
                            ->exists();

                        if ($checkExist) {
                            continue;
                        }

                        $lessonIds[] = $lId;
                    }

                    $total = count($lessonIds);
                    if ($total > 0 && $total % $minValue == 0) {
                        $isValid = true;
                    }
                } else {
                    $checkExist = UserPointHistory::find()
                        ->alias('h')
                        ->joinWith('historyReferences hr')
                        ->where([
                            'h.mission_id' => $mission->id,
                            'h.user_id' => $this->userCourse->user_id
                        ])
                        ->andWhere(['hr.reference_id' => $lesson->id])
                        ->exists();

                    if (!$checkExist) {
                        $lessonIds[] = $lesson->id;
                        $isValid = true;
                    }
                }

                if ($mission->condition_operator == 'AND') {
                    $validMission = $validMission && $isValid;
                } elseif ($mission->condition_operator == 'OR') {
                    $validMission = $validMission || $isValid;
                    if ($validMission) {
                        break;
                    }
                }
            }

            if ($validMission) {
                $point = $mission->k_point;
                if ($mission->k_point_for_free_course != null && $is_free_course) {
                    $point = $mission->k_point_for_free_course;
                }

                Mission::addPointByMission($this->userCourse->user_id, $point, $mission->id, $lessonIds, $lesson->course_id);
            }
        }

    }

    public function getUserCourse()
    {
        return $this->hasOne(UserCourse::className(), ['id' => 'user_course_id']);
    }

    public function getCourseLesson()
    {
        return $this->hasOne(CourseLesson::className(), ['id' => 'course_lesson_id']);
    }

    public function afterProcessUpdated()
    {
        /* @var $lesson CourseLesson */
        $lesson = $this->courseLesson;

        switch ($lesson->type) {
            case CourseLesson::TYPE_VIDEO:
                $this->checkMissionVideoProcess($this->process, $lesson->course_id, $lesson->course->category_id, $lesson->course->sellPrice == 0 ? true : false);

                break;

            default:
                break;
        }
    }

    public function checkMissionVideoProcess($process, $course_id, $category_id, $is_free_course)
    {
        $missions = Mission::findByVideoProcess($process, $course_id, $category_id);

        foreach ($missions as $mission) {
            $checkExist = UserPointHistory::find()
                ->alias('h')
                ->joinWith('historyVideos hv')
                ->where([
                    'h.user_id' => Yii::$app->user->id,
                    'h.mission_id' => $mission->id,
                    'hv.video_id' => $this->course_lesson_id
                ])
                ->exists();

            if (!$checkExist) {
                $point = $mission->k_point;
                if ($mission->k_point_for_free_course != null && $is_free_course) {
                    $point = $mission->k_point_for_free_course;
                }

                Mission::addPointByMission($this->userCourse->user_id, $point, $mission->id, $this->course_lesson_id, $course_id);
            }
        }
    }
}
