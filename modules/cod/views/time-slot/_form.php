<?php

use yii\helpers\Html;
use kartik\form\ActiveForm;
use kartik\field\FieldRange;

$crudTitles = Yii::$app->params['crudTitles'];
?>

<div class="betime-slot-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="form-group">
        <?=
        FieldRange::widget([
            'form' => $form,
            'model' => $model,
            'label' => 'Thời gian trực',
            'attribute1' => 'start_time',
            'attribute2' => 'end_time',
            'type' => FieldRange::INPUT_TIME,
            'widgetOptions1' => [
                'pluginOptions' => [
                    'showMeridian' => false
                ]
            ],
            'widgetOptions2' => [
                'pluginOptions' => [
                    'showMeridian' => false
                ]
            ]
        ]);
        ?>
    </div>

    <div class="form-group">
        <table class="table">
                <tr class="danger">
                    <td>Thứ 2</td>
                    <td>Thứ 3</td>
                    <td>Thứ 4</td>
                    <td>Thứ 5</td>
                    <td>Thứ 6</td>
                    <td>Thứ 7</td>
                    <td>Chủ nhật</td>
                </tr>
                <tr>
                    <?php
                    $userData = yii\helpers\ArrayHelper::map($users, 'id', 'profile.name');
                    ?>
                    <?php foreach ($weekDays as $day) : ?>
                        <td>
                            <?= Html::activeCheckboxList($model, $day, $userData, ['separator' => '<br>']) ?>
                        </td>
                    <?php endforeach; ?>
                </tr>
            </table>
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? $crudTitles['create'] : $crudTitles['update'], ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::a($crudTitles['cancel'], ['index'], ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
