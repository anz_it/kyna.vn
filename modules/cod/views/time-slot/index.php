<?php

use yii\helpers\Html;
use yii\grid\GridView;

$crudTitles = Yii::$app->params['crudTitles'];

$this->title = Yii::$app->controller->mainTitle;
$this->params['breadcrumbs'][] = $this->title;

$cols = [
    ['class' => 'yii\grid\SerialColumn'],
//    'id',
    'start_time',
    'end_time',
];

foreach ($weekDays as $day) {
    $col = [
        'attribute' => $day,
        'format' => 'html',
        'value' => function ($model, $key, $index, $column) {
            $result = '';
            
            foreach ($model->{$column->attribute} as $userId) {
                if (!empty($userId)) {
                    $result .= \common\models\User::find()->where(['id' => $userId])->with('profile')->one()->profile->name . '<br>';
                }
            }
            
            return $result;
        }
    ];
    
    $cols[] = $col;
}
$cols[] = [
    'class' => 'yii\grid\ActionColumn',
    'template' => '{update}'
];
?>
<div class="row">
    <div class="col-xs-12">
        <div class="betime-slot-index">
            <nav class="navbar navbar-default navbar-static-top">
                <div class="container-fluid">
                    <ul class="navbar-nav nav navbar-right">
                        <li><?= Html::a('<i class="fa fa-plus"></i> ' . $crudTitles['create'] . ' Time slot', ['create']) ?></li>
                    </ul>
                </div>
            </nav>

            <div class="box">
                <?=
                GridView::widget([
                    'dataProvider' => $dataProvider,
                    'columns' => $cols,
                ]);
                ?>
            </div>
        </div>
    </div>
</div>