<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\BETimeSlot */

$this->title = Yii::$app->params['crudTitles']['create'];
$this->params['breadcrumbs'][] = ['label' => Yii::$app->controller->mainTitle, 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="betime-slot-create">

    <?= $this->render('_form', [
        'model' => $model,
        'users' => $users,
        'weekDays' => $weekDays
    ]) ?>

</div>
