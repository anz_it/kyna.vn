<?php

namespace app\modules\cod;

class CodModule extends \app\components\BEModule
{
    public $controllerNamespace = 'app\modules\cod\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
