<?php

namespace kyna\payment\models;

use Yii;

/**
 * This is the model class for table "payment_transaction_meta".
 *
 * @property integer $id
 * @property integer $payment_transaction_id
 * @property string $key
 * @property string $value
 */
class PaymentTransactionMeta extends \kyna\base\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'payment_transaction_meta';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['payment_transaction_id'], 'required'],
            [['payment_transaction_id'], 'integer'],
            [['value'], 'string'],
            [['key'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'payment_transaction_id' => 'Payment Transaction ID',
            'key' => 'Name',
            'value' => 'Value',
        ];
    }
}
