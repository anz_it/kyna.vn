<?php

namespace kyna\payment\models;
use common\helpers\PhoneNumberHelper;
use kyna\order\models\Order;

/**
 * This is the model class for table "payment_transactions".
 *
 * @property int $id
 * @property int $transaction_id
 * @property int $payment_method_id
 * @property int $order_id
 * @property string $payment_method
 * @property float $payment_fee
 * @property float $total_amount
 * @property integer $shipping_status
 */
class PaymentTransaction extends \kyna\base\ActiveRecord
{
    const TOP_UP_TYPE = 1;
    const CHARGING = 2;
    const STATUS_FAILED = -1;
    const STATUS_SUCCESS = 1;
    const STATUS_WAITING = 0;
    protected function enableMeta()
    {
        return [
            'payment_transaction',
            '\\kyna\\payment\\models\\PaymentTransactionMeta'
        ];
    }

    protected function metaKeys()
    {
        return ['response_string', 'pinCode', 'serial', 'serviceCode', 'type'];
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'payment_transactions';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['order_id'], 'integer'],
            [['transaction_code'], 'safe'],
            [['payment_method'], 'required'],
            [['payment_fee', 'total_amount', 'amount'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'transaction_code' => 'Transaction Code',
            'order_id' => 'Order',
            'payment_method' => 'Payment Method',
            'payment_fee' => 'Payment Fee',
            'total_amount' => 'Total Amount',
        ];
    }

    public function getServiceName()
    {
        return PhoneNumberHelper::getServiceName($this->serviceCode);
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        if( $this->status != -1 &&  $this->payment_method == 'cod') {
            $order = Order::findOne($this->order_id);
            if($order && empty($order->to_shipper_time)) {
                $order->to_shipper_time = time();
                $order->save(false);
            }
        }
    }
}
