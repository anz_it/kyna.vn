<?php

namespace kyna\payment\models;

use kyna\settings\models\Setting;
use Yii;
use yii\base\InvalidParamException;
use yii\helpers\ArrayHelper;
use yii\helpers\Inflector;
use kyna\payment\events\TransactionEvent;
use kotchuprik\sortable\behaviors\Sortable;

class PaymentMethod extends \kyna\base\ActiveRecord
{
    const PAYMENT_TYPE_SERVICE = 2;
    const PAYMENT_TYPE_REDIRECT = 1;
    const PAYMENT_TYPE_NONE = 0;
    const LIMIT_PAYMENT = 3000000;
    const MOMO_MIN_LIMIT_PAYMENT = 1000;
    const MOMO_MAX_LIMIT_PAYMENT = 20000000;
    const PAYOO_MIN_LIMIT_PAYMENT = 1000;
    const PAYOO_MAX_LIMIT_PAYMENT = 20000000;
    const EVENT_PAYMENT_FAILED = 'payment-failed';
    const EVENT_PAYMENT_SUCCESS = 'payment-success';
    const EVENT_PAYMENT_PAY = 'payment-pay';
    const GET_AVAILABLE_ALL = 0;
    const GET_AVAILABLE_COD = 1;
    const GET_AVAILABLE_NOT_COD = 2;
    const GET_AVAILABLE_ANOTHER = 3;

    public $errors;
    public $transaction;
    public $returnUrl;
    public $paymentParams = false;
    public $continue_pay = false;

    private $_client;
    private $_settings;
    private $_libNamespace = 'kyna\\payment\\lib\\';

    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['sortable'] = [
            'class' => Sortable::className(),
            'orderAttribute' => 'position',
            'query' => self::find(),
        ];

        return $behaviors;
    }
    
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'payment_methods';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['status'], 'boolean'],
            [['payment_type', 'created_time', 'updated_time', 'position'], 'integer'],
            [['content'], 'string'],
            [['name'], 'string', 'max' => 255],
            [['class'], 'string', 'max' => 20],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Tên phương thức',
            'class' => 'Class',
            'payment_type' => 'Loại hình thanh toán',
            'status' => 'Trạng thái',
            'created_time' => 'Created Time',
            'updated_time' => 'Updated Time',
            'content' => 'Hướng dẫn thanh toán',
        ];
    }

    public static function getPaymentTypes()
    {
        return [
            self::PAYMENT_TYPE_SERVICE => 'Thông qua webservice',
            self::PAYMENT_TYPE_REDIRECT => 'Chuyển đến trang thanh toán',
            self::PAYMENT_TYPE_NONE => 'Không (chỉ hiển thị hướng dẫn)',
        ];
    }

    public function __get($key)
    {
        if ($key === 'client') {
            return $this->_getClient();
        }

        return parent::__get($key);
    }

    public function init()
    {
        parent::init();

        $this->on(self::EVENT_PAYMENT_SUCCESS, [$this, 'updatePaymentResult']);
        $this->on(self::EVENT_PAYMENT_FAILED, [$this, 'updatePaymentResult']);
    }

    public function isPayable()
    {
        return self::PAYMENT_TYPE_NONE !== $this->payment_type;
    }
    
    public function getIsCod() 
    {
        $client = $this->client;

        return !is_null($client) ? $client->isCod : false;
    }

    public static function loadModel($class, $formData = false)
    {
        $model = self::find()->where(['class' => $class])->one();

        if (empty($model)) {
            return false;
        }
        if ($formData) {
            $model->paymentParams = $formData;
        }

        return $model;
    }

    public function pay($order, $shipping_method = null, $params = null)
    {
        if ($this->payment_type == self::PAYMENT_TYPE_NONE) {
            throw new InvalidParamException('Instruction-Only payment not support method `pay`');
        }
        if (in_array($order->payment_method, ['epay', 'ipay','payoo']) && $order->paidAmount >= $order->realPayment) {
            $event = new TransactionEvent();
            $event->addData([
                'orderId' => $order->id,
                'transactionCode' => null
            ]);
            if($order->payment_method != 'payoo'){
                $this->trigger(self::EVENT_PAYMENT_SUCCESS, $event);
            }
            return true;
        }

        if ($transaction = $this->createTransaction($order)) {
            $event = new TransactionEvent();

            if (isset($shipping_method)) {
                if (isset($params)) {
                    if (isset($params['pick_up_location'])) {
                        $this->paymentParams['pick_up_location'] = $params['pick_up_location'];
                    }
                    if (isset($params['expected_payment_fee'])) {
                        $this->paymentParams['expected_payment_fee'] = $params['expected_payment_fee'];
                    }
                    if (isset($params['note'])) {
                        if (!empty($this->paymentParams['note'])) {
                            $this->paymentParams['note'] .= " - " . $params['note'];
                        } else {
                            $this->paymentParams['note'] = $params['note'];
                        }
                    }
                }
                $result = $this->client->pay($transaction->id, $order->id, $order->realPayment, $this->paymentParams, $shipping_method);
            } else {
                $this->paymentParams = $params;
                $result = $this->client->pay($transaction->id, $order->id, $order->realPayment, $this->paymentParams);
            }
            $event->addData($result);
            $event->transId = $transaction->id;

            if ($this->payment_type !== self::PAYMENT_TYPE_REDIRECT) {
                if ($result['error']) {
                    $this->errors = $result['error'];
                    $this->trigger(self::EVENT_PAYMENT_FAILED, $event);

                    Yii::error($result['error'], 'payment');
                    return $result;
                }

                $this->updatePaymentResult($event);

                if (in_array($order->payment_method, ['epay', 'ipay'])) {
                    $this->continue_pay = true;
                    $message = 'Nạp thành công <b>' . Yii::$app->formatter->asCurrency($result['amount']) . '</b>';
                    if ($order->realPayment > $order->paidAmount) {
                        $message .= ', vui lòng nạp tiếp để hoàn tất đơn hàng.';
                    }
                    Yii::$app->session->setFlash('success', $message);

                    return $result;
                }
                elseif($order->payment_method == 'payoo'){
                    $this->registerSuccess($order);
                }
                else {
                    $this->continue_pay = false;
                    $this->trigger(self::EVENT_PAYMENT_FAILED, $event);
                }
            } else {
                $this->trigger(self::EVENT_PAYMENT_PAY, $event);
            }
            return $result;
        }

        return false;
    }

    public function registerSuccess($order)
    {
        $mailer = Yii::$app->mailer;
        $mailer->htmlLayout = false;

        $settings = ArrayHelper::map(Setting::find()->all(), 'key', 'value');
        $mailer->compose('order/register_success', [
            'order' => $order,
            'settings' => $settings,
            'code' => !empty($code) ? $code : null
        ])->setTo($order->user->email)
            ->setSubject("Đăng ký đơn hàng #{$order->id} thành công.")
            ->send();
    }

    public function createTransaction($order)
    {
        if ($this->payment_type == self::PAYMENT_TYPE_NONE) {
            throw new InvalidParamException('Transactions supported by redirect payment only');
        }

        $transaction = new PaymentTransaction();
        $transaction->attributes = [
            'order_id' => $order->id,
            'payment_method' => $this->class,
            'total_amount' => $order->realPayment,
        ];

        if ($transaction->save()) {
            $this->transaction = $transaction;

            return $transaction;
        }
        return false;
    }

    public function query()
    {
        // TODO: Later
    }

    public function ipn($response)
    {
        if ($this->payment_type != self::PAYMENT_TYPE_REDIRECT) {
            throw new InvalidParamException('IPN supported by redirect payment only');
        }

        $result = $this->client->ipn($response);
        
        if ($result) {
            $transEvent = new TransactionEvent();
            if (method_exists($this->client, 'getTransactionId')) {
                $transEvent->transId = $this->client->getTransactionId($result['transactionId'], $result['orderId']);
            }
            $transEvent->addData(['orderId' => $result['orderId']]);
            $transEvent->addData(['transactionCode' => $result['transactionCode']]);
            $transEvent->addData(['responseString' => $result['responseString']]);

            if (empty($result['error'])) {
                $eventPayment = self::EVENT_PAYMENT_SUCCESS;
            } else {
                $transEvent->addData(['error' => $result['error']]);
                $eventPayment = self::EVENT_PAYMENT_FAILED;
            }
            $this->trigger($eventPayment, $transEvent);

            return $result;
        }
        
        return false;
    }

    public function printIpnResponse()
    {
        echo $this->client->ipnResponse;
    }

    public function updatePaymentResult($event)
    {
        $transData = $event->transData;
        $transactionId = $event->transId;

        $transaction = $this->findTransaction($transactionId);
        if (!$transaction) {
            return false;
        }

        if (!isset($transData['error']) || empty($transData['error'])) {
            if (isset($transData['transactionCode'])) {
                $transaction->transaction_code = $transData['transactionCode'];
            }
            if (!empty($transData['fee'])) {
                $transaction->payment_fee = $transData['fee'];
            }
            if(!empty($transData['amount'])){
                $transaction->amount = $transData['amount'];
            }
            if (!empty($this->paymentParams['pinCode'])) {
                $transaction->pinCode = $this->paymentParams['pinCode'];
            }
            if (!empty($this->paymentParams['serviceCode'])) {
                $transaction->serviceCode = $this->paymentParams['serviceCode'];
            }
            if (!empty($this->paymentParams['serial'])) {
                $transaction->serial = $this->paymentParams['serial'];
            }
            if (in_array($transaction->payment_method, ['payoo'])) {
                $transaction->status = PaymentTransaction::STATUS_WAITING;
            }else{
                $transaction->status = PaymentTransaction::STATUS_SUCCESS;
            }
        } else {
            if (!empty($transData['transactionCode'])) {
                $transaction->transaction_code = $transData['transactionCode'];
            }
            $transaction->status = PaymentTransaction::STATUS_FAILED;
        }
        if (isset($transData['responseString'])) {
            $transaction->response_string = $transData['responseString'];
        }

        return $transaction->save();
    }

    private function _getClient()
    {
        if ($this->payment_type == self::PAYMENT_TYPE_NONE) {
            return;
        }

        $className = $this->_libNamespace.$this->class.'\\'.Inflector::camelize($this->class);
        $this->_client = \Yii::createObject($className);
        $this->_client->returnUrl = $this->returnUrl;
        //$this->_settings = (new PaymentSettings($this->class))->config;
        return $this->_client;
    }

    public function getClientSettings() {
        return $this->client->settings;
    }
    public function setClientSettings($settings) {
        $this->client->saveSettings($settings);
    }

    public static function getAvailable($flag = self::GET_AVAILABLE_ALL)
    {
        $methods = self::find()->where(['status' => self::STATUS_ACTIVE])->orderBy('position')->all();
        if($flag === self::GET_AVAILABLE_ANOTHER){
            $result = [];
            foreach ($methods as $method) {
                if(!in_array($method->class,['payoo','momo'])){
                    $result[] = $method;
                }
            }
            return $result;
        }
        if ($flag !== self::GET_AVAILABLE_COD && $flag !== self::GET_AVAILABLE_NOT_COD && $flag !== true) {
            return $methods;
        }

        $result = [];
        foreach ($methods as $method) {
            if ($flag === self::GET_AVAILABLE_COD || $flag === true) {
                if ($method->isCod) {
                    $result[] = $method;
                }
            }

            if ($flag === self::GET_AVAILABLE_NOT_COD) {
                if (!$method->isCod) {
                    $result[] = $method;
                }
            }

        }
        return $result;
    }

    protected function findTransaction($transactionId)
    {
        if ($this->payment_type == self::PAYMENT_TYPE_NONE) {
            throw new InvalidParamException('Transactions supported by redirect payment only');
        }

        return PaymentTransaction::findOne($transactionId);
    }

    public static function getPaymentIsCodClasses() {
        $ids = [];
        $paymentMethods = self::find()->all();
        foreach ($paymentMethods as $method) {
            if ($method->isCod) {
                $ids[] = $method->class;
            }
        }
        return $ids;
    }
}