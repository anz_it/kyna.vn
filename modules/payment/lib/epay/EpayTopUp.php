<?php
/**
 * @author: Hong Ta
 * @desc: Top-up with remain money
 */
namespace kyna\payment\lib\epay;

use kyna\servicecaller\traits\SoapTrait;
use Yii;

class EpayTopUp {

    use SoapTrait;

    private $config;

    private $cmd_paymentCDV = 'paymentCDV';

    private $cmd_topup = 'topup';

    protected static $wsdl;


    public function __construct()
    {
//        if(YII_ENV == 'dev'){
//            $this->config = !empty(Yii::$app->params['topup']['sandbox']) ? Yii::$app->params['topup']['sandbox'] : null;
//            self::$wsdl = $this->config['ws_url'];
//        }else{
            $this->config = !empty(Yii::$app->params['topup']['production']) ? Yii::$app->params['topup']['production'] : null;
            self::$wsdl = $this->config['ws_url'];
//        }
    }

    /**
     * @param $info
     * @return bool
     */
    public function topup($data_send, $params = false)
    {
        $client = new \SoapClient($this->config['ws_url']);
        $request_id = $this->config['partnerName'].'_'.time().rand(000, 999);
        $data = array(
            'requestId' => $request_id,
            'partnerName' => $this->config['partnerName'],
            'provider' => $data_send['provider'],
            'account' => $data_send['phone_number'],
            'amount' => $data_send['amount'],
            'sign' => $this->sign($request_id.$this->config['partnerName'].$data_send['provider'].$data_send['phone_number'].$data_send['amount'])
        );
        try{
            $result = $client->__soapCall($this->cmd_topup, $data);
            return $result;
        }catch (\Exception $ex){
            return $ex;
        }
    }

    /**
     * @param $data
     * @return string
     */
    private function sign($data)
    {
        $private_key = file_get_contents(__DIR__."/topup/id_rsa.pem");
        //Sign
        openssl_sign($data, $binary_signature, $private_key, OPENSSL_ALGO_SHA1);
        $signature = base64_encode($binary_signature);

        return $signature;

    }

    /**
     * @param $data
     * @param $sign
     * @return bool
     */
    private function verify_sign($data, $sign)
    {
        $public_key = file_get_contents(__DIR__. "/topup/public_test");
        $verify = openssl_verify($data, base64_decode($sign), $public_key, OPENSSL_ALGO_SHA1);
        if($verify == 1){
            return true;
        }else{
            return false;
        }
    }

    /**
     * @param $info
     * @return bool
     */
    public function paymentCDV($info)
    {
//        $client = new \SoapClient($this->config['ws_url']);
        $request_id = $this->config['partnerName'].'_'.time().rand(000, 999);
        $time_out = $this->config['time_out'];
        $data = array(
            'requestId' => $request_id,
            'partnerName' => $this->config['partnerName'],
            'provider' => $info['provider'],
            'type' => $info['type'],
            'account' => $info['account'],
            'amount' => $info['amount'],
            'timeOut' => $time_out,
            'sign' => $this->sign($request_id.$this->config['partnerName'].$info['provider'].$info['type'].$info['account'].$info['amount'].$time_out)
        );


        print_r($data);

        try{
            $result = $this->call($this->cmd_paymentCDV, $data);
            return $result;
        }catch (\Exception $ex){
            return false;
        }

    }


    /**
     * @return bool
     */
    public function queryBalance()
    {
        $client = new \SoapClient($this->config['ws_url']);
        $data = array(
            'partnerName' => $this->config['partnerName'],
            'sign' => $this->sign($this->config['partnerName'])
        );
        try{
            $result = $client->__soapCall("queryBalance", $data);
            return $result;
        }catch (\Exception $ex){
            return false;
        }
    }

    /**
     * @param $info
     * @return bool
     */
    public function downloadSofpin($info)
    {
        $client = new \SoapClient($this->config['ws_url']);
        $request_id = $this->config['partnerName'].'_'.time().rand(000, 999);
        $data = array(
            'requestId' => $request_id,
            'partnerName' => $this->config['partnerName'],
            'provider' => $info['provider'],
            'amount' => $info['amount'],
            'quantity' => $info['quantity'],
            'sign' => $this->sign($request_id.$this->config['partnerName'].$info['provider'].$info['amount'].$info['quantity'])
        );
        try{
            $result = $client->__soapCall("downloadSoftpin", $data);
            return $result;
        }catch (\Exception $ex){
            return false;
        }
    }

    /**
     * @param null $request_id
     * @return bool
     */
    public function checkOrdersCVD($request_id = null)
    {
        if(!empty($request_id)){
            $client = new \SoapClient($this->config['ws_url']);
            $data = array(
                'requestId' => $request_id,
                'partnerName' => $this->config['partnerName'],
                'sign' => $this->sign($request_id.$this->config['partnerName'])
            );
            try{
                $result = $client->__soapCall("checkOrdersCDV", $data);
                return $result;
            }catch (\Exception $ex){
                return false;
            }
        }else{
            return false;
        }
    }

    /**
     * @param null $request_id
     * @param int $type
     * @return bool
     */
    public function checkTrans($request_id = null, $type = 1)
    {
        if(!empty($request_id)){
            $client = new \SoapClient($this->config['ws_url']);
            $data = array(
                'requestId' => $request_id,
                'partnerName' => $this->config['partnerName'],
                'type' => $type,
                'sign' => $this->sign($request_id.$this->config['partnerName'].$type)
            );
            try{
                $result = $client->__soapCall("checkTrans", $data);
                return $result;
            }catch (\Exception $ex){
                return false;
            }
        }else{
            return false;
        }
    }

    /**
     * @param null $request_id
     * @return bool
     */

    public function reDownloadSoftpin($request_id = null)
    {
        if(!empty($request_id)){
            $client = new SoapClient($this->config['ws_url']);
            $data = array(
                'requestId' => $request_id,
                'partnerName' => $this->config['partnerName'],
                'sign' => $this->sign($request_id.$this->config['partnerName'])
            );
            try{
                $result = $client->__soapCall("reDownloadSoftpin", $data);
                return $result;
            }catch (Exception $ex){
                return false;
            }
        }else{
            return false;
        }
    }


}