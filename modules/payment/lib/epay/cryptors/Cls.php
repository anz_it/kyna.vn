<?php

namespace kyna\payment\lib\epay\cryptors;

class Cls
{
    private static function _getPublicKey($key_file = null)
    {
        $filePath = __DIR__.'/' . $key_file;

        $fp = fopen($filePath, 'r');
        $key = fread($fp, filesize($filePath));
        fclose($fp);

        openssl_get_publickey($key);

        return $key;
    }

    private static function _getPrivateKey($key_file = null)
    {
        $filePath = __DIR__.'/' .$key_file;

        $fp = fopen($filePath, 'r');
        $key = fread($fp, filesize($filePath));
        fclose($fp);

        return $key;
    }

    public static function encrypt($source, $key_file)
    {
        //path holds the certificate path present in the system
        $publicKey = self::_getPublicKey($key_file);
        //$source="sumanth";
        $j = 0;
        $x = strlen($source) / 10;
        $y = floor($x);
        $crt = '';
        for ($i = 0; $i < $y; ++$i) {
            $cryptedText = '';

            openssl_public_encrypt(substr($source, $j, 10), $cryptedText, $publicKey);
            $j += 10;
            $crt .= $cryptedText;
            $crt .= ':::';
        }
        if (strlen($source) % 10) {
            openssl_public_encrypt(substr($source, $j), $cryptedText, $publicKey);
            $crt .= $cryptedText;
        }
        return $crt;
    }
    //Decryption with private key
    public static function decrypt($cryptedText, $key_file)
    {
        $privKey = self::_getPrivateKey($key_file);
        $tt = explode(':::', $cryptedText);
        $count = sizeof($tt);
        $i = 0;
        $str = '';
        while ($i < $count) {
            openssl_private_decrypt($tt[$i], $str1, $privKey);
            $str .= $str1;
            ++$i;
        }

        return $str;
    }
}
