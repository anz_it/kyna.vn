<?php

namespace kyna\payment\lib\epay;

use Yii;
use kyna\payment\lib\epay\cryptors\Cls;
use kyna\payment\lib\epay\cryptors\TripleDes;
use kyna\servicecaller\traits\SoapTrait;

class Epay extends \kyna\payment\lib\BasePayment
{

    use SoapTrait;

    private $_PartnerID = '';
    private $_MPIN = '';
    private $_UserName = '';
    private $_Pass = '';
    private $_PartnerCode = '';
    private $_Target = '';
    private $_encryptedPass;
    private $_sessionId;
    //Production Link
    public $wsdl;
    public $public_key_file = '';
    public $private_key_file = '';

    //Test Link
    //protected static $wsdl = 'http://192.168.0.85:10001/CardChargingGW_0108/services/Services?wsdl';

    protected function callerOptions()
    {
        return [
            'location' => $this->wsdl,
            'uri' => "http://113.161.78.134/VNPTEPAY/",
        ];
    }

    protected static $defaultMethod = 'wsdl';

    public function __construct()
    {
        $this->getInfo();
    }

    public function signIn()
    {
        $command = 'login';

        $this->_encryptPassword();
        $response = $this->call($command, $this->wsdl, $this->_UserName, $this->_encryptedPass, $this->_PartnerID);

        if (!isset($response->sessionid)) {
            $this->errors = $this->errorMap($response->status);

            return false;
        }

        $this->_decryptToken($response->sessionid);

        return true;
    }

    public function getInfo()
    {
        $sandbox = Yii::$app->params['epaySandBox'];
        if ($sandbox) {
            // sand box
            $account = $this->getTestAccount();
        } else {
            // production
            $account = $this->getAccount();
        }
        $this->_PartnerID = $account['m_PartnerID'];
        $this->_PartnerCode = $account['m_PartnerCode'];
        $this->_MPIN = $account['m_MPIN'];
        $this->_UserName = $account['m_UserName'];
        $this->_Pass = $account['m_Pass'];
        $this->public_key_file = $account['public_key'];
        $this->private_key_file = $account['private_key'];
        $this->wsdl = $account['wsdl'];
    }

    public function getTestAccount()
    {
        return [
            'm_PartnerID' => 'charging01',
            'm_MPIN' => 'pajwtlzcb',
            'm_UserName' => 'charging01',
            'm_Pass' => 'bcblcn',
            'm_PartnerCode' => '00477',
            'wsdl' => 'http://charging-test.megapay.net.vn:10001/CardChargingGW_V2.0/services/Services?wsdl',
            'public_key' => 'public_key.pem',
            'private_key' => 'private_key.pem',
        ];
    }

    private function _encryptPassword()
    {
        $password = Cls::encrypt($this->_Pass, $this->public_key_file);
        $this->_encryptedPass = base64_encode($password);
    }

    private function _decryptToken($sessionId)
    {
        $sesstionId = Cls::decrypt(base64_decode($sessionId), $this->private_key_file);
        $this->setToken($sesstionId); // HEX
    }

    protected function getToken()
    {
        $this->signIn();
        return parent::getToken(); // TODO: Change the autogenerated stub
    }

    public function pay($transId, $orderId, $amount, $params = false)
    {
        $command = 'cardCharging';
        $_sessionId = $this->getToken(); // HEX
        $encryptKey = hex2bin($_sessionId);
        $transactionPrefix = \Yii::$app->params['transactionPrefix'];
        if (empty($transactionPrefix)) {
            $transactionPrefix = 'test_';
        }

        $requestTransId = $this->_PartnerCode . "_{$transactionPrefix}" . $transId . '_' . $orderId;
        $username = $this->_UserName;
        $partnerId = $this->_PartnerID;
        $target = $orderId;

        $mPin = $this->_MPIN;
        $mPin = TripleDes::encrypt($this->_MPIN, $encryptKey);
        $mPin = bin2hex($mPin);

        $cardData = [$params['serial'], $params['pinCode'], $amount, $params['serviceCode']];
        $cardData = implode(':', $cardData);
        $cardData = TripleDes::encrypt($cardData, $encryptKey);
        $cardData = bin2hex($cardData);

        $responseObj = $this->call($command, $this->wsdl, $requestTransId, $username, $partnerId, $mPin, $target, $cardData, md5($_sessionId));
        $response = get_object_vars($responseObj);

        if (!is_numeric($responseObj->responseamount)) {
            $responseAmount = hex2bin($responseObj->responseamount);
            $responseAmount = TripleDes::decrypt($responseAmount, $encryptKey);
        } else {
            $responseAmount = $responseObj->responseamount;
        }


        return $this->_return($responseObj, [
                    'transactionId' => $transId,
                    'transactionCode' => 'transid',
                    'amount' => $responseAmount,
                    'responseString' => http_build_query($response),
                    'orderId' => $orderId,
        ]);
    }

    public function query($transId)
    {
        $command = 'getTransactionStatus';

        $requestTransId = $this->_PartnerCode . '_' . $transId;
        $username = $this->_UserName;
        $partnerId = $this->_PartnerID;
        $_sessionId = $this->getToken();

        $encryptKey = hex2bin($_sessionId);

        $responseObj = $this->call($command, $this->wsdl, $requestTransId, $username, $partnerId, md5($_sessionId));
        $response = get_object_vars($responseObj);

        $responseAmount = hex2bin($responseObj->responseamount);
        $responseAmount = TripleDes::decrypt($responseAmount, $encryptKey);
        return $this->_return($responseObj, [
                    'transactionId' => $transId,
                    'transactionCode' => 'transid',
                    'responseString' => http_build_query($response),
                    'amount' => $responseAmount,
        ]);
    }

    private function _return($response, $returnArray)
    {
        $error = null;

        if ($response->status != 1) {
            $errorMap = $this->errorMap();
            $error = isset($errorMap[$response->status]) ? $errorMap[$response->status] : 'Unknown Error';
        }

        $return = ['error' => $error];
        foreach ($returnArray as $key => $value) {
            $responseValue = isset($response->$value) ? $response->$value : $value;
            $return[$key] = $responseValue;
        }

        return $return;
    }

    public function calculateFee($amount, $param = null)
    {
        return 0;
    }

    public function ipn($response)
    {
        return 'IPN not supported';
    }

    protected function errorMap()
    {
        return [
            -24 => 'Bad card data',
            -11 => 'No provider found',
            -10 => 'Card format is incorrect',
            0 => 'Fail',
            1 => 'Success',
            3 => 'Invalid Session ID',
            4 => 'Card is incorrect',
            5 => 'Wrong card input exceed',
            7 => 'Session Time out',
            8 => 'Invalid IP request',
            9 => 'VMS charging chanel overload',
            10 => 'Provider error',
            11 => 'Telco connector corrupted',
            12 => 'Transaction duplicate',
            13 => 'System busy',
            -2 => 'Card is locked',
            -3 => 'Card is expired',
            50 => 'Card is used or card do not exist',
            51 => 'Card serial is invalid',
            52 => 'Card serial and pin is not match',
            53 => 'Card serial or pin is incorrect',
            55 => 'Card is block for 24 hours',
            62 => 'Invalid password',
            57 => 'Invalid mpin',
            58 => 'Invalid parameter',
            59 => 'Card is not activate',
            60 => 'Invalid partner id',
            61 => 'Invalid user',
            56 => 'TargetAccount is locked',
            63 => 'Transaction is not exist',
            64 => 'Decrypt data fail',
            99 => 'Transaction pending',
        ];
    }

    /**
     * @return array
     */
    public static function getPercentTopUp()
    {
        return [
            'VPN' => 550 / 100,
            'VMS' => 560 / 100,
            'VTT' => 4
        ];
    }

    /**
     * @return array
     */
    public static function getPercentCharge()
    {
        return [
            'VPN' => 17,
            'VMS' => 17,
            'VTT' => 18
        ];
    }

    public function getAccount()
    {
        return [
            'm_PartnerID' => 'dreamviet_v3',
            'm_MPIN' => 'fduydmvhm',
            'm_UserName' => 'dreamviet_v3',
            'm_Pass' => 'xxtivfuxm',
            'm_PartnerCode' => '01215',
            'public_key' => 'Epay_Public_key_dreamviet_v3.pem',
            'private_key' => 'private_key_dreamviet_v3.pem',
            'wsdl' => 'http://charging-service.megapay.net.vn/CardChargingGW_V2.0/services/Services?wsdl'
        ];

//        return [
//            'm_PartnerID' => 'CTT_HQ071',
//            'm_MPIN' => 'duezibleb',
//            'm_UserName' => 'CTT_HQ071',
//            'm_Pass' => 'ievrbwjlc',
//            'm_PartnerCode' => '00039',
//            'public_key' => 'DTT_public_key_CTT_HQ071.pem',
//            'private_key' => 'private_key_CTT_HQ071.pem',
//            'wsdl' => 'http://cttcorp.net/ChargingGW/services/Services?wsdl'
//        ];
    }

}
