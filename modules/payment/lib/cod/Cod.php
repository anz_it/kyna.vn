<?php

namespace kyna\payment\lib\cod;

use Yii;
use yii\base\InvalidConfigException;
use yii\base\InvalidParamException;
use yii\helpers\Json;

use PhpOffice\PhpSpreadsheet\Shared\Trend\PolynomialBestFit;
use donatj\Ini\Builder;

use kyna\payment\lib\ghn\Ghn;
use kyna\payment\lib\proship\Proship;
use kyna\payment\lib\ghtk\Ghtk;
use kyna\payment\lib\tiki\Tiki;
use kyna\servicecaller\traits\CurlTrait;

class Cod extends \kyna\payment\lib\BasePayment
{
    use CurlTrait;

    public $isCod = true;

    public $shippingMethod = null;

    public $shipper = null;
    private $_configFile = 'cod.ini';
    public $settings;


    public function __construct()
    {
        $configFile = __DIR__.'/'.$this->_configFile;

        if (!file_exists($configFile)) {
            file_put_contents($configFile, '');
        }
        $this->settings = parse_ini_file($configFile, true);
    }

    protected function beforeAction($shippingMethod = null)
    {
        if (isset($shippingMethod)) {
            switch ($shippingMethod) {
                case 'tiki':
                    $this->shipper = new Tiki();
                    break;
                case 'proship':
                    $this->shipper = new Proship();
                    break;
                case 'ghn':
                    $this->shipper = new Ghn();
                    break;
                case 'ghtk':
                    $this->shipper = new Ghtk();
                    break;
                default:
                    break;
            }
        }
    }

    protected function signIn($shippingMethod = null)
    {
        $this->beforeAction($shippingMethod);
        if (!isset($this->shipper))
            return false;
        return $this->shipper->signIn();
    }

    public function pay($transId, $orderId, $amount, $params = false, $shippingMethod = null)
    {
        $this->beforeAction($shippingMethod);
        if (!isset($this->shipper))
            return false;
        return $this->shipper->pay($transId,$orderId, $amount, $params);
    }

    public function ipn($response, $shippingMethod = null)
    {
        $this->beforeAction($shippingMethod);
        if (!isset($this->shipper))
            return false;
        return $this->shipper->ipn($response);
    }

    public function calculateFee($params, $shippingMethod = null)
    {
        $this->beforeAction($shippingMethod);
        if (!isset($this->shipper))
            return false;
        return $this->shipper->calculateFee($params);
    }

    public function query($shippingCode, $shippingMethod = null)
    {
        $this->beforeAction($shippingMethod);
        if (!isset($this->shipper))
            return false;
        return $this->shipper->calculateFee($shippingCode);
    }

    public function setShippingMethod($shippingMethod)
    {
        switch ($shippingMethod) {
            case 'ghn':
                $this->shippingMethod = new Ghn();
                break;
            case 'proship':
                $this->shippingMethod = new Proship();
                break;
            case 'ghtk':
                $this->shippingMethod = new Ghtk();
                break;
            default:
                break;
        }
    }
}