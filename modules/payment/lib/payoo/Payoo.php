<?php

namespace kyna\payment\lib\payoo;
use kyna\order\models\Order;
use kyna\payment\models\PaymentTransaction;
use yii\helpers\Url;
use yii\httpclient\Client;
use yii\httpclient\CurlTransport;
use Yii;

class Payoo extends \kyna\payment\lib\BasePayment
{
    private static $_client = null;
    private $_username = '';		    // Business' account
    private $_shop_id = '';				// Business' ShopId
    private $_shop_title = '';			// Business' ShopTitle
    private $_serectKey = '';			// Business' secret key
    private $_notify_url = '';  		// Business' url to receive notification payment result from Payoo's system.
    private $_apiUsername = '';
    private $_apiPassword = '';
    private $_signature = '';
    private $_payUrl = '';
    private $_businessAPIUrl = '';
    private $_merchantName = "KYNA";
    private $_shop_domain = '';
    private $_shop_back_url = '';
    const Payment_Expire_Date = 2;
    public function __construct()
    {
        if (!empty(Yii::$app->params['payoo'])) {
            $configs = Yii::$app->params['payoo'];
            $this->_username = $configs['_username'];
            $this->_shop_id = $configs['_shop_id'];
            $this->_shop_title = $configs['_shop_title'];
            $this->_shop_domain = $configs['_shop_domain'];
            $this->_apiUsername = $configs['_apiUsername'];
            $this->_apiPassword = $configs['_apiPassword'];
            $this->_signature = $configs['_signature'];
            $this->_serectKey = $configs['_serectKey'];
            $this->_payUrl = $configs['_payUrl'];
            $this->_businessAPIUrl = $configs['_businessAPIUrl'];
        }
    }

    protected function signIn()
    {
        // TODO: Implement signIn() method.
    }

    public function pay($transId, $orderId, $amount, $params = false)
    {
        $this->_notify_url =  $this->returnUrl;
        $ip = \Yii::$app->request->getUserIP();
        if (!$ip) {
            return $this->error('user ip cannot be detected');
        }
        $transactionPrefix = \Yii::$app->params['transactionPrefix'];
        if (empty($transactionPrefix)) {
            $transactionPrefix = 'test_';
        }
        $order = Order::findOne(['id'=>$orderId]);
        $order->updateStatus(Order::ORDER_STATUS_PAYMENT_FAILED);
        
        $requestTransId = $this->_merchantName . "_{$transactionPrefix}" . $transId . '_' . $order->id;
        $phoneNumber = $params['phone_number'];
        $contactName = $params['contact_name'];
        $address = $params['street_address'];
        $email = $params['email'];
        $note = $this->noteOrderToStringPayoo($orderId,$amount);
        $expireDate = self::Payment_Expire_Date;
        $validity_time = date('YmdHis', strtotime("+$expireDate day", time()));
        $order_ship_date = date('d/m/Y');
        $order_ship_days = 1;
        $order =json_encode(array(
            "OrderNo"=>$requestTransId,
            "ShopID"=>$this->_shop_id,
            "FromShipDate"=>$order_ship_date,
            "ShipNumDay"=>$order_ship_days,
            "Description"=>$note,
            "CyberCash"=>$amount,
            "PaymentExpireDate"=>$validity_time,
            "NotifyUrl"=>$this->_notify_url,
            "InfoEx"=>urlencode('<InfoEx><CustomerPhone>'.$phoneNumber.'</CustomerPhone><CustomerName>'.$contactName.'</CustomerName><CustomerAddress>'.$address.'</CustomerAddress><CustomerEmail>'.$email.'</CustomerEmail><Title>'.$contactName.'</Title></InfoEx>')
        ));

        // generate hash string with SHA512 algorithm to verify data
        $signature =	hash('sha512',$this->_serectKey.$order);

        $HeaderRequest=array(
            'cache-control:no-cache',
            'apiusername:'.$this->_apiUsername,
            'apipassword:'.$this->_apiPassword,
            'apisignature:'.$this->_signature,
            'content-type:application/json');

        $bodyData =json_encode(array("RequestData"=>$order,"Signature"=>$signature));
        $response = self::createRequest($this->_businessAPIUrl.'/getbillingcode', 'POST', $bodyData, $HeaderRequest);
        $repData = json_decode($response["ResponseData"]);
        $error = $this->errorMap();
        if($repData->ResponseCode !=0){
            return [
                'orderId' => $orderId,
                'result' => false,
                'amount' => $amount,
                'error' => $error[$repData->ResponseCode],
                'transactionCode'=> isset($repData->BillingCode)? $repData->BillingCode : 0,
                'responseString'=> $error[$repData->ResponseCode]
            ];
        }else{
            if($this->verifyChecksum($this->_serectKey,$response["ResponseData"],$response["Signature"])!= true)
            {
                return [
                    'orderId' => $orderId,
                    'result' => false,
                    'amount' => $amount,
                    'error' => $error[$repData->ResponseCode],
                    'transactionCode'=> isset($repData->BillingCode)? $repData->BillingCode : 0,
                    'responseString'=> $error[$repData->ResponseCode]
                ];
            }else{
                $order = Order::findOne(['id'=>$orderId]);
                /* @var $order Order*/
                $order->updateStatus(Order::ORDER_STATUS_PENDING_PAYMENT);
                return [
                    'orderId' => $orderId,
                    'result' => true,
                    'amount' => $amount,
                    'error' => false,
                    'transactionCode'=> isset($repData->BillingCode)? $repData->BillingCode : 0,
                    'responseString'=> $error[$repData->ResponseCode]
                ];
            }
        }
    }

    public function query($transId)
    {
        // TODO: Implement query() method.
    }

    public function ipn($response)
    {
    }

    private function _validateHash($response)
    {
        $strhash = hash('sha512', $this->_serectKey . '.' . $response['order_no'] . '.' . $response['status']);
        if(isset($response['checksum'])){
            if(strtoupper($strhash) == strtoupper($response['checksum']))
            {
                return true;
            }else{
                return false;
            }
        }else{
            return false;
        }

    }

    public function calculateFee($amount)
    {
        // TODO: Implement calculateFee() method.
    }

    private static function createRequest($url, $method, $data = [], $headers = [])
    {
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 60,
            CURLOPT_SSL_VERIFYHOST => FALSE,
            CURLOPT_SSL_VERIFYPEER => FALSE,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => $method,
            CURLOPT_POSTFIELDS => $data,
            CURLOPT_HTTPHEADER => $headers,
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        return (array)json_Decode($response,true);
    }

    public function getTransactionId($transactionRequestId, $orderId)
    {
        return $transactionRequestId;
    }

    function verifyChecksum($secretKey, $data, $checksum)
    {
        $strhash = hash('sha512',$secretKey.$data);
        if(strtoupper($strhash) == strtoupper($checksum))
        {
            return true;
        }
        return false;
    }

    protected function errorMap()
    {
        return [
            0 => 'Giao dịch thành công.',
            -1 => 'Tạo mã thất bại',
            -2 => 'Mã thanh toán đã tồn tại.',
            83 => 'Đơn hàng đã tồn tại.',
            84 => 'Thời gian giao hàng không đúng.',
            85 => 'Số tiền đơn hàng không hợp lệ.',
            86 => 'Hạn thanh toán không hợp lệ.',

        ];
    }

    public function validateIpnMessage($params)
    {
        if (empty($params) || empty($params['order_no']))
            return false;

        if($this->_validateHash($params) == false){
            return false;
        }

        // Transaction code & transaction id not match return false
        $transaction = PaymentTransaction::findOne([
            'transaction_code' => $params['order_no'],
        ]);

        if (empty($transaction))
            return false;

        return true;
    }

    public function updatePayooStatus($invoice,$keyFields,$signature){
        $result = [];
        $result['success'] = false;
        // Verified is TRUE
        if($this->verifyChecksumNotifiyResponse($invoice,$keyFields,$signature)== TRUE)
        {
            /* @var $invoice PaymentNotification */
            list($prefix,$hosts,$transactionId,$orderId) = explode("_", $invoice->getOrderNo());
            $transaction = PaymentTransaction::findOne([
                'id' => $transactionId,
                'order_id' => $orderId,
            ]);

            if($invoice->getState() == 'PAYMENT_RECEIVED')
            {
                if (!empty($transaction)) {
                    $this->updateShippingTransaction($transaction->id, $invoice->getState());
                    if (!empty($order = $this->getOrder($transaction->order_id))) {
                        if($order->status == Order::ORDER_STATUS_COMPLETE){
                            $result['message'] = "Đã thanh toán thành công payoo NOTIFY_RECEIVED";
                            $result['success'] = true;
                            return $result;
                        }
                        Order::complete($transaction->order_id, $transaction->transaction_code);
                        $result['message'] = "Thanh toán thành công payoo NOTIFY_RECEIVED";
                        $result['success'] = true;
                    }else{
                        $result['message'] = "Không tìm thấy đơn hàng {$invoice->getOrderNo()}";
                    }
                }else {
                    $result['message'] = "Transaction code {$transactionId} does not exist";
                }
            }else{
                $this->updateShippingTransaction($transaction->id, 'PAYMENT_PROCESSING');
                $result['message'] = "User chưa thanh toán Payoo PAYMENT_PROCESSING";
            }
        }else{
            $result['message'] = "verifyChecksumNotifiyResponse Payoo thất bại";
        }
        return $result;
    }
    private function updateShippingTransaction($id, $status)
    {
        $transaction = PaymentTransaction::findOne($id);
        if ($transaction) {
            if($status == 'PAYMENT_RECEIVED'){
                $transaction->status = PaymentTransaction::STATUS_SUCCESS;
                $transaction->save(false);
            }else{
                $transaction->status = PaymentTransaction::STATUS_FAILED;
                $transaction->save(false);
            }
        }
    }

    private function getOrder($id, $status = null)
    {
        if ($status == null) {
            $status = [
                Order::ORDER_STATUS_NEW,
                Order::ORDER_STATUS_PENDING_PAYMENT,
                Order::ORDER_STATUS_PENDING_CONTACT,
                Order::ORDER_STATUS_DELIVERING,
                Order::ORDER_STATUS_PAYMENT_FAILED,
                Order::ORDER_STATUS_COMPLETE,
            ];
        }
        return Order::find()->filterWhere([
            'id' => $id,
            'status' => $status
        ])->one();
    }

    function verifyChecksumNotifiyResponse($DataResponse, $KeyFields, $Checksum)
    {
        $strData = $this->_serectKey;
        unset($DataResponse->DigitalSignature);

        if(!empty($KeyFields))
        {
            $arr_Keys = explode('|', $KeyFields);
            for ($i = 0; $i < count($arr_Keys); $i++)
            {
                $strData .= '|' . $DataResponse->{$arr_Keys[$i]};
            }
        }
        if(strtoupper(hash('sha512',$strData))!= strtoupper($Checksum))
        {
            return FALSE;
        }
        return TRUE;
    }

    public function noteOrderToStringPayoo($orderId,$amount){
        /* @var $order Order */
        $orderString =  "
        Mã đơn hàng: #{$orderId} </br>
        Tổng tiền: {$amount} đ </br>
        Hình thức thanh toán: Tại cửa hàng tiện lợi (Payoo) </br>
        ";
        return $orderString;
    }
}