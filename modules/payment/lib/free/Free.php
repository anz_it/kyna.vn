<?php

namespace kyna\payment\lib\free;

use kyna\payment\lib\epay\cryptors\Cls;
use kyna\payment\lib\epay\cryptors\TripleDes;
use kyna\servicecaller\traits\SoapTrait;

class Free extends \kyna\payment\lib\BasePayment
{
    use SoapTrait;

    public function signIn()
    {
        return true;
    }

    public function pay($transId, $orderId, $amount, $params = false)
    {
        $responseObj = new \StdClass();
        $responseObj->status = 1;
        if ($amount > 0) {
            $responseObj->status = 0;
        }

        return $this->_return($responseObj, [
                    'transactionId' => $transId,
                    'transactionCode' => null,
                    'amount' => 0,
                    'responseString' => null,
                    'orderId' => $orderId,
        ]);
    }

    public function query($transId)
    {
        return $this->_return(true, [
                    'transactionId' => $transId,
                    'transactionCode' => null,
                    'responseString' => null,
                    'amount' => 0,
        ]);
    }

    public function calculateFee($amount, $param = null)
    {
        return 0;
    }

    public function ipn($response)
    {
        return 'IPN not supported';
    }

    private function _return($response, $returnArray)
    {
        $error = null;
        if ($response->status != 1) {
            $errorMap = $this->errorMap();
            $error = isset($errorMap[$response->status]) ? $errorMap[$response->status] : 'Unknown Error';
        }

        $return = ['error' => $error];
        foreach ($returnArray as $key => $value) {
            $responseValue = isset($response->$value) ? $response->$value : $value;
            $return[$key] = $responseValue;
        }

        return $return;
    }

    protected function errorMap()
    {
        return [
            0 => 'Fail',
            1 => 'Success',
        ];
    }
}
