<?php
/**
 * Created by PhpStorm.
 * User: ngunp
 * Date: 8/7/2017
 * Time: 11:09 AM
 */

namespace kyna\payment\lib\ghtk;

use kyna\user\models\User;
use Yii;
use kyna\payment\lib\BasePayment;
use kyna\servicecaller\traits\CurlTrait;
use yii\helpers\Json;
use yii\base\InvalidParamException;
use kyna\payment\lib\ghtk\Mapper;

use kyna\order\models\Order;
use kyna\payment\models\PaymentTransaction;
use kyna\payment\models\TopupTransaction;
use kyna\order\models\OrderInterface;


class Ghtk extends BasePayment
{
    use CurlTrait;

    const KYNA_TRANSACTION_PREFIX = "";
    const KYNA_TRANSACTION_FIELD = "transaction_code";

    const SHIPPING_STATUS__HUY_DON_HANG = -1;
    const SHIPPING_STATUS__CHUA_TIEP_NHAN = 1;
    const SHIPPING_STATUS__DA_TIEP_NHAN = 2;
    const SHIPPING_STATUS__DA_LAY_HANG = 3;
    const SHIPPING_STATUS__DANG_GIAO_HANG = 4;
    const SHIPPING_STATUS__DA_GIAO_HANG = 5;
    const SHIPPING_STATUS__DA_DOI_SOAT = 6;
    const SHIPPING_STATUS__KHONG_LAY_DUOC_HANG = 7;
    const SHIPPING_STATUS__HOAN_LAY_HANG = 8;
    const SHIPPING_STATUS__KHONG_GIAO_DUOC_HANG = 9;
    const SHIPPING_STATUS__DELAY_GIAO_HANG = 10;
    const SHIPPING_STATUS__DA_DOI_SOAT_CONG_NO_TRA_HANG = 11;
    const SHIPPING_STATUS__DANG_LAY_HANG = 12;
    const SHIPPING_STATUS__DANG_TRA_HANG = 20;
    const SHIPPING_STATUS__DA_TRA_HANG = 21;
    const SHIPPING_STATUS__SHIPPER_BAO_DA_LAY_HANG = 123;
    const SHIPPING_STATUS__SHIPPER_BAO_KHONG_LAY_DUOC_HANG = 127;
    const SHIPPING_STATUS__SHIPPER_BAO_DELAY_LAY_HANG = 128;
    const SHIPPING_STATUS__SHIPPER_BAO_DA_GIAO_HANG = 45;
    const SHIPPING_STATUS__SHIPPER_BAO_KHONG_GIAO_DUOC_HANG = 49;
    const SHIPPING_STATUS__SHIPPER_BAO_DELAY_GIAO_HANG = 410;

    public static $shippingStatusText = [
        self::SHIPPING_STATUS__HUY_DON_HANG => 'Hủy Đơn Hàng',
        self::SHIPPING_STATUS__CHUA_TIEP_NHAN => 'Chưa tiếp nhận',
        self::SHIPPING_STATUS__DA_TIEP_NHAN => 'Đã tiếp nhận',
        self::SHIPPING_STATUS__DA_LAY_HANG => 'Đã lấy hàng',
        self::SHIPPING_STATUS__DANG_GIAO_HANG => 'Đang giao hàng',
        self::SHIPPING_STATUS__DA_GIAO_HANG => 'Đã giao hàng',
        self::SHIPPING_STATUS__DA_DOI_SOAT => 'Đã đối soát',
        self::SHIPPING_STATUS__KHONG_LAY_DUOC_HANG => 'Không lấy được hàng',
        self::SHIPPING_STATUS__HOAN_LAY_HANG => 'Hoãn lấy hàng',
        self::SHIPPING_STATUS__KHONG_GIAO_DUOC_HANG => 'Không giao được hàng',
        self::SHIPPING_STATUS__DELAY_GIAO_HANG => 'Delay giao hàng',
        self::SHIPPING_STATUS__DA_DOI_SOAT_CONG_NO_TRA_HANG => 'Đã đối soát công nợ trả hàng',
        self::SHIPPING_STATUS__DANG_LAY_HANG => 'Đang lấy hàng',
        self::SHIPPING_STATUS__DANG_TRA_HANG => 'Đang trả hàng',
        self::SHIPPING_STATUS__DA_TRA_HANG => 'Đã trả hàng',
        self::SHIPPING_STATUS__SHIPPER_BAO_DA_LAY_HANG => 'Shippier báo đã lấy hàng',
        self::SHIPPING_STATUS__SHIPPER_BAO_KHONG_LAY_DUOC_HANG => 'Shipper báo không lấy được hàng',
        self::SHIPPING_STATUS__SHIPPER_BAO_DELAY_LAY_HANG => 'Shipper báo delay lấy hàng',
        self::SHIPPING_STATUS__SHIPPER_BAO_DA_GIAO_HANG => 'Shipper đã giao hàng',
        self::SHIPPING_STATUS__SHIPPER_BAO_KHONG_GIAO_DUOC_HANG => 'Shipper báo không giao được hàng',
        self::SHIPPING_STATUS__SHIPPER_BAO_DELAY_GIAO_HANG => 'Shipper báo delay giao hàng',
    ];


    private $_apiUrl;
    private $_pickUps;
    private $_token;
    private $mapper;
    public $isCod = true;


    public function __construct()
    {
        if (isset(Yii::$app->params['ghtk']) && $configs = Yii::$app->params['ghtk']) {
            $this->_apiUrl = $configs['_apiUrl'];
            $this->_pickUps = $configs['_pickUps'];
            $this->_token = $configs['_token'];
            $this->mapper = new Mapper();
        } else {
            // khong co trong param thi throw exception
            throw new \Exception("Cant find proship config - please re-check app params");
        }
    }

    // --------------------------------BASE PAYMENT FUNCTIONS--------------------------------------
    public function pay($transId, $orderId, $amount, $params = false)
    {
        $district = $this->mapper->getLocationAlias($params['location_id']);
        $city = $this->mapper->getLocationAlias($params['city_id']);

        // Check if city, district in on service
        if ($city == -1 || $district == -1) {
            $filtered_result['error'] = 'GHTK không hỗ trợ địa điểm này';
            return $filtered_result;
        }

        // Check if district is supported
        if ($district['is_supported'] === false) {
            $filtered_result['error'] = 'GHTK không hỗ trợ địa điểm này';
            return $filtered_result;
        }

        // Validate city code / district code
        if (!self::_checkIsValidLocation($city['ghtk_name'], $district['ghtk_id'])) {
            $filtered_result['error'] = 'GHTK không hỗ trợ địa điểm này';
            return $filtered_result;
        }

        // Get Pickup Id & address
        if (isset($params) && isset($params['pick_up_location']))
            $senderId = $params['pick_up_location'];
        else
            $senderId = $city['pickup_id'];
        $sender = $this->mapper->getPickUpFromId($senderId);
        if (empty($sender)) {
            $filtered_result['error'] = 'GHTK không hỗ trợ địa điểm này';
            return $filtered_result;
        }


        // Get User
        $order = Order::findOne([
            'id' => $orderId,
        ]);
        if (empty($order)) {
            $filtered_result['error'] = 'Đơn hàng không tồn tại';
            return $filtered_result;
        }
        $user = User::findOne([
            'id' => $order->user_id,
        ]);
        if (empty($user)) {
            $filtered_result['error'] = 'User không tồn tại';
            return $filtered_result;
        }

        // validate other things here

        // Prepare input
        $input = [
            'product_name' => 'Đơn hàng #' . $orderId . '. Phiếu đăng ký khóa học trên Kyna.vn',
            'id' => self::KYNA_TRANSACTION_PREFIX . $transId,
            'pick_money' => $amount,
            'pick_province' => $sender['name'],
            'pick_district' => $sender['districtName'],
            'pick_address' => $sender['address'],
            'pick_tel' => $sender['tel'],

            'name' => $params['contact_name'],
            'address' => $params['street_address'],
            'province_name' => $city['ghtk_name'],
            'district_name' => $district['ghtk_name'],
            'tel' => $params['phone_number'],
            'email' => $user['email'],
        ];
        // Call Add Order API
        $result = self::addOrder($input);


        // Filter result:
        $filtered_result = [];
        $filtered_result['error'] = null;
        if ($result['success'] != true) {
            // Error
            $filtered_result['error'] = $result['message'];
        } else {
            // Success
            $filtered_result['transactionId'] = $transId;
            $filtered_result['transactionCode'] = $result['order']['label'];
            $filtered_result['amount'] = $amount;
            $filtered_result['responseString'] = http_build_query($result);
            $filtered_result['orderId'] = $orderId;
            $filtered_result['fee'] = $result['order']['fee'] + $result['order']['insurance_fee'];

            // Update shipping_status, pick_up_location_id, expected_payment_fee - NOT avalable for GHN
            $transaction = PaymentTransaction::findOne([
                'id' => $transId,
            ]);
            if (isset($transaction)) {
                // Get shipping status
                $res = $this->getShippingStatus(['transaction_code' => $result['order']['label']]);
//                var_dump($transaction['transaction_code']); die();
                if ($res['success'] == true) {
//                    var_dump($res);
//                $transaction->updateAttributes(['shipping_status' => $result['data']['invoiceStatus']]);
//                $transaction['shipping_status'] = $result['data']['invoiceStatus'];
                    $transaction['shipping_status'] = $res['order']['status'];
                }
                $transaction['pick_up_location_id'] = $senderId;
                if (isset($params['expected_payment_fee']))
                    $transaction['expected_payment_fee'] = $params['expected_payment_fee'];
//                $transaction['bill_code'] = $result['data']['billCode'];

                $transaction->save();
            }
        }
        return $filtered_result;
    }

    public function query($transId)
    {
        return false;
    }

    public function ipn($response)
    {
        return false;
    }

    public function calculateFee($params)
    {
        return $this->getShippingFee($params);
    }

    public function signIn()
    {
        // TODO: Implement signIn() method.
        // do nothing,
    }
    // -----------------------------END BASE PAYMENT FUNCTIONS-------------------------------------

    // -------------------------------------API FUNCTIONS------------------------------------------
    public function getSupportedList()
    {
        $command = '/services/address/getSupports';
        $endpoint = $this->_buildUrl($command);
        $response = $this->call($endpoint);
        return $response;
    }

    public function addOrder($params)
    {
        $command = '/services/shipment/order';
        $_params = [
            'products' => [
                [
                    'name' => $params['product_name'],                  // product name
                    'weight' => 0.1,                                    // 100grams
                ],
            ],
            'order' => [
                'id' => $params['id'],                              // order id
                'pick_name' => 'KYNA',
                'pick_money' => $params['pick_money'],              // order's price
//                'pick_address_id' => $params['pick_address_id'],    // pick_up id
                'pick_address' => $params['pick_address'],
                'pick_district' => $params['pick_district'],
                'pick_province' => $params['pick_province'],
                'pick_tel' => $params['pick_tel'],

                'name' => $params['name'],                          // ten nguoi nhan
                'address' => $params['address'],                    // dia chi Nguoi nhan hang
                'province' => $params['province_name'],              // Ten Tinh/Tp
                'district' => $params['district_name'],             // Ten Quan/Huyen
                'tel' => $params['tel'],                            // SDT
                'email' => $params['email'],                        // Email
                'is_freeship' => 1,
                'note' => 'Không cho xem hàng'

            ],
        ];
        $endpoint = $this->_buildUrl($command);
        $response = $this->call($endpoint, $_params, 'POST');
        return $response;
    }

    public function getShippingFee($params)
    {
        $command = '/services/shipment/fee';
        $_params = [
//            'pick_address_id' => $params['pick_address_id'],
            'pick_province' => $params['pick_province'],
            'pick_district' => $params['pick_district'],

            'province' => $params['province_name'],
            'district' => $params['district_name'],
            'address' => '',
            'value' => $params['value'],                        // not required
            'weight' => 100,                                // 100grams
        ];
        $endpoint = $this->_buildUrl($command, $_params);
        $response = $this->call($endpoint);
        return $response;
    }

    public function getShippingStatus($params)
    {
        $command = '/services/shipment/' . $params['transaction_code'];

        $endpoint = $this->_buildUrl($command);
        $response = $this->call($endpoint);
        return $response;
    }

    public function cancelOrder($params)
    {
        $command = '/services/shipment/cancel/' . $params['transaction_code'];
        $endpoint = $this->_buildUrl($command);
        $response = $this->call($endpoint, null, 'POST');
        return $response;
    }

    // --------------------------------END API FUNCTIONS-------------------------------------------

    // ---------------------------------OTHER FUNCTIONS--------------------------------------------
    public function getFeeList($params)
    {
        $formatter = Yii::$app->formatter;

        // get ghtk city/district
        $city = $this->mapper->getLocationAlias($params['cityId']);
        $district = $this->mapper->getLocationAlias($params['districtId']);

        // Validate city/district
        $isValidated = $this->_checkIsValidLocation($city['ghtk_name'], $district['ghtk_id']);
        if (!$isValidated)
            return false;

        // Get Pickups List
        $pickUpList = $this->_pickUps;

        // Get codCost
        $order = Order::findOne([
            'id' => $params['orderId']
        ]);
        if (!isset($order))
            return false;
        $codCost = (isset($order['total'])) ? $order['total'] : 0;
        $result = [];
        foreach ($pickUpList as $pickUp) {
            $input = [
//                'pick_address_id' => $pickUp['id'],
                'pick_province' => $pickUp['name'],
                'pick_district' => $pickUp['districtName'],
                'province_name' => $city['ghtk_name'],
                'district_name' => $district['ghtk_name'],
                'value' => $codCost,
                'weight' => 100,                                // 100grams
            ];
            $res = $this->getShippingFee($input);
            if (!isset($res))
                continue;
            if ($res['success'] != true)
                continue;
            $result[] = [
                'id' => $pickUp['id'],
                'name' => $pickUp['name'],
                'fee' => $res['fee']['fee'] + $res['fee']['insurance_fee'],
                'displayFee' => $formatter->asCurrency($res['fee']['fee'] + $res['fee']['insurance_fee']),
            ];

        }
        return $result;
    }

    public function updateShippingStatus($params)
    {
        // Validate params here
        if (!isset($params['status']))
            return false;
        // Get payment_transaction
        $transaction = PaymentTransaction::find()->where([
            'transaction_code' => $params['transaction_code'],
        ])->one();
        // Get order
        $order = Order::find()->where([
            'id' => $transaction['order_id'],
        ])->one();

        $result['transaction'] = $transaction;
        $result['order'] = $order;
        switch ($params['status']) {
            case self::SHIPPING_STATUS__CHUA_TIEP_NHAN:
            case self::SHIPPING_STATUS__DA_TIEP_NHAN:
            case self::SHIPPING_STATUS__DANG_LAY_HANG:
            case self::SHIPPING_STATUS__DA_LAY_HANG:
            case self::SHIPPING_STATUS__DANG_GIAO_HANG:
                // Update paymnet_transaction
                $transaction['shipping_status'] = $params['status'];
                $transaction['status'] = TopupTransaction::STATUS_SUCCESS;
                if (isset($params['fee']))
                    $transaction['payment_fee'] = $params['fee'];
                $transaction->save();
                // Update order
                if ($order['status'] != OrderInterface::ORDER_STATUS_COMPLETE) {
//                    $order['payment_method'] = 'cod';
//                    $order['shipping_method'] = 'ghtk';
                    $order['status'] = OrderInterface::ORDER_STATUS_DELIVERING;
                    $order->save();
                }
                break;
            case self::SHIPPING_STATUS__HUY_DON_HANG:
                // Update payment_transaction
                $transaction['shipping_status'] = $params['status'];
                $transaction['status'] = TopupTransaction::STATUS_FAIL;
                $transaction->save();
                // Update order
                if ($order['status'] != OrderInterface::ORDER_STATUS_COMPLETE) {
                    $order['payment_method'] = 'cod';   // default
                    $order['shipping_method'] = null;
                    $order['status'] = OrderInterface::ORDER_STATUS_PENDING_CONTACT;
                    $order->save();
                }
                break;
            case self::SHIPPING_STATUS__DANG_TRA_HANG:
                // Update payment_transaction
                $transaction['shipping_status'] = $params['status'];
                $transaction['status'] = TopupTransaction::STATUS_SUCCESS;
                if (isset($params['fee']))
                    $transaction['payment_fee'] = $params['fee'];
                $transaction->save();
                // Update order
                if ($order['status'] != OrderInterface::ORDER_STATUS_COMPLETE) {
                    $order['status'] = OrderInterface::ORDER_STATUS_WAITING_RETURN;
                    $order->save();
                }
                break;
            case self::SHIPPING_STATUS__DA_TRA_HANG:
                // Update payment_transaction
                $transaction['shipping_status'] = $params['status'];
                $transaction['status'] = TopupTransaction::STATUS_SUCCESS;
                if (isset($params['fee']))
                    $transaction['payment_fee'] = $params['fee'];
                $transaction->save();
                if ($order['status'] != OrderInterface::ORDER_STATUS_COMPLETE) {
                    Order::codReturn($order['id']);
                }
                break;
            case self::SHIPPING_STATUS__DA_GIAO_HANG:
                // Update payment_transaction
                $transaction['shipping_status'] = $params['status'];
                $transaction['status'] = TopupTransaction::STATUS_SUCCESS;
                $transaction['is_call'] = TopupTransaction::BOOL_YES;
                if (isset($params['fee']))
                    $transaction['payment_fee'] = $params['fee'];
                $transaction->save();
                if ($order['status'] != OrderInterface::ORDER_STATUS_COMPLETE) {
                    Order::complete($order['id'], $order['total']);
                }
                break;
            default:
                $transaction['shipping_status'] = $params['status'];
                $transaction->save();
                break;
        }
        return $result;
    }

    protected function beforeCall(&$ch, &$data, $method = false)
    {
        // do nothing
        // TODO: init Curl Handler and preserve data for fucntion calling
        // Json encode data
        if (isset($data))
            $data = json_encode($data);
        else
            $data = json_encode([]);

        // Add
        switch ($method) {
            case 'POST':
                curl_setopt(
                    $ch,
                    CURLOPT_HTTPHEADER,
                    [
                        'Content-Type: application/json',
                        'Token: ' . $this->_token,
                    ]
                );
                break;
            case 'PUT':
                curl_setopt(
                    $ch,
                    CURLOPT_HTTPHEADER,
                    [
                        'Content-Type: application/json',
                        'Content-Length: ' . strlen($data)
                    ]
                );
                break;
            case 'GET':
                // no break
            default:
                curl_setopt(
                    $ch,
                    CURLOPT_HTTPHEADER,
                    [
                        'Token: ' . $this->_token,
                    ]
                );
                break;
        }
    }

    protected function afterCall(&$response)
    {
        try {
            $response = Json::decode($response, true);
        } catch (InvalidParamException $e) {
            echo $response;
        }
    }

    private function _buildUrl($command, $params = false)
    {
        $url = $this->_apiUrl . $command;
        if ($params) {
            $url .= '?' . http_build_query($params);
        }
        return $url;
    }

    private function _checkIsValidLocation($cityName, $districtId)
    {
        // Get List of city code from Proship - check if citySettings exist in cityList
        $isValid = false;
        $_city = null;
        $supportedList = self::getSupportedList();
        foreach ($supportedList['data'] as $city) {
            if ($city['province'] == $cityName) {
                $_city = $city;
                $isValid = true;
                break;
            }
        }

        if (!$isValid)
            return false;
        // Get List of district code from Proship - check if districtSettings exist in districList
        $isValid = false;

        foreach ($_city['districts'] as $district) {
            if ($district['code'] == $districtId) {
                $isValid = true;
                break;
            }
        }
        return $isValid;
    }

    public function validateIpnMessage($params)
    {
        $hash = Yii::$app->params['ghtk']['_hash'];
        if (empty($params) || empty($params['label_id']) || empty($params['partner_id']) || empty($params['status_id']))
            return false;
        // Wrong hash return false
        if (!isset($params['hash']) || $params['hash'] != $hash)
            return false;

        // Transaction code & transaction id not match return false
        $transaction = PaymentTransaction::findOne([
            'transaction_code' => $params['label_id'],
        ]);
        if (empty($transaction))
            return false;
        if ($transaction['id'] != $params['partner_id'])
            return false;

        return true;
    }

    public static function getShippingStatusText($shippingStatusInt)
    {
        if (isset(self::$shippingStatusText[$shippingStatusInt]))
            return self::$shippingStatusText[$shippingStatusInt];
        else
            return 'unknown';
    }

    public function isSupportLocation($locationId)
    {
        $district = $this->mapper->getLocationAlias($locationId);
        if ($district == -1)
            return false;
        return $district['is_supported'];
    }

}