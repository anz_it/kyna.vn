<?php
/**
 * Created by PhpStorm.
 * User: ngunp
 * Date: 8/8/2017
 * Time: 11:12 AM
 */

namespace kyna\payment\lib\ghtk;

use Yii;

class Mapper
{
    private $defaultSenderId = null;
    private $locationAlias = [];

    // Get Location Alias
    public function getLocationAlias($id)
    {
        return isset($this->locationAlias[$id])? $this->locationAlias[$id] : -1;
    }

    public function getPickUpFromId($id)
    {
        foreach (Yii::$app->params['ghtk']['_pickUps'] as $key => $pickUp) {
            if ($pickUp['id'] == $id) {
                return $pickUp;
            }
        }
        return null;
    }

    public function __construct()
    {
        if (!isset(Yii::$app->params['ghtk'])
            || !isset(Yii::$app->params['ghtk']['_pickUps']))
            throw new \Exception("Cant find proship config - please re-check app params");

        $this->defaultSenderId = array_values(Yii::$app->params['ghtk']['_pickUps'])[0]['id'];


        $this->locationAlias = [
            // Province/city
            //      Pattern
            // <DB location ID> => ["ghtk_id" => <ghtk location Id>, "ghtk_name" => <ghtk_name>, "pickup_id" => <pickup_id>, "is_supported" => <true/false>], // City name

            // Province => id = null
            "1" => ["ghtk_id" => null, "ghtk_name" => "An Giang", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "2" => ["ghtk_id" => null, "ghtk_name" => "Bà Rịa - Vũng Tàu", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "3" => ["ghtk_id" => null, "ghtk_name" => "Bạc Liêu", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "4" => ["ghtk_id" => null, "ghtk_name" => "Bắc Kạn", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "5" => ["ghtk_id" => null, "ghtk_name" => "Bắc Giang", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "6" => ["ghtk_id" => null, "ghtk_name" => "Bắc Ninh", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "7" => ["ghtk_id" => null, "ghtk_name" => "Bến Tre", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "8" => ["ghtk_id" => null, "ghtk_name" => "Bình Dương", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "9" => ["ghtk_id" => null, "ghtk_name" => "Bình Định", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "10" => ["ghtk_id" => null, "ghtk_name" => "Bình Phước", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "11" => ["ghtk_id" => null, "ghtk_name" => "Bình Thuận", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "12" => ["ghtk_id" => null, "ghtk_name" => "Cà Mau", "pickup_id" => $this->defaultSenderId, "is_supported" => true],

            "13" => ["ghtk_id" => null, "ghtk_name" => "Cao Bằng", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "14" => ["ghtk_id" => null, "ghtk_name" => "Cần Thơ", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "15" => ["ghtk_id" => null, "ghtk_name" => "Đà Nẵng", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "16" => ["ghtk_id" => null, "ghtk_name" => "Đắk Lắk", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "17" => ["ghtk_id" => null, "ghtk_name" => "Đắk Nông", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "18" => ["ghtk_id" => null, "ghtk_name" => "Đồng Nai", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "19" => ["ghtk_id" => null, "ghtk_name" => "Đồng Tháp", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "20" => ["ghtk_id" => null, "ghtk_name" => "Điện Biên", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "21" => ["ghtk_id" => null, "ghtk_name" => "Gia Lai", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "22" => ["ghtk_id" => null, "ghtk_name" => "Hà Giang", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "23" => ["ghtk_id" => null, "ghtk_name" => "Hà Nam", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "24" => ["ghtk_id" => null, "ghtk_name" => "Hà Nội", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "25" => ["ghtk_id" => null, "ghtk_name" => "Hà Tĩnh", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "26" => ["ghtk_id" => null, "ghtk_name" => "Hải Dương", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "27" => ["ghtk_id" => null, "ghtk_name" => "Hải Phòng", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "28" => ["ghtk_id" => null, "ghtk_name" => "Hòa Bình", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "29" => ["ghtk_id" => null, "ghtk_name" => "Hậu Giang", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "30" => ["ghtk_id" => null, "ghtk_name" => "Hưng Yên", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "31" => ["ghtk_id" => null, "ghtk_name" => "TP Hồ Chí Minh", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "32" => ["ghtk_id" => null, "ghtk_name" => "Khánh Hòa", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "33" => ["ghtk_id" => null, "ghtk_name" => "Kiên Giang", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "34" => ["ghtk_id" => null, "ghtk_name" => "Kon Tum", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "35" => ["ghtk_id" => null, "ghtk_name" => "Lai Châu", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "36" => ["ghtk_id" => null, "ghtk_name" => "Lào Cai", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "37" => ["ghtk_id" => null, "ghtk_name" => "Lạng Sơn", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "38" => ["ghtk_id" => null, "ghtk_name" => "Lâm Đồng", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "39" => ["ghtk_id" => null, "ghtk_name" => "Long An", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "40" => ["ghtk_id" => null, "ghtk_name" => "Nam Định", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "41" => ["ghtk_id" => null, "ghtk_name" => "Nghệ An", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "42" => ["ghtk_id" => null, "ghtk_name" => "Ninh Bình", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "43" => ["ghtk_id" => null, "ghtk_name" => "Ninh Thuận", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "44" => ["ghtk_id" => null, "ghtk_name" => "Phú Thọ", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "45" => ["ghtk_id" => null, "ghtk_name" => "Phú Yên", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "46" => ["ghtk_id" => null, "ghtk_name" => "Quảng Bình", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "47" => ["ghtk_id" => null, "ghtk_name" => "Quảng Nam", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "48" => ["ghtk_id" => null, "ghtk_name" => "Quảng Ngãi", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "49" => ["ghtk_id" => null, "ghtk_name" => "Quảng Ninh", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "50" => ["ghtk_id" => null, "ghtk_name" => "Quảng Trị", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "51" => ["ghtk_id" => null, "ghtk_name" => "Sóc Trăng", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "52" => ["ghtk_id" => null, "ghtk_name" => "Sơn La", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "53" => ["ghtk_id" => null, "ghtk_name" => "Tây Ninh", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "54" => ["ghtk_id" => null, "ghtk_name" => "Thái Bình", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "55" => ["ghtk_id" => null, "ghtk_name" => "Thái Nguyên", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "56" => ["ghtk_id" => null, "ghtk_name" => "Thanh Hóa", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "57" => ["ghtk_id" => null, "ghtk_name" => "Thừa Thiên Huế", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "58" => ["ghtk_id" => null, "ghtk_name" => "Tiền Giang", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "59" => ["ghtk_id" => null, "ghtk_name" => "Trà Vinh", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "60" => ["ghtk_id" => null, "ghtk_name" => "Tuyên Quang", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "61" => ["ghtk_id" => null, "ghtk_name" => "Vĩnh Long", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "62" => ["ghtk_id" => null, "ghtk_name" => "Vĩnh Phúc", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "63" => ["ghtk_id" => null, "ghtk_name" => "Yên Bái", "pickup_id" => $this->defaultSenderId, "is_supported" => true],


            // District
                // An Giang
            // "1" => ["ghtk_id" => null, "ghtk_name" => "An Giang", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "72" => ["ghtk_id" =>  "1469", "ghtk_name" => "Huyện An Phú", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "169" => ["ghtk_id" =>  "1470", "ghtk_name" => "Huyện Châu Phú", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "170" => ["ghtk_id" =>  "1471", "ghtk_name" => "Huyện Châu Thành", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "188" => ["ghtk_id" =>  "1472", "ghtk_name" => "Huyện Chợ Mới", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "515" => ["ghtk_id" =>  "1473", "ghtk_name" => "Huyện Phú Tân", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "658" => ["ghtk_id" =>  "1474", "ghtk_name" => "Huyện Thoại Sơn", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "683" => ["ghtk_id" =>  "1475", "ghtk_name" => "Huyện Tịnh Biên", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "696" => ["ghtk_id" =>  "1476", "ghtk_name" => "Huyện Tri Tôn", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "411" => ["ghtk_id" =>  "1477", "ghtk_name" => "Thành phố Long Xuyên", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "167" => ["ghtk_id" =>  "1478", "ghtk_name" => "Thành Phố Châu Đốc", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "605" => ["ghtk_id" =>  "1479", "ghtk_name" => "Thị xã Tân Châu", "pickup_id" => $this->defaultSenderId, "is_supported" => true],

                // Ba Ria - Vung Tau
            // "2" => ["ghtk_id" => null, "ghtk_name" => "Bà Rịa - Vũng Tàu", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "168" => ["ghtk_id" => "1480", "ghtk_name" => "Huyện Châu Đức",  "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "197" => ["ghtk_id" => "1481", "ghtk_name" => "Huyện Côn Đảo",  "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "237" => ["ghtk_id" => "1482", "ghtk_name" => "Huyện Đất Đỏ",  "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "404" => ["ghtk_id" => "1483", "ghtk_name" => "Huyện Long Điền",  "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "617" => ["ghtk_id" => "1484", "ghtk_name" => "Huyện Tân Thành",  "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "759" => ["ghtk_id" => "1485", "ghtk_name" => "Huyện Xuyên Mộc",  "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "755" => ["ghtk_id" => "1486", "ghtk_name" => "Thành phố Vũng Tàu",  "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "83" => ["ghtk_id" => "1487", "ghtk_name" => "Thành Phố Bà Rịa",  "pickup_id" => $this->defaultSenderId, "is_supported" => true],

                // Bac Lieu
            // "3" => ["ghtk_id" => null, "ghtk_name" => "Bạc Liêu", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "251" => ["ghtk_id" => "1506", "ghtk_name" => "Huyện Đông Hải", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "279" => ["ghtk_id" => "1507", "ghtk_name" => "Thị Xã Giá Rai", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "316" => ["ghtk_id" => "1508", "ghtk_name" => "Huyện Hòa Bình", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "335" => ["ghtk_id" => "1509", "ghtk_name" => "Huyện Hồng Dân", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "532" => ["ghtk_id" => "1510", "ghtk_name" => "Huyện Phước Long", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "744" => ["ghtk_id" => "1511", "ghtk_name" => "Huyện Vĩnh Lợi", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "86" => ["ghtk_id" => "1512", "ghtk_name" => "Thành Phố Bạc Liêu", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
//            "279" => ["ghtk_id" => "8033", "ghtk_name" => "Thị xã Giá Rai", "pickup_id" => $this->defaultSenderId, "is_supported" => false],

                // Bac Kan
            // "4" => ["ghtk_id" => null, "ghtk_name" => "Bắc Kạn", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "76" => ["ghtk_id" => "1498", "ghtk_name" => "Huyện Ba Bể", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "88" => ["ghtk_id" => "1499", "ghtk_name" => "Huyện Bạch Thông", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "184" => ["ghtk_id" => "1500", "ghtk_name" => "Huyện Chợ Đồn", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "187" => ["ghtk_id" => "1501", "ghtk_name" => "Huyện Chợ Mới", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "453" => ["ghtk_id" => "1502", "ghtk_name" => "Huyện Na Rì", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "468" => ["ghtk_id" => "1503", "ghtk_name" => "Huyện Ngân Sơn", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "498" => ["ghtk_id" => "1504", "ghtk_name" => "Huyện Pác Nặm", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "100" => ["ghtk_id" => "1505", "ghtk_name" => "Thành phố Bắc Kạn", "pickup_id" => $this->defaultSenderId, "is_supported" => true],

                // Bac Giang
            // "5" => ["ghtk_id" => null, "ghtk_name" => "Bắc Giang", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "313" => ["ghtk_id" => "1488", "ghtk_name" => "Huyện Hiệp Hòa", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "391" => ["ghtk_id" => "1489", "ghtk_name" => "Huyện Lạng Giang", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "415" => ["ghtk_id" => "1490", "ghtk_name" => "Huyện Lục Nam", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "416" => ["ghtk_id" => "1491", "ghtk_name" => "Huyện Lục Ngạn", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "584" => ["ghtk_id" => "1492", "ghtk_name" => "Huyện Sơn Động", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "622" => ["ghtk_id" => "1493", "ghtk_name" => "Huyện Tân Yên", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "735" => ["ghtk_id" => "1494", "ghtk_name" => "Huyện Việt Yên", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "764" => ["ghtk_id" => "1495", "ghtk_name" => "Huyện Yên Dũng", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "775" => ["ghtk_id" => "1496", "ghtk_name" => "Huyện Yên Thế", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "98" => ["ghtk_id" => "1497", "ghtk_name" => "Thành phố Bắc Giang", "pickup_id" => $this->defaultSenderId, "is_supported" => true],

                // Bac Ninh
            // "6" => ["ghtk_id" => null, "ghtk_name" => "Bắc Ninh", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "274" => ["ghtk_id" => "1513", "ghtk_name" => "Huyện Gia Bình", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "419" => ["ghtk_id" => "1514", "ghtk_name" => "Huyện Lương Tài", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "561" => ["ghtk_id" => "1515", "ghtk_name" => "Huyện Quế Võ", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "671" => ["ghtk_id" => "1516", "ghtk_name" => "Huyện Thuận Thành", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "675" => ["ghtk_id" => "1517", "ghtk_name" => "Huyện Tiên Du", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "772" => ["ghtk_id" => "1518", "ghtk_name" => "Huyện Yên Phong", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "102" => ["ghtk_id" => "1519", "ghtk_name" => "Thành phố Bắc Ninh", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "714" => ["ghtk_id" => "1520", "ghtk_name" => "Thị xã Từ Sơn", "pickup_id" => $this->defaultSenderId, "is_supported" => true],

                // Ben Tre
            // "7" => ["ghtk_id" => null, "ghtk_name" => "Bến Tre", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "81" => ["ghtk_id" => "1521", "ghtk_name" => "Huyện Ba Tri", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "116" => ["ghtk_id" => "1522", "ghtk_name" => "Huyện Bình Đại", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "171" => ["ghtk_id" => "1523", "ghtk_name" => "Huyện Châu Thành", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "186" => ["ghtk_id" => "1524", "ghtk_name" => "Huyện Chợ Lách", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "284" => ["ghtk_id" => "1525", "ghtk_name" => "Huyện Giồng Trôm", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "431" => ["ghtk_id" => "1526", "ghtk_name" => "Huyện Mỏ Cày Bắc", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "432" => ["ghtk_id" => "1527", "ghtk_name" => "Huyện Mỏ Cày Nam", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "652" => ["ghtk_id" => "1528", "ghtk_name" => "Huyện Thạnh Phú", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "112" => ["ghtk_id" => "1529", "ghtk_name" => "Thành phố Bến Tre", "pickup_id" => $this->defaultSenderId, "is_supported" => true],

                // Binh Duong
            // "8" => ["ghtk_id" => null, "ghtk_name" => "Bình Dương", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "664" => ["ghtk_id" => "1100", "ghtk_name" => "Thủ Dầu Một", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "667" => ["ghtk_id" => "1101", "ghtk_name" => "Thị Xã Thuận An", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "208" => ["ghtk_id" => "1102", "ghtk_name" => "Thị Xã Dĩ An", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "109" => ["ghtk_id" => "1103", "ghtk_name" => "Thị Xã Bến Cát", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "620" => ["ghtk_id" => "1104", "ghtk_name" => "Thị Xã Tân Uyên", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "96" => ["ghtk_id" => "1105", "ghtk_name" => "Huyện Bàu Bàng", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "206" => ["ghtk_id" => "2086", "ghtk_name" => "Huyện Dầu Tiếng", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "506" => ["ghtk_id" => "2087", "ghtk_name" => "Huyện Phú Giáo", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "105" => ["ghtk_id" => "2107", "ghtk_name" => "Bắc Tân Uyên", "pickup_id" => $this->defaultSenderId, "is_supported" => true],

                // Binh Dinh
            // "9" => ["ghtk_id" => null, "ghtk_name" => "Bình Định", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "69" => ["ghtk_id" => "1530", "ghtk_name" => "Huyện An Lão", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
//            "71" => ["ghtk_id" => "1531", "ghtk_name" => "Thị xã An Nhơn", "pickup_id" => $this->defaultSenderId, "is_supported" => false],           // trung => uu tien  support = true
            "320" => ["ghtk_id" => "1532", "ghtk_name" => "Huyện Hoài  Ân", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "322" => ["ghtk_id" => "1533", "ghtk_name" => "Huyện Hoài Nhơn", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "523" => ["ghtk_id" => "1534", "ghtk_name" => "Huyện Phù Mỹ", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "521" => ["ghtk_id" => "1535", "ghtk_name" => "Huyện Phù Cát", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "627" => ["ghtk_id" => "1536", "ghtk_name" => "Huyện Tây Sơn", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "709" => ["ghtk_id" => "1537", "ghtk_name" => "Huyện Tuy Phước", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "728" => ["ghtk_id" => "1538", "ghtk_name" => "Huyện Vân Canh", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "745" => ["ghtk_id" => "1539", "ghtk_name" => "Huyện Vĩnh Thạnh", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "562" => ["ghtk_id" => "1540", "ghtk_name" => "Thành phố Quy Nhơn", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "71" => ["ghtk_id" => "14988", "ghtk_name" => "Thị Xã An Nhơn", "pickup_id" => $this->defaultSenderId, "is_supported" => true],

                // Binh Phuoc
            // "10" => ["ghtk_id" => null, "ghtk_name" => "Bình Phước", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "130" => ["ghtk_id" => "1541", "ghtk_name" => "Huyện Bù Đăng", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "131" => ["ghtk_id" => "1542", "ghtk_name" => "Huyện Bù Đốp", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "132" => ["ghtk_id" => "1543", "ghtk_name" => "Huyện Bù Gia Mập", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "189" => ["ghtk_id" => "1544", "ghtk_name" => "Huyện Chơn Thành", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "258" => ["ghtk_id" => "1545", "ghtk_name" => "Huyện Đồng Phú", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "332" => ["ghtk_id" => "1546", "ghtk_name" => "Huyện Hớn Quản", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "414" => ["ghtk_id" => "1547", "ghtk_name" => "Huyện Lộc Ninh", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "120" => ["ghtk_id" => "1548", "ghtk_name" => "Thị xã Bình Long", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "260" => ["ghtk_id" => "1549", "ghtk_name" => "Thị xã Đồng Xoài", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "531" => ["ghtk_id" => "1550", "ghtk_name" => "Thị xã Phước Long", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "514" => ["ghtk_id" => "2106", "ghtk_name" => "Phú Riềng", "pickup_id" => $this->defaultSenderId, "is_supported" => true],

                // Binh Thuan
            // "11" => ["ghtk_id" => null, "ghtk_name" => "Bình Thuận", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "267" => ["ghtk_id" => "1551", "ghtk_name" => "Huyện  Đức Linh", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "97" => ["ghtk_id" => "1552", "ghtk_name" => "Huyện Bắc Bình", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "307" => ["ghtk_id" => "1553", "ghtk_name" => "Huyện Hàm Tân", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "308" => ["ghtk_id" => "1554", "ghtk_name" => "Huyện Hàm Thuận Bắc", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "309" => ["ghtk_id" => "1555", "ghtk_name" => "Huyện Hàm Thuận Nam", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "512" => ["ghtk_id" => "1556", "ghtk_name" => "Huyện Phú Qúi", "pickup_id" => $this->defaultSenderId, "is_supported" => false],
            "601" => ["ghtk_id" => "1557", "ghtk_name" => "Huyện Tánh Linh", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "708" => ["ghtk_id" => "1558", "ghtk_name" => "Huyện Tuy Phong", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "500" => ["ghtk_id" => "1559", "ghtk_name" => "Thành phố Phan Thiết", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "384" => ["ghtk_id" => "1560", "ghtk_name" => "Thị xã La Gi", "pickup_id" => $this->defaultSenderId, "is_supported" => true],

                // Ca Mau
            // "12" => ["ghtk_id" => null, "ghtk_name" => "Cà Mau", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "140" => ["ghtk_id" => "1561", "ghtk_name" => "Huyện Cái Nước", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "234" => ["ghtk_id" => "1562", "ghtk_name" => "Huyện Đầm Dơi", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "462" => ["ghtk_id" => "1563", "ghtk_name" => "Huyện Năm Căn", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "475" => ["ghtk_id" => "1564", "ghtk_name" => "Huyện Ngọc Hiển", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "516" => ["ghtk_id" => "1565", "ghtk_name" => "Huyện Phú Tân", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "662" => ["ghtk_id" => "1566", "ghtk_name" => "Huyện Thới Bình", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "695" => ["ghtk_id" => "1567", "ghtk_name" => "Huyện Trần Văn Thời", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "716" => ["ghtk_id" => "1568", "ghtk_name" => "Huyện U Minh", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "136" => ["ghtk_id" => "1569", "ghtk_name" => "Thành phố Cà Mau", "pickup_id" => $this->defaultSenderId, "is_supported" => true],

                // Cao Bang
            // "13" => ["ghtk_id" => null, "ghtk_name" => "Cao Bằng", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "89" => ["ghtk_id" => "1570", "ghtk_name" => "Huyện Bảo Lạc", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "90" => ["ghtk_id" => "1571", "ghtk_name" => "Huyện Bảo Lâm", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "298" => ["ghtk_id" => "1572", "ghtk_name" => "Huyện Hạ Lang", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "293" => ["ghtk_id" => "1573", "ghtk_name" => "Huyện Hà Quảng", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "315" => ["ghtk_id" => "1574", "ghtk_name" => "Huyện Hòa An", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "480" => ["ghtk_id" => "1575", "ghtk_name" => "Huyện Nguyên Bình", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "529" => ["ghtk_id" => "1576", "ghtk_name" => "Huyện Phục Hòa", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "545" => ["ghtk_id" => "1577", "ghtk_name" => "Huyện Quảng Uyên", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "629" => ["ghtk_id" => "1578", "ghtk_name" => "Huyện Thạch An", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "659" => ["ghtk_id" => "1579", "ghtk_name" => "Huyện Thông Nông", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "686" => ["ghtk_id" => "1580", "ghtk_name" => "Huyện Trà Lĩnh", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "699" => ["ghtk_id" => "1581", "ghtk_name" => "Huyện Trùng Khánh", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "147" => ["ghtk_id" => "1582", "ghtk_name" => "Thành Phố Cao Bằng", "pickup_id" => $this->defaultSenderId, "is_supported" => true],

                // Can Tho
            // "14" => ["ghtk_id" => null, "ghtk_name" => "Cần Thơ", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "199" => ["ghtk_id" => "1460", "ghtk_name" => "Huyện Cờ Đỏ", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "502" => ["ghtk_id" => "1461", "ghtk_name" => "Huyện Phong Điền", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "663" => ["ghtk_id" => "1462", "ghtk_name" => "Huyện Thới Lai", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "746" => ["ghtk_id" => "1463", "ghtk_name" => "Huyện Vĩnh Thạnh", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "127" => ["ghtk_id" => "1464", "ghtk_name" => "Quận Bình Thủy", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "141" => ["ghtk_id" => "1465", "ghtk_name" => "Quận Cái Răng", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "491" => ["ghtk_id" => "1466", "ghtk_name" => "Quận Ninh Kiều", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "497" => ["ghtk_id" => "1467", "ghtk_name" => "Quận Ô Môn", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "661" => ["ghtk_id" => "1468", "ghtk_name" => "Quận Thốt Nốt", "pickup_id" => $this->defaultSenderId, "is_supported" => true],

                // Da Nang
            // "15" => ["ghtk_id" => null, "ghtk_name" => "Đà Nẵng", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "643" => ["ghtk_id" => "1094", "ghtk_name" => "Quận Thanh Khê", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "156" => ["ghtk_id" => "1095", "ghtk_name" => "Quận Cẩm Lệ", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "302" => ["ghtk_id" => "1096", "ghtk_name" => "Quận Hải Châu", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "479" => ["ghtk_id" => "1097", "ghtk_name" => "Quận Ngũ Hành Sơn", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "592" => ["ghtk_id" => "1098", "ghtk_name" => "Quận Sơn Trà", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "402" => ["ghtk_id" => "1099", "ghtk_name" => "Quận Liên Chiểu", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "319" => ["ghtk_id" => "2074", "ghtk_name" => "Huyện Hòa Vang", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "326" => ["ghtk_id" => "2075", "ghtk_name" => "Huyện Hoàng Sa", "pickup_id" => $this->defaultSenderId, "is_supported" => false],

                // Dak Lak
            // "16" => ["ghtk_id" => null, "ghtk_name" => "Đắk Lắk", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "133" => ["ghtk_id" => "1583", "ghtk_name" => "Huyện Buôn Đôn", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "202" => ["ghtk_id" => "1584", "ghtk_name" => "Huyện Cư Kuin", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "204" => ["ghtk_id" => "1585", "ghtk_name" => "Huyện Cư MGar", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "272" => ["ghtk_id" => "1586", "ghtk_name" => "Huyện Ea Kar", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "273" => ["ghtk_id" => "1587", "ghtk_name" => "Huyện Ea Súp", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "271" => ["ghtk_id" => "1588", "ghtk_name" => "Huyện EaHLeo", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "373" => ["ghtk_id" => "1589", "ghtk_name" => "Huyện Krông Ana", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "374" => ["ghtk_id" => "1590", "ghtk_name" => "Huyện Krông Bông", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "375" => ["ghtk_id" => "1591", "ghtk_name" => "Huyện Krông Búk", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "376" => ["ghtk_id" => "1592", "ghtk_name" => "Huyện Krông Năng", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "379" => ["ghtk_id" => "1593", "ghtk_name" => "Huyện Krông Pắc", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "394" => ["ghtk_id" => "1594", "ghtk_name" => "Huyện Lắk", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "426" => ["ghtk_id" => "1595", "ghtk_name" => "Huyện MDrắk", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "135" => ["ghtk_id" => "1596", "ghtk_name" => "Thành phố Buôn Ma Thuột", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "134" => ["ghtk_id" => "1597", "ghtk_name" => "Thị xã Buôn Hồ", "pickup_id" => $this->defaultSenderId, "is_supported" => true],

                // Dak Nong
            // "17" => ["ghtk_id" => null, "ghtk_name" => "Đắk Nông", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "203" => ["ghtk_id" => "1598", "ghtk_name" => "Huyện Cư Jút", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "228" => ["ghtk_id" => "1599", "ghtk_name" => "Huyện Đắk GLong", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "230" => ["ghtk_id" => "1600", "ghtk_name" => "Huyện Đắk Mil", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "231" => ["ghtk_id" => "1601", "ghtk_name" => "Huyện Đắk RLấp", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "232" => ["ghtk_id" => "1602", "ghtk_name" => "Huyện Đắk Song", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "377" => ["ghtk_id" => "1603", "ghtk_name" => "Huyện KRông Nô", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "706" => ["ghtk_id" => "1604", "ghtk_name" => "Huyện Tuy Đức", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "277" => ["ghtk_id" => "1605", "ghtk_name" => "Thị xã Gia Nghĩa", "pickup_id" => $this->defaultSenderId, "is_supported" => true],

                // Dong Nai
            // "18" => ["ghtk_id" => null, "ghtk_name" => "Đồng Nai", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
//            "18" => ["ghtk_id" => "822", "ghtk_name" => "Dia chi sai", "pickup_id" => $this->defaultSenderId, "is_supported" => false],
            "157" => ["ghtk_id" => "1615", "ghtk_name" => "Huyện Cẩm Mỹ", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "244" => ["ghtk_id" => "1616", "ghtk_name" => "Huyện Định Quán", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "410" => ["ghtk_id" => "1617", "ghtk_name" => "Huyện Long Thành", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "484" => ["ghtk_id" => "1618", "ghtk_name" => "Huyện Nhơn Trạch", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "612" => ["ghtk_id" => "1619", "ghtk_name" => "Huyện Tân Phú", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "660" => ["ghtk_id" => "1620", "ghtk_name" => "Huyện Thống Nhất", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "692" => ["ghtk_id" => "1621", "ghtk_name" => "Huyện Trảng Bom", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "739" => ["ghtk_id" => "1622", "ghtk_name" => "Huyện Vĩnh Cửu", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "757" => ["ghtk_id" => "1623", "ghtk_name" => "Huyện Xuân Lộc", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "113" => ["ghtk_id" => "1624", "ghtk_name" => "Thành phố Biên Hòa", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "406" => ["ghtk_id" => "1625", "ghtk_name" => "Thị xã Long Khánh", "pickup_id" => $this->defaultSenderId, "is_supported" => true],

                // Dong Thap
            // "19" => ["ghtk_id" => null, "ghtk_name" => "Đồng Tháp", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "172" => ["ghtk_id" => "1627", "ghtk_name" => "Huyện Châu Thành", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "337" => ["ghtk_id" => "1628", "ghtk_name" => "Huyện Hồng Ngự", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "389" => ["ghtk_id" => "1629", "ghtk_name" => "Huyện Lai Vung", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "398" => ["ghtk_id" => "1630", "ghtk_name" => "Huyện Lấp Vò", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "599" => ["ghtk_id" => "1631", "ghtk_name" => "Huyện Tam Nông", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "608" => ["ghtk_id" => "1632", "ghtk_name" => "Huyện Tân Hồng", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "639" => ["ghtk_id" => "1633", "ghtk_name" => "Huyện Thanh Bình", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "654" => ["ghtk_id" => "1634", "ghtk_name" => "Huyện Tháp Mười", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "149" => ["ghtk_id" => "1635", "ghtk_name" => "Thành phố Cao Lãnh", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "338" => ["ghtk_id" => "1636", "ghtk_name" => "Thị xã Hồng Ngự", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "570" => ["ghtk_id" => "1637", "ghtk_name" => "Thành Phố Sa Đéc", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
//            "149" => ["ghtk_id" => "16127", "ghtk_name" => "Huyện Cao Lãnh", "pickup_id" => $this->defaultSenderId, "is_supported" => true],          // trung

                // Dien Bien
            // "20" => ["ghtk_id" => null, "ghtk_name" => "Điện Biên", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "239" => ["ghtk_id" => "1606", "ghtk_name" => "Huyện Điện Biên", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "240" => ["ghtk_id" => "1607", "ghtk_name" => "Huyện Điện Biên Đông", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "439" => ["ghtk_id" => "1608", "ghtk_name" => "Huyện Mường Chà", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "444" => ["ghtk_id" => "1609", "ghtk_name" => "Huyện Mương Nhé", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "438" => ["ghtk_id" => "1610", "ghtk_name" => "Huyện Mường Ảng", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "702" => ["ghtk_id" => "1611", "ghtk_name" => "Huyện Tủa Chùa", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "703" => ["ghtk_id" => "1612", "ghtk_name" => "Huyện Tuần Giáo", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "241" => ["ghtk_id" => "1613", "ghtk_name" => "Thành phố Điện Biên phủ", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "443" => ["ghtk_id" => "1614", "ghtk_name" => "Thị xã Mường Lay", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "463" => ["ghtk_id" => "2099", "ghtk_name" => "Huyện Nậm Pồ", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
//            "20" => ["ghtk_id" => "16159", "ghtk_name" => "Xã Thanh Minh", "pickup_id" => $this->defaultSenderId, "is_supported" => false],           // kyna db khong co loc nay
//            "20" => ["ghtk_id" => "16160", "ghtk_name" => "Xã Thanh Xương", "pickup_id" => $this->defaultSenderId, "is_supported" => false],          // kyna db khong co loc nay
//            "20" => ["ghtk_id" => "16161", "ghtk_name" => "Xã Thanh Luông", "pickup_id" => $this->defaultSenderId, "is_supported" => false],          // kyna db khong co loc nay

                // Gia Lai
            // "21" => ["ghtk_id" => null, "ghtk_name" => "Gia Lai", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "190" => ["ghtk_id" => "1638", "ghtk_name" => "Huyện Chư Păh", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "192" => ["ghtk_id" => "1639", "ghtk_name" => "Huyện Chư Pưh", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "193" => ["ghtk_id" => "1640", "ghtk_name" => "Huyện Chư Sê", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "191" => ["ghtk_id" => "1641", "ghtk_name" => "Huyện ChưPRông", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "224" => ["ghtk_id" => "1642", "ghtk_name" => "Huyện Đăk Đoa", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "225" => ["ghtk_id" => "1643", "ghtk_name" => "Huyện Đăk Pơ", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "264" => ["ghtk_id" => "1644", "ghtk_name" => "Huyện Đức Cơ", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "349" => ["ghtk_id" => "1645", "ghtk_name" => "Huyện Ia Grai", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "351" => ["ghtk_id" => "1646", "ghtk_name" => "Huyện Ia Pa", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "352" => ["ghtk_id" => "1647", "ghtk_name" => "Huyện KBang", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "372" => ["ghtk_id" => "1648", "ghtk_name" => "Huyện Kông Chro", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "378" => ["ghtk_id" => "1649", "ghtk_name" => "Huyện Krông Pa", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "425" => ["ghtk_id" => "1650", "ghtk_name" => "Huyện Mang Yang", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "517" => ["ghtk_id" => "1651", "ghtk_name" => "Huyện Phú Thiện", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "534" => ["ghtk_id" => "1652", "ghtk_name" => "Thành phố PleiKu", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "74" => ["ghtk_id" => "1653", "ghtk_name" => "Thị xã AYun Pa", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "67" => ["ghtk_id" => "1654", "ghtk_name" => "Thị xã An Khê", "pickup_id" => $this->defaultSenderId, "is_supported" => true],

                // Ha Giang
            // "22" => ["ghtk_id" => null, "ghtk_name" => "Hà Giang", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "101" => ["ghtk_id" => "1655", "ghtk_name" => "Huyện Bắc Mê", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "103" => ["ghtk_id" => "1656", "ghtk_name" => "Huyện Bắc Quang", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "259" => ["ghtk_id" => "1657", "ghtk_name" => "Huyện Đồng Văn", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "327" => ["ghtk_id" => "1658", "ghtk_name" => "Huyện Hoàng Su Phì", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "427" => ["ghtk_id" => "1659", "ghtk_name" => "Huyện Mèo Vạc", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "537" => ["ghtk_id" => "1660", "ghtk_name" => "Huyện Quản Bạ", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "538" => ["ghtk_id" => "1661", "ghtk_name" => "Huyện Quang Bình", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "733" => ["ghtk_id" => "1662", "ghtk_name" => "Huyện Vị Xuyên", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "756" => ["ghtk_id" => "1663", "ghtk_name" => "Huyện Xín Mần", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "769" => ["ghtk_id" => "1664", "ghtk_name" => "Huyện Yên Minh", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "292" => ["ghtk_id" => "1665", "ghtk_name" => "thành phố Hà Giang", "pickup_id" => $this->defaultSenderId, "is_supported" => true],

                // Ha Nam
            // "23" => ["ghtk_id" => null, "ghtk_name" => "Hà Nam", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "121" => ["ghtk_id" => "1666", "ghtk_name" => "Huyện Bình Lục", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "211" => ["ghtk_id" => "1667", "ghtk_name" => "Huyện Duy Tiên", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "363" => ["ghtk_id" => "1668", "ghtk_name" => "Huyện Kim Bảng", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "420" => ["ghtk_id" => "1669", "ghtk_name" => "Huyện Lý Nhân", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "644" => ["ghtk_id" => "1670", "ghtk_name" => "Huyện Thanh Liêm", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "526" => ["ghtk_id" => "1671", "ghtk_name" => "Thành phố Phủ Lý", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
//            "616" => ["ghtk_id" => "6765", "ghtk_name" => "Tân Sơn", "pickup_id" => $this->defaultSenderId, "is_supported" => false],         // kyna db Tan son thuoc Phu Tho

                // Ha Noi
            // "24" => ["ghtk_id" => null, "ghtk_name" => "Hà Nội", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "78" => ["ghtk_id" => "2", "ghtk_name" => "Ba Đình", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "323" => ["ghtk_id" => "3", "ghtk_name" => "Hoàn Kiếm", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "625" => ["ghtk_id" => "4", "ghtk_name" => "Tây Hồ", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "403" => ["ghtk_id" => "5", "ghtk_name" => "Long Biên", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "164" => ["ghtk_id" => "6", "ghtk_name" => "Cầu Giấy", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "262" => ["ghtk_id" => "7", "ghtk_name" => "Đống Đa", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "300" => ["ghtk_id" => "8", "ghtk_name" => "Hai Bà Trưng", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "324" => ["ghtk_id" => "9", "ghtk_name" => "Hoàng Mai", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "650" => ["ghtk_id" => "10", "ghtk_name" => "Thanh Xuân","pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "291" => ["ghtk_id" => "11", "ghtk_name" => "Hà Đông", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "589" => ["ghtk_id" => "12", "ghtk_name" => "Sơn Tây", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "82" => ["ghtk_id" => "13", "ghtk_name" => "Ba Vì", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "194" => ["ghtk_id" => "14", "ghtk_name" => "Chương Mỹ", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "226" => ["ghtk_id" => "15", "ghtk_name" => "Đan Phượng", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "248" => ["ghtk_id" => "16", "ghtk_name" => "Đông Anh", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "275" => ["ghtk_id" => "17", "ghtk_name" => "Gia Lâm", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "321" => ["ghtk_id" => "18", "ghtk_name" => "Hoài Đức", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "428" => ["ghtk_id" => "19", "ghtk_name" => "Mê Linh", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "446" => ["ghtk_id" => "20", "ghtk_name" => "Mỹ Đức", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "520" => ["ghtk_id" => "21", "ghtk_name" => "Phú Xuyên", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "527" => ["ghtk_id" => "22", "ghtk_name" => "Phúc Thọ", "pickup_id" => $this->defaultSenderId, "is_supported" => false],
            "563" => ["ghtk_id" => "23", "ghtk_name" => "Quốc Oai", "pickup_id" => $this->defaultSenderId, "is_supported" => false],
            "576" => ["ghtk_id" => "24", "ghtk_name" => "Sóc Sơn", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "632" => ["ghtk_id" => "25", "ghtk_name" => "Thạch Thất", "pickup_id" => $this->defaultSenderId, "is_supported" => false],
            "646" => ["ghtk_id" => "26", "ghtk_name" => "Thanh Oai", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "649" => ["ghtk_id" => "27", "ghtk_name" => "Thanh Trì", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "673" => ["ghtk_id" => "28", "ghtk_name" => "Thường Tín", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "107" => ["ghtk_id" => "29", "ghtk_name" => "Từ Liêm", "pickup_id" => $this->defaultSenderId, "is_supported" => true],                  // Bac Tu Liem
            "461" => ["ghtk_id" => "29", "ghtk_name" => "Từ Liêm", "pickup_id" => $this->defaultSenderId, "is_supported" => true],                  // Nam Tu Liem
            "719" => ["ghtk_id" => "30", "ghtk_name" => "Ứng Hòa", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
//            "24" => ["ghtk_id" => "13427", "ghtk_name" => "huyện Đông Hưng", "pickup_id" => $this->defaultSenderId, "is_supported" => false],     // kyna db khong co loc nay
//            "24" => ["ghtk_id" => "13443", "ghtk_name" => "xã Tân Sơn", "pickup_id" => $this->defaultSenderId, "is_supported" => false],          // kyna db Tan Son thuoc Phu Tho
//            "24" => ["ghtk_id" => "13565", "ghtk_name" => "huyện Hoa Lư", "pickup_id" => $this->defaultSenderId, "is_supported" => false],        // kyna db Hoa Lu thuoc Ninh Binh
//            "24" => ["ghtk_id" => "13577", "ghtk_name" => "Hoa Lư", "pickup_id" => $this->defaultSenderId, "is_supported" => false],              // kyna db Hoa Lu thuoc Ninh Binh
//            "24" => ["ghtk_id" => "15267", "ghtk_name" => "huyện Đông Sơn", "pickup_id" => $this->defaultSenderId, "is_supported" => false],      // kyna db khong co loc nay

                // Ha Tinh
            // "25" => ["ghtk_id" => null, "ghtk_name" => "Hà Tĩnh", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "160" => ["ghtk_id" => "1672", "ghtk_name" => "Huyện Cẩm Xuyên", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "145" => ["ghtk_id" => "1673", "ghtk_name" => "Huyện Can Lộc", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "269" => ["ghtk_id" => "1674", "ghtk_name" => "Huyện Đức Thọ", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "343" => ["ghtk_id" => "1675", "ghtk_name" => "Huyện Hương Khê", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "344" => ["ghtk_id" => "1676", "ghtk_name" => "Huyện Hương Sơn", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "380" => ["ghtk_id" => "1677", "ghtk_name" => "Huyện Kỳ Anh", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "413" => ["ghtk_id" => "1678", "ghtk_name" => "Huyện Lộc Hà", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "470" => ["ghtk_id" => "1679", "ghtk_name" => "Huyện Nghi Xuân", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "630" => ["ghtk_id" => "1680", "ghtk_name" => "Huyện Thạch Hà", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "751" => ["ghtk_id" => "1681", "ghtk_name" => "Huyện Vũ Quang", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "295" => ["ghtk_id" => "1682", "ghtk_name" => "Thành phố Hà Tĩnh", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "336" => ["ghtk_id" => "1683", "ghtk_name" => "Thị xã Hồng Lĩnh", "pickup_id" => $this->defaultSenderId, "is_supported" => false],
//            "25" => ["ghtk_id" => "15467", "ghtk_name" => "Thành Phố Hà Tĩnh", "pickup_id" => $this->defaultSenderId, "is_supported" => false],   // trung  uu tien support = true

                // Hai Duong
            // "26" => ["ghtk_id" => null, "ghtk_name" => "Hải Dương", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "118" => ["ghtk_id" => "1684", "ghtk_name" => "Huyện Bình Giang", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "154" => ["ghtk_id" => "1685", "ghtk_name" => "Huyện Cẩm Giàng", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "276" => ["ghtk_id" => "1686", "ghtk_name" => "Huyện Gia Lộc", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "367" => ["ghtk_id" => "1687", "ghtk_name" => "Huyện Kim Thành", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "368" => ["ghtk_id" => "1688", "ghtk_name" => "Huyện Kinh Môn", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "458" => ["ghtk_id" => "1689", "ghtk_name" => "Huyện Nam Sách", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "488" => ["ghtk_id" => "1690", "ghtk_name" => "Huyện Ninh Giang", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "641" => ["ghtk_id" => "1691", "ghtk_name" => "Huyện Thanh Hà", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "645" => ["ghtk_id" => "1692", "ghtk_name" => "Huyện Thanh Miện", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "713" => ["ghtk_id" => "1693", "ghtk_name" => "Huyện Tứ Kỳ", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "303" => ["ghtk_id" => "1694", "ghtk_name" => "Thành phố Hải Dương", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "182" => ["ghtk_id" => "1695", "ghtk_name" => "Thị xã Chí Linh", "pickup_id" => $this->defaultSenderId, "is_supported" => true],

                // Hai Phong
            // "27" => ["ghtk_id" => null, "ghtk_name" => "Hải Phòng", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "334" => ["ghtk_id" => "1106", "ghtk_name" => "Quận Hồng Bàng", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "478" => ["ghtk_id" => "1107", "ghtk_name" => "Quận Ngô Quyền", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "400" => ["ghtk_id" => "1108", "ghtk_name" => "Quận Lê Chân", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "301" => ["ghtk_id" => "1109", "ghtk_name" => "Quận Hải An", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "359" => ["ghtk_id" => "1110", "ghtk_name" => "Quận Kiến An", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "66" => ["ghtk_id" => "2076", "ghtk_name" => "Huyện An Dương", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "68" => ["ghtk_id" => "2077", "ghtk_name" => "Huyện An Lão", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "87" => ["ghtk_id" => "2078", "ghtk_name" => "Huyện Bạch Long Vĩ", "pickup_id" => $this->defaultSenderId, "is_supported" => false],
            "152" => ["ghtk_id" => "2079", "ghtk_name" => "Huyện Cát Hải", "pickup_id" => $this->defaultSenderId, "is_supported" => false],
            "360" => ["ghtk_id" => "2080", "ghtk_name" => "Huyện Kiến Thụy", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "672" => ["ghtk_id" => "2081", "ghtk_name" => "Huyện Thủy Nguyên", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "677" => ["ghtk_id" => "2082", "ghtk_name" => "Huyện Tiên Lãng", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "737" => ["ghtk_id" => "2083", "ghtk_name" => "Huyện Vĩnh Bảo", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "215" => ["ghtk_id" => "2084", "ghtk_name" => "Quận Dương Kinh", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "247" => ["ghtk_id" => "2085", "ghtk_name" => "Quận Đồ Sơn", "pickup_id" => $this->defaultSenderId, "is_supported" => true],

                // Hoa Binh
            // "28" => ["ghtk_id" => null, "ghtk_name" => "Hòa Bình", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "151" => ["ghtk_id" => "1703", "ghtk_name" => "Huyện Cao Phong", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "218" => ["ghtk_id" => "1704", "ghtk_name" => "Huyện Đà Bắc", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "364" => ["ghtk_id" => "1705", "ghtk_name" => "Huyện Kim Bôi", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "382" => ["ghtk_id" => "1706", "ghtk_name" => "Huyện Kỳ Sơn", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "386" => ["ghtk_id" => "1707", "ghtk_name" => "Huyện Lạc Sơn", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "387" => ["ghtk_id" => "1708", "ghtk_name" => "Huyện Lạc Thủy", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "418" => ["ghtk_id" => "1709", "ghtk_name" => "Huyện Lương Sơn", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "422" => ["ghtk_id" => "1710", "ghtk_name" => "Huyện Mai Châu", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "611" => ["ghtk_id" => "1711", "ghtk_name" => "Huyện Tân Lạc", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "776" => ["ghtk_id" => "1712", "ghtk_name" => "Huyện Yên Thủy", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "317" => ["ghtk_id" => "1713", "ghtk_name" => "Thành phố Hòa Bình", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
//            "382" => ["ghtk_id" => "16638", "ghtk_name" => "Huyện Kỳ Sơn", "pickup_id" => $this->defaultSenderId, "is_supported" => false],       // trung -> uu tien support = true

                // Hau Giang
            // "29" => ["ghtk_id" => null, "ghtk_name" => "Hậu Giang", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "173" => ["ghtk_id" => "1696", "ghtk_name" => "Huyện Châu Thành", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "180" => ["ghtk_id" => "1697", "ghtk_name" => "Huyện Châu Thành A", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "407" => ["ghtk_id" => "1698", "ghtk_name" => "Huyện Long Mỹ", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "530" => ["ghtk_id" => "1699", "ghtk_name" => "Huyện Phụng Hiệp", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "732" => ["ghtk_id" => "1700", "ghtk_name" => "Huyện Vị Thủy", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "731" => ["ghtk_id" => "1701", "ghtk_name" => "Thành Phố Vị Thanh", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "466" => ["ghtk_id" => "1702", "ghtk_name" => "Thị xã Ngã Bảy", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "408" => ["ghtk_id" => "6289", "ghtk_name" => "Thị xã Long Mỹ", "pickup_id" => $this->defaultSenderId, "is_supported" => true],

                // Hung Yen
            // "30" => ["ghtk_id" => null, "ghtk_name" => "Hưng Yên", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "75" => ["ghtk_id" => "1714", "ghtk_name" => "Huyện Ân Thi", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "356" => ["ghtk_id" => "1715", "ghtk_name" => "Huyện Khoái Châu", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "365" => ["ghtk_id" => "1716", "ghtk_name" => "Huyện Kim Động", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "447" => ["ghtk_id" => "1717", "ghtk_name" => "Huyện Mỹ Hào", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "522" => ["ghtk_id" => "1718", "ghtk_name" => "Huyện Phù Cừ", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "678" => ["ghtk_id" => "1719", "ghtk_name" => "Huyện Tiên Lữ", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "723" => ["ghtk_id" => "1720", "ghtk_name" => "Huyện Văn Giang", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "725" => ["ghtk_id" => "1721", "ghtk_name" => "Huyện Văn Lâm", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "771" => ["ghtk_id" => "1722", "ghtk_name" => "Huyện Yên Mỹ", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "342" => ["ghtk_id" => "1723", "ghtk_name" => "Thành phố Hưng Yên", "pickup_id" => $this->defaultSenderId, "is_supported" => true],

                // Ho Chi Minh
            // "31" => ["ghtk_id" => null, "ghtk_name" => "TP Hồ Chí Minh", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "547" => ["ghtk_id" => 127, "ghtk_name" => "Quận 1", "pickup_id" => $this->defaultSenderId, "is_supported" => true], // Quận 1
            "548" => ["ghtk_id" => 128, "ghtk_name" => "Quận 2", "pickup_id" => $this->defaultSenderId, "is_supported" => true], // Quận 2
            "549" => ["ghtk_id" => 1074, "ghtk_name" => "Quận 3", "pickup_id" => $this->defaultSenderId, "is_supported" => true], // Quận 3
            "550" => ["ghtk_id" => 1075, "ghtk_name" => "Quận 4", "pickup_id" => $this->defaultSenderId, "is_supported" => true], // Quận 4
            "551" => ["ghtk_id" => 1076, "ghtk_name" => "Quận 5", "pickup_id" => $this->defaultSenderId, "is_supported" => true], // Quận 5
            "552" => ["ghtk_id" => 1077, "ghtk_name" => "Quận 6", "pickup_id" => $this->defaultSenderId, "is_supported" => true], // Quận 6
            "553" => ["ghtk_id" => 1078, "ghtk_name" => "Quận 7", "pickup_id" => $this->defaultSenderId, "is_supported" => true], // Quận 7
            "554" => ["ghtk_id" => 1079, "ghtk_name" => "Quận 8", "pickup_id" => $this->defaultSenderId, "is_supported" => true], // Quận 8
            "555" => ["ghtk_id" => 1080, "ghtk_name" => "Quận 9", "pickup_id" => $this->defaultSenderId, "is_supported" => true], // Quận 9
            "556" => ["ghtk_id" => 1081, "ghtk_name" => "Quận 10", "pickup_id" => $this->defaultSenderId, "is_supported" => true], // Quận 10
            "557" => ["ghtk_id" => 1082, "ghtk_name" => "Quận 11", "pickup_id" => $this->defaultSenderId, "is_supported" => true], // Quận 11
            "558" => ["ghtk_id" => 1083, "ghtk_name" => "Quận 12", "pickup_id" => $this->defaultSenderId, "is_supported" => true], // Quận 12
            "604" => ["ghtk_id" => 1084, "ghtk_name" => "Quận Tân Bình", "pickup_id" => $this->defaultSenderId, "is_supported" => true], // Quận Tân Bình
            "613" => ["ghtk_id" => 1085, "ghtk_name" => "Quận Tân Phú", "pickup_id" => $this->defaultSenderId, "is_supported" => true], // Quận Tân Phú
            "510" => ["ghtk_id" => 1086, "ghtk_name" => "Quận Phú Nhuận", "pickup_id" => $this->defaultSenderId, "is_supported" => true], // Quận Phú Nhuận
            "290" => ["ghtk_id" => 1087, "ghtk_name" => "Quận Gò Vấp", "pickup_id" => $this->defaultSenderId, "is_supported" => true], // Quận Gò Vấp
            "126" => ["ghtk_id" => 1088, "ghtk_name" => "Quận Bình Thạnh", "pickup_id" => $this->defaultSenderId, "is_supported" => true], // Quận Bình Thạnh
            "665" => ["ghtk_id" => 1089, "ghtk_name" => "Quận Thủ Đức", "pickup_id" => $this->defaultSenderId, "is_supported" => true], // Quận Thủ Đức
            "124" => ["ghtk_id" => 1090, "ghtk_name" => "Quận Bình Tân", "pickup_id" => $this->defaultSenderId, "is_supported" => true], // Quận Bình Tân
            "330" => ["ghtk_id" => 1091, "ghtk_name" => "Huyện Hóc Môn", "pickup_id" => $this->defaultSenderId, "is_supported" => true], // Huyện Hóc Môn
            "482" => ["ghtk_id" => 1092, "ghtk_name" => "Huyện Nhà Bè", "pickup_id" => $this->defaultSenderId, "is_supported" => true], // Huyện Nhà Bè
            "115" => ["ghtk_id" => 1093, "ghtk_name" => "Huyện Bình Chánh", "pickup_id" => $this->defaultSenderId, "is_supported" => true], // Huyện Bình Chánh
            "201" => ["ghtk_id" => 1136, "ghtk_name" => "Huyện Củ Chi", "pickup_id" => $this->defaultSenderId, "is_supported" => true], // Huyện Củ Chi
            "162" => ["ghtk_id" => 1137, "ghtk_name" => "Huyện Cần Giờ", "pickup_id" => $this->defaultSenderId, "is_supported" => true], // Huyện Cần Giờ

                // Khanh Hoa
            // "32" => ["ghtk_id" => null, "ghtk_name" => "Khánh Hòa", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "142" => ["ghtk_id" => "1724", "ghtk_name" => "Huyện Cam Lâm", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "209" => ["ghtk_id" => "1725", "ghtk_name" => "Huyện Diên Khánh", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "354" => ["ghtk_id" => "1726", "ghtk_name" => "Huyện Khánh Sơn", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "355" => ["ghtk_id" => "1727", "ghtk_name" => "Huyện Khánh Vĩnh", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "490" => ["ghtk_id" => "1728", "ghtk_name" => "Thị Xã Ninh Hòa", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "701" => ["ghtk_id" => "1729", "ghtk_name" => "Huyện Trường Sa", "pickup_id" => $this->defaultSenderId, "is_supported" => false],
            "720" => ["ghtk_id" => "1730", "ghtk_name" => "Huyện Vạn Ninh", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "481" => ["ghtk_id" => "1731", "ghtk_name" => "Thành phố Nha Trang", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "144" => ["ghtk_id" => "1732", "ghtk_name" => "Thành phố Cam Ranh", "pickup_id" => $this->defaultSenderId, "is_supported" => true],

                // Kien Giang
            // "33" => ["ghtk_id" => null, "ghtk_name" => "Kiên Giang", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "65" => ["ghtk_id" => "1733", "ghtk_name" => "Huyện An Biên", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "70" => ["ghtk_id" => "1734", "ghtk_name" => "Huyện An Minh", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "174" => ["ghtk_id" => "1735", "ghtk_name" => "Huyện Châu Thành", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "280" => ["ghtk_id" => "1736", "ghtk_name" => "Huyện Giang Thành", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "283" => ["ghtk_id" => "1737", "ghtk_name" => "Huyện Giồng Riềng", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "289" => ["ghtk_id" => "1738", "ghtk_name" => "Huyện Gò Quao", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "331" => ["ghtk_id" => "1739", "ghtk_name" => "Huyện Hòn Đất", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "357" => ["ghtk_id" => "1740", "ghtk_name" => "Huyện Kiên Hải", "pickup_id" => $this->defaultSenderId, "is_supported" => false],
            "358" => ["ghtk_id" => "1741", "ghtk_name" => "Huyện Kiên Lương", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "513" => ["ghtk_id" => "1742", "ghtk_name" => "Huyện Phú Quốc", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "607" => ["ghtk_id" => "1743", "ghtk_name" => "Huyện Tân Hiệp", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "717" => ["ghtk_id" => "1744", "ghtk_name" => "Huyện U Minh Thượng", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "747" => ["ghtk_id" => "1745", "ghtk_name" => "Huyện Vĩnh Thuận", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "569" => ["ghtk_id" => "1746", "ghtk_name" => "Thành phố Rạch Giá", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "294" => ["ghtk_id" => "1747", "ghtk_name" => "Thị xã Hà Tiên", "pickup_id" => $this->defaultSenderId, "is_supported" => true],

                // Kon Tum
            // "34" => ["ghtk_id" => null, "ghtk_name" => "Kon Tum", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "227" => ["ghtk_id" => "1748", "ghtk_name" => "Huyện Đắk Glei", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "229" => ["ghtk_id" => "1749", "ghtk_name" => "Huyện Đắk Hà", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "233" => ["ghtk_id" => "1750", "ghtk_name" => "Huyện Đắk Tô", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "369" => ["ghtk_id" => "1751", "ghtk_name" => "Huyện Kon Plông", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "370" => ["ghtk_id" => "1752", "ghtk_name" => "Huyện Kon Rẫy", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "476" => ["ghtk_id" => "1753", "ghtk_name" => "Huyện Ngọc Hồi", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "572" => ["ghtk_id" => "1754", "ghtk_name" => "Huyện Sa Thầy", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "704" => ["ghtk_id" => "1755", "ghtk_name" => "Huyện Tu Mơ Rông", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "371" => ["ghtk_id" => "1756", "ghtk_name" => "Thành phố Kon Tum", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "350" => ["ghtk_id" => "2105", "ghtk_name" => "Ia H'Drai", "pickup_id" => $this->defaultSenderId, "is_supported" => true],

                // Lai Chau
            // "35" => ["ghtk_id" => null, "ghtk_name" => "Lai Châu", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "445" => ["ghtk_id" => "1757", "ghtk_name" => "Huyện Mường Tè", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "503" => ["ghtk_id" => "1758", "ghtk_name" => "Huyện Phong Thổ", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "575" => ["ghtk_id" => "1759", "ghtk_name" => "Huyện Sìn Hồ", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "597" => ["ghtk_id" => "1760", "ghtk_name" => "Huyện Tam Đường", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "621" => ["ghtk_id" => "1761", "ghtk_name" => "Thị Xã Tân Uyên", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "637" => ["ghtk_id" => "1762", "ghtk_name" => "Huyện Than Uyên", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "388" => ["ghtk_id" => "1763", "ghtk_name" => "Thành phố Lai Châu", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "464" => ["ghtk_id" => "2104", "ghtk_name" => "Nậm Nhùn", "pickup_id" => $this->defaultSenderId, "is_supported" => true],

                // Lao Cai
            // "36" => ["ghtk_id" => null, "ghtk_name" => "Lào Cai", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "99" => ["ghtk_id" => "1787", "ghtk_name" => "Huyện Bắc Hà", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "93" => ["ghtk_id" => "1788", "ghtk_name" => "Huyện Bảo Thắng", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "94" => ["ghtk_id" => "1789", "ghtk_name" => "Huyện Bảo Yên", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "95" => ["ghtk_id" => "1790", "ghtk_name" => "Huyện Bát Xát", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "440" => ["ghtk_id" => "1791", "ghtk_name" => "Huyện Mường Khương", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "571" => ["ghtk_id" => "1792", "ghtk_name" => "Huyện Sa Pa", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "574" => ["ghtk_id" => "1793", "ghtk_name" => "Huyện Si Ma Cai", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "721" => ["ghtk_id" => "1794", "ghtk_name" => "Huyện Văn Bàn", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "393" => ["ghtk_id" => "1795", "ghtk_name" => "Thành phố Lào Cai", "pickup_id" => $this->defaultSenderId, "is_supported" => true],

                // Lang Son
            // "37" => ["ghtk_id" => null, "ghtk_name" => "Lạng Sơn", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "104" => ["ghtk_id" =>  "1776", "ghtk_name" => "Huyện Bắc Sơn", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "117" => ["ghtk_id" =>  "1777", "ghtk_name" => "Huyện Bình Gia", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "150" => ["ghtk_id" =>  "1778", "ghtk_name" => "Huyện Cao Lộc", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "181" => ["ghtk_id" =>  "1779", "ghtk_name" => "Huyện Chi Lăng", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "242" => ["ghtk_id" =>  "1780", "ghtk_name" => "Huyện Đình Lập", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "348" => ["ghtk_id" =>  "1781", "ghtk_name" => "Huyện Hữu Lũng", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "412" => ["ghtk_id" =>  "1782", "ghtk_name" => "Huyện Lộc Bình", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "690" => ["ghtk_id" =>  "1783", "ghtk_name" => "Huyện Tràng Định", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "724" => ["ghtk_id" =>  "1784", "ghtk_name" => "Huyện Văn Lãng", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "726" => ["ghtk_id" =>  "1785", "ghtk_name" => "Huyện Văn Quan", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "392" => ["ghtk_id" =>  "1786", "ghtk_name" => "Thành phố Lạng Sơn", "pickup_id" => $this->defaultSenderId, "is_supported" => true],

                // Lam Dong
            // "38" => ["ghtk_id" => null, "ghtk_name" => "Lâm Đồng", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "91" => ["ghtk_id" => "1764", "ghtk_name" => "Huyện Bảo Lâm", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "153" => ["ghtk_id" => "1765", "ghtk_name" => "Huyện Cát Tiên", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "220" => ["ghtk_id" => "1766", "ghtk_name" => "Huyện Đạ Huoai", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "221" => ["ghtk_id" => "1767", "ghtk_name" => "Huyện Đạ Tẻh", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "236" => ["ghtk_id" => "1768", "ghtk_name" => "Huyện Đam Rông", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "207" => ["ghtk_id" => "1769", "ghtk_name" => "Huyện Di Linh", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "263" => ["ghtk_id" => "1770", "ghtk_name" => "Huyện Đơn Dương", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "270" => ["ghtk_id" => "1771", "ghtk_name" => "Huyện Đức Trọng", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "385" => ["ghtk_id" => "1772", "ghtk_name" => "Huyện Lạc Dương", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "396" => ["ghtk_id" => "1773", "ghtk_name" => "Huyện Lâm Hà", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "92" => ["ghtk_id" => "1774", "ghtk_name" => "Thành phố Bảo Lộc", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "219" => ["ghtk_id" => "1775", "ghtk_name" => "Thành phố Đà Lạt", "pickup_id" => $this->defaultSenderId, "is_supported" => true],

                // Long An
            // "39" => ["ghtk_id" => null, "ghtk_name" => "Long An", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "111" => ["ghtk_id" => "1796", "ghtk_name" => "Huyện Bến Lức", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "161" => ["ghtk_id" => "1797", "ghtk_name" => "Huyện Cần Đước", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "163" => ["ghtk_id" => "1798", "ghtk_name" => "Huyện Cần Giuộc", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "175" => ["ghtk_id" => "1799", "ghtk_name" => "Huyện Châu Thành", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "265" => ["ghtk_id" => "1800", "ghtk_name" => "Huyện Đức Hòa", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "266" => ["ghtk_id" => "1801", "ghtk_name" => "Huyện Đức Huệ", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "436" => ["ghtk_id" => "1802", "ghtk_name" => "Huyện Mộc Hóa", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "609" => ["ghtk_id" => "1803", "ghtk_name" => "Huyện Tân Hưng", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "618" => ["ghtk_id" => "1804", "ghtk_name" => "Huyện Tân Thạnh", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "619" => ["ghtk_id" => "1805", "ghtk_name" => "Huyện Tân Trụ", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "651" => ["ghtk_id" => "1806", "ghtk_name" => "Huyện Thạnh Hóa", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "666" => ["ghtk_id" => "1807", "ghtk_name" => "Huyện Thủ Thừa", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "740" => ["ghtk_id" => "1808", "ghtk_name" => "Huyện Vĩnh Hưng", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "602" => ["ghtk_id" => "1809", "ghtk_name" => "Thành phố Tân An", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "362" => ["ghtk_id" => "2098", "ghtk_name" => "Thị xã Kiến Tường", "pickup_id" => $this->defaultSenderId, "is_supported" => true],

                // Nam Dinh
            // "40" => ["ghtk_id" => null, "ghtk_name" => "Nam Định", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "281" => ["ghtk_id" => "1810", "ghtk_name" => "Huyện Giao Thủy", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "305" => ["ghtk_id" => "1811", "ghtk_name" => "Huyện Hải Hậu", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "448" => ["ghtk_id" => "1812", "ghtk_name" => "Huyện Mỹ Lộc", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "460" => ["ghtk_id" => "1813", "ghtk_name" => "Huyện Nam Trực", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "473" => ["ghtk_id" => "1814", "ghtk_name" => "Huyện Nghĩa Hưng", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "700" => ["ghtk_id" => "1815", "ghtk_name" => "Huyện Trực Ninh", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "753" => ["ghtk_id" => "1816", "ghtk_name" => "Huyện Vụ Bản", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "758" => ["ghtk_id" => "1817", "ghtk_name" => "Huyện Xuân Trường", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "760" => ["ghtk_id" => "1818", "ghtk_name" => "Huyện Ý Yên", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "455" => ["ghtk_id" => "1819", "ghtk_name" => "Thành phố Nam Định", "pickup_id" => $this->defaultSenderId, "is_supported" => true],

                // Nghe An
            // "41" => ["ghtk_id" => null, "ghtk_name" => "Nghệ An", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "73" => ["ghtk_id" => "1820", "ghtk_name" => "Huyện Anh Sơn", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "195" => ["ghtk_id" => "1821", "ghtk_name" => "Huyện Con Cuông", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "210" => ["ghtk_id" => "1822", "ghtk_name" => "Huyện Diễn Châu", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "246" => ["ghtk_id" => "1823", "ghtk_name" => "Huyện Đô Lương", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "341" => ["ghtk_id" => "1824", "ghtk_name" => "Huyện Hưng Nguyên", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "383" => ["ghtk_id" => "1825", "ghtk_name" => "Huyện Kỳ Sơn", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "454" => ["ghtk_id" => "1826", "ghtk_name" => "Huyện Nam Đàn", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "469" => ["ghtk_id" => "1827", "ghtk_name" => "Huyện Nghi Lộc", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "471" => ["ghtk_id" => "1828", "ghtk_name" => "Huyện Nghĩa Đàn", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "559" => ["ghtk_id" => "1829", "ghtk_name" => "Huyện Quế Phong", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "564" => ["ghtk_id" => "1830", "ghtk_name" => "Huyện Quỳ Châu", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "565" => ["ghtk_id" => "1831", "ghtk_name" => "Huyện Quỳ Hợp", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "566" => ["ghtk_id" => "1832", "ghtk_name" => "Huyện Quỳnh Lưu", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "610" => ["ghtk_id" => "1833", "ghtk_name" => "Huyện Tân Kỳ", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "640" => ["ghtk_id" => "1834", "ghtk_name" => "Huyện Thanh Chương", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "715" => ["ghtk_id" => "1835", "ghtk_name" => "Huyện Tương Dương", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "774" => ["ghtk_id" => "1836", "ghtk_name" => "Huyện Yên Thành", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "736" => ["ghtk_id" => "1837", "ghtk_name" => "Thành phố Vinh", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "205" => ["ghtk_id" => "1838", "ghtk_name" => "Thị xã Cửa Lò", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "634" => ["ghtk_id" => "1839", "ghtk_name" => "Thị xã Thái Hòa", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "325" => ["ghtk_id" => "2103", "ghtk_name" => "Thị xã Hoàng Mai", "pickup_id" => $this->defaultSenderId, "is_supported" => true],

                // Ninh Binh
            // "42" => ["ghtk_id" => null, "ghtk_name" => "Ninh Bình", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "278" => ["ghtk_id" => "1840", "ghtk_name" => "Huyện Gia Viễn", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "314" => ["ghtk_id" => "1841", "ghtk_name" => "Huyện Hoa Lư", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "366" => ["ghtk_id" => "1842", "ghtk_name" => "Huyện Kim Sơn", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "483" => ["ghtk_id" => "1843", "ghtk_name" => "Huyện Nho Quan", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "766" => ["ghtk_id" => "1844", "ghtk_name" => "Huyện Yên Khánh", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "770" => ["ghtk_id" => "1845", "ghtk_name" => "Huyện Yên Mô", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "487" => ["ghtk_id" => "1846", "ghtk_name" => "Thành phố Ninh Bình", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "596" => ["ghtk_id" => "1847", "ghtk_name" => "Thành phố Tam Điệp", "pickup_id" => $this->defaultSenderId, "is_supported" => true],

                // Ninh Thuan
            // "43" => ["ghtk_id" => null, "ghtk_name" => "Ninh Thuận", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "85" => ["ghtk_id" =>  "1848", "ghtk_name" => "Huyên Bác ái", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "489" => ["ghtk_id" =>  "1849", "ghtk_name" => "Huyện Ninh Hải", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "492" => ["ghtk_id" =>  "1850", "ghtk_name" => "Huyện Ninh Phước", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "493" => ["ghtk_id" =>  "1851", "ghtk_name" => "Huyện Ninh Sơn", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "668" => ["ghtk_id" =>  "1852", "ghtk_name" => "Huyện Thuận Bắc", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "670" => ["ghtk_id" =>  "1853", "ghtk_name" => "Huyện Thuận Nam", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "499" => ["ghtk_id" =>  "1854", "ghtk_name" => "Thành phố Phan Rang-Tháp Chàm", "pickup_id" => $this->defaultSenderId, "is_supported" => true],

                // Phu Tho
            // "44" => ["ghtk_id" => null, "ghtk_name" => "Phú Thọ", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
//            "734" => ["ghtk_id" => "815", "ghtk_name" => "Việt Trì", "pickup_id" => $this->defaultSenderId, "is_supported" => false],         // trung -> uu tien support = true
            "155" => ["ghtk_id" => "1855", "ghtk_name" => "Huyện Cẩm Khê", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "245" => ["ghtk_id" => "1856", "ghtk_name" => "Huyện Đoan Hùng", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "297" => ["ghtk_id" => "1857", "ghtk_name" => "Huyện Hạ Hòa", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "397" => ["ghtk_id" => "1858", "ghtk_name" => "Huyện Lâm Thao", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "524" => ["ghtk_id" => "1859", "ghtk_name" => "Huyện Phù Ninh", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "600" => ["ghtk_id" => "1860", "ghtk_name" => "Huyện Tam Nông", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "616" => ["ghtk_id" => "1861", "ghtk_name" => "Huyện Tân Sơn", "pickup_id" => $this->defaultSenderId, "is_supported" => false],
            "638" => ["ghtk_id" => "1862", "ghtk_name" => "Huyện Thanh Ba", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "647" => ["ghtk_id" => "1863", "ghtk_name" => "Huyện Thanh Sơn", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "648" => ["ghtk_id" => "1864", "ghtk_name" => "Huyện Thanh Thủy", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "768" => ["ghtk_id" => "1865", "ghtk_name" => "Huyện Yên Lập", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "734" => ["ghtk_id" => "1866", "ghtk_name" => "Thành phố Việt Trì", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "518" => ["ghtk_id" => "1867", "ghtk_name" => "Thị xã Phú Thọ", "pickup_id" => $this->defaultSenderId, "is_supported" => true],

                // Phu Yen
            // "45" => ["ghtk_id" => null, "ghtk_name" => "Phú Yên", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
//            "707" => ["ghtk_id" =>  "133", "ghtk_name" => "Tuy Hòa", "pickup_id" => $this->defaultSenderId, "is_supported" => false],         // trung -> uu tien support = true
            "252" => ["ghtk_id" =>  "1868", "ghtk_name" => "Huyện Đông Hòa", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "261" => ["ghtk_id" =>  "1869", "ghtk_name" => "Huyện Đồng Xuân", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "507" => ["ghtk_id" =>  "1870", "ghtk_name" => "Huyện Phú Hòa", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "587" => ["ghtk_id" =>  "1871", "ghtk_name" => "Huyện Sơn Hòa", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "580" => ["ghtk_id" =>  "1872", "ghtk_name" => "Huyện Sông Hinh", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "624" => ["ghtk_id" =>  "1873", "ghtk_name" => "Huyện Tây Hòa", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "705" => ["ghtk_id" =>  "1874", "ghtk_name" => "Huyện Tuy An", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "707" => ["ghtk_id" =>  "1875", "ghtk_name" => "Thành phố Tuy Hòa", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "578" => ["ghtk_id" =>  "1876", "ghtk_name" => "Thị xã Sông Cầu", "pickup_id" => $this->defaultSenderId, "is_supported" => true],

                // Quang Binh
            // "46" => ["ghtk_id" => null, "ghtk_name" => "Quảng Bình", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "129" => ["ghtk_id" => "1877", "ghtk_name" => "Huyện Bố Trạch", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "401" => ["ghtk_id" => "1878", "ghtk_name" => "Huyện Lệ Thủy", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "429" => ["ghtk_id" => "1879", "ghtk_name" => "Huyện Minh Hoá", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "541" => ["ghtk_id" => "1880", "ghtk_name" => "Huyện Quảng Ninh", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "542" => ["ghtk_id" => "1881", "ghtk_name" => "Huyện Quảng Trạch", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "710" => ["ghtk_id" => "1882", "ghtk_name" => "Huyện Tuyên Hoá", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "256" => ["ghtk_id" => "1883", "ghtk_name" => "Thành phố Đồng Hới", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "79" => ["ghtk_id" => "2102", "ghtk_name" => "Thị xã Ba Đồn", "pickup_id" => $this->defaultSenderId, "is_supported" => true],

                // Quang Nam
            // "47" => ["ghtk_id" => null, "ghtk_name" => "Quảng Nam", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "106" => ["ghtk_id" => "1884", "ghtk_name" => "Huyện Bắc Trà My", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "222" => ["ghtk_id" => "1885", "ghtk_name" => "Huyện Đại Lộc", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "238" => ["ghtk_id" => "1886", "ghtk_name" => "Thị xã Điện Bàn", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "249" => ["ghtk_id" => "1887", "ghtk_name" => "Huyện Đông Giang", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "212" => ["ghtk_id" => "1888", "ghtk_name" => "Huyện Duy Xuyên", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "312" => ["ghtk_id" => "1889", "ghtk_name" => "Huyện Hiệp Đức", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "457" => ["ghtk_id" => "1890", "ghtk_name" => "Huyện Nam Giang", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "459" => ["ghtk_id" => "1891", "ghtk_name" => "Huyện Nam Trà My", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "495" => ["ghtk_id" => "1892", "ghtk_name" => "Huyện Nông Sơn", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "496" => ["ghtk_id" => "1893", "ghtk_name" => "Huyện Núi Thành", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "511" => ["ghtk_id" => "1894", "ghtk_name" => "Huyện Phú Ninh", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "533" => ["ghtk_id" => "1895", "ghtk_name" => "Huyện Phước Sơn", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "560" => ["ghtk_id" => "1896", "ghtk_name" => "Huyện Quế Sơn", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "623" => ["ghtk_id" => "1897", "ghtk_name" => "Huyện Tây Giang", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "655" => ["ghtk_id" => "1898", "ghtk_name" => "Huyện Thăng Bình", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "679" => ["ghtk_id" => "1899", "ghtk_name" => "Huyện Tiên Phước", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "333" => ["ghtk_id" => "1900", "ghtk_name" => "Thành phố Hội An", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "598" => ["ghtk_id" => "1901", "ghtk_name" => "Thành phố Tam Kỳ", "pickup_id" => $this->defaultSenderId, "is_supported" => true],

                // Quang Ngai
            // "48" => ["ghtk_id" => null, "ghtk_name" => "Quảng Ngãi", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "80" => ["ghtk_id" => "1902", "ghtk_name" => "Huyện Ba Tơ", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "123" => ["ghtk_id" => "1903", "ghtk_name" => "Huyện Bình Sơn", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "268" => ["ghtk_id" => "1904", "ghtk_name" => "Huyện Đức Phổ", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "421" => ["ghtk_id" => "1905", "ghtk_name" => "Huyện Lý sơn", "pickup_id" => $this->defaultSenderId, "is_supported" => false],
            "430" => ["ghtk_id" => "1906", "ghtk_name" => "Huyện Minh Long", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "434" => ["ghtk_id" => "1907", "ghtk_name" => "Huyện Mộ Đức", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "472" => ["ghtk_id" => "1908", "ghtk_name" => "Huyện Nghĩa Hành", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "586" => ["ghtk_id" => "1909", "ghtk_name" => "Huyện Sơn Hà", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "590" => ["ghtk_id" => "1910", "ghtk_name" => "Huyện Sơn Tây", "pickup_id" => $this->defaultSenderId, "is_supported" => false],
            "591" => ["ghtk_id" => "1911", "ghtk_name" => "Huyện Sơn Tịnh", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "628" => ["ghtk_id" => "1912", "ghtk_name" => "Huyện Tây Trà", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "684" => ["ghtk_id" => "1913", "ghtk_name" => "Huyện Trà Bồng", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "712" => ["ghtk_id" => "1914", "ghtk_name" => "Huyện Tư Nghĩa", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "540" => ["ghtk_id" => "1915", "ghtk_name" => "Thành phố Quảng Ngãi", "pickup_id" => $this->defaultSenderId, "is_supported" => true],

                // Quang Ninh
            // "49" => ["ghtk_id" => null, "ghtk_name" => "Quảng Ninh", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "77" => ["ghtk_id" => "1916", "ghtk_name" => "Huyện Ba Chẽ", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "119" => ["ghtk_id" => "1917", "ghtk_name" => "Huyện Bình Liêu", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "196" => ["ghtk_id" => "1918", "ghtk_name" => "Huyện Cô Tô", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "235" => ["ghtk_id" => "1919", "ghtk_name" => "Huyện Đầm Hà", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "255" => ["ghtk_id" => "1920", "ghtk_name" => "Huyện Đông Triều", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "304" => ["ghtk_id" => "1921", "ghtk_name" => "Huyện Hải Hà", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "328" => ["ghtk_id" => "1922", "ghtk_name" => "Huyện Hoành Bồ", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "680" => ["ghtk_id" => "1923", "ghtk_name" => "Huyện Tiên Yên", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "729" => ["ghtk_id" => "1924", "ghtk_name" => "Huyện Vân Đồn", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
//            "49" => ["ghtk_id" => "1925", "ghtk_name" => "Huyện Yên Hưng", "pickup_id" => $this->defaultSenderId, "is_supported" => false],       // khong co trong kyna db
            "299" => ["ghtk_id" => "1926", "ghtk_name" => "Thành phố Hạ Long", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "433" => ["ghtk_id" => "1927", "ghtk_name" => "Thành phố Móng Cái", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "158" => ["ghtk_id" => "1928", "ghtk_name" => "Thành phố Cẩm Phả", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "718" => ["ghtk_id" => "1929", "ghtk_name" => "Thành phố Uông Bí", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "544" => ["ghtk_id" => "17286", "ghtk_name" => "Thị xã Quảng Yên", "pickup_id" => $this->defaultSenderId, "is_supported" => true],

                // Quang Tri
            // "50" => ["ghtk_id" => null, "ghtk_name" => "Quảng Trị", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "143" => ["ghtk_id" => "1930", "ghtk_name" => "Huyện Cam Lộ", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "198" => ["ghtk_id" => "1931", "ghtk_name" => "Huyện Cồn Cỏ", "pickup_id" => $this->defaultSenderId, "is_supported" => false],
            "217" => ["ghtk_id" => "1932", "ghtk_name" => "Huyện Đa Krông", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "282" => ["ghtk_id" => "1933", "ghtk_name" => "Huyện Gio Linh", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "306" => ["ghtk_id" => "1934", "ghtk_name" => "Huyện Hải Lăng", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "347" => ["ghtk_id" => "1935", "ghtk_name" => "Huyện Hướng Hóa", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "697" => ["ghtk_id" => "1936", "ghtk_name" => "Huyện Triệu Phong", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "741" => ["ghtk_id" => "1937", "ghtk_name" => "Huyện Vính Linh", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "250" => ["ghtk_id" => "1938", "ghtk_name" => "Thành phố Đông Hà", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "543" => ["ghtk_id" => "1939", "ghtk_name" => "Thị xã Quảng Trị", "pickup_id" => $this->defaultSenderId, "is_supported" => true],

                // Soc Trang
            // "51" => ["ghtk_id" => null, "ghtk_name" => "Sóc Trăng", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "176" => ["ghtk_id" => "1940", "ghtk_name" => "Huyện Châu Thành", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "200" => ["ghtk_id" => "1941", "ghtk_name" => "Huyện Cù Lao Dung", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "353" => ["ghtk_id" => "1942", "ghtk_name" => "Huyện Kế Sách", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "409" => ["ghtk_id" => "1943", "ghtk_name" => "Huyện Long Phú", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "450" => ["ghtk_id" => "1944", "ghtk_name" => "Huyện Mỹ Tú", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "451" => ["ghtk_id" => "1945", "ghtk_name" => "Huyện Mỹ Xuyên", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "467" => ["ghtk_id" => "1946", "ghtk_name" => "Huyện Ngã Năm", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "653" => ["ghtk_id" => "1947", "ghtk_name" => "Huyện Thạnh Trị", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "694" => ["ghtk_id" => "1948", "ghtk_name" => "Huyện Trần Đề", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "738" => ["ghtk_id" => "1949", "ghtk_name" => "Huyện Vĩnh Châu", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "577" => ["ghtk_id" => "1950", "ghtk_name" => "Thành phố Sóc Trăng", "pickup_id" => $this->defaultSenderId, "is_supported" => true],

                // Son La
            // "52" => ["ghtk_id" => null, "ghtk_name" => "Sơn La", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "108" => ["ghtk_id" => "1951", "ghtk_name" => "Huyện Bắc Yên", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "423" => ["ghtk_id" => "1952", "ghtk_name" => "Huyện Mai Sơn", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "435" => ["ghtk_id" => "1953", "ghtk_name" => "Huyện Mộc Châu", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "441" => ["ghtk_id" => "1954", "ghtk_name" => "Huyện Mường La", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "525" => ["ghtk_id" => "1955", "ghtk_name" => "Huyện Phù Yên", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "567" => ["ghtk_id" => "1956", "ghtk_name" => "Huyện Quỳnh Nhai", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "582" => ["ghtk_id" => "1957", "ghtk_name" => "Huyện Sông Mã", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "583" => ["ghtk_id" => "1958", "ghtk_name" => "Huyện Sốp Cộp", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "669" => ["ghtk_id" => "1959", "ghtk_name" => "Huyện Thuận Châu", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "763" => ["ghtk_id" => "1960", "ghtk_name" => "Huyện Yên Châu", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "588" => ["ghtk_id" => "1961", "ghtk_name" => "Thành phố Sơn La", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "730" => ["ghtk_id" => "2101", "ghtk_name" => "Huyện Vân Hồ", "pickup_id" => $this->defaultSenderId, "is_supported" => true],

                // Tay Ninh
            // "53" => ["ghtk_id" => null, "ghtk_name" => "Tây Ninh", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "110" => ["ghtk_id" => "1962", "ghtk_name" => "Huyện Bến Cầu", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "177" => ["ghtk_id" => "1963", "ghtk_name" => "Huyện Châu Thành", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "216" => ["ghtk_id" => "1964", "ghtk_name" => "Huyện Dương Minh Châu", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "288" => ["ghtk_id" => "1965", "ghtk_name" => "Huyện Gò Dầu", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "318" => ["ghtk_id" => "1966", "ghtk_name" => "Huyện Hòa Thành", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "603" => ["ghtk_id" => "1967", "ghtk_name" => "Huyện Tân Biên", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "606" => ["ghtk_id" => "1968", "ghtk_name" => "Huyện Tân Châu", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "691" => ["ghtk_id" => "1969", "ghtk_name" => "Huyện Trảng Bàng", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "626" => ["ghtk_id" => "1970", "ghtk_name" => "Thành phố Tây Ninh", "pickup_id" => $this->defaultSenderId, "is_supported" => true],

                // Thai Binh
            // "54" => ["ghtk_id" => null, "ghtk_name" => "Thái Bình", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "253" => ["ghtk_id" => "1971", "ghtk_name" => "Huyện Đông Hưng", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "340" => ["ghtk_id" => "1972", "ghtk_name" => "Huyện Hưng Hà", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "361" => ["ghtk_id" => "1973", "ghtk_name" => "Huyện Kiến Xương", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "568" => ["ghtk_id" => "1974", "ghtk_name" => "Huyện Quỳnh Phụ", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "636" => ["ghtk_id" => "1975", "ghtk_name" => "Huyện Thái Thụy", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "676" => ["ghtk_id" => "1976", "ghtk_name" => "Huyện Tiền Hải", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "752" => ["ghtk_id" => "1977", "ghtk_name" => "Huyện Vũ Thư", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "633" => ["ghtk_id" => "1978", "ghtk_name" => "Thành phố Thái Bình", "pickup_id" => $this->defaultSenderId, "is_supported" => true],

                // Thai Nguyen
            // "55" => ["ghtk_id" => null, "ghtk_name" => "Thái Nguyên", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "223" => ["ghtk_id" => "1979", "ghtk_name" => "Huyện Đại Từ", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "243" => ["ghtk_id" => "1980", "ghtk_name" => "Huyện Định Hóa", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "257" => ["ghtk_id" => "1981", "ghtk_name" => "Huyện Đồng Hỷ", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "504" => ["ghtk_id" => "1982", "ghtk_name" => "Huyện Phổ Yên", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "505" => ["ghtk_id" => "1983", "ghtk_name" => "Huyện Phú Bình", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "509" => ["ghtk_id" => "1984", "ghtk_name" => "Huyện Phú Lương", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "750" => ["ghtk_id" => "1985", "ghtk_name" => "Huyện Võ Nhai", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "635" => ["ghtk_id" => "1986", "ghtk_name" => "Thành phố Thái Nguyên", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "579" => ["ghtk_id" => "1987", "ghtk_name" => "Thành phố Sông Công", "pickup_id" => $this->defaultSenderId, "is_supported" => true],

                // Thanh Hoa
            // "56" => ["ghtk_id" => null, "ghtk_name" => "Thanh Hóa", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "84" => ["ghtk_id" => "1988", "ghtk_name" => "Huyện Bá Thước", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "159" => ["ghtk_id" => "1989", "ghtk_name" => "Huyện Cẩm Thủy", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "254" => ["ghtk_id" => "1990", "ghtk_name" => "Huyện Đông Sơn", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "296" => ["ghtk_id" => "1991", "ghtk_name" => "Huyện Hà Trung", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "311" => ["ghtk_id" => "1992", "ghtk_name" => "Huyện Hậu Lộc", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "329" => ["ghtk_id" => "1993", "ghtk_name" => "Huyện Hoằng Hóa", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "390" => ["ghtk_id" => "1994", "ghtk_name" => "Huyện Lang Chánh", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "442" => ["ghtk_id" => "1995", "ghtk_name" => "Huyện Mường Lát", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "465" => ["ghtk_id" => "1996", "ghtk_name" => "Huyện Nga Sơn", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "477" => ["ghtk_id" => "1997", "ghtk_name" => "Huyện Ngọc Lặc", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "485" => ["ghtk_id" => "1998", "ghtk_name" => "Huyện Như Thanh", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "486" => ["ghtk_id" => "1999", "ghtk_name" => "Huyện Như Xuân", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "494" => ["ghtk_id" => "2000", "ghtk_name" => "Huyện Nông Cống", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "535" => ["ghtk_id" => "2001", "ghtk_name" => "Huyện Quan Hóa", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "536" => ["ghtk_id" => "2002", "ghtk_name" => "Huyện Quan Sơn", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "546" => ["ghtk_id" => "2003", "ghtk_name" => "Huyện Quảng Xương", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "631" => ["ghtk_id" => "2004", "ghtk_name" => "Huyện Thạch Thành", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "656" => ["ghtk_id" => "2005", "ghtk_name" => "Huyện Thiệu Hóa", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "657" => ["ghtk_id" => "2006", "ghtk_name" => "Huyện Thọ Xuân", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "674" => ["ghtk_id" => "2007", "ghtk_name" => "Huyện Thường Xuân", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "682" => ["ghtk_id" => "2008", "ghtk_name" => "Huyện Tĩnh Gia", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "698" => ["ghtk_id" => "2009", "ghtk_name" => "Huyện Triệu Sơn", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "743" => ["ghtk_id" => "2010", "ghtk_name" => "Huyện Vĩnh Lộc", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "765" => ["ghtk_id" => "2011", "ghtk_name" => "Huyện Yên Định", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "642" => ["ghtk_id" => "2012", "ghtk_name" => "Thành phố Thanh Hóa", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "114" => ["ghtk_id" => "2013", "ghtk_name" => "Thị xã Bỉm Sơn", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "573" => ["ghtk_id" => "2014", "ghtk_name" => "Thị xã Sầm Sơn", "pickup_id" => $this->defaultSenderId, "is_supported" => true],

                // Thua Thien Hue
            // "57" => ["ghtk_id" => null, "ghtk_name" => "Thừa Thiên Huế", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "64" => ["ghtk_id" => "2015", "ghtk_name" => "Huyện A Lưới", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "346" => ["ghtk_id" => "2016", "ghtk_name" => "Thị Xã Hương Trà", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "456" => ["ghtk_id" => "2017", "ghtk_name" => "Huyện Nam Dông", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "501" => ["ghtk_id" => "2018", "ghtk_name" => "Huyện Phong Điền", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "508" => ["ghtk_id" => "2019", "ghtk_name" => "Huyện Phú Lộc", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "519" => ["ghtk_id" => "2020", "ghtk_name" => "Huyện Phú Vang", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "539" => ["ghtk_id" => "2021", "ghtk_name" => "Huyện Quảng Điền", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "339" => ["ghtk_id" => "2022", "ghtk_name" => "Thành phố Huế", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "345" => ["ghtk_id" => "2023", "ghtk_name" => "Thị xã Hương Thủy", "pickup_id" => $this->defaultSenderId, "is_supported" => true],

                // Tien Giang
            // "58" => ["ghtk_id" => null, "ghtk_name" => "Tiền Giang", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "139" => ["ghtk_id" => "2024", "ghtk_name" => "Huyện Cái Bè", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "137" => ["ghtk_id" => "2025", "ghtk_name" => "Huyện Cai Lậy", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "178" => ["ghtk_id" => "2026", "ghtk_name" => "Huyện Châu Thành", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "185" => ["ghtk_id" => "2027", "ghtk_name" => "Huyện Chợ Gạo", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "286" => ["ghtk_id" => "2028", "ghtk_name" => "Huyện Gò Công Đông", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "287" => ["ghtk_id" => "2029", "ghtk_name" => "Huyện Gò Công Tây", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "614" => ["ghtk_id" => "2030", "ghtk_name" => "Huyện Tân Phú Đông", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "615" => ["ghtk_id" => "2031", "ghtk_name" => "Huyện Tân Phước", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "449" => ["ghtk_id" => "2032", "ghtk_name" => "Thành phố Mỹ Tho", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "285" => ["ghtk_id" => "2033", "ghtk_name" => "Thị xã Gò Công", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "138" => ["ghtk_id" => "17533", "ghtk_name" => "Thị xã Cai Lậy", "pickup_id" => $this->defaultSenderId, "is_supported" => true],

                // Tra Vinh
            // "59" => ["ghtk_id" => null, "ghtk_name" => "Trà Vinh", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "146" => ["ghtk_id" => "2034", "ghtk_name" => "Huyện Càng Long", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "165" => ["ghtk_id" => "2035", "ghtk_name" => "Huyện Cầu Kè", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "166" => ["ghtk_id" => "2036", "ghtk_name" => "Huyện Cầu Ngang", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "179" => ["ghtk_id" => "2037", "ghtk_name" => "Huyện Châu Thành", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "213" => ["ghtk_id" => "2038", "ghtk_name" => "Huyện Duyên Hải", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "214" => ["ghtk_id" => "2038", "ghtk_name" => "Huyện Duyên Hải", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "681" => ["ghtk_id" => "2039", "ghtk_name" => "Huyện Tiểu Cần", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "685" => ["ghtk_id" => "2040", "ghtk_name" => "Huyện Trà Cú", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "688" => ["ghtk_id" => "2041", "ghtk_name" => "Thành phố Trà Vinh", "pickup_id" => $this->defaultSenderId, "is_supported" => true],

                // Tuyen Quang
            // "60" => ["ghtk_id" => null, "ghtk_name" => "Tuyên Quang", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "183" => ["ghtk_id" => "2042", "ghtk_name" => "Huyện Chiêm Hóa", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "310" => ["ghtk_id" => "2043", "ghtk_name" => "Huyện Hàm Yên", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "452" => ["ghtk_id" => "2044", "ghtk_name" => "Huyện Na hang", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "585" => ["ghtk_id" => "2045", "ghtk_name" => "Huyện Sơn Dương", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "773" => ["ghtk_id" => "2046", "ghtk_name" => "Huyện Yên Sơn", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "711" => ["ghtk_id" => "2047", "ghtk_name" => "Thành phố Tuyên Quang", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "395" => ["ghtk_id" => "2100", "ghtk_name" => "Huyện Lâm Bình", "pickup_id" => $this->defaultSenderId, "is_supported" => true],

                // Vinh Long
            // "61" => ["ghtk_id" => null, "ghtk_name" => "Vĩnh Long", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
//            "122" => ["ghtk_id" => "2048", "ghtk_name" => "Thị Xã Bình Minh", "pickup_id" => $this->defaultSenderId, "is_supported" => false],        // trung -> uu tiên support = true
            "125" => ["ghtk_id" => "2049", "ghtk_name" => "Huyện Bình Tân", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "405" => ["ghtk_id" => "2050", "ghtk_name" => "Huyện Long Hồ", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "424" => ["ghtk_id" => "2051", "ghtk_name" => "Huyện Mang Thít", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "593" => ["ghtk_id" => "2052", "ghtk_name" => "Huyện Tam Bình", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "687" => ["ghtk_id" => "2053", "ghtk_name" => "Huyện Trà Ôn", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "754" => ["ghtk_id" => "2054", "ghtk_name" => "Huyện Vũng Liêm", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "742" => ["ghtk_id" => "2055", "ghtk_name" => "Thành phố Vĩnh Long", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "122" => ["ghtk_id" => "8097", "ghtk_name" => "Thị xã Bình Minh", "pickup_id" => $this->defaultSenderId, "is_supported" => true],

                // Vinh Phuc
            // "62" => ["ghtk_id" => null, "ghtk_name" => "Vĩnh Phúc", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "128" => ["ghtk_id" => "2056", "ghtk_name" => "Huyện Bình Xuyên", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "399" => ["ghtk_id" => "2057", "ghtk_name" => "Huyện Lập Thạch", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "581" => ["ghtk_id" => "2058", "ghtk_name" => "Huyện Sông Lô", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "595" => ["ghtk_id" => "2059", "ghtk_name" => "Huyện Tam Đảo", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "594" => ["ghtk_id" => "2060", "ghtk_name" => "Huyện Tam Dương", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "748" => ["ghtk_id" => "2061", "ghtk_name" => "Huyện Vĩnh Tường", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "767" => ["ghtk_id" => "2062", "ghtk_name" => "Huyện Yên Lạc", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "749" => ["ghtk_id" => "2063", "ghtk_name" => "Thành phố Vĩnh Yên", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "528" => ["ghtk_id" => "2064", "ghtk_name" => "Thị xã Phúc Yên", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
//            "62" => ["ghtk_id" => "11240", "ghtk_name" => "đường Lý Thái Tổ", "pickup_id" => $this->defaultSenderId, "is_supported" => false],        // Khong co trong Kyna db
//            "62" => ["ghtk_id" => "11245", "ghtk_name" => "đường Nguyễn Trãi", "pickup_id" => $this->defaultSenderId, "is_supported" => false],       // Khong co trong Kyna db
//            "62" => ["ghtk_id" => "11254", "ghtk_name" => "đường Lý Thái Tổ", "pickup_id" => $this->defaultSenderId, "is_supported" => false],        // Khong co trong Kyna db
//            "62" => ["ghtk_id" => "11255", "ghtk_name" => "đường Đào Duy Anh", "pickup_id" => $this->defaultSenderId, "is_supported" => false],       // Khong co trong Kyna db
//            "62" => ["ghtk_id" => "11256", "ghtk_name" => "đường Tô Hiệu", "pickup_id" => $this->defaultSenderId, "is_supported" => false],           // Khong co trong Kyna db
//            "62" => ["ghtk_id" => "11258", "ghtk_name" => "đường Đàm Vạc", "pickup_id" => $this->defaultSenderId, "is_supported" => false],           // Khong co trong Kyna db
//            "62" => ["ghtk_id" => "11259", "ghtk_name" => "đường Lý Bôn", "pickup_id" => $this->defaultSenderId, "is_supported" => false],            // Khong co trong Kyna db
//            "62" => ["ghtk_id" => "11265", "ghtk_name" => "đường An Sơn", "pickup_id" => $this->defaultSenderId, "is_supported" => false],            // Khong co trong Kyna db

                // Yen Bai
            // "63" => ["ghtk_id" => null, "ghtk_name" => "Yên Bái", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "417" => ["ghtk_id" => "2065", "ghtk_name" => "Huyện Lục Yên", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "437" => ["ghtk_id" => "2066", "ghtk_name" => "Huyện Mù Cang Chải", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "689" => ["ghtk_id" => "2067", "ghtk_name" => "Huyện Trạm Tấu", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "693" => ["ghtk_id" => "2068", "ghtk_name" => "Huyện Trấn Yên", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "722" => ["ghtk_id" => "2069", "ghtk_name" => "Huyện Văn Chấn", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "727" => ["ghtk_id" => "2070", "ghtk_name" => "Huyện Văn Yên", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "762" => ["ghtk_id" => "2071", "ghtk_name" => "Huyện Yên Bình", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "761" => ["ghtk_id" => "2072", "ghtk_name" => "Thành phố Yên Bái", "pickup_id" => $this->defaultSenderId, "is_supported" => true],
            "474" => ["ghtk_id" => "2073", "ghtk_name" => "Thị xã Nghĩa Lộ", "pickup_id" => $this->defaultSenderId, "is_supported" => true],


            // Tp Ho Chi Minh
        ];
    }
}