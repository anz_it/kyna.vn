<?php

/**
 * Created by PhpStorm.
 * User: ngunp
 * Date: 5/12/2017
 * Time: 4:19 PM
 */
namespace kyna\payment\lib\proship;

use Yii;
use yii\base\InvalidParamException;
use yii\base\InvalidConfigException;
use yii\helpers\Json;
use yii\httpclient\JsonParser;

use common\helpers\ArrayHelper;
use donatj\Ini\Builder;


use kyna\order\models\Order;
use kyna\order\models\OrderInterface;
use kyna\payment\lib\proship\Mapper;
use kyna\payment\models\PaymentTransaction;
use kyna\payment\models\PaymentTransactionMeta;
use kyna\payment\models\TopupTransaction;
use kyna\servicecaller\traits\CurlTrait;
use kyna\settings\models\ShippingSetting;
use kyna\taamkru\models\Transaction;

class Proship extends \kyna\payment\lib\BasePayment
{
    use CurlTrait;

    /**
     * Constant
     */
    const KYNA_TRANSACTION_PREFIX = "KYN";
    const KYNA_TRANSACTION_FIELD = "id";

    const INVOICE_STATUS__CHO_LAY_HANG = 1;
    const INVOICE_STATUS__DANG_LAY_HANG = 2;
    const INVOICE_STATUS__HUY_LAY_HANG = 3;
    const INVOICE_STATUS__LUU_KHO = 4;
    const INVOICE_STATUS__DANG_GIAO_HANG = 5;
    const INVOICE_STATUS__GIAO_THAT_BAI = 6;
    const INVOICE_STATUS__TRA_HANG = 8;
    const INVOICE_STATUS__DA_GIAO_HANG = 9;

    public static  $invoiceStatusText = [
        self::INVOICE_STATUS__CHO_LAY_HANG => "Chờ lấy hàng",
        self::INVOICE_STATUS__DANG_LAY_HANG => "Đang lấy hàng",
        self::INVOICE_STATUS__HUY_LAY_HANG => "Hủy lấy hàng",
        self::INVOICE_STATUS__LUU_KHO => "Lưu kho",
        self::INVOICE_STATUS__DANG_GIAO_HANG => "Đang giao hàng",
        self::INVOICE_STATUS__GIAO_THAT_BAI => "Giao thất bại",
        self::INVOICE_STATUS__TRA_HANG => "Trả hàng",
        self::INVOICE_STATUS__DA_GIAO_HANG => "Đã giao hàng",
    ];

    const SHIPPING_METHOD__TIET_KIEM = 1;
    const SHIPPING_METHOD__SIEU_TOC = 2;
    const SHIPPING_METHOD__GIAO_NHANH = 3;
    const SHIPPING_METHOD__QUA_NGAY = 4;
    const SHIPPING_METHOD__BAN_DEM = 5;

    public static $shippingMethodText = [
        self::SHIPPING_METHOD__TIET_KIEM => "Tiết kiệm",
        self::SHIPPING_METHOD__SIEU_TOC => "Siêu tốc",
        self::SHIPPING_METHOD__GIAO_NHANH => "Giao nhanh",
        self::SHIPPING_METHOD__QUA_NGAY => "Qua ngày",
        self::SHIPPING_METHOD__BAN_DEM => "Ban đêm (Siêu tốc)",
    ];

    const  ADDITION_SERVICE__BAO_HIEM = 1;
    const  ADDITION_SERVICE__THU_HO = 2;
    const  ADDITION_SERVICE__HEN_GIO = 3;
    const  ADDITION_SERVICE__GUI_TAI_KHO = 7;

    public static $additionServiceText = [
        self::ADDITION_SERVICE__BAO_HIEM => "Bảo hiểm",
        self::ADDITION_SERVICE__THU_HO => "Thu hộ",
        self::ADDITION_SERVICE__HEN_GIO => "Hẹn giờ",
        self::ADDITION_SERVICE__GUI_TAI_KHO => "Gửi tại kho",
    ];


    /**
     * default values: these values will be replaced by config values in app->params
     *
     */
    private $_merchantSiteCode = '';
    private $_secureCode = "";
    private $_apiUrl = "";
    private $_pickUps = [];
    public $isCod = true;
    private $mapper;

    /**
     * Proship constructor.
     * load config values in app->params
     */
    public function __construct()
    {
        if (isset(Yii::$app->params['proship']) && $configs = Yii::$app->params['proship']) {
            $this->_merchantSiteCode = $configs['_merchantSiteCode'];
            $this->_secureCode = $configs['_secureCode'];
            $this->_apiUrl = $configs['_apiUrl'];
            $this->_pickUps = $configs['_pickUps'];

            $this->mapper = new Mapper();
        } else {
            // khong co trong param thi throw exception
            throw new \Exception("Cant find proship config - please re-check app params");
        }
    }

    protected function beforeCall(&$ch, &$data, $method=false)
    {
        // do nothing
        // TODO: init Curl Handler and preserve data for fucntion calling
        // Json encode data
        if (isset($data))
            $data = json_encode($data);
        else
            $data = json_encode([]);

        // Add
        switch ($method) {
            case 'POST':
                curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
                break;
            case 'PUT':
                curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json','Content-Length: ' . strlen($data)));
                break;
            default:
                break;
        }


    }
    protected function Signin()
    {
        // do nothing
    }

    /**
     * Validate, prepare shipping order information
     * Send shipping order to shipping company
     *
     * @param int $transId
     * @param $orderId
     * @param $amount
     * @param bool $params
     *
     *
     */
    public function pay($transId, $orderId, $amount, $params = false)
    {
        // Lay city / district code
        $mapper = new Mapper();
        $districtSettings = $mapper->getLocationAlias($params['location_id']);
        $citySettings = $mapper->getLocationAlias($params['city_id']);

        // Check if city, district in on service
        if ($citySettings == -1 || $districtSettings == -1)
            return false;

        // Validate city code / district code
        if (!self::_checkIsValidLocation($citySettings, $districtSettings))
            return false;

        // If co senderId -> use this senderId, else use default senderId
        if (isset($params) && isset($params['pick_up_location']))
            $senderId = $params['pick_up_location'];
        else
            $senderId = $citySettings['pickup_id'];

        // Validate other things here

        // Prepare params
        $orderDetails = [
            'orderCode' => self::KYNA_TRANSACTION_PREFIX . $transId, // NOT REQUIRED
            'productName' => 'Đơn hàng #'.$orderId.'. Phiếu đăng ký khóa học trên Kyna.vn',
            'weigh' => 0,
            'addressId' => $senderId,
            'codCost' => $amount,
            'description' => $params['note'],
            'receiverName' => $params['contact_name'],
            'receiverPhone' => $params['phone_number'],
            'receiverAddress' => $params['street_address'],
            'receiverCity' => $citySettings,
            'receiverDistrict' => $districtSettings,
//            'receiverWard' => 0,   // NOT REQUIRED
            'shippingMethod' => self::SHIPPING_METHOD__QUA_NGAY,   // 1: Tiết kiệm, 2: Siêu tốc, 3: Giao nhanh, 4: Qua ngày, 5: Ban đêm - Tam thoi de default = 1
            'additionService' => [self::ADDITION_SERVICE__GUI_TAI_KHO,], // 1: Bảo Hiểm, 2: Thu Hộ, 3: Hẹn giờ, 7: Gửi tại kho
            'payBy' => 1,            // 1: Người gửi trả phí - 2: Người nhận trả phí
        ];
        // Call Add Order API
        $result = json_decode(self::addOrder($orderDetails), true);

        // Filter result:
        $filtered_result = [];
        $filtered_result['error'] = null;
        if ($result['code'] != 0) {
            // Error
            $filtered_result['error'] = $result['message'];
        } else {
            // Success
            $filtered_result['transactionId'] = $transId;
            $filtered_result['transactionCode'] = $result['data']['invoiceId'];
            $filtered_result['amount'] = $amount;
            $filtered_result['responseString'] = http_build_query($result);
            $filtered_result['orderId'] = $orderId;
            $filtered_result['fee'] = $result['data']['sumFee'];

            // Update shipping_status, pick_up_location_id, expected_payment_fee - NOT avalable for GHN
            $transaction = PaymentTransaction::findOne([
                'id' => $transId,
            ]);
            if (isset($transaction)) {
//                $transaction->updateAttributes(['shipping_status' => $result['data']['invoiceStatus']]);
                $transaction['shipping_status'] = $result['data']['invoiceStatus'];
                $transaction['pick_up_location_id'] = $senderId;
                if (isset($params['expected_payment_fee']))
                    $transaction['expected_payment_fee'] = $params['expected_payment_fee'];
                $transaction['bill_code'] = $result['data']['billCode'];

                $transaction->save();
            }
        }
        return $filtered_result;
    }

    public function query($transId)
    {
        // Do nothing
    }
    public function ipn($response)
    {
        // Do nothing
    }

    /**
     * Get shipping fee for every PickUp point
     * @param $params
     */
    public function calculateFee($params)
    {
        return $this->getShippingFee($params);
        // do nothing
    }


    public function getFeeList($params)
    {
        $formatter = Yii::$app->formatter;
        // get proship cityId/districtId
        $cityId = $this->mapper->getLocationAlias($params['cityId']);
        $districtId = $this->mapper->getLocationAlias($params['districtId']);

        // Validate cityId/districtId
        $isValidated = $this->_checkIsValidLocation($cityId, $districtId);
        if (!$isValidated)
            return false;

        // Get Pickups List
        $pickUpList = $this->_pickUps;

        // Get codCost
        $order = Order::findOne([
            'id' => $params['orderId']
        ]);
        if (!isset($order))
            return false;
        $codCost = (isset($order['total']))? $order['total'] : 0;

        $result = [];
        foreach ($pickUpList as $pickUp) {
            $input = [
                'price' => 0,
                'senderCity' => $pickUp['cityId'],
                'senderDistrict' => $pickUp['districtId'],
                'codCost' => $codCost,
                'receiverCity' => $cityId,
                'receiverDistrict' => $districtId,
                'shippingMethod' => self::SHIPPING_METHOD__QUA_NGAY,
                'additionService' => [self::ADDITION_SERVICE__GUI_TAI_KHO,],
            ];
            $res = Json::decode($this->getShippingFee($input), true);
            if (!isset($res))
                continue;
            if ($res['code'] != 0)
                continue;
            $result[] = [
                'id' => $pickUp['id'],
                'name' => $pickUp['name'],
                'fee' => $res['data']['sumFee'],
                'displayFee' => $formatter->asCurrency($res['data']['sumFee']),
            ];
        }
        return $result;
    }

    /**
     * get-city-list API
     * @return mixed
     */
    public function getCityList()
    {
        $method = 'GET';
        $command = 'v1/city/list';
        $params = [
            'merchantSiteCode' => $this->_merchantSiteCode,
            'secureCode' => $this->_secureCode
        ];
        $endpoint = $this->_buildUrl($command, $params);
//        return $this->_call($endpoint, null, $method);
        return $this->call($endpoint, null, $method);
    }

    /**
     * get-district-list API
     * @param $cityId
     * @return mixed
     */
    public function getDistrictList($cityId)
    {
        $method = 'GET';
        $command = 'v1/district/list';
        $params = [
            'merchantSiteCode' => $this->_merchantSiteCode,
            'secureCode' => $this->_secureCode,
            'cityId' => $cityId
        ];
        $endpoint = $this->_buildUrl($command, $params);
//        return $this->_call($endpoint, null, $method);
        return $this->call($endpoint, null, $method);
    }

    /**
     * @param $districId
     * @return mixed
     */
    public function getWardList($districId)
    {
        $method = 'GET';
        $command = 'v1/ward/list';
        $params = [
            'merchantSiteCode' => $this->_merchantSiteCode,
            'secureCode' => $this->_secureCode,
            'districId' => $districId
        ];
        $endpoint = $this->_buildUrl($command, $params);
//        return $this->_call($endpoint, null, $method);
        return $this->call($endpoint, null, $method);
    }

    public function getOrderDetail($invoiceId)
    {
        $method = 'GET';
        $command = 'v1/invoice/detail';
        $params = [
            'merchantSiteCode' => $this->_merchantSiteCode,
            'secureCode' => $this->_secureCode,
            'invoiceId' => $invoiceId
        ];
        $endpoint = $this->_buildUrl($command, $params);
//        return $this->_call($endpoint, null, $method);
        return $this->call($endpoint, null, $method);
    }

    public function addOrder($orderDetails)
    {
        $method = 'POST';
        $isJsonData = true;
        $command = 'v1/invoice/add';

        // Prepare params
            // Short
//        $params = $orderDetails;
//        $params['merchantSiteCode'] = $this->_merchantSiteCode;
//        $params['secureCode'] = $this->_secureCode;

            // Long
        $params = [
            'merchantSiteCode' => (int)$this->_merchantSiteCode,
            'secureCode' => (string)$this->_secureCode,
            'orderCode' => (string)$orderDetails['orderCode'], // NOT REQUIRED
            'productName' => (string)$orderDetails['productName'],
            'addressId' => (int)$orderDetails['addressId'],
            'weigh' => (float)$orderDetails['weigh'],
            'codCost' => (float)$orderDetails['codCost'],
            'description' => (string)$orderDetails['description'],
            'receiverName' => (string)$orderDetails['receiverName'],
            'receiverPhone' => (string)$orderDetails['receiverPhone'],
            'receiverAddress' => (string)$orderDetails['receiverAddress'],
            'receiverCity' => (int)$orderDetails['receiverCity'],
            'receiverDistrict' => (int)$orderDetails['receiverDistrict'],
//            'receiverWard' => (int)$orderDetails['receiverWard'],   // NOT REQUIRED
            'shippingMethod' => (int)$orderDetails['shippingMethod'],   // 1: Tiết kiệm, 2: Siêu tốc, 3: Giao nhanh, 4: Qua ngày, 5: Ban đêm - Tam thoi de default = 1
            'additionService' => $orderDetails['additionService'], // 1: Bảo Hiểm, 2: Thu Hộ, 3: Hẹn giờ, 7: Gửi tại kho
            'payBy' => (int)$orderDetails['payBy'],            // 1: Người gửi trả phí - 2: Người nhận trả phí
        ];

        $endpoint = $this->_buildUrl($command);
        return self::call($endpoint, $params, $method);
    }

    public function cancelOrder($invoiceId)
    {
        $method = 'POST';
        $command = 'v1/invoice/cancel-receive';
        $params = [
            'merchantSiteCode' => $this->_merchantSiteCode,
            'secureCode' => $this->_secureCode,
            'invoiceId' => $invoiceId,
        ];
        $endpoint = $this->_buildUrl($command);
//        return $this->_call($endpoint, $params, $method);
        return $this->call($endpoint, $params, $method);
    }

    /**
     * PROSHIP API - calculate Shipping Fee
     * @param $params
     * @return bool|mixed
     */
    public function getShippingFee($params)
    {
        $method = 'POST';
        $command = 'v1/invoice/calculate-fee';
        // Validate params
        if (!isset($params)
            || !isset($params['price'])
            || !isset($params['senderCity'])
            || !isset($params['senderDistrict'])
            || !isset($params['codCost'])
            || !isset($params['receiverCity'])
            || !isset($params['receiverDistrict'])
            || !isset($params['shippingMethod'])
        ) {
            return false;
        }
        // prepare Params
        $sentParams = [
            'merchantSiteCode' => $this->_merchantSiteCode,
            'secureCode' => $this->_secureCode,
            'weigh' => 0.1,
            'price' => $params['price'],
            'senderCity' => $params['senderCity'],
            'senderDistrict' => $params['senderDistrict'],
            'codCost' => $params['codCost'],
            'receiverCity' => $params['receiverCity'],
            'receiverDistrict' => $params['receiverDistrict'],
            'additionService' => $params['additionService'],
            'shippingMethod' => $params['shippingMethod'],
        ];
        $endpoint = $this->_buildUrl($command, $sentParams);
//        return $this->_call($endpoint, $sentParams, $method);
        return $this->call($endpoint, $sentParams, $method);
    }

    public static function getInvoiceStatusText($invoiceStatusInt)
    {
        if (isset(self::$invoiceStatusText[$invoiceStatusInt]))
            return self::$invoiceStatusText[$invoiceStatusInt];
        else
            return 'unknown';
    }

    public function updateShippingStatus($transId, $statusId, $fee = null)
    {
        // Get payment_transaction
        $transaction = PaymentTransaction::find()->where([
            'transaction_code' => $transId,
        ])->one();
        // Get order
        $order = Order::find()->where([
            'id' => $transaction['order_id'],
        ])->one();

        $result['transaction'] = $transaction;
        $result['order'] = $order;
        switch ($statusId) {
            case self::INVOICE_STATUS__CHO_LAY_HANG:                      // 1
            case self::INVOICE_STATUS__DANG_LAY_HANG:                     // 2
            case self::INVOICE_STATUS__LUU_KHO:                           // 4
            case self::INVOICE_STATUS__DANG_GIAO_HANG:                    // 5
            case self::INVOICE_STATUS__GIAO_THAT_BAI:                     // 6
            // Update payment_transaction
                $transaction['shipping_status'] = $statusId;
                $transaction['status'] = TopupTransaction::STATUS_SUCCESS;
                if (isset($fee))
                    $transaction['payment_fee'] = $fee;
                $transaction->save();
                // Update order
                if ($order['status'] != OrderInterface::ORDER_STATUS_COMPLETE) {
                    $order['payment_method'] = 'cod';
                    $order['shipping_method'] = 'proship';
                    $order['status'] = OrderInterface::ORDER_STATUS_DELIVERING;
//                    if (isset($fee))
//                        $order['shipping_fee'] = $fee;
                    $order->save();
                }
                break;
            case self::INVOICE_STATUS__HUY_LAY_HANG:                      // 3
                // Update payment_transaction
                $transaction['shipping_status'] = $statusId;
                $transaction['status'] = TopupTransaction::STATUS_FAIL;
                $transaction->save();
                // Update order
                if ($order['status'] != OrderInterface::ORDER_STATUS_COMPLETE) {
                    $order['payment_method'] = 'cod';   // default
                    $order['shipping_method'] = null;
//                  $order['affiliate_id'] = 0;
                    $order['status'] = OrderInterface::ORDER_STATUS_PENDING_CONTACT;
//                    $order['payment_fee'] = 0.00;
//                    $order['shipping_fee'] = 0.00;
                    $order->save();
                }
                break;
//            case self::INVOICE_STATUS__GIAO_THAT_BAI:                     // 6
            case self::INVOICE_STATUS__TRA_HANG:                          // 8
                // Update payment_transaction
                $transaction['shipping_status'] = $statusId;
                $transaction['status'] = TopupTransaction::STATUS_SUCCESS;
                if (isset($fee))
                    $transaction['payment_fee'] = $fee;
                $transaction->save();
                // Update order
                if ($order['status'] != OrderInterface::ORDER_STATUS_COMPLETE) {
                    $order['status'] = OrderInterface::ORDER_STATUS_WAITING_RETURN;
                    $order->save();
                }
                break;
//            case self::INVOICE_STATUS__TRA_HANG:                          // 8
//                // Update payment_transaction
//                $transaction['shipping_status'] = $statusId;
//                $transaction['status'] = TopupTransaction::STATUS_SUCCESS;
//                if (isset($fee))
//                    $transaction['payment_fee'] = $fee;
//                $transaction->save();
//                // Update order
//                Order::codReturn($order['id']);
//                if ($order['status'] != OrderInterface::ORDER_STATUS_COMPLETE) {
//                    Order::codReturn($order['id']);
///                }
//                break;
            case self::INVOICE_STATUS__DA_GIAO_HANG:                      // 9
                // Update payment_transaction
                $transaction['shipping_status'] = $statusId;
                $transaction['status'] = TopupTransaction::STATUS_SUCCESS;
                $transaction['is_call'] = TopupTransaction::BOOL_YES;
//                if (isset($fee))
//                    $transaction['payment_fee'] = $fee;
                $transaction->save();
                // Update order
//                Order::complete($order['id'], $order['total']);
                if ($order['status'] != OrderInterface::ORDER_STATUS_COMPLETE) {
                    Order::complete($order['id'], $order['total']);
//                    $order['payment_method'] = 'cod';
//                    $order['shipping_method'] = 'proship';
//                    $order['is_paid'] = 1;
//                    $order['is_done_telesale_process'] = 1;                             // telesale's job's done
//                    $order['status'] = OrderInterface::ORDER_STATUS_COMPLETE;
////                    if (isset($fee))
////                        $order['shipping_fee'] = $fee;
//                    $order->save();
                }
//                $order->scenario = Order::SCENARIO_COMPLETE;
//                $order->afterSave();
                break;
            default:
                break;
        }
        return $result;
    }

    private function _buildUrl($command, $params=null)
    {
        $url = $this->_apiUrl.$command;
        if ($params) {
            $url .= '?'.http_build_query($params);
        }
        return $url;
    }

    /**
     * Ham validate city_code, district_code
     * @param $citySettings
     * @param $districSettings
     * @return bool
     */
    private function _checkIsValidLocation($citySettings, $districtSettings)
    {
        // Get List of city code from Proship - check if citySettings exist in cityList
        $isValid = false;
        $cityList = json_decode(self::getCityList(), true);
        foreach ($cityList['data'] as $city) {
            if ($city['cityId'] == $citySettings) {
                $isValid = true;
                break;
            }
        }
        if (!$isValid)
            return false;
        // Get List of district code from Proship - check if districtSettings exist in districList
        $isValid = false;
        $districtList = json_decode(self::getDistrictList($citySettings), true);
        foreach ($districtList['data'] as $district) {
            if ($district['districtId'] == $districtSettings) {
                $isValid = true;
                break;
            }
        }
        return $isValid;
    }

    public function validateIpnMessage($params)
    {
        if (!isset($params))
            return false;

        if (!isset($params['method']))
            return false;

        switch ($params['method']) {
            case 'updateInvoiceStatus':
                if (!isset($params['data'])
                    || !isset($params['data']['invoiceId'])
                    || !isset($params['data']['invoiceStatus'])
                    || !isset($params['data']['orderCode'])
                    || !isset($params['data']['certificateCode'])
                ) {
                    return false;
                }
                $_invoiceId = $params['data']['invoiceId'];
                $_invoiceStatus = $params['data']['invoiceStatus'];
                $_orderCode = $params['data']['orderCode'];
                return (md5($_invoiceId. ' ' . $_invoiceStatus. ' ' . $_orderCode . ' '. $this->_secureCode) === $params['data']['certificateCode']);
                break;
            default;
                return false;
        }
    }

    public function  getPickUpList() {
        return $this->_pickUps;
    }
}