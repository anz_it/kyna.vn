<?php

namespace kyna\payment\lib\ipay;

use Yii;
use yii\httpclient\Client;
use yii\httpclient\CurlTransport;

class Ipay extends \kyna\payment\lib\BasePayment
{

    private static $_client = null;

    public function signIn()
    {
        // TODO: Implement signIn() method.
    }

    public function pay($transId, $orderId, $amount, $params = false)
    {
//        return [
//            'result' => true,
//            'amount' => 300000,
//            'transactionId' => $transId,
//            'error' => false
//        ];
        $data = [
            'transaction_id' => $orderId .' - '. $transId,
            'amount' => $amount,
            'serial' => $params['serial'],
            'pinCode' => $params['pinCode'],
            'telcoCode' => self::convertTelcoCode($params['serviceCode']),
        ];

        $request = self::createRequest(Yii::$app->params['kyna_payment_url'] . '/ipay/pay', 'POST', $data);
        $response = $request->send();

        $data = $response->data;
        if ($data['resCode'] == '00') {
            return [
                'result' => true,
                'amount' => $data['cardValue'],
                'transactionId' => $data['partnerTransId'],
                'error' => false
            ];
        } else {
            return [
                'result' => false,
                'amount' => 0,
                'error' => $data['description'],
                'transactionCode' => $data['resCode'],
            ];
        }
    }

    public static function convertTelcoCode($serviceCode)
    {
        $data = [
            'VTT' => 'VT',
            'VMS' => 'MB',
            'VNP' => 'VP',
        ];

        return isset($data[$serviceCode]) ? $data[$serviceCode] : '';
    }

    public function query($transId)
    {
        // TODO: Implement query() method.
    }

    public function ipn($response)
    {
        // TODO: Implement ipn() method.
    }

    public function calculateFee($amount)
    {
        return 0;
    }

    public static function getClient()
    {
        if (self::$_client == null)
            self::$_client = new Client(['transport' => CurlTransport::className()]);
        return self::$_client;
    }

    private static function createRequest($url, $method, $data = [], $headers = [])
    {
        $client = self::getClient();
        $request = $client->createRequest()
            ->setMethod($method)
            ->setUrl($url)
            ->setData($data)
            ->addHeaders($headers);

        return $request;
    }
}
