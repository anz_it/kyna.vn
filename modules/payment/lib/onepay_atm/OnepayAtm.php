<?php

namespace kyna\payment\lib\onepay_atm;

use Yii;
use kyna\servicecaller\traits\CurlTrait;

class OnepayAtm extends \kyna\payment\lib\BasePayment
{
    use CurlTrait;

    private $_vpc_User = 'op01';
    private $_vpc_Password = 'op123456';
    private $_vpc_AccessCode = 'J729DXFN';
    private $_vpc_SecureSecret = '0CA0D9A99DD30F07924C5BFADD4611DC';
    private $_vpc_Merchant = 'DVEDUCATION';

    private $_PayUrl = 'https://onepay.vn/onecomm-pay/vpc.op';
    private $_QueryUrl = 'https://mtf.onepay.vn/onecomm-pay/Vpcdps.op';
    protected static $defaultMethod = 'POST';
    
    public function __construct()
    {
        if (isset(Yii::$app->params['onepay']['atm']) && $configs = Yii::$app->params['onepay']['atm']) {
            $this->_vpc_User = $configs['_vpc_User'];
            $this->_vpc_Password = $configs['_vpc_Password'];
            $this->_vpc_AccessCode = $configs['_vpc_AccessCode'];
            $this->_vpc_SecureSecret = $configs['_vpc_SecureSecret'];
            $this->_vpc_Merchant = $configs['_vpc_Merchant'];
            
            $this->_PayUrl = $configs['_PayUrl'];
            $this->_QueryUrl = $configs['_QueryUrl'];
        }
    }

    public function pay($transId, $orderId, $amount, $params = false)
    {
        $ip = \Yii::$app->request->getUserIP();
        if (!$ip) {
            return $this->error('user ip cannot be detected');
        }

        $transactionPrefix = \Yii::$app->params['transactionPrefix'];
        if (empty($transactionPrefix)) {
            $transactionPrefix = 'test_';
        }

        $requestTransId = $this->_vpc_Merchant."_{$transactionPrefix}".$transId.'_'.$orderId;

        $data = [
            'vpc_Version' => 2,
            'vpc_Command' => 'pay',
            'vpc_Locale' => 'vn',
            'vpc_Currency' => 'VND',
            'vpc_AccessCode' => $this->_vpc_AccessCode,
            'vpc_Merchant' => $this->_vpc_Merchant,
            'vpc_ReturnURL' => $this->returnUrl,
            'vpc_MerchTxnRef' => $requestTransId,
            'vpc_OrderInfo' => $orderId,
            'vpc_Amount' => (int) $amount * 100,
            'vpc_TicketNo' => $ip,
            'Title' => 'Thanh toan cho Kyna.vn, don hang '.$orderId,
            'AgainLink' => $this->returnUrl,
        ];

        ksort($data);

        $queryString = http_build_query($data).'&vpc_SecureHash='.$this->_hash($data);

        return $this->_PayUrl.'?'.$queryString;
    }

    public function calculateFee($amount)
    {
        return 0;
    }

    public function signIn()
    {
    }

    public function query($transId)
    {
        $data = [
            'vpc_Command' => 'queryDR',
            'vpc_Version' => 1,
            'vpc_MerchTxnRef' => $transId,
            'vpc_Merchant' => $this->_vpc_Merchant,
            'vpc_AccessCode' => $this->_vpc_AccessCode,
            'vpc_User' => $this->_vpc_User,
            'vpc_Password' => $this->_vpc_Password,
        ];

        $queryString = http_build_query($data).'&vpc_SecureHash='.$this->_hash($data);
        $endpoint = $this->_QueryUrl.'?'.$queryString;

        $responseStr = $this->call($endpoint, null);
        parse_str($responseStr, $response);

        $amount = isset($response['vpc_Amount']) ? $response['vpc_Amount'] / 100 : 0;

        $return = $this->_getResponseStatus($response, [
            'transactionId' => 'vpc_MerchTxnRef',
            'transactionCode' => 'vpc_TransactionNo',
            'responseString' => $responseStr,
            'amount' => $amount,
        ]);
        if ($return['error']) {
            return false;
        }

        return $return;
    }

    public function ipn($response)
    {
        if (!$this->_validateHash($response)) {
            $this->ipnResponse = 'responsecode=0';

            return false;
        }

        $responseStr = http_build_query($response);
        $amount = isset($response['vpc_Amount']) ? $response['vpc_Amount'] / 100 : 0;

        $return = $this->_getResponseStatus($response, [
            'transactionId' => 'vpc_MerchTxnRef',
            'transactionCode' => 'vpc_TransactionNo',
            'amount' => $amount,
            'responseString' => $responseStr,
            'orderId' => 'vpc_OrderInfo',
        ]);

        if ($return['error']) {
            $this->ipnResponse = 'responsecode=0';
        } else {
            $this->ipnResponse = 'responsecode=1&desc=confirm-success';
        }

        return $return;
    }

    /**
     * Get Transaction ID from Database
     * @param $transactionRequestId
     * @param $orderId
     * @return mixed
     */
    public function getTransactionId($transactionRequestId, $orderId)
    {
        $transactionPrefix = \Yii::$app->params['transactionPrefix'];
        if (empty($transactionPrefix)) {
            $transactionPrefix = 'test_';
        }
        $requestParam = $this->_vpc_Merchant."_{$transactionPrefix}";
        return preg_replace("/{$requestParam}([0-9]+)_{$orderId}/", "$1", $transactionRequestId);
    }

    protected function beforeCall(&$ch, &$data)
    {
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    }

    protected function errorMap()
    {
        return [
            1 => 'Ngân hàng từ chối giao dịch',
            3 => 'Mã đơn vị không tồn tại',
            4 => 'Không đúng access code',
            5 => 'Số tiền không hợp lệ',
            6 => 'Mã tiền tệ không tồn tại',
            7 => 'Lỗi không xác định',
            8 => 'Số thẻ không đúng',
            9 => 'Tên chủ thẻ không đúng',
            10 => 'Thẻ hết hạn/Thẻ bị khóa',
            11 => 'Thẻ chưa đăng ký sử dụng dịch vụ',
            12 => 'Ngày phát hành/Hết hạn không đúng',
            13 => 'Vượt quá hạn mức thanh toán',
            21 => 'Số tiền không đủ để thanh toán',
            22 => 'Thông tin tài khoản không đúng',
            23 => 'Tài khoản bị khóa',
            24 => 'Thông tin thẻ không đúng',
            25 => 'OTP không đúng',
            253 => 'Quá thời gian thanh toán',
            99 => 'Người sử dụng hủy giao dịch',
            300 => 'Giao dịch đang chờ thực hiện',
        ];
    }

    private function _getResponseStatus($response, $returnArray)
    {
        $error = null;
        if (isset($response['vpc_DRExists']) and $response['vpc_DRExists'] !== 'Y') {
            return ['error' => 'Transaction doesn\'t exist'];
        }
        if (!isset($response['vpc_TxnResponseCode'])) {
            return ['error' => 'Invalid response code'];
        }
        if ($response['vpc_TxnResponseCode'] != '0') {
            $responseCode = $response['vpc_TxnResponseCode'];

            $errorMap = $this->errorMap();
            $error = array_key_exists($responseCode, $errorMap) ? $errorMap[$responseCode] : 'Mã lỗi không hợp lệ';
        }

        $return = ['error' => $error];
        foreach ($returnArray as $key => $value) {
            $responseValue = isset($response[$value]) ? $response[$value] : $value;
            $return[$key] = $responseValue;
        }

        return $return;
    }

    private function _validateHash($data)
    {
        if (!isset($data['vpc_SecureHash'])) {
            return false;
        }

        $vpc_SecureHash = strtoupper($data['vpc_SecureHash']);
        $hash = $this->_hash($data);

        return $vpc_SecureHash === $hash;
    }

    private function _hash($data)
    {
        $secret = $this->_vpc_SecureSecret;

        unset($data['AgainLink']);
        unset($data['Title']);
        unset($data['vpc_SecureHash']);

        $arr = [];
        foreach ($data as $key => $val) {
            $res = \common\helpers\StringHelper::startWith('vpc_', $key);
            if ($res) {
                $arr[$key] = $val;
            }
        }
        unset($arr['vpc_SecureHash']);
        ksort($arr);

        $hashString = '';
        foreach ($arr as $key => $val) {
            $hashString .= $key.'='.$val.'&';
        }
        $hashString = trim($hashString, '&');
        $hash = hash_hmac('SHA256', $hashString, pack('H*', $secret));

        return strtoupper($hash);
    }
}
