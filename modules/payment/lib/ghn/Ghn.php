<?php

namespace kyna\payment\lib\ghn;

use Yii;
use yii\base\InvalidConfigException;
use yii\helpers\Json;
use yii\base\InvalidParamException;
use donatj\Ini\Builder;
use kyna\servicecaller\traits\CurlTrait;
use kyna\order\models\Order;
use kyna\payment\models\PaymentTransaction;
use kyna\order\models\OrderInterface;

class Ghn extends \kyna\payment\lib\BasePayment
{
    use CurlTrait;

    const KYNA_TRANSACTION_PREFIX = "";
    const KYNA_TRANSACTION_FIELD = "transaction_code";

    // shipping status from GHN
    const SHIPPING_STATUS_READY_TO_PICK = 'ReadyToPick';
    const SHIPPING_STATUS_PICKING = 'Picking';
    const SHIPPING_STATUS_STORING = 'Storing';
    const SHIPPING_STATUS_DELIVERING = 'Delivering';
    const SHIPPING_STATUS_DELIVERED = 'Delivered';
    const SHIPPING_STATUS_RETURN = 'Return';
    const SHIPPING_STATUS_RETURNED = 'Returned';
    const SHIPPING_STATUS_CANCEL = 'Cancel';

    public static $shippingStatusCode = [
        self::SHIPPING_STATUS_READY_TO_PICK => 1,
        self::SHIPPING_STATUS_PICKING => 2,
        self::SHIPPING_STATUS_STORING => 3,
        self::SHIPPING_STATUS_DELIVERING => 4,
        self::SHIPPING_STATUS_DELIVERED => 5,
        self::SHIPPING_STATUS_RETURN => 6,
        self::SHIPPING_STATUS_RETURNED => 7,
        self::SHIPPING_STATUS_CANCEL => 8,
    ];

    public static $shippingStatusText = [
        self::SHIPPING_STATUS_READY_TO_PICK => 'Chờ lấy hàng',
        self::SHIPPING_STATUS_PICKING => 'Đang lấy hàng',
        self::SHIPPING_STATUS_STORING => 'Lưu kho',
        self::SHIPPING_STATUS_DELIVERING => 'Đang giao hàng',
        self::SHIPPING_STATUS_DELIVERED => 'Đã giao hàng',
        self::SHIPPING_STATUS_RETURN => 'Đang trả hàng',
        self::SHIPPING_STATUS_RETURNED => 'Đã trả hàng',
        self::SHIPPING_STATUS_CANCEL => 'Hủy lấy hàng',
    ];

    public static function getShippingStatusText($shippingStatusCode)
    {
        $shippingStatus = array_search($shippingStatusCode, self::$shippingStatusCode);
        if (isset(self::$shippingStatusText[$shippingStatus])) {
            return self::$shippingStatusText[$shippingStatus];
        }
        return false;
    }

    // private $_apiUrl = 'https://apipds.ghn.vn/External/B2C/';
    // private $_clientId = '53813';
    // private $_apiKey = 'dSLjeJstcjwcbcLe';
    // private $_apiSecretKey = 'F7BB3C08E9BCA7E8D880B3D9D57FB4DF';
    // private $_password = '1234567890';

    // Production Mode
    //apiUrl: https://apipds.ghn.vn/External/B2C/
    //clientID: 67556
    //apiKey: dSLjeJstcjwcbcLe
    //apiSecretKey: F7BB3C08E9BCA7E8D880B3D9D57FB4DF
    //password: xkHtw5jEjpxjtzxqn
    private $_apiUrl =  'https://apipds.ghn.vn/External/B2C/';
    private $_clientId = '67556';
    private $_apiKey = 'dSLjeJstcjwcbcLe';
    private $_apiSecretKey = 'F7BB3C08E9BCA7E8D880B3D9D57FB4DF';
    private $_password = 'xkHtw5jEjpxjtzxqn';

    private $_configFile = 'ghn.ini';
    public $settings;
    public $isCod = true;

    protected static $defaultMethod = 'POST';

    public function __construct()
    {
        $this->_config();
        if (!$this->getToken()) {
            $this->signIn();
        }

        if (isset(Yii::$app->params['ghn']) && $configs = Yii::$app->params['ghn']) {
            $this->_apiUrl = $configs['_apiUrl'];
            $this->_clientId = $configs['_clientId'];
            $this->_apiKey = $configs['_apiKey'];
            $this->_apiSecretKey = $configs['_apiSecretKey'];
            $this->_password = $configs['_password'];
        }
    }

    protected function signIn()
    {
        $command = 'SignIn';

        $endpoint = $this->_buildUrl($command);
        $response = $this->call($endpoint);

        if (!$response['SessionToken']) {
            throw new InvalidConfigException('GHN: '.$response['ErrorMessage']);
        }
        $this->setToken($response['SessionToken']);
    }

    public function pay($transId, $orderId, $amount, $params = false)
    {
        $command = 'CreateShippingOrder';
        $locationSettings = $this->settings['Locations'];
        $districtSettings = $locationSettings[$params['location_id']];
        $citySettings = $locationSettings[$params['city_id']];

        if(!empty($districtSettings['service'])){
            $serviceId = $districtSettings['service'];
        }else{
            $serviceId = $citySettings['service'];
        }
        // Get Service Available Before Create Order
        $fromDistCode = $this->getDistCodeFromPickHubsId(trim($citySettings['pickup']));
        $serviceAvailable = $this->getServiceID($fromDistCode, trim($districtSettings['code']));
        if(!empty($serviceAvailable['Services'][0])){
            $serviceId = self::getServiceIdFromGHN($serviceAvailable['Services']);
            if ($serviceId == 0) {
                $serviceId = $serviceAvailable['Services'][0]['ShippingServiceID'];
            }
        }
        $params = [
            'RecipientName' => $params['contact_name'],
            'DeliveryAddress' => $params['street_address'],
            'RecipientPhone' => $params['phone_number'],
            'ClientOrderCode' => self::KYNA_TRANSACTION_PREFIX . $transId,
            'CODAmount' => $amount,
            'ContentNote' => 'Đơn hàng #'.$orderId.'. Phiếu đăng ký khóa học trên Kyna.vn',
            'DeliveryDistrictCode' => trim($districtSettings['code']),
            'ServiceID' => trim($serviceId),
            'PickHubID' => trim($citySettings['pickup']),
            'Weight' => 0,
            'Length' => 0,
            'Width' => 0,
            'Height' => 0,
            'IsDropOff' => true
        ];

        $endpoint = $this->_buildUrl($command);
        $result = $this->call($endpoint, $params);

        return $this->_return($result, [
            'transactionId' => $transId,
            'transactionCode' => 'OrderCode',
            'amount' => $amount,
            'responseString' => http_build_query($result),
            'orderId' => $orderId,
            'fee' => 'TotalFee',
        ]);
    }

    public function ipn($response) {
        // do nothing
    }

    public function calculateFee($params)
    {
        $command = 'CalculateServiceFee';

        $locationSettings = $this->settings['Locations'];
        $districtSettings = $locationSettings[$params['location_id']];
        $citySettings = $locationSettings[$params['city_id']];

        $from = '1A02'; // Currently fixed
        $to = $districtSettings['code']; // Currently fixed
        $serviceId = $citySettings['service']; // Currently fixed

        $data = [
            'Items' => [
                [
                    'FromDistrictCode' => $from,
                    'ToDistrictCode' => $to,
                    'ServiceID' => $serviceId,
                    'Weight' => 0,
                    'Width' => 0,
                    'Length' => 0,
                    'Height' => 0,
                ],
            ],
        ];

        $endpoint = $this->_buildUrl($command);
        $result = $this->call($endpoint, $data);

        $result['TotalFee'] = 0;
        if (isset($result['Items']) and is_array($result['Items']) and sizeof($result['Items'])) {
            foreach ($result['Items'] as $item) {
                $result['TotalFee'] += $item['ServiceFee'];
            }
        }
        return $this->_return($result, [
            'fee' => 'TotalFee',
        ]);
    }

    public function query($shippingCode)
    {
        $command = 'GetOrderInfo';
        $params = ['OrderCode' => $shippingCode];

        $endpoint = $this->_buildUrl($command);
        $result = $this->call($endpoint, $params);

        return $this->_return($result, [
            'status' => 'CurrentStatus',
            'returnCode' => 'ReturnCode',
        ]);
    }

    public function saveSettings($settings) {
        $builder = new Builder();
        $configFile = __DIR__.'/'.$this->_configFile;
        $fileContent = $builder->generate($settings);
        $tmpFile = $configFile.'.tmp';
        $break = false;

        $fp = fopen($tmpFile, 'a+');
        stream_set_blocking($fp, 0);

        $pieces = str_split($fileContent, 1024 * 4);
        foreach ($pieces as $piece) {
            if (flock($fp, LOCK_EX)) {
                if (fwrite($fp, $piece, strlen($piece)) === false) {
                    $break = true;
                    break;
                }
            }
            flock($fp, LOCK_UN);
        }
        fclose($fp);

        if (!$break) {
            rename($tmpFile, $configFile);
        }
    }

    /*
    public function cancelOrder($shippingCode)
    {
        $command = 'CancelOrder';
        $params = ['OrderCode' => $shippingCode];

        $endpoint = $this->_buildUrl($command);
        $result = $this->call($endpoint, $params);

        return $this->_return($result, true);
    }
    */

    protected function beforeCall(&$ch, &$data)
    {
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8'));

        if (!$data or !is_array($data)) {
            $data = [];
        }
        if ($token = $this->getToken()) {
            $data += ['SessionToken' => $token];
        }
        $data += $this->_header();
        $data = Json::encode($data);
    }
    protected function afterCall(&$response)
    {
        try {
            $response = Json::decode($response);
        } catch (InvalidParamException $e) {
            echo $response;
        }
    }

    private function _config()
    {
        $configFile = __DIR__.'/'.$this->_configFile;

        if (!file_exists($configFile)) {
            file_put_contents($configFile, '');
        }

        $this->settings = parse_ini_file($configFile, true);
    }

    private function _return($response, $returnArray)
    {
        // update shipping status default
        if (!empty($returnArray['transactionId']) && empty($response['ErrorMessage'])) {
            $transaction = PaymentTransaction::findOne([
                'id' => $returnArray['transactionId'],
            ]);
            $transaction->shipping_status = self::$shippingStatusCode[self::SHIPPING_STATUS_READY_TO_PICK];
            $transaction->save(false);
        }
        $return = ['error' => null];
        if (!empty($response['ErrorMessage'])) {
            $return['error'] = $response['ErrorMessage'];
        }

        if (!is_array($returnArray)) {
            return $returnArray;
        }

        if (!sizeof($returnArray)) {
            return $response;
        }

        foreach ($returnArray as $key => $value) {
            $responseValue = isset($response[$value]) ? $response[$value] : $value;
            $return[$key] = $responseValue;
        }

        return $return;
    }

    private function _header()
    {
        return [
            'ApiKey' => $this->_apiKey,
            'ApiSecretKey' => $this->_apiSecretKey,
            'ClientID' => $this->_clientId,
            'Password' => $this->_password,
        ];
    }

    private function _buildUrl($command, $params = false)
    {
        $url = $this->_apiUrl.$command;
        if ($params) {
            $url .= '?'.http_build_query($params);
        }

        return $url;
    }

    public function getPickHub()
    {
        $pickHubs = null;
        $command = 'GetPickHubs';

        $endpoint = $this->_buildUrl($command);
        $pickHubs = $this->call($endpoint, null);
        return $pickHubs;
    }

    public function getStaticPickHub()
    {
        return [
            [   'PickHubID' => '178039',
                'Address' => "Lầu 6 Tòa nhà Thịnh Phát - 178/8 Đường D1, Phường 25, Quận Bình Thạnh, TP Hồ Chí Minh",
                'DistrictCode' => '0216',
                'DistrictName' =>  'Quận Bình Thạnh'
            ],
            [   'PickHubID' => '178043',
                'Address' => "Tầng 6, Tòa Nhà Viện Công Nghệ - 25 Vũ Ngọc Phan, Phường Láng Hạ, Quận Đống Đa, TP Hà Nội",
                'DistrictCode' => '1A04',
                'DistrictName' =>  'Quận Đống Đa'
            ],
            [   'PickHubID' => '195792',
                'Address' => "90/45 Lý Nam Đế , Phường 7 ",
                'DistrictCode' => '0211',
                'DistrictName' =>  'Quận 11'
            ]
        ];
    }

    /**
     * @param $pickHubId
     * @return null
     */
    public function getDistCodeFromPickHubsId($pickHubId)
    {
        $fromDistCode = null;
        $pickHub = $this->getPickHub();
        if(empty($pickHub['HubInfo'])){
            $pickHub['HubInfo'] = $this->getStaticPickHub();
        }
        if(!empty($pickHub['HubInfo'])){
            foreach($pickHub['HubInfo'] as $code){
                if($code['PickHubID'] == $pickHubId){
                    $fromDistCode = $code['DistrictCode'];
                    return $fromDistCode;
                }
            }
        }
        return $fromDistCode;
    }

    /**
     * @param $fromDistCode
     * @param $toDistCode
     * @return mixed
     * @throws \kyna\servicecaller\traits\Exception
     */
    public function getServiceID($fromDistCode, $toDistCode)
    {
        $command = 'GetServiceList';

        $params = [
            'FromDistrictCode' => $fromDistCode,
            'ToDistrictCode' => $toDistCode,
        ];

        $endpoint = $this->_buildUrl($command);
        $services = $this->call($endpoint, $params);

        return $services;
    }

    /**
     * Define all shipping service
     * @return array
     */
    private function getShippingServiceList() {
        return array(
            //'17' => 'Gói siêu tốc (kiện)',
            //'18' => 'Gói tiết kiệm (kiện)',
            '53327' => '6 Ngày',
            '53324' => '5 Ngày',
            '53323' => '4 Ngày',
            '53322' => '3 Ngày',
            '53321' => '2 Ngày',
            '53320' => '1 Ngày',
            '53319' => '6 Giờ',
            '53326' => '4 Giờ',
            '53329' => '60 phút',
            //'53325' => 'Prime',
            //'53330' => 'Chuyển phát cá nhân',
            //'53339' => '266',
            //'53346' => 'Thu hộ',
            //'53347' => 'Dịch vụ ứng tiền 60P'
        );
    }

    /**
     * Return Service with longest time
     * @param $serviceAvailable
     * @return int
     */
    private function getServiceIdFromGHN($serviceAvailable) {
        $returnValue = 0;
        $serviceList = self::getShippingServiceList();
        foreach ($serviceList as $serviceKey => $value) {
            foreach ($serviceAvailable as $item) {
                if ($item['ShippingServiceID'] == intval($serviceKey)) {
                    $returnValue = $item['ShippingServiceID'];
                }
                if ($returnValue > 0) {
                    break;
                }
            }
            if ($returnValue > 0) {
                break;
            }
        }
        return $returnValue;
    }

    /**
     * Validate params from IPN API GHN
     * @param $params
     * @return bool
     */
    public function validateIpnMessage($params)
    {
        if (empty($params['orderCode']) || empty($params['orderStatus'])) {
            return false;
        }
        return true;
    }

    public function updateShippingStatus($params)
    {
        $transaction = PaymentTransaction::find()
            ->where(['transaction_code' => $params['orderCode']])
            ->one();
        if (!empty($transaction)) {
            // update shipping status
            $this->updateShippingTransaction($transaction->id, $params['orderStatus']);
            // update order
            switch ($params['orderStatus']) {
                case 'Delivered':
                    $this->completeTransaction($transaction->id);
                    if (!empty($this->getOrder($transaction->order_id))) {
                        $result['message'] = "Complete transaction code {$transaction->transaction_code}";
                        Order::complete($transaction->order_id, $transaction->transaction_code);
                    }
                    break;
                case 'Return':
                    if (!empty($this->getOrder($transaction->order_id))) {
                        $result['message'] = "Return transaction code {$transaction->transaction_code}";
                        Order::makeCodWaitingForReturn($transaction->order_id);
                    }
                    break;
                case 'Returned':
                    $this->completeTransaction($transaction->id);
                    if (!empty($this->getOrder($transaction->order_id, Order::ORDER_STATUS_WAITING_RETURN))) {
                        $result['message'] = "Returned transaction code {$transaction->transaction_code}";
                        Order::codReturn($transaction->order_id);
                    }
                    break;
                case 'Cancel':
                    $order = $this->getOrder($transaction->order_id, Order::ORDER_STATUS_DELIVERING);
                    if (!empty($order)) {
                        $order->shipping_method = null;
                        $order->status = OrderInterface::ORDER_STATUS_PENDING_CONTACT;
                        $order->save(false);
                        $result['message'] = "Cancel transaction code {$transaction->transaction_code}";
                    }
                    break;
                case 'ReadyToPick':
                case 'Picking':
                case 'Storing':
                case 'Delivering':
                    $result['message'] = "Updated transaction status";
                    break;
                default:
                    $result['message'] = "Invalid transaction status";
                    break;
            }
            $result['success'] = true;
            if (empty($result['message'])) {
                $result['message'] = "Transaction code {$params['orderCode']} has been updated before";
            }
        } else {
            $result['message'] = "Transaction code {$params['orderCode']} does not exist";
        }

        return $result;
    }

    private function completeTransaction($id)
    {
        $transaction = PaymentTransaction::findOne($id);
        if ($transaction) {
            $transaction->is_call = PaymentTransaction::BOOL_YES;
            $transaction->status = PaymentTransaction::BOOL_YES;
            $transaction->save(false);
        }
    }

    private function updateShippingTransaction($id, $shippingStatus)
    {
        $transaction = PaymentTransaction::findOne($id);
        if ($transaction && isset(self::$shippingStatusCode[$shippingStatus])) {
            if ($shippingStatus == self::SHIPPING_STATUS_CANCEL) {
                $transaction->status = PaymentTransaction::STATUS_FAILED;
            }
            $transaction->shipping_status = self::$shippingStatusCode[$shippingStatus];
            $transaction->save(false);
        }
    }

    private function getOrder($id, $status = null)
    {
        if ($status == null) {
            $status = [
                Order::ORDER_STATUS_NEW,
                Order::ORDER_STATUS_PENDING_PAYMENT,
                Order::ORDER_STATUS_PENDING_CONTACT,
                Order::ORDER_STATUS_DELIVERING,
            ];
        }
        return Order::find()->filterWhere([
            'id' => $id,
            'status' => $status
        ])->one();
    }
}
