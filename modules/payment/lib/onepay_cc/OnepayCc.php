<?php

namespace kyna\payment\lib\onepay_cc;

use Yii;
use kyna\servicecaller\traits\CurlTrait;

class OnepayCc extends \kyna\payment\lib\BasePayment
{
    use CurlTrait;

    private $_vpc_User = 'op01';
    private $_vpc_Password = 'op123456';
    private $_vpc_AccessCode = '5B42E1BA';
    private $_vpc_SecureSecret = 'FBE2666800B3E7F832520D80256BE936';
    private $_vpc_Merchant = 'DVEDUCATION';

    private $_PayUrl = 'https://onepay.vn/vpcpay/vpcpay.op';
    private $_QueryUrl = 'https://mtf.onepay.vn/vpcpay/Vpcdps.op';
    protected static $defaultMethod = 'POST';

    public function __construct()
    {
        if (isset(Yii::$app->params['onepay']['cc']) && $configs = Yii::$app->params['onepay']['cc']) {
            $this->_vpc_User = $configs['_vpc_User'];
            $this->_vpc_Password = $configs['_vpc_Password'];
            $this->_vpc_AccessCode = $configs['_vpc_AccessCode'];
            $this->_vpc_SecureSecret = $configs['_vpc_SecureSecret'];
            $this->_vpc_Merchant = $configs['_vpc_Merchant'];

            $this->_PayUrl = $configs['_PayUrl'];
            $this->_QueryUrl = $configs['_QueryUrl'];
        }
    }

    public function pay($transId, $orderId, $amount, $params = false)
    {
        $ip = \Yii::$app->request->getUserIP();
        if (!$ip) {
            return $this->error('user ip cannot be detected');
        }

        $transactionPrefix = \Yii::$app->params['transactionPrefix'];
        if (empty($transactionPrefix)) {
            $transactionPrefix = 'test_';
        }

        $requestTransId = $this->_vpc_Merchant."_{$transactionPrefix}".$transId.'_'.$orderId;

        $data = [
            'vpc_Version' => 2,
            'vpc_Command' => 'pay',
            'vpc_Locale' => 'vn',
            'vpc_AccessCode' => $this->_vpc_AccessCode,
            'vpc_Merchant' => $this->_vpc_Merchant,
            'vpc_ReturnURL' => $this->returnUrl,
            'vpc_MerchTxnRef' => $requestTransId,
            'vpc_OrderInfo' => $orderId,
            'vpc_Amount' => (int) $amount * 100,
            'vpc_TicketNo' => $ip,
            'Title' => 'Thanh toan cho Kyna.vn, don hang '.$orderId,
            'AgainLink' => $this->returnUrl,
        ];

        ksort($data);

        $queryString = http_build_query($data).'&vpc_SecureHash='.$this->_hash($data);

        return $this->_PayUrl.'?'.$queryString;
    }

    public function calculateFee($amount)
    {
        return 0;
    }

    public function signIn()
    {
    }

    public function query($transId)
    {
        $data = [
            'vpc_Command' => 'queryDR',
            'vpc_Version' => 1,
            'vpc_MerchTxnRef' => $transId,
            'vpc_Merchant' => $this->_vpc_Merchant,
            'vpc_AccessCode' => $this->_vpc_AccessCode,
            'vpc_User' => $this->_vpc_User,
            'vpc_Password' => $this->_vpc_Password,
        ];

        $queryString = http_build_query($data).'&vpc_SecureHash='.$this->_hash($data);
        $endpoint = $this->_QueryUrl.'?'.$queryString;

        $responseStr = $this->call($endpoint, null);
        parse_str($responseStr, $response);
        $return = $this->_getResponseStatus($response, [
            'transactionId' => 'vpc_MerchTxnRef',
            'transactionCode' => 'vpc_TransactionNo',
            'responseString' => $responseStr,
        ]);
        if ($return['error']) {
            return false;
        }

        return $return;
    }

    public function ipn($response)
    {
        if (!$this->_validateHash($response)) {
            $this->ipnResponse = 'responsecode=0';

            return false;
        }

        $responseStr = http_build_query($response);
        $return = $this->_getResponseStatus($response, [
            'transactionId' => 'vpc_MerchTxnRef',
            'transactionCode' => 'vpc_TransactionNo',
            'responseString' => $responseStr,
            'orderId' => 'vpc_OrderInfo',
        ]);

        if ($return['error']) {
            $this->ipnResponse = 'responsecode=0';
        } else {
            $this->ipnResponse = 'responsecode=1&desc=confirm-success';
        }

        return $return;
    }

    /**
     * Get Transaction ID from Database
     * @param $transactionRequestId
     * @param $orderId
     * @return mixed
     */
    public function getTransactionId($transactionRequestId, $orderId)
    {
        $transactionPrefix = \Yii::$app->params['transactionPrefix'];
        if (empty($transactionPrefix)) {
            $transactionPrefix = 'test_';
        }
        $requestParam = $this->_vpc_Merchant."_{$transactionPrefix}";
        return preg_replace("/{$requestParam}([0-9]+)_{$orderId}/", "$1", $transactionRequestId);
    }

    protected function beforeCall(&$ch, &$data)
    {
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    }

    protected function errorMap()
    {
        return [
            1 => 'Ngân hàng phát hành thẻ không cấp phép',
            2 => 'Ngân hàng phát hành từ chối cấp phép',
            3 => 'Cổng thanh toán không nhận được kết quả trả về từ ngân hàng phát hành thẻ.',
            4 => 'Thẻ hết hạn sử dụng',
            5 => 'Thẻ không đủ hạn mức hoặc tài khoản không đủ số dư thanh toán',
            6 => 'Lỗi từ ngân hàng phát hành thẻ.',
            7 => 'Lỗi phát sinh trong quá trình xử lý giao dịch',
            8 => 'Ngân hàng phát hành thẻ không hỗ trợ giao dịch Internet',
            9 => 'Ngân hàng phát hành thẻ từ chối giao dịch.',
            99 => 'Người dùng hủy giao dịch',
            'B' => 'Không xác thực được 3D-Secure (chủ thẻ chưa tham gia, không nhập hoặc nhập sai mật khẩu)',
            'F' => 'Giao dịch không qua được bước xác thực 3D-Secure (khách hàng nhập sai mã xác thực)',
            'I' => 'Xác thực CSC bị lỗi',
            'U' => 'Thông tin CSC sai',
            'W' => 'Không xác thực được 3D-Secure',
            'E' => 'Nhập sai CSC hoặc thẻ vượt quá hạn mức lần thanh toán',
            'Z' => 'Bị chặn bởi hệ thống ODF',
        ];
    }

    private function _getResponseStatus($response, $returnArray)
    {
        $error = null;
        if (isset($response['vpc_DRExists']) and $response['vpc_DRExists'] !== 'Y') {
            return ['error' => 'Transaction doesn\'t exist'];
        }
        if (!isset($response['vpc_TxnResponseCode'])) {
            return ['error' => 'Invalid response code'];
        }
        if ($response['vpc_TxnResponseCode'] != '0') {
            $responseCode = $response['vpc_TxnResponseCode'];

            $errorMap = $this->errorMap();
            $error = array_key_exists($responseCode, $errorMap) ? $errorMap[$responseCode] : 'Mã lỗi không hợp lệ';
        }

        $return = ['error' => $error];
        foreach ($returnArray as $key => $value) {
            $responseValue = isset($response[$value]) ? $response[$value] : $value;
            $return[$key] = $responseValue;
        }

        return $return;
    }

    private function _validateHash($data)
    {
        if (!isset($data['vpc_SecureHash'])) {
            return false;
        }

        $vpc_SecureHash = strtoupper($data['vpc_SecureHash']);
        $hash = $this->_hash($data);

        return $vpc_SecureHash === $hash;
    }

    private function _hash($data)
    {
        $secret = $this->_vpc_SecureSecret;
        $arr = [];
        foreach ($data as $key => $val) {
            $res = \common\helpers\StringHelper::startWith('vpc_', $key);
            if ($res) {
                $arr[$key] = $val;
            }
        }
        unset($arr['vpc_SecureHash']);
        ksort($arr);

        $hashString = '';
        foreach ($arr as $key => $val) {
            $hashString .= $key.'='.$val.'&';
        }
        $hashString = rtrim($hashString, '&');

        $hash = hash_hmac('SHA256', $hashString, pack('H*', $secret));

        return strtoupper($hash);
    }
}
