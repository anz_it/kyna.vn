<?php

namespace kyna\payment\lib;

abstract class BasePayment extends \kyna\servicecaller\BaseCaller
{
    public $method;
    public $returnUrl;
    public $transNo;
    public $ipnResponse;
    public $errors;
    public $isCod = false;

    abstract public function pay($transId, $orderId, $amount, $params = false);
    abstract public function query($transId);
    abstract public function ipn($response);
    abstract public function calculateFee($amount);
}
