<?php
/**
 * Created by PhpStorm.
 * User: macintosh
 * Date: 11/6/17
 * Time: 2:29 PM
 */

namespace kyna\payment\lib\tiki;

use kyna\payment\lib\BasePayment;

class Tiki extends BasePayment
{

    const KYNA_TRANSACTION_PREFIX = "";
    const KYNA_TRANSACTION_FIELD = "order_id";

    public function pay($transId, $orderId, $amount, $params = false)
    {
        return true;
    }

    public function query($transId)
    {
        return false;
    }
    public function ipn($response)
    {
        return false;
    }

    public function calculateFee($params)
    {
        return 0;
    }

    public function signIn()
    {
        // TODO: Implement signIn() method.
        // do nothing,
    }
}