<?php

namespace kyna\payment\lib\momo;
use yii\httpclient\Client;
use yii\httpclient\CurlTransport;
use Yii;

class Momo extends \kyna\payment\lib\BasePayment
{
    private static $_client = null;
    private $_payUrl = "";
    private $_partnerCode = "";
    private $_accessKey = "";
    private $_serectKey = "";
    private $_notifyUrl = "https://momo.vn/notify";
    private $_requestType = "captureMoMoWallet";
    private $_extraData = "merchantName=KYNA";
    private $_merchantName = "KYNA";

    public function __construct()
    {
        if (!empty(Yii::$app->params['momo'])) {
            $configs = Yii::$app->params['momo'];

            $this->_payUrl = $configs['_payUrl'];
            $this->_partnerCode = $configs['_partnerCode'];
            $this->_accessKey = $configs['_accessKey'];
            $this->_serectKey = $configs['_serectKey'];
        }
    }

    protected function signIn()
    {
        // TODO: Implement signIn() method.
    }

    public function pay($transId, $orderId, $amount, $params = false)
    {
        $ip = \Yii::$app->request->getUserIP();
        if (!$ip) {
            return $this->error('user ip cannot be detected');
        }

        $transactionPrefix = \Yii::$app->params['transactionPrefix'];
        if (empty($transactionPrefix)) {
            $transactionPrefix = 'test_';
        }

        $requestTransId = $this->_merchantName . "_{$transactionPrefix}" . $transId . '_' . $orderId;

        $data = [];
        $data['partnerCode'] = $this->_partnerCode;
        $data['accessKey'] = $this->_accessKey;
        $data['requestId'] = $requestTransId;
        $data['amount'] = (string)$amount;
        $data['orderId'] = (string)$orderId."_".$transId;
        $data['orderInfo'] = "Thanh toán đơn hàng ".$orderId;
        $data['returnUrl'] = $this->returnUrl;
        $data['notifyUrl'] = $this->_notifyUrl;
        $data['extraData'] = $this->_extraData;
        $data['requestType'] = $this->_requestType;
        $hashString = $this->_hash($data);
        $data['signature'] = $hashString;

        $request = self::createRequest($this->_payUrl, 'POST', $data, ['Content-Type' => 'application/json;charset=UTF-8']);

        $response = $request->send();

        $result = $response->data;
        if ($result['errorCode'] == 0) {
            return $result['payUrl'];
        }
        return false;
    }

    public function query($transId)
    {
        // TODO: Implement query() method.
    }

    public function ipn($response)
    {
        if (!$this->_validateHash($response)) {
            $this->ipnResponse = 'responsecode=0';
            return false;
        }
        $responseStr = http_build_query($response);
        $amount = isset($response['amount']) ? $response['amount'] : 0;

        $return = $this->_getResponseStatus($response, [
            'transactionCode' => 'transId',
            'amount' => $amount,
            'responseString' => $responseStr,
            'orderId' => 'orderId',
        ]);

        if(!empty($return['orderId'])){
            $parts = explode("_", $return['orderId']);
            $return['orderId'] = $parts[0];
            //Get transaction Id
            if(!empty($parts[1])){
                $return['transactionId'] = $parts[1];
            }
        }

        if ($return['error']) {
            $this->ipnResponse = 'responsecode=0';
        } else {
            $this->ipnResponse = 'responsecode=1&desc=confirm-success';
        }

        return $return;
    }
    private function _getResponseStatus($response, $returnArray)
    {
        $error = null;

        if ($response['errorCode'] != '0') {
            $responseCode = $response['errorCode'];

            $errorMap = $this->errorMap();
            $error = array_key_exists($responseCode, $errorMap) ? $errorMap[$responseCode] : 'Mã lỗi không hợp lệ';
        }

        $return = ['error' => $error];
        foreach ($returnArray as $key => $value) {
            $responseValue = isset($response[$value]) ? $response[$value] : $value;
            $return[$key] = $responseValue;
        }

        return $return;
    }
    public function calculateFee($amount)
    {
        // TODO: Implement calculateFee() method.
    }

    private function _hash($data)
    {

        $rawHash = "partnerCode=" . $data['partnerCode'] . "&accessKey=" . $data['accessKey'] . "&requestId=" . $data['requestId']
            . "&amount=" . $data['amount'] . "&orderId=" . $data['orderId']
            . "&orderInfo=" . $data['orderInfo'] . "&returnUrl=" . $data['returnUrl'] .
            "&notifyUrl=" . $data['notifyUrl'] . "&extraData=" . $data['extraData'];
        return hash_hmac("sha256", $rawHash, $this->_serectKey);

    }
    private function _hashReturn($data){
        $rawHash =
            "partnerCode=".$data['partnerCode'].
            "&accessKey=".$data['accessKey'].
            "&requestId=".$data['requestId'].
            "&amount=".$data['amount'].
            "&orderId=".$data['orderId'].
            "&orderInfo=".$data['orderInfo'].
            "&orderType=".$data['orderType'].
            "&transId=".$data['transId'].
            "&message=".$data['message'].
            "&localMessage=".$data['localMessage'].
            "&responseTime=".$data['responseTime'].
            "&errorCode=".$data['errorCode'].
            "&payType=".$data['payType'].
            "&extraData=".$data['extraData'];
        $result = hash_hmac("sha256", $rawHash, $this->_serectKey);
        return $result;
    }

    private function _validateHash($data)
    {
        if (!isset($data['signature'])) {
            return false;
        }
        $secureHash = strtolower($data['signature']);
        $hash = $this->_hashReturn($data);
        return $secureHash === $hash;
    }

    public static function getClient()
    {
        if (self::$_client == null)
            self::$_client = new Client(['transport' => CurlTransport::className()]);
        return self::$_client;
    }

    private static function createRequest($url, $method, $data = [], $headers = [])
    {
        $client = self::getClient();
        $request = $client->createRequest()
            ->setMethod($method)
            ->setUrl($url)
            ->setData($data)
            ->addHeaders($headers)
            ->setFormat(Client::FORMAT_JSON);

        return $request;
    }

    public function getTransactionId($transactionRequestId, $orderId)
    {
        return $transactionRequestId;
    }

    protected function errorMap()
    {
        return [
            0 => 'Giao dịch thành công.',
            7 => 'Giao dịch này không được thực hiện. Quý khách vui lòng liên hệ (08) 399 171 99 để được hỗ trợ',
            12 => 'Số tiền không hợp lệ. Vui lòng kiểm tra thông tin trước khi tiếp tục giao dịch.',
            19 => 'Tài khoản Khách hàng ngừng giao dịch. Vui lòng liên hệ (08) 399 171 99 để được hỗ trợ.',
            22 => 'Tài khoản nhận cần khác với tài khoản chuyển. Vui lòng kiểm tra thông tin trước khi tiếp tục thực hiện giao dịch.',
            23 => 'Số tiền tối thiểu là xxđ. Vui lòng nhập số tiền lớn hơn.',
            24 => 'Số tiền tối đa là xxđ. Vui lòng nhập số tiền nhỏ hơn.',
            32 => ' Mất kết nối. Sau khi giao dịch  thành công, tiền sẽ trừ vào tài khoản MoMo. Quý khách vui lòng liên hệ (08) 399 171 99 để được hỗ trợ.',
            37 => 'Mất kết nối. Sau khi giao dịch thành công, tiền sẽ trừ vào tài khoản MoMo. Quý khách vui lòng liên hệ (08) 399 171 99 để được hỗ trợ.',
            46 => ' Số tiền giao dịch vượt quá giới hạn cho phép. Vui lòng liên hệ (08) 399 171 99 để được hỗ trợ.',
            57 => 'Tài khoản Khách hàng đang ngừng sử dụng dịch vụ. Vui lòng liên hệ (08) 399 171 99 để được hỗ trợ.',
            58 => 'Tài khoản Khách hàng đang tạm khóa. Vui lòng liên hệ (08) 399 171 99 để được hỗ trợ.',
            70 => 'Tài khoản Khách hàng đang thực hiện một giao dịch khác. Vui lòng chờ kết quả hoặc liên hệ (08) 399 171 99 để được hỗ trợ.',
            71 => 'Tài khoản Khách hàng không tồn tại. Vui lòng kiểm tra thông tin hoặc liên hệ (08) 399 171 99 để được hỗ trợ.',
            72 => 'Target is deleted Giao dịch tạm thời không được thực hiện. Vui lòng liên hệ (08) 399 171 99 để được hỗ trợ.',
            103 => 'Giao dịch gặp lỗi. Quý khách vui lòng thực hiện lại.',
            1001 => 'Khách hàng cần nạp tiền vào Ví để tiếp tục giao dịch.',
            1003 => 'Số dư tài khoản MoMo người nhận vượt hạn mức cho phép.',
            1004 => 'Quý khách hết hạn mức giao dịch trong ngày. Vui lòng liên kết Ví với Tài khoản ngân hàng để tiếp tục giao dịch hoặc liên hệ (08) 399 171 99 để được hỗ trợ.',
            1006 => 'Dịch vụ tạm thời gặp lỗi. Quý khách vui lòng thực hiện lại sau.',
            1014 => 'Mã thanh toán hết hiệu lực, vui lòng đổi mã mới và thực hiện thanh toán lại. Liên hệ hỗ trợ: 1900 5454 41',
            150 => 'Sai mã thanh toán. Vui lòng kiểm tra trước khi thực hiện lại giao dịch.',

            151 => 'Số tiền có chứa kí tự không hợp lệ. Vui lòng kiểm tra trước khi thực hiện lại.',
            153 => 'Giao dịch gặp lỗi. Quý khách vui lòng thực hiện lại.',
            154 => 'Giao dịch gặp lỗi. Quý khách vui lòng thực hiện lại.',
            155 => 'Sai mã thanh toán. Vui lòng kiểm tra trước khi thực hiện lại giao dịch.',
            156 => 'Data payment in hash not match.',
            159 => 'Giao dịch gặp lỗi. Quý khách vui lòng thực hiện lại.',
            161 => 'Hạn mức thanh toán Quý khách đã chọn là < số tiền khách hàng Vui lòng cài đặt hạn mức lớn hơn để tiếp tục thanh toán.',
            801 => 'amount is invalid',
            802 => 'invalid date format',
            803 => 'amount is pending or on process. Pleasefinish the previous transaction',
            804 => 'not yet refund',
            2125 => 'Bill can’t refund. Bad data.',
            2118 => 'Dublicate requestId',
            2400 => 'Bad format data.',
        ];
    }
}