<?php

namespace kyna\payment\events;

class TransactionEvent extends \yii\base\Event
{
    public $transData = [];
    public $transId;

    public function addData($meta)
    {
        $this->transData = (array) $meta + $this->transData;
    }
}
