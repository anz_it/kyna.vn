<?php
/**
 * Created by PhpStorm.
 * User: hoa
 * Date: 15/11/2018
 * Time: 11:03
 */

namespace kyna\promotion\models;


use kyna\order\models\Order;
use kyna\order\models\OrderDetails;

class BirthDaySoroban
{
    const PREFIX = '';
    const PAYMENT_METHODS = ['onepay_atm','onepay_cc','cod','bank-transfer','momo','payoo'];
    const CourseSoroban = 898;
    const KynaEnglish = 1285;
    public static function checkIsPaymentOnline($payment_method){
        if(in_array($payment_method,self::PAYMENT_METHODS)){
            return true;
        }
        return false;
    }

    public static function generateCode($order_id){
        return self::PREFIX.$order_id;
    }

    public static function isCampaignSoroban($order_id){
        $orderDetailTable = OrderDetails::tableName();
        $orderTable = Order::tableName();
        $order = Order::find()
            ->leftJoin($orderDetailTable,"$orderDetailTable.order_id = $orderTable.id")
            ->where(["$orderTable.id"=>$order_id])
            ->andWhere(['or',
                ["$orderDetailTable.course_combo_id" => self::CourseSoroban],
                ["$orderDetailTable.course_combo_id" => self::KynaEnglish]
            ])
            ->andWhere(['IN',"$orderTable.payment_method",self::PAYMENT_METHODS])
            ->andWhere(["$orderTable.status"=>Order::ORDER_STATUS_COMPLETE])
            ->limit(1)
            ->one();

        if($order){
            return true;
        }
        return false;
    }

    public static function isCampaignSorobanCOD($order_id){
        $statusOrder = [Order::ORDER_STATUS_COMPLETE,Order::ORDER_STATUS_DELIVERING];
        $orderDetailTable = OrderDetails::tableName();
        $orderTable = Order::tableName();
        $order = Order::find()
            ->leftJoin($orderDetailTable,"$orderDetailTable.order_id = $orderTable.id")
            ->where(["$orderTable.id"=>$order_id])
            ->andWhere(['or',
                ["$orderDetailTable.course_combo_id" => self::CourseSoroban],
                ["$orderDetailTable.course_combo_id" => self::KynaEnglish]
            ])
            ->andWhere(['IN',"$orderTable.payment_method",self::PAYMENT_METHODS])
            ->andWhere(['IN',"$orderTable.status",$statusOrder])
            ->limit(1)
            ->one();

        if($order){
            return true;
        }
        return false;
    }

    public static function isDateRunCampaign(){
        if(isset(\Yii::$app->params['intervals_time_birthday_soroban'])){
            $date = \Yii::$app->params['intervals_time_birthday_soroban'];
            $from = $date[0];
            $to = $date[1];
            $start_time = new \DateTime($from);
            $end_time = new \DateTime($to);
            $now = new \DateTime('now');
            if ($start_time < $now && $now < $end_time) {
                return true;
            }
        }
        return false;
    }
}