<?php
/**
 * Created by PhpStorm.
 * User: vothienhoa
 * Date: 10/10/2018
 * Time: 10:22
 */

namespace kyna\promotion\models;

use kyna\user\models\User;
use kyna\promotion\models\Promotion;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "user_voucher_free".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $code
 * @property string $prefix
 * @property integer $total_introduce
 * @property integer $total_received
 * @property integer $total_consume
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $created_time
 * @property integer $updated_time
 */
class UserVoucherFree extends \kyna\base\ActiveRecord
{
    const PREFIX = 'FREEKYNA';

    function __construct() {
        parent::__construct();
        $this->setTotalIntroduce(0);
        $this->setTotalConsume(0);
        $this->setTotalReceived(1);
    }
    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * @param int $user_id
     */
    public function setUserId($user_id)
    {
        $this->user_id = $user_id;
    }

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param string $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }

    /**
     * @return string
     */
    public function getPrefix()
    {
        return $this->prefix;
    }

    /**
     * @param string $prefix
     */
    public function setPrefix($prefix)
    {
        $this->prefix = $prefix;
    }

    /**
     * @return int
     */
    public function getTotalIntroduce()
    {
        return $this->total_introduce;
    }

    /**
     * @param int $total_introduce
     */
    public function setTotalIntroduce($total_introduce)
    {
        $this->total_introduce = $total_introduce;
    }

    /**
     * @return int
     */
    public function getTotalReceived()
    {
        return $this->total_received;
    }

    /**
     * @param int $total_received
     */
    public function setTotalReceived($total_received)
    {
        $this->total_received = $total_received;
    }

    /**
     * @return int
     */
    public function getTotalConsume()
    {
        return $this->total_consume;
    }

    /**
     * @param int $total_consume
     */
    public function setTotalConsume($total_consume)
    {
        $this->total_consume = $total_consume;
    }

    /**
     * @return int
     */
    public function getCreatedBy()
    {
        return $this->created_by;
    }

    /**
     * @param int $created_by
     */
    public function setCreatedBy($created_by)
    {
        $this->created_by = $created_by;
    }

    /**
     * @return int
     */
    public function getUpdatedBy()
    {
        return $this->updated_by;
    }

    /**
     * @param int $updated_by
     */
    public function setUpdatedBy($updated_by)
    {
        $this->updated_by = $updated_by;
    }

    /**
     * @return int
     */
    public function getCreatedTime()
    {
        return $this->created_time;
    }

    /**
     * @param int $created_time
     */
    public function setCreatedTime($created_time)
    {
        $this->created_time = $created_time;
    }

    /**
     * @return int
     */
    public function getUpdatedTime()
    {
        return $this->updated_time;
    }

    /**
     * @param int $updated_time
     */
    public function setUpdatedTime($updated_time)
    {
        $this->updated_time = $updated_time;
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'code','prefix'], 'required'],
            [['user_id', 'total_introduce', 'total_received',
                'total_consume','created_time','updated_time','created_by','updated_by'], 'integer'],
            [['code'], 'string', 'max' => 255],
            [['prefix'], 'string', 'max' => 255],
            [['code','user_id'], 'unique']
        ];
    }

    public function checkUserExits()
    {
        if (!User::find()->where(['id' => $this->user_id])->exists()) {
            $this->addError('user_id', 'User không tồn tại trong hệ thống');
        }
    }

    public static function checkUserUseFirstMyVoucher($userPrefix,$userInCart,$code){
        $userVoucherFree = UserVoucherFree::findOne(['user_id'=>$userInCart]);
        if(!$userVoucherFree){
            $userVoucherFree = UserVoucherFree::newUserVoucherFree($userInCart,$code,UserVoucherFree::PREFIX);
        }
        $totalReceived = $userVoucherFree->getTotalReceived();
        $totalConsume = $userVoucherFree->getTotalConsume();
        if($totalReceived == 1 && !UserVoucherFree::isNotMyVoucher($userPrefix,$userInCart) && $totalConsume == 0){
            return false;
        }
        return true;
    }

    public static function checkApplyOrder($userPrefix, $userInCart){
        $userVoucherFree = UserVoucherFree::findOne(['user_id'=>$userInCart]);
        if($userVoucherFree){
            $totalReceived = $userVoucherFree->getTotalReceived();
            $totalConsume = $userVoucherFree->getTotalConsume();
            $totalIntroduce = $userVoucherFree->getTotalIntroduce();
            if(($totalReceived <= $totalConsume) || ($totalIntroduce % 3  != 0) ||($totalIntroduce == 0) ){
                if ($totalReceived > $totalConsume && UserVoucherFree::isNotMyVoucher($userPrefix,$userInCart) && $totalIntroduce % 3 != 0){
                    return true;
                }
                if ($totalReceived > $totalConsume && UserVoucherFree::isNotMyVoucher($userPrefix,$userInCart) && $totalIntroduce == 0){
                    return true;
                }
                return false;
            }else{
                return true;
            }
        }
    }

    public static function applyVoucherFree($userPrefix,$code,$prefix,$userInCart){
        if($prefix == self::PREFIX && !empty($userPrefix) && !empty($userInCart)){
            UserVoucherFree::updateTotalUsePrefix($userPrefix,$code,$prefix,$userInCart);
            UserVoucherFree::updateTotalUserInCart($userInCart,$code,$prefix);
        }
    }

    public static function updateTotalUsePrefix($userPrefix,$code,$prefix,$userInCart){
        $userVoucherFree = UserVoucherFree::findOne(['user_id'=>$userPrefix]);
        if($userVoucherFree){
            $totalIntroduce = $userVoucherFree->getTotalIntroduce();
            if(UserVoucherFree::isNotMyVoucher($userPrefix,$userInCart)){
                $userVoucherFree->setTotalIntroduce($totalIntroduce+1);
            }
            $userVoucherFree->setTotalReceived(floor($userVoucherFree->getTotalIntroduce()/3)+1);
            $userVoucherFree->save(false);
        }else{
            UserVoucherFree::addUserPrefix($userPrefix,$code,$prefix);
        }
    }

    public static function updateTotalUserInCart($userInCart,$code,$prefix){
        $userVoucherFree = UserVoucherFree::findOne(['user_id'=>$userInCart]);
        if($userVoucherFree){
            $totalConsume = $userVoucherFree->getTotalConsume();
            if($userVoucherFree->canUpdateTotalConsume($userVoucherFree->getTotalReceived(),$totalConsume)){
                $userVoucherFree->setTotalConsume($totalConsume+1);
            }
            $userVoucherFree->save(false);
        }else{
            UserVoucherFree::addUserInCart($userInCart,$code,$prefix);
        }
    }

    public function canUpdateTotalReceived($totalIntroduce){
        if(($totalIntroduce % 3 == 0) && $totalIntroduce != 0){
            return true;
        }
        return false;
    }

    public function canUpdateTotalConsume($totalReceived,$totalConsume){
        if($totalReceived > $totalConsume){
            return true;
        }
        return false;
    }

    public static function addUserPrefix($userPrefix,$code,$prefix){
        $userVoucherFree = new UserVoucherFree();
        $totalIntroduce = $userVoucherFree->getTotalIntroduce();
        $userVoucherFree->setTotalIntroduce($totalIntroduce+1);

        $userVoucherFree->setUserId($userPrefix);
        $userVoucherFree->setCode($code.$userPrefix);
        $userVoucherFree->setPrefix($prefix);
        $flag = $userVoucherFree->save(false);
        return $flag;
    }

    public static function addUserInCart($userInCart,$code,$prefix){
        $userVoucherFree = new UserVoucherFree();
        $totalConsume = $userVoucherFree->getTotalConsume();
        $userVoucherFree->setTotalConsume($totalConsume+1);
        $userVoucherFree->setUserId($userInCart);
        $userVoucherFree->setCode($code.$userInCart);
        $userVoucherFree->setPrefix($prefix);
        $flag = $userVoucherFree->save(false);
        return $flag;
    }

    public static function newUserVoucherFree($userId,$code,$prefix){
        $userVoucherFree = new UserVoucherFree();
        $userVoucherFree->setUserId($userId);
        $userVoucherFree->setCode($code.$userId);
        $userVoucherFree->setPrefix($prefix);
        $userVoucherFree->save(false);
        return $userVoucherFree;
    }

    public static function isNotMyVoucher($userPrefix,$userInCart){
        if($userPrefix != $userInCart){
            return true;
        }
        return false;
    }

    public static function isUserVoucherFree($voucherCode){

        $data = str_split($voucherCode, 8);
        if(!empty($data)){
            if($data[0] == self::PREFIX && isset($data[1]) && !is_null($data[1]) && is_numeric($data[1])){
                return $data;
            }else if($data[0] == self::PREFIX){
                $data[1] = null;
                return $data;
            }else{
                return false;
            }
        }
        return false;
    }

    public function getListCourseInPromotionVoucherFree(){
        $promotion = Promotion::findOne(['code'=>UserVoucherFree::PREFIX,'is_deleted'=>null,'status'=>1]);
        $data = $promotion->getCourses();
    }

    public static function getListMaxPriceProducts($cartItems,$listCourseCanApply){


        $items = [];
        foreach ($cartItems as $item) {
            $items[$item->id] =  $item->sellPrice;
        }
        $promotion = Promotion::findOne(['code'=>UserVoucherFree::PREFIX,'is_deleted'=>null,'status'=>1]);
        $dataCourses = $promotion->getCourses()
            ->andWhere(['id'=>array_keys($items)])->all();
        $dataWithCourseId =ArrayHelper::map($dataCourses,'id','id');
        if(empty($dataWithCourseId)){
            return $dataWithCourseId;
        }
        $data = [];
        foreach ($dataWithCourseId as $id){
            $data[$id] = $items[$id];
        }

        $maxValue = array_search(max($data), $data);
        $productMatch = [];
        foreach ($listCourseCanApply as $key => $value) {
            if($maxValue == $value)
            {
                $productMatch[] = $value;
            }
        }
        return $productMatch;
    }

    public static function getUserVoucherFree($user_id){

        $userVoucherFree = UserVoucherFree::findOne(['user_id'=>$user_id]);
        if(!$userVoucherFree){
            $userVoucherFree = UserVoucherFree::newUserVoucherFree($user_id,UserVoucherFree::PREFIX,UserVoucherFree::PREFIX);
        }
        return $userVoucherFree;
    }

    public static function isDateRunCampaignVoucherFree(){
        if(isset(\Yii::$app->params['intervals_time_voucher_free'])){
            $date = \Yii::$app->params['intervals_time_voucher_free'];
            $from = $date[0];
            $to = $date[1];
            $start_time = new \DateTime($from);
            $end_time = new \DateTime($to);
            $now = new \DateTime('now');
            if ($start_time < $now && $now < $end_time) {
                return true;
            }
            return false;

        }
    }
}