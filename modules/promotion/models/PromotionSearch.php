<?php

namespace kyna\promotion\models;

use yii\data\ActiveDataProvider;
use kyna\promotion\models\Promotion;

/**
 * VoucherSearch represents the model behind the search form about `kyna\promotion\models\Promotion`.
 */
class   PromotionSearch extends Promotion
{
    
    public $from_date;
    public $to_date;
    
    /**
     * @inheritdoc
     */
    public $kind = self::KIND_ORDER_APPLY;

    public function rules()
    {
        return [
            [['id', 'value', 'type', 'min_amount', 'end_date', 'user_id', 'partner_id', 'order_id', 'used_date', 'created_by', 'created_time', 'updated_time', 'number_usage', 'current_number_usage','user_number_usage', 'status'], 'safe'],
            [['code', 'note', 'from_date', 'to_date'], 'safe'],
            [['is_deleted', 'is_used'], 'boolean'],
        ];
    }

    public function search($params)
    {
        $query = Promotion::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'created_time' => SORT_DESC
                ]
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            $query->where('0=1');
            return $dataProvider;
        }
        $current_number_usage = $this->current_number_usage;
        if($current_number_usage != null){
            if(is_numeric($current_number_usage)){
                $current_number_usage = (int)$this->current_number_usage;
            }
            else{
                $current_number_usage = -1;
            }
        }
        
        $query->andFilterWhere([
            'id' => $this->id,
            'value' => $this->value,
            'type'  => $this->type,
            'min_amount' => $this->min_amount,
            'end_date' => $this->end_date,
            'seller_id' => $this->seller_id,
            'user_id' => $this->user_id,
            'order_id' => $this->order_id,
            'is_used' => $this->is_used,
            'used_date' => $this->used_date,
            'created_by' => $this->created_by,
            'is_deleted' => $this->is_deleted,
            'created_time' => $this->created_time,
            'updated_time' => $this->updated_time,
            'current_number_usage' => $current_number_usage,
            'number_usage' => $this->number_usage,
            'user_number_usage' => $this->user_number_usage,
            'partner_id' => $this->partner_id,
            'status' => $this->status,
            'issued_person' => $this->issued_person
        ]);
        
        $query->andFilterWhere(['like', 'code', $this->code])
            ->andFilterWhere(['like', 'note', $this->note]);
        
        if (!empty($this->from_date)) {
            $query->andWhere("DATE_FORMAT(from_unixtime(start_date), '%d/%m/%Y') = :from_date", ['from_date' => $this->from_date]);
        }
        
        if (!empty($this->to_date)) {
            $query->andWhere("DATE_FORMAT(from_unixtime(end_date), '%d/%m/%Y') = :to_date", ['to_date' => $this->to_date]);
        }

        return $dataProvider;
    }

    public function searchPrintHtml($params)
    {
        $query = Promotion::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'created_time' => SORT_DESC
                ]
            ],
            'pagination' => [
                'pageSize' => 500,
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'value' => $this->value,
            'type'  => $this->type,
            'min_amount' => $this->min_amount,
            'end_date' => $this->end_date,
            'seller_id' => $this->seller_id,
            'user_id' => $this->user_id,
            'order_id' => $this->order_id,
            'is_used' => $this->is_used,
            'used_date' => $this->used_date,
            'created_by' => $this->created_by,
            'is_deleted' => $this->is_deleted,
            'created_time' => $this->created_time,
            'updated_time' => $this->updated_time,
            'current_number_usage' => $this->current_number_usage,
            'number_usage' => $this->number_usage,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'code', $this->code])
            ->andFilterWhere(['like', 'note', $this->note]);

        return $dataProvider;
    }
}
