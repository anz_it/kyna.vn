<?php
/**
 * Created by PhpStorm.
 * User: hoa
 * Date: 22/11/2018
 * Time: 14:37
 */

namespace kyna\promotion\models;


use common\helpers\ArrayHelper;

class MinigameVoucher
{
    const PREFIX = 'FREEMINIGAME';

    public static function checkPrefix($code){
        $code = explode('_',$code);
        if(!empty($code[0]) && ($code[0] == self::PREFIX)){
            return true;
        }
        return false;
    }

    public static function checkProductCartMiniGame($cartItems,$listCourseCanApply){
        $items = 0;
        foreach ($cartItems as $item) {
            foreach($listCourseCanApply as $id)
            {
                if($item->id == $id){
                    $items ++;
                }
                if($items >=2){
                    return false;
                }
            }
        }
        return true;
    }
}