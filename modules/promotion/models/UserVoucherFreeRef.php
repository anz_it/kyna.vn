<?php
/**
 * Created by PhpStorm.
 * User: vothienhoa
 * Date: 10/10/2018
 * Time: 10:22
 */

namespace kyna\promotion\models;

use app\modules\cart\models\Product;
use kyna\course\models\Course;
use kyna\order\models\Order;
use kyna\tag\models\CourseTag;
use kyna\tag\models\Tag;
use kyna\user\models\User;
use kyna\promotion\models\Promotion;
use kyna\user\models\UserCourse;
use yii\helpers\ArrayHelper;
use yii\web\Cookie;

/**
 * This is the model class for table "user_voucher_free_ref".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $total_introduce
 * @property integer $total_received
 * @property integer $total_consume
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $created_time
 * @property integer $updated_time
 * @property string $user_relation_ref
 * @property string $prefix
 */
class UserVoucherFreeRef extends \kyna\base\ActiveRecord
{
    const EVENT_APPLY = 'apply';

    const LINK_CAMPAIGN = '/p/campaign/free-kyna';

    function __construct()
    {
        parent::__construct();
        $prefix = \Yii::$app->params['prefix_voucher_free_ref'];
        if (!empty($prefix)) {
            $this->setPrefix($prefix);
        }
        $this->setTotalIntroduce(0);
        $this->setTotalConsume(0);
        $this->setTotalReceived(1);
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * @param int $user_id
     */
    public function setUserId($user_id)
    {
        $this->user_id = $user_id;
    }

    /**
     * @return int
     */
    public function getTotalIntroduce()
    {
        return $this->total_introduce;
    }

    /**
     * @param int $total_introduce
     */
    public function setTotalIntroduce($total_introduce)
    {
        $this->total_introduce = $total_introduce;
    }

    /**
     * @return int
     */
    public function getTotalReceived()
    {
        return $this->total_received;
    }

    /**
     * @param int $total_received
     */
    public function setTotalReceived($total_received)
    {
        $this->total_received = $total_received;
    }

    /**
     * @return int
     */
    public function getTotalConsume()
    {
        return $this->total_consume;
    }

    /**
     * @param int $total_consume
     */
    public function setTotalConsume($total_consume)
    {
        $this->total_consume = $total_consume;
    }

    /**
     * @return int
     */
    public function getCreatedBy()
    {
        return $this->created_by;
    }

    /**
     * @param int $created_by
     */
    public function setCreatedBy($created_by)
    {
        $this->created_by = $created_by;
    }

    /**
     * @return int
     */
    public function getUpdatedBy()
    {
        return $this->updated_by;
    }

    /**
     * @param int $updated_by
     */
    public function setUpdatedBy($updated_by)
    {
        $this->updated_by = $updated_by;
    }

    /**
     * @return int
     */
    public function getCreatedTime()
    {
        return $this->created_time;
    }

    /**
     * @param int $created_time
     */
    public function setCreatedTime($created_time)
    {
        $this->created_time = $created_time;
    }

    /**
     * @return int
     */
    public function getUpdatedTime()
    {
        return $this->updated_time;
    }

    /**
     * @param int $updated_time
     */
    public function setUpdatedTime($updated_time)
    {
        $this->updated_time = $updated_time;
    }


    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return array
     */
    public function getUserRelationRef()
    {
        return explode(',', $this->user_relation_ref);
    }

    /**
     * @param string $user_relation_ref
     */
    public function setUserRelationRef($user_relation_ref)
    {
        if (empty($this->user_relation_ref)) {
            $this->user_relation_ref = $user_relation_ref;
        } else {
            $arrayUser = explode(',', $this->user_relation_ref);
            $arrayUser[] = $user_relation_ref;
            $string = implode(',', $arrayUser);
            $this->user_relation_ref = $string;
        }
    }

    /**
     * @return string
     */
    public function getPrefix()
    {
        return $this->prefix;
    }

    /**
     * @param string $prefix
     */
    public function setPrefix($prefix)
    {
        $this->prefix = $prefix;
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'prefix'], 'required'],
            [['user_id', 'total_introduce', 'total_received',
                'total_consume', 'created_time', 'updated_time', 'created_by', 'updated_by'], 'integer'],
            [['prefix'], 'string', 'max' => 255],
            [['prefix', 'user_id'], 'unique']
        ];
    }

    public static function getUserVoucherFreeRef($userRef)
    {
        $prefix = \Yii::$app->params['prefix_voucher_free_ref'];
        $userRef = User::findOne(['id' => $userRef]);
        if ($userRef && !empty($prefix)) {
            $userVoucherFreeRef = UserVoucherFreeRef::findOne(['user_id' => $userRef->getId(), 'prefix' => $prefix]);
            if (!$userVoucherFreeRef) {
                $userVoucherFreeRef = new UserVoucherFreeRef();
                $userVoucherFreeRef->setUserId($userRef->getId());
//                var_dump($userVoucherFreeRef);die;
                $userVoucherFreeRef->save(false);
            }
            return $userVoucherFreeRef;
        }
    }


    public static function isUseFirst($userRef)
    {
        $userVoucherFreeRef = self::getUserVoucherFreeRef($userRef);
        $totalReceived = $userVoucherFreeRef->getTotalReceived();
        $totalConsume = $userVoucherFreeRef->getTotalConsume();

        if ($totalReceived == 1 && $totalConsume == 0) {
            return true;
        }
        return false;
    }

    /**
     * @param $userRef
     */
    public function apply($userRef)
    {
        $this->applyRef($userRef);
        $this->applyMy();
    }


    public function applyRef($userRefId)
    {
        if ($userRefId) {
            $userRefObject = User::findOne(['id' => $userRefId]);
            $userVoucherFreeRef = self::getUserVoucherFreeRef($userRefId);
            if ($userRefObject && $userVoucherFreeRef) {
                $totalIntroduce = $userVoucherFreeRef->getTotalIntroduce();
                if (($userRefId != \Yii::$app->user->getId()) && !$this->checkUserRefInList($userRefId)) {
                    $userVoucherFreeRef->setTotalIntroduce($totalIntroduce + 1);
                }
                $userVoucherFreeRef->setTotalReceived(floor($userVoucherFreeRef->getTotalIntroduce() / 3) + 1);
                $userVoucherFreeRef->save(false);
            }
        }
    }

    public function applyMy()
    {
        $userVoucherFreeRef = self::getUserVoucherFreeRef($this->user_id);
        if ($userVoucherFreeRef) {
            $totalConsume = $userVoucherFreeRef->getTotalConsume();
            $userVoucherFreeRef->setTotalConsume($totalConsume + 1);
            $userVoucherFreeRef->save(false);
        }
    }

    public function checkUserRefInList($userRef)
    {
        $listUserRelationRef = $this->getUserRelationRef();
        if (in_array($userRef, $listUserRelationRef))
            return true;

        return false;
    }

    public static function canApply($userVoucherFreeRef)
    {
        /* @var $userVoucherFreeRef UserVoucherFreeRef */
        $totalReceived = $userVoucherFreeRef->getTotalReceived();
        $totalConsume = $userVoucherFreeRef->getTotalConsume();

        if ($totalReceived > $totalConsume) {
            return true;
        }
        return false;
    }

    /**
     * @return bool
     */
    public static function isDateRunCampaignVoucherFree()
    {
        if (isset(\Yii::$app->params['intervals_time_voucher_free_ref'])) {
            $date = \Yii::$app->params['intervals_time_voucher_free_ref'];
            $from = $date[0];
            $to = $date[1];
            $start_time = new \DateTime($from);
            $end_time = new \DateTime($to);
            $now = new \DateTime('now');
            if ($start_time < $now && $now < $end_time) {
                return true;
            }
            return false;

        }
    }

    /**
     * @param $course_id
     */
    public function createOrder($course_id)
    {
        $landingPageId = \Yii::$app->params['landing_page_id_voucher_free_ref'];
        $order = new Order();
        $order->setScenario(Order::SCENARIO_DEFAULT);
        $course = Product::findOne(['id' => $course_id]);
        $order->newOrderVoucherFreeRef($this->getUserId(), $course, $landingPageId);
        return $order;
    }

    public static function getCookieRef()
    {
        $cookies = \Yii::$app->request->cookies;
        if ($cookies->has('user_ref') === true) {
            $userRefData = $cookies->getValue('user_ref');
            return $userRefData;
        }
        return null;
    }

    /**
     * void()
     */
    public static function setCookieRef()
    {
        $user_id = \Yii::$app->user->getId();
        $user_ref = \Yii::$app->request->get('ref');
        if ($user_ref) {
            $userRef = User::findOne(['id' => $user_ref]);
            if ($userRef) {
                if (\Yii::$app->getRequest()->getCookies()->has('user_ref') === false) {
                    \Yii::$app->getResponse()->getCookies()->add(new Cookie([
                        'name' => 'user_ref',
                        'value' => $user_ref,
                        'expire' => time() + 86400 * 365
                    ]));
                } else {
                    if ($user_id != $user_ref) {
                        \Yii::$app->getResponse()->getCookies()->add(new Cookie([
                            'name' => 'user_ref',
                            'value' => $user_ref,
                            'expire' => time() + 86400 * 365
                        ]));
                    }
                }
            }
        }

    }

    /**
     * @return null|string
     */
    public static function getLinkShare($userId)
    {
        if (!empty($userId)) {
            $domain = \Yii::$app->params['baseUrl'];
            return $domain . self::LINK_CAMPAIGN . '?ref=' . $userId;
        }
        return null;
    }

    /**
     * @param $tag_id
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function getListCourseByTag($tag_id)
    {
        $courseTagTable = CourseTag::tableName();
        $courseTable = Course::tableName();
        $courses = Course::find()
            ->leftJoin($courseTagTable, "$courseTagTable.course_id = $courseTable.id")
            ->where(["$courseTagTable.tag_id" => $tag_id])
            ->andWhere(["$courseTable.is_deleted" => 0, "$courseTable.status" => Course::STATUS_ACTIVE])
            ->all();

        return $courses;
    }

    public static function checkAlreadyInCourse($pId)
    {
        return UserCourse::find()->where([
            'user_id' => \Yii::$app->user->id,
            'course_id' => $pId
        ])->exists();

    }

    public static function isDateBeforeRunCampaign()
    {
        if (isset(\Yii::$app->params['intervals_time_voucher_free_ref'])) {
            $date = \Yii::$app->params['intervals_time_voucher_free_ref'];
            $from = $date[0];
            $start_time = new \DateTime($from);
            $now = new \DateTime('now');
            if($start_time > $now)
            {
                return true;
            }
        }
        return false;
    }

    public static function isDateAfterRunCampaign()
    {
        if (isset(\Yii::$app->params['intervals_time_voucher_free_ref'])) {
            $date = \Yii::$app->params['intervals_time_voucher_free_ref'];
            $to = $date[1];
            $end_time = new \DateTime($to);
            $now = new \DateTime('now');
            if($end_time < $now){
                return true;
            }
        }
        return false;
    }
}