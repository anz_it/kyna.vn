<?php

namespace kyna\promotion\models;

use common\helpers\DateTimeHelper;
use kyna\course\models\Course;
use kyna\order\models\Order;
use kyna\settings\models\Setting;
use kyna\user\models\User;
use Yii;
use common\helpers\RequestModel;

/**
 * Promo Code Form model for Shopping Cart payment.
 * @property  $_promotion \kyna\promotion\models\Promotion
 */
class PromoCodeForm extends RequestModel
{

    public $code;
    public $_promotion;
    public $order_id;
    public $products;
    public $user_email;

    public function rules()
    {
        return [
            [['code'], 'required'],
            [['code'], 'validateCode'],
            [['order_id'], 'safe']
        ];
    }

    public function attributeLabels()
    {
        return [
            'Code' => 'Mã giảm giá',
        ];
    }

    public function validateCode()
    {
        $this->validateUserVoucherFree($this->code);

        $this->_promotion = Promotion::find()->andWhere([
            'code' => $this->code, 'status'=>Promotion::STATUS_ACTIVE
        ])->one();

        if ($this->_promotion == null) {
            $this->addError('code', 'Mã giảm giá không chính xác.');
            return;
        }

        if(!empty($this->_promotion->start_date) && strtotime("now") < $this->_promotion->start_date){
            $this->addError('code', 'Mã giảm giá chưa tới thời hạn sử dụng.');
            return;
        }
        //Check gift promotion
        if(!empty($this->_promotion->user_id)){
           $user = User::findOne(['email'=> $this->user_email]);
           if(!empty($user)) {
               if ($this->_promotion->user_id != $user->id) {
                   $this->addError('code', 'Mã giảm giá không hợp lệ.');
                   return;
               }
           }
           else{
               $this->addError('code', 'Mã giảm giá không hợp lệ.');
               return;
           }
        }

        if (!$this->_promotion->getCanUseInFrontend()) {
            $this->addError('code', 'Mã giảm giá không hợp lệ.');
            return;
        }


        $user_used_times = Order::find()
            ->where(['user_id' => Yii::$app->user->id, 'promotion_code' => $this->code])
            ->andWhere(['NOT IN', 'status', [
                Order::ORDER_STATUS_CANCELLED
            ]])
            ->count();

        if($user_used_times >= $this->_promotion->user_number_usage)
        {
            $this->addError('code', 'Mã giảm giá đã hết lần sử dụng.');
            return;
        }

       /* if (!empty($user_used_times) && empty(Yii::$app->params['discount_combo'])) {
            $this->addError('code', 'Mã giảm giá đã được sử dụng.');
            return;
        }*/

        if ($this->_promotion->number_usage <= $this->_promotion->current_number_usage) {
            $this->addError('code', 'Mã giảm giá đã hết số lần sử dụng.');
            return;
        }

        if (!((empty($this->_promotion->end_date) || strtotime("now") <= $this->_promotion->end_date) &&
            (empty($this->_promotion->start_date) || strtotime("now") >= $this->_promotion->start_date))) {
            $this->addError('code', 'Mã giảm giá đã hết hạn sử dụng.');
            return;
        }

      /*  $coursesCanUseCode = $this->getCoursesCanUseCode();
        if (empty($coursesCanUseCode)) {
            $this->addError('code', 'Không có khóa học nào trong giỏ hàng khả dụng mã giảm giá này.');
            return;
        }*/

        if(!$this->checkIsApplyOrder($this->products))
        {
            $this->addError('code', 'Khóa học không thỏa điều kiện áp dụng.');
            return;
        }
        else
        {
           // $this->addError('code', 'Ok.');
           // return;
        }

        if ($this->_promotion->value && ($this->_promotion->type != Promotion::KIND_COURSE_APPLY) ) {
            if ($this->checkCondition($this->_promotion, $this->order_id) === false) {
                $this->addError('code', 'Đơn hàng không đủ điều kiện sử dụng mã giảm giá này.');
                return;
            }
        }
    }

    public function validateUserVoucherFree($codeVoucher){
        list($code,$userId) = UserVoucherFree::isUserVoucherFree($codeVoucher);

        if(!empty($code) && !empty($userId)){
            $this->code = $code;
            $user = User::findOne(['id' => $userId]);

            if (!$user) {
                $this->addError('code', 'Mã giảm giá không chính xác.');
                return;
            }
        }else if(isset($code) && !empty($code)){
            if($codeVoucher === UserVoucherFree::PREFIX){
                $this->addError('code', 'Mã giảm giá không chính xác.');
                return;
            }
        }
    }

    public function getDiscountPrice()
    {
        if ($this->_promotion) {
            if ($this->_promotion->value && $this->_promotion->discount_type == Promotion::TYPE_FEE) {
                return $this->_promotion->value;
            }
        }
        return 0;
    }

    public function getPercentagePrice($percentage, $course_price)
    {
        $result = (int)$course_price * (int)$percentage / 100;
        return $result;
    }

    /**
     * Parity Price for Course calculation, apply into discount
     */
    public function getParityPrice($course_price = null)
    {
        $result = 0;
        if ($this->_promotion->value && $this->_promotion->discount_type == Promotion::TYPE_PARITY && $this->_promotion->value < $course_price) {
            $result = $course_price - $this->_promotion->value;
        }
        return $result;
    }

    /**
     * @return bool
     */
    public function getIsCoupon()
    {
        if (!empty($this->_promotion)) {
            if ($this->_promotion->type == Promotion::KIND_COURSE_APPLY) {
                return true;
            }
        }
    }

    /**
     * @return bool
     */
    public function getIsPercentage()
    {
        if (!empty($this->_promotion)) {
            if ($this->_promotion->discount_type == Promotion::TYPE_PERCENTAGE) {
                return true;
            }
        }
    }

    public function getPercentage()
    {
        if (!empty($this->_promotion)) {
            if ($this->_promotion->discount_type == Promotion::TYPE_PERCENTAGE) {
                return $this->_promotion->value;
            }
        }
    }

    public function getIsParity()
    {
        if (!empty($this->_promotion)) {
            if ($this->_promotion->discount_type == Promotion::TYPE_PARITY) {
                return true;
            }
        }
    }
    public function getIsFee()
    {
        if (!empty($this->_promotion)) {
            if ($this->_promotion->discount_type == Promotion::TYPE_FEE) {
                return true;
            }
        }
    }

    /**
     * @return mixed
     */
    protected function getPromotion()
    {
        $this->_promotion = Promotion::find()->andWhere([
            'code' => $this->code,
            'is_used' => Promotion::IS_NOT_USED
        ])->andWhere(
            'promotion.end_date IS NULL OR promotion.end_date = 0 OR promotion.end_date > :current_date', [':current_date' => time()
        ])->one();

        if ($this->_promotion === NULL) {
            $promotion = Promotion::find()->andWhere([
                'code' => $this->code,
                'is_used' => Promotion::IS_USED
            ])->andWhere(
                'promotion.end_date > :current_date', [':current_date' => time()
            ])->one();

            if ($promotion && $promotion->current_number_usage < $promotion->number_usage) {
                $this->_promotion = $promotion;
            }
        }

        return $this->_promotion;
    }

    /**
     * @param $promotion
     * @return bool
     */
    public function checkCondition($promotion, $order_id = null)
    {
        $cartCostWithDiscount = 0;
        if(!empty($this->products)) {

            foreach ($this->products as $course) {
                $cartCostWithDiscount += $course->getPrice(true);

            }
            if (!empty($promotion->min_amount) && $promotion->min_amount > $cartCostWithDiscount) {
                return false;
            }
        }
        return true;
    }

    /**
     * Get all course ids can use coupon code
     * @return array
     */
    public function getCoursesCanUseCode()
    {
        $skipExceptCourses = false;
        $app_prefix = substr($this->code, 0, 4);
        if (strtolower($app_prefix) == "app_" && DateTimeHelper::checkAppPromotionTime()) {
            $skipExceptCourses = true;
        }

        $noApplyIds = [];
        //các khóa học set trong common ngoại trừ
        if(!$skipExceptCourses) {
            $exceptCourses = Setting::find()->andWhere(['key' => 'no_apply_voucher_courses', 'is_deleted' => 0])->one();
            if (!empty($exceptCourses)) {
                $noApplyIds = explode(",", $exceptCourses->value);
            }
        }
        $courseIds = [];
        $result = [];

        $order = $this->order;
        if (empty($order)) {
            $cartItems = $this->products;
            foreach ($cartItems as $item) {

                //Skip free course
               if($item->sellPrice <= 0){
                   continue;
               }
                //Skip courses in setting
               if(!empty($noApplyIds))
               {
                   if(in_array($item->id, $noApplyIds))
                       continue;
               }
                //Loai bo danh sach kho hoc khong ap dung tren mobile
                if(DateTimeHelper::checkAppPromotionTime()) {
                    if (strtolower($app_prefix) == "app_" && in_array($item->id, [1105, 1185, 817])) {
                        continue;
                    }
                }
                $isCombo = $item->type == Course::TYPE_COMBO ? 1 : 0;
                //All course is unselected
                if(empty($this->_promotion->apply_all)) {
                    //Select apply combo
                    if(!empty($isCombo)) {
                        if (!empty($this->_promotion->apply_all_combo)) {
                            if (!empty($isCombo) && $item->sellPrice > 0) {
                                $courseIds[] = $item->id;

                            }
                        }
                    }
                    else {
                        //Single course selected
                        if (!empty($this->_promotion->apply_all_single_course) && empty($this->_promotion->apply_all_single_course_double)) {
                            if (empty($item->discountAmount)) {
                                $courseIds[] = $item->id;
                            }
                        }
                        //Single course selected and check double discount
                        if (!empty($this->_promotion->apply_all_single_course) && !empty($this->_promotion->apply_all_single_course_double)) {
                                $courseIds[] = $item->id;
                        }
                    }

                    // User select courses
                    if(!empty($this->_promotion->courses)) {
                        foreach ($this->_promotion->courses as $course) {
                            if ($course->id == $item->id) {
                                if(!in_array($course->id, $courseIds))
                                  $courseIds[] = $item->id;
                            }
                        }
                    }
                }
                ////All course is selected
                else
                {
                    $courseIds[] = $item->id;
                }
                //$courseIds[] = $item->id;
            }
        } else {
            foreach ($order->details as $detail) {

                if(!empty($noApplyIds))
                {
                    if(in_array($detail->course->id, $noApplyIds))
                        continue;
                }
               // $combo = $detail->getCombo();
                //All course is unselected
                if(empty($this->_promotion->apply_all)) {
                    if(!empty($detail->course_combo_id)) {
                        //Select apply combo
                        if (!empty($this->_promotion->apply_all_combo)) {
                            if (!empty($detail->course_combo_id) && $detail->course->sellPrice > 0) {
                                $courseIds[] = $detail->course->id;
                                //continue;
                            }
                        }
                    }else {
                        //Single course selected
                        if (!empty($this->_promotion->apply_all_single_course)) {
                            if (empty($detail->course->discountAmount)) {
                                $courseIds[] = $detail->course->id;
                            }
                        }
                        //Single course selected and check double discount
                        if (!empty($this->_promotion->apply_all_single_course) && !empty($this->_promotion->apply_all_single_course_double)) {
                            if (!empty($detail->course->discountAmount)) {
                                $courseIds[] = $detail->course->id;
                            }
                        }

                        // User select courses
                        if (!empty($this->_promotion->courses)) {
                            foreach ($this->_promotion->courses as $course) {
                                if ($course->id == $detail->course->id) {
                                    if (!in_array($course->id, $courseIds))
                                        $courseIds[] = $detail->course->id;
                                }
                            }
                        }
                    }
                }
                ////All course is selected
                else
                {
                    $courseIds[] = $detail->course->id;
                }
            }
        }

        return $courseIds;
    }

    public function checkIsApplyCourse($course)
    {
        //Free course
        if($course->getPrice(true) <= 0)
            return false;

        if($this->_promotion->type == Promotion::KIND_COURSE_APPLY || $this->_promotion->type == Promotion::KIND_ORDER_APPLY){
            $noApplyIds = [];
            $exceptCourses = Setting::find()->andWhere(['key'=>'no_apply_voucher_courses', 'is_deleted'=>0])->one();
            if(!empty($exceptCourses))
            {
                $noApplyIds = explode(",", $exceptCourses->value);
            }
            if(in_array($course->id, $noApplyIds))
                return false;

            if(!empty($this->_promotion->apply_all)) {
                return true;
            }
            else
            {
                $isCombo = $course->type == Course::TYPE_COMBO ? 1 : 0;
                if(!empty($isCombo)) {
                    //Select apply combo
                    if (!empty($this->_promotion->apply_all_combo)) {
                        if ($course->sellPrice > 0) {
                            return true;

                        }
                    }
                }else {
                    //Single course selected
                    if (!empty($this->_promotion->apply_all_single_course)) {
                        if (empty($course->discountAmount)) {
                           return true;
                        }
                    }
                    //Single course selected and check double discount
                    if (!empty($this->_promotion->apply_all_single_course) && !empty($this->_promotion->apply_all_single_course_double)) {
                        if (!empty($course->discountAmount)) {
                            return true;
                        }
                    }

                    // User select courses
                    if (!empty($this->_promotion->courses)) {
                        foreach ($this->_promotion->courses as $c) {
                            if ($c->id == $course->id) {
                                return true;
                            }
                        }
                    }
                }
            }
        }
        return false;
    }


    public function checkIsApplyOrder($cartItems)
    {
        //$cartItems = Yii::$app->cart->getPositions();

        $courseIds = $this->getCoursesCanUseCode();
        if(!empty($courseIds))
        {
            if($this->_promotion->type == Promotion::KIND_ORDER_APPLY)
            {
                if($this->_promotion->apply_condition === Promotion::ONE_COURSE_CONDITION)
                {
                    if(count($courseIds) <= count($cartItems))
                        return true;
                    return false;
                }
                if($this->_promotion->apply_condition === Promotion::ALL_COURSE_CONDITION)
                {
                    if(count($courseIds) == count($cartItems))
                        return true;
                    return false;
                }
            }
            return true;

        }
        return false;

    }
    public function getDiscountType()
    {
       return $this->_promotion->discount_type;
    }

    public function getOrder()
    {
        return Order::findOne($this->order_id);
    }
}
