<?php

namespace kyna\gamification\models;

use Yii;

/**
 * This is the model class for table "{{%mission_conditions}}".
 *
 * @property integer $id
 * @property integer $mission_id
 * @property string $type
 * @property string $title
 * @property integer $min_value
 * @property integer $max_value
 * @property boolean $is_deleted
 * @property integer $created_time
 * @property integer $created_user_id
 * @property integer $updated_time
 * @property integer $updated_user_id
 */
class MissionCondition extends \kyna\base\ActiveRecord
{

    const TYPE_COURSE_COMPLETE = 'course-complete';
    const TYPE_QUIZ_DOING = 'quiz-doing';
    const TYPE_QUIZ_DOING_LAST = 'quiz-doing-last';
    const TYPE_COURSE_PROCESS = 'course-process';
    const TYPE_QNA_APPROVED = 'qna-approved';
    const TYPE_QNA_GET_LIKE = 'qna-get-like';
    const TYPE_DISCUSS_GET_LIKE = 'discuss-get-like';

    const TYPE_LESSON_QUIZ_PASS_MULTI_CHOICE = 'pass-multi-choice-quiz';
    const TYPE_LESSON_QUIZ_PASS_MULTI_CHOICE_LAST = 'pass-last-multi-choice-quiz-of-course';
    const TYPE_LESSON_QUIZ_PASS_OTHER = 'pass-other-type-of-quiz';
    const TYPE_LESSON_QUIZ_PASS_LAST_OTHER = 'pass-last-other-quiz-type-of-course';
    const TYPE_LESSON_VIDEO_PROCESS = 'lesson-video-process';
    const TYPE_LESSON_CONTENT_COMPLETE = 'complete-lesson-content';
    const TYPE_ORDER_COMPLETE = 'complete-order';
    const TYPE_COURSE_REVIEW = 'review-course';
    const TYPE_COURSE_GRADUATED = 'graduate-course';
    const TYPE_COURSE_GRADUATED_EXCELLENT = 'graduate-course-excellent';

    public static function softDelete()
    {
        return true;
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mission_conditions';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type'], 'required'],
            ['min_value', 'required', 'message' => 'Giá trị này không được để trống'],
            ['max_value', 'required', 'when' => function ($model) {
                return $this->type == self::TYPE_ORDER_COMPLETE;
            }, 'whenClient' => "function(attribute, value) {
                    var selectedType = $('#missioncondition-type').val();
                    return selectedType == '" . self::TYPE_ORDER_COMPLETE . "';
                }"
                , 'message' => 'Giá trị này không được để trống'],
            [['mission_id', 'min_value', 'max_value', 'created_time', 'created_user_id', 'updated_time', 'updated_user_id'], 'integer'],
            [['is_deleted'], 'boolean'],
            [['type'], 'string', 'max' => 64],
            [['title'], 'string', 'max' => 32],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'mission_id' => 'Mission ID',
            'type' => 'Điều kiện',
            'title' => 'Title',
            'min_value' => $this->type == self::TYPE_ORDER_COMPLETE ? 'Giá trị đơn hàng tối thiểu' : ($this->type == self::TYPE_LESSON_VIDEO_PROCESS ? 'Phần trăm video' : 'Số lượng tối thiểu cần được'),
            'max_value' => $this->type == self::TYPE_ORDER_COMPLETE ? 'Giá trị đơn hàng tối đa' : ($this->type == self::TYPE_LESSON_VIDEO_PROCESS ? 'Phần trăm video tối đa' : 'Số lượng tối đa cần được'),
            'is_deleted' => 'Is Deleted',
            'created_time' => 'Created Time',
            'created_user_id' => 'Created User ID',
            'updated_time' => 'Updated Time',
            'updated_user_id' => 'Updated User ID',
        ];
    }

    public static function getTypeOptions()
    {
        return [
            self::TYPE_COURSE_GRADUATED => 'Đạt chứng chỉ',
            self::TYPE_COURSE_GRADUATED_EXCELLENT => 'Đạt chứng chỉ loại xuất sắc',
            self::TYPE_COURSE_REVIEW => 'Tham gia đánh giá khóa học',
            self::TYPE_LESSON_CONTENT_COMPLETE => 'Xem bài đọc(không phải video)',
            self::TYPE_LESSON_QUIZ_PASS_LAST_OTHER => 'Vượt qua bài quiz tự luận và hỗn hợp cuối khóa',
            self::TYPE_LESSON_QUIZ_PASS_OTHER => 'Vượt qua bài quiz tự luận và hỗn hợp',
            self::TYPE_LESSON_QUIZ_PASS_MULTI_CHOICE => 'Vượt qua bài quiz trắc nghiệm',
            self::TYPE_LESSON_QUIZ_PASS_MULTI_CHOICE_LAST => 'Vượt qua bài quiz trắc nghiệm cuối khóa',
            self::TYPE_ORDER_COMPLETE => 'Mua khóa học',
            self::TYPE_LESSON_VIDEO_PROCESS => 'Xem thời lượng video',
        ];
    }
}
