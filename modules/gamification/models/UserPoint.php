<?php

namespace kyna\gamification\models;

use kyna\user\models\Profile;
use kyna\user\models\User;
use Yii;

/**
 * This is the model class for table "{{%user_points}}".
 *
 * @property integer $user_id
 * @property integer $k_point
 * @property integer $k_point_usable
 */
class UserPoint extends \kyna\base\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user_points}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['k_point', 'k_point_usable'], 'integer'],
            [['user_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'user_id' => 'Học viên',
            'k_point' => 'Số điểm',
            'k_point_usable' => 'Số điểm khả dụng',
        ];
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function getProfile()
    {
        return $this->hasOne(Profile::className(), ['user_id' => 'user_id']);
    }
}
