<?php

namespace kyna\gamification\models;

use kyna\course\models\Category;
use Yii;

/**
 * This is the model class for table "{{%mission_categories}}".
 *
 * @property integer $id
 * @property integer $mission_id
 * @property integer $category_id
 * @property boolean $is_deleted
 * @property integer $created_time
 * @property integer $created_user_id
 * @property integer $updated_time
 * @property integer $updated_user_id
 */
class MissionCategory extends \kyna\base\ActiveRecord
{

    public static function softDelete()
    {
        return true;
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mission_categories';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['mission_id', 'category_id'], 'required'],
            [['mission_id', 'category_id', 'created_time', 'created_user_id', 'updated_time', 'updated_user_id'], 'integer'],
            [['is_deleted'], 'boolean'],
            [['mission_id', 'category_id'], 'unique', 'targetAttribute' => ['mission_id', 'category_id'], 'message' => 'The combination of Mission ID and Category ID has already been taken.'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'mission_id' => 'Mission ID',
            'category_id' => 'Category ID',
            'is_deleted' => 'Is Deleted',
            'created_time' => 'Created Time',
            'created_user_id' => 'Created User ID',
            'updated_time' => 'Updated Time',
            'updated_user_id' => 'Updated User ID',
        ];
    }

    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'category_id']);
    }
}
