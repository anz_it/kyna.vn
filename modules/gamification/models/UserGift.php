<?php

namespace kyna\gamification\models;

use kyna\promotion\models\Promotion;
use kyna\user\models\User;
use Yii;

/**
 * This is the model class for table "{{%user_gifts}}".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $gift_id
 * @property string $code
 * @property integer $used_date
 * @property integer $created_time
 */
class UserGift extends \kyna\base\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user_gifts}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'gift_id'], 'required'],
            [['user_id', 'gift_id', 'used_date', 'created_time'], 'integer'],
            [['code'], 'string', 'max' => 32],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'gift_id' => 'Gift ID',
            'code' => 'Code',
            'used_date' => 'Ngày sử dụng',
            'created_time' => 'Ngày nhận',
        ];
    }

    public function getPromotion()
    {
        return $this->hasOne(Promotion::className(), ['code' => 'code']);
    }

    public function getGift()
    {
        return $this->hasOne(Gift::className(), ['id' => 'gift_id']);
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
