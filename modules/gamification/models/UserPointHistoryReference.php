<?php

namespace kyna\gamification\models;

use Yii;

/**
 * This is the model class for table "user_point_history_references".
 *
 * @property integer $id
 * @property integer $user_point_history_id
 * @property integer $reference_id
 */
class UserPointHistoryReference extends \kyna\base\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_point_history_references';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_point_history_id', 'reference_id'], 'required'],
            [['user_point_history_id', 'reference_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_point_history_id' => 'User Point History ID',
            'reference_id' => 'Reference ID',
        ];
    }
}
