<?php

namespace kyna\gamification\models;

use Yii;

/**
 * This is the model class for table "user_point_history_videos".
 *
 * @property integer $id
 * @property integer $user_point_history_id
 * @property integer $video_id
 */
class UserPointHistoryVideo extends \kyna\base\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_point_history_videos';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_point_history_id', 'video_id'], 'required'],
            [['user_point_history_id', 'video_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_point_history_id' => 'User Point History ID',
            'video_id' => 'Video ID',
        ];
    }
}
