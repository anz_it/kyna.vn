<?php

namespace kyna\gamification\models;

use kyna\user\models\User;
use Yii;
use common\helpers\CDNHelper;

/**
 * This is the model class for table "{{%gifts}}".
 *
 * @property integer $id
 * @property string $title
 * @property string $image_url
 * @property integer $k_point
 * @property integer $expiration_date
 * @property boolean $is_deleted
 * @property integer $status
 * @property integer $created_time
 * @property integer $created_user_id
 * @property integer $updated_time
 * @property integer $updated_user_id
 */
class Gift extends \kyna\base\ActiveRecord
{

    public $imageSize = [
        'cover' => [
            CDNHelper::IMG_SIZE_ORIGINAL
        ],
        'contain' => [],
        'crop' => []
    ];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%gifts}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['image_url'], 'string'],
            [['title', 'k_point'], 'required'],
            [['status', 'created_time', 'created_user_id', 'updated_time', 'updated_user_id'], 'integer'],
            [['k_point'], 'integer', 'min' => 1],
            [['is_deleted'], 'boolean'],
            ['expiration_date', 'date'],
            [['title'], 'string', 'max' => 64],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Tên',
            'image_url' => 'Hình',
            'k_point' => 'Số điểm quy đổi',
            'expiration_date' => 'Ngày hết hạn',
            'is_deleted' => 'Is Deleted',
            'status' => 'Trạng thái',
            'created_time' => 'Created Time',
            'created_user_id' => 'Created User ID',
            'updated_time' => 'Updated Time',
            'updated_user_id' => 'Updated User ID',
        ];
    }

    public function getGiftContent()
    {
        return $this->hasOne(GiftContent::className(), ['gift_id' => 'id']);
    }

    public function beforeSave($insert)
    {
        $ret = parent::beforeSave($insert);

        $exprirationDate = date_create_from_format('d/m/Y', trim($this->expiration_date));
        if ($exprirationDate !== false) {
            $this->expiration_date = $exprirationDate->getTimestamp();
        }

        return $ret;
    }

    public function getCreatedUser()
    {
        return $this->hasOne(User::className(), ['id' => 'created_user_id']);
    }

    public function getUpdatedUser()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_user_id']);
    }
}
