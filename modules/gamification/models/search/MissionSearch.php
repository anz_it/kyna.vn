<?php

namespace kyna\gamification\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use kyna\gamification\models\Mission;

/**
 * MissionSearch represents the model behind the search form about `kyna\gamification\models\Mission`.
 */
class MissionSearch extends Mission
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'k_point', 'k_point_for_free_course', 'expiration_time', 'status', 'created_time', 'created_user_id', 'updated_time', 'updated_user_id'], 'integer'],
            [['name', 'condition_operator', 'apply_course_type'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Mission::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC
                ]
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'k_point' => $this->k_point,
            'k_point_for_free_course' => $this->k_point_for_free_course,
            'expiration_time' => $this->expiration_time,
            'status' => $this->status,
            'created_time' => $this->created_time,
            'created_user_id' => $this->created_user_id,
            'updated_time' => $this->updated_time,
            'updated_user_id' => $this->updated_user_id,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'condition_operator', $this->condition_operator])
            ->andFilterWhere(['like', 'apply_course_type', $this->apply_course_type]);

        return $dataProvider;
    }
}
