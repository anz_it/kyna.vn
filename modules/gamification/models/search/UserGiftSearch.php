<?php

namespace kyna\gamification\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use kyna\gamification\models\UserGift;

/**
 * UserGiftSearch represents the model behind the search form about `kyna\gamification\models\UserGift`.
 */
class UserGiftSearch extends UserGift
{

    public $userInfo;
    public $expirationDate;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'gift_id', 'created_time'], 'integer'],
            ['userInfo', 'string'],
            [['used_date', 'expirationDate'], 'date'],
            [['code'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = self::find()->with('promotion');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'created_time' => SORT_DESC
                ]
            ],
            'pagination' => [
                'pageSize' => 12
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'gift_id' => $this->gift_id,
        ]);

        if (!empty($this->used_date)) {
            $date = \DateTime::createFromFormat('d/m/Y', $this->used_date);
            $query->andWhere([
                'date(from_unixtime(user_gifts.used_date))' => $date->format('Y-m-d')
            ]);
        }

        if (!empty($this->expirationDate)) {
           $query->joinWith('promotion');

            $date = \DateTime::createFromFormat('d/m/Y', $this->expirationDate);
            $query->andWhere([
                'date(from_unixtime(promotion.end_date))' => $date->format('Y-m-d')
            ]);
        }

        if (!empty($this->userInfo)) {
            $query->joinWith('user');
            $query->joinWith('user.profile p');
            $query->andWhere([
                'OR',
                ['LIKE', 'user.email', $this->userInfo],
                ['LIKE', 'p.name', $this->userInfo],
                ['LIKE', 'p.phone_number', $this->userInfo],
            ]);
        }
        if (!empty($this->code)) {
            $query->joinWith('promotion');
            $query->andFilterWhere(['like', 'promotion.code', $this->code]);
        }

        return $dataProvider;
    }
}
