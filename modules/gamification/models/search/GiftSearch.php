<?php

namespace kyna\gamification\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use kyna\gamification\models\Gift;

/**
 * GiftSearch represents the model behind the search form about `kyna\gamification\models\Gift`.
 */
class GiftSearch extends Gift
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'k_point', 'expiration_date', 'status', 'created_time', 'created_user_id', 'updated_time', 'updated_user_id'], 'integer'],
            [['title', 'image_url'], 'safe'],
            [['is_deleted'], 'boolean'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Gift::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC
                ]
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'k_point' => $this->k_point,
            'expiration_date' => $this->expiration_date,
            'is_deleted' => $this->is_deleted,
            'status' => $this->status,
            'created_time' => $this->created_time,
            'created_user_id' => $this->created_user_id,
            'updated_time' => $this->updated_time,
            'updated_user_id' => $this->updated_user_id,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'image_url', $this->image_url]);

        return $dataProvider;
    }
}
