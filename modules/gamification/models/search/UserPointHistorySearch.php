<?php

namespace kyna\gamification\models\search;

use kyna\gamification\models\UserPoint;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use kyna\gamification\models\UserPointHistory;

/**
 * UserPointHistorySearch represents the model behind the search form about `kyna\gamification\models\UserPointHistory`.
 */
class UserPointHistorySearch extends UserPointHistory
{

    public $date_ranger;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'k_point', 'type', 'mission_id', 'created_time'], 'integer'],
            [['description', 'date_ranger'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = UserPointHistory::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'created_time' => SORT_DESC
                ],
                'attributes' => ['created_time']
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            $query->where('0=1');
            return $dataProvider;
        }

        if (!empty($this->date_ranger)) {
            $dateArr = explode(' - ', $this->date_ranger);
            if (count($dateArr) == 2) {
                $startTime = date_create_from_format('d/m/Y H:i', trim($dateArr[0]). ' 00:00')->getTimestamp();
                $endTime = date_create_from_format('d/m/Y H:i', trim($dateArr[1]). '23:59')->getTimestamp();

                $query->andFilterWhere(['>=', 'created_time', $startTime]);
                $query->andFilterWhere(['<=', 'created_time', $endTime]);
            }
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'k_point' => $this->k_point,
            'type' => $this->type,
            'mission_id' => $this->mission_id,
        ]);

        $query->andFilterWhere(['like', 'description', $this->description]);

        return $dataProvider;
    }

    public function getUserPoint()
    {
        return $this->hasOne(UserPoint::className(), ['user_id' => 'user_id']);
    }
}
