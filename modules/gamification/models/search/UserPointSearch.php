<?php

namespace kyna\gamification\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use kyna\gamification\models\UserPoint;

/**
 * UserPointSearch represents the model behind the search form about `kyna\gamification\models\UserPoint`.
 */
class UserPointSearch extends UserPoint
{

    public $userInfo;
    public $userEmail;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'k_point'], 'integer'],
            ['userInfo', 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = UserPoint::find()->alias('up');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'k_point' => SORT_DESC
                ]
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            $query->where('0=1');
            return $dataProvider;
        }

        $query->joinWith(['user', 'profile p']);
        $query->andFilterWhere([
            'OR',
            [
                'LIKE',
                'user.email',
                $this->userInfo,
            ],
            [
                'LIKE',
                'p.phone_number',
                $this->userInfo,
            ],
            [
                'LIKE',
                'p.name',
                $this->userInfo,
            ]
        ]);

        $query->select(['up.user_id', 'up.k_point', 'user.email as userEmail']);
        $query->groupBy(['up.user_id', 'up.k_point', 'user.email']);

        // grid filtering conditions
        $query->andFilterWhere([
            'user_id' => $this->user_id,
            'k_point' => $this->k_point,
        ]);

        return $dataProvider;
    }
}
