<?php

namespace kyna\gamification\models;

use kyna\course\models\Category;
use kyna\course\models\Course;
use kyna\course\models\CourseLesson;
use kyna\course\models\CourseRating;
use kyna\course\models\CourseSection;
use kyna\course\models\Quiz;
use kyna\learning\models\UserCourseLesson;
use kyna\user\models\UserCourse;
use kyna\user\models\User;
use Yii;

/**
 * This is the model class for table "{{%missions}}".
 *
 * @property integer $id
 * @property string $name
 * @property integer $k_point
 * @property integer $k_point_for_free_course
 * @property integer $expiration_time
 * @property string $condition_operator
 * @property string $apply_course_type
 * @property integer $status
 * @property integer $created_time
 * @property integer $created_user_id
 * @property integer $updated_time
 * @property integer $updated_user_id
 */
class Mission extends \kyna\base\ActiveRecord
{

    const APPLY_COURSE_TYPE_ALL = 'ALL';
    const APPLY_COURSE_TYPE_SOME = 'SOME';
    const APPLY_COURSE_TYPE_CATEGORY = 'CATEGORY';

    public $listCourseIds;
    public $listCategoryIds;
    public $summaryText;

    public static function softDelete()
    {
        return true;
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%missions}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'k_point'], 'required'],
            [['k_point', 'k_point_for_free_course', 'status', 'created_time', 'created_user_id', 'updated_time', 'updated_user_id'], 'integer'],
            [['condition_operator', 'apply_course_type'], 'string'],
            [['name'], 'string', 'max' => 64],
            [['listCategoryIds', 'listCourseIds', 'expiration_time'], 'safe'],
            [
                'listCategoryIds', 'required',
                'when' => function ($model) {
                    return $model->apply_course_type == self::APPLY_COURSE_TYPE_CATEGORY;
                },
                'whenClient' => "function(attribute, value) {
                    var type = $('input[name=\'Mission[apply_course_type]\']:checked').val();
                    return type == '" . self::APPLY_COURSE_TYPE_CATEGORY . "';
                }",
            ],
            [
                'listCourseIds', 'required',
                'when' => function ($model) {
                    return $model->apply_course_type == self::APPLY_COURSE_TYPE_SOME;
                },
                'whenClient' => "function(attribute, value) {
                    var type = $('input[name=\'Mission[apply_course_type]\']:checked').val();
                    return type == '" . self::APPLY_COURSE_TYPE_SOME . "';
                }",
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Tên nhiệm vụ',
            'k_point' => 'Số điểm',
            'k_point_for_free_course' => 'Số điểm(Khóa miễn phí)',
            'expiration_time' => 'Ngày hết hạn',
            'condition_operator' => 'Condition Operator',
            'apply_course_type' => 'Áp dụng cho',
            'status' => 'Trạng thái',
            'created_time' => 'Ngày tạo',
            'created_user_id' => 'Người tạo',
            'updated_time' => 'Ngày cập nhật',
            'updated_user_id' => 'Người cập nhật cuối',
            'listCategoryIds' => 'Danh mục',
            'listCourseIds' => 'Khóa học',
        ];
    }

    public function beforeSave($insert)
    {
        $ret = parent::beforeSave($insert);

        if (!empty($this->expiration_time)) {
            $expireDate = date_create_from_format('d/m/Y H:i', $this->expiration_time);
            if ($expireDate !== false) {
                $this->expiration_time = $expireDate->getTimestamp();
            }
        }

        return $ret;
    }

    public static function getApplyForCourseOptions()
    {
        return [
            self::APPLY_COURSE_TYPE_ALL => 'Tất cả khóa học',
            self::APPLY_COURSE_TYPE_SOME => 'Một số khóa học',
            self::APPLY_COURSE_TYPE_CATEGORY => 'Một số danh mục'
        ];
    }

    public function getApplyCourseTypeText()
    {
        $options = self::getApplyForCourseOptions();

        return isset($options[$this->apply_course_type]) ? $options[$this->apply_course_type] : null;
    }

    public function getApplyForCourseTypeText()
    {
        $options = self::getApplyForCourseOptions();

        return isset($options[$this->apply_course_type]) ? $options[$this->apply_course_type] : null;
    }

    public function getMissionCategories()
    {
        return $this->hasMany(MissionCategory::className(), ['mission_id' => 'id']);
    }

    public function getMissionCourses()
    {
        return $this->hasMany(MissionCourse::className(), ['mission_id' => 'id']);
    }

    public function getMissionConditions()
    {
        return $this->hasMany(MissionCondition::className(), ['mission_id' => 'id']);
    }

    public function afterDelete()
    {
        $ret = parent::afterDelete();

        MissionCategory::deleteAll(['mission_id' => $this->id]);
        MissionCourse::deleteAll(['mission_id' => $this->id]);
        MissionCondition::deleteAll(['mission_id' => $this->id]);

        return $ret;
    }

    public function getCreatedUser()
    {
        return $this->hasOne(User::className(), ['id' => 'created_user_id']);
    }

    public function getUpdatedUser()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_user_id']);
    }

    public function getUserPointHistories()
    {
        return $this->hasMany(UserPointHistory::className(), ['mission_id' => 'id']);
    }

    public static function findByQuizMultiChoice($is_end_quiz, $course_id, $category_id)
    {
        $missionCourseTblName = MissionCourse::tableName();
        $missionCategoryTblName = MissionCategory::tableName();
        $category = Category::findOne($category_id);

        $types = [
            MissionCondition::TYPE_LESSON_QUIZ_PASS_MULTI_CHOICE,
        ];
        if ($is_end_quiz) {
            $types[] = MissionCondition::TYPE_LESSON_QUIZ_PASS_MULTI_CHOICE_LAST;
        }

        return self::findMissionsByType($types)
            ->andWhere([
                'OR',
                ['t.apply_course_type' => self::APPLY_COURSE_TYPE_ALL],
                [
                    't.apply_course_type' => self::APPLY_COURSE_TYPE_SOME,
                    $missionCourseTblName . '.course_id' => $course_id
                ],
                [
                    't.apply_course_type' => self::APPLY_COURSE_TYPE_CATEGORY,
                    $missionCategoryTblName . '.category_id' => [$category_id, $category->parent_id]
                ],
            ])->all();

    }

    public static function findByQuizOther($is_end_quiz, $course_id, $category_id)
    {
        $category = Category::findOne($category_id);
        $missionCourseTblName = MissionCourse::tableName();
        $missionCategoryTblName = MissionCategory::tableName();

        $types = [
            MissionCondition::TYPE_LESSON_QUIZ_PASS_OTHER,
        ];
        if ($is_end_quiz) {
            $types[] = MissionCondition::TYPE_LESSON_QUIZ_PASS_LAST_OTHER;
        }

        return self::findMissionsByType($types)
            ->andWhere([
                'OR',
                ['t.apply_course_type' => self::APPLY_COURSE_TYPE_ALL],
                [
                    't.apply_course_type' => self::APPLY_COURSE_TYPE_SOME,
                    $missionCourseTblName . '.course_id' => $course_id
                ],
                [
                    't.apply_course_type' => self::APPLY_COURSE_TYPE_CATEGORY,
                    $missionCategoryTblName . '.category_id' => [$category_id, $category->parent_id]
                ],
            ])->all();

    }

    public static function findByLessonContentFinished($course_id, $category_id)
    {
        $category = Category::findOne($category_id);
        $missionCourseTblName = MissionCourse::tableName();
        $missionCategoryTblName = MissionCategory::tableName();

        return self::findMissionsByType(MissionCondition::TYPE_LESSON_CONTENT_COMPLETE)
            ->andWhere([
                'OR',
                ['t.apply_course_type' => self::APPLY_COURSE_TYPE_ALL],
                [
                    't.apply_course_type' => self::APPLY_COURSE_TYPE_SOME,
                    $missionCourseTblName . '.course_id' => $course_id
                ],
                [
                    't.apply_course_type' => self::APPLY_COURSE_TYPE_CATEGORY,
                    $missionCategoryTblName . '.category_id' => [$category_id, $category->parent_id]
                ],
            ])->all();
    }

    public static function findMissionsByCourseGraduated($course_id)
    {
        $course = Course::findOne($course_id);
        $missionCourseTblName = MissionCourse::tableName();
        $missionCategoryTblName = MissionCategory::tableName();

        return self::findMissionsByType(MissionCondition::TYPE_COURSE_GRADUATED)
            ->andWhere([
            'OR',
            ['t.apply_course_type' => self::APPLY_COURSE_TYPE_ALL],
            [
                't.apply_course_type' => self::APPLY_COURSE_TYPE_SOME,
                $missionCourseTblName . '.course_id' => $course_id
            ],
            [
                't.apply_course_type' => self::APPLY_COURSE_TYPE_CATEGORY,
                $missionCategoryTblName. '.category_id' => [$course->category_id, $course->category->parent_id]
            ],
        ])->all();
    }

    public static function findMissionsByCourseGraduatedExcellent($course_id)
    {
        $course = Course::findOne($course_id);
        $missionCourseTblName = MissionCourse::tableName();
        $missionCategoryTblName = MissionCategory::tableName();

        return self::findMissionsByType(MissionCondition::TYPE_COURSE_GRADUATED_EXCELLENT)
            ->andWhere([
                'OR',
                ['t.apply_course_type' => self::APPLY_COURSE_TYPE_ALL],
                [
                    't.apply_course_type' => self::APPLY_COURSE_TYPE_SOME,
                    $missionCourseTblName . '.course_id' => $course_id
                ],
                [
                    't.apply_course_type' => self::APPLY_COURSE_TYPE_CATEGORY,
                    $missionCategoryTblName . '.category_id' => [$course->category_id, $course->category->parent_id]
                ],
            ])->all();
    }

    private static function findMissionsByType($types = [])
    {
        $missionConditionTblName = MissionCondition::tableName();
        $missionCourseTblName = MissionCourse::tableName();
        $missionCategoryTblName = MissionCategory::tableName();

        return self::find()
            ->alias('t')
            ->joinWith([
                'missionConditions ' . $missionConditionTblName,
                'missionCourses ' . $missionCourseTblName,
                'missionCategories ' . $missionCategoryTblName,
            ])
            ->where([
                'AND',
                [
                    $missionConditionTblName . '.type' => $types
                ],
                'expiration_time is null or (expiration_time is not null and expiration_time > UNIX_TIMESTAMP())',
                ['status' => self::STATUS_ACTIVE],
            ])
            ->select(['t.id', 't.k_point', 't.k_point_for_free_course', 't.condition_operator', 't.apply_course_type'])
            ->groupBy(['t.id', 't.k_point', 't.k_point_for_free_course', 't.condition_operator', 't.apply_course_type']);
    }

    public static function findMissionsByCourseUpdatedProcess($course_id, $process)
    {
        $missionCourseTblName = MissionCourse::tableName();
        $missionCategoryTblName = MissionCategory::tableName();
        $missionConditionTblName = MissionCondition::tableName();

        $course = Course::findOne($course_id);

        return self::findMissionsByType(MissionCondition::TYPE_COURSE_PROCESS)
            ->andWhere([
                'OR',
                ['t.apply_course_type' => self::APPLY_COURSE_TYPE_ALL],
                [
                    't.apply_course_type' => self::APPLY_COURSE_TYPE_SOME,
                    $missionCourseTblName . '.course_id' => $course_id
                ],
                [
                    't.apply_course_type' => self::APPLY_COURSE_TYPE_CATEGORY,
                    $missionCategoryTblName . '.category_id' => [$course->category_id, $course->category->parent_id]
                ],
            ])
            ->andWhere(['<=',  $missionConditionTblName . '.min_value', $process])
            ->all();
    }

    public static function addPointByMission($user_id, $point, $mission_id, $reference_ids, $course_id = null)
    {
        if (User::find()->where([
            'id' => $user_id,
            'is_kynabiz' => true
        ])->exists()) {
            // don't added kpoint for Kynabiz Users
            return false;
        }

        if (!empty($reference_ids) && !is_array($reference_ids)) {
            $reference_ids = [$reference_ids];
        }

        $userPoint = UserPoint::findOne($user_id);
        if ($userPoint == null) {
            $userPoint = new UserPoint();
            $userPoint->user_id = $user_id;
        }
        $userPoint->k_point += $point;

        if ($userPoint->save()) {
            $history = new UserPointHistory();
            $history->user_id = $user_id;
            $history->k_point = $point;
            $history->mission_id = $mission_id;
            $history->type = UserPointHistory::TYPE_MISSION;
            $history->description = 'Hoàn thành nhiệm vụ: <b>"' . $history->mission->name . '"</b>';
            $history->is_added = UserPointHistory::BOOL_NO;
            $history->usable_date = time() + Yii::$app->params['kpoint_usable_time']; // 72 hours
            if (!empty($course_id)) {
                $history->course_id = $course_id;
            }
            if (!empty($reference_ids)) {
                $history->list_reference_ids = implode(',', $reference_ids);
            }

            $mCondition = $history->mission->getMissionConditions()->one();
            if ($mCondition != null && in_array($mCondition->type, [
                MissionCondition::TYPE_COURSE_GRADUATED,
                MissionCondition::TYPE_COURSE_GRADUATED_EXCELLENT,
                MissionCondition::TYPE_COURSE_REVIEW,
            ]) && $mCondition->min_value == 1) {
                $course = Course::find()->select('name')->where(['id' => $history->list_reference_ids])->one();
                $history->description .= ' khóa học <b>"' . $course->name . '"</b>';
            }
            $history->save();

            if (isset(Yii::$app->session)) {
                Yii::$app->session->setFlash('mission', [
                    'point' => $point,
                    'missionName' => $history->mission->name
                ]);
            }
        } else {
            return false;
        }

        return true;
    }

    public static function findMissionsByQnaApproved($course_id)
    {
        $missionCourseTblName = MissionCourse::tableName();
        $missionCategoryTblName = MissionCategory::tableName();

        $course = Course::findOne($course_id);

        return self::findMissionsByType(MissionCondition::TYPE_QNA_APPROVED)
            ->andWhere([
                'OR',
                ['t.apply_course_type' => self::APPLY_COURSE_TYPE_ALL],
                [
                    't.apply_course_type' => self::APPLY_COURSE_TYPE_SOME,
                    $missionCourseTblName . '.course_id' => $course_id
                ],
                [
                    't.apply_course_type' => self::APPLY_COURSE_TYPE_CATEGORY,
                    $missionCategoryTblName . '.category_id' => [$course->category_id, $course->category->parent_id]
                ],
            ])
            ->all();
    }

    public static function findByVideoProcess($process, $course_id, $category_id)
    {
        $missionCourseTblName = MissionCourse::tableName();
        $missionCategoryTblName = MissionCategory::tableName();
        $category = Category::findOne($category_id);

        return self::findMissionsByType(MissionCondition::TYPE_LESSON_VIDEO_PROCESS)
            ->andWhere([
                'OR',
                ['t.apply_course_type' => self::APPLY_COURSE_TYPE_ALL],
                [
                    't.apply_course_type' => self::APPLY_COURSE_TYPE_SOME,
                    $missionCourseTblName . '.course_id' => $course_id
                ],
                [
                    't.apply_course_type' => self::APPLY_COURSE_TYPE_CATEGORY,
                    $missionCategoryTblName . '.category_id' => [$category_id, $category->parent_id]
                ],
            ])
            ->andWhere([
                '<=', 'min_value', $process
            ])
            ->all();
    }

    public static function findAllByRatingApproved($course_id, $category_id)
    {
        $missionCourseTblName = MissionCourse::tableName();
        $missionCategoryTblName = MissionCategory::tableName();
        $category = Category::findOne($category_id);

        return self::findMissionsByType(MissionCondition::TYPE_COURSE_REVIEW)
            ->andWhere([
                'OR',
                ['t.apply_course_type' => self::APPLY_COURSE_TYPE_ALL],
                [
                    't.apply_course_type' => self::APPLY_COURSE_TYPE_SOME,
                    $missionCourseTblName . '.course_id' => $course_id
                ],
                [
                    't.apply_course_type' => self::APPLY_COURSE_TYPE_CATEGORY,
                    $missionCategoryTblName . '.category_id' => [$category_id, $category->parent_id]
                ],
            ])
            ->all();
    }

    public static function findByOrderComplete($order_amount)
    {
        return self::findMissionsByType(MissionCondition::TYPE_ORDER_COMPLETE)
            ->andWhere([
                '<=',
                'min_value',
                $order_amount
            ])
            ->andWhere([
                'OR',
                ['max_value' => null],
                [
                    '>=',
                    'max_value',
                    $order_amount
                ]
            ])
            ->all();
    }

    public function getSummaryByCourse($course_id, $user_id)
    {
        /**
         * @var $mCondition MissionCondition
         * @var $course Course
         */
        $mCondition = $this->getMissionConditions()->one();
        $courseTbl = Course::tableName();
        $cSectionTbl = CourseSection::tableName();
        $cLessonTbl = CourseLesson::tableName();
        $ucLessonTbl = UserCourseLesson::tableName();
        $uCourseTbl = UserCourse::tableName();
        $quizTbl = Quiz::tableName();

        $clQuery = CourseLesson::find()
            ->alias($cLessonTbl)
            ->joinWith('section ' . $cSectionTbl)
            ->andWhere([
                "$cSectionTbl.visible" => self::BOOL_YES,
                "$cSectionTbl.active" => self::BOOL_YES,
                "$cSectionTbl.course_id" => $course_id,
                "$cLessonTbl.status" => CourseLesson::BOOL_YES,
            ])
            ->select(["$cLessonTbl.id"])
            ->groupBy(["$cLessonTbl.id"]);

        $ucQuery = UserCourse::find()
            ->alias($uCourseTbl)
            ->select(["$uCourseTbl.course_id"])
            ->groupBy(["$uCourseTbl.course_id"])
            ->andWhere(["$uCourseTbl.user_id" => $user_id]);

        $total = 0;
        $totalFinished = 0;

        switch ($mCondition->type) {
            case MissionCondition::TYPE_LESSON_CONTENT_COMPLETE:
                $clQuery->andWhere([
                    "$cLessonTbl.type" => CourseLesson::TYPE_CONTENT,
                ]);

                $total = $clQuery->count();

                $clQuery->leftJoin("$ucLessonTbl ucl", "ucl.course_lesson_id = $cLessonTbl.id")
                    ->leftJoin("$uCourseTbl uc", "uc.id = ucl.user_course_id")
                    ->andWhere(["IS NOT", 'ucl.id', null])
                    ->andWhere(['uc.user_id' => $user_id])
                    ->andWhere(['ucl.is_passed' => UserCourseLesson::BOOL_YES])
                ;
                $totalFinished = $clQuery->count();

                break;

            case MissionCondition::TYPE_LESSON_VIDEO_PROCESS:
                $clQuery->andWhere([
                    "$cLessonTbl.type" => CourseLesson::TYPE_VIDEO,
                ]);

                $total = $clQuery->count();

                $clQuery->leftJoin("$ucLessonTbl ucl", "ucl.course_lesson_id = $cLessonTbl.id")
                    ->leftJoin("$uCourseTbl uc", "uc.id = ucl.user_course_id")
                    ->andWhere(["IS NOT", 'ucl.id', null])
                    ->andWhere(['uc.user_id' => $user_id])
                    ->andWhere(['>=', 'ucl.process', $mCondition->min_value])
                ;
                $totalFinished = $clQuery->count();
                break;

            case MissionCondition::TYPE_LESSON_QUIZ_PASS_OTHER:
                $clQuery->andWhere([
                    "$cLessonTbl.type" => CourseLesson::TYPE_QUIZ,
                ]);
                $clQuery->joinWith("quiz $quizTbl")
                    ->andWhere(["!=", "$quizTbl.type", Quiz::TYPE_ONE_CHOICE]);

                $total = $clQuery->count();

                $clQuery->leftJoin("$ucLessonTbl ucl", "ucl.course_lesson_id = $cLessonTbl.id")
                    ->leftJoin("$uCourseTbl uc", "uc.id = ucl.user_course_id")
                    ->andWhere(["IS NOT", 'ucl.id', null])
                    ->andWhere(['uc.user_id' => $user_id])
                    ->andWhere(['ucl.is_passed' => UserCourseLesson::BOOL_YES])
                ;
                $totalFinished = $clQuery->count();
                break;

            case MissionCondition::TYPE_LESSON_QUIZ_PASS_LAST_OTHER:
                $clQuery->andWhere(["$cLessonTbl.type" => CourseLesson::TYPE_QUIZ])
                    ->leftJoin('quiz_meta qm', "qm.quiz_id = $quizTbl.id")
                    ->andWhere([
                        'qm.key' => 'is_end_quiz',
                        'qm.value' => 1
                    ])
                    ->joinWith("quiz $quizTbl")
                    ->andWhere(["!=", "$quizTbl.type", Quiz::TYPE_ONE_CHOICE]);

                $total = $clQuery->count();

                $clQuery->leftJoin("$ucLessonTbl ucl", "ucl.course_lesson_id = $cLessonTbl.id")
                    ->leftJoin("$uCourseTbl uc", "uc.id = ucl.user_course_id")
                    ->andWhere(["IS NOT", 'ucl.id', null])
                    ->andWhere(['uc.user_id' => $user_id])
                    ->andWhere(['ucl.is_passed' => UserCourseLesson::BOOL_YES])
                ;
                $totalFinished = $clQuery->count();

                break;

            case MissionCondition::TYPE_LESSON_QUIZ_PASS_MULTI_CHOICE:
                $clQuery->andWhere([
                    "$cLessonTbl.type" => CourseLesson::TYPE_QUIZ,
                ]);
                $clQuery->joinWith("quiz $quizTbl")
                    ->andWhere(["$quizTbl.type" => Quiz::TYPE_ONE_CHOICE]);

                $total = $clQuery->count();

                $clQuery->leftJoin("$ucLessonTbl ucl", "ucl.course_lesson_id = $cLessonTbl.id")
                    ->leftJoin("$uCourseTbl uc", "uc.id = ucl.user_course_id")
                    ->andWhere(["IS NOT", 'ucl.id', null])
                    ->andWhere(['uc.user_id' => $user_id])
                    ->andWhere(['ucl.is_passed' => UserCourseLesson::BOOL_YES])
                ;
                $totalFinished = $clQuery->count();
                break;

            case MissionCondition::TYPE_LESSON_QUIZ_PASS_MULTI_CHOICE_LAST:
                $clQuery->andWhere(["$cLessonTbl.type" => CourseLesson::TYPE_QUIZ,])
                    ->joinWith("quiz $quizTbl")
                    ->andWhere(["$quizTbl.type" => Quiz::TYPE_ONE_CHOICE])
                    ->leftJoin('quiz_meta qm', "qm.quiz_id = $quizTbl.id")
                    ->andWhere([
                        'qm.key' => 'is_end_quiz',
                        'qm.value' => 1
                    ]);

                $total = $clQuery->count();

                $clQuery->leftJoin("$ucLessonTbl ucl", "ucl.course_lesson_id = $cLessonTbl.id")
                    ->leftJoin("$uCourseTbl uc", "uc.id = ucl.user_course_id")
                    ->andWhere(["IS NOT", 'ucl.id', null])
                    ->andWhere(['uc.user_id' => $user_id])
                    ->andWhere(['ucl.is_passed' => UserCourseLesson::BOOL_YES])
                ;
                $totalFinished = $clQuery->count();
                break;

            case MissionCondition::TYPE_COURSE_GRADUATED:
                $total = $ucQuery->count();

                $ucQuery->andWhere(["$uCourseTbl.is_graduated" => UserCourse::BOOL_YES]);
                $totalFinished = $ucQuery->count();
                break;

            case MissionCondition::TYPE_COURSE_GRADUATED_EXCELLENT:
                $ucQuery->joinWith("course $courseTbl");
                $ucQuery->andWhere("$courseTbl.percent_can_be_excellent is not null");

                $total = $ucQuery->count();

                $ucQuery->andWhere("$uCourseTbl.process >= $courseTbl.percent_can_be_excellent");
                $totalFinished = $ucQuery->count();

                break;

            case MissionCondition::TYPE_COURSE_REVIEW:
                $total = $ucQuery->count();
                $courseRatingTbl = CourseRating::tableName();

                $ucQuery->leftJoin("course_ratings $courseRatingTbl", "$courseRatingTbl.course_id = $uCourseTbl.course_id AND $courseRatingTbl.user_id = $uCourseTbl.user_id");
                $ucQuery->andWhere(["$courseRatingTbl.status" => CourseRating::BOOL_YES]);

                $totalFinished = $ucQuery->count();
                break;
        }

        return [
            $totalFinished,
            $total
        ];
    }
}
