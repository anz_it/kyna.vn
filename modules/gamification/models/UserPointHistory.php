<?php

namespace kyna\gamification\models;

use kyna\course\models\Course;
use kyna\course\models\CourseLesson;
use kyna\course\models\Quiz;
use kyna\user\models\User;
use Yii;

/**
 * This is the model class for table "user_point_histories".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $k_point
 * @property integer $type
 * @property integer $mission_id
 * @property string $list_reference_ids
 * @property string $description
 * @property integer $created_time
 * @property integer $course_id
 * @property integer $is_added
 * @property integer $usable_date
 */
class UserPointHistory extends \kyna\base\ActiveRecord
{

    const TYPE_MISSION = 1;
    const TYPE_MANUAL = 2;
    const TYPE_EXCHANGE_GIFT = 3;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_point_histories';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'description'], 'required'],
            [['user_id', 'k_point', 'type', 'mission_id', 'created_time', 'usable_date', 'is_added', 'course_id'], 'integer'],
            [['description'], 'string'],
            [['list_reference_ids'], 'string', 'max' => 125],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'k_point' => 'Số điểm thay đổi',
            'type' => 'Nguồn',
            'mission_id' => 'Nhiệm vụ',
            'list_reference_ids' => 'Tham chiếu',
            'description' => 'Chi tiết',
            'created_time' => 'Thời điểm',
            'created_user_id' => 'Người thực hiện',
            'course_id' => 'Khóa học liên quan',
            'usable_date' => 'Ngày được sử dụng',
            'is_added' => 'Đã thêm',
        ];
    }

    public function getMission()
    {
        return $this->hasOne(Mission::className(), ['id' => 'mission_id']);
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function getCreatedUser()
    {
        return $this->hasOne(User::className(), ['id' => 'created_user_id']);
    }

    public static function getTypes()
    {
        return [
            self::TYPE_MANUAL => 'Thay đổi trực tiếp',
            self::TYPE_MISSION => 'Nhiệm vụ',
            self::TYPE_EXCHANGE_GIFT => 'Đổi quà',
        ];
    }

    public function getTypeText()
    {
        $types = self::getTypes();

        return isset($types[$this->type]) ? $types[$this->type] : null;
    }

    public function getHistoryReferences()
    {
        return $this->hasMany(UserPointHistoryReference::className(), ['user_point_history_id' => 'id']);
    }

    public function getHistoryVideos()
    {
        return $this->hasMany(UserPointHistoryVideo::className(), ['user_point_history_id' => 'id']);
    }

    public function getReferenceText()
    {
        if ($this->type != self::TYPE_MISSION) {
            return null;
        }
        $courseName = '';
        $names = [];
        $condition = $this->mission->getMissionConditions()->one();

        switch ($condition->type) {
            case MissionCondition::TYPE_COURSE_GRADUATED_EXCELLENT:
            case MissionCondition::TYPE_COURSE_GRADUATED:
            case MissionCondition::TYPE_COURSE_REVIEW:
                $refIds = $this->getHistoryReferences()->select(['reference_id'])->column();
                $names = Course::find()->where(['id' => $refIds])->select('name')->column();

                break;

            case MissionCondition::TYPE_LESSON_VIDEO_PROCESS:
            case MissionCondition::TYPE_LESSON_CONTENT_COMPLETE;
                $course = Course::find()->where(['id' => $this->course_id])->one();
                if ($course != null) {
                    $courseName = 'Khóa học <b>' . $course->name . '</b> | ';
                }

                $names = CourseLesson::find()
                    ->alias('t')
                    ->joinWith('section cs')
                    ->where(['t.id' => $this->list_reference_ids])
                    ->select(["CONCAT(cs.name, ' >> ', t.name) as lessonName"])->column();

                break;

            case MissionCondition::TYPE_LESSON_QUIZ_PASS_LAST_OTHER:
            case MissionCondition::TYPE_LESSON_QUIZ_PASS_OTHER:
            case MissionCondition::TYPE_LESSON_QUIZ_PASS_MULTI_CHOICE:
            case MissionCondition::TYPE_LESSON_QUIZ_PASS_MULTI_CHOICE_LAST:
                $course = Course::find()->where(['id' => $this->course_id])->one();
                if ($course != null) {
                    $courseName = 'Khóa học <b>' . $course->name . '</b> | ';
                }

                $names = Quiz::find()
                    ->where(['id' => explode(',', $this->list_reference_ids)])->select('name')->column();

                break;

            default:
                break;
        }

        return $courseName . implode(', ', $names);
    }

    public function afterSave($insert, $changedAttributes)
    {
        $ret = parent::afterSave($insert, $changedAttributes);

        $refIds = explode(',', $this->list_reference_ids);

        if(!empty($this->mission)) {
            $condition = $this->mission->getMissionConditions()->one();
            switch ($condition->type) {
                case MissionCondition::TYPE_LESSON_VIDEO_PROCESS:
                    foreach ($refIds as $refId) {
                        $isExist = UserPointHistoryVideo::find()->where(['user_point_history_id' => $this->id, 'video_id' => $refId])->exists();

                        if (!$isExist) {
                            $video = new UserPointHistoryVideo();
                            $video->user_point_history_id = $this->id;
                            $video->video_id = $refId;
                            $video->save();
                        }
                    }
                    break;

                default:
                    foreach ($refIds as $refId) {
                        $isExist = UserPointHistoryReference::find()->where(['user_point_history_id' => $this->id, 'reference_id' => $refId])->exists();

                        if (!$isExist) {
                            $video = new UserPointHistoryReference();
                            $video->user_point_history_id = $this->id;
                            $video->reference_id = $refId;
                            $video->save();
                        }
                    }
                    break;
            }
        }

        return $ret;
    }
}
