<?php

namespace kyna\promo\models;

use Yii;
use common\helpers\StringHelper;
use kyna\promo\models\Promotion;
use kyna\promo\models\CouponInterface;

/**
 * @inheritdoc
 */
class Voucher extends Promotion implements CouponInterface
{
    
    public $auto;
    public $quantity = 1;

    public function init()
    {
        parent::init();
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['value', 'created_user_id'], 'required'],
            [['value', 'min_amount', 'seller_id', 'partner_id', 'user_id', 'order_id', 'used_date', 'created_user_id', 'created_time', 'updated_time', 'type', 'kind'], 'integer'],
            [['quantity', 'value', 'number_usage'], 'integer', 'min' => 1],
            [['is_deleted', 'is_used'], 'boolean'],
            [['code'], 'string', 'max' => 20],
            [['note'], 'string', 'max' => 100],
            [['prefix'], 'string', 'max' => 255],
            ['code', 'required', 'when' => function ($model) { return !$model->auto; }, 'whenClient' => "function (attribute, value) {
                return $('#voucher-auto').prop('checked') == false;
            }"],
            ['prefix', 'required', 'when' => function ($model) { return $model->auto; }, 'whenClient' => "function (attribute, value) {
                return $('#voucher-auto').prop('checked') == true;
            }"],
            [['code'], 'unique'],
            [['expiration_date', 'auto'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'prefix' => 'Tiền tố',
            'code' => 'Code',
            'value' => 'Mệnh giá',
            'min_amount' => 'Giá trị đơn hàng tối thiểu',
            'note' => 'Ghi chú',
            'expiration_date' => 'Ngày hết hạn',
            'seller_id' => 'Người bán',
            'user_id' => 'Người sử dụng',
            'order_id' => 'Order ID',
            'is_used' => 'Đã sử dụng?',
            'used_date' => 'Ngày sử dụng',
            'created_user_id' => 'Người tạo',
            'is_deleted' => 'Is Deleted',
            'created_time' => 'Created Time',
            'updated_time' => 'Updated Time',
            'quantity' => 'Số lượng',
            'auto' => 'Auto generate code',
            'number_usage' => 'Số lần sử dụng tối đa',
            'current_number_usage' => 'Số lần đã sử dụng',
            'status' => 'Trạng thái'
        ];
    }

    public static function find()
    {
        return parent::find()->andWhere(['kind' => self::KIND_VOUCHER]);
    }
    
    public function beforeSave($insert)
    {
        $ret = parent::beforeSave($insert);
        
        if ($insert) {
            if (empty($this->code)) {
                if (!empty($this->prefix)) {
                    $this->code = $this->prefix . StringHelper::random(10);
                } else {
                    $this->code = StringHelper::random(10);
                }
            } else {
                if (!empty($this->prefix)) {
                    $this->code = $this->prefix . $this->code;
                }
            }

            $this->kind = self::KIND_VOUCHER;
            $this->type = self::TYPE_FEE;
        }
        if (!empty($this->expiration_date)) {
            $expireDate = date_create_from_format('d/m/Y H:i', $this->expiration_date);
            if ($expireDate !== false) {
                $this->expiration_date = $expireDate->getTimestamp();
            }
        }

        return $ret;
    }
    
    public static function create($value, $quantity, $minAmount = null, $created_user_id = null)
    {
        $codes = [];
        
        for ($i = 1; $i <= $quantity; $i++) {
            $model = new static;
            
            $model->value = $value;
            $model->kind = static::KIND_VOUCHER;
            $model->type = static::TYPE_FEE;
            $model->auto = self::BOOL_YES;
            if($created_user_id != null){
                $model->created_user_id = (int)$created_user_id;
            } else {
                $model->created_user_id = Yii::$app->user->id;
            }
            if (!empty($minAmount)) {
                $model->min_amount = $minAmount;
            }
            
            if ($model->save(false)) {
                $codes[] = $model->code;
            }
        }
        
        return $codes;
    }
    public static function createBirthdayCode($value, $quantity, $minAmount = null, $created_user_id = null)
    {
        $codes = [];

        for ($i = 1; $i <= $quantity; $i++) {
            $model = new static;

            $model->value = $value;
            $model->kind = static::KIND_VOUCHER;
            $model->type = static::TYPE_FEE;
            $model->auto = self::BOOL_YES;
            if($created_user_id != null){
                $model->created_user_id = (int)$created_user_id;
                $model->user_id = (int)$created_user_id;
            } else {
                $model->created_user_id = Yii::$app->user->id;
                $model->user_id = Yii::$app->user->id;;
            }
            if (!empty($minAmount)) {
                $model->min_amount = $minAmount;
            }
            $model->start_date = strtotime('2018-07-01 00:00');
            $model->expiration_date = strtotime('2018-09-30 23:59');

            if ($model->save(false)) {
                $codes[] = $model->code;
            }
        }

        return $codes;
    }
    
}
