<?php

namespace kyna\promo\models;

use kyna\gamification\models\Gift;
use kyna\gamification\models\UserGift;
use kyna\order\models\Order;
use Yii;

use common\helpers\ArrayHelper;

use kyna\user\models\User;
use kyna\course\models\Course;
use kyna\promo\models\traits\PromoTrait;
use kyna\commission\models\AffiliateCategory;
use kyna\commission\models\AffiliateUser;

/**
 * This is the model class for table "promotions".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $order_id
 * @property integer $seller_id
 * @property integer $partner_id
 * @property integer $status
 * @property integer $number_usage
 * @property integer $current_number_usage
 * @property string $code
 * @property string $note
 * @property integer $type
 * @property integer $kind
 * @property integer $value
 * @property integer $used_date
 * @property integer $start_date
 * @property integer $expiration_date
 * @property boolean $is_used
 * @property boolean $is_deleted
 * @property integer $created_user_id
 * @property integer $created_time
 * @property integer $updated_time
 * @property integer $issued_person
 * @property integer $min_amount
 * @property integer $apply_scope
 *
 * @property CouponCourses[] $couponCourses
 */
class Promotion extends \kyna\base\ActiveRecord implements CouponInterface
{

    /**
     * @inheritdoc
     */
    use PromoTrait;

    public static function tableName()
    {
        return "{{%promotions}}";
    }

    public static function softDelete()
    {
        return true;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [[
            'user_id', 'order_id', 'seller_id', 'partner_id',
            'status', 'number_usage', 'current_number_usage', 'type', 'kind',
            'value', 'used_date',
            'created_user_id', 'created_time', 'updated_time',
            'issued_person', 'apply_scope'
                ], 'integer'],
            [['is_used'], 'boolean'],
            [['created_user_id'], 'required'],
            [['code'], 'string', 'max' => 32],
            [['start_date', 'expiration_date'], 'safe'],
            [['note', 'prefix'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'order_id' => 'Order ID',
            'seller_id' => 'Seller ID',
            'partner_id' => 'Partner ID',
            'status' => 'Trạng thái',
            'number_usage' => 'Số lần sử dụng',
            'code' => 'Code',
            'prefix' => 'Prefix',
            'type' => 'Loại giảm giá',
            'kind' => 'Kind',
            'value' => 'Mức giảm',
            'used_date' => 'Used Date',
            'start_date' => 'Ngày bắt đầu',
            'expiration_date' => 'Ngày kết thúc',
            'is_used' => 'Trạng thái sử dụng',
            'is_deleted' => 'Is Deleted',
            'created_user_id' => 'Người tạo',
            'created_time' => 'Ngày tạo',
            'updated_time' => 'Ngày cập nhật',
            'course_id' => 'Khóa học áp dụng',
            'apply_for_all' => 'Áp dụng cho tất cả khóa học',
            'apply_scope' => 'Áp dụng ở đâu?',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCouponCourses()
    {
        return $this->hasMany(CouponCourse::className(), ['promotion_id' => 'id']);
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }


    public function getCreatedUser()
    {
        return $this->hasOne(User::className(), ['id' => 'created_user_id']);
    }

    public function getCourses()
    {
        return $this->hasMany(Course::className(), ['id' => 'course_id'])->viaTable(CouponCourse::tableName(), ['promotion_id' => 'id']);
    }

    public function getDiscountPrice()
    {
        if ($this->type == self::TYPE_FEE) {
            return $this->value;
        }
        return 0;
    }

    public function getPercentagePrice($percentage, $course_price)
    {
        $result = (int)$course_price * (int)$percentage / 100;
        return $result;
    }

    /**
     * Parity Price for Course calculation, apply into discount
     * @param $coursePrice
     * @return double
     */
    public function getParityPrice($coursePrice = null)
    {
        $result = 0;
        if ($this->value && $this->type == Coupon::TYPE_PARITY && $this->value < $coursePrice) {
            $result = $coursePrice - $this->value;
        }
        return $result;
    }

    public static function changeStatus($obj)
    {
        $promotion = self::find()->andWhere(['code' => $obj->promotion_code])->one();
        $promotion->is_used = self::IS_USED;
        $promotion->used_date = time();
        $promotion->user_id = $obj->user_id;
        $promotion->order_id = $obj->id;
        $promotion->current_number_usage += 1;
        if ($promotion->save()) {
            return true;
        }
        return null;
    }

    public static function removeFromOrder($order)
    {
        $promotion = self::find()->andWhere(['code' => $order->promotion_code])->one();
        if ($promotion && $promotion->current_number_usage > 0) {
            $promotion->current_number_usage -= 1;
            $promotion->save(false);
        }
    }
    
    public function getCanUse()
    {
        return $this->number_usage > $this->current_number_usage &&
        (empty($this->expiration_date) || strtotime("now") <= $this->expiration_date) &&
        (empty($this->start_date) || strtotime("now") >= $this->start_date);

    }
    
    public static function getAvailablePartners()
    {
        $affiliateCategoryTable = AffiliateCategory::tableName();
        
        $affPartners = AffiliateUser::find()->joinWith('affiliateCategory')->andWhere(["$affiliateCategoryTable.affiliate_group_id" => [
            Yii::$app->params['id_aff_group_third_party'],
            Yii::$app->params['id_aff_group_teacher_owner']
        ]])->all();
        

        return ArrayHelper::map($affPartners, 'user_id', 'user.profile.name');
    }
    
    public function getPartner()
    {
        return $this->hasOne(User::className(), ['id' => 'partner_id']);
    }

    public function getAffiliateUser()
    {
        return $this->hasOne(AffiliateUser::className(), ['user_id' => 'partner_id']);
    }

    public function getIsExistOrder($currentOrderID) {
        // hardcode with campaign discount
        if (!empty(Yii::$app->params['discount_combo'])) {
            return false;
        }
        return Order::find()
            ->where(['user_id' => Yii::$app->user->id, 'promotion_code' => $this->code])
            ->andWhere(['NOT IN', 'status', [
                Order::ORDER_STATUS_NEW,
                Order::ORDER_STATUS_CANCELLED
            ]])
            ->andWhere(['NOT IN', 'id', [$currentOrderID]])
            ->exists();
    }

    public function getUserGift()
    {
        return $this->hasOne(UserGift::className(), ['code' => 'code']);
    }

    public function getGift()
    {
        return $this->hasOne(Gift::className(), ['id' => 'gift_id'])->via('userGift');
    }

    public static function listScope()
    {
        $list = [
            self::APPLY_SCOPE_ALL => 'Tất cả',
            self::APPLY_SCOPE_FRONTEND => 'Frontend',
            self::APPLY_SCOPE_BACKEND => 'Backend',
        ];

        return $list;
    }

    public function getScopeText() // TODO: move to trait
    {
        $listScopes = static::listScope();
        if (isset($listScopes[$this->apply_scope])) {
            return $listScopes[$this->apply_scope];
        }
        return null;
    }

    public function getCanUseInFrontend()
    {
        return in_array($this->apply_scope, [self::APPLY_SCOPE_ALL, self::APPLY_SCOPE_FRONTEND]);
    }

    public static function createLingoPromotion($userId)
    {
        $dateTimeFormat = str_replace('php:', '', Yii::$app->formatter->datetimeFormat);
        $code = new Coupon();
        $code->start_date = date($dateTimeFormat);
        $code->apply_for_all = Coupon::BOOL_YES;
        $code->prefix = 'LINGO_';
        $code->value = 500000;
        $code->type = self::TYPE_FEE;
        $code->created_user_id = Yii::$app->user->id;
        $code->min_amount = 0;
        $code->user_id = $userId;
        $code->apply_scope = self::APPLY_SCOPE_BACKEND;
        $code->expiration_date = date($dateTimeFormat, strtotime('+1 years'));
        if ($code->save()) {
            return $code;
        }
        return false;
    }

    public function getIsPercentage()
    {
        return $this->type == Coupon::TYPE_PERCENTAGE;
    }

    public function getPercentage()
    {
        if ($this->type == Coupon::TYPE_PERCENTAGE) {
            return $this->value;
        }
    }

    public function getIsParity()
    {
        return $this->type == Coupon::TYPE_PARITY;
    }
}
