<?php
/**
 * @author: Hong Ta
 * @desc: Coupon interface, define constance: event.
 */
namespace kyna\promo\models;


interface CouponInterface{
    //Scenario Create New
    const SCENARIO_CREATE = 'coupon_create';
    //Scenario Updated
    const SCENARIO_UPDATE = 'coupon_update';
    //Scenario Deleted
    const SCENARIO_DELETE = 'coupon_delete';
    //Event create coupon
    const EVENT_COUPON_CREATED = 'event_coupon_created';
    //Event coupon update
    const EVENT_COUPON_UPDATED = 'event_coupon_updated';
    //Coupon type
    const TYPE_PERCENTAGE = 0;
    const TYPE_FEE = 1;
    const TYPE_PARITY = 2;
    //Kind of Model
    const KIND_COUPON = 1;

    const KIND_VOUCHER = 0;

    //Used status.
    const IS_USED = 1;
    const IS_NOT_USED = 0;

    // Limit Coupon
    const LIMIT_COUPON_QUANTITY = 100;
    const LIMIT_COUPON_PERCENTAGE = 70;

    // Apply scope: all, frontend, backend
    const APPLY_SCOPE_ALL = 0;
    const APPLY_SCOPE_FRONTEND = 1;
    const APPLY_SCOPE_BACKEND = 2;

    const PROMOTION_NOT_FRONTEND_DESC = 'Mã chỉ áp dụng khi mua hàng trực tiếp với team Tư vấn tuyển sinh';
}