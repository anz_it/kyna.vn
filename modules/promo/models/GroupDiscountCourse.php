<?php

namespace kyna\promo\models;

use Yii;

/**
 * This is the model class for table "{{%group_discount}}".
 *
 * @property integer $id
 * @property integer $course_id
 */
class GroupDiscountCourse extends \kyna\base\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%group_discount_courses}}';
    }


}
