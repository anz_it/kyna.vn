<?php

namespace kyna\promo\models;

use Yii;
use common\helpers\ArrayHelper;
use common\helpers\StringHelper;

use kyna\course\models\Category;
use kyna\course\models\Course;
use kyna\promo\models\CouponInterface;
use kyna\base\events\ActionEvent;
use kyna\course\models\CourseComboItem;

/**
 * Class Coupon
 * @package kyna\promo\models
 *
 * @property int $id
 * @property string $coupon_code
 * @property string $start_date
 * @property string $end_date
 * @property int $coupon_type
 * @property string $coupon_price
 * @property string $coupon_quantity
 * @property string $coupon_reg_number
 * @property int $partner_id
 * @property int $status
 * @property int $is_deleted
 * @property string $created_time
 * @property string $updated_time
 * @property int $apply_for_all
 */
class Coupon extends Promotion implements CouponInterface
{

    public $actionParams = false;
    public $quantity;
    public $course_id;
    public $name;

    public function init()
    {
        parent::init(); // TODO: Change the autogenerated stub

        $this->on(self::EVENT_AFTER_INSERT, [$this, 'saveCouponCourse']);

        return;
    }

    public function rules()
    {
        return array_merge(parent::rules(), [
            [['value', 'start_date', 'expiration_date'], 'required'],
            [['code', 'number_usage', 'course_id'], 'required', 'on' => 'backend-teacher'],
            ['value', 'integer', 'min' => 1, 'max' => 100, 'on' => 'backend-teacher'],
            [['course_id', 'session'], 'safe'],
            [['code'], 'unique'],
            [['value', 'number_usage', 'type', 'kind', 'partner_id', 'apply_for_all', 'quantity'], 'integer'],
            ['value', 'limitCouponPercent', 'on' => 'backend-teacher'],
            ['expiration_date', 'validDateRange', 'on' => 'backend-teacher'],
        ]);
    }

    public function attributeLabels()
    {
        return parent::attributeLabels(); // TODO: Change the autogenerated stub
    }

    public function scenarios()
    {
        return parent::scenarios(); // TODO: Change the autogenerated stub
    }

    public function beforeSave($insert)
    {
        if ($this->isNewRecord) {
            if (empty($this->code)) {
                $code = StringHelper::random(10);
                if(!empty($this->prefix)){
                    $this->code = $this->prefix . $code;
                }else{
                    $this->code = $code;
                }
            }else{
                if(!empty($this->prefix)){
                    $this->code = $this->prefix . $this->code;
                }
            }
            $dateTimeFormat = str_replace('php:', '', Yii::$app->formatter->datetimeFormat);

            $startDate = date_create_from_format($dateTimeFormat, trim($this->start_date));
            if ($startDate !== false) {
                $this->start_date = $startDate->getTimestamp();
            }
            
            $exprirationDate = date_create_from_format($dateTimeFormat, trim($this->expiration_date));
            if ($exprirationDate !== false) {
                $this->expiration_date = $exprirationDate->getTimestamp();
            }

            $this->kind = self::KIND_COUPON;
        }
        return parent::beforeSave($insert); // TODO: Change the autogenerated stub
    }

    public function afterSave($insert, $changedAttributes)
    {
        $ret = parent::afterSave($insert, $changedAttributes);

        $event = new ActionEvent();
        if ($this->actionParams) {
            $event->addMeta($this->actionParams);
        }
        $event->addMeta(['coupon' => $this->attributes]);

        if ($insert) {
            $this->trigger(self::EVENT_AFTER_INSERT, $event);
        }
        
        return $ret;
    }

    public function getCourses()
    {
        return $this->hasMany(Course::className(), ['id' => 'course_id'])->viaTable(CouponCourse::tableName(), ['promotion_id' => 'id']);
    }

    public function getCourseChoose()
    {
        return null;
    }

    public static function getCouponType()
    {
        return [
            self::TYPE_FEE => 'Theo giá cố định',
            self::TYPE_PERCENTAGE => 'Theo phần trăm',
            self::TYPE_PARITY   => 'Đồng giá'
        ];
    }

    public function getCouponText()
    {
        $type = self::getCouponType();

        return isset($type[$this->type]) ? $type[$this->type] : null;
    }

    public static function getStatus()
    {
        return [
            self::STATUS_ACTIVE => 'Kích hoạt',
            self::STATUS_DEACTIVE => 'Không kích hoạt',
        ];
    }

    public static function getUsed()
    {
        return [
            self::IS_USED => 'Đã sử dụng',
            self::IS_NOT_USED => 'Chưa sử dụng'
        ];
    }

    public function getIsUsed()
    {
        $type = self::getUsed();

        return isset($type[$this->is_used]) ? $type[$this->is_used] : null;
    }

    public function getAvailableCourses()
    {
        $category = Category::find()->andWhere(['status' => Category::STATUS_ACTIVE])->all();
        $data = [];
        
        foreach ($category as $item) {
            $courseQuery = $item->getCourses();
            
            $data[$item->name] = ArrayHelper::map($courseQuery->all(), 'id', 'name');
        }
        
        return $data;
    }

    public static function avalable_courses()
    {
        $data = ArrayHelper::map(Course::find()->where(['status' => Course::STATUS_ACTIVE])->all(), 'id', 'name');
        return $data;
    }

    /**
     * Check course map with coupon
     */
    public function checkCourses($newCourses)
    {
        $oldItem = null;
        $assignedItem = $this->courses;
        foreach ($assignedItem as $item) {
            $oldItem[] = $item->id;
        }
//        $diff = array_diff($newCourses, $oldItem);
//        $one = array_diff($oldItem, $newCourses);
        foreach (array_diff($oldItem, $newCourses) as $item) {
            CouponCourse::deleteByCourseCoupon($item, $this->id);
        }
        foreach (array_diff($newCourses, $oldItem) as $item) {
            $this->assignCourse($item, $this->id);
        }
        return true;
    }

    /**
     * After save coupon, insert into CouponCourse table
     * @params $course_id, $coupon_id
     * @type integer
     * @return bool
     */
    protected function saveCouponCourse($event)
    {
        if ($this->apply_for_all)
            return true;

        $courses = $event->sender->course_id;
        $couponId = $event->sender->id;
        if (empty ($courses))
            return true;
        foreach ($courses as $course) {
            $this->assignCourse($course, $couponId);
        }
        return true;
    }

    /**
     *
     */
    protected function assignCourse($courseId, $couponId)
    {
        $assign_item = new CouponCourse();
        $assign_item->promotion_id = $couponId;
        $assign_item->course_id = $courseId;
        $assign_item->save();
        return true;
    }

    public function limitCouponPercent($attribute, $params)
    {
        if ($this->type == self::TYPE_PERCENTAGE && $this->value > self::LIMIT_COUPON_PERCENTAGE) {
            $this->addError($attribute, 'Không thể tạo mức giảm giá trên 70%');
        }
    }

    public function canTeacherCreate()
    {
        if (Yii::$app->user->can('Teacher')) {
            $totalInMonth = self::find()
                ->where([
                    'created_user_id' => Yii::$app->user->id
                ])
                ->andWhere('YEAR(FROM_UNIXTIME(created_time)) = YEAR(CURRENT_DATE)')
                ->andWhere('MONTH(FROM_UNIXTIME(created_time)) = MONTH(CURRENT_DATE)')
                ->count();
            if (intval($totalInMonth) >= self::LIMIT_COUPON_QUANTITY) {
                return false;
            }
        }
        return true;
    }

    public function validDateRange($attribute, $params)
    {
        if (Yii::$app->user->can('Teacher')) {
            $dateTimeFormat = str_replace('php:', '', Yii::$app->formatter->datetimeFormat);
            $startDate = date_create_from_format($dateTimeFormat, trim($this->start_date));
            $exprirationDate = date_create_from_format($dateTimeFormat, trim($this->expiration_date));
            if ($startDate !== false && $exprirationDate !== false) {
                $startTime = $startDate->getTimestamp();
                $exprirationTime = $exprirationDate->getTimestamp();
                if ($startTime >= $exprirationTime) {
                    $this->addError($attribute, 'Ngày hết hạn phải lớn hơn ngày bắt đầu');
                } else {
                    $nextMonth = $startDate->add(new \DateInterval('P1M'));
                    $nextmonthTime = $nextMonth->getTimestamp();
                    if ($nextmonthTime < $exprirationTime) {
                        $this->addError($attribute, 'Thời gian áp dụng coupon không vượt quá 1 tháng');
                    }
                }
            }
        }
        return true;
    }
    
    public static function find()
    {
        return parent::find()->andWhere(['kind' => self::KIND_COUPON]);
    }

}
