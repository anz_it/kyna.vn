<?php

namespace kyna\promo\models;

use Yii;
use yii\helpers\ArrayHelper;
use kyna\course\models\Course;
/**
 * This is the model class for table "{{%group_discount}}".
 *
 * @property integer $id
 * @property string $name
 * @property double $percent_discount
 * @property integer $max_discount_amount
 * @property integer $course_quantity
 * @property boolean $is_deleted
 * @property integer $status
 * @property integer $created_time
 * @property integer $updated_time
 */
class GroupDiscount extends \kyna\base\ActiveRecord
{
    const EQUAL_TYPE = 0;
    const GREATER_TYPE = 1;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%group_discounts}}';
    }

    protected static function softDelete()
    {
        return TRUE;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'percent_discount', 'course_quantity','type','is_frontend'], 'required'],
            [['percent_discount'], 'number'],
            [['course_quantity', 'status', 'created_time', 'updated_time', 'max_discount_amount'], 'integer'],
            [['is_frontend'], 'boolean'],
            [['is_deleted'], 'boolean'],
            ['percent_discount', 'integer', 'max' => 100, 'min' => 0],
            ['course_quantity', 'integer', 'max' => 10, 'min' => 0],
            ['course_quantity', 'unique','on'=>'create'],
            [['name'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Tên',
            'percent_discount' => 'Phần trăm giảm giá',
            'course_quantity' => 'Số lượng khóa học',
            'is_frontend' => 'Áp dụng front end',
            'is_deleted' => 'Is Deleted',
            'status' => 'Trạng thái',
            'created_time' => 'Created Time',
            'updated_time' => 'Updated Time',
            'max_discount_amount' => 'Số tiền tối đa được giảm',
            'type' => 'Điều kiện giảm giá',
        ];
    }
    public function getTypes()
    {
        return array (self::EQUAL_TYPE =>'Bằng', self::GREATER_TYPE =>'Lớn hơn bằng');
    }

    public static function getNextDiscountText($cartItems, $product)
    {
        $cartCount = 0;
        foreach ($cartItems as $cartItem)
        {
          if($cartItem->type != Course::TYPE_COMBO && $cartItem->isCampaignGroupDiscount)
              $cartCount++;
        }
        if($product->isCampaignGroupDiscount)
        {
            $nextDisCountGroup = self::getCurrentGroupDiscount($cartCount + 1);
            if(!empty($nextDisCountGroup))
            {
                return "Thêm khóa này sẽ được giảm ". $nextDisCountGroup->percent_discount. "%";
            }

        }
        return "";
    }
    public static function applyGroupDiscountCartItems($cartItems)
    {
        $cartCount = 0;
        $totalGroupDiscount = 0;
        foreach ($cartItems as $cartItem)
        {
            if($cartItem->type != Course::TYPE_COMBO && $cartItem->isCampaignGroupDiscount)
                $cartCount++;
        }

        $groupDiscount =  self::getCurrentGroupDiscount($cartCount);
        foreach ($cartItems as $cartItem)
        {
            if($cartItem->isCampaignGroupDiscount && empty($cartItem->course_combo_id)) {
                $cartItem->setGroupDiscount($groupDiscount);
                $totalGroupDiscount += $cartItem->getDiscountGroupAmount();
            }
        }
        return ['currentGroupDiscount'=>$groupDiscount, 'totalGroupDiscount'=>$totalGroupDiscount];
    }

    public static function getCurrentGroupDiscount($courseQuantity)
    {
        $allGroups = self::find()
            ->andWhere(['status' => self::STATUS_ACTIVE])
            ->orderBy(['course_quantity' => SORT_ASC])
            ->all();
        $currentGroup = null;
        foreach ($allGroups as $group) {
            if ($group->type == self::EQUAL_TYPE && $group->course_quantity == $courseQuantity) {
                $currentGroup = $group;
            }
        }
        if (empty($currentGroup)) {
            foreach ($allGroups as $group) {
                if ($group->type == self::GREATER_TYPE && $group->course_quantity <= $courseQuantity) {
                    $currentGroup = $group;
                }
            }
        }
        return $currentGroup;
    }
    public static function getGroupDiscountCourses()
    {
        $courses = GroupDiscountCourse::find()->select('course_id')->asArray()->all();
        if (!empty($courses))
            return ArrayHelper::getColumn($courses, 'course_id');
        return [];
    }

    public static function getTotalPriceBirthday($courses){
        $course_ids = self::getGroupDiscountCourses();
        $total = 0;
        foreach ($courses as $course){
            if(in_array($course->id, $course_ids)){
                $total += $course->sellPrice;
            }
        }
        return $total;
    }
    public static function checkApplyBirthday($details){
        $course_ids = self::getGroupDiscountCourses();
        $total = 0;
        foreach ($details as $detail){
            if(in_array($detail->course_id, $course_ids)){
                $total += $detail->course->sellPrice;
            }
        }

       return $total >= 500000;
    }

}
