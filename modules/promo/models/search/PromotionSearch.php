<?php

namespace kyna\promo\models\search;

use yii\data\ActiveDataProvider;
use kyna\promo\models\Promotion;

/**
 * VoucherSearch represents the model behind the search form about `kyna\promo\models\Voucher`.
 */
class PromotionSearch extends Promotion
{
    
    public $from_date;
    public $to_date;
    
    /**
     * @inheritdoc
     */
    public $kind = self::KIND_VOUCHER;

    public function rules()
    {
        return [
            [['id', 'value', 'min_amount', 'expiration_date', 'seller_id', 'user_id', 'partner_id', 'order_id', 'used_date', 'created_user_id', 'created_time', 'updated_time', 'number_usage', 'current_number_usage', 'status'], 'safe'],
            [['code', 'note', 'from_date', 'to_date'], 'safe'],
            [['is_deleted', 'is_used'], 'boolean'],
        ];
    }

    public function search($params)
    {
        $query = Promotion::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'created_time' => SORT_DESC
                ]
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'value' => $this->value,
            'kind'  => $this->kind,
            'min_amount' => $this->min_amount,
            'expiration_date' => $this->expiration_date,
            'seller_id' => $this->seller_id,
            'user_id' => $this->user_id,
            'order_id' => $this->order_id,
            'is_used' => $this->is_used,
            'used_date' => $this->used_date,
            'created_user_id' => $this->created_user_id,
            'is_deleted' => $this->is_deleted,
            'created_time' => $this->created_time,
            'updated_time' => $this->updated_time,
            'current_number_usage' => $this->current_number_usage,
            'number_usage' => $this->number_usage,
            'partner_id' => $this->partner_id,
            'status' => $this->status,
        ]);
        
        $query->andFilterWhere(['like', 'code', $this->code])
            ->andFilterWhere(['like', 'note', $this->note]);
        
        if (!empty($this->from_date)) {
            $query->andWhere("DATE_FORMAT(from_unixtime(start_date), '%d/%m/%Y') = :from_date", ['from_date' => $this->from_date]);
        }
        
        if (!empty($this->to_date)) {
            $query->andWhere("DATE_FORMAT(from_unixtime(expiration_date), '%d/%m/%Y') = :to_date", ['to_date' => $this->to_date]);
        }

        return $dataProvider;
    }
}
