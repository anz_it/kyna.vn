<?php

namespace kyna\promo\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use kyna\promo\models\PromotionSchedule;

/**
 * PromotionScheduleSearch represents the model behind the search form about `kyna\promo\models\PromotionSchedule`.
 */
class PromotionScheduleSearch extends PromotionSchedule
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'course_id', 'start_time', 'end_time', 'discount_amount', 'price', 'reg_count', 'status', 'created_time', 'updated_time'], 'integer'],
            [['discount_percent'], 'number'],
            [['note', 'image_url'], 'safe'],
            [['is_deleted'], 'boolean'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PromotionSchedule::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'course_id' => $this->course_id,
            'start_time' => $this->start_time,
            'end_time' => $this->end_time,
            'discount_percent' => $this->discount_percent,
            'discount_amount' => $this->discount_amount,
            'price' => $this->price,
            'reg_count' => $this->reg_count,
            'is_deleted' => $this->is_deleted,
            'status' => $this->status,
            'created_time' => $this->created_time,
            'updated_time' => $this->updated_time,
        ]);

        $query->andFilterWhere(['like', 'note', $this->note])
            ->andFilterWhere(['like', 'image_url', $this->image_url]);
        
        $query->orderBy('id desc');

        return $dataProvider;
    }
}
