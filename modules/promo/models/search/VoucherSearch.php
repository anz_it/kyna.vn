<?php

namespace kyna\promo\models\search;

use kyna\promo\models\Voucher;
use kyna\promotion\models\Promotion;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * VoucherSearch represents the model behind the search form about `kyna\promo\models\Voucher`.
 */
class VoucherSearch extends Promotion
{
    /**
     * @inheritdoc
     */
    public $kind = self::KIND_ORDER_APPLY;

    public function rules()
    {
        return [
            [['id', 'value', 'min_amount', 'end_date', 'seller_id', 'user_id', 'order_id', 'used_date', 'created_by', 'created_time', 'updated_time', 'number_usage', 'current_number_usage', 'status'], 'safe'],
            [['code', 'note'], 'safe'],
            [['is_deleted', 'is_used'], 'boolean'],
            [['value','number_usage', 'current_number_usage'], 'number'],
            [['code', 'value', 'note', 'number_usage', 'current_number_usage'], 'trim'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function searchPrintHtml($params)
    {
        $query = Promotion::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'created_time' => SORT_DESC
                ]
            ],
            'pagination' => [
                'pageSize' => 500,
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'value' => $this->value,
            'type'  => $this->type,
            'min_amount' => $this->min_amount,
            'end_date' => $this->end_date,
            'seller_id' => $this->seller_id,
            'user_id' => $this->user_id,
            'order_id' => $this->order_id,
            'is_used' => $this->is_used,
            'used_date' => $this->used_date,
            'created_by' => $this->created_by,
            'is_deleted' => $this->is_deleted,
            'created_time' => $this->created_time,
            'updated_time' => $this->updated_time,
            'current_number_usage' => $this->current_number_usage,
            'number_usage' => $this->number_usage,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'code', $this->code])
            ->andFilterWhere(['like', 'note', $this->note]);

        return $dataProvider;
    }

    public function search($params)
    {
        $query = Voucher::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'created_time' => SORT_DESC
                ]
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'value' => $this->value,
            'type'  => $this->type,
            'min_amount' => $this->min_amount,
            'end_date' => $this->end_date,
            'seller_id' => $this->seller_id,
            'user_id' => $this->user_id,
            'order_id' => $this->order_id,
            'is_used' => $this->is_used,
            'used_date' => $this->used_date,
            'created_by' => $this->created_by,
            'is_deleted' => $this->is_deleted,
            'created_time' => $this->created_time,
            'updated_time' => $this->updated_time,
            'current_number_usage' => $this->current_number_usage,
            'number_usage' => $this->number_usage,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'code', $this->code])
            ->andFilterWhere(['like', 'note', $this->note]);

        return $dataProvider;
    }
}
