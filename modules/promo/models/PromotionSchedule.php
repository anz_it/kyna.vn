<?php

namespace kyna\promo\models;

use Yii;
use kyna\course\models\Course;
use kyna\course_combo\models\CourseCombo;
use kyna\course_combo\models\CourseComboItem;

/**
 * This is the model class for table "{{%promotion_schedules}}".
 *
 * @property integer $id
 * @property integer $course_id
 * @property integer $start_time
 * @property integer $end_time
 * @property string $discount_percent
 * @property integer $discount_amount
 * @property integer $price
 * @property integer $reg_count
 * @property string $note
 * @property string $image_url
 * @property boolean $is_deleted
 * @property integer $status
 * @property integer $created_time
 * @property integer $updated_time
 */
class PromotionSchedule extends \kyna\base\ActiveRecord
{
    
    public $old_price;
    public $date_ranger;
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%promotion_schedules}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['course_id', 'discount_percent', 'discount_amount', 'price'], 'required'],
            ['date_ranger', 'required', 'on' => ['create', 'update']],
            [['course_id', 'start_time', 'end_time', 'reg_count', 'status', 'created_time', 'updated_time'], 'integer'],
            [['discount_amount', 'price'], 'integer', 'min' => 1],
            [['discount_percent'], 'number', 'min' => 0.01, 'max' => 99.99],
            [['image_url'], 'string'],
            [['is_deleted'], 'boolean'],
            [['note'], 'string', 'max' => 255],
        ];
    }
    
    public function checkCourseAlreadyInCombo($attribute)
    {
        if (!$this->hasErrors($attribute)) {
            $comboQuery = CourseCombo::find();
            
            $itemTable = CourseComboItem::tableName();
            $comboQuery->joinWith('comboItems');
            $comboQuery->andWhere(["$itemTable.course_id" => $this->$attribute]);
            
            if ($comboQuery->exists()) {
                $this->addError($attribute, 'Khóa học nãy đã tồn tại trong combo nên không được áp dụng chương trình khuyến mãi khác.');
            }
        }
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'course_id' => 'Khóa học',
            'start_time' => 'Bắt đầu',
            'end_time' => 'Kết thúc',
            'discount_percent' => 'Phần trăm giảm giá',
            'discount_amount' => 'Số tiền được giảm',
            'price' => 'Học phí thanh toán',
            'reg_count' => 'Số lượng đăng ký',
            'note' => 'Ghi chú',
            'image_url' => 'Link hình quảng cáo',
            'is_deleted' => 'Is Deleted',
            'status' => 'Trạng thái',
            'created_time' => 'Ngày tạo',
            'updated_time' => 'Updated Time',
            'date_ranger' => 'Thời gian áp dụng'
        ];
    }
    
    public function getCourse()
    {
        return $this->hasOne(Course::className(), ['id' => 'course_id']);
    }
    
    public function afterFind()
    {
        $ret = parent::afterFind();
        
        if (!empty(Yii::$app->params['time']['to'])) {
            $timeToText = Yii::$app->params['time']['to'];

            $dateTimeFormat = str_replace('php:', '', Yii::$app->formatter->datetimeFormat);
            $this->date_ranger = date($dateTimeFormat, $this->start_time) . " $timeToText " . date($dateTimeFormat, $this->end_time);
        }
        
        $this->old_price = $this->course->price;
        
        return $ret;
    }

    public function beforeSave($insert)
    {
        $ret = parent::beforeSave($insert);

        $time = time();

        $this->created_time = $time;
        $this->updated_time = $time;

        if ($insert && $this->hasAttribute('created_user_id') && empty($this->created_user_id) && !empty(Yii::$app->user)) {
            $this->created_user_id = Yii::$app->user->id;
        }

        if ($this->hasAttribute('updated_user_id') && !empty(Yii::$app->user)) {
            $this->updated_user_id = Yii::$app->user->id;
        }


        return $ret;
    }
}
