<?php

namespace kyna\settings\components;
use yii\base\Component;
use kyna\settings\models\Setting as SettingModel;

class Settings extends \stdClass {
    public function __construct() {
        //parent::init();
        $settings = SettingModel::find()->all();

        foreach ($settings as $setting) {
            $this->{$setting->key} = $setting->value;
        }
    }
}
