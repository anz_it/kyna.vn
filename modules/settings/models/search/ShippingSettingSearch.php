<?php

namespace kyna\settings\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use kyna\settings\models\ShippingSetting;

/**
 * ShippingSettingSearch represents the model behind the search form about `kyna\settings\models\ShippingSetting`.
 */
class ShippingSettingSearch extends ShippingSetting
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'city_id', 'type', 'fee', 'min_amount', 'status', 'created_time', 'updated_time'], 'integer'],
            [['is_deleted'], 'boolean'],
            [['id', 'fee', 'min_amount'], 'trim'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ShippingSetting::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'city_id' => $this->city_id,
            'type' => $this->type,
            'fee' => $this->fee,
            'min_amount' => $this->min_amount,
            'is_deleted' => $this->is_deleted,
            'status' => $this->status,
            'created_time' => $this->created_time,
            'updated_time' => $this->updated_time,
        ]);

        return $dataProvider;
    }
}
