<?php

namespace kyna\settings\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use kyna\settings\models\Banner;

/**
 * BannerSearch represents the model behind the search form about `kyna\setting\models\Banner`.
 */
class BannerSearch extends Banner
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'status', 'category_id', 'created_time', 'updated_time','app_action'], 'integer'],
            [['title', 'description', 'link', 'image_url', 'type', 'app_action_data'    ], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $defaultTypes = [
        BannerSearch::TYPE_CROSS_PRODUCT_BOTTOM,
        BannerSearch::TYPE_CROSS_PRODUCT_LEFT
    ])
    {
        $query = self::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC
                ]
            ]
        ]);

        $this->load($params);

        $types = $this->type;
        if ($types == null && !empty($defaultTypes)) {
            $types = $defaultTypes;
        }

        if (!$this->validate()) {
            $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'type' => $types,
            'category_id' => $this->category_id,
            'app_action' => $this->app_action,
            'status' => $this->status,
            'created_time' => $this->created_time,
            'updated_time' => $this->updated_time,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'link', $this->link])
            ->andFilterWhere(['like', 'app_action_data', $this->app_action_data])
            ->andFilterWhere(['like', 'image_url', $this->image_url]);

        return $dataProvider;
    }
}
