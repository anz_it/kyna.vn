<?php

namespace kyna\settings\models;

use Yii;
use kyna\base\models\Location;

/**
 * This is the model class for table "{{%shipping_configs}}".
 *
 * @property integer $id
 * @property integer $city_id
 * @property integer $type
 * @property integer $fee
 * @property integer $min_amount
 * @property boolean $is_deleted
 * @property integer $status
 * @property integer $created_time
 * @property integer $updated_time
 */
class ShippingSetting extends \kyna\base\ActiveRecord
{
    
    public static function softDelete()
    {
        return true;
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%shipping_settings}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['city_id', 'type', 'fee', 'min_amount', 'status', 'created_time', 'updated_time'], 'integer'],
            [['type', 'fee'], 'required'],
            [['is_deleted'], 'boolean'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'city_id' => 'Tỉnh/Thành',
            'type' => 'Loại',
            'fee' => 'Phí',
            'min_amount' => 'Giá trị đơn hàng tối thiểu',
            'is_deleted' => 'Is Deleted',
            'status' => 'Trạng thái',
            'created_time' => 'Created Time',
            'updated_time' => 'Updated Time',
        ];
    }
    
    public function getTypeText()
    {
        $types = Location::getTypes();
        
        return isset($types[$this->type]) ? $types[$this->type] : null;
    }
    
    public function getCity()
    {
        return $this->hasOne(Location::className(), ['id' => 'city_id']);
    }
}
