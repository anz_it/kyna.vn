<?php

namespace kyna\settings\models;

use Yii;

/**
 * This is the model class for table "banner_group_items".
 *
 * @property integer $id
 * @property integer $banner_group_id
 * @property integer $banner_id
 * @property string $position
 */
class BannerGroupItem extends \kyna\base\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'banner_group_items';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['banner_group_id', 'banner_id'], 'required'],
            [['banner_group_id', 'banner_id'], 'integer'],
            [['position'], 'string', 'max' => 20],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'banner_group_id' => 'Banner Group',
            'banner_id' => 'Banner',
            'position' => 'Vị trí',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBanner()
    {
        return $this->hasOne(Banner::className(), ['id' => 'banner_id']);
    }
}
