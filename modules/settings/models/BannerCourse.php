<?php

namespace kyna\settings\models;

use kyna\course\models\Course;
use Yii;

/**
 * This is the model class for table "banner_courses".
 *
 * @property integer $id
 * @property integer $banner_id
 * @property integer $course_id
 */
class BannerCourse extends \kyna\base\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'banner_courses';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['banner_id', 'course_id'], 'required'],
            [['banner_id', 'course_id'], 'integer'],
            [['banner_id', 'course_id'], 'unique', 'targetAttribute' => ['banner_id', 'course_id'], 'message' => 'The combination of Banner ID and Course ID has already been taken.'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'banner_id' => 'Banner ID',
            'course_id' => 'Course ID',
        ];
    }

    public function getCourse()
    {
        return $this->hasOne(Course::className(), ['id' => 'course_id']);
    }
}
