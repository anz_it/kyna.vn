<?php

namespace kyna\settings\models;

use Yii;

/**
 * This is the model class for table "{{%teacher_settings}}".
 *
 * @property integer $id
 * @property integer $teacher_id
 * @property string $key
 * @property string $value
 * @property integer $updated_time
 */
class TeacherSetting extends \kyna\base\ActiveRecord
{

    const KEY_RECEIVE_MAIL_DATE = 'receive_mail_date';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%teacher_settings}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['teacher_id', 'key'], 'required'],
            [['teacher_id', 'updated_time'], 'integer'],
            [['value'], 'string'],
            [['key'], 'string', 'max' => 32],
            [['teacher_id', 'key'], 'unique', 'targetAttribute' => ['teacher_id', 'key'], 'message' => 'The combination of Teacher ID and Key has already been taken.'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'teacher_id' => 'Teacher ID',
            'key' => 'Key',
            'value' => 'Value',
            'updated_time' => 'Updated Time',
        ];
    }
}
