<?php

namespace kyna\settings\models;

use Yii;

/**
 * This is the model class for table "{{%banner_groups}}".
 *
 * @property integer $id
 * @property string $title
 * @property string $description
 * @property integer $type
 * @property string $from_date
 * @property string $to_date
 * @property integer $status
 * @property integer $created_time
 * @property integer $updated_time
 */
class BannerGroup extends \kyna\base\ActiveRecord
{

    const TYPE_HOME_SLIDER = 1;

    const POSITION_MAIN = 'main';
    const POSITION_SUB = 'sub';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%banner_groups}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'type', 'status'], 'required'],
            [['description'], 'string'],
            [['type', 'status', 'created_time', 'updated_time'], 'integer'],
            [['from_date', 'to_date'], 'safe'],
            [['title'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'description' => 'Mô tả',
            'type' => 'Loại',
            'from_date' => 'Bắt đầu',
            'to_date' => 'Kết thúc',
            'status' => 'Trạng thái',
            'created_time' => 'Ngày tạo',
            'updated_time' => 'Ngày cập nhật',
        ];
    }

    public static function getTypes()
    {
        return [
            self::TYPE_HOME_SLIDER => 'Home slider'
        ];
    }

    public function getTypeText()
    {
        $types = self::getTypes();
        return (isset($types[$this->type]) ? $types[$this->type] : null);
    }

    public function getItems()
    {
        return $this->hasMany(BannerGroupItem::className(), ['banner_group_id' => 'id']);
    }

    public function getItemsByPositionQuery($position)
    {
        return $this->getItems()->where(['position' => $position]);
    }

    public function afterDelete()
    {
        $ret = parent::afterDelete();

        $items = $this->items;

        foreach ($items as $item) {
            $item->delete();
        }

        return $ret;
    }

}
