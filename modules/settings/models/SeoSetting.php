<?php

namespace kyna\settings\models;

use kyna\base\models\MetaField;

/**
 * This is the model class for table "seo_setting_meta".
 *
 * @property integer $id
 * @property string $key
 * @property string $value
 */
class SeoSetting extends \yii\db\ActiveRecord
{

    public static function get($key)
    {
        $obj = SeoSetting::find()->where(['key' => $key])->one();
        if ($obj != null)
            return $obj->value;
        return "";
    }

    public function enableMeta()
    {
        return 'seo_setting';
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'seo_setting_meta';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['value'], 'string'],
            [['key'], 'string', 'max' => 255],
            [['key'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'key' => 'Key',
            'value' => 'Value',
        ];
    }

    public function saveMeta()
    {

    }

    public function getMetaField()
    {
        return MetaField::findOne(['key' => $this->key]);
    }
}
