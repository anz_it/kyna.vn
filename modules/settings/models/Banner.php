<?php

namespace kyna\settings\models;

use kyna\tag\models\Tag;
use Yii;
use kyna\course\models\Category;
use common\widgets\upload\UploadRequiredValidator;
use common\helpers\CDNHelper;

/**
 * This is the model class for table "{{%banners}}".
 *
 * @property integer $id
 * @property string $title
 * @property string $description
 * @property integer $type
 * @property string $link
 * @property string $image_url
 * @property string $mobile_image_url
 * @property integer $category_id
 * @property integer $status
 * @property date $from_date
 * @property date $to_date
 * @property string $app_action
 * @property string $app_action_data
 * @property integer $created_time
 * @property integer $updated_time
 * @property integer $from_date_time
 * @property integer $to_date_time
 */
class Banner extends \kyna\base\ActiveRecord
{

    const TYPE_TOP = 1;
    const TYPE_TOP_SUB = 7;
    const TYPE_HOME_SLIDER = 2;
    const TYPE_CAMPAIGN_CATEGORY_LARGE = 3;
    const TYPE_CAMPAIGN_CATEGORY_SMALL = 6;
    const TYPE_CROSS_PRODUCT_LEFT = 4;
    const TYPE_CROSS_PRODUCT_BOTTOM = 5;
    const TYPE_CHECKOUT_PAGE = 8;
    const TYPE_POPUP = 9;
    const TYPE_POPUP_LEARNING = 10;
    const TYPE_APP_HOME_SLIDER = 11;
    const TYPE_TOP_MY_COURSE = 12;
    const TYPE_TAG_PAGE = 13;
    const TYPE_TOP_MY_COURSE_BOTTOM = 14;
    const TYPE_POPUP_MY_COURSE = 15;

    const APP_ACTION_DETAIL = 1;
    const APP_ACTION_SEARCH = 2;
    const APP_ACTION_WEBLINK = 3;
    const APP_ACTION_TAG = 4;

    public $imageSize = [
        'cover' => [
            CDNHelper::IMG_SIZE_ORIGINAL
        ],
        'contain' => [],
        'crop' => []
    ];

    public $listCourseIds;
    public $listTagIds;


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%banners}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // required
            [['type', 'status'], 'required'],
            ['link', 'required', 'on' => ['default', 'campaign-category', 'popup']],
            ['link', 'safe', 'on' => ['checkout-banner', 'app-home', 'tag-banner']],
            ['category_id', 'required', 'on' => 'campaign-category'],

            // type
            [['description', 'app_action_data'], 'string'],
            ['link', 'url', 'defaultScheme' => 'https'],
            [['from_date', 'to_date'], 'date', 'format' => 'php:d/m/Y'],
            [['from_date_time', 'to_date_time'], 'safe', 'on' => ['tag-banner']],
            [['type', 'category_id', 'app_action', 'status', 'created_time', 'updated_time'], 'integer'],
            // extra
            [['title'], 'string', 'max' => 50],
            ['to_date', 'validateDate'],
            ['to_date_time', 'validateDateTime'],
            // files
            [['image_url', 'mobile_image_url'], UploadRequiredValidator::className(), 'skipOnEmpty' => true],
            [['image_url', 'mobile_image_url'], 'image', 'skipOnEmpty' => true, 'maxSize' => '4194304', 'mimeTypes' => 'image/png,image/jpeg,image/gif'], // 4MiB
            // safe
            ['listCourseIds', 'safe'],
            ['listTagIds', 'safe'],

            [['app_action', 'app_action_data'],'required', 'on' => 'app-home'],
        ];
    }


    public function validateDate($attibute)
    {
        if (!$this->hasErrors('to_date') && !empty($this->to_date) && !$this->hasErrors('from_date') && !empty($this->from_date)) {
            $toDate = date_create_from_format('d/m/Y', $this->to_date);
            $fromDate = date_create_from_format('d/m/Y', $this->from_date);
            if ($toDate == false) {
                $this->addError('to_date', 'Sai địng dạng');
                return false;
            }
            if ($fromDate == false) {
                $this->addError('from_date', 'Sai địng dạng');
                return false;
            }
            if ($toDate->getTimestamp() < $fromDate->getTimestamp()) {
                $this->addError('to_date', 'Ngày kết thúc phải sau ngày bắt đầu');
            }
        }
    }
    public function validateDateTime($attibute)
    {
        if(!empty($this->from_date_time) && !empty($this->to_date_time)){
            if($this->from_date_time > $this->to_date_time){
                $this->addError($attibute, 'Ngày kết thúc phải sau ngày bắt đầu');
                return false;
            }
        }
    }

    public function afterValidate()
    {
        $ret = parent::afterValidate();

        if ($this->scenario == 'app-home'   && (empty($this->app_action)  || empty($this->app_action_data))) {
            $this->addError('app', 'Bạn phải nhập ít nhất 1 trong 3 thông tin về action và data');
        }

        return $ret;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'description' => 'Mô tả',
            'type' => 'Type',
            'link' => 'Link',
            'image_url' => 'Hình',
            'mobile_image_url' => 'Hình mobile',
            'category_id' => 'Danh mục',
            'status' => 'Trạng thái',
            'from_date' => 'Bắt đầu',
            'to_date' => 'Kết thúc',
            'from_date_time' => 'Thời gian bắt đầu',
            'to_date_time' => 'Thời gian kết thúc',
            'created_time' => 'Ngày tạo',
            'updated_time' => 'Ngày cập nhật',
            'app_action' => 'App Action',
            'app_action_data' => 'App Action Data',
        ];
    }

    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'category_id']);
    }

    public static function getTypes()
    {
        return self::getTopTypes() + self::getCampaignCategoryTypes() + self::getCrossProductTypes() + self::getPopupTypes();
    }

    public static function getTopTypes()
    {
        return [
            self::TYPE_TOP => 'Top main',
            self::TYPE_TOP_SUB => 'Top second',
            self::TYPE_TOP_MY_COURSE => 'My course page(top)',
            self::TYPE_TOP_MY_COURSE_BOTTOM => 'My course page(bottom)',
        ];
    }

    public static function getCampaignCategoryTypes()
    {
        return [
            self::TYPE_CAMPAIGN_CATEGORY_LARGE => 'Campaign category large',
            self::TYPE_CAMPAIGN_CATEGORY_SMALL => 'Campaign category small',
        ];
    }

    public static function getPopupTypes()
    {
        return [
            self::TYPE_POPUP => 'Trang chủ',
            self::TYPE_POPUP_LEARNING => 'Trang bài học',
            self::TYPE_POPUP_MY_COURSE => 'Trang khóa học của tôi',
        ];
    }

    public static function getCrossProductTypes()
    {
        return [
            self::TYPE_CROSS_PRODUCT_LEFT => 'Left',
            self::TYPE_CROSS_PRODUCT_BOTTOM => 'Bottom'
        ];
    }

    public function getTypeText()
    {
        $options = self::getTypes();

        return (isset($options[$this->type]) ? $options[$this->type] : null);
    }

    public function getBannerGroupItems()
    {
        return $this->hasMany(BannerGroupItem::className(), ['banner_id' => 'id']);
    }

    public function getBannerGroups()
    {
        return $this->hasMany(BannerGroup::className(), ['id' => 'banner_group_id'])->via('bannerGroupItems');
    }

    public function getBannerCourses()
    {
        return $this->hasMany(BannerCourse::className(), ['banner_id' => 'id']);
    }
    public function getTags(){
        return $this->hasMany(Tag::className(), ['banner_id' => 'id']);
    }

    public function getAppActionText()
    {
        $listType = self::listAppActions();

        return isset($listType[$this->app_action]) ? $listType[$this->app_action] : null;
    }


    public static function listAppActions()
    {
        return [
            self::APP_ACTION_DETAIL => 'Vào chi tiết khóa học',
            self::APP_ACTION_SEARCH => 'Tìm kiếm khóa học',
            self::APP_ACTION_WEBLINK => 'Mở webview',
            self::APP_ACTION_TAG => 'Mở tag'
        ];
    }

    public function beforeSave($insert)
    {
        $ret =  parent::beforeSave($insert);
        if($this->type == self::TYPE_TAG_PAGE){
            if(!empty($this->listTagIds)){
                foreach ($this->listTagIds as $tagId){
                    $tag = Tag::findOne(['id' => $tagId]);
                    if(!empty($tag)){
                        $tag->banner_id = $this->id;
                        $tag->save(false);
                    }
                }
            }
            if (!empty($this->from_date_time)) {
                $startDate = date_create_from_format('d/m/Y H:i', $this->from_date_time);
                if ($startDate !== false) {
                    $this->from_date_time = $startDate->getTimestamp();
                }
            } else {
                $startDate = new \DateTime();
                $this->from_date_time = $startDate->getTimestamp();
            }
            if (!empty($this->to_date_time)) {
                $expireDate = date_create_from_format('d/m/Y H:i', $this->to_date_time);
                if ($expireDate !== false) {
                    $this->to_date_time = $expireDate->getTimestamp();
                }
            }

        }
        return $ret;
    }
}
