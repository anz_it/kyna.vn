<?php

namespace kyna\page\controllers;

use common\campaign\CampaignTet;
use common\helpers\DateTimeHelper;
use kyna\payment\models\PaymentMethod;
use kyna\promo\models\GroupDiscount;
use kyna\promotion\models\PromoCodeForm;
use kyna\promotion\models\Promotion;
use kyna\promotion\models\UserVoucherFree;
use Yii;
use yii\helpers\Url;
use yii\web\Response;
use yii\web\View;
use yii\web\Cookie;
use yii\web\NotFoundHttpException;
use yii\helpers\Json;

use common\helpers\TreeHelper;
use common\helpers\RoleHelper;
use common\components\BrowserDetect;
use common\helpers\CaptchaHelper;
use common\helpers\CDNHelper;
use common\helpers\LandingPageHelper;

use kyna\order\models\Order;
use kyna\page\models\Page;
use kyna\base\models\Location;
use kyna\user\models\UserTelesale;
use kyna\course\models\Course;
use kyna\user\models\TimeSlot;
use kyna\user\models\User;

use app\modules\cart\models\Product;

class PageController extends \app\components\Controller
{

    public $layout = '@app/views/pages/layout';
    public $viewPath;
    const FLASHSALE_NUMBER_KEY  = 'FLASHSALE_NUMBER';
    const FLASHSALE_LASTUPDATE_KEY  = 'FLASHSALE_LASTUPDATE';
    public function init()
    {
        parent::init();
        $this->setViewPath($this->viewPath);
    }

    public function renderScriptUserCare()
    {
        return false;
    }

    public function beforeAction($action)
    {
        if ($action->id == 'submit-query-district' // || $action->id== 'submit'
        ) {
            $this->enableCsrfValidation = false;
        }
        if (isset(Yii::$app->params['maintenance']) && Yii::$app->params['maintenance'] === true) {
            return $this->redirect('/bao-tri');
        }

        $get = Yii::$app->request->get();

        if (isset($get['dir']) && isset($get['slug'])) {
            $key = $get['dir'] . '/' . $get['slug'];
            $redirects = Yii::$app->params['landing-page-redirects'];
            if (isset($redirects[$key])) {
                unset($get['slug']);
                unset($get['dir']);
                $newUrl = '/p/' . $redirects[$key] . (!empty($get) ? ('?' . http_build_query($get)) : '');

                return $this->redirect($newUrl);
            }
        }

        return parent::beforeAction($action);
    }

    public function actionIndex($slug, $dir = '', $affiliate_id = 0)
    {
        //var_dump($this->settings);

        //var_dump(Yii::$app->request->get());
        //die;
        $data = [];

        !($dir) OR $slug = $dir . '/' . $slug;

        $page = Page::find()->where([
            'slug' => $slug,
//            'status' => Page::STATUS_ACTIVE
        ])->one();



        if (!$page) {
            throw new NotFoundHttpException('link ko tồn tại');
        }

        if ($affiliate_id) {
            Yii::$app->session->setFlash('affiliate_id', $affiliate_id);
            $get = Yii::$app->request->get();
            unset($get['slug']);
            unset($get['dir']);
            unset($get['affiliate_id']);
            $newURL = $page->getUrl(false); // FIXME: Neu get URL voi tham so khac FALSE se bi lap vo tan.

            if (!empty($get)) {
                $arrURL = http_build_query($get);
                $newURL .= '?' . $arrURL;
            }

            $this->redirect($newURL, 301);
        }
        if ($page['status'] == Page::STATUS_DEACTIVE) {
            // Get redirect Url
            if (!empty($page['redirect_url'])) {
                $redirectUrl = $page['redirect_url'];
                $this->redirect($redirectUrl, 301);
            } else {
                throw  new NotFoundHttpException('link không tồn tại');
            }
        }


        /* tạo các giá trị mặc định */
        $submitURL = \Yii::$app->urlManager->createUrl('page/default/submit');

        $utmSource = Yii::$app->request->get('utm_source', '');
        $trafficId = Yii::$app->request->get('traffic_id', false);

        if ($utmSource === 'masoffer' and $trafficId) {
            // Lưu cookie
            $time = time();
            $expire = $time + (60 * 24 * 60 * 60);
            $cookies = Yii::$app->response->cookies;

            $cookies->add(new Cookie([
                'name' => 'utm_source',
                'value' => $utmSource,
                'expire' => $expire,
                'path' => '/',
                'domain' => '.' . Yii::$app->request->serverName
            ]));

            // add a new cookie to the response to be sent
            $cookies->add(new Cookie([
                'name' => 'traffic_id',
                'value' => $trafficId,
                'expire' => $expire,
                'path' => '/',
                'domain' => '.' . Yii::$app->request->serverName
            ]));
        }

        /* Lấy thông tin quận huyện */
        $locationModels = Location::find()->all();
        $locations = TreeHelper::dataMapper($locationModels);

        $cityData = [
            ['id' => '', 'name' => 'Chọn Tỉnh/Thành phố']
        ];

        // Mục đích để đưa TP HCM, HN, ĐN lên đầu, không xóa đống này đi.
        $cityData[31] = [];
        $cityData[24] = [];
        $cityData[15] = [];

        foreach ($locations as $key => $location) {
            if ($location['parent_id'] == 0) {
                $cityData[$key] = ['id' => $key, 'name' => $location['name']];
            }
        }

        $data['submitURL'] = $submitURL;

        $data['slug'] = $slug;
        $data['city'] = $cityData;

        $ga_landing_page = "!function(e,a,n,t,c,s,o){e.GoogleAnalyticsObject=c,e[c]=e[c]||function(){(e[c].q=e[c].q||[]).push(arguments)},e[c].l=1*new Date,s=a.createElement(n),o=a.getElementsByTagName(n)[0],s.async=1,s.src=t,o.parentNode.insertBefore(s,o)}(window,document,'script','//www.google-analytics.com/analytics.js','ga'),ga('create','UA-5367336-36',{name:'ldp'}),ga('ldp.send','pageview');";
        $timestamp = strtotime(date('Y-m-d H:i:s'));
        $cdnUrl = CDNHelper::getMediaLink();
        $this->view->registerJsFile($cdnUrl . '/js/pages/init.js?v=1536293250', ['position' => View::POS_HEAD]);
        $this->view->registerJsFile($cdnUrl . '/js/pages/replace_URL.js?v=2', ['position' => View::POS_HEAD]);
        if (!empty($page->external_script)) {
            $this->view->registerJs($page->external_script, View::POS_HEAD);
        }

        $userId = Yii::$app->user->id;
        if (empty($userId))
            $userId = 0;
        $piwikId = Yii::$app->piwik->piwikId;

        if (isset($this->settings['landing_page_script'])) {


            $script = str_replace("USER_ID", $userId, $this->settings['landing_page_script']);
            $script = str_replace("//NATIVE_CODE//",
                "_paq.push(['setCustomVariable',1, 'piwikId','{$piwikId}', 'visit' ]);",
                $script
            );
            //echo $script;
            $this->view->registerJs($script, \yii\web\View::POS_BEGIN);
        }

        if (isset($this->settings['page_apply_mobile_track'])) {
            $pageApplyScript = explode(",", $this->settings['page_apply_mobile_track']);
            if (in_array($page->id, $pageApplyScript) || in_array('all', $pageApplyScript)) {
                // Check xem có cookie hay chưa, nếu chưa có thì redirect, nếu có rồi thì thôi.
                $browser = new BrowserDetect();
                if ($browser->isMobile() || $browser->isTablet()) {
                    if (!Yii::$app->request->cookies->has('pageTracked')) {
                        $pageTrackedCookie = new Cookie();
                        $pageTrackedCookie->name = "pageTracked";
                        $pageTrackedCookie->value = "1";
                        $pageTrackedCookie->expire = strtotime("+60 days");

                        Yii::$app->response->cookies->add($pageTrackedCookie);
                        $param = $piwikId . "-" . $page->id;

                        $gets = http_build_query($_GET);
                        return $this->redirect(Yii::$app->params['mobileTrackUrl'] . '?piwikId=' . $param . '&userId=' . $userId . "&ref=" . urlencode($slug) . '&' . $gets);
                    }
                }
            }
        }

        $gtmNoScript = "$( window ).load(function() {
            $('body').append('<noscript><iframe src=\"https://www.googletagmanager.com/ns.html?id=GTM-TVZVN3\" height=\"0\" width=\"0\" style=\"display:none;visibility:hidden\"></iframe></noscript>');
        });";
        $gtm = "$( window ).load(function() {
            window.dataLayer = window.dataLayer || [];
            var gtmUA = new GTMUA();
            <!-- Google Tag Manager -->
            (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
            })(window,document,'script','dataLayer','GTM-TVZVN3');
            <!-- End Google Tag Manager -->
        });";

        $this->view->registerJsFile($cdnUrl . '/src/js/gtm.js?v=1508125121', ['position' => View::POS_BEGIN]);
        $this->view->registerJs($gtm, View::POS_BEGIN); //POS_END
        $this->view->registerJs($gtmNoScript, View::POS_BEGIN); //POS_END

        //$this->view->registerJs($ga_landing_page, View::POS_END); //POS_END

        // payment redirect
        $get = Yii::$app->request->get();
        if (isset($get['payment_status'])) {
            Yii::$app->session->setFlash('payment_status', $get['payment_status']);
            unset($get['payment_status']);
            $route = ['/page/default/index'];
            if (sizeof($get)) {
                $route = array_merge($route, $get);
            }
            $url = Url::toRoute($route);
            $this->redirect($url, 301);
            Yii::$app->end();
        }
        if (Yii::$app->session->hasFlash('payment_status')) {
            $protocol = Yii::$app->request->isSecureConnection ? 'https://' : 'http://';
            $myCourseLink = $protocol . Yii::$app->request->serverName . Yii::$app->urlManager->createUrl(['trang-ca-nhan/khoa-hoc']);
            $paymentStatus = Yii::$app->session->getFlash('payment_status');
            if ($paymentStatus == true) {
                $res['status'] = true;
                $res['msg'] = "Cảm ơn bạn đã đăng ký thành công khóa học tại Kyna.vn! <br> Trong trường hợp bạn đã thanh toán online thành công, khóa học đã được thêm vào tài khoản học của bạn trên Kyna.vn. Bạn vui lòng đăng nhập và bắt đầu học bằng email bạn vừa dùng đăng ký. Trong trường hợp lần đầu đăng ký trên Kyna.vn, bạn truy cập email để biết thêm chi tiết cách đăng nhập. Nếu có thắc mắc, bạn có thể liên hệ hotline hoặc email hỗ trợ bên dưới. <br> Xem khóa học của tôi: <a href=\"{$myCourseLink}\">{$myCourseLink}</a>";
                $urlSuccess = $protocol . Yii::$app->request->serverName . Yii::$app->urlManager->createUrl('dang-ky-thanh-cong');
                $successScript = "
                    $(document).ready(function() {
                        if (typeof successHandler == 'function') {
                            successHandler('{$urlSuccess}');
                        }
                    });
                ";
                $this->view->registerJs($successScript, View::POS_END);
            } else {
                $res['status'] = false;
                $res['msg'] = "Quá trình thanh toán online không thành công. Bạn có thể kiểm tra email để nhận được hướng dẫn thanh toán lại. <br> Hoặc vui lòng liên hệ số hotline hoặc email bên dưới cũng như khung chat ở góc phải để nhận được hỗ trợ.";
            }
            $modalHtml = $this->_renderPaymentModal($res);
            $responseStatus = $res['status'] ? 1 : 0;
            if(in_array($get['payment_method'],['onepay_cc','onepay_atm'])){
                if($responseStatus == 1){
                    $triggerModalScript = "$(document).ready(function() {
                        $('#modalATM').modal();
                    });";
                }else{
                    if($get['payment_method'] == 'onepay_atm') {
                        $triggerModalScript = "$(document).ready(function() {
                        $('#modalfailedATM').modal();
                    });";
                    }
                    if($get['payment_method'] == 'onepay_cc') {
                        $triggerModalScript = "$(document).ready(function() {
                        $('#modalfailedCC').modal();
                    });";
                    }
                }

            }
            elseif(in_array($get['payment_method'],['bank-transfer'])){
                $triggerModalScript = "$(document).ready(function() {
                    $('#modalCK').modal();
                });";
            }
            elseif(in_array($get['cod'],['cod'])){
                $triggerModalScript = "$(document).ready(function() {
                    $('#modalCOD').modal();
                });";
            }
            else{
                $triggerModalScript = "
                $('#thankyou_modal').modal();
            ";
            }

            $this->view->registerJs($triggerModalScript, View::POS_END);
        }

        $viewFile = Yii::getAlias($this->viewPath . '/' . $slug . '/index.php');

        if (empty($page->content)) {
            if (!file_exists($viewFile)) {
                throw new NotFoundHttpException('link ko tá»“n táº¡i');
            }
            return $this->render("$slug/index", ['data' => $data]);
        } else {
            $view = LandingPageHelper::getView($page);
            return $this->render($view, ['data' => $data, 'page' => $page]);
        }
    }

    public function actionSubmit()
    {
        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();


            /* Tùy chỉnh các thông số */
            list($user_type, $plan_id, $course_id, $combo_id, $cod_money, $bonus, $combo_tuong_tac,
                $combo_khuyen_mai, $advice_name, $filter_advice, $khoa_hoc_quan_tam, $alias_name, $email,
                $phonenumber, $fullname, $firstName, $address, $city, $district, $note, $cong_viec, $han_che,
                $noi_lam_viec, $full_url, $page_slug, $landing_type, $list_course_ids, $slug, $payment_type, $is_non_check_email) = $this->getParamsValue($post);


            // verify re-captcha
            $clientSessionKey = 'lp_client_session_' . $slug;
            CaptchaHelper::setClientSession($clientSessionKey);
            $this->_verifyReCaptcha($post, $clientSessionKey);

            if(empty($email) && !empty($is_non_check_email))
            {
                $email = "hotro" . time() . '@kyna.vn';
            }

            $res = $this->checkPhoneEmail($phonenumber, $email);
            if (isset($res['status']) && !$res['status']) {
                return $this->_responseJson($post, $res, $user_type);
            }

            if ($plan_id > 0) {
                $user_type = UserTelesale::TYPE_PRICING_PLAN;
            } elseif ($course_id > 0) {
                $user_type = UserTelesale::TYPE_SINGLE_COURSE;
                // Tính Cod money
                $course = Course::findOne($course_id);
                if ($course) {
                    if ($advice_name == '') {
                        $advice_name = $course->name;
                    }
                    $cod_money = $course->sellPrice;
                }
            } elseif ($combo_id > 0) {
                $user_type = UserTelesale::TYPE_COMBO;
                // Tính Cod money
                $course = Course::findOne($combo_id);
                if ($course) {
                    $advice_name = $course->name;
                    $combo_items = $course->comboItems;
                    if ($combo_items) {
                        foreach ($combo_items as $key => $item) {
                            $cod_money += $item->price;
                        }
                    }
                }
                if ($alias_name != '') {
                    $advice_name = $alias_name;
                }
            } elseif (!empty($list_course_ids)) {
                $user_type = UserTelesale::TYPE_SINGLE_COURSE;
            }
            if ($combo_khuyen_mai == 'the-hoc-monkey-junior-tieng-anh' || $combo_khuyen_mai == 'the-hoc-monkey-junior-tieng-anh-da-ngon-ngu') {
                $user_type = UserTelesale::TYPE_COMBO;
            }
            if ($combo_tuong_tac === 'hoc-tieng-hoa-truc-tiep-cung-chuyen-gia'
                || $combo_tuong_tac === 'tieng-trung-giao-tiep-cung-chuyen-gia'
                || $combo_tuong_tac === 'thi-xep-lop-tieng-trung-giao-tiep'
                || $combo_tuong_tac === 'luyen-thi-ielts-speaking-55-v4'
                || $combo_tuong_tac === 'luyen-thi-chung-chi-jlpt'
                || $combo_tuong_tac === 'tieng-nhat-cho-nguoi-moi-bat-dau'
                || $combo_tuong_tac === 'hoc-tieng-trung-so-cap-qua-skype'
                || $combo_tuong_tac === 'tu-hoc-tieng-nhat-co-ban-danh-cho-nguoi-moi-bat-dau'
                || $combo_tuong_tac === 'tang-khoa-ielts-mien-phi'
                || $combo_tuong_tac === 'luyen-thi-ielts-speaking-55'
                || $combo_tuong_tac === 'luyen-thi-ielts-speaking-55-v2'
                || $combo_tuong_tac === 'luyen-thi-ielts-speaking-55-v3'
                || $combo_tuong_tac === 'luyen-thi-chung-chi-jlpt-v2'
                || $combo_tuong_tac === 'luyen-ielts-speaking-cung-chuyen-gia'
                || $combo_tuong_tac === 'dao-tao-nhan-su-cho-doanh-nghiep'
                || $combo_tuong_tac === 'tieng-trung-giao-tiep-cung-chuyen-gia-v3'

            ) {
                $user_type = UserTelesale::TYPE_COMBO_INTERACTIVE;
            }

            $model = Page::getUserTelesale($email, $user_type, $advice_name);
            $existOrder = false;
            if (!empty($course) || !empty($list_course_ids)) {
                $courseIDs = !empty($course) ? $course->id : explode(',', $list_course_ids);
                $existOrder = Page::getIsExistOrder($email, $courseIDs);
            }

            $cookies = Yii::$app->request->cookies;
            if ($existOrder) {
                $affiliate_id_cookie = (int)$cookies->getValue('affiliate_id');
                if ($model) {
                    if ($model->affiliate_id > 0 && $model->affiliate_id != $affiliate_id_cookie && $affiliate_id_cookie > 0) {
                        $old_affiliate_id = $model->affiliate_id;;
                        $model->affiliate_id = $affiliate_id_cookie;
                        $model->old_affiliate_id = $old_affiliate_id;
                        $model->updated_time = time();
                    } else if ($model->affiliate_id == 0 && $affiliate_id_cookie > 0) {
                        $model->affiliate_id = $affiliate_id_cookie;
                    }
                    $model->page_slug = $page_slug;
                    $model->save(false);
                }
                $res['status'] = 0;
                $res['errors'] = [];
                if (in_array($payment_type, [Page::PAYMENT_TYPE_ONEPAY_CC, Page::PAYMENT_TYPE_ONEPAY_ATM, Page::PAYMENT_TYPE_BANK_TRANSFER, Page::PAYMENT_TYPE_COD])) {
                    $protocol = Yii::$app->request->isSecureConnection ? 'https://' : 'http://';
                    $myCourseLink = $protocol . Yii::$app->request->serverName . Yii::$app->urlManager->createUrl(['trang-ca-nhan/khoa-hoc']);
                    $res['msg'] = 'Một số khoá học bạn đã chọn đã có trong tài khoản học của bạn! Vui lòng chọn khóa học khác. <br> Hoặc có thể liên hệ với Kyna qua khung chat bên dưới hoặc thông tin hỗ trợ';
                } 
            } else {

                if ($model) {
                    // update old user telesale + note
                    $model->setScenario(UserTelesale::SCENARIO_REREGISTER);
                    if ($payment_type != '') {
                        $model->reregister_message = $model->getAttributeLabel('payment_type') . ' : ' . $payment_type;
                    }
                    if ($model->phone_number) {
                        $model->reregister_message .= (($payment_type != '') ? ', ' : '') . $model->getAttributeLabel('phone_number') . ' cũ : ' . $model->phone_number;
                    }
                    $model->reregister_message .= ($model->reregister_message ? ', ' : '') . 'Ngày đăng ký trước đó: ' . Yii::$app->formatter->asDatetime($model->created_time);
                    $model->created_time = time();
                } else {
                    $model = new UserTelesale();
                    $model->setScenario(UserTelesale::SCENARIO_LANDING_PAGE);
                    $model->tel_id = TimeSlot::getOperatorsInShift(RoleHelper::ROLE_TELESALE, $user_type);
                    if ($payment_type != '') {
                        $model->payment_type = $model->getAttributeLabel('payment_type') . ' : ' . $payment_type;
                    }
                }

                list($filter_advice, $note) = $this->getFilterAdvice($advice_name, $note, $filter_advice, $khoa_hoc_quan_tam, $cong_viec, $han_che, $noi_lam_viec);

                $model->email = $email;
                $model->full_name = $fullname . $firstName;
                $model->phone_number = $phonenumber;
                $model->type = $user_type;
                $model->amount = $cod_money;
                if ($course_id > 0) {
                    $model->course_id = $course_id;
                }
                if ($combo_id > 0) {
                    $model->course_id = $combo_id;
                }
                if (!empty($list_course_ids)) {
                    $model->list_course_ids = $list_course_ids;
                }
                $model->bonus = $bonus;

                $model->affiliate_id = $cookies->getValue('affiliate_id');
                $model->street_address = $address;
                $model->form_name = $advice_name; // chưa làm cái này
                $model->location_id = $district;
                $model->note = $note;
                $model->page_slug = $page_slug;



                $url = Yii::$app->request->serverName . Yii::$app->urlManager->createUrl('dang-ky-thanh-cong');
                if (Yii::$app->request->isSecureConnection) {
                    $url_success_page = 'https://' . $url;
                } else {
                    $url_success_page = 'http://' . $url;
                }
                $res['succespage'] = $url_success_page;
                if (!empty($list_course_ids)) {
                    $res['product_id'] = $model->list_course_ids;
                } else {
                    $res['product_id'] = $model->course_id;
                }
                $res['price'] = $model->amount;

                if ($model->save()) {
                    Yii::$app->session->setFlash('lead', $model->attributes);
                    $post['saved_id'] = $model->id;
                    Yii::$app->session['landing_page_register_id'] = $model->id;
                    // response page_id to submit publishers
                    $page = Page::findOne(['slug' => $page_slug]);
                    if ($page) {
                        $res['page_id'] = $page->id;
                    }
                    $res['status'] = 1;
                    $res['errors'] = [];
                    if ($model->type == UserTelesale::TYPE_ADVICE) {
                        $res['msg'] = 'Bạn đã đăng ký nhận tư vấn thành công. <br> Kyna.vn sẽ sớm liên hệ với bạn để tư vấn thêm về chương trình học!';
                    } else {
                        $res['msg'] = 'Cảm ơn bạn đã đăng ký thành công khóa học tại Kyna.vn! <br> Nhân viên CSKH Kyna.vn sẽ sớm liên hệ với bạn trong vòng 24 giờ.';
                    }
                    // check page can redirect to checkout
                    if ($page && $page->is_redirect) {
                        $courseIDs = !empty($model->course_id) ? [$model->course_id] : explode(',', $model->list_course_ids);
                        if (!empty($courseIDs)) {
                            foreach ($courseIDs as $courseID) {
                                $product = $this->loadProductModel($courseID);
                                if ($product && !Yii::$app->cart->hasPosition($product->id)) {
                                    if (!$this->checkAlreadyInCourse($product->id)) {
                                        Yii::$app->cart->put($product, 1, $product->discountAmount);
                                    }
                                }
                            }
                            // save action log
                            $model->scenario = UserTelesale::SCENARIO_REDIRECT_CHECKOUT;
                            $model->redirect_message = "Chuyển sang trang thanh toán";
                            $model->save(false);
                            // response redirect checkout url
                            $res['redirectUrl'] = Url::toRoute(['/cart/checkout/index']);
                            Yii::$app->session->setFlash('user_telesale_id', $model->id);
                        }
                    }
                } else {
                    $res['status'] = 0;
                    $res['errors'] = $model->errors;
                    $res['msg'] = 'Quá trình gởi dữ liệu về hệ thống bị lỗi. Vui lòng thử lại sau. <br> Hoặc có thể liên hệ với Kyna qua khung chat bên dưới hoặc thông tin hỗ trợ';
                }
                /* do payment if page can */
                if (Page::canPayment($slug) && in_array($payment_type, [Page::PAYMENT_TYPE_ONEPAY_CC, Page::PAYMENT_TYPE_ONEPAY_ATM, Page::PAYMENT_TYPE_BANK_TRANSFER])) {
                    $order = $this->createOrder($model, $payment_type, $slug, $email);



                    // update status from payment online
                    if (!$order) {
                        $res['status'] = 0;
                        $res['errors'] = "Tạo đơn hàng không thành công";
                        $res['msg'] = 'Quá trình gởi dữ liệu về hệ thống bị lỗi. Vui lòng thử lại sau. <br> Hoặc có thể liên hệ với Kyna qua khung chat bên dưới hoặc thông tin hỡ trợ';
                    } else {
                        // update note when create order
                        $model->scenario = UserTelesale::SCENARIO_CREATE_ORDER;
                        $model->note = (($model->reregister_message) ? $model->reregister_message : $model->payment_type) . ", Đơn hàng: #{$order->id}";
                        $model->user_id = $order->user->id;
                        $model->save(false);
                        if($payment_type != Page::PAYMENT_TYPE_BANK_TRANSFER){
                            // process payment
                            $response = Page::doPayment($order);
                            if (!$response['status'] || empty($response['redirectUrl'])) {
                                Order::makeFailedPayment($order->id);
                                $res['status'] = 0;
                                $res['msg'] = $response['msg'];
                            } else {
                                $res['status'] = 1;
                                $res['msg'] = '';
                                $res['redirectUrl'] = $response['redirectUrl'];
                                $res['payment_method'] = $order->payment_method;
                            }
                        }else{
                            $res['status'] = 1;
                            $res['payment_method'] = $order->payment_method;
                        }
                        $res['user_info'] = implode(" - ", [$phonenumber, $fullname, $email, $order->id]);
                    }
                }
            }

            if($res['status'] == 1){
                if(CampaignTet::InTimesCampaign()){
                    CampaignTet::resetCart();
                }
            }

            return $this->_responseJson($post, $res, $user_type);
        }
    }

    /**
     * @param $post
     * @param $res
     * @param $user_type
     */
    private function _responseJson($post, $res, $user_type = UserTelesale::TYPE_ADVICE)
    {
        if (!isset($res['redirectUrl']) || $res['redirectUrl'] == '') {
            $relatedCourses = null;
            if (isset($post['RelatedCourses']) && !empty($post['RelatedCourses'])) {
                $relatedCourses = $post['RelatedCourses'];
            }
            $res['html'] = $this->renderPartial('@app/views/pages/thankyou-modal', [
                'relatedCourses' => $relatedCourses,
                'isAdvice' => ($user_type == UserTelesale::TYPE_ADVICE) ? true : false,
                'status' => $res['status'],
                'msg' => !empty($res['msg']) ? $res['msg'] : ''
            ]);
        }
        if (isset($post['saved_id'])) {
            $res['saved_id'] = $post['saved_id'];
        }
        echo Json::encode($res);
    }

    private function _renderPaymentModal($res)
    {
        return $this->renderPartial('@app/views/pages/thankyou-modal', [
            'relatedCourses' => null,
            'isAdvice' => false,
            'status' => $res['status'],
            'msg' => $res['msg']
        ]);
    }

    public function getFilterAdvice($advice_name, $note, $filter_advice, $khoa_hoc_quan_tam, $cong_viec, $han_che, $noi_lam_viec)
    {

        if ($filter_advice == 'hoc_cung_chuyen_gia') {
            $advice_name = 'PR KYNA - VOUCHER 100K';
            $note = 'Yêu cầu liên hệ tặng voucher 100k cho học viên.<br/>';
            if ($khoa_hoc_quan_tam != '') {
                $note .= 'Khóa học quan tâm: ' . $khoa_hoc_quan_tam . ' <br/>';
            }
        }
        if ($filter_advice == 'hoc_khong_gioi_han') {
            $advice_name = 'CAMPAIGN HỌC KHÔNG GIỚI HẠN HV MỚI';
            $note = 'Yêu cầu liên hệ tặng voucher 100k cho học viên.<br/>';
            $note .= ($cong_viec != '' ? 'Công việc: ' . $cong_viec . ' <br/>' : '');
            $note .= ($han_che != '' ? 'Hạn chế: ' . $han_che . ' <br/>' : '');
            $note .= ($noi_lam_viec != '' ? 'Nơi làm việc: ' . $noi_lam_viec . ' <br/>' : '');
        }
        return [
            $advice_name,
            $note
        ];
    }

    public function getParamsValue($post)
    {
        $user_type = isset($post['user_type']) ? $post['user_type'] : UserTelesale::TYPE_ADVICE;
        $plan_id = isset($post['plan_id']) ? (int)$post['plan_id'] : 0;
        $course_id = isset($post['course_id']) ? (int)$post['course_id'] : 0;
        $combo_id = isset($post['combo_id']) ? (int)$post['combo_id'] : 0;
        $cod_money = 0;
        $bonus = isset($post['bonus']) ? (int)$post['bonus'] : 2;
        $combo_tuong_tac = isset($post['combo_tuong_tac']) ? $post['combo_tuong_tac'] : '';
        $combo_khuyen_mai = isset($post['combo_khuyen_mai']) ? $post['combo_khuyen_mai'] : '';
        $advice_name = isset($post['advice_name']) ? $post['advice_name'] : '';
        $filter_advice = isset($post['filter_advice']) ? $post['filter_advice'] : '';
        $khoa_hoc_quan_tam = isset($post['khoa_hoc_quan_tam']) ? $post['khoa_hoc_quan_tam'] : '';
        $alias_name = isset($post['alias_name']) ? $post['alias_name'] : '';
        $email = isset($post['email']) ? $post['email'] : '';
        $phonenumber = isset($post['phonenumber']) ? $post['phonenumber'] : '';
        $fullname = isset($post['fullname']) ? $post['fullname'] : '';
        $firstName = isset($post['firstName']) ? $post['firstName'] : '';
        $address = isset($post['address']) ? $post['address'] : '';
        $city = isset($post['city']) ? (int)$post['city'] : 0;
        $district = isset($post['district']) ? (int)$post['district'] : 0;
        $note = '';
        $cong_viec = isset($post['cong_viec']) ? $post['cong_viec'] : '';
        $han_che = isset($post['han_che']) ? $post['han_che'] : '';
        $noi_lam_viec = isset($post['noi_lam_viec']) ? $post['noi_lam_viec'] : '';
        $full_url = isset($post['full_url']) ? $post['full_url'] : '';
        $page_slug = isset($post['page_slug']) ? $post['page_slug'] : '';
        $landing_type = isset($post['landing_type']) ? $post['landing_type'] : '';
        $list_course_ids = isset($post['list_course_ids']) ? $post['list_course_ids'] : [];
        $slug = isset($post['slug']) ? $post['slug'] : '';
        $payment_type = isset($post['payment_type']) ? $post['payment_type'] : '';
        $is_non_check_email = isset($post['is_non_check_email']) ? $post['is_non_check_email'] : '';
        return [
            $user_type,
            $plan_id,
            $course_id,
            $combo_id,
            $cod_money,
            $bonus,
            $combo_tuong_tac,
            $combo_khuyen_mai,
            $advice_name,
            $filter_advice,
            $khoa_hoc_quan_tam,
            $alias_name,
            $email,
            $phonenumber,
            $fullname,
            $firstName,
            $address,
            $city,
            $district,
            $note,
            $cong_viec,
            $han_che,
            $noi_lam_viec,
            $full_url,
            $page_slug,
            $landing_type,
            $list_course_ids,
            $slug,
            $payment_type,
            $is_non_check_email
        ];
    }

    public function checkPhoneEmail($phonenumber, $email)
    {
        $res = [];
        $phonenumber = preg_replace('/[^0-9]/', '', $phonenumber);
        if (strlen($phonenumber) > 12) {
            $res['status'] = 0;
            $res['msg'] = 'Bạn vui lòng điền đúng số điện thoại';
        }

        if ($email == '') {
            $res['status'] = 0;
            $res['msg'] = 'Bạn vui lòng điền đúng email';
        }

        if(in_array($email, \Yii::$app->params['lp_banned_emails'])) {
            $res['status'] = 0;
            $res['msg'] = 'Đăng kí không thành công. Bạn đã đăng kí khóa học này, cảm ơn bạn !';
        }
        return $res;
    }



    public function actionSubmitQueryDistrict()
    {

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();
            $this->getDistrictByCity($post['city']);
        }

        echo json_encode(['ok' => 'success']);
    }

    public function getDistrictByCity($city_id)
    {

        $locationModels = Location::find()->all();
        $locations = TreeHelper::dataMapper($locationModels);
        $locationTree = TreeHelper::createTree($locations);
        $currentCityDistricts = $locationTree[$city_id]['_children'];
        $result = [
            [
                'id' => '',
                'name' => 'Quận/Huyện'
            ]
        ];

        if ($currentCityDistricts) {
            foreach ($currentCityDistricts as $key => $value) {
                $result[] = ['id' => $value['id'], 'name' => $value['name']];
            }
        }
        echo json_encode($result);
        exit;
    }

    public function actionSubmitPublisherCivi()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();
            $cookies = Yii::$app->request->cookies;
            $affiliate_id = $cookies->getValue('affiliate_id');
            if ($affiliate_id == 354959 && isset($post['publisher']['CIVI'])) {
                $civi = explode('|', $post['publisher']['CIVI']);
                if (count($civi) == 1 || !isset(Yii::$app->session['landing_page_register_id'])) {
                    return [
                        'url' => '', 'script' => ''
                    ];
                }
                $userTelesale = UserTelesale::findOne(Yii::$app->session['landing_page_register_id']);
                if ($userTelesale == null) {
                    return [
                        'url' => '', 'script' => ''
                    ];
                }
                $tokenKey = $civi[0]; // FROM CIVI
                $id = Yii::$app->session['landing_page_register_id'] . "_" . date('YmdHi', $userTelesale->created_time);
                $token = md5($id . $tokenKey);
                $price = isset($post['price']) ? $post['price'] : '0';
                $items = "{$id},1,$price";
                $url = "https://member.civi.vn/cpa/per/?spid={$civi[1]}&u={$id}&items={$items}&utoken={$token}";
                $script = "
                    var affiliate_params = {
                        isitemact:'1',
                        spid : '{$civi[1]}',
                        u: '{$id}',
                        items: '{$items}',
                        utoken: '{$token}',
                    };
                ";
                // check type of campaign $civi[2] follow by: product/fixed price, default is product price
                if (!empty($civi[2]) && $civi[2] == 'fixed') {
                    $url = "https://member.civi.vn/cpa/per/?spid={$civi[1]}&u={$id}&utoken={$token}";
                    $script = "
                        var affiliate_params = {
                            isitemact:'0',
                            spid : '{$civi[1]}',
                            u: '{$id}',
                            utoken: '{$token}',
                        };
                    ";
                }
                return [
                    'url' => $url,
                    'script' => $script
                ];
            }
        }
        return [
            'url' => '', 'script' => ''
        ];
    }

    public function actionSubmitPublisherAdpia()
    {
        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();
            $fullname = isset($post['fullname']) ? $post['fullname'] : '';
            $email = isset($post['email']) ? $post['email'] : '';
            $page_slug = isset($post['page_slug']) ? $post['page_slug'] : '';
            $cookies = Yii::$app->request->cookies;
            $publisher = isset($post['publisher']) ? $post['publisher'] : [];

            if (isset($cookies["APINFO"]) && isset($publisher[strtoupper('adpia')]) && $publisher[strtoupper('adpia')] === 'true') {

                $SampleXml = "<ADPIA>"
                    . "<MID>kyna</MID>"
                    . "<AID>" . $cookies['APINFO'] . "</AID>"
                    . "<MBRID>$fullname</MBRID>"
                    . "<OCD>$email</OCD>"
                    . "<PCD>$page_slug</PCD>"
                    . "<ITCNT>1</ITCNT>"
                    . "<SALES>0</SALES>"
                    . "<CCD></CCD>"
                    . "<PNM>Đăng ký khóa học</PNM>"
                    . "<IPADDR>" . $_SERVER['REMOTE_ADDR'] . "</IPADDR>"
                    . "<TYPE>CPS</TYPE>"
                    . "</ADPIA>";

                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, "http://purchase.adpia.vn/api/purchase_xml.php");
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $SampleXml);
                curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: text/plain']);

                $result = trim(curl_exec($ch));
                echo Json::encode(['status' => $result]);
                exit;
            } else {
                echo Json::encode(['status' => false]);
                exit;
            }
        }
    }

    public function actionSubmitPublisherAccesstrade()
    {
        $cookies = Yii::$app->request->cookies;
        $affiliate_id = $cookies->getValue('affiliate_id');

        Yii::$app->response->format = Response::FORMAT_JSON;
        $res = [];
        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();
            $publisher = isset($post['publisher']) ? !empty($post['publisher']) : [];
            //60519
            //311761
            if ($affiliate_id == 311761 && isset($publisher) && !empty($publisher) > 0) {
                // get user telesale register ID
                $registerID = Yii::$app->session->get('landing_page_register_id', 0);
                $userTelesale = UserTelesale::findOne($registerID);
                if ($userTelesale) {
                    $identifier = $registerID . "_" . date('YmdHis', $userTelesale->created_time);
                    $res['order_id'] = $identifier;
                    $res['price'] = isset($post['price']) ? $post['price'] : 0;
                    $res['page_id'] = isset($post['page_id']) ? $post['page_id'] : 0;
                    $res['product_id'] = isset($post['combo_id']) ? 'LP' .$post['combo_id'] : 0;
                    if(!isset($post['combo_id'])){
                        $res['product_id'] = isset($post['course_id']) ? 'LP' .$post['course_id'] : 0;
                    }
                    $res['product_category'] = isset($post['slug']) ? $post['slug'] : '';
                }
            }
        }
        return $res;
    }

    public function actionSubmitPublisherYeah1Offer()
    {
        $cookies = Yii::$app->request->cookies;
        $affiliate_id = $cookies->getValue('affiliate_id');
        $res = ['script' => ''];
        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();
            $publisher = isset($post['publisher']) ? $post['publisher'] : [];
            if ($affiliate_id == 362401 && !empty($publisher[strtoupper('yeah1offer')])) {
                // get user telesale register ID
                $registerID = Yii::$app->session->get('landing_page_register_id', 0);
                $userTelesale = UserTelesale::findOne($registerID);
                if ($userTelesale) {
                    $identifier = $registerID . "_" . date('YmdHi', $userTelesale->created_time);
                    $price = isset($post['price']) ? $post['price'] : 0;
                    $pageID = isset($post['page_id']) ? $post['page_id'] : '';
                    $goodsID = 'LP' . $pageID;
                    $res['script'] = "
                        var scriptEl = document.getElementById('pap_x2s6df8d');
                        if (scriptEl == null) {
                            function scriptYeah1OfferLoaded(accountId, cost, orderID, productID) {
                                PostAffTracker.setAccountId(accountId);
                                var sale = PostAffTracker.createSale();
                                sale.setTotalCost(cost);
                                sale.setOrderID(orderID);
                                sale.setProductID(productID);
                                PostAffTracker.register();
                            }
                            scriptEl = document.createElement('script');
                            scriptEl.setAttribute('type', 'text/javascript');
                            scriptEl.setAttribute('id', 'pap_x2s6df8d');
                            document.body.appendChild(scriptEl);
                            scriptEl.setAttribute('src', 'https://go.yeah1offer.com/scripts/trackjs.js');
                            scriptEl.addEventListener('load', function() {
                                scriptYeah1OfferLoaded('{$publisher[strtoupper('yeah1offer')]}', '{$price}', '{$identifier}', '{$goodsID}');
                            });
                        } else {
                            if (typeof scriptYeah1OfferLoaded == 'function') {
                                scriptYeah1OfferLoaded('{$publisher[strtoupper('yeah1offer')]}', '{$price}', '{$identifier}', '{$goodsID}');
                            }
                        }
                    ";
                }
            }
        }
        Yii::$app->response->format = Response::FORMAT_JSON;
        return $res;
    }

    public function actionSubmitPublisherMasoffer()
    {
        $cookies = Yii::$app->request->cookies;
        $affiliate_id = $cookies->getValue('affiliate_id');

        if (Yii::$app->request->isPost) {

            $utm_source = $cookies->getValue('utm_source');
            $traffic_id = $cookies->getValue('traffic_id');


            $post = Yii::$app->request->post();
            $publisher = isset($post['publisher']) ? $post['publisher'] : [];
            $course_id = isset($post['course_id']) ? (int)$post['course_id'] : 0;
            $combo_id = isset($post['combo_id']) ? (int)$post['combo_id'] : 0;
            $cod_money = 0;


            if ($course_id > 0) {
                // Tính Cod money
                $course = Course::findOne($course_id);
                $cod_money = $course->sellPrice;
            } else if ($combo_id > 0) {
                // Tính Cod money
                $course = Course::findOne($combo_id);
                $combo_items = $course->comboItems;
                if ($combo_items) {
                    foreach ($combo_items as $key => $course) {
                        $cod_money += $course->price;
                    }
                }
            }

            $email = isset($post['email']) ? $post['email'] : '';
            $phonenumber = isset($post['phonenumber']) ? $post['phonenumber'] : '';
            $fullname = isset($post['fullname']) ? $post['fullname'] : '';
            $page_slug = isset($post['page_slug']) ? $post['page_slug'] : '';

            if (isset($utm_source) && isset($traffic_id) && (int)$affiliate_id == 0 && isset($publisher[strtoupper('masoffer')]) && $publisher[strtoupper('masoffer')] === 'true') {


                $token = 'E77w*E=n4xm^Tk6b';
                $transaction_id = 'KYNA' . $this->generateCode(10);
                $offer_id = 'kyna';
                $transaction_time = strtotime(date('Y/m/d H:i:s')) * 1000;

                $product_id = 0;
                if ($course_id > 0)
                    $product_id = $course_id;
                else if ($combo_id > 0) {
                    $product_id = $combo_id;
                }
                $mo_data = [
                    'transaction_id' => $transaction_id,
                    'offer_id' => $offer_id,
                    'transaction_time' => $transaction_time,
                    'signature' => md5($token . $offer_id . $transaction_id . $transaction_time),
                    'traffic_id' => $traffic_id,
                    'products' => [
                        'id' => $product_id,
                        'url' => $_SERVER["HTTP_REFERER"],
                        'price' => (isset($cod_money) ? $cod_money : 0),
                        'name' => $page_slug,
                        'status_code' => 0,
                        'quantity' => 1
                    ],
                    'form' => [
                        'name' => $fullname,
                        'phone' => $phonenumber,
                        'email' => $email,
                        'content' => ''
                    ]
                ];
                $data_string = Json::encode($mo_data);

                Yii::$app->session->setFlash('mo_data', $data_string);

                $res = ['success' => true];
                echo Json::encode($res);
            } else {
                $res = ['success' => null];
                echo Json::encode($res);
            }
        }
    }

    public function generateCode($size = 8)
    {
        $text = date('YmdHis') . "ABCDEFGHIJKLMNPQRSTUVWXYZ" . rand(10000, 9999999999);
        $text = str_replace('0', rand(1, 9), $text);
        $len = strlen($text) - 1;
        $res = '';
        for ($i = 0; $i < $size; $i++)
            $res .= $text[rand(0, $len)];

        return $res;
    }

    /* Dành cho đối tác Adpia */

    public function actionAdpia()
    {

        $a_id = '';
        $m_id = '';
        $p_id = '';
        $l_id = '';
        $l_cd1 = '';
        $l_cd2 = '';
        $rd = '';
        $url = '';
        if (!get_cfg_var("register_globals")) {
            //print_r($_GET);echo "<br/>";
            $a_id = isset($_GET["a_id"]) ? $_GET["a_id"] : '';
            $m_id = isset($_GET["m_id"]) ? $_GET["m_id"] : '';
            $p_id = isset($_GET["p_id"]) ? $_GET["p_id"] : '';
            $l_id = isset($_GET["l_id"]) ? $_GET["l_id"] : '';
            $l_cd1 = isset($_GET["l_cd1"]) ? $_GET["l_cd1"] : '';
            $l_cd2 = isset($_GET["l_cd2"]) ? $_GET["l_cd2"] : '';
            $rd = isset($_GET["rd"]) ? $_GET["rd"] : '';
            $url = isset($_GET["url"]) ? $_GET["url"] : '';
        }
        //echo $a_id."---".$m_id."---".$p_id."---".$l_id."---".$l_cd1."---".$l_cd2."---".$rd."---".$url;
        //exit;
        if (strlen($p_id) == 0) {
            $ctime = floor(time() / 60);
            $new_cseq = rand(0, 99);
            $p_id = $ctime . "FFFF" . sprintf("%02X", $new_cseq);
        }

        if ($a_id == "" || $m_id == "" || $p_id == "" || $l_id == "" || $l_cd1 == "" || $l_cd2 == "" || $rd == "" || $url == "") {
            echo "<html><head><script type=\"text/javascript\">
	alert('APMS: You cannot connect site. Please contact Adpia');
	history.go(-1);
</script></head></html>";
            exit;
        }

        Header("P3P:CP=\"NOI DEVa TAIa OUR BUS UNI\"");

        $cookies = Yii::$app->response->cookies;

        if ($rd == 0) {
            $cookies->add(new \yii\web\Cookie([
                'name' => 'APINFO',
                'value' => $a_id . "|" . $p_id . "|" . $l_id . "|" . $l_cd1 . "|" . $l_cd2,
                'expire' => 0,
                'path' => '/',
                'domain' => '.' . Yii::$app->request->serverName
            ]));
        } else {
            // add a new cookie to the response to be sent
            $cookies->add(new \yii\web\Cookie([
                'name' => 'APINFO',
                'value' => $a_id . "|" . $p_id . "|" . $l_id . "|" . $l_cd1 . "|" . $l_cd2,
                'expire' => time() + ($rd * 24 * 60 * 60),
                'path' => '/',
                'domain' => '.' . Yii::$app->request->serverName
            ]));
        }


        $this->redirect($url);
    }

    private function _verifyReCaptcha($post, $key = 'lp_client_session')
    {
        $enableCaptcha = CaptchaHelper::getIsEnable($key);
        if ($enableCaptcha) {
            $response = CaptchaHelper::verifyReCaptcha($post);
            if ($response == false) {
                // render popup re-captcha
                $result['status'] = 0;
                $result['recaptcha'] = 1;
                $result['html'] = $this->renderPartial('@app/views/pages/recaptcha-modal', [
                    'data' => $post
                ]);
                echo Json::encode($result);
                Yii::$app->end();
            }
        }
    }

    public function createOrder($model, $paymentType, $slug, $email = null)
    {

        $orderDetails = [];
        $courseIDs = !empty($model->course_id) ? [$model->course_id] : explode(',', $model->list_course_ids);

        $products = Product::find()->where(['id' => $courseIDs])->indexBy('id')->all();
        $totalSingleCourseAmount = 0;
        foreach ($courseIDs as $courseID) {
            $course = Course::findOne($courseID);
            if (!is_null($course)) {
                if ($course->type == Product::TYPE_COMBO) {
                    foreach ($course->comboItems as $comboItem) {
                        $pItem = Product::findOne($comboItem->course_id);
                        $discount = $pItem->price - $comboItem->price;
                        $pItem->setDiscountAmount($discount);
                        $pItem->setCombo($course);
                        $orderDetails[] = $pItem;
                    }
                } else {
                    $pItem = Product::findOne($course->id);
                    $orderDetails[] = $pItem;
                    $totalSingleCourseAmount += $pItem->oldPrice - $pItem->originDiscount;
                }
            }
        }
        $promotionCode = null;
        if(CampaignTet::InTimesCampaign()){
            $promotionCode = CampaignTet::voucherLixTet($totalSingleCourseAmount);
            if(!empty($promotionCode)){
                $result = $this->applyPromotionCode($products, $promotionCode, $email);

                $products = $result['courses'];
                $promotionCode = $result['promotion_code'];
                $orderDetails = $this->prepareOrderItems($products);
            }
        }


        if (!empty($orderDetails)) {
            $order = Order::createEmpty(Order::SCENARIO_LANDING_PAGE);
            $page = Page::findOne(['slug' => $slug]);
            if(!empty($promotionCode)){
                $order->setPromotion($promotionCode);
            }
            $order->status = Order::ORDER_STATUS_PENDING_CONTACT;

            $order->point_of_sale = Order::POINT_OF_SALE_LANDING_PAGE;

            $order->payment_method = $paymentType;
            $order->page_id = $page ? $page->id : 0;

            $order->affiliate_id = $model->affiliate_id;
            $order->operator_id = $model->tel_id;
            if($order->payment_method == "bank-transfer"){
                $order->is_done_telesale_process = Order::BOOL_NO;
            }else {
                $order->is_done_telesale_process = Order::BOOL_YES;
            }
            $user = $this->findUser($model);

            $order->user_id = $user !== false ? $user->id : null;

            $order->addDetails($orderDetails);

            if ($order->save()) {
                return $order;
            }
        }
        return false;
    }

    public function findUser($userTelesale)
    {
        $user = User::find()->where(['email' => $userTelesale->email])->one();

        if (!$user) {
            $user = new User([
                'scenario' => 'create',
            ]);
            $user->load(['User' => [
                'email' => $userTelesale->email,
                'username' => $userTelesale->email,
            ]]);
            // update meta fields
            $user = $this->updateMeta($user, $userTelesale);
            if ($user->create(false)) {
                // trigger job send welcome email
                $user->sendWelcomeEmailConsole();
                // register profile
                $this->afterCreateUser($user, $userTelesale);
            }
        }

        return $user;
    }

    /**
     * Update meta fields
     * @param $user
     * @return mixed
     */
    public function updateMeta($user, $userTelesale)
    {
        if ($userTelesale) {
            $user->phone = $userTelesale->phone_number;
            $user->address = $userTelesale->street_address;
            $user->location_id = $userTelesale->location_id;
        }
        return $user;
    }

    /**
     * Update profile if Telesale user
     * @param $user
     */
    public function afterCreateUser($user, $userTelesale)
    {
        if ($userTelesale) {
            $profile = $user->profile;
            $profile->public_email = $userTelesale->email;
            $profile->name = $userTelesale->full_name;
            $profile->phone_number = $userTelesale->phone_number;
            $profile->location = $userTelesale->location_id;
            $profile->save(false);
        }
    }

    public function actionGetFlashsaleNumber()
    {
        $this->calFlashSaleNumber();
        $number = Yii::$app->cache->get(self::FLASHSALE_NUMBER_KEY);
        //$res= [];
        $res['result'] = $number;
        echo Json::encode($res);

    }

    private function calFlashSaleNumber()
    {
        $intervals_time = Yii::$app->params['intervals_time'];
        $flash_sale_interval_increase = Yii::$app->params['flashsale_interval_increase'];
        $flash_sale_increase_number = Yii::$app->params['flashsale_increase_number'];
        $lastUpdate = Yii::$app->cache->get(self::FLASHSALE_LASTUPDATE_KEY);
        $now = new \DateTime('now');
        if(empty($lastUpdate) ) {
            Yii::$app->cache->set(self::FLASHSALE_NUMBER_KEY,0, 0);
            Yii::$app->cache->set(self::FLASHSALE_LASTUPDATE_KEY, $now, 0 );

        } else {
            $isInFlashSale = false;
            $index  = 0;
            foreach ($intervals_time as $interval) {
                $startHour = $interval[0];
                $endHour = $interval[1];
                // var_dump($startHour, $endHour); exit;
                if (DateTimeHelper::isWithinTimeRange($startHour, $endHour) == true) {
                    $intervals_time = $now->getTimestamp() - $lastUpdate->getTimestamp();
                    if ($intervals_time >= $flash_sale_interval_increase) {
                        $increase_number = 0;
                        for ($x = 0; $x <= $intervals_time / $flash_sale_interval_increase; $x++) {
                            $increase_number = $increase_number + rand(1, $flash_sale_increase_number);
                        }
                        $current_number = Yii::$app->cache->get(self::FLASHSALE_NUMBER_KEY);
                        Yii::$app->cache->set(self::FLASHSALE_NUMBER_KEY,$current_number + $increase_number, 0);
                        Yii::$app->cache->set(self::FLASHSALE_LASTUPDATE_KEY, $now, 0 );
                    }
                    $isInFlashSale = true;
                    break;

                }
            }
            if($isInFlashSale == false) {
                Yii::$app->cache->set(self::FLASHSALE_NUMBER_KEY,0, 0);
                Yii::$app->cache->set(self::FLASHSALE_LASTUPDATE_KEY, $now, 0 );
            }


        }

    }

    public function applyPromotionCode($products, $promotion_code, $user_email)
    {

        $result = array('courses' => $products, 'promotion_code' => '', 'errors'=> '');
        $promoForm = new PromoCodeForm();
        $promoForm->user_email = $user_email;
        $promoForm->products = $products;
        $promoForm->code = $promotion_code;
        if ($promoForm->validate()) {
            //tính tổng giá trị đơn hàng được apply vourcher
            $total_order_can_apply = 0;
            $listCourseCanApply = $promoForm->getCoursesCanUseCode();

            list($code,$userId) = UserVoucherFree::isUserVoucherFree($promotion_code);
            if(!empty($code) && !empty($userId)){
                $listCourseCanApply = UserVoucherFree::getListMaxPriceProducts($products,$listCourseCanApply);
            }

            foreach ($products as $course) {
                if (in_array($course->id, $listCourseCanApply)) {
                    $total_order_can_apply = $total_order_can_apply + $course->oldPrice - $course->originDiscount;
                }
            }

            // tổng giá trị vourcher được giảm
            $total_vourcher_value = 0;

            // case %
            if ($promoForm->getDiscountType() == Promotion::TYPE_PERCENTAGE) {
                $total_vourcher_value = $promoForm->getPercentagePrice($promoForm->percentage, $total_order_can_apply);
            } else {
                $total_vourcher_value = $promoForm->getDiscountPrice();
            }
            // case fix

            $voucherValueDiscount = 0;
            $final_products = [];
            if ($promoForm->_promotion->type == Promotion::KIND_COURSE_APPLY) {
                foreach ($products as $course) {
                    if (in_array($course->id, $listCourseCanApply)) {
                        $price_after_original_discount = $course->oldPrice - $course->originDiscount;
                        if ($promoForm->getDiscountType() == Promotion::TYPE_FEE) // giá cố định
                        {
                            $voucherValueDiscount = $price_after_original_discount > $total_vourcher_value ? $total_vourcher_value : $price_after_original_discount;
                        } else { //giảm phần trăm
                            $voucherValueDiscount = $promoForm->getPercentagePrice($promoForm->getPercentage(), $price_after_original_discount);
                        }
                        $course->voucherDiscountAmount = $voucherValueDiscount;
                        $course->setPromotionCode($promoForm->code);
                    }
                    $final_products[] = $course;
                }
            } else {
                foreach ($products as $course) {
                    $price_after_original_discount = $course->oldPrice - $course->originDiscount;
                    if ($total_vourcher_value >= $total_order_can_apply) {
                        $voucherValueDiscount = $course->oldPrice - $course->discountAmount;
                    } else {
                        $voucherValueDiscount = $total_vourcher_value * ($price_after_original_discount / $total_order_can_apply);
                    }
                    if (in_array($course->id, $listCourseCanApply)) {
                        $course->voucherDiscountAmount = $voucherValueDiscount;
                        $course->setPromotionCode($promoForm->code);
                    }
                    $final_products[] = $course;
                }
            }
            $result['courses'] = $final_products;
            $result['promotion_code'] = $promotion_code;
        }else {
            $result['errors'] = $promoForm->getErrors();
        }
        return $result;
    }

    protected function prepareOrderItems($products)
    {


        $addItems = [];
        $checkCampaign = 0;
        $prevCampaign = null;
        /* @var $course Course */
        foreach ($products as $course) {

            if ($course->type == Course::TYPE_COMBO) {

                $total_combo_price =  $course->getOriginalPrice() - $course->discountAmount ; //tông gia combo da giam chua ap dung
                $comboItems = $course->comboItems;
                foreach ($comboItems as $item) {
                    if (!array_key_exists($item->course_id, $addItems)) {
                        $pItem = Product::findOne($item->course_id);
                        $pItem->setCombo($course);
                        $discountAmount = $pItem->price - $item->price;
                        if($total_combo_price > 0) {
                            $pItem->setVoucherDiscountAmount($course->voucherDiscountAmount * ($item->price / $total_combo_price));
                        }
                        else{
                            $pItem->setVoucherDiscountAmount(0);
                        }
                        $pItem->setDiscountAmount($discountAmount + $pItem->getVoucherDiscountAmount());

                        $pItem->setPromotionCode($course->getPromotionCode());
                        $addItems[$pItem->id] = $pItem;
                    }
                }
            } else {

                $new_item = clone $course;
                $new_item->discountAmount = $course->discountAmount + $course->voucherDiscountAmount;
                $addItems[$course->id] = $new_item;
            }
        }

        return $addItems;
    }
}
