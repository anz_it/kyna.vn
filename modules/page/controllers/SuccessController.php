<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace kyna\page\controllers;

use Yii;
use yii\web\NotFoundHttpException;
use kyna\page\models\Page;
use common\helpers\TreeHelper;
use kyna\base\models\Location;
use kyna\user\models\UserTelesale;
use kyna\course\models\Course;
use kyna\user\models\TimeSlot;
use yii\helpers\Json;

class SuccessController extends \app\components\Controller {

    public $layout = false; //'@app/views/layouts/landing_page/landing_page_main';
    public $viewPath;

    public function init() {
        parent::init();
        $this->setViewPath($this->viewPath);
    }
    public function actionIndex(){
        
       return $this->render('success');
    }
}