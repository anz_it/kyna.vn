<?php

namespace kyna\page\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use kyna\page\models\Page;

/**
 * PageSearch represents the model behind the search form about `kyna\page\models\Page`.
 */
class PageSearch extends Page
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'is_payment'], 'integer'],
            [['title', 'slug', 'external_script', 'success_script'], 'safe'],
            [['status'], 'integer'],
            [['id', 'title', 'slug'], 'trim'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Page::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'status' => $this->status,
            'is_payment' => $this->is_payment
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'slug', $this->slug])
            ->andFilterWhere(['like', 'external_script', $this->external_script])
            ->andFilterWhere(['like', 'success_script', $this->success_script]);

        return $dataProvider;
    }
}
