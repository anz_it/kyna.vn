<?php

namespace kyna\page\models;

use kyna\order\models\Order;
use kyna\order\models\OrderDetails;
use kyna\payment\models\PaymentMethod;
use kyna\user\models\User;
use kyna\user\models\UserTelesale;
use Yii;
use yii\base\Exception;
use yii\helpers\Url;

/**
 * This is the model class for table "page".
 *
 * @property integer $id
 * @property string $title
 * @property string $slug
 * @property string $external_script
 * @property string $success_script
 * @property integer status
 * @property integer is_payment
 * @property integer created_time
 * @property integer updated_time
 * @property integer seo_is_sitemap
 * @property integer is_redirect
 * @property string $redirect_url
 * @property string $content
 */
class Page extends \kyna\base\ActiveRecord
{
    const PAYMENT_TYPE_COD = 'cod';
    const PAYMENT_TYPE_ONEPAY_CC = 'onepay_cc';
    const PAYMENT_TYPE_ONEPAY_ATM = 'onepay_atm';
    const PAYMENT_TYPE_BANK_TRANSFER = 'bank-transfer';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%pages}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['external_script', 'success_script', 'content'], 'string'],
            [['title', 'slug'], 'required'],
            [['title', 'slug'], 'string', 'max' => 255],
            ['status', 'default', 'value' => 1],
            [['created_time', 'updated_time', 'is_payment', 'seo_is_sitemap', 'is_redirect'], 'integer'],
            [['redirect_url'], 'url'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Tiêu đề trang',
            'slug' => 'Slug',
            'status' => 'Trạng thái',
            'external_script' => 'Script đăng ký',
            'success_script' => 'Script thành công',
            'created_time' => 'Ngày tạo',
            'updated_time' => 'Ngày cập nhật',
            'is_payment' => 'Thanh toán',
            'seo_is_sitemap' => 'Add to sitemap',
            'is_redirect' => 'Redirect to Checkout',
            'redirect_url' => 'Redirect URL',
            'content' => 'Nội dung HTML'
        ];
    }

    public function getUrl($affiliate_id = false)
    {

        $slug = basename($this->slug);
        $dir = dirname($this->slug);
        if (!$dir OR $dir === '.' OR $dir === '/' OR $dir === '/p') {
            $arr = ['/page/default/index', 'slug' => $slug];
        } else {
            $arr = ['/page/default/index', 'slug' => $slug, 'dir' => $dir];
        }

        if ($affiliate_id)
            $arr['affiliate_id'] = $affiliate_id;
        return \yii\helpers\Url::toRoute($arr);
    }

    public static function getUserTelesale($email, $userType, $adviceName)
    {
        return UserTelesale::find()->where([
            'email' => $email,
            'type' => $userType,
            'form_name' => $adviceName,
            'user_id' => null
        ])->one();
    }

    public static function getIsExistOrder($email, $courseIDs)
    {
        $order = Order::find()
            ->alias('o')
            ->join('INNER JOIN', User::tableName(), User::tableName() . '.id = o.user_id')
            ->join('INNER JOIN', OrderDetails::tableName(), OrderDetails::tableName() . '.order_id = o.id')
            ->where([
                User::tableName() . '.email' => $email
            ])
            ->andFilterWhere(['OR',
                [OrderDetails::tableName() . '.course_id' => $courseIDs],
                [OrderDetails::tableName() . '.course_combo_id' => $courseIDs]
            ])
            ->andFilterWhere(['NOT IN', 'o.status', [
                Order::ORDER_STATUS_CANCELLED,
                Order::ORDER_STATUS_RETURN,
                Order::ORDER_STATUS_PAYMENT_FAILED,
                Order::ORDER_STATUS_NEW
            ]])
            ->exists();
        if ($order) {
            return true;
        }
        return false;
    }

    public function getIsPayment()
    {
        return ($this->is_payment == Page::BOOL_YES) ? true : false;
    }

    public static function canPayment($slug)
    {
        $page = self::findOne(['slug' => $slug]);
        return $page ? $page->isPayment : false;
    }

    public static function doPayment(Order $order)
    {
        $result = [
            'status' => false,
            'msg' => "",
            'redirectUrl' => ''
        ];
        $paymentMethodModel = PaymentMethod::loadModel($order->payment_method);
        if (!empty($paymentMethodModel) && $paymentMethodModel->isPayable() && $paymentMethodModel->payment_type === PaymentMethod::PAYMENT_TYPE_REDIRECT) {
            $paymentMethodModel->returnUrl = Url::toRoute(['/page/api/response', 'method' => $order->payment_method], true);
            try {
                $response = $paymentMethodModel->pay($order);
                $result['status'] = true;
                $result['redirectUrl'] = $response;
            } catch (Exception $e) {
                $result['msg'] = 'Quá trình gởi dữ liệu về hệ thống bị lỗi. Vui lòng thử lại sau. <br> Hoặc có thể liên hệ với Kyna qua khung chat bên dưới hoặc thông tin hỡ trợ';
            }
        }else if(!empty($paymentMethodModel) &&
            in_array($order->payment_method, [Page::PAYMENT_TYPE_BANK_TRANSFER])){
            $result['status'] = true;
        }
        else {
            $result['msg'] = "Phương thức thanh toán {$order->payment_method} không khả dụng";
        }
        return $result;
    }
}
