<?php

namespace kyna\api\models;

use Yii;
use dektrium\user\helpers\Password;

/**
 * This is the model class for table "{{%api_credentials}}".
 *
 * @property integer $id
 * @property string $username
 * @property string $password_hash
 * @property string $auth_key
 * @property string $access_token
 * @property integer $status
 * @property integer $created_time
 * @property integer $updated_time
 */
class ApiCredential extends \kyna\base\ActiveRecord implements \yii\web\IdentityInterface
{

    public $password;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%api_credentials}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['password'], 'required', 'on' => 'create'],
            ['password', 'string', 'min' => 6, 'max' => 72, 'on' => 'create'],
            [['username'], 'required'],
            [['auth_key', 'access_token'], 'string'],
            [['status', 'created_time', 'updated_time'], 'integer'],
            [['username', 'password_hash'], 'string', 'max' => 255],
            [['username'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Username',
            'password_hash' => 'Password Hash',
            'auth_key' => 'Auth Key',
            'access_token' => 'Access Token',
            'status' => 'Status',
            'created_time' => 'Created Time',
            'updated_time' => 'Updated Time',
        ];
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return self::findOne($id);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return self::findOne(['access_token' => $token]);
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return self::findOne(['username' => $username]);
    }

    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }

    public function beforeSave($insert)
    {
        $ret = parent::beforeSave($insert);

        if (!empty($this->password)) {
            $this->setAttribute('password_hash', Yii::$app->security->generatePasswordHash($this->password));
        }

        return $ret;
    }
}
