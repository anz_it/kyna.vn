<?php

namespace kyna\notification\models;
use common\helpers\CDNHelper;

use common\helpers\NotificationHelper;
use kyna\course\models\Category;
use kyna\course\models\Course;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "notification".
 * @property string $name
 *
 */
class Notification extends ActiveRecord
{
//    public $is_type;
    const IS_MOBILE = 1;
    const IS_DESKTOP = 2;
    public $imageSize = [
        'cover' => [
            CDNHelper::IMG_SIZE_THUMBNAIL,
            CDNHelper::IMG_SIZE_THUMBNAIL_SMALL
        ],
        'contain' => [],
        'crop' => [
            CDNHelper::IMG_SIZE_THUMBNAIL_YOUTUBE,
            CDNHelper::IMG_SIZE_THUMBNAIL_YOUTUBE_LARGE
        ]
    ];

    const TOPIC_STAGING = 'staging-topic1';
    const TOPIC_LIVE = 'kyna_mobile';


    public static function listTypeDevice(){
        return[
            self::IS_MOBILE=>'Mobile',
            self::IS_DESKTOP=>'Desktop'
        ];
    }

    public static function listTopic(){
        return[
            self::TOPIC_LIVE=>'Topic Production',
            self::TOPIC_STAGING=>'Topic Staging'
        ];
    }

    public static $screen_type_mobile =[
        1=>'Trang khóa học chi tiết',
        2=>'Trang Category',
        3=>'Trang Tag chi tiết',
        4=>'Trang Search',
        5=>'Webview',
    ];

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'notification';
    }

    public function rules()
    {
        return [
            ['name', 'string', 'max' => 256],
            ['title', 'string', 'max' => 512],
            ['body', 'string', 'max' => 1024],
            ['icon', 'image', 'skipOnEmpty' => true, 'mimeTypes' => 'image/png,image/jpeg'],
            ['image', 'image', 'skipOnEmpty' => true, 'mimeTypes' => 'image/png,image/jpeg'],
            ['redirect_url', 'url'],
            ['description', 'string', 'max' => 4096],
            [['is_type'], 'integer'],
            [['topic'], 'string'],
            [['name', 'title', 'body','screen_type_mobile'], 'required', 'message' => 'Thông tin này là bắt buộc'],
            [['screen_type_mobile'], 'integer'],
            [['data'], 'string'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'name' => 'Name',
            'title' => 'Title',
            'body' => 'Body',
            'icon' => 'Icon\'s URL',
            'image' => 'Image\'s URL',
            'redirect_url' => 'Redirect link',
            'description' => 'Description',
            'screen_type_mobile' => 'Màn hình hiển thị Mobile',
            'data' => 'Data',
            'is_type' => 'Select Device',
        ];
    }

    public static function getName($type,$data){
        $name = '';
        switch ($type){
            case NotificationHelper::COURSE :

                $course = Course::findOne($data);
                if(!empty($course)){
                    $name = $course->name;
                }
                break;
            case NotificationHelper::CATEGORY :
                $category = Category::findOne($data);
                if(!empty($category)){
                    $name = $category->name;
                }
                break;
            case NotificationHelper::TAG :
                $tag = \kyna\tag\models\Tag::findOne(['slug'=>$data]);
                if(!empty($tag)){
                    $name = $tag->tag;
                }
                break;
            case NotificationHelper::SEARCH :
                return $data;
                break;
            case NotificationHelper::URL :
                return $data;
                break;
        }
        return $name;
    }

}