<?php

namespace kyna\notification\models;

use yii\db\ActiveRecord;

class Schedule extends ActiveRecord
{
    public $start_time_formatted;

    protected function enableMeta()
    {
        return 'notification_schedule';
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'notification_schedule';
    }
    public function rules()
    {
//        return parent::rules();
        return [
            ['is_enabled', 'string', 'max' => 10],
            ['start_time_formatted', 'date', 'format'=>'yyyy-M-d H:m'],
            ['type', 'string', 'max' => 10],

            ['setting', 'string', 'max' => 4096],
            ['adv_setting', 'string', 'max' => 4096],
//

            [['is_enabled', 'start_time_formatted', 'type'], 'required', 'message' => 'Thông tin này là bắt buộc'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'enable' => 'Is Enable',
            'start_time_formatted' => 'Start Time',
            'repeat_type' => 'Repeat Type',

            'setting' => 'Setting',
            'adv_settings' => 'Advanced Settings'
        ];
    }
}