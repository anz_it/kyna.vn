<?php

namespace kyna\tracking\models;

use Yii;

/**
 * This is the model class for table "tracking_user_care".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $last_assign
 */
class TrackingUserCare extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tracking_user_care';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id'], 'required'],
            [['user_id', 'last_assign'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'last_assign' => 'Last Assign',
        ];
    }
}
