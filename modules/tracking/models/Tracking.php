<?php

namespace kyna\tracking\models;

use Yii;

/**
 * This is the model class for table "tracking".
 *
 * @property integer $id
 * @property string $number
 * @property string $piwik_id
 * @property integer $updated_time
 * @property string $piwik_visitor_id
 * @property integer $update_piwik_times
 * @property integer $number_called
 * @property integer $last_access
 * @property string $note
 * @property string $profile
 * @property integer $page_id
 * @property integer $user_id_care
 * @property integer $is_registered
 */
class Tracking extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tracking';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['updated_time'], 'required'],
            [['updated_time', 'update_piwik_times', 'number_called', 'last_access', 'page_id', 'user_id_care', 'is_registered'], 'integer'],
            [['note', 'profile'], 'string'],
            [['number', 'piwik_id', 'piwik_visitor_id'], 'string', 'max' => 50],
            [['number'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'number' => 'Số điện thoại',
            'piwik_id' => 'Piwik ID',
            'updated_time' => 'Cập nhật lúc',
            'piwik_visitor_id' => 'Piwik Visitor ID',
            'update_piwik_times' => 'Update Piwik Times',
            'number_called' => 'Đã gọi',
            'last_access' => 'Last Access',
            'note' => 'Note',
            'profile' => 'Profile',
            'page_id' => 'Page ID',
            'user_id_care' => 'User Id Care',
            'is_registered' => 'Is Registered',
        ];
    }
}
