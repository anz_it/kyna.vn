<?php

namespace kyna\tracking\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use kyna\tracking\models\Tracking;

/**
 * TrackingSearch represents the model behind the search form about `kyna\tracking\models\Tracking`.
 */
class TrackingSearch extends Tracking
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'updated_time', 'update_piwik_times', 'number_called', 'last_access', 'page_id', 'user_id_care', 'is_registered'], 'integer'],
            [['number', 'piwik_id', 'piwik_visitor_id', 'note', 'profile'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Tracking::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'updated_time' => $this->updated_time,
            'update_piwik_times' => $this->update_piwik_times,
            'number_called' => $this->number_called,
            'last_access' => $this->last_access,
            'page_id' => $this->page_id,
            'user_id_care' => $this->user_id_care,
            'is_registered' => $this->is_registered,
        ]);

        $query->andFilterWhere(['like', 'number', $this->number])
            ->andFilterWhere(['like', 'piwik_id', $this->piwik_id])
            ->andFilterWhere(['like', 'piwik_visitor_id', $this->piwik_visitor_id])
            ->andFilterWhere(['like', 'note', $this->note])
            ->andFilterWhere(['like', 'profile', $this->profile]);

        return $dataProvider;
    }
}
