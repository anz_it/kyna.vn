<?php
namespace app\modules\user\lib;

use Yii;
use common\widgets\upload\lib\Upload;

class AvatarUpload extends Upload {
    public $model;
    public $attribute;

    public $tableName;

    public function __construct($model, $attribute) {
        $this->model = $model;
        $this->attribute = $attribute;

        if (!$this->tableName) {
            $this->tableName = $model->tableSchema->name;
        }
    }

    public function getFileName($file) {
        return $this->attribute.'-'.time().'.'.$file->extension;
    }

    public function getUploadPath($relativeDir) {
        return Yii::getAlias($this->uploadRoot.'/'.$this->tableName.'/'.$this->model->id.'/img');
    }

    public function getUploadUrl($relativeDir) {
        return Yii::getAlias($this->uploadRootUrl.'/'.$this->tableName.'/'.$this->model->id.'/img');
    }
}
