<?php

namespace app\modules\user\models\forms;

use Yii;
use kyna\order\models\Order;

class OrderForm extends \yii\base\Model
{

    protected $_order;

    const SCENARIO_ACTIVE_COD = 'active_cod';

    public $activation_code;

    public function scenarios()
    {
        return [
            self::SCENARIO_ACTIVE_COD => ['activation_code']
        ];
    }

    public function rules()
    {
        return [
            [['activation_code'], 'string', 'on' => [self::SCENARIO_ACTIVE_COD]],
            [['activation_code'], 'required', 'on' => [self::SCENARIO_ACTIVE_COD]],
            [['activation_code'], 'checkExist', 'on' => [self::SCENARIO_ACTIVE_COD]]
        ];
    }

    public function attributeLabels()
    {
        return [
            'activation_code' => 'Nhập mã kích hoạt'
        ];
    }

    public function checkExist()
    {
        // check if activation code can use
        $result = $this->getOrder();
        if ($result == null) {
            $this->addError('activation_code', 'Mã kích hoạt không hợp lệ.');
        }
    }

    public function complete()
    {
        $ret = false;
        if (empty($this->_order)) {
            $this->_order = $this->getOrder();
        }
        if ($this->_order != null) {
            // active courses
            if (Order::activate($this->_order->id) !== false) {
                $ret = true;
            }
        }
        return $ret;
    }

    public function getOrder()
    {
        if (empty($this->_order)) {
            $this->_order = Order::find()->where(['activation_code' => $this->activation_code, 'user_id' => Yii::$app->user->id])->one();
        }
        return $this->_order;
    }

}