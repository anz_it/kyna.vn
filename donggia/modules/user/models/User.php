<?php

namespace app\modules\user\models;

use Yii;
use app\modules\user\components\Mailer;
use kyna\user\models\UserCourse;

class User extends \dektrium\user\models\User
{
    
    protected function getMailer()
    {
        return Yii::$container->get(Mailer::className());
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        if ($insert) {
            UserCourse::active($this->id, Yii::$app->params['beginLearningCourseId']);
        }
    }
}

