<?php

namespace app\modules\user\components;

use Yii;
use yii\filters\AccessControl;

/**
 * Base Controller for module User
 */
class Controller extends \app\components\Controller
{

    public $layout = '@app/modules/user/views/layouts/user';
    
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function getViewPath()
    {
        return Yii::getAlias('@app/modules/user/views/' . $this->id);
    }

    public function init()
    {
        if(Yii::$app->devicedetect->isMobile() || Yii::$app->devicedetect->isTablet()){
            $this->layout = '@app/modules/user/views/layouts/user_mobile';
        }
        return parent::init();
    }

}
