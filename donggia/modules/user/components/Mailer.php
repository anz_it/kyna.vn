<?php

namespace app\modules\user\components;

use Yii;
use dektrium\user\models\Token;
use dektrium\user\models\User;

class Mailer extends \dektrium\user\Mailer
{
    
    /**
     * @desc override dektrium Mailer sendWelcomeMessage function
     * @param User $user
     * @param Token $token
     * @param type $showPassword
     * @return type
     */
    public function sendWelcomeMessage(User $user, Token $token = null, $showPassword = false)
    {
        return $this->sendMessage(
            $user->email,
            'Chúc mừng bạn đã đăng ký tài khoản thành công tại Kyna.vn',
            '@common/mail/user/welcome',
            [
                'fullname' => !empty($user->profile->name) ? $user->profile->name : $user->email,
                'email' => $user->email,
                'settings' => Yii::$app->controller->settings
            ]
        );
    }
    
    public function sendRecoveryMessage(User $user, Token $token)
    {
        return $this->sendMessage(
            $user->email,
            $this->getRecoverySubject(),
            '@common/mail/user/recover_password',
            [
                'fullname' => !empty($user->profile->name) ? $user->profile->name : $user->email,
                'link' => $token->url,
                'settings' => Yii::$app->controller->settings
            ]
        );
    }
    
    protected function sendMessage($to, $subject, $view, $params = [])
    {
        $mailer = Yii::$app->mailer;
        $mailer->htmlLayout = '@common/mail/layouts/main';

        if ($this->sender === null) {
            $this->sender = isset(Yii::$app->params['adminEmail']) ?
                Yii::$app->params['adminEmail']
                : 'no-reply@example.com';
        }

        return $mailer->compose($view, $params)
            ->setTo($to)
            ->setFrom($this->sender)
            ->setSubject($subject)
            ->send();
    }
}
