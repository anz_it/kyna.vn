<?php

namespace app\modules\user\controllers;

use common\helpers\ResponseError;
use common\helpers\ResponseModel;
use kyna\course\models\CourseDocument;
use kyna\course\models\response\AnalyticResponseModel;
use kyna\course\models\search\CourseLearnerQnaSearch;
use kyna\user\models\UserCourse;
use Yii;
use kyna\user\models\search\UserCourseSearch;

/**
 * CourseController
 */
class CourseController extends \app\modules\user\components\Controller
{
 
    public function actionIndex()
    {
        $searchModel = new UserCourseSearch();
        $searchModel->user_id = Yii::$app->user->id;
        $searchModel->is_activated = UserCourseSearch::BOOL_YES;
        $searchModel->is_started = UserCourse::BOOL_NO;
        
        if (Yii::$app->request->isAjax) {
            return $this->renderPartial('index', [
                'dataProvider' => $searchModel
            ]);
        }
        
        return $this->render('index', [
            'dataProvider' => $searchModel
        ]);
    }

    public function actionStarted()
    {
        $searchModel = new UserCourseSearch();
        $searchModel->user_id = Yii::$app->user->id;
        $searchModel->is_activated = UserCourseSearch::BOOL_YES;
        $searchModel->is_started = UserCourse::BOOL_YES;

        if (Yii::$app->request->isAjax) {
            return $this->renderPartial('started', [
                'dataProvider' => $searchModel
            ]);
        }

        return $this->render('started', [
            'dataProvider' => $searchModel
        ]);
    }

    public function actionAnalytic()
    {
        if(Yii::$app->request->isAjax){

            $resp = new AnalyticResponseModel();

            $courseIds = UserCourse::find()->joinWith('course')->where(['user_id' => Yii::$app->user->id, 'is_activated' => UserCourseSearch::BOOL_YES])->select('course_id')->column();

            $resp->course_count = count($courseIds);

            $question = new CourseLearnerQnaSearch();

            $question->user_id = Yii::$app->user->id;

            $resp->question_count = $question->search(null)->totalCount;

            $resp->document_count = CourseDocument::find()->where(['course_id' => $courseIds])->count();

            return ResponseModel::responseJson($resp);
        }

        return ResponseModel::responseJson(null, 200, 'Http request is not allowed');
    }
}