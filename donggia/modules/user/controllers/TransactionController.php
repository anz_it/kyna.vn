<?php

namespace app\modules\user\controllers;

use Yii;
use kyna\order\models\search\OrderSearch;

class TransactionController extends \app\modules\user\components\Controller
{
    


    public function actionComplete()
    {
        $searchModel = new OrderSearch();

        $searchModel->user_id = Yii::$app->user->id;

        $searchModel->status = OrderSearch::ORDER_STATUS_COMPLETE;

        if (Yii::$app->request->isAjax) {
            return $this->renderPartial('complete',[
                'searchModel' => $searchModel
            ]);
        }

        return $this->render('complete', [
            'searchModel'  => $searchModel
        ]);
    }

    public function actionInComplete()
    {
        $searchModel = new OrderSearch();

        $searchModel->user_id = Yii::$app->user->id;

        $searchModel->status = null;

        if (Yii::$app->request->isAjax) {
            return $this->renderPartial('in_complete',[
                'searchModel' => $searchModel
            ]);
        }

        return $this->render('in_complete', [
            'searchModel'  => $searchModel
        ]);
    }
}
