<?php

namespace app\modules\user\controllers;

use app\modules\order\models\ActionForm;
use kyna\base\events\ActionEvent;
use app\models\Course;
use kyna\order\models\Order;
use kyna\order\models\traits\OrderTrait;
use yii;
use app\modules\user\models\forms\OrderForm;

class OrderController extends \app\modules\user\components\Controller
{
    use OrderTrait;
    public $_form;

    public $_model;

    /**
     * @return string
     */
    public function actionActiveCod()
    {
        $order_form = new OrderForm();
        $order_form->scenario = OrderForm::SCENARIO_ACTIVE_COD;

        if (Yii::$app->request->isPost) {

            $data = Yii::$app->request->post('OrderForm');

            if (!empty($data)) {
                $order_form->setAttributes($data);
                if ($order_form->validate()) {
                    $message = null;
                    if ($order_form->complete()) {
                        $message = 'Bạn đã kích hoạt thành công.';
                    } else {
                        $message = 'Mã kích hoạt không hợp lệ.';
                    }

                    return $this->renderAjax('active_cod', ['message' => $message]);
                }
            }
        }

        return $this->renderAjax('active_cod', [
            'order_form' => $order_form
        ]);
    }
}
