<?php

namespace app\modules\user\controllers;

use Yii;
use app\modules\user\models\forms\RecoveryForm;
use dektrium\user\models\Token;
use app\models\Category;
use yii\web\View;
use yii\helpers\ArrayHelper;
use kyna\settings\models\Setting;

class RecoveryController extends \dektrium\user\controllers\RecoveryController
{

    public $rootCats = [];
    public $settings = [];
    public $bodyClass = 'recover-body';
    public $layout = '/one_column';

    public function init()
    {
        $ret = parent::init();

        $this->rootCats = Category::getList(0, ['id', 'name', 'slug']);
        $this->settings = ArrayHelper::map(Setting::find()->all(), 'key', 'value');

        if (!empty($this->settings['js-head'])) {
            $js = $this->settings['js-head'];
            $this->view->registerJs($js, View::POS_HEAD);
        }
        if (!empty($this->settings['js-foot'])) {
            $js = $this->settings['js-foot'];
            $this->view->registerJs($js, View::POS_FOOT);
        }

        return $ret;
    }

    /**
     * Shows page where user can request password recovery.
     *
     * @return string
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionRequest()
    {
        $flag = false;
        $renderFunction = Yii::$app->request->isAjax ? 'renderPartial' : 'render';
        if (!Yii::$app->request->isAjax) {
            $flag = true;
        }

        /** @var RecoveryForm $model */
        $model = Yii::createObject([
            'class'    => RecoveryForm::className(),
            'scenario' => RecoveryForm::SCENARIO_REQUEST,
        ]);
        $event = $this->getFormEvent($model);

        $this->trigger(self::EVENT_BEFORE_REQUEST, $event);

        if ($model->load(Yii::$app->request->post()) && $model->sendRecoveryMessage()) {
            $this->trigger(self::EVENT_AFTER_REQUEST, $event);
        }

        return $this->$renderFunction('@app/modules/user/views/recovery/request', [
            'model' => $model,
            'flag' => $flag
        ]);
    }

    public function actionReset($id, $code)
    {
        /** @var Token $token */
        $token = $this->finder->findToken(['user_id' => $id, 'code' => $code, 'type' => Token::TYPE_RECOVERY])->one();
        $event = $this->getResetPasswordEvent($token);

        $this->trigger(self::EVENT_BEFORE_TOKEN_VALIDATE, $event);

        if ($token === null || $token->isExpired || $token->user === null) {
            $this->trigger(self::EVENT_AFTER_TOKEN_VALIDATE, $event);            
            Yii::$app->session->setFlash('error', '<p class="link-error">Link này đã hết hạn, vui lòng nhập lại email để lấy link khôi phục khác</p>');
            return $this->redirect(['request']);
        }

        /** @var RecoveryForm $model */
        $model = Yii::createObject([
            'class'    => RecoveryForm::className(),
            'scenario' => RecoveryForm::SCENARIO_RESET,
        ]);
        $event->setForm($model);

        $this->performAjaxValidation($model);
        $this->trigger(self::EVENT_BEFORE_RESET, $event);

        if ($model->load(Yii::$app->getRequest()->post()) && $model->resetPassword($token)) {
            $this->trigger(self::EVENT_AFTER_RESET, $event);
            Yii::$app->session->setFlash('success', 'Mật khẩu của bạn đã được cập nhật. Đăng nhập để tiếp tục học.');
            return $this->redirect(['/user/security/login']);
        }

        return $this->render('@app/modules/user/views/recovery/reset', [
            'model' => $model,
        ]);
    }

}
