<?php

namespace app\modules\user\controllers;

use dektrium\user\models\Profile;
use kyna\user\models\User;
use kyna\user\models\UserMeta;
use yii\helpers\Url;

/**
 * CourseController.
 */
class AuthController extends \app\components\Controller
{

    public function beforeAction($action)
    {
        \Yii::$app->session->open();
        return parent::beforeAction($action);
    }

    public function actionFb()
    {
        $fbUser = \Yii::$app->session->get('fbUser');
        if (!$fbUser) {
            $helper = \Yii::$app->facebook->getRedirectLoginHelper();
            try {
                // Returns a `Facebook\FacebookResponse` object
                $accessToken = $helper->getAccessToken();
                $response = \Yii::$app->facebook->get('/me?fields=id,name,email', $accessToken);
            } catch (\Facebook\Exceptions\FacebookSDKException $e) {
                echo 'Facebook SDK returned an error: ' . $e->getMessage();
                exit;
            }

            $fbUser = $response->getGraphUser();
        }

        if ($fbUser) {
            $redirectUrl = Url::toRoute([
                '/user/registration/register',
                'id' => $fbUser->getId(),
                'email' => $fbUser->getEmail(),
                'name' => $fbUser->getName(),
            ], true);
            return $this->redirect($redirectUrl);
        }

        //$this->goHome();
    }

    private function _matchingFb($fbId)
    {
        return User::find()->leftJoin(Profile::tableName() . ' p', User::tableName() . '.id = p.user_id')
            ->where([
                'p.facebook_id' => $fbId,
            ])->one();
    }

    private function _matchingEmail($email)
    {
        return User::find()->where(['email' => $email])->one();
    }

}
