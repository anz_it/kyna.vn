<?php

namespace app\modules\user\controllers;

use Yii;
use yii\helpers\ArrayHelper;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;

use dektrium\user\models\LoginForm;

use kyna\user\models\Token;
use kyna\user\models\User;
use kyna\user\models\UserCourse;
use kyna\settings\models\Setting;

use app\models\Category;


/*
 * Class SecurityController override dektrium Controller.
 */
class SecurityController extends \dektrium\user\controllers\SecurityController
{
    public $rootCats = [];
    public $settings = [];
    public $bodyClass = '';

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    ['allow' => true, 'actions' => ['login', 'auth', 'blocked', 'forgot-password', 'force-login'], 'roles' => ['?']],
                    ['allow' => true, 'actions' => ['login', 'auth', 'logout', 'force-login'], 'roles' => ['@']],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function init()
    {

        $ret = parent::init();

        $this->rootCats = Category::getList(0, ['id', 'name', 'slug']);
        $this->settings = ArrayHelper::map(Setting::find()->all(), 'key', 'value');

        $this->on(self::EVENT_AFTER_LOGIN, [$this, 'afterLogin']);
        //$this->on(self::EVENT_AFTER_LOGOUT, [$this, 'afterLogout']);

        return $ret;
    }

    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            $this->goHome();
        }

        /** @var LoginForm $model */
        $model = Yii::createObject(LoginForm::className());
        $event = $this->getFormEvent($model);

        $this->trigger(self::EVENT_BEFORE_LOGIN, $event);

        if ($model->load(Yii::$app->request->post())) {
            if ($model->login()) {
                $user = Yii::$app->user->identity;

                $user->auth_key = Yii::$app->security->generateRandomString();
                $user->save(false);

                $this->trigger(self::EVENT_AFTER_LOGIN, $event);

                $redirectUrl = Yii::$app->request->referrer;
                $this->redirect($redirectUrl);
            }

            if (Yii::$app->request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return [
                    'result' => false,
                    'errors' => $model->errors
                ];
            }
        }

        if(Yii::$app->request->isAjax) {
            return $this->renderAjax('@app/modules/user/views/security/login');
        }

        return $this->render('@app/modules/user/views/security/login');
    }

    public function afterLogin()
    {

    }

    public function actionForceLogin($user_id, $hash)
    {
        if (!Yii::$app->user->isGuest) {
            Yii::$app->user->logout();
        }

        $user = User::findOne($user_id);
        if (is_null($user) || $user->auth_key !== $hash) {
            throw new NotFoundHttpException();
        }

        if (Yii::$app->user->login($user)) {
            $redirectUrl = Yii::$app->request->referrer;
            return $this->redirect($redirectUrl);
        }
    }

    /**
     * Logs the user out and then redirects to the homepage.
     *
     * @return Response
     */
    public function actionLogout()
    {
        $event = $this->getUserEvent(\Yii::$app->user->identity);

        $this->trigger(self::EVENT_BEFORE_LOGOUT, $event);

        \Yii::$app->getUser()->logout();

        $this->trigger(self::EVENT_AFTER_LOGOUT, $event);

        $redirectUrl = Yii::$app->request->referrer;
        return $this->redirect($redirectUrl);
    }

    public function afterLogout() {

    }

}
