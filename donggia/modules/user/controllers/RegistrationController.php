<?php

namespace app\modules\user\controllers;

use Yii;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;

use dektrium\user\controllers\RegistrationController as BaseRegistrationController;

use kyna\user\models\User;
use kyna\user\models\Profile;
use kyna\settings\models\Setting;
use kyna\user\models\Token;

use app\models\Category;
use app\modules\user\models\forms\RegistrationForm;

use common\helpers\PhoneNumberHelper;

class RegistrationController extends BaseRegistrationController
{

    public $rootCats = [];
    public $settings = [];
    public $bodyClass = '';

    public function init()
    {
        parent::init();
        $this->rootCats = Category::getList(0, ['id', 'name', 'slug']);
        $this->settings = ArrayHelper::map(Setting::find()->all(), 'key', 'value');

        //$this->on(self::EVENT_BEFORE_REGISTER, [$this, 'beforeRegister']);
        $this->on(self::EVENT_AFTER_REGISTER, [$this, 'afterRegister']);
    }

    public function actionRegister()
    {
        //var_dump(PhoneNumberHelper::getCarrier('0977654321'));die;
        if (!$this->module->enableRegistration) {
            throw new NotFoundHttpException();
        }

        $renderFunction = Yii::$app->request->isAjax ? 'renderAjax' : 'render';

        /** @var RegistrationForm $model */
        $model = Yii::createObject(RegistrationForm::className());

        $fbId = Yii::$app->request->get('id');

        if (!empty($fbId)) {
            $model->fbId = $fbId;
            $model->name = Yii::$app->request->get('name');
            $model->email = Yii::$app->request->get('email');
        }

        $event = $this->getFormEvent($model);

        if(!empty($model) && !empty($fbId)){

            $this->trigger(self::EVENT_BEFORE_REGISTER, $event);
        }

        $this->performAjaxValidation($model);

        if ($model->load(Yii::$app->request->post()) && $model->register()) {
            $this->trigger(self::EVENT_AFTER_REGISTER, $event);

            return $this->$renderFunction('/message', [
                        'title' => Yii::t('user', 'Your account has been created'),
                        'module' => $this->module,
            ]);
        }

        return $this->$renderFunction('@app/modules/user/views/registration/register', [
            'model' => $model,
            'module' => $this->module,
        ]);
    }

    public function beforeAction($action)
    {
        if (!Yii::$app->user->isGuest) {
            return $this->redirect(['/user/course/index']);
        }

        if ($action->id === 'register' and ($fbId = Yii::$app->request->get('id'))) {
            $redirectUrl = Yii::$app->request->referrer ? Yii::$app->request->referrer : '/';
            if ($user = $this->_matchingFb($fbId)) {
                if (!Yii::$app->user->login($user)) {
                    Yii::$app->response->refresh();
                    return false;
                }
                $this->createToken($user);
                Yii::$app->response->redirect($redirectUrl);
                return false;
            }

            $email = Yii::$app->request->get('email');
            if ($user = $this->_matchingEmail($email)) {
                $profile = $user->profile;
                $profile->facebook_id = $fbId;
                $profile->save();

                if (!Yii::$app->user->login($user)) {
                    if(!empty($user->loginToken->code)) {
                        Yii::$app->session->destroySession($user->loginToken->code);
                    }

                    Yii::$app->response->refresh();
                    return false;
                }
                $this->createToken($user);
                Yii::$app->response->redirect($redirectUrl);
                return false;
            }
        }
        return parent::beforeAction($action);
    }

    private function createToken($user)
    {
        $token = new Token();
        $token->user_id = $user->id;
        $token->code = Yii::$app->session->id;
        $token->type = Token::TYPE_LOGIN;
        $token->save(false);
    }

    public function afterRegister($event)
    {
        $form = $event->form;

        if ($user = $this->_matchingEmail($form->email)) {
            $profile = $user->profile;
            $profile->name = $form->name;
            $profile->phone_number = $form->phonenumber;

            if (!empty($form->fbId)) {
                $profile->facebook_id = $form->fbId;
            }

            $profile->save(false);
            if (Yii::$app->user->login($user)) {
//                $this->goHome();
            }
        }
        $redirectUrl = Yii::$app->request->referrer;
        $this->redirect($redirectUrl);
    }

    private function _matchingFb($fbId)
    {
        return User::find()->leftJoin(Profile::tableName() . ' p', User::tableName() . '.id = p.user_id')
            ->where([
                'p.facebook_id' => $fbId,
            ])->one();
    }

    private function _matchingEmail($email)
    {
        return User::find()->where(['email' => $email])->one();
    }

}
