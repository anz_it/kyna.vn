<?php

namespace app\modules\user\controllers;

use Yii;

class MessageController extends \app\modules\user\components\Controller
{
    
    public function actionIndex()
    {
        if (Yii::$app->request->isAjax) {
            return $this->renderPartial('index.tpl');
            
            Yii::$app->end();
        }
        
        return $this->render('index.tpl');
    }
}
