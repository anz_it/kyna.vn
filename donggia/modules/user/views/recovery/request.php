<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use common\widgets\Alert;

$this->title = 'Quên mật khẩu';
$this->params['breadcrumbs'][] = $this->title;
?>
<?= ($flag === TRUE)?'<div id="k-recover-password">':''?>
<div class="modal-dialog" role="document">
    <div class="modal-content k-popup-account-mb-content">
        <div class="modal-header">
            <button type="button" class="k-popup-account-close close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
            <h4 class="modal-title">Lấy lại mật khẩu</h4>
        </div>
        <div class="modal-body clearfix">
            <ul class="k-popup-account-top">
                <li>
                </li>
                <li>Để lấy lại mật khẩu, bạn nhập email đăng nhập vào ô dưới đây. Sau đó Kyna.vn sẽ gửi email hướng dẫn bạn khôi phục mật khẩu</li>
            </ul>
            <?php
            $form = ActiveForm::begin([
                        'id' => 'password-recovery-form',
                        'enableAjaxValidation' => false,
                        'enableClientValidation' => false,
                        'action' => Url::toRoute(['/user/recovery/request']),
                        'options' => [
                            'data-ajax' => ($flag === TRUE)?false:true,
                            'data-target' => ($flag === TRUE)?'#k-recover-password':'#k-popup-account-reset'
                        ]
            ]);
            ?>
            <?= Alert::widget() ?>

            <?= $form->errorSummary($model, ['class' => 'error-summary alert alert-warning']) ?>

            <?=
            $form->field($model, 'email', [
                'template' => '{beginWrapper}<span class="icon icon-mail"></span>{input}' . PHP_EOL . '{error}{endWrapper}',
                'options' => ['class' => 'form-group'],
                'inputOptions' => [
                    'placeholder' => 'Email của bạn'
                ]
            ])->textInput(['autofocus' => true])->error(false)
            ?>
            <div class="button-submit">
                <?= Html::submitButton(Yii::t('user', 'Continue'), ['id' => 'btn-submit-forgot']) ?><br>
            </div>
            <?php ActiveForm::end(); ?>

            <a href="<?= Url::toRoute(['/user/security/login']) ?>" data-target="#k-popup-account-login" data-toggle="modal" data-ajax data-push-state="false" class="back-login">Quay lại đăng nhập</a>

            <ul class="k-popup-account-bottom">
                <li>Nếu bạn chưa có tài khoản</li>
                <li><a href="<?= Url::toRoute(['/user/registration/register']) ?>" data-target="#k-popup-account-register" data-toggle="modal" data-ajax data-push-state="false">Đăng ký</a></li>
            </ul>
        </div><!--end modal-body-->
    </div>
</div>
<?= ($flag === TRUE)?'</div>':''?>