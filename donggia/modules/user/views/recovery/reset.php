<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use common\widgets\Alert;

$this->title = 'Phục hồi mật khẩu';
$this->params['breadcrumbs'][] = $this->title;
?>
<!-- Css temp for reset password screen -->
<div class="container">
    <div class="row" style="margin-top: 100px;">
        <div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3" id="wrapper-password-recovery-form">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title password-recovery-title"><?= Html::encode($this->title) ?></h3>
                </div>
                <div class="panel-body">
                    <?= Alert::widget() ?>
                    <?php if (!empty($model)) : ?>
                        <?php
                        $form = ActiveForm::begin([
                                    'id' => 'password-recovery-form',
                                    'enableAjaxValidation' => true,
                                    'enableClientValidation' => false,
                        ]);
                        ?>

                        <?=
                        $form->field($model, 'password', [
                            'template' => '{beginWrapper}<span class="icon icon-lock"></span>{input}' . PHP_EOL . '{error}{endWrapper}',
                            'inputOptions' => [
                                'placeholder' => 'Mật khẩu'
                            ]
                        ])->passwordInput();
                        ?>
                        <div class="button-submit">
                            <?= Html::submitButton(Yii::t('user', 'Finish'), ['class' => 'btn btn-success btn-block']) ?><br>
                        </div>
                        <?php ActiveForm::end(); ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>
