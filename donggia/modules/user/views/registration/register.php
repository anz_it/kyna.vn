<?php

use yii\helpers\Url;
use yii\web\View;
use yii\bootstrap\ActiveForm;
use app\modules\user\helpers\FacebookHelper;
?>
<ul class="menu-login-register nav sub-menu hidden-md-up">
    <li><a href="<?= Url::toRoute(['/user/security/login'])?>">Đăng nhập</a></li>
    <li class="active-tab"><a href="#"  data-toggle="tab" >Đăng ký</a></li>
    <div class="clearfix"></div>
</ul>
<div id="wrap-register-form-mb" class="k-popup-account">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header hidden-sm-down">
                <button type="button" class="k-popup-account-close close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Đăng ký</h4>
            </div>
            <div class="modal-body clearfix">
                <ul class="k-popup-account-top">
                    <?php if (!$model->fbId) : ?>
                        <li>
                            <?= FacebookHelper::loginButton() ?>
                        </li>
                        <li>- Hoặc đăng ký tài khoản Kyna -</li>
                    <?php else : ?>
                        <li>
                            <img src="http://graph.facebook.com/<?= $model->fbId ?>/picture?type=large&amp;width=80&amp;height=80" alt="<?= $model->name ?>" class="fb-avatar">
                            <h5><?= $model->name ?></h5>
                        </li>
                        <li>Để hoàn tất đăng ký trên Kyna.vn,<br> bạn cần điền các thông tin sau</li>
                    <?php endif; ?>
                </ul>

                <?php
                $form = ActiveForm::begin([
                            'id' => 'facebook-register-form',
                            'enableAjaxValidation' => true,
                            'action' => ['/user/registration/register']
                        ])
                ?>
                <?= $form->errorSummary($model, ['class' => 'error-summary alert alert-warning']) ?>

                <?= $form->field($model, 'fbId')->hiddenInput()->label(false) ?>

                <?=
                $form->field($model, 'email', [
                    'template' => '{beginWrapper}<span class="icon icon-mail"></span>{input}' . PHP_EOL . '{error}{endWrapper}',
                    'inputOptions' => [
                        'placeholder' => 'Email của bạn'
                    ]
                ])->input('email')->label(false)->error(false);
                ?>

                <?=
                $form->field($model, 'password', [
                    'template' => '{beginWrapper}<span class="icon icon-lock"></span>{input}' . PHP_EOL . '{error}{endWrapper}',
                    'inputOptions' => [
                        'placeholder' => 'Mật khẩu'
                    ]
                ])->passwordInput()->label(false)->error(false);
                ?>

                <?=
                $form->field($model, 'name', [
                    'template' => '{beginWrapper}<span class="icon icon-user"></span>{input}' . PHP_EOL . '{error}{endWrapper}',
                    'inputOptions' => [
                        'placeholder' => 'Họ tên'
                    ]
                ])->textInput()->label(false)->error(false);
                ?>

                <?=
                $form->field($model, 'phonenumber', [
                    'template' => '{beginWrapper}<span class="icon icon-call"></span>{input}' . PHP_EOL . '{error}{endWrapper}',
                    'inputOptions' => [
                        'placeholder' => 'Số điện thoại'
                    ]
                ])->textInput()->label(false)->error(false);
                ?>

                <div class="button-submit">
                    <button type="submit" class="btn btn-default background-green hover-bg-green">Đăng ký</button>
                </div><!--end .button-popup-->

                <?php ActiveForm::end() ?>
                <ul class="k-popup-account-bottom hidden-md-down">
                    <li>Nếu đã có tài khoản</li>
                    <li><a href="<?= Url::toRoute(['/user/security/login']) ?>" data-target="#k-popup-account-login" data-toggle="modal" data-ajax data-push-state="false">Đăng nhập</a></li>
                </ul>
            </div>
            <!--end .modal-body-->
        </div>
    </div>
</div>

<?php
$css = "
       .popup-register {
            background-color: transparent;
       }

       .k-popup-account-top .fb-avatar {
           width: 80px;
           height: 80px;
           -webkit-border-radius: 50%;
           -moz-border-radius: 50%;
           -ms-border-radius: 50%;
           -o-border-radius: 50%;
           border-radius: 50%;
           margin-bottom: .75em;
       }
       .k-popup-account-top h5 {
           margin-bottom: 1em;
       }
       #wrap-register-form-mb {
           margin-top: 50px;
       }
    ";
$this->registerCss($css);
?>
