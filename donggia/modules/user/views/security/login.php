<?php

use yii\web\View;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\modules\user\models\forms\LoginForm;
use app\modules\user\helpers\FacebookHelper;

$model = \Yii::createObject(LoginForm::className());

$this->title = 'Đăng nhập';
?>
<ul class="menu-login-register nav sub-menu hidden-md-up">
    <li class="active-tab"><a data-toggle="tab" href="#k-popup-account-login-mb">Đăng nhập</a></li>
    <li><a href="<?= Url::toRoute(['/user/registration/register'])?>">Đăng ký</a></li>
    <div class="clearfix"></div>
</ul>
<!-- POPUP LOGIN -->
<section class="main-content">




    <div class="k-popup-account-mb" id="k-popup-account-login-mb">
        <div class="k-popup-account-mb-content">
            <div class="modal-header hidden-sm-down">
                <button type="button" class="k-popup-account-close close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title">Đăng nhập</h4>
            </div>
            <div class="modal-body clearfix">
            <ul class="k-popup-account-top">
                <li>
                    <?= FacebookHelper::loginButton() ?>
                </li>
                <li>- Hoặc đăng nhập bằng tài khoản Kyna -</li>
            </ul>
            <?php
            $form = ActiveForm::begin([
                        'id' => 'login-form',
                        'action' => Url::toRoute(['/user/security/login']),
                        'method' => 'POST',
                        'enableClientValidation' => false
            ]);
            ?>
                <?php if (Yii::$app->session->hasFlash('success')) : ?>
                    <div class="alert alert-success">
                        <?= Yii::$app->session->getFlash('success') ?>
                    </div>
                <?php endif; ?>
                <?= $form->errorSummary($model, ['header' => '', 'class' => 'error-summary alert alert-warning']); ?>


                <?= $form->field($model, 'login', ["template" => '<span class="icon icon-mail"></span>{input}{hint}'])->textInput(['placeholder' => 'Email của bạn', 'autofocus' => 'autofocus', 'class' => 'text form-control', 'tabindex' => '1', 'id' => 'user-login'])->label(false); ?>


                <?= $form->field($model, 'password', ["template" => '<span class="icon icon-lock"></span>{input}{hint}'])->passwordInput(['placeholder' => 'Mật khẩu', 'class' => 'text form-control', 'tabindex' => '2', 'id' => 'user-password'])->label(false); ?>

                <div class="button-submit">
                    <?= Html::submitButton('Đăng nhập', ['id' => 'btn-submit-login', 'tabindex' => '3']); ?>
                </div><!--end .button-popup-->
            <?php ActiveForm::end(); ?>

             <a href="<?= Url::toRoute(['/user/recovery/request']) ?>" class="forgot-pass" data-target="#k-popup-account-reset" data-toggle="modal" data-ajax data-push-state="false">Quên mật khẩu?</a>
                <ul class="k-popup-account-bottom">
                    <li>Nếu bạn chưa có tài khoản</li>
                    <li>
                        <a href="<?= Url::toRoute(['/user/registration/register']) ?>" data-target="#k-popup-account-register" data-toggle="modal" data-ajax data-push-state="false">Đăng ký</a>
                    </li>

                </ul>
        </div>
    </div>
        </div>
</section>

<!-- END POPUP LOGIN -->
<?php $script = "
    ;(function($, window, document, undefined){
        $(document).ready(function(){
            $('#k-popup-account-login').on('hidden.bs.modal', function() {
                $('#login-form .error-summary').hide();
                $('#user-login').val('');
                $('#user-password').val('');
            })

            $('body').on('submit', '#login-form', function (event) {
                event.preventDefault();

                var data = $(this).serializeArray();
                data.push({name: 'currentUrl', value: window.location})

                $.post('/user/security/login', data, function (response) {
                    if (response.result) {
                        // login success
                        if (response.redirectUrl) {
                            window.location.replace(response.redirectUrl);
                        } else {
                            window.location.reload();
                        }
                    } else {
                        // login failed
                        var liHtml = '';
                        $.each(response.errors, function (key, value){
                            liHtml += '<li>' + value[0] + '</li>';
                        });

                        $('#login-form .error-summary ul').html(liHtml);
                        $('#login-form .error-summary').show();
                    }

                });
            });
        });
    })(window.jQuery || window.Zepto, window, document);";
    $css = "
    #k-popup-account-login .modal-content {
        background-color: transparent;
    }
    ";
?>
<?php
$this->registerCss($css);
$this->registerJs($script, View::POS_END, 'login-submit');
?>
