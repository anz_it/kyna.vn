<?php

namespace app\modules\user\helpers;

use Yii;
use yii\bootstrap\Html;
use yii\helpers\Url;

class FacebookHelper {
    public static function init($view) {
        $appId = Yii::$app->facebook->app_id;

        return $view->render('@app/views/layouts/common/facebook-sdk', [
            'appId' => $appId
        ]);
    }

    // <a href="/dang-ky" class="button-facebook" data-push-state="false" data-toggle="popup" data-target="#popup-register"><i class="fa fa-facebook"></i> Đăng nhập bằng Facebook</a>
    public static function loginButton($text = 'Đăng nhập bằng Facebook', $options = []) {
        $helper = Yii::$app->facebook->getRedirectLoginHelper();

        $scheme = Yii::$app->request->isSecureConnection ? 'https' : 'http';
        $registerUrl = Url::toRoute(['/user/registration/register'], $scheme);

        $authUrl = Url::toRoute(['/user/auth/fb'], $scheme);
        $url = $helper->getLoginUrl($authUrl, ['email']);

        $text = '<i class="icon-facebook"></i> '.$text;

        $extendOptions = [
            'data-toggle' => 'modal',
            'data-target' => "#k-popup-account-register",
            'data-remote' => $registerUrl,
            'data-show' => 'false',
            'class' => 'button-facebook',
            'disabled' => 'true',
        ];

        $options = array_merge($options, $extendOptions);

        return Html::a($text, $url, $options);
    }
}
