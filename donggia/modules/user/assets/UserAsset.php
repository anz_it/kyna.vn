<?php

namespace frontend\modules\user\assets;

use yii\web\AssetBundle;
use yii\web\View;

/**
 * This is class asset bunle for `course` layout
 */
class UserAsset extends AssetBundle
{

    public $basePath = '@webroot';
    public $baseUrl = '@web';

    // include css files
    public $css = [
        // FontAwesome
        'css/main.min.css',
//        'css/font-awesome.min.css',
        // Style main
//        'css/style.css',
//        'css/media.css',
        // Style Menu Mobile
//        'css/jquery.sidr.dark.css',
//        'css/my-courses.css'
    ];

    // include js
    public $js = [
        //['js/roundProgress.min.js', 'position' => View::POS_END],

        ['src/js/courses.js', 'position' => View::POS_END],
        ['src/js/offpage.js', 'position' => View::POS_END],
        ['/js/script-main.js', 'position' => View::POS_END],
        ['src/js/main.js', 'position' => View::POS_END],
        ['js/bootstrap.min.js', 'position' => View::POS_END],
        ['src/js/owl.carousel.min.js', 'position' => View::POS_END],
        // JS Menu Mobile
        ['js/jquery.sidr.min.js', 'position' => View::POS_END],
        // ajax
        ["js/ajax-caller.js", 'position' => View::POS_END],
        // cart
        ['js/check-payment.js', 'position' => View::POS_END],
    ];

    public $depends = [
        'yii\web\YiiAsset',
    ];

    public $jsOptions = ['position' => View::POS_HEAD];
}
