<?php

namespace app\modules\course\assets\components;

use yii\bootstrap\Html;
use yii\helpers\Url;

/*
 * To change this template file, choose Tools | Templates
 */
class LessonTree extends \kartik\tree\TreeView
{
    /**
     * @var the main course of current TreeView
     */
    public $courseId;

    public $wrapperOptions = [];
    public $mainTemplate = '{wrapper}';
    public $linkTemplate = '{nodeId}';
    public $currentNodeId = 0;

    public function renderWidget()
    {
        return $this->renderTree();
    }

    /**
     * Renders the markup for the tree hierarchy - uses a fast non-recursive mode of tree traversal.
     *
     * @return string
     */
    public function renderTree()
    {
        $struct = $this->_module->treeStructure + $this->_module->dataStructure;
        extract($struct);
        $nodeDepth = $currDepth = $counter = 0;
        $out = '';
        foreach ($this->_nodes as $node) {

            /* @noinspection PhpUndefinedVariableInspection */
            $nodeDepth = $node->$depthAttribute;
            /* @noinspection PhpUndefinedVariableInspection */
            $nodeLeft = $node->$leftAttribute;
            /* @noinspection PhpUndefinedVariableInspection */
            $nodeRight = $node->$rightAttribute;
            /* @noinspection PhpUndefinedVariableInspection */
            $nodeKey = $node->$keyAttribute;
            /* @noinspection PhpUndefinedVariableInspection */
            $nodeName = $node->$nameAttribute;
            /* @noinspection PhpUndefinedVariableInspection */
            $nodeIcon = $node->$iconAttribute;
            /* @noinspection PhpUndefinedVariableInspection */
            $nodeIconType = $node->$iconTypeAttribute;

            $isChild = ($nodeRight == $nodeLeft + 1);
            $indicators = '';
            $css = '';

            $indicators .= '<i class="fa fa-caret-down" aria-hidden="true"></i> ';
            //$indicators .= $this->showCheckbox ? $this->renderCheckboxIconContainer(false) . "\n" : '';
            $css = trim($css);
            if (!empty($css)) {
                Html::addCssClass($nodeOptions, $css);
            }

            $beginNodeWrapper = '';
            $endNodeWrapper = '';
            $class = '';
            if ($nodeDepth == 0) {
                $beginNodeWrapper = '</ul><ul class="detail-part show"><li class="title-part"><i class="icon-arrow-collapse-open hidden-sm-down"></i>';
                $endNodeWrapper = '<i class="icon-arrow-down pull-right hidden-md-up"></i></li>';
//                $class = 'title';
                $indicators = '';
            }
            if ($nodeDepth == 1 and !$isChild) {
                $beginNodeWrapper = '<strong class="text-uppercase">';
                $endNodeWrapper = '</strong>';
            }

            if ($node->id == $this->currentNodeId) {
                $class = ' see-ing';
            }else{
                $class = null;
            }

            if ($isChild and $nodeDepth > 0) {
                $link = str_replace('{nodeId}', $node->id, urldecode($this->linkTemplate));
                $beginNodeWrapper = '<li class="sub-part icon-text-wrapping' . $class. '">'.Html::beginTag('a', [
                        'href' => $link,
                    ]);
                $endNodeWrapper = '</a></li>';
                $indicators = '';
            }

            $out .=
                $beginNodeWrapper.
                $indicators."\n".
                //$this->renderNodeIcon($nodeIcon, $nodeIconType, $isChild) . "\n" .
                $nodeName.
                $endNodeWrapper;
            ++$counter;
        }
        $out .= "";

        return Html::tag('div', $out, $this->wrapperOptions);
    }

    public function renderRoot()
    {
        return;
    }

    public function registerAssets()
    {
        return;
    }
}
