<?php

namespace frontend\modules\course\assets;

use yii\web\AssetBundle;
use yii\web\View;

/**
 * This is class asset bunle for `course` layout
 */
class LearningAsset extends AssetBundle
{

    public $basePath = '@webroot';
    public $baseUrl = '@web';

    // include css files
    public $css = [
        // FontAwesome
        'css/main.min.css',
//        'css/font-awesome.min.css',
//        // Style main
//        'css/style.css',
//        'css/media.css',
//        'css/lesson.css',
//        // Style Menu Mobile
//        'css/jquery.sidr.dark.css',
//        // Style Fl
//        'css/functional.css',

    ];

    // include js
    public $js = [
        ['/js/script-main.js', 'position' => View::POS_END],
//        ['js/bootstrap.min.js', 'position' => View::POS_END],
        // JS Menu Mobile
        ['js/jquery.sidr.min.js', 'position' => View::POS_END],
//        ['js/flowplayer/flowplayer-3.2.13.min.js', 'position' => View::POS_END],
        // ajax
        ["js/ajax-caller.js", 'position' => View::POS_END],
        ["src/js/offpage.js", 'position' => View::POS_END],
        ["js/script-lesson.js", 'position' => View::POS_END],
        ["src/js/autosize.min.js", 'position' => View::POS_END],
        ["src/js/iscroll.js", 'position' => View::POS_END],
        ["src/js/main-iscroll.js", 'position' => View::POS_END],
        ["js/lesson.js", 'position' => View::POS_END],
        ["src/js/lesson.js", 'position' => View::POS_END],
        ["src/js/reset-visible-box-menutabs-lesson.js", 'position' => View::POS_END],
        ['src/js/menutabs-courses-jquery.js', 'position' => View::POS_END],
        ['src/js/check-payment.js', 'position' => View::POS_END],
    ];

    public $depends = [
        'yii\web\YiiAsset',
//        'yii\bootstrap\BootstrapAsset',
    ];

    public $jsOptions = ['position' => View::POS_HEAD];
}
