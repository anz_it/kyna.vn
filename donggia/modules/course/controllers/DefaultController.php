<?php

namespace app\modules\course\controllers;

use \Yii;
use yii\web\NotFoundHttpException;
use yii\helpers\Url;

use app\models\Course;
use app\models\CourseSearch;
use kyna\user\models\UserCourse;

class DefaultController extends \app\components\Controller
{

    public $refinerSet = null;

    public function init()
    {
        $parentRet = parent::init();

        // init facet filters
        $this->refinerSet = new \pahanini\refiner\Set([
            'refiners' => [
                'price' => [
                    'class' => 'app\modules\course\components\RefinerPromotionCount',
                ],
                'level' => [
                    'class' => 'app\modules\course\components\RefinerCount',
                ],
                'total_time' => [
                    'class' => 'app\modules\course\components\RefinerMultiRange',
                    'ranges' => [
                        '1' => ['id' => 1, 'min' => 0, 'max' => 10799, 'title' => 'Dưới 3 tiếng'],
                        '2' => ['id' => 2, 'min' => 10800, 'max' => 86399, 'title' => 'Dưới 1 ngày'],
                        '3' => ['id' => 3, 'min' => 86400, 'max' => 129599, 'title' => '1 - 3 ngày'],
                        '4' => ['id' => 4, 'min' => 129600, 'max' => 999999, 'title' => 'Trên 3 ngày']
                    ]
                ],
            ]
        ]);

        return $parentRet;
    }

    /**
     * @desc action for show list, list with category, search result, filter of courses
     * @return view render
     * @throws \yii\web\HttpException
     */
    public function actionIndex()
    {
        throw new NotFoundHttpException("Page not found");
    }

    /**
     * @param $id
     * @return string
     * @throws \yii\web\HttpException
     * @desc view full screen.
     */
    public function actionView($code)
    {
        $model = $this->loadModel();

        $data['code'] = $code;

        $id = $model->id;
        Yii::$app->category->current = $model->category;

        $data['model'] = $model;
        
        $data['newItem'] = '';
        if (!$this->checkAlreadyInCourse($model->id)) {
            $data['newItem'] = '1';
        }
        else{
            $data['newItem'] = '2';
        }

        if (Yii::$app->request->isAjax) {
            return $this->renderPartial('popup', $data);
        }

        $this->layout = '@app/views/layouts/one_column';
        $this->bodyClass = 'k-detail';

        return $this->render('view', $data);
    }

    public function loadModel()
    {
        $id = null;
        if ($id = Yii::$app->request->get('id')) {
            $model = Course::find()->andWhere(['type' => [Course::TYPE_VIDEO, Course::TYPE_SOFTWARE], 'id' => $id, 'status' => Course::STATUS_ACTIVE])->one();
        }
        elseif ($slug = Yii::$app->request->get('slug')) {
            $model = Course::find()->andWhere(['type' => [Course::TYPE_VIDEO, Course::TYPE_SOFTWARE], 'slug' => $slug, 'status' => Course::STATUS_ACTIVE])->one();
        }

        if (empty($model)) {
            throw new NotFoundHttpException("Không tồn tại khóa học có id: $id");
        }

        if ($affiliate_id = Yii::$app->request->get('affiliate_id') or isset($id)) {
            $get = Yii::$app->request->get();

            if ($id) unset($get['id']);
            if ($affiliate_id) unset($get['affiliate_id']);
            unset($get['slug']);

            $url = $model->url . (sizeof($get) ? '?' . http_build_query($get) : '');
            $this->redirect($url, 301);
        }

        return $model;
    }

    protected function checkAlreadyInCourse($pId)
    {
        return UserCourse::find()->where([
                'user_id' => Yii::$app->user->id,
                'course_id' => $pId
            ])->exists();
    }
}
