<?php

namespace app\modules\course\components;

use app\components\Controller;
use yii\filters\AccessControl;

/**
 * Class CourseController
 * @package app\modules\course\components
 * @property $course Course
 */
class CourseController extends Controller
{

    public $course;

    public $qnaModel;

    public $lesson;

    public $user;

    public $lessionDataProvider;

    public $finishedLessons;

    public $sectionId;

    public function init()
    {
        return parent::init();
    }

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['document'],
                        'roles' => ['?'],
                    ],
                ],
            ],
        ];
    }
}