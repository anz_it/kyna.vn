<?php
use yii\web\View;
use yii\helpers\StringHelper;
use yii\helpers\Url;
use common\helpers\ImageHelper;
use app\widgets\YoutubePlayer;

$formatter = Yii::$app->formatter;
$this->title = "{$model->name} | Kyna.vn";
?>
<link href="/css/popup.css" rel="stylesheet"/>

<div class="modal-header">
    <button type="button" class="k-popup-lesson-close close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">×</span>
    </button>
    <h4><?= $model->name ?></h4>
</div>
<div class="modal-body clearfix">
    <h6><?= (!empty($model->teacher) ? ($model->teacher->profile->name . (!empty($model->teacher->title) ? ' / ' . $model->teacher->title : '')) : '') ?></h6>
    <!--end .detail-title-->
    <div class="col-md-6 col-xs-12 k-popup-lesson-content">
        <!-- Copy & Pasted from YouTube -->
        <div class="videoWrapper">
            <iframe width="560" height="349" src="<?= $model->youtubeEmbedUrl ?>" frameborder="0" allowfullscreen></iframe>
        </div>
        <!--end .videoWrapper-->
        <div class="clearfix"></div>
        <div class="k-popup-lesson-info">
            <ul>
                <li><i class="icon icon-clock-outline"></i> Thời lượng: <?= $model->getTotalTimeText(false) ?></li>
                <li><i class="icon icon-profile"></i> Trình độ: <?= $model->levelText ?></li>
                <li><i class="icon icon-arrow-circle-right-line"></i> Bài học: <?= $model->lessonCount; ?> bài</li>
                <li><i class="icon icon-certificate"></i> Cấp chứng nhận hoàn thành</li>
            </ul>
        </div>
    </div>
    <!--end .k-popup-lesson-content-->

    <div class="col-md-6 col-xs-12 k-popup-lesson-detail">
        <p>
            <?php
            if (!empty($model->description)) {
                echo StringHelper::truncateWords($model->description, 120);
            }
            ?>
        </p>
        <ul class="k-popup-lesson-detail-price">
            <li>
                <span class="bold">
                    <?php
                    if (!empty($model->discountAmount)) {
                        echo $formatter->asCurrency($model->sellPrice);
                    } elseif (!empty($model->oldPrice)) {
                        echo $formatter->asCurrency($model->oldPrice);
                    } else {
                        echo 'Miễn phí';
                    }
                    ?>
                </span>
            </li>
            <li class="register">
                <?php if ($newItem === '1') : ?>
                    <a href="#" data-pid='<?= $model->id ?>' data-code="<?= $code ?>" class="button add-to-cart k-popup">Đăng ký học</a>
                <?php endif; ?>
                <?php if ($newItem === '2') : ?>
                    <a class="add-to-cart reg" href="#" data-pid='<?= $model->id ?>'>Đã mua khóa học</a>
                <?php endif; ?>
            </li>
        </ul>
        <ul class="k-popup-lesson-detail-care">
            <li id='fb-save-button'>
              <a data-toggle="dropdown" href="#">
                <div class="fb-save" data-uri="<?= Yii::$app->request->absoluteUrl ?>"></div>
              </a>
              <div class="popup-guide">
                  Khóa học sẽ được lưu vào phần <a href="https://facebook.com/saved" target="_blank">Saved</a> (Đã lưu)
                  trong tài khoản Facebook của bạn. Bạn có thể tìm xem lại bất kì khi nào.
              </div>
            </li>
        </ul>

    </div>
    <!--end .col-md-4 col-xs-12 left-->
</div>
<!--end .modal-body-->
