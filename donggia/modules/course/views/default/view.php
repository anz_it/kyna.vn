<?php

use common\helpers\ImageHelper;
use yii\web\View;
use common\helpers\GaHtmlHelper;
use yii\helpers\Url;
use app\widgets\YoutubePlayer;
use common\helpers\StringHelper;

$formatter = \Yii::$app->formatter;
//$this->title = "{$model->name} | Kyna.vn";
?>

<main>
    <div class="k-course-details-header container">
        <div class="col-xl-8 col-md-11 col-xs-12 k-course-details-video">
            <div class="k-course-details-title">
                <h1><?= $model->name ?></h1>
                <span><?= "{$model->teacher->profile->name} / {$model->teacher->title}" ?></span>
            </div><!--end k-course-details-title-->
            <!-- Copy & Pasted from YouTube -->
            <div class="videoWrapper">
                <iframe width="560" height="349" src="<?= $model->youtubeEmbedUrl ?>" frameborder="0" allowfullscreen></iframe>
            </div>
        </div><!--end .k-course-details-video-->
        <div class="col-xl-4 col-md-11 col-xs-12 k-course-details-info">
            <ul class="list">
                <!--
                <li class="review">
                     <span class="icon-star-list">
                         <i class="icon icon-star active"></i>
                         <i class="icon icon-star active"></i>
                         <i class="icon icon-star active"></i>
                         <i class="icon icon-star active"></i>
                         <i class="icon icon-star active"></i>
                         121 người học đã đánh giá
                     </span>
                </li>
                -->
                <li class="price-list">
                    <ul>
                        <?php if (!empty($model->discountAmount)) : ?>
                            <li class="bold"><?= $formatter->asCurrency($model->sellPrice) ?></li>
                            <li class="sale"><del><?= $formatter->asCurrency($model->oldPrice) ?></del></li>
                        <?php elseif (!empty($model->sellPrice)) : ?>
                            <li class="bold"><?= $formatter->asCurrency($model->sellPrice) ?></li>
                        <?php else: ?>
                            <li class="bold">Miễn phí</li>
                        <?php endif; ?>
                    </ul>
                </li>
                <li class="button">
                    <ul>
                        <li>
                            <?php if ($newItem === '1') : ?>
                            <?=
                            GaHtmlHelper::a('Đăng ký học<span class="icon-add-flyToElement"></span>', '#', [
                                'data-pid' => $model->id,
                                'data-code' => $code,
                                'class' => 'add-to-cart' . (!Yii::$app->user->isGuest ? ' register' : ''),
                                'category' => 'CourseDetail',
                                'action' => 'AddToCart',
                                'label' => $model->name,
                            ])
                            ?>
                            <?php endif; ?>
                            <?php if ($newItem === '2') : ?>
                            <a class="add-to-cart reg" href="#">Đã mua khóa học</a>
                            <?php endif; ?>
                        </li>
                        <li class="alert-add-to-cart">
                            <?php if ($newItem === '2') : ?>
                            <a target="_blank" href="<?= StringHelper::getHomeUrl() . Url::toRoute(['/trang-ca-nhan/khoa-hoc']) ?>"><b><i class="icon-long-arrow-right"></i> Xem khoá học của tôi</b></a>
                            <?php endif; ?>
                        </li>
<!--
                        <li>
                            <a href="#" class="button-care"><i class="icon-heart icon"></i>Quan tâm</a>
                        </li>
-->
                    </ul>
                </li>

                <li class="info">
                    <ul class="detail-info">
                        <li><i class="icon icon-clock"></i><span>Thời lượng: <?= $model->getTotalTimeText(false) ?></span></li>
                        <li><i class="icon icon-user"></i><span>Trình độ: <?= $model->levelText ?></span></li>
                        <li><i class="icon icon-arrow-circle-right-line"></i><span>Bài học: <?= $model->lessions; ?> bài</span></li>
                        <li><i class="icon icon-certificate"></i><span>Cấp chứng nhận hoàn thành</span></li>
                        <li>
                            <ul class="social-media clearfix">
                                <li id='fb-save-button'>
                                  <a data-toggle="dropdown" href="#">
                                    <div class="fb-save" data-uri="<?= Yii::$app->request->absoluteUrl ?>" data-size="small"></div>
                                  </a>
                                  <ul class="popup-guide">
                                      Khóa học sẽ được lưu vào phần <a href="https://facebook.com/saved" target="_blank">Saved</a> (Đã lưu)
                                      trong tài khoản Facebook của bạn. Bạn có thể tìm xem lại bất kì khi nào.
                                  </ul>
                                </li>
                                <li class="fb">
                                    <div class="fb-like" data-href="<?= Yii::$app->request->absoluteUrl ?>" data-layout="button_count" data-action="like" data-size="small" data-show-faces="false" data-share="true"></div>
                                </li>
                                <li class="gg">
                                    <!-- Place this tag in your head or just before your close body tag. -->
                                    <script src="https://apis.google.com/js/platform.js" async defer>
                                      {lang: 'vi'}
                                    </script>

                                    <!-- Place this tag where you want the +1 button to render. -->
                                    <div class="g-plusone" data-size="medium" data-href="<?= Yii::$app->request->absoluteUrl ?>"></div>
                                </li>
                            </ul>

                        </li>
                    </ul>
                </li>
            </ul>
        </div><!--end .k-course-details-info-->
    </div><!--end .k-course-details-header-->
    <div class="box-container">
        <div class="container">
            <div class="col-xl-8 col-md-11 col-xs-12">
                <div class="k-course-details-main">
                    <div class="k-course-details-main-left">
                        <section>
                            <div id="k-course-details-about" class="k-course-details-about" path="scrolling">
                                <ul class="list hidden-md-down">
                                    <li><a href="javascript:void(0);" link="#k-course-details-about">Giới thiệu</a></li>
                                    <li><a href="javascript:void(0);" link="#k-course-details-curriculum">Nội dung học</a></li>
                                    <li><a href="javascript:void(0);" link="#k-course-details-author">Giảng viên</a></li>
                                    <li><a href="javascript:void(0);" link="#detail-comment-facebook">Bình luận</a></li>
                                    <!-- <li><a href="javascript:void(0);" link="#k-course-details-comment">Đánh giá &amp; bình luận</a></li>-->
                                </ul>
                                <h4 class="title-content hidden-lg-up">Giới thiệu khoá học</h4>
                                <div class="course-overview"><?= !empty($model->overview) ? $model->overview : $model->description ?></div>
                            </div><!--end .k-course-details-about-->
                        </section>

                        <?= $this->render('view/_detail', ['model' => $model]) ?>

                    </div><!--end .k-course-details-main-left-->
                </div><!--end .k-course-details-main-->

                <section>
                    <div id="k-course-details-author" class="k-course-details-author" path="scrolling">
                        <div class="">
                            <h4 class="title-content">Thông tin giảng viên</h4>
                            <ul>

                                <li class="k-course-details-author-img">
                                    <?=
                                    ImageHelper::activeImage($model->teacher, 'avatar', [
                                        'alt' => $model->teacher->profile->name,
                                        'class' => 'img-fluid',
                                        'size' => ImageHelper::IMG_SIZE_AVATAR_LARGE,
                                        'resizeMode' => 'crop',
                                    ])
                                    ?>
                                    <div class="info-teacher hidden-md-up">
                                        <div class="name"><b><?= $model->teacher->profile->name ?></b></div>
                                        <div class="info"><?= $model->teacher->title ?></div>
                                    </div>
                                </li>

                                <li class="k-course-details-author-teacher">
                                    <span class="name hidden-sm-down"><?= $model->teacher->profile->name ?></span>
                                    <span class="info hidden-sm-down"><?= $model->teacher->title ?></span>
                                    <div class="content-teacher">
                                        <div class="content-teacher-box">
                                            <?= $model->teacher->introduction; ?>
                                        </div>
                                    </div>
                                    <div class="k-open-content-author" id="k-open-content-author">
                                        <div class="kbtn-see-more">Xem thêm</div>
                                    </div><!--end .wrap-content-teacher-->
                                    <!--                            <div class="collapse-more"></div><!--end collapse-more-->
                                    <!--                            <span class="read-more"><a href="javascript:void(0);" class="clearfix">Thu gọn</a></span>-->
                                </li>
                            </ul>
                        </div><!--end .col-md-8 col-sm-7 col-xs-12-->
                    </div><!--end .k-course-details-author-->
                </section>
            </div>
        </div>
    </div>
</main>

<?php
$this->registerJsFile('/src/js/add-to-cart.js?v=1514949776', ['position' => View::POS_END]);
$css = "
.k-course-details-main .course-overview {
    padding: 15px;
    background-color: white;
    margin-bottom: 1em;
}
.k-course-details-main .course-overview p {
    padding: 0;
    margin: 0 0 1em;
    background-color: transparent;
}
.k-course-details-main .course-overview ul li {
    margin-right: 0;
    position: static;
    font-size: 14px;
    display: list-item;
    list-style: inherit;
}
.k-course-details-main .course-overview ul {
    display: block;
    width: auto;
    padding: 0 0 0 3.2em;
    margin-bottom: 1em;
    list-style-type: initial;
}
.course-overview > p {
    border: none !important;
}
";

$this->registerCss($css);
// $this->registerJsFile('/js/script-detail.js', ['position' => View::POS_END]);
?>
