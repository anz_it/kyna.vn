<?php

namespace donggia\models;

use Yii;

/**
 * This is the model class for table "{{%donggia}}".
 *
 * @property integer $id
 * @property string $code
 * @property string $serial
 * @property integer $price
 * @property string $course_list
 * @property integer $course_id
 * @property integer $user_id
 * @property integer $order_id
 * @property string $campaign
 * @property integer $is_activated
 * @property integer $created_time
 * @property integer $updated_time
 */
class Donggia extends \kyna\base\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%donggia}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['code', 'serial', 'price', 'course_list', 'campaign'], 'required'],
            [['price', 'course_id', 'user_id', 'order_id', 'is_activated', 'created_time', 'updated_time'], 'integer'],
            [['code', 'serial', 'course_list', 'campaign'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'code' => Yii::t('app', 'Code'),
            'serial' => Yii::t('app', 'Serial'),
            'price' => Yii::t('app', 'Price'),
            'course_list' => Yii::t('app', 'Course List'),
            'course_id' => Yii::t('app', 'Course ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'order_id' => Yii::t('app', 'Order ID'),
            'campaign' => Yii::t('app', 'Campaign'),
            'is_activated' => Yii::t('app', 'Is Activated'),
            'created_time' => Yii::t('app', 'Created Time'),
            'updated_time' => Yii::t('app', 'Updated Time'),
        ];
    }
}
