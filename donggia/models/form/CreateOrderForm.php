<?php

namespace app\models\form;

use common\helpers\ArrayHelper;
use kyna\settings\models\Setting;
use Yii;
use yii\base\InvalidValueException;

use kyna\course\models\Course;
use kyna\order\models\Order;
use kyna\order\models\OrderQueue;
use kyna\user\models\Address;
use kyna\user\models\UserCourse;
use kyna\commission\models\AffiliateUser;
use kyna\user\models\UserTelesale;
use kyna\commission\Affiliate;

use \kyna\user\models\User;

class CreateOrderForm extends \yii\base\Model
{
    
    public $user_email;
    public $direct_discount_amount;
    public $group_discount;
    public $payment_method;
    public $course_list;
    public $shipping_contact_name;
    public $shipping_phone_number;
    public $shipping_street_address;
    public $shipping_location_id;
    public $operator_id;
    public $group_discount_amount;
    public $user_telesale_id;
    public $activation_code;
    public $note;
    public $donggia_price;

    public function scenarios()
    {
        return [
            'default' => [
                'user_email',
                'direct_discount_amount',
                'group_discount_amount',
                'group_discount',
                'course_list',
                'payment_method',
                'shipping_contact_name',
                'shipping_phone_number',
                'shipping_street_address',
                'shipping_location_id',
                'operator_id',
                'point_of_sale',
                'activation_code',
                'note',
                'donggia_price'
            ],
        ];
    }

    public function rules()
    {
        return [
            ['user_email', 'email'],
            ['operator_id', 'integer'],
            [['direct_discount_amount', 'group_discount_amount'], 'number'],
            [['direct_discount_amount', 'group_discount_amount'], 'default', 'value' => 0],
            ['group_discount', 'boolean'],
            ['shipping_method', 'string', 'max' => 20],
            ['activation_code', 'string', 'max' => 50],

            [['shipping_contact_name', 'shipping_phone_number', 'shipping_street_address', 'shipping_location_id'], 'required', 'when' => function ($model) {
                return $model->payment_method == 'ghn';
            }, 'whenClient' => 'function (attribute, value) {
                return $(\'[name*="[payment_method]"]\').val() == "ghn";
            }', 'message' => 'Thông tin này bắt buộc nếu đơn hàng là COD'],
            ['note', 'string'],
            [['user_email', 'course_list'], 'required'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'user_email' => 'Email học viên',
            'direct_discount_amount' => 'Giảm giá trực tiếp',
            'group_discount' => 'Giảm giá theo nhóm',
            'group_discount_amount' => 'Mức giảm giá theo nhóm',
            'payment_method' => 'Hình thức thanh toán',
            'order_details' => 'Danh sách khóa học',
            'shipping_vendor_id' => 'Dịch vụ giao nhận',
            'course_list' => 'Danh sách đăng ký',
            'subtotal' => 'Thành tiền',
            'shipping_contact_name' => 'Tên người nhận',
            'shipping_phone_number' => 'Số điện thoại',
            'activation_code' => 'Code hoặc số Series [Dùng Tham Chiếu]',
            'shipping_street_address' => 'Địa chỉ',
            'shipping_location_id' => 'Địa phương',
            'shipping_vendor_id' => 'Đơn vị giao vận',
            'note' => 'Ghi chú',
        ];
    }

    public function create()
    {
        /* Validate user before create */
        if (!$this->user_email) {
            return false;
        }

        $user = $this->findUser($this->user_email);

        if (!$user) {
            throw new InvalidValueException("Can't create user with this email");
        } else {
            $totalPrice = 0;
            $totalDiscount = 0;
            // check duplicate user courses
            $courseIds = explode(',', $this->course_list);
            $removedCourses = [];
            foreach ($courseIds as $key => $courseId) {
                $course = Course::findOne($courseId);
                $totalPrice += $course->sellPrice;
                if (empty($course)) {
                    unset($courseIds[$key]);
                    continue;
                }
                $alreadyCourses = [];
                $removed = false;
                
                if ($course->type == Course::TYPE_COMBO) {
                    foreach ($course->comboItems as $item) {
                        if (UserCourse::find()->where(['user_id' => $user->id, 'course_id' => $item->course_id])->exists()) {
                            $removed = true;
                            $alreadyCourses[] = $item->course->name;
                        }
                    }
                } else {
                    if (UserCourse::find()->where(['user_id' => $user->id, 'course_id' => $course->id])->exists()) {
                        $removed = true;
                    }
                }
                
                if ($removed) {
                    unset($courseIds[$key]);
                    $removedCourses[] = "<b>{$course->name}</b>" . (!empty($alreadyCourses) ? "(đã học: <b>" . implode(', ', $alreadyCourses) . "</b>)" : '');
                }
            }
            
            if (!empty($removedCourses)) {
                $this->course_list = implode(',', $courseIds);
                
                Yii::$app->session->setFlash('warning', "Đã loại bỏ các khóa học sau: " . implode(', ', $removedCourses) . ", học viên <b>{$user->profile->name}</b> đã tham gia các khóa học này.");
                return false;
            }
        }

        /* Start creating order */
        $order = Order::createEmpty(Order::SCENARIO_CREATE_BACKEND);
        $order->on(Order::EVENT_ORDER_CREATED, [$this, 'pushToQueue']);

        $order->direct_discount_amount = $totalPrice - $this->donggia_price;
        $order->activation_code = $this->activation_code;
        $order->group_discount_amount = $this->group_discount_amount;
        $order->payment_method = $this->payment_method;
        $order->user_id = $user->id;
        $order->reference_id = $this->operator_id;
        $order->operator_id = $this->operator_id;
        $order->is_done_telesale_process = Order::BOOL_YES;
        $order->payment_fee = 0;

        if ($order->isCod and $address = $this->updateUserAddress($user->id)) {
            $order->shippingAddress = !empty($this->note) ? ($address->attributes + ['note' => $this->note]) : $address;
        }

        // get CyberPay affiliate user
        $settings = ArrayHelper::map(Setting::find()->all(), 'key', 'value');
        $affiliateEmail = !empty($settings['cyberpay_affiliate']) ? $settings['cyberpay_affiliate'] : 0;
        $user = User::findOne(['email' => $affiliateEmail]);
        if ($user) {
            $affiliateUser = AffiliateUser::find()->where(['user_id' => $user->id, 'status' => AffiliateUser::STATUS_ACTIVE])->one();
            if (!empty($affiliateUser)) {
                // set affiliate
                $order->affiliate_id = $affiliateUser->user_id;
            }
        }

        // save order details
        $order->addDetails($this->prepareOrderItems());
        if ($order->save()) {
            // stuck for cod payment fee
            $order->payment_fee = 0;
            $order->save();
            return $order;
        }

        return false;
    }
    
    protected function prepareOrderItems()
    {
        $courseIds = explode(',', $this->course_list);
        $courses = Course::find()->where(['id' => $courseIds])->all();
        
        $addItems = [];
        foreach ($courses as $key => $course) {
            if ($course->type == Course::TYPE_COMBO) {
                unset($courses[$key]);
                
                $comboItems = $course->comboItems;
                foreach($comboItems as $item) {
                    if (!array_key_exists($item->course_id, $courses)) {
                        $product = Product::findOne($item->course_id);
                        $product->setCombo($course);
                        $product->setDiscountAmount($product->price - $item->price);
                        
                        $addItems[$product->id] = $product;
                    }
                }
            } else {
                $addItems[$course->id] = $course;
            }
        }
        
        return $addItems;
    }

    protected function updateUserAddress($userId)
    {
        if (!$address = Address::find()->where(['user_id' => $userId])->one()) {
            $address = new Address();
            $address->user_id = $userId;
        }

        $address->attributes = [
            'email' => $this->user_email,
            'contact_name' => $this->shipping_contact_name,
            'phone_number' => $this->shipping_phone_number,
            'street_address' => $this->shipping_street_address,
            'location_id' => $this->shipping_location_id,
        ];

        if (!$address->save()) {
            var_dump($address->errors);die;
            $this->errors = $address->errors;
            return false;
        }

        return $address;
    }

    protected function findUser($email)
    {
        $user = User::find()->where(['email' => $email])->one();

        if (!$user) {
            $user = new User([
                'scenario' => 'create',
            ]);
            $user->load(['User' => [
                'email' => $email,
                'username' => $email,
            ]]);
            // update meta fields
            $user = $this->beforeCreateUser($user);
            if ($user->create()) {
                // register profile
                $this->afterCreateUser($user);
            }
        }

        return $user;
    }

    /**
     * Update meta fields
     * @param $user
     * @return mixed
     */
    public function beforeCreateUser($user)
    {
        $userTelesale = UserTelesale::findOne($this->user_telesale_id);
        if ($userTelesale) {
            $user->phone = $userTelesale->phone_number;
            $user->address = $userTelesale->street_address;
            $user->location_id = $userTelesale->location_id;
        }
        return $user;
    }

    /**
     * Update profile if Telesale user
     * @param $user
     */
    public function afterCreateUser($user)
    {
        $userTelesale = UserTelesale::findOne($this->user_telesale_id);
        if ($userTelesale) {
            $profile = $user->profile;
            $profile->public_email = $userTelesale->email;
            $profile->name = $userTelesale->full_name;
            $profile->phone_number = $userTelesale->phone_number;
            $profile->location = $userTelesale->location_id;
            $profile->save(false);
        }
    }

    public function pushToQueue($actionEvent)
    {
        $order = $actionEvent->sender;
        OrderQueue::pushToQueue($order);
    }
}
