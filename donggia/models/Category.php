<?php

namespace app\models;

use Yii;
use yii\helpers\Url;
/*
 * This is override active record class of table `categories` for Frontend usage
 */
class Category extends \kyna\course\models\Category
{
    private $_categoryUrlSegments = [
        'module' => 'course',
        'controller' => 'default',
        'action' => 'index',
    ];

    /**
     * @desc This is methods to get list of categories depends on parent_id, default is get roots
     * @param type $parent_id
     * @return array Category instances
     */
    public static function getList($parent_id = 0, $select = ['*'])
    {
        if (!($list = Yii::$app->cache->get('children_category_of_'.$parent_id)) OR !isset(Yii::$app->cache)) {
            $list = self::find()->select($select)
                ->andWhere(['parent_id' => $parent_id])
                ->andFilterWhere(['status' => self::STATUS_ACTIVE])
                ->orderBy('order')->all();

            if (isset(Yii::$app->cache)) {
                Yii::$app->cache->set('featured_course_ids', $list, 600);
            }
        }

        return $list;
    }


    public function getUrl($withId = false, $scheme = false)
    {
        if (Yii::$app->category) {
            $params[] = Yii::$app->category->defaultRoute;
            $withId = Yii::$app->category->urlWithId;
        }
        else {
            $url = '/';
            if ($this->_categoryUrlSegments['module']) {
                $url .= $this->_categoryUrlSegments['module'].'/';
            }

            $url .= ($this->_categoryUrlSegments['controller']) ? $this->_categoryUrlSegments['controller'].'/' : 'default/';
            $url .= ($this->_categoryUrlSegments['action']) ? $this->_categoryUrlSegments['action'].'/' : 'index';

            $params[] = $url;
        }

        if ($this->slug) {
            $params['slug'] = $this->slug;
        }
        else {
            // force id if slug not found
            $withId = true;
            $params['slug'] = 'danh-muc';
        }

        if ($withId) {
            $params['catId'] = $this->id;
        }

        if ($this->parent_id > 0 and $parent = $this->parent->slug) {
            $params['parent'] = $parent;
        }

        return Url::toRoute($params, $scheme);
        //return \yii\helpers\Url::toRoute(['/course/default/index', 'catId' => $this->id, 'slug' => $this->slug]);
    }
}
