<?php

namespace app\models;

use Yii;
use yii\data\ActiveDataProvider;
use kyna\course\models\CourseMeta;
use kyna\user\models\Profile;

/*
 * This is search class of table `{{%courses}}` for Frontend usage
 */
class CourseSearch extends Course
{

    const SORT_NEW = 'new';
    const SORT_FEATURE = 'feature';
    const SORT_PROMOTION = 'promotion';

    public function search($params)
    {
        $tblCourse = self::tableName();

        $query = self::find()->andWhere([$tblCourse . '.status' => self::STATUS_ACTIVE])
                ->andWhere(['type' => [Course::TYPE_VIDEO, Course::TYPE_SOFTWARE]]);

        if (!empty($params['q'])) {
            $profileTable = Profile::tableName();
            $query->leftJoin($profileTable . ' p', "p.user_id = $tblCourse.teacher_id");
            
            // not use FULL TEXT search yet, TODO: apply Elastic search
            $query->andWhere("$tblCourse.name LIKE :term OR short_name LIKE :term OR keyword LIKE :term OR p.name LIKE :term", [':term' => '%' . $params['q'] . '%']);
        }

        $sort = Yii::$app->request->get('sort', self::SORT_NEW);

        if (!empty($params['catId'])) {
            $catModel = Category::findOne($params['catId']);
            if(!empty($catModel->relatedIds)) {
                $relatedIds = $catModel->relatedIds;
                $query->andFilterWhere(['in', 'category_id', $relatedIds]);
            }
        }

        switch ($sort) {
            case self::SORT_NEW:
                break;

            case self::SORT_PROMOTION:
                break;

            case self::SORT_FEATURE:
                $courseMetaTbl = CourseMeta::tableName();
                $query->leftJoin($courseMetaTbl . ' cm', 'cm.course_id = courses.id');
                $query->andWhere(['cm.key' => 'is_feature']);
                $query->andWhere(['cm.value' => CourseMeta::BOOL_YES]);

                break;
        }

        Yii::$app->controller->refinerSet->applyTo($query);

        $query->orderBy('created_time DESC');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => Yii::$app->params['pageSize'],
            ],
        ]);

        return $dataProvider;
    }
}
