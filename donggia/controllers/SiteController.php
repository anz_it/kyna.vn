<?php

namespace donggia\controllers;

use app\models\form\CreateOrderForm;
use common\helpers\ArrayHelper;
use donggia\models\Donggia;
use kyna\order\models\Order;
use kyna\user\models\Token;
use kyna\user\models\UserCourse;
use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use app\models\Course;
use yii\web\Response;

/**
 * Site controller
 */
class SiteController extends \app\components\Controller
{

    public $layout = 'home';

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
                'view' => '404',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $code = Yii::$app->request->get('q', null);
        $donggia = Donggia::findOne(['code' => trim($code)]);
        $courses = null;
        if (!empty($donggia) && $donggia->is_activated == Course::BOOL_NO) {
            $courses = Course::findAll([
                'id' => explode(',', $donggia->course_list),
                'status' => Course::STATUS_ACTIVE
            ]);
        }
        return $this->render('index', ['courses' => $courses, 'q' => $code]);
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        $token = Token::find()->where(['user_id' => Yii::$app->user->id])->one();
        if($token)
            $token->delete();
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionAdd() {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $pId = Yii::$app->request->post('pid');
        $code = Yii::$app->request->post('code');
        $result = [
            'status' => false,
            'login' => false,
            'msg' => 'Không thể đăng kí học với mã thẻ <b>' . $code . '</b>'
        ];
        if (Yii::$app->user->isGuest) {
            $result['status'] = false;
            $result['login'] = true;
            $result['msg'] = $this->renderPartial('../partials/_required_login_modal', []);
        } elseif (!$this->checkAlreadyInCourse($pId)) {
            $result = $this->_preOrder($code, $pId);
        }
        return $result;
    }

    private function _preOrder($code, $pId) {
        $result['status'] = false;
        $result['login'] = false;
        $result['msg'] = 'Không thể đăng kí học với mã thẻ <b>' . $code . '</b>';
        $donggia = Donggia::findOne(['code' => trim($code)]);
        if (!empty($donggia) && $donggia->is_activated == Course::BOOL_NO) {
            $course = Course::findOne([
                'id' => $pId,
                'status' => Course::STATUS_ACTIVE
            ]);
            if (!empty($course)) {
                // place order for cyberpay
                $result = $this->placeOrder($code, $pId);
            }
        }
        return $result;
    }

    private function placeOrder($code, $pId) {
        $result['status'] = false;
        $result['login'] = false;
        $result['msg'] = 'Đăng kí không thành công';
        $model = new CreateOrderForm();
        $donggia = Donggia::findOne(['code' => trim($code)]);
        $data['CreateOrderForm'] = [
            'user_email' => Yii::$app->user->identity->email,
            'payment_method' => 'ghn',
            'course_list' => "{$pId}",
            'shipping_contact_name' => Yii::$app->user->identity->profile->name,
            'shipping_phone_number' => Yii::$app->user->identity->profile->phone_number,
            'shipping_street_address' => '',
            'shipping_location_id' => 0,
            'operator_id' => 0,
            'note' => 'Order from donggia',
            'donggia_price' => $donggia->price
        ];

        if ($model->load($data) and ($order = $model->create())) {
            // completed order
            Order::complete($order->id, $order->total);
            // active courses for order
            Order::activate($order->id);
            // active donggia code
            $donggia->is_activated = Donggia::BOOL_YES;
            $donggia->course_id = $pId;
            $donggia->user_id = $order->user_id;
            $donggia->order_id = $order->id;
            $donggia->save();
            // response
            $result['status'] = true;
            $result['login'] = false;
            $result['msg'] = $this->renderPartial('../partials/_success', []);
        }
        return $result;
    }

    protected function checkAlreadyInCourse($pId)
    {
        return UserCourse::find()->where([
            'user_id' => Yii::$app->user->id,
            'course_id' => $pId
        ])->exists();
    }

}
