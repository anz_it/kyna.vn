(function ($, window, document, undefined) {
    $(document).ready(function () {
        $('body').on('click', '.add-to-cart', function (e) {
            var thisBtn = $(this);
            e.preventDefault();
            if (!thisBtn.hasClass('reg')){
                thisBtn.addClass('reg');
                var pid = thisBtn.data('pid');
                var code = thisBtn.data('code');
                var csrfToken = $('meta[name="csrf-token"]').attr("content");
                $.ajax({
                    url: '/course-register',
                    type: 'POST',
                    data: {
                        code: code,
                        pid: pid,
                        _csrf: csrfToken
                    },
                    success: function (response) {
                        if (response.status) {
                            $(".k-popup-lesson .close").click();
                            var bootstrapDialog = new BootstrapDialog();
                            bootstrapDialog.setTitle('Thành công!');
                            bootstrapDialog.setMessage(response.msg);
                            bootstrapDialog.setType(BootstrapDialog.TYPE_SUCCESS);
                            bootstrapDialog.open();
                        } else {
                            $(".k-popup-lesson .close").click();
                            var bootstrapDialog = new BootstrapDialog();
                            bootstrapDialog.setTitle('Lỗi!');
                            bootstrapDialog.setMessage(response.msg);
                            bootstrapDialog.setType(BootstrapDialog.TYPE_WARNING);
                            bootstrapDialog.open();
                        }
                        $('body').on('click', '.button-required-login', function (e) {
                            bootstrapDialog.close();
                        });
                    }
                });
            }
        });
    });
})(window.jQuery, window, document);