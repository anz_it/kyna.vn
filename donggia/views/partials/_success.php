<?php
/**
 * Created by IntelliJ IDEA.
 * User: tn
 * Date: 12/20/2016
 * Time: 11:09
 */

use \common\helpers\StringHelper;
use \yii\helpers\Url;

?>
<div class="text-center">
    <span >Đăng kí khóa học thành công!
    Bắt đầu học tại <a target="_blank" href="<?= StringHelper::getHomeUrl() . '/trang-ca-nhan/khoa-hoc' ?>"><b>khóa học của tôi</b></a>
</span>
</div>
