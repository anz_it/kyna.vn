<?php

use yii\helpers\Url;

$this->title = "Kyna.vn | Không tìm thấy trang";

$settings = Yii::$app->controller->settings;
$hotKeywords = !empty($settings['hot_keyword']) ? explode(',', $settings['hot_keyword']) : [
    'Giao tiếp',
    'Monkey junior',
    'Bán hàng',
    'Giáo dục sớm',
    'Tiếng anh',
    'Marketing',
    'Bất động sản',
    'Tiếng hoa',
    'Tiếng Nhật'
];
?>
<main>
 
    <section>
        <div id="k-page-404" class="k-height-header container">
            <div class="k-page-404-wrap">
                <div class="col-md-6 col-xs-12 box img">
                    <h2><img src="/src/img/404/404.png" class="img-fluid" alt="404"></h2>
                </div>
                <!--end .img-->
                <div class="col-md-6 col-xs-12 box text">
                    <h3>Đừng lo lắng</h3>
                    <p class="top">Trong cuộc sống chúng ta vẫn thường hay đi vào những ngã rẽ không đúng!<br>Đây không phải là lớp học Online bạn đang tìm!</p>
                    <p class="bottom">Trở lại đúng hướng và xem các gợi ý tìm kiếm khóa học khác của chúng tôi ở bên dưới</p>
                    <ul>
                        <?php foreach ($hotKeywords as $keyword) : ?>
                            <li><a href="<?= Url::toRoute(['/course/default/index', 'q' => $keyword]) ?>" title="<?= $keyword ?>"><?= $keyword ?></a></li>
                        <?php endforeach; ?>
                    </ul>
                </div>
                <!--end .text-->
            </div>
        </div><!--end #page-404-->
    </section>
    
</main>
