<?php
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\bootstrap\Html;

$this->title = "Kyna.vn - Học online cùng chuyên gia";

?>

<main>
    <section>
        <div id="k-banner" class="clearfix k-height-header pc">
            <div class="container">
                <div class="wrap-content col-md-6 col-md-offset-3 pd0">
                    <h1 class="title">Nhập mã thẻ Airpay</h1>
                    <?php $form = ActiveForm::begin([
                        'id' => 'search-form-index',
                        'action' => Url::toRoute(['/']),
                        'method' => 'get',
                        'options' => [
                            'class' => 'clearfix form-search'
                        ]
                    ]); ?>
                    <input value="<?= $q ?>" type="text" name="q" class="form-control" placeholder="Nhập mã thẻ Airpay để tìm kiếm khóa học tương ứng">
                    <button class="btn btn-default" type="submit">
                        <i class="icon-search icon"></i>
                    </button>
                    <?php ActiveForm::end(); ?>
                </div>
                <!--end .wrap-content-banner-top -->
            </div><!--end .container-->
        </div><!--end #k-banner-->
    </section>


    <section>
        <div id="k-highlights" class="container">
            <?php if (!empty($courses)): ?>
                <h2 class="title">Danh sách khóa học</h2>
                <ul class="clearfix k-box-card-list">
                    <?php foreach ($courses as $course) { ?>
                        <li class="col-xl-3 col-lg-4 col-md-6 col-xs-12 k-box-card">
                            <?= $this->render('@app/views/layouts/common/box-product', ['model' => $course, 'code' => $q]); ?>
                        </li>
                    <?php } ?>
                </ul>
            <?php elseif (!is_null($q)): ?>
                <h2 class="title">Không tìm thấy khóa học</h2>
            <?php endif; ?>
        </div><!--end #k-highlights-->
    </section>
</main>
