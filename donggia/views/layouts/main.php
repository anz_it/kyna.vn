<!DOCTYPE HTML>
<?php

use yii\helpers\Html;
use yii\web\View;
use donggia\widgets\HeaderWidget;
use donggia\widgets\FooterWidget;
use frontend\widgets\GaWidget;
use \app\modules\user\components\Facebook;
use app\modules\user\helpers\FacebookHelper;

use app\assets\HomeAsset;

HomeAsset::register($this);
$this->beginPage();
?>
<html  lang="<?= Yii::$app->language ?>">
<head>
    <?= $this->render('common/html_head') ?>
</head>

<body>
<?= FacebookHelper::init($this) ?>
<?php $this->beginBody()?>

<?= HeaderWidget::widget([]); ?>

<?= $content; ?>

<?= FooterWidget::widget(); ?>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
