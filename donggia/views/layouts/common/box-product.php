<?php

use yii\helpers\StringHelper;
use common\helpers\CDNHelper;

$formatter = \Yii::$app->formatter;
$flag = false;
if(Yii::$app->devicedetect->isMobile() || Yii::$app->devicedetect->isTablet()){
    $flag = true;
}
?>

<div class="k-box-card-wrap">
    <div class="img">
        <?= CDNHelper::image($model->image_url, [
            'alt' => $model->name,
            'class' => 'img-fluid',
            'size' => CDNHelper::IMG_SIZE_THUMBNAIL,
            'resizeMode' => 'cover',
        ]) ?>
        <div class="teacher mb">
            <?php if (!empty($model->teacher)) : ?>
            <ul>
                <li>

                    <?= CDNHelper::image($model->teacher->avatar, [
                        'alt' => $model->teacher->profile->name,
                        'class' => 'img-teacher',
                        'size' => CDNHelper::IMG_SIZE_AVATAR_SMALL,
                        'resizeMode' => 'crop',
                    ]) ?>
                </li>
                <li>
                    <?= $model->teacher->profile->name ?>
                </li>
            </ul>
            <?php endif; ?>
        </div>
        <span class="time"><?= $model->totalTimeText ?></span>
        <span class="background-detail">
          <span class="wrap-position">
              <?php if ($flag == false) { ?>
                  <a href="<?= $model->url . '/' . $code ?>" data-ajax data-toggle="popup" data-target="#modal">Xem nhanh</a>
              <?php } else { ?>
                  <a href="<?= $model->url . '/' . $code ?>" >Xem nhanh</a>
              <?php } ?>
          </span>
        </span>
    </div>
    <!--end .img-->
    <div class="content">
        <h4><?= $model->name ?></h4>
        <?php if (!empty($model->teacher)) : ?>
            <span class="author pc"><?= $model->teacher->profile->name ?></span>
            <span class="pc"><?= $model->teacher->title ?></span>
        <?php endif; ?>
        <!--
        <ul>
            <li>
              <span class="icon-star-list">
              <i class="icon icon-star active"></i>
              <i class="icon icon-star active"></i>
              <i class="icon icon-star active"></i>
              <i class="icon icon-star-half active"></i>
              <i class="icon icon-star"></i>
              </span>
            </li>

            <li>(1200 đánh giá)</li>
        </ul>
        -->
    </div>
    <!--end .content -->
    <div class="view-price">
        <ul>
            <?php if (!empty($model->discountAmount)) : ?>
                <li class="sale"><span><?= $formatter->asCurrency($model->oldPrice) ?></span></li>
                <li class="price"><span><?= $formatter->asCurrency($model->sellPrice) ?></span>
                </li>
            <?php elseif (!empty($model->oldPrice)) : ?>
                <li class="price"><span><?= $formatter->asCurrency($model->oldPrice) ?></span></li>
            <?php else : ?>
                <li class="price"><span>Miễn phí</span></li>
            <?php endif; ?>
        </ul>
    </div>
    <!--end .view-price-->
    <?php if($flag == false) { ?>
        <a href="<?= $model->url . '/' . $code ?>" class="link-wrap" data-ajax data-toggle="popup" data-target="#modal"></a>
    <?php }else{ ?>
        <a href="<?= $model->url . '/' . $code ?>" class="link-wrap"></a>
    <?php } ?>
</div>
<?php if($flag == false) { ?>
    <a href="<?= $model->url . '/' . $code ?>" class="card-popup" data-ajax data-toggle="popup" data-target="#modal"></a>
<?php }else { ?>
    <a href="<?= $model->url . '/' . $code ?>" class="card-popup" ></a>
<?php } ?>
<!--end .wrap-->
