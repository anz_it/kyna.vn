<?php

use yii\helpers\Url;
use yii\helpers\Html;
use common\helpers\CDNHelper;
use \common\helpers\StringHelper;

$user = Yii::$app->user->identity;
$flag = false;
if(Yii::$app->devicedetect->isMobile() || Yii::$app->devicedetect->isTablet()){
    $flag = true;
}
?>


<ul class="nav navbar-nav k-header-info login col-md-3 col-md-push-7">
    <?php if($flag == false) { ?>
    <li class="hotline">
        <span class="text">Hotline</span>
        <span class="number"><?= Yii::$app->params['hotline'] ?></span>
    </li>

    <li class="account dropdown wrap">
        <div class="sub-wrap">
            <?php $avUrl = CDNHelper::image($user->avatarImage, [
                'size' => CDNHelper::IMG_SIZE_AVATAR_SMALL,
                'resizeMode' => 'crop',
                'returnMode' => 'url'
            ]);
            if (!$avUrl) {
                $avUrl = '/src/img/default_avatar.png';
            }

            ?>
            <img src="<?=$avUrl?>" class="img-responsive" />
            <div class="text">
                <span class="user" ><?= $user->profile->name ?></span>
            </div>
        </div>

        <ul class="dropdown-menu dropdown-user">
            <li class="inner clearfix">
                <ul>
                    <li><a target="_blank" href="<?= StringHelper::getHomeUrl() . Url::toRoute(['/user/course/index']) ?>"><span class="icon profile-my-courses"></span>Khóa học của tôi</a></li>
                    <li class="button-out">
                        <?= Html::beginForm(['/user/security/logout'], 'post') ?>
                        <a href="javascript:" onclick="$(this).closest('form').submit()"><span class="icon profile-logout"><i class="icon icon-sign-out"></i></span>Thoát</a>
                        <?= Html::endForm() ?>
                    </li>
                </ul>
            </li>
        </ul>

        <!--
        <ul class="dropdown-menu dropdown-login">
            <li><h4 class="title">Bắt đầu học trên Kyna bằng các cách sau</h4></li>
           <li><a href="" class="button-facebook" data-push-state="false" data-toggle="popup" data-target="#popup-register"><i class="fa fa-facebook"></i> Đăng nhập bằng facebook</a></li>
           <li><a href="#" class="button-google"><i class="fa fa-google" aria-hidden="true"></i> Đăng nhập bằng facebook</a></li>
           <li><a href="#" class="button-login" data-toggle="modal" data-target="#k-popup-account-login">Đăng nhập Kyna</a></li>
           <li><a href="#" class="button-register" data-toggle="modal" data-target="#k-popup-account-register">Đăng ký Kyna</a></li>
        </ul>
        -->
    </li>
    <?php } else { ?>
        <li class="account wrap borderradius">
            <a href="<?= StringHelper::getHomeUrl() . Url::toRoute('/user/security/login') ?>" class="dropdown-toggle-profile" >
                <span class="sub-wrap">Vào học <!--<i class="icon-arrow-collapse-bottom icon"></i>--> </span>
            </a>
        </li>
    <?php } ?>
</ul>
