<?php

use yii\helpers\Url;
use app\modules\user\helpers\FacebookHelper;
use \common\helpers\StringHelper;


$flag = false;
if(Yii::$app->devicedetect->isMobile() || Yii::$app->devicedetect->isTablet()){
    $flag = true;
}
?>

<ul class="nav navbar-nav k-header-info guest col-md-3 col-md-push-7">
    <?php if($flag == false) { ?>
        <li class="hotline">
            <span class="text">Hotline</span>
            <span class="number"><?= Yii::$app->params['hotline'] ?></span>
        </li>

        <li class="account dropdown wrap">
            <a href="#" class="dropdown-toggle-profile" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                <span class="sub-wrap">Tài khoản <i class="icon-arrow-collapse-bottom icon"></i> </span>
            </a>

            <ul class="dropdown-menu dropdown-login">
                <li><h4 class="title">Bắt đầu học trên Kyna bằng các cách sau</h4></li>
               <li><a href="<?= Url::toRoute('/user/security/login') ?>" class="button-login" data-toggle="modal" data-target="#k-popup-account-login" >Đăng nhập Kyna</a></li>
               <li><a href="<?= Url::toRoute(['/user/registration/register'])?>" class="button-register" data-toggle="modal" data-target="#k-popup-account-register" data-ajax data-push-state=false>Đăng ký Kyna</a></li>
            </ul>

        </li>
    <?php } else { ?>
        <li class="account wrap borderradius">
            <a href="<?= StringHelper::getHomeUrl() . Url::toRoute('/user/security/login') ?>" class="dropdown-toggle-profile" >
                <span class="sub-wrap">Đăng nhập <!--<i class="icon-arrow-collapse-bottom icon"></i>--> </span>
            </a>
        </li>
    <?php } ?>
</ul>
