<?php $this->beginContent('@app/views/layouts/main.php'); ?>
<section>
    <?= $content; ?>
</section>
<?php $this->endContent(); ?>
