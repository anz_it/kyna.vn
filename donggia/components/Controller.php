<?php

namespace app\components;

use Yii;
use yii\web\Cookie;
use yii\helpers\ArrayHelper;

use kyna\settings\models\Setting;
use kyna\commission\models\AffiliateUser;
use kyna\user\models\User;

use app\models\Category;

/*
 * This is override core Controller class for Frontend usage
 */

class Controller extends \yii\web\Controller
{

    public $layout = '@app/views/layouts/two_columns';
    public $rootCats = [];
    public $settings = [];
    public $bodyClass = '';

    public function init()
    {
        $ret = parent::init();

        $this->rootCats = Category::getList(0, ['id', 'name', 'slug']);
        $this->settings = ArrayHelper::map(Setting::find()->all(), 'key', 'value');

        if (!empty($this->settings['js-head'])) {
            $js = $this->settings['js-head'];
            $this->view->registerJs($js, View::POS_HEAD);
        }
        if (!empty($this->settings['js-foot'])) {
            $js = $this->settings['js-foot'];
            $this->view->registerJs($js, View::POS_FOOT);
        }

        if (!empty(Yii::$app->user->identity->loginToken)) {
            $code = Yii::$app->user->identity->loginToken->code;

            Yii::$app->session->open();

            if ($code != Yii::$app->session->id){
                Yii::$app->user->logout();
            }

            Yii::$app->session->close();
        }

        $this->_registerMetaTag();

        return $ret;
    }

    public function beforeAction($action)
    {
        $ret = parent::beforeAction($action);

        if (!Yii::$app->request->get('debug') && $debugModule = Yii::$app->getModule('debug')) {
            $debugModule->instance->allowedIPs = [];
        }

        $this->detectAffiliate();

        return $ret;
    }

    protected function detectAffiliate()
    {
        $user = null;
        if ($affiliate_id = Yii::$app->request->get('affiliate_id')) {
            if ($affiliate_id < 300000) {
                $user = User::find()->andWhere(['v2_id' => $affiliate_id])->one();
            }
            if (is_null($user)) {
                $user = User::findOne($affiliate_id);
            }
            if (is_null($user)) {
                return false;
            }
            $affiliate_id = $user->id;

            $cookies = Yii::$app->request->cookies;
            $currentId = $cookies->getValue('affiliate_id');

            // check if not exist or update affiliate_id
            if (empty($currentId) || $currentId != $affiliate_id) {
                $affiliateUser = AffiliateUser::find()->where(['user_id' => $affiliate_id, 'status' => AffiliateUser::STATUS_ACTIVE])->one();
                if (empty($affiliateUser)) {
                    return;
                }

                $cookies = Yii::$app->response->cookies;

                // add affiliate cookie to the response
                $cookies->add(new Cookie([
                    'name' => 'affiliate_id',
                    'value' => $affiliate_id,
                    'expire' => $affiliateUser->cookieExpireTimestamp
                ]));
            }
        }
    }

    /**
     * Register Meta Tags
     */
    private function _registerMetaTag() {
        $this->view->registerMetaTag([
            'name' => 'google-site-verification',
            'content' => 'JQvj84cgEFSsi0_LRfTN39yKNQ5ZogkvSUw9bcQR_t0'
        ]);
    }
}
