<?php

namespace app\components\htmlmeta;
use Yii;
use yii\helpers\Url;
use app\models\Course;
use app\models\Category;
use app\components\Settings;

class OpenGraph extends BaseMetaData {
    public $video;
    public $appId;

    public function register() {
        $this->view->registerMetaTag([
            'property' => 'fb:app_id',
            'content' => Yii::$app->facebook->app_id,
        ]);

        foreach (['type', 'title', 'description', 'image', 'url', 'video'] as $prop) {
            if (isset($this->$prop)) {
                $this->view->registerMetaTag([
                    'property' => 'og:'.$prop,
                    'content' => $this->$prop,
                ]);
            }
        }
    }

    public function setData($object) {
        if ($object instanceof Course) {
            $this->_fromCourse($object);
            return;
        }
        if ($object instanceof Category) {
            $this->_fromCategory($object);
            return;
        }
        if ($object instanceof Settings) {
            $this->_fromHome($object);
            return;
        }
    }

    private function _fromCourse($course) {
        $this->type = 'article';
        if (isset($course->og_title)) {
            $this->title = $course->og_title;
        }
        elseif(isset($course->meta_title)) {
            $this->title = $course->meta_title;
        }
        else {
            $this->title = $course->name;
        }

        if (isset($course->og_description)) {
            $this->description = $course->og_description;
        }
        elseif(isset($course->meta_description)) {
            $this->description = $course->meta_description;
        }
        else {
            $this->description = $course->description;
        }

        if (isset($course->og_image)) {
            $this->image = $course->og_image;
        }
        else {
            $this->image = $course->image_url;
        }

        $this->image = $this->absUrl($this->image);

        if (isset($course->video_url)) {
            parse_str( parse_url( $course->video_url, PHP_URL_QUERY ), $vars );
            if (array_key_exists('v', $vars)) {
                $this->video = 'https://www.youtube.com/v/'.$vars['v'];
            }
        }

        if (isset($course->og_url)) {
            $this->url = $course->og_url;
        }
        elseif(isset($course->meta_canonical)) {
            $this->url = $course->meta_canonical;
        }
        else {
            $this->url = $course->getUrl(false, $this->scheme);
            //$this->url = Url::toRoute(['/course/view', 'id' => $course->id], 'http');
        }
    }
    private function _fromCategory($category) {
        $this->type = 'article';
        if (isset($category->og_title)) {
            $this->title = $category->og_title;
        }
        elseif(isset($category->meta_title)) {
            $this->title = $category->meta_title;
        }
        else {
            $this->title = $category->name;
        }

        if (isset($category->og_description)) {
            $this->description = $category->og_description;
        }
        elseif(isset($category->meta_description)) {
            $this->description = $category->meta_description;
        }
        else {
            $this->description = $category->description;
        }

        if (isset($category->og_image)) {
            $this->image = $category->og_image;
        }

        if ($this->image) {
            $this->image = $this->absUrl($this->image);
        }

        if (isset($category->og_url)) {
            $this->url = $category->og_url;
        }
        elseif(isset($category->meta_canonical)) {
            $this->url = $category->meta_canonical;
        }
        else {
            //$this->url = Url::toRoute(['/course', 'catId' => $category->id], 'http');
            $this->url = $category->getUrl(false, $this->scheme);
        }
    }

    private function _fromHome($siteSetting) {
        $this->type = 'website';
        if ($siteSetting->keyExists('og_title')) {
            $this->title = $siteSetting->og_title;
        }
        elseif($siteSetting->keyExists('meta_title')) {
            $this->title = $siteSetting->meta_title;
        }
        elseif ($siteSetting->keyExists('site_name')) {
            $this->title = $siteSetting->site_name;
        }

        if ($siteSetting->keyExists('og_description')) {
            $this->description = $siteSetting->og_description;
        }
        elseif($siteSetting->keyExists('meta_description')) {
            $this->description = $siteSetting->meta_description;
        }

        if ($siteSetting->keyExists('og_image')) {
            $this->image = $siteSetting->og_image;
        }
        elseif($siteSetting->keyExists('logo_url')) {
            $this->image = $siteSetting->logo_url;
        }
        if ($this->image) {
            $this->image = $this->absUrl($this->image);
        }

        if ($siteSetting->keyExists('base_url')) {
            $this->url = $siteSetting->base_url;
        }
        else {
            $this->url = Url::toRoute(['/'], $this->scheme);
        }
    }
}
