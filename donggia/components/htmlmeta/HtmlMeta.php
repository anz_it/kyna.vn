<?php
namespace app\components\htmlmeta;

use Yii;
use yii\helpers\Url;
use app\models\Course;
use app\models\Category;

class HtmlMeta extends BaseMetaData {
    public $robots = [
        'index' => 'index',
        'follow' => 'follow'
    ];

    public function register() {
        if (isset($this->title)) {
            $this->view->title = $this->title;
        }
        if (isset($this->description)) {
            $this->view->registerMetaTag([
                'name' => 'description',
                'content' => $this->description,
            ]);
        }

        $siteSetting = Yii::$app->settings;
        if ($siteSetting->keyExists('meta_robots_noindex') and ($siteSetting->meta_robots_noindex == true)) {
            $this->robots['index'] = 'noindex';
        }
        if ($siteSetting->keyExists('meta_robots_nofollow') and ($siteSetting->meta_robots_nofollow == true)) {
            $this->robots['follow'] = 'nofollow';
        }

        if (!empty($this->robots)) {
            $this->view->registerMetaTag([
                'name' => 'robots',
                'content' => implode(',', $this->robots),
            ]);
        }

        if (isset($this->url)) {
            $this->view->registerLinkTag([
                'rel' => 'canonical',
                'href' => $this->url,
            ]);
        }
    }

    public function setData($object) {
        if ($object instanceof Course) {
            $this->_fromCourse($object);
            return;
        }

        if ($object instanceof Category) {
            $this->_fromCategory($object);
            return;
        }

        $settingClass = Yii::$app->settings->className();
        if ($object instanceof $settingClass) {
            $this->_fromHome($object);
            return;
        }
    }

    private function _fromHome($siteSetting) {
        $this->type = 'website';
        if($siteSetting->keyExists('meta_title')) {
            $this->title = $siteSetting->meta_title;
        }
        elseif ($siteSetting->keyExists('site_name')) {
            $this->title = $siteSetting->site_name;
        }

        if($siteSetting->keyExists('meta_description')) {
            $this->description = $siteSetting->meta_description;
        }

        if ($siteSetting->keyExists('meta_robots_noindex') and ($siteSetting->meta_robots_noindex == true)) {
            $this->robots['index'] = 'noindex';
        }
        if ($siteSetting->keyExists('meta_robots_nofollow') and ($siteSetting->meta_robots_nofollow == true)) {
            $this->robots['follow'] = 'nofollow';
        }

        if($siteSetting->keyExists('meta_canonical')) {
            $this->url = $siteSetting->meta_canonical;
        }
        else {
            $this->url = Url::toRoute(['/'], $this->scheme);
        }
    }

    private function _fromCourse($course) {
        if(isset($course->meta_title)) {
            $this->title = $course->meta_title;
        }
        else {
            $this->title = $course->name;
        }

        if(isset($course->meta_description)) {
            $this->description = $course->meta_description;
        }
        else {
            $this->description = $course->description;
        }

        if (isset($course->meta_robots_noindex) and ($course->meta_robots_noindex == true)) {
            $this->robots['index'] = 'noindex';
        }
        if (isset($course->meta_robots_nofollow) and ($course->meta_robots_nofollow == true)) {
            $this->robots['index'] = 'nofollow';
        }

        if(isset($course->meta_canonical)) {
            $this->url = $course->meta_canonical;
        }
        else {
            //$this->url = Url::toRoute(['/course/default/view', 'id' => $course->id], 'http');
            $this->url = $course->getUrl(false, $this->scheme);
        }
    }
    private function _fromCategory($category) {
        if(isset($category->meta_title)) {
            $this->title = $category->meta_title;
        }
        else {
            $this->title = $category->name;
        }

        if(isset($category->meta_description)) {
            $this->description = $category->meta_description;
        }
        else {
            $this->description = $category->description;
        }

        if(isset($category->meta_canonical)) {
            $this->url = $category->meta_canonical;
        }
        else {
            $this->url = $category->getUrl(false, $this->scheme);
        }
    }
}
