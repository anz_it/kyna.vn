<?php
namespace app\components\htmlmeta;

use Yii;
use yii\helpers\Url;

use app\models\Course;
use app\models\Category;
use app\components\Settings;

class StructuredData extends BaseMetaData {
    const TYPE_COURSE_LIST = 'ItemList';
    const TYPE_COURSE = 'Course';
    const TYPE_PRODUCT = 'Product';
    const TYPE_DEFAULT = 'Organization';

    public $provider;
    public $itemListElement;
    public $logo;
    public $contactPoint = [];

    public $offers;
    public $brand;

    private $_schema;
    private $_schemaTemplate = [
        '@context' => 'http://schema.org',
    ];

    public function register() {
        if ($this->type) {
            $schema = $this->_getSchema($this->type);
            foreach(['name', 'description', 'image', 'url', 'logo', 'contactPoint', 'provider', 'itemListElement', 'offers', 'brand'] as $prop) {
                if (!empty($this->$prop)) {
                    $schema[$prop] = $this->$prop;
                }
            }
            $this->view->jsLd[$this->type] = json_encode($schema, JSON_PRETTY_PRINT|JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES);
        }
    }

    public function setData($object) {
        if (is_array($object)) {
            $courses = array_filter($object, function ($course) {
                if ($course instanceof Course) {
                    return true;
                }
            });
            $this->_fromCourseList($courses);
            return;
        }

        if ($object instanceof Course) {
            if ($this->type === self::TYPE_PRODUCT) {
                $this->_fromCourseAsProduct($object);
            }
            else {
                $this->_fromCourse($object);
            }
            return;
        }

        if ($object instanceof Settings) {
            $this->_fromCompany($object);
            return;
        }
    }

    private function _getSchema() {
        $schema = $this->_schemaTemplate;
        $schema['@type'] = $this->type;
        return $schema;
    }

    private function _fromCompany($siteSetting) {
        $this->type = self::TYPE_DEFAULT;

        if ($siteSetting->keyExists('sd_title')) {
            $this->name = $siteSetting->sd_title;
        }
        elseif($siteSetting->keyExists('meta_title')) {
            $this->name = $siteSetting->meta_title;
        }
        elseif($siteSetting->keyExists('name')) {
            $this->name = $siteSetting->name;
        }

        if($siteSetting->keyExists('logo_url')) {
            $this->logo = $siteSetting->logo_url;
        }
        if ($this->logo) {
            $this->logo = $this->absUrl($this->logo);
        }

        if ($siteSetting->keyExists('base_url')) {
            $this->url = $siteSetting->base_url;
        }
        else {
            $this->url = Url::toRoute(['/'], $this->scheme);
        }

        if ($siteSetting->keyExists('hot_line')) {
            $this->contactPoint[] = [
                '@type' => 'ContactPoint',
                'telephone' => $siteSetting->hot_line,
                'contactType' => 'customer service',
            ];
        }
    }

    private function _fromCourseAsProduct($course) {
        //$this->type = self::TYPE_PRODUCT;
        if (isset($course->sd_title)) {
            $this->name = $course->sd_title;
        }
        elseif(isset($course->meta_title)) {
            $this->name = $course->meta_title;
        }
        else {
            $this->name = $course->name;
        }

        if (isset($course->sd_description)) {
            $this->description = $course->sd_description;
        }
        elseif(isset($course->meta_description)) {
            $this->description = $course->meta_description;
        }
        else {
            $this->description = $course->description;
        }

        if (isset($course->sd_image)) {
            $this->image = $course->sd_image;
        }
        else {
            $this->image = $course->image_url;
        }
        $this->image = $this->absUrl($this->image);

        $this->brand = 'Kyna.vn';

        $this->offers = [
            '@type' => 'Offer',
            'priceCurrency' => 'VND',
            'price'         => $course->price,
            'availability'  => 'http://schema.org/InStock',
        ];

        /* TODO: add AggregateRating schema () */
    }

    private function _fromCourse($course) {
        $this->type = self::TYPE_COURSE;
        if (isset($course->sd_title)) {
            $this->name = $course->sd_title;
        }
        elseif(isset($course->meta_title)) {
            $this->name = $course->meta_title;
        }
        else {
            $this->name = $course->name;
        }

        if (isset($course->sd_description)) {
            $this->description = $course->sd_description;
        }
        elseif(isset($course->meta_description)) {
            $this->description = $course->meta_description;
        }
        else {
            $this->description = $course->description;
        }

        $this->provider = [
            '@type' => 'Person',
            'name' => $course->teacher->profile->name,
        ];
    }

    private function _fromCourseList($courses) {
        $this->type = self::TYPE_COURSE_LIST;

        $n = sizeof($courses);
        for ($i = 0; $i < $n; $i++) {
            $course = $courses[$i];
            $this->itemListElement[] = [
                '@type' => 'ListItem',
                'position' => $i + 1,
                'url' => $course->getUrl(false, $this->scheme)
                //'url' => Url::toRoute(['/course/view', 'id' => $courses[$i]->id], 'http')
            ];
        }
    }
}
