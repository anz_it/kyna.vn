<?php

namespace app\components\htmlmeta;
use yii\helpers\Url;
use yii\base\InvalidConfigException;
use app\components\View;

abstract class BaseMetaData {
    public $view;

    public $type;
    public $title;
    public $name;
    public $description;
    public $image;
    public $url;

    public $scheme = 'https';

    public function __construct($view, $object, $type = false) {
        if (!($view instanceof \app\components\View)) {
            throw new InvalidConfigException('View class must be an instance of "app\components\View"');
        }
        $this->view = $view;

        if ($type) {
            $this->type = $type;
        }
        $this->setData($object);
    }

    protected function absUrl($url) {
        if (Url::isRelative($url)) {
            return Url::to($url, $this->scheme);
        }
        return $url;
    }

    public abstract function register();
    public abstract function setData($object);
}
