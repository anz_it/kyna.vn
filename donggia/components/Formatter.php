<?php

namespace app\components;

/* 
 * This is helper class support all formats: number, money, time, datetime, string...
 */
class Formatter
{
    
    public static function money($number)
    {
        return number_format($number, 0, ',', '.') . "đ";
    }
}
