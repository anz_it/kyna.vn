<?php

namespace app\components\jscompiler;

use Yii;
use yii\base\Component;
use app\components\View;

class ClosureCompiler extends JsCompiler {
    const LEVEL_SIMPLE = 'SIMPLE_OPTIMIZATIONS';
    const LEVEL_ADVANCED = 'ADVANCED_OPTIMIZATIONS';
    const LEVEL_WHITESPACE = 'WHITESPACE_ONLY';

    public $java = '/usr/bin/java';
    public $compiler = '@root/compiler.jar';
    public $compilationLevel = self::LEVEL_SIMPLE;

    public $suffix = '.closure';

    protected function getCommand() {
        return $this->java.' -jar '.Yii::getAlias($this->compiler).
            ' --compilation_level '.$this->compilationLevel.
            ' --js '.$this->inputString.' --js_output_file '.$this->outputFilename.
            ' 2>&1';
    }
}
