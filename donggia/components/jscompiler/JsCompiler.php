<?php

namespace app\components\jscompiler;

use Yii;
use yii\base\Exception;
use yii\base\Component;
use app\components\View;

abstract class JsCompiler extends Component implements JsCompilerInterface {
    public $view;
    public $overwrite = false;
    public $currentPosition;
    public $outputDir = '@frontend/web/assets';
    public $suffix = '';

    public $excludeOnAjax = [
        'jquery.min.js',
        'bootstrap.min.js',
        // 'yii.js',
        // 'yii.activeForm.js',
        //'jquery.min.js',
    ];

    public $compileScripts = true;

    protected $_files = [];
    protected $_command;
    protected $_inputFilesString = '';
    protected $_outputFilename = [];

    public function init() {
        parent::init();

        $this->_files = [];
        $this->_inputFilesString = [];
        $this->_outputFilename = [];

        $this->view->on(View::EVENT_BEFORE_HEAD, [$this, 'initScript']);

        $this->view->on(View::EVENT_BEFORE_HEAD, [$this, 'registerCompiled'], ['position' => View::POS_HEAD]);
        $this->view->on(View::EVENT_BEFORE_BODY_END, [$this, 'registerCompiled'], ['position' => View::POS_END]);
    }

    public function initScript() {
        foreach ($this->view->jsFiles as $position => $files) {
            $this->_files[$position] = [];
            $jsFiles = array_keys($files);

            if (Yii::$app->request->isAjax) {
                $exclusions = $this->excludeOnAjax;
                $jsFiles = array_filter($jsFiles, function($file) use ($exclusions) {
                    return !in_array(basename($file), $exclusions);
                });
            }

            $this->addFiles($jsFiles, $position);
        }

        if (!$this->compileScripts) {
            return;
        }

        foreach ($this->view->js as $position => $scripts) {
            if (!isset($this->_files[$position])) {
                $this->_files[$position] = [];
            }
            $this->addScripts($this->view->js[$position], $position);
        }

        if (Yii::$app->request->isAjax) {
            //var_dump($this->_files);die;
        }
    }

    public function addScripts($js, $position) {
        $scriptFilename = '';
        $scriptContent = '';
        foreach($js as $name => $script) {
            $scriptFilename .= $name;
            $scriptContent = $script.PHP_EOL;
        }

        $file = Yii::getAlias($this->outputDir.'/'.md5($name).'.js');
        file_put_contents($file, $scriptContent);

        $file = str_replace('\\', '/', strtolower($file));
        $file = str_replace(Yii::getAlias('@webroot'), '', $file);

        $this->addFiles($file, $position);
    }

    public function addFiles($files, $position) {
        if(is_string($files)) {
            $files = explode(' ', $files);
        }

        $this->_files[$position] = array_merge($this->_files[$position], $files);
    }

    public function getOutput() {
        if ($output = $this->outputFilename) {
            $output = str_replace('\\', '/', strtolower($output));
            return str_replace(Yii::getAlias('@webroot'), '', $output);
        }

        return false;
    }

    public function registerCompiled($event) {
        if (empty($event->data['position'])) {
            throw new Exception("Please specific current position of js script");
        }

        $position = $this->currentPosition = $event->data['position'];

        if (($result = $this->compile()) and $this->output) {
            if (isset($this->view->jsFiles[$position])) {
                unset($this->view->jsFiles[$position]);
            }
            if (isset($this->view->js[$position]) and $this->compileScripts) {
                unset($this->view->js[$position]);
            }
            $this->view->registerJsFile($this->output, ['position' => $position], 'closure.compiled.js');
        }
    }

    protected function getInputString() {
        $position = $this->currentPosition;
        $webroot = Yii::getAlias('@webroot');

        if (!isset($this->_files[$position])) {
            return null;
        }

        $files = array_map(function($file) use ($webroot) {
            return $webroot.$file;
        }, $this->_files[$position]);

        return implode(' ', $files);
    }

    protected function getOutputFilename() {
        $position = $this->currentPosition;
        if (isset($this->_outputFilename[$position])) {
            return $this->_outputFilename[$position];
        }

        $webroot = Yii::getAlias('@webroot');
        $compiledFilename = '';
        if (!isset($this->_files[$position])) {
            return;
        }

        $files = $this->_files[$position];
        foreach($files as $file) {
            $filename = $webroot.$file;
            $compiledFilename .= '+'.$file;
        }

        $compiledFilename = trim($compiledFilename, "+");
        $compiledFilename = urlencode($compiledFilename);
        $compiledFilename = md5($compiledFilename);

        $this->_outputFilename[$position] = Yii::getAlias($this->outputDir.'/'.$compiledFilename.$this->suffix.'.js');

        return $this->_outputFilename[$position];
    }

    protected function compile() {
        if (!$this->overwrite and file_exists($this->outputFilename)) {
            return true;
        }
        exec($this->command, $output, $returnVar);

        return $returnVar !== 0;
    }

    protected abstract function getCommand();
}
