<?php

namespace app\components\jscompiler;

interface JsCompilerInterface {
    // add js script as string
    public function addScripts($js, $position);

    // add js script as .js file
    public function addFiles($files, $position);

    // return output url
    public function getOutput();
}
