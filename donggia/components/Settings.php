<?php

namespace app\components;

use kyna\settings\models\Setting;
use yii\helpers\ArrayHelper;
use yii\base\Component;
use yii\db\BaseActiveRecord;

class Settings extends Component {
    private $_settings;

    public function init() {
        parent::init();
        $this->_settings = ArrayHelper::map(Setting::find()->all(), 'key', 'value');
    }

    public function keyExists($key) {
        $allowedKeys = array_keys($this->_settings);
        return in_array($key, $allowedKeys);
    }

    public function __get($key) {
        if ($this->keyExists($key)) {
            return $this->_settings[$key];
        }

        return parent::__get($key);
    }
}
