<?php

namespace app\components;

use Yii;
use yii\web\View as BaseView;
use yii\bootstrap\Html;
use app\components\htmlmeta\OpenGraph;
use app\components\htmlmeta\HtmlMeta;
use app\components\htmlmeta\StructuredData;

class View extends BaseView {
    const EVENT_BEFORE_HEAD = 'beforeHead';
    const EVENT_BEFORE_BODY_BEGIN = 'beforeBodyBegin';
    const EVENT_BEFORE_BODY_END = 'beforeBodyEnd';

    public $openGraph;
    public $structuredData;
    public $htmlMeta;
    public $jsCompiler = null;

    public $jsLd = [];

    private $_jsCompiler = null;
    private $_isAjax = false;

    public function init() {
        parent::init();

        $this->_isAjax = Yii::$app->request->isAjax;

        if ($this->jsCompiler) {
            $this->jsCompiler['view'] = $this;
            $this->_jsCompiler = Yii::createObject($this->jsCompiler);
        }
    }

    public function registerMetaData($object) {
        $htmlMeta = new HtmlMeta($this, $object);
        $openGraph = new OpenGraph($this, $object);

        $htmlMeta->register();
        $openGraph->register();

        $structuredData = new StructuredData($this, $object);
        $structuredData->register();

        $structuredData = new StructuredData($this, $object, StructuredData::TYPE_PRODUCT);
        $structuredData->register();

        $settings = Yii::$app->settings;

        $companyStructuredData = new StructuredData($this, $settings);
        $companyStructuredData->register();
    }

    protected function renderJsonLd() {
        $jsonLd = '';
        foreach ($this->jsLd as $jsLd) {
            $jsonLd .= PHP_EOL.'<script type="application/ld+json">';
            $jsonLd .= PHP_EOL.$jsLd;
            $jsonLd .= PHP_EOL.'</script>';
        }
        return $jsonLd;
    }

    protected function renderBodyEndHtml($ajaxMode) {
        $this->trigger(self::EVENT_BEFORE_BODY_END);

        return parent::renderBodyEndHtml($ajaxMode);
    }

    protected function renderHeadHtml() {
        $this->trigger(self::EVENT_BEFORE_HEAD);

        $head = parent::renderHeadHtml();
        return $head.$this->renderJsonLd();
    }
}
