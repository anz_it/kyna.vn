<?php
use \common\helpers\StringHelper;
?>
<footer>
    <div id="k-footer">
        <div class="container">
            <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 hotline">
                <h4 class="bold text-transform title">Kết nối với Kyna</h4>

                <div class="social">
                    <?php if (empty($settings) || empty($settings['facebook_url'])) { ?>
                        <a href="https://www.facebook.com/kyna.vn" target="_blank" class="facebook"><i class="icon icon-facebook"></i></a>
                    <?php } else { ?>
                        <a href="<?= $settings['facebook_url'] ?>" target="_blank" class="facebook"><i class="icon icon-facebook"></i></a>
                    <?php } ?>
                    <?php if (empty($settings) || empty($settings['youtube_url'])) { ?>
                        <a href="https://www.youtube.com/user/kynavn" target="_blank" class="youtube"><i class="icon icon-youtube"></i></a>
                    <?php } else { ?>
                        <a href="<?= $settings['youtube_url'] ?>" target="_blank" class="youtube"><i class="icon icon-youtube"></i></a>
                    <?php } ?>
                </div>
                <!--end .social-->

                <ul class="bottom">
                    <?php if (empty($settings) || empty($settings['hot_line'])) { ?>
                        <li>Hotline: 1900 6364 09</li>
                    <?php } else { ?>
                        <li>Hotline: <?= $settings['hot_line']; ?></li>
                    <?php } ?>
                    <?php if (empty($settings) || empty($settings['email_footer'])) { ?>
                        <li>Email: hotro@kyna.vn</li>
                    <?php } else { ?>
                        <li>Email: <?= $settings['email_footer']; ?></li>
                    <?php } ?>
                </ul>
                <!--end .bottom-->

            </div>
            <!--end .hotline -->
            <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 info">
                <h4 class="bold title">Thông tin Kyna</h4>
                <ul>
                    <li><a href="<?= StringHelper::getHomeUrl() ?>/danh-sach-khoa-hoc">Danh sách khóa học</a></li>
                    <li><a href="<?= StringHelper::getHomeUrl() ?>/p/kyna/cau-hoi-thuong-gap">Câu hỏi thường gặp</a></li>
                    <li><a href="<?= StringHelper::getHomeUrl() ?>/p/kyna/cau-hoi-thuong-gap/huong-dan-thanh-toan-hoc-phi">Hướng dẫn thanh toán học phí</a></li>
                    <li><a href="<?= StringHelper::getHomeUrl() ?>/p/kyna/cau-hoi-thuong-gap/chinh-sach-hoan-hoc-phi">Chính sách hoàn học phí</a></li>
                </ul>
                <!--end .top-->
            </div>
            <!--end .info-->
            <div class="col-lg-2 col-md-4 col-sm-6 col-xs-12 about">
                <h4 class="bold text-transform title">Về Kyna</h4>
                <ul>
                    <li><a href="<?= StringHelper::getHomeUrl() ?>/p/kyna/gioi-thieu" class="hover-color-green">Giới thiệu về công ty</a></li>
                    <!--
                    <li><a href="#" class="hover-color-green">Thư viện bài viết</a></li>
                    -->
                    <li><a href="<?= StringHelper::getHomeUrl() ?>/p/kyna/hop-tac" class="hover-color-green">Hợp tác cùng Kyna.vn</a></li>
                    <li><a href="<?= StringHelper::getHomeUrl() ?>/p/kyna/giang-day" class="hover-color-green">Giảng dạy tại Kyna.vn</a></li>
                </ul>
                <!--end .top-->
            </div>
            <!--end .about-->
            <div class="col-lg-4 col-md-12 col-xs-12 fanpage">
                <div class="face-content">
                    <iframe src="//www.facebook.com/plugins/likebox.php?href=https://www.facebook.com/kyna.vn&amp;colorscheme=light&amp;show_faces=true&amp;stream=false&amp;header=false&amp;height=350" scrolling="no" frameborder="0" style="border:none; overflow:hidden; height:220px;" allowTransparency="false"></iframe>
                </div>
            </div>
            <!--end .fanpage-->
        </div>
        <!--end .container-->
    </div>
    <!--end #wrap-footer-->
    <!--    Copyright   -->
    <div id="k-footer-copyright">
        <div class="container">
            <div class="col-lg-8 col-xs-12 address">
                
                <div class="text">
                    <p class="text-copyright">© 2014 - Bản quyền của Công Ty Cổ Phần Dream Viet Education</p>
                    <p>
                        <?php if (empty($settings['company_address'])) { ?>
                            Địa chỉ: Biệt thự 298/3 Điện Biên Phủ, phường 17, quận Bình Thạnh, TP Hồ Chí Minh
                            <?php
                        } else {
                            echo $settings['company_address'];
                        }
                        ?>
                    </p>
                    <p>
                        <?php if (empty($settings['bussiness_certificate'])) { ?>
                            Giấy phép ĐKKD số 0312401818 do Sở Kế hoạch và Đầu tư TPHCM cấp ngày 05/08/2013
                            <?php
                        } else {
                            echo $settings['bussiness_certificate'];
                        }
                        ?>
                    </p>
                </div>
                <!--end col-xs-8 text-->
            </div>
            <!--end .col-sm-7 col-xs-12 left-->
            <div class="col-lg-4 col-xs-12 info">
                <a href="http://online.gov.vn/HomePage/WebsiteDisplay.aspx?DocId=22275" target="_blank"><img alt="" title="" src="/src/img/dadangky.png" data-pin-nopin="true" class="img-fluid"></a>
                <ul>
                    <li><a href="<?= StringHelper::getHomeUrl() ?>/p/kyna/dieu-khoan-dich-vu" class="hover-color-green">Điều khoản dịch vụ</a></li>
                    <li><a href="<?= StringHelper::getHomeUrl() ?>/p/kyna/chinh-sach-bao-mat" class="hover-color-green">Chính sách bảo mật</a></li>
                </ul>
            </div>
            <!--end .col-sm-5 col-xs-12 right-->
        </div>
        <!--end .container-->
    </div>
    <!--end #wrap-copyright-->
    <div id="k-footer-mb">
        <ul class="k-footer-mb-contact">
            <?php if (empty($settings) || empty($settings['hot_line'])) { ?>
                <li>
                    <a href="tel:1900 6364 09" target="_blank"><i class="icon icon-call"></i> 1900 6364 09</a>
                </li>
            <?php } else { ?>
                <li>
                    <a href="tel:<?= $settings['hot_line']; ?>" target="_blank"><i class="icon icon-call"></i> <?= $settings['hot_line']; ?></a>
                </li>
            <?php } ?>
            <?php if (empty($settings) || empty($settings['email_footer'])) { ?>
                <li>
                    <a href="mailto:hotro@kyna.vn" target="_blank"><i class="icon icon-mail-outline"></i> hotro.kyna.vn</a>
                </li>
            <?php } else { ?>
                <li>
                    <a href="mailto:<?= $settings['email_footer']; ?>" target="_blank"><i class="icon icon-mail-outline"></i> <?= $settings['email_footer']; ?></a>
                </li>
            <?php } ?>
        </ul>
        <ul class="k-footer-mb-social">
            <li>
                <a href="<?= StringHelper::getHomeUrl() ?>/p/kyna/cau-hoi-thuong-gap" target="_blank"><i class="icon icon-hind"></i></a>
            </li>
            <li>
                <a href="https://www.facebook.com/kyna.vn" target="_blank"><i class="icon icon-facebook"></i></a>
            </li>
            <li>
                <a href="https://www.youtube.com/user/kynavn" target="_blank"><i class="icon icon-youtube"></i></a>
            </li>
        </ul>
        <p>&copy; 2014 - Công ty Cổ Phần Dream Viet Education</p>
    </div><!--end #k-footer-mb-->

</footer>

<!-- POPUP REGISTER -->
<div class="modal fade k-popup-account" id="k-popup-account-register" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">

</div>
<!-- END POPUP REGISTER -->

<!-- POPUP  -->
<div class="modal fade k-popup-lesson" id="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        </div>
    </div>
</div>

<!-- POPUP  -->
<div class="modal fade popup-form-header k-popup-account" id="popup-register" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content popup-register">
        </div>
    </div>
</div>
<!-- END POPUP -->
<!-- /.modal -->
<div class="modal modal-activeCOD fade" id="activeCOD" tabindex="-1" role="dialog" aria-labelledby="activeCOD" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade k-popup-account" id="k-popup-account-reset" tabindex="-1" role="dialog">

</div>

<!--<div class="container">-->
    <div class="popup modal fade modal-thong-bao-deltaxu" id="popup-thong-bao-deltaxu"  tabindex="-1" role="dialog" aria-labelledby="popup-thong-bao-deltaxu" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12" style="font-style: italic;">
                            <p>Xin chào!</p>
                            <p>Cảm ơn sự ủng hộ của bạn dành cho Kyna.vn trong thời gian vừa qua. Đây là phiên bản mới của Kyna.vn với nhiều thay đổi và cải tiến. Trong phiên bản này, <b>Deltaxu</b> sẽ được chuyển thành điểm thưởng <b>K-point</b>.</p>
                            <p>Theo đó, Deltaxu trong phiên bản cũ sẽ được chuyển thành các mã Voucher tương ứng. Cụ thể hơn, nếu trong tài khoản của bạn có 200.000 Deltaxu thì mã voucher bạn nhận được sẽ có giá trị 200.000đ.</p>
                            <p>Bạn có thể sử dụng mã voucher này cho các giao dịch khóa học tại Kyna.vn như bình thường. Thông tin chi tiết về sự thay đổi sẽ được gửi kèm theo email đến bạn.</p>
                        </div>

                        <div class="col-md-6 col-md-offset-6">
                            <div class="col-sm-12">
                                <img src="<?php echo Yii::$app->getUrlManager()->getBaseUrl() ?>/src/img/signature.png" alt="" class="img-responsive" style="margin: 0 auto; display: block;" />
                            </div>
                            <div class="col-sm-12" >
                                <h4 style="margin: 5px auto; font-weight: 300 !important; text-align: center;">Nguyễn Thanh Minh</h4>
                                <h5 style="font-weight: 300 !important; text-align: center;">Co-founder Kyna.vn</h5>
                            </div>
                        </div>

                    </div>
                </div>

            </div>
        </div>

    </div>

<!--</div>-->

<div class="modal fade k-popup-account" id="k-popup-account-login" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">


                </div>
        </div>
    </div>
</div>
<?php //echo $this->render('@app/views/layouts/common/login_form') ?>
