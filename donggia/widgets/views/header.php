<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ActiveForm;

?>

<header>
    <!-- -->
    <nav class="navbar navbar-light k-header-wrap">
        <div class="container">
            <a href="/" class="navbar-brand logo col-lg-2 col-md-2">
                <?php if (empty($settings) || empty($settings['logo_url'])) { ?>
                    <img src="<?= Url::toRoute("/"); ?>src/img/logo.svg" alt="Kyna.vn" class="img-fluid">
                <?php } else { ?>
                    <img src="<?= $settings['logo_url']; ?>" alt="Kyna.vn" class="img-fluid"/>
                <?php } ?>
            </a>

            <?php if (!Yii::$app->user->isGuest) { ?>
                <?php echo $this->render('@app/views/layouts/common/header_right_login') ?>
            <?php } else { ?>
                <?php echo $this->render('@app/views/layouts/common/header_right_guest') ?>
            <?php } ?>

        </div><!--end .container-->
    </nav>

    <!-- Menu mobile -->
    <div id="nav-mobile" class="offpage-menu offpage-left">
        <header>
            <div class="k-header-offpage-menu">
                <a href="/">
                    <img src="<?= Url::toRoute("/"); ?>src/img/logo.svg" alt="Kyna.vn" class="img-responsive">
                </a>
                <a href="#" class="right offpage-close" data-offpage="#nav-mobile">
                    <i class="icon icon-arrow-right-bold" aria-hidden="true"></i>
                </a>
            </div><!--header -->
        </header>
        <div class="content">
            <div class="panel-group" id="accordion">

            </div>
            <!-- ./panel-group -->
        </div><!--end .content-->
    </div><!--end #nav-mobile-->
    <!-- End Menu mobile-->
</header>

<?php $script = "
;(function($){
    $('body').on('submit', '#profile-form', function(e) {
         e.preventDefault();

         var url = $(this).attr('action');
         var form = $(e.target);
         console.log(form);
         console.log(form.parent());

         $.post(url, form.serialize(), function (res) {
             form.parents('.k-profile-edit-content').html(res);
         });
    });
    $('body').on('submit', '#active-cod-form', function(e){
        e.preventDefault();

        var url = $(this).attr('action');
        var form = $(e.target);

        $.post(url, form.serialize(), function (res) {
            form.parent().html(res);
        });
    });
})(jQuery);
" ?>

<?php $this->registerJs($script, View::POS_END, 'profile-submit') ?>
