<?php

namespace donggia\widgets;

use yii;
use common\widgets\base\BaseWidget;

class HeaderWidget extends BaseWidget
{

    public function run()
    {
        return $this->getHeader();
    }

    public function getHeader()
    {
        return $this->render('header', [
                    'settings' => $this->settings,
        ]);
    }

}
