<?php

$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'), require(__DIR__ . '/../../common/config/params-local.php'), require(__DIR__ . '/params.php'), require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-donggia',
    'basePath' => dirname(__DIR__),
    'bootstrap' => [
        'log',
    ],
    'controllerNamespace' => 'donggia\controllers',
    'modules' => [
        'course' => [
            'class' => 'app\modules\course\CourseModule',
        ],
        'user' => [
            'class' => 'dektrium\user\Module',
            'enableUnconfirmedLogin' => true,
            'enableConfirmation' => false,
            'enableRegistration' => true,
            'modelMap' => [
                'User' => 'kyna\user\models\User',
                'Profile' => 'kyna\user\models\Profile',
                'LoginForm' => 'dektrium\user\models\LoginForm',
                'RegistrationForm' => 'app\modules\user\models\forms\RegistrationForm',
            ],
            'controllerMap' => [
                'security' => 'app\modules\user\controllers\SecurityController',
                'recovery' => 'app\modules\user\controllers\RecoveryController',
                'profile' => 'app\modules\user\controllers\ProfileController',
                'course' => 'app\modules\user\controllers\CourseController',
                'message' => 'app\modules\user\controllers\MessageController',
                'question' => 'app\modules\user\controllers\QuestionController',
                'transaction' => 'app\modules\user\controllers\TransactionController',
                'document' => 'app\modules\user\controllers\DocumentController',
                'discuss' => 'app\modules\user\controllers\DiscussController',
                'order'     => 'app\modules\user\controllers\OrderController',
                'registration'  => 'app\modules\user\controllers\RegistrationController',
                'auth'  => 'app\modules\user\controllers\AuthController',
            ],
            'urlRules' => [],
        ],
        'treemanager' =>  [
            'class' => '\kartik\tree\Module',
        ]
    ],
    'components' => [
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'categories' => ['ipn'],
                    'levels' => ['error', 'warning', 'info'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'rules' => require __DIR__.'/route.php',
        ],
        'view' => [
            'class' => 'app\components\View',
        ],
        'assetManager' => [
            //'baseUrl' => '@cdn/assets',
            'bundles' => [
                'yii\web\JqueryAsset' => [
                    'sourcePath' => '@bower',
                    'js' => [
                        'jquery/dist/jquery.min.js',
                    ]
                ],
                'yii\bootstrap\BootstrapAsset' => [
                    'css' => [],
                ],
            ],
        ],
        'settings' => [
            'class' => 'app\components\Settings',
        ],
        'category' => [
            'class' => 'app\modules\course\components\Category',
            'urlWithId' => false,
        ],
        'facebook' => [
            'class' => 'app\modules\user\components\Facebook',
        ],
    ],
    'params' => $params,
];
