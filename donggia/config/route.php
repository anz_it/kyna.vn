<?php

return [
    'dang-ky'       => '/user/registration/register',
    'dang-nhap'     => '/user/security/login',
    'course-register' => 'site/add',
    // single course
    '<slug:[a-z0-9\-]+>/<code:[a-zA-Z0-9\-]+>' => 'course/default/view',// single course
    '<slug:[a-z0-9\-]+>' => 'course/default/view',
    'trang-ca-nhan/khoa-hoc'  => '/user/course/index',
];
