<?php

namespace app\assets;

use yii\web\AssetBundle;
use \yii\web\View;

/**
 * This is class asset bunle for `home` layout
 */
class HomeAsset extends AssetBundle
{

    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        // FontAwesome
        "src/css/main.min.css",
        "src/css/bootstrap-dialog.css",
        // Style main
        "src/css/donggia.css",
    ];
    public $js = [
        ["src/js/bootstrap.min.js", 'position' => View::POS_END],
        ["src/js/bootstrap-dialog.js", 'position' => View::POS_END],
        ["src/js/main.js", 'position' => View::POS_END],
        ["src/js/details.js?v=1521199186", 'position' => View::POS_END],
        // ajax
        ["src/js/ajax-caller.js", 'position' => View::POS_END],
        ["src/js/add-to-cart.js?v=1514949776", 'position' => View::POS_END],
        ["src/js/facebook.js", 'position' => View::POS_END],

    ];
    public $depends = [
        'yii\web\YiiAsset',
    ];
    public $jsOptions = ['position' => View::POS_HEAD];
}
