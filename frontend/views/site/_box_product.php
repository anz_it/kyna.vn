<?php

use yii\helpers\StringHelper;
use common\helpers\CDNHelper;
use kyna\course\models\Course;
use frontend\modules\course\widgets\CountDownTimer;
use frontend\modules\course\widgets\VoucherFree;
use frontend\modules\course\widgets\DongGia;
use frontend\modules\course\widgets\HotSale;
use frontend\modules\course\widgets\DongGiaFlashSaleTet;
use frontend\modules\course\widgets\CountDownCampaignTetTimer;
$formatter = \Yii::$app->formatter;
$flag = false;
if (Yii::$app->devicedetect->isMobile() || Yii::$app->devicedetect->isTablet()) {
    $flag = true;
}
if (is_array($model)) {
    $model = (object)$model;
}

$isPopupModal = false;

$totalTimePartial = Course::getTotalTimeInfo($model->total_time);

$cdnUrl = CDNHelper::getMediaLink();
?>

<div class="k-box-card-wrap clearfix" data-id="<?= $model->id?>" data-course-type="<?= $model->type ?>">
    <div class="img">
        <?= CDNHelper::image($model->image_url, [
            'alt' => $model->name,
            'class' => 'img-fluid',
            'size' => CDNHelper::IMG_SIZE_THUMBNAIL,
            'resizeMode' => 'cover',
        ]) ?>

        <div class="label-wrap">
            <?php if (!empty($model->is_birthday_discount) && Yii::$app->params['campaign_birthday']): ?>
              <img src="<?= $cdnUrl ?>/img/sinh-nhat-kyna/tag.png" alt="">
            <?php endif;?>
            <?php if(HotSale::isDateRunCampaignHotSale()):?>
                <?= HotSale::widget(['course_id' => $model->id])?>
            <?php else:?>
                <?php if ($model->is_new): ?>
                    <span class="lb-new">NEW</span>
                <?php endif; ?>

                <?php if ($model->is_hot): ?>
                    <span class="lb-hot">HOT</span>
                <?php endif; ?>
            <?php endif;?>

            <?= VoucherFree::widget(['course_id' => $model->id])?>
            <?= DongGia::widget(['course_id' => $model->id])?>
            <?= DongGiaFlashSaleTet::widget(['course_id' => $model->id])?>
        </div>
        <!-- end .label-wrap -->
        <!-- <div class="teacher mb">
            <ul>
                <li>
                    <?= CDNHelper::image($model->teacher_avatar, [
                        'alt' => $model->teacher_name,
                        'class' => 'img-teacher',
                        'size' => CDNHelper::IMG_SIZE_AVATAR_SMALL,
                        'resizeMode' => 'crop',
                    ]) ?>
                </li>
                <li>
                    <?= $model->teacher_name ?>
                </li>
            </ul>
        </div> -->

        <!--start course rating-->
        <?= $this->render('_rating', ['model' => $model]) ?>
        <!--end rating-->

        <span class="background-detail">
          <span class="wrap-position">
              <?php if ($flag == false) { ?>
                  <div class="inner">
                    <a href="<?= $model->url ?>" data-ajax data-toggle="popup" data-target="#modal">Xem nhanh</a>
                    <a href="<?= $model->url ?>" class="view-detail">Xem chi tiết</a>
                  </div>
              <?php } else { ?>
                  <a href="<?= $model->url ?>">Xem nhanh</a>
              <?php } ?>
          </span>
        </span>
    </div>
    <!--end .img-->

    <div class="content">
        <div class="box-style">
            <?php if ($model->type == Course::TYPE_VIDEO): ?>
            <span class="st-video"><i class="fa fa-youtube-play" aria-hidden="true"></i> Khóa học video</span>
            <?php elseif ($model->type == Course::TYPE_SOFTWARE): ?>
            <span class="st-app"><i class="fa fa-mobile" aria-hidden="true"></i> Phần mềm</span>
            <?php endif; ?>
            <span class="time pc"><i class="fa fa-clock-o" aria-hidden="true"></i> <?= $model->total_time_text ?></span>
        </div>
        <h4><?= $model->name ?></h4>
        <span class="author"><?= $model->teacher_name ?></span>
        <span class="major"><?= $model->teacher_title ?></span>
    </div>

    <!--end .content -->


    <div class="content-mb">
        <b><?= $model->teacher_name ?></b><span><b>,</b> <?= $model->teacher_title ?></span>
    </div>

    <!--end .content mb -->
    <div class="view-price">
        <ul>
            <?php if (!empty($model->discount_amount)) : ?>
                <li class="price"><strong><?= number_format($model->sell_price, "0", "", ".") ?>đ</strong>
                </li>
                <li class="sale">
                    <span><?= number_format($model->old_price, "0", "", ".") ?>đ</span>
                    <div class="label-discount">
                        <?php if ($model->discount_percent > 0): ?>
                            (-<?= $model->discount_percent ?>%)
                        <?php endif; ?>
                    </div>
                </li>
            <?php elseif (!empty($model->old_price)) : ?>
                <li class="price"><strong><?= number_format($model->old_price, "0", "", ".") ?>đ</strong></li>
            <?php else : ?>
                <li class="price"><strong>Miễn phí</strong></li>
            <?php endif; ?>
        </ul>
        <?= CountDownTimer::widget(['course_id' => $model->id])?>
        <?= CountDownCampaignTetTimer::widget(['course_id' => $model->id])?>
    </div>
    <!--end .view-price-->


    <div class="view-price-mb">
        <div class="student">
            <div class="number"><?= $model->total_users_count ?></div>
            <div class="text">học viên</div>
        </div>
        <div class="time">
            <div class="number"><?= $totalTimePartial['number'] ?></div>
            <div class="text"><?= $totalTimePartial['unit'] ?></div>
        </div>
        <div class="price">
            <div class="label-price">
                <?php if (!empty($model->discount_amount)) : ?>
                    <div class="first"><?= number_format($model->sell_price, "0", "", ".") ?>đ</div>
                    <div class="last"><s><?= number_format($model->old_price, "0", "", ".") ?>đ</s></div>
                <?php elseif (!empty($model->old_price)) : ?>
                    <div class="first"><?= number_format($model->old_price, "0", "", ".") ?>đ</div>
                <?php else : ?>
                    <div class="first">Miễn phí</div>
                <?php endif; ?>
            </div>
        </div>
    </div>

    <!--end .view-price mb-->

    <?php if ($flag == false) { ?>
        <?php if ($isPopupModal) { ?>
            <a href="<?= $model->url ?>" class="link-wrap" data-ajax data-toggle="popup" data-target="#modal"></a>
        <?php } else { ?>
            <a href="<?= $model->url ?>" class="link-wrap"></a>
        <?php } ?>
    <?php } else { ?>
        <a href="<?= $model->url ?>" class="link-wrap"></a>
    <?php } ?>
</div>
<?php if ($flag == false) { ?>
    <?php if ($isPopupModal) { ?>
        <a href="<?= $model->url ?>" class="card-popup" data-ajax data-toggle="popup" data-target="#modal"></a>
    <?php } else { ?>
        <a href="<?= $model->url ?>" class="card-popup"></a>
    <?php } ?>
<?php } else { ?>
    <a href="<?= $model->url ?>" class="card-popup"></a>
<?php } ?>
<!--end .wrap-->
