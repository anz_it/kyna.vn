<?php
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\bootstrap\Html;
use kyna\tag\models\Tag;
use common\helpers\CDNHelper;
use kyna\home\models\HomeTeacher;
use kyna\course\models\Course;
use yii\widgets\ListView;
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                
$this->title = "Kyna.vn - Học online cùng chuyên gia";
$cdnUrl = CDNHelper::getMediaLink();
$hotKeywords = Tag::topTags(Tag::TYPE_DESKTOP, 10);
$hotMobileKeywords = Tag::topTags(Tag::TYPE_MOBILE, 10);
$homeTeachers = HomeTeacher::topTeachers(4);

// Set breadcrumbs css
$this->registerCss("
    @media (max-width: 767px) {
        .breadcrumb-container {
            display: none;
        }
        
        #k-highlights h2 {
            text-align: left;
            margin-left: 5px;
            margin-bottom: 12px;
        }
    }
    .breadcrumb-container {
        margin-top: 67px;
        background-color: #fafafa;
    }
	@media (min-width: 768px) and (max-width: 991px){
		.breadcrumb-container{
			margin-top: 0;
		}
	}
    .breadcrumb {

        border-radius: 0px;
        padding: 8px 0px;
        margin: 0px;
        background-color: inherit;
    }
    .breadcrumb > li + li::before {
        padding-right: .5rem;
        padding-left: .5rem;
        color: #a0a0a0;
        content: \"»\";
    }
    .breadcrumb > li {
        color: #666 !important;
        font-weight: bold;
    }
    .breadcrumb > li > a {
        color: inherit;
    }
    .breadcrumb > li.active {
        color: #666 !important;
        font-weight: normal;
    }
    #k-listing {
        margin-top: 0px;
        padding-top: 30px;
    }

    #hot-courses h3 {
        display: none;
    }

    @media (min-width: 768px) {
        #hot-courses h3 {
            display: block;
            color: #50ad4e;
            font-size: 18px;
            padding-left: 15px;
            padding-right: 14px;
            padding-bottom: 22px;
        }

        #best-seller-courses h3 {
            display: block;
            color: #50ad4e;
            font-size: 18px;
            padding-left: 15px;
        }
    }


    .hot-category .name {
        position: absolute;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        text-align: center;
        padding-top: 22%;
        font-size: large;
        font-weight: bolder;
        color: #FFFFFF;
    }

    .hot-category:hover .overlay {
        width:100%;
        height:100%;
        position:absolute;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        background-color:#000;
        opacity:0.2;
        border-radius:1px;
    }
    


");

?>

<main>
    <section>
        <div id="k-banner" class="clearfix k-height-header pc">
            <div class="container">
                <div class="wrap-content col-md-10 col-md-offset-1 pd0">
                    <h1 class="title"><b>Tìm khóa học bạn đang quan tâm</b></h1>
                    <?php $form = ActiveForm::begin([
                        'id' => 'search-form-index',
                        'action' => Url::toRoute(['/course/default/index']),
                        'method' => 'get',
                        'options' => [
                            'class' => 'clearfix form-search'
                        ]
                    ]); ?>
                    <input type="text" name="q" class="form-control" placeholder="Nhập từ khóa để tìm khóa học bạn cần">
                    <button class="btn btn-default" type="submit">
                        <i class="icon-search icon"></i>
                    </button>
                    <?php ActiveForm::end(); ?>
                </div>
                <!--end .wrap-content-banner-top -->
            </div><!--end .container-->
        </div><!--end #k-banner-->

        <?= \frontend\widgets\SliderWidget::widget() ?>
    </section>

    <section class="tags-what-learn-today none_on_mobile">
        <div class="container">
            <h2 class="text-center">Bạn muốn học gì hôm nay</h2>
            <div class="wrapper-tags">
                <?php foreach ($hotKeywords as $tag):
                    $tagImage = CDNHelper::image($tag->image_url, [
                        'alt' => $tag->tag,
                        'class' => 'img-fluid',
                        'size' => CDNHelper::IMG_SIZE_ORIGINAL,
                        'returnMode' => CDNHelper::IMG_RETURN_MODE_URL,
                        'resizeMode' => 'cover',
                    ]);
                    ?>
                    <div class="item">
                        <div class="box" style="background-image: url('<?= $tagImage ?>');">
                            <a href="<?= Url::toRoute(['/course/default/index', 'tag' => $tag->slug]); ?>" title="<?= $tag->title ?>">
                                <span><?= $tag->tag; ?></span>
                            </a>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </section>  <!-- End section tags-what-learn-today-->

    <section class="tags-what-learn-today none_on_desktop">
        <div class="container">
            <h2 class="text-center">Bạn muốn học gì hôm nay</h2>
            <div class="wrapper-tags">
                <?php foreach ($hotMobileKeywords as $tag):
                    $tagImage = CDNHelper::image($tag->image_url, [
                        'alt' => $tag->tag,
                        'class' => 'img-fluid',
                        'size' => CDNHelper::IMG_SIZE_ORIGINAL,
                        'returnMode' => CDNHelper::IMG_RETURN_MODE_URL,
                        'resizeMode' => 'cover',
                    ]);
                    ?>
                    <div class="item">
                        <div class="box" style="background-image: url('<?= $tagImage ?>');">
                            <a href="<?= Url::toRoute(['/course/default/index', 'tag' => $tag->slug]); ?>" title="<?= $tag->title ?>">
                                <span><?= $tag->tag; ?></span>
                            </a>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </section>  <!-- End section tags-what-learn-today-->

    <section>
        <div id="k-highlights" class="container">
            <h2 class="title"><b>Khóa học nổi bật cho bạn</b></h2>

            <?= ListView::widget([
                'dataProvider' => $hotCourses,
                'layout' => "{items}\n",
                'itemView' => function ($model) {
                    $model = (object)$model;
                    return $this->render($model->type == Course::TYPE_COMBO ? '_box_combo' : '_box_product', ['model' => $model]);
                },
                'options' => [
                    'tag' => 'ul',
                    'class' => 'clearfix k-box-card-list'
                ],
                'itemOptions' => [
                    'tag' => 'li',
                    'class' => 'col-xl-3 col-lg-4 col-md-6 col-xs-12 k-box-card'
                ],
            ])
            ?>
        <span class="button-more">
            <a href="/danh-sach-khoa-hoc" class="btn btn-primary-kyna">Xem tất cả khóa học</a>
        </span>
        </div><!--end #k-highlights-->
    </section>


    <section>
        <div id="k-supply-list">
            <div class="container k-supply-list-wrap">
                <div class="k-supply-list-inner col-lg-10 col-lg-offset-1 col-sm-12">
                    <h2 class="title"><b>Danh mục khóa học đang cung cấp</b></h2>
                    <div class="k-supply-list-box clearfix">
                        <ul class="desktop">
                            <?php foreach ($this->context->rootCats as $key => $rootCat) { ?>
                                <li>
                                    <a href="<?= $rootCat->url ?>" title="<?= $rootCat->name; ?>">
                                        <span class="text"><?= $rootCat->name; ?></span>
                                        <img class="icon" src="<?= $cdnUrl . (!empty($rootCat->home_icon) ? $rootCat->home_icon : '/src/img/home/icon-list-course.png') ?>" alt="">
                                    </a>
                                </li>
                            <?php } ?>
                        </ul>

                        <ul class="mobile">
                            <?php foreach ($this->context->rootCats as $key => $rootCat) { ?>
                                <?php if ($key < 9) { ?>
                                    <li>
                                        <a href="<?= $rootCat->url ?>" title="<?= $rootCat->name; ?>">
                                            <img class="icon" src="<?= $cdnUrl . (!empty($rootCat->home_icon) ? $rootCat->home_icon : '/src/img/home/icon-list-course.png') ?>" alt="">
                                            <span class="text"><?= $rootCat->name; ?></span>
                                        </a>
                                    </li>
                                <?php } ?>
                            <?php } ?>
                            <li>
                                  <span>
                                    <a href="<?= Url::toRoute(['/course/default/index']); ?>" class="title button">
                                        XEM TẤT CẢ DANH MỤC
                                    </a>
                                  </span>
                            </li>
                        </ul>
                    </div>
                    <!--end .k-supply-list-box-->
                </div>
                <!--end .wrap-content-banner-bottom-->
            </div>
            <!--end .k-supply-list-wrap-->
        </div><!--end #k-supply-list-->
    </section>



    <section>
        <div id="k-about-us" class="clearfix pc d-none d-sm-block">
            <div class="col-lg-6 col-md-12 col-xs-12 k-about-us-background"></div>
            <div class="container">
                <div class="col-lg-6 col-md-12 col-xs-12 k-about-us-why">
                    <h2 class="title"><b>Tại sao nên chọn Kyna?</b></h2>
                    <ul>
                        <li>
                    <span class="icon">
                    <img src="<?= $cdnUrl ?>/src/img/home/hoc-cung-chuyen-gia.svg" alt="Học cùng chuyên gia" class="img-fluid">
                    </span>
                    <span class="text">
                    <span>Học cùng chuyên gia: </span>
                    Tương tác với chuyên gia, nhận sự trợ giúp từ chuyên gia.
                    </span>
                        </li>
                        <li>
                    <span class="icon">
                    <img src="<?= $cdnUrl ?>/src/img/home/hoc-mai-mai.svg" alt="Thanh toán một lần" class="img-fluid">
                    </span>
                    <span class="text">
                    <span>Thanh toán một lần: </span>
                    Phương thức thanh toán linh hoạt, thanh toán một lần sở hữu bài học mãi mãi.
                    </span>
                        </li>
                        <li>
                    <span class="icon">
                    <img src="<?= $cdnUrl ?>/src/img/home/hoc-moi-noi.svg" alt="Mọi lúc mọi nơi" class="img-fluid">
                    </span>
                    <span class="text">
                    <span>Mọi lúc mọi nơi: </span>
                    Học mọi lúc mọi nơi qua điện thoại, máy tính, ipad có kết nối Internet.
                    </span>
                        </li>
                        <li>
                    <span class="icon">
                    <img src="<?= $cdnUrl ?>/src/img/home/hoan-tien.svg" alt="Cam kết hoàn tiền" class="img-fluid">
                    </span>
                    <span class="text">
                    <span>Cam kết hoàn tiền: </span>
                    Học viên được hoàn tiền học phí nếu thấy khóa học không hiệu quả.
                    </span>
                        </li>
                    </ul>
                </div>
                <!--end .col-md-6 col-xs-12 left-->
                <div class="col-lg-6 col-md-12 col-xs-12 k-about-us-comment">
                    <h2 class="title"><b>Học viên nói về chúng tôi</b></h2>
                    <div class="k-about-us-slide">
                        <ul>
                            <li>
                                <div class="k-about-us-customer clearfix">
                                    <div class="col-md-4 col-xs-12 img">
                                        <img src="<?= $cdnUrl ?>/src/img/home/hinhme1.png" alt="Chị Lê Hương" class="img-fluid"/>
                                    </div><!--end .col-md-4 col-xs-12 left-->
                                    <div class="col-md-8 col-xs-12 text">
                                        <span>Chị Lê Hương - mẹ bé Quỳnh Anh (Tp.HCM)</span>
                                        <span>Cùng con tham gia khóa <b>"Học tính toán nhanh cùng bàn tính Soroban"</b></span>
                                    </div><!--end .col-md-8 col-xs-12 right-->
                                </div><!--end .detail-->
                                <p>Vì muốn Quỳnh Anh có sự chuẩn bị thật tốt trước khi vào lớp một nên mình cho bé làm quen với toán sớm. Nhưng khi nhắc đến toán thì bé cứ lơ đi, không có hứng thú. Tình cờ mình biết đến khóa học toán online của Kyna, rất bất ngờ là bé rất vui vẻ và học rất chăm chỉ. Nhờ khóa học mà Quỳnh Anh nhà mình giờ đây không còn sợ toán nữa, cách học thuận tiện của Kyna cũng giúp mình được bên con nhiều hơn và theo dõi việc học của con dễ dàng.</p>
                            </li>

                            <li>
                                <div class="k-about-us-customer clearfix">
                                    <div class="col-md-4 col-xs-12 img">
                                        <img src="<?= $cdnUrl ?>/src/img/home/hinhme2.png" alt="Phạm Nguyên Vũ" class="img-fluid"/>
                                    </div><!--end .col-md-4 col-xs-12 left-->
                                    <div class="col-md-8 col-xs-12 text">
                                        <span>Anh Phạm Nguyên Vũ (Tp.HCM)</span>
                                        <span>Học viên khóa <b>"Kỹ năng thuyết phục khách hàng và xử lý từ chối"</b></span>
                                    </div><!--end .col-md-8 col-xs-12 right-->
                                </div><!--end .detail-->
                                <p>Làm kinh doanh, điều quan trọng nhất đối với tôi đó chính là khách hàng. Vì có họ thì mình mới tồn tại được. Mấy năm trước do còn non kinh nghiệm nên tôi luôn gặp vấn đề với những “thượng đế” của mình. Nhưng bây giờ thì đã khác, từ khi tham gia khóa học của thầy Lê Kim Tú và ứng dụng những gì thầy dạy vào thực tế, tôi trở nên tự tin và chủ động hơn, biết cách nắm bắt tâm lý để từ đó thuyết phục khách hàng. Khi họ từ chối cũng không sao, tôi cũng đã biết cách tạo cơ hội tốt để hợp tác với họ vào một dịp khác trong tương lai.</p>
                            </li>

                            <li>
                                <div class="k-about-us-customer clearfix">
                                    <div class="col-md-4 col-xs-12 img">
                                        <img src="<?= $cdnUrl ?>/src/img/home/hinhme3.png" alt="Nguyễn Hoàng Thảo Uyên" class="img-fluid"/>
                                    </div><!--end .col-md-4 col-xs-12 left-->
                                    <div class="col-md-8 col-xs-12 text">
                                        <span>Bạn Nguyễn Hoàng Thảo Uyên (Huế)</span>
                                        <span>Học viên khóa <b>"Tự học tiếng Nhật cho người mới bắt đầu"</b></span>
                                    </div><!--end .col-md-8 col-xs-12 right-->
                                </div><!--end .detail-->
                                <p>Mình mê Nhật Bản từ những bộ phim hoạt hình và luôn mong một ngày nào đó sẽ biết tiếng Nhật. Vậy mà, đi học rồi ra trường đi làm bận rộn, mãi vẫn không thực hiện được. Hồi đầu năm vừa rồi, ngồi lên plan cho năm mới thì quyết tâm là sẽ học tiếng Nhật thôi. Thế là tham gia khóa học tại Kyna. Vì học online nên không ảnh hưởng lắm đến công việc, chương trình học được biên soạn rất khoa học, bài bản, sinh động nữa,… nên mình học và tiếp thu rất nhanh. Đến giờ qua năm mới rồi, mình đã có một vốn tiếng Nhật kha khá và đặc biệt là hoàn thành được mục tiêu. Hehe! ^^</p>
                            </li>
                        </ul>

                    </div><!--end .wrap-slider-content-bottom-->
                </div>
                <!--end .col-md-6 col-xs-12 right-->
            </div>
            <!--end .container-->
        </div>
        <!--end #wrap-content-bottom-->
    </section>      <!--End section k-about-us-->


    <section>
        <div id="k-teaching-kyna">
            <div class="k-teaching-kyna-wrap">
                <?php foreach ($homeTeachers as $index => $teacher): ?>
                <div class="item">
                    <img class="bg-item" src="<?= $cdnUrl . $teacher->image_url ?>" alt="">
                    <div class="box-item">
                        <div class="name"><?= $teacher->name ?></div>
                        <div class="major"><?= $teacher->title ?></div>
                    </div>
                </div>
                <?php if ($index == 1): ?>
                <div class="item">
                    <img class="bg-item" src="<?= $cdnUrl ?>/img/kyna-teach/bg-centerbox.png" alt="">
                    <div class="box-item">
                        <div class="box-middle">
                            <img src="<?= $cdnUrl ?>/img/kyna-teach/icon-teach.png" alt="">
                            <div class="title">Tham gia<br> giảng dạy tại Kyna</div>
                            <a href="/p/kyna/giang-day" class="btn btn-see-more">Tìm hiểu thêm</a>
                        </div>
                    </div>
                </div>
                <?php endif; ?>
                <?php endforeach; ?>
            </div>
        </div>

        <div class="clearfix k-teaching">
            <div class="col-xs-5 img">
                <img src="<?= $cdnUrl ?>/src/img/mobile/teaching.png" alt=""/>
            </div><!--end .img -->
            <div class="col-xs-7 text">
                <a href="/p/kyna/gioi-thieu" class="btn">Giới thiệu về Kyna</a>
            </div><!--end .text -->
        </div><!--end .k-teaching-->
    </section>

    <!-- <div id="feedback-v3">
        <a href="/p/kyna/gop-y-phien-ban-moi?utm_source=website&utm_medium=mainsite&utm_campaign=surveylaunch" target="_blank">
            <span>Góp ý</span>
            <img src="../src/img/icon-feedback.png" alt="Góp ý">
        </a>
    </div> -->


    <script type="text/javascript">
      jQuery(document).ready(function($) {
        $(document).on('shown.bs.modal', "#modal", function () {
          setTimeout(function () {
            FB.XFBML.parse();
          }, 2000);
        });
      });
      $zopim(function() {
        $zopim.livechat.window.onShow(function () {
          $('#feedback').css('bottom', '450px');
        });
        $zopim.livechat.window.onHide(function () {
          $('#feedback').css('bottom', '40px');
        })
      });
    </script>
</main>
