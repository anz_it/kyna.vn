<?php

use yii\helpers\Url;
use kyna\tag\models\Tag;
use common\helpers\CDNHelper;

$cdnUrl = CDNHelper::getMediaLink();

$this->title = "Kyna.vn | Bảo Trì";

$settings = Yii::$app->controller->settings;
$hotKeywords = Tag::topTags(Tag::TYPE_DESKTOP, 10);
?>
<main>

    <section id="k-page-503">
        <div class="wrapper-content">
            <img class="logo" src="<?= $cdnUrl ?>/img/logo/Kynavnraftlogo.svg" alt="logo">
            <h1>Đã có lỗi không mong muốn xảy ra.</h1>
            <img class="img-fluid icon-img" src="<?= $cdnUrl ?>/src/img/503/503.png" alt="503">
            <p>Xin lỗi vì sự bất tiện. Chúng tôi đang nghiên cứu và khắc phục. Bạn hãy quay lại sau nhé!</p>
        </div>
    </section>
    
</main>
