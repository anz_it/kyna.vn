<?php
/**
 * Created by PhpStorm.
 * User: nguyenphanngu
 * Date: 9/8/17
 * Time: 10:57 AM
 */

/**
 * @var $this \yii\web\View
 */

use yii\helpers\StringHelper;
use common\helpers\CDNHelper;
use kyna\course\models\Course;
use yii\widgets\ListView;
use yii\web\View;

$formatter = \Yii::$app->formatter;
?>

<?= ListView::widget([
    'dataProvider' => $dataProvider,
    'layout' => "{items}\n",
    'itemView' => function ($model) {
        $model = (object)$model;
        return $this->render($model->type == Course::TYPE_COMBO ? '_box_combo' : '_box_product', ['model' => $model]);
    },
    'options' => [
        'tag' => 'ul',
        'class' => 'k-box-card-list hot-courses-container'
    ],
    'itemOptions' => [
        'tag' => 'li',
        'class' => 'col-xl-4 col-lg-6 col-xs-12 k-box-card'
    ],
])
?>
<?php
    $hotBoxJx = "        
        var slickSettings_1200px = {
            infinite: true,
            // dots: true,
            slidesToShow: 2,
            slidesToScroll: 1,
             autoplay: true,
             autoplaySpeed: 5000
        }
        
        var slickSettings_992px = {
            infinite: true,
            // dots: true,
            slidesToShow: 1,
            slidesToScroll: 1,
             autoplay: true,
             autoplaySpeed: 5000
        }
        
        var slickSettings = {
            infinite: true,
            // dots: true,
            slidesToShow: 4,
            slidesToScroll: 1,
            autoplay: true,
            autoplaySpeed: 5000,
            prevArrow: $('.previous'),
            nextArrow: $('.next'),
            responsive: [
                {
                    breakpoint: 1200,
                    settings: slickSettings_1200px
                },
                {
                    breakpoint: 992,
                    settings: slickSettings_992px
                }
                
            ]
        };

        
    ";

    $hotBoxCss = "
        .slider-slick{
            float: right;
        }

        .slider-slick a {
            text-decoration: none;
            display: inline-block;
            padding: 1px 11px;
        }

        .slider-slick a:hover {
            background-color: #ddd;
            color: black;
        }

        .slider-slick .previous {
            background-color: #4CAF50;
            color: white;
            font-size: 22px;
        }

        .slider-slick .next {
            background-color: #4CAF50;
            color: white;
            font-size: 22px;
        }

        slider-slick. .round {
            border-radius: 50%;
        }
    ";

    $this->registerJs($hotBoxJx, View::POS_END);
    $this->registerCss($hotBoxCss, ['position' => View::POS_END]);
?>
