<?php
use common\helpers\CDNHelper;

$cdnUrl = CDNHelper::getMediaLink();
$settings = Yii::$app->controller->settings;
$logoImage = $settings['lp_logo_image'];
$logoUrl = $settings['lp_logo_url'];
?>
<div class="wrap-header clearfix mb">
    <div class="col-xs-4 left">
        <ul class="clearfix main">
            <li class="menu"><a id="menu-main-mb" href="#sidr"><span><i class="fa fa-bars"></i></span></a></li>
            <li class="search"><a href="#" id="button-search-mobile"><span><i class="fa fa-search"></i></span></a></li>
        </ul>

        <div id="sidr" class="menu-mobile-main">
            <!-- Your content -->
            <ul class="clearfix">
                <li><i class="fa fa-align-justify"></i> Danh m?c khóa h?c</li>
                <li>
                    <a href="#">
                        <span class="title"><i class="fa fa-align-justify"></i> Giao ti?p</span>
                        <span class="sub-title">Giao ti?p, thuy?t trình, thuong lu?ng</span>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <span class="title"><i class="fa fa-align-justify"></i> Giao ti?p</span>
                        <span class="sub-title">Giao ti?p, thuy?t trình, thuong lu?ng</span>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <span class="title"><i class="fa fa-align-justify"></i> Giao ti?p</span>
                        <span class="sub-title">Giao ti?p, thuy?t trình, thuong lu?ng</span>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <span class="title"><i class="fa fa-align-justify"></i> Giao ti?p</span>
                        <span class="sub-title">Giao ti?p, thuy?t trình, thuong lu?ng</span>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <span class="title"><i class="fa fa-align-justify"></i> Giao ti?p</span>
                        <span class="sub-title">Giao ti?p, thuy?t trình, thuong lu?ng</span>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <span class="title"><i class="fa fa-align-justify"></i> Giao ti?p</span>
                        <span class="sub-title">Giao ti?p, thuy?t trình, thuong lu?ng</span>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <span class="title"><i class="fa fa-align-justify"></i> Giao ti?p</span>
                        <span class="sub-title">Giao ti?p, thuy?t trình, thuong lu?ng</span>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <span class="title"><i class="fa fa-align-justify"></i> Giao ti?p</span>
                        <span class="sub-title">Giao ti?p, thuy?t trình, thuong lu?ng</span>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <span class="title"><i class="fa fa-align-justify"></i> Giao ti?p</span>
                        <span class="sub-title">Giao ti?p, thuy?t trình, thuong lu?ng</span>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <span class="title"><i class="fa fa-align-justify"></i> Giao ti?p</span>
                        <span class="sub-title">Giao ti?p, thuy?t trình, thuong lu?ng</span>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <span class="title"><i class="fa fa-align-justify"></i> Giao ti?p</span>
                        <span class="sub-title">Giao ti?p, thuy?t trình, thuong lu?ng</span>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <span class="title"><i class="fa fa-align-justify"></i> Giao ti?p</span>
                        <span class="sub-title">Giao ti?p, thuy?t trình, thuong lu?ng</span>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <span class="title"><i class="fa fa-align-justify"></i> Giao ti?p</span>
                        <span class="sub-title">Giao ti?p, thuy?t trình, thuong lu?ng</span>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <span class="title"><i class="fa fa-align-justify"></i> Giao ti?p</span>
                        <span class="sub-title">Giao ti?p, thuy?t trình, thuong lu?ng</span>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <span class="title"><i class="fa fa-align-justify"></i> Giao ti?p</span>
                        <span class="sub-title">Giao ti?p, thuy?t trình, thuong lu?ng</span>
                    </a>
                </li>
            </ul>
        </div>

        <script>
            $(document).ready(function() {
                $('#menu-main-mb').sidr();
            });
        </script>
    </div><!-- col-xs-4 left-->
    <div class="col-xs-4 center">
        <h2 class="logo"><a href="<?= $logoUrl ?>"><img src="<?= $logoImage ?>" alt="Kyna.vn" class="img-responsive" /></a></h2>
    </div><!--end col-xs-4 center-->
    <div class="col-xs-4 right">
        <a href="#" class="login" data-toggle="modal" data-target="#popup-login-mobile">Đăng nhập</a>
    </div><!-- col-xs-4 right-->
</div><!--end .wrap-header-mb-->

<div class="col-sm-12 wrap-search-mobile">
    <div class="input-group wrap-form-search">
        <form class="clearfix">
            <input type="text" class="form-control" class="search-form" placeholder="">
            <span class="input-group-btn icon">
                <button class="btn btn-default" type="button"><i class="fa fa-search"></i></button>
            </span>
            <p class="text input">Tìm ki?m khóa h?c
              <span
                  class="txt-rotate"
                  data-period="2000"
                  data-rotate='[ "Marketing.", "Giao ti?p.", "Nuôi d?y con.", "Công ngh?.", "Kinh doanh!", "..." ]'>
              </span>
            </p>
        </form>
    </div><!-- /input-group -->
</div><!--end .col-sm-12 wrap-search-->