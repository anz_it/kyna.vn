<?php

use common\helpers\CDNHelper;
use yii\helpers\Html;
use yii\helpers\Url;

$user = Yii::$app->user->identity;

$cdnUrl = CDNHelper::getMediaLink();

$flag = false;
if (Yii::$app->devicedetect->isMobile() || Yii::$app->devicedetect->isTablet()) {
	$flag = true;
}
?>


<ul class="nav navbar-nav k-header-info col-md-4 col-md-push-4 <?php echo $flag == false ? "" : "mob" ?>">
    <?php if ($flag == false) {
	?>
    <li class="cart dropdown">
        <a href="<?=Url::toRoute(['/cart/default/index'])?>" class="dropdown-toggle cart_anchor" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
            <span class="count-number"><?=Yii::$app->cart->getCount()?></span>
        </a>
        <?php echo $this->render("@app/views/layouts/common/short_cart") ?>
    </li>
    <li class="button-cod">
      <a class="btn btn-cod-active" href="<?=Url::toRoute(['/user/order/active-cod'])?>" data-toggle="popup" data-target="#activeCOD" data-ajax data-push-state=false>Kích hoạt COD</a>
    </li>

    <li class="account dropdown wrap">
        <a href="<?=Url::toRoute(['/user/course/index'])?>" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
            <div class="sub-wrap">
                <?php $avUrl = CDNHelper::image($user->avatarImage, [
		'size' => CDNHelper::IMG_SIZE_AVATAR_SMALL,
		'resizeMode' => 'crop',
		'returnMode' => 'url',
	]);
	if (!$avUrl) {
		$avUrl = '/img/default_avatar.png';
	}

	?>
                <img src="<?=$avUrl?>" class="img-responsive" />
                <div class="text">
                    <span class="user" ><?=$user->profile->name?></span>
                    <span class="title">Khóa học của tôi</span>
                </div>
            </div>
        </a>


        <ul class="dropdown-menu dropdown-user">
            <li class="inner clearfix">
                <ul>
                    <li><a href="<?=Url::toRoute(['/user/course/index'])?>"><span class="icon profile-my-courses"></span>Khóa học của tôi</a></li>
                    <li><a href="<?=Url::toRoute(['/user/point/index'])?>"><i class="icon fa fa-trophy" aria-hidden="true"></i></span>Điểm Kpoint</a></li>
                    <li><a href="<?=Url::toRoute(['/user/transaction/in-complete'])?>"><span class="icon profile-purchase"></span>Lịch sử giao dịch</a></li>
                    <li class="edit"><a href="<?=Url::toRoute(['/user/profile/edit'])?>" ><span class="icon profile-editinfo"></span>Chỉnh sửa thông tin</a></li>
                    <li><a href="<?=Url::toRoute(['/user/order/active-cod'])?>" data-toggle="popup" data-target="#activeCOD" data-ajax="" data-push-state="false"><span class="icon profile-activation-code"></span>Kích hoạt mã COD</a></li>
                    <li class="button-out">
                        <?=Html::beginForm(['/user/security/logout'], 'post')?>
                        <a href="javascript:" onclick="$(this).closest('form').submit()"><span class="icon profile-logout"><i class="icon icon-sign-out"></i></span>Thoát</a>
                        <?=Html::endForm()?>
                    </li>
                </ul>
            </li>
        </ul>

        <!--
        <ul class="dropdown-menu dropdown-login">
            <li><h4 class="title">Bắt đầu học trên Kyna bằng các cách sau</h4></li>
           <li><a href="" class="button-facebook" data-push-state="false" data-toggle="popup" data-target="#popup-register"><i class="fa fa-facebook"></i> Đăng nhập bằng facebook</a></li>
           <li><a href="#" class="button-google"><i class="fa fa-google" aria-hidden="true"></i> Đăng nhập bằng facebook</a></li>
           <li><a href="#" class="button-login" data-toggle="modal" data-target="#k-popup-account-login">Đăng nhập Kyna</a></li>
           <li><a href="#" class="button-register" data-toggle="modal" data-target="#k-popup-account-register">Đăng ký Kyna</a></li>
        </ul>
        -->
    </li>
    <?php } else {?>
        <li class="cart">
            <a href="<?=Url::toRoute(['/cart/default/index'])?>" class="dropdown-toggle cart_anchor">
                <img src="<?=$cdnUrl?>/img/cart/icon-cart-checkout.png" alt="">
                <span class="count-number"><?=Yii::$app->cart->getCount()?></span>
            </a>
        </li>
        <li class="button-cod">
          <a class="btn btn-cod-active" href="<?=Url::toRoute(['/user/order/active-cod'])?>">Kích hoạt COD</a>
        </li>
        <li class="account wrap borderradius">
            <a href="<?=Url::toRoute(['/trang-ca-nhan/khoa-hoc'])?>" class="dropdown-toggle-profile" >
                <span id="sub-name-mobile" class="sub-wrap">Vào học <!--<i class="icon-arrow-collapse-bottom icon"></i>--><span id="name-mobile" class="user" ><?=$user->profile->name?></span> </span>
            </a>
        </li>
    <?php }?>
</ul>
