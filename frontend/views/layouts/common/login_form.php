<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\modules\user\models\forms\LoginForm;

$model = \Yii::createObject(LoginForm::className());

?>

<!-- POPUP LOGIN -->
<div class="modal fade k-popup-account" id="k-popup-account-login" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="k-popup-account-close close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                 <h4 class="modal-title">Đăng nhập</h4>
            </div>
            <div class="modal-body clearfix">

                <ul class="k-popup-account-top">
                    <li>
                        <a class="facebook auth-link button-facebook" href="<?= Url::to(['/user/security/auth', 'authclient' => 'facebook'])?>" title="Facebook" data-popup-width="860" data-popup-height="480"><i class="icon-facebook"></i> Đăng nhập bằng Facebook</a>
                    </li>
                    <li>- Hoặc đăng nhập bằng tài khoản Kyna -</li>
                </ul>
                <?php $form = ActiveForm::begin([
                    'id' => 'login-form',
                    'action' => Url::toRoute(['/user/security/login']),
                    'method' => 'POST',
                    'enableClientValidation' => false
                ]);
                ?>
                    <?= $form->errorSummary($model, ['header' => '', 'class' => 'error-summary alert alert-warning']); ?>

                    <?= $form->field($model, 'login', ["template" => '<span class="icon icon-mail"></span>{input}{hint}'])->textInput(['placeholder' => 'Email của bạn', 'autofocus' => 'autofocus', 'class' => 'text form-control', 'tabindex' => '1', 'id' => 'user-login'])->label(false); ?>

                    <?= $form->field($model, 'password', ["template" => '<span class="icon icon-lock"></span>{input}{hint}'])->passwordInput(['placeholder' => 'Mật khẩu', 'class' => 'text form-control', 'tabindex' => '2', 'id' => 'user-password'])->label(false); ?>

                    <div class="button-submit">
                        <?= Html::submitButton('Đăng nhập', ['id' => 'btn-submit-login', 'tabindex' => '3']); ?>
                    </div><!--end .button-popup-->
                <?php ActiveForm::end(); ?>

                <a href="<?= Url::toRoute(['/user/recovery/request']) ?>" class="forgot-pass" data-target="#k-popup-account-reset" data-toggle="modal" data-ajax data-push-state="false">Quên mật khẩu</a>
                <ul class="k-popup-account-bottom">
                    <li>Nếu bạn chưa có tài khoản</li>
                    <li>
                        <a href="<?= Url::toRoute(['/user/registration/register']) ?>" data-target="#k-popup-account-register" data-toggle="modal" data-ajax data-push-state="false">Đăng ký</a>
                    </li>

                </ul>

            </div>
            <!--end .modal-body-->
        </div>
    </div>
</div>
<!-- END POPUP LOGIN -->
<?php $script = "
    ;(function($){
        $(document).ready(function(){
            $('#k-popup-account-login').on('hidden.bs.modal', function() {
                $('#login-form .error-summary').hide();
                $('#user-login').val('');
                $('#user-password').val('');
            })

            $('body').on('submit', '#login-form', function (event) {
                event.preventDefault();

                var data = $(this).serializeArray();
                data.push({name: 'currentUrl', value: window.location})

                $.post('/user/security/login', data, function (response) {
                    if (response.result) {
                        // login success
                        if (response.redirectUrl) {
                            window.location.replace(response.redirectUrl);
                        } else {
                            window.location.reload();
                        }
                    } else {
                        // login failed
                        var liHtml = '';
                        $.each(response.errors, function (key, value){
                            liHtml += '<li>' + value[0] + '</li>';
                        });

                        $('#login-form .error-summary ul').html(liHtml);
                        $('#login-form .error-summary').show();
                    }

                });
            });
        });
    })(jQuery);"
?>
<?php
$this->registerJs($script, \yii\web\View::POS_END, 'login-submit');
?>
