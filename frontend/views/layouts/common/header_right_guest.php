<?php

use common\helpers\CDNHelper;
use yii\helpers\Url;

$flag = false;
if (Yii::$app->devicedetect->isMobile() || Yii::$app->devicedetect->isTablet()) {
	$flag = true;
}

$cdnUrl = CDNHelper::getMediaLink();
?>

<ul class="nav navbar-nav k-header-info col-md-4 col-md-push-4 <?php echo $flag == false ? "" : "mob" ?>">
    <?php if ($flag == false) {?>
        <li class="cart dropdown">
            <a href="<?=Url::toRoute(['/cart/default/index'])?>" class="dropdown-toggle cart_anchor" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">

                <span class="count-number"><?=Yii::$app->cart->getCount()?></span>
            </a>
            <?php echo $this->render("@app/views/layouts/common/short_cart") ?>
        </li>
        <li class="button-cod">
            <a class="btn btn-cod-active" href="<?=Url::toRoute(['/user/order/active-cod'])?>" data-toggle="popup" data-target="#activeCOD" data-ajax="" data-push-state="false">Kích hoạt COD</a>
        </li>
        <li class="login-signup">
          <a href="/dang-nhap" class="button-login" data-toggle="modal" data-target="#k-popup-account-login" data-ajax="" data-push-state="false">Đăng nhập</a>
          <a href="/dang-ky" class="button-register" data-toggle="modal" data-target="#k-popup-account-register" data-ajax="" data-push-state="false">Đăng ký</a>
        </li>

    <?php } else {?>
        <li class="cart">
            <a href="<?=Url::toRoute(['/cart/default/index'])?>" class="dropdown-toggle cart_anchor" >
                <img src="<?=$cdnUrl?>/img/cart/icon-cart-checkout.png" alt="">
                <span class="count-number"><?=Yii::$app->cart->getCount()?></span>
            </a>
        </li>
        <li class="button-cod">
          <a class="btn btn-cod-active" href="<?=Url::toRoute(['/user/order/active-cod'])?>">Kích hoạt COD</a>
        </li>
        <li class="account wrap borderradius">
            <a href="<?=Url::toRoute(['/dang-nhap'])?>" class="dropdown-toggle-profile" >
                <span class="sub-wrap">Đăng nhập <!--<i class="icon-arrow-collapse-bottom icon"></i>--> </span>
            </a>
        </li>
    <?php }?>
</ul>
