<?php

use app\widgets\CategoryWidget;
use app\widgets\ElasticFacet;
use frontend\widgets\AnchorText;
use frontend\widgets\TopTags;
use frontend\widgets\BannerWidget;
use kyna\settings\models\Banner;
?>

<?= CategoryWidget::widget() ?>

<?= TopTags::widget() ?>

<?= BannerWidget::widget(['type' => Banner::TYPE_CROSS_PRODUCT_LEFT]) ?>
