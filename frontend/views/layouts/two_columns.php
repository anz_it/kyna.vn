<?php

use yii\widgets\Breadcrumbs;
use common\helpers\GoogleSnippetHelper;
use frontend\widgets\BannerWidget;
use kyna\settings\models\Banner;
use app\widgets\HeaderFacet;
use yii\web\View;

$this->beginContent('@app/views/layouts/main.php');
?>

<!-- Breadcrumbs start -->
<div class="breadcrumb-container">
    <div class="container">
        <?php
        $homeUrl = yii\helpers\Url::toRoute(['/'],true);
        echo Breadcrumbs::widget([
            'options' => [
                'class' => 'breadcrumb',
                'itemscope' => '',
                'itemtype' => 'http://schema.org/BreadcrumbList',
            ],
            'homeLink' => [
                'label' => '<i class="fa fa-home"></i> Trang chủ',
                'url' => $homeUrl,
                'encode' => false,
                'template' => GoogleSnippetHelper::renderBreadcrumbTemplate($homeUrl, "<i class='fa fa-home'></i> Trang chủ", 1),
            ],
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ])
        ?>
        <?php echo BannerWidget::widget(['type' => Banner::TYPE_TOP_SUB]) ?>
    </div>
</div>
<!-- Breadcrumbs end -->

<main>
    <?= HeaderFacet::widget() ?>
    <div id="k-listing" class="container k-height-header">
        <div class="col-lg-3 col-xs-12 k-listing-sidebar" id="offpage-listing-sidebar">
            <?php echo $this->render("@app/views/layouts/two_columns/left_side"); ?>
        </div>
        <!--end .k-listing-sidebar-->
        <div class="col-lg-9 col-xs-12 k-listing-content">
            <?= $content; ?>
        </div><!--end .k-listing-content-->
</div><!--end #k-listing-->
</main>

<?php $this->endContent();?>
