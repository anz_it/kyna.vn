<!DOCTYPE HTML>
<?php

use yii\web\View;

use common\helpers\CDNHelper;
use common\assets\BootstrapNotifyAsset;

use frontend\assets\MaintainAsset;


$cdnUrl = Yii::$app->controller->cdnUrl;

MaintainAsset::register($this);
//BootstrapNotifyAsset::register($this);
$cdnUrl = CDNHelper::getMediaLink();
$this->beginPage();

?>
<html  lang="<?= Yii::$app->language ?>">
<head data-user-id="<?= Yii::$app->user->id;?>">
    <?= $this->render('common/html_head') ?>
    <script type="text/javascript">
        var mediaBaseUrl = '<?= Yii::$app->params['media_link']?>';
    </script>
    <!-- Google Tag Manager -->
    <script type="text/javascript" src= "<?= Yii::$app->params['media_link']?>/src/js/gtm.js?v=1508382297"></script>
    <script type="text/javascript" src="<?= Yii::$app->params['media_link']?>/src/js/fb-pixel.js?v=1521778876"></script>
    <script type="text/javascript" src="<?= Yii::$app->params['media_link']?>/src/js/headScript.js?v=1521778878"></script>
    <!-- End Google Tag Manager -->
</head>

<body>
    <?php $this->beginBody()?>

        <?php echo Yii::$app->settings->bodyScript;?>


        <?= $content; ?>




    <?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
