<!DOCTYPE HTML>
<?php

use frontend\widgets\HeaderWidget;
use frontend\widgets\FooterWidget;
use frontend\assets\HomeAsset;
use common\assets\BootstrapNotifyAsset;

HomeAsset::register($this);
BootstrapNotifyAsset::register($this);
use yii\web\View;

use common\helpers\CDNHelper;
$cdnUrl = CDNHelper::getMediaLink();
$this->beginPage();


?>
<html  lang="<?= Yii::$app->language ?>">
<head data-user-id="<?= Yii::$app->user->id;?>">
    <?= $this->render('common/html_head') ?>
    <link rel="stylesheet" href="/css/font-awesome.min.css">
    <!-- Google Tag Manager -->
    <script type="text/javascript" src= "<?= Yii::$app->params['media_link']?>/src/js/gtm.js?v=1508382298"></script>
    <script type="text/javascript" src="<?= Yii::$app->params['media_link']?>/src/js/fb-pixel.js?v=1521778876"></script>
    <script type="text/javascript" src="<?= Yii::$app->params['media_link']?>/src/js/headScript.js?v=1521778878"></script>
    <!-- End Google Tag Manager -->
</head>

<body <?php if(!empty($this->context->bodyClass)) { ?> class="<?= $this->context->bodyClass; ?>" <?php } ?> >
    <?php $this->beginBody()?>

    <?php echo Yii::$app->settings->bodyScript ?>
        <?= HeaderWidget::widget(['rootCats' => !empty($this->context->rootCats) ? $this->context->rootCats : null]); ?>

        <?= $content; ?>

        <?= FooterWidget::widget(); ?>
        
    <?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
