<head>
    <script src="../../js/jquery-1.11.2.min.js"></script>
    <!-- Google Tag Manager -->
    <script type="text/javascript" src= "<?= Yii::$app->params['media_link']?>/src/js/gtm.js?v=1508382297"></script>
    <script type="text/javascript" src="<?= Yii::$app->params['media_link']?>/src/js/fb-pixel.js?v=1521778876"></script>
    <script type="text/javascript" src="<?= Yii::$app->params['media_link']?>/src/js/headScript.js?v=1521778878"></script>
    <!-- End Google Tag Manager -->
</head>
<?php echo Yii::$app->settings->bodyScript ?>
<?php
use yii\helpers\Json;
use common\helpers\CDNHelper;

$cdnUrl = CDNHelper::getMediaLink();

$mo_data = Json::decode(Yii::$app->session->getFlash('mo_data'));
$lead = Yii::$app->session->getFlash('lead');

if (count($mo_data) > 0) {
    ?>
    <script type="text/javascript">
        masoffer_order_info = {
            transaction_id: '<?php echo $mo_data['transaction_id'] ?>',
            offer_id: '<?php echo $mo_data['offer_id'] ?>',
            transaction_time: <?php echo $mo_data['transaction_time'] ?>,
            signature: '<?php echo $mo_data['signature'] ?>',
            traffic_id: '<?php echo $mo_data['traffic_id'] ?>',
            products: [
                {
                    id: '<?php echo $mo_data['products']['id'] ?>',
                    url: '<?php echo $mo_data['products']['url'] ?>',
                    price: <?php echo $mo_data['products']['price'] ?>,
                    name: '<?php echo $mo_data['products']['name'] ?>',
                    status_code: 0,
                    quantity: 1
                }
            ],
            form: {
                name: '<?php echo $mo_data['form']['name'] ?>',
                phone: '<?php echo $mo_data['form']['phone'] ?>',
                email: '<?php echo $mo_data['form']['email'] ?>',
                content: '<?php echo $mo_data['form']['content'] ?>'
            }
        };
    </script>
    <script type="text/javascript" src="https://static.masoffer.net/php/cpl_tracking_js.php?type=kyna"></script>
    <?php
} ?>

<!-- Success content -->
<div id="thankyou_modal" class="" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="md-body">
                <img src="<?= $cdnUrl ?>/img/landing-page/thankyou-icon.png" alt="thank you">
                <h3>Đăng ký thành công!</h3>
                <p>
                    Hotline: <b>1900.6364.09 </b>- Email: <b>hotro@kyna.vn</b>
                </p>
            </div>
        </div>
    </div>
    <style>
        #thankyou_modal .modal-content{
            padding: 15px 15px 50px;
        }
        #thankyou_modal img{
            display: block;
            width: 150px;
            margin: 30px auto;
        }
        #thankyou_modal h3{
            text-transform: uppercase;
            color: #50ad4e;
            font-size: 24px;
            font-weight: bold;
            text-align: center;
            margin: 40px auto 20px;
        }
        #thankyou_modal p{
            font-size: 14px !important;
            color: #000;
            text-align: center;
            margin: 10px auto;
        }
        #thankyou_modal .md-footer{
            padding-left: 0;
            padding-right: 0;
            margin-left: 0;
            margin-right: 0;
        }
        #thankyou_modal .md-footer h4{
            text-transform: uppercase;
            color: #000;
            font-weight: bold;
            text-align: center;
            margin: 50px auto 30px;
        }
        #thankyou_modal .md-footer ul{
            text-align: center;
            padding-left: 0;
        }
        #thankyou_modal .md-footer li{
            text-align: left;
            display: -webkit-inline-flex;
            display: -moz-inline-flex;
            display: -ms-inline-flex;
            display: inline-flex;
            align-items: center;
            width: 30%;
        }
        #thankyou_modal .md-footer span{
            display: inline-block;
            background: #50ad4e;
            width: 30px;
            height: 30px;
            padding: 5px 10px;
            font-weight: bold;
            color: #fff;
            border-radius: 50%;
            margin-right: 3px;
        }
        #thankyou_modal .md-footer a{
            color: #333;
            font-size: 14px;
            /*border-bottom: 1px dashed black;*/
            text-decoration: underline !important;
        }
    </style>
</div>

