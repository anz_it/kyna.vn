<?php
use common\helpers\CaptchaHelper;

$captchaParams = CaptchaHelper::getReCaptchaParams();
?>
<!-- Modal -->
<script src="https://www.google.com/recaptcha/api.js" async defer></script>
<div id="recaptcha_modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <form action="/page/default/submit" method="POST" name="recaptcha-form">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">Vui lòng xác nhận</h4>
                </div>
                <div class="modal-body">
                    <?php if (!empty($data)): ?>
                        <?php foreach ($data as $key => $value): ?>
                            <?php if (is_array($value) && !empty($value)): ?>
                                <?php foreach ($value as $subKey => $subValue): ?>
                                    <input type="hidden" name="<?= $key ?>[<?= $subKey ?>]" value="<?= $subValue ?>">
                                <?php endforeach; ?>
                            <?php else: ?>
                                <input type="hidden" name="<?= $key ?>" value="<?= $value ?>">
                            <?php endif; ?>
                        <?php endforeach; ?>
                    <?php endif; ?>
                    <div class="g-recaptcha" data-sitekey="<?= $captchaParams['site_key'] ?>"></div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success">Tiếp tục</button>
                </div>
            </div>
        </form>
    </div>
    <style>
        #recaptcha_modal h4{
            color: black;
            text-align: left;
            font-family: "Open Sans", "Proxima Nova", sans-serif !important;
        }
    </style>
</div>
