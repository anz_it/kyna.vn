<?php
use yii\helpers\Url;
use yii\helpers\Html;
use common\helpers\CDNHelper;

$cdnUrl = CDNHelper::getMediaLink();

$name = !empty(Yii::$app->user->identity->profile->name) ? Yii::$app->user->identity->profile->name : Yii::$app->user->identity->username;

?>
<li class="nav-item dropdown nav-item-profile">
    <a href="<?= Url::toRoute(['/user/course/index']) ?>" class="nav-link dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
        <img src="<?= $cdnUrl ?>/img/banner-top.jpg" alt="<?= $name ?>" class="avatar avatar-small"/>
        <div class="profile-label hidden-md-down">
            <div class="user-name"><?= array_pop(explode(' ', $name)) ?></div>
            <div>
                <small class="title text-muted">Khóa học của tôi</small>
            </div>
        </div>
    </a>

    <div class="dropdown-menu dropdown-menu-right">
        <div class="dropdown-content">
            <a class="dropdown-item" href="<?= Url::toRoute(['/user/course/index']) ?>">
                <span class="icon icon-book"></span> Khóa học của tôi
            </a>
            <!-- <a class="dropdown-item" href="<?= Url::toRoute(['/user/course/index']) ?>">
                <span class="icon "></span>Khóa học đang quan tâm
            </a> -->
            <a class="dropdown-item" href="<?=Url::toRoute(['/user/course/index']) ?>">
                <span class="icon icon-document"></span> Lịch sử giao dịch
            </a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="<?= Url::toRoute(['/user/profile/edit']) ?>">
                <span class="icon icon-edit"></span> Chỉnh sửa thông tin
            </a>
            <a class="dropdown-item" href="#">
                <span class="icon icon-check"></span> Kích hoạt mã COD
            </a>
            <div class="dropdown-divider"></div>
            <?= Html::beginForm(['/user/security/logout'], 'post') ?>
            <button class="dropdown-item btn btn-link" type="submit">
                <i class="icon icon-sign-out"></i> Thoát
            </button>
            <?= Html::endForm() ?>
        </div>
    </div>
</li>
