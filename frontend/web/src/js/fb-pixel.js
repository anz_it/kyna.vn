/**
 * Created by tn on 12/02/2016.
 */
<!-- Facebook Pixel Code -->
'use strict'
/**
 Pages
 - Trang chủ: k-highlights, key: homepage
 - Trang cate: k-listing, key: category
 - Trang subcate
 - Trang Search result
 - Trang detail page (Course relevant): k-detail
 */
function FBPIXEL() {
    this.init();
}
FBPIXEL.prototype = {
    homePage: 'Homepage',
    categoryPage: 'Category',
    subCategoryPage: 'Sub Category',
    searchPage: 'Search Result',
    detailRelevantPage: 'Detail - Course Relevant',
    cartPage: 'Cart Page',
    checkoutPage: 'Checkout Page',
    purchasePage: 'Purchase Page',
    otherPage: 'Other Page',
    init: function () {
        var that = this;
        that.initDataLayer();
    },
    initDetailImpression: function (page) {
        var that = this;
        var productObj = null;
        switch (page) {
            case that.detailRelevantPage:
                var $container = $('.k-course-details-header');
                productObj = {
                    'content_ids': [window.location.pathname],
                    'content_type': 'product',
                    'value': that.deCurrency($container.find('.price-list>ul>li:first').text()),
                    'currency': 'VND'
                };
                break;
            default:
                break;
        }
        that.insertDataDetailImpression(productObj);
    },
    initAddToCart: function (page) {
        var that = this;
        switch (page) {
            case that.homePage:
                var $container = $('.k-popup-lesson');
                $('body').on('click', '.add-to-cart', function (e) {
                    var productObj = {
                        'content_ids': [window.location.pathname],
                        'content_type': 'product',
                        'value': that.deCurrency($container.find('ul.k-popup-lesson-detail-price>li:first>span.bold').text()),
                        'currency': 'VND'
                    };
                    that.insertDataAddToCart(productObj);
                });
                break;
            case that.detailRelevantPage:
                var $container = $('.k-course-details-header');
                $container.find('.add-to-cart').click(function () {
                    var productObj = {
                        'content_ids': [window.location.pathname],
                        'content_type': 'product',
                        'value': that.deCurrency($container.find('.price-list>ul>li:first').text()),
                        'currency': 'VND'
                    };
                    that.insertDataAddToCart(productObj);
                });
                break;
            default:
                break;
        }
    },
    initPurchase: function (page) {
        var that = this;
        switch (page) {
            case that.purchasePage:
                var $container = $('#checkout-succ-online');
                if ($container.find('.checkout-succ-title').length > 0) {
                    var $checkoutListPrice = $container.find('.checkout-list-price');
                    var productObj = {
                        'content_ids': [],
                        'content_type': 'product',
                        'value': that.deCurrency($checkoutListPrice.find('>ul>li:last>span.price').text()),
                        'currency': 'VND'
                    };
                    $container.find('ul.list>li').each(function (index) {
                        productObj.content_ids.push(that.deUrl($(this).find('>div.text>h6>a').attr('href')));
                    });
                    that.insertDataPurchase(productObj);
                }
                break;
            default:
                break;
        }
    },
    insertDataDetailImpression: function (productObj) {
        fbq('track', 'ViewContent', productObj);
    },
    insertDataAddToCart: function (productObj) {
        fbq('track', 'AddToCart', productObj);
    },
    insertDataPurchase: function (productObj) {
        fbq('track', 'Purchase', productObj);
    },
    initDataLayer: function () {
        var that = this;
        var pathName = window.location.pathname;
        var search = window.location.search;
        var slugPattern = /[a-z0-9-]+/g;
        var regex = new RegExp(slugPattern);
        var pageMatch = pathName.match(regex);
        if (pageMatch === null) {
            that.initAddToCart(that.homePage);
        } else if (pageMatch.length > 0 && pageMatch[0] === "gio-hang") {
            // code for cart page
        } else if (pageMatch.length > 0 && pageMatch[0] === "thanh-toan" && pageMatch[1] === "thanh-cong") {
            that.initPurchase(that.purchasePage);
        } else if (pageMatch.length > 0 && pageMatch[0] === "thanh-toan") {
            // code for checkout page
        } else if (pageMatch.length > 0 && search === "" && $('.k-course-details-related').length > 0) {
            that.initDetailImpression(that.detailRelevantPage);
            that.initAddToCart(that.detailRelevantPage);
        } else if (pageMatch.length < 3 && search === "" && $('#k-listing').length > 0) {
            // code for category page
        } else if (pageMatch.length >= 3 && search === "" && $('#k-listing').length > 0) {
            // code for sub-category page
        } else if (pageMatch.length > 0 && search !== "" && $('#k-listing').length > 0) {
            // code for search page
        } else {
            // code for other page
        }
    },
    deCurrency: function (originalStr) {
        var str = originalStr.match(/[\d\,\.]+/g);
        if (str) {
            return str[0].replace(/,/g, '.').replace(/\s/g, '').replace(/\./g, ''); // return decimal
        }
        return originalStr;
    },
    deUrl: function (str) {
        return str.replace(/\-p[0-9]{1,}.*/g, '');
    }
};

