HeaderAction = {
    Init: function(){
        /* Go to link when width < 768 */
        $(".account.dropdown").click(function(){
            if (window.innerWidth < 768)
                location.href = 'account_courses.html';
        })

        /* Show/Hide Menu */
        $(".nav-link-menu").click(function(){
            $(this).parent().toggleClass("open");
        })
        
        
        /* Seemore Menu */
        /*$("li.see-more").click(function(event){
            HeaderAction.SeeMoreMenu(this);
        });*/


        /* Hide div when click outside */
        $(document).mouseup(function (e)
        {
            var container = $(".k-header-menu .nav-wrap.open ul.dropdown-menu");

            if (!container.is(e.target) // if the target of the click isn't the container...
                && container.has(e.target).length === 0) // ... nor a descendant of the container
            {
                $(container).parent().removeClass("open");
            }
        });
        
        /* Scroll On Div ID nav-drop-menu*/
        $('#nav-drop-menu').bind('mousewheel DOMMouseScroll', function(e) {
            var scrollTo = null;

            if (e.type == 'mousewheel') {
                scrollTo = (e.originalEvent.wheelDelta * -1);
            }
            else if (e.type == 'DOMMouseScroll') {
                scrollTo = 20 * e.originalEvent.detail;
            }

            if (scrollTo) {
                e.preventDefault();
                $(this).scrollTop(scrollTo + $(this).scrollTop());
            }
        });


    },
    SeeMoreMenu: function(t){
        var cur = $(t).parent();
        if ($(cur).hasClass("see-more"))
        {
            $(t).html("Xem thêm");
            $(cur).animate({ scrollTop: 0, height: 320 }, "fast");
        }
        else{
            $(t).html("Rút gọn");
            $(cur).animate({height: 380 }, "fast");
        }
        $(cur).toggleClass("see-more");
    },
};
HeaderAction.Init();
