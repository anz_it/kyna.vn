(function ($) {
    "use strict";
    $(document).on("click", "[data-offpage]", function (e) {
        e.preventDefault();
        $(this).parents("body").addClass("offpage-overflow");     
        var target = $(this).data("offpage");
        if ($(this).hasClass("offpage-close")) {        
            $(target).removeClass("in");
            $(this).parents("body").removeClass("offpage-overflow");
            $('main').removeClass('filterCourses'); 
        } else {
            $(target).toggleClass("in");
        }
    });
    $(".k-listing-button-filter.k-button-mobile").on('click', function(e){
        e.preventDefault();
        $('main').addClass('filterCourses');
    })
})(jQuery);
