(function ($, window, document, undefined) {
    var showPopup = true;

    // trigger event when user mouse out from kyna
    $(document).mouseleave(function(e) {
        var hasCart = countCart > 0;

        if (hasCart && showPopup) {
            showPopup = false;
            $('#btn-rethink-to-payment').trigger('click');
        }
    });
})(window.jQuery, window, document);