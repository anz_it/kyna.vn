<?php
$params = require_once ('param.php');
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta property="og:locale" content="vi_VN" />
<meta property="og:type" content="website" />
<meta property="og:image" content="#">
<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
<title>Bảo trì hệ thống!</title>
    <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="css/style.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://kyna.vn/media/landing_theme/TimeCircles.css" type="text/css"/>
    <script type="text/javascript" src="js/jquery-1.11.3.min.js"></script>
    <script type="text/javascript" src="https://kyna.vn/media/bootstraps/js/bootstrap.min.js"></script>
    <META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">
</head>

<body>
	<div class="container-fluid mg">
    	<div class="row-m">
        	<div class="col-sm-7" align="center">
            	<div class="col-sm-12"><a href="https://kyna.vn"><img src="https://kyna.vn/media/landing_theme/images/advice_kngt_toan_dien/logo.png" alt="kyna logo" width="115px"></a></div>
                <div class="col-sm-12"><img src="img/hinh.png" alt="bao-tri" style="width:100%"></div>
            </div>
            <div class="col-sm-5">
            	<div class="title">THÔNG BÁO BẢO TRÌ HỆ THỐNG KYNA.VN</div>
                <section class="text-ct">
                	<span>Nhằm mục đích phục vụ người học trên <strong class="green-color">Kyna.vn</strong> tốt hơn, từ <strong><?=$params['from_hour']?></strong> ngày <strong><?=$params['from_date']?></strong> đến <strong><?=$params['to_hour']?> ngày </strong><strong class="deadline"> <?=$params['to_date']?></strong>, <strong class="green-color">Kyna.vn</strong> sẽ tiến hành nâng cấp và bảo trì hệ thống. Sau thời gian trên,<strong class="green-color"> Kyna.vn</strong> sẽ tiếp tục hoạt động bình thường.</span>
<!--                	 <div id="CountDownTimer" class="timecounter" > </div>-->
                </section>
            </div>
        </div>
    </div>
       <script type="text/javascript">
          $(function () {
      
          $('.toggle').click(function (event) {
              event.preventDefault();
              var target = $(this).attr('href');
              $(target).toggleClass('hidden show');
          });
      
      });
          
//           function TimeCircles(){
//                  var data =
//                  {
//                      circle_bg_color:"#f4f4f4", fg_width:0.025, bg_width: 1.5,
//                      time: {
//                          Days: {color: "#ecd984" },
//                          Hours: { color: "#ecd984" },
//                          Minutes: { color: "#ecd984" },
//                          Seconds: { color: "#ecd984" }
//                      }
//                  }, dur = 3600000*24*2, begin_date = (new Date('2016/01/17 00:00:00')).getTime(), today = (new Date()).getTime(), remain = begin_date;
//
//                  while(remain <= today){ remain += dur;}
//                  data.ref_date = remain;
//                  $("#CountDownTimer").TimeCircles(data).start();
//                  return remain;
//              }
//
//              function deadline_date(time){
//                  var date = new Date(time),
//                      str  = pad(date.getDate()) + '/'  + pad(date.getMonth()+1) + '/' + date.getFullYear();
//                  $(".deadline").html(str);
//              }
//
//              $(function(){
//                  $("#knw_bgf").height(window.innerHeight);
//                  deadline_date(TimeCircles());
//                  $("#move_down").click(function(){
//                      var of = $("#bg2").offset().top;
//                      $("html,body").animate({scrollTop:of-1}, 900);
//                  });
//
//              });
             
          </script> 
<!--  <script type="text/javascript" src="https://kyna.vn/media/landing_theme/TimeCircles_SK2_min.js"></script>-->
</body>
</html>
