<?php
/**
 * Created by PhpStorm.
 * User: khanhphan
 * Date: 5/15/17
 * Time: 11:58 AM
 */
return [
    'from_hour' => '12h',
    'from_date' => date('d/m/Y'),
    'to_hour' => '15h',
    'to_date' => date('d/m/Y'),
];