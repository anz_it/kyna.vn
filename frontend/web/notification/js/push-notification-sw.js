/**
 * Created by ngunp on 6/28/2017.
 */
importScripts("firebase.js");

// Initialize Firebase
var config = {
    apiKey: "AIzaSyBI_ef5ohWbES8r1Hwsp9SDIK7hp4fNaZQ",
    authDomain: "kyna-564c3.firebaseapp.com",
    databaseURL: "https://kyna-564c3.firebaseio.com",
    projectId: "kyna-564c3",
    storageBucket: "kyna-564c3.appspot.com",
    messagingSenderId: "884971139947"
};
firebase.initializeApp(config);

const messaging = firebase.messaging();

messaging.setBackgroundMessageHandler(function(payload) {
    var title = "default title";
    if (payload.data.title != null){
        title = payload.data.title;
    }
    var options = {
        // icon: "url to default icon",
        // body: "default body",
        // image: "url to default image",
        // actions: [
        //     {action: 'explore', title: 'open',
        //         // icon: 'images/checkmark.png'
        //     },
        //     {action: 'close', title: 'Close',
        //         // icon: 'images/xmark.png'
        //     },
        // ]
    };
    if (payload.data.body != null) {
        options.body = payload.data.body;
    }
    if (payload.data.icon != null) {
        options.icon = payload.data.icon;
    }
    if (payload.data.image != null) {
        options.image = payload.data.image;
    }
    if (payload.data.data != null) {
        options.data = payload.data.data;
    }
    // if (payload.data.actions != null) {
    //     options.actions = payload.actions;
    // }
    return self.registration.showNotification(title, options);
});

self.addEventListener('notificationclick', function(event) {
    var notification = event.notification;
    // var primarykey = notification.data.primaryKey;
    var action = event.action;
    // console.log(action);
    console.log(event);

    if (notification.data != null) {
        var data = JSON.parse(notification.data);
        console.log(data);
    }


    switch (action) {
        // case 'close':
        //     console.log("acion close");
        //     notification.close();
        //     break;
        // case 'explore':
        //     console.log("action explore");
        //     clients.openWindow("http://kyna.frontend.local");
        //     notification.close();
        //     break;
        default:
            console.log("unrecognized action");
            if (data != null && data.url != null)
                clients.openWindow(data.url);
            // clients.openWindow("https://kyna.vn");
            notification.close();
    }
});