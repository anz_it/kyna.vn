<?php

// Check if function exists (php5.4+ includes this method)
if(!function_exists("hex2bin")){
    function hex2bin($h)
    {
        if (!is_string($h))
            return null;
        $r = '';
        for ($a=0;$a<strlen($h);$a+=2)
        {
            $r .= chr(hexdec($h{$a}.$h{($a+1)}));
        }
        return $r;
    }
}

$isValid = true;
if(empty($_SERVER['HTTP_REFERER']) || !strpos($_SERVER['HTTP_REFERER'], 'kyna') !== false  ) {
    $isValid = false;
}
if (! $isValid)
{
    header('HTTP/1.0 403 Forbidden');
}
else
{
    header("Access-Control-Allow-Origin: *");
    header('Content-Type: binary/octet-stream');
    header('Pragma: no-cache');


    echo hex2bin('5656A2039E604570FEE2EEC7768DB80B');

    exit(); // this is needed to ensure cr/lf is not added to output
}

?>