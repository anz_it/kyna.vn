;(function() {
    'use strict'

    $("body")
        .on("show.bs.modal", "#modal", function (e) {
            //console.log(e);
            var that = this,
                clickHandle = e.relatedTarget,
                cssClass = $(clickHandle).data("modal-class"),
                url = (clickHandle !== undefined) ? clickHandle.href : false,
                hasClose = $(clickHandle).data("close"),
                close = $("<button>").attr({
                    'type': "button",
                    "class": "close",
                    "data-dismiss": "modal",
                    "aria-label": "Close"
                }).html("<span aria-hidden='true'>&times;</span>")

            function addCloseTo(target) {
                if (hasClose !== undefined && hasClose !== null && hasClose !== false) {
                    $(target).prepend(close)
                }
            };

            $(this).addClass(cssClass)
            if (url) {
                $(this).find(".modal-content").load(url, function (e) {
                    addCloseTo(this);
                })
            }
            else {
                addCloseTo($(this).find(".modal-content")[0]);
            }
        })
        .on("hidden.bs.modal", "#modal", function (e) {
            $(this).attr("class", "modal fade").find(".modal-content").html("");
        })
})(jQuery);
