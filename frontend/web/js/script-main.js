$(document).ready(function() {
/* HEADER */

    /* JS MENU PC */
    /* Hide Header on on scroll down */
    if($(window).width() > 990){
        var didScroll;
        var lastScrollTop = 0;
        var delta = 5;
        var navbarHeight = $('.wrap-header').outerHeight();               
        
        $(window).scroll(function(event){
            didScroll = true;
        });
        
        setInterval(function() {
            if (didScroll) {
                hasScrolled();
                didScroll = false;
            }
        }, 250);
        
        function hasScrolled() {
            var st = $(this).scrollTop();
            
            /* Make sure they scroll more than delta */
            if(Math.abs(lastScrollTop - st) <= delta)
                return;                
            if (st > lastScrollTop && st > navbarHeight){
                /* Scroll Down */
                $('.wrap-header').removeClass('menu-nav-down').addClass('menu-nav-up');                
                /* Scroll Bar Page Detail */
                $('.hidden-course-bar').removeClass('slideDown-header');                                                            
                /* End */
            } else {
                /* Scroll Up */
                if(st + $(window).height() < $(document).height()) {
                    $('.wrap-header').removeClass('menu-nav-up').addClass('menu-nav-down');
                    /* Scroll Bar Page Detail */
                    $('.hidden-course-bar').addClass('slideDown-header');                                                                                                                                                                            
                    /* End */
                }
            }
            
            lastScrollTop = st;
        } 
    }
      
    /* 1. SEARCH HEADER */
    var TxtRotate = function(el, toRotate, period) {
      this.toRotate = toRotate;
      this.el = el;
      this.loopNum = 0;
      this.period = parseInt(period, 10) || 2000;
      this.txt = '';
      this.tick();
      this.isDeleting = false;
    };
    
    TxtRotate.prototype.tick = function() {
      var i = this.loopNum % this.toRotate.length;
      var fullTxt = this.toRotate[i];
    
      if (this.isDeleting) {
        this.txt = fullTxt.substring(0, this.txt.length - 1);
      } else {
        this.txt = fullTxt.substring(0, this.txt.length + 1);
      }
    
      this.el.innerHTML = '<span class="wrap">'+this.txt+'</span>';
    
      var that = this;
      var delta = 300 - Math.random() * 100;
    
      if (this.isDeleting) { delta /= 2; }
    
      if (!this.isDeleting && this.txt === fullTxt) {
        delta = this.period;
        this.isDeleting = true;
      } else if (this.isDeleting && this.txt === '') {
        this.isDeleting = false;
        this.loopNum++;
        delta = 500;
      }
    
      setTimeout(function() {
        that.tick();
      }, delta);
    };
    
    window.onload = function() {
      var elements = document.getElementsByClassName('txt-rotate');
      for (var i=0; i<elements.length; i++) {
        var toRotate = elements[i].getAttribute('data-rotate');
        var period = elements[i].getAttribute('data-period');
        if (toRotate) {
          new TxtRotate(elements[i], JSON.parse(toRotate), period);
        }
      }
      /* INJECT CSS */
      var css = document.createElement("style");
      css.type = "text/css";
      css.innerHTML = ".txt-rotate > .wrap { border-right: 0.08em solid #666 }";
      document.body.appendChild(css);
    };

    /* CART + DEFAULT */      
       
    $(".click-button-cart")
    .mouseover(function(){ 
        $(this).addClass("add");
        $(".right-absolute-cart").show();       
    })
    .mouseout(function() {
        $(this).removeClass("add");
        $(".right-absolute-cart").hide();
    });

   
    $(".right-absolute-cart")
    .mouseover(function(e){  
        $(".click-button-cart").addClass("add");    
        $(this).show();
    }) 
    .mouseout(function(){ 
        $(".click-button-cart").removeClass("add"); 
        $(this).hide();
    });
    
     
    /* CART + LOGIN */
 
    $(".click-button-cart-login")
    .mouseover(function(){ 
        $(this).addClass("add");
        $(".right-absolute-cart-login").show();       
    })
    .mouseout(function() {
        $(this).removeClass("add");
        $(".right-absolute-cart-login").hide();
    });

   
    $(".right-absolute-cart-login")
    .mouseover(function(e){  
        $(".click-button-cart-login").addClass("add");    
        $(this).show();
    }) 
    .mouseout(function(){ 
        $(".click-button-cart-login").removeClass("add"); 
        $(this).hide();
    }); 
   
    
    /* ACCOUNT */  
             
    $(".click-button-account")
    .mouseover(function(){ 
        $(this).addClass("add");
        $(".right-absolute").show();       
    })
    .mouseout(function() {
        $(this).removeClass("add");
        $(".right-absolute").hide();
    });

   
    $(".right-absolute")
    .mouseover(function(e){  
        $(".click-button-account").addClass("add");    
        $(this).show();
    }) 
    .mouseout(function(){ 
        $(".click-button-account").removeClass("add"); 
        $(this).hide();
    });            
                    
    /* MENU */
    
    $(".click-button-menu")
    .mouseover(function(){ 
        $(this).addClass("add");
    	$(".left-absolute").show();       
    })
    .mouseout(function() {
        $(this).removeClass("add");
        $(".left-absolute").hide();
    });
   
    $(".left-absolute")
    .mouseover(function(e){  
        $(".click-button-menu").addClass("add");  	
    	$(this).show();
    }) 
    .mouseout(function(){ 
        $(".click-button-menu").removeClass("add"); 
        $(this).hide();
    });    
            
    
    /* NEWS */
 
    $(".click-button-news")
    .mouseover(function(){ 
        $(this).addClass("add");
        $(".left-first-info").show();       
    })
    .mouseout(function() {
        $(this).removeClass("add");
        $(".left-first-info").hide();
    });

   
    $(".left-first-info")
    .mouseover(function(e){  
        $(".click-button-news").addClass("add");    
        $(this).show();
    }) 
    .mouseout(function(){ 
        $(".click-button-news").removeClass("add"); 
        $(this).hide();
    }); 
    
    
    /* LOGIN */         
        
    $(".click-button-account-login")      
    .mouseover(function(){ 
        $(this).addClass("add");
        $(".right-child-info").show();       
    })
    .mouseout(function() {
        $(this).removeClass("add");    
        $(".right-child-info").hide();
    });

   
    $(".right-child-info")
    .mouseover(function(e){  
        $(".click-button-account-login").addClass("add");
        $(this).show();
    }) 
    .mouseout(function(){ 
        $(".click-button-account-login").removeClass("add"); 
        $(this).hide();
    });            
        
  
    
    /* DROPDOWN MENU */
    /*       
    $(document).ready(function (e) {
        function t(t) {
            e(t).bind("click", function (t) {
                t.preventDefault();
                e(this).parent().fadeOut()
            })
        }
        e(".drop_btn_click").click(function () {
            var t = e(this).parents(".button-dropdown").children(".dropdown-show-box").is(":hidden");
            e(".button-dropdown .dropdown-show-box").hide();
            e(".button-dropdown .drop_btn_click").removeClass("active");
            if (t) {
                e(this).parents(".button-dropdown").children(".dropdown-show-box").toggle().parents(".button-dropdown").children(".drop_btn_click").addClass("active")
            }
        });
        e(document).bind("click", function (t) {
            var n = e(t.target);
            if (!n.parents().hasClass("button-dropdown")) e(".button-dropdown .dropdown-show-box").hide();
        });
        e(document).bind("click", function (t) {
            var n = e(t.target);
            if (!n.parents().hasClass("button-dropdown")) e(".button-dropdown .drop_btn_click").removeClass("active");
        });
    });
    */
    /* BOX PRODUCT */                   
    $(".box-product").hover(
      function() {
        $(this).find('.wrap').addClass("block");
      }, function() {
        $(this).find('.wrap').removeClass("block");
      }
    );   
    $(".box-product .wrap a").on('click',function(){
        $(this).parents('body').find('.wrap-header').addClass('menu-nav-down').removeClass('menu-nav-up');
    });
              
                
    /* SEARCH */   
    function checkChange($this){
        var value = $this.val();              
            var sv=$this.data("SearchStored");                    
            if(value!=sv)
                $this.trigger("SearchChange");            
    }
    $(document).ready(function(){
        $(this).data("SearchStored",$(this).val());   
            $(".wrap-form-search input").bind("keyup",function(){  
            checkChange($(this));
        });               
        $(".wrap-form-search input").bind("SearchChange",function(){
            
            $(this).parents('.wrap-form-search').find('.text.input').addClass('hidden');            
        });        
    });    
          
    /* HIDDEN MENU MOBILE */ 
    $("#menu-main-mb").click(function(){        
    	$("html").toggleClass("scroll-y-hidden");
    });                        
    /*    
    $( ".wrap-form-search .search-form" ).hover(
        function() {
            $(".wrap-form-search .text").removeClass("hidden");
        }, function() {
            $(".wrap-form-search .text").addClass("hidden");
        }
    );    
    
    $( ".wrap-form-search .search-form" ).click(
        function() {
            $(".wrap-form-search .text").removeClass("hidden");
        }, function() {
            $(".wrap-form-search .text").addClass("hidden");
        }
    );   
    */      
    
    /* SEARCH MOBILE */                      
    var flagtable = 'hide';
	var flag2 = 'search-hide';    	
	$('#button-search-mobile').click(function(){
		if (flag2 == 'search-hide'){
			$('.wrap-search-mobile').addClass('show-search');
			$('#button-search-mobile i').removeClass('fa-search');
			$('#button-search-mobile i').addClass('fa-times');
			flag2 = 'search-show';
		}
		else if (flag2 == 'search-show'){
			$('.wrap-search-mobile').removeClass('show-search');
			$('#button-search-mobile i').removeClass('fa-times');
			$('#button-search-mobile i').addClass('fa-search');
			flag2 = 'search-hide';
		}
	});    
        
    /* GET HEIGHT HEADER */         
    updateGetHeightHeader();
    $(window).resize(function() {
        updateGetHeightHeader();
    });
    function updateGetHeightHeader(){
        var heightHeader = $('.wrap-header').height() + 19;
        $('.get-height-header').attr('style','height:' + heightHeader + 'px; display: block');       
    }  
    
    /* RESPONSIVE EQUAL HEIGHT BLOCKS */    
    ;( function( $, window, document, undefined )
    {
        'use strict';
     
        var $list       = $( '.wrap-box-product' ),
            $items      = $list.find( '.box-product .content' ),
            setHeights  = function()
            {
                $items.css( 'height', 'auto' );
     
                var perRow = Math.floor( $list.width() / $items.width() );
                if( perRow == null || perRow < 2 ) return true;
     
                for( var i = 0, j = $items.length; i < j; i += perRow )
                {
                    var maxHeight   = 0,
                        $row        = $items.slice( i, i + perRow );
     
                    $row.each( function()
                    {
                        var itemHeight = parseInt( $( this ).outerHeight() );
                        if ( itemHeight > maxHeight ) maxHeight = itemHeight;
                    });
                    $row.css( 'height', maxHeight );
                }
            };
     
        setHeights();
        $( window ).on( 'resize', setHeights );
        $list.find( 'img' ).on( 'load', setHeights );
    })( jQuery, window, document );

    /* CLOSE ADD TO CART - POPUP */    
    $("body").bind('click',function(e){     
        var obj = $(e.target);
        var obj_key = obj.parents('#detail-form-register');
        if(obj.hasClass('fa-times')){      
            obj_key.hide();  
        }                   
    });

    
/* END DOCUMENT */
});
