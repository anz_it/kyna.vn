/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$(document).ready(function () {

    /* xử lý change city */
    changeCity();
    submitForm();
    initPublisherObj();
});

function changeCity() {
    $("#city").bind('change', function () {
        var city_id = $(this).val();
        if (city_id > 0) {
            var action_form = $(this).parents("form").attr('action');
            $("#district").html('<option value="" >--- Đang lấy dữ liệu ----</option>');
            var queryDistrictURL = action_form.replace('submit', 'submit-query-district');
            $.ajax({
                type: "POST",
                url: queryDistrictURL,
                data: {city: city_id},
                dataType: "JSON",
                success: function (data) {
                    addDistrictElement(data);
                },
                error: function (er) {
                    console.log(er);
                }

            });
        } else {
            $("#district").html('<option value="" >--- Quận/Huyện ---</option>');
        }


    });
}

function addDistrictElement(data) {
    if (data.length > 0) {
        var Str_HTML = '';
        $.each(data, function (index, value) {
            Str_HTML += '<option value="' + value.id + '" >' + value.name + '</option>';
        });
        $("#district").html(Str_HTML);
    }
}

function submitForm() {
    $("#landing-page-id, form[name='landing-page-id']").submit(function (e) {
        e.preventDefault();
		debugger;
        var csrfToken = $('meta[name="csrf-token"]').attr("content");
        $(this).find("#csrf").val(csrfToken);
        var city = $("#city option:selected").val();
        var district = $("#district option:selected").val();
        var email = $("#email").val();
        /*
        if (city == "0" || district == "0" || email.length == 0) {
            if (email.length > 0) {
                alert('Vui lòng bổ sung địa chỉ đăng ký');
            } else {
                alert('Nhập email đăng ký');
            }
            return false;
        }
        */
        var url = this.action;
        var data = $(this).serialize();


        $(".btn_box_register").prop('disabled', true);
        $(".btn_box_register").text('Đang xử lý...');

        var modal = $("#modal");

        $.ajax({
            type: "POST",
            url: url,
            data: data,
            dataType: "JSON",
            success: function (res) {
                if (res.status == 0) {
                    var con = modal.find('.popup_body');
                    con.html('<div class="alert alert-warning" role="alert">' + res.msg + '</div>');
                } else {
                    var con = modal.find('.popup_body');
                    var mess = res.msg;
                    var isDone = 0;
                    if (typeof changeContent == 'function') {
                        mess = changeContent();
                    }
                    con.html('<div class="alert alert-warning" role="alert">' + mess + '</div>');
                    /* submit data cho publisher */
                    /* Ajax Adpia */
                    var url_publisher_adpia = url.replace('submit', 'submit-publisher-adpia');
                    /* Ajax AccessTrade */
                    var url_publisher_accesstrade = url.replace('submit', 'submit-publisher-accesstrade');
                    /* Ajax MasOffer */
                    var url_publisher_masoffer = url.replace('submit', 'submit-publisher-masoffer');

                    function successHandler() {
                      /* Tiến hành add iframe đăng ký thành công */
                      $("<div id='showIframeSEONgon' style='display:none;'></div>").prependTo($('body'));
                      var iframe = document.createElement('iframe');
                      iframe.width = "100%";
                      iframe.height = "1px";
                      iframe.frameborder = "0";
                      iframe.style.border = '0';
                      iframe.src = (res.succespage !== undefined ? res.succespage : 'https://kyna.vn/dang-ky-thanh-cong');
                      $("#showIframeSEONgon").length > 0 && $("#showIframeSEONgon").html(iframe);
                    }
                    function ajaxCompleteHandler(rs1, rs2, rs3) {
                      successHandler();
                    }
                    function ajaxAdpia() {
                      return $.ajax({
                          type: "POST",
                          url: url_publisher_adpia,
                          data: data,
                          dataType: "JSON"
                      });
                    }

                    function ajaxAccessTrade() {
                      return $.ajax({
                        type: "POST",
                        url: url_publisher_accesstrade,
                        data: data,
                        dataType: "JSON",
                        success: function (accesstrade) {
                            /* Insert element image vào boby */
                            if (accesstrade.img_tag !== '') {
                                $(accesstrade.img_tag).prependTo(modal);
                            }
                        }
                      });
                    }

                    function ajaxMasoffer() {
                      return $.ajax({
                          type: "POST",
                          url: url_publisher_masoffer,
                          data: data,
                          dataType: "JSON"
                      });
                    }

                    $.when(ajaxAdpia(), ajaxAccessTrade(), ajaxMasoffer()).then(ajaxCompleteHandler);

                    /* reset cac tham so neu co */
                    $("#fullname").val('');
                    $("#email").val('');
                    $("#phonenumber").val('');
                    $("#address").val('');
                    $("#city option:first").prop('selected', true);
                    $("#district option:first").prop('selected', true);

                }
                modal.modal();
                $(".btn_box_register").prop('disabled', false);
                $(".btn_box_register").text('ĐĂNG KÝ HỌC NGAY');
            },
            error: function (er) {
                $(".btn_box_register").prop('disabled', false);
                $(".btn_box_register").text('ĐĂNG KÝ HỌC NGAY');
            }

        });
    });
}

function initPublisherObj() {
    /* kiểm tra */

    if (typeof publisherObj !== 'undefined') {
        var myForm = $("#landing-page-id");
        var HTML = '';
        $.each(publisherObj, function (index, obj) {
            HTML += "<input type='hidden'  name='publisher[" + obj.publisherName + "]' value='" + obj.publisherValue + "' />";
        });
        if (myForm.length == 1) {
            $(HTML).prependTo(myForm);
        } else {
            var multiForm = $("form[name='landing-page-id']");
            if (multiForm.length > 0) {
                $.each(multiForm, function (index, vForm) {
                    $(HTML).prependTo(vForm);
                });
            }
        }

    }



}
