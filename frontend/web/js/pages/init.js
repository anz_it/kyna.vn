/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$(document).ready(function () {
    // check if multi form
    if ($('form').length > 1) {
        changeCityMulti();
        submitFormMulti();
        initPublisherObjMulti();
    } else {
        changeCity();
        submitForm();
        initPublisherObj();
    }
});

function changeCity() {
    $("#district").attr("disabled", "disabled");
    $("#city").bind('change', function () {
        var city_id = $(this).val();
        if (city_id > 0) {
            var action_form = $(this).parents("form").attr('action');
            $("#district").removeAttr("disabled", "disabled");
            $("#district").html('<option value="" >--- Đang lấy dữ liệu ----</option>');
            var queryDistrictURL = action_form.replace('submit', 'submit-query-district');
            $.ajax({
                type: "POST",
                url: queryDistrictURL,
                data: {city: city_id},
                dataType: "JSON",
                success: function (data) {
                    addDistrictElement(data);
                },
                error: function (er) {
                    console.log(er);
                }

            });
        } else {
            $("#district").html('<option value="" >--- Quận/Huyện ---</option>');
        }


    });
}

function addDistrictElement(data) {
    if (data.length > 0) {
        var Str_HTML = '';
        $.each(data, function (index, value) {
            Str_HTML += '<option value="' + value.id + '" >' + value.name + '</option>';
        });
        $("#district").html(Str_HTML);
    }
}

function submitForm() {
    $("#landing-page-id, form[name='landing-page-id']").submit(function (e) {
        e.preventDefault();
        var csrfToken = $('meta[name="csrf-token"]').attr("content");
        $(this).find("#csrf").val(csrfToken);
		if ($("#code").length > 0 && $("#code").val() != ''){
			var _code = $("input[name='alias_name']").val() + " - Mã GT: " + $("#code").val();
			$("input[name='alias_name']").val(_code);
		}
        var city = $("#city option:selected").val();
        var district = $("#district option:selected").val();
        var fullname = $('#fullname').val();
        var phonenumber = $('#phonenumber').val();
        var address = $('#address').val();
        var email = $("#email").val();
        var pageSlug = $("input[name='page_slug']").val();

        /*if(email.length===0 || fullname.length === 0 || phonenumber.length === 0 || ($('#address').length > 0 && address.length === 0) || ($('#city').length > 0 && city.length === 0) || ($('#district').length > 0 && district.length === 0)){
          $("#landing-page-id").append('<p class="text-danger" style="color: red; font-size: 14px; text-align: center;">Vui lòng điền đầy đủ thông tin</p>');
          return false;
        } else{
          $(".text-danger") && $(".text-danger").remove();
        }*/
		if ($(this).attr("data-type") == "multiple"){
			var _id = [], adv = [];
			var obj1 = $("form select[id='lst_course_ids'] option");
			if($(obj1).length > 0){
			$(obj1).each(function(){
				_id.push($(this).attr("value"));
			})
			}
			else{
				$("#landing-page-id").append('<p class="text-danger" style="color: red; font-size: 14px; text-align: center;">Vui lòng chọn khoá học</p>');
				return false;
			}
			var obj2 = $("form select[id='lst_advice_name'] option");
			$(obj2).each(function(){
				adv.push($(this).html());
			})
			$("form input[name='list_course_ids']").val(_id);
			$("form input[name='advice_name']").val(adv);
		}
        /*
         if (city == "0" || district == "0" || email.length == 0) {
         if (email.length > 0) {
         alert('Vui lòng bổ sung địa chỉ đăng ký');
         } else {
         alert('Nhập email đăng ký');
         }
         return false;
         }
         */
        var $thisform = $(this);
        var url = this.action;
        var href = location.href;
        var slug = href.match(/p\/(.*)/)[1];
        var data = $(this).serialize();
        console.log(data);
        data = data + '&slug=' + slug;
        //data = data + '&payment_type=onepay_cc';
        var registerText = $(".btn_box_register").html();


        $(".btn_box_register").prop('disabled', true);
        $(".btn_box_register").text('Đang xử lý...');

        var modal = $("#modal");

        $.ajax({
            type: "POST",
            url: url,
            data: data,
            dataType: "JSON",
            success: function (res) {
                submitSuccess(res, $thisform, data, url, registerText);
            },
            error: function (er) {
                submitError(registerText);
            }
        });
    });
}

function initPublisherObj() {
    /* kiểm tra */

    if (typeof publisherObj !== 'undefined') {
        var myForm = $("#landing-page-id");
        var HTML = '';
        $.each(publisherObj, function (index, obj) {
            HTML += "<input type='hidden'  name='publisher[" + obj.publisherName + "]' value='" + obj.publisherValue + "' />";
        });
        if (myForm.length == 1) {
            $(HTML).prependTo(myForm);
        } else {
            var multiForm = $("form[name='landing-page-id']");
            if (multiForm.length > 0) {
                $.each(multiForm, function (index, vForm) {
                    $(HTML).prependTo(vForm);
                });
            }
        }

    }



}

function changeCityMulti() {
    $("select[name='city']").bind('change', function () {
        var city_id = $(this).val();
        if (city_id > 0) {
            var action_form = $(this).parents("form").attr('action');
            $("select[name='district']").html('<option value="" >--- Đang lấy dữ liệu ----</option>');
            var queryDistrictURL = action_form.replace('submit', 'submit-query-district');
            $.ajax({
                type: "POST",
                url: queryDistrictURL,
                data: {city: city_id},
                dataType: "JSON",
                success: function (data) {
                    addDistrictElementMulti(data);
                },
                error: function (er) {
                    console.log(er);
                }
            });
        } else {
            $("select[name='district']").html('<option value="" >--- Quận/Huyện ---</option>');
        }
    });
}

function addDistrictElementMulti(data) {
    if (data.length > 0) {
        var Str_HTML = '';
        $.each(data, function (index, value) {
            Str_HTML += '<option value="' + value.id + '" >' + value.name + '</option>';
        });
        $("select[name='district']").html(Str_HTML);
    }
}

function submitFormMulti() {
    $("form[name='landing-page-id']").submit(function (e) {
        e.preventDefault();
        var $thisform = $(this);
        var csrfToken = $('meta[name="csrf-token"]').attr("content");
        $(this).find("input[name='_csrf']").val(csrfToken);
        var url = this.action;
        var href = location.href;
        var slug = href.match(/p\/(.*)/)[1];
        var data = $(this).serialize();
        data = data + '&slug=' + slug;
        var registerText = $(".btn_box_register").html();

        $(".btn_box_register").prop('disabled', true);
        $(".btn_box_register").text('Đang xử lý...');

        $.ajax({
            type: "POST",
            url: url,
            data: data,
            dataType: "JSON",
            success: function (res) {
                submitSuccess(res, $thisform, data, url, registerText);
            },
            error: function (er) {
                submitError(registerText);
            }

        });
    });
}

function initPublisherObjMulti() {
    /* kiểm tra */

    if (typeof publisherObj !== 'undefined') {
        var myForm = $("form[name='landing-page-id']");
        var HTML = '';
        $.each(publisherObj, function (index, obj) {
            HTML += "<input type='hidden'  name='publisher[" + obj.publisherName + "]' value='" + obj.publisherValue + "' />";
        });
        if (myForm.length == 1) {
            $(HTML).prependTo(myForm);
        } else {
            var multiForm = $("form[name='landing-page-id']");
            if (multiForm.length > 0) {
                $.each(multiForm, function (index, vForm) {
                    $(HTML).prependTo(vForm);
                });
            }
        }
    }
}

function submitSuccess(res, $thisform, data, url, registerText) {
    if (res.status == 1) {

        // Submit Publishers
        submitPublishers(res, data, url);

        /* reset cac tham so neu co */
        $thisform.find("input[name='fullname']").val('');
        $thisform.find("input[name='email']").val('');
        $thisform.find("input[name='phonenumber']").val('');
        $thisform.find("input[name='address']").val('');
        $thisform.find("select[name='city'] option:first").prop('selected', true);
        $thisform.find("select[name='district'] option:first").prop('selected', true);

        /* Custom content success */
        if (typeof changeContent == 'function') {
            var customMessage = changeContent();
        }
    }

    if (res.recaptcha && res.recaptcha == 1) {
        if ($('#recaptcha_modal').length > 0) {
            $('#recaptcha_modal').remove();
            $('#recaptcha_modal').modal('hide');
            $('.modal-backdrop').remove();
        }
        $('body').append(res.html);
        $('#recaptcha_modal').modal();
        submitReCaptcha($thisform);
    } else {
        if ($('#recaptcha_modal').length > 0) {
            $('#recaptcha_modal').remove();
            $('#recaptcha_modal').modal('hide');
            $('.modal-backdrop').remove();
        }
        if ($('#thankyou_modal').length > 0) {
            $('#thankyou_modal').remove();
            $('#thankyou_modal').modal('hide');
            $('.modal-backdrop').remove();
        }
        $('body').append(res.html);

        // replace custom message success
        if (customMessage) {
            $('#thankyou_modal .md-body .message').html(customMessage);
        }

        $('#thankyou_modal').modal();
    }

    $(".btn_box_register").prop('disabled', false);
    $(".btn_box_register").html(registerText);
}

function submitError(registerText) {
    $(".btn_box_register").prop('disabled', false);
    $(".btn_box_register").html(registerText);
}

function submitReCaptcha($thisform) {
    $("form[name='recaptcha-form']").submit(function (e) {
        e.preventDefault();
        $(this).find('button[type="submit"]').prop('disabled', true);;
        var url = this.action;
        var data = $(this).serialize();
        $.ajax({
            type: "POST",
            url: url,
            data: data,
            dataType: "JSON",
            success: function (res) {
                submitSuccess(res, $thisform, data, url);
            },
            error: function (er) {
                submitError();
            }

        });
    });
}

function submitPublishers(res, data, url) {
    /* Ajax Adpia */
    //var url_publisher_adpia = url.replace('submit', 'submit-publisher-adpia');
    /* Ajax AccessTrade */
    var url_publisher_accesstrade = url.replace('submit', 'submit-publisher-accesstrade');
    /* Ajax MasOffer */
    //var url_publisher_masoffer = url.replace('submit', 'submit-publisher-masoffer');

    var url_publisher_civi = url.replace('submit', 'submit-publisher-civi');

    var url_publisher_yeah1_offer = url.replace('submit', 'submit-publisher-yeah1-offer');

    var modal = $("#modal");

    if (res.saved_id) {
        data += "&saved_id=" + res.saved_id;
    }

    // landing page id
    if (res.page_id) {
        data = data + '&page_id=' + res.page_id;
    }

    if (res.price) {
        data = data + '&price=' + res.price;
    }

    var ajaxCivi = $.ajax({
        type: 'POST',
        url: url_publisher_civi,
        data: data,
        dataType: "JSON"
    });

    var ajaxYeah1Offer = $.ajax({
        type: 'POST',
        url: url_publisher_yeah1_offer,
        data: data,
        dataType: "JSON"
    });

    var ajaxAccessTrade = (function () {
        return $.ajax({
            type: "POST",
            url: url_publisher_accesstrade,
            data: data,
            dataType: "JSON"
        });
    })()

    $.when(ajaxAccessTrade, ajaxCivi, ajaxYeah1Offer).then(function (atRes, cvRes, y1oRes) {
        if (cvRes[0].url != ''){
            $('body').append('<script type="text/javascript">'+cvRes[0].script+'</script>');
            $('body').append('<script type="text/javascript" src="https://member.civi.vn/js/aff_act.js"></script>');
            $('body').append('<noscript><img src="'+cvRes[0].url+'" style="width:1px; height: 1px"/></noscript>');
        }

        /* Insert element image AT into boby */
        if (atRes[0].url != '') {
            $('body').append('<img src="'+atRes[0].url+'" style="width:1px; height: 1px"/>');
        }

        /* Insert script register for Yeah1Offer */
        if (y1oRes[0].script != ''){
            $('body').append('<script type="text/javascript">'+y1oRes[0].script+'</script>');
        }

        // redirect to payment if any
        if (res.redirectUrl) {
            window.location.href = res.redirectUrl;
            return false;
        }
        var successUrl = res.succespage !== undefined ? res.succespage : '';
        successHandler(successUrl);
    });
}

function successHandler(successUrl) {
    /* Tiến hành add iframe đăng ký thành công */
    $("<div id='showIframeSEONgon' style='display:none;'></div>").prependTo($('body'));
    var iframe = document.createElement('iframe');
    iframe.width = "100%";
    iframe.height = "1px";
    iframe.frameborder = "0";
    iframe.style.border = '0';
    iframe.src = (successUrl !== undefined && successUrl != '' ? successUrl : 'https://kyna.vn/dang-ky-thanh-cong');
    $("#showIframeSEONgon").length > 0 && $("#showIframeSEONgon").html(iframe);
}
