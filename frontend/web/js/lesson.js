;(function ($) {
    // var finishedVideo = 0;
    //
    // $(this).ready(function () {
    //     window.player = $(".flowplayer").data("flowplayer");
    //     // console.log(player.video)
    //     if (player != null) {
    //         player.on( "finish", finishVideo);
    //
    //         player.on( "progress", checkToFinishLesson);
    //     }
    //
    // });
    //
    // function checkToFinishLesson(e, player) {
    //     var progressPercent = player.video.time * 100 / player.video.duration;
    //
    //     if (progressPercent >= 80 && finishedVideo == 0) {
    //         finishedVideo = 1;
    //         var lessonId = $('.flowplayer').eq(0).attr('data-lesson-id');
    //         var userCourseId = $('.flowplayer').eq(0).attr('data-user-course-id');
    //
    //         $.ajax({
    //             url: '/course/learning/end-lesson',
    //             type: 'POST',
    //             data: {
    //                 user_course_id: userCourseId,
    //                 lesson_id: lessonId
    //             }
    //         });
    //     }
    // }
    //
    // function finishVideo(e, player) {
    //     $li = $('#thelist li.active').next();
    //     if ($li.length > 0) {
    //
    //         window.location.href = $li.find('a').attr('href');
    //     }
    // }

    $("#transcript").on("click", "p", function (e) {
        e.preventDefault();

        var sec = $(this).data('sec');
        player.seek(sec);
        console.log(sec);
    });

    $("#mycourses").on("shown.bs.tab", "[data-toggle='tab']", function (e) {
        var pane = $(e.target.getAttribute("href")),
            url = pane.data('remote');
        if (url !== undefined) {
            pane.find(".ajax-content").load(url);
        }
    });
    $("#mobile-tab-sub").on("shown.bs.tab", "[data-toggle='tab']", function (e) {
        var pane = $(e.target.getAttribute("href")),
            url = pane.data('remote');
        if (url !== undefined) {
            pane.find(".ajax-content").load(url);
        }
    });
})(jQuery);
