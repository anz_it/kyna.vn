;(function($) {
    'use strict';

    $("body")
        .on("click", "a[data-ajax]", function(e) {
            e.preventDefault();
            window.ajaxCaller.doAjax(this, this.href, $(this).data());
        })
        .on("submit", "form[data-ajax]", function(e) {
            e.preventDefault();
            window.ajaxCaller.doAjax(this, this.action, $(this).serialize());
        });
})(jQuery);

window.ajaxCaller = window.ajaxCaller || {};
window.ajaxCaller.loadResponse = function(target, resp, el) {
    if (target !== undefined && target.selector !== '') {
        if (resp.content !== undefined) {
            target.html(resp.content);
        } else {
            target.html(resp);
        }
    } else {
        bootbox.dialog({
            message: '<div style="text-align: center;">' + resp.content + '</div>',
            closeButton: true,
            backdrop: true,
            onEscape: true
        });
    }

    if ($(el).is("form") && $(el).data("keep-values") !== true && $(el).data("keep-values") !== 'true') {
        el.reset();
        // enable submit button if any
        var submitBtn = $(el).find('button[type=submit]');
        if (submitBtn.length > 0) {
            submitBtn.prop('disabled', false);
        }
    }

    $(el).trigger("ajax.updated", [resp]);
};

window.ajaxCaller.doAjax = function(el, url, submitData) {
    var data = $(el).data(),
        title = el.title || $(el).text() || url,
        targetSelector = $(el).data("target"),
        $target = $(targetSelector);

    if (!$(el).is("form")) {
        $target.removeData("bs.modal").removeData("modal");

        if (data.pushState !== "false" && data.pushState !== false) {
            window.history.pushState(data, title, url);
        }

        if (data.toggle === "popup") {
            $target.one("shown.bs.modal", function(e) {
                $.get(url, submitData, function(resp) {
                    var $modalContent = $(e.target).find('.modal-content');
                    window.ajaxCaller.loadResponse($modalContent, resp, el);
                });
            });
            $target.one("hidden.bs.modal", function(e) {
                $(e.target).removeData("bs.modal").removeData("modal").find(".modal-content").empty();

                if (data.pushState !== "false" && data.pushState !== false) {
                    window.history.back();
                }
            });
            $target.modal("show");
            return;
        }
        $.get(url, submitData, function(resp) {
            window.ajaxCaller.loadResponse($target, resp, el);
        });
    } else {
        // disable button submit if any
        var submitBtn = $(el).find('button[type=submit]');
        if (submitBtn.length > 0) {
            submitBtn.prop('disabled', 'disabled');
        }
        if (el.method !== 'get' && el.method !== 'GET') {
            $.post(url, submitData, function(resp) {
                window.ajaxCaller.loadResponse($target, resp, el);
            });
        } else {
            $.get(url, submitData, function(resp) {
                window.ajaxCaller.loadResponse($target, resp, el);
            });
        }
    }

}
