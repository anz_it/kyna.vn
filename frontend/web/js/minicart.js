+function($) {
    'use strict';

    var MiniCart = function(element, options) {
        this.$element = $(element)
        this.$total = this.$element.find('total')

        //this.init();
    }

    MiniCart.prototype.update = function (resp) {
        var $element = this.$element;
        if (resp != null || resp != undefined) {
            $element.html(resp)
        }
        else {
            var url = $element.data("remote")
            $.get(url, function (resp) {
                $element.html(resp)
                $element.trigger("k.cart.updated")
            })
        }
    }

    MiniCart.prototype.add = function (relatedTarget, data) {
        var that = this,
            $element = this.$element
        $element.trigger("k.cart.add")
        $.post(data.url, data, function (resp) {
            $element.trigger("k.cart.added")
            that.update(resp)
        });
    }

    MiniCart.VERSION = '1.0.0';

    MiniCart.DEFAULTS = {
    }

    function Plugin(option, _relatedTarget, params) {
        return this.each(function() {
            var $this = $(this)
            var data = $this.data('k.minicart')
            var options = $.extend({}, MiniCart.DEFAULTS, $this.data(), typeof option == 'object' && option)

            if (!data) $this.data('k.minicart', (data = new MiniCart(this, options)))
            else if (typeof option == 'string') data[option](_relatedTarget, params)
        })
    }

    var old = $.fn.minicart

    $.fn.minicart = Plugin
    $.fn.minicart.Constructor = MiniCart

    $.fn.minicart.noConflict = function() {
        $.fn.minicart = old
        return this
    }

    $(window).on("load", function() {
        var $target = $(".minicart");
        Plugin.call($target, $target.data());

        $("body")
            .on("click", "[data-toggle='addToCart']", function (e) {
                e.preventDefault();
                var data = $(this).data();
                data.url = this.href;
                Plugin.call($target, 'add', this, data);
            })
            .on("submit", "form[data-submit='addToCart']", function (e) {
                e.preventDefault();
                var data = $(this).serializeArray();
                data.url = this.action;

                Plugin.call($target, 'add', this, data);
                return false;
            })
    });
}(jQuery);
