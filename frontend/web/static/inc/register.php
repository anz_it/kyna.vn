<!-- POPUP REGISTER -->
<div class="modal fade k-popup-account" id="k-popup-account-register" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
           <div class="modal-header">
              <button type="button" class="k-popup-account-close close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
              </button>
           </div>
           <div class="modal-body clearfix">

                <ul class="k-popup-account-top">
                    <li><a href="<?= Url::toRoute('/user/registration/register') ?>" class="button-facebook"><i class="icon-facebook"></i> Đăng nhập bằng facebook</a></li>
                    <li>- Hoặc đăng nhập bằng tài khoản Kyna -</li>
                </ul>
                <form>
                    <div class="form-group">
                        <span class="icon icon-mail"></span>
                        <input type="email" value="" name="" id="" placeholder="Email của bạn" class="text form-control">
                    </div>
                    <div class="form-group">
                        <span class="icon icon-lock"></span>
                        <input type="password" value="" name="" id="" placeholder="Mật khẩu" class="text form-control">
                    </div>
                    <div class="form-group">
                        <span class="icon icon-user"></span>
                        <input type="text" value="" name="" id="" placeholder="Họ tên" class="text form-control">
                    </div>
                    <div class="form-group">
                        <span class="icon icon-call"></span>
                        <input type="number" value="" name="" id="" placeholder="Số điện thoại" class="text form-control">
                    </div>
                    <div class="button-submit">
                        <button type="submit">Đăng ký</button>
                    </div><!--end .button-popup-->
                </form>
                <ul class="k-popup-account-bottom">
                    <li>Nếu đã có tài khoản</li>
                    <li><a href="#">Đăng nhập</a></li>
                </ul>

           </div>
           <!--end .modal-body-->
      </div>
   </div>
</div>
<!-- END POPUP REGISTER -->
