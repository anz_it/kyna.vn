<footer>
   <div id="k-footer">
      <div class="container">
         <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 hotline">
            <h4 class="bold text-transform title">Kết nối với Kyna</h4>
            <div class="social">
               <a href="#" target="_blank" class="facebook"><i class="icon icon-facebook"></i></a>
               <a href="#" target="_blank" class="youtube"><i class="icon icon-youtube"></i></a>
            </div>
            <!--end .social-->
            <ul class="bottom">
               <li>Hotline: 1900 6364 09</li>
               <li>Email: hotro@kyna.vn</li>
            </ul> 
            <!--end .bottom-->
         </div>
         <!--end .hotline -->
         <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 info">
            <h4 class="bold title">Thông tin Kyna</h4>
            <ul>
               <li><a href="/danh-sach-khoa-hoc">Danh sách khóa học</a></li>
               <li><a href="faq_detail.php">Câu hỏi thường gặp</a></li>
               <li><a href="faq_detail.php#k-faq-sidebar-2">Hướng dẫn thanh toán học phí</a></li>
               <li><a href="faq_detail.php#k-faq-sidebar-10" data-toggle="tab" role="tab" aria-expanded="true">Chính sách hoàn học phí</a></li>
            </ul>
            <!--end .top-->
         </div>
         <!--end .info-->
         <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 about">
            <h4 class="bold text-transform title">Về Kyna</h4>
            <ul>
               <li><a href="#" class="hover-color-green">Giới thiệu về công ty</a></li>
               <li><a href="#" class="hover-color-green">Thư viện bài viết</a></li>
               <li><a href="affiliate.php" class="hover-color-green">Hợp tác cùng Kyna.vn</a></li>
               <li><a href="teaching.php" class="hover-color-green">Giảng dạy tại Kyna.vn</a></li>
            </ul>
            <!--end .top-->
         </div>
         <!--end .about-->
         <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 fanpage">
         </div>
         <!--end .fanpage-->
      </div>
      <!--end .container-->
   </div>    
   <!--end #wrap-footer-->     
   <!--    Copyright   -->
   <div id="k-footer-copyright">
      <div class="container">
         <div class="col-lg-8 col-xs-12 address">
            <div class="col-sm-3 col-xs-4 img pd0">
               <a href="#"><span class="img-cer-bo"></span></a>
            </div>
            <!--end col-xs-4 img pd0-->
            <div class="col-sm-9 col-xs-8 text pd0">
                <p class="text-copyright">© 2014 - Bản quyền của Công Ty Cổ Phần Dream Viet Education</p>
               <p>Địa chỉ: Biệt thự 298/3 Điện Biên Phủ, phường 17, quận Bình Thạnh, TP Hồ Chí Minh</p>
               <p>Giấy phép ĐKKD số 0312401818 do Sở Kế hoạch và Đầu tư TPHCM cấp ngày 05/08/2013</p>
            </div>
            <!--end col-xs-8 text-->
         </div>
         <!--end .col-sm-7 col-xs-12 left-->
         <div class="col-lg-4 col-xs-12 info">
            <ul>
               <li><a href="policies.php" class="hover-color-green">Điều khoản dịch vụ</a></li>
               <li><a href="terms.php" class="hover-color-green">Chính sách bảo mật</a></li>
            </ul>
         </div>
         <!--end .col-sm-5 col-xs-12 right-->
         <div class="col-xs-12 md phone">
             <ul>
                 <li></li>
             </ul>
         </div><!--end .phone-->
      </div>
      <!--end .container-->
   </div>
   <!--end #wrap-copyright-->
   <div id="k-footer-mb">           
        <ul class="k-footer-mb-contact">
            <li>
                <a href="#" target="_blank"><i class="icon icon-call"></i> 1900 6364 09</a>
            </li>
            <li>
                <a href="#" target="_blank"><i class="icon icon-mail-outline"></i> hotro.kyna.vn</a>
            </li>
        </ul>
        <ul class="k-footer-mb-social">
            <li>
                <a href="#" target="_blank"><i class="icon icon-facebook"></i></a>
            </li>
            <li>
                <a href="#" target="_blank"><i class="icon icon-youtube"></i></a>
            </li>
        </ul>
        <p>&copy; 2014 - Công ty Cổ Phần Dream Viet Education</p>
   </div><!--end #k-footer-mb-->
   
</footer>    
  


<div id="nav-mobile" class="offpage-menu offpage-left">    
    <header>
       <div class="k-header-offpage-menu">
            <img src="img/logo.svg" alt="Kyna.vn" class="img-responsive">       
            <a href="#" class="right offpage-close" data-offpage="#nav-mobile">
            <i class="icon icon-arrow-right-bold" aria-hidden="true"></i>
            </a>        
        </div><!--header -->   
    </header> 
    <div class="content">
        <div class="panel-group" id="accordion">
          <!-- Panel 1 -->
          <div class="panel panel-info">
            <div class="panel-heading">
               <h4 class="panel-title icourses">
                  <a data-toggle="collapse" data-parent="#accordion" href="#col1">Khóa học Combo</a>
               </h4>
            </div>            
          </div>
          <!-- ./panel -->
          <!-- Panel 2 -->
          <div class="panel panel-info">
            <div class="panel-heading">
               <h4 class="panel-title ibussiness">
                  <a data-toggle="collapse" data-parent="#accordion" href="#col2">Kinh doanh</a>
               </h4>
            </div>            
          </div>
         <!-- ./panel -->
         <!-- Panel 3 -->
          <div class="panel panel-info">
            <div class="panel-heading">
               <h4 class="panel-title imarketing">
                  <a data-toggle="collapse" data-parent="#accordion" href="#col3">Truyền thông - Tiếp thị</a>
                  <div class="icon-arrow-down icon"></div>
               </h4>
            </div> 
            <div id="col3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                <ul>
                    <li><a href="#">Truyên thông 1</a></li>
                    <li><a href="#">Truyên thông 2</a></li>
                    <li><a href="#">Truyên thông 3</a></li>
                </ul>
    </div>       
          </div>
         <!-- ./panel -->  
         <!-- Panel 4 -->
          <div class="panel panel-info">
            <div class="panel-heading">
               <h4 class="panel-title iworking">
                  <a data-toggle="collapse" data-parent="#accordion" href="#col4">Kỹ năng cho người đi làm</a>
               </h4>
            </div>            
          </div>
         <!-- ./panel --> 
         <!-- Panel 5 -->
          <div class="panel panel-info">
            <div class="panel-heading">
               <h4 class="panel-title icomputing">
                  <a data-toggle="collapse" data-parent="#accordion" href="#col5">Vi tính văn phòng</a>
               </h4>
            </div>            
          </div>
         <!-- ./panel -->  
         <!-- Panel 6 -->
          <div class="panel panel-info">
            <div class="panel-heading">
               <h4 class="panel-title itechnology">
                  <a data-toggle="collapse" data-parent="#accordion" href="#col6">Công nghệ, lập trình và đồ họa</a>
               </h4>
            </div>            
          </div>
         <!-- ./panel --> 
         <!-- Panel 7 -->
          <div class="panel panel-info">
            <div class="panel-heading">
               <h4 class="panel-title ieducation">
                  <a data-toggle="collapse" data-parent="#accordion" href="#col7">Nuôi dạy con</a>
               </h4>
            </div>            
          </div>
         <!-- ./panel --> 
         <!-- Panel 8 -->
          <div class="panel panel-info">
            <div class="panel-heading">
               <h4 class="panel-title ifamilly">
                  <a data-toggle="collapse" data-parent="#accordion" href="#col8">Phụ nữ và gia đình</a>
               </h4>
            </div>            
          </div>
         <!-- ./panel --> 
         <!-- Panel 9 -->
          <div class="panel panel-info">
            <div class="panel-heading">
               <h4 class="panel-title ipersonal">
                  <a data-toggle="collapse" data-parent="#accordion" href="#col9">Phát triển cá nhân</a>
               </h4>
            </div>            
          </div>
         <!-- ./panel --> 
         <!-- Panel 10 -->
          <div class="panel panel-info">
            <div class="panel-heading">
               <h4 class="panel-title ilife">
                  <a data-toggle="collapse" data-parent="#accordion" href="#col10">Nghệ thuật và đời sống</a>
               </h4>
            </div>            
          </div>
         <!-- ./panel --> 
         <!-- Panel 11 -->
          <div class="panel panel-info">
            <div class="panel-heading">
               <h4 class="panel-title ilearn">
                  <a data-toggle="collapse" data-parent="#accordion" href="#col11">Luyện thi</a>
               </h4>
            </div>            
          </div>
         <!-- ./panel --> 
         <!-- Panel 12 -->
          <div class="panel panel-info">
            <div class="panel-heading">
               <h4 class="panel-title ilanguage">
                  <a data-toggle="collapse" data-parent="#accordion" href="#col12">Ngoại ngữ</a>
               </h4>
            </div>            
          </div>
         <!-- ./panel --> 
         <!-- Panel 13 -->
          <div class="panel panel-info">
            <div class="panel-heading">
               <h4 class="panel-title iseeall">
                  <a data-toggle="collapse" data-parent="#accordion" href="#col13">Tất cả danh mục</a>
               </h4>
            </div>            
          </div>
         <!-- ./panel -->        
        </div>
      <!-- ./panel-group -->
    </div><!--end .content-->    
</div><!--end #nav-mobile-->

<!-- POPUP PRODUCT -->
<div class="modal fade k-popup-lesson" id="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
   <div class="modal-dialog" role="document">
      <div class="modal-content">                       
           <div class="modal-header">
              <button type="button" class="k-popup-lesson-close close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
              </button>
              <h4>Kỹ năng tư duy sáng tạo</h4>
           </div>
           <div class="modal-body clearfix">                              
                 <h6>Nguyễn Hoàng Khắc Hiếu / Tiến sĩ tâm lý học</h6>
                 <!--end .detail-title-->
                 <div class="col-md-6 col-xs-6 k-popup-lesson-content">                    
                    <div class="videoWrapper">
                      <!-- Copy & Pasted from YouTube -->
                      <iframe width="560" height="349" src="https://www.youtube.com/embed/3V4bGjB8vAU" frameborder="0" allowfullscreen=""></iframe>
                   </div>                                           
                    <!--end .videoWrapper-->
                    <div class="clearfix"></div>
                    <div class="k-popup-lesson-info">
                       <ul>
                          <li><i class="icon icon-clock-outline"></i> Thời lượng: 5h0'</li>
                          <li><i class="icon icon-profile"></i> Trình độ: Mới bắt đầu</li>
                          <li><i class="icon icon-arrow-circle-right-line"></i> Bài học: 12 bài</li>
                          <li><i class="icon icon-certificate"></i> Cấp chứng nhận hoàn thành</li>
                       </ul>
                    </div>
                    <!-- k-popup-lesson-info -->
                    <!--
                    <div id="fb-root"></div>
                    <script>
                       (function (d, s, id) {
                           // face book like
                           var js, fjs = d.getElementsByTagName(s)[0];
                           if (d.getElementById(id)) return;
                           js = d.createElement(s);
                           js.id = id;
                           js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.5";
                           fjs.parentNode.insertBefore(js, fjs);

                       }(document, 'script', 'facebook-jssdk'));
                    </script>
                    <div class="fb-like" data-href="https://developers.facebook.com/docs/plugins/" data-layout="standard" data-action="like" data-show-faces="true" data-share="true"></div>
                    -->
                 </div>
                 <!--end .k-popup-lesson-content-->
                 
                 <div class="col-md-6 col-xs-6 k-popup-lesson-detail">
                    <p>
                       Khóa học Kỹ năng tư duy sáng tạo của Ths. Nguyễn Hoàng Khắc Hiếu là công cụ đắc lực giúp bạn sáng tạo hơn, suy nghĩ mới hơn, giải quyết vấn đề linh hoạt hơn.                    
                    </p>
                    <ul class="k-popup-lesson-detail-price">
                       <li>
                          <span class="bold">VND 2,050,000</span>
                       </li>
                       <li>
                         <a href="#" data-pid="6" class="button add-to-cart">Đăng ký học</a>
                         <!--
                          <div class="alert alert-info">Bạn đã tham gia khóa học này.</div>-->
                       </li>
                    </ul>
                    <ul class="k-popup-lesson-detail-care">
                       <li><i class="icon icon-heart-o"></i> Quan tâm</li>
                       <li><a href="/khoa-hoc/ky-nang-tu-duy-sang-tao-p2"><i class="icon icon-long-arrow-right"></i> Xem chi tiết</a></li>
                    </ul>                    
                 </div>
                 <!--end .col-md-4 col-xs-12 left-->             
           </div>
           <!--end .modal-body-->            
      </div>
   </div>
</div>
<!-- END POPUP PRODUCT -->


<?php include 'inc/login.php'; ?>
<?php include 'inc/register.php'; ?>

<script type="text/javascript" src="/bower_components/jquery/dist/jquery.js"></script> 
<script type="text/javascript" src="/bower_components/tether/dist/js/tether.min.js"></script>   
<script type="text/javascript" src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script> 
<script type="text/javascript" src="../src/js/owl.carousel.min.js"></script> 
<script type="text/javascript" src="../src/js/main.js"></script>
<script type="text/javascript" src="../js/faq.js"></script>
<script type="text/javascript" src="../js/lesson.js"></script>


<script src="../src/js/offpage.js"></script>
<script type="text/javascript" src="../src/js/header.js"></script>
</body>
</html>
