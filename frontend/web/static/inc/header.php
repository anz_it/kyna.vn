<!DOCTYPE HTML>
<html lang="vi">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="/favo_ico.png">
    <title>Kyna</title>
    <!-- Bootstrap core CSS -->

    <link href="../css/main.min.css?version=1521198358" type="text/css" rel="stylesheet"/>
    <link href="../css/owl.carousel.css" rel="stylesheet"/>
    <link href="../css/owl.theme.css" rel="stylesheet"/>
    <link href="../css/owl.transitions.css" rel="stylesheet"/>
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,500,700,300' rel='stylesheet' type='text/css'>
    <!--<link href="https://file.myfontastic.com/mxTCtkdfZKioAG2DtSWHCm/icons.css" rel="stylesheet">-->
</head>
<body>
<header>
    <!-- -->
    <nav class="navbar navbar-light k-header-wrap">
        <div class="container">
            <a href="/" class="navbar-brand logo col-lg-2 col-md-2">
                <img src="/src/img/logo.svg" alt="Kyna.vn" class="img-fluid">
            </a>


            <ul class="nav navbar-nav k-header-info col-md-4 col-md-push-7 navbar-right">
                <li class="hotline">
                    <span class="text">Hotline</span>
                    <span class="number">1900 6364 09</span>
                </li>
                <li class="hotline">
                    <a href="/user/course/index" style="display: inline-block; padding: 10px 0;">Khóa học của tôi</a>
                </li>
            </ul>
            <div class="form-inline k-header-search col-md-4 col-md-pull-4 col-xs-12">
                <form id="search-form" action="/danh-sach-khoa-hoc" method="get">
                    <div class="input-group">
                        <input name="q" type="text" class="form-control" placeholder="Tìm khóa học bạn cần"
                               autocomplete="off">
                        <span class="input-group-btn">
                            <button class="btn btn-secondary search-button" type="submit"><i
                                    class="icon-search icon hidden-480"></i>
                            <span class="mob">Tìm</span></button>
                        </span>
                    </div>

                </form>
            </div>

        </div><!--end .container-->
    </nav>

    <!-- Menu mobile -->
    <div id="nav-mobile" class="offpage-menu offpage-left">
        <header>
            <div class="k-header-offpage-menu">
                <a href="/">
                    <img src="/src/img/logo.svg" alt="Kyna.vn" class="img-responsive">
                </a>
                <a href="#" class="right offpage-close" data-offpage="#nav-mobile">
                    <i class="icon icon-arrow-right-bold" aria-hidden="true"></i>
                </a>
            </div><!--header -->
        </header>
        <div class="content">
            <div class="panel-group" id="accordion">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <div class="panel-title giao-tiep">
                            <a href="/danh-sach-khoa-hoc/giao-tiep-c16">Giao tiếp</a>
                        </div>
                    </div>
                </div>
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <div class="panel-title kham-pha-ban-than">
                            <a href="/danh-sach-khoa-hoc/kham-pha-ban-than-c17">Khám phá bản thân</a>
                        </div>
                    </div>
                </div>
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <div class="panel-title kinh-doanh-khoi-nghiep">
                            <a href="/danh-sach-khoa-hoc/kinh-doanh-khoi-nghiep-c18">Kinh doanh - Khởi nghiệp</a>
                        </div>
                    </div>
                </div>
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <div class="panel-title nuoi-day-con">
                            <a href="/danh-sach-khoa-hoc/nuoi-day-con-c19">Nuôi dạy con</a>
                        </div>
                    </div>
                </div>
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <div class="panel-title cong-nghe">
                            <a href="/danh-sach-khoa-hoc/cong-nghe-c20">Công nghệ</a>
                        </div>
                    </div>
                </div>
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <div class="panel-title nghe-thuat-va-ngoai-ngu">
                            <a href="/danh-sach-khoa-hoc/nghe-thuat-va-ngoai-ngu-c21">Nghệ thuật và Ngoại ngữ</a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ./panel-group -->
        </div><!--end .content-->
    </div><!--end #nav-mobile-->
    <!-- End Menu mobile-->
</header>
