<?php include 'inc/header.php'; ?>

<main>
    <div class="container k-height-header k-faq">
        <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 k-faq-box">
           <a href="faq_detail.php#k-faq-sidebar-1">
                <div class="card">            
                  <img class="card-img-top" src="../src/img/faq/thanh-toan.png" alt="Cách thanh toán khi mua khóa học trên Kyna">
                  <img class="card-img-top-hv" src="../src/img/faq/thanh-toan-hv.png" alt="Cách thanh toán khi mua khóa học trên Kyna">
                  <div class="card-block">
                    <p class="card-text">Cách <strong>thanh toán</strong> khi mua khóa học trên Kyna</p>
                  </div>
                </div><!--end .card-->
            </a>
        </div><!--end .box-->

        <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 k-faq-box">
           <a href="faq_detail.php#k-faq-sidebar-2">
            <div class="card">
              <img class="card-img-top" src="../src/img/faq/dang-ky-khoa-hoc.png" alt="Cách đăng ký khóa học trên Kyna">
              <img class="card-img-top-hv" src="../src/img/faq/dang-ky-khoa-hoc-hv.png" alt="Cách đăng ký khóa học trên Kyna">
              <div class="card-block">
                <p class="card-text">Cách <strong>đăng ký khóa học</strong> trên Kyna</p>
              </div>
            </div><!--end .card-->
            </a>
        </div><!--end .box-->

        <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 k-faq-box">
           <a href="faq_detail.php#k-faq-sidebar-3">
            <div class="card">
              <img class="card-img-top" src="../src/img/faq/thoi-gian-hoc.png" alt="Thời gian học một khóa trên Kyna">
              <img class="card-img-top-hv" src="../src/img/faq/thoi-gian-hoc-hv.png" alt="Thời gian học một khóa trên Kyna">
              <div class="card-block">
                <p class="card-text"><strong>Thời gian học</strong> một khóa trên Kyna</p>
              </div>
            </div><!--end .card-->
            </a>
        </div><!--end .box-->

        <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 k-faq-box">
           <a href="faq_detail.php#k-faq-sidebar-4">
            <div class="card">
              <img class="card-img-top" src="../src/img/faq/hoan-hoc-phi.png" alt="Chính sách hoàn học phí trên Kyna">
               <img class="card-img-top-hv" src="../src/img/faq/hoan-hoc-phi-hv.png" alt="Chính sách hoàn học phí trên Kyna">
              <div class="card-block">
                <p class="card-text">Chính sách <strong>hoàn học phí</strong> trên Kyna</p>
              </div>
            </div><!--end .card-->
            </a>
        </div><!--end .box-->


        <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 k-faq-box">
           <a href="faq_detail.php#k-faq-sidebar-5">
            <div class="card">
               <img class="card-img-top" src="../src/img/faq/hoc-truc-tuyen.png" alt="Cách học trực tuyến trên Kyna">
               <img class="card-img-top-hv" src="../src/img/faq/hoc-truc-tuyen-hv.png" alt="Cách học trực tuyến trên Kyna">
              <div class="card-block">
                <p class="card-text">Cách <strong>học trực tuyến</strong> trên Kyna</p>
              </div>
            </div><!--end .card-->
            </a>
        </div><!--end .box-->


        <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 k-faq-box">
           <a href="faq_detail.php#k-faq-sidebar-6">
            <div class="card">
              <img class="card-img-top" src="../src/img/faq/mua-va-kich-hoat.png" alt="Các mua và kích hoạt thẻ Cyberpay">
              <img class="card-img-top-hv" src="../src/img/faq/mua-va-kich-hoat-hv.png" alt="Các mua và kích hoạt thẻ Cyberpay">
              <div class="card-block">
                <p class="card-text">Cách <strong>mua và kích hoạt</strong> thẻ Cyberpay</p>
              </div>
            </div><!--end .card-->
            </a>
        </div><!--end .box-->


        <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 k-faq-box">
           <a href="faq_detail.php#k-faq-sidebar-7">
            <div class="card">
              <img class="card-img-top" src="../src/img/faq/uy-tin-xac-thuc.png" alt="Tại sao Kyna.vn là đơn vị uy tín, xác thực">
              <img class="card-img-top-hv" src="../src/img/faq/uy-tin-xac-thuc-hv.png" alt="Tại sao Kyna.vn là đơn vị uy tín, xác thực">
              <div class="card-block">
                <p class="card-text">Tại sao Kyna.vn là đơn vị <strong>uy tín, xác thực</strong></p>
              </div>
            </div><!--end .card-->
            </a>
        </div><!--end .box-->


        <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 k-faq-box">
           <a href="faq_detail.php#k-faq-sidebar-8">
            <div class="card">
            <img class="card-img-top" src="../src/img/faq/cap-chung-nhan.png" alt="Khóa học có cấp chứng nhân không?">
            <img class="card-img-top-hv" src="../src/img/faq/cap-chung-nhan-hv.png" alt="Khóa học có cấp chứng nhân không?">

              <div class="card-block">
                <p class="card-text">Khóa học có <strong>cấp chứng nhận</strong> không?</p>
              </div>
            </div><!--end .card-->
            </a>
        </div><!--end .box-->


        <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 k-faq-box">
           <a href="faq_detail.php#k-faq-sidebar-9">
            <div class="card">
              <img class="card-img-top" src="../src/img/faq/ho-tro.png" alt="Làm sao để được hỗ trợ khi học trên Kyna">
              <img class="card-img-top-hv" src="../src/img/faq/ho-tro-hv.png" alt="Làm sao để được hỗ trợ khi học trên Kyna">
              <div class="card-block">
                <p class="card-text">Làm sao để được <strong>hỗ trợ</strong> khi học trên Kyna</p>
              </div>
            </div><!--end .card-->
            </a>
        </div><!--end .box-->


        <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 k-faq-box">
           <a href="faq_detail.php#k-faq-sidebar-10">
            <div class="card">
              <img class="card-img-top" src="../src/img/faq/hoc-online.png" alt="Vì sao nên học online">
              <img class="card-img-top-hv" src="../src/img/faq/hoc-online-hv.png" alt="Vì sao nên học online">
              <div class="card-block">
                <p class="card-text">Vì sao nên <strong>học online</strong></p>
              </div>
            </div><!--end .card-->
            </a>
        </div><!--end .box-->
    </div><!--end .container-->
</main>

<main>
    <div class="container k-height-header k-faq-wrap">
        <section id="k-faq-sidebar" class="col-sm-4">
            <h4>Câu hỏi thường gặp</h4>
            <ul class="nav nav-tabs" role="tablist">
                <li class="nav-item icon-arrow-right">
                    <a class="nav-link active" data-toggle="tab" href="#k-faq-sidebar-1" role="tab">1. Làm thế nào để mua khóa học tại Kyna.vn?</a>
                </li>
                <li class="nav-item icon-arrow-right">
                    <a class="nav-link" data-toggle="tab" href="#k-faq-sidebar-2" role="tab">2. Sau khi đăng ký khóa học, tôi phải thanh toán khóa học bằng cách nào?</a>
                </li>
                <li class="nav-item icon-arrow-right">
                    <a class="nav-link" data-toggle="tab" href="#k-faq-sidebar-3" role="tab">3. Một khóa học mà tôi đăng ký tại Kyna.vn có thời hạn trong bao lâu?</a>
                </li>
                <li class="nav-item icon-arrow-right">
                    <a class="nav-link" data-toggle="tab" href="#k-faq-sidebar-4" role="tab">4. Tôi có được hoàn học phí nếu thấy không hài lòng không?</a>
                </li>
                <li class="nav-item icon-arrow-right">
                    <a class="nav-link" data-toggle="tab" href="#k-faq-sidebar-5" role="tab">5. Học Online có đáng tin không?</a>
                </li>
                <li class="nav-item icon-arrow-right">
                    <a class="nav-link" data-toggle="tab" href="#k-faq-sidebar-6" role="tab">6. Ai sẽ giải đáp những thắc mắc trong khi học cho tôi?</a>
                </li>
                <li class="nav-item icon-arrow-right">
                    <a class="nav-link" data-toggle="tab" href="#k-faq-sidebar-7" role="tab">7. Sau khi hoàn thành khóa học, tôi có được cấp chứng nhận gì không?</a>
                </li>
                <li class="nav-item icon-arrow-right">
                    <a class="nav-link" data-toggle="tab" href="#k-faq-sidebar-8" role="tab">8. Kyna.vn có đáng tin không?</a>
                </li>
                <li class="nav-item icon-arrow-right">
                    <a class="nav-link" data-toggle="tab" href="#k-faq-sidebar-9" role="tab">9. Hướng dẫn học Online tại Kyna.vn</a>
                </li>
                <li class="nav-item icon-arrow-right">
                    <a class="nav-link" data-toggle="tab" href="#k-faq-sidebar-10" role="tab">10. Mua và kích hoạt thẻ học bằng Cyberpay bằng cách nào?</a>
                </li>
            </ul>
        </section>
        <section id="k-faq-content" class="col-sm-8">
            <div class="tab-content">
                <div class="tab-pane active" id="k-faq-sidebar-1" role="tabpanel">
                    <h3>Làm thế nào để mua khóa học tại Kyna.vn?</h3>
                    <p>1. Làm thế nào để mua khóa học tại Kyna.vn?</p>
                    <p>Quý khách có thể mua khóa học trực tiếp tại website Kyna.vn thông qua 5 bước cơ bản:</p>
                    <p><strong>Bước 1: Tìm kiếm khóa học</strong></p>
                    <p>Qúy khách có thể tìm sản phẩm theo 3 cách:</p>
                    <p>a. Gõ tên sản phẩm vào thanh tìm kiếm</p>
                    <p>b. Tìm theo “Danh mục khóa học”</p>
                    <p>c. Tìm theo mục “Các khóa học mới nhất”, “Các khóa học nổi bật”.</p>
                    <p>Trong trường hợp quý khách muốn đăng ký các chương trình Ưu đãi nhóm, sau khi
                        chọn “Danh mục khóa học”, vui lòng chọn “Ưu đãi nhóm” để được dẫn về trang giới
                        thiệu các Nhóm khóa học của Kyna.vn: https://kyna.vn/khuyen-mai-nhom-khoa-hoc
                    </p>
                    <p><strong>Bước 2: Thêm khóa học vào giỏ hàng</strong></p>
                    <p>Khi đã tìm được khóa học mong muốn, quý khách vui lòng bấm vào hình ảnh hoặc tên sản phẩm để vào trang thông tin chi tiết của khóa học, sau đó:</p>
                    <p>a. Xem thông tin khóa học: intro, giá, thông tin khóa học, thông tin giảng viên, thông tin khuyến mãi (nếu có).</p>
                    <p>b. Thêm khóa học vào giỏ hàng.</p>
                    <p><strong>Bước 3: Kiểm tra giỏ hàng và đặt hàng khóa học</strong></p>
                    <p>Nếu quý khách muốn đăng ký nhiều khóa học khác nhau trong cùng 1 đơn hàng, vui
                        lòng thực hiện theo các bước sau:
                    </p>
                    <p>a. Chọn nút &quot;Tiếp tục mua&quot; hoặc click vào logo Kyna để trở về trang chủ</p>
                    <p>b. Thêm khóa học vào giỏ hàng như Bước 2.</p>
                    <p>Qúy khách có thể lặp lại qúa trình này cho đến khi hoàn tất việc chọn tất cả các khóa
                        học mà quý khách muốn đặt mua vào giỏ hàng. 
                    </p>
                    <p>Sau khi hoàn tất các bước trên quý khách có thể sử dụng Mã coupon, voucher (nếu
                        có) và tiếp tục các bước sau để đặt hàng: 
                    </p>
                    <p>c. Điều chỉnh số lượng và cập nhật giỏ hàng</p>
                    <p>d. Bấm &quot;Đăng ký học&quot; để bắt đầu đặt hàng. </p>
                    <p><strong>Bước 4: Đăng nhập hoặc đăng ký tài khoản tại Kyna.vn</strong></p>
                    <p>Trong trường hợp, quý khách không có tài khoản trên Kyna.vn:</p>
                    <p>Sau khi chọn “đăng ký học”, hệ thống sẽ yêu cầu quý khách đăng ký để có đủ quyền
                        thanh toán khóa học. Qúy khách có thể điền thông tin đăng ký và tạo tài khoản trên
                        Kyna.vn. Để thuận lợi và tiết kiệm thời gian quý khách có thể đăng nhập bằng tài
                        khoản Facebook cá nhân và sử dụng tài khoản đó như tài khoản Kyna.vn.
                    </p>
                    <p>Trong trường hơn, quý khách đã có tài khoản trên Kyna.vn:</p>
                    <p>Sau khi chọn “đăng ký học”, quý khách có thể đăng nhập ngay hoặc giữ mặc định
                        đăng nhập khi vào trang Kyna.vn
                    </p>
                    <p><strong>Bước 5: Chọn cách thanh toán và tiến hành thanh toán</strong></p>
                    <p>Sau khi hoàn tất giỏ hàng và đăng nhập thành công, hệ thống sẽ chuyển đến mục
                        thanh toán khoá học.
                    </p>
                    <p>Tại Kyna.vn, quý khách có 5 phương thức thanh toán khóa học để chọn lựa:</p>
                    <ol>
                        <li>- Giao khóa học và thu tiền tận nơi (COD)</li>
                        <li>- Thanh toán bằng mã thẻ cào điện thoại</li>
                        <li>- Thanh toán trực tuyến bằng thẻ ATM hoặc thẻ VISA MASTER</li>
                        <li>- Chuyển khoản ngân hàng</li>
                        <li>- Thanh toán trực tiếp tại văn phòng Kyna.vn</li>
                    </ol>
                    <p>Sau khi chọn phương thức thanh toán phù hợp, quý khách có thể nhập lại <span class="font-italic">Mã Coupon,
                            voucher (nếu có)</span>.
                    </p>
                    <p>Sau khi thanh toán thành công, hệ thống sẽ gửi cho quý khách một thông báo xác nhận
                        thanh toán thành công và quý khách có thể bắt đầu tham gia khóa học.
                    </p>
                </div>
                <div class="tab-pane" id="k-faq-sidebar-2" role="tabpanel">
                    <h3>Sau khi đăng ký khóa học, tôi phải thanh toán khóa học bằng cách nào?</h3>
                    <p> Tại Kyna.vn, quý khách có 5 phương thức thanh toán khóa học để chọn lựa:</p>
                    <ul class="list-first">
                        <li>- Giao khóa học và thu tiền tận nơi (COD)</li>
                        <li>- Thanh toán bằng mã thẻ cào điện thoại</li>
                        <li>- Thanh toán trực tuyến bằng thẻ ATM hoặc thẻ VISA MASTER</li>
                        <li>- Chuyển khoản ngân hàng</li>
                        <li>- Thanh toán trực tiếp tại văn phòng Kyna.vn</li>
                    </ul>
                    <p><strong>Cách 1: Giao khóa học và thu tiền tận nơi (COD)</strong></p>
                    <p>Kyna.vn hỗ trợ giao hàng và thanh toán tận nơi trên toàn quốc theo khung sau:</p>
                    <ul class="list-first">
                        <li>a. COD chỉ được áp dụng đối với đơn hàng có giá trị từ 140.000 đồng trở lên</li>
                        <li>b. COD miễn phí đối với các đơn hàng có giá trị từ 300.000 đồng trở lên (áp dụng trên toàn quốc).</li>
                    </ul>
                    <p>
                        * Riêng đối với các đơn hàng nội thành TP Hồ Chí Minh và nội thành TP Hà Nội
                        thì COD sẽ áp dụng miễn phí đối với các đơn hàng có giá trị từ 140.000 đồng trở
                        lên. Chính sách miễn phí chỉ áp dụng cho lần giao hàng đầu tiên cho một đơn
                        hàng, nếu việc giao hàng thất bại thì các lần giao hàng sau (đối với đơn hàng đó)
                        Kyna.vn sẽ tính phí theo biểu phí thường lệ.
                    </p>
                    <p>
                        c. Đối với những đơn hàng dưới 300.000 VNĐ, Kyna.vn sẽ áp dụng biểu giá của
                        dịch vụ giao nhận như sau:
                    </p>
                    <ul class="list-first">
                        <li>- Phí giao nhận tại các quận ngoại thành Tp. Hồ Chí Minh, ngoại thành Hà Nội: 10.000VNĐ</li>
                        <li>- Phí giao nhận tại khu vực nội thành các tỉnh khác: 20.000 VNĐ</li>
                        <li>- Phí giao nhận tại khu vực ngoại thành tỉnh khác: 30.000 VNĐ</li>
                    </ul>
                    <p> Đối với phương thức COD, qúy khách sẽ nhận hàng và thanh toán trực tiếp với nhân
                        viên giao hàng.
                    </p>
                    <p><strong>Cách 2: Thanh toán bằng thẻ cào điện thoại:</strong></p>
                    <p><strong>Kyna.vn chỉ áp dụng thanh toán với thẻ cào Mobifone và Vinaphone</strong></p>
                    <p>Để tiến hành thanh toán bằng thẻ cào điện thoại, quý khách vui lòng thực hiện các
                        bước sau:
                    </p>
                    <p>Bước 1: Mua thẻ cào Mobifone hoặc Vinaphone với giá trị tương ứng. Vào trang
                        Phương thức thanh toán, bạn vui lòng chọn Thanh toán bằng thẻ cào điện thoại
                    </p>
                    <p>Bước 2: Cào lớp tráng bạc để biết mã thẻ. Chọn loại thẻ, nhập mã thẻ và số sê-ri vào ô
                        trống, cuối cùng chọn nút Nạp tiền.
                    </p>
                    <p><strong>Lưu ý viết liền không khoảng trắng hoặc dấu –</strong></p>
                    <p>Nếu quý khách có nhiều thẻ cào, vui lòng lặp lại các thao tác trên.
                    <p>
                    <p><strong>Cách 3: Thanh toán trực tuyến bằng thẻ ATM hoặc thẻ VISA MASTER</strong></p>
                    <p><strong>Thanh toán qua thẻ ATM có đăng ký thanh toán trực tuyến:</strong></p>
                    <p>Để sử dụng phương thức thanh toán này, tài khoản ngân hàng của quý khách cần đăng
                        ký dịch vụ Internet Banking với ngân hàng. Kyna.vn hiện hỗ trợ thanh toán cho phần
                        lớn các ngân hàng tại Việt Nam:
                    </p>
                    <p><strong>Thanh toán qua thẻ Visa/Master:</strong></p>
                    <p>Phí thanh toán tùy thuộc vào từng loại thẻ quý khách dùng và ngân hàng phát hành
                        thẻ. Vui lòng liên hệ với ngân hàng phát hành thẻ để biết rõ phí thanh toán phát sinh.
                    </p>
                    <p><strong>Bước 1:</strong> Nhập số tiền cần nạp vào ví, nếu ví chưa có tiền, quý khách vui lòng điền giá
                        trị bằng với giá khóa học thành tiền hiện bạn đang cần trả.
                    </p>
                    <p>Nếu ví đã có tiền nhưng chưa đủ để thanh toán, quý khách chỉ cần nạp phần chênh
                        lệch. Sau đó chọn Thanh toán online bằng thẻ VISA, MASTERCARD, JCB và nhấp
                        vào nút Thanh toán.
                    </p>
                    <p><strong>Bước 2:</strong> Hệ thống sẽ dẫn về trang thanh toán của Đối tác Onepay Payment Gateway.
                        Tại đây quý khách hãy chọn và điền đầy đủ thông tin theo yêu cầu để thực hiện thanh
                        toán. Nếu thẻ hợp lệ, quý khách sẽ thanh toán thành công.
                    </p>
                    <p><strong> Cách 4: Chuyển khoản ngân hàng</strong></p>
                    <p><strong>Chuyển khoản qua ngân hàng</strong></p>
                    <p>Bạn có thể đến bất kỳ ngân hàng nào ở Việt Nam (hoặc sử dụng Internet Banking) để
                        chuyển tiền theo thông tin bên dưới:
                    </p>
                    <ul class="list-last">
                        <li>Số tài khoản: 0531 0025 11245</li>
                        <li>Chủ tài khoản: Công ty cổ phần Dream Viet Education</li>
                        <li>Ngân hàng: Ngân hàng Vietcombank, chi nhánh Đông Sài Gòn, TP.HCM</li>
                    </ul>
                    <p>Ghi chú khi chuyển khoản:</p>
                    <ul class="list-last">
                        <li>Tại mục “Ghi chú” khi chuyển khoản, bạn ghi rõ: Số điện thoại – Họ và tên – Email đăng ký học – Khóa học đăng ký</li>
                        <li>Ví dụ: 0909090909 – Nguyen Thi Huong Lan – nguyenthihuonglan@gmail.com Kỹ năng quản lý cảm xúc</li>
                    </ul>
                    <p><strong>Chuyển khoản qua PayPal</strong></p>
                    <ul class="list-last">
                        <li>Địa chỉ email ketoan@kyna.vn. Tỉ giá áp dụng 1 USD = 21.000 VND (tỉ giá trên PayPal)</li>
                        <li>Tại mục “Message” khi chuyển tiền, bạn ghi rõ: Số điện thoại – Họ và tên – Email đăng ký học – Khóa học đăng ký</li>
                    </ul>
                    <p><strong>Cách 5: Thanh toán tại văn phòng KYNA.VN</strong></p>
                    <ul class="list-last">
                        <li>Địa chỉ văn phòng: Tầng 6, Tòa nhà Thịnh Phát, 178/8 Đường D1, Phường 25,
                            Quận Bình Thạnh, Thành phố Hồ Chí Minh, Việt Nam
                        </li>
                        <li>Thời gian làm việc từ 08:30 – 17:30, từ thứ Hai tới thứ Bảy hàng tuần.</li>
                    </ul>
                    <p>Trong trường hợp bạn đã có đủ tiền trong ví tại Kyna.vn:</p>
                    <p>Khi này, bạn sẽ được chuyển đến trang xác nhận thanh toán, bạn chỉ cần click
                        nút “Xác nhận thanh toán”. Khi này sẽ có thông báo thanh toán thành công và bạn có
                        thể bắt đầu vào học ngay!
                    </p>
                </div>
                <div class="tab-pane" id="k-faq-sidebar-3" role="tabpanel">
                    <h3>Một khóa học mà tôi đăng ký tại Kyna.vn có thời hạn trong bao lâu?</h3>
                    <p>Đối với mọi khóa học trên Kyna.vn, bạn chỉ cần thanh toán một lần và được quyền sở
                        hữu bài học mãi mãi. Điều này đồng nghĩa với việc bạn có thể xem lại bài học bất cứ
                        lúc nào bạn muốn chỉ với một lần thanh toán.
                    </p>
                </div>
                <div class="tab-pane" id="k-faq-sidebar-4" role="tabpanel">
                    <h3>Tôi có được hoàn học phí nếu thấy không hài lòng không?</h3>
                    <p>Nếu có bất cứ sự không hài lòng nào về hiệu quả khóa học, học viên đều có thể được hoàn học phí. Điều kiện để hoàn học phí như sau:</p>
                    <ul class="list-first">
                        <li>- Hoàn học phí khi học viên thấy không hài lòng về khóa học</li>
                        <li>- Thời hạn cho việc hoàn học phí là 30 ngày kể từ ngày thanh toán khóa học</li>
                        <li>- Chỉ áp dụng với học viên học theo phương pháp chuẩn (tiến độ mặc định mỗi ngày một vài bài học như hệ thống học đưa ra).</li>
                    </ul>
                    <p> Để tiến hành phản hồi và nhận lại học phí, quý khách vui lòng gửi email đến hotro@kyna.vn. Trong các trường hợp trên, quý khách chỉ cần gửi email phản hồi sẽ được hoàn học phí 100% mà không cần phải giải thích lý do gì cả.</p>
                    <p>Lưu ý:</p>
                    <ul class="list-last">
                        <li>Số tiền quý khách nhận được sẽ trừ đi phí thanh toán (nếu có) và phí chuyển khoản (nếu có).</li>
                        <li>Qúy khách sẽ nhận lại học phí trong vòng 7 ngày kể từ lúc gửi yêu cầu hoàn học phí.</li>
                        <li>Không áp dụng hoàn học phí với học viên học theo phương pháp cấp tốc (tất cả các bài học đều hiện ra hết một lần).</li>
                    </ul>
                </div>
                <div class="tab-pane" id="k-faq-sidebar-5" role="tabpanel">
                    <h3>Học Online có đáng tin không?</h3>
                    <p>
                        Sự kỳ vọng của cả thế giới vào xu hướng e-learning là có lý do. Ở các nước có tốc độ
                        phát triển nhanh trên thế giới, e-learning - học Online được xem là một giải pháp hiệu
                        quả nhất trong học tập. Bạn có thể học bất cứ lúc nào bạn muốn và bạn có thể làm chủ
                        không gian, thời gian học của mình. <br/>
                        Nội dung các chương trình học tại Kyna.vn đảm bảo chất lượng, bắt kịp với xu hướng
                        hiện đại. Hơn hết, khi học Online tại Kyna.vn bạn sẽ được học cùng giảng viên,
                        chuyên gia hàng đầu trong mọi lĩnh vực tại Việt Nam. Bạn có thể vẫn đảm bảo công
                        việc, sở thích, trách nhiệm gia đình nhưng vẫn có thể cùng Kyna.vn phấn đấu vì sự
                        nghiệp và mục tiêu của mình.
                    </p>
                </div>
                <div class="tab-pane" id="k-faq-sidebar-6" role="tabpanel">
                    <h3>Ai sẽ giải đáp những thắc mắc trong khi học cho tôi?</h3>
                    <p>
                        Khi tham gia học tại Kyna.vn, quý khách sẽ được tham gia vào một môi trường học có
                        tính tương tác cao.
                    </p>
                    <p>Tại Kyna.vn, bạn sẽ nhận được sự trợ giúp, giải đáp thắc mắc từ:</p>
                    <ul class="list-first">
                        <li>- Bạn học</li>
                        <li>- Giảng viên</li>
                        <li>- Chuyên gia</li>
                        <li>- Tư vấn viên của Kyna.vn</li>
                    </ul>
                    <p>
                        Trong một số khóa học, bạn cũng sẽ được cung cấp email của giảng viên để được trao
                        đổi trực tiếp với giảng viên.
                    </p>
                    <p>
                        Một số giảng viên cũng có thể tổ chức các buổi giao lưu trực tuyến hoặc giao lưu trực
                        tiếp (miễn phí hoặc có tính phí tùy vào từng giảng viên) cùng học viên (khoảng vài
                        tuần một lần).
                    </p>
                    <p>
                        Nếu bạn có thắc mắc về các vấn đề kỹ thuật, bạn có thể liên hệ với Kyna.vn qua email
                        hotro@kyna.vn hoặc số điện thoại 1900 6364 09 (1000đ/phút, 8h30-22h00 kể cả T7,
                        CN) để được hỗ trợ.
                    </p>
                </div>
                <div class="tab-pane" id="k-faq-sidebar-7" role="tabpanel">
                    <h3>Sau khi hoàn thành khóa học, tôi có được cấp chứng nhận gì không?</h3>
                    <p>
                        Tất cả các khóa học trên kyna.vn đều cấp chứng nhận hoàn thành khóa học đối với
                        những học viên hoàn thành các bài tập trong khóa học và hoàn thành bài tiểu luận cuối
                        khóa học.
                    </p>
                    <p>
                        Học viên có thể lựa chọn việc nhận chứng nhận online (hoàn toàn miễn phí, học viên
                        sẽ nhận được file ảnh chứng nhận gửi qua email học viên) hoặc nhận offline (nhận bản
                        in, có chữ ký và đóng dấu công ty, gửi về tận nhà cho học viên. Chi phí in ấn, lồng
                        khung gỗ, cấp chứng nhận và gửi chuyển phát nhanh về tận nhà là 200,000đ).
                    </p>
                </div>
                <div class="tab-pane" id="k-faq-sidebar-8" role="tabpanel">
                    <h3>Kyna.vn có đáng tin không?</h3>
                    <p> Kyna.vn là một sản phẩm thuộc hệ sinh thái giáo dục trực tuyến (e-learning) KYNA.
                        Từ khi thành lập vào năm 2013 đến nay, Kyna.vn đã thu hút hơn 250.000 học viên,
                        xây dựng trên 6000 video bài giảng với sự cộng tác của 150+ chuyên gia đầu ngành.
                        Kyna.vn đã trở thành địa chỉ học tập đáng tin cậy của hàng trăm nghìn học viên trên
                        toàn Việt Nam.
                    </p>
                    <p>
                        Kyna.vn rất mong muốn trở thành nguồn sức mạnh tích cực trên con đường phấn đấu
                        vì tương lai của hàng triệu người Việt.
                    </p>
                    <ul class="list-last">
                        <li>Để xác nhận uy tín của công ty, quý khách vui lòng xem giới thiệu về Kyna.vn,
                            các thành tích đạt được cũng như Kyna.vn trên các phương tiện truyền
                            thông: http://kyna.vn/gioi-thieu
                        </li>
                        <li>Để trải nghiệm chất lượng, phương pháp học tập tại Kyna.vn, bạn có thể tham
                            gia một số khóa học miễn phí của Kyna.vn tại https://kyna.vn/danh-sach- khoa-
                            hoc/mien-phi
                        </li>
                    </ul>
                    <p>
                        Mọi thắc mắc trong quá trình học, bạn liên hệ với email hotro@kyna.vn hoặc số điện
                        thoại 1900 6364 09 (1000đ/phút, 8h30-22h00 kể cả T7, CN) để được hỗ trợ ngay.
                    </p>
                </div>
                <div class="tab-pane" id="k-faq-sidebar-9" role="tabpanel">
                    <h3>Hướng dẫn học Online tại Kyna.vn</h3>
                    <iframe width="560" height="315" src="https://www.youtube.com/embed/qifLMpgUEZc" frameborder="0" allowfullscreen></iframe>
                </div>
                <div class="tab-pane" id="k-faq-sidebar-10" role="tabpanel">
                    <h3>Mua và kích hoạt thẻ học bằng Cyberpay bằng cách nào?</h3>
                    <p><strong>1. Địa điểm mua thẻ học online Kyna.vn</strong></p>
                    <p>
                        Qúy khách có thể tìm mua thẻ học online Kyna tại hơn 15.000 điểm giao dịch
                        Cyberpay trên toàn quốc.
                    </p>
                    <p>
                        Thông tin chi tiết các địa điểm giao dịch của Cyberpay tại đây:
                        http://info.cyberpay.vn/dai-ly/diem-giao-dich-cyberpay
                    </p>
                    <p>
                        Qúy khách chọn loại thẻ (Thẻ học 100.000 đồng hoặc 150.000 đồng) và số lượng thẻ
                        cần mua. Sau khi thanh toán tại các địa điểm giao dịch của Cyberpay, quý khách sẽ
                        nhận được một thẻ mềm và tin nhắn xác nhận. Vui lòng kiểm tra lại loại thẻ đúng với
                        nhu cầu của bạn và đảm bảo thẻ nạp, mã nạp còn nguyên vẹn.  
                    </p>
                    <p><strong>2. Danh sách các khóa học thẻ học online Kyna.vn</strong></p>
                    <p>Với các thẻ học 100.000 đồng và 150.000 đồng mua tại Cyberpay, quý khách có thể
                        lựa chọn thỏa thích một trong các khóa học sau tùy theo mệnh giá thẻ; chi phí rẻ hơn
                        rất nhiều so với học phí gốc. Xem thêm về thẻ học online Kyna.vn phát hành
                        tại Cyberpay tại đây: https://kyna.vn/bai-viet/kyna-hop-tac-cung-cyberpay-phat-hanh-the-hoc-online-Kyna 
                    </p>
                    <p><strong>3. Các bước kích hoạt thẻ học online Kyna.vn</strong></p>
                    <p><strong>Bước 1: Truy cập website donggia.kyna.vn </strong></p>
                    <p>  a. Nếu quý khách chưa đăng ký tài khoản trên Kyna.vn. Hệ thống sẽ chuyển sang
                        giao diện tạo tài khoản, thực hiện các bước sau:
                    </p>
                    <ul>
                        <li>+ Đăng ký tài khoản:  Chọn “Đăng ký” hoặc chọn “Đăng nhập bằng Facebook”
                            (nếu bạn có tài khoản Facebook).
                        </li>
                        <li>+ Nếu quý khách chọn nút “Đăng ký”, điền đầy đủ thông tin như hướng dẫn sau
                            đó hệ thống sẽ chuyển về trang Kích hoạt mã thẻ. 
                        </li>
                    </ul>
                    <p>
                        b. Nếu quý khách đã có tài khoản trên Kyna.vn hoặc sau khi đã hoàn thành đăng ký
                        tài khoản, quý khách sẽ được chuyển về trang Kích hoạt mã thẻ, nhập MÃ NẠP in
                        trên phiếu vào khung, sau đó nhấn vào nút “Kích hoạt”. 
                    </p>
                    <p><strong>
                            Bước 2. Sau khi nhập mã nạp, chọn khóa học mà quý khách muốn học trong
                            danh sách                    
                        </strong>
                    </p>
                    <p>
                        <strong>a. Đối với loại thẻ 100k:</strong> Xuất hiện giao diện khóa học tương ứng, chọn các
                        khóa bạn muốn học và chọn “Xác nhận”.
                    </p>
                    <p>
                        <strong> b. Đối với loại thẻ 150k:</strong> Xuất hiện giao diện khóa học tương ứng, chọn các
                        khóa bạn muốn học và chọn nút “Xác nhận”.
                    </p>
                    <p><strong>
                            Bước 3. Tham gia khóa học
                        </strong>
                    </p>
                    <p>
                        Sau khi chọn “Xác nhận”, giao diện sẽ chuyển sang mục &quot;Khóa học của tôi&quot;, chọn
                        khóa học và bắt đầu tham gia khóa học.
                    </p>
                </div>
            </div>
        </section>
    </div>
    <!--end .container-->

    <div class="container k-height-header k-faq-wrap-mobile">
        <div id="accordion" role="tablist" aria-multiselectable="true">
            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="heading1">
                    <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse1" aria-expanded="true" aria-controls="collapse1">
                            1. Làm thế nào để mua khóa học tại Kyna.vn?
                        </a>
                    </h4>
                </div>
                <div id="collapse1" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading1">                                        
                    <p>Quý khách có thể mua khóa học trực tiếp tại website Kyna.vn thông qua 5 bước cơ bản:</p>
                    <p><strong>Bước 1: Tìm kiếm khóa học</strong></p>
                    <p>Qúy khách có thể tìm sản phẩm theo 3 cách:</p>
                    <p>a. Gõ tên sản phẩm vào thanh tìm kiếm</p>
                    <p>b. Tìm theo “Danh mục khóa học”</p>
                    <p>c. Tìm theo mục “Các khóa học mới nhất”, “Các khóa học nổi bật”.</p>
                    <p>Trong trường hợp quý khách muốn đăng ký các chương trình Ưu đãi nhóm, sau khi
                        chọn “Danh mục khóa học”, vui lòng chọn “Ưu đãi nhóm” để được dẫn về trang giới
                        thiệu các Nhóm khóa học của Kyna.vn: https://kyna.vn/khuyen-mai-nhom-khoa-hoc
                    </p>
                    <p><strong>Bước 2: Thêm khóa học vào giỏ hàng</strong></p>
                    <p>Khi đã tìm được khóa học mong muốn, quý khách vui lòng bấm vào hình ảnh hoặc tên sản phẩm để vào trang thông tin chi tiết của khóa học, sau đó:</p>
                    <p>a. Xem thông tin khóa học: intro, giá, thông tin khóa học, thông tin giảng viên, thông tin khuyến mãi (nếu có).</p>
                    <p>b. Thêm khóa học vào giỏ hàng.</p>
                    <p><strong>Bước 3: Kiểm tra giỏ hàng và đặt hàng khóa học</strong></p>
                    <p>Nếu quý khách muốn đăng ký nhiều khóa học khác nhau trong cùng 1 đơn hàng, vui
                        lòng thực hiện theo các bước sau:
                    </p>
                    <p>a. Chọn nút &quot;Tiếp tục mua&quot; hoặc click vào logo Kyna để trở về trang chủ</p>
                    <p>b. Thêm khóa học vào giỏ hàng như Bước 2.</p>
                    <p>Qúy khách có thể lặp lại qúa trình này cho đến khi hoàn tất việc chọn tất cả các khóa
                        học mà quý khách muốn đặt mua vào giỏ hàng. 
                    </p>
                    <p>Sau khi hoàn tất các bước trên quý khách có thể sử dụng Mã coupon, voucher (nếu
                        có) và tiếp tục các bước sau để đặt hàng: 
                    </p>
                    <p>c. Điều chỉnh số lượng và cập nhật giỏ hàng</p>
                    <p>d. Bấm &quot;Đăng ký học&quot; để bắt đầu đặt hàng. </p>
                    <p><strong>Bước 4: Đăng nhập hoặc đăng ký tài khoản tại Kyna.vn</strong></p>
                    <p>Trong trường hợp, quý khách không có tài khoản trên Kyna.vn:</p>
                    <p>Sau khi chọn “đăng ký học”, hệ thống sẽ yêu cầu quý khách đăng ký để có đủ quyền
                        thanh toán khóa học. Qúy khách có thể điền thông tin đăng ký và tạo tài khoản trên
                        Kyna.vn. Để thuận lợi và tiết kiệm thời gian quý khách có thể đăng nhập bằng tài
                        khoản Facebook cá nhân và sử dụng tài khoản đó như tài khoản Kyna.vn.
                    </p>
                    <p>Trong trường hơn, quý khách đã có tài khoản trên Kyna.vn:</p>
                    <p>Sau khi chọn “đăng ký học”, quý khách có thể đăng nhập ngay hoặc giữ mặc định
                        đăng nhập khi vào trang Kyna.vn
                    </p>
                    <p><strong>Bước 5: Chọn cách thanh toán và tiến hành thanh toán</strong></p>
                    <p>Sau khi hoàn tất giỏ hàng và đăng nhập thành công, hệ thống sẽ chuyển đến mục
                        thanh toán khoá học.
                    </p>
                    <p>Tại Kyna.vn, quý khách có 5 phương thức thanh toán khóa học để chọn lựa:</p>
                    <ol>
                        <li>- Giao khóa học và thu tiền tận nơi (COD)</li>
                        <li>- Thanh toán bằng mã thẻ cào điện thoại</li>
                        <li>- Thanh toán trực tuyến bằng thẻ ATM hoặc thẻ VISA MASTER</li>
                        <li>- Chuyển khoản ngân hàng</li>
                        <li>- Thanh toán trực tiếp tại văn phòng Kyna.vn</li>
                    </ol>
                    <p>Sau khi chọn phương thức thanh toán phù hợp, quý khách có thể nhập lại <span class="font-italic">Mã Coupon,
                            voucher (nếu có)</span>.
                    </p>
                    <p>Sau khi thanh toán thành công, hệ thống sẽ gửi cho quý khách một thông báo xác nhận
                        thanh toán thành công và quý khách có thể bắt đầu tham gia khóa học.
                    </p>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="heading2">
                    <h4 class="panel-title">
                        <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse2" aria-expanded="false" aria-controls="collapseTwo">
                            2. Sau khi đăng ký khóa học, tôi phải thanh toán khóa học bằng cách nào?
                        </a>
                    </h4>
                </div>
                <div id="collapse2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading2">                  
                    <p> Tại Kyna.vn, quý khách có 5 phương thức thanh toán khóa học để chọn lựa:</p>
                    <ul class="list-first">
                        <li>- Giao khóa học và thu tiền tận nơi (COD)</li>
                        <li>- Thanh toán bằng mã thẻ cào điện thoại</li>
                        <li>- Thanh toán trực tuyến bằng thẻ ATM hoặc thẻ VISA MASTER</li>
                        <li>- Chuyển khoản ngân hàng</li>
                        <li>- Thanh toán trực tiếp tại văn phòng Kyna.vn</li>
                    </ul>
                    <p><strong>Cách 1: Giao khóa học và thu tiền tận nơi (COD)</strong></p>
                    <p>Kyna.vn hỗ trợ giao hàng và thanh toán tận nơi trên toàn quốc theo khung sau:</p>
                    <ul class="list-first">
                        <li>a. COD chỉ được áp dụng đối với đơn hàng có giá trị từ 140.000 đồng trở lên</li>
                        <li>b. COD miễn phí đối với các đơn hàng có giá trị từ 300.000 đồng trở lên (áp dụng trên toàn quốc).</li>
                    </ul>
                    <p>
                        * Riêng đối với các đơn hàng nội thành TP Hồ Chí Minh và nội thành TP Hà Nội
                        thì COD sẽ áp dụng miễn phí đối với các đơn hàng có giá trị từ 140.000 đồng trở
                        lên. Chính sách miễn phí chỉ áp dụng cho lần giao hàng đầu tiên cho một đơn
                        hàng, nếu việc giao hàng thất bại thì các lần giao hàng sau (đối với đơn hàng đó)
                        Kyna.vn sẽ tính phí theo biểu phí thường lệ.
                    </p>
                    <p>
                        c. Đối với những đơn hàng dưới 300.000 VNĐ, Kyna.vn sẽ áp dụng biểu giá của
                        dịch vụ giao nhận như sau:
                    </p>
                    <ul class="list-first">
                        <li>- Phí giao nhận tại các quận ngoại thành Tp. Hồ Chí Minh, ngoại thành Hà Nội: 10.000VNĐ</li>
                        <li>- Phí giao nhận tại khu vực nội thành các tỉnh khác: 20.000 VNĐ</li>
                        <li>- Phí giao nhận tại khu vực ngoại thành tỉnh khác: 30.000 VNĐ</li>
                    </ul>
                    <p> Đối với phương thức COD, qúy khách sẽ nhận hàng và thanh toán trực tiếp với nhân
                        viên giao hàng.
                    </p>
                    <p><strong>Cách 2: Thanh toán bằng thẻ cào điện thoại:</strong></p>
                    <p><strong>Kyna.vn chỉ áp dụng thanh toán với thẻ cào Mobifone và Vinaphone</strong></p>
                    <p>Để tiến hành thanh toán bằng thẻ cào điện thoại, quý khách vui lòng thực hiện các
                        bước sau:
                    </p>
                    <p>Bước 1: Mua thẻ cào Mobifone hoặc Vinaphone với giá trị tương ứng. Vào trang
                        Phương thức thanh toán, bạn vui lòng chọn Thanh toán bằng thẻ cào điện thoại
                    </p>
                    <p>Bước 2: Cào lớp tráng bạc để biết mã thẻ. Chọn loại thẻ, nhập mã thẻ và số sê-ri vào ô
                        trống, cuối cùng chọn nút Nạp tiền.
                    </p>
                    <p><strong>Lưu ý viết liền không khoảng trắng hoặc dấu –</strong></p>
                    <p>Nếu quý khách có nhiều thẻ cào, vui lòng lặp lại các thao tác trên.
                    <p>
                    <p><strong>Cách 3: Thanh toán trực tuyến bằng thẻ ATM hoặc thẻ VISA MASTER</strong></p>
                    <p><strong>Thanh toán qua thẻ ATM có đăng ký thanh toán trực tuyến:</strong></p>
                    <p>Để sử dụng phương thức thanh toán này, tài khoản ngân hàng của quý khách cần đăng
                        ký dịch vụ Internet Banking với ngân hàng. Kyna.vn hiện hỗ trợ thanh toán cho phần
                        lớn các ngân hàng tại Việt Nam:
                    </p>
                    <p><strong>Thanh toán qua thẻ Visa/Master:</strong></p>
                    <p>Phí thanh toán tùy thuộc vào từng loại thẻ quý khách dùng và ngân hàng phát hành
                        thẻ. Vui lòng liên hệ với ngân hàng phát hành thẻ để biết rõ phí thanh toán phát sinh.
                    </p>
                    <p><strong>Bước 1:</strong> Nhập số tiền cần nạp vào ví, nếu ví chưa có tiền, quý khách vui lòng điền giá
                        trị bằng với giá khóa học thành tiền hiện bạn đang cần trả.
                    </p>
                    <p>Nếu ví đã có tiền nhưng chưa đủ để thanh toán, quý khách chỉ cần nạp phần chênh
                        lệch. Sau đó chọn Thanh toán online bằng thẻ VISA, MASTERCARD, JCB và nhấp
                        vào nút Thanh toán.
                    </p>
                    <p><strong>Bước 2:</strong> Hệ thống sẽ dẫn về trang thanh toán của Đối tác Onepay Payment Gateway.
                        Tại đây quý khách hãy chọn và điền đầy đủ thông tin theo yêu cầu để thực hiện thanh
                        toán. Nếu thẻ hợp lệ, quý khách sẽ thanh toán thành công.
                    </p>
                    <p><strong> Cách 4: Chuyển khoản ngân hàng</strong></p>
                    <p><strong>Chuyển khoản qua ngân hàng</strong></p>
                    <p>Bạn có thể đến bất kỳ ngân hàng nào ở Việt Nam (hoặc sử dụng Internet Banking) để
                        chuyển tiền theo thông tin bên dưới:
                    </p>
                    <ul class="list-last">
                        <li>Số tài khoản: 0531 0025 11245</li>
                        <li>Chủ tài khoản: Công ty cổ phần Dream Viet Education</li>
                        <li>Ngân hàng: Ngân hàng Vietcombank, chi nhánh Đông Sài Gòn, TP.HCM</li>
                    </ul>
                    <p>Ghi chú khi chuyển khoản:</p>
                    <ul class="list-last">
                        <li>Tại mục “Ghi chú” khi chuyển khoản, bạn ghi rõ: Số điện thoại – Họ và tên – Email đăng ký học – Khóa học đăng ký</li>
                        <li>Ví dụ: 0909090909 – Nguyen Thi Huong Lan – nguyenthihuonglan@gmail.com Kỹ năng quản lý cảm xúc</li>
                    </ul>
                    <p><strong>Chuyển khoản qua PayPal</strong></p>
                    <ul class="list-last">
                        <li>Địa chỉ email ketoan@kyna.vn. Tỉ giá áp dụng 1 USD = 21.000 VND (tỉ giá trên PayPal)</li>
                        <li>Tại mục “Message” khi chuyển tiền, bạn ghi rõ: Số điện thoại – Họ và tên – Email đăng ký học – Khóa học đăng ký</li>
                    </ul>
                    <p><strong>Cách 5: Thanh toán tại văn phòng KYNA.VN</strong></p>
                    <ul class="list-last">
                        <li>Địa chỉ văn phòng: Tầng 6, Tòa nhà Thịnh Phát, 178/8 Đường D1, Phường 25,
                            Quận Bình Thạnh, Thành phố Hồ Chí Minh, Việt Nam
                        </li>
                        <li>Thời gian làm việc từ 08:30 – 17:30, từ thứ Hai tới thứ Bảy hàng tuần.</li>
                    </ul>
                    <p>Trong trường hợp bạn đã có đủ tiền trong ví tại Kyna.vn:</p>
                    <p>Khi này, bạn sẽ được chuyển đến trang xác nhận thanh toán, bạn chỉ cần click
                        nút “Xác nhận thanh toán”. Khi này sẽ có thông báo thanh toán thành công và bạn có
                        thể bắt đầu vào học ngay!
                    </p>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="heading3">
                    <h4 class="panel-title">
                        <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse3" aria-expanded="false" aria-controls="collapse3">
                            3. Một khóa học mà tôi đăng ký tại Kyna.vn có thời hạn trong bao lâu?
                        </a>
                    </h4>
                </div>
                <div id="collapse3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading3">
                    <p>Đối với mọi khóa học trên Kyna.vn, bạn chỉ cần thanh toán một lần và được quyền sở
                        hữu bài học mãi mãi. Điều này đồng nghĩa với việc bạn có thể xem lại bài học bất cứ
                        lúc nào bạn muốn chỉ với một lần thanh toán.
                    </p>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="heading4">
                    <h4 class="panel-title">
                        <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse4" aria-expanded="false" aria-controls="collapse4">
                            4. Tôi có được hoàn học phí nếu thấy không hài lòng không?
                        </a>
                    </h4>
                </div>
                <div id="collapse4" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading4">
                    <p>Nếu có bất cứ sự không hài lòng nào về hiệu quả khóa học, học viên đều có thể được hoàn học phí. Điều kiện để hoàn học phí như sau:</p>
                    <ul class="list-first">
                        <li>- Hoàn học phí khi học viên thấy không hài lòng về khóa học</li>
                        <li>- Thời hạn cho việc hoàn học phí là 30 ngày kể từ ngày thanh toán khóa học</li>
                        <li>- Chỉ áp dụng với học viên học theo phương pháp chuẩn (tiến độ mặc định mỗi ngày một vài bài học như hệ thống học đưa ra).</li>
                    </ul>
                    <p> Để tiến hành phản hồi và nhận lại học phí, quý khách vui lòng gửi email đến hotro@kyna.vn. Trong các trường hợp trên, quý khách chỉ cần gửi email phản hồi sẽ được hoàn học phí 100% mà không cần phải giải thích lý do gì cả.</p>
                    <p>Lưu ý:</p>
                    <ul class="list-last">
                        <li>Số tiền quý khách nhận được sẽ trừ đi phí thanh toán (nếu có) và phí chuyển khoản (nếu có).</li>
                        <li>Qúy khách sẽ nhận lại học phí trong vòng 7 ngày kể từ lúc gửi yêu cầu hoàn học phí.</li>
                        <li>Không áp dụng hoàn học phí với học viên học theo phương pháp cấp tốc (tất cả các bài học đều hiện ra hết một lần).</li>
                    </ul>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="heading5">
                    <h4 class="panel-title">
                        <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse5" aria-expanded="false" aria-controls="collapse5">
                            5. Học Online có đáng tin không?
                        </a>
                    </h4>
                </div>
                <div id="collapse5" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading5">
                    <p>
                        Sự kỳ vọng của cả thế giới vào xu hướng e-learning là có lý do. Ở các nước có tốc độ
                        phát triển nhanh trên thế giới, e-learning - học Online được xem là một giải pháp hiệu
                        quả nhất trong học tập. Bạn có thể học bất cứ lúc nào bạn muốn và bạn có thể làm chủ
                        không gian, thời gian học của mình. <br/>
                        Nội dung các chương trình học tại Kyna.vn đảm bảo chất lượng, bắt kịp với xu hướng
                        hiện đại. Hơn hết, khi học Online tại Kyna.vn bạn sẽ được học cùng giảng viên,
                        chuyên gia hàng đầu trong mọi lĩnh vực tại Việt Nam. Bạn có thể vẫn đảm bảo công
                        việc, sở thích, trách nhiệm gia đình nhưng vẫn có thể cùng Kyna.vn phấn đấu vì sự
                        nghiệp và mục tiêu của mình.
                    </p>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="heading6">
                    <h4 class="panel-title">
                        <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse6" aria-expanded="false" aria-controls="collapse6">
                            6.Ai sẽ giải đáp những thắc mắc trong khi học cho tôi?
                        </a>
                    </h4>
                </div>
                <div id="collapse6" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading6">
                    <p>
                        Khi tham gia học tại Kyna.vn, quý khách sẽ được tham gia vào một môi trường học có
                        tính tương tác cao.
                    </p>
                    <p>Tại Kyna.vn, bạn sẽ nhận được sự trợ giúp, giải đáp thắc mắc từ:</p>
                    <ul class="list-first">
                        <li>- Bạn học</li>
                        <li>- Giảng viên</li>
                        <li>- Chuyên gia</li>
                        <li>- Tư vấn viên của Kyna.vn</li>
                    </ul>
                    <p>
                        Trong một số khóa học, bạn cũng sẽ được cung cấp email của giảng viên để được trao
                        đổi trực tiếp với giảng viên.
                    </p>
                    <p>
                        Một số giảng viên cũng có thể tổ chức các buổi giao lưu trực tuyến hoặc giao lưu trực
                        tiếp (miễn phí hoặc có tính phí tùy vào từng giảng viên) cùng học viên (khoảng vài
                        tuần một lần).
                    </p>
                    <p>
                        Nếu bạn có thắc mắc về các vấn đề kỹ thuật, bạn có thể liên hệ với Kyna.vn qua email
                        hotro@kyna.vn hoặc số điện thoại 1900 6364 09 (1000đ/phút, 8h30-22h00 kể cả T7,
                        CN) để được hỗ trợ.
                    </p>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="heading7">
                    <h4 class="panel-title">
                        <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse7" aria-expanded="false" aria-controls="collapse7">
                            7. Sau khi hoàn thành khóa học, tôi có được cấp chứng nhận gì không?
                        </a>
                    </h4>
                </div>
                <div id="collapse7" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading7">
                    <p>
                        Tất cả các khóa học trên kyna.vn đều cấp chứng nhận hoàn thành khóa học đối với
                        những học viên hoàn thành các bài tập trong khóa học và hoàn thành bài tiểu luận cuối
                        khóa học.
                    </p>
                    <p>
                        Học viên có thể lựa chọn việc nhận chứng nhận online (hoàn toàn miễn phí, học viên
                        sẽ nhận được file ảnh chứng nhận gửi qua email học viên) hoặc nhận offline (nhận bản
                        in, có chữ ký và đóng dấu công ty, gửi về tận nhà cho học viên. Chi phí in ấn, lồng
                        khung gỗ, cấp chứng nhận và gửi chuyển phát nhanh về tận nhà là 200,000đ).
                    </p>

                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="heading8">
                    <h4 class="panel-title">
                        <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse8" aria-expanded="false" aria-controls="collapse8">
                            8. Kyna.vn có đáng tin không?
                        </a>
                    </h4>
                </div>
                <div id="collapse8" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading8">
                    <p> Kyna.vn là một sản phẩm thuộc hệ sinh thái giáo dục trực tuyến (e-learning) KYNA.
                        Từ khi thành lập vào năm 2013 đến nay, Kyna.vn đã thu hút hơn 250.000 học viên,
                        xây dựng trên 6000 video bài giảng với sự cộng tác của 150+ chuyên gia đầu ngành.
                        Kyna.vn đã trở thành địa chỉ học tập đáng tin cậy của hàng trăm nghìn học viên trên
                        toàn Việt Nam.
                    </p>
                    <p>
                        Kyna.vn rất mong muốn trở thành nguồn sức mạnh tích cực trên con đường phấn đấu
                        vì tương lai của hàng triệu người Việt.
                    </p>
                    <ul class="list-last">
                        <li>Để xác nhận uy tín của công ty, quý khách vui lòng xem giới thiệu về Kyna.vn,
                            các thành tích đạt được cũng như Kyna.vn trên các phương tiện truyền
                            thông: http://kyna.vn/gioi-thieu
                        </li>
                        <li>Để trải nghiệm chất lượng, phương pháp học tập tại Kyna.vn, bạn có thể tham
                            gia một số khóa học miễn phí của Kyna.vn tại https://kyna.vn/danh-sach- khoa-
                            hoc/mien-phi
                        </li>
                    </ul>
                    <p>
                        Mọi thắc mắc trong quá trình học, bạn liên hệ với email hotro@kyna.vn hoặc số điện
                        thoại 1900 6364 09 (1000đ/phút, 8h30-22h00 kể cả T7, CN) để được hỗ trợ ngay.
                    </p>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="heading9">
                    <h4 class="panel-title">
                        <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse9" aria-expanded="false" aria-controls="collapse9">
                            9. Hướng dẫn học Online tại Kyna.vn
                        </a>
                    </h4>
                </div>
                <div id="collapse9" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading9">
                    <iframe width="560" height="315" src="https://www.youtube.com/embed/qifLMpgUEZc" frameborder="0" allowfullscreen></iframe>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="heading10">
                    <h4 class="panel-title">
                        <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse10" aria-expanded="false" aria-controls="collapse10">
                            10. Mua và kích hoạt thẻ học bằng Cyberpay bằng cách nào?
                        </a>
                    </h4>
                </div>
                <div id="collapse10" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading10">
                    <p><strong>1. Địa điểm mua thẻ học online Kyna.vn</strong></p>
                    <p>
                        Qúy khách có thể tìm mua thẻ học online Kyna tại hơn 15.000 điểm giao dịch
                        Cyberpay trên toàn quốc.
                    </p>
                    <p>
                        Thông tin chi tiết các địa điểm giao dịch của Cyberpay tại đây:
                        http://info.cyberpay.vn/dai-ly/diem-giao-dich-cyberpay
                    </p>
                    <p>
                        Qúy khách chọn loại thẻ (Thẻ học 100.000 đồng hoặc 150.000 đồng) và số lượng thẻ
                        cần mua. Sau khi thanh toán tại các địa điểm giao dịch của Cyberpay, quý khách sẽ
                        nhận được một thẻ mềm và tin nhắn xác nhận. Vui lòng kiểm tra lại loại thẻ đúng với
                        nhu cầu của bạn và đảm bảo thẻ nạp, mã nạp còn nguyên vẹn.  
                    </p>
                    <p><strong>2. Danh sách các khóa học thẻ học online Kyna.vn</strong></p>
                    <p>Với các thẻ học 100.000 đồng và 150.000 đồng mua tại Cyberpay, quý khách có thể
                        lựa chọn thỏa thích một trong các khóa học sau tùy theo mệnh giá thẻ; chi phí rẻ hơn
                        rất nhiều so với học phí gốc. Xem thêm về thẻ học online Kyna.vn phát hành
                        tại Cyberpay tại đây: https://kyna.vn/bai-viet/kyna-hop-tac-cung-cyberpay-phat-hanh-the-hoc-online-Kyna 
                    </p>
                    <p><strong>3. Các bước kích hoạt thẻ học online Kyna.vn</strong></p>
                    <p><strong>Bước 1: Truy cập website donggia.kyna.vn </strong></p>
                    <p>  a. Nếu quý khách chưa đăng ký tài khoản trên Kyna.vn. Hệ thống sẽ chuyển sang
                        giao diện tạo tài khoản, thực hiện các bước sau:
                    </p>
                    <ul>
                        <li>+ Đăng ký tài khoản:  Chọn “Đăng ký” hoặc chọn “Đăng nhập bằng Facebook”
                            (nếu bạn có tài khoản Facebook).
                        </li>
                        <li>+ Nếu quý khách chọn nút “Đăng ký”, điền đầy đủ thông tin như hướng dẫn sau
                            đó hệ thống sẽ chuyển về trang Kích hoạt mã thẻ. 
                        </li>
                    </ul>
                    <p>
                        b. Nếu quý khách đã có tài khoản trên Kyna.vn hoặc sau khi đã hoàn thành đăng ký
                        tài khoản, quý khách sẽ được chuyển về trang Kích hoạt mã thẻ, nhập MÃ NẠP in
                        trên phiếu vào khung, sau đó nhấn vào nút “Kích hoạt”. 
                    </p>
                    <p><strong>
                            Bước 2. Sau khi nhập mã nạp, chọn khóa học mà quý khách muốn học trong
                            danh sách                    
                        </strong>
                    </p>
                    <p>
                        <strong>a. Đối với loại thẻ 100k:</strong> Xuất hiện giao diện khóa học tương ứng, chọn các
                        khóa bạn muốn học và chọn “Xác nhận”.
                    </p>
                    <p>
                        <strong> b. Đối với loại thẻ 150k:</strong> Xuất hiện giao diện khóa học tương ứng, chọn các
                        khóa bạn muốn học và chọn nút “Xác nhận”.
                    </p>
                    <p><strong>
                            Bước 3. Tham gia khóa học
                        </strong>
                    </p>
                    <p>
                        Sau khi chọn “Xác nhận”, giao diện sẽ chuyển sang mục &quot;Khóa học của tôi&quot;, chọn
                        khóa học và bắt đầu tham gia khóa học.
                    </p>
                </div>
            </div>


        </div>
    </div><!--end .mobile-->
</main>
<?php include 'inc/footer.php'; ?>