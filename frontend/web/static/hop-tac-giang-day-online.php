
<?php include 'inc/header.php'; ?>
<main>
    <div class="container teaching-cooperation k-height-header">   
        <article class="tl_wrap">
            <h2 class="intro-title center text-uppercase">
                Hợp tác giảng dạy cùng Dream Viet Education
            </h2>
            <div class="tl_tag_bottom_title">
                <span class="texture_tag_title"></span>
                <span class="tl_tag_name">Giới thiệu Kyna.vn và Dream Viet Education</span>
            </div>
            <div>
                <p><span style="color: #50ad4e; font-weight: bold;">Kyna.vn</span> là e-learning đào tạo các kỹ năng mềm và kỹ năng chuyên môn, được quản lý bởi Công ty CP <strong>Dream Viet Education</strong>, một trong những đơn vị đi đầu trong lĩnh vực đào tạo trực tuyến các kỹ năng ở Việt nam.</p>
                <p>Với sứ mệnh là cầu nối giữa giảng viên và học viên – chia đều cơ hội học tập, giảng dạy cho tất cả mọi người, <span style="color: #50ad4e; font-weight: bold;">Kyna.vn</span> hướng đến một nền tảng học tập hữu ích, toàn diện cho giới trẻ Việt.</p>
                <div>
                    <img src="img/hop_tac_giang_day_gioi_thieu.jpg" alt="hop tac giang day tai Kyna.vn" width="100%"/>
                </div>
            </div>
            <div class="tl_tag_bottom_title">
                <span class="texture_tag_title"></span>
                <span class="tl_tag_name">Những ai có thể giảng dạy trên <span style="color: #50ad4e; font-weight: bold;">Kyna.vn</span>?</span>
            </div>
            <div>
                <p class="justify">Đó là các giảng viên / chuyên gia / chuyên viên / nhân sự cấp cao / cấp quản lý / doanh nhân có khả năng giảng dạy tốt ở một trong các chủ đề sau:</p>
                <ul class="ul_square_color">
                    <li><strong>Sales & Marketing:</strong> Content Marketing, Google Adwords, Facebook Marketing, Social Media, Email Marketing, Bán hàng shop online, Copywriter, PR,…</li>
                    <li><strong>Tiếng Anh:</strong> Tiếng Anh cho người bận rộn, Phát âm chuẩn tiếng Anh, luyện thi TOEIC, IELTS,…</li>
                    <li><strong>Nuôi dạy con cái:</strong> dạy con ngoan, cho con ăn, làm bạn cùng con, giữ lửa hạnh phúc gia đình…</li>
                    <li><strong>Quản lý & Lãnh đạo:</strong> Tài chính cho lãnh đạo, nhân sự cho lãnh đạo, kỹ năng ủy thác, chủ trì cuộc họp, phong cách lãnh đạo …</li>
                    <li><strong>Kỹ năng mềm:</strong> giao tiếp, tư duy phản biện, ra quyết định,  …</li>
                    <li><strong>Công nghệ:</strong> tin học văn phòng, lập trình, photoshop, làm phim, graphic design …</li>
                    <li><strong>Tài chính:</strong> huy động vốn, quản lý chi phí, lập kế hoạch tài chính, làm giàu, đầu tư ….</li>
                    <li><strong>Lĩnh vực khác:</strong>  âm nhạc, rèn luyện sức khỏe, nghệ thuật, nấu ăn, …</li>
                </ul>
                <p style="margin-top: 20px">Ưu tiên những giảng viên có thêm 1 trong 2 đặc điểm sau:</p>
                <ul class="ul_line_height">
                    <li>Nổi tiếng / có uy tín trong ngành</li>
                    <li>Có kinh nghiệm thực tiễn và kinh nghiệm giảng dạy ở lĩnh vực mà mình muốn giảng dạy.</li>
                </ul>
                <p class="justify">Nếu bạn là chuyên gia hoặc có hiểu biết sâu sắc về một kỹ năng nào đó trong các lĩnh vực trên, hãy liên hệ ngay với chúng tôi để trở thành giảng viên trên <span style="color: #50ad4e; font-weight: bold;">Kyna.vn</span>:<br>
                    <a class="btn_lite" href="https://docs.google.com/forms/d/17hm273clfe92sug5-qfqLgFm2porUuXoRaslGiS5LF0/viewform" target="_blank"><strong>Đăng ký</strong></a>
                </p>
            </div>
            <div class="tl_tag_bottom_title">
                <span class="texture_tag_title"></span>
                <span class="tl_tag_name">Tại sao bạn nên hợp tác giảng dạy trên <span style="color: #50ad4e; font-weight: bold;">Kyna.vn</span>?</span>
            </div>
            <div>
                <p>Rất nhiều người mong muốn nhận được những chia sẻ giá trị từ lĩnh vực mà bạn đam mê, hiểu biết. Hãy cùng <span style="color: #50ad4e; font-weight: bold;">Kyna.vn</span> đem đến cơ hội học tập, phát triển bản thân cho nhiều người hơn nữa và nhận những lợi ích xứng đáng dành cho bạn. Điều này đồng nghĩa với uy tín và tầm ảnh hưởng của bạn sẽ ngày càng gia tăng.</p>
                <img src="img/hop_tac_giang_phat_trien.jpg" alt="phat trien giang hoc truc tuyen " width="100%"/>
            </div>

            <div class="tl_tag_bottom_title">
                <span class="texture_tag_title"></span>
                <span class="tl_tag_name">Quyền lợi của giảng viên</span>
            </div>
            <div>
                <ul class="ul_square_color">
                    <li>Toàn bộ khâu sản xuất, biên tập, hậu kỳ của khóa học sẽ được <span style="color: #50ad4e; font-weight: bold;">Kyna.vn</span> hỗ trợ hoàn toàn MIỄN PHÍ. Giảng viên chỉ cần biên soạn nội dung khóa học khóa học chất lượng, hữu ích và đến Kyna.vn studio để quay lại khóa học.</li>
                    <li><span style="color: #50ad4e; font-weight: bold;">Kyna.vn</span> chịu trách nhiệm về phần xuất bản và quảng bá khóa học trên <span style="color: #50ad4e; font-weight: bold;">Kyna.vn</span>.</li>
                    <li>Với mỗi khóa học online hợp tác với <span style="color: #50ad4e; font-weight: bold;">Kyna.vn</span>, giảng viên được chia sẻ lợi nhuận trên học phí nhận được (sau khi trừ đi phí thanh toán).</li>
                </ul>
            </div>

            <div class="tl_tag_bottom_title">
                <span class="texture_tag_title"></span>
                <span class="tl_tag_name">Liên hệ ngay để hợp tác cùng chúng tôi</span>
            </div>
            <div>
                <p>Hãy đăng ký để giảng dạy tại <span style="color: #50ad4e; font-weight: bold;">Kyna.vn</span> qua form đăng ký bên dưới:</p>
                <p><iframe src="https://docs.google.com/forms/d/17hm273clfe92sug5-qfqLgFm2porUuXoRaslGiS5LF0/viewform?embedded=true" width="100%" height="1695" frameborder="0" marginwidth="0" marginheight="0"></iframe></p>
            </div>

        </article>
    </div><!--end .container-->
</main>
<?php include 'inc/footer.php'; ?>