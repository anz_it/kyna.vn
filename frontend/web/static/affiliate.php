<?php include 'inc/header.php'; ?>

<main>
    <section>
        <div class="k-affiliate container">
            <div class="af-box clearfix">
                <div class="af-title col-xs-12">Đối tác sản xuất nội dung</div>
                <div class="af-box-left col-md-5 col-xs-12">
                    <div>
                        <img class="af-contact" src="img/af_contact.png" alt="deltaviet landing page">
                        <div class="af-img-text">
                            Đăng ký làm đối tác sản xuất nội dung bằng cách điền vào form dưới đây:
                        </div>
                        <div class="af-btn">
                            <a href="/hop-tac-giang-day-online"><div class="btn-lite">Form đăng ký</div></a>
                        </div>
                    </div>
                </div>
                <div class="af-box-right col-md-7 col-xs-12">
                    <ul>
                        <li><span class="af-group-name">Đối tượng phù hợp:</span></li>
                        <li><span class="af-icon-point"></span>
                            <span class="af-info-text">Các giảng viên / chuyên gia / doanh nhân có trên 3 năm kinh nghiệm trong lĩnh vực muốn giảng dạy</span>
                        </li>
                        <li><span class="af-icon-point"></span>
                            <span class="af-info-text">Kỹ năng trình bày văn nói tốt, ngôn ngữ cơ thể tốt</span>
                        </li>
                        <li><span class="af-icon-point"></span>
                            <span class="af-info-text">Nội dung khóa học mang tính ứng dụng, thực tiễn cao</span>
                        </li>
                        <li><span class="af-benefit-name af-group-name">Lợi ích hợp tác:</span></li>
                        <li><span class="af-icon-point"></span>
                            <span class="af-info-text">Kyna.vn hỗ trợ quay video bài giảng và biên tập bài giảng (có thể quay miễn phí tại Studio của Kyna.vn hoặc quay trực tiếp tại lớp học / trung tâm đào tạo của đối tác)</span>
                        </li>
                        <!--<li><span class="af-icon-point"></span>
                            <span class="af-info-text">Hưởng lợi nhuận chia sẻ từ 50 - 70%</span>
                        </li>-->
                        <li><span class="af-icon-point"></span>
                            <span class="af-info-text">Tiếp cận &gt; 100.000 học viên và cộng đồng Delta với &gt; 250.000 fan</span>
                        </li>
                        <li><span class="af-icon-point"></span>
                            <span class="af-info-text">Gia tăng uy tín và ảnh hưởng của đối tác trên toàn quốc</span>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="af-box clearfix">
                <div class="af-title col-xs-12">ĐỐI TÁC PHÂN PHỐI NỘI DUNG (AFFILIATE)</div>
                <div class="af-box-left col-md-5 col-xs-12">
                    <div>
                        <img class="af-contact" src="img/af_distribution.png" alt="deltaviet landing page">
                        <div class="af-img-text">
                            Đăng ký làm đối tác phân phối nội dung (affiliate) bằng cách điền vào form dưới đây:
                        </div>
                        <div class="af-btn">
                            <a href="/hop-tac-giang-day-online"><div class="btn-lite">Form đăng ký</div></a>
                        </div>
                    </div>
                </div>
                <div class="af-box-right col-md-7 col-xs-12">
                    <ul>
                        <li><span class="af-group-name">Đối tượng phù hợp:</span></li>
                        <li><span class="af-icon-point"></span>
                            <span class="af-info-text">Sở hữu website có lượt truy cập > 10.000 Visit/ngày (Unique) hoặc Mạng cộng đồng > 50.000 Fan (thực)</span>
                        </li>
                        <li><span class="af-icon-point"></span>
                            <span class="af-info-text">Website/Fanpage có nội dung phù hợp với học viên của Kyna.vn</span>
                        </li>
                        <li><span class="af-icon-point"></span>
                            <span class="af-info-text">Các đơn vị có lượng thành viên/cộng đồng/sinh viên/ nhân viên số lượng lớn</span>
                        </li>
                        <li><span class="af-benefit-name af-group-name">Lợi ích hợp tác:</span></li>
                        <li><span class="af-icon-point"></span>
                            <span class="af-info-text">Hưởng Hoa hồng 20% - 50% từ việc giới thiệu (Affiliate)</span>
                        </li>
                        <!--<li><span class="af-icon-point"></span>
                            <span class="af-info-text">Hưởng lợi nhuận chia sẻ từ 50 - 70%</span>
                        </li>-->
                        <li><span class="af-icon-point"></span>
                            <span class="af-info-text">Cookie trong 60 ngày</span>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="af-box clearfix">
                <div class="af-title col-xs-12">MUA NỘI DUNG</div>
                <div class="af-box-left col-md-5 col-xs-12">
                    <div>
                        <img class="af-contact" src="img/af_sale.png" alt="deltaviet landing page">
                        <!--<div class="af-img-text">
                            Đăng ký làm đối tác sản xuất nội dung bằng cách điền vào form dưới đây:
                        </div>
                        <div class="af-btn">
                            <a href="/hop-tac-giang-day-online"><div class="btn-lite">Form đăng ký</div></a>
                        </div>-->
                    </div>
                </div>
                <div class="af-box-right col-md-7 col-xs-12">
                    <ul>
                        <li><span class="af-group-name">Đối tượng phù hợp:</span></li>
                        <li><span class="af-icon-point"></span>
                            <span class="af-info-text">Trường học cần mua khóa học trang bị cho học sinh/sinh viên với số lượng từ 50 học viên</span>
                        </li>
                        <li><span class="af-icon-point"></span>
                            <span class="af-info-text">Doanh nghiệp cần mua khóa học để đào tạo cho với số lượng từ 50 nhân sự</span>
                        </li>
                        <li><span class="af-benefit-name af-group-name">Lợi ích hợp tác:</span></li>
                        <li><span class="af-icon-point"></span>
                            <span class="af-info-text">Chiết khấu ưu đãi từ 50-80% dưa trên số lượng học viên</span>
                        </li>
                        <li><span class="af-icon-point"></span>
                            <span class="af-info-text">Có thể học trực tiếp trên nền tảng học trực tuyến của Kyna.vn, hoặc đối tác có thể mua bản quyền nội dung để sử dụng trong hệ thống đào tạo nội bộ</span>
                        </li>
                    </ul>
                </div>
                <div class="af-contact-info col-xs-12"><span>Chi tiết liên hệ qua email: <a>hieu@kyna.vn</a></span></div>
            </div>
        </div>
    </section>
</main>

<?php include 'inc/footer.php'; ?>
