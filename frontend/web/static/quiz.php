
<?php include 'inc/header.php'; ?>
<main>
    <div class="container k-height-header k-faq-wrap">
    <div class="wrap-quiz-lesson">
        <div class="box-quiz-test " style="display: block">
            <section class="col-sm-9">
                <div class="k-listing-characteristics">
                    <h3>1. Sau khi chương trình Java được biên dịch, một loại mã đặc biệt được tạo ra gọi là:</h3>
                    <ul class="k-listing-characteristics-list">
                        <div class="form-group field-quizsession-answers-43022">
                            <input type="hidden" name="QuizSession[answers][43022]" value=""><div id="quizsession-answers-43022">
                                <li class="radio"><input type="radio" id="radio-26860" name="QuizSession[answers][43022]" value="2686" checked disabled><label class="right" for="radio-26860"><span><span></span></span>Mã Java bytecode</label></li>
                                <li class="radio"><input type="radio" id="radio-26871" name="QuizSession[answers][43022]" value="2687" disabled><label for="radio-26871"><span><span></span></span>Mã máy</label></li>
                                <li class="radio"><input type="radio" id="radio-26882" name="QuizSession[answers][43022]" value="2688" disabled><label for="radio-26882"><span><span></span></span>Mã nhị phân</label></li>
                            
                                <li class="radio"><input type="radio" id="radio-26893" name="QuizSession[answers][43022]" value="2689" disabled><label class="wrong" for="radio-26893" checked disabled><span><span></span></span>Mã thực thi</label></li>
                            </div>
                        </div>
                    </ul>
                </div>
            </section>
        </div>
    </div>
</div><!--end .container-->
</main>

<?php include 'inc/footer.php'; ?>