<!DOCTYPE HTML>
<html lang="vi">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" href="/favo_ico.png">
        <title>Giới thiệu - Kyna.vn</title>
        <!-- Bootstrap core CSS -->

        <link href="../css/main.min.css?version=1521198358" type="text/css" rel="stylesheet"/>
        <link href="../css/owl.carousel.css" rel="stylesheet"/>
        <link href="../css/owl.theme.css" rel="stylesheet"/>
        <link href="../css/owl.transitions.css" rel="stylesheet"/>
        <link href='https://fonts.googleapis.com/css?family=Roboto:400,500,700,300' rel='stylesheet' type='text/css'>

    </head>
    <body id="page-about">
        <div class="page-about-header">
            <header>
                <div class="container">
                    <nav class="wrap-main">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <h2 class="logo"><a href="#"><img src="../src/img/logo.svg" alt="Kyna.vn" class="img-responsive" /></a></h2>
                        </div>
                        <div id="navbar" class="navbar-collapse collapse menu">
                            <ul class="nav navbar-nav navbar-right">
                                <li><a href="#">Giới thiệu</a></li>
                                <li><a href="#">Giảng dạy</a></li>
                                <li><a href="#">Đội ngũ</a></li>
                                <li><a href="#">Tuyển dụng</a></li>
                            </ul>
                        </div><!--/.nav-collapse -->
                    </nav>
                </div><!--end .container-->
            </header>

            <section id="banner">
                <div class="container">
                    <p class="col-sm-8 col-xs-12">Sứ mệnh của Kyna.vn là<br/>Nâng cao giá trị tri thức<br/>phục vụ hàng triệu người Việt Nam</p>
                    <span><i class="fa fa-chevron-down" aria-hidden="true"></i></span>
                </div><!--end .container-->
            </section>
        </div><!--end wrap-header-->

        <section id="section-1">
            <div class="container">
                <h2>Với Kyna, bạn có thể</h2>
                <p class="text">Tham gia bất kỳ khóa học nào liên quan đến kỹ năng mềm, kỹ năng chuyên môn công nghệ, nghệ thuật, ngoại ngữ, làm đẹp hay nuôi dạy con cái... từ các giảng viên hàng đầu với chi phí thật tiết kiệm. Với việc học Online, bạn thoải mái tham gia học tập vào bất cứ lúc nào, bất cứ nơi đâu bạn muốn.</p>
                <ul class="col-md-8 col-md-offset-2 col-xs-12">
                    <li class="col-md-3 col-sm-6 col-xs-12"><i class="fa fa-wifi" aria-hidden="true"></i><p>Dạy và học online 100%</p></li>
                    <li class="col-md-3 col-sm-6 col-xs-12"><i class="fa fa-star" aria-hidden="true"></i><p>Học từ chuyên gia, doanh nhân nhiều kinh nghiệm</p></li>
                    <li class="col-md-3 col-sm-6 col-xs-12"><i class="fa fa-globe" aria-hidden="true"></i><p>Học mọi lúc, mọi nơi</p></li>
                    <li class="col-md-3 col-sm-6 col-xs-12"><i class="fa fa-users" aria-hidden="true"></i><p>Đào tạo nhân sự nội bộ</p></li>
                </ul>
            </div><!--end.container-->
        </section>

        <section id="section-2">
            <div class="container">
                <h2>Kyna.vn và những cột mốc ấn tượng</h2>
                <p>Là một trong những đơn vị đi đầu trong công nghệ đào tạo trực tuyến các kỹ năng tại Việt Nam, được quản lý bởi Công ty Cổ phần Dream Vieeet Education.</p>
                <ul class="col-lg-6 col-lg-offset-3 col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2 col-xs-12">
                    <li class="col-sm-4 col-xs-12"><span>200.000+</span><p>Học viện</p></li>
                    <li class="col-sm-4 col-xs-12"><span>200+</span><p>Khóa học</p></li>
                    <li class="col-sm-4 col-xs-12"><span>5000+</span><p>Video</p></li>
                </ul>
            </div><!--end .container-->
        </section>

        <section id="section-3">
            <div class="container">
                <h2>Giao diện học tập chuyên nghiệp tại Kyna.vn</h2>
                <img src="../src/img/about/video.png" alt="Giao diện học tập chuyên nghiệp tại Kyna.vn" class="img-responsive"/>
            </div><!--end .container-->
        </section>

        <section id="section-4">
            <div class="container">
                <h2>Điểm khác biệt của Kyna.vn</h2>
                <ul class="wrap">
                    <li>
                        <ul>
                            <li><p>Học trực tuyến <span class="text-transform">Mọi lúc mọi nơi</span></p></li>
                            <li><p>Thanh toán một lần <span class="text-transform">Học mãi mãi</span></p></li>
                            <li><p><span class="text-transform">Cấp chứng nhận</span> hoàn thành khóa học</p></li>
                        </ul>
                    </li>
                    <li>
                        <ul>
                            <li><p><span class="text-transform">Hoàn tiền</span> nếu không hài lòng</p></li>
                            <li><p>Phương thức thanh toán <span class="text-transform">Linh hoạt</span></p></li>
                            <li><p><span class="text-transform">Giao khóa học miễn phí</span> tận nhà</p></li>
                        </ul>
                    </li>
                </ul>
            </div><!--end .container-->
        </section>

        <section id="section-5">
            <div class="container">
                <h2 class="text-transform">Báo chí nói về chúng tôi</h2>
                <ul class="wrap">
                    <li>
                        <img src="../src/img/about/about-dip-cau-dau-tu.png" alt="Nhịp cầu đầu tư" class="img-responsive">
                        <p>Nếu bạn thật sự đam mê một điều gì đó hãy bắt đầu ngay hôm nay</p>
                        <span><a href="#">Xem chi tiết</a></span>
                    </li>
                    <li>
                        <img src="../src/img/about/about-dip-cau-dau-tu.png" alt="Nhịp cầu đầu tư" class="img-responsive">
                        <p>Nếu bạn thật sự đam mê một điều gì đó hãy bắt đầu ngay hôm nay</p>
                        <span><a href="#">Xem chi tiết</a></span>
                    </li>
                </ul>
            </div><!--end .container-->
        </section>

        <section id="section-6">
            <div class="container">
                <h2>Thông tin liên hệ</h2>
                <ul class="col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1 col-xs-12 wrap">
                    <li class="col-md-6 col-xs-12 box">
                        <div class="wrap">
                            <h4>Hỗ trợ</h4>
                            <p>Nếu có bất kỳ câu hỏi nào liên quan đến việc học tập và giảng dạy tại Kyna.vn, hãy để chúng tôi hỗ trợ bạn tốt nhất qua<br/>Hotline <span class="bold">1900 6364 09</span> (1000đ/phút; 8h30-22h00 kể cả T7, CN) và email <span>Hotro@kyna.vn</span>.</p>
                        </div><!--end .wrap-->
                    </li>

                    <li class="col-md-6 col-xs-12 box">
                        <div class="wrap">
                            <h4>Thông tin hợp tác</h4>
                            <p>Vui lòng liên hệ Giám đốc kinh doanh: Mr Nguyễn Tấn Hiếu<br/>Email <span>Hieu@kyna.vn</span>.</p>
                        </div><!--end .wrap-->
                    </li>

                    <li class="col-md-6 col-xs-12 box">
                        <div class="wrap">
                            <h4>Tuyển dụng</h4>
                            <p>Nếu giáo dục trực tuyến là thế mạnh và đam mê của bạn, hãy <span class="bold">tham gia ngay</span> cùng chúng tôi.</p>
                        </div><!--end .wrap-->
                    </li>

                    <li class="col-md-6 col-xs-12 box">
                        <div class="wrap">
                            <h4>Địa chỉ</h4>
                            <p>Lầu 6 Tòa nhà Thịnh Phát - 178/8 Đường D1, Phường 25, Quận Bình Thạnh, TP Hồ Chí Minh</p>
                        </div><!--end .wrap-->
                    </li>
                </ul>
            </div><!--end .container-->
        </section>
        <script type="text/javascript">
            $(document).ready(function () {
                var HeightAboutWrapHeader = $("#page-about .page-about-header").height();
                $('#page-about #banner').css({"height": +HeightAboutWrapHeader + "px"})
                $("#page-about #banner span i").click(function () {
                    $('html,body').animate({scrollTop: window.innerHeight}, 'slow');
                });
            });
        </script>
        <script>
            /* SLIDER CO THE BAN QUAN TAM */
            $("#section-5 ul.wrap").owlCarousel({
                autoPlay: true,
                items: 1,
                itemsDesktop: [1199, 1],
                itemsDesktopSmall: [979, 1],
                itemsTablet: [768, 1],
                itemsMobile: [479, 1],
                mouseDrag: true,
                navigationText: ["<i class='fa fa-angle-right rotate-180 icon'></i>",
                    "<i class='fa fa-angle-right icon'></i>"],
                navigation: true,
                pagination: false,
            });
        </script>

        <script>
            ;
            (function ($, window, document, undefined)
            {
                'use strict';

                var $list = $('#section-6 .wrap'),
                        $items = $list.find('.box'),
                        setHeights = function ()
                        {
                            $items.css('height', 'auto');

                            var perRow = Math.floor($list.width() / $items.width());
                            if (perRow == null || perRow < 2)
                                return true;

                            for (var i = 0, j = $items.length; i < j; i += perRow)
                            {
                                var maxHeight = 0,
                                        $row = $items.slice(i, i + perRow);

                                $row.each(function ()
                                {
                                    var itemHeight = parseInt($(this).outerHeight());
                                    if (itemHeight > maxHeight)
                                        maxHeight = itemHeight;
                                });
                                $row.css('height', maxHeight);
                            }
                        };

                setHeights();
                $(window).on('resize', setHeights);
            })(jQuery, window, document);
        </script>
        <script type="text/javascript" src="../src/js/owl.carousel.min.js"></script>

    </body>
</html>
