<?php include 'inc/header.php'; ?>

<main>
    <div class="container-fluid">
        <div class="row" id="teaching-info">
            <div class="baner-teaching hidden-sm-down"></div>
            <h2 class="tit-teaching"><label><span>KYNA.VN</span> HỢP TÁC VỚI CHUYÊN GIA NHƯ THẾ NÀO?</label></h2>
            <div class="img-processing hidden-sm-down"><img src="img/sd.jpg" alt=""></div>
            <div class="img-processing hidden-md-up"><img src="img/sdn.jpg" alt=""></div>
        </div>
    </div>
    <div class="container">
        <div class="row" id="teaching-text">
            <h2 class="tit-teaching">LỢI ÍCH KHI THAM GIA GIẢNG DẠY TẠI <span>KYNA.VN</span></h2>
            <p class="text17">Đã có hơn 100.000 lý do vì sao bạn tham gia giảng dạy tại Kyna.vn</p>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row" id="teaching-type">
            <div class="container">
                <div class="col-lg-3 col-md-6">
                    <div class="item-html">
                        <img  src="img/icon1.png" alt="">
                        <p>Kyna.vn giúp bạn có thêm <strong>nguồn thu nhập</strong> hàng tháng mà chỉ dành thời gian xây dựng khoá học ban đầu</p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="item-html" align="center">
                        <img  src="img/icon2.png" alt="">
                        <p>Kyna.vn đem lại cơ hội cho các <strong>Chuyên Gia quảng bá hình ảnh đến nhiều học viên trên cả nước</strong></p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="item-html" align="center">
                        <img  src="img/icon3.png" alt="">
                        <p>Kyna.vn sẽ tư vấn và <strong>hỗ trợ các Chuyên Gia truyền đạt dễ dàng</strong>, đội ngũ video xử lý hậu kỳ nhằm tạo ra bài giảng chuyên nghiệp, thu hút</p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="item-html" align="center">
                        <img  src="img/icon4.png" alt="">
                        <p>Đã có trên 100,000 học viên tham gia học trên Kyna.vn và rất nhiều chuyên gia, diễn giả đã hợp tác cùng Kyna.vn</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row-s" id="teaching-team">
            <h2 class="title-team">ĐỘI NGŨ GiẢNG VIÊN TẠI <span>KYNA.VN</span></h2>
            <div class="col-md-4 col-xl-3" align="center">
                <div class="item-teacher">
                    <img  src="img/nguyenthanhminh.jpg" alt="">
                    <div class="name">
                        <span>Nguyễn Thanh Minh</span><br>
                        <span class="text12">CEO  Dream Viet Education</span>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-xl-3" align="center">
                <div class="item-teacher">
                    <img  src="img/ailien.jpg" alt="">
                    <div class="name"><span>Ths. Trần Thị Ái Liên</span><br>
                        <span class="text12">Thạc sĩ Tâm lý học</span></div>
                </div>
            </div>
            <div class="col-md-4 col-xl-3" align="center">
                <div class="item-teacher">
                    <img  src="img/khachieu.jpg" alt="">
                    <div class="name"><span>Nguyễn Hoàng Khắc Hiếu </span><br>
                        <span class="text12">Thạc sĩ Tâm lý học</span></div>
                </div>
            </div>

            <div class="col-md-4 col-xl-3" align="center">
                <div class="item-teacher">
                    <img  src="img/duongngocdung.jpg" alt="">
                    <div class="name"><span>Dương Ngọc Dũng </span><br>
                        <span class="text12">Tiến sĩ Tôn giáo học ĐH Boston</span></div>
                </div>
            </div>

        </div>
    </div>
    <div class="container-fluid">
        <div class="row" id="teaching-recruitment">
            <div class="container">
                <h2 class="title-recruitment"><label>THAM GIA NGAY CÙNG CHÚNG TÔI!</label></h2>
                <div><span><strong>Tham gia trải nghiệm hình thức giảng dạy mới thú vị và chia sẻ kiến thức của bạn đến hàng trăm ngàn học viên tại Kyna.vn</strong></span></div>
            </div>
        </div>

        <div id="teaching-search" class="container">
            <div class="title-search">TÌM KIẾM CHUYÊN GIA</div>
            <table class="table">
                <thead>
                    <tr>
                        <th><span class="text15">LĨNH VỰC GIẢNG DẠY</span></th>
                        <th>Số lượng</th>
                        <th>Thời hạn</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
    <div class="container">
        <div id="teaching-footer">
            <div class="teaching-footer-title"><label><span>“Đối với các trường đào tạo, tổ chức phát triển nội dung đào tạo, doanh nghiệp, đối tác phân phối, vui lòng tham khảo tại đây”</span></label></div>
            <a href="affiliate.php" target="_blank"><img src="img/icon5.png" alt="Hợp tác cùng Kyna"></a>
        </div>
    </div>
    <div class="k-subscriber">
        <div class="container">
            <div class="col-lg-7 col-xs-12 k-subscriber-title">
                <ul>
                    <li>ĐĂNG KÝ NHẬN MAIL</li>
                    <li>Đăng ký để nhận những bài viết thú vị và ưu đã đặc biệt từ Kyna.vn</li>
                </ul>
            </div>
            <div class="col-lg-5 col-xs-12 k-subscriber-form">
                <form action="https://kyna.vn/giang-day" id="form_subscribe" onsubmit="return submit_subscribe();" method="post" accept-charset="utf-8">
                    <div class="col-sm-8" id="inp_subscribe">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="icon-mail-outline"></i> </span>
                            <input type="text" class="form-control" placeholder="Email" name="email" required="" id="email_footer_subscribe">
                        </div>
                    </div>
                    <div class="col-sm-4 btn-wrap">
                        <input type="submit" name="submit" value="ĐĂNG KÝ" class="btn btn-primary">
                    </div>
                </form>
            </div>
        </div><!--end .container-->
    </div><!--end k-subscriber--> 
</main>
<?php include 'inc/footer.php'; ?>
