<?php include 'inc/header.php'; ?>

<main>
    <section>
        <div class="k-policy container k-height-header">
            <h2>Chính sách bảo mật thông tin cá nhân</h2>

            <p>Có nhiều cách mà bạn sẽ sử dụng dịch vụ của chúng tôi - để tìm kiếm và chia sẻ thông tin,

                để giao tiếp với người khác hoặc tạo ra nội dung mới. Khi bạn chia sẻ thông tin với chúng

                tôi, chẳng hạn như tạo tài khoản Kyna ID, chúng tôi có thể phục vụ bạn tốt hơn như giới

                thiệu thông tin, vận chuyển hàng hóa... Khi bạn sử dụng dịch vụ của chúng tôi, chúng tôi

                muốn nêu ra rõ ràng chúng tôi sẽ sử dụng và bảo vệ thông tin của bạn như thế nào.

                Chính sách bảo mật của chúng tôi sẽ mô tả: 1. Thông tin chúng tôi sẽ thu thập và lý do thu

                thập thông tin đó. 2. Chúng tôi sẽ sử dụng thông tin này thế nào.</p>

            <p>Xin hãy đọc tài liệu này, nếu bạn có thắc mắc gì xin hãy liên lạc với chúng tôi.</p>

            <h3>Thông tin chúng tôi thu thập</h3>

            <p>Chúng tôi thu thập các thông tin mà chúng tôi cho là cần thiết do bạn cung cấp để phục vụ

                mục đích cải tiến dịch vụ dành cho người dùng.</p>
            <p>
                Chúng tôi cũng có sử dụng những cookie, tracking pixel và các công nghệ tương tự cùa các đối tác thứ ba như Google, Facebook, Adroll. Cookie là những dữ liệu nhỏ được lưu trữ trong trình duyệt của bạn nhằm tối ưu hơn trải nghiệm dành cho bạn; trong đó có việc lưu trữ thông tin của bạn cho những lần đăng nhập sau, và việc đưa những mẫu quảng cáo và ưu đãi phù hợp dành cho bạn.
            </p>

            <h3>Thông tin chúng tôi sử dụng</h3>

            <p>Từ thông tin thu thập được, chúng tôi sẽ sử dụng để cải tiến dịch vụ của chúng tôi.
                Thông tin của bạn sẽ được chúng tôi lưu trữ trong suốt thời gian bạn sử dụng dịch vụ của chúng tôi.</p>

            <h3>Bảo mật thông tin</h3>

            <p>Chúng tôi đã cố gắng để bảo mật thông tin người dùng từ những truy cập, thay đổi, phá hủy

                dữ liệu không hợp lệ. Đặc biệt chúng tôi mã hóa nhiều dịch vụ bằng SSL.</p>
            <p>Chúng tôi cam đoan sẽ không bán, chia sẻ dẫn đến làm lộ thông tin cá nhân của bạn vì
                mục đích thương mại vi phạm cam kết của chúng tôi ghi trong chính sách bảo mật này.</p>
            <p>
                Chúng tôi hiểu rằng quyền lợi của bạn trong việc bảo vệ thông tin cá nhân cũng chính là trách nhiệm của chúng tôi
                nên trong bất kỳ trường hợp có thắc mắc, góp ý nào liên quan đến chính sách bảo mật của chúng tôi,
                vui lòng liên hệ qua số điện thoại: 1900 6364 09 hoặc email: hotro@kyna.vn
            </p>

            <h3>Thay đổi</h3>

            <p>Chính sách bảo mật của chúng tôi có thể thay đổi theo thời gian, để đáp ứng những thay đổi

                của luật pháp hoặc dịch vụ của chúng tôi.</p>
        </div>
    </section>
</main>
<?php include 'inc/footer.php'; ?>
