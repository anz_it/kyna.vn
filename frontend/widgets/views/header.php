<?php
/* @var $this yii\web\View */

use common\helpers\CDNHelper;
use frontend\widgets\BannerWidget;
use kyna\settings\models\Banner;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\ActiveForm;
use \kyna\tag\models\Tag;
use frontend\widgets\FacetMobile;
use common\helpers\DateTimeHelper;

$cdnUrl = CDNHelper::getMediaLink();

$settings = Yii::$app->controller->settings;
$logoImage = $settings['lp_logo_image'];
$logoUrl = $settings['lp_logo_url'];
$hotMobileKeywords = Tag::topTags(Tag::TYPE_MOBILE, 10);
?>



<header id="header">
    <?php
    $module = Yii::$app->controller->module->id;
    $controller = Yii::$app->controller->id;
    $banner_type = Banner::TYPE_TOP;
    // Hien thi banner trang khoa hoc cua toi
    if($module == 'user' && $controller == 'course'){
      $banner_type = Banner::TYPE_TOP_MY_COURSE;
    }
    ?>
    <?=BannerWidget::widget([
	'type' => $banner_type,
	'id' => 'fixed-topbar',
])?>
    <div id="fb-root"></div>
    <nav class="navbar navbar-light k-header-wrap">
        <div class="container">
            <div class="navbar-brand logo col-lg-2 col-md-2">
                <?php if (empty($settings) || empty($settings['logo_url'])) {?>
                    <a href="<?=$logoUrl?>"><img src="<?=$logoImage?>" alt="Kyna.vn" class="img-fluid"></a>
                <?php } else {?>
                    <img src="<?= $settings['logo_url'];?>" alt="Kyna.vn" class="img-fluid"/>
                <?php }?>
                <script type="application/ld+json">
                    {
                      "@context": "http://schema.org",
                      "@type": "Organization",
                      "url": "<?= Yii::$app->params['baseUrl'] ?>",
                      "logo": "<?= !empty($logoImage) ? $logoImage : $settings['logo_url']; ?>"
                    }
                </script>
            </div>

            <ul class="nav navbar-nav k-header-menu col-xl-2 col-md-1">
                <li class="nav-item dropdown">
                    <div class="nav-wrap">
                        <a class="nav-link" href="#" data-toggle="dropdown" role="button" aria-haspopup="true"
                           aria-expanded="false"><i class="icon icon-bars"></i>
                            <span>Danh mục khóa học</span></a>
                            <span>
                                <a href="#" class="nav-mb" data-offpage="#nav-mobile">
                                    <i class="icon icon-bars"></i>
                                    <p id="name-tag-mobile">Danh mục</p>
                                    </a>
                            </span>
                        <ul class="dropdown-menu wrap-menu-list clearfix ">
                            <!--Button SeeMore-->
                            <?php foreach ($rootCats as $rootCat) {?>
                                <li class="<?=$rootCat->slug;?>">
                                    <a href="<?=$rootCat->url?>"><img src="<?=$cdnUrl . $rootCat->menu_icon?>" alt=""><?=$rootCat->name;?></a>
                                </li>
                            <?php }?>
                        </ul>
                    </div>
                </li>
            </ul>

            <?php if (!Yii::$app->user->isGuest) {?>
                <?php echo $this->render('@app/views/layouts/common/header_right_login') ?>
            <?php } else {?>
                <?php echo $this->render('@app/views/layouts/common/header_right_guest') ?>
            <?php }?>

            <?php if (Yii::$app->request->url != Url::toRoute(['/user/security/login']) && Yii::$app->request->url != Url::toRoute(['/user/registration/register'])) {
	?>


            <div class="form-inline k-header-search col-md-4 col-md-pull-4 col-xs-12">
                <div class="row-menu-bar-mobile hidden-sm-up">
                    <div class="k-menu-list-course col-xs-8">
                        <a class="nav-link" href="#" data-offpage="#nav-mobile">
                            <i class="icon icon-bars"></i>
                            <span>Danh mục khóa học</span>
                        </a>
                    </div>
                    <div class="k-button-search-course col-xs-4">
                        <a id="k-button-search-course-mb" class="nav-link" href="#" data-offpage="#nav-mobile-search">
                            <i class="icon-search icon"></i>
                            <span>Tìm</span>
                        </a>
                    </div>
                </div>

                <?php
$get = $_GET;
	unset($get['q']);
	unset($get['sort']);
	unset($get['page']);
	$form = ActiveForm::begin([
		'id' => 'search-form',
		'action' => Url::toRoute(['/course/default/index']),
		'method' => 'get',
	]);?>

                <div class="input-group">
                    <button class="icon-search hidden-sm-up"></button>
                    <input id="live-search-bar" name="q" type="text" class="form-control live-search-bar" placeholder="Tìm khóa học bạn quan tâm"
                           autocomplete="off">
                        <span class="input-group-btn">
                            <button class="btn btn-secondary search-button" type="submit"><i
                                  class="icon-search icon hidden-480"></i>
                            <span class="mob">Tìm</span></button>
                        </span>
                    <span class="k-close-search-popup hidden-sm-up" id="k-close-search-popup">
                        <svg width="12" height="12" viewBox="0 0 24 24"><path d="m367 256l142-141c2-2 3-5 3-8c0-3-1-5-3-7l-96-97c-2-2-4-3-7-3c0 0 0 0 0 0c-3 0-6 1-8 3l-142 142l-141-142c-2-2-5-3-8-3c0 0 0 0 0 0c-3 0-5 1-7 3l-96 96c-5 4-5 11-1 15l142 142l-142 141c-4 4-4 11 0 15l96 97c2 2 4 3 7 3l0 0c3 0 6-1 8-3l142-142l141 142c2 2 5 3 8 3c3 0 5-1 7-3l97-96c4-4 4-11 0-15z" transform="scale(0.046875 0.046875)"></path></svg>
                    </span>
                </div>
                <?php ActiveForm::end();?>
                </div>
                <!--               Live earch result-->
                <div id="live-search-result" class="live-search-result" ></div>
                <?php $this->registerJsFile($cdnUrl . '/src/js/jquery-ui.js', ['position' => View::POS_END])?>
                <?php $this->registerJsFile($cdnUrl . '/src/js/autocomplete.js', ['position' => View::POS_END])?>
                <!--                End live search result-->


            <?php }?>

        </div><!--end .container-->
    </nav>

    <!-- Menu mobile -->
    <div id="nav-mobile" class="offpage-menu offpage-left">
        <header>
            <div class="k-header-offpage-menu">
                <a href="<?=$logoUrl?>">
                    <img src="<?=$logoImage?>" alt="Kyna.vn" class="img-responsive">
                </a>
                <a href="#" class="right offpage-close" data-offpage="#nav-mobile">
                    <i class="icon icon-arrow-left-bold" aria-hidden="true"></i>
                </a>
            </div><!--header -->
        </header>
        <div class="content">
            <div class="panel-group" id="accordion">
                <?php foreach ($rootCats as $rootCat) {?>
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <div class="panel-title <?=$rootCat->slug;?>">
                                <a href="<?=$rootCat->url;?>"><img src="<?=$cdnUrl . $rootCat->menu_icon?>" alt=""><?=$rootCat->name;?></a>
                            </div>
                        </div>
                    </div>
                <?php }?>
            </div>
            <!-- ./panel-group -->
        </div><!--end .content-->
    </div><!--end #nav-mobile-->
    <!-- End Menu mobile-->

    <!-- Menu Search Mobile -->
    <div id="nav-mobile-search" class="offpage-menu offpage-right">
        <header>
            <div class="k-header-offpage-menu">
                <h2>Tìm kiếm</h2>
                <a href="#" class="right offpage-close" data-offpage="#nav-mobile-search">
                    <i class="icon icon-arrow-right-bold" aria-hidden="true"></i>
                </a>
            </div><!--header -->
        </header>
        <div class="content">
            <?php
$form = ActiveForm::begin([
	'id' => 'search-form',
	'action' => Url::toRoute(['/course/default/index']),
	'method' => 'get',
]);?>
            <div class="input-group">
                <input style="margin-bottom: 2px"  id="m-live-search-bar" name="q" type="text" class="form-control live-search-bar" placeholder="Tìm kiếm...">
                <i class="icon icon-search"></i>
            </div>

            <?php ActiveForm::end();?>
            <div id="m-live-search-result" class="live-search-result" ></div>
            <h3 style="color: #50ad4e; padding: 20px 10px 0 10px;">Gợi ý cho bạn</h3>

            <?php foreach ($hotMobileKeywords as $tag):
?>
                <div id="tag-search-mobile" class="item">
                    <div class="box">
                        <a href="<?=Url::toRoute(['/course/default/index', 'tag' => $tag->slug]);?>" title="<?=$tag->title?>">
                            <span><?=$tag->tag;?></span>
                        </a>
                    </div>
                </div>
            <?php endforeach;?>
        </div><!--end .content-->
    </div>
    <!-- End Search mobile-->

    <!-- Menu Filter Mobile -->
    <div id="nav-filter-search" class="offpage-menu offpage-right">
        <header>
            <div class="k-header-offpage-menu">
                <h2>Bộ Lọc</h2>
                <a href="#" class="right offpage-close" data-offpage="#nav-filter-search">
                    <i class="icon icon-arrow-right-bold" aria-hidden="true"></i>
                </a>
            </div><!--header -->
        </header>
        <div class="content">
            <?= FacetMobile::widget() ?>
        </div><!--end .content-->
    </div>
    <!-- End Filter mobile-->

</header>
<style type="text/css">
    #m-live-search-result .ui-autocomplete {
        padding: 0px;
        background-color: white;
        border-radius: 0px;
        width: initial !important;
        border: 1px solid #aab2bd;
        top: 2px !important;
        margin-bottom: 0px;
    }
    #m-live-search-result .ui-autocomplete .ui-menu-item {
        font-size: small;
        padding: 0px !important;
    }
    #m-live-search-result .ui-autocomplete .ui-menu-item a {
        color: black;
        display: inline-block;
        width: 100%;
        padding: 10px 10px 0px 10px;
    }
</style>

<script>
    window.onscroll = function() {myFunction()};

    var navbar = document.getElementById("header");
    var sticky = navbar.offsetTop;

    function myFunction() {
      if (window.pageYOffset >= sticky + 60) {
        navbar.classList.add("sticky-top-mb")
      } else {
        navbar.classList.remove("sticky-top-mb");
      }
    }
</script>

<?php $script = "
;(function($){
    if($('#topbar').length > 0){
      var imgHeight = $('#topbar').find('img:visible').height();
      var LISTING_MARGIN_TOP = 67;
      $('.k-header-wrap').css('top', imgHeight + 'px');
      $('#k-listing').css('marginTop', LISTING_MARGIN_TOP + imgHeight + 'px');
    }

    $('body').on('submit', '#profile-form', function(e) {
         e.preventDefault();

         var url = $(this).attr('action');
         var form = $(e.target);
         console.log(form);
         console.log(form.parent());

         $.post(url, form.serialize(), function (res) {
             form.parents('.k-profile-edit-content').html(res);
         });
    });
    $('body').on('submit', '#active-cod-form', function(e){
        e.preventDefault();

        var url = $(this).attr('action');
        var form = $(e.target);

        $.post(url, form.serialize(), function (res) {
            form.parent().html(res);
        });
    });

})(jQuery);
"?>

<?php $this->registerJs($script, View::POS_END, 'profile-submit')?>

