<?php

$eventScript = '';
if ($loadEventScript) {
    $eventScript = <<<SCRIPT
/* GA tracking event */
(function ($, window, document, undefined) {
    $("body").on("click", "[data-ga]", function (e) {
        var eventData = $(e.target).data();

        ga('send', eventData.ga, {
            eventCategory: eventData.category,
            eventAction: eventData.action,
            eventLabel: eventData.label || e.target.href
        });
    });
})(window.jQuery, window, document);
SCRIPT;
    
}

$script = <<<SCRIPT
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', '$id', 'auto');
  ga('send', 'pageview');
$eventScript
SCRIPT;

$this->registerJs($script, $position, 'ga-script-' . $id);