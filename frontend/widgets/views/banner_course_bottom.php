<?php

use common\helpers\CDNHelper;

$cdnUrl = CDNHelper::getMediaLink();

?>
<div class="bottombar-lesson">
    <a class="close-bottombar" href="javascript:void(0)">
        <img src="<?= $cdnUrl ?>/img/lesson/close.png" alt="">
    </a>
    <a class="img-bottombar" href="<?= $banner->link ?>" target="_blank">
        <?= \common\helpers\Html::img($cdnUrl . $banner->image_url, [
            'alt' => $banner->title,
            'title' => $banner->title,
            'class' => 'img-pc'
        ]);
        ?>
        <?= \common\helpers\Html::img($cdnUrl . $banner->mobile_image_url, [
            'alt' => $banner->title,
            'title' => $banner->title,
            'class' => 'img-sp',
        ]);
        ?>
    </a>
</div>
<style>
    .bottombar-lesson {
        position: fixed;
        bottom: -1px;
        z-index: 20000000;
        width: 100%;
    }

    .close-bottombar {
        position: absolute;
        right: 15%;
        top: -5px;
    }

    @media (max-width: 540px) {
        .close-bottombar {
            right: 0;
            top: -20px;
        }
    }

    .img-bottombar {
        width: 100%;
        display: block;
    }

    .img-bottombar img {
        width: 100%;

    }

    @media (max-width: 540px) {
        .img-pc {
            display: none;
        }
    }

    .img-sp {
        display: none;
    }

    @media (max-width: 540px) {
        .img-sp {
            display: block;
        }
    }
</style>
<script>
    $(document).ready(function () {
        var date = new Date();
        if (getCookie('banner_bottom_mycourse_showed_<?= $course_id ?>') == date.getDate()) {
            $('.bottombar-lesson').hide();
        }
        $(".close-bottombar").click(function () {
            $(".bottombar-lesson").hide();
            $("#k-feedback-button").show();
            setCookie('banner_bottom_mycourse_showed_<?= $course_id ?>', date.getDate(), 1);
        });
        if ($('.bottombar-lesson').css('display') == 'none') {
            $("#k-feedback-button").show();
        } else {
            $("#k-feedback-button").hide();
        }
    });
</script>