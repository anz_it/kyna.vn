<?php

use yii\helpers\Url;
use common\helpers\CDNHelper;
use frontend\widgets\AnchorText;
use common\helpers\DateTimeHelper;
use kyna\promotion\models\UserVoucherFree;

$cdnUrl = CDNHelper::getMediaLink();
?>
<?php
$disable = '';
$updateWidth='';
$groupWidth ='';
$swap ='';
$disable = false;
if(isset(Yii::$app->params['disable_position'])){
    $disable = Yii::$app->params['disable_position'];
}

if(isset(Yii::$app->params['disable_position']) && !empty(Yii::$app->params['disable_position'])){
    $disable = 'style= "display: none;"';
    $updateWidth = 'style="width: 250px;"';
    $groupWidth ='style="width: 48.33%;"';
    $swap = 'style="max-width: 650px;"';
}
?>
<footer>
    <div id="k-footer">
        <div class="box container clearfix">
            <div <?= $updateWidth;?> class="hotline">
                <h4 class="bold text-transform title">Kết nối với Kyna</h4>

                <div class="social">
                    <?php if (empty($settings) || empty($settings['facebook_url'])) { ?>
                        <a href="https://www.facebook.com/kyna.vn" target="_blank"><img
                                    src="<?= $cdnUrl ?>/img/fb-icon.png" alt="facebook"></a>
                    <?php } else { ?>
                        <a href="<?= $settings['facebook_url'] ?>" target="_blank"><img
                                    src="<?= $cdnUrl ?>/img/fb-icon.png" alt="facebook"></a>
                    <?php } ?>

                    <?php if (empty($settings) || empty($settings['youtube_url'])) { ?>
                        <a href="https://www.youtube.com/user/kynavn" target="_blank" style="margin:0 5px"><img
                                    src="<?= $cdnUrl ?>/img/youtube-icon.png" alt="youtube"></a>
                    <?php } else { ?>
                        <a href="<?= $settings['youtube_url'] ?>" target="_blank" style="margin:0 5px"><img
                                    src="<?= $cdnUrl ?>/img/youtube-icon.png" alt="youtube"></a>
                    <?php } ?>

                    <a href="https://zalo.me/1985686830006307471" target="_blank"><img
                                src="<?= $cdnUrl ?>/img/zalo-icon.png" alt="zalo"></a>
                </div>
                <!--end .social-->

                <ul class="bottom">
                    <?php if (empty($settings) || empty($settings['hot_line'])) { ?>
                        <li>
                            <first>Hotline</first>
                            <second>1900 6364 09</second>
                        </li>
                    <?php } else { ?>
                        <li>
                            <first>Hotline</first>
                            <second><?= $settings['hot_line']; ?></second>
                        </li>
                    <?php } ?>
                    <?php if (empty($settings) || empty($settings['email_footer'])) { ?>
                        <li>
                            <first>Email</first>
                            <second>hotro@kyna.vn</second>
                        </li>
                    <?php } else { ?>
                        <li>
                            <first>Email</first>
                            <second><?= $settings['email_footer']; ?></second>
                        </li>
                    <?php } ?>
                </ul>
                <!--end .bottom-->

            </div>
            <!--end .hotline -->
            <div <?= $updateWidth;?> class="info">
                <h4 class="bold title">Thông tin Kyna</h4>
                <ul>
                    <li><a href="/danh-sach-khoa-hoc">Danh sách khóa học</a></li>
                    <li><a href="/p/kyna/cau-hoi-thuong-gap">Câu hỏi thường gặp</a></li>
                    <li><a href="/p/kyna/cau-hoi-thuong-gap/huong-dan-thanh-toan-hoc-phi">Cách thanh toán học phí</a></li>
                    <li><a href="/p/kyna/cau-hoi-thuong-gap/chinh-sach-hoan-hoc-phi">Chính sách hoàn học phí</a></li>
                    <li <?= $disable;?>><a href="/bai-viet">Thông tin hữu ích</a></li>
                </ul>
                <!--end .top-->
            </div>
            <!--end .info-->
            <div <?= $updateWidth;?> class="about">
                <h4 class="bold text-transform title">Về Kyna</h4>
                <ul>
                    <li><a href="/p/kyna/gioi-thieu" class="hover-color-green">Giới thiệu về công ty</a></li>
                    <li class="iconhot"><a href="/p/kyna/tuyen-dung" class="hover-color-green">Tuyển dụng</a></li>
                    <li><a href="/p/kyna/dao-tao-doanh-nghiep" class="hover-color-green">Đào tạo doanh nghiệp</a></li>
                    <li class="iconfeature"><a href="/p/kyna/giang-day" class="hover-color-green">Giảng dạy tại
                            Kyna.vn</a></li>
                    <li><a href="/p/kyna/quy-che-hoat-dong-sgdtmdt" class="hover-color-green">Quy chế hoạt động Sàn GDTMĐT</a></li>
                </ul>
                <!--end .top-->
            </div>
            <!--end .about-->
            <div <?= $disable;?> class="app-col">
                <h4 class="bold title">TẢI ỨNG DỤNG MOBILE</h4>
                <div class="icon-app">
                    <a href="https://itunes.apple.com/us/app/kyna/id1384374935?ls=1&mt=8" target="_blank" title="IOS">
                        <img src="<?= $cdnUrl ?>/img/apple-app-icon.svg" alt="apple-app-icon">
                    </a>
                    <a href="https://play.google.com/store/apps/details?id=com.kyna.app" target="_blank"
                       title="Android">
                        <img src="<?= $cdnUrl ?>/img/android-app-icon.svg" alt="android-app-icon">
                    </a>

                </div>
                <img src="<?= $cdnUrl ?>/img/qr-code.svg" alt="qr-code">
                <a class="details" href="https://kyna.vn/p/kyna/cau-hoi-thuong-gap/huong-dan-cai-dat-app-kyna"
                   target="_blank" title="Xam chi tiết"><span>?</span> Xem chi tiết</a>

            </div>
            <div class="fanpage">
                <div class="face-content">
                    <iframe src="//www.facebook.com/plugins/likebox.php?href=https://www.facebook.com/kyna.vn&amp;colorscheme=light&amp;show_faces=true&amp;stream=false&amp;header=false&amp;height=350&amp;width=255"
                            scrolling="no" frameborder="0"
                            style="border:none; overflow:hidden; width:100%; height:220px;"
                            allowTransparency="false"></iframe>
                </div>
            </div>
            <!--end .fanpage-->
        </div>
        <!--end .container-->
    </div>
    <!--end #wrap-footer-->
    <div id="kyna-group">
        <div class="container">
            <h4>Các sản phẩm <span>kyna group</span></h4>
            <div class="wrap" <?= $swap ?>>
                <div <?=$groupWidth?> class="item">
                    <a href="/" class="img"><img src="<?= $cdnUrl ?>/img/logo/kynavn.png" alt="kyna.vn"></a>
                    <p class="hidden-sm-down">Học kỹ năng mềm và chuyên môn online</p>
                </div>
                <div <?= $disable;?> class="item">
                    <a href="https://kynaforkids.vn/" class="img" target="_blank"><img
                                src="<?= $cdnUrl ?>/img/logo/kid.png" alt="kynaforkids.vn"></a>
                    <p class="hidden-sm-down">Trường học trực tuyến cho trẻ</p>
                </div>
                <div <?=$groupWidth?> class="item">
                    <a href="https://kynabiz.vn" class="img" target="_blank"><img
                                src="<?= $cdnUrl ?>/img/logo/kynabiz.png" alt="kynabiz.vn"></a>
                    <p class="hidden-sm-down">Đào tạo trực tuyến cho doanh nghiệp</p>
                </div>
            </div>
        </div>
    </div>

    <!--    Copyright   -->
    <div id="k-footer-copyright">
        <div class="container">
            <!-- Start Anchor Text -->
            <?= AnchorText::widget() ?>
            <!-- End Anchor Text -->
            <div class="col-lg-8 col-xs-12 address">

                <div class="text">
                    <p class="text-copyright">© 2016 - Bản quyền của Công Ty Cổ Phần Dream Viet Education</p>
                    <p>
                        <?php if (empty($settings['company_address'])) { ?>
                            Địa chỉ: Biệt thự 298/3 Điện Biên Phủ, phường 17, quận Bình Thạnh, TP Hồ Chí Minh
                            <?php
                        } else {
                            echo $settings['company_address'];
                        }
                        ?>
                    </p>
                    <p>
                        <?php if (empty($settings['bussiness_certificate'])) { ?>
                            Giấy phép ĐKKD số 0312401818 do Sở Kế hoạch và Đầu tư TPHCM cấp ngày 05/08/2013
                            <?php
                        } else {
                            echo $settings['bussiness_certificate'];
                        }
                        ?>
                    </p>
                </div>
                <!--end col-xs-8 text-->
            </div>
            <!--end .col-sm-7 col-xs-12 left-->
            <div class="col-lg-4 col-xs-12 info">
                <a href="http://online.gov.vn/HomePage/WebsiteDisplay.aspx?DocId=22275" target="_blank"><img alt=""
                                                                                                             title=""
                                                                                                             src="<?= $cdnUrl ?>/src/img/dadangky.png"
                                                                                                             data-pin-nopin="true"
                                                                                                             class="img-fluid"></a>
                <ul>
                    <li><a href="/p/kyna/dieu-khoan-dich-vu" class="hover-color-green">Điều khoản dịch vụ</a></li>
                    <li><a href="/p/kyna/chinh-sach-bao-mat" class="hover-color-green">Chính sách bảo mật</a></li>
                </ul>
            </div>
            <!--end .col-sm-5 col-xs-12 right-->
        </div>
        <!--end .container-->
    </div>
    <!--end #wrap-copyright-->
    <div id="k-footer-mb">
        <ul class="k-footer-mb-contact">
            <?php if (empty($settings) || empty($settings['hot_line'])) { ?>
                <li>
                    <a href="tel:1900 6364 09" target="_blank"><i class="icon icon-call"></i> 1900 6364 09</a>
                </li>
            <?php } else { ?>
                <li>
                    <a href="tel:<?= $settings['hot_line']; ?>" target="_blank"><i
                                class="icon icon-call"></i> <?= $settings['hot_line']; ?></a>
                </li>
            <?php } ?>
            <?php if (empty($settings) || empty($settings['email_footer'])) { ?>
                <li>
                    <a href="mailto:hotro@kyna.vn" target="_blank"><i class="icon icon-mail-outline"></i> hotro.kyna.vn</a>
                </li>
            <?php } else { ?>
                <li>
                    <a href="mailto:<?= $settings['email_footer']; ?>" target="_blank"><i
                                class="icon icon-mail-outline"></i> <?= $settings['email_footer']; ?></a>
                </li>
            <?php } ?>
        </ul>
        <div class="link">
            <a href="/p/kyna/cau-hoi-thuong-gap" class="link-text" target="_blank" title="">CÂU HỎI THƯỜNG GẶP</a>
            <a href="/bai-viet/" target="_blank" class="link-text" title="">THÔNG TIN HỮU ÍCH</a>
            <?php if($disable == false):?>
                <a href="http://onelink.to/2mrz6w" target="_blank">
                    <img src="<?= $cdnUrl ?>/img/ios-android-app.svg" alt="">
                </a>
            <?php endif;?>
        </div>
        <ul class="k-footer-mb-social">
            <li>
                <a href="https://www.facebook.com/kyna.vn" target="_blank"><i class="icon icon-facebook"></i></a>
            </li>
            <li>
                <a href="https://www.youtube.com/user/kynavn" target="_blank"><i class="icon icon-youtube"></i></a>
            </li>
        </ul>
        <p>&copy; 2016 - Công ty Cổ Phần Dream Viet Education</p>

        <?php if($disable == false):?>
            <?php if (!DateTimeHelper::checkAppPromotionTime()): ?>
                    <?php if ( Url::toRoute('course/default/view') == Url::toRoute( Yii::$app->controller->getRoute())):?>
                        <div class="mobilefooterbar" style="margin-bottom: 53px;">
                    <?php else:?>
                        <div class="mobilefooterbar"">
                    <?php endif;?>

                    <p>KYNA RA APP - HỌC CỰC ĐÃ</p>
                    <a href="http://onelink.to/2mrz6w" target="_blank" title="App">
                        <img class="icon-app" src="<?= $cdnUrl ?>/img/ios-android-app.svg" alt="App">
                    </a>
                    <img class="close" src="<?= $cdnUrl ?>/img/close-banner-footer.png" alt="Close">
                </div>
            <?php else: ?>
                <?php if ( Url::toRoute('course/default/view') == Url::toRoute( Yii::$app->controller->getRoute())):?>
                    <div class="mobilefooterbar" style="padding: 0 ;margin-bottom: 51px;">
                <?php else:?>
                    <div class="mobilefooterbar" style="padding: 0">
                <?php endif;?>
                    <a href="http://onelink.to/2mrz6w" target="_blank" title="App">
                        <img style="width: 100%" src="<?= $cdnUrl ?>/img/app_promotion/app_promotion_footer.png" alt="App">
                    </a>
                    <img class="close" src="<?= $cdnUrl ?>/img/close-banner-footer.png" alt="Close">
                </div>
            <?php endif; ?>
        <?php endif;?>

    </div><!--end #k-footer-mb-->

</footer>

<!-- Góp ý -->
<?php if (Yii::$app->controller->id !== 'checkout') : ?>
    <?php echo $this->render("@app/views/partials/common/feedback.php"); ?>
<?php endif; ?>

<!-- POPUP REGISTER -->
<div class="modal fade k-popup-account" id="k-popup-account-register" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel">

</div>
<!-- END POPUP REGISTER -->

<!-- POPUP  -->
<div class="modal fade k-popup-lesson" id="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">

        </div>
    </div>
</div>

<!-- POPUP  -->
<div class="modal fade popup-form-header k-popup-account" id="popup-register" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content popup-register">
        </div>
    </div>
</div>
<!-- END POPUP -->
<!-- /.modal -->
<div class="modal modal-activeCOD fade" id="activeCOD" tabindex="-1" role="dialog" aria-labelledby="activeCOD"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade k-popup-account" id="k-popup-account-reset" tabindex="-1" role="dialog">
</div>

<?php $user_id = Yii::$app->user->getId(); if(!empty($user_id)): ?>
    <?php $userVoucherFree = UserVoucherFree::getUserVoucherFree(Yii::$app->user->getId()); ?>
    <?php if (UserVoucherFree::isDateRunCampaignVoucherFree() == true): ?>
        <!-- Modal Voucher-->
        <div class="modal fade" id="modalvoucher" tabindex="-1" role="dialog" aria-labelledby="modalVoucher"
             aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>

                    <div class="modal-body">
                        <img class="title" src="<?= $cdnUrl ?>/img/voucher-free/voucher-free-title.png">
                        <img class="title-sp" src="<?= $cdnUrl ?>/img/voucher-free/voucher-free-title-sp.png">
                        <div class="copy-voucher">
                            <p>Đây là mã của bạn</p>
                            <input id="showvoucher" value="<?php echo $userVoucherFree->getCode(); ?>" type="text">
                            <a href="#" onclick="Copyvoucher()">Sao chép</a>
                        </div>

                        <ul>
                            <li>Cứ giới thiệu thành công 03 người khác sử dụng mã này, bạn sẽ được tặng thêm 01 lượt sử
                                dụng mã VOUCHER FREE trong chương trình. Bạn có thể xem lại mã này trong trang Khóa học
                                của tôi.
                            </li>
                            <li>Việc bạn sử dụng mã VOUCHER FREE của mình hay của người khác thì cũng tính là 01 lượt sử
                                dụng mã VOUCHER FREE trong chương tình.
                            </li>
                            <li>Trong lượt sử dụng đầu tiên thì bạn không thể sử dụng mã VOUCHER FREE của chính mình mà
                                bạn phải sử dụng mã VOUCHER FREE của người khác.
                            </li>
                            <li>Đối với lượt được tặng thêm, bạn có quyền sử dụng mã của mình hoặc của người khác. Ghi
                                chú: không tính lượt giới thiệu đối với lượt tự sử dụng mã của mình.
                            </li>
                        </ul>
                        <a class="fb-share" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=https://kyna.vn/p/campaign/voucher-free">Chia sẻ Facebook</a>
                        <div class="lp-voucher">
                            <a class="lp-link" target="_blank" href="https://kyna.vn/p/campaign/voucher-free">>> Xem giới thiệu chương
                                trình <<</a>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <p>Bạn đã giới thiệu được: <br><span><?php echo $userVoucherFree->getTotalIntroduce(); ?></span>
                            lượt dùng mã</p>
                        <p>Được tặng thêm: <span>0<?php echo $userVoucherFree->getTotalReceived(); ?></span> lượt
                            <br>(đã sử dụng <span>0<?php echo $userVoucherFree->getTotalConsume(); ?></span> lượt)</p>
                    </div>
                </div>
            </div>
        </div>
    <?php endif; ?>
<?php endif; ?>
<script>
    function Copyvoucher() {
        var copyText = document.getElementById("showvoucher");
        copyText.select();
        document.execCommand("copy");

    }
</script>
<style>
    #modalvoucher .modal-body {
        background-image: url("<?= $cdnUrl ?>/img/voucher-free/bg-popup-voucher-free.png");
        background-position: top;
        background-size: cover;
        background-repeat: no-repeat;
        text-align: center;
        padding: 20px 40px;
        color: #ffffff;

    }

    #modalvoucher .modal-body .title {
        margin-bottom: 15px;

    }

    #modalvoucher .modal-body .title-sp {
        display: none;

    }

    #modalvoucher .modal-body .copy-voucher p {

        padding-bottom: 10px;
        font-weight: 700;
        font-size: 15px;
        margin-bottom: 0;

    }

    #modalvoucher .modal-body .copy-voucher input {
        width: 182px;
        height: 35px;
        padding: 5px 10px;
        font-weight: 700;
        color: #000000;
        font-size: 15px;
        border: none;
        outline: none;
        border-radius: 5px 0 0 5px;
    }

    #modalvoucher .modal-body .copy-voucher a {
        background-color: #ffe800;
        color: #000000;
        font-size: 12px;
        font-weight: 700;
        padding: 12px 12px 9px;
        position: relative;
        left: -4px;
        border-radius: 0 5px 5px 0;
    }

    #modalvoucher .modal-body .copy-voucher a:active{
        background-color: #e5d000;
    }

    #modalvoucher .modal-body ul {
        text-align: left;
        padding: 15px 0 0 0;
        font-size: 14px;
        line-height: 1.4;
        margin-bottom: 20px;

    }

    #modalvoucher .modal-body ul li {
        padding-bottom: 8px;
        position: relative;
        text-align: justify;

    }

    #modalvoucher .modal-body ul li:before {
        content: '\f0da';
        font-family: FontAwesome;
        position: absolute;
        left: -10px;

    }

    #modalvoucher .modal-body .fb-share {
        border: 1px solid #ffffff;
        padding: 10px 20px;
        font-size: 15px;
        font-weight: 700;
        border-radius: 30px;
        color: #ffffff;
        margin-bottom: 15px;
    }

    #modalvoucher .modal-body .fb-share:before {
        content: '\f09a';
        font-family: FontAwesome;
        margin-right: 8px;
    }

    #modalvoucher .modal-body .lp-voucher {
        margin: 20px 0 0 0;
    }

    #modalvoucher .modal-body .lp-voucher .lp-link {
        border-bottom: 1px solid #ffffff;
        font-weight: 700;
        font-size: 15px;
        color: #ffffff;
    }

    #modalvoucher button {
        position: absolute;
        right: -20px;
        top: -20px;
        padding: 0px 10px;
        border-radius: 50%;
        background: white;
        z-index: 1;
        opacity: 1;
    }

    #modalvoucher button span {
        font-size: 40px;
        font-weight: 400;
        color: grey;
    }

    #modalvoucher .modal-footer {
        padding: 18px;
        text-align: center;
        background-color: #dd4f1b;
        border-top: none;
        color: #ffffff;

    }

    #modalvoucher .modal-footer p {
        font-size: 15px;
        line-height: 1.4;
        margin-bottom: 0;
    }

    #modalvoucher .modal-footer p br {
        display: none;
    }

    #modalvoucher .modal-footer p span {
        font-weight: 700;
    }

    @media screen and (max-width: 425px) {
        #modalvoucher .modal-body .title-sp {
            display: block;
            margin: 0 auto;
            margin-bottom: 15px;

        }

        #modalvoucher .modal-body .title {
            display: none;

        }

        #modalvoucher .modal-body {
            padding: 20px;
        }

        #modalvoucher .modal-body .copy-voucher input {
            width: 154px;
        }

        #modalvoucher .modal-footer p br {
            display: block;
        }
    }
</style>

<!--<div class="container">-->
<div class="popup modal fade modal-thong-bao-deltaxu" id="popup-thong-bao-deltaxu" tabindex="-1" role="dialog"
     aria-labelledby="popup-thong-bao-deltaxu" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12" style="font-style: italic;">
                        <p>Xin chào!</p>
                        <p>Cảm ơn sự ủng hộ của bạn dành cho Kyna.vn trong thời gian vừa qua. Đây là phiên bản mới của
                            Kyna.vn với nhiều thay đổi và cải tiến. Trong phiên bản này, <b>Deltaxu</b> sẽ được chuyển
                            thành điểm thưởng <b>K-point</b>.</p>
                        <p>Theo đó, Deltaxu trong phiên bản cũ sẽ được chuyển thành các mã Voucher tương ứng. Cụ thể
                            hơn, nếu trong tài khoản của bạn có 200.000 Deltaxu thì mã voucher bạn nhận được sẽ có giá
                            trị 200.000đ.</p>
                        <p>Bạn có thể sử dụng mã voucher này cho các giao dịch khóa học tại Kyna.vn như bình thường.
                            Thông tin chi tiết về sự thay đổi sẽ được gửi kèm theo email đến bạn.</p>
                    </div>

                    <div class="col-md-6 col-md-offset-6">
                        <div class="col-sm-12">
                            <img src="<?= $cdnUrl ?>/src/img/signature.png" alt="" class="img-responsive"
                                 style="margin: 0 auto; display: block;"/>
                        </div>
                        <div class="col-sm-12">
                            <h4 style="margin: 5px auto; font-weight: 300 !important; text-align: center;">Nguyễn Thanh
                                Minh</h4>
                            <h5 style="font-weight: 300 !important; text-align: center;">Co-founder Kyna.vn</h5>
                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>

</div>

<!--</div>-->

<div class="modal fade k-popup-account" id="k-popup-account-login" tabindex="-1" role="dialog">
</div>

<a href="<?= Url::toRoute('/cart/default/add-user-info') ?>" class="button-login" data-toggle="modal"
   data-target="#popup-user-info" id="btn-add-user-info" style="display: none;">Add User Info</a>
<div class="modal fade sale-promotion" id="popup-user-info" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <div class="modal-content">
        </div>
    </div>
</div>

<a href="<?= Url::toRoute('/cart/default/rethink-to-payment') ?>" class="button-login" data-toggle="modal"
   data-target="#modal-rethink-to-payment" id="btn-rethink-to-payment" style="display: none;">Rethink to payment</a>
<!-- Modal -->
<div class="modal fade sale-promotion" id="modal-rethink-to-payment" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <div class="modal-content">

        </div>
    </div>
</div>
<!-- End Modal -->

<!-- Modal Receive Teacher Notify -->
<div class="modal modal-notify fade" id="teacher_notify" tabindex="-1" role="dialog" aria-labelledby="teacher_notify"
     aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">

        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- Modal Notify-Campaign -->
<div class="modal fade" id="popup_checkout_page">
    <div class="modal-dialog content_popup">
        <img src="<?= $cdnUrl ?>/img/img-task-1000/warning-icon.png" alt=""/>
        <p>Bạn được tặng 1 khóa học trong chương trình <a href="/tag/mua1tang1" title="">Mua 1 Tặng 1</a> <br/>
            Bạn chưa chọn khóa tặng. Bạn chắc chắn muốn đặt hàng?</p>
        <div class="btn">
            <a href="/tag/mua1tang1">Chọn khóa tặng</a>
            <a href="<?= Url::toRoute(['/cart/checkout/index']) ?>" title="">Chắc chắn</a>
        </div>
    </div>
</div>

<div class="modal fade" id="get1_buy1">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="loading"
                 style="height: 190px; background-image: url('<?= $cdnUrl ?>/img/loading.gif'); background-repeat: no-repeat; background-position: center"></div>
        </div>
    </div>
</div>

<script>
    var countCart = <?= count(Yii::$app->cart->getPositions()) ?>;
    var sendData = true;

    // Close banner app mobile
    $('.mobilefooterbar .close').click(function () {
        $('.mobilefooterbar').hide();
    });

    function setCookie(name, value, expiredDay) {
        var expired = "";
        if (expiredDay !== null) {
            var date = new Date();
            date.setTime(date.getTime() + (expiredDay * 24 * 60 * 60 * 1000));
            expired = "expires=" + date.toUTCString();
        }
        document.cookie = name + "=" + value + ";" + expired + ";path=/";
    }

    function getCookie(name) {
        var name = name + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }

    // Set show mobilefooterbar once a day
    $(document).ready(function () {
        var date = new Date();
        if (getCookie('mobilefooterbar_showed') === "" && getCookie('mobilefooterbar_showed') !== date.getDate()) {
            setCookie('mobilefooterbar_showed', date.getDate(), 1);
            $('.mobilefooterbar').show();
        } else {
            $('.mobilefooterbar').hide();
        }
    });

</script>

<?php if (Yii::$app->controller->module->id === 'cart' || (Yii::$app->controller->module->id === 'course' && Yii::$app->controller->action->id === 'view')): ?>
    <style type="text/css">
        @media (max-width: 767px) {
            .zopim {
                display: none !important;
            }

            #k-wrap-feedback {
                display: none !important;
            }
        }
    </style>
<?php endif; ?>

<?php
$countDownTime = \common\helpers\DateTimeHelper::getCountDownTime();
$countDownTimeTet = \frontend\modules\course\widgets\CountDownCampaignTetTimer::getHoursCountDownIntervalTime();

if (!empty($countDownTime)):
    ?>
    <script type="text/javascript">
        $(document).ready(function () {
            var deadline = new Date(Date.parse(new Date()) + <?= $countDownTime?> * 1000);
            initializeClock('clock-flash-sale', deadline);
        });
    </script>
<?php endif; ?>

<?php if(\common\campaign\CampaignTet::InTimesCampaign()):?>
    <?php if (!empty($countDownTimeTet)):
        ?>
        <script type="text/javascript">
            $(document).ready(function () {
                var deadline = new Date(Date.parse(new Date()) + <?= $countDownTimeTet?> * 1000);
                initializeClock('clock-flash-sale', deadline);
            });
        </script>
    <?php endif; ?>
<?php endif; ?>
