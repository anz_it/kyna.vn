<?php
/**
 * Created by PhpStorm.
 * User: macintosh
 * Date: 5/16/17
 * Time: 2:08 PM
 */

use kyna\settings\models\BannerGroup;
use common\helpers\CDNHelper;

$cdnUrl = CDNHelper::getMediaLink();

?>
<div id="k-slide">
    <div class="container">
        <div class="owl-carousel">
            <?php foreach ($groups as $key => $group) : ?>
            <div class="box">
                <div class="lg-banner col-md-8 col-xs-12">
                    <?php
                    /* @var $group \kyna\settings\models\BannerGroup */
                    $mainBannerItems = $group->getItemsByPositionQuery(BannerGroup::POSITION_MAIN)->all();

                    foreach ($mainBannerItems as $mainBannerItem) {
                        /* @var $mainBannerItem \kyna\settings\models\BannerGroupItem */
                        /* @var $mainBanner \kyna\settings\models\Banner */
                        $mainBanner = $mainBannerItem->banner;
                    ?>
                        <div class="item">
                        <a href="<?= $mainBanner->link ?>" target="_blank">
                            <div class="info">
                                <h5>
                                    <?= $mainBanner->title ?>
                                </h5>
                                <?= $mainBanner->description ?>
                            </div>
                            <div class="img-wrap">
                                <?= CDNHelper::image($mainBanner->image_url, [
                                    'alt' => $mainBanner->title,
                                    'class' => 'img-fluid',
                                    'size' => CDNHelper::IMG_SIZE_ORIGINAL,
                                ]) ?>
                            </div>
                        </a>
                        </div>
                    <!-- end .item -->
                    <?php } ?>
                </div>
                <!-- end .lg-banner -->

                <div class="sm-banner col-md-4 col-xs-12">
                    <?php
                    /* @var $group \kyna\settings\models\BannerGroup */
                    $subBannerItems = $group->getItemsByPositionQuery(BannerGroup::POSITION_SUB)->all();

                    foreach ($subBannerItems as $subBannerItem) {
                        /* @var $subBanner \kyna\settings\models\Banner */
                        /* @var $subBannerItem \kyna\settings\models\BannerGroupItem */
                        $subBanner = $subBannerItem->banner;
                    ?>
                    <div class="item">
                        <div class="inner">
                            <a href="<?= $subBanner->link ?>" target="_blank" <?= $subBanner->title ?>>
                                <?= CDNHelper::image($subBanner->image_url, [
                                    'alt' => $subBanner->title,
                                    'class' => 'img-fluid',
                                    'size' => CDNHelper::IMG_SIZE_ORIGINAL,
                                ]) ?>
                            </a>
                            <div class="info">
                                <h5>
                                    <a href="<?= $subBanner->link ?>" target="_blank" <?= $subBanner->title ?>>
                                        <?= $subBanner->title ?>
                                    </a>
                                </h5>
                                <?= $subBanner->description ?>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                </div>
                <!-- end .sm-banner -->
            </div>
            <!-- end box<?= $key ?> -->
            <?php endforeach; ?>
        </div>
        <div class="owl-buttons-custom hidden-xs-up">
            <div class="owl-prev">
                <img src="<?= $cdnUrl ?>/src/img/home/arrow-left.png" class="arrow-left icon">
            </div>
            <div class="owl-next">
                <img src="<?= $cdnUrl ?>/src/img/home/arrow-right.png" class="arrow-right icon">
            </div>
        </div>
    </div>
    <!--end .container-->
</div>
<!--end #k-slide-->
