<?php

use common\helpers\Html;
use common\elastic\Course;
use common\helpers\CDNHelper;

$keys = [
    'type' => ['title' => 'Tìm theo loại khóa học', 'type' => 'checkbox'],
    'discount' => ['title' => 'Tìm theo chương trình khuyến mãi', 'type' => 'checkbox'],
    'time' => ['title' => 'Theo thời lượng', 'type' => 'select'],
    'level' => ['title' => 'Theo trình độ', 'type' => 'select'],
];

$arrKeys = array_keys($keys);
usort($facets, function ($a, $b) use ($arrKeys) {
    return array_search($a['key'], $arrKeys) > array_search($b['key'], $arrKeys);
});

$url = parse_url(Yii::$app->request->url);
$sort = Yii::$app->request->get('sort', isset($_GET['q']) && trim($_GET['q']) != '' ? '_score' : 'new');
?>
<?= Html::beginForm($url['path'], 'get', [
    'id' => 'facets-form',
    'class' => 'k-listing-filter form-inline'
]) ?>
    <?php
    if (isset($_GET['q'])) {
        echo Html::hiddenInput('q', $_GET['q']);
    }
    ?>
    <div class="container">
        <a href="javascript:" class="btn-listing-filter"><img
                    src="<?= CDNHelper::getMediaLink(); ?>/img/kyna-teach/icon-filter.png" alt="">Lọc kết quả</a>
        <?php foreach ($facets as $facet):
            if (!isset($keys[$facet['key']])) {
                continue;
            }
            $item = $keys[$facet['key']];
            sort($facet['facet_value']['buckets']);
            $fun = "getFacet" . ucfirst($facet['key']);
            ?>
            <?php if ($item['type'] == 'checkbox'): ?>
            <?php foreach ($facet['facet_value']['buckets'] as $bucket):
                $value = Course::$fun($bucket['key']);
                ?>
                <div class="checkbox">
                    <input id="facet-<?= $facet['key'] . '-' . $bucket['key'] ?>"
                           type="checkbox"
                           name="facets[<?= $facet['key'] ?>][<?= $bucket['key'] ?>]"
                            <?= isset($_GET['facets'][$facet['key']][$bucket['key']]) ? 'checked="checked"' : '' ?>
                           value="<?= $bucket['key'] ?>">
                    <label for="facet-<?= $facet['key'] . '-' . $bucket['key'] ?>">
                        <span><span></span></span><?= $value ?>
                    </label>
                </div>
            <?php endforeach; ?>
            <?php endif; ?>
            <?php if ($item['type'] == 'select'): ?>
            <select class="form-control"
                    id="facet-<?= $facet['key'] ?>"
                    name="facets[<?= $facet['key'] ?>][value]"
                    value="<?= isset($_GET['facets'][$facet['key']]['value']) ? $_GET['facets'][$facet['key']]['value'] : '' ?>">
                <option value=""><?= $item['title'] ?></option>
                <?php foreach ($facet['facet_value']['buckets'] as $bucket):
                    $value = Course::$fun($bucket['key']);
                    ?>
                    <option <?= (isset($_GET['facets'][$facet['key']]['value']) && $_GET['facets'][$facet['key']]['value'] == $bucket['key']) ? 'selected' : '' ?> value="<?= $bucket['key'] ?>"><?= $value ?></option>
                <?php endforeach; ?>
            </select>
            <?php endif; ?>
        <?php endforeach; ?>
        <?php if ($sort != '_score'): ?>
        <div class="field-sort">
            <label>Sắp xếp:</label>
            <select class="form-control"
                    id="sort"
                    name="sort"
                    value="<?= isset($_GET['sort']) ? $_GET['sort'] : '' ?>">
                <option <?= $sort == 'new' ? 'selected' : '' ?> value="new">Mới nhất</option>
                <option <?= $sort == 'feature' ? 'selected' : '' ?> value="feature">Nổi bật</option>
                <option <?= $sort == 'promotion' ? 'selected' : '' ?> value="promotion">% khuyến mãi</option>
            </select>
        </div>
        <?php endif; ?>
    </div>
<?= Html::endForm(); ?>

<script>
    $('#facets-form :checkbox').click(function () {
        var params = decodeURIComponent($('#facets-form').serialize());
        var action = $('#facets-form').attr('action');
        window.location.href = action + '?' + params;
    });
    $('#facets-form select').change(function () {
        var params = decodeURIComponent($('#facets-form').serialize());
        var action = $('#facets-form').attr('action');
        window.location.href = action + '?' + params;
    });

</script>
