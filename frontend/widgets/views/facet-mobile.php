<?php

use common\helpers\Html;
use common\elastic\Course;
use common\helpers\CDNHelper;

$keys = [
    'type' => ['title' => 'Phân loại khóa học', 'new-group' => true, 'end-group' => false],
    'discount' => ['title' => 'Phân loại khóa học', 'new-group' => false , 'end-group' => true],
    'level' => ['title' => 'Theo trình độ', 'new-group' => true, 'end-group' => true],
];

$arrKeys = array_keys($keys);
foreach ($facets as $index => $facet) {
    if (!in_array($facet['key'], $arrKeys)) {
        unset($facets[$index]);
    }
}
usort($facets, function ($a, $b) use ($arrKeys) {
    return array_search($a['key'], $arrKeys) > array_search($b['key'], $arrKeys);
});

$url = parse_url(Yii::$app->request->url);
$sort = Yii::$app->request->get('sort', isset($_GET['q']) && trim($_GET['q']) != '' ? '_score' : '');
?>
<?= Html::beginForm($url['path'], 'get', [
    'id' => 'filter-form',
    'class' => 'filter-form'
]) ?>
    <?php
    if (isset($_GET['q'])) {
        echo Html::hiddenInput('q', $_GET['q']);
    }
    $count = 0;
    ?>
    <?php foreach ($facets as $index => $facet):
        if (!isset($keys[$facet['key']])) {
            continue;
        }
        $count ++;
        $item = $keys[$facet['key']];
        sort($facet['facet_value']['buckets']);
        $fun = "getFacet" . ucfirst($facet['key']);
        ?>
        <?php if ($item['new-group'] || $count == 1): ?>
        <div class="k-listing-characteristics price-facet">
            <?php if (!empty($item['title'])): ?>
            <h3><?= $item['title'] ?></h3>
            <?php endif; ?>
            <ul class="k-listing-characteristics-list">
        <?php endif; ?>
        <?php foreach ($facet['facet_value']['buckets'] as $bucket):
            $value = Course::$fun($bucket['key']);
            ?>
            <li class="checkbox">
                <input id="mobile-facet-<?= $facet['key'] . '-' . $bucket['key'] ?>"
                       type="checkbox"
                       name="facets[<?= $facet['key'] ?>][<?= $bucket['key'] ?>]"
                        <?= isset($_GET['facets'][$facet['key']][$bucket['key']]) ? 'checked="checked"' : '' ?>
                       value="<?= $bucket['key'] ?>">
                <label for="mobile-facet-<?= $facet['key'] . '-' . $bucket['key'] ?>"> <span><span></span></span><?= $value ?></label>
            </li>
        <?php endforeach; ?>
        <?php if ($item['end-group']): ?>
            </ul>
        </div>
        <?php endif; ?>
    <?php endforeach; ?>
    <?php if ($sort != '_score'): ?>
        <div class="k-listing-characteristics price-facet">
            <h3>Sắp xếp</h3>
            <ul class="k-listing-characteristics-list">
                <li class="radio">
                    <input id="sort-new" type="radio" name="sort" value="new" <?= $sort == 'new' ? 'checked="checked"' : '' ?>>
                    <label for="sort-new"> <span><span></span></span>Mới nhất</label>
                </li>
                <li class="radio">
                    <input id="sort-hot" type="radio" name="sort" value="feature" <?= $sort == 'feature' ? 'checked="checked"' : '' ?>>
                    <label for="sort-hot"> <span><span></span></span>Nổi bật</label>
                </li>
                <li class="radio">
                    <input id="sort-promotion" type="radio" name="sort" value="promotion" <?= $sort == 'promotion' ? 'checked="checked"' : '' ?>>
                    <label for="sort-promotion"> <span><span></span></span>% khuyến mãi</label>
                </li>
            </ul>
        </div>
    <?php endif; ?>
    <div class="group-price-facet">
        <button id="mobile_filter_reset" class="btn btn-cancel" type="button">Đặt lại</button>
        <button class="btn btn-submit" type="submit">Áp dụng</button>
    </div>
<?= Html::endForm(); ?>

<script>
    $('#mobile_filter_reset').click(function(){
        $('input[type=checkbox], input[type=radio]').prop('checked', false);
    });
    $('#filter-form').submit(function (e) {
        e.preventDefault();
        var params = decodeURIComponent($('#filter-form').serialize());
        var action = $('#filter-form').attr('action');
        window.location.href = action + '?' + params;
    });
</script>
