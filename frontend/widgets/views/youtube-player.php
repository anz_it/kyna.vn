<?php

use yii\helpers\Url;
use common\helpers\CDNHelper;
use common\helpers\StringHelper;

?>
<div class="videoWrapper <?= ($thumbnailSize == CDNHelper::IMG_SIZE_THUMBNAIL_YOUTUBE) ? 'ytp-small-mode' : '' ?>">
    <?php if (!empty($model->video_cover_image_url) && !is_null($model->youtubeEmbedUrl)): ?>
        <div id="play_video" class="cursor-pointer">
            <?= CDNHelper::image($model->video_cover_image_url, [
                'alt' => $model->name,
                'class' => 'img-fluid',
                'size' => $thumbnailSize,
                'resizeMode' => 'crop',
            ]) ?>
            <button class="ytp-large-play-button ytp-button" aria-label="<?= $model->name ?>">
                <svg height="100%" version="1.1" viewBox="0 0 68 48" width="100%"><path class="ytp-large-play-button-bg" d="m .66,37.62 c 0,0 .66,4.70 2.70,6.77 2.58,2.71 5.98,2.63 7.49,2.91 5.43,.52 23.10,.68 23.12,.68 .00,-1.3e-5 14.29,-0.02 23.81,-0.71 1.32,-0.15 4.22,-0.17 6.81,-2.89 2.03,-2.07 2.70,-6.77 2.70,-6.77 0,0 .67,-5.52 .67,-11.04 l 0,-5.17 c 0,-5.52 -0.67,-11.04 -0.67,-11.04 0,0 -0.66,-4.70 -2.70,-6.77 C 62.03,.86 59.13,.84 57.80,.69 48.28,0 34.00,0 34.00,0 33.97,0 19.69,0 10.18,.69 8.85,.84 5.95,.86 3.36,3.58 1.32,5.65 .66,10.35 .66,10.35 c 0,0 -0.55,4.50 -0.66,9.45 l 0,8.36 c .10,4.94 .66,9.45 .66,9.45 z" fill="#1f1f1e" fill-opacity="0.81"></path><path d="m 26.96,13.67 18.37,9.62 -18.37,9.55 -0.00,-19.17 z" fill="#fff"></path><path d="M 45.02,23.46 45.32,23.28 26.96,13.67 43.32,24.34 45.02,23.46 z" fill="#ccc"></path></svg>
            </button>
        </div>
        <div id="youtube_video_wrapper">
            <!-- Copy & Pasted from YouTube -->
            <iframe width="560" height="349" src="<?= $model->youtubeEmbedUrl ?>?enablejsapi=1" frameborder="0" allowfullscreen></iframe>
        </div>
    <?php elseif (!is_null($model->youtubeEmbedUrl)): ?>
        <!-- Copy & Pasted from YouTube -->
        <div id="play_video" class="cursor-pointer">
            <img class="img-fluid" src="https://img.youtube.com/vi/<?php echo StringHelper::getYouTubeIdFromURL($model->youtubeEmbedUrl) ?>/maxresdefault.jpg" alt="" title="">
            <button class="ytp-large-play-button ytp-button" aria-label="<?= $model->name ?>">
                <svg height="100%" version="1.1" viewBox="0 0 68 48" width="100%"><path class="ytp-large-play-button-bg" d="m .66,37.62 c 0,0 .66,4.70 2.70,6.77 2.58,2.71 5.98,2.63 7.49,2.91 5.43,.52 23.10,.68 23.12,.68 .00,-1.3e-5 14.29,-0.02 23.81,-0.71 1.32,-0.15 4.22,-0.17 6.81,-2.89 2.03,-2.07 2.70,-6.77 2.70,-6.77 0,0 .67,-5.52 .67,-11.04 l 0,-5.17 c 0,-5.52 -0.67,-11.04 -0.67,-11.04 0,0 -0.66,-4.70 -2.70,-6.77 C 62.03,.86 59.13,.84 57.80,.69 48.28,0 34.00,0 34.00,0 33.97,0 19.69,0 10.18,.69 8.85,.84 5.95,.86 3.36,3.58 1.32,5.65 .66,10.35 .66,10.35 c 0,0 -0.55,4.50 -0.66,9.45 l 0,8.36 c .10,4.94 .66,9.45 .66,9.45 z" fill="#1f1f1e" fill-opacity="0.81"></path><path d="m 26.96,13.67 18.37,9.62 -18.37,9.55 -0.00,-19.17 z" fill="#fff"></path><path d="M 45.02,23.46 45.32,23.28 26.96,13.67 43.32,24.34 45.02,23.46 z" fill="#ccc"></path></svg>
            </button>
        </div>
        <div id="youtube_video_wrapper">
            <iframe width="560" height="349" src="<?= $model->youtubeEmbedUrl ?>?enablejsapi=1" frameborder="0" allowfullscreen></iframe>
        </div>
    <?php elseif (!empty($model->video_cover_image_url)): ?>
        <div id="play_video">
            <?= CDNHelper::image($model->video_cover_image_url, [
                'alt' => $model->name,
                'class' => 'img-fluid',
                'size' => $thumbnailSize,
                'resizeMode' => 'crop',
            ]) ?>
        </div>
    <?php else : ?>
        <div id="play_video">
            <?= CDNHelper::image($model->image_url, [
                'alt' => $model->name,
                'class' => 'img-fluid',
                'size' => $thumbnailSize,
                'resizeMode' => 'crop',
            ]) ?>
        </div>
    <?php endif; ?>
    <div class="label-wrap">
        <span class="lb-new">NEW</span>
        <span class="lb-hot">HOT</span>
    </div>
    <?= $this->render('_rating', ['model' => $model]) ?>
</div>
<style>
    #youtube_video_wrapper {
        display: none;
    }
    #play_video {
        margin-top: -25px;
    }
    #play_video > img {
        width: 100%;
    }
    .cursor-pointer {
        cursor: pointer;
    }
    .ytp-button {
        border: none;
        background-color: transparent;
        padding: 0;
        color: inherit;
        text-align: inherit;
        font-size: 100%;
        font-family: inherit;
        cursor: default;
        line-height: inherit;
    }
    .ytp-button:focus, .ytp-button {
        outline: 0;
    }
    .ytp-large-play-button {
        position: absolute;
        left: 50%;
        top: 50%;
        width: 68px;
        height: 48px;
        margin-left: -34px;
        margin-top: -24px;
        -moz-transition: opacity .25s cubic-bezier(0.0,0.0,0.2,1);
        -webkit-transition: opacity .25s cubic-bezier(0.0,0.0,0.2,1);
        transition: opacity .25s cubic-bezier(0.0,0.0,0.2,1);
    }
    .ytp-small-mode .ytp-large-play-button {
        width: 42px;
        height: 30px;
        margin-left: -21px;
        margin-top: -15px;
    }
    .ytp-button:not([aria-disabled=true]):not([disabled]):not([aria-hidden=true]) {
        cursor: pointer;
    }
    .html5-video-player svg {
        pointer-events: none;
    }
    .ytp-large-play-button-bg {
        -moz-transition: fill .1s cubic-bezier(0.4,0.0,1,1),fill-opacity .1s cubic-bezier(0.4,0.0,1,1);
        -webkit-transition: fill .1s cubic-bezier(0.4,0.0,1,1),fill-opacity .1s cubic-bezier(0.4,0.0,1,1);
        transition: fill .1s cubic-bezier(0.4,0.0,1,1),fill-opacity .1s cubic-bezier(0.4,0.0,1,1);
        fill: #1f1f1f;
        fill-opacity: .81;
    }
    .videoWrapper:hover .ytp-large-play-button-bg {
        -moz-transition: fill .1s cubic-bezier(0.0,0.0,0.2,1),fill-opacity .1s cubic-bezier(0.0,0.0,0.2,1);
        -webkit-transition: fill .1s cubic-bezier(0.0,0.0,0.2,1),fill-opacity .1s cubic-bezier(0.0,0.0,0.2,1);
        transition: fill .1s cubic-bezier(0.0,0.0,0.2,1),fill-opacity .1s cubic-bezier(0.0,0.0,0.2,1);
        fill: #cc181e;
        fill-opacity: 1;
    }
</style>
<script type="application/javascript">
    ;(function($, window, document, undefined){
        $(document).ready(function(){
            $('#play_video').on('click', function (event) {
                event.preventDefault();
                $('#youtube_video_wrapper > iframe')[0].contentWindow.postMessage('{"event":"command", "func":"playVideo", "args":""}', '*');
                $(this).hide();
                $('#youtube_video_wrapper').show();
            });
        });
    })(window.jQuery || window.Zepto, window, document);
</script>
