<div class="category-mobile">
    <?php if (!empty($rootCats)) : ?>
        <?php foreach ($rootCats as $rootCat):
            $menuIcon = (empty($rootCat->menu_icon) && !empty($rootCat->parent)) ? $rootCat->parent->menu_icon : $rootCat->menu_icon;
            ?>
            <div>
                <a  class="<?= $catId == $rootCat->id ? 'active' : ''?>" href="<?= $rootCat->url ?>"><?= $rootCat->name ?></a>
            </div>


        <?php endforeach; ?>
    <?php else: ?>
        <span class="empty">Không có danh mục nào</span>
    <?php endif; ?>
</div>