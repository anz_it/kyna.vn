<?php

use yii\helpers\Url;
use app\models\Course;
use common\helpers\CDNHelper;

$cdnUrl = CDNHelper::getMediaLink();
$settings = Yii::$app->controller->settings;
$logoImage = $settings['lp_logo_image'];
?>
<header>
    <div class="k-header-offpage-menu">
        <img src="<?= $logoImage ?>" alt="Kyna.vn" class="img-responsive">
        <a href="#" class="left offpage-close" data-offpage="#offpage-listing-sidebar">
            <i class="icon icon-arrow-right-bold" aria-hidden="true"></i>
        </a>
        <!--<button type="button" class="k-button">Bỏ lọc</button>       -->
    </div><!--header -->
</header>
<section>
    <div class="k-listing-category">
        <?php if (empty($catModel)) : ?>
            <h3>Danh mục khóa học</h3>
        <?php else: ?>
            <h1 class="title-cat"><?= $catModel->name ?></h1>
        <?php endif; ?>
        <ul class="k-category-list pd0">
            <?php if (!empty($rootCats)) : ?>
                <?php foreach ($rootCats as $rootCat):
                    $menuIcon = (empty($rootCat->menu_icon) && !empty($rootCat->parent)) ? $rootCat->parent->menu_icon : $rootCat->menu_icon;
                    ?>
                    <li class="<?= $rootCat->slug ?>">
                        <a href="<?= $rootCat->url ?>">
                            <img src="<?= $cdnUrl . $menuIcon ?>" alt=""><?= $rootCat->name ?>
                        </a>
                        <?php if (!empty($rootCat->children)) : ?>
                            <span class="float-right"><i class="icon-arrow-right"></i></span>
                        <?php endif; ?>
                    </li>
                <?php endforeach; ?>
            <?php else: ?>
                <li class="empty">Không có danh mục nào</li>
            <?php endif; ?>
            <?php if (!empty($catId)) : ?>
                <li class="turn-back">
                    <a href="<?= Url::toRoute(['/course/default/index']) ?>">
                        <i class="icon-arrow-left-bold icon"></i> Xem các danh mục khác
                    </a>
                </li>
            <?php endif; ?>
        </ul>
    </div><!--end k-category-->
</section>
