<?php

use yii\widgets\ActiveForm;
use yii\web\View;
use yii\helpers\Url;
use app\models\CourseSearch;
use kyna\tag\models\Tag;
use yii\helpers\Html;

$settings = Yii::$app->controller->settings;

// TODO: get hot key words by search
$hotKeywords = Tag::getDesktopTags(10);
?>
<?php $form = ActiveForm::begin([
    'id' => 'facet-filter-form',
    'action' => $action,
    'method' => 'get'
]) ?>
<section>
    <?php if (!empty($refiners['price'])) : ?>
        <div class="k-listing-characteristics price-facet">
            <h3>Tìm theo chương trình khuyến mãi</h3>
            <ul class="k-listing-characteristics-list">
                <?php foreach($refiners['price'] as $item) : ?>
                    <li class="checkbox">
                       <input id="facet-course-promotion-<?= $item['id'] ?>" type="checkbox" name="price[<?= $item['id'] ?>]" value="<?= $item['id'] ?>" <?php if (!empty($get['price']) && in_array($item['id'], $get['price'])) : ?>checked="checked"<?php endif; ?>>
                       <label for="facet-course-promotion-<?= $item['id'] ?>">
                           <span><span></span></span><?= $item['id'] == 0 ? 'Miễn phí' : 'Đang khuyến mãi' ?>
                       </label>
                       <span class="tag"><?= $item['active'] ?></span>
                    </li>
                <?php endforeach; ?>
            </ul>
        </div>
    <?php endif; ?>

    <?php if (!empty($refiners['type'])) : ?>
        <?php
        $show = false;
        foreach ($refiners['type'] as $item) {
            if (!empty($item['all'])) {
                $show = true;
            }
        }
        ?>
        <?php if ($show) : ?>
            <div class="k-listing-characteristics type-facet">
                <h3>Tìm theo đặc điểm khóa học</h3>
                <ul class="k-listing-characteristics-list">
                    <?php foreach($refiners['type'] as $item) : ?>
                        <li class="checkbox">
                            <input id="facet-course-type-<?= $item['id'] ?>" type="checkbox" name="type[<?= $item['id'] ?>]" value="1" <?php if (!empty($get['type']) && key_exists($item['id'], $get['type'])) : ?>checked="checked"<?php endif; ?>>
                            <label for="facet-course-type-<?= $item['id'] ?>">
                                <span><span></span></span><?= $item['title'] ?>
                            </label>
                            <span class="tag"><?= $item['active'] ?></span>
                        </li>
                    <?php endforeach; ?>
                </ul>
            </div>
        <?php endif; ?>
    <?php endif; ?>

    <?php if (!empty($refiners['total_time'])) : ?>
        <?php
        $show = false;
        foreach ($refiners['total_time'] as $item) {
            if (!empty($item['all'])) {
                $show = true;
            }
        }
        ?>
        <?php if ($show) : ?>
            <div class="k-listing-time">
                <h3 class="title">Tìm theo thời lượng</h3>
                <ul class="k-listing-time-list">
                    <?php foreach($refiners['total_time'] as $item) : ?>
                        <?php if (!empty($item['active'])) : ?>
                            <li class="checkbox">
                                <input id="facet-course-time-range-<?= $item['id'] ?>" type="checkbox" name="total_time[<?= $item['id'] ?>]" value="<?= $item['id'] ?>" <?php if (!empty($get['total_time']) && in_array($item['id'], $get['total_time'])) : ?>checked="checked"<?php endif; ?>>
                                <label for="facet-course-time-range-<?= $item['id'] ?>"><span><span></span></span><?= $item['title'] ?></label>
                                <span class="tag"><?= $item['active'] ?></span>
                            </li>
                        <?php endif; ?>
                    <?php endforeach; ?>
                </ul>
            </div><!--end .cat-box-3-->
        <?php endif; ?>
    <?php endif; ?>

    <?php if (!empty($refiners['level'])) : ?>
        <div class="k-listing-level">
            <h3>Tìm theo trình độ yêu cầu</h3>
            <ul class="k-listing-level-list">
                <?php foreach ($refiners['level'] as $item) : ?>
                    <?php if (!empty($item['active'])) : ?>
                        <li class="checkbox">
                            <input id="facet-course-level-<?= $item['id'] ?>" type="checkbox" name="level[<?= $item['id'] ?>]" value="<?= $item['id'] ?>" <?php if (!empty($get['level']) && in_array($item['id'], $get['level'])) : ?>checked="checked"<?php endif; ?>>
                            <label for="facet-course-level-<?= $item['id'] ?>"><span><span></span></span><?= $listLevel[$item['id']] ?></label>
                            <span class="tag"><?= $item['active'] ?></span>
                        </li>
                    <?php endif; ?>
                <?php endforeach; ?>
            </ul>
        </div>
    <?php endif; ?>
        
    <div class="k-listing-hot-topics">
        <h3>Chủ đề đang hot</h3>
        <ul class="k-listing-hot-topics-list">
            <?php foreach ($hotKeywords as $tag) : ?>
                <li><a href="<?= Url::toRoute(['/course/default/index', 'tag' => $tag->slug]) ?>" title="<?= $tag->tag ?>"><?= $tag->tag ?></a></li>
            <?php endforeach; ?>
        </ul>
    </div><!--end .cat-box-5-->
    
    <?php if (!empty(Yii::$app->request->get('q'))) : ?>
        <?php $q = Yii::$app->request->get('q'); ?>
        <input type="hidden" name="q" value="<?= \common\helpers\Html::encode($q) ?>"/>
    <?php endif; ?>
        
    <input type="hidden" name="sort" value="<?= Html::encode(Yii::$app->request->get('sort', CourseSearch::SORT_NEW)) ?>"/>
    <button class="btn btn-update hidden-md-up">Cập nhật</button>
</section>
<?php ActiveForm::end() ?>

<?php
$script = "
    $('#facet-filter-form input[type=\'checkbox\']').on('change', function() {
        var checkedEle = $('#facet-filter-form .price-facet input[type=\'checkbox\']:checked');
        var sortEle = $('#facet-filter-form input[name=\'sort\']');

        if (checkedEle.length === 0) {
            sortEle.val('" . CourseSearch::SORT_NEW . "');
        } else if ($('#facet-course-promotion-1').prop('checked') === false) {
            if (sortEle.val() === '" . CourseSearch::SORT_PROMOTION . "') {
                sortEle.val('" . CourseSearch::SORT_NEW . "');
            }
        } else if (sortEle.val() !== '" . CourseSearch::SORT_PROMOTION . "') {
            sortEle.val('" . CourseSearch::SORT_PROMOTION . "');
        }
        if (window.matchMedia('(min-width: 768px)').matches)
            $('#facet-filter-form').submit();
    });
    
    $('body').on('click', '.sort-type li a.nav-link', function () {
        var type = $(this).data('sort');
        $('#facet-filter-form input[name=\'sort\']').val(type);
        
        switch (type) {
            case '" . CourseSearch::SORT_PROMOTION . "':
                var promoEle = $('#facet-course-promotion-1');
                if (promoEle.length === 0) {
                    $('#facet-filter-form').append('<input type=\'hidden\' value=\'1\' name=\'price[1]\' id=\'facet-course-promotion-1\'>');
                } else {
                    $('#facet-course-promotion-1').prop('checked', true);
                }

                break;
                
            default:
                $('#facet-course-promotion-1').prop('checked', false);
                
                break;
        }
        $('#facet-filter-form').submit();
    });
";
        
$this->registerJs($script, View::POS_END, 'my-options');
?>