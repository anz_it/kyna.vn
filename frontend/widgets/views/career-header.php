<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ActiveForm;
use common\helpers\CDNHelper;

$cdnUrl = CDNHelper::getMediaLink();
$settings = Yii::$app->controller->settings;
$logoImage = $settings['lp_logo_image'];
$logoUrl = $settings['lp_logo_url'];
?>

<header>
    <div class="container">
        <nav class="wrap-main">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <h2 class="logo"style="width: 155px; line-height: 0; padding: 10px 0">
					<a href="<?= $logoUrl ?>"><img src="<?= $logoImage ?>" alt="Kyna.vn" class="img-responsive" /></a>
				</h2>
            </div>
            <div id="navbar" class="navbar-collapse collapse menu">
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="/p/kyna/tuyen-dung">Tuyển dụng</a></li>
                    <li><a href="/p/kyna/doi-ngu">Đội ngũ</a></li>
                    <li><a href="/p/kyna/giang-day">Giảng dạy</a></li>
					          <li><a href="/p/kyna/dao-tao-doanh-nghiep">Đào tạo doanh nghiệp</a></li>
                    <li><a href="/p/kyna/gioi-thieu">Giới thiệu</a></li>
                </ul>
            </div>
            <!--/.nav-collapse -->
        </nav>
    </div>
    <!--end .container-->
</header>
