<?php
/**
 * Created by PhpStorm.
 * User: khanhphan
 * Date: 4/28/17
 * Time: 12:39 AM
 */

namespace app\widgets;


use kyna\course\models\Course;
use yii\base\Widget;
use yii\helpers\Url;

class ElasticFacet extends Widget
{

    private function getAction()
    {
        $get = \Yii::$app->request->get();
        if (isset($get['course_type']) && $get['course_type'] == Course::TYPE_COMBO) {
            $params = ['/course/default/index', 'course_type' => Course::TYPE_COMBO];
        } else {
            $params = ['/course/default/index'];
        }

        if (isset($get['tag'])) {
            $params['tag'] = $get['tag'];
        }
        if (isset($get['catId'])) {
            $params['catId'] = $get['catId'];
        }
        if (isset($get['slug'])) {
            $params['slug'] = $get['slug'];
        }
        if (isset($get['page'])) {
            $params['page'] = $get['page'];
        }

        return Url::toRoute($params);
    }
    public function run()
    {
        $facets = \Yii::$app->controller->elasticFacets;
        return $this->render('elastic_facet', ['facets' => $facets, 'action' => $this->getAction()]);
    }
}