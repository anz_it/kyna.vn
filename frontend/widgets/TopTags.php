<?php
/**
 * Created by PhpStorm.
 * User: khanhphan
 * Date: 5/1/17
 * Time: 1:24 PM
 */

namespace frontend\widgets;


use kyna\tag\models\Tag;
use yii\base\Widget;

class TopTags extends Widget
{
    public function run()
    {
        $tags = Tag::topTags(Tag::TYPE_DESKTOP, 10);
        return $this->render('top_tags', ['tags' => $tags]);
    }

}