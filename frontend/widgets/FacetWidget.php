<?php

namespace app\widgets;

use Yii;
use yii\helpers\Url;
use app\models\Course;

/*
 * This is KFacet widget to render list facets of course data
 */

class FacetWidget extends yii\base\Widget
{

    public function run()
    {
        return "";
        $get = Yii::$app->request->get();

        $refiners = Yii::$app->controller->refinerSet->getRefinerValues();
        $listLevel = Course::listLevel();

        return $this->render('facet', [
                    'refiners' => $refiners,
                    'listLevel' => $listLevel,
                    'get' => $get,
                    'action' => $this->getAction($get)
        ]);
    }

    private function getAction($get)
    {
        if (isset($get['course_type']) && $get['course_type'] == Course::TYPE_COMBO) {
            $params = ['/course/default/index', 'course_type' => Course::TYPE_COMBO];
        } else {
            $params = ['/course/default/index'];
        }

        if (isset($get['tag'])) {
            $params['tag'] = $get['tag'];
        }
        if (isset($get['catId'])) {
            $params['catId'] = $get['catId'];
        }
        if (isset($get['slug'])) {
            $params['slug'] = $get['slug'];
        }
        if (isset($get['page'])) {
            $params['page'] = $get['page'];
        }

        return Url::toRoute($params);
    }

}
