<?php
/**
 * Created by PhpStorm.
 * User: macintosh
 * Date: 8/8/17
 * Time: 4:26 PM
 */

namespace frontend\widgets;

use mobile\models\UserCourse;
use Yii;
use yii\web\Cookie;
use yii\base\Widget;
use kyna\settings\models\Banner;

class PopupWidget extends Widget
{

    const POSITION_ALL = 'ALL';
    const POSITION_ALL_EXCEPT_CHECKOUT = 'ALL-EXCEPT-CHECKOUT';
    const POSITION_LEARNING_COURSE = 'LEARNING-COURSE';
    const POSITION_MY_COURSE = 'MY-COURSE';

    /*
     * Position to display: all, checkout, list courses...
     */
    public $position;

    public $course_id;

    public function run()
    {
        $date = date('Y-m-d');

        $types = [];
        switch ($this->position) {
            case self::POSITION_ALL:
                $types[] = Banner::TYPE_POPUP;

                break;

            case self::POSITION_LEARNING_COURSE;
                $types[] = Banner::TYPE_POPUP_LEARNING;
                break;

            case self::POSITION_MY_COURSE;
                $types[] = Banner::TYPE_POPUP_MY_COURSE;
                break;

            default;
        }



        if($this->position == self::POSITION_MY_COURSE ){
            $userId = Yii::$app->getUser()->getId();
            if($userId == null)
                return;
            $popup = Banner::find()->where([
                'type' => $types
            ])
                ->joinWith('bannerCourses bc')
                ->rightJoin('user_courses uc',"uc.course_id = bc.course_id AND uc.user_id = $userId")
                ->andWhere("from_date is null or (from_date is not null and from_date <= '{$date}')")
                ->andWhere("to_date is null or (to_date is not null and to_date >= '{$date}')")
                ->andWhere(['banners.status' => Banner::STATUS_ACTIVE])
                ->orderBy('id DESC')
                ->one();
        }else{
            $popup = Banner::find()->where([
                'type' => $types
            ])
                ->joinWith('bannerCourses bc')
                ->andWhere("from_date is null or (from_date is not null and from_date <= '{$date}')")
                ->andWhere("to_date is null or (to_date is not null and to_date >= '{$date}')")
                ->andWhere(['status' => Banner::STATUS_ACTIVE])
                ->andFilterWhere(['bc.course_id' => $this->course_id])
                ->orderBy('id DESC')
                ->one();
        }
        if ($popup == null) {
            return;
        }

        $expire_time = 86400;

        if($popup->type == Banner::TYPE_POPUP_LEARNING)
        {
            $expire_time = $expire_time * 3;
        }

        $cookies = Yii::$app->request->cookies;

        $key = 'popup-' . $popup->id . '-' . $date;
        $popupCookie = $cookies->getValue($key);
        if ($popupCookie != null) {
            return;
        }

        // if user have not seen popup today then register pop
        Yii::$app->response->cookies->add(new Cookie([
            'name' => $key,
            'value' => $popup->id,
            'expire' => time() + $expire_time
        ]));

        $cdnUrl = Yii::$app->controller->cdnUrl;
        $this->view->registerCssFile($cdnUrl . '/css/jquery.fancybox.css?v=3');
        $this->view->registerJsFile($cdnUrl . '/js/fancybox/jquery.fancybox.js');

        return $this->render('popup', [
            'popup' => $popup,
            'cdnUrl' => $cdnUrl
        ]);
    }

}