<?php


namespace frontend\widgets;

use yii\base\Widget;
use kyna\settings\models\SeoSetting;

class AnchorText extends Widget
{
    public function run()
    {
        $anchorText = SeoSetting::find()->andWhere([
            'key' => 'seo_anchor_text',
        ])->one();

        if (empty($anchorText) || empty($anchorText->value)) {
            return false;
        }

        return $this->render('anchor_text', ['anchorText' => $anchorText]);
    }

}