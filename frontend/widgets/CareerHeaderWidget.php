<?php

namespace frontend\widgets;

use yii;
use common\widgets\base\BaseWidget;

class CareerHeaderWidget extends BaseWidget
{

    public $rootCats;

    public function run()
    {
        return $this->getHeader();
    }

    public function getHeader()
    {
        return $this->render('career-header', []);
    }

}
