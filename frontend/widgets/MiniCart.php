<?php

namespace app\widgets;

use Yii;
use yii\base\Widget;
use yii\data\ArrayDataProvider;
use app\models\Category;

class MiniCart extends Widget {
    private $_cart;

    public function init() {
        parent::init();
        $this->_cart = Yii::$app->cart;
    }

    public function run() {
        $dataProvider = new ArrayDataProvider([
            'allModels' => $this->_cart->positions,
        ]);
        return $this->render('minicart', [
            'dataProvider' => $dataProvider,
            'cartTotal' => $this->_cart->cost,
            'itemCount' => $this->_cart->count,
        ]);
    }

}
