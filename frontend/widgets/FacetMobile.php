<?php

namespace frontend\widgets;

use Yii;
use yii\base\Widget;

class FacetMobile extends Widget
{
    public function run()
    {
        if(!empty(Yii::$app->controller->elasticFacets)) {
            $facets = Yii::$app->controller->elasticFacets;
            return $this->render('facet-mobile', ['facets' => $facets]);
        }
    }

}