<?php
/**
 * Created by PhpStorm.
 * User: macintosh
 * Date: 5/11/17
 * Time: 4:42 PM
 */

namespace frontend\widgets;

use common\helpers\ArrayHelper;
use yii\base\Widget;
use kyna\settings\models\Banner;
use Yii;

class BannerWidget extends Widget
{

    /**
     * The type of banner, the values are constants of class kyna\settings\models\Banner
     * @var string $type
     */
    public $type;

    /**
     * The count of banners will be return
     * @var int $limit
     */
    public $limit = 1;

    /**
     * The offset when get the banner, default value is 0 means get the first row of list banners return
     * @var int $offset
     */
    public $offset = 0;

    /**
     * Id of course category in case the type is Campaign Category
     * @var integer $category_id
     */
    public $category_id;

    /**
     * The html class attribute of div banner
     * @var string
     */
    public $containerClass = 'banner';

    /**
     * The html id attribute of div banner, default value is null
     * @var $id string
     */
    public $id;

    /**
     * Render banner view
     * @return bool|string
     */

    public $course_id;

    public function run()
    {
        $date = date('Y-m-d');

        $banners = Banner::find()->where([
            'type' => $this->type
        ])
            ->andWhere("from_date is null or (from_date is not null and from_date <= '{$date}')")
            ->andWhere("to_date is null or (to_date is not null and to_date >= '{$date}')")
            ->andWhere(['status' => Banner::STATUS_ACTIVE])
            ->andFilterWhere(['category_id' => $this->category_id])
            ->offset($this->offset)
            ->limit($this->limit)
            ->orderBy('id DESC')
            ->all();

        if (empty($banners)) {
            return false;
        }
        if ($this->type == Banner::TYPE_TOP_MY_COURSE_BOTTOM) {
            foreach ($banners as $banner){
                if (!empty($this->course_id)) {
                    $bannerCourses = $banner->bannerCourses;
                    if (!empty($bannerCourses)) {
                        $courseIds = ArrayHelper::getColumn($bannerCourses, 'course_id');
                        if (in_array($this->course_id, $courseIds)) {
                            return $this->render('banner_course_bottom', ['banner' => $banner, 'course_id' => $this->course_id]);
                        }
                    }
                }
            }
            return false;
        }
        return $this->render('banner', ['banners' => $banners]);
    }


}