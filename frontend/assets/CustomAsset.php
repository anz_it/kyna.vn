<?php
/**
 * @link http://www.yiiframework.com/
 *
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use common\assets\FrontendAsset;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 *
 * @since 2.0
 */
class CustomAsset extends FrontendAsset
{
    public $css = [
        'css' => 'css/main.min.css',
    ];
    public $cssOptions = [
        'type' => 'text/css'
    ];
    public $js = [
        "src/js/offpage.js?version=1536292805",
        "src/js/main.js?version=56456456",
        'src/js/add-to-cart.js?v=1536292805',
        'src/js/course-pop-up.js',
        'js/ajax-caller.js',
        "js/user-info.js",
        "js/modal.js",
        "js/login.js",
    ];
    public $depends = [
        'app\assets\AppAsset',
    ];
}
