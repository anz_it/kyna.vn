<?php

namespace frontend\assets;

use common\assets\FrontendAsset;
use \yii\web\View;


class MaintainAsset extends FrontendAsset
{
    public $css = [
        // FontAwesome
        "css/main.min.css",
        "css/font-awesome.min.css",
    ];
    public $cssOptions = [
        'type' => 'text/css'
    ];
    public $js = [
        // select 2

        ["src/js/offpage.js?version=1520491591", 'position' => View::POS_END],
        // push-notification
        ["js/push-notification/firebase.js?v=4.1.3", "position" => View::POS_END],
        ["js/push-notification/push-notification-main.js?v=15028795721111", "position" => View::POS_END],
        //slick

    ];
    public $depends = [
        'yii\web\YiiAsset',
        'app\modules\course\assets\BootboxAsset',
//        'yii\bootstrap\BootstrapAsset',
    ];
    public $jsOptions = ['position' => View::POS_HEAD];
}
