<?php

namespace frontend\assets;

use common\assets\FrontendAsset;
use \yii\web\View;

/**
 * This is class asset bunle for `home` layout
 */
class HomeAsset extends FrontendAsset
{
    public $css = [
        // FontAwesome
        "css/main.min.css",
        "css/font-awesome.min.css",
        // Style main
        "css/owl.carousel.css",
        "css/owl.theme.css",
        "css/owl.transitions.css",
        "css/select2.min.css",

//        "js/slick/slick-theme.css",
    ];
    public $cssOptions = [
        'type' => 'text/css'
    ];
    public $js = [
        // select 2
        ["src/js/select2.min.js", "position" => View::POS_END],

        ["src/js/tether.min.js", 'position' => View::POS_END],
        ["src/js/bootstrap.min.js", 'position' => View::POS_END],
        ["src/js/owl.carousel.min.js", 'position' => View::POS_END],
        ["src/js/offpage.js?version=1536292805", 'position' => View::POS_END],
        ["src/js/main.js?version=56456456", 'position' => View::POS_END],
        ["src/js/details.js?v=1536292805", 'position' => View::POS_END],
        // ajax
        ["src/js/ajax-caller.js?v=1536292805", 'position' => View::POS_END],

        // push-notification
        ["js/push-notification/firebase.js?v=4.1.3", "position" => View::POS_END],
        ["js/push-notification/push-notification-main.js?v=15028795721111", "position" => View::POS_END],
        //slick
        ["js/slick/slick.min.js", "position" => View::POS_END],
        // jquery validate
        ["src/js/jquery.validate.min.js", "position" => View::POS_END],
        ["src/js/masonry.pkgd.min.js", "position" => View::POS_END],
        ["js/countdown/countdown.js?v=2.2", "position" => View::POS_HEAD],
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'app\modules\course\assets\BootboxAsset',
//        'yii\bootstrap\BootstrapAsset',
    ];
    public $jsOptions = ['position' => View::POS_HEAD];
}
