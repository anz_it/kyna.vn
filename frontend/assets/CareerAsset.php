<?php

namespace frontend\assets;

use common\assets\FrontendAsset;
use \yii\web\View;

/**
 * This is class asset bunle for `career` layout
 */
class CareerAsset extends FrontendAsset
{

    public function __construct()
    {
        parent::__construct();
        $this->basePath .= '/career';
        $this->baseUrl .= '/career';
    }

    public $css = [
        "../css/main.min.css",
        'css/bootstrap.css',
        'css/font-awesome.min.css',
        'css/style.css?v=6',
        'css/owl.carousel.css',
        'css/owl.theme.css',
        'css/owl.transitions.css',
        'css/icon-career.css',

    ];
    public $js = [
        'js/jquery-1.11.2.min.js',
        'js/bootstrap.min.js',
        'js/owl.carousel.min.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
    ];
    public $jsOptions = ['position' => View::POS_HEAD];
}
