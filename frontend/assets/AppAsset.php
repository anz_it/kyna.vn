<?php
/**
 * @link http://www.yiiframework.com/
 *
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace frontend\assets;

use common\assets\FrontendAsset;
use yii\web\View;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 *
 * @since 2.0
 */
class AppAsset extends FrontendAsset
{
    public $css = [
        // FontAwesome
        'css/main.min.css',
        'css/font-awesome.min.css',
        // Style main
        'css/style.css',
        'css/media.css',
        'css/owl.carousel.css',
        'css/owl.theme.css',
        'css/owl.transitions.css',
        // Style Menu Mobile
        'css/jquery.sidr.dark.css',
        // custom
        'css/custom.css',

        //payoo
        'css/payoo/jquery.fancybox.css'

//        "js/slick/slick-theme.css",
    ];
    public $cssOptions = [
        'type' => 'text/css'
    ];
    public $js = [
        ["src/js/tether.min.js", 'position' => View::POS_END],
        ["src/js/bootstrap.min.js", 'position' => View::POS_END],
        ["src/js/owl.carousel.min.js", 'position' => View::POS_END],
        ['src/js/iscroll.js', 'position' => View::POS_END],
        ["src/js/main.js?version=56456456", 'position' => View::POS_END],
        ["src/js/details.js?v=1536292805", 'position' => View::POS_END],
        // ajax
        ["src/js/ajax-caller.js?v=1536292805", 'position' => View::POS_END],
        ["src/js/js_cookie.js", 'position' => View::POS_END],
        ['src/js/courses.js?v=153629280513', 'position' => View::POS_END],
        ["src/js/offpage.js?version=1536292805", 'position' => View::POS_END],
        ['js/script-main.js', 'position' => View::POS_END],
        //slick
        ["js/slick/slick.min.js", "position" => View::POS_END],

        //payoo
        ["js/payoo/jquery.fancybox.pack.js", "position" => View::POS_END],
        ["js/countdown/countdown.js?v=2.2", "position" => View::POS_HEAD],
    ];
    public $depends = [
        'yii\web\YiiAsset',
    ];
    public $jsOptions = ['position' => View::POS_HEAD];
}
