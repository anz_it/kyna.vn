<?php

namespace app\components;

use common\helpers\CDNHelper;
use kyna\settings\models\Setting;
use yii\base\NotSupportedException;
use yii\helpers\ArrayHelper;
use yii\base\Component;
use yii\db\BaseActiveRecord;
use Yii;

class Settings extends Component implements \ArrayAccess
{
    private $_settings;

    public function init() {
        parent::init();
        $this->_settings = ArrayHelper::map(Setting::find()->all(), 'key', 'value');
    }

    public function keyExists($key) {
        $allowedKeys = array_keys($this->_settings);
        return in_array($key, $allowedKeys);
    }

    public function __get($key) {
        if ($this->keyExists($key)) {
            return $this->_settings[$key];
        }

        return parent::__get($key);
    }

    public function getBodyScript()
    {

        if (isset(Yii::$app->settings['body-script'])) {

            $userId = Yii::$app->user->id;
            $userCode = "";
            if (!empty($userId))
                $userCode = " _paq.push(['setUserId', '{$userId}']);";

            $piwikId = Yii::$app->piwik->piwikId;
            $script = str_replace('USER_ID', $userId, Yii::$app->settings['body-script']);
            $script = str_replace("//NATIVE_CODE//",
                "
                {$userCode}
                _paq.push(['setCustomVariable',1, 'piwikId','{$piwikId}', 'visit' ]);",
                $script
            );
            $cdnUrl = CDNHelper::getMediaLink();
            $script = str_replace("%MEDIA_LINK%",$cdnUrl,$script);
            $script = str_replace("%CURRENT_USERID%",$userId ,$script);
            return $script;
        }
        return "";
    }

    /**
     * Whether a offset exists
     * @link http://php.net/manual/en/arrayaccess.offsetexists.php
     * @param mixed $offset <p>
     * An offset to check for.
     * </p>
     * @return boolean true on success or false on failure.
     * </p>
     * <p>
     * The return value will be casted to boolean if non-boolean was returned.
     * @since 5.0.0
     */
    public function offsetExists($offset)
    {
        return isset($this->_settings[$offset]);
    }

    /**
     * Offset to retrieve
     * @link http://php.net/manual/en/arrayaccess.offsetget.php
     * @param mixed $offset <p>
     * The offset to retrieve.
     * </p>
     * @return mixed Can return all value types.
     * @since 5.0.0
     */
    public function offsetGet($offset)
    {
        return $this->_settings[$offset];
    }

    /**
     * Offset to set
     * @link http://php.net/manual/en/arrayaccess.offsetset.php
     * @param mixed $offset <p>
     * The offset to assign the value to.
     * </p>
     * @param mixed $value <p>
     * The value to set.
     * </p>
     * @return void
     * @since 5.0.0
     * @throws  NotSupportedException
     */
    public function offsetSet($offset, $value)
    {
        throw new NotSupportedException("Not supported");
    }

    /**
     * Offset to unset
     * @link http://php.net/manual/en/arrayaccess.offsetunset.php
     * @param mixed $offset <p>
     * The offset to unset.
     * </p>
     * @return void
     * @since 5.0.0
     * @throws NotSupportedException
     */
    public function offsetUnset($offset)
    {
        throw new NotSupportedException();
    }
}
