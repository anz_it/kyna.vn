<?php
/**
 * Created by IntelliJ IDEA.
 * User: trangnguyen
 * Date: 3/27/2018
 * Time: 11:16 AM
 */
namespace app\components;

use Yii;
use yii\captcha\CaptchaAction;

class TextCaptchaAction extends CaptchaAction
{
    public $minLength = 5;
    public $maxLength = 5;
    public $transparent= true;
    public $foreColor = 0x000000;
    public $padding = 3;
    public $offset = 0;

    public function validate($input, $caseSensitive)
    {
        if(Yii::$app->request->isAjax == false) {
            $this->getVerifyCode(true);
        }
        return parent::validate($input, $caseSensitive);
    }
}