<?php
namespace app\components\htmlmeta;

use Yii;
use yii\helpers\Url;
use app\models\Course;
use app\models\Category;
use kyna\course\models\Teacher;
use kyna\course_combo\models\CourseCombo;
use kyna\settings\models\SeoSetting;
use kyna\tag\models\Tag;

class HtmlMeta extends BaseMetaData
{
    public $robots = [
        'index' => 'index',
        'follow' => 'follow'
    ];

    public function register()
    {
        if (isset($this->title)) {
            $this->view->title = $this->title;
        }
        if (isset($this->description)) {
            $this->view->registerMetaTag([
                'name' => 'description',
                'content' => $this->description,
            ]);
        }
        if (!empty ($this->keyword)) {
            $this->view->registerMetaTag([
                'name' => 'keywords',
                'content' => $this->keyword
            ]);
        }

        $siteSetting = Yii::$app->settings;
        if ($siteSetting->keyExists('meta_robots_noindex') and ($siteSetting->meta_robots_noindex == true)) {
            $this->robots['index'] = 'noindex';
        }
        if ($siteSetting->keyExists('meta_robots_nofollow') and ($siteSetting->meta_robots_nofollow == true)) {
            $this->robots['follow'] = 'nofollow';
        }
        $currentUrl = Yii::$app->request->getUrl();
        $noIndexUrls = Yii::$app->params['no-index-urls'];
        if(!empty($noIndexUrls)){
            if(in_array($currentUrl, $noIndexUrls)){
                $this->robots['index'] = 'noindex';
                $this->robots['follow'] = 'nofollow';
            }
        }

        if (!empty($this->robots)) {
            $this->view->registerMetaTag([
                'name' => 'robots',
                'content' => implode(',', $this->robots),
            ]);
        }

        if (isset($this->url)) {
            $this->view->registerLinkTag([
                'rel' => 'canonical',
                'href' => $this->url,
            ]);
        }
    }

    public function setData($object)
    {

        if ($object instanceof Course) {
            $this->_fromCourse($object);
            return;
        }

        if ($object instanceof Category) {
            $this->_fromCategory($object);
            return;
        }

        if ($object instanceof Tag) {
            $this->_fromTag($object);
            return;
        }

        if ($object instanceof Teacher) {
            $this->_fromTeacher($object);
            return;
        }

        if ($object == 'combo') {
            $this->_fromCombo($object);
        }

        if ($object == 'category_all') {
            $this->_fromCategoryAll($object);
        }

        if ($object == 'combo') {
            $this->_fromCombo($object);
        }



        $settingClass = Yii::$app->settings->className();
        if ($object instanceof $settingClass) {
            $this->_fromHome($object);
            return;
        }
    }

    private function _fromHome($siteSetting)
    {
        $this->type = 'website';
        if ($siteSetting->keyExists('meta_title')) {
            $this->title = $siteSetting->meta_title;
        } elseif ($siteSetting->keyExists('site_name')) {
            $this->title = $siteSetting->site_name;
        }

        if ($siteSetting->keyExists('meta_description')) {
            $this->description = $siteSetting->meta_description;
        }

        if ($siteSetting->keyExists('meta_robots_noindex') and ($siteSetting->meta_robots_noindex == true)) {
            $this->robots['index'] = 'noindex';
        }
        if ($siteSetting->keyExists('meta_robots_nofollow') and ($siteSetting->meta_robots_nofollow == true)) {
            $this->robots['follow'] = 'nofollow';
        }

        if ($siteSetting->keyExists('meta_canonical')) {
            $this->url = $siteSetting->meta_canonical;
        } else {
            $this->url = Url::toRoute(['/'], $this->scheme);
        }
    }

    /**
     * @param $course Course
     */
    private function _fromCourse($course)
    {
        list($title, $description, $keyword) = $course->getSeoMeta();
        $this->title = $title;
        $this->description = $description;
        if (!empty ($keyword))
            $this->keyword = $keyword;


        if (isset($course->meta_robots_noindex) and ($course->meta_robots_noindex == true)) {
            $this->robots['index'] = 'noindex';
        }
        if (isset($course->meta_robots_nofollow) and ($course->meta_robots_nofollow == true)) {
            $this->robots['index'] = 'nofollow';
        }

        if (isset($course->meta_canonical)) {
            $this->url = $course->meta_canonical;
        } else {
            //$this->url = Url::toRoute(['/course/default/view', 'id' => $course->id], 'http');
            $this->url = $course->getUrl(false, $this->scheme);
        }
    }

    private function _fromCategory($category)
    {
        if (!empty($category->title)) {
            $this->title = $category->title;
        } else {
            $this->title = $category->name;
        }

        if (isset($category->meta_description)) {
            $this->description = $category->meta_description;
        } else {
            $this->description = $category->description;
        }

        if (isset($category->meta_canonical)) {
            $this->url = $category->meta_canonical;
        } else {
            $this->url = $category->getUrl(false, $this->scheme);
        }
        if(!empty(Yii::$app->request->get()['facets']))
        {
            $this->robots = [
                'index' => 'noindex',
                'follow' => 'nofollow'
            ];
        }
        else if(Yii::$app->request->isGet && !empty(Yii::$app->request->get()['page']) && Yii::$app->request->get()['page'] > 1) {
            $this->robots = [
                'index' => 'noindex',
                'follow' => 'follow'
            ];
        }

    }

    private function _fromCombo()
    {
        $this->title = 'Khuyến mãi Nhóm Khóa Học Combo, Khóa Học Online';

        $this->description = 'Tổng hợp nhóm Khóa Học Combo, Khóa Học Online miễn phí với chuyên gia trong nhiều lĩnh vực. Học tại Kyna.vn giảm đến 40% học phí, thanh toán nhanh chóng. Học Ngay!';

        $this->url = 'https://kyna.vn/khuyen-mai/nhom-khoa-hoc';

        $this->robots = [
            'index' => 'index',
            'follow' => 'follow'
        ];

        if(!empty(Yii::$app->request->get()['facets']))
        {
            $this->robots = [
                'index' => 'noindex',
                'follow' => 'nofollow'
            ];
        }
        else if(Yii::$app->request->isGet && !empty(Yii::$app->request->get()['page']) && Yii::$app->request->get()['page'] > 1) {
            $this->robots = [
                'index' => 'noindex',
                'follow' => 'follow'
            ];
        }
    }

    private function _fromCategoryAll()
    {
        $this->title = 'Tổng hợp Tất Cả Khóa Học Online mới nhất tại Kyna';

        $this->description = 'Tổng hợp Tất Cả Khóa Học Online mới nhất với hàng trăm khóa học về nhiều lĩnh vực khác nhau, với chuyên gia giảng dạy hàng đầu trong lĩnh vực. Đăng Kí Ngay!';

        $this->url = 'https://kyna.vn/danh-sach-khoa-hoc';

        $this->robots = [
            'index' => 'noindex',
            'follow' => 'nofollow'
        ];

        
    }

    /**
     * @param Tag $tagModel
     */
    private function _fromTag($tagModel)
    {


        list($title, $description, $keyword, $robots, $canonical) = $tagModel->getSeoMeta();

        if (!empty ($title))
            $this->title = $title;

        if (!empty ($description))
            $this->description = $description;

        if (!empty ($keyword))
            $this->keyword = $keyword;

        // default value for robots, just apply for tags
        $this->robots = [
            'index' => 'noindex',
            'follow' => 'follow'
        ];
        if (!empty ($robots))
            $this->robots = $robots;

        if (!empty ($canonical))
            $this->url = $canonical;
        if(!empty(Yii::$app->request->get()['facets']))
        {
            $this->robots = [
                'index' => 'noindex',
                'follow' => 'nofollow'
            ];
        }
        else if(Yii::$app->request->isGet && !empty(Yii::$app->request->get()['page']) && Yii::$app->request->get()['page'] > 1) {
            $this->robots = [
                'index' => 'noindex',
                'follow' => 'follow'
            ];
        }



    }

    /**
     * @param Teacher $teacher
     */
    private function _fromTeacher($teacher)
    {
        list($title, $description, $keyword, $robots, $canonical) = $teacher->getSeoMeta();

        if (!empty ($title))
            $this->title = $title;

        if (!empty ($description))
            $this->description = $description;

        if (!empty($keyword))
            $this->keyword = $keyword;

        $this->robots = [
            'index' => 'index',
            'follow' => 'follow'
        ];
        if (!empty($robots['index']))
            $this->robots['index'] = $robots['index'];
        if (!empty($robots['follow']))
            $this->robots['follow'] = $robots['follow'];

        $this->url = (!empty($canonical)) ? $canonical : Yii::$app->request->hostInfo . '/' . Yii::$app->request->pathInfo;
    }
}
