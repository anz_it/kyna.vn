{ActiveForm assign='form' id='facet-filter-form' action="{$action}" method='get'}
<section>
    <form id="facet-filter-form" action="/danh-sach-khoa-hoc">
       <div class="k-listing-characteristics">
          <h3>Tìm theo đặc điểm khóa học</h3>
          <ul class="k-listing-characteristics-list">
             <li class="checkbox">
                <input id="checkbox1" type="checkbox" name="type" value="1">
                <label for="checkbox1"><span><span></span></span>Khóa học combo</label>                                
                <span class="tag">100</span>
             </li>
             <li class="checkbox">
                <input id="checkbox2" type="checkbox" name="checkbox" value="2">     
                <label for="checkbox2"><span><span></span></span>Đang khuyến mãi</label>
                <span class="tag">100</span>
             </li>
             <li class="checkbox">
                <input id="checkbox3" type="checkbox" name="checkbox" value="3">
                <label for="checkbox3"><span><span></span></span>Miễn phí</label>
                <span class="tag">100</span>
             </li>
          </ul>
       </div>
       <!--end .k-listing-characteristics-->
        {if !empty($refiners['total_time'])}
        <div class="k-listing-time">
          <h3>Tìm theo thời lượng</h3>
          <ul class="k-listing-time-list">
            {foreach from=$refiners['total_time'] item=$item}
                {if !empty($item.active)}
                <li class="checkbox">
                   <input id="facet-course-time-range-{$item.id}" type="checkbox" name="total_time[{$item.id}]" value="{$item.id}" {if !empty($get['total_time']) && in_array($item.id, $get['total_time'])}checked="checked"{/if}>
                   <label for="facet-course-time-range-{$item.id}"><span><span></span></span>{$item.title}</label>
                   <span class="tag">{$item.active}</span>
                </li>
                {/if}
            {/foreach}
          </ul>
        </div>
        <!--end .k-listing-time-->
        {/if}
        
        {if !empty($refiners['level'])}
        <div class="k-listing-level">
           <h3>Tìm theo trình độ yêu cầu</h3>
           <ul class="k-listing-level-list">
                {foreach from=$refiners['level'] item=$item}
                {if !empty($item.active)}
                <li class="checkbox">
                   <input id="facet-course-level-{$item.id}" type="checkbox" name="level[{$item.id}]" value="{$item.id}" {if !empty($get['level']) && in_array($item.id, $get['level'])}checked="checked"{/if}>
                   <label for="facet-course-level-{$item.id}"><span><span></span></span>{$listLevel[$item.id]}</label>
                   <span class="tag">{$item.active}</span>
                </li>
                {/if}
                {/foreach}                                        
           </ul>
        </div>
        <!--end .k-listing-level-->
        {/if}

       <div class="k-listing-hot-topics">
          <h3>Chủ đề đang hot</h3>
          <ul class="k-listing-hot-topics-list">
             <li><a href="#">Facebook Marketing</a></li>
             <li><a href="#">Tiếng hoa</a></li>
             <li><a href="#">Tiếng nhật</a></li>
             <li><a href="#">Seo</a></li>
          </ul>
       </div>
       <!--end .k-listing-hot-topics-->
        {if !empty(Yii::$app->request->get('q'))}
            {$q = Yii::$app->request->get('q')}
            <input type="hidden" name="q" value="{$q}"/>            
        {/if}
        
       <div class="k-listing-save">
           <button type="button" class="btn">Cập nhật</button>
       </div>
       <!--end .k-listing-save-->

    </form> 
</section>
{/ActiveForm}

{use class="yii\web\View"}
{$script = "
            $('#facet-filter-form input[type=\'checkbox\']').on('change', function() {
                $('#facet-filter-form').submit();
            });
"}
        
{$this->registerJs($script, View::POS_END, 'my-options')|void}