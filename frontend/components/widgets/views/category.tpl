<section>
    <div class="k-listing-category">
        {if empty($catModel)}
        <h3>Tìm theo danh mục</h3>
        {else}
            <h3>{$catModel.name}</h3>
        {/if}
        <ul class="k-category-list pd0">
            {if empty($catId)}
                <li>
                    {use class="\yii\helpers\Url"}
                    <a href="{Url::toRoute(['/course/default/index'])}">
                        <i class="icon icon-archive"></i>Tất cả khóa học
                    </a>                    
                </li>
                <li>
                    <a href="{Url::toRoute(['/course/combo/index'])}">
                        <i class="icon icon-archive"></i>Khóa học Combo
                    </a>                    
                </li>
            {/if} 
            {foreach from=$rootCats item=$rootCat}
                <li>
                    <a href="{$rootCat->url}">
                        <i class="icon icon-archive"></i>{$rootCat.name}
                    </a>
                    {if !empty($rootCat.children)}
                    <span class="float-right"><i class="icon-arrow-right"></i></span>
                    {/if}
                </li>
            {/foreach}           
         </ul>
    </div><!--end k-category-->
</section>