<?php
/**
 * Created by PhpStorm.
 * User: truongn
 * Date: 5/29/18
 * Time: 3:35 PM
 */

namespace app\components;


use common\components\GoogleStackDriverLog;
use yii\web\ErrorAction;
use yii\web\HttpException;

class WebErrorHandler extends ErrorAction
{
    public function run()
    {

        if (($exception = \Yii::$app->getErrorHandler()->exception) === null) {
            // action has been invoked not from error handler, but by direct route, so we display '404 Not Found'
            $exception = new HttpException(404, Yii::t('yii', 'Page not found.'));
        }

        if ($exception instanceof HttpException) {
            $code = $exception->statusCode;

        } else {
            //$code = $exception->getCode();
            $code = 500;
        }
        if($code < 500 )
        {
            $this->view = "404";
            return parent::run();
        }
        if ($exception instanceof Exception) {
            $name = $exception->getName();
        } else {
            $name = $this->defaultName ?: \Yii::t('yii', 'Error');
        }
        if ($code) {
            $name .= " (#$code)";
        }
        if ($exception instanceof UserException) {
            $message = $exception->getMessage();
        } else {
            $message = $this->defaultMessage ?: \Yii::t('yii', 'An internal server error occurred.');
        }
        $this->view = "503";
        \Yii::$app->logger->log($exception->getMessage(), GoogleStackDriverLog::LEVEL_CRITICAL, $exception->getTraceAsString());

        if (\Yii::$app->getRequest()->getIsAjax()) {
            return "$name: $message";
        } else {
            return $this->controller->render($this->view ?: $this->id, [
                'name' => $name,
                'message' => $message,
                'exception' => $exception,
            ]);
        }
    }

}