<?php

namespace app\components;

use yii\db\Expression;
use pahanini\refiner\db\Range;

/* 
 * Class MultiRange
 */
class KRefinerMultiRange extends \pahanini\refiner\common\Base
{
    
    public $paramToArray = true;
    public $paramType = 'int';
    public $ranges = [];
    
    public function init()
    {
        $ret = parent::init();
        
        if (!$this->refine) {
            $this->refine = [$this, 'refine'];
        }
        
        return $ret;
    }
    
    public function applyTo($query)
    {
        if (is_callable($this->refine) && ($params = $this->getParams())) {
            call_user_func($this->refine, $query, $params);
        }
        return $this;
    }
    
    public function query($query, $name, $min = null, $max = null)
    {
        return $query
            ->select(["COUNT(*) as $name"])
            ->andWhere(new Expression("$this->columnName IS NOT NULL"))
            ->andWhere(new Expression("$this->columnName >= $min"))
            ->andWhere(new Expression("$this->columnName < $max"));
    }
    
    public function getValues()
    {
        $result = [];
        $query = $this->set->getBaseQueryOrigin();
        if ($this->valueFilter) {
            $query->andWhere($this->valueFilter);
        }
        foreach ($this->ranges as $range) {
            $tmp = $this->query(clone $query, "`all`", $range['min'], $range['max']);
            
            $all = $tmp->asArray()->all();
            
            $active = [];
            $tmp = $this->query(clone $query, "active", $range['min'], $range['max']);
            foreach ($this->set->getRefiners() as $refiner) {
                if ($refiner === $this) {
                    continue;
                }
                $refiner->applyTo($tmp);
            }
            $active = $tmp->asArray()->all();
            
            $result[] = [
                'id' => $range['id'],
                'title' => $range['title'],
                'all' => $all[0]['all'],
                'active' => $active[0]['active']
            ];
        }
        return $result;
    }
    
    public function refine($query, $params)
    {
        $conditions = [];
        $pars = [];
        foreach ($params as $param) {
            if (isset($this->ranges[$param])) {
                $filter = $this->ranges[$param];

                $conditions[] = "($this->columnName >= :{$param}Min AND $this->columnName < :{$param}Max)";

                $pars[":{$param}Min"] = $filter['min'];
                $pars[":{$param}Max"] = $filter['max'];
            }
        }

        if (!empty($conditions)) {
            $query->andWhere(implode(' OR ', $conditions), $pars);
        }
        
        return $query;
    }
}
