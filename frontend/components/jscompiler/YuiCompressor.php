<?php

namespace app\components\jscompiler;

use Yii;
use yii\base\Component;
use app\components\View;

class YuiCompressor extends JsCompiler {
    public $java = '/usr/bin/java';
    public $compiler = '@root/yuicompressor.jar';
    public $suffix = '.yui';

    protected function getCommand() {
        return 'cat '.$this->inputString.' | '.
            $this->java.' -jar '.Yii::getAlias($this->compiler).
            ' --type js'.
            ' --charset utf8'.
            ' -o '.$this->outputFilename.
            ' 2>&1';
    }
}
