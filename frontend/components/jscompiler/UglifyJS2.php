<?php

namespace app\components\jscompiler;

use Yii;
use yii\base\Component;
use app\components\View;

class UglifyJS2 extends JsCompiler {
    public $compiler = 'uglifyjs';

    public $suffix = '.uglify';

    protected function getCommand() {
        return $this->compiler.
            ' '.$this->inputString.
            ' -c -m'.
            ' -o '.$this->outputFilename.
            ' 2>&1';
    }
}
