<?php

namespace app\components;

use common\helpers\RequestCookieHelper;
use kyna\order\models\Order;
use kyna\order\models\traits\OrderTrait;
use Yii;
use yii\web\User as BaseWebUser;
use yii\web\Cookie;

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class WebUser extends BaseWebUser
{
    
    public function init()
    {
        $ret = parent::init();
        
        $this->on(self::EVENT_AFTER_LOGIN, [$this, 'onAfterLogin']);
        $this->on(self::EVENT_AFTER_LOGOUT, [$this, 'onAfterLogout']);

        return $ret;
    }
    /**
     * @desc override switchIdentity function to ignore changing session_id after login
     * @param type $identity
     * @param type $duration
     * @return type
     */
    public function switchIdentity($identity, $duration = 0)
    {
        $this->setIdentity($identity);

        if (!$this->enableSession) {
            return;
        }

        $session = Yii::$app->getSession();
        $session->remove($this->idParam);
        $session->remove($this->authTimeoutParam);

        if ($identity) {
            $session->set($this->idParam, $identity->getId());
            if ($this->authTimeout !== null) {
                $session->set($this->authTimeoutParam, time() + $this->authTimeout);
            }
            if ($this->absoluteAuthTimeout !== null) {
                $session->set($this->absoluteAuthTimeoutParam, time() + $this->absoluteAuthTimeout);
            }
            if ($duration > 0 && $this->enableAutoLogin) {
                $this->sendIdentityCookie($identity, $duration);
            }
        } elseif ($this->enableAutoLogin) {
            Yii::$app->getResponse()->getCookies()->remove(new Cookie($this->identityCookie));
        }
    }
    
    public function onAfterLogin($event)
    {
        $user = $event->identity;
       
        $oldLoginToken = $user->loginToken;
        if (!is_null($oldLoginToken)) {
            Yii::$app->session->destroySession($oldLoginToken->code);
        }

        $user->createLoginToken();

        if (!empty($user->loginToken) && $user->loginToken->code != Yii::$app->session->id) {
            $user->loginToken->code = Yii::$app->session->id;
            $user->loginToken->save();
        }

        // active code if has session
        if (Yii::$app->session->has('activation_code')) {
            $order = Order::findOne(['activation_code' => Yii::$app->session->get('activation_code')]);
            if ($order && $order->user_id == $user->id) {
                OrderTrait::activate($order->id);
                Yii::$app->session->remove('activation_code');
                Yii::$app->user->setReturnUrl(['/user/course/index']);
                return false;
            }
        }
    }
    
    public function onAfterLogout($event)
    {
        $user = $event->identity;
        $oldLoginToken = $user->loginToken;
        if (!is_null($oldLoginToken)) {
            $oldLoginToken->delete();
        }
        // remove request cookie
//        RequestCookieHelper::removeCookie($user->id);
    }

}
