<?php

namespace app\modules\page\controllers;

use common\campaign\CampaignTet;
use kyna\course\models\Course;
use kyna\base\Mailer;
use kyna\page\models\Page;
use kyna\payment\events\TransactionEvent;
use kyna\order\models\Order;
use kyna\payment\models\PaymentMethod;

use kyna\promotion\models\UserVoucherFreeRef;
use kyna\user\models\Profile;
use dektrium\user\models\Token;
use kyna\user\models\UserCourse;
use Yii;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;

use app\components\Controller;
use app\modules\cart\models\form\TopUpForm;
use kyna\user\models\User;
/* 
 * Class ApiController to handle all ipn requests.
 */
class ApiController extends Controller
{
    public function init()
    {
        $ret = parent::init();

        TransactionEvent::on(PaymentMethod::className(), PaymentMethod::EVENT_PAYMENT_SUCCESS, [$this, 'completeOrder']);

        return $ret;
    }

    /**
     * @desc action for RedirectUrl for Landing Page
     * @param $method
     * @throws NotFoundHttpException
     */
    public function actionResponse($method)
    {
        $paymentMethod = PaymentMethod::loadModel($method);
        $get = Yii::$app->request->get();
        $result = $paymentMethod->ipn($get);
        if (empty($result['error'])) {
            $order = Order::findOne($result['orderId']);
            $page = $order ? Page::findOne($order->page_id) : null;
            if ($order && $page) {
                // redirect to LP with status success
                if(CampaignTet::InTimesCampaign()){
                    $tagParam = Yii::$app->session->getFlash('tag-param', '');
                    $cateParam = Yii::$app->session->getFlash('cate-param', '');
                    $this->redirect(['/page/default/index', 'slug' => $page->slug,'tag'=> $tagParam,'cate'=>$cateParam,
                        'payment_status' => true,'payment_method'=>$order->payment_method]);

                }else{
                    $this->redirect(['/page/default/index', 'slug' => $page->slug, 'payment_status' => true,'payment_method'=>$order->payment_method]);
                }
            } else {
                throw new NotFoundHttpException('Order or Page not found');
            }
        } else {
            if (!empty($get['vpc_OrderInfo'])) {
                Order::makeFailedPayment($get['vpc_OrderInfo']);
                $order = Order::findOne($get['vpc_OrderInfo']);
                $page = $order ? Page::findOne($order->page_id) : null;
                if ($order && $page) {
                    // redirect to LP with status fail
                    $this->redirect(['/page/default/index', 'slug' => $page->slug, 'payment_status' => false,'payment_method'=>$order->payment_method]);
                } else {
                    throw new NotFoundHttpException('Order or Page not found');
                }
            } else {
                throw new NotFoundHttpException('Payment failure');
            }
        }
    }

    /**
     * @param $transactionEvent
     */
    public function completeOrder($transactionEvent)
    {
        $transData = $transactionEvent->transData;
        Order::complete($transData['orderId'], $transData['transactionCode']);
    }

    public function actionAddUserCourse(){
        $post = Yii::$app->request->post();
        $user = User::findOne(['email'=>$post['email']]);
        $dataNotification['error'] = 0;
        $isAddNewUser =false;
        if(empty($user)){
            $isAddNewUser = true;

            $user = new User();
            $user->username = $post['email'];
            $user->email = $post['email'];
            $user->create(false);

            $token = new Token();
            $token->user_id = $user->id;
            $token->type = Token::TYPE_RECOVERY;
            if (!$token->save(false)) {
                return false;
            }

            $course = Course::findOne(['id'=>$post['combo_id']]);
            $mailer = Yii::$app->mailer;
            $mailer->htmlLayout = false;
            $mailer->compose('user/welcome-ky-luat-khong-nuoc-mat', [
                'fullName' => $post['fullname'],
                'email' => $user->email,
                'url' => $course->getUrl(true),
                'name'=> $course->name,
                'link' => str_replace('dashboard.', '', Url::toRoute(['/user/recovery/reset', 'id' => $user->id, 'code' => $token->code], true)),
            ])->setTo($user->email)
                ->setSubject("Chúc Mừng Bạn Đã Đăng Ký Thành Công Khóa Miễn Phí")
                ->send();

        }
        $user = User::findOne(['email'=>$post['email']]);
        $userCourse = UserCourse::findOne(['user_id'=>$user->getId(),'course_id'=>$post['combo_id']]);
        if(empty($userCourse)){
            $userCourse = new UserCourse();
            $userCourse->user_id = $user->getId();
            $userCourse->course_id = $post['combo_id'];
            $userCourse->is_activated = UserCourse::BOOL_YES;
            $userCourse->activation_date = time();
            $userCourse->save(false);
            $course = Course::findOne(['id'=>$post['combo_id']]);
            if($isAddNewUser == false){
                $mailer = Yii::$app->mailer;
                $mailer->htmlLayout = false;
                $mailer->compose('user/ky-luat-khong-nuoc-mat', [
                    'fullName' => $post['fullname'],
                    'email' => $user->email,
                    'url' => $course->getUrl(true),
                    'name'=> $course->name
                ])->setTo($user->email)
                    ->setSubject("Chúc Mừng Bạn Đã Đăng Ký Thành Công Khóa Miễn Phí")
                    ->send();
            }

            $dataNotification['error'] = 0;
        }else{
            // thong bao popup user da mua khoa hoc nay roi
            $dataNotification['error'] = 1;
        }
        return json_encode($dataNotification);
    }

    public function actionOrder(){
        $post = Yii::$app->request->post();
        $user_id = Yii::$app->user->id;
        $return = ['status'=>false];

        if($post && !empty($post['course_id']) && $user_id){
            $prefix = \Yii::$app->params['prefix_voucher_free_ref'];
            $userVoucherFreeRef = UserVoucherFreeRef::findOne(['user_id'=>$user_id,'prefix'=>$prefix]);
            if(!$userVoucherFreeRef){
                $userVoucherFreeRef = new UserVoucherFreeRef();
                $userVoucherFreeRef->setUserId($user_id);
            }
            if(UserVoucherFreeRef::canApply($userVoucherFreeRef))
            {
                $order = $userVoucherFreeRef->createOrder($post['course_id']);
                if($order){
                    $userVoucherFreeRef->apply($post['user_ref']);
                    if(!$userVoucherFreeRef->checkUserRefInList($post['user_ref'])){
                        $userVoucherFreeRef->setUserRelationRef($post['user_ref']);
                        $userVoucherFreeRef->save(false);
                    }
                }

                $userVoucherFreeRef = UserVoucherFreeRef::findOne(['user_id'=>$user_id]);
                $return = ['status'=>true,'order'=>$order,
                    'introduce'=>$userVoucherFreeRef->getTotalIntroduce(),
                    'used'=>$userVoucherFreeRef->getTotalReceived() - $userVoucherFreeRef->getTotalConsume()];
            }
        }

        return json_encode($return);
    }
}
