<?php

namespace app\modules\page\controllers;

use Yii;
use yii\web\NotFoundHttpException;
use app\modules\cart\components\ShoppingCart;
use app\modules\cart\models\Product;
use common\helpers\ArrayHelper;
use kyna\user\models\UserCourse;
use yz\shoppingcart\CartActionEvent;

/**
 * Default controller for the `page` module
 */
class DefaultController extends \kyna\page\controllers\PageController
{

    public $viewPath = '@app/web/pages';

    public function init()
    {
        $ret = parent::init();

        CartActionEvent::on(ShoppingCart::className(), ShoppingCart::EVENT_POSITION_PUT, [$this, 'cartAddedItem']);
        CartActionEvent::on(ShoppingCart::className(), ShoppingCart::EVENT_BEFORE_POSITION_REMOVE, [$this, 'cartRemovedItem']);

        return $ret;
    }

    /**
     * @desc trigger when add combo then check if has already any combo items in cart => delete combo item
     * @param type $cartActionEvent
     */
    public function cartAddedItem($cartActionEvent)
    {
        $product = $cartActionEvent->position;

        if ($product->type == Product::TYPE_COMBO) {
            $comboItemCourseIds = ArrayHelper::map($product->comboItems, 'course_id', 'course_id');
            $cartPositions = Yii::$app->cart->getPositions();
            $cartItemIds = array_keys($cartPositions);

            $existItemsInComboIds = array_intersect($comboItemCourseIds,  $cartItemIds);
            $removedPositions = [];
            foreach ($existItemsInComboIds as $existsItemId) {
                $removePosition = $cartPositions[$existsItemId];
                if (Yii::$app->cart->remove($removePosition)) {
                    $removedPositions[] = $removePosition->name;
                }
            }

            if (!empty($removedPositions)) {
                Yii::$app->session->setFlash('warning', "Khóa học <b>" . implode(',', $removedPositions) . "</b> đã bao gồm trong Combo <b>" . $product->name . "</b> nên đã được bỏ ra khỏi giỏ hàng.");
            }
        }
    }

    public function cartRemovedItem($cartActionEvent)
    {
        $product = $cartActionEvent->position;
        if (!empty($product->combo)) {
            $positions = Yii::$app->cart->getPositions();
            $removedComboItems = [];

            foreach ($positions as $position) {
                if ($position->id == $product->id) {
                    continue;
                }
                if (!empty($position->combo) && $position->combo->id == $product->combo->id) {
                    $discount = !empty($position->price_discount) ? ($position->getPrice(false) - $position->price_discount) : 0;
                    Yii::$app->cart->update($position, 1, $discount);
                    $removedComboItems[] = $position->name;
                }
            }

            if (!empty($removedComboItems)) {
                Yii::$app->session->setFlash('warning', "Các khóa học vừa không được hưởng giá khuyến mãi của Combo <b>{$product->combo->name}</b>: " . implode(', ', $removedComboItems));
            }
        }
    }

    public function loadProductModel($pId)
    {
        $product = Product::findOne($pId);
        return $product;
    }

    public function checkAlreadyInCourse($pId)
    {
        return UserCourse::find()->where([
            'user_id' => Yii::$app->user->id,
            'course_id' => $pId
        ])->exists();
    }
}
