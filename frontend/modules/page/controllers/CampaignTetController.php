<?php

namespace app\modules\page\controllers;

use Yii;
use common\campaign\CampaignTet;
use app\components\Controller;

class CampaignTetController extends Controller
{
    public function actionAddToCartCampaignTet(){
        $post = Yii::$app->request->post();
        $course_id = $post['course_id'];
        $flag = CampaignTet::addCart($course_id);
        if($flag == true){
            return json_encode([
                CampaignTet::renderListItemCart(),
                CampaignTet::renderFormHtmlCart(true,$post),
                CampaignTet::renderCountCourseHtml(),
                CampaignTet::renderButtonHasSelect($course_id,$post),
                CampaignTet::renderFormCartBottomMobile(),
            ]);
        }else{
            return false;
        }
    }

    public function actionRemoveCourseCart(){
        $post = Yii::$app->request->post();
        $course_id = $post['course_id'];
        $flag =CampaignTet::removeCart($course_id);
        if($flag == true){
            return json_encode([
                CampaignTet::renderListItemCart(),
                CampaignTet::renderFormHtmlCart(true,$post),
                CampaignTet::renderCountCourseHtml(),
                CampaignTet::renderButtonBuyCart($course_id,$post),
                CampaignTet::renderFormCartBottomMobile()
            ]);
        }else{
            return false;
        }
    }

    public function actionLoadMoreCourse(){
        $post = Yii::$app->request->post();
        $html = CampaignTet::getCourseOnCampaign($limit = 36,$isAjax = true,$post['page'],$post['cate'],$post['tag'],$post['search']);
        return $html;
    }

    public function actionResetCart(){
        CampaignTet::resetCart();
    }
}
