<?php

namespace app\modules\page\controllers;

use yii\web\Controller;

/**
 * Default controller for the `page` module
 */
class SuccessController extends \kyna\page\controllers\SuccessController
{
    public $viewPath = '@app/views/pages';
}
