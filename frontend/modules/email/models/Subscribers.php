<?php

namespace app\modules\email\models;

use Yii;

/**
 * This is the model class for table "subscribers".
 *
 * @property integer $id
 * @property string $email
 * @property string $landing_page
 * @property integer $subscribe_time
 * @property integer $opt_out
 * @property integer $created_time
 * @property integer $updated_time
 */
class Subscribers extends \yii\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'subscribers';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email', 'landing_page'], 'required'],
            [['subscribe_time', 'opt_out'], 'integer'],
            [['email', 'landing_page'], 'string', 'max' => 255],
            [['created_time', 'updated_time'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [            
            'email' => 'Email',
            'landing_page' => 'Landing Page',
            'subscribe_time' => 'Subscribe Time',
            'opt_out' => 'Opt Out',
            'created_time' => 'Created Time',
            'updated_time' => 'Updated Time',
        ];
    }

}
