<?php

namespace app\modules\email\controllers;

use Yii;
use yii\rest\ActiveController;
use kyna\promotion\models\Promotion;

/**
 * Default controller for the `email` module
 */
class DefaultController extends ActiveController
{

    public $modelClass = '\app\modules\email\models\Subscribers';
    public $enableCsrfValidation = false;

    public function behaviors()
    {
        return [
            // ...
            'contentNegotiator' => [
                'class' => \yii\filters\ContentNegotiator::className(),
//                'only' => ['index', 'view'],
                'formatParam' => '_format',
                'formats' => [
                    'application/json' => \yii\web\Response::FORMAT_JSON,
                    'application/xml' => \yii\web\Response::FORMAT_XML,
                ],
            ],
        ];
    }

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionGetEmail()
    {
        Yii::$app->getResponse()->format = \yii\web\Response::FORMAT_JSON;

        if (Yii::$app->request->isPost) {

            $email = Yii::$app->request->post('email');
            $landing_page = Yii::$app->request->post('landing_page', '/p/kyna/gop-y-phien-ban-moi');
            $subscribe_time_temp = Yii::$app->request->post('subscribe_time', date('Y-m-d H:i:s'));
            $subscribe_time = strtotime($subscribe_time_temp);
            /* lưu vào trong subscribers */
            $subscribe = new $this->modelClass();
            $subscribe->email = $email;
            $subscribe->landing_page = $landing_page;
            $subscribe->subscribe_time = $subscribe_time;
            $timestamp = time();
            $subscribe->created_time = $timestamp;
            $subscribe->updated_time = $timestamp;
            try {
                if ($subscribe->save()) {

                    /* Tiến hành tạo mã voucher */
                    $arr_voucherCode = $this->_generateVouchers();
                    $voucherCode = '';
                    if (is_array($arr_voucherCode) && count($arr_voucherCode) > 0)
                        $voucherCode = $arr_voucherCode[0];
                    /* Tiến hành gởi mail */
                    $subject = "[Kyna.vn_Thông báo] Cảm ơn bạn đã tham gia khảo sát";

                    Yii::$app->mailer->compose()
                            ->setHtmlBody($this->renderPartial('@common/mail/campaign/gop_y', [
                                        'voucherCode' => $voucherCode
                                            ], true))
                            ->setTo($subscribe->email)
                            ->setSubject($subject)
                            ->send();
                    return array('success' => true, 'voucherCode' => $voucherCode, 'email' => $subscribe->email, 'message' => 'success');
                }
                else {
                    if ($subscribe->errors && isset($subscribe->errors['email'])) {
                        return array('success' => false, 'voucherCode' => '', 'email' => '', 'message' => $subscribe->errors['email'][0]);
                    }
                }
            } catch (\yii\base\Exception $exc) {
                if ($exc->getCode() == 23000) {
                    return array('success' => false, 'message' => 'Email này đã được dùng cho chương trình này.');
                }
            }
        } else {
            return array('success' => false);
        }
    }

    public function actionDeleteEmail()
    {
        Yii::$app->getResponse()->format = \yii\web\Response::FORMAT_JSON;
        if (Yii::$app->request->isPost) {

            $email = Yii::$app->request->post('email');
            $findEmail = \app\modules\email\models\Subscribers::deleteAll('email = :email', [':email' => $email]);
            return array('status' => $findEmail);
        } else {
            return array('status' => false, 'message' => 'sai method ');
        }
    }

    public function actionCheckEmail()
    {
        Yii::$app->getResponse()->format = \yii\web\Response::FORMAT_JSON;
        if (Yii::$app->request->isPost) {

            $email = Yii::$app->request->post('email');
            $result = \app\modules\email\models\Subscribers::find()->where('email = :email', [':email' => $email])->all();
            return $result;
        } else {
            return array('status' => false, 'message' => 'sai method ');
        }
    }

    private function _generateVouchers()
    {
        $quantity = 1;
        $order_apply_from = 300000;
        $voucher_value = 150000;
        $admin_id = 3;
        $codes = Promotion::create($voucher_value, $quantity, $order_apply_from, $admin_id);
        return $codes;
    }

}
