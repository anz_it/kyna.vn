<?php

namespace app\modules\faq\controllers;

use kyna\faq\models\Faq;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * Default controller for the `faq` module
 */
class FaqController extends \app\components\Controller
{
    public $layout =  '@app/modules/faq/views/layouts/main';
    public $viewPath = '@app/modules/faq/views/faq';
    public $bodyId = 'page-about';

    public function init()
    {
        parent::init();
        $this->setViewPath($this->viewPath);
    }

    public function actionIndex($slug)
    {
        // get available career items
        $data = Faq::findAll(['status' => Faq::BOOL_YES]);
        // format render data
        $renderData = [];
        foreach ($data as $item) {
            $renderData[$item->category_id][] = $item;
        }
        $model = Faq::findOne(['slug' => $slug]);
        if (!$model) {
            throw new NotFoundHttpException('link ko tồn tại');
        }
        return $this->render("index", [
            'model' => $model,
            'data' => $renderData
        ]);
    }
}
