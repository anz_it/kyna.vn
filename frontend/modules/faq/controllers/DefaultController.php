<?php

namespace app\modules\faq\controllers;

use Yii;
use kyna\faq\models\Faq;
use kyna\faq\models\FaqCategory;
use kyna\faq\models\search\FaqSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * Default controller for the `faq` module
 */
class DefaultController extends \app\components\Controller
{
    public $layout =  '@app/modules/faq/views/layouts/main';
    public $viewPath = '@app/modules/faq/views/default';
    public $bodyId = 'page-about';

    public function init()
    {
        parent::init();
        $this->setViewPath($this->viewPath);
    }

    public function actionIndex()
    {
        // get available faq category items
        $data = FaqCategory::findAll(['status' => FaqCategory::BOOL_YES]);

        return $this->render("index", ['data' => $data]);
    }
    public function actionView($slug)
    {
        $model = Faq::findOne(['slug' => $slug, 'status' => Faq::BOOL_YES]);
        if (empty($model)) {
            throw new NotFoundHttpException('Không tìm thấy faq');
        }
        return $this->render("view", [
            'model' => $model,
        ]);
    }

    /**
     * Finds the Faq model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Faq the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Faq::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
