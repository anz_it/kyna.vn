<?php
use yii\web\View;
use \kyna\faq\models\Faq;
use yii\widgets\ListView;
use yii\widgets\Breadcrumbs;
use common\helpers\CDNHelper;
use yii\helpers\Url;

use common\helpers\GoogleSnippetHelper;


$cdnUrl = CDNHelper::getMediaLink();

$this->title = 'Câu hỏi thường gặp - Kyna.vn';

// breadcrumbs css
$this->registerCss("
        @media (max-width: 640px) {
            .breadcrumb-container {
                display: none;
            }
        }
         .breadcrumb-container {
            margin: 0px;
            background-color: #fafafa;
            text-align: left;
            padding-top: 75px;
         }
         .breadcrumb {
            border-radius: 0px;
            padding: 8px 0px;
            margin: 0px 0px 0px 7px;
            background-color: inherit;
         }
        .breadcrumb > li + li::before {
            padding-right: .5rem;
            padding-left: .5rem;
            color: #a0a0a0;
            content: \"»\";
        }
         .breadcrumb > li {
            color: #666 !important;
            font-weight: bold;
         }
         .breadcrumb > li > a {
            color: inherit;
         }
         .breadcrumb > li.active {
            color: #666 !important;
            font-weight: normal;
         }
        #banner div p {
            top: 650px; 
        }
        #banner div span i {
            top: 750px;
        }
        .box-faq-category .detail img {
            max-width: 100%;
        }
}
");

?>
    <div class="page-about-header">
        <!-- Start breadcrumbs-->
        <div class="breadcrumb-container">
            <div class="container">
                <?php
                $homeUrl = Url::toRoute(['/'], true);
                $faqIndexLink = Url::toRoute(['/faq/default/index'], true);
                $faqViewLink = Url::toRoute(['/faq/default/view', 'slug' => $model->slug], true);
                echo Breadcrumbs::widget([
                    'options' => [
                        'class' => 'breadcrumb',
                        'itemscope' => '',
                        'itemtype' => 'http://schema.org/BreadcrumbList',
                    ],
                    'homeLink' => [
                        'label' => '<h5><i class="fa fa-home"></i> Trang chủ</h5>',
                        'url' => $homeUrl,
                        'encode' => false,
                        'template' => GoogleSnippetHelper::renderBreadcrumbTemplate($homeUrl, "<i class=\"fa fa-home\"></i> Trang chủ", 1)

                    ],
                    'links' => [
                        [
                            'label' => "FAQ",
                            'url' => $faqIndexLink,
                            'template' => GoogleSnippetHelper::renderBreadcrumbTemplate($faqIndexLink, 'Câu hỏi thường gặp', 2, false)
                        ],
                        [
                            'label' => "FAQ",
                            'template' => GoogleSnippetHelper::renderBreadcrumbTemplate($faqViewLink, $model->title, 3, true)
                        ],
                    ],
                ])
                ?>
            </div>
        </div>
        <!-- End breadcrumbs -->
    </div>
    <!--end wrap-header-->
    <div class="box-faq-category container">
        <div class="row">
            <div class="col-xs-12 col-md-4">
                <?= $this->render('_boxfaq', ['item' => $model->category]) ?>
                <div class="title-category pc">
                    <a href="<?= Url::toRoute(['/faq/default/index']) ?>"><img src="<?= $cdnUrl ?>/img/faq/question.png" alt=""> Xem tất cả câu hỏi thường gặp</a>
                </div>
            </div>
            <div class="col-xs-12 col-md-8">
                <h3 class="title-faq-question"><?= $model->title ?></h3>
                <div class="detail">
                    <?= $model->content ?>
                </div>
            </div>
            <div class="col-xs-12 mb">
                <div class="title-category">
                    <a href="<?= Url::toRoute(['/faq/default/index']) ?>"><img src="<?= $cdnUrl ?>/img/faq/question.png" alt=""> Xem tất cả câu hỏi thường gặp</a>
                </div>
            </div>
        </div>
    </div>
