<?php
use yii\web\View;
use \kyna\faq\models\Faq;
use yii\widgets\ListView;
use yii\widgets\Breadcrumbs;
use common\helpers\CDNHelper;

use common\helpers\GoogleSnippetHelper;


$cdnUrl = CDNHelper::getMediaLink();

$this->title = 'Câu hỏi thường gặp - Kyna.vn';

?>

<div class="box-faq-category container">
    <div class="row masonry-grid">
        <?php if (!empty($data)): ?>
            <?php foreach ($data as $item): ?>
                <div class="col-xs-12 col-md-6 masonry-grid-item">
                    <?= $this->render('_boxfaq', ['item' => $item]) ?>
                </div>
            <?php endforeach; ?>
        <?php endif; ?>
    </div>
</div>

<?php
// breadcrumbs css
$this->registerCss("
@media (max-width: 640px) {
    .breadcrumb-container {
        display: none;
    }
}
.breadcrumb-container {
    margin: 0px;
    background-color: #fafafa;
    text-align: left;
    padding-top: 75px;
}
.breadcrumb {
    border-radius: 0px;
    padding: 8px 0px;
    margin: 0px 0px 0px 7px;
    background-color: inherit;
}
.breadcrumb > li + li::before {
    padding-right: .5rem;
    padding-left: .5rem;
    color: #a0a0a0;
    content: \"»\";
}
.breadcrumb > li {
    color: #666 !important;
    font-weight: bold;
}
.breadcrumb > li > a {
    color: inherit;
}
.breadcrumb > li.active {
    color: #666 !important;
    font-weight: normal;
}
#banner div p {
    top: 650px; 
}
#banner div span i {
    top: 750px;
}
");
?>
