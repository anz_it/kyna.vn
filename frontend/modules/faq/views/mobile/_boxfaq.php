<?php
use common\helpers\CDNHelper;
use yii\helpers\Url;

$cdnUrl = CDNHelper::getMediaLink();

?>
<div class="title-category">
    <img src="<?= $cdnUrl . $item->image ?>" alt=""> <?= $item->title ?>
</div>
<ul>
    <?php
    $faqs = $item->faqs;
    foreach ($faqs as $faq):
        ?>
        <li><a href="<?= Url::toRoute(['/faq/mobile/view', 'slug' => $faq->slug]) ?>"><i class="fa fa-chevron-right"></i> <?= $faq->title ?><?= $faq->is_important ? " <i class='fa fa-star' style='color: #fb6a00;'></i>" : "" ?></a></li>
    <?php endforeach; ?>
</ul>