<!DOCTYPE HTML>
<?php

use frontend\widgets\FooterWidget;
use frontend\widgets\HeaderWidget;
use frontend\assets\HomeAsset;

HomeAsset::register($this);
$this->beginPage();
?>
<html  lang="<?= Yii::$app->language ?>">
<head>
 <?= $this->render('@app/views/layouts/common/html_head') ?>
 <!-- LINK FONT -->
 <link href='https://fonts.googleapis.com/css?family=Roboto:400,500,700,300' rel='stylesheet' type='text/css'>
</head>

<body <?php if(!empty($this->context->bodyId)) { ?> id="<?= $this->context->bodyId; ?>" <?php } ?> <?php if(!empty($this->context->bodyClass)) { ?> class="<?= $this->context->bodyClass; ?>" <?php } ?> >
<?php $this->beginBody()?>
<?= HeaderWidget::widget(['rootCats' => $this->context->rootCats]); ?>

<?= $content; ?>

<section class="faq-form-question">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <h2>Không tìm thấy câu hỏi hướng dẫn phù hợp?</h2>
                <form class="form" name="faq-form" accept-charset="utf-8" method="post">
                    <div class="input-group">
                        <input type="text" id="email" placeholder="Nhập email của bạn để nhận câu trả lời" name="email" class="form-control">
                    </div>
                    <div class="input-group">
                        <textarea rows="3" id="content" placeholder="Nhập nội dung câu hỏi" name="content" class="form-control"></textarea>
                    </div>
                    <div class="text-center">
                        <button type="submit" class="btn btn-send">Gửi câu hỏi</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
<?= FooterWidget::widget() ?>
<div class="modal fade" id="modal-send-faq">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="my-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="md-body">
                <img src="https://kyna.vn/img/landing-page/thankyou-icon.png" alt="thank you">
                <h3>Cám ơn bạn đã gửi câu hỏi.</h3>
                <p>Chúng tôi sẽ hồi âm câu trả lời qua email đã cung cấp trong thời gian sớm nhất có thể.</p>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $('document').ready(function () {
        $('.masonry-grid').masonry({
            // options
            itemSelector: '.masonry-grid-item',
        });
        // custom validate email
        jQuery.validator.addMethod("customEmail", function(value, element) {
            return this.optional(element) || (value.trim() !== '' && /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(value));
        }, 'Vui lòng nhập chính xác email của bạn');

        // custom validate content
        jQuery.validator.addMethod("content", function(value, element) {
            return this.optional(element) || value.trim() !== '';
        }, 'Vui lòng nhập nội dung câu hỏi');

        // validate faq form on keyup and submit
        $('[name="faq-form"]').validate({
            rules: {
                email: {
                    required: true,
                    customEmail: true
                },
                content: {
                    required: true,
                    content: true
                }
            },
            messages: {
                email: "Vui lòng nhập chính xác email của bạn",
                content: "Vui lòng nhập nội dung câu hỏi"
            },
            submitHandler: function(form) {
                var $email = $("#email");
                var $content = $("#content");
                var email = $email.val().trim();
                var content = $content.val().trim();
                var data = {
                    'entry.1395887131': email,
                    'entry.1085247646': content
                };
                var url = "https://docs.google.com/forms/d/e/1FAIpQLScbqs4KRfoucGbUY0SFXlAsN0HdSVOhkvQnxaK3yuSxZLnJ6A/formResponse";
                $.ajax({
                    'url': url,
                    'method': 'POST',
                    'dataType': 'XML',
                    'data': data,
                    'statusCode': {
                        0: function() {
                            $email.val('');
                            $content.val('');
                            $("#modal-send-faq").modal();
                            successHandler();
                        },
                        200: function() {
                            $email.val('');
                            $content.val('');
                            $("#modal-send-faq").modal();
                            successHandler();
                        }
                    }
                });
            }
        });
        function successHandler() {
            $("<div id='showIframeSEONgon' style='display:none;'></div>").prependTo($('body'));
            var iframe = document.createElement('iframe');
            iframe.width = "100%";
            iframe.height = "1px";
            iframe.frameborder = "0";
            iframe.style.border = '0';
            iframe.src = 'https://kyna.vn/dang-ky-thanh-cong';
            $("#showIframeSEONgon").length > 0 && $("#showIframeSEONgon").html(iframe);
        }
    })
</script>
<style>
    textarea {
        resize: none;
    }
    #modal-send-faq{
        margin-top: 100px;
    }
    #modal-send-faq .modal-content{
        padding: 15px 15px 50px;
    }
    #modal-send-faq img{
        display: block;
        width: 150px;
        margin: 30px auto;
    }
    #modal-send-faq h3{
        text-transform: uppercase;
        color: #50ad4e;
        font-size: 20px;
        font-weight: bold;
        text-align: center;
        margin: 40px auto 20px;
    }
    #modal-send-faq .md-body p{
        text-align: center;
    }
    #modal-send-faq p{
        font-size: 14px !important;
        color: #000;
        text-align: center;
        margin: 10px auto;
    }
    #modal-send-faq .md-footer{
        padding-left: 0;
        padding-right: 0;
        margin-left: 0;
        margin-right: 0;
    }
    #modal-send-faq .md-footer h4{
        text-transform: uppercase;
        color: #000;
        font-weight: bold;
        text-align: center;
        margin: 50px auto 30px;
    }
    #modal-send-faq .md-footer ul{
        text-align: center;
        padding-left: 0;
    }
    #modal-send-faq .md-footer li{
        text-align: left;
        display: -webkit-inline-flex;
        display: -moz-inline-flex;
        display: -ms-inline-flex;
        display: inline-flex;
        align-items: center;
        width: 30%;
    }
    #modal-send-faq .md-footer span{
        display: inline-block;
        background: #50ad4e;
        width: 30px;
        height: 30px;
        padding: 5px 10px;
        font-weight: bold;
        color: #fff;
        border-radius: 50%;
        margin-right: 3px;
    }
    #modal-send-faq .md-footer a{
        color: #333;
        font-size: 14px;
        /*border-bottom: 1px dashed black;*/
        text-decoration: underline !important;
    }
    label.error {
        color: red;
        font-style: italic;
        float: right;
    }
    .btn-send:focus {
        outline: none !important;
    }
    body#page-about {
        background-color: white !important;
    }
</style>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
