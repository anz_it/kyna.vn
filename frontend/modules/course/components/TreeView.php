<?php

namespace app\modules\course\components;

use yii\bootstrap\Html;
use yii\helpers\Url;

/*
 * To change this template file, choose Tools | Templates
 */
class TreeView extends \kartik\tree\TreeView
{
    /**
     * @var the main course of current TreeView
     */
    public $courseId;

    public $wrapperOptions = [];
    public $mainTemplate = '{wrapper}';
    public $linkTemplate = '{nodeId}';
    public $currentNodeId = 0;

    public function renderWidget()
    {
        return $this->renderTree();
    }

    /**
     * Renders the markup for the tree hierarchy - uses a fast non-recursive mode of tree traversal.
     *
     * @return string
     */
    public function renderTree()
    {
        $struct = $this->_module->treeStructure + $this->_module->dataStructure;
        extract($struct);
        $nodeDepth = $currDepth = $counter = 0;
        $out = Html::beginTag('ul', $this->treeOptions)."\n";
        foreach ($this->_nodes as $node) {

            /* @noinspection PhpUndefinedVariableInspection */
            $nodeDepth = $node->$depthAttribute;
            /* @noinspection PhpUndefinedVariableInspection */
            $nodeLeft = $node->$leftAttribute;
            /* @noinspection PhpUndefinedVariableInspection */
            $nodeRight = $node->$rightAttribute;
            /* @noinspection PhpUndefinedVariableInspection */
            $nodeKey = $node->$keyAttribute;
            /* @noinspection PhpUndefinedVariableInspection */
            $nodeName = $node->$nameAttribute;
            /* @noinspection PhpUndefinedVariableInspection */
            $nodeIcon = $node->$iconAttribute;
            /* @noinspection PhpUndefinedVariableInspection */
            $nodeIconType = $node->$iconTypeAttribute;

            $isChild = ($nodeRight == $nodeLeft + 1);
            $indicators = '';
            $css = '';

            $indicators .= '<i class="fa fa-caret-down" aria-hidden="true"></i> ';
            //$indicators .= $this->showCheckbox ? $this->renderCheckboxIconContainer(false) . "\n" : '';
            $css = trim($css);
            if (!empty($css)) {
                Html::addCssClass($nodeOptions, $css);
            }

            $beginNodeWrapper = '';
            $endNodeWrapper = '';
            $class = '';
            if ($nodeDepth == 0) {
                $beginNodeWrapper = '<h4>';
                $endNodeWrapper = '</h4>';
                $class = 'title';
                $indicators = '<i class="fa fa-caret-down" aria-hidden="true"></i> ';
            }
            if ($nodeDepth == 1 and !$isChild) {
                $beginNodeWrapper = '<strong class="text-uppercase">';
                $endNodeWrapper = '</strong>';
            }
            if ($isChild and $nodeDepth > 0) {
                $link = str_replace('{nodeId}', $node->id, urldecode($this->linkTemplate));
                $beginNodeWrapper = '<span class="text">'.Html::beginTag('a', [
                    'href' => $link,
                    'class' => 'link-video',
                ]);
                $endNodeWrapper = '</a></span>';
                $indicators = '';
            }
            if ($node->id == $this->currentNodeId) {
                $class = ' active';
            }

            $out .= Html::beginTag('li', ['class' => $class]).
                $beginNodeWrapper.
                $indicators."\n".
                //$this->renderNodeIcon($nodeIcon, $nodeIconType, $isChild) . "\n" .
                $nodeName.
                $endNodeWrapper.
                '</li>';
            ++$counter;
        }
        $out .= "</ul>\n";

        return Html::tag('div', $out, $this->wrapperOptions);
    }

    public function renderRoot()
    {
        return;
    }

    public function registerAssets()
    {
        return;
    }
}
