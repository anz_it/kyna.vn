<?php

namespace app\modules\course\components;

use kyna\course\models\Course;
use yii\db\Expression;

class RefinerPromotionCount extends \pahanini\refiner\db\Count
{
    
    public function init()
    {
        $ret = parent::init();
        
        if (!$this->refine) {
            $this->refine = [$this, 'refine'];
        }
        
        return $ret;
    }
    
    public function applyTo($query)
    {
        if (is_callable($this->refine) && ($params = $this->getParams())) {
            call_user_func($this->refine, $query, $params);
        }
        return $this;
    }
    
    public function query($query, $name, $free = 0)
    {
        $query->select(["COUNT(*) as $name"]);
        
        if ($free == 0) {
            return $query->andWhere(new Expression("$this->columnName IS NOT NULL"))
                ->andWhere(new Expression("$this->columnName = 0"));
        } else {
            $now = time();
            return $query->leftJoin('promotion_schedules ps', "ps.course_id = courses.id AND start_time <= $now AND end_time >= $now AND ps.price > 0")
                ->andWhere('(price_discount > 0 OR ps.id IS NOT NULL) OR (type = '.Course::TYPE_COMBO.')');
        }
    }
    
    public function getValues()
    {
        $result = [];
        $query = $this->set->getBaseQueryOrigin();
        if ($this->valueFilter) {
            $query->andWhere($this->valueFilter);
        }
        
        $rangers = [0,1];
        
        foreach ($rangers as $ranger) {
            $tmp = $this->query(clone $query, "`all`", $ranger);

            $all = $tmp->asArray()->all();
            
            $active = [];
            $tmp = $this->query(clone $query, "active", $ranger);
            foreach ($this->set->getRefiners() as $refiner) {
                if ($refiner === $this) {
                    continue;
                }
                $refiner->applyTo($tmp);
            }
            
            $active = $tmp->asArray()->all();
            if ($active[0]['active'] > 0) {
                $result[] = [
                    'id' => $ranger,
                    'all' => $all[0]['all'],
                    'active' => $active[0]['active']
                ];
            }
        }
        return $result;
    }
    
    public function refine($query, $params)
    {
        $conditions = [];
        $pars = [];
        foreach ($params as $param) {
            if ($param == 0) {
                $conditions[] = "(courses.$this->columnName = 0)";
            } elseif ($param == 1) {
                $now = time();
                $query->leftJoin('promotion_schedules ps', "ps.course_id = courses.id AND start_time <= $now AND end_time >= $now AND ps.price > 0");
                $conditions[] = '((price_discount > 0 OR ps.id IS NOT NULL) OR (type = '.Course::TYPE_COMBO.'))';
            }
        }

        if (!empty($conditions)) {
            $query->andWhere(implode(' OR ', $conditions), $pars);
        }
        
        return $query;
    }
}
