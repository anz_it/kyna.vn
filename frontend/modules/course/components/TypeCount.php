<?php

namespace app\modules\course\components;

use yii\db\Expression;

class TypeCount extends \pahanini\refiner\common\Base
{

    public $types = [];

    public function init()
    {
        $ret = parent::init();

        if (!$this->refine) {
            $this->refine = [$this, 'refine'];
        }

        return $ret;
    }

    public function applyTo($query)
    {
        if (is_callable($this->refine) && ($params = $this->getParams())) {
            call_user_func($this->refine, $query, $params);
        }
        return $this;
    }

    public function query($query, $name, $field)
    {
        return $query
            ->select(["COUNT(*) as $name"])
            ->andWhere(new Expression("$field = 1"));
    }

    public function getValues()
    {
        $result = [];
        $query = $this->set->getBaseQueryOrigin();
        if ($this->valueFilter) {
            $query->andWhere($this->valueFilter);
        }
        foreach ($this->types as $field => $label) {
            $tmp = $this->query(clone $query, "`all`", $field);

            $all = $tmp->asArray()->all();

            $tmp = $this->query(clone $query, "active", $field);
            foreach ($this->set->getRefiners() as $refiner) {
                if ($refiner === $this) {
                    continue;
                }
                $refiner->applyTo($tmp);
            }
            $active = $tmp->asArray()->all();

            $result[] = [
                'id' => $field,
                'title' => $label,
                'all' => $all[0]['all'],
                'active' => $active[0]['active']
            ];
        }
        return $result;
    }

    public function refine($query, $params)
    {
        if (!is_array($params)) {
            return $query;
        }
        $conditions = [];
        $pars = [];
        foreach ($params as $key => $val) {
            if (isset($this->types[$key])) {
                $conditions[] = "(`$key` = $val)";
            }
        }

        if (!empty($conditions)) {
            $query->andWhere(implode(' OR ', $conditions), $pars);
        }

        return $query;
    }
}
