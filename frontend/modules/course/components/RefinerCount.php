<?php

namespace app\modules\course\components;

class RefinerCount extends \pahanini\refiner\db\Count
{
    
    public function all($query)
    {
        return $this->query($query, '`all`');
    }
}
