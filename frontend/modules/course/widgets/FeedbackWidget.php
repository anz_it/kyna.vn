<?php

namespace frontend\modules\course\widgets;

use yii;
use common\widgets\base\BaseWidget;

class FeedbackWidget extends BaseWidget{

    public function run()
    {
        return $this->getFeedback();
    }

    public function getFeedback()
    {
        return $this->render('feedback');
    }
}