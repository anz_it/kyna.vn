<?php
/**
 * Created by PhpStorm.
 * User: Tan
 * Date: 9/19/2018
 * Time: 11:11 AM
 */

namespace frontend\modules\course\widgets;


use common\helpers\CDNHelper;
use common\widgets\base\BaseWidget;

class DongGia extends BaseWidget
{
    public $class_name;
    public $course_id;

    public function run()
    {
        if(isset(\Yii::$app->params['campaign_dong_gia']) && $this->isDateRunCampaignDongGia()){
            $list_course_ids = \Yii::$app->params['campaign_dong_gia'];
            if (in_array($this->course_id, $list_course_ids)) {
                return $this->viewDongGia();
            }
        }
    }

    public function viewDongGia()
    {
        $cdnUrl = CDNHelper::getMediaLink();
        return $this->render('dong-gia', ['cdnUrl' => $cdnUrl]);
    }

    public function checkTimeRange($startTime, $endTime, $time = 'now')
    {
        $start_time = new \DateTime($startTime);
        $end_time = new \DateTime($endTime);
        $now = new \DateTime($time);
        if ($start_time < $now && $now < $end_time) {
            return true;
        }
        return false;
    }

    public function isDateRunCampaignDongGia(){
        if(isset(\Yii::$app->params['intervals_time_dong_gia'])){
            $date = \Yii::$app->params['intervals_time_dong_gia'];
            $from = $date[0];
            $to = $date[1];
            $start_time = new \DateTime($from);
            $end_time = new \DateTime($to);
            $now = new \DateTime('now');
            if ($start_time < $now && $now < $end_time) {
                return true;
            }
            return false;

        }
    }
}