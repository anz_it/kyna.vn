<?php
/**
 * Created by PhpStorm.
 * User: Tan
 * Date: 11/13/2018
 * Time: 4:18 PM
 */

namespace frontend\modules\course\widgets;


use common\widgets\base\BaseWidget;
use kyna\settings\models\Banner;
use kyna\tag\models\Tag;

class TagBanner extends BaseWidget
{
    /* @var Tag $tag */
    public $tag;

    public function run()
    {
        /* @var Banner $banner */
        $banner = $this->tag->getBanner();
        if (!empty($banner)) {
            return $this->render('tag_banner', ['banner' => $banner]);
        }
    }


}