<?php

namespace frontend\modules\course\widgets;

use app\modules\sync\models\Teacher;
use kyna\user\models\User;
use yii;
use common\widgets\base\BaseWidget;
use yii\helpers\Url;
use common\recommendation\Recommendation;
use pahanini\refiner\DataProvider;
class ContentLessonWidget extends BaseWidget{

    public $contentTabs;

    public $course;

    public $lesson;

    public $user;

    public $qnaModel;

    public function run()
    {
        return $this->getHead();
    }

    public function getHead()
    {
        $this->contentTabs = $this->getContentTabs();
        $course_of_teacher = Recommendation::getCourseByTeacher($this->course->teacher_id,6);
        $teacher_full_name = User::findOne($this->course->teacher_id)->profile->name;
        $course_of_teachers = [];
        for ($i = 0; $i < 4; $i++) {
                    if (count($course_of_teacher) <= 0) {
                            break;
            }
            $index = rand(0, count($course_of_teacher) - 1 );
            $course_of_teachers[] =  $course_of_teacher[$index];
            array_splice($course_of_teacher, $index, 1);
        }

        $courseDataProvider = new DataProvider();
        $courseDataProvider->setModels($course_of_teachers);


        $hotCourse = Recommendation::getHotCourse();
        $shortHotCourses = [];
        for ($i = 0; $i < 4; $i++) {
            if (count($hotCourse) <= 0) {
                break;
            }
            $index = rand(0, count($hotCourse) - 1 );
            $shortHotCourses[] =  $hotCourse[$index];
            array_splice($hotCourse, $index, 1);
        }
        $hotCourseDataProvider = new DataProvider();
        $hotCourseDataProvider->setModels($shortHotCourses);

        return $this->render('content_lesson', [
            'course' => $this->course,
            'lesson' => $this->lesson,
            'user'  => $this->user,
            'qnaModel' => $this->qnaModel,
            'contentTabs' => $this->getContentTabs(),
            'course_of_teacher' => $courseDataProvider
            ,'hotCourses'=>$hotCourseDataProvider
            ,'teacher_full_name'=>$teacher_full_name
        ]);
    }

    public function getContentTabs()
    {
        $tabs = [
            [
                'label' => 'Tổng quan',
                'url' => '#detail-tab-lesson',

            ],
            [
                'label' => 'Tài liệu',
                'url' => '#document-tab-lesson',
                'linkOptions' => [
                    'data-target' => '#lesson-document-content',
                    'data-push-state' => 'false'
                ]
            ],
            [
                'label' => 'Hỏi giảng viên',
                'url' => '#question-tab-lesson',
                'linkOptions' => [
                    'id' => 'tab-qna',
                    'data-target' => '#lesson-latest-qa',
                    'data-push-state' => 'false'
                ],
            ]
        ];
        
        $tabs[] = [
                'label' => 'Thảo luận',
                'url' => '#discuss-tab-lesson',
                'linkOptions' => [
                    'data-target' => '#discuss-content',
                    'data-push-state' => 'false'
                ]
            ];
//            [
//                'label' => '<i class="fa fa-file-text-o" aria-hidden="true"></i> Xem Transcript',
//                'url' => '#transcript-tab-lesson',
//            ],
        
        return $tabs;
    }
}