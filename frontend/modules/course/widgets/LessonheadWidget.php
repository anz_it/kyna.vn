<?php

namespace frontend\modules\course\widgets;

use yii;
use common\widgets\base\BaseWidget;

class LessonheadWidget extends BaseWidget{

    public $course;

    public function run()
    {
        return $this->getHead();
    }

    public function getHead()
    {
        return $this->render('lesson_head',[
            'course' => $this->course
        ]);
    }
}