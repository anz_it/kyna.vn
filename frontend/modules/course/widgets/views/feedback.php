<!-- /.modal -->
<div class="modal fade" id="feedback" tabindex="-1" role="dialog" aria-labelledby="feedback" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header clearfix">
                <div class="logo-feedback"></div>
                <div class="title-feedback">Khảo sát sự hài lòng của học viên với khóa học</div>
                <a class="btn-close" data-dismiss="modal"><i class="icon-cross"></i></a>
            </div>
            <div class="modal-body">
                <div class="description">Chào bạn, ý kiến đóng góp của bạn là nguồn nguyên liệu quý giá để Kyna.vn và các giảng viên hoàn thiện khóa học. Hãy giúp chúng tôi phát triển hơn nữa những khóa học chất lượng và bổ ích nhé!</div>
                <div class="row-feedback">
                    <div class="question-feedback">1. Đánh giá của bạn về chất lượng nội dung bài học</div>
                    <div class="answer-feedback">Rất không hữu ích<span class="choose-feedback"><span>1</span><span>2</span><span class="choose">3</span><span>4</span><span>5</span></span>Rất hữu ích</div>
                </div>
                <div class="row-feedback">
                    <div class="question-feedback">2. Đánh giá của bạn về chất lượng âm thanh của khóa học</div>
                    <div class="answer-feedback">Rất tệ<span class="choose-feedback"><span>1</span><span>2</span><span class="choose">3</span><span>4</span><span>5</span></span>Rất tốt</div>
                </div>
                <div class="row-feedback">
                    <div class="question-feedback">3. Đánh giá của bạn về hình ảnh minh họa</div>
                    <div class="answer-feedback">Rất không hài lòng<span class="choose-feedback"><span>1</span><span>2</span><span class="choose">3</span><span>4</span><span>5</span></span>Rất hài lòng</div>
                </div>
                <div class="row-feedback">
                    <div class="question-feedback">4. Nhìn chung, mức độ hài lòng của bạn về khóa học này như thế nào</div>
                    <div class="answer-feedback">Rất không hài lòng<span class="choose-feedback"><span>1</span><span>2</span><span class="choose">3</span><span>4</span><span>5</span></span>Rất hài lòng</div>
                </div>
                <div class="row-feedback">
                    <div class="question-feedback">5. Góp ý của bạn để khóa học hữu ích hơn:</div>
                    <textarea class="note-feedback"></textarea>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-send">Gửi</button>
                <a class="btn-close" data-dismiss="modal">Hủy</a>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
