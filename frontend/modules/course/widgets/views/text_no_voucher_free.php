<li class="text-no-voucher-free">
    <p>Khóa học không áp dụng Voucher Free</p>
    <a href="https://kyna.vn/tag/voucher-free" target="_blank"><span>Xem danh sách áp dụng</span></a>
</li>
<style>
    .text-no-voucher-free{
        padding-bottom: 10px;
    }
    .text-no-voucher-free p{
        font-weight: 700;
        font-size: 20px;
        margin-bottom: 5px;
    }
    @media screen and (max-width: 425px){
        .text-no-voucher-free{
            margin: 0 -15px;
            background-color: #f5f5f5;
            padding: 0 15px 10px;
        }
        .text-no-voucher-free p{
            font-size: 16px;
        }
    }
    .text-no-voucher-free a{
        color: #f96a20;

    }
    .text-no-voucher-free a:after{
        content: '\f0da';
        font-family: 'FontAwesome';
        padding-left: 8px;

    }
    .text-no-voucher-free span{
        border-bottom: 1px solid #f96a20;
    }

</style>