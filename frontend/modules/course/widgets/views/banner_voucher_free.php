<li class="banner-detail-voucher-free">
    <img src="<?= $cdnUrl ?>/img/voucher-free/banner-detail-voucher.png" alt="">
</li>

<style>
    .banner-detail-voucher-free img{
        width: 100%;
        margin-bottom: 10px;
    }
    @media screen and (max-width: 425px) {
        .banner-detail-voucher-free {
            margin: 0 -15px;
            text-align: center;
            background: #f5f5f5;
        }
        .banner-detail-voucher-free img{
            width: calc(100% - 30px) ;
        }
    }

</style>