<?php
$cdnUrl = \common\helpers\CDNHelper::getMediaLink();
/* @var \kyna\settings\models\Banner $banner */
?>
<div class="box tag-banner">

    <?php if (!empty($banner->link)): ?>
        <a href="<?= $banner->link ?>">
            <img class="banner-img" src="<?= $cdnUrl . $banner->image_url ?>">
        </a>

    <?php endif; ?>
    <?php if (empty($banner->link)): ?>
        <img class="banner-img" src="<?= $cdnUrl . $banner->image_url ?>">
    <?php endif; ?>

</div>
<style type="text/css">
    .tag-banner {
        padding: 0px 15px;
        padding-bottom: 25px;
    }

    .tag-banner .banner-img {
        max-width: 100%;
        width: 100%;
        margin: 0 auto;
        border-radius: 10px;
        box-shadow: 0 3px 14px rgba(0, 0, 0, 0.15);
    }
</style>