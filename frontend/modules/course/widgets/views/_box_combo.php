<?php

use common\helpers\ImageHelper;
use common\helpers\CDNHelper;
use frontend\modules\course\widgets\VoucherFree;
use frontend\modules\course\widgets\DongGia;
use frontend\modules\course\widgets\HotSale;
use frontend\modules\course\widgets\DongGiaFlashSaleTet;
$formatter = \Yii::$app->formatter;
$isMobile = Yii::$app->devicedetect->isMobile() || Yii::$app->devicedetect->isTablet();

$isPopupModal = false;
?>

<div class="k-box-card-wrap clearfix" data-id="<?= $model->id ?>" data-course-type="<?= $model->type ?>">
    <div class="img">
        <?= CDNHelper::image($model->image_url, [
            'alt' => $model->name,
            'class' => 'img-fluid',
            'size' => CDNHelper::IMG_SIZE_THUMBNAIL,
            'resizeMode' => 'cover',
        ]) ?>
        <div class="label-wrap">
            <?php if(HotSale::isDateRunCampaignHotSale()):?>
                <?= HotSale::widget(['course_id' => $model->id])?>
            <?php else:?>
                <?php if ($model->is_new): ?>
                    <span class="lb-new">NEW</span>
                <?php endif; ?>

                <?php if ($model->is_hot): ?>
                    <span class="lb-hot">HOT</span>
                <?php endif; ?>
            <?php endif;?>
            <?= VoucherFree::widget(['course_id' => $model->id])?>
            <?= DongGia::widget(['course_id' => $model->id])?>
            <?= DongGiaFlashSaleTet::widget(['course_id' => $model->id])?>
        </div>
        <!-- end .label-wrap -->
        <span class="background-detail">
          <span class="wrap-position">
            
          </span>
        </span>
    </div>
    <!--end .img-->
    <div class="content">
        <div class="box-style">
            <span class="st-combo"><span class="st-combo hidden-sm-up">Khóa học</span> COMBO</span>
            <span class="time pc"><?= $model->combo_item_count ?> khóa học</span>
        </div>
        <h4><?= $model->name ?></h4>
        <span class="author pc">Khóa học combo</span>
        <span class="pc">Bao gồm <?= $model->combo_item_count ?> khóa học</span>
    </div>
    <!--end .content -->


    <div class="content-mb">
    </div>

    <div class="view-price">
        <ul>
            <?php if (!empty($model->discount_amount)) : ?>
                <li class="price"><strong><?= number_format($model->sell_price, "0", "", ".") ?>đ</strong></li>
                <li class="sale">
                    <span><?= number_format($model->old_price, "0", "", ".") ?>đ</span>
                    <div class="label-discount">
                        <?php if ($model->discount_percent > 0): ?>
                            (-<?= $model->discount_percent ?>%)
                        <?php endif; ?>
                    </div>
                </li>
            <?php elseif (!empty($model->sell_price)) : ?>
                <li class="price"><span><?= $formatter->asCurrency($model->old_price) ?></span></li>
            <?php else : ?>
                <li class="price"><span>Miễn phí</span></li>
            <?php endif; ?>
        </ul>
    </div>
    <!--end .view-price-->

    <div class="view-price-mb">
        <div class="student">
            <div class="number"><?= $model->combo_item_count ?></div>
            <div class="text">học viên</div>
        </div>
        <div class="time">
            <div class="number"><?= !empty($model->teacher_count) ? $model->teacher_count : 0 ?></div>
            <div class="text">chuyên gia</div>
        </div>
        <div class="price">
            <div class="label-price">
                <?php if (!empty($model->discount_amount)) : ?>
                    <div class="first"><?= number_format($model->sell_price, "0", "", ".") ?>đ</div>
                    <div class="last"><s><?= number_format($model->old_price, "0", "", ".") ?>đ</s></div>
                <?php elseif (!empty($model->old_price)) : ?>
                    <div class="first"><?= number_format($model->old_price, "0", "", ".") ?>đ</div>
                <?php else : ?>
                    <div class="first">Miễn phí</div>
                <?php endif; ?>
            </div>
        </div>
    </div>
    <!--end .view-price mb-->
    <?php if ($isPopupModal) { ?>
        <a href="<?php  echo $model->url ?>" class="link-wrap" <?php  echo !$isMobile ?  'data-ajax data-toggle="popup" data-target="#modal"' : ''?></a>
    <?php } else { ?>
        <a href="<?php  echo $model->url ?>" class="link-wrap"></a>
    <?php } ?>

</div>

<?php if ($isPopupModal) { ?>
    <a href="<?php echo $model->url ?>" class="card-popup" <?php echo !$isMobile ?  'data-ajax data-toggle="popup" data-target="#modal"' : ''?></a>
<?php } else { ?>
    <a href="<?php echo $model->url ?>" class="card-popup"></a>
<?php } ?>
<!--end .wrap-->
