<?php
/**
 * Created by PhpStorm.
 * User: nguyenphanngu
 * Date: 9/8/17
 * Time: 10:57 AM
 */

/**
 * @var $this \yii\web\View
 */

use yii\helpers\StringHelper;
use common\helpers\CDNHelper;
use kyna\course\models\Course;
use yii\widgets\ListView;
use yii\web\View;

$formatter = \Yii::$app->formatter;
?>

<?= ListView::widget([
    'dataProvider' => $dataProvider,
    'layout' => "{items}\n",
    'itemView' => function ($model) {
        $model = (object)$model;
        return $this->render($model->type == Course::TYPE_COMBO ? '_box_combo' : '_box_product', ['model' => $model]);
    },
    'options' => [
        'tag' => 'ul',
        'class' => 'k-box-card-list'
    ],
    'itemOptions' => [
        'tag' => 'li',
        'class' => 'col-xl-3 col-lg-4 col-md-6 col-xs-12 k-box-card'
    ],
//    'pager' => [
//        'prevPageLabel' => '<span>&laquo;</span>',
//        'nextPageLabel' => '<span>&raquo;</span>',
//        'options' => [
//            'class' => 'pagination',
//        ]
//    ]
])
?>
<?php
    $hotBoxJx = "        
        var slickSettings_1200px = {
            infinite: true,
            // dots: true,
            slidesToShow: 2,
            slidesToScroll: 1,
             autoplay: true,
             autoplaySpeed: 5000
        }
        
        var slickSettings_992px = {
            infinite: true,
            // dots: true,
            slidesToShow: 1,
            slidesToScroll: 1,
             autoplay: true,
             autoplaySpeed: 5000
        }
        
        var slickSettings = {
            infinite: true,
            // dots: true,
            slidesToShow: 4,
            slidesToScroll: 1,
            autoplay: true,
            autoplaySpeed: 5000,
            prevArrow: $('.p'),
            nextArrow: $('.n'),
            responsive: [
                {
                    breakpoint: 1200,
                    settings: slickSettings_1200px
                },
                {
                    breakpoint: 992,
                    settings: slickSettings_992px
                }
                
            ]
        };
        
        var list = $('.k-box-card-list'),
        items = list.find('.k-box-card .content'),
        setHeights = function() {
            items.css('height', 'auto');

            var perRow = Math.floor(list.width() / items.width());
            if (perRow == null || perRow < 2)
                return true;
            var maxHeight = 0;
            items.each(function() {
                var itemHeight = parseInt($(this).outerHeight());
                if (itemHeight > maxHeight)
                    maxHeight = itemHeight;
            });

            items.each(function() {
                if($(\"#k-highlights\").length > 0){
                    maxHeight += \"px !important\";
                    $(this).attr('style', \"height:\" + maxHeight);
                }
                else {
                    $(this).css('height', maxHeight);
                }

            });
        };
        setHeights();
        $(window).on('resize', setHeights);
        list.find('img').on('load', setHeights);
    ";

    $hotBoxCss = "
        .slider-slick{
            float: right;
        }

        .slider-slick a {
            text-decoration: none;
            display: inline-block;
            padding: 8px 16px;
        }

        .slider-slick a:hover {
            background-color: #ddd;
            color: black;
        }

        .slider-slick .p {
            background-color: #f1f1f1;
            color: black;
            font-size: 10px;
        }

        .slider-slick .n {
            background-color: #4CAF50;
            color: white;
            font-size: 10px;
        }

        slider-slick. .round {
            border-radius: 50%;
        }
        .desktop{
        padding-top:25px;
        }
    ";

    $this->registerJs($hotBoxJx, View::POS_END);
    $this->registerCss($hotBoxCss, ['position' => View::POS_END]);
?>
