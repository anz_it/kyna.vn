<?php

use yii\bootstrap\Tabs;
use yii\helpers\Url;
/* @var $this \yii\web\View */

?>
<div class="menu-desktop hidden-sm-down clearfix col-md-9 pull-right" id="course-learning">
    <?php
    // Set breadcrumbs css
    $this->registerCss("
    @media (max-width: 767px) {
        .breadcrumb-container {
            display: none;
        }
    }
    .breadcrumb-container {
        margin-top: 67px;
        background-color: #fafafa;
    }
	@media (min-width: 768px) and (max-width: 991px){
		.breadcrumb-container{
			margin-top: 0;
		}
	}
    .breadcrumb {

        border-radius: 0px;
        padding: 8px 0px;
        margin: 0px;
        background-color: inherit;
    }
    .breadcrumb > li + li::before {
        padding-right: .5rem;
        padding-left: .5rem;
        color: #a0a0a0;
        content: \"»\";
    }
    .breadcrumb > li {
        color: #666 !important;
        font-weight: bold;
    }
    .breadcrumb > li > a {
        color: inherit;
    }
    .breadcrumb > li.active {
        color: #666 !important;
        font-weight: normal;
    }
    #k-listing {
        margin-top: 0px;
        padding-top: 30px;
    }

    #hot-courses h3 {
        display: none;
    }

    @media (min-width: 768px) {
        #hot-courses h3 {
            display: block;
            color: #50ad4e;
            font-size: 18px;
            padding-left: 15px;
            padding-right: 14px;
            padding-bottom: 10px;
            padding-top: 10px;
        }

        #best-seller-courses h3 {
            display: block;
            color: #50ad4e;
            font-size: 18px;
            padding-left: 15px;
        }
    }


    .hot-category .name {
        position: absolute;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        text-align: center;
        padding-top: 22%;
        font-size: large;
        font-weight: bolder;
        color: #FFFFFF;
    }

    .hot-category:hover .overlay {
        width:100%;
        height:100%;
        position:absolute;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        background-color:#000;
        opacity:0.2;
        border-radius:1px;
    }

");

    ?>
    <?= Tabs::widget([
        'options' => [
            'class' => 'nav nav-tabs sub-menu col-xl-12',
            'id' => 'mycourses'
        ],
        'linkOptions' => [
            'role' => "tab",
            'data-toggle' => "tab",
        ],
        'encodeLabels' => false,
        'renderTabContent' => false,
        'items' => $contentTabs,
    ]);
    ?>
</div>

<div class="box-of-menutabs hidden-sm-down desktop col-md-9 col-xs-12 pull-right" id="detail-tab-lesson">
    <?php if ($course->overview) { ?>
        <?= $course->overview; ?>
    <?php } ?>
</div>
<?php if(!empty($lesson->transcript)):?>
    <div class="box-of-menutabs hidden-sm-down desktop col-md-9 col-xs-12 pull-right" id="transcript-tab-lesson">
        <?= $lesson->transcript; ?>
    </div>
<?php endif;?>
<div role="tabpanel" class="box-of-menutabs hidden-sm-down desktop col-md-9 col-xs-12 pull-right"
     id="document-tab-lesson" data-remote="<?= Url::toRoute(['/course/learning/documents', 'id' => $course->id]) ?>">
    <!--    <div  class="tab-pane" id="lesson-document" >-->
    <div class="ajax-content" id="lesson-document-content"></div>
    <!--    </div>-->
</div>

<div role="tabpanel" class="box-of-menutabs hidden-sm-down desktop col-md-9 col-xs-12 pull-right"
         id="question-tab-lesson" data-remote="<?= Url::toRoute(['/course/learning/qna', 'id' => $course->id]) ?>">
        <?php if (!$course->is_disabled_qna) : ?>
        <?= $this->render('@app/modules/course/views/learning/_qna-form', [
            'qnaModel' => $qnaModel,
            'user' => $user,
            'courseId' => $course->id,
        ]) ?>
        <div id="lesson-latest-qa" class="ajax-content">
        </div>
        <?php else:?>
            <?= $this->render('@app/modules/course/views/learning/_qna_disabled');?>
        <?php endif; ?>
</div>



<div role="tabpanel" class="box-of-menutabs hidden-sm-down desktop col-md-9 col-xs-12 pull-right"
     id="discuss-tab-lesson" data-remote="<?= Url::toRoute(['/course/learning/discuss', 'id' => $course->id]) ?>">
    <!--    <div  class="tab-pane" id="lesson-comment" >-->
    <?= $this->render('@app/modules/course/views/learning/tabs/discuss', [
        'course' => $course
    ]); ?>
    <!--    </div>-->
</div>
<div role="tabpanel" class="tab-pane" id="lesson-transcript">

</div>

<div class="hidden-sm-down desktop col-xs-12 pull-right">
    <section id="hot-courses">
        <h3>
            <strong style="color:#000">Các khóa học khác của Giảng viên <span style="color: #50ad4e;" >
                <?php echo $teacher_full_name?></span> </strong>
        </h3>

        <div class="box">
            <?= $this->render('_list_hot_teacher', [
                'dataProvider' => $course_of_teacher,
            ]);
            ?>
        </div>
        <div class="hr"></div>
    </section>
</div>

<div class="hidden-sm-down desktop col-xs-12 pull-right">

    <section id="hot-courses">
        <h3>
            <strong style="color:#000">Người mua khóa học này cũng mua các khóa học dưới đây :</strong>

        </h3>

        <div class="box">
            <?= $this->render('_list_hot', [
                'dataProvider' => $hotCourses,
            ]);
            ?>
        </div>
        <div class="hr"></div>
    </section>
</div>



<?php
$css = "
        .main-lesson .box-of-menutabs.desktop {
            min-height: 0;
        }
        .main-lesson .box-of-menutabs#detail-tab-lesson {
            min-height: 400px;
        }
    ";
$this->registerCss($css);

if (Yii::$app->request->get('question_id') > 0) {
    $script = <<<JS
$('document').ready(function () {
    $('#tab-qna').trigger('click');
    $('html, body').animate({
        scrollTop:$('#lesson-latest-qa').offset().top
    },'slow');
    
});
JS;

    $this->registerJs($script, \yii\web\View::POS_END);
}
?>
