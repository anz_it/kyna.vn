<?php
/**
 * Created by PhpStorm.
 * User: nguyenphanngu
 * Date: 9/8/17
 * Time: 10:57 AM
 */

/**
 * @var $this \yii\web\View
 */

use yii\helpers\StringHelper;
use common\helpers\CDNHelper;
use kyna\course\models\Course;
use yii\widgets\ListView;
use yii\web\View;

$formatter = \Yii::$app->formatter;
?>

<?= ListView::widget([
    'dataProvider' => $dataProvider,
    'layout' => "{items}\n",
    'itemView' => function ($model) {
        $model = (object)$model;
        return $this->render($model->type == Course::TYPE_COMBO ? '_box_combo' : '_box_product', ['model' => $model]);
    },
    'options' => [
        'tag' => 'ul',
        'class' => 'k-box-card-list'
    ],
    'itemOptions' => [
        'tag' => 'li',
        'class' => 'col-xl-3 col-lg-4 col-md-6 col-xs-12 k-box-card '
    ],
//    'pager' => [
//        'prevPageLabel' => '<span>&laquo;</span>',
//        'nextPageLabel' => '<span>&raquo;</span>',
//        'options' => [
//            'class' => 'pagination',
//        ]
//    ]
])
?>
<?php
    $hotBoxJx = "        
        var slickSettings_1200px_teacher = {
            infinite: true,
            // dots: true,
            slidesToShow: 2,
            slidesToScroll: 1,
             autoplay: true,
             autoplaySpeed: 5000
        }
        
        var slickSettings_992px_teacher = {
            infinite: true,
            // dots: true,
            slidesToShow: 1,
            slidesToScroll: 1,
             autoplay: true,
             autoplaySpeed: 5000
        }
        
        var slickSettingsteacher = {
            infinite: true,
            slidesToShow: 4,
            slidesToScroll: 1,
            autoplay: true,
            autoplaySpeed: 5000,
            prevArrow: $('#pteacher'),
            nextArrow: $('#nteacher'),
            responsive: [
                {
                    breakpoint: 1200,
                    settings: slickSettings_1200px_teacher
                },
                {
                    breakpoint: 992,
                    settings: slickSettings_992px_teacher
                }
                
            ]
        };

        
        
        $(document).ready(function() {
            $('.hot-courses-container-t').slick(slickSettingsteacher);
        });
    ";

    $hotBoxCss = "
        .slider-slick{
            float: right;
        }

        .slider-slick a {
            text-decoration: none;
            display: inline-block;
            padding: 1px 11px !important;
        }

        .slider-slick a:hover {
            background-color: #ddd;
            color: black;
        }

        #pteacher {
            background-color: #4CAF50;
            color: white;
            font-size: 22px;
        }

        #nteacher {
            background-color: #4CAF50;
            color: white;
            font-size: 22px;
        }

        slider-slick. .round {
            border-radius: 50%;
        }

        .slick-arrow.slick-hidden {
            display: inline;
        }
       .desktop{
            padding-top:25px;
        }
    ";

    $this->registerJs($hotBoxJx, View::POS_END);
    $this->registerCss($hotBoxCss, ['position' => View::POS_END]);
?>
