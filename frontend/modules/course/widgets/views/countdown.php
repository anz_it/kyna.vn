<div class="clock-flash-sale <?= $class ?>">
    <div class="col-xs-4">
        <div class="clock-flash-sale-card">
            <p><span class="hours">00</span>giờ</p>
        </div>
    </div>
    <div class="col-xs-4">
        <div class="clock-flash-sale-card">
            <p><span class="minutes">00</span>phút</p>
        </div>
    </div>
    <div class="col-xs-4">
        <div class="clock-flash-sale-card">
            <p><span class="seconds">00</span>giây</p>
        </div>
    </div>
</div>