<?php
use yii\helpers\Url;
use yii\helpers\Html;
use common\helpers\CDNHelper;

$cdnUrl = CDNHelper::getMediaLink();
$user = Yii::$app->user->identity;
$settings = Yii::$app->controller->settings;
$logoImage = $settings['lp_logo_image'];
$logoUrl = $settings['lp_logo_url'];
?>
<div class="header-lesson col-xs-12 clearfix">
    <div class="container">
        <div class="courses back">
          <link rel="stylesheet" href="<?= $cdnUrl ?>/css/font-awesome.min.css">
          <a class="back-mycourse" href="<?= Url::toRoute(['/user/course/index']) ?>">
              <i class="icon-back-mycourse"></i>
          </a>
        </div>
        <a href="<?= $logoUrl ?>" class="navbar-brand logo col-lg-2 col-md-2">
            <img src="<?= $logoImage ?>" alt="Kyna.vn" class="img-fluid">
        </a>
        <div class="courses title-course hidden-sm-down">
          <h4><?= $course->name; ?></h4>
        </div>
        <div class="account dropdown wrap">
            <a id="btn-dropdown-mobile-lesson" href="<?= Url::toRoute(['/user/course/index']) ?>" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                <div class="sub-wrap">
                    <?php $avUrl = CDNHelper::image($user->avatarImage, [
                        'size' => CDNHelper::IMG_SIZE_AVATAR_SMALL,
                        'resizeMode' => 'crop',
                        'returnMode' => 'url'
                    ]);
                    if (!$avUrl) {
                        $avUrl = '/img/default_avatar.png';
                    }

                    ?>
                    <img src="<?=$avUrl?>" class="img-responsive" />
                    <div class="text">
                        <span class="user" ><?= $user->profile->name ?></span>
                        <span class="title">Khóa học của tôi</span>
                    </div>
                </div>
            </a>


            <ul id="dropdown-mobile-lesson" class="dropdown-menu dropdown-user">
                <li class="inner clearfix">
                    <ul>
                        <li><a href="<?= Url::toRoute(['/user/course/index']) ?>"><span class="icon profile-my-courses"></span>Khóa học của tôi</a></li>


                        <li><a href="<?= Url::toRoute(['/user/point/index']) ?>"><i class="icon fa fa-trophy" aria-hidden="true"></i></span>Điểm Kpoint</a></li>
                        <!--
                        <li><a href="<?= Url::toRoute(['/user/course/index']) ?>"><span class="icon profile-my-wishlist"></span>Khóa học đang quan tâm</a></li>
                        -->
                        <li><a href="<?= Url::toRoute(['/user/transaction/in-complete']) ?>"><span class="icon profile-purchase"></span>Lịch sử giao dịch</a></li>
                        <li class="edit"><a href="<?= Url::toRoute(['/user/profile/edit']) ?>" ><span class="icon profile-editinfo"></span>Chỉnh sửa thông tin</a></li>
                        <li><a href="<?= Url::toRoute(['/user/order/active-cod']) ?>" data-toggle="popup" data-target="#activeCOD" data-ajax="" data-push-state="false"><span class="icon profile-activation-code"></span>Kích hoạt mã COD</a></li>
                        <li class="button-out">
                            <?= Html::beginForm(['/user/security/logout'], 'post') ?>
                            <a href="javascript:" onclick="$(this).closest('form').submit()"><span class="icon profile-logout"><i class="icon icon-sign-out"></i></span>Thoát</a>
                            <?= Html::endForm() ?>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>

</div>

<script>
    $(document).ready(function(){
        if ($(window).width() <= 425){
            $("#btn-dropdown-mobile-lesson").click(function(){
                $("#dropdown-mobile-lesson").toggle();
            });
        }
    });
</script>
