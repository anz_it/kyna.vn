<?php
/**
 * Created by PhpStorm.
 * User: Tan
 * Date: 9/19/2018
 * Time: 11:11 AM
 */

namespace frontend\modules\course\widgets;

use Yii;
use common\helpers\CDNHelper;
use common\widgets\base\BaseWidget;

class CampaignTetBanner extends BaseWidget
{
    public $class_name;
    public $course_id;

    public function run()
    {
        if(isset(\Yii::$app->params['intervals_time_campaign_tet']) && $this->isDateRunCampaignTet()){
            $isMobile = Yii::$app->devicedetect->isMobile()
                || Yii::$app->devicedetect->isTablet();
            if(!$isMobile){
                return $this->viewBanner();
            }else{
                return $this->viewBannerMobile();
            }

        }
    }

    public function viewBanner()
    {
        $cdnUrl = CDNHelper::getMediaLink();
        return $this->render('campaign_banner_tet', ['cdnUrl' => $cdnUrl]);
    }
    public function viewBannerMobile()
    {
        $cdnUrl = CDNHelper::getMediaLink();
        return $this->render('campaign_banner_tet_mobile', ['cdnUrl' => $cdnUrl]);
    }

    public function isDateRunCampaignTet(){
        if(isset(\Yii::$app->params['intervals_time_campaign_tet'])){
            $date = \Yii::$app->params['intervals_time_campaign_tet'];
            $from = $date[0];
            $to = $date[1];
            $start_time = new \DateTime($from);
            $end_time = new \DateTime($to);
            $now = new \DateTime('now');
            if ($start_time < $now && $now < $end_time) {
                return true;
            }
            return false;

        }
    }
}