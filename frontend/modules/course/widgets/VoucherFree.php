<?php
/**
 * Created by PhpStorm.
 * User: Tan
 * Date: 9/19/2018
 * Time: 11:11 AM
 */

namespace frontend\modules\course\widgets;


use common\helpers\CDNHelper;
use common\widgets\base\BaseWidget;

class VoucherFree extends BaseWidget
{
    public $class_name;
    public $course_id;

    public function run()
    {
        if(isset(\Yii::$app->params['campaign_voucher_free']) && $this->isDateRunCampaignVoucherFree()){
            $list_course_ids = \Yii::$app->params['campaign_voucher_free'];
            if (in_array($this->course_id, $list_course_ids)) {
                return $this->viewVoucherFree();
            }
        }

    }

    public function viewVoucherFree()
    {
        $cdnUrl = CDNHelper::getMediaLink();
        return $this->render('voucher_free', ['cdnUrl' => $cdnUrl]);
    }
    public function viewNotVoucherFree()
    {
        $cdnUrl = CDNHelper::getMediaLink();
        return $this->render('not_voucher_free', ['cdnUrl' => $cdnUrl]);
    }
    public function checkTimeRange($startTime, $endTime, $time = 'now')
    {
        $start_time = new \DateTime($startTime);
        $end_time = new \DateTime($endTime);
        $now = new \DateTime($time);
        if ($start_time < $now && $now < $end_time) {
            return true;
        }
        return false;
    }

    public function isDateRunCampaignVoucherFree(){
        if(isset(\Yii::$app->params['intervals_time_voucher_free'])){
            $date = \Yii::$app->params['intervals_time_voucher_free'];
            $from = $date[0];
            $to = $date[1];
            $start_time = new \DateTime($from);
            $end_time = new \DateTime($to);
            $now = new \DateTime('now');
            if ($start_time < $now && $now < $end_time) {
                return true;
            }
            return false;

        }
    }
}