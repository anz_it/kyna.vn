<?php
/**
 * Created by PhpStorm.
 * User: Tan
 * Date: 9/19/2018
 * Time: 11:11 AM
 */

namespace frontend\modules\course\widgets;


use common\helpers\CDNHelper;
use common\widgets\base\BaseWidget;
use kyna\course\models\Course;

class HotSale extends BaseWidget
{
    public $class_name;
    public $course_id;

    public function run()
    {
        if(isset(\Yii::$app->params['campaign_hot_sale']) && $this->isDateRunCampaignHotSale()){
            $list_course_ids = \Yii::$app->params['campaign_hot_sale'];
            if (in_array($this->course_id, $list_course_ids)) {
                return $this->viewHotSale();
            }else{
                $model = Course::findOne(['id'=>$this->course_id]);
                if(!empty($model)){
                    return $this->viewNewHot($model);
                }
            }
        }
    }

    public function viewHotSale()
    {
        $cdnUrl = CDNHelper::getMediaLink();
        return $this->render('hot_sale', ['cdnUrl' => $cdnUrl]);
    }

    public function viewNewHot($model){
        return $this->render('new_hot', ['model' => $model]);
    }

    public function checkTimeRange($startTime, $endTime, $time = 'now')
    {
        $start_time = new \DateTime($startTime);
        $end_time = new \DateTime($endTime);
        $now = new \DateTime($time);
        if ($start_time < $now && $now < $end_time) {
            return true;
        }
        return false;
    }

    public static function isDateRunCampaignHotSale(){
        if(isset(\Yii::$app->params['intervals_time_campaign_hot_sale'])){
            $date = \Yii::$app->params['intervals_time_campaign_hot_sale'];
            $from = $date[0];
            $to = $date[1];
            $start_time = new \DateTime($from);
            $end_time = new \DateTime($to);
            $now = new \DateTime('now');
            if ($start_time < $now && $now < $end_time) {
                return true;
            }
            return false;

        }
    }
}