<?php
/**
 * Created by PhpStorm.
 * User: Tan
 * Date: 9/19/2018
 * Time: 11:11 AM
 */

namespace frontend\modules\course\widgets;


use common\widgets\base\BaseWidget;

class CountDownTimer extends BaseWidget
{
    public $class_name;
    public $course_id;

    private $list_course_ids;
    public function run()
    {
        $this->list_course_ids = \Yii::$app->params['campaign_list_course_ids'];
        $countDownTime = \common\helpers\DateTimeHelper::getCountDownTime();
        if (!empty($countDownTime) && !empty($this->list_course_ids) && in_array($this->course_id, $this->list_course_ids)) {
            return $this->getCountDownTimer($this->class_name, $this->course_id);
        }
    }

    public function getCountDownTimer($className)
    {
        return $this->render('countdown', ['class' => $className]);
    }
}