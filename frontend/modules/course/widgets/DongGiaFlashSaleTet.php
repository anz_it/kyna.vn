<?php

namespace frontend\modules\course\widgets;


use common\helpers\CDNHelper;
use common\widgets\base\BaseWidget;
use kyna\course\models\Course;
use common\campaign\CampaignTet;
use kyna\tag\models\CourseTag;
use kyna\tag\models\Tag;
class DongGiaFlashSaleTet extends BaseWidget
{
    public $class_name;
    public $course_id;

    public function run()
    {
        if(isset(\Yii::$app->params['intervals_time_campaign_tet']) && $this->isDateRunCampaignTet()){
            $tag = Tag::findOne(['slug'=>CampaignTet::FLASH_SALE]);
            $count = CourseTag::find()->where(['course_id'=>$this->course_id,'tag_id'=>$tag->id])->count();
            if($count > 0){
                list($hours9_12,$hours14_17,$hours20_23) = CampaignTet::hoursFlashSale();
                if(CampaignTet::isBetweenTimes($hours9_12)
                    || CampaignTet::isBetweenTimes($hours14_17)
                    || CampaignTet::isBetweenTimes($hours20_23)
                ){
                    return $this->viewFlashSale();
                }
            }else{
                $tagDongGia199 = Tag::find()->where(['slug'=>CampaignTet::DONG_GIA_199K])->one();
                $tagDongGia299 = Tag::find()->where(['slug'=>CampaignTet::DONG_GIA_299K])->one();
                $tagDongGia399 = Tag::find()->where(['slug'=>CampaignTet::DONG_GIA_399K])->one();

                if(!empty($tagDongGia199))
                    $countDongGia199 = CourseTag::find()->where(['course_id'=>$this->course_id,'tag_id'=>$tagDongGia199->id])->count();
                else{
                    $countDongGia199 = 0;
                }

                if(!empty($tagDongGia299)){
                    $countDongGia299 = CourseTag::find()->where(['course_id'=>$this->course_id,'tag_id'=>$tagDongGia299->id])->count();
                }else{
                    $countDongGia299 = 0;
                }

                if(!empty($tagDongGia399)){
                    $countDongGia399 = CourseTag::find()->where(['course_id'=>$this->course_id,'tag_id'=>$tagDongGia399->id])->count();
                }else{
                    $countDongGia399 = 0;
                }

                if($countDongGia199 > 0){
                    return $this->viewDongGia();
                }elseif ($countDongGia399 >0){
                    return $this->viewDongGia();
                }elseif ($countDongGia299 >0){
                    return $this->viewDongGia();
                }else{
                    return null;
                }
            }
        }
    }

    public function viewDongGia()
    {
        $cdnUrl = CDNHelper::getMediaLink();
        return $this->render('dong_gia_tet', ['cdnUrl' => $cdnUrl]);
    }

    public function viewFlashSale(){
        $cdnUrl = CDNHelper::getMediaLink();
        return $this->render('flash_sale_tet', ['cdnUrl' => $cdnUrl]);
    }

    public function checkTimeRange($startTime, $endTime, $time = 'now')
    {
        $start_time = new \DateTime($startTime);
        $end_time = new \DateTime($endTime);
        $now = new \DateTime($time);
        if ($start_time < $now && $now < $end_time) {
            return true;
        }
        return false;
    }

    public static function isDateRunCampaignTet(){
        if(isset(\Yii::$app->params['intervals_time_campaign_tet'])){
            $date = \Yii::$app->params['intervals_time_campaign_tet'];
            $from = $date[0];
            $to = $date[1];
            $start_time = new \DateTime($from);
            $end_time = new \DateTime($to);
            $now = new \DateTime('now');
            if ($start_time < $now && $now < $end_time) {
                return true;
            }
        }
        return false;
    }
}