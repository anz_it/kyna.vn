<?php
namespace frontend\modules\course\widgets;
use common\campaign\CampaignTet;
use common\widgets\base\BaseWidget;
use kyna\tag\models\CourseTag;
use kyna\tag\models\Tag;

class CountDownCampaignTetTimer extends BaseWidget
{
    public $class_name;
    public $course_id;

    public function run()
    {
        if ($this->isFlashSaleCourseInTag($this->course_id)) {
            return $this->getCountDownTimer($this->class_name, $this->course_id);
        }
    }

    public function getCountDownTimer($className)
    {
        return $this->render('countdown', ['class' => $className]);
    }

    public function isFlashSaleCourseInTag($courseId){
        if(CampaignTet::InTimesCampaign()){
            $tag = Tag::findOne(['slug'=>CampaignTet::FLASH_SALE]);
            $count = CourseTag::find()->where(['course_id'=>$courseId,'tag_id'=>$tag->id])->count();
            if($count >0 && isset(\Yii::$app->params['intervals_time_campaign_tet']) && $this->isDateRunCampaignTet()){
                list($hours9_12,$hours14_17,$hours20_23) = CampaignTet::hoursFlashSale();
                if(CampaignTet::isBetweenTimes($hours9_12)){
                    return true;
                }elseif(CampaignTet::isBetweenTimes($hours14_17)){
                    return true;
                }else if(CampaignTet::isBetweenTimes($hours20_23)){
                    return true;
                }else{
                    return false;
                }
            }
        }
        return false;
    }

    public static function getHourseCountWaitingTime(){
        list($hours9_12,$hours14_17,$hours20_23) = CampaignTet::hoursFlashSale();
        $hour9 = $hours9_12[0];
        $hour14 = $hours14_17[0];
        $hour20 = $hours20_23[0];

        $currentTime = time();
        if($currentTime < strtotime($hour9)){
            $timer = strtotime($hour9) - $currentTime;
        }else if(date('H:i:s') < $hour14 ){
            $timer = strtotime($hour14) - $currentTime;
        }else if(date('H:i:s') < $hour20 ){
            $timer = strtotime($hour20) - $currentTime;
        }else{
            $timer = strtotime("+1 day", strtotime($hour9)) - $currentTime;
        }
        return $timer;
    }

    public static function getHoursCountDownIntervalTime(){
        list($hours9_12,$hours14_17,$hours20_23) = CampaignTet::hoursFlashSale();
        $intervalTime = [];
        if(CampaignTet::isBetweenTimes($hours9_12)){
            list($hourBegin,$minBegin,$secBegin) = explode(':',$hours9_12[0]);
            list($hourEnd,$minEnd,$secEnd) = explode(':',$hours9_12[1]);

            $start = CampaignTet::getHoursPromotionSchedule($hourBegin,$minBegin,$secBegin);
            $end = CampaignTet::getHoursPromotionSchedule($hourEnd,$minEnd,$secEnd);
            $start = date('Y-m-d H:i:s',$start);
            $end = date('Y-m-d H:i:s',$end);
            $intervalTime[0] = $start;
            $intervalTime[1] = $end;
            return self::getCountDownCampaignTime($intervalTime);

        }elseif(CampaignTet::isBetweenTimes($hours14_17)){
            list($hourBegin,$minBegin,$secBegin) = explode(':',$hours14_17[0]);
            list($hourEnd,$minEnd,$secEnd) = explode(':',$hours14_17[1]);

            $start = CampaignTet::getHoursPromotionSchedule($hourBegin,$minBegin,$secBegin);
            $end = CampaignTet::getHoursPromotionSchedule($hourEnd,$minEnd,$secEnd);

            $start = date('Y-m-d H:i:s',$start);
            $end = date('Y-m-d H:i:s',$end);
            $intervalTime[0] = $start;
            $intervalTime[1] = $end;
            return self::getCountDownCampaignTime($intervalTime);

        }else if(CampaignTet::isBetweenTimes($hours20_23)){
            list($hourBegin,$minBegin,$secBegin) = explode(':',$hours20_23[0]);
            list($hourEnd,$minEnd,$secEnd) = explode(':',$hours20_23[1]);

            $start = CampaignTet::getHoursPromotionSchedule($hourBegin,$minBegin,$secBegin);
            $end = CampaignTet::getHoursPromotionSchedule($hourEnd,$minEnd,$secEnd);

            $start = date('Y-m-d H:i:s',$start);
            $end = date('Y-m-d H:i:s',$end);
            $intervalTime[0] = $start;
            $intervalTime[1] = $end;
            return self::getCountDownCampaignTime($intervalTime);

        }else{
            return null;
        }
    }

    public static function getCountDownCampaignTime($interval)
    {
        if (self::checkTimeRange($interval[0], $interval[1], 'now')) {
            $endTime = new \DateTime($interval[1]);
            $now = new \DateTime('now');
            return $endTime->getTimestamp() - $now->getTimestamp();
        }
        return null;

    }



    public static function checkTimeRange($startTime, $endTime, $time = 'now')
    {
        $start_time = new \DateTime($startTime);
        $end_time = new \DateTime($endTime);
        $now = new \DateTime($time);
        if ($start_time < $now && $now < $end_time) {
            return true;
        }
        return false;
    }

    public static function isDateRunCampaignTet(){
        if(isset(\Yii::$app->params['intervals_time_campaign_tet'])){
            $date = \Yii::$app->params['intervals_time_campaign_tet'];
            $from = $date[0];
            $to = $date[1];
            $start_time = new \DateTime($from);
            $end_time = new \DateTime($to);
            $now = new \DateTime('now');
            if ($start_time < $now && $now < $end_time) {
                return true;
            }
        }
        return false;
    }

}