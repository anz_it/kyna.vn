<?php

namespace app\modules\course\assets;

use yii\web\View;

/**
 * This is class asset bunle for `course` layout.
 */
class PlayerAsset extends \yii\web\AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    // include css files
    public $css = [
        'css/functional.css',
    ];

    // include js
    public $js = [
//        ['js/flowplayer/flowplayer-3.2.13.min.js', 'position' => View::POS_END],
    ];

    public $depends = [
        'yii\web\YiiAsset',
    ];

    public $jsOptions = ['position' => View::POS_HEAD];
}
