<?php

namespace frontend\modules\course\assets;

use common\assets\FrontendAsset;
use yii\web\View;

/**
 * This is class asset bunle for `course` layout
 */
class LearningAsset extends FrontendAsset
{
    // include css files
    public $css = [
        // FontAwesome
        'css/main.min.css?v=1523523259',
        'css/quiz.css',
        'css/introjs.css',
        // slick
    ];
    public $cssOptions = [
        'type' => 'text/css'
    ];
    // include js
    public $js = [
        ['js/script-main.js', 'position' => View::POS_END],
        // JS Menu Mobile
        ['js/jquery.sidr.min.js', 'position' => View::POS_END],
        // ajax
        ["src/js/ajax-caller.js?v=1536292805", 'position' => View::POS_END],
        ["src/js/offpage.js", 'position' => View::POS_END],
        ["js/script-lesson.js?v=1530001706", 'position' => View::POS_END],
        ["src/js/autosize.min.js", 'position' => View::POS_END],
        ["src/js/iscroll.js", 'position' => View::POS_END],
        ["src/js/js_cookie.js", 'position' => View::POS_END],
        ["src/js/main-iscroll.js", 'position' => View::POS_END],
        ["js/lesson.js?v=1523521993", 'position' => View::POS_END],
        ["src/js/lesson.js?v=1539837092", 'position' => View::POS_END],
        ["src/js/reset-visible-box-menutabs-lesson.js", 'position' => View::POS_END],
        ['src/js/menutabs-courses-jquery.js', 'position' => View::POS_END],
        ['/src/js/course-pop-up.js', 'position' => View::POS_END],
        ['js/intro.js', 'position' => View::POS_END],
        //slick
        ["js/slick/slick.min.js", "position" => View::POS_END],
        // select 2
        ["src/js/select2.min.js", "position" => View::POS_END],

        ["src/js/tether.min.js", 'position' => View::POS_END],
        ["src/js/bootstrap.min.js", 'position' => View::POS_END],
        ["src/js/owl.carousel.min.js", 'position' => View::POS_END],
        ["src/js/offpage.js?version=1536292805", 'position' => View::POS_END],
        ["src/js/details.js?v=1521199186", 'position' => View::POS_END],
        ["js/countdown/countdown.js?v=2.2", "position" => View::POS_HEAD],
    ];

    public $depends = [
        'yii\web\YiiAsset',
    ];

    public $jsOptions = ['position' => View::POS_HEAD];
}
