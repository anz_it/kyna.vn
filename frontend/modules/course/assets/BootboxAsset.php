<?php

namespace app\modules\course\assets;

use yii\web\AssetBundle;

class BootboxAsset extends AssetBundle
{
    public $sourcePath = '@bower/';

    public $css = [];

    public $js = [
        'bootbox.js/bootbox.js'
    ];
    public $depends = [];

}