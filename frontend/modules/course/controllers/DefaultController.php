<?php

namespace app\modules\course\controllers;

use app\models\Category;
use common\recommendation\Recommendation;
use pahanini\refiner\DataProvider;
use Yii;
use yii\db\Query;
use yii\helpers\Url;
use yii\web\Response;
use yii\data\Pagination;
use yii\data\ArrayDataProvider;
use yii\data\ActiveDataProvider;
use yii\web\NotFoundHttpException;

use kyna\user\models\UserCourse;
use kyna\course\models\CourseRating;
use kyna\tag\models\Tag;

use common\helpers\StringHelper;

use app\models\Course;
use app\models\CourseSearch;
use app\modules\course\components\Controller;

class DefaultController extends Controller
{

    /**
     * @desc action for show list, list with category, search result, filter of courses
     * @return view render
     * @throws \yii\web\HttpException
     */
    public function actionIndex()
    {
        $catModel = Yii::$app->category->current;

        $get = Yii::$app->request->get();
        if (isset($get['catId']) || isset($get['affiliate_id'])) {
            if (isset($get['catId'])) {
                unset($get['catId']);
            }
            if (isset($get['affiliate_id'])) {
                Yii::$app->session->setFlash('affiliate_id', $get['affiliate_id']);
                unset($get['affiliate_id']);
            }
            if (isset($get['slug'])) {
                unset($get['slug']);
            }
            if (isset($get['parent'])) {
                unset($get['parent']);
            }
            $route = $catModel != null ? [$catModel->url] : ['/course/default/index'];
            if (sizeof($get)) {
                $route = array_merge($route, $get);
            }

            $url = Url::toRoute($route);
            return $this->redirect($url, 301);
        }

        if (!empty($catModel['slug']) && $catModel['status'] == Category::STATUS_DEACTIVE) {
            // found redirect_url -> redirect to redirect_url
            if (!empty($catModel['redirect_url'])) {
                $redirectUrl = $catModel['redirect_url'];
                $this->redirect($redirectUrl, 301);
                Yii::$app->end();
            }
            // found parent -> redirect to parent
            if ($catModel['parent']) {
                // get parent
                $parentCate = $catModel['parent'];
                $redirectUrl = Url::toRoute('/danh-sach-khoa-hoc/'. $parentCate['slug']);
                $this->redirect($redirectUrl, 301);
                Yii::$app->end();
            }
            // cant find anything -> return 404
            throw new NotFoundHttpException('Category Not Found ' . Yii::$app->request->get('slug'));
        }

        if (!empty($get['tag'])) {
            $tagModel = Tag::findOne(['slug' => $get['tag']]);
            if ($tagModel) {
                Yii::$app->view->registerMetaData($tagModel);
            }
        }

        if ($catSlug = Yii::$app->request->get('slug')) {
            if ($catModel) {
                Yii::$app->view->registerMetaData($catModel);
                $get['catId'] = $catModel->id;
            } else {
                throw new NotFoundHttpException('Category Not Found ' . $catSlug);
            }
        }

        $isCombo = false;
        $searchModel = new CourseSearch();
        if (isset($get['course_type']) && $get['course_type'] == CourseSearch::TYPE_COMBO) {
            $searchModel->type = CourseSearch::TYPE_COMBO;
            $isCombo = true;
            Yii::$app->view->registerMetaData('combo');
        }

        // all courses page
        if (empty($tagModel) && empty($catModel) && !$isCombo) {
            Yii::$app->view->registerMetaData('category_all');
        }

        // remove empty facets
        if (!empty($get['facets'])) {
            foreach ($get['facets'] as $index => $facet) {
                $facet = array_values($facet);
                if ($facet[0] == "") {
                    unset($get['facets'][$index]);
                }
            }
        }

        $currentPage = Yii::$app->request->get('page', 1);
        $limit = Yii::$app->params['pageSize'];
        $result = \common\elastic\Course::getListCourse($get, ($currentPage - 1)*$limit, $limit);
        $this->elasticFacets = $result['agg_search_data'];
        $dataProvider = new ArrayDataProvider();
        $dataProvider->setModels($result['data']);
        $dataProvider->setTotalCount($result['total']);

        $hotCourse = Recommendation::getHotCourse();
        $shortHotCourses = [];
        for ($i = 0; $i < 6; $i++) {
            if (count($hotCourse) <= 0) {
                break;
            }
            $index = rand(0, count($hotCourse) - 1 );
            $shortHotCourses[] =  $hotCourse[$index];
            array_splice($hotCourse, $index, 1);
        }
        $hotCourseDataProvider = new DataProvider();
        $hotCourseDataProvider->setModels($shortHotCourses);

        //Mobile
        $shortHotCoursesMobile = [];
        for ($i = 0; $i < 3; $i++) {
            if (count($hotCourse) <= 0) {
                break;
            }
            $index = rand(0, count($hotCourse) - 1 );
            $shortHotCoursesMobile[] =  $hotCourse[$index];
            array_splice($hotCourse, $index, 1);
        }
        $hotCourseDataProviderMobile = new DataProvider();
        $hotCourseDataProviderMobile->setModels($shortHotCoursesMobile);


        $pagination = new Pagination([
            'totalCount' => $result['total'],
            'defaultPageSize' =>$limit
        ]);

        if (!empty($catModel)) {
            $hotCategories = Recommendation::getRelatedCategories($catModel->id);
        } else {
            $hotCategories = Recommendation::getBestSellerCategories(isset($catModel->id) ? $catModel->id : null);
        }
        $shortHotCategories = [];
        for ($i = 0; $i < 6; $i++) {
            if (count($hotCategories) <= 0) {
                break;
            }
            $index = rand(0, count($hotCategories) - 1 );
            $shortHotCategories[] =  $hotCategories[$index];
            array_splice($hotCategories, $index, 1);
        }

        $hotCategoriesDataProvider = new DataProvider();
        $hotCategoriesDataProvider->setModels($shortHotCategories);

        return $this->render('index', [
            'catModel' => $catModel,
            'dataProvider' => $dataProvider,
            'hotCourses' => $hotCourseDataProvider,
            'hotCoursesMobile' => $hotCourseDataProviderMobile,
            'hotCategories' => $hotCategoriesDataProvider,
            'q' => isset($get['q']) ? $get['q'] : '',
            'tag' => !empty($get['tag']) ? $tagModel : null,
            'isCombo' => $isCombo,
            'pagination' => $pagination
        ]);
    }

    /**
     * @param $id
     * @return string
     * @throws \yii\web\HttpException
     * @desc view full screen.
     */
    public function actionView()
    {
        $slug = Yii::$app->request->get('slug');
        if ($slug != strtolower($slug)) {
            return $this->redirect(strtolower(Yii::$app->request->url));
        }
        $model = $this->loadModel();
        Yii::$app->view->registerMetaData($model);
        Yii::$app->category->current = $model->category;

        $data['model'] = $model;

        // check if first view in session
        if (Yii::$app->session->get('course_'.$model->id) !== 'viewed') {
            // First view -> add new record to course_view_logs
            Yii::$app->session->set('course_'.$model->id, 'viewed');
            $query = new Query();
            $query->createCommand()->insert(
                'course_view_logs',
                [
                    'session' => Yii::$app->session->id,
                    'course_id' => $model->id,
                    'created_time' => time(),
                    'updated_time' => time(),
                ]
            )->execute();
        } else {
            // Viewed -> Do nothing
        }

        // log search text
        $searchType = Yii::$app->request->get('searchType');
        $searchValue = Yii::$app->request->get('searchValue');
        if (isset($searchType) && is_numeric($searchType)  && !empty($searchValue)) {
            $query = new Query();
            $query->createCommand()->insert(
                'course_search_click_logs',
                [
                    'user_id' => isset(Yii::$app->user->id)? Yii::$app->user->id : null,
                    'course_id' => $model->id,
                    'search_type' => $searchType,
                    'search_value' => $searchValue,
                    'created_time' => time(),
                    'updated_time' => time(),
                ]
            )->execute();

            // redirect
            $getParams = Yii::$app->request->get();
            unset($getParams['searchType']);
            unset($getParams['searchValue']);
            unset($getParams['slug']);
            $redirectUrl = $this->_buildUrl('/'.$slug, $getParams);

            $this->redirect($redirectUrl);
        }

        $data['newItem'] = '';
        if (!Yii::$app->cart->hasPosition($model->id)) {
            if (!$this->checkAlreadyInCourse($model->id)) {
                $data['newItem'] = '1';
            }
            else{
                $data['newItem'] = '2';
            }
        } else {
            $data['newItem'] = '3';
        }




        if (Yii::$app->request->isAjax) {
            echo $this->renderPartial($model->type == Course::TYPE_COMBO ? 'combo_popup' : 'popup', $data);
        } else {
            $this->layout = '@app/views/layouts/one_column';
            $this->bodyClass = 'k-detail';

            return $this->render($model->type == Course::TYPE_COMBO ? 'view_combo' : 'view', $data);
        }

    }

    public function actionLivesearch()
    {
//        $result = Yii::$app->cache->delete('auto_complete_term'); // delete cache every time - for DEBUG only
        // Set respone format to JSON
        Yii::$app->response->format = Response::FORMAT_JSON;
        $get = Yii::$app->request->get();

        // Get searchText - Remove special characters
        if (empty($get['searchText']))
            $searchText = "";
        else
        {
            // Alow unicode char, number only
            $searchText = $get['searchText'];
            $searchText = preg_replace('/[^A-Za-z0-9\-\' '.StringHelper::getUnicodeCharaters().']/', '', $searchText);  // escape apostraphe
        }

        // Neu searchText rong, tra ve 5 hot courses
        if (empty($searchText) || trim($searchText)=="") {
            // Neu hot couses co san trong cache, return hot courses
            $result = Yii::$app->cache->get('auto_complete_term');
            if (!empty($result)) {
                return $result;
            }
            // Khong co trong cache, search course trong elastic search
            $result = \common\elastic\Course::getCompletionTerm("");
            // Filter ket qua nhan duoc
            $filter_result= [];
            $url_params = [
                // seo
                'utm_source' => 'kynav3',
                'utm_medium' => 'searchbox',
                'utm_campaign' => 'autotop5',
            ];

//            $utm_param = '?utm_source=kynav3&utm_medium=searchbox&utm_campaign=autotop5';

            foreach ($result['data'] as $key => $value)
            {
                $filter_result['data'][$key]['id'] = $result['data'][$key]['id'];                       // course's ID
                $filter_result['data'][$key]['name'] = $result['data'][$key]['name'];                   // course's name
                $filter_result['data'][$key]['highlight_name'] = $result['data'][$key]['name'];         // course's high-lighted name

//                $filter_result['data'][$key]['url'] = $result['data'][$key]['url'].$utm_param;          // course's url
                $filter_result['data'][$key]['url'] = self::_buildUrl($result['data'][$key]['url'], $url_params);          // course's url

                $filter_result['data'][$key]['teacher_name'] = $result['data'][$key]['teacher_name'];   // teacher's name
                $filter_result['data'][$key]['highlight_teacher_name'] = $result['data'][$key]['teacher_name'];   // teacher's high-lighted name
                $filter_result['data'][$key]['type'] = $result['data'][$key]['type'];                   // course's type: 1: normal, 2: combo
            }
            // Save filtered result to cache
            Yii::$app->cache->set('auto_complete_term', $filter_result, 60 * 10);
            return ($filter_result);
        }

        // Toi day la searchText khong rong - Start searching
        // Get search result
        $result = \common\elastic\Course::getCompletionTerm($searchText);
        // Hight light matched words
            // Generate pattern
        $words = explode(" ", $searchText);
        $pt = "";
        foreach ($words as $word)
        {
            if (!empty ($word)){
                $word = StringHelper::addSign($word);
                $pt .= "$word|";
            }
        }
        $pt = substr($pt, 0, strlen($pt) - 1);
            // Mark match words, filter result
        $url_params = [
            'searchType' => 0,
            'searchValue' => $searchText,
        ];


        $filter_result = [];
        foreach ($result['data'] as $key => $value)
        {
            $highlight_name = preg_replace("/($pt)/iu", '<mark>$1</mark>', $result['data'][$key]['name']);
            $highlight_name = preg_replace('[</mark> <mark>]', ' ', $highlight_name);

            $highlight_teacher_name = preg_replace("/($pt)/iu", '<mark>$1</mark>', $result['data'][$key]['teacher_name']);
            $highlight_teacher_name = preg_replace('[</mark> <mark>]', ' ', $highlight_teacher_name);

            $filter_result['data'][$key]['id'] = $result['data'][$key]['id'];                       // course's ID
            $filter_result['data'][$key]['name'] = $result['data'][$key]['name'];                   // course's name
            $filter_result['data'][$key]['highlight_name'] = $highlight_name;                       // course's high-lighted name

//            $filter_result['data'][$key]['url'] = $result['data'][$key]['url'];                     // course's url
            $filter_result['data'][$key]['url'] = self::_buildUrl($result['data'][$key]['url'], $url_params);                     // course's url

            $filter_result['data'][$key]['teacher_name'] = $result['data'][$key]['teacher_name'];   // teacher's name
            $filter_result['data'][$key]['highlight_teacher_name'] = $highlight_teacher_name;       // teacher's high-lighted name
            $filter_result['data'][$key]['type'] = $result['data'][$key]['type'];                   // course's type: 1: normal, 2: combo
        }
        return $filter_result;
    }

    public function loadModel()
    {
        $id = null;
        if ($id = Yii::$app->request->get('id')) {
            $model = Course::find()->andWhere([
                'type' => [Course::TYPE_COMBO, Course::TYPE_VIDEO, Course::TYPE_SOFTWARE],
                'id' => $id,
//                'status' => Course::STATUS_ACTIVE,
            ])->one();
        } elseif ($slug = Yii::$app->request->get('slug')) {
            $model = Course::find()->andWhere([
                'type' => [Course::TYPE_COMBO, Course::TYPE_VIDEO, Course::TYPE_SOFTWARE],
                'slug' => $slug,
//                'status' => Course::STATUS_ACTIVE,
            ])->one();
        }

        if (empty($model)) {
            throw new NotFoundHttpException("Không tồn tại khóa học có id: $id");
        }

        if ($affiliate_id = Yii::$app->request->get('affiliate_id') or isset($id)) {
            $get = Yii::$app->request->get();

            if ($id) unset($get['id']);
            if ($affiliate_id) {
                Yii::$app->session->setFlash('affiliate_id', $get['affiliate_id']);
                unset($get['affiliate_id']);
            }
            unset($get['slug']);

            $url = $model->url . (sizeof($get) ? '?' . http_build_query($get) : '');
            $this->redirect($url, 301);
            return $model;

        }

        if ($model['status'] == Course::STATUS_DEACTIVE) {
                // get direct category url
            $category = Category::findOne(['id' => $model['category_id']]);
            if (!empty($category)) {
                $redirectUrl = Url::toRoute('/danh-sach-khoa-hoc/'.$category['slug']);
                $this->redirect($redirectUrl, 301);
                return $model;
            }
                // category not found - return 404
            throw new NotFoundHttpException("Không tồn tại khóa học có id: $id");
        }

        return $model;
    }

    public function actionReview($course_id)
    {
        $model = new CourseRating();
        $model->course_id = $course_id;
        $model->user_id = Yii::$app->user->id;

        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                Yii::$app->response->format = Response::FORMAT_JSON;

                $result['status'] = true;
                $result['showModal'] = true;
                $result['content'] = 'Cám ơn bạn đã gửi đánh giá. Đánh giá của bạn sẽ được Kyna.vn kiểm duyệt trong thời gian sớm nhất có thể.';
                return $result;
            }
        }

        return $this->renderPartial('rating/_form', [
            'model' => $model,
            'course_id' => $course_id
        ]);

    }

    public function actionLoadReview($course_id)
    {
        $course = Course::findOne($course_id);
        $page = Yii::$app->request->get('page', 1);

        $query = $course->getRatings();
        $query->andWhere(['status' => CourseRating::STATUS_ACTIVE]);
        if ($course->is_disable_seeding) {
            $query->andWhere(['is_cheat' => CourseRating::BOOL_NO]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'created_time' => SORT_DESC
                ]
            ],
            'pagination' => [
                'pageSize' => Yii::$app->params['course-rating-page-size'],
            ]
        ]);
        $dataProvider->pagination->setPage($page - 1);

        return $this->renderPartial('rating/list', ['dataProvider' => $dataProvider]);
    }

    public function actionTest()
    {
//        var_dump(\common\recommendation\Course::getBestSeller());
//        var_dump(Recommendation::updateCourseHotScore());
//        var_dump(Recommendation::updateCourseSellScore());
        var_dump(Recommendation::getHotCourse());
    }

    protected function checkAlreadyInCourse($pId)
    {
        return UserCourse::find()->where([
                'user_id' => Yii::$app->user->id,
                'course_id' => $pId
            ])->exists();

    }

    private function _buildUrl($baseUrl, $params=null)
    {
        $url = $baseUrl;
        if ($params) {
            $url .= '?'.http_build_query($params);
        }
        return $url;
    }


}
