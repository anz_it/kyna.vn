<?php

namespace app\modules\course\controllers;

use Yii;
use yii\web\NotFoundHttpException;

use kyna\course\traits\CertificateControllerTrait;

use common\helpers\DocumentHelper;

class CertificateController extends \app\components\Controller
{

    use CertificateControllerTrait;
    public $layout = '@app/views/layouts/one_column';

    public function beforeAction($action)
    {
        $ret = parent::beforeAction($action);

        $queryParams = Yii::$app->request->queryParams;
        if (!isset($queryParams['hash'])) {
            throw new NotFoundHttpException();
        }
        $hash = DocumentHelper::hashParams($queryParams);
        if ($hash != $queryParams['hash']) {
            throw new NotFoundHttpException();
        }

        if ($action->id == 'index' && !Yii::$app->user->isGuest) {
            $user = Yii::$app->user->identity;

            if ($user->profile == null || empty($user->birthday) || empty($user->profile->name)) {
                Yii::$app->session->setFlash('warning', 'Bạn cần phải điền đúng thông tin Tên và Ngày sinh trước khi nhận chứng chỉ.');
                return $this->redirect(['/user/profile/edit', 'returnUrl' => Yii::$app->request->url]);
            }
        }

        return $ret;
    }
}
