<?php

namespace app\modules\course\controllers;

use Yii;
use yii\base\InvalidRouteException;
use yii\filters\VerbFilter;
use yii\data\ActiveDataProvider;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\Response;
use yii\web\NotFoundHttpException;
use yii\web\ServerErrorHttpException;

use kyna\course\models\CourseLesson;
use kyna\course\models\CourseDocument;
use kyna\course\models\CourseLearnerQna;
use kyna\course\models\CourseDiscussion;
use kyna\user\models\UserCourse;
use kyna\learning\models\UserCourseLesson;

use app\models\Course;
use app\modules\course\components\CourseController;


class LearningController extends CourseController
{

    public $layout = 'lesson';

    public function behaviors()
    {
        $parentBehaviors = parent::behaviors();

        $parentBehaviors['verbs'] = [
            'class' => VerbFilter::className(),
            'actions' => [
                'start-course' => ['post'],
                'end-lesson' => ['post'],
            ],
        ];

        return $parentBehaviors;
    }

    /**
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionIndex($id)
    {
        $userId = Yii::$app->user->getId();
        // get learning model, if not then throw not found exception
        $userCourse = UserCourse::getLearning($id, $userId);
        $this->course = $this->findCourse($id);
        // get section id from request, if not then get the current or default(first) section of course
        $this->sectionId = Yii::$app->request->get('section');
        if ($this->sectionId == null) {
            if ($userCourse->current_section != null) {
                $this->sectionId = $userCourse->current_section;
            } else if ($this->course->defaultSection != null) {
                $this->sectionId = $this->course->defaultSection->id;
            } else {
                throw new NotFoundHttpException('Không tìm được section');
            }
        }
        // get lesson to learn: if not in request then get first lesson of current section
        $lessonId = Yii::$app->request->get('lesson');
        if (!$lessonId) {
            $lessonId = CourseLesson::find()
                ->select('id')
                ->andWhere(['section_id' => $this->sectionId, 'status' => CourseLesson::BOOL_YES])
                ->orderBy('order')->one();
        }

        // load lesson model, if no lesson then init empty lesson
        $this->lesson = CourseLesson::findOne(['id' => $lessonId, 'status' => CourseLesson::STATUS_ACTIVE]);
        if (!$this->lesson) {
            $this->lesson = new CourseLesson();
        }
        // get list lesson of current section to show
        $this->lessonDataProvider = new ActiveDataProvider([
            'query' => CourseLesson::find()->andWhere(['section_id' => $this->sectionId, 'status' => CourseLesson::BOOL_YES])
                ->orderBy(['order' => SORT_ASC]),
            'pagination' => false,
        ]);
        // init user and qna model
        $this->user = Yii::$app->user->identity;
        $this->initQnaModel($id);
        // check if is embed course then just show content (base on field custom_content)
        if ($this->course != null && !empty($this->course->custom_content)  ) {
            $this->layout = 'embed';
            return $this->render('embed', [
                'course' => $this->course,
                'userCourse' => $userCourse
            ]);
        }
        // check can learn lesson now
        $canLearn = $this->lesson->getCanLearnByDate($userId);
        $isLearnQuick = $this->lesson->getIsQuick($userId);
        // check to save current section
        if ($userCourse->current_section != $this->sectionId) {
            $userCourse->current_section = $this->sectionId;
            $userCourse->save(false);
        }
        // check to save current lesson
        if ($userCourse->current_section != $this->lesson->id) {
            $userCourse->current_lesson = $this->lesson->id;
            $userCourse->save(false);
        }

        return $this->render('index', [
            'course' => $this->course,
            'finishedLessons' => $this->course->getFinishedLessons($userId),
            'lessonDataProvider' => $this->lessonDataProvider,
            'lesson' => $this->lesson,
            'sectionId' => $this->sectionId,
            'user' => $this->user,
            'canLearn' => $canLearn,
            'isLearnQuick' => $isLearnQuick,
            'userCourse' => $userCourse
        ]);
    }

    /**
     * Make start lesson
     * @return array of json
     */
    public function actionStartLesson()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $userCourseId = Yii::$app->request->post('user_course_id', 0);
        $lessonId = Yii::$app->request->post('lesson_id', 0);

        $result = false;

        $userCourseLesson = UserCourseLesson::find()->where([
            'user_course_id' => $userCourseId,
            'course_lesson_id' => $lessonId,
        ])->one();

        if ($userCourseLesson == null) {
            $userCourseLesson = new UserCourseLesson();
            $userCourseLesson->user_course_id = $userCourseId;
            $userCourseLesson->course_lesson_id = $lessonId;
            $userCourseLesson->start_time = time();

            $result = $userCourseLesson->save();
        }

        return [
            'result' => $result
        ];
    }

    /**
     * Make lesson is passed
     * @return array of json
     */
    public function actionEndLesson()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $userCourseId = Yii::$app->request->post('user_course_id', 0);
        $lessonId = Yii::$app->request->post('lesson_id', 0);

        $result = false;

        $isNotPassedLesson = UserCourseLesson::find()->where([
            'user_course_id' => $userCourseId,
            'course_lesson_id' => $lessonId,
            'is_passed' => UserCourseLesson::BOOL_YES
        ])->andWhere(['IS NOT', 'process', null])->exists();

        if (!$isNotPassedLesson) {
            $result = UserCourseLesson::pass($userCourseId, $lessonId);
            $userCourse = UserCourse::findOne($userCourseId);
            if ($userCourse) {
                $userCourse->calculateProgress();
            }
        }

        if ($messages = Yii::$app->session->getFlash('mission', false)) {
            $title = '<span class="label label-info" style="padding: 5px;">+' . $messages['point'] . ' KPOINT</span><br>';

            list($finishedMissions, $notFinishedMissions) = $userCourse->course->getSummaryMissionByUser($userCourse->user_id);
            $finishedMisssionsCount = count($finishedMissions);
            $totalMissionCount = count($notFinishedMissions) + $finishedMisssionsCount;

            $sumary = "{$finishedMisssionsCount}/{$totalMissionCount}";

            return [
                'result' => $result,
                'notify' => 1,
                'title' => $title,
                'message' => '<b>' . $sumary . ':</b> Wohoo ~ Bạn vừa hoàn thành nhiệm vụ "' . $messages['missionName'] . '" <a class="btn-mission-tab" href="#" style="font-style: italic">» Nhiệm vụ</a>'
            ];
        }

        return [
            'result' => $result
        ];
    }

    public function actionSyncPercent()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $result = false;

        if (isset($_POST['duration']) && isset($_POST['time']) && isset($_POST['lessonId']) && isset($_POST['userCourseId']) && Yii::$app->request->isAjax) {

            /** @var UserCourse $userCourse */
            $userCourse = UserCourse::find()->where(['id' => $_POST['userCourseId']] )->one();
            if ($userCourse == null || $userCourse->user_id != Yii::$app->user->id)
                throw new NotFoundHttpException();


            /** @var  $userCourseLesson UserCourseLesson */
            $userCourseLesson = UserCourseLesson::find()->where(['user_course_id' => $userCourse->id, 'course_lesson_id' => $_POST['lessonId']])->one();

            $percent = round($_POST['time']/$_POST['duration']*100, 2);
            if ($userCourseLesson != null && $userCourseLesson->process < $percent) {
                $userCourseLesson->scenario = UserCourseLesson::SCENARIO_PROCESS;

                $userCourseLesson->user_course_id = $userCourse->id;
                $userCourseLesson->course_lesson_id = $_POST['lessonId'];
                $userCourseLesson->process = $percent;
                $userCourseLesson->save(false);

                $userCourse->calculateProgress();

                $result = true;
            }

        }

        if ($messages = Yii::$app->session->getFlash('mission', false)) {
            $title = '<span class="label label-info" style="padding: 5px;">+' . $messages['point'] . ' KPOINT</span><br>';

            list($finishedMissions, $notFinishedMissions) = $userCourse->course->getSummaryMissionByUser($userCourse->user_id);
            $finishedMisssionsCount = count($finishedMissions);
            $totalMissionCount = count($notFinishedMissions) + $finishedMisssionsCount;

            $sumary = "{$finishedMisssionsCount}/{$totalMissionCount}";
            return [
                'result' => $result,
                'notify' => 1,
                'title' => $title,
                'message' => '<b>' . $sumary . ':</b> Wohoo ~ Bạn vừa hoàn thành nhiệm vụ "' . $messages['missionName'] . '" <a class="btn-mission-tab" href="#" style="font-style: italic">» Nhiệm vụ</a>'
            ];
        }

        return [
            'result' => $result
        ];
    }

    public function actionSyncFinish()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $result = false;

        if (isset($_POST['duration']) && isset($_POST['time']) && isset($_POST['lessonId']) && isset($_POST['userCourseId']) && Yii::$app->request->isAjax) {

            /** @var UserCourse $userCourse */
            $userCourse = UserCourse::find()->where(['id' => $_POST['userCourseId']] )->one();
            if ($userCourse == null || $userCourse->user_id != Yii::$app->user->id)
                throw new NotFoundHttpException();
            /** @var  $userCourseLesson UserCourseLesson */
            if (UserCourseLesson::pass($userCourse->id, $_POST['lessonId'])) {
                $result = $userCourse->calculateProgress();
            }
        }

        if ($messages = Yii::$app->session->getFlash('mission', false)) {
            $title = '<span class="label label-info" style="padding: 5px;">+' . $messages['point'] . ' KPOINT</span><br>';

            list($finishedMissions, $notFinishedMissions) = $userCourse->course->getSummaryMissionByUser($userCourse->user_id);
            $finishedMisssionsCount = count($finishedMissions);
            $totalMissionCount = count($notFinishedMissions) + $finishedMisssionsCount;

            $sumary = "{$finishedMisssionsCount}/{$totalMissionCount}";

            return [
                'result' => $result,
                'notify' => 1,
                'title' => $title,
                'message' => '<b>' . $sumary . ':</b> Wohoo ~ Bạn vừa hoàn thành nhiệm vụ "' . $messages['missionName'] . '" <i class=""></i><a class="#" style="font-style: italic">» Nhiệm vụ</a>'
            ];
        }

        return [
            'result' => $result
        ];
    }

    public function actionQna()
    {
        $courseLearnerQna = new CourseLearnerQna();

        if (Yii::$app->request->isPost) {
            $formData = Yii::$app->request->post($courseLearnerQna->formName());
            if (isset($formData['course_id'])) {
                $id = $formData['course_id'];
            }
        } else {
            $id = Yii::$app->request->get('id');
        }

        if (!isset($id) or !$id) {
            throw new InvalidRouteException();
        }

        if (Yii::$app->request->isPost) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $result = [
                'status' => true,
                'content' => null,
                'showModal' => false
            ];
            $courseLearnerQna = new CourseLearnerQna();
            if (!$courseLearnerQna->load(Yii::$app->request->post()) or !$courseLearnerQna->save()) {
                $result['status'] = false;
                $errors = $courseLearnerQna->errors;
                $result['content'] = isset($errors['content']) ? $errors['content'][0] : 'Đã xảy ra lỗi trong quá trình xử lý.';
            } else {
                $result['status'] = true;
                $result['showModal'] = true;
                $result['content'] = 'Cám ơn bạn đã gửi câu hỏi cho giảng viên. Câu hỏi của bạn sẽ được Kyna.vn kiểm duyệt và gửi đến cho giảng viên trả lời trong thời gian sớm nhất có thể.';
            }
            return $result;
        }

        /* @var $course Course */
        $course = $this->findCourse($id);

        $query = $course->getLearnerQuestions();
        $query->andWhere(['is_approved' => $course::BOOL_YES]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'attributes' => ['id'],
                'defaultOrder' => [
                    'id' => SORT_DESC
                ]
            ]
        ]);

        return $this->renderPartial('_qna-list', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionDiscuss($id)
    {
        $csIds = Yii::$app->db->createCommand("select user_id from auth_assignment where item_name = 'CustomerService'")->queryColumn();
        if (Yii::$app->request->isPost) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $result = [
                'status' => true,
                'content' => null,
            ];
            $mDiscuss = new CourseDiscussion();

            if (!$mDiscuss->load(Yii::$app->request->post()) or !$mDiscuss->save()) {
                // TODO: Error message here
                $result['status'] = false;
                $errors = $mDiscuss->errors;
                $result['content'] = isset($errors['comment']) ? $errors['comment'][0] : 'Đã xảy ra lỗi trong quá trình xử lý.';
            } else {
                if (!empty($mDiscuss->parent_id)) {
                    $result['status'] = true;
                    $result['content'] = $this->renderPartial('tabs/discuss/_list_reply', [
                        'model' => $mDiscuss->parent,
                        'csIds' => $csIds
                    ]);
                } else {
                    $result['status'] = true;
                    $result['content'] = $this->_renderDiscussion($id, $csIds);
                }
            }

            return $result;
        }

        return $this->_renderDiscussion($id, $csIds);
    }

    private function _renderDiscussion($id, $csIds)
    {
        $course = $this->findCourse($id);
        $query = $course->getDiscussions();
        $query->with('comments');
        $query->orderBy('created_time DESC');
        $query->andFilterWhere(['parent_id' => 0]);
        $query->andWhere(['status' => CourseDiscussion::STATUS_ACTIVE]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        return $this->renderPartial('tabs/discuss/_list', [
            'dataProvider' => $dataProvider,
            'csIds' => $csIds
        ]);
    }

    public function actionDocuments($id)
    {
        $course = $this->findCourse($id);

        $query = $course->getDocuments();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        return $this->renderPartial('_doc-list', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function initQnaModel($course_id)
    {
        $this->qnaModel = new CourseLearnerQna();
        $this->qnaModel->course_id = $course_id;
        $this->qnaModel->user_id = Yii::$app->user->id;
    }

    public function actionDocument($id, $action = 'view')
    {
        $queryParams = Yii::$app->request->getQueryParams();
        $doc = CourseDocument::findOne($id);
        $docLink = $doc->docLink;
        $docName = $doc->title . '.' . $doc->file_ext;
        $header_response = get_headers($docLink, 1);
        if (strpos($header_response[0], "404") !== false || !$doc->validateHash($queryParams)) {
            throw new NotFoundHttpException();
        }

        $response = Yii::$app->response;
        if (!is_resource($response->stream = fopen($docLink, 'r'))) {
            throw new ServerErrorHttpException('file access failed: permission deny');
        }

        if ($action === 'download') {
            $response->headers->set('Content-Type', $doc->mime_type);
            $response->headers->set('Content-Length', $doc->size);
            $response->headers->set('Content-Disposition', "attachment; filename='{$docName}'");
        } else {
            $response->headers->set('Content-Type', $doc->mime_type);
            $response->headers->set('Content-Disposition', "inline; filename='{$docName}'");
            $response->format = Response::FORMAT_RAW;
        }
        return $response->send();
    }

    /**
     * Load course model
     * @param $id
     * @return static
     * @throws NotFoundHttpException
     */
    protected function findCourse($id)
    {
        $model = Course::findOne($id);
        if ($model !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Make quick learn a course
     * @param $id
     */
    public function actionQuickLearn($id)
    {
        $userId = Yii::$app->user->getId();
        $course = $this->findCourse($id);

        $user_course = UserCourse::find()->where(['user_id' => $userId, 'course_id' => $course->id])->one();

        $sectionId = Yii::$app->request->get('section');

        if (!$sectionId) {
            $sectionId = $course->defaultSection->id;
        }

        $lessonId = Yii::$app->request->get('lession');
        if (!empty($user_course)) {
            $user_course->is_quick = UserCourse::BOOL_YES;
            if ($user_course->validate()) {
                $user_course->save();
                if (!$lessonId) {
                    $lessonId = CourseLesson::find()->where(['section_id' => $sectionId])->min('id');
                }
                $this->redirect(Url::toRoute([
                    '/course/learning/index',
                    'id' => $id,
                    'section' => $sectionId,
                    'lesson' => $lessonId,
                ]));
            } else {
                var_dump($user_course->getErrors());
            }
        }
    }

    /**
     * Start learning course
     * @return array json
     */
    public function actionStartCourse()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $courseId = Yii::$app->request->post('course_id', 0);
        $learning = UserCourse::getLearning($courseId);
        $result = false;

        if ($learning != false && $learning->is_started == UserCourse::BOOL_NO) {
            $learning->is_started = UserCourse::BOOL_YES;
            $learning->started_date = time();
            if ($learning->save(false)) {
                $result = true;
            }
        }

        return [
            'result' => $result
        ];
    }

    /**
     * Request rating course
     * @return array json
     */
    public function actionRequestRating()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $courseId = Yii::$app->request->post('course_id', 0);
        /* @var $learning UserCourse */
        $learning = UserCourse::getLearning($courseId);
        $result = false;

        if ($learning->is_requested_rating == UserCourse::BOOL_NO) {
            $learning->is_requested_rating = UserCourse::BOOL_YES;
            if ($learning->save(false)) {
                $result = true;
            }
        }

        return [
            'result' => $result
        ];
    }

    /**
     * @param $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionLoadEmbed($id)
    {
        $userId = Yii::$app->user->getId();

        $userCourse = UserCourse::getLearning($id, $userId);
        if ($userCourse == false) {
            throw new NotFoundHttpException();
        }

        $this->course = $this->findCourse($id);

        return $this->course->custom_content;
    }

}
