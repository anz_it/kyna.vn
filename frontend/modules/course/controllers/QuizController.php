<?php

namespace app\modules\course\controllers;

use Yii;
use yii\caching\TagDependency;
use yii\web\NotFoundHttpException;
use yii\filters\AccessControl;

use kyna\course\models\Quiz;
use kyna\course\models\QuizSession;
use kyna\course\models\QuizSessionAnswer;

use common\helpers\CacheHelper;

class QuizController extends \app\components\Controller
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionIndex($id)
    {
        $quiz = Quiz::findOne($id);
        $reload_page = false;
        $userId = Yii::$app->user->getId();
        $position = 0;
        $btnSubmit = Yii::$app->request->post('btnSubmit', false);
        
        $status = Yii::$app->request->get('status', false);
        if ($status == QuizSession::STATUS_REDO) {
            $redoSession = QuizSession::find()->where([
                        'user_id' => $userId,
                        'quiz_id' => $id,
                        'status' => [QuizSession::STATUS_SUBMITTED, QuizSession::STATUS_WAITING_FOR_MARK, QuizSession::STATUS_MARKED]
                    ])->one();
            if (!empty($redoSession)) {
                $redoSession->status = $status;
                $redoSession->is_passed = QuizSession::BOOL_NO;
                $redoSession->start_time = null;
                $redoSession->save();
                unset($status);
            }
            Yii::$app->session->set('last-position-of-quiz-' . $quiz->id, 0);
            $reload_page = true;
        }
        
        if ($quiz->style_show == 'Từng câu một') {
            $position = Yii::$app->session->get('last-position-of-quiz-' . $quiz->id, 1);
            if ($position < 1) {
                $position = 1;
            }
        }

        $session = $this->_loadSessionModel($quiz, $userId);
        if ($session->load(Yii::$app->request->post())) {
            $session->calculateScore();
            if ($btnSubmit !== false && $btnSubmit == 'btnFinish') {
                // mark the result if have no essay question
                $session->finish();

                $reload_page = true;
            } else {
                $isPrev = ($btnSubmit !== false && $btnSubmit == 'btnPrev');
                if ($isPrev || $position < $session->maxPosition) {
                    if ($isPrev) {
                        $position -= 1;
                    } else {
                        $position += 1;
                    }
                    Yii::$app->session->set('last-position-of-quiz-' . $quiz->id, $position);
                } else {
                    $session->finish();
                    $reload_page = true;
                }
            }

            $session->save(false);
        } elseif (!$reload_page && $session->start_time == null) {
            $session->start_time = time();
            $session->save(false);
        }
        
        $answerModelQuery = $session->getAnswerModels();
        $answerModelQuery->andWhere(['or', 'parent_id is null', 'parent_id = 0']);
        // one by one question
        if ($quiz->style_show == 'Từng câu một' && !empty($position)) {
            $answerModelQuery->andWhere(['position' => $position]);
        }

        $callable = function () use ($answerModelQuery) {
            return $answerModelQuery->all();
        };
        $cacheTags = new TagDependency(['tags' => QuizSessionAnswer::tableName() . '-' . $id . '-' . $userId]);
        $answerModels = CacheHelper::getQueryCacheByTag($callable, $cacheTags);

        return $this->renderPartial('index', [
            'answerModels' => $answerModels,
            'quiz' => $quiz,
            'session' => $session,
            'position' => $position,
            'reload_page' => $reload_page
        ]);
    }
    
    public function actionResult($id)
    {
        $session = QuizSession::findOne($id);
        if ($session == null) {
            throw new NotFoundHttpException;
        }
        
        if ($back = Yii::$app->request->get('back', false)) {
            return $this->renderPartial('@app/modules/course/views/learning/_quiz', ['session' => $session, 'quiz' => $session->quiz]);
        }
        $answerModelQuery = $session->getAnswerModels();
        $answerModelQuery->andWhere(['or', 'parent_id is null', 'parent_id = 0']);
        // one by one question

        $answerModels = $answerModelQuery->all();
        //$answerModels = $session->answerModels;
        
        return $this->renderPartial('result', [
            'session' => $session,
            'answerModels' => $answerModels,
        ]);
    }

    /**
     * @param $quiz Quiz
     * @param $userId
     * @return QuizSession
     */
    private function _loadSessionModel($quiz, $userId)
    {
        $session = $quiz->getSession($userId);

        $now = time();
        if ($session->status == QuizSession::STATUS_DEFAULT) {
            if ($session->start_time == $now) {
                $session->time_remaining = $session->duration;
                $session->status = QuizSession::STATUS_DOING;
            } elseif ($session->time_remaining != null && $session->time_remaining <= 0) {
                $session->time_remaining = 0;
                $session->status = QuizSession::STATUS_SUBMITTED;
            } else {
                $session->time_remaining = $session->duration - ($now - $session->start_time);
                $session->status = QuizSession::STATUS_DOING;
            }
            $session->last_interactive = $now;
            $session->save(false);
        }

        return $session;
    }

}
