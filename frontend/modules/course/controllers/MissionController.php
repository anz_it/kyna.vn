<?php
/**
 * Created by PhpStorm.
 * User: macintosh
 * Date: 10/25/17
 * Time: 2:43 PM
 */

namespace app\modules\course\controllers;

use kyna\course\models\Course;
use Yii;

class MissionController extends \app\components\Controller
{

    public function actionSummary($course_id)
    {
        $course = Course::findOne($course_id);

        list($finishedMissions, $notFinishedMissions) = $course->getSummaryMissionByUser(Yii::$app->user->id);

        return $this->renderPartial('summary', [
            'course' => $course,
            'finishedMissions' => $finishedMissions,
            'notFinishedMissions' => $notFinishedMissions,
        ]);
    }
}