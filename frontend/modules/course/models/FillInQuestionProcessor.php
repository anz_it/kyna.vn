<?php
/**
 * Created by PhpStorm.
 * User: khanhphan
 * Date: 11/2/16
 * Time: 4:09 PM
 */

namespace app\modules\course\models;


use kyna\course\models\QuizSessionAnswer;

class FillInQuestionProcessor extends QuestionProcessor implements QuestionProcessorInterface
{
    
    public function calculateScore($answer)
    {
        if($this->question->parent_id == null || $this->question->parent_id == 0)
            return 0;
        $correctAnswers =  $this->question->answers;
        foreach ($correctAnswers as $correctAnswer) {
            if (trim($correctAnswer->content) == trim($answer))
                return 1;
        }
        return 0;
    }
    
    public function render($view, $form, $question, $sessionAnswer)
    {
        $children = QuizSessionAnswer::find()->where(['quiz_session_id' => $sessionAnswer->quiz_session_id, 'parent_id' => $sessionAnswer->quiz_detail_id])->all();
        $contents = [];
        foreach ($children as $child) {
            $contents[$child->question->content] = $child->id;
        }
        $questionContent = preg_replace_callback('/_+\(\d+\)_+/',
            function ($matches) use ($question, $sessionAnswer, $contents){

                return '<input type="text" name="QuizSession[answers]['.$contents[$matches[0]].']" data-value="'.$matches[0].'" class="fill-in-txt"
                    style = " border:none;
                    border-bottom: 1px dotted gray;
                    position: relative;
                    font-style: italic;
                    top: -4px;"
                    
                    onkeydown = "if(event.keyCode == 13) return false;"
                />';
            },
            nl2br($this->question->content));
        return $this->renderQuestion($sessionAnswer) . $questionContent;

    }

    public function renderResult($view, $sessionAnswer)
    {
        $children = QuizSessionAnswer::find()->where(['quiz_session_id' => $sessionAnswer->quiz_session_id, 'parent_id' => $sessionAnswer->quiz_detail_id])->all();
        $contents = [];
        foreach ($children as $child) {

            $contents[trim($child->question->content)] = $child;
        }


        $question = $this->question;

        $questionContent = preg_replace_callback('/_+\(\d+\)_+/',
            function ($matches) use ($question, $sessionAnswer, $contents){

                $q = $contents[$matches[0]];
                if ($q->is_correct)
                    return "<strong style='color:green; text-decoration: underline;'>".$q->answer."</strong>";
                $correctAnswer = array_values($q->question->answers)[0];

                return "
                <span style='text-decoration: underline'> 
                    <span style='text-decoration: line-through'>{$q->answer}</span>
                    <span style='color:green'> {$correctAnswer->content }</span>
                </span>";
            },
            nl2br($this->question->content));

        return  $this->renderQuestion($sessionAnswer)
                .$questionContent
                .$this->renderExplain($this->question);
    }

    private function renderQuestion ($sessionAnswer)
    {
        return "<h3>{$sessionAnswer->position }. Điền vào chỗ trống .</h3> <br>".$this->renderMedia();
    }


}