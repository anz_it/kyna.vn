<?php
/**
 * Created by PhpStorm.
 * User: khanhphan
 * Date: 11/2/16
 * Time: 4:08 PM
 */

namespace app\modules\course\models;

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use kyna\course\models\QuizAnswer;

class MultipleChoiceQuestionProcessor extends QuestionProcessor implements QuestionProcessorInterface
{
    
    public function calculateScore($answer)
    {
        $answerIds = explode(', ', $answer);
        
        if (empty($answerIds)) {
            return 0;
        }
        
        $correctAnswerQuery = $this->getQuestion()->getCorrectAnswers();
        $correctAnswerIds = array_keys($correctAnswerQuery->select('id')->indexBy('id')->asArray()->all());
        
        $diff = array_merge(array_diff($answerIds, $correctAnswerIds), array_diff($correctAnswerIds, $answerIds));
        if (count($diff) > 0) {
            // wrong, then score = 0
            return 0;
        }
        
        // right => calculate score and return
        return $this->getQuestion()->calculateScore();
    }
    
    public function render($view, $form, $question, $sessionAnswer)
    {
        $html = $this->_renderQuestion($view, $form, $question, $sessionAnswer);
        
        $html .= '<ul class="k-listing-characteristics-list">';
                
        $html .= $this->_renderAnswer($view, $form, $question, $sessionAnswer);
        
        $html .= '</ul>';
        
        return $html;
    }
    
    public function renderResult($view, $sessionAnswer)
    {
        $question = $sessionAnswer->question;
        
        $questionAnswers = $question->getAnswers()->indexBy('id')->all();
        $answerSet = ArrayHelper::map($questionAnswers, 'id', 'content');
        
        $html = $this->_renderQuestion($view, null, $question, $sessionAnswer);
        
        $html .= '<ul class="k-listing-characteristics-list">';
             
        $answereds = explode(', ', $sessionAnswer->answer);
        foreach ($answerSet as $index => $qAnswer) {
            $checked = in_array($index, $answereds);
            $class = '';
            $answerResultClass = '';
            
            if ($checked) {
                $class = 'fa fa-check-circle-o';

                if ($questionAnswers[$index]->is_correct == QuizAnswer::BOOL_YES) {
                    $answerResultClass = 'right';
                } else {
                    $answerResultClass = 'wrong';
                }
            } elseif ($questionAnswers[$index]->is_correct == QuizAnswer::BOOL_YES) {
                $answerResultClass = 'right';
            }
            $html .= '<li class="checkbox' . (!empty($answerResultClass) ? (' answer-' . $answerResultClass) : '') . '">' .
                        '<div class="answer-input">' .
                            Html::checkbox("answers[{$sessionAnswer->id}]", $checked, ['value' => $index, 'id' => 'checkbox-' . $sessionAnswer->id . $index]) .
                            '<label for="checkbox-' . $sessionAnswer->id . $index . '"><span><span class="' . $class . '"></span></span></label>' .
                        '</div>' .
                        '<div class="answer-content">' .
                            $qAnswer . (!empty($questionAnswers[$index]->image_url) ? "<img src='{$this->mediaLink}{$questionAnswers[$index]->image_url}' style='width: 100%'/>" : '') .
                        '</div>' .
                    '</li>';
        }
        
        $html .= '</ul>';
        
        $html .= $this->renderExplain($question);
        
        return $html;
        
    }

    private function _renderQuestion($view, $form, $question, $sessionAnswer)
    {
        return '<div class="question-position">' .
                    $sessionAnswer->position . '.&nbsp;' .
                '</div>' .
                '<div class="question-content">'
                    . $question->content .
                '</div>' .
                $this->renderMedia();
    }
    
    private function _renderAnswer($view, $form, $question, $sessionAnswer)
    {
        $session = $sessionAnswer->session;
        $answers = $question->answers;
        $answerSet = ArrayHelper::map($answers, 'id', 'content');
        
        return $form->field($session, 'answers[' . $sessionAnswer->id . ']', ['template' => '{input}'])->checkboxList($answerSet, [
            'item' => function ($index, $label, $name, $checked, $value) use ($answers) {
                $html =
                    '<li class="checkbox">' .
                        '<div class="answer-input">' .
                            Html::checkbox($name, $checked, ['value' => $value, 'id' => 'checkbox-' . $value . $index]) .
                            '<label for="checkbox-' . $value . $index . '" id="label-' . $value . $index . '"><span><span></span></span></label>' .
                            '</div>' .
                            '<div class="answer-content" data-id="' . $value . $index . '">' .
                                $answers[$value]->content . (!empty($answers[$value]->image_url) ? "<img src='{$this->mediaLink}{$answers[$value]->image_url}' style='width: 100%'/>" : '') .
                            '</div>' .

                    '</li>';
                return $html;
            }
        ]);
    }

}