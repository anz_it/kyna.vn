<?php
/**
 * Created by PhpStorm.
 * User: khanhphan
 * Date: 11/2/16
 * Time: 4:09 PM
 */

namespace app\modules\course\models;

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use kyna\course\models\QuizAnswer;

class SingleChoiceQuestionProcessor extends QuestionProcessor implements QuestionProcessorInterface
{
    
    public function calculateScore($answer)
    {
        $isCorrect = QuizAnswer::find()->andWhere([
            'id' => $answer,
            'is_correct' => QuizAnswer::BOOL_YES
        ])->exists();
        
        if ($isCorrect) {
            return $this->getQuestion()->calculateScore();
        }
        
        return 0;
    }
    
    public function render($view, $form, $question, $sessionAnswer)
    {
        $html = $this->_renderQuestion($view, $form, $question, $sessionAnswer);
        
        $html .= '<ul class="k-listing-characteristics-list">';
                
        $html .= $this->_renderAnswer($view, $form, $question, $sessionAnswer);
        
        $html .= '</ul>';
        
        return $html;
    }
    
    public function renderResult($view, $sessionAnswer)
    {
        $question = $sessionAnswer->question;
        
        $questionAnswers = $question->getAnswers()->indexBy('id')->all();
        $answerSet = ArrayHelper::map($questionAnswers, 'id', 'content');

        $html = $this->_renderQuestion($view, null, $question, $sessionAnswer);
        
        $html .= '<ul class="k-listing-characteristics-list">';
             
        foreach ($answerSet as $index => $qAnswer) {
            $checked = $sessionAnswer->answer == $index;
            $class = '';
            $answerResultClass = '';
            
            if ($checked) {
                $class = 'fa fa-check-circle-o';

                if ($sessionAnswer->is_correct == QuizAnswer::BOOL_YES) {
                    $answerResultClass = 'right';
                } else {
                    $answerResultClass = 'wrong';
                }
            } elseif ($questionAnswers[$index]->is_correct == QuizAnswer::BOOL_YES) {
                $answerResultClass = 'right';
            }
            $html .= '<li class="radio' . (!empty($answerResultClass) ? (' answer-' . $answerResultClass) : '') . '">' .
                        '<div class="answer-input">' .
                            Html::radio("answers[{$sessionAnswer->id}]", $checked, ['disabled' => 'disabled', 'value' => $index, 'id' => 'radio-' . $sessionAnswer->id . $index]) .
                            '<label for="radio-' . $sessionAnswer->id . $index . '"><span><span class="' . $class . '"></span></span></label>' .
                        '</div>' .
                        '<div class="answer-content">' .
                            $qAnswer . (!empty($questionAnswers[$index]->image_url) ? "<img src='{$this->mediaLink}{$questionAnswers[$index]->image_url}' style='width: 100%'/>" : '') .
                        '</div>' .
                    '</li>';
        }
        
        $html .= '</ul>';
        
        $html .= $this->renderExplain($question);
        
        return $html;
        
    }
    
    private function _renderQuestion($view, $form, $question, $sessionAnswer)
    {
        return '<div class="question-position">' .
                    $sessionAnswer->position . '.&nbsp;' .
                '</div>' .
                '<div class="question-content">'
                    . $question->content .
                '</div>' .
                $this->renderMedia();

    }

    private function _renderAnswer($view, $form, $question, $sessionAnswer)
    {
        $session = $sessionAnswer->session;
        $answers = $question->answers;
        $answerSet = ArrayHelper::map($answers, 'id', 'content');
        
        return $form->field($session, 'answers[' . $sessionAnswer->id . ']', ['template' => '{input}'])->radioList($answerSet, [
            'item' => function ($index, $label, $name, $checked, $value) use ($answers, $sessionAnswer) {
                $isChecked = $sessionAnswer->answer == $value;
                
                return '<li class="radio">' .
                            '<div class="answer-input">' .
                                Html::radio($name, $isChecked, ['value' => $value, 'id' => 'radio-' . $value . $index]) .
                                '<label for="radio-' . $value . $index . '" id="label-' . $value . $index . '"><span><span></span></span></label>' .
                            '</div>' .
                            '<div class="answer-content" data-id="' . $value . $index . '">' .
                                $answers[$value]->content . (!empty($answers[$value]->image_url) ? "<img src='{$this->mediaLink}{$answers[$value]->image_url}' style='width: 100%'/>" : '') .
                            '</div>' .
                       '</li>';
            }
        ]);
    }
}