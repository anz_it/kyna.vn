<?php
/**
 * Created by PhpStorm.
 * User: khanhphan
 * Date: 11/2/16
 * Time: 4:05 PM
 */

namespace app\modules\course\models;


interface QuestionProcessorInterface
{

    /**
     * 
     * @param \yii\web\View $view
     * @param \yii\widgets\ActiveForm $form
     * @param \kyna\course\models\QuizQuestion $question
     * @param \kyna\course\models\QuizSessionAnswer $sessionAnswer
     */
    function render($view, $form, $question, $sessionAnswer);

    /**
     * @param string $answer the value can be integer, string
     * @return mixed
     */
    function calculateScore($answer);

    /**
     * @param $question
     * @return mixed
     */
    function renderExplain ($question);

    /**
     * @param $view
     * @param $sessionAnswer
     * @return mixed
     */
    function renderResult($view, $sessionAnswer);
}