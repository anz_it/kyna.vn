<?php
/**
 * Created by PhpStorm.
 * User: khanhphan
 * Date: 11/2/16
 * Time: 4:09 PM
 */

namespace app\modules\course\models;


use common\helpers\Html;
use kartik\sortable\Sortable;
use kyna\course\models\QuizAnswer;
use kyna\course\models\QuizSessionAnswer;

class ConnectQuestionProcessor extends QuestionProcessor implements QuestionProcessorInterface
{

    private function renderQuestion($sessionAnswer)
    {
        return '<div class="question-position">' .
                    $sessionAnswer->position . '. ' .
                '</div>' .
                '<div class="question-content">'
                    . $this->question->content .
                '</div>' .
                $this->renderMedia();
    }

    /**
     *
     * @param \yii\web\View $view
     * @param \yii\widgets\ActiveForm $form
     * @param \kyna\course\models\QuizQuestion $question
     * @param \kyna\course\models\QuizSessionAnswer $sessionAnswer
     * @return string
     */
    public function render($view, $form, $question, $sessionAnswer)
    {

        $result = $this->renderQuestion($sessionAnswer);

        $children = $this->question->children;
        $questions = [];
        $answers = [];
        foreach ($children as $child) {
            $questions[] = ['content' => $child->content . (!empty($child->image_url) ? "<img src='{$this->mediaLink}{$child->image_url}' style='width: 100%'/>" : ''), 'order' => $child->order, 'id' => $child->id];
            $ans = array_values($child->answers);
            if (!empty ($ans)) {
                $answers[] = ['content' => $ans[0]->content . (!empty($ans[0]->image_url) ? "<img src='{$this->mediaLink}{$ans[0]->image_url}' style='width: 100%'/>" : ''), 'order' => $ans[0]->order, 'id' => $ans[0]->id, 'options' => ['data-id' => $ans[0]->id]];
            }

        }
        usort($answers, function ($a, $b) {
            return $a['order'] > $b['order'];
        });
        usort($questions, function ($a, $b) {
            return $a['order'] > $b['order'];
        });

        foreach ($questions as $k => $q) {
            $qsa = QuizSessionAnswer::find()->where(['quiz_question_id' => $q['id'], 'quiz_session_id' => $sessionAnswer->quiz_session_id])->one();

            if (!empty($qsa)) {
                echo Html::hiddenInput("QuizSession[answers][{$qsa->id}]", $answers[$k]['id'], ["class" => "answer_sort"]);
            }
        }
        echo Html::hiddenInput("ConnectQuestion", "0", ["class" => "connect_question"]);

        $list1 = Sortable::widget([
            'connected' => true,
            'itemOptions' => ['class' => 'alert'],
            'options' => [
                'id' => 'list-1-' . $question->id,
            ],
            'items' => $questions
        ]);
        $list2 = Sortable::widget([
            'connected' => true,
            'options' => [
                'id' => 'list-2-' . $question->id,
            ],
            'itemOptions' => ['class' => 'alert alert-warning'],
            'items' => $answers
        ]);
        $result .= "
        <div class='row'>
            <div class='col-md-6'>
                {$list1}
            </div>
            <div class='col-md-6'>
                {$list2}
            </div>
        </div>
        
        <script>
            jQuery('#list-2-{$question->id}').sortable()
                .on('sortupdate', function (e) {
                    var questionRootEle = $(this).closest('.k-listing-characteristics');
                    questionRootEle.find('input.connect_question').val(1);

                    for(var i = 0; i<e.currentTarget.children.length; i++) {
                        $('.answer_sort').eq(i).val($(e.currentTarget.children[i]).attr('data-id'));
                    }
                    
                })
            ;
        </script>
        ";

        $result .= '<div class="clearfix"></div>';
        return $result;
    }

    /**
     * @param string $answer the value can be integer, string
     * @return mixed
     */
    public function calculateScore($answer)
    {
        if ($this->question->parent_id == null || $this->question->parent_id == 0) {
            return 0;
        }
        // this answers is id;
        $answerObject = QuizAnswer::findOne($answer);

        if ($answerObject != null && $answerObject->question_id == $this->question->id)
            return 1;

        return 0;
    }

    public function renderResult($view, $sessionAnswer)
    {
        $result = $this->renderQuestion($sessionAnswer);

        $children = $this->question->children;
        $questions = [];
        $answers = [];
        foreach ($children as $child) {
            $questions[] = ['content' => $child->content . (!empty($child->image_url) ? "<img src='{$this->mediaLink}{$child->image_url}' style='width: 100%'/>" : ''), 'order' => $child->order, 'id' => $child->id];
            $ans = array_values($child->answers);
            if (!empty ($ans)) {
                $answers[] = ['content' => $ans[0]->content . (!empty($ans[0]->image_url) ? "<img src='{$this->mediaLink}{$ans[0]->image_url}' style='width: 100%'/>" : ''), 'order' => $ans[0]->order, 'questionOrder' => $child->order, 'options' => ['data-id' => $ans[0]->id]];
            }

        }
        usort($answers, function ($a, $b) {
            return $a['questionOrder'] > $b['questionOrder'];
        });
        usort($questions, function ($a, $b) {
            return $a['order'] > $b['order'];
        });

        foreach ($questions as $q) {
            $qsa = QuizSessionAnswer::find()->where(['quiz_question_id' => $q['id'], 'quiz_session_id' => $sessionAnswer->quiz_session_id])->one();

            if (!empty($qsa)) {
                echo Html::hiddenInput("QuizSession[answers][{$qsa->id}]", "", ["class" => "answer_sort"]);
            }
        }

        $list1 = Sortable::widget([
            'connected' => true,
            'options' => [
                'id' => 'list-1-' . $this->question->id,
            ],
            'items' => $questions
        ]);
        $list2 = Sortable::widget([
            'connected' => true,
            'options' => [
                'id' => 'list-2-' . $this->question->id,
            ],
            'itemOptions' => ['class' => 'alert alert-warning'],
            'items' => $answers
        ]);
        $result .= "
        <div class='row'>
            <div class='col-md-6'>
                {$list1}
            </div>
            <div class='col-md-6'>
                {$list2}
            </div>
        </div>
        ";

        $result .= '<div class="clearfix"></div>';
        $result .= $this->renderExplain($this->question);

        return $result;
    }
}