<?php

namespace app\modules\course\models;

use kyna\course\models\Quiz;
use kyna\course\models\QuizDetail;
use kyna\course\models\QuizAnswer;

class QuizAnswerForm extends \yii\base\Model
{
    public $quiz_id;
    public $user_id;
    public $_answers = [];

    private $_cacheKey;

    public function safeAttributes()
    {
        return [
            'answers',
        ];
    }

    public function init()
    {
        return parent::init();
    }

    public function getQuiz()
    {
        return Quiz::findOne($this->quiz_id);
    }

    public function setAnswers($answers)
    {
        $answers = array_filter($answers, function ($item) {
            return !empty($item);
        });
        $answers = $answers + $this->answers;

        \Yii::$app->cache->set($this->cacheKey, $answers);
    }

    public function getAnswers()
    {
        $return = \Yii::$app->cache->get($this->cacheKey);
        if (!$return) {
            $return = $this->_prepareQuestions();
        }

        return $return;
    }

    public function getInstantAnswer()
    {
    }

    public function getResults($toBool = true)
    {
        $answersIds = array_values($this->answers);
        $answers = QuizAnswer::find()->where([
            'id' => $answersIds,
        ])->select(['question_id', 'is_correct'])->indexBy('question_id')->asArray()->all();

        return array_map(function ($item) use ($toBool) {
            if (!$toBool) {
                return $item['is_correct'];
            }

            return (bool) $item['is_correct'];
        }, $answers);
    }

    public function getScore()
    {
        $results = $this->getResults(false);
        $counts = array_count_values($results);
        $correctAnswers = $counts[1];

        return $correctAnswers / sizeof($results);
    }

    public function getIsPassed()
    {
        return $this->score >= $this->quiz->min_score;
    }

    public function getCacheKey()
    {
        if (!$this->_cacheKey and $this->user_id and $this->quiz_id) {
            $this->_cacheKey = $this->user_id.'-'.$this->quiz_id;
        }

        return $this->_cacheKey;
    }

    public function clearResult()
    {
        \Yii::$app->cache->delete($this->cacheKey);
    }

    private function _prepareQuestions()
    {
        $qids = QuizDetail::getQuestionIds($this->quiz_id);

        $answers = array_fill_keys($qids, null);
        \Yii::$app->cache->set($this->cacheKey, $answers);

        return $answers;
    }
}
