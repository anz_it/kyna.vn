<?php

namespace app\modules\course\models;

use kyna\course\models\QuizQuestion;
use common\helpers\CDNHelper;
use yii\base\Object;

/**
 * Class QuestionProcessor
 * @package app\modules\course\models
 *
 * @property QuizQuestion $question
 */
class QuestionProcessor extends Object
{

    private $_question;
    protected $mediaLink;

    public function __construct(array $config = [])
    {
        parent::__construct($config);
        $this->mediaLink = CDNHelper::getMediaLink();
    }

    public function setQuestion($question)
    {
        $this->_question = $question;
    }

    public function getQuestion()
    {
        return $this->_question;
    }

    public function renderExplain($question)
    {
        $html = '';

        if (!empty($question->answer_explain)) {
            $html .= "<div class='question-explain-answer'>";

            $html .= "<div><b>Giải thích:</b><p>" . nl2br($question->answer_explain) . "</p></div>";


            $html .= '</div>';
        }

        return $html;
    }

    public function renderMedia()
    {
        $result = "";
//        if (!empty($this->question->image_url) && !empty($this->question->video_embed)) {
//            $result.= "
//            <div class='row'>
//                <div class='col-md-6'>
//                    <img src='{$this->question->image_url}' style='width: 100%' />
//                </div>
//                <div class='col-md-6'>
//                    {$this->question->video_embed}
//                </div>
//            </div>";
//        } else

        if (!empty ($this->question->image_url)) {
            $result .= "<div class='row' style='margin-bottom: 10px'><img src='{$this->mediaLink}{$this->question->image_url}' style='width: 100%;'/></div>";
        }
        if (!empty ($this->question->video_embed)) {
            $embed = preg_replace('/width=\"\d+\"/', 'width="100%"', $this->question->video_embed);
            $result .= "<div class='row' style='margin-bottom: 10px'>{$embed}</div>";
        }
        if (!empty ($this->question->sound_url)) {
            $result .= "<div class='row' style='margin-bottom: 10px'>
                 <audio controls src='{$this->mediaLink}{$this->question->sound_url}' style='width:100%; display:block'></audio>
             </div>";
        }
        return $result;
    }
}
