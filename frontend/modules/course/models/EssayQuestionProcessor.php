<?php
/**
 * Created by PhpStorm.
 * User: khanhphan
 * Date: 11/2/16
 * Time: 4:08 PM
 */

namespace app\modules\course\models;

use yii\helpers\Html;

class EssayQuestionProcessor extends QuestionProcessor implements QuestionProcessorInterface
{
    
    public function calculateScore($anwer)
    {
        if (empty($anwer)) {
            return 0;
        }
        return null;
    }

    public function render($view, $form, $question, $sessionAnswer)
    {
        $html = $this->_renderQuestion($view, $form, $question, $sessionAnswer);
        
        $html .= '<ul class="k-listing-characteristics-list">';
                
        $html .= $this->_renderAnswer($view, $form, $question, $sessionAnswer);
        
        $html .= '</ul>';
        
        return $html;
    }
    
    public function renderResult($view, $sessionAnswer)
    {
        $question = $sessionAnswer->question;
        
        $html = $this->_renderQuestion($view, null, $question, $sessionAnswer);
        
        $html .= '<ul class="k-listing-characteristics-list">';
                
        $html .= Html::textarea('answers[' . $sessionAnswer->id . ']', $sessionAnswer->answer, ['readonly' => true, 'class' => 'form-control']);
        
        $html .= '</ul>';
        
        $html .= $this->renderExplain($question);
        
        return $html;
    }

    private function _renderQuestion($view, $form, $question, $sessionAnswer)
    {
        return '<div class="question-position">' .
                    $sessionAnswer->position . '.&nbsp;' .
                '</div>' .
                '<div class="question-content">'
                    . $question->content .
                '</div>' .
                $this->renderMedia();
    }
    
    private function _renderAnswer($view, $form, $question, $sessionAnswer)
    {
        $quizSession = $sessionAnswer->session;
        
        return $form->field($quizSession, 'answers[' . $sessionAnswer->id . ']')->textArea()->label(false)->error(false);
    }

}