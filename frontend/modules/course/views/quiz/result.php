<?php

use yii\helpers\Html;
use yii\helpers\Url;
?>

<?php foreach ($answerModels as $index => $model) {
    echo $this->render('_question_result', [
        'model' => $model,
        'index' => $index,
    ]);
} ?>

<div class="quiz-footer">
    <ul class="list-buttons">
        <li class="btn-quiz">
            <?= Html::a('Quay lại', Url::toRoute(['/course/quiz/result', 'id' => $session->id, 'back' => true]), [
                'class' => 'btn-back',
                'data-ajax' => true,
                'data-target' => '#quiz-container',
                'data-push-state' => 'false',
            ]) ?>
        </li>
    </ul>
</div>