<?php

/**
 * @var $model \kyna\course\models\QuizSessionAnswer
 * @var $question QuizQuestion
 */
$question = $model->question;

?>
<div class="k-listing-characteristics">
    <?= $question->processor->render($this, $form, $question, $model) ?>
</div>

