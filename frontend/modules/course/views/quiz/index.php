<?php

use yii\bootstrap\ActiveForm;

?>
<?php if ($reload_page) : ?>
    <script type="text/javascript">
        $(document).ready(function () {
            window.location.reload();
        });
    </script>
<?php else : ?>
    <?php $form = ActiveForm::begin([
        'id' => 'form-quiz',
        'options' => [
            'data-ajax' => true,
            'data-target' => '#quiz-content',
            'data-push-state' => 'false',
        ]
    ]) ?>
    <?php foreach ($answerModels as $index => $model) {
        echo $this->render('_question', [
            'model' => $model,
            'index' => $index,
            'form' => $form,
            'quiz' => $quiz,
        ]);
    } ?>

    <div class="quiz-footer">
        <ul class="list-buttons">
            <li class="btn-quiz">
                <input type="hidden" name="btnSubmit" value="" id="btn-submit"/>
                <?php if ($quiz->style_show == 'Từng câu một') : ?>
                    <?php if ($position > 1) : ?>
                        <!-- <input type="submit" name="btnPrev" class="btn btn-prev" value="Câu trước" /> -->
                        <?php if ($quiz->is_back_or_next) : ?>
                            <button type="submit" name="btnPrev" class="btn btn-prev btnnp">
                                <i class="icon-arrow-left icon"></i> Câu trước
                            </button>
                        <?php endif ?>
                    <?php endif; ?>
                    <?php if ($position < $session->maxPosition) : ?>
                        <!-- <input type="submit" name="btnNext" class="btn btn-next" value="Câu tiếp" /> -->
                        <button type="submit" name="btnNext" class="btn btn-next btnnp">
                            Câu tiếp <i class="icon-arrow-right icon"></i></button>
                    <?php endif; ?>
                <?php endif; ?>
                <input type="submit" name="btnFinish" class="btn btn-answer" value="Hoàn thành"/>
            </li>
        </ul>
    </div>
    <?php ActiveForm::end() ?>
    <script type="text/javascript">
        (function ($, window, document, undefined) {
            $('body').on('click', '.answer-content', function (){
                var id = $(this).data('id');
                $('#label-' + id).trigger('click');
            });

            $('.countdown-time-start').attr('style', 'display: block;');
            $('.countdown-time-start .question-summarry .current-pos').html(<?= $position ?>);

            $('body').on('click', '#form-quiz .btnnp', function () {
                $('#btn-submit').val($(this).attr('name'));

                return true;
            });

            $('body').on('click', '#form-quiz input[name=\'btnFinish\']', function () {
                $('#btn-submit').val($(this).attr('name'));

                var hasNoAnswerQuestion = false;

                $('.k-listing-characteristics').each(function (index) {
                    var flag = false;

                    var checkInputs = $(this).find('li.radio input, li.checkbox input');
                    if (checkInputs.length > 0) {
                        // single and multi choice
                        checkInputs.each(function (index) {
                            if ($(this).prop('checked')) {
                                flag = true;
                            }
                        });
                    } else {
                        var textArea = $(this).find('textarea');
                        if (textArea.length > 0) {
                            // essay question
                            if (textArea.val().length > 0) {
                                flag = true;
                            }
                        } else {
                            var connectQuestionEle = $(this).find('input.connect_question');
                            if (connectQuestionEle.length > 0) {
                                // connect question
                                if (connectQuestionEle.val() == 1) {
                                    flag = true;
                                }
                            } else {
                                var fillInInputs = $(this).find('input.fill-in-txt');

                                if (fillInInputs.length > 0) {
                                    // fillin question
                                    var subFlag = true;

                                    fillInInputs.each(function (index) {
                                        if ($(this).val().length <= 0) {
                                            subFlag = false;
                                            return false;
                                        }
                                    });

                                    if (subFlag) {
                                        flag = true;
                                    }
                                }
                            }
                        }
                    }

                    if (!flag) {
                        hasNoAnswerQuestion = true;
                        return false;
                    }
                });

                if (!hasNoAnswerQuestion) {
                    return true;
                }

                bootbox.confirm('Bạn chưa trả lời tất cả các câu hỏi. Tiếp tục nộp bài?', function (confirm) {
                    if (confirm) {
                        $('#form-quiz').submit();
                    }
                    return true;
                });

                return false;
            });
        })(window.jQuery, window, document);
    </script>
<?php endif; ?>
