<div class="scroll-box">
    <div class="detail-note show">
        <div class="title-note icon-sticky-note"><i class="icon-arrow-collapse-open hidden-sm-down"></i> #Ghi chú <?=$index?><i class="icon-arrow-up hidden-md-up pull-right"></i></div>
        <div class="content-note">
            <div style="word-wrap:break-word;"><?= $model->note ?></div>
        </div>
    </div>
</div>