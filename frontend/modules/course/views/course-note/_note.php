<?php
use yii\widgets\ListView;
use yii\bootstrap\Html;
use yii\helpers\Url;
$flag = false;
if(Yii::$app->devicedetect->isMobile() || Yii::$app->devicedetect->isTablet()){
    $flag = true;
}
?>
<style>
    @media (max-width: 767px){
        .main-lesson .group-top .pull-right {
            height: auto !important;
        }
    }
</style>

<?php if($flag == true) { ?>

<?= Html::beginForm(Url::toRoute('/course/course-note/add'), 'post', [
    'id' => 'add-note-form',
]) ?>
<?=  Html::hiddenInput('id', !empty($lesson_id) ? $lesson_id : Yii::$app->request->get('id')); ?>
<?= Html::hiddenInput('t', 0, [
    'id' => 'playback-time'
]); ?>
<?= Html::textarea('note', '', [
    'id' => 'note-input',
    'placeholder' => 'Nhập ghi chú và Enter để lưu lại',
    'class' => 'add-note hidden-md-up '
]) ?>
<button class="btn btn-add-note hidden-md-up">Thêm ghi chú</button>
<?= Html::endForm(); ?>
<?php } ?>
<?= ListView::widget([
    'dataProvider' => $noteDataProvider,
    'layout' => '{items}',
    'itemOptions' => [
        'tag' => false,
    ],
    'options' => [
        'class' => 'clearfix',
        'tag' => 'ul',
        'style' => 'margin-right: 5px;' // convert to scss after
    ],
    'itemView' => function ($model, $key, $index, $widget) {
        return $this->render('_note-item',[
            'model' => $model,
            'key' => $key,
            'index' => $index + 1
        ]);
    },
]) ?>
<?php if($flag == true) { ?>
<script type="text/javascript">
    $('#note-input').keypress(function (e) {

        if (e.which == 13) {
            var note = $('#note-input').val();
            if(empty(note) === true){
                alert('Bạn chưa nhập ghi chú');
                return false;
            }
            // player = $(".flowplayer").data("flowplayer");
            // console.log(player);
            var playbackTime = 0;
            if (typeof player !== 'undefined') {
                if (player != null) {
                    playbackTime = player.video.time;
                }
            }

            $('#add-note-form').find("#playback-time").val(playbackTime);
            $('#add-note-form').submit();
            return false;
        }
        //console.log(x);
    });
    $('#add-note-form').on("submit", function (e) {
        e.preventDefault();
        var note = $('#note-input').val();
        var $form = $(this),
            url = this.action,
            data = $form.serialize();
        if(empty(note) === true){
            alert('Bạn chưa nhập ghi chú');
            return false;
        }
        $.post(url, data, function (response) {
            console.log(response);
            if (response.success !== false) {
                $('#note-input').val("");
                updateNoteList(function () {
                    $('a[href="#note-tab-lesson"]').click();
                });
            }
        });
    });

    function empty(str)
    {
        if (typeof str == 'undefined' || !str || str.length === 0 || str === "" || !/[^\s]/.test(str) || /^\s*$/.test(str) || str.replace(/\s/g,"") === "")
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    var $noteList = $("#note-tab-lesson");

    function updateNoteList (callback) {
        $.ajaxSetup({'method': 'GET'});
        $noteList.load('/course/course-note/get?id=' + $noteList.data('id'), callback);
    }
</script>
<?php } ?>

