<?php

/* @var $this \yii\web\View */

$this->title = 'Chứng nhận số #'.$certNumber;

$this->registerMetaTag([
    'property' => 'fb:app_id',
    'content' => Yii::$app->facebook->app_id,
]);
$this->registerMetaTag([
    'property' => 'og:type',
    'content' => 'article',
]);
$this->registerMetaTag([
    'property' => 'og:title',
    'content' => $this->title.' cấp bởi Kyna.vn',
]);
$this->registerMetaTag([
    'property' => 'og:description',
    'content' => 'Chứng nhận '.$userName.' đã hoàn thành xuất sắc khóa học '.$courseName.' tại Kyna.vn',
]);
$this->registerMetaTag([
    'property' => 'og:url',
    'content' => $shareUrl,
]);
$this->registerMetaTag([
    'property' => 'og:image',
    'content' => $url,
]);

$this->registerCss("
    .sharebox {
        text-align: center;
    }
    .certificate-image {
        width: 100%;
    }
    .sharebox .button-facebook {
        background: #3b5998;
        color: white;
        border-bottom: 2px solid #344e86;
        border-top: none;
        border-right: none;
        border-left: none;
        display: block;
        border-radius: 3px;
        transition: background-color 100ms ease-in-out, color 100ms ease-in-out, border-color 100ms ease-in-out;
        height: 35px;
        line-height: 35px;
        padding: 0 !important;
        text-align: center;
        width: 100px;
        margin: auto;
    }
");

?>
<div style="padding: 70px 0 30px; position: relative; background-color: #fff" class="container">
    <p>
        <img src="<?= $url . '?v=' . time() ?>" alt="<?= $this->title ?>" class="certificate-image">
    </p>
    <div class="sharebox">

        <div class="fb-share-button" data-href="<?= $shareUrl ?>" data-layout="button_count" data-size="large" data-mobile-iframe="true">
            <a class="fb-xfbml-parse-ignore button-facebook" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?= urlencode($shareUrl) ?>&amp;src=sdkpreparse"><i class="icon-facebook"></i> Chia sẻ</a>
        </div>

    </div>
</div>