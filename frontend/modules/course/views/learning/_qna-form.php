<?php

use yii\bootstrap\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use common\helpers\CDNHelper;

?>
<div class="row-ask clearfix">
    <div class="avatar hidden-sm-down">
        <?= CDNHelper::image(Yii::$app->user->identity->avatarImage, [
            'alt' => "",
            'class' => 'img-responsive',
            'size' => CDNHelper::IMG_SIZE_AVATAR_SMALL,
            'resizeMode' => 'crop',
        ]) ?>
    </div><!--end .left-->
    <div class="box-ask">
        <div class="box-comment">
            <?php $form = ActiveForm::begin([
                'action' => Url::toRoute('/course/learning/qna/', ['id' => $courseId]),
                'id' => 'qna-post-form',
                'options' => [
                    'data-ajax' => true,
                ],
            ]); ?>
                <?= Html::activeHiddenInput($qnaModel, 'course_id') ?>
                <?= Html::activeHiddenInput($qnaModel, 'user_id') ?>
                <?= Html::activeTextarea($qnaModel, 'content', [
                    'rows' => '3',
                    'placeholder' => 'Nhập thắc mắc của bạn',
                    'class' => 'add-question',
                ]) ?>
                    <div class="name hidden-sm-down"><?= $user->profile->name ?></div>
                    <button type="submit" class="btn btn-ask">Gửi câu hỏi</button>
            <?php ActiveForm::end() ?>
        </div><!--end .box-comment-->
    </div><!--end .right-->
</div><!--end .form-->
