<?php

use kyna\course\lib\H5p;

$key = false;
if (!empty(Yii::$app->params['flowplayer'])) {
    $key = Yii::$app->params['flowplayer']['key'];
}

$analytics = false;
if (!empty(Yii::$app->params['googleAnalytics'])) {
    $analytics = Yii::$app->params['googleAnalytics'];
}

// get H5p content
$h5pLib = new H5p();
$h5pContent = $h5pLib->getContent($video);

?>

<div class="wrap-video-lesson col-xs-12">
    <?php if (!empty($h5pContent)): ?>
        <div class="h5p-iframe-wrapper">
            <iframe id="h5p-iframe-<?= $video ?>" class="h5p-iframe" data-content-id="<?= $video ?>" src="about:blank" frameBorder="0" scrolling="no"></iframe>        </div>
        <script>
            H5PIntegration = <?= json_encode($h5pContent['script']) ?>;
        </script>
        <?php foreach ($h5pContent['scriptLinks'] as $item): ?>
            <script src="<?= $item ?>"></script>
        <?php endforeach; ?>
        <?php foreach ($h5pContent['styles'] as $item): ?>
            <link rel="stylesheet" href="<?= $item ?>">
        <?php endforeach; ?>
    <?php endif; ?>
</div>
<style>
    @media (max-width: 991px) {
        #k-wrap-feedback, .zopim {
            display: none;
        }
    }
</style>
