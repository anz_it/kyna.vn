<?php
use app\modules\course\assets\components\LessonTree;
use yii\helpers\Url;
?>

    <?= LessonTree::widget([
        'query' => $course->getSectionTreeQuery(),
        'linkTemplate' => Url::toRoute(['/course/learning/index', 'id' => $course->id, 'section' => '{nodeId}']),
        'currentNodeId' => $sectionId,
        'wrapperOptions' => [
            'class' => 'scroll-box',
//            'role' => 'tabpanel'
        ],
//        'treeOptions' => [
//            'class' => 'detail-part show',
//        ],
    ]) ?>
