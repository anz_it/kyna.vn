<?php

use yii\web\View;
use yii\helpers\Url;

use app\modules\course\assets\BootboxAsset;
use kyna\course\models\CourseLesson;
use kyna\user\models\UserCourse;
use kyna\course\models\CourseRating;
use common\helpers\CDNHelper;

BootboxAsset::register($this);

$this->title = "Giáo trình khóa học - {$course->name}";
$isMobile = false;
if (Yii::$app->devicedetect->isMobile() && !Yii::$app->devicedetect->isTablet()) {
    $isMobile = true;
}

$this->registerCssFile('/css/font-awesome.min.css');
$this->registerCssFile('/css/kv-sortable.min.css');
$this->registerJsFile('/js/html.sortable.min.js');

$content_type = $lesson->video_type;

$ratingModel = CourseRating::findOne([
    'course_id' => $course->id,
    'user_id' => $userCourse->user_id
]);

$cdnUrl = CDNHelper::getMediaLink();

?>
<div class="<?php echo 'pull-right background-dark col-md-9 col-xs-12 ' .  'video_' . $content_type ?>">
    <div id="part-video" class="part-video col-xs-12">
        <div id="scroller">
            <div id="bar-scroll">
            <?php $number = 0; ?>
            <?php if ($lessonDataProvider->totalCount): ?>
                <?= $this->render('_lesson-list', [
                    'lessonDataProvider' => $lessonDataProvider,
                    'finishedLessons' => $finishedLessons,
                    'number' => $number
                ]) ?>
            <?php endif; ?>
            </div>
        </div>
        <div class="tool-bar">
            <div id="view-mode" class="view-mode">Xem kiểu &nbsp&nbsp<img class="list-view" alt="xem bình thường" src="<?= $cdnUrl ?>/img/list-view.png">&nbsp<img class="full-view" alt="xem toàn trang" src="<?= $cdnUrl ?>/img/full-view.png"></div>
            <div class="item-review">
                <span id="click-review-course" class="icon-star-list <?= $ratingModel != null ? 'done' : ''?>"><!-- thêm class done nếu đã review -->
                    <i class="icon icon-star <?= ($ratingModel != null && $ratingModel->score > 0 ? 'active' : '') ?>"></i>
                    <i class="icon icon-star <?= ($ratingModel != null && $ratingModel->score > 1 ? 'active' : '') ?>"></i>
                    <i class="icon icon-star <?= ($ratingModel != null && $ratingModel->score > 2 ? 'active' : '') ?>"></i>
                    <i class="icon icon-star <?= ($ratingModel != null && $ratingModel->score > 3 ? 'active' : '') ?>"></i>
                    <i class="icon icon-star <?= ($ratingModel != null && $ratingModel->score > 4 ? 'active' : '') ?>"></i>
                </span>
            </div>
            <div class="btn-tutorial" onclick="javascript: ActionLesson.TutorialClickPc();"><span><img src="<?= $cdnUrl ?>/img/tutorial.png" alt=""> Xem hướng dẫn</span></div>
        </div>
        <div class="pre"><i class="icon-arrow-left"></i></div>
        <div class="nex"><i class="icon-arrow-right"></i></div>
    </div>
    <?php
    echo $this->render('_review', ['model' => $course, 'ratingModel' => $ratingModel, 'userCourse' => $userCourse]);
    ?>
    <?php
    if ($canLearn || $isLearnQuick) {
        switch ($lesson->type) {
            case CourseLesson::TYPE_QUIZ:
                echo $this->render('_quiz', [
                    'session' => !empty($lesson->quiz) ? $lesson->quiz->getSession($user->id) : null,
                    'quiz' => !empty($lesson->quiz) ? $lesson->quiz : null,
                ]);
                break;

            case CourseLesson::TYPE_CONTENT:
                echo $this->render('_content', [
                    'content' => $lesson,
                ]);
                break;

            case CourseLesson::TYPE_VIDEO:

                if($lesson->video_type == CourseLesson::VIDEO_H5P)
                {

                    echo $this->render('_h5p', [
                        'video' => $lesson->processVideoLink(),
                        'lesson_id' => $lesson->id,
                        'user_course_id' => $userCourse->id,
                        'course_id' => $userCourse->course_id,
                        'title' => $lesson->name,

                    ]);


                } else if($lesson->video_type == CourseLesson::VIDEO_UIZA)
                {
                    echo $this->render('_uiza', [
                        'video' => $lesson->processVideoLink(),
                        'lesson_id' => $lesson->id,
                        'user_course_id' => $userCourse->id,
                        'course_id' => $userCourse->course_id,
                        'title' => $lesson->name,
                        'uiza_video_id' => $lesson->uiza_video_id
                    ]);
                } else {
                    echo $this->render('_video', [
                        'video' => $lesson->processVideoLink(),
                        'lesson_id' => $lesson->id,
                        'user_course_id' => $userCourse->id,
                        'course_id' => $userCourse->course_id,
                        'title' => $lesson->name,

                    ]);
                }
                break;

            default:

                break;
        }
    } else {
        echo $this->render('_alert', [
            'course' => $lesson->course,
            'lession' => $lesson,
            'user_id' => $user->id
        ]);
    }
    ?>
</div>
<div class="header-lesson hidden-md-up col-xs-12">
    <h5 class="title"><?= $course->name; ?></h5>
    <!--    <p class="courses">Khóa: <span>30 ngày để tự tin và giao tiếp hiệu quả</span></p>-->
</div>
<div class="wrap-menu-mobile clearfix col-md-3 col-xs-12" style="position: relative; z-index: 2">
    <div id="mobile-tab" class="mobile-tab col-xs-12">
        <ul class="nav nav-tabs sub-menu" id="mobile-tab-sub">
            <li><a data-toggle="tab" href="#content-tab-lesson">Nội dung</a></li>
            <li class="hidden-md-up"><a data-toggle="tab" href="#question-tab-lesson">Hỏi giảng viên</a></li>
            <li class="hidden-md-up"><a data-toggle="tab" href="#discuss-tab-lesson">Thảo luận</a></li>
            <li id="li-mission-tab"><a data-toggle="tab" href="#mission-tab-content" id="tab-mission">Nhiệm vụ</a></li>
            <li id="li-note-tab"><a data-toggle="tab" href="#note-tab-lesson">Ghi chú</a></li>
        </ul>
    </div>
    <div class="clearfix"></div>
    <div class="box-of-menutabs" id="content-tab-lesson">
        <?= $this->render('_sections', [
            'course' => $course,
            'sectionId' => $sectionId,
        ]) ?>
    </div>
    <div class="box-of-menutabs" id="mission-tab-content">
    </div>
    <?= $this->render('_note', [
        'lessonId' => $lesson->id,
        'isMobile' => $isMobile,
        'lesson' => $lesson,
    ]) ?>
    <div class="feedback-lesson hidden-md-down">
        <div class="banner-lesson large-banner">
            <a href="https://www.facebook.com/TuyenDungKyna/photos/a.824520251010427/1740723689390074/?type=1&theater" target="_blank">
                <img class="" src="<?= $cdnUrl ?>/img/lesson/banner-lesson.png" alt="">
            </a>
        </div>
        <div class="box-hotline icon-hot-line">
            <p class="title-hotline">Gọi <span>1900 6364 09</span></p>
            <p>Cần hỗ trợ hướng dẫn khi học</p>
        </div>
        <!--
        <div class="btn-feedback icon-feedback">Góp ý cho khóa học</div>
        -->
        <div class="socialmedia">
            <!-- Load Facebook SDK for JavaScript -->
            <div id="fb-root"></div>
            <script>
                (function (d, s, id) {
                    var js, fjs = d.getElementsByTagName(s)[0];
                    if (d.getElementById(id)) return;
                    js = d.createElement(s);
                    js.id = id;
                    js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.0&appId=191634267692814";
                    fjs.parentNode.insertBefore(js, fjs);
                }(document, 'script', 'facebook-jssdk'));
            </script>
            <div class="fb-like" data-href="<?= $actual_link = "http://$_SERVER[HTTP_HOST]/$_SERVER[REQUEST_URI]"; ?>"
                 data-layout="standard" data-width="200" data-action="like" data-size="small" data-show-faces="false"
                 data-share="true"></div>
        </div>
    </div>

    <script type="text/javascript">
        $('.wrap-quiz-lesson').on('scroll', function () {
            $('.countdown-time-start').css('top', $(this).scrollTop() - 30 + 'px');
        });

    </script>

    <!--   zopim chat-->
    <script type="text/javascript">
        window.$zopim || (function (d, s) {
            var z = $zopim = function (c) {
                    z._.push(c)
                },
                $ = z.s =
                    d.createElement(s),
                e = d.getElementsByTagName(s)[0];
            z.set = function (o) {
                z.set._.push(o)
            };
            z._ = [];
            z.set._ = [];
            $.async = !0;
            $.setAttribute("charset", "utf-8");
            $.src = "//v2.zopim.com/?1bMWnWprkt76mXJXRb6xBuP2dAD7uxPq";
            z.t = +new Date;
            $.type = "text/javascript";
            e.parentNode.insertBefore($, e)
        })(document, "script");

    </script>
    <script type="text/javascript">
        $(window).load(function () {
            $zopim(function () {
                $zopim.livechat.setLanguage('vi');
            });
            $zopim(function () {
                $zopim.livechat.window.hide();
            });
        });
    </script>
    <!--   end zopim chat-->
</div>
<?php
$script = "
;(function ($, window, document, undefined) {
    function loadMission() {
        $.ajax({
            url: '/course/mission/summary?course_id=" . $userCourse->course_id . "',
            method: 'GET',
            success: function (res) {
                $('#mission-tab-content').html(res);
            }
        });
    }


    $(document).ready(function () {
        $('body').on('click', 'a.btn-mission-tab', function () {
            $('#li-mission-tab a').trigger('click');
            $('html, body').animate({
                    scrollTop: $('#li-mission-tab').offset().top
                }, 2000);
        });

        $('body').on('click', '#li-mission-tab a', function () {
            loadMission();
        });
";

if ($messages = Yii::$app->session->getFlash('mission', false)) {
    $title = '<span class="label label-info" style="padding: 5px;">+' . $messages['point'] . ' KPOINT</span><br>';

    list($finishedMissions, $notFinishedMissions) = $userCourse->course->getSummaryMissionByUser($userCourse->user_id);
    $finishedMisssionsCount = count($finishedMissions);
    $totalMissionCount = count($notFinishedMissions) + $finishedMisssionsCount;

    $sumary = "{$finishedMisssionsCount}/{$totalMissionCount}";

    $message = '<b>' . $sumary . ':</b> Wohoo ~ Bạn vừa hoàn thành nhiệm vụ "' . $messages['missionName'] . '" <a class="btn-mission-tab" href="#" style="font-style: italic">» Nhiệm vụ</a>';

    $script .= "
        $.notify({
            title: '{$title}',
            message: '{$message}',
            allow_dismiss: true
        }, {
            type: 'notify-kpoint',
            animate: {
                enter: 'animated bounceInDown',
                exit: 'animated bounceOutUp'
            },
            placement: {
                from: 'bottom',
                align: 'right'
            }
        });

        loadMission();
    ";
}

if (($canLearn || $isLearnQuick)) {
    $script .= "
        $.ajax({
            url: '/course/learning/start-lesson',
            type: 'POST',
            data: {
                user_course_id: " . $userCourse->id . ",
                lesson_id: " . $lesson->id . "
            }
        });
    ";
}

if ((($canLearn || $isLearnQuick) && $lesson->type == CourseLesson::TYPE_CONTENT ) || ($lesson->type = CourseLesson::TYPE_VIDEO && $lesson->video_type == CourseLesson::VIDEO_H5P)) {
    $script .= "
        $.ajax({
            url: '/course/learning/end-lesson',
            type: 'POST',
            data: {
                user_course_id: " . $userCourse->id . ",
                lesson_id: " . $lesson->id . "
            },
            success: function (res) {
                if (res.notify) {
                    $.notify({
                        title: res.title,
                        message: res.message,
                        allow_dismiss: true,
                    }, {
                        type: 'notify-kpoint',
                        animate: {
                            enter: 'animated bounceInDown',
                            exit: 'animated bounceOutUp'
                        },
                        placement: {
                            from: 'bottom',
                            align: 'right'
                        }
                    });

                    loadMission();
                }
            }
        });
    ";
}

if ($userCourse->is_started == UserCourse::BOOL_NO) {
    // make started course by ajax
    $script .= "
        $.ajax({
            url: '/course/learning/start-course',
            type: 'POST',
            data: {
                course_id: " . $userCourse->course_id . "
            }
        });
    ";
}

$script .= "
    });
})(window.jQuery, window, document);";

$this->registerJs($script, View::POS_END, 'script-start-course');
?>
