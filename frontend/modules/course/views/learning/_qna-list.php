<?php
use yii\widgets\ListView;
?>
<?= ListView::widget([
    'dataProvider' => $dataProvider,
    'emptyText' => 'Hãy trở thành người đầu tiên đặt câu hỏi cho giảng viên!',
    'layout' => '{items}',
    'itemView' => '_qna-list-item',
]) ?>
