<?php
use yii\bootstrap\Html;
?>
<div class="wrap-content-lesson col-xs-12">
    <div class="text-content-lesson">
        <?=
        Html::decode($content->content);
        ?>
    </div>
</div>
