<?php

use yii\web\View;
use kyna\user\models\UserCourse;

$this->title = "Giáo trình khóa học - {$course->name}";
?>
<?php if ($course):?>
    <iframe id="embed_iframe" src="<?=$course->custom_content?>">
        <p>Your browser does not support iframes.</p>
    </iframe>
<?php endif; ?>

    <script type="text/javascript">
        $(document).bind('contextmenu',function(){
           return false;
        });
        $(document).keydown(function(event){
            if(event.keyCode==123){
                return false;
            }
            else if((event.ctrlKey && event.shiftKey && event.keyCode==73) || (event.ctrlKey && event.keyCode==83)){
                return false;  //Prevent from ctrl+shift+i
            }
        });
    </script>
<?php
    $css = "
        #fixed-topbar{
            display: none !important;
        }
        .main-lesson iframe {
            width: 100%;
            height: 100%;
            position: absolute;
        }
        .main-lesson .group-top {
            background-color: transparent;
        }
        main iframe {
            border: none;
            margin-top: 68px;
            display: block;
            width: 100%;
            height: 100vh;
            min-height: 500px;
            padding: 10px;
        }
    ";
$this->registerCss($css);

if ($userCourse->is_started == UserCourse::BOOL_NO) {
    // make started course by ajax
    $script = "
        ;(function ($, window, document, undefined) {
            $(document).ready(function () {
                $.ajax({
                    url: '/course/learning/start-course',
                    type: 'POST',
                    data: {
                        course_id: " . $userCourse->course_id . "
                    }
                });
            });
        })(window.jQuery, window, document);
    ";

    $this->registerJs($script, View::POS_END, 'script-start-course');
}

?>
