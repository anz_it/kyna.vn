<?php
use yii\helpers\Html;
use common\helpers\CDNHelper;

?>

<div class="row-ask clearfix">
    <div class="avatar hidden-sm-down">
        <a href="#">
            <i class="icon-send-question"></i>
            <?= CDNHelper::image($model->user->avatarImage, [
                'alt' => $model->user->profile->name,
                'data-holder-rendered' => true,
                'size' => CDNHelper::IMG_SIZE_AVATAR_SMALL,
                'resizeMode' => 'crop',
            ]) ?>
        </a>
    </div>
    <div class="box-ask">
        <div class="title">
            <span class="title-name">
                <?= $model->user->profile->name ?>
            </span>
            <span>
                <i class="icon-circle hidden-sm-down"></i>
            </span>
            <span class="title-date hidden-sm-down">
                <?= $model->postedTime ?>
            </span>
            <span class="title-date hidden-md-up">(<?= $model->postedTime ?>)</span>
        </div>
        <div class="ask-sentence">
            <?= (strip_tags($model->comment, '<p><a><br><font>')) ?>
        </div>
        <div class="row-action hidden-sm-down">
            <!--<span>
              1 <i class="icon-like"></i>
            </span>
            <span>
              <i class="icon-circle"></i>
            </span>-->
            <a href="#lesson-form-reply-<?= $model->id ?>">
                <span class="btn-reply-discuss courses-style">
                  <i class="icon-reply"></i> Trả lời
                </span>
            </a>
            <!--<span>
              <i class="icon-circle"></i>
            </span>
            <span class="courses-style">
              <i class="icon-follow"></i> Theo dõi
            </span>-->
        </div>

        <?= $this->render('_list_reply', ['model' => $model, 'csIds' => $csIds]) ?>

        <?= $this->render('_reply_form', ['model' => $model]) ?>
    </div>
    <!--end .media-body-->
</div>