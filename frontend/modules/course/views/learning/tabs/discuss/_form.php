<?php

use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\helpers\Html;
use kyna\course\models\CourseDiscussion;
use common\helpers\CDNHelper;

$user = Yii::$app->user->identity;

$mDiscuss = new CourseDiscussion();
$mDiscuss->course_id = $course->id;
$mDiscuss->user_id = $user->id;
?>

<div class="row-ask clearfix">
    <div class="avatar hidden-sm-down">
        <?= CDNHelper::image($user->avatarImage, [
            'alt' => $user->profile->name,
            'class' => 'img-responsive',
            'size' => CDNHelper::IMG_SIZE_AVATAR,
            'resizeMode' => 'crop',
        ]) ?>
    </div><!--end .left-->
    <div class="box-ask">
        <div class="box-comment">
            <?php $form = ActiveForm::begin([
                'id' => 'discuss-form',
                'action' => Url::toRoute(['/course/learning/discuss', 'id' => $course->id]),
                'options' => [
                    'data-ajax' => true,
                    'data-target' => '#discuss-content'
                ],
            ]) ?>
                <?= Html::activeHiddenInput($mDiscuss, 'course_id') ?>
                <?= Html::activeHiddenInput($mDiscuss, 'user_id') ?>
                <?= Html::activeTextarea($mDiscuss, 'comment', [
                    'rows' => '3',
                    'placeholder' => 'Nhập thảo luận bạn ở đây',
                    'class' => 'add-question',
                ]) ?>
                    <div class="name hidden-sm-down"><?= $user->profile->name ?></div>
                    <button type="submit" class="btn btn-ask">Gửi thảo luận</button>
            <?php ActiveForm::end() ?>
        </div><!--end .box-comment-->
    </div><!--end .right-->
</div><!--end .form-->
