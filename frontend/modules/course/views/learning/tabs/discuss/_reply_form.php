<?php

use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\helpers\Html;
use kyna\course\models\CourseDiscussion;
use common\helpers\CDNHelper;

$user = Yii::$app->user->identity;

$mDiscuss = new CourseDiscussion();
$mDiscuss->course_id = $model->course_id;
$mDiscuss->user_id = $user->id;
$mDiscuss->parent_id = $model->id;
?>

<div class="row-ask clearfix" id="lesson-form-reply-<?= $model->id ?>" role="group">
    <div class="avatar hidden-sm-down">
        <?= CDNHelper::image($user->avatarImage, [
            'alt' => $user->profile->name,
            'class' => 'media-object img-responsive',
            'data-holder-rendered' => true,
            'size' => CDNHelper::IMG_SIZE_AVATAR_SMALL,
            'resizeMode' => 'crop',
        ]) ?>
    </div><!--end .left-->
    <div class="box-ask">
            <?php $form = ActiveForm::begin([
                'id' => 'discuss-form',
                'action' => Url::toRoute(['/course/learning/discuss', 'id' => $model->course_id]),
                'options' => [
                    'data-ajax' => true,
                    'data-target' => '#listview-replies-' . $model->id
                ],
            ]) ?>  
                <?= Html::activeHiddenInput($mDiscuss, 'course_id') ?>
                <?= Html::activeHiddenInput($mDiscuss, 'user_id') ?>
                <?= Html::activeHiddenInput($mDiscuss, 'parent_id') ?>
                <?= Html::activeTextarea($mDiscuss, 'comment', [
                    'rows' => '3',
                    'placeholder' => 'Nhập trả lời của bạn ở đây',
                    'class' => 'add-question',
                ]) ?>
                    <div class="name hidden-sm-down"><?= $user->profile->name ?></div>
					<button type="reset" class="btn btn-default close-form">Hủy</button>
                    <button type="submit" class="btn btn-ask">Trả lời</button>
            <?php ActiveForm::end() ?>
    </div><!--end .right-->
</div><!--end .form-->
