<?php

use yii\widgets\ListView;
use yii\data\ArrayDataProvider;
use kyna\course\models\CourseDiscussion;

/**
 * @var $model \kyna\course\models\CourseDiscussion
 */
?>

<?= ListView::widget([
    'dataProvider' => new ArrayDataProvider(['allModels' => $model->getComments()->andWhere(['status' => CourseDiscussion::STATUS_ACTIVE])->all()]),
    'layout' => '<div class="content-sub">{items}</div>',
    'id' => 'listview-replies-' . $model->id,
    'itemView' => '_reply_item',
    'viewParams' => ['csIds' => $csIds],
    'emptyText' => false
]) ?>