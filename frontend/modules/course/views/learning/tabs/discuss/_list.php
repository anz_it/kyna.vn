<?php

use yii\widgets\ListView;

?>

<?= ListView::widget([
    'dataProvider' => $dataProvider,
    'layout' => '{items}',
    'itemView' => '_item',
    'viewParams' => ['csIds' => $csIds],
    'emptyText' => 'Hãy trở thành người đầu tiên tham gia thảo luận.',
    'options' => [
        'tag' => 'ul',
        'class' => 'media-list'
    ],
    'itemOptions' => [
        'tag' => 'li',
        'class' => 'media media-key'
    ],
]) ?> 