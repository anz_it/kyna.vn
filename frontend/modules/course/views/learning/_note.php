<?php

use yii\helpers\Url;
use yii\bootstrap\Html;
use yii\data\ActiveDataProvider;
use kyna\course\models\CourseLessionNote;

$noteDataProvider = new ActiveDataProvider([
    'query' => CourseLessionNote::find()->where(['course_lession_id' => $lessonId, 'user_id' => Yii::$app->user->id])
]);

?>
<?php if ($isMobile == false) : ?>
    <div class="box-of-menutabs" id="note-tab-lesson" data-id="<?= $lessonId ?>">
        <?= $this->render('@app/modules/course/views/course-note/_note', [
            'noteDataProvider' => $noteDataProvider,
            'flag' => $isMobile,
            'lesson_id' => $lessonId,
        ]) ?>
    </div>
    <?= Html::beginForm(Url::toRoute('/course/course-note/add'), 'post', [
        'id' => 'add-note-form',
    ]) ?>
    <?= Html::hiddenInput('id', $lessonId); ?>
    <?= Html::hiddenInput('t', 0, [
        'id' => 'playback-time'
    ]); ?>
    <?= Html::textarea('note', '', [
        'id' => 'note-input',
        'placeholder' => 'Nhập ghi chú và Enter để lưu lại',
        'class' => 'add-note hidden-sm-down'
    ]) ?>
    <?= Html::endForm(); ?>
    <script type="text/javascript">
        $('#note-input').keypress(function (e) {
            if (e.which == 13) {
                var note = $('#note-input').val();
                if(empty(note) === true){
                    alert('Bạn chưa nhập ghi chú');
                    return false;
                }
                // player = $(".flowplayer").data("flowplayer");
                // console.log(player);
                var playbackTime = 0;
                if (typeof player !== 'undefined') {
                    if (player != null) {
                        playbackTime = player.video.time;
                    }
                }
                $('#add-note-form').find("#playback-time").val(playbackTime);
                $('#add-note-form').submit();
                return false;
            }
        });
        $('#add-note-form').on("submit", function (e) {
            e.preventDefault();
            var $form = $(this),
                url = this.action,
                data = $form.serialize();
            var note = $('#note-input').val();
            if(empty(note) === true){
                alert('Bạn chưa nhập ghi chú');
                return false;
            }
            $.post(url, data, function (response) {
                console.log(response);
                if (response.success !== false) {
                    $('#note-input').val("");
                    updateNoteList(function () {
                        $('a[href="#note-tab-lesson"]').click();
                    });
                }
            });
        });

        function empty(str)
        {
            if (typeof str == 'undefined' || !str || str.length === 0 || str === "" || !/[^\s]/.test(str) || /^\s*$/.test(str) || str.replace(/\s/g,"") === "")
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        var $noteList = $("#note-tab-lesson");

        function updateNoteList(callback) {
            $.ajaxSetup({'method': 'GET'});
            $noteList.load('/course/course-note/get?id=' + $noteList.data('id'), callback);
        }
    </script>
<?php else: ?>
    <div class="box-of-menutabs" id="note-tab-lesson" data-id="<?= $lessonId ?>">
        <?= $this->render('@app/modules/course/views/course-note/_note', [
            'noteDataProvider' => $noteDataProvider,
            'flag' => $isMobile,
            'lesson' => $lesson,
            'lesson_id' => $lessonId,
        ]) ?>
    </div>
<?php endif ?>