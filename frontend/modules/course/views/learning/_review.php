<?php

use kyna\course\models\CourseRating;

/**
 * Created by PhpStorm.
 * User: macintosh
 * Date: 6/6/17
 * Time: 5:34 PM
 */

/* @var $userCourse \kyna\user\models\UserCourse */

if (!isset($ratingModel)) {
    $ratingModel = CourseRating::findOne([
        'user_id' => Yii::$app->user->id,
        'course_id' => $model->id
    ]);
}
$flag = false;
if(Yii::$app->devicedetect->isMobile() || Yii::$app->devicedetect->isTablet()){
    $flag = true;
}
?>
<div class="review-course" id="review-course">
    <div class="k-course-reviews-wrap">
        <div class="k-course-reviews-rate">
            <center>
                <?php if ($ratingModel == null) : ?>
                    <div class="tit-review">Các đánh giá của bạn giúp người học khác dễ dàng lựa chọn khoá học</div>
                    <div class="button">
                        <button id="show-review" type="submit" class="btn btn-submit-rating" style="margin-top: 20px;">Gửi đánh giá</button>
                        <br><br>
                        <button id="hide-review" type="submit" class="btn btn-ignore-rating">Bỏ qua đánh giá</button>
                    </div>
                <?php elseif ($ratingModel->status == CourseRating::STATUS_ACTIVE) : ?>
                    <p class="text-bottom" > Cám ơn bạn đã gửi đánh giá.</p >
                <?php else: ?>
                    <p class="text-bottom" > Cám ơn bạn đã gửi đánh giá. Đánh giá của bạn sẽ được Kyna.vn kiểm duyệt trong thời gian sớm nhất có thể.</p >
                <?php endif; ?>
            </center>
        </div>
    </div>
    <?php if ($ratingModel == null) : ?>
        <div class="k-course-reviews-wrap hide">
            <div class="k-course-reviews-rate">
                <p class="text-top">Đánh giá của bạn</p>
                <div id="course-rating-form-div">
                    <?php
                    $ratingModel = new CourseRating();
                    echo $this->render('@app/modules/course/views/default/rating/_form', ['model' => $ratingModel, 'course_id' => $model->id]);
                    ?>
                </div>
                <div class="button close-review-form">
                    <br>
                    <button id="return-courses" type="submit" class="btn btn-ignore-rating">Quay lại bài học</button>
                </div>
            </div><!--end .k-course-details-rate-->
        </div><!--end .k-course-details-wrap-->
        <script type="text/javascript">
            <?php if ($userCourse->isGraduated && !$userCourse->is_requested_rating) : ?>
                $('document').ready(function() {
                    $.ajax({
                        url: '/course/learning/request-rating',
                        method: 'POST',
                        data: {
                            'course_id': <?= $userCourse->course_id ?>,
                        },
                        success: function (res) {
                            if (res.result) {
                                $('#review-course').toggleClass('show');
                            }
                        }
                    });
                });
            <?php endif; ?>
            $('body').on('click', 'form .icon-star-list .icon-star', function(){
                var cur = $(this).parents('li');
                var cur_obj = $(cur).find('.icon-star').removeClass('active');
                var num_cur = $(this).index() + 1;
                $(this).parent().attr('rating',num_cur);
                for (var i = 0; i < num_cur; i++){
                    $(cur_obj[i]).addClass('active');
                }
                $('form[name="intro_form_review"] #' + $(cur).attr('for')).val(num_cur);
                var text = $(this).data('text');
                $(this).parent().find('text-des-active').html(text);
            })
            <?php if($flag == false) { ?>
            $('body').on('mouseover', 'form .icon-star-list .icon-star', function(){
                var _index = $(this).index() + 1;
                var obj = $(this).parent().find('.icon-star');
                var obj_hover = $(this).parent().find('.icon-star.active');
                var compare = obj_hover.length - _index;
                if (compare < 0)
                    for (var i = 0; i < _index; i++){
                        $(obj[i]).addClass('active');
                    }
                else
                    for (var i = _index; i < obj_hover.length; i++){
                        $(obj[i]).removeClass('active');
                    }
                var text = $(this).data('text');
                $(this).parent().find('text-des').show().html(text);
                $(this).parent().find('text-des-active').hide();
            }).on('mouseleave', 'form .icon-star-list', function(){
                var _index = $(this).attr('rating');
                if (_index == undefined){
                    $(this).find('.icon-star').removeClass('active');
                }
                else{
                    var obj = $(this).find('.icon-star');
                    var obj_hover = $(this).find('.icon-star.active');
                    var compare = obj_hover.length - _index;
                    if (compare < 0)
                        for (var i = 0; i < _index; i++){
                            $(obj[i]).addClass('active');
                        }
                    else
                        for (var i = _index; i < obj_hover.length; i++){
                            $(obj[i]).removeClass('active');
                        }
                }
                $(this).find('text-des').hide();
                $(this).find('text-des-active').show();
            })
            <?php } ?>
            $('#click-review-course, #return-courses').on('click', function(){
                $('#review-course').toggleClass('show');
                var cur_path = $('#review-course').find('.k-course-reviews-wrap')[0];
                if ($('#review-course .k-course-reviews-wrap form').length > 0){
                    $(cur_path).removeClass('hide');
                    $(cur_path).next('.k-course-reviews-wrap').addClass('hide');
                }
                else{
                    $(cur_path).addClass('hide');
                    $(cur_path).next('.k-course-reviews-wrap').removeClass('hide');
                }
                if($(window).width() < 543){
                    if ($('#review-course').hasClass('show')){
                        $('body').css('overflow','hidden');
                        $('main').addClass('popup-review-lesson');
                    }
                    else{
                        $('body').css('overflow','auto');
                        $('main').removeClass('popup-review-lesson');
                    }
                }
            });
            $('#show-review').on('click', function(){
                var cur_path = $(this).parents('.k-course-reviews-wrap');
                $(cur_path).addClass('hide');
                $(cur_path).next('.k-course-reviews-wrap').removeClass('hide');
            });

            $('#hide-review').on('click', function(){
                $('#review-course').toggleClass('show');
                if($(window).width() < 543 ){
                    $('body').css('overflow','auto');
                    $('main').removeClass('popup-review-lesson');
                }
            });
        </script>
    <?php endif; ?>
</div>
