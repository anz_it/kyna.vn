<?php
use yii\widgets\ListView;

?>

<?= ListView::widget([
    'dataProvider' => $lessonDataProvider,
    'layout' => '{items}',
    'itemOptions' => [
        'tag' => false,
    ],
    'options' => [
        'class' => 'clearfix',
        'tag' => 'ul',
        'id' => 'thelist'
    ],
    'itemView' => function ($model, $key, $index, $widget) use ($finishedLessons, &$number) {
        if (!empty($finishedLessons) && in_array($model->id, $finishedLessons)) {
            $finished = true;
        } else {
            $finished = false;
        }
        if ($model->type == 'video' || $model->type == 'content') {
            $number++;
        }
        return $this->render('_lesson-list-item', [
            'model' => $model,
            'index' => $index,
            'number' => $number,
            'finished' => $finished
        ]);
    },
]) ?>
<!-- khong lay duoc lesson thi de lesson dau la seeing-->
<script type="application/javascript">
    if ($('#thelist .seeing').length <= 0) {
        $('#thelist .clearfix a').first().addClass('seeing');
    }
</script>