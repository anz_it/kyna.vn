<?php

$key = false;
if (!empty(Yii::$app->params['flowplayer'])) {
    $key = Yii::$app->params['flowplayer']['key'];
}

$analytics = false;
if (!empty(Yii::$app->params['googleAnalytics'])) {
    $analytics = Yii::$app->params['googleAnalytics'];
}

?>

<div class="wrap-video-lesson col-xs-12">
        <?php
        echo \kyna\videomanager\widgets\videoplayer\Html5Player::widget([
            'video' => $video,
            'lessonId' => $lesson_id,
            'userCourseId' => $user_course_id,
            'courseId' => $course_id,
            'title' => $title,
            //'analytics' => $analytics,
        ]);
//        if ((Yii::$app->devicedetect->isMobile() || Yii::$app->devicedetect->isTablet())) {
//            echo \kyna\videomanager\widgets\videoplayer\Html5Player::widget([
//                'video' => $video,
//                'lessonId' => $lesson_id,
//                'userCourseId' => $user_course_id,
//                'title' => $title,
//                //'analytics' => $analytics,
//            ]);
//        } else {
//            echo \kyna\videomanager\widgets\videoplayer\FlashPlayer::widget([
//                'video' => $video,
//                'lessonId' => $lesson_id,
//                'userCourseId' => $user_course_id,
//                'title' => $title
//            ]);
//        }
        ?>
        <!-- <div class="popup-no-flash-player">
             <div class="box">
                 <p>Để xem bài học, bạn cần cài đặt phiên bản Adobe Flash Player mới nhất.<br>
                   Click icon hoặc link bên dưới để download phiên bản mới nhất và tiến hành cài đặt.</p>
                 <a href="https://get.adobe.com/flashplayer/"><img src="/img/icon-flash-player.png" alt=""><br>https://get.adobe.com/flashplayer/</a>
                 <div class="line"></div>
                 <p>Trong trường hợp đã cài đặt nhưng vẫn không xem được bài học, bạn vui lòng Bật tính năng Flash Player trong trình duyệt bạn đang sử dụng. Xem hướng dẫn tại <a href="/p/kyna/cau-hoi-thuong-gap#huong-dan-xem-bai-hoc-voi-flash-player-11">Hướng dẫn xem bài học với Flash Player</a><br>
                   Hoặc chat với nhân viên chăm sóc khách hàng ở góc phải trang để nhận được trợ giúp</p>
             </div>
         </div>
         <script type="text/javascript" src="/js/detect-flash-player.js"></script>
         <script language="javascript" type="text/javascript">
             if (pluginlist.indexOf("Flash")== -1)
                 $('.popup-no-flash-player').show();
         </script> -->
</div>
