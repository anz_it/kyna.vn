<?php

use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\helpers\Html;
use common\helpers\CDNHelper;
use kyna\course\models\CourseLearnerQna;

$user = Yii::$app->user->identity;

$mDiscuss = new CourseLearnerQna();
$mDiscuss->course_id = $model->course_id;
$mDiscuss->user_id = $user->id;
$mDiscuss->question_id = $model->id;

$avUrl = CDNHelper::image($model->user->avatarImage, [
    'size' => CDNHelper::IMG_SIZE_AVATAR_SMALL,
    'resizeMode' => 'crop',
    'returnMode' => 'url'
]);
?>

<div class="row-ask clearfix" id="qna-reply-<?= $model->id ?>" role="group">
    <div class="avatar hidden-sm-down">
        <img class="media-object img-responsive" alt="<?= $user->profile->name ?>" src="<?= $avUrl ?>" data-holder-rendered="true">
    </div><!--end .left-->
    <div class="box-ask">
        <?php $form = ActiveForm::begin([
            'id' => 'qna-reply-form-' . $model->id,
            'action' => Url::toRoute(['/course/learning/qna', 'id' => $model->course_id]),
            'options' => [
                'data-ajax' => true,
//                'data-target' => '#qna-replies-' . $model->id
            ],
        ]) ?>
        <?= Html::activeHiddenInput($mDiscuss, 'course_id') ?>
        <?= Html::activeHiddenInput($mDiscuss, 'user_id') ?>
        <?= Html::activeHiddenInput($mDiscuss, 'question_id') ?>
        <?= Html::activeTextarea($mDiscuss, 'content', [
            'rows' => '3',
            'placeholder' => 'Nhập trả lời của bạn ở đây',
            'class' => 'add-question',
        ]) ?>
        <div class="name hidden-sm-down"><?= $user->profile->name ?></div>
        <button type="reset" class="btn btn-default close-form">Hủy</button>
        <button type="submit" class="btn btn-ask">Trả lời</button>
        <?php ActiveForm::end() ?>
    </div><!--end .right-->
</div><!--end .form-->
