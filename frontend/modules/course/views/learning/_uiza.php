<?php

$key = false;
if (!empty(Yii::$app->params['flowplayer'])) {
    $key = Yii::$app->params['flowplayer']['key'];
}

$analytics = false;
if (!empty(Yii::$app->params['googleAnalytics'])) {
    $analytics = Yii::$app->params['googleAnalytics'];
}



?>

<div id='player' class="wrap-video-lesson col-xs-12">
    <script src='https://sdk.uiza.io/uizaplayer.js'></script>

    <!-- 2. The video player will replace this <div> tag. You should create css style for this tag -->

    <script>
        // 3. This code creates an UIZA player, and return the player for the callback
        UZ.Player.init(
            '#player', // player element, exp: #element_id, .element_class
            {
                api: 'Ly9reW5hLWFwaS51aXphLmlvL2FwaQ==',
                appId: 'f239da0369854d4bb8167fdd82b5c485',
                playerId: 'c74ad1d1-cadc-4cb3-b15a-cbbce42e177f',
              //  entityId: 'd145d8cd-4f6f-4cfc-bcb6-3e612f6d7da6',
                width: '100%', // player width, percentage or pixel, exp: '400px'
                entityId : '<?php echo  $video ?>',
                height: '100%', // player height, percentage or pixel, exp: '200px'
            },
            function(player) {
                // 4. You can add event listener at here
                player.on('play', function() {
                    console.log('play');
                });
                player.on('pause', function() {
                    console.log('pause');
                });
            }
        );
    </script>

</div>
