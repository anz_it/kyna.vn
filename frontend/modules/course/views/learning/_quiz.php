<?php

use yii\bootstrap\Html;
use yii\helpers\Url;
use kyna\course\models\QuizSession;
use kyna\course\models\Quiz;
use common\helpers\CDNHelper;

$cdnUrl = CDNHelper::getMediaLink();
?>
<div class="wrap-quiz-lesson col-xs-12">
    <div class="container clearfix" id="quiz-container">
        <?php if ($session != null && $quiz != null && !empty($quiz->details)) : ?>
            <div class="title col-xs-12">
                <h4><?= $quiz->name ?></h4>
                <p class="hidden-md-up">Khóa: <span class="courses"><?= $quiz->course->name; ?></span></p>
            </div>
            <div class="wrap-lesson col-xs-12">
                <?php
                    $array = [
                        QuizSession::STATUS_SUBMITTED,
                        QuizSession::STATUS_MARKED,
                        QuizSession::STATUS_WAITING_FOR_MARK
                    ];
                ?>
                <?php if (in_array($session->status, $array)) { ?>
                    <div class="ready">
                        <div class="box-quiz-test" id="quiz-content" style="display: block">
                            <?php if ($session->status !== QuizSession::STATUS_WAITING_FOR_MARK) : ?>
                                <div class="complete">
                                    <div class="iconComplete icon-check-mark-circle hidden-md-up"></div>
                                    <div class="title-alert hidden-md-up">Hoàn thành bài trắc nghiệm</div>
                                    <div class="result-quiz-lesson hidden-md-up">
                                        Số điểm đạt được: <span class="result-number"><?= $session->total_score ?>/<?= $session->total_quiz_score ?></span>
                                    </div>
                                    <div class="wrap-quote-result clearfix hidden-sm-down">
                                        <img alt="" src="<?= $cdnUrl ?>/img/lesson/icon-start.png" class="pull-left">
                                        <div class="pull-left">
                                            Số điểm đạt được
                                            <br />
                                            <span class="number"><span class="number-result"><?= $session->total_score ?></span>/<?= $session->total_quiz_score ?>(<?=  $session->total_quiz_score ? (number_format($session->total_score * 100 / $session->total_quiz_score, 2)) : 100 ?> %)</span>
                                            <br />
                                            <?php if ($session->is_passed) : ?>
                                                <div class="btn-result check">ĐẠT</div>
                                            <?php else: ?>
                                                <div class="btn-result">KHÔNG ĐẠT</div>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                </div>
                            <?php else: ?>
                                <h3> Bạn đã hoàn thành bài tập và đã nộp bài, vui lòng chờ giảng viên chấm điểm </h3>
                            <?php endif ?>
                            <div class="redo-container">
                                <?php
                                    $cannotRedo = $quiz->max_of_redo > 0 && intval($session->countOfRedo) >= intval($quiz->max_of_redo);
                                    $extrabBtnRedoText = !empty($quiz->max_of_redo) ? " ({$session->countOfRedo}/{$quiz->max_of_redo})" : '';

                                    echo Html::a("Làm lại{$extrabBtnRedoText}", $cannotRedo ? '#' : Url::toRoute(['/course/quiz/', 'id' => $quiz->id, 'status' => QuizSession::STATUS_REDO]), [
                                        'class' => 'next-quiz btn-redo' . ($cannotRedo ? ' btn-disabled' : ''),
                                        'data-ajax' => $cannotRedo ? false : true,
                                        'data-target' => '#quiz-content',
                                        'data-push-state' => 'false',
                                    ]);

                                if ($quiz->is_show_result_when_done_a_question == Quiz::BOOL_YES) {
                                    echo Html::a('Xem đáp án', Url::toRoute(['/course/quiz/result', 'id' => $session->id]), [
                                        'class' => 'btn-result btn-link pull-right',
                                        'data-ajax' => true,
                                        'data-target' => '#quiz-content',
                                        'data-push-state' => 'false',
                                    ]);
                                    echo "<div class='clearfix'></div>";
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                <?php } else { ?>
                    <div class="ready">
                        <?php
                            $time = $session->remainingTime;
                        ?>
                        <div class="quote-lesson hidden-sm-down">
                            <div class="wrap-quote">
                                <img src="<?= $cdnUrl ?>/img/lesson/icon-start.png" alt="" class="img-responsive media-object"/>
                                <div>
                                    Bạn sẽ có <span class="bold"><?= gmdate('H:i:s', $time) ?></span> để hoàn tất bài tập này.
                                    <br>
                                    Bấm "Bắt đầu trả lời" khi đã sẵn sàng.
                                </div>
                            </div>
                        </div>

                        <p class="countdown-time-start">
                            <span class="hidden-md-up">Thời gian:</span>
                            <?php if ($quiz->style_show == 'Từng câu một') : ?>
                                <span class="question-summarry"><span class="current-pos"><?= Yii::$app->session->get('last-position-of-quiz-' . $quiz->id) ?></span>/<?= $session->maxPosition ?> câu</span>
                            <?php endif; ?>
                            <span class="timer" data-time="<?= $time; ?>" stop="false">
                                <?= gmdate('H:i:s', $time); ?>
                            </span>
                        </p>

                        <?= Html::a('Bắt đầu trả lời', Url::toRoute(['/course/quiz/', 'id' => $quiz->id]), [
                           'class' => 'btn-start',

                           'data-ajax' => true,
                           'data-target' => '#quiz-content',
                           'data-push-state' => 'false',
                        ]) ?>
                        <div class="box-quiz-test" id="quiz-content">
                        </div>
                    </div>

                <?php } ?>
            </div><!--end .quiz-start-->
        <?php else : ?>
            <div class="title col-xs-12">
                Hiện tại nội dung bài tập đang được cập nhật. Học viên vui lòng quay lại sau.
            </div>
        <?php endif; ?>
    </div>
</div>