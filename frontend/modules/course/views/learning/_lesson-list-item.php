<?php
use yii\bootstrap\Html;
use yii\helpers\Url;

$urlParams = ['/course/learning/index'];
$urlParams = array_merge($urlParams, \Yii::$app->request->get());
$urlParams['lesson'] = $model->id;
$urlParams['title'] = $model->name;

if($model->type == 'video' || $model->type == 'content'){
    $text = $number;
    $class ='';
}else{
    $text = '';
    $class = 'quiz';
}

$class .= (\Yii::$app->request->get('lesson') == $model->id) ? ' seeing' : '';

if (is_null(\Yii::$app->request->get('lesson')) && ($index == 0)) {
    $class .= ' seeing';
}

if ($finished) {
    $class .= ' ' . 'icon-check-complete-lesson';
}
//$class = $finished ? 'icon-check-complete-lesson' : $class;



?>
<li class="clearfix">
<?= Html::a($text, Url::toRoute($urlParams), [
    'class' => $class,
    'data-video-iframe' => Url::toRoute(['/course/learning/video-frame', 'v' => $urlParams]),
]) ?>
</li>
