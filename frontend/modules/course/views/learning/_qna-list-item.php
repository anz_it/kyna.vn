<?php
use yii\data\ActiveDataProvider;
use yii\widgets\ListView;
use kyna\course\models\CourseLearnerQna;
use common\helpers\CDNHelper;

/** @var $model CourseLearnerQna */

$avUrl = CDNHelper::image($model->user->avatarImage, [
    'size' => CDNHelper::IMG_SIZE_AVATAR_SMALL,
    'resizeMode' => 'crop',
    'returnMode' => 'url'
]);
?>

<div class="row-ask clearfix">
    <div class="avatar hidden-sm-down">
        <a href="#">
            <i class="icon-send-question"></i>
            <img alt="<?= $model->user->profile->name ?>" src="<?= $avUrl ?>" data-holder-rendered="true"/>
        </a>
    </div>
    <div class="box-ask">
        <div class="title">
      <span class="title-name">
        <?= $model->user->profile->name ?>
      </span>
            <span>
        <i class="icon-circle hidden-sm-down"></i>
      </span>
            <span class="title-date hidden-sm-down">
        <?= $model->postedTime ?>
      </span>
            <span class="title-date hidden-md-up">(<?= $model->postedTime ?>)</span>
        </div>
        <div class="ask-sentence">
            <?= strip_tags($model->content) ?>
        </div>
        <?= ListView::widget([
            'dataProvider' => new ActiveDataProvider([ 'query' => $model->getAnswers()->andWhere(['is_approved' => CourseLearnerQna::BOOL_YES])]),
            'layout' => '{items}',
            'showOnEmpty' => false,
            'options' => [
                'class' => 'content-sub box-ask',
                'id' => 'qna-replies-' . $model->id
            ],
            'emptyText' => false,
            'itemView' => function($model) {
                $isTeacher = $model->is_on_behalf_of_teacher || $model->user_id == $model->course->teacher_id;
                $isUser = !$isTeacher && ($model->question == null || $model->user_id == $model->question->user_id);

                $tagStr = $isTeacher ? 'GV' : (!$isUser ? 'QTV' : '') ;
                $name = $isTeacher ? $model->course->teacher->profile->name : ($isUser ? $model->user->profile->name : 'Trợ lý học tập Kyna');
                $avUrl = CDNHelper::image($isTeacher ? $model->course->teacher->avatarImage : $model->user->avatarImage, [
                    'size' => CDNHelper::IMG_SIZE_AVATAR_SMALL,
                    'resizeMode' => 'crop',
                    'returnMode' => 'url'
                ]);

                return '<div class="row-ask clearfix">
                     <div class="avatar hidden-sm-down">
                        <a href="#">
                            <img class="media-object img-responsive" src="' . $avUrl . '" data-holder-rendered="true">
                        </a>
                     </div>
                     <div class="box-ask">
                        <div class="title">
                           <span class="title-name">' . $name . '</span>
                           ' . (!empty($tagStr) ? '
                            <span class="qtv" style="background:#ff6826; color:#fff; font-size: 14px; padding:0px 5px; display:inline-block">
                                ' . $tagStr . '
                            </span>' : '') . '
                        </div>
                        <div class="ask-sentence">'. $model->content .'</div>
                        <div class="row-action hidden-sm-down">
                        </div>
                     </div>
                  </div>';
            }
        ]) ?>
        <?php
        if ($model->user_id == Yii::$app->user->id) {
            echo $this->render('_qna_reply_form', ['model' => $model]);
        }
        ?>
    </div>
    <!--end .media-body-->
</div>
