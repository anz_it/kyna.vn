<?php

use common\helpers\FontAwesomeHelper;
use yii\grid\GridView;
use yii\grid\ActionColumn;
use yii\helpers\Url;

?>
<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'emptyText' => 'Khóa học này hiện không có tài liệu bổ sung.',
    'tableOptions' => ['class' => 'table'],
    'showHeader' => false,
    'summary' => false,
    'columns' => [
        [
            'attribute' => 'title',
            'format' => 'html',
            'value' => function ($model) {
                return FontAwesomeHelper::file($model->mime_type, ['3x', 'pull-left']).
                    '<strong>'.$model->title.'</strong><br>'.
                    '<em class="text-muted">'.Yii::$app->formatter->asSize($model->size).'</em>';
            },
        ],
        [
            'class' => ActionColumn::className(),
            'template' => '{view} {download}',
            'contentOptions' => [
                'class' => 'text-right'
            ],
            'urlCreator' => function ($action, $model) {
                $queryParams = [
                    'id' => $model->id,
                    'action' => $action
                ];
                $hash = $model->getHash($queryParams);
                return Url::toRoute(['/course/learning/document', 'id' => $model->id, 'action' => $action, 'hash' => $hash]);
            },
            'buttons' => [
                'view' => function ($url, $model) {
                    return '<a href="'.$url.'" class="btn btn-default" target="_blank"><i class="fa fa-file-o"></i> Xem</a>';
                },
                'download' => function ($url, $model) {
                    return '<a href="'.$url.'" class="btn btn-default"><i class="fa fa-cloud-download"></i> Download</a>';
                }
            ]
        ]
    ],
]) ?>
<script type="text/javascript">
    $( "#lesson-document-content .pagination a" ).on("click", function(e) {
        e.preventDefault();
        var url = $(this).attr("href");
        $.ajax({
            url: url,
        }).success(function(result) {
            $("#lesson-document-content").html(result);
        });
    });
</script>
