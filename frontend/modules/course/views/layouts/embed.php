<?php

use yii\helpers\Html;
use frontend\modules\course\assets\LearningAsset;
use frontend\widgets\HeaderWidget;
use frontend\widgets\FooterWidget;
use frontend\widgets\GaWidget;
use frontend\modules\course\widgets\FeedbackWidget;
use frontend\modules\course\widgets\LessonheadWidget;
use frontend\modules\course\widgets\ContentLessonWidget;

LearningAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <link rel="icon" href="/favo_ico.png"/>

    <!-- Google Tag Manager -->
    <script type="text/javascript" src= "<?= Yii::$app->params['media_link']?>/src/js/gtm.js?v=1508382297"></script>
    <script type="text/javascript" src="<?= Yii::$app->params['media_link']?>/src/js/fb-pixel.js?v=1521778876"></script>
    <script type="text/javascript" src="<?= Yii::$app->params['media_link']?>/src/js/headScript.js?v=1521778878"></script>
    <!-- End Google Tag Manager -->

</head>
<body>
<?php $this->beginBody() ?>
<?php echo Yii::$app->settings->bodyScript?>
<?= HeaderWidget::widget(['rootCats' => $this->context->rootCats]); ?>

<main>
    <?= $content ?>
</main>
<?= FooterWidget::widget(); ?>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
