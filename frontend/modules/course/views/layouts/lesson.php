<?php

use yii\helpers\Html;
use common\assets\BootstrapNotifyAsset;
use frontend\modules\course\assets\LearningAsset;
use frontend\widgets\FooterWidget;
use frontend\widgets\GaWidget;
use frontend\modules\course\widgets\FeedbackWidget;
use frontend\modules\course\widgets\LessonheadWidget;
use frontend\modules\course\widgets\ContentLessonWidget;
use common\helpers\CDNHelper;
use frontend\widgets\PopupWidget;
use frontend\widgets\BannerWidget;
use kyna\settings\models\Banner;
$cdnUrl = CDNHelper::getMediaLink();

LearningAsset::register($this);
BootstrapNotifyAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
        <link rel="icon" href="/favo_ico.png"/>

        <!-- Google Tag Manager -->
        <script type="text/javascript" src= "<?= Yii::$app->params['media_link']?>/src/js/gtm.js?v=1508382297"></script>
        <script type="text/javascript" src="<?= Yii::$app->params['media_link']?>/src/js/fb-pixel.js?v=1521778876"></script>
        <script type="text/javascript" src="<?= Yii::$app->params['media_link']?>/src/js/headScript.js?v=1521778878"></script>
        <!-- End Google Tag Manager -->

        <style>
            @media(max-width: 991px){
                .zopim{
                    display: none !important;
                }
            }
        </style>
    </head>
    <body>
        <?php $this->beginBody() ?>
            <?php echo Yii::$app->settings->bodyScript ?>


            <main>
                <section class="main-lesson clearfix">
                    <div class="wrap-top col-xs-12">
                        <?= LessonheadWidget::widget(['course' => !empty($this->context->course) ? $this->context->course : null ]); ?>
                        <div class="group-top col-xs-12 pd0">
                            <div class="container">
                                <?= $content ?>
                            </div>
                        </div>
                        <div class="container">
                            <?= ContentLessonWidget::widget([
                                'course' => !empty($this->context->course) ? $this->context->course : null,
                                'user'  => !empty($this->context->user) ? $this->context->user : null,
                                'lesson' => !empty($this->context->lesson) ? $this->context->lesson : null,
                                'qnaModel' => !empty($this->context->qnaModel) ? $this->context->qnaModel : null
                            ]); ?>
                        </div>
                    </div>
                    <div class="clearfix">

                    </div>
                    <div class="container">
                        <div class="banner-lesson small-banner">
                            <a href="https://www.facebook.com/TuyenDungKyna/photos/a.824520251010427/1740723689390074/?type=1&theater" target="_blank">
                                <img class="" src="<?= $cdnUrl ?>/img/lesson/banner-lesson.png" alt="">
                            </a>
                        </div>
                    </div>
                </section>
            </main>
            <?= FeedbackWidget::widget(); ?>
            <?= FooterWidget::widget(); ?>

            <?= PopupWidget::widget([
                'position' => PopupWidget::POSITION_LEARNING_COURSE,
                'course_id' => Yii::$app->request->get('id')
            ]) ?>
<!--            <div class="modal fade" id="popup-lesson">-->
<!--                <div class="modal-dialog">-->
<!--                    <div class="modal-content">-->
<!--                        <div class="modal-body">-->
<!--                            <div class="popup_body">-->
<!--                                <a href="https://kyna.vn/p/nhom-khoa-hoc/hoc-tieng-anh-qua-truyen-cung-ung-dung-farfaria/405782?utm_source=khoahocbanner&utm_medium=cpm&utm_campaign=farfaria_15_05_2018" target="_blank" title=""><img src="--><?//= $cdnUrl ?><!--/img/lesson/20180516-popup.png" alt=""></a>-->
<!--                                <img class="closePopup" src="--><?//= $cdnUrl ?><!--/img/lesson/btn-close.png" alt="">-->
<!--                            </div>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
        <?= BannerWidget::widget(['type' => Banner::TYPE_TOP_MY_COURSE_BOTTOM, 'course_id' => Yii::$app->request->get('id'), 'limit' => 20]) ?>

        <?php $this->endBody() ?>

        <?= $this->render('script-popup') ?>

    </body>
</html>
<?php $this->endPage() ?>
