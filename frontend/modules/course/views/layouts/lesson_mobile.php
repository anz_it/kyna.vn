<?php

use yii\helpers\Html;
use frontend\modules\course\assets\LearningAsset;
use frontend\widgets\HeaderWidget;
use frontend\widgets\FooterWidget;
use frontend\widgets\GaWidget;
use frontend\modules\course\widgets\FeedbackWidget;
use frontend\modules\course\widgets\LessonheadWidget;

LearningAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <link rel="icon" href="/favo_ico.png"/>

    <!-- Google Tag Manager -->
    <script type="text/javascript" src= "<?= Yii::$app->params['media_link']?>/src/js/gtm.js?v=1508382297"></script>
    <script type="text/javascript" src="<?= Yii::$app->params['media_link']?>/src/js/fb-pixel.js?v=1521778876"></script>
    <script type="text/javascript" src="<?= Yii::$app->params['media_link']?>/src/js/headScript.js?v=1521778878"></script>
    <!-- End Google Tag Manager -->

</head>
<body>
<?php $this->beginBody() ?>
<?php echo Yii::$app->settings->bodyScript?>
<?= HeaderWidget::widget(['rootCats' => $this->context->rootCats]); ?>

<main>
    <section class="main-lesson clearfix">
        <div class="wrap-top col-xs-12">
            <?= LessonheadWidget::widget(); ?>
            <div class="group-top col-xs-12 pd0">
                <div class="container">
                    <?= $content ?>
                </div>
            </div>
            <div class="container">
                <?= \frontend\modules\course\widgets\ContentLessonMobileWidget::widget([
                    'course' => !empty($this->context->course) ? $this->context->course : null,
                    'user'  => !empty($this->context->user) ? $this->context->user : null,
                    'lesson' => !empty($this->context->lesson) ? $this->context->lesson : null,
                    'qnaModel' => !empty($this->context->qnaModel) ? $this->context->qnaModel : null,
                    'lessionDataProvider' => !empty($this->context->lessionDataProvider) ? $this->context->lessionDataProvider : null,
                    'finishedLessons' => !empty($this->context->finishedLessons) ? $this->context->finishedLessons : null,
                    'sectionId' => !empty($this->context->sectionId) ? $this->context->sectionId : null,
                ]) ?>
            </div>
        </div>
    </section>
</main>
<?= FeedbackWidget::widget(); ?>
<?= FooterWidget::widget(); ?>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
