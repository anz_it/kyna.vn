
<script type="text/javascript">
    $(document).ready(function() {
        var session_user_id = 'popup-lesson-soroban' + '-<?php echo Yii::$app->user->id ?>';
        var link_path = '/lop-hoc';
        var fromDate = new Date("2017-08-21 00:00:00");
        var toDate = new Date("2018-09-03 23:59:59");
        setTimeout(function() {

            var display = false;
            var v_date = new Date();
            var year_month_day = v_date.getFullYear() + v_date.getMonth() + v_date.getDate();
            switch (link_path) {
                case '/lop-hoc':
                    if (typeof Cookies.get(session_user_id) == 'undefined') {
                        display = true;
                        Cookies.set(session_user_id, year_month_day, {
                            expires: 90,
                            path: link_path
                        });
                    } else {
                        if (parseFloat(year_month_day) == parseFloat(Cookies.get(session_user_id))) {
                            display = false;
                        } else {
                            display = true;
                            Cookies.set(session_user_id, year_month_day, {
                                expires: 90,
                                path: link_path
                            });
                        }

                    }
                    break;
                default:
                    display = true;
                    break;
            }
            if (display) {
                // && v_date >= fromDate && v_date <= toDate) {
                if (location.pathname.indexOf('/lop-hoc/894') > -1 ||
                    location.pathname.indexOf('/lop-hoc/895') > -1) {
                    $('#popup-lesson').modal();
                }
            }
        }, 1000);
        // Close popup
        $('.closePopup').click(function(){
          $('#popup-lesson').modal('hide');
        });
    });
</script>
