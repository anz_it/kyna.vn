<?php

use yii\helpers\Url;
use common\helpers\CDNHelper;

$style = <<<CSS
.k-details-fb-gg .k-details-cart-right p {
    text-align: center;
}
.k-details-fb-gg .k-details-cart-right p span {
    padding: 0;
    display: block;
}
CSS;
$this->registerCss($style);
$cdnUrl = CDNHelper::getMediaLink();

?>
<section>
    <div class="k-details-fb-gg">


        <div class="k-details-cart-right">
            <p>
                <span class="count-number"><?= \Yii::$app->cart->getCount() ?></span>
                <span><small>Khóa học</small></span>
            </p>
            <a href="<?= Url::toRoute(['/cart/default/index']) ?>"><img src="<?= $cdnUrl ?>/src/img/detail/icon-cart-detail.png" alt="Kyna.vn" class="img-responsive cart_anchor_right"></a>
        </div><!--end .detail-icon-cart-->
    </div>
    <div id="fb-root"></div>
    <script>(function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.10&appId=191634267692814";
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
</section>
