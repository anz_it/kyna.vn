<?php
use common\helpers\CDNHelper;
use kyna\course\models\Course;

$sectionTitle = ($model->type == Course::TYPE_COMBO) ? 'Nhóm khóa học liên quan' : 'Khóa học liên quan';
?>

<?php if (!empty($model->relatedCourses)): ?>
<section>
    <div class="k-course-details-related">
        <h3><?= $sectionTitle ?></h3>
        <ul class="row k-course-details-related-list">
            <?php foreach($model->relatedCourses as $course) : ?>
            <li class="col-sm-12 col-xs-6 list" data-id="<?= $course->id ?>" data-brand="<?= (!empty($course->teacher))? $course->teacher->profile->name : ''?>">
                <div class="item-list clearfix">
                    <div class="col-sm-4 col-xs-12 k-related-courses-img">
                        <a href="<?= $course->url ?>">
                            <?= CDNHelper::image($course->image_url, [
                                'alt' => $course->name,
                                'class' => 'img-fluid',
                                'size' => CDNHelper::IMG_SIZE_THUMBNAIL,
                                'resizeMode' => 'cover',
                            ]) ?>
                        </a>
                    </div>
                    <div class="col-sm-8 col-xs-12 k-related-courses-text">
                        <h6><a href="<?= $course->url ?>" title="<?= $course->name ?>"><?= $course->name ?></a></h6>
                        <ul>
                            <li>
                                <span><?= Yii::$app->formatter->asCurrency($course->sellPrice) ?></span>
                            </li>
                            <li>
                                <!--
                                <a href="#">
                                    <span><i class="icon icon-heart"></i>Quan tâm</span>
                                </a>
                                -->
                            </li>
                        </ul>
                    </div>
                </div>
            </li>
            <?php endforeach; ?>
        </ul>
    </div><!--end .k-related-courses-->
</section>
<?php endif; ?>