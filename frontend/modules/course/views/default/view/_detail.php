<?php
use app\modules\course\components\TreeView;
use yii\helpers\Url;
$cdnUrl = \common\helpers\CDNHelper::getMediaLink();
?>
<div id="k-course-details-curriculum" class="k-course-details-curriculum" path="scrolling">
    <h3 class="hidden-md-down">Nội dung khóa học</h3>
    <ul class="wrap-detail">
        <?php foreach ($model->sections as $key => $section) { ?>
            <?php if ($section->lvl == 0) { ?>
                <?php if($key != 0 ) { ?>
                        </ul>
                    </li>
                <?php } ?>
                <li class="part">
                    <h4 class="icon-arrow-down"><?= $section->name; ?></h4>
                    <ul>
            <?php } else { ?>
                <li>
                    <span><img src="<?= $cdnUrl ?>/img/icon-arrow-circle-right-line.png" alt=""><?= $section->name; ?></span>
                </li>
            <?php } ?>
        <?php } ?>
    </ul>
</div><!--end .k-course-details-curriculum-->
