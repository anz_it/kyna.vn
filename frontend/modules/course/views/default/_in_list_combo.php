<?php

use common\helpers\CDNHelper;
$combos = $model->listCombo;
if($combo = $model->checkHasManyCombo()){
}else{
    $combo = $combos[0];
}
$formatter = Yii::$app->formatter;
?>
<?php if(!empty($combo)):?>
    <div class="k-course-details-list-combo clearfix">
    <h4>Combo có khóa học này</h4>
        <div class="item">
            <?= CDNHelper::image($combo->image_url, [
                'alt' => $combo->name,
                'class' => 'img-fluid',
                'size' => CDNHelper::IMG_SIZE_THUMBNAIL_SMALL,
                'resizeMode' => 'cover',
            ]) ?>
            <div class="info">
                <a class="course-name" href="<?= $combo->url ?>" target="_blank" title="<?= $combo->name?>">
                    <?= $combo->name?>
                </a>
                <ul class="hidden-md-down">
                    <li><i class="icon icon-arrow-circle-right-line"></i><span>Khóa học: <?= $combo->getComboItems()->count() ?> khóa</span></li>
                    <li><i class="icon icon-user"></i><span>Giảng viên: <?= $combo->teacherCount ?> chuyên gia</span></li>
                    <li><i class="icon icon-certificate"></i><span>Cấp chứng nhận hoàn thành</span></li>
                </ul>
                <div class="price hidden-md-down">
                    <b><?= $formatter->asCurrency($combo->sellPrice) ?></b>
                    <del><?= $formatter->asCurrency($combo->oldPrice) ?></del>
                    <span class="red">-<?= $combo->discountPercent ?>%</span>
                </div>
            </div>
            <div class="price hidden-lg-up">
                <i class="fa fa-tags"></i> <b>Học phí: </b>
                <b><?= $formatter->asCurrency($combo->sellPrice) ?></b>
                <del><?= $formatter->asCurrency($combo->oldPrice) ?></del>
                <span class="red">-<?= $combo->discountPercent ?>%</span>
            </div>
        </div>
</div>
<?php endif;?>