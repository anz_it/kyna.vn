<?php

use yii\helpers\StringHelper;
use yii\helpers\Url;

use kyna\course_combo\models\CourseCombo;
use common\helpers\CDNHelper;
use app\widgets\YoutubePlayer;

$formatter = Yii::$app->formatter;

$this->title = "{$model->name} | Kyna.vn";

$itemCount = count($model->comboItems);
$comboModel = CourseCombo::findOne($model->id);
?>
<link href="/css/popup.css" rel="stylesheet"/>

<div class="modal-header">
    <button type="button" class="k-popup-lesson-close close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">×</span>
    </button>
    <h4><?= $model->name ?></h4>
</div>
<div class="modal-body clearfix">
    <h6>
        Khóa học commbo / Gồm <?= $itemCount ?> khóa học
    </h6>
    <!--end .detail-title-->
    <div class="col-md-6 col-xs-12 k-popup-lesson-content">
        <?= YoutubePlayer::widget(array(
            'thumbnailSize' => CDNHelper::IMG_SIZE_THUMBNAIL_YOUTUBE,
            'model' => $model
        )) ?>
        <!--end .videoWrapper-->
        <div class="clearfix"></div>
    </div>
    <!--end .k-popup-lesson-content-->

    <div class="col-md-6 col-xs-12 k-popup-lesson-detail">
        <div class="box-style">
            <span class="st-combo">COMBO</span>
        </div>
        <div class="k-popup-lesson-detail-price">
            <span class="bold">
                <?php if (!empty($model->discountAmount)) : ?>
                    <i class="fa fa-tags" aria-hidden="true"></i>
                    <span class="price-sell"><?= number_format($model->sellPrice, '0', '', '.') ?>đ</span>
                    <span class="price-old"><s><?= number_format($model->oldPrice, '0', '', '.') ?>đ</s></span>
                    <span class="price-percent">(-<?= $model->discountPercent ?>%)</span>
                <?php elseif (!empty($model->oldPrice)) : ?>
                    <i class="fa fa-tags" aria-hidden="true"></i>
                    <span class="price-sell"><?= number_format($model->oldPrice, '0', '', '.') ?>đ</span>
                <?php else : ?>
                    <span class="price-free">Miễn phí</span>
                <?php endif; ?>
            </span>
        </div>
        <div class="k-popup-lesson-info">
            <ul>
                <li><i class="icon icon-arrow-circle-right-line"></i> Khóa học: Gồm <?= $itemCount ?> khóa</li>
                <li><i class="icon icon-profile"></i> Giảng viên: <?= $comboModel->teacherCount ?> giảng viên</li>
                <li><i class="icon icon-certificate"></i> Cấp chứng nhận hoàn thành</li>
                <li id='fb-save-button'>
                    <a data-toggle="dropdown" href="#">
                        <div class="fb-save" data-uri="<?= Yii::$app->request->absoluteUrl ?>"></div>
                    </a>
                    <div class="popup-guide">
                        Khóa học sẽ được lưu vào phần <a href="https://facebook.com/saved" target="_blank">Saved</a> (Đã
                        lưu)
                        trong tài khoản Facebook của bạn. Bạn có thể tìm xem lại bất kì khi nào.
                    </div>
                </li>
            </ul>
        </div>

        <!--<ul class="k-popup-lesson-detail-care">
        </ul>-->

    </div>
    <div class="col-xs-12 k-popup-lesson-description">
        <p>
            <?php
                if (!empty($comboModel->benefit)) {
                    echo StringHelper::truncateWords($comboModel->benefit, 60);
                }
            ?>
            <a href="<?= $model->url ?>">» Xem chi tiết</a>
        </p>
    </div>
    <div class="row-action clearfix">
        <?php if ($newItem === '1') : ?>
        <div class="btn-action">
            <a class="buy-now combo btn-buy-now" href="javascript:" data-pid='<?= $model->id ?>'>MUA NGAY</a>
        <?php endif; ?>
        <?php if ($newItem === '3') : ?>
        <div class="status-course">
            <b><i class="fa fa-check-circle"></i> Đã thêm Combo này vào giỏ hàng</b>
        </div>

        <div class="btn-action">
            <a href="<?= Url::toRoute(['/cart/default/index']) ?>" class="btn-go-to-cart">
                Xem giỏ hàng và thanh toán
            </a>
        <?php endif; ?>
        <?php if ($newItem === '2') : ?>
        <div class="status-course">
            <b><i class="fa fa-check-circle"></i> Bạn đã mua Combo này</b>
        </div>

        <div class="btn-action">
            <a href="<?= Url::toRoute(['/trang-ca-nhan/khoa-hoc']) ?>" class="btn-go-to-courses">
                Vào lớp học
            </a>
        <?php endif; ?>
        </div>
    </div>
    <!--end .col-md-4 col-xs-12 left-->

</div>
<!--end .modal-body-->
