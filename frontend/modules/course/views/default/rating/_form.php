<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * Created by PhpStorm.
 * User: macintosh
 * Date: 6/6/17
 * Time: 10:58 AM
 */
/* @var $model \kyna\course\models\CourseRating */
?>
<?php $form = ActiveForm::begin([
    'action' => Url::toRoute(['/course/default/review/', 'course_id' => $course_id]),
    'id' => 'intro_form_review',
    'enableAjaxValidation' => true,
    'options' => [
        'data-ajax' => true,
        'name' => 'intro_form_review',
        'accept-charset' => 'utf-8',
        'data-target' => '#course-rating-form-div',
    ],
]); ?>
    <?= $form->errorSummary($model, ['class' => 'alert alert-warning']) ?>
    <ul>
        <li for="course">
            <span>Bài giảng</span>
            <span class="icon-star-list">
                <i class="icon icon-star <?= ($model->score_of_content >= 1 ? 'active' : '') ?>" data-text="(Kém)"></i>
                <i class="icon icon-star <?= ($model->score_of_content >= 2 ? 'active' : '') ?>" data-text="(Chưa tốt)"></i>
                <i class="icon icon-star <?= ($model->score_of_content >= 3 ? 'active' : '') ?>" data-text="(Đạt)"></i>
                <i class="icon icon-star <?= ($model->score_of_content >= 4 ? 'active' : '') ?>" data-text="(Khá)"></i>
                <i class="icon icon-star <?= ($model->score_of_content >= 5 ? 'active' : '') ?>" data-text="(Tốt)"></i>
                <text-des></text-des>
                <text-des-active></text-des-active>
            </span>
            <div class="popup-guide">
                Bao gồm nội dung bài giảng, cách sắp xếp mục lục bài, kiến thức trong bài, nội dung bài test …
                <close>x</close>
            </div>
        </li>
        <li for="video">
            <span>Video</span>
            <span class="icon-star-list">
                <i class="icon icon-star <?= ($model->score_of_video >= 1 ? 'active' : '') ?>" data-text="(Kém)"></i>
                <i class="icon icon-star <?= ($model->score_of_video >= 2 ? 'active' : '') ?>" data-text="(Chưa tốt)"></i>
                <i class="icon icon-star <?= ($model->score_of_video >= 3 ? 'active' : '') ?>" data-text="(Đạt)"></i>
                <i class="icon icon-star <?= ($model->score_of_video >= 4 ? 'active' : '') ?>" data-text="(Khá)"></i>
                <i class="icon icon-star <?= ($model->score_of_video >= 5 ? 'active' : '') ?>" data-text="(Tốt)"></i>
                <text-des></text-des>
                <text-des-active></text-des-active>
            </span>
            <div class="popup-guide">
                Bao gồm cách trình bày, chất lượng, hình ảnh trong video bài học …
                <close>x</close>
            </div>
        </li>
        <li for="teacher">
            <span>Giảng viên</span>
            <span class="icon-star-list">
                <i class="icon icon-star <?= ($model->score_of_teacher >= 1 ? 'active' : '') ?>" data-text="(Kém)"></i>
                <i class="icon icon-star <?= ($model->score_of_teacher >= 2 ? 'active' : '') ?>" data-text="(Chưa tốt)"></i>
                <i class="icon icon-star <?= ($model->score_of_teacher >= 3 ? 'active' : '') ?>" data-text="(Đạt)"></i>
                <i class="icon icon-star <?= ($model->score_of_teacher >= 4 ? 'active' : '') ?>" data-text="(Khá)"></i>
                <i class="icon icon-star <?= ($model->score_of_teacher >= 5 ? 'active' : '') ?>" data-text="(Tốt)"></i>
                <text-des></text-des>
                <text-des-active></text-des-active>
            </span>
            <div class="popup-guide">
                Bao gồm cách giảng dạy, phong cách dạy, giọng nói giảng viên …
                <close>x</close>
            </div>
        </li>
    </ul>
    <script>
        if ($(window).width() < 767){
            $('body').on('click','#intro_form_review ul li', function(){
                $('#intro_form_review ul li .popup-guide').removeClass('open');
                $(this).find('.popup-guide').addClass('open');
            })
            $('body').on('click','#intro_form_review ul li .popup-guide', function(e){
                e.stopPropagation();
                $(this).removeClass('open');
            })
            $(document).mouseup(function(e)
            {
                var check_icon_star = $('#intro_form_review ul li');
                if (!check_icon_star.is(e.target) && check_icon_star.has(e.target).length === 0)
                {
                    $(check_icon_star).find('.popup-guide').removeClass('open');
                }
            });
        }
    </script>
    <?= Html::activeHiddenInput($model, 'score_of_video', ['id' => 'video']) ?>
    <?= Html::activeHiddenInput($model, 'score_of_content', ['id' => 'course']) ?>
    <?= Html::activeHiddenInput($model, 'score_of_teacher', ['id' => 'teacher']) ?>
    <?= Html::activeTextarea($model, 'review_content', ['rows' => 3, 'placeholder' => 'Nhập đánh giá của bạn']) ?>
    <div class="button">
        <?= Html::submitButton('Gửi đánh giá', [
            'class' => 'btn btn-submit-rating',
        ]) ?>
    </div>
<?php ActiveForm::end() ?>
<p class="text-bottom">Các đánh giá của bạn giúp người học khác dễ dàng lựa chọn khóa học</p>
