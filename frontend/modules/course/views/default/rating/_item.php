<?php
/**
 * Created by PhpStorm.
 * User: macintosh
 * Date: 6/1/17
 * Time: 10:13 AM
 */

/* @var $model \kyna\course\models\CourseRating */
?>

<div class="info">
    <div class="bold name"><?= !empty($model->cheat_name) ? $model->cheat_name : (!empty($model->user->profile->name) ? $model->user->profile->name : $model->user->email) ?></div>
</div>
<p data-content="<?= htmlentities($model->review_content) ?>" class="not-show"></p>
<div class="time_rating clearfix">
    <div class="date"><span><i class="fa fa-clock-o" aria-hidden="true"></i></span><?= $model->postedTimeText ?></div>
    <span class="icon-star-list">
        <i class="icon icon-star <?= ($model->score >= 1 ? 'active' : '') ?>"></i>
        <i class="icon icon-star <?= ($model->score >= 2 ? 'active' : '') ?>"></i>
        <i class="icon icon-star <?= ($model->score >= 3 ? 'active' : '') ?>"></i>
        <i class="icon icon-star <?= ($model->score >= 4 ? 'active' : '') ?>"></i>
        <i class="icon icon-star <?= ($model->score >= 5 ? 'active' : '') ?>"></i>
    </span>
</div>

