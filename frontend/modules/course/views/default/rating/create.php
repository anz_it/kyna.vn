<?php

use yii\helpers\Url;
use kyna\course\models\CourseRating;

/* @var $model \app\models\Course */

$ratingTblName = CourseRating::tableName();
$sql = "select score, count(*) as totalUser
        from $ratingTblName where course_id = {$model->id}
        and status = " . CourseRating::STATUS_ACTIVE;
if ($model->is_disable_seeding) {
    $sql .= " AND is_cheat = " . CourseRating::BOOL_NO;
}
$sql .= " group by score";
$summaryRatings = Yii::$app->db->createCommand($sql)->queryAll();

$summaryData = [
    5 => 0,
    4 => 0,
    3 => 0,
    2 => 0,
    1 => 0,
    0 => 0,
];

foreach ($summaryRatings as $result) {
    $summaryData[$result['score']] = $result['totalUser'];
}

$flag = false;
if(Yii::$app->devicedetect->isMobile() || Yii::$app->devicedetect->isTablet()){
    $flag = true;
}
?>

<div class="k-course-reviews-wrap">
    <ul class="k-course-reviews-top">
        <li>Có <strong><?= $model->rating_users_count ?></strong> lượt đánh giá</li>
        <li> <?= $model->rating ?> <i class="icon-star"></i></li>
    </ul>

    <div class="k-course-reviews-progress">
        <ul class="k-course-reviews-progress-list">
            <?php foreach ($summaryData as $key => $data) : ?>
                <?php
                // don't show rating 0 if have no user make it
                if ($key == 0 && $data == 0) {
                    continue;
                }

                $percent = $model->rating_users_count > 0 ? round($data * 100 / $model->rating_users_count) : 0;
                ?>
                <li class="list">
                    <ul>
                        <li><?= $key ?> sao</li>
                        <li>
                            <div class="progress">
                                <progress class="progress progress-success" value="<?= $percent ?>" max="100"></progress>
                            </div><!--end .progress-->
                        </li>
                        <li><?= $data ?></li>
                    </ul>
                </li>
            <?php endforeach; ?>
        </ul>

    </div><!--end .k-course-details-reviews-progress-->

    <?php if (Yii::$app->user->isGuest) : ?>
        <div class="k-course-reviews-rate-register">
            <p>Đánh giá của bạn</p>
            <p>Để đánh giá khóa học, bạn cần <a href="<?= Url::to(['/user/security/login']) ?>" data-target="#k-popup-account-login" data-toggle="modal" data-ajax="" data-push-state="false">đăng nhập</a> Kyna.vn và là học viên của khóa này.</p>
        </div><!--end .k-course-details-rate-register-->
    <?php else : ?>
        <div class="k-course-reviews-rate">
            <p class="text-top">Đánh giá của bạn</p>
            <div id="course-rating-form-div">
                <?php
                if ($isLearnedCourse) {
                    // user have joined this course then can make a review.
                    $ratingModel = CourseRating::findOne([
                        'user_id' => Yii::$app->user->id,
                        'course_id' => $model->id
                    ]);
                    if ($ratingModel == null) {
                        $ratingModel = new CourseRating();
                        echo $this->render('_form', ['model' => $ratingModel, 'course_id' => $model->id]);
                    } elseif ($ratingModel->status == CourseRating::STATUS_ACTIVE) {
                        echo '<p class="text-bottom" > Cám ơn bạn đã gửi đánh giá.</p >';
                    } else {
                        echo '<p class="text-bottom" > Cám ơn bạn đã gửi đánh giá. Đánh giá của bạn sẽ được Kyna.vn kiểm duyệt trong thời gian sớm nhất có thể.</p >';
                    }
                } else {
                    // user don't join this course
                    echo '<p class="text-bottom">Để đánh giá khóa học, bạn cần là học viên của khóa này.</p>';
                }
                ?>
            </div>
        </div><!--end .k-course-details-rate-->
    <?php endif; ?>
</div><!--end .k-course-details-wrap-->
<script type="text/javascript">
    $('body').on('click', 'form .icon-star-list .icon-star', function(){
        var cur = $(this).parents('li');
        var cur_obj = $(cur).find('.icon-star').removeClass('active');
        var num_cur = $(this).index() + 1;
        $(this).parent().attr('rating',num_cur);
        for (var i = 0; i < num_cur; i++){
            $(cur_obj[i]).addClass('active');
        }
        $('form[name="intro_form_review"] #' + $(cur).attr('for')).val(num_cur);
        var text = $(this).data('text');
        $(this).parent().find('text-des-active').html(text);
    })
    <?php if($flag == false) { ?>
    $('body').on('mouseover', 'form .icon-star-list .icon-star', function(){
        var _index = $(this).index() + 1;
        var obj = $(this).parent().find('.icon-star');
        var obj_hover = $(this).parent().find('.icon-star.active');
        var compare = obj_hover.length - _index;
        if (compare < 0)
            for (var i = 0; i < _index; i++){
                $(obj[i]).addClass('active');
            }
        else
            for (var i = _index; i < obj_hover.length; i++){
                $(obj[i]).removeClass('active');
            }
        var text = $(this).data('text');
        $(this).parent().find('text-des').show().html(text);
        $(this).parent().find('text-des-active').hide();
    }).on('mouseleave', 'form .icon-star-list', function(){
        var _index = $(this).attr('rating');
        if (_index == undefined){
            $(this).find('.icon-star').removeClass('active');
        }
        else{
            var obj = $(this).find('.icon-star');
            var obj_hover = $(this).find('.icon-star.active');
            var compare = obj_hover.length - _index;
            if (compare < 0)
                for (var i = 0; i < _index; i++){
                    $(obj[i]).addClass('active');
                }
            else
                for (var i = _index; i < obj_hover.length; i++){
                    $(obj[i]).removeClass('active');
                }
        }
        $(this).find('text-des').hide();
        $(this).find('text-des-active').show();
    })
    <?php } ?>
</script>
