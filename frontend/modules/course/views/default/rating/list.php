<?php

use yii\widgets\ListView;
use common\helpers\CDNHelper;

/**
 * Created by PhpStorm.
 * User: macintosh
 * Date: 6/7/17
 * Time: 4:58 PM
 */

$cdnUrl = CDNHelper::getMediaLink();
?>

<?= ListView::widget([
    'dataProvider' => $dataProvider,
    'itemView' => '_item',
    'layout' => '{items}',
    'options' => [
        'tag' => 'ul',
        'class' => 'comments-box'
    ],
    'itemOptions' => [
        'tag' => 'li',
        'class' => 'box'
    ],
]) ?>

<?php if ($dataProvider->pagination->page < $dataProvider->pagination->pageCount - 1) : ?>
    <span class="button-more"><img class="loading-review" style="display: none" src="<?= $cdnUrl ?>/img/loading.gif" alt=""><br><button href="#">Xem thêm đánh giá</button></span>
<?php endif; ?>
