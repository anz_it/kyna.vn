<?php
/**
 * @var $this \yii\web\View
 */

use yii\web\View;
use yii\helpers\Url;

use kyna\course_combo\models\CourseCombo;
use common\helpers\CDNHelper;
use common\helpers\GaHtmlHelper;
use common\helpers\GoogleSnippetHelper;
use app\widgets\YoutubePlayer;
use frontend\widgets\TopTags;
use frontend\widgets\AnchorText;
use frontend\modules\course\widgets\TextNoVoucherFree;
use frontend\modules\course\widgets\DongGia;
$formatter = \Yii::$app->formatter;
$comboModel = CourseCombo::findOne($model->id);
$items = $model->getComboItems()->orderBy('position')->all();
$itemCount = count($items);

$cdnUrl = CDNHelper::getMediaLink();

$this->registerCss("
    .k-combo-details-header .k-combo-details-info .combo-info .bought-status {
        text-align: left;
        padding-left: 20px;
    }
    .k-combo-details-header .k-combo-details-info .combo-info .price-list {
        background-color: transparent;
        font-size: medium;
        #text-align: center;
        color: black;
        padding-left: 8px;
        padding-right: 0px;
        border-top: none;
        border-bottom: none;
    }
    .k-combo-details-header .k-combo-details-info .combo-info .button {
        margin-top: auto;
    }
    .k-combo-details-header .k-combo-details-info .combo-info .button {
        text-align: center;
    }
    .k-combo-details-header .k-combo-details-info .combo-info .button li{
        display: inline;
    }
    .k-combo-details-header .k-combo-details-info .combo-info .button .thanh-toan{
        background: #fb6a00;
        color: white;
    }
");

// Set breadcrumbs data
if (!empty($model)) {
    // Danh sach khoa hoc
    $this->params['breadcrumbs'][] = ['label' => 'Danh sách khóa học', 'url' => ['index']];
    // Khuyen mai
    $url = yii\helpers\Url::toRoute(['/'],true).'khuyen-mai/nhom-khoa-hoc/';
    $this->params['breadcrumbs'][] = [
        'label' => "Khuyến mãi",
        'url' => $url,
        'template' => GoogleSnippetHelper::renderBreadcrumbTemplate($url, "Khuyến mãi", 2)
    ];

    // Current course
    $url = yii\helpers\Url::toRoute(['/'],true).$model->slug;
    $this->params['breadcrumbs'][] = [
        'label' => $model->name,
        'template' => GoogleSnippetHelper::renderBreadcrumbTemplate($url, $model->name, 3, false)
    ];
}

// Set breadcrums css
$this->registerCss("
    @media (max-width: 640px) {
        .breadcrumb-container {
            display: none;
        }
    }
    .breadcrumb-container {
        margin: 0px;
        background-color: #fafafa;
    }
    .breadcrumb {
        border-radius: 0px;
        padding: 8px 0px;
        margin: 0px;
        background-color: inherit;
    }
    .breadcrumb > li + li::before {
        padding-right: .5rem;
        padding-left: .5rem;
        color: #a0a0a0;
        content: \"»\";
    }
    .breadcrumb > li {
        color: #666 !important;
        font-weight: bold;
    }
    .breadcrumb > li > a {
        color: inherit;
    }
    .breadcrumb > li.active {
        color: #666 !important;
        font-weight: normal;
    }
    .k-combo-title-md {
        padding-top: 10px;
    }

");

?>
<main>
    <div class="k-course-details-header container" data-id="<?= $model->id?>" data-course-type="<?= $model->type ?>">
        <div class="k-course-details-title">
            <h1><?= $model->name ?></h1>
        </div><!--end k-course-details-title-->
        <div class="col-xl-8 col-md-11 col-xs-12 k-course-details-video">
            <?= YoutubePlayer::widget(array(
                'thumbnailSize' => CDNHelper::IMG_SIZE_THUMBNAIL_YOUTUBE_LARGE,
                'model' => $model
            )) ?>
        </div>
        <!--end .k-combo-details-video-->

        <div class="col-xl-4 col-md-11 col-xs-12 k-course-details-info sidebar-info">
            <ul class="list">
                <li class="box-style hidden-lg-down">
                    <span class="st-combo">COMBO</span>
                </li>
                <li class="price-list">
                    <!-- Chuan bi thong so -->
                    <?php
                    $salePrice = $model->sellPrice;
                    if (!empty($model->oldPrice)) {
                        $originalPrice = $model->oldPrice;
                    } else {
                        $originalPrice = $salePrice;
                    }
                    $discountMoney = $originalPrice - $salePrice;
                    if ($originalPrice > 0) {
                        $discountRate = $discountMoney/$originalPrice;
                    } else {
                        $discountRate = 0;
                    }
                    ?>
                    <ul>
                        <!-- Case 1: course co discount -->
                        <!-- Hien thi: salePrice + oldPrice + discountRate -->
                        <!-- Case 1: course co discount -->
                        <!-- Hien thi: salePrice + oldPrice + discountRate -->
                        <?php if ($discountRate > 0) : ?>
                            <li>
                                <div class="hidden-xl-up"><b><i class="fa fa-tags"></i><span class="label"> Học phí</span></b></div>
                                <span class="price-sale"><b><i class="fa fa-tags hidden-lg-down"></i> <?=$formatter->asInteger($salePrice)?>đ</b></span>
                                <span class="price-old"><s>(<?=$formatter->asInteger($originalPrice)?>đ)</s></span>
                                <span class="price-discount"><span class="hidden-lg-down">(</span>-<?=$formatter->asPercent($discountRate)?><span class="hidden-lg-down">)</span></span>
                            </li>
                            <!-- Case 2: course khong co discount -->
                            <!-- Hien thi: salePrice -->
                        <?php elseif ($salePrice > 0) : ?>
                            <li>
                                <div class="hidden-xl-up"><b><i class="fa fa-tags"></i><span class="label"> Học phí</span></b></div>
                                <span class="price-sale"><b><i class="fa fa-tags hidden-lg-down"></i> <?=$formatter->asInteger($salePrice)?>đ</b></span>
                            </li>
                            <!-- Case 3: Others (e.g. sale_price = 0-->
                            <!-- Hien thi: Mien phi -->
                        <?php else: ?>
                            <li>
                                <span class="price-sale"><b>Miễn phí</b></span>
                            </li>
                        <?php endif; ?>
                    </ul>
                </li>
                <?= TextNoVoucherFree::widget(['course_id' => $model->id])?>
                <li class="info">
                    <ul class="detail-info">
                        <li><i class="icon icon-arrow-circle-right-line"></i><span>Bao gồm <?= $itemCount?> khóa học</span></li>
                        <li><i class="icon icon-user"></i><span>Giảng viên: <?= $comboModel->teacherCount ?> chuyên gia</span></li>
                        <li><i class="icon icon-certificate"></i><span>Cấp chứng nhận hoàn thành</span></li>
                    </ul>
                </li>
                <li class="bought-status">
                    <ul>
                    </ul>
                </li>
                <!-- Case 1: Da add khoa hoc + Da thanh toan - newItem = 2 -->
                <!-- Hien thi: "Ban da mua khoa hoc nay" -->
                <?php if ($newItem === '2') : ?>
                    <script>
                        $("li.bought-status > ul").html('<li class="text-success"><b><i class="fa fa-check-circle"></i> Bạn đã mua khóa học này</b></li>');
                    </script>
                    <!-- Case 2: Da add khoa hoc + Chua thanh toan  - newItem = 3 -->
                    <!-- Hien thi: "Da them khoa hoc nay vao gio hang" -->
                <?php elseif ($newItem === '3') : ?>
                    <script>
                        $("li.bought-status > ul").html('<li class="text-success"><b><i class="fa fa-check-circle"></i> Đã thêm khóa học này vào giỏ hàng</b></li>');
                    </script>
                    <!-- Case 3: Chua add khoa hoc + Chua thanh toan - newItem = 1 -->
                    <!-- Khong hien thi gi het -->
                <?php elseif ($newItem === '1') : ?>
                <?php endif; ?>


                <!-- Neu da mua khoa hoc roi - newItem = 2 -->
                <!-- Hide price list -->
                <?php if ($newItem === '2') : ?>
                    <script>
                        $("li.price-list").hide();
                    </script>
                <?php endif; ?>

                <li class="button">
                    <ul>
                        <li class="action-button">
                            <!-- Case 1: Chua add khoa hoc + Chua thanh toan - newItem = 1 -->
                            <!-- Button action: DANG KY HOC-->
                            <?php if ($newItem === '1') : ?>
                                <a class="btn-buy-now" data-toggle="dropdown" href="#" id="buy-now" data-pid="<?= $model->id ?>" style="text-align: center;"><b>MUA NGAY</b></a>
                                <?=
                                GaHtmlHelper::a('<b>Thêm vào giỏ hàng</b>', '#', [
                                    'data-pid' => $model->id,
                                    'class' => 'go-to-cart add-to-cart dang-ky-hoc hidden-md-down' . (!Yii::$app->user->isGuest ? ' register' : ''),
                                    'style' => 'text-align: center; margin-top: 10px;',
                                    'category' => 'CourseDetail',
                                    'action' => 'AddToCart',
                                    'label' => $model->name,
                                ])
                                ?>

                                <!-- Case 2: Da add khoa hoc + chua thanh toan - newItem = 3 -->
                                <!-- Button action: XEM GIO HANG & THANH TOAN -->
                            <?php elseif ($newItem === '3') : ?>
                                <a href="<?= Url::toRoute(['/cart/default/index']) ?>" class="btn-lg btn-block thanh-toan"><b>XEM GIỎ HÀNG VÀ THANH TOÁN</b></a>
                                <!-- Case 3: Da add khoa hoc + da thanh toan - newItem = 3 -->
                                <!-- Button action: VAO LOP HOC -->
                            <?php elseif ($newItem === '2') : ?>
                                <a href="<?= Url::toRoute(['/course/learning/index', 'id' => $model->id]) ?>"><div class="btn-success btn-lg btn-block vao-lop-hoc" ><b>VÀO LỚP HỌC</b></div></a>
                            <?php endif; ?>
                        </li>
                    </ul>
                </li>
                <li>
                    <ul class="social-media clearfix">
                        <li id='fb-save-button'>
                            <a data-toggle="dropdown" href="#">
                                <div class="fb-save" data-uri="<?= Yii::$app->request->absoluteUrl ?>"
                                     data-size="small"></div>
                            </a>
                            <ul class="popup-guide">
                                Khóa học sẽ được lưu vào phần <a href="https://facebook.com/saved" target="_blank">Saved</a>
                                (Đã lưu)
                                trong tài khoản Facebook của bạn. Bạn có thể tìm xem lại bất kì khi nào.
                            </ul>
                        </li>
                        <li class="like-share">
                            <div class="fb-like fb" data-href="<?= Yii::$app->request->absoluteUrl ?>"
                                 data-layout="button_count" data-action="like" data-size="small" data-show-faces="false"
                                 data-share="true"></div>
                            <div class="gg combo" style="display: inline; position: relative; top: 8px;">
                                <!-- Place this tag in your head or just before your close body tag. -->
                                <script src="https://apis.google.com/js/platform.js" async defer>
                                    {
                                        lang: 'vi'
                                    }
                                </script>

                                <!-- Place this tag where you want the +1 button to render. -->
                                <div class="g-plusone" data-size="medium"
                                     data-href="<?= Yii::$app->request->absoluteUrl ?>"></div>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
        <!--end .k-combo-details-info-->
    </div>
<!--end .k-combo-details-header-->

<!--<div class="mb-fixed-price">
    <strong><?/*= number_format($model->sellPrice, "0", "", ".") */?>đ</strong>
</div>-->
<!--<script type="text/javascript">
    $(window).scroll(function () {
        var vis = $(window).scrollTop() > ($('#buy-now').offset().top + $('#buy-now').height());
        $('.mb-fixed-price').css('display', vis ? 'block' : 'none');
    });
</script>-->
<!-- end .mb-fixed-price -->

    <div class="box-container">
        <div class="container">
            <div class="col-xl-8 col-md-11 col-xs-12">
                <div class="k-course-details-main main-container">
                    <div class="k-course-details-main-left">
                        <section>
                            <div id="k-combo-details-about" class="k-course-details-about" path="scrolling">
                                <ul class="list hidden-md-down">
                                    <li><a href="javascript:void(0);" link="#k-combo-details-about">Giới thiệu
                                            combo</a></li>
                                    <li><a href="javascript:void(0);" link="#k-combo-details-curriculum">Chi tiết
                                            combo</a></li>
                                    <li><a href="javascript:void(0);" link="#k-combo-details-comment">Bình luận</a></li>
                                </ul>
                                <div class="course-overview" id="k-combo-details-about">
                                    <input id="showmore" type="checkbox">
                                    <label for="showmore" class="showmore-combo"></label>
                                    <div class="text">
                                        <?php
                                        if (!empty($comboModel->description)) : ?>
                                            <span class="title">Mô tả</span>
                                            <?= $comboModel->description ?>
                                        <?php endif ?>
                                        <?php
                                        if (!empty($comboModel->benefit)) : ?>
                                            <span class="title">Lợi ích</span>
                                            <?= $comboModel->benefit ?>
                                        <?php endif ?>
                                        <?php if (!empty($comboModel->object)) : ?>
                                            <span class="title">Đối tượng</span>
                                            <?= $comboModel->object ?>
                                        <?php endif ?>
                                    </div>
                                </div>
                                <!-- end .combo-overview -->
                            </div>
                            <!--end .k-combo-details-about-->
                        </section>
                        <!-- end combo about -->
                        <section>
                            <div id="k-combo-details-list" class="k-combo-details-list" path="scrolling">
                                <div id="k-combo-details-curriculum" class="k-combo-details-item" path="scrolling">
                                    <h3 class="title">Chi tiết combo</h3>
                                    <div class="panel-group" id="accordion-combo">
                                        <?php foreach ($items as $key => $item) : ?>
                                            <div class="panel item">
                                                <div class="item-heading">
                                                        <a aria-expanded="<?= ($key == 0 ? 'true' : 'false') ?>" data-parent="#accordion-combo"
                                                           data-toggle="collapse"
                                                           href="#collapse-<?php echo $key; ?>">
                                                            <span class="index"><?php echo $key + 1 ?>. </span>
                                                            <span><?= $item->course->name; ?></span>
                                                        </a>
                                                        <p class="lecture-name">
                                                            Giảng viên: <a href="<?= Url::toRoute(['/user/teacher/index', 'slug' => $item->course->teacher->slug]); ?>"><?= $item->course->teacher->profile->name ?></a>
                                                        </p>

                                                </div>
                                                <!-- end .item-heading -->
                                                    <div class="item-body collapse<?= ($key == 0 ? ' in' : '') ?>"
                                                         id="collapse-<?php echo $key; ?>">
                                                        <div class="detail-info">
                                                            <ul>
                                                                <li><i class="icon icon-clock"></i><span>Thời lượng: <?= $item->course->getTotalTimeText(false) ?></span>
                                                                </li>
                                                                <li>
                                                                    <i class="icon icon-arrow-circle-right-line"></i><span>Bài học: <?= $item->course->lessonCount; ?> bài</span>
                                                                </li>
                                                                <li><i class="icon icon-user"></i><span>Trình độ: <?= $item->course->levelText ?></span>
                                                                </li>
                                                                <li><i class="icon icon-certificate"></i><span>Cấp chứng nhận hoàn thành</span>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                        <p>Giá gốc: <b style="color: #e25f00;"><?= $formatter->asCurrency($item->course->oldPrice) ?></b></p>

                                                        <?= !empty($item->course->overview) ? $item->course->overview : $item->course->description ?>
                                                    </div>
                                                <!-- end .item-body -->
                                            </div>
                                            <!-- end .item -->
                                        <?php endforeach; ?>
                                    </div>
                                    <!-- end list-items -->
                                </div>
                                <!--end .k-combo-details-item-->
                            </div>
                            <!--end .k-combo-details-list-->
                            <script type="text/javascript">
                                $('#accordion').find('a').on('click', function () {
                                    $('.collapse').hasClass('in').collapse('hide');
                                })
                            </script>
                        </section>
                        <!-- end combo list -->

                    </div>
                    <!--end .k-combo-details-main-left-->
                </div>
                <!--end .k-combo-details-main-->
            </div>
            <!-- end left -->

            <div class="col-xl-4 col-md-11 col-xs-12 k-combo-details-main-right">
                <?php echo $this->render('view/_related_courses', ['model' => $model]) ?>
            </div>
            <!--end right-->
        </div>
    </div>
    <!-- end combo-intro -->

    <section id="k-combo-details-comment" path="scrolling">
        <div class="container">

            <div class="comments-facebook-wrap">
                <h4 class="title-content">Bình luận</h4>
                <div class="fb-comments fb_iframe_widget fb_iframe_widget_fluid"
                     data-href="<?= Yii::$app->request->absoluteUrl ?>" data-width="100%" data-numposts="10"
                     data-colorscheme="light" fb-xfbml-state="rendered"></div>
            </div>

        </div>
    </section>

<?= $this->render('view/_like_facebook') ?>

<!-- $this->render('view/_card_slide') -->

<div class="hidden-course-bar" data-spy="scroll" data-target="#scrollspy-combo" data-offset-top="100">
    <div class="container">
        <div class="row flex-ae">
            <div class="col-lg-8 col-sm-12 fs12-lg" id="scrollspy-combo">
                <ul class="tab-btns nav">
                    <li class="active"><a href="javascript:void(0);" link="#k-combo-details-about"
                                          class="scroll-detail-about nav-link">Giới thiệu combo</a></li>
                    <li><a href="javascript:void(0);" link="#k-combo-details-curriculum"
                           class="scroll-curriculum nav-link">Chi tiết combo</a></li>
                    <li><a href="javascript:void(0);" link="#k-combo-details-comment"
                           class="scroll-detail-author nav-link">Bình luận</a></li>
                    <!-- <li><a href="javascript:void(0);" link="#k-combo-details-comment" class="scroll-detail-comment nav-link">Đánh giá &amp; bình luận</a></li> -->
                </ul>
            </div>

            <div class="col-lg-4 hidden-sm">
                <ul class="button">
                    <li>
                        <a class="btn-buy-now" href="#">Mua ngay</a>
                    </li>
                    <!-- <li>
                        <a href="#" class="button-care"><i class="icon-heart icon"></i>Quan tâm</a>
                    </li> -->

                </ul>
            </div>
        </div>
    </div>
</div>
</main>
<?php $this->registerJsFile($cdnUrl . '/src/js/add-to-cart.js', ['position' => View::POS_END]); ?>
