<?php
/**
 * Created by PhpStorm.
 * User: nguyenphanngu
 * Date: 9/13/17
 * Time: 2:19 PM
 */

use yii\helpers\StringHelper;
use common\helpers\CDNHelper;

$formatter = \Yii::$app->formatter;
$flag = false;
if (Yii::$app->devicedetect->isMobile() || Yii::$app->devicedetect->isTablet()) {
    $flag = true;
}
if (is_array($model)) {
    $model = (object)$model;
}
$url = \yii\helpers\Url::toRoute('/danh-sach-khoa-hoc/'.$model->slug, true);
?>

<div class="k-box-card-wrap-custom k-box-card-wrap hot-category">
        <div class="img">
                <?= CDNHelper::image($model->images_thumb, [
                'alt' => $model->name,
                'class' => 'img-fluid',
                'size' => CDNHelper::IMG_SIZE_ORIGINAL,
                'resizeMode' => 'cover',
                ]) ?>
        </div>

    <div class="overlay"></div>
    <a href="<?= $url ?>" class="name">
        <span>
            <?= $model->name?>
        </span>
    </a>

</div>