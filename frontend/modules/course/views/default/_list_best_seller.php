<?php
/**
 * Created by PhpStorm.
 * User: nguyenphanngu
 * Date: 9/8/17
 * Time: 10:57 AM
 */

/**
 * @var $this \yii\web\View
 */

use yii\helpers\StringHelper;
use common\helpers\CDNHelper;
use kyna\course\models\Course;
use yii\widgets\ListView;
use yii\web\View;

$formatter = \Yii::$app->formatter;
?>

<?= ListView::widget([
    'dataProvider' => $dataProvider,
    'layout' => "{items}\n",
    'itemView' => function ($model) {
        $model = (object)$model;
        return $this->render('_box_category', ['model' => $model]);
    },
    'options' => [
        'tag' => 'ul',
        'class' => 'k-box-card-list-category best-seller-categories-container w3'
    ],
    'itemOptions' => [
        'tag' => 'li',
        'class' => 'col-xl-4 col-lg-6 col-xs-6 k-box-card'
    ],
//    'pager' => [
//        'prevPageLabel' => '<span>&laquo;</span>',
//        'nextPageLabel' => '<span>&raquo;</span>',
//        'options' => [
//            'class' => 'pagination',
//        ]
//    ]
])
?>