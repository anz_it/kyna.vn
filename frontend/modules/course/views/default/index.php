<?php

use yii\web\View;
use yii\helpers\Url;
use yii\widgets\ListView;
use yii\widgets\LinkPager;

use app\models\Course;
use frontend\widgets\BannerWidget;
use kyna\settings\models\Banner;

use common\helpers\Html;
use common\helpers\GoogleSnippetHelper;
use common\helpers\CDNHelper;
use frontend\widgets\FacetMobile;
use \app\widgets\CategoryWidget;

/**
 * @var $this \yii\web\View
 */

$cdnUrl = CDNHelper::getMediaLink();

if (empty($catModel) && empty($tag)) {
    if (!empty($q)) {
        $this->title = "Kyna.vn - Học online cùng chuyên gia";
    }
}

$sort = Yii::$app->request->get('sort', isset($_GET['q']) && trim($_GET['q']) != '' ? '_score' : 'new');
$totalCourse = $dataProvider->totalCount;

//Set breadcrumbs data
if (!empty($catModel)) {
    // Co Category

    // Danh sach khoa hoc
    $this->params['breadcrumbs'][] = ['label' => 'Danh sách khóa học', 'url' => ['index']];

    // Category list
    // Get category list
    $catList = [];
    $curCatModel = $catModel->parent;
    while (!empty($curCatModel)) {
        $catList[] = $curCatModel;
        $curCatModel = $curCatModel->parent;
    }
    // Add to breadcrumbs
    $position = 2;
    $url = Url::toRoute(['index'], true);
    while (!empty($catList)) {
        $curCatModel = array_pop($catList);
        $url = $url . "/" . $curCatModel->slug;
        $this->params['breadcrumbs'][] = [
            'label' => $curCatModel->name,
            'url' => ['index', 'slug' => $curCatModel->slug],
            'template' => GoogleSnippetHelper::renderBreadcrumbTemplate($url, $curCatModel->name, $position),
        ];
        $position += 1;
    }

    // Current category
    $url = $url = $url . "/" . $catModel->slug;
    $this->params['breadcrumbs'][] = [
        'label' => $catModel->name,
        'template' => GoogleSnippetHelper::renderBreadcrumbTemplate($url, $catModel->name, $position, true),
    ];
} elseif (!empty($tag)) {
    // Co tags
    $url = Url::toRoute(['/'], true) . 'tag/' . $tag->tag;
    $this->params['breadcrumbs'][] = [
        'label' => 'Chủ đề: ' . $tag->tag,
        'template' => GoogleSnippetHelper::renderBreadcrumbTemplate($url, 'Chủ đề: ' . $tag->tag, 2, true),
    ];
} elseif ($isCombo) {
    // Trang khuyen mai

    // Danh sach khoa hoc
    $this->params['breadcrumbs'][] = ['label' => 'Danh sách khóa học', 'url' => ['index']];
    // Current category
    $url = Url::toRoute(['/'], true) . 'khuyen-mai/nhom-khoa-hoc';
    $this->params['breadcrumbs'][] = [
        'label' => 'Khuyến mãi',
        'template' => GoogleSnippetHelper::renderBreadcrumbTemplate($url, "Khuyến mãi", 2, true),
    ];
} else {
    // Con lai: danh sach khoa hoc ---> cai nay nghi ky lai xem co on khong
    $this->params['breadcrumbs'][] = "Danh sách khóa học";
}
// Set breadcrumbs css
$this->registerCss("

    .k-box-card-list .k-box-card .k-box-card-wrap {
        border: 1px solid #ddd;
    }
    
    h3.mobile{
        display: none !important;
        font-weight: bold;
    }
    
    
    .k-box-card-list .k-box-card .k-box-card-wrap {

    }
    #best-seller-courses div.hr {
        border: 1px solid #ddd !important;
        margin-bottom: 25px !important;
    }
    .best-seller-categories-container{
        margin-top: 1.3rem !important;
    }
    
            
    #best-seller-courses h2 {
        display: block;
        color: #50ad4e;
        font-size: 18px;
        padding-left: 15px;
        font-weight: bold;
    }
    
    
    @media (max-width: 767px) {
    
        #banner-campaign-category h3 {
            display: block;
            font-weight: bold;
            padding-left: 15px;
        }
    
        .hot-category .img {
            width: 100% !important;
            box-shadow: none !important;
        }
        
        .k-listing-content span.menu-heading {
            margin-top: -11px !important;
        }
    
        
        .slick-dotted.slick-slider {
            margin-bottom: 5px !important;
        }
    
        .slick-slider {
            margin-top: 0 !important;
        }
    
        #banner-campaign-category .sm-banner {
            padding-bottom: 15px;
            display: none;
        }
        
        .hot-category {
            border: 0px solid #ddd !important;
        }
    
        #hot-courses .box{
            margin: 10px;
        }
    
        .breadcrumb-container {
            display: none;
        }
        
        .best-seller-categories-container   
            .col-xl-4, 
            .col-lg-6,
            .col-xs-6
        {
            padding: 0px !important;    
        }
        
        .slick-slide.k-box-card-list{
             padding-bottom: 0 !important;
        }
        h3.mobile{
            display: block !important;
            font-weight: bold;
            padding-left: 1px;
            color: #50ad4e;
        }
        
        .slick-dots {
            position: absolute;
            bottom: 5px;
         }
    
        .breadcrumb-container {
            margin-top: 67px;
            background-color: #fafafa;
        }
        
         .slick-dots li button:before {
            font-size: 10px !important;
            line-height: 25px;
        }
        
        .best-seller-categories-container {
            margin-top: 1px !important;
        }
        .best-seller-categories-container {
            margin-top: 1px !important;
        }
        
        .hot-category {
            padding: 5px !important;
        }
        
        #best-seller-courses h2 {
            display: block;
            color: #50ad4e;
            font-size: 20px;
            padding-left: 15px;
        }
        #best-seller-courses div.hr {
            margin-top: 20px !important;
            margin-left: 3.5%;
            width: 93%;
           
        }
        
        .hot-category .name {
            font-size: unset !important;
            
        }

    }
    
    .breadcrumb-container {
        margin-top: 67px;
        background-color: #fafafa;
    }
    .breadcrumb {
        border-radius: 0px;
        padding: 8px 0px;
        margin: 0px;
        background-color: inherit;
    }
    .breadcrumb > li + li::before {
        padding-right: .5rem;
        padding-left: .5rem;
        color: #a0a0a0;
        content: \"»\";
    }
    .breadcrumb > li {
        color: #666 !important;
        font-weight: bold;
    }
    .breadcrumb > li > a {
        color: inherit;
    }
    .breadcrumb > li.active {
        color: #666 !important;
        font-weight: normal;
    }
    #k-listing {
  
    }

    #hot-courses h3 {
        display: none;
    }
    #hot-courses{
        margin-bottom: -15px;
    }
    @media (min-width: 768px) {
        #hot-courses h3 {
            display: block;
            color: #50ad4e;
            font-size: 18px;
            padding-left: 15px;
            padding-right: 14px;
            padding-bottom: 0;
        }


    }
    


    div.hr {
        width: 96%;
        border-bottom-style: solid;
        border-bottom-width: initial;
        border-color: #aab2bd;
        margin-top: -18px;
        margin-bottom: 40px;
        margin-left: 2%
    }

    .hot-category .name {
        position: absolute;
        top: 50%;
        -webkit-transform: translateY(-50%);
        -moz-transform: translateY(-50%);
        -ms-transform: translateY(-50%);
        -o-transform: translateY(-50%);
        transform: translateY(-50%);
        left: 0;
        right: 0;
        bottom: 0;
        text-align: center;
        font-size: large;
        font-weight: bolder;
        color: #FFFFFF;
        height: 100%;
    }
    
    .hot-category .name span{
            position: absolute;
            width: 94%;
            top: 50%;
            -webkit-transform: translateY(-50%);
            -moz-transform: translateY(-50%);
            -ms-transform: translateY(-50%);
            -o-transform: translateY(-50%);
            transform: translateY(-50%);
            left: 2.5%;
    }
       
    
    .hot-category:hover .overlay {
        width:100%;
        height:100%;
        position:absolute;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        background-color:#000;
        opacity:0.2;
        border-radius:1px;
    }
    @media(max-width: 425px){
        .hot-category{
            position: unset !important;
            }
        #w2{
            padding: 0 12px;
        }
         .w3{
            padding: 0 12px;
        }
    }
    #w2{
        
            margin-bottom: 20px;
    }
    .w3{
        
            margin-bottom: 20px;
    }
    
    k-box-card-wrap.k-box-card-wrap-hot{
        
    }

");
$url = parse_url(Yii::$app->request->url);
$get = $_GET;
unset($get['sort']);
unset($get['page']);
?>

<?php
$date = date('Y-m-d');

if ($catModel == null) {
    if (Yii::$app->request->get('course_type') == Course::TYPE_COMBO) {
        $catId = Yii::$app->params['category-combo-id'];
    } else {
        $catId = Yii::$app->params['category-all-id'];
    }
} else {
    $catId = $catModel->id;
}

$hasCampaignCategoryBanners = Banner::find()->where([
    'type' => [
        Banner::TYPE_CAMPAIGN_CATEGORY_LARGE,
        Banner::TYPE_CAMPAIGN_CATEGORY_SMALL
    ],
    'status' => Banner::STATUS_ACTIVE
])
    ->andWhere("from_date is null or (from_date is not null and from_date <= '{$date}')")
    ->andWhere("to_date is null or (to_date is not null and to_date >= '{$date}')")
    ->andFilterWhere(['category_id' => $catId])
    ->exists();
?>
<?php if (empty($tag) && !isset($_GET['q']) && $hasCampaignCategoryBanners) : ?>
    <section id="banner-campaign-category">
        <h3 style="color:#50ad4e; font-weight: bold;">Có gì hot trên Kyna?</h3>
        <div class="box">
            <?= BannerWidget::widget(['type' => Banner::TYPE_CAMPAIGN_CATEGORY_LARGE, 'category_id' => $catId, 'containerClass' => 'banner lg-banner']) ?>
            <!-- end .lg-banner -->
            <?= BannerWidget::widget([
                'type' => Banner::TYPE_CAMPAIGN_CATEGORY_SMALL,
                'category_id' => $catId,
                'containerClass' => 'banner sm-banner',
                'limit' => 2
            ])
            ?>
        </div>
        <!-- end .box -->
    </section>
    <!-- end #banner-campaign-category -->
<?php endif; ?>

<!-- start hot courses-->
<?php if (empty($catModel) && empty($tag) && !$isCombo) { ?>
    <?php
    if (Yii::$app->devicedetect->isMobile()) {
        $hotCourses = $hotCoursesMobile;
    } else if (Yii::$app->devicedetect->isTablet()) {
        $hotCourses = $hotCourses;
    }
    ?>
    <?php if (empty($q)) : ?>
        <section id="hot-courses">
            <h3 style="padding: 0 15px; color:#50ad4e; font-weight: bold;">
                Khóa học hot dành cho bạn
                <div class="slider-slick">
                    <a href="#" class="previous round">&#8249;</a>
                    <a href="#" class="next round">&#8250;</a>
                </div>
            </h3>

            <div class="box">
                <h3 class="mobile">
                    Khóa học hot dành cho bạn
                </h3>
                <?= $this->render('_list_hot', [
                    'dataProvider' => $hotCourses,
                ]);
                ?>
            </div>
        </section>
    <?php endif;?>
<?php }; ?>
<!-- end hot courses -->

<div>
    <?php
    if (!empty($tag)):?>
        <?= \frontend\modules\course\widgets\TagBanner::widget(['tag' => $tag]) ?>
    <?php endif;
    ?>
    <span class="menu-heading">
        <?php if (!empty($catModel)) : ?>
            <h2 class="-mob"><b><?= $catModel->name ?></b></h2>
            <?= CategoryWidget::widget(['is_mobile' => true]) ?>

            <div class="box-inline">
                <b><?= $totalCourse ?></b> <b class="-pc">khóa học </b><span class="-mob">khóa học </span><br> <h2
                        class="-pc"> <b><?= $catModel->name ?></b></h2>
                <div class="k-listing-button-filter k-button-mobile" data-offpage="#nav-filter-search">Lọc kết quả <img
                            src="<?= $cdnUrl ?>/img/icon-filter.png" alt=""></div>
            </div>
        <?php elseif (empty($q) && empty($tag)) : ?>
            <h1><b>Tất cả</b></h1>
            <div class="box-inline">
                <b><?= $totalCourse ?></b> khóa học <?= (Yii::$app->request->get('course_type') == Course::TYPE_COMBO) ? ' Combo' : '' ?>
                <br>
                <div class="k-listing-button-filter k-button-mobile" data-offpage="#nav-filter-search">Lọc kết quả <img
                            src="<?= $cdnUrl ?>/img/icon-filter.png" alt=""></div>
            </div>
        <?php else: ?>
            <?= $totalCourse ?> Kết quả tìm kiếm từ khóa
            <h1 style="display: inline">'<?= !empty($q) ? Html::encode($q) : Html::encode($tag->tag) ?>'</h1>
        <?php endif; ?>

    </span>
</div>


<section>
    <?= ListView::widget([
        'dataProvider' => $dataProvider,
        'layout' => "{items}\n",
        'itemView' => function ($model) {
            $model = (object)$model;
            return $this->render($model->type == Course::TYPE_COMBO ? '_box_combo' : '_box_product', ['model' => $model]);
        },
        'options' => [
            'tag' => 'ul',
            'class' => 'k-box-card-list'
        ],
        'itemOptions' => [
            'tag' => 'li',
            'class' => 'col-xl-4 col-md-6 col-xs-12 k-box-card'
        ],
        'pager' => [
            'prevPageLabel' => '<span>&laquo;</span>',
            'nextPageLabel' => '<span>&raquo;</span>',
            'options' => [
                'class' => 'pagination',
            ]
        ]
    ]) ?>

    <nav id="pager-container">
        <?= LinkPager::widget([
            'pagination' => $pagination,
            'nextPageLabel' => '>',
            'prevPageLabel' => '<'
        ]) ?>
    </nav>

    <?= BannerWidget::widget(['type' => Banner::TYPE_CROSS_PRODUCT_BOTTOM]) ?>
</section>

<?php if ($hotCategories->getCount() > 0): ?>
    <section id="best-seller-courses">
        <div class="hr"></div>
        <h2 style="color: #50ad4e;"> Các danh mục khác phù hợp</h2>
        <div class="box">
            <?= $this->render('_list_best_seller', [
                'dataProvider' => $hotCategories,
            ]);
            ?>
        </div>
    </section>
<?php endif; ?>

<?php if (!empty($catModel) && !empty($catModel->seo_description)): ?>
    <section class="category-seo-description">
        <?= $catModel->seo_description ?>
    </section>
<?php endif; ?>
<?php if (!empty($tag) && !empty($tag->description)): ?>
    <section class="category-seo-description">
        <?= $tag->description ?>
    </section>
<?php endif; ?>


<script type="text/javascript">
    jQuery(document).ready(function ($) {
        $(document).on('shown.bs.modal', "#modal", function () {
            setTimeout(function () {
                FB.XFBML.parse();
            }, 1000);
        });
    });
</script>
<?php
$this->registerJsFile($cdnUrl . '/src/js/add-to-cart.js', ['position' => View::POS_END]);
$this->registerJsFile($cdnUrl . '/src/js/course-pop-up.js', ['position' => View::POS_END]);
?>
