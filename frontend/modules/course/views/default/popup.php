<?php

use yii\web\View;
use yii\helpers\StringHelper;
use yii\helpers\Url;
use common\helpers\CDNHelper;
use app\widgets\YoutubePlayer;
use kyna\course\models\Course;
use frontend\modules\course\widgets\CountDownTimer;
use frontend\modules\course\widgets\CountDownCampaignTetTimer;
$formatter = Yii::$app->formatter;
$combo_course = true;
$discountPercent = 0;

$this->title = "{$model->name} | Kyna.vn";
$cdnUrl = CDNHelper::getMediaLink();
?>
<?php
$countDownTime = \common\helpers\DateTimeHelper::getCountDownTime();
if(!empty($countDownTime)):
    ?>
    <script type="text/javascript">
        $(document).ready(function() {
            var deadline = new Date(Date.parse(new Date()) + <?= $countDownTime?> * 1000);
            initializeClock('popup-clock', deadline);
        });
    </script>
<?php endif;?>
<link href="/css/popup.css" rel="stylesheet"/>

<div class="modal-header">
    <button type="button" class="k-popup-lesson-close close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">×</span>
    </button>
    <h4><?= $model->name ?></h4>
</div>
<div class="modal-body clearfix" data-id="<?= $model->id ?>" data-brand="<?= (!empty($model->teacher)) ? $model->teacher->profile->name : '' ?>">
    <h6><?= (!empty($model->teacher) ? ('<b>' . $model->teacher->profile->name . '</b>' . (!empty($model->teacher->title) ? ', ' . $model->teacher->title : '')) : '') ?></h6>
    <!--end .detail-title-->
    <div class="col-md-6 col-xs-12 k-popup-lesson-content">
        <?= YoutubePlayer::widget(array(
            'thumbnailSize' => CDNHelper::IMG_SIZE_THUMBNAIL_YOUTUBE,
            'model' => $model
        )) ?>
        <!--end .videoWrapper-->
        <div class="clearfix"></div>
    </div>
    <!--end .k-popup-lesson-content-->

    <div class="col-md-6 col-xs-12 k-popup-lesson-detail">
        <div class="box-style">
            <?php if ($model->type == Course::TYPE_VIDEO): ?>
            <span class="st-video"><i class="fa fa-youtube-play" aria-hidden="true"></i> Khóa học video</span>
            <?php elseif ($model->type == Course::TYPE_SOFTWARE): ?>
            <span class="st-app"><i class="fa fa-mobile" aria-hidden="true"></i> Phần mềm</span>
            <?php endif; ?>
            <span class="time pc"><i class="fa fa-clock-o" aria-hidden="true"></i> <?= $model->getTotalTimeText(false) ?></span>
        </div>
        <div class="k-popup-lesson-detail-price">
            <span class="bold">
                <?php if (!empty($model->discountAmount)) : ?>
                    <i class="fa fa-tags" aria-hidden="true"></i>
                    <span class="price-sell"><?= number_format($model->sellPrice, '0', '', '.') ?>đ<span>/<?= $model->purchaseTypeTextFrontend ?></span></span>
                    <span class="price-old"><s><?= number_format($model->oldPrice, '0', '', '.') ?>đ</s></span>
                    <span class="price-percent">(-<?= $model->discountPercent ?>%)</span>
                <?php elseif (!empty($model->oldPrice)) : ?>
                    <i class="fa fa-tags" aria-hidden="true"></i>
                    <span class="price-sell"><?= number_format($model->oldPrice, '0', '', '.') ?>đ<span>/<?= $model->purchaseTypeTextFrontend ?></span></span>
                <?php else : ?>
                    <span class="price-free">Miễn phí</span>
                <?php endif; ?>
            </span>
            <?= CountDownTimer::widget( ['class_name' => 'popup-clock', 'course_id' => $model->id])?>
            <?= CountDownCampaignTetTimer::widget( ['class_name' => 'popup-clock', 'course_id' => $model->id])?>
        </div>
        <div class="k-popup-lesson-info">
            <ul>
                <li><i class="fa fa-user" aria-hidden="true"></i> Trình độ: <?= $model->levelText ?></li>
                <li><i class="fa fa-play-circle" aria-hidden="true"></i> Bài học: <?= $model->lessonCount; ?> bài</li>
                <li><i class="fa fa-eye" aria-hidden="true"></i> Xem được trên máy tính, điện thoại, tablet</li>
                <?php if($model->purchaseTypeText):?>
                <li><i class="icon icon-certificate"></i><?= $model->purchaseTypeText ?></li>
                <?php endif; ?>
                <li><i class="fa fa-graduation-cap" aria-hidden="true"></i> Cấp chứng nhận hoàn thành</li>
            </ul>
        </div>
    </div>
    <div class="col-xs-12 k-popup-lesson-description">
        <p>
            <?php
            if (!empty($model->description)) {
                echo StringHelper::truncateWords($model->description, 120);
            }
            ?>
            <a href="<?= $model->url ?>">» Xem chi tiết</a>
        </p>
    </div>
    <div class="row-action clearfix">
        <?php if ($newItem === '1') : ?>
        <div class="btn-action">
            <?php if($model->isBirthdayDiscount && Yii::$app->params['campaign_birthday']):?>
                <a id="sinhnhat" href="<?= Url::toRoute(['/course/default/index', 'tag' => 'giamsinhnhatkyna']) ?>" style="float: none; display: none;">
                    <img style="width: 50%; position: absolute; right: 0; bottom: 65px;" src="<?= $cdnUrl ?>/img/sinh-nhat-kyna/popup.png?v=2" alt="">
                </a>
           <?php endif;?>
            <a href="javascript:" data-pid='<?= $model->id ?>' class="<?= $model->isCampaign11 ? 'campaign11' : '' ?> add-to-cart notCombo k-popup">Thêm vào giỏ hàng</a>
            <a id="muangay-hover" class="buy-now btn-buy-now" href="javascript:" data-pid='<?= $model->id ?>'>MUA NGAY</a>
        <?php endif; ?>
        <?php if ($newItem === '3') : ?>
        <div class="status-course">
            <b><i class="fa fa-check-circle"></i> Đã thêm khóa học này vào giỏ hàng</b>
        </div>

        <div class="btn-action">
            <a href="<?= Url::toRoute(['/cart/default/index']) ?>" class="btn-go-to-cart">
                Xem giỏ hàng và thanh toán
            </a>
        <?php endif; ?>
        <?php if ($newItem === '2') : ?>
        <div class="status-course">
            <b><i class="fa fa-check-circle"></i> Bạn đã mua khóa học này</b>
        </div>

        <div class="btn-action">
            <a href="<?= Url::toRoute(['/trang-ca-nhan/khoa-hoc']) ?>" class="btn-go-to-courses">
                Vào lớp học
            </a>
        <?php endif; ?>
        </div>
    </div>
    <!--end .col-md-4 col-xs-12 left-->
</div>
<!--end .modal-body-->
        <script>
            $(document).ready(function(){
                $("#muangay-hover").mouseover(function(){
                    $("#sinhnhat").css("display", "block");
                });
                $("#muangay-hover").mouseout(function(){
                    $("#sinhnhat").css("display", "none");
                });
            });
            $(document).ready(function(){
                $("#sinhnhat").mouseover(function(){
                    $("#sinhnhat").css("display", "block");
                });
                $("#sinhnhat").mouseout(function(){
                    $("#sinhnhat").css("display", "none");
                });
            });
        </script>