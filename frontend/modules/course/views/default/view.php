<?php
/**
* @var $this \yii\web\View
*/

use yii\web\View;
use yii\helpers\Url;

use common\helpers\CDNHelper;
use common\helpers\GaHtmlHelper;
use common\helpers\GoogleSnippetHelper;

use app\widgets\YoutubePlayer;
use frontend\widgets\TopTags;
use frontend\widgets\AnchorText;
use kyna\course\models\Course;
use frontend\modules\course\widgets\CountDownTimer;
use frontend\modules\course\widgets\BannerVoucherFree;
use frontend\modules\course\widgets\TextNoVoucherFree;
use frontend\modules\course\widgets\CountDownCampaignTetTimer;
$cdnUrl = CDNHelper::getMediaLink();

$formatter = \Yii::$app->formatter;

$disable = false;
if(isset(Yii::$app->params['disable_position'])){
    $disable = Yii::$app->params['disable_position'];
}

// Set breadcrumbs data
if (!empty($model)) {
    // Danh sach khoa hoc
    $this->params['breadcrumbs'][] = ['label' => 'Danh sách khóa học', 'url' => ['index']];

    // Category list
        // Get category list
    $curCatModel = $model->category;
    $catList = [];
    while (!empty($curCatModel)) {
        $catList[] = $curCatModel;
        $curCatModel = $curCatModel->parent;
    }
        // Add to breadcrumbs
    $position = 2;
    $url = yii\helpers\Url::toRoute(['index'],true);
    while (!empty($catList)) {
        $curCatModel = array_pop($catList);
        $url = $url."/".$curCatModel->slug;
        $this->params['breadcrumbs'][] = [
            'label' => $curCatModel->name,
            'url' => ['index', 'slug' => $curCatModel->slug],
            'template' => GoogleSnippetHelper::renderBreadcrumbTemplate($url, $curCatModel->name, $position),
        ];
        $position += 1;
    }

    // Current course
    $url = $url."/".$model->slug;
    $this->params['breadcrumbs'][] = [
        'label' => $model->name,
        'url' => ['index', 'slug' => $model->slug],
        'template' => GoogleSnippetHelper::renderBreadcrumbTemplate($model->slug, $model->name, $position, false),
    ];
}

// Set breadcrumbs css
$this->registerCss("
    @media (max-width: 640px) {
        .breadcrumb-container {
            display: none;
        }
    }
    .breadcrumb-container {
        margin: 0px;
        background-color: #fafafa;
    }
    .breadcrumb {
        border-radius: 0px;
        padding: 8px 15px;
        margin: 0px;
        background-color: inherit;
    }
    .breadcrumb > li + li::before {
        padding-right: .5rem;
        padding-left: .5rem;
        color: #a0a0a0;
        content: \"»\";
    }
    .breadcrumb > li {
        color: #666 !important;
        font-weight: bold;
    }
    .breadcrumb > li > a {
        color: inherit;
    }
    .breadcrumb > li.active {
        color: #666 !important;
        font-weight: normal;
    }

");

$teacherUrl = Url::toRoute(['/user/teacher/index', 'slug' => $model->teacher->slug]);
$totalTimePartial = $model->getTotalTimePartials();
$timeUnits = Course::totalTimeUnits();
?>
<main>
    <div class="k-course-details-header container" data-id="<?= $model->id ?>" data-course-type="<?= $model->type ?>">
        <div class="k-course-details-title">
            <h1><?= $model->name ?></h1>
            <a href="<?= $teacherUrl ?>"><?= $model->teacher->profile->name ?></a><span><?= "<b>,</b> {$model->teacher->title}" ?></span>
            <?= $this->render('_rating', ['model' => $model]) ?>
        </div><!--end k-course-details-title-->

        <div class="col-xl-8 col-md-11 col-xs-12 k-course-details-video">
            <?= YoutubePlayer::widget(array(
                'thumbnailSize' => CDNHelper::IMG_SIZE_THUMBNAIL_YOUTUBE_LARGE,
                'model' => $model
            )) ?>
            <!--<div class="k-course-details-title hidden-lg-up">
                <h1><?= $model->name ?></h1>
                <a href="<?= $teacherUrl ?>"><?= $model->teacher->profile->name ?></a>
                <span><?= " / {$model->teacher->title}" ?></span>
                <?= $this->render('_rating', ['model' => $model]) ?>
            </div><!--end k-course-details-title-->
        </div><!--end .k-course-details-video-->
        <div class="col-xl-4 col-md-11 col-xs-12 k-course-details-info sidebar-info course">
            <ul class="list">
                <li class="box-style hidden-lg-down">
                    <?php if ($model->type == Course::TYPE_VIDEO): ?>
                        <span class="st-video"><i class="fa fa-youtube-play" aria-hidden="true"></i> Khóa học video</span>
                    <?php elseif ($model->type == Course::TYPE_SOFTWARE): ?>
                        <span class="st-app"><i class="fa fa-mobile" aria-hidden="true"></i> Phần mềm</span>
                    <?php endif; ?>
                    <span class="time pc"><i class="fa fa-clock-o" aria-hidden="true"></i> <?= $model->getTotalTimeText(false) ?></span>
                </li>
                <li class="box-info hidden-xl-up">
                    <div class="wrapper">
                        <div class="item">
                            <img src="<?= $cdnUrl ?>/img/icon-info-video-1.png" alt="">
                            <div class="number"><?= $model->total_users_count ?></div>
                            <div class="text">đăng ký học</div>
                        </div>
                        <div class="item">
                            <img src="<?= $cdnUrl ?>/img/icon-info-video-2.png" alt="">
                            <div class="number"><?= $totalTimePartial['number'] ?></div>
                            <div class="text"><?= $timeUnits[$totalTimePartial['unit']] ?></div>
                        </div>
                        <div class="item">
                            <img src="<?= $cdnUrl ?>/img/icon-info-video-3.png" alt="">
                            <div class="number"><?= $model->rating ?></div>
                            <div class="text"><?= $model->rating_users_count ?> đánh giá</div>
                        </div>
                    </div>
                </li>
                <li class="price-list">
                    <!-- Chuan bi thong so -->
                    <?php
                        $salePrice = $model->sellPrice;
                        if (!empty($model->oldPrice)) {
                            $originalPrice = $model->oldPrice;
                        } else {
                            $originalPrice = $salePrice;
                        }
                        $discountMoney = $originalPrice - $salePrice;
                        if ($originalPrice > 0) {
                            $discountRate = $discountMoney/$originalPrice;
                        } else {
                            $discountRate = 0;
                        }
                    ?>
                    <ul>
                        <!-- Case 1: course co discount -->
                        <!-- Hien thi: salePrice + oldPrice + discountRate -->
                        <?php if ($discountRate > 0) : ?>
                            <li>
                                <div class="hidden-xl-up"><b><i class="fa fa-tags"></i><span class="label"> Học phí</span></b></div>
                                <span class="price-sale"><b><i class="fa fa-tags hidden-lg-down"></i> <?=$formatter->asInteger($salePrice)?>đ<span class="hidden-lg-down"><?= $model->purchaseTypeTextFrontend === NULL ? "" : "/" . $model->purchaseTypeTextFrontend ?></b></span>
                                <span class="price-old"><s>(<?=$formatter->asInteger($originalPrice)?>đ)</s></span>
                                <span class="price-discount"><span class="hidden-lg-down">(</span>-<?=$formatter->asPercent($discountRate)?><span class="hidden-lg-down">)</span></span>
                            </li>
                        <!-- Case 2: course khong co discount -->
                        <!-- Hien thi: salePrice -->
                        <?php elseif ($salePrice > 0) : ?>
                            <li>
                                <div class="hidden-xl-up"><b><i class="fa fa-tags"></i><span class="label"> Học phí</span></b></div>
                                <span class="price-sale"><b><i class="fa fa-tags hidden-lg-down"></i> <?=$formatter->asInteger($salePrice)?>đ<span class="hidden-lg-down"><?= $model->purchaseTypeTextFrontend === NULL ? "" : "/" . $model->purchaseTypeTextFrontend ?></b></span>
                            </li>
                        <!-- Case 3: Others (e.g. sale_price = 0-->
                        <!-- Hien thi: Mien phi -->
                        <?php else: ?>
                            <li>
                                <span class="price-sale"><b>Miễn phí</b></span>
                            </li>
                        <?php endif; ?>
                        <?= CountDownTimer::widget(['course_id' => $model->id])?>
                        <?= CountDownCampaignTetTimer::widget(['course_id' => $model->id])?>
                    </ul>

                </li>
                <?= BannerVoucherFree::widget(['course_id' => $model->id])?>
                <?= TextNoVoucherFree::widget(['course_id' => $model->id])?>
                <li class="info">
                    <ul class="detail-info">
                        <li class="hidden-xl-up">
                            <?php if ($model->type == Course::TYPE_VIDEO): ?>
                                <i class="fa fa-youtube-play red" aria-hidden="true"></i> <span class="red">Khóa học video</span>
                            <?php elseif ($model->type == Course::TYPE_SOFTWARE): ?>
                                <i class="fa fa-mobile blue" aria-hidden="true"></i> <span class="blue">Phần mềm</span>
                            <?php endif; ?>
                        </li>
                        <li><i class="fa fa-user" aria-hidden="true"></i> <span>Trình độ: <?= $model->levelText ?></span></li>
                        <?php if ($model->type == Course::TYPE_VIDEO): ?>
                        <li><i class="fa fa-play-circle" aria-hidden="true"></i> <span>Bài học: <?= $model->lessonCount; ?> bài</span></li>
                        <?php endif; ?>
                        <li><i class="fa fa-eye" aria-hidden="true"></i> Xem được trên máy tính, điện thoại, tablet</li>
                        <?php if ($model->type == Course::TYPE_SOFTWARE && !empty($model->max_device)): ?>
                        <li><i class="fa fa-desktop" aria-hidden="true"></i> <span>Số thiết bị: <?= $model->max_device; ?></span></li>
                        <?php endif; ?>
                        <?php if (!empty($model->purchaseTypeText)): ?>
                        <li><i class="icon icon-certificate"></i><?= $model->purchaseTypeText ?></li>
                        <?php endif; ?>
                        <?php if ($model->type == Course::TYPE_VIDEO): ?>
                        <li><i class="fa fa-graduation-cap" aria-hidden="true"></i> Cấp chứng nhận hoàn thành</li>
                        <?php endif; ?>
                    </ul>
                </li>
                <li class="bought-status">
                    <ul>
                    </ul>
                </li>
                <!-- Case 1: Da add khoa hoc + Da thanh toan - newItem = 2 -->
                <!-- Hien thi: "Ban da mua khoa hoc nay" -->
                <?php if ($newItem === '2') : ?>
                    <script>
                        $("li.bought-status > ul").html('<li class="text-success"><b><i class="fa fa-check-circle"></i> Bạn đã mua khóa học này</b></li>');
                    </script>
                    <!-- Case 2: Da add khoa hoc + Chua thanh toan  - newItem = 3 -->
                    <!-- Hien thi: "Da them khoa hoc nay vao gio hang" -->
                <?php elseif ($newItem === '3') : ?>
                    <script>
                        $("li.bought-status > ul").html('<li class="text-success"><b><i class="fa fa-check-circle"></i> Đã thêm khóa học này vào giỏ hàng</b></li>');
                    </script>
                    <!-- Case 3: Chua add khoa hoc + Chua thanh toan - newItem = 1 -->
                    <!-- Khong hien thi gi het -->
                <?php elseif ($newItem === '1') :?>

                <?php endif; ?>


                <!-- Neu da mua khoa hoc roi - newItem = 2 -->
                <!-- Hide price list -->
                <?php if ($newItem === '2') : ?>
                    <script>
                        $("li.price-list").hide();
                    </script>
                <?php endif; ?>

                <li class="button">
                    <ul>
                        <li class="action-button">
                            <!-- Case 1: Chua add khoa hoc + Chua thanh toan - newItem = 1 -->
                            <!-- Button action: DANG KY HOC-->
                            <?php if ($newItem === '1') : ?>
                                <?=
                                GaHtmlHelper::a('<b>MUA NGAY</b>', '#', [
                                    'data-pid' => $model->id,
                                    'class' => 'btn-buy-now dang-ky-hoc hidden-md-down' . (!Yii::$app->user->isGuest ? ' register' : ''),
                                    'style' => 'text-align: center;',
                                    'category' => 'CourseDetail',
                                    'action' => 'AddToCart',
                                    'label' => $model->name,
                                ])
                                ?>
                                <?=
                                GaHtmlHelper::a("<b class='hidden-md-down'>Thêm vào giỏ hàng</b><b class='hidden-lg-up'>ĐĂNG KÝ HỌC: {$formatter->asInteger($salePrice)}đ</b>", '#', [
                                    'data-pid' => $model->id,
                                    'class' => 'go-to-cart add-to-cart dang-ky-hoc' . (!Yii::$app->user->isGuest ? ' register' : ''),
                                    'style' => 'text-align: center; margin-top: 10px;',
                                    'category' => 'CourseDetail',
                                    'action' => 'AddToCart',
                                    'label' => $model->name,
                                ])
                                ?>
                            <!-- Case 2: Da add khoa hoc + chua thanh toan - newItem = 3 -->
                            <!-- Button action: XEM GIO HANG & THANH TOAN -->
                            <?php elseif ($newItem === '3') : ?>
                                <a href="<?= Url::toRoute(['/cart/default/index']) ?>" class="btn-lg btn-block thanh-toan"><b>XEM GIỎ HÀNG VÀ THANH TOÁN</b></a>
                            <!-- Case 3: Da add khoa hoc + da thanh toan - newItem = 3 -->
                            <!-- Button action: VAO LOP HOC -->
                            <?php elseif ($newItem === '2') : ?>
                                <a href="<?= Url::toRoute(['/course/learning/index', 'id' => $model->id]) ?>"><div class="btn-success btn-lg btn-block vao-lop-hoc" ><b>VÀO LỚP HỌC</b></div></a>
                            <?php endif; ?>
                            <?php if($newItem == 1 && $model->isBirthdayDiscount):
                                ?>
                                <a href="<?= Url::toRoute(['/course/default/index', 'tag' => 'giamsinhnhatkyna']) ?>">
                                    <img style="width: 100%; padding-top: 10px;" src="<?= $cdnUrl ?>/img/sinh-nhat-kyna/giohang.png?v=2" alt="">
                                </a>
                            <?php
                            endif;
                            ?>
                        </li>
                    </ul>
                </li>
                <li>
                    <ul class="social-media clearfix">
                         <li id='fb-save-button'>
                            <a data-toggle="dropdown" href="#">
                                <div class="fb-save" data-uri="<?= Yii::$app->request->absoluteUrl ?>" data-size="small"></div>
                            </a>
                            <ul class="popup-guide">
                                Khóa học sẽ được lưu vào phần <a href="https://facebook.com/saved" target="_blank">Saved</a> (Đã lưu)
                                trong tài khoản Facebook của bạn. Bạn có thể tìm xem lại bất kì khi nào.
                            </ul>
                        </li> 
                        <li class="fb">
                            <div class="fb-like" data-href="<?= Yii::$app->request->absoluteUrl ?>" data-layout="button_count" data-action="like" data-size="large" data-show-faces="false" data-share="true"></div>
                        </li>
                        <li class="gg">
                            <!-- Place this tag in your head or just before your close body tag. -->
                            <script src="https://apis.google.com/js/platform.js" async defer>
                                {lang: 'vi'}
                            </script>

                            <!-- Place this tag where you want the +1 button to render. -->
                            <div class="g-plusone" data-size="standard" data-href="<?= Yii::$app->request->absoluteUrl ?>"></div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div><!--end .k-course-details-info-->
    </div><!--end .k-course-details-header-->
    <div class="box-container">
        <div class="container">
            <div class="col-xl-8 col-md-11 col-xs-12">
                <div class="k-course-details-main main-container">
                    <div class="k-course-details-main-left">
                        <section>
                            <div id="k-course-details-about" class="k-course-details-about" path="scrolling">
                                <ul class="list hidden-md-down">
                                    <li><a href="javascript:void(0);" link="#k-course-details-about">Giới thiệu</a></li>
                                    <?php if ($model->type == Course::TYPE_VIDEO): ?>
                                    <li><a href="javascript:void(0);" link="#k-course-details-curriculum">Nội dung học</a></li>
                                    <li><a href="javascript:void(0);" link="#k-course-details-author">Giảng viên</a></li>
                                    <?php endif; ?>
                                    <?php if ($model->type == Course::TYPE_SOFTWARE): ?>
                                    <?php if (!empty($model->screenshots)): ?>
                                        <li><a href="javascript:void(0);" link="#k-course-details-screenshot">Screenshot ứng dụng</a></li>
                                    <?php endif; ?>
                                    <li><a href="javascript:void(0);" link="#k-course-details-author">Đơn vị cung cấp</a></li>
                                    <?php endif; ?>

                                    <?php if($disable == false):?>
                                        <li><a href="javascript:void(0);" link="#detail-comment-facebook">Đánh giá &amp; bình luận</a></li>
                                    <?php endif;?>
                                </ul>
                                <div class="course-overview">
                                    <?php if (!empty($model->what_you_learn)): ?>
                                    <div class="title">Bạn sẽ học được gì</div>
                                    <input id="showmore-content" type="checkbox" style="display: none">
                                    <div class="content-list">
                                        <?= $model->what_you_learn ?>
                                    </div>
                                    <label class="showmore-content-1" for="showmore-content">Xem Thêm</label>
                                    <label class="showmore-content-2" for="showmore-content">Thu Gọn</label>
                                    <?php endif; ?>
                                    <?php if (!empty($model->opinions)): ?>
                                    <div class="slider-review hidden-md-down">
                                        <div class="slider-title">Ý kiến học viên</div>
                                        <div class="slider-top">
                                            <?php foreach ($model->opinions as $item): ?>
                                            <div class="content"><?= $item->description ?> - <b><?= $item->user_name ?></b></div>
                                            <?php endforeach; ?>
                                        </div>
                                        <div class="slider-bottom<?= count($model->opinions) == 2 ? ' dupble' : '' ?>" data-center-mode="<?= count($model->opinions) == 1 || count($model->opinions) == 2 ? 'false' : 'true' ?>" data-number-slider="<?= count($model->opinions) == 2 ? '2' : '1' ?>">
                                            <?php foreach ($model->opinions as $item): ?>
                                            <div class="image"><img src="<?= $cdnUrl . $item->avatar_url ?>" alt=""></div>
                                            <?php endforeach; ?>
                                        </div>
                                    </div>
                                    <?php endif; ?>
                                    <?php if ($model->type == Course::TYPE_VIDEO && (!empty($model->overview) || !empty($model->description))): ?>
                                        <div class="title hidden-md-down">Giới thiệu khóa học</div>
                                        <div class="box-application hidden-md-down">
                                            <?= !empty($model->overview) ? $model->overview : $model->description ?>
                                        </div>
                                    <?php endif; ?>
                                    <?php if ($model->type == Course::TYPE_SOFTWARE): ?>
                                        <div class="title hidden-md-down">Giới thiệu khóa học</div>
                                        <div class="box-application hidden-md-down">
                                            <div class="course-app clearfix">
                                                <i class="fa fa-mobile" aria-hidden="true"></i>
                                                <div class="desc">
                                                    <p><b>Khóa học ứng dụng</b></p>
                                                    <p>Sau khi mua sẽ cài và sử dụng trên thiết bị di động của bạn</p>
                                                </div>
                                            </div>
                                            <?php if (!empty($model->devicesText)): ?>
                                            <div class="course-app-link clearfix">
                                                <div class="app-tit">Ứng dụng có thể cài đặt, sử dụng trên:<br><b><?= $model->devicesText ?></b></div>
                                                <div class="app-box-url">
                                                    <?php if (!empty($model->window_app_link)): ?>
                                                        <a href="<?= $model->window_app_link ?>" class="url pc-win"><img src="<?= $cdnUrl ?>/img/device/icon-pc-win.png"></a>
                                                    <?php endif; ?>
                                                    <?php if (!empty($model->mac_app_link)): ?>
                                                        <a href="<?= $model->mac_app_link ?>" class="url pc-mac"><img src="<?= $cdnUrl ?>/img/device/icon-pc-mac.png"></a>
                                                    <?php endif; ?>
                                                    <?php if (!empty($model->ios_app_link)): ?>
                                                        <a href="<?= $model->ios_app_link ?>" class="url pc-ios"><img src="<?= $cdnUrl ?>/img/device/icon-mb-ios.png"></a>
                                                    <?php endif; ?>
                                                    <?php if (!empty($model->android_app_link)): ?>
                                                        <a href="<?= $model->android_app_link ?>" class="url pc-adr"><img src="<?= $cdnUrl ?>/img/device/icon-mb-adr.png"></a>
                                                    <?php endif; ?>
                                                </div>
                                            </div>
                                            <?php endif; ?>
                                            <?= !empty($model->overview) ? $model->overview : $model->description ?>
                                        </div>
                                    <?php endif; ?>
                                </div>
                            </div><!--end .k-course-details-about-->
                        </section>
                        <?php if ($model->type == Course::TYPE_SOFTWARE && !empty($model->screenshots)): ?>
                        <section>
                            <div id="k-course-details-screenshot" class="k-course-details-screenshot hidden-md-down" path="scrolling">
                                <div class="slider-title">Screenshot màn hình Ứng dụng</div>
                                <?php if (count($model->screenshots) == 1): ?>
                                <div class="slider-image-single">
                                    <img src="<?= $cdnUrl . $model->screenshots[0]->image_url ?>" alt="">
                                </div>
                                <div class="slider-content-single">
                                    <p><?= $model->screenshots[0]->description ?></p>
                                </div>
                                <?php else: ?>
                                <div class="slider-image">
                                    <?php foreach ($model->screenshots as $item): ?>
                                        <div class="item">
                                            <img src="<?= $cdnUrl . $item->image_url ?>" alt="">
                                        </div>
                                    <?php endforeach; ?>
                                </div>
                                <div class="slider-content">
                                    <?php foreach ($model->screenshots as $item): ?>
                                        <div class="item">
                                            <p><?= $item->description ?></p>
                                        </div>
                                    <?php endforeach; ?>
                                </div>
                                <?php endif; ?>
                            </div>
                        </section>
                        <?php endif; ?>
                        <?php if ($model->type == Course::TYPE_VIDEO): ?>
                        <section class="hidden-md-down">
                            <?= $this->render('view/_detail', ['model' => $model]) ?>
                        </section>
                        <?php endif; ?>

                    </div><!--end .k-course-details-main-left-->
                </div><!--end .k-course-details-main-->
                <div class="part-parent">
                    <div class="part-child-1">
                    <?php if ($model->type == Course::TYPE_VIDEO): ?>
                        <h3 class="hidden-lg-up">NỘI DUNG KHÓA HỌC</h3>
                        <section class="k-course-details-main hidden-lg-up">
                            <input id="showmore-course-list" type="checkbox" style="display: none">
                            <?= $this->render('view/_detail', ['model' => $model]) ?>
                            <label class="showmore-course-list-1" for="showmore-course-list">Xem Thêm</label>
                            <label class="showmore-course-list-2" for="showmore-course-list">Thu Gọn</label>
                        </section>
                    <?php endif; ?>
                    <?php if ($model->type == Course::TYPE_SOFTWARE && !empty($model->screenshots)): ?>
                        <h3 class="hidden-lg-up">SCREENSHOT ỨNG DỤNG</h3>
                        <section class="hidden-lg-up" style="padding: 20px; background: #fff; margin-top: 15px;">
                            <div id="k-course-details-screenshot-mb" class="k-course-details-screenshot">
                                <?php if (count($model->screenshots) == 1): ?>
                                    <div class="slider-image-single">
                                        <img src="<?= $cdnUrl . $model->screenshots[0]->image_url ?>" alt="">
                                    </div>
                                    <div class="slider-content-single">
                                        <p><?= $model->screenshots[0]->description ?></p>
                                    </div>
                                <?php else: ?>
                                    <div class="slider-image">
                                        <?php foreach ($model->screenshots as $item): ?>
                                            <div class="item">
                                                <img src="<?= $cdnUrl . $item->image_url ?>" alt="">
                                            </div>
                                        <?php endforeach; ?>
                                    </div>
                                    <div class="slider-content">
                                        <?php foreach ($model->screenshots as $item): ?>
                                            <div class="item">
                                                <p><?= $item->description ?></p>
                                            </div>
                                        <?php endforeach; ?>
                                    </div>
                                    <div class="slick-arrow-mb">
                                        <i class="prev fa fa-chevron-left slick-arrow" style="display: block;"></i>
                                        <i class="next fa fa-chevron-right slick-arrow" style="display: block;"></i>
                                    </div>
                                <?php endif; ?>
                            </div>
                        </section>
                    <?php endif; ?>
                    <?php
                    if ($model->getListCombo()->count() > 0) {
                        echo $this->render('_in_list_combo', ['model' => $model]);
                    }
                    ?>
                    </div>
                    <!-- end #list-combo-course -->
                    <div class="part-child-2">
                        <h3 class="hidden-lg-up">GIỚI THIỆU KHÓA HỌC</h3>
                        <section>
                            <div id="k-course-details-author" class="k-course-details-author" path="scrolling">
                                <div class="">
                                    <h4 class="title-content hidden-md-down"><?= ($model->type == Course::TYPE_VIDEO) ? 'Thông tin giảng viên' : 'Đơn vị cung cấp Ứng dụng' ?></h4>
                                    <ul>

                                        <li class="k-course-details-author-img">
                                            <?=
                                            CDNHelper::image($model->teacher->avatar, [
                                                'alt' => $model->teacher->profile->name,
                                                'class' => 'img-fluid',
                                                'size' => CDNHelper::IMG_SIZE_AVATAR_LARGE,
                                                'resizeMode' => 'crop',
                                            ])
                                            ?>
                                            <div class="info-teacher hidden-lg-up">
                                                <a href="<?= $teacherUrl ?>" class="name"><b><?= $model->teacher->profile->name ?></b></a>
                                                <div class="info"><?= $model->teacher->title ?></div>
                                            </div>
                                        </li>

                                        <li class="k-course-details-author-teacher">
                                            <div class="k-course-details-author-teacher-title">
                                                <a href="<?= $teacherUrl ?>" class="name hidden-md-down"><?= $model->teacher->profile->name ?></a>
                                                <span class="info hidden-md-down"><?= $model->teacher->title ?></span>
                                            </div>
                                            <a href="<?= $teacherUrl ?>" class="btn btn-info-teacher main hidden-md-down">XEM THÊM THÔNG TIN</a>
                                            <div class="content-teacher hidden-md-down">
                                                <?= $model->teacher->introduction; ?>
                                            </div>

                                            <?php if ($model->type == Course::TYPE_SOFTWARE): ?>
                                                <div class="box-application hidden-lg-up">
                                                    <div class="title-app"><i class="fa fa-mobile" aria-hidden="true"></i> <b>Phần mềm</b></div>
                                                    <p>Sau khi mua sẽ cài và sử dụng trên thiết bị di động của bạn</p>
                                                    <?php if (!empty($model->devicesText)): ?>
                                                    <div class="app-tit">Ứng dụng có thể cài đặt, sử dụng trên:<br><b><?= $model->devicesText ?></b></div>
                                                    <?php endif; ?>
                                                </div>
                                            <?php endif; ?>
                                            <input id="showmore-desciption-full" type="checkbox" style="display: none">
                                            <div class="desciption-full hidden-lg-up">
                                                <div class="wrapper">
                                                    <?= !empty($model->overview) ? $model->overview : $model->description ?>
                                                </div>
                                            </div>
                                            <label for="showmore-desciption-full" class="btn btn-info-teacher btn-info-teacher-1 hidden-lg-up">XEM THÊM</label>
                                            <label for="showmore-desciption-full" class="btn btn-info-teacher btn-info-teacher-2 hidden-lg-up">THU GỌN</label>
                                        </li>
                                    </ul>
                                </div><!--end .col-md-8 col-sm-7 col-xs-12-->
                            </div><!--end .k-course-details-author-->
                        </section>
                    </div>
                </div>
            </div>

            <div class="col-xl-4 col-md-11 col-xs-12 k-course-details-main-right hidden-md-down">

                <?= $this->render('view/_related_courses', ['model' => $model]) ?>

            </div><!--end .k-course-details-main-right-->

        </div>
    </div>

    <section>
        <?php
            $disable = '';
            if(isset(Yii::$app->params['disable_position']) && !empty(Yii::$app->params['disable_position'])){
                $disable = 'style= "display: none;"';
            }
        ?>
        <div <?= $disable ?> id="detail-comment-facebook" class="k-course-details-comments-reviews" path="scrolling">
            <div class="container">
                <div class="col-xl-8 col-md-11 col-xs-12 k-course-details-comments-list none_on_mobile">
                    <div class="wrapper">
                        <ul class="nav nav-tabs tablist-comment" role="tablist">
                            <li class="nav-item active">
                                <a class="nav-link" data-toggle="tab" href="#review" role="tab">Đánh giá (<?= $model->rating_users_count ?>)</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#comments-facebook" role="tab">Bình luận facebook</a>
                            </li>
                        </ul>

                        <div class="tab-content clearfix">
                        <div role="tabpanel" class="tab-pane clearfix active review-content" id="review">

                        </div>
                        <div role="tabpanel" class="tab-pane clearfix" id="comments-facebook">
                            <div class="comments-facebook-wrap">
                                <div class="fb-comments fb_iframe_widget fb_iframe_widget_fluid" data-href="<?= Yii::$app->request->absoluteUrl ?>" data-width="100%" data-numposts="10" data-colorscheme="light" fb-xfbml-state="rendered"><span style="height: 2964px;"><iframe id="f3de58d558cc564" name="f2a61674dcfc51c" scrolling="no" title="Facebook Social Plugin" class="fb_ltr fb_iframe_widget_lift" src="https://www.facebook.com/plugins/comments.php?api_key=0&amp;channel_url=http%3A%2F%2Fstaticxx.facebook.com%2Fconnect%2Fxd_arbiter%2Fr%2Fbz-D0tzmBsw.js%3Fversion%3D42%23cb%3Df1e525654c0931c%26domain%3Dlocalhost.vn%26origin%3Dhttp%253A%252F%252Flocalhost.vn%252Ff197b8f9d4f158%26relation%3Dparent.parent&amp;colorscheme=light&amp;href=https%3A%2F%2Fkyna.vn%2Fnhom-khoa-hoc%2Fbi-quyet-giao-dich-bat-dong-san-thanh-cong&amp;locale=vi_VN&amp;numposts=10&amp;sdk=joey&amp;skin=light&amp;version=v2.3&amp;width=100%25" style="border: none; overflow: hidden; height: 2964px; width: 100%;"></iframe></span></div>
                            </div>
                        </div>
                    </div>
                    </div>
                </div>

                <div class="rating_mb none_on_desktop">
                    <h3>ĐÁNH GIÁ</h3>
                    <?= $this->render('rating/create', ['model' => $model, 'isLearnedCourse' => $newItem == 2 ? true : false]) ?>
                    <div id="review_mb" class="review-content">

                    </div>
                </div>

                <div class="comments-facebook-wrap comment_mb none_on_desktop">
                    <h3>BÌNH LUẬN FACEBOOK</h3>
                    <div class="fb-comments fb_iframe_widget fb_iframe_widget_fluid" data-href="<?= Yii::$app->request->absoluteUrl ?>" data-width="100%" data-numposts="10" data-colorscheme="light" fb-xfbml-state="rendered"><span style="height: 2964px;"><iframe id="f3de58d558cc564" name="f2a61674dcfc51c" scrolling="no" title="Facebook Social Plugin" class="fb_ltr fb_iframe_widget_lift" src="https://www.facebook.com/plugins/comments.php?api_key=0&amp;channel_url=http%3A%2F%2Fstaticxx.facebook.com%2Fconnect%2Fxd_arbiter%2Fr%2Fbz-D0tzmBsw.js%3Fversion%3D42%23cb%3Df1e525654c0931c%26domain%3Dlocalhost.vn%26origin%3Dhttp%253A%252F%252Flocalhost.vn%252Ff197b8f9d4f158%26relation%3Dparent.parent&amp;colorscheme=light&amp;href=https%3A%2F%2Fkyna.vn%2Fnhom-khoa-hoc%2Fbi-quyet-giao-dich-bat-dong-san-thanh-cong&amp;locale=vi_VN&amp;numposts=10&amp;sdk=joey&amp;skin=light&amp;version=v2.3&amp;width=100%25" style="border: none; overflow: hidden; height: 2964px; width: 100%;"></iframe></span></div>
                </div>

                <div class="col-xl-4 col-md-11 col-xs-12 k-course-reviews none_on_mobile">
                    <?= $this->render('rating/create', ['model' => $model, 'isLearnedCourse' => $newItem == 2 ? true : false]) ?>
                </div>
            </div>
        </div>
    </section>

    <?= $this->render('view/_like_facebook') ?>

    <!-- $this->render('view/_card_slide') -->

    <div class="hidden-course-bar" data-spy="scroll" data-target="#scrollspy-course" data-offset-top="100">
        <div class="container">
            <div class="row flex-ae">
                <div class="col-lg-8 col-sm-12 fs12-lg" id="scrollspy-course">
                    <ul class="tab-btns nav">
                        <li class="active"><a href="javascript:void(0);" link="#k-course-details-about" class="scroll-detail-about nav-link">Giới thiệu</a></li>
                        <?php if ($model->type == Course::TYPE_VIDEO): ?>
                            <li><a href="javascript:void(0);" link="#k-course-details-curriculum" class="scroll-curriculum nav-link">Nội dung học</a></li>
                            <li><a href="javascript:void(0);" link="#k-course-details-author" class="scroll-detail-author nav-link">Giảng viên</a></li>
                        <?php endif; ?>
                        <?php if ($model->type == Course::TYPE_SOFTWARE): ?>
                            <?php if (!empty($model->screenshots)): ?>
                            <li><a href="javascript:void(0);" link="#k-course-details-screenshot" class="scroll-curriculum nav-link">Screenshot ứng dụng</a></li>
                            <?php endif; ?>
                            <li><a href="javascript:void(0);" link="#k-course-details-author" class="scroll-detail-author nav-link">Đơn vị cung cấp</a></li>
                        <?php endif; ?>

                        <?php if($disable == false):?>
                            <li><a href="javascript:void(0);" link="#detail-comment-facebook" class="scroll-detail-comment nav-link">Đánh giá &amp; bình luận</a></li>
                        <?php endif;?>
                    </ul>
                </div>

                <div class="col-lg-4 hidden-sm">
                    <ul class="button">
                        <li class="scrollBarDetail">
                            <?php if ($newItem === '1') : ?>
                                <?=
                                GaHtmlHelper::a('Đăng ký học<span class="icon-add-flyToElement"></span>', '#', [
                                    'data-pid' => $model->id,
                                    'class' => 'add-to-cart' . (!Yii::$app->user->isGuest ? ' register add-to-cart-scrollspy' : ''),
                                    'category' => 'CourseDetail',
                                    'action' => 'AddToCart',
                                    'label' => $model->name,
                                ])
                                ?>
                            <?php endif; ?>
                            <?php if ($newItem === '3') : ?>
                                <a class="add-to-cart notCombo reg" href="<?= Url::toRoute(['/cart/default/index']) ?>">Xem giỏ hàng và thanh toán</a>
                            <?php endif; ?>
                            <?php if ($newItem === '2') : ?>
                                <a class="go-to-class" href="<?= Url::toRoute(['/course/learning/index', 'id' => $model->id]) ?>">
                                    VÀO LỚP HỌC
                                </a>
                            <?php endif; ?>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</main>

<?php
$this->registerJsFile($cdnUrl . '/src/js/add-to-cart.js?v=15039938495', ['position' => View::POS_END]);
$this->registerJsFile($cdnUrl . '/src/js/slick.min.js', ['position' => View::POS_END]);
$loadReviewUrl = Url::to(['/course/default/load-review', 'course_id' => $model->id]);
$jsScript = "
$(document).ready(function(){
    var curPage = 0;

    function loadReview()
    {
        curPage++;
        $.ajax({
            method: 'get',
            url: '{$loadReviewUrl}',
            data: {
                page: curPage,
            },
            success: function (res) {
                $('.review-content .button-more').remove();
                $('.review-content .button-more img.loading-review').hide();
                $('.review-content').append(res);

                var obj_review = $('.review-content').find('.comments-box p.not-show');
                $(obj_review).each(function(){
                    var _text_review = $(this).data('content');
                    if (_text_review.length > 100){
                        _text_review = $(this).data('content').substr(0,99);
                        _text_review += ' ... <span style=\"background-color: #acb5be;color: white; padding: 0 4px; cursor: pointer\">+</span>';
                    }
                    $(this).html(_text_review);
                    $(this).removeClass('not-show');
                })
            }
        });
    }

    loadReview();

    $('body').on('click', '.review-content .button-more button', function(e){
        e.preventDefault();
        $('#review .button-more img.loading-review').show();
        $('#review .button-more button').hide();
        loadReview();
    });

    $('body').on('click', '.review-content .comments-box p', function(){
        var _text_review = $(this).data('content');
        if (_text_review.length > 100){
            if ($(this).hasClass('p-close'))
                $(this).html(_text_review.substr(0,99) + ' ... <span style=\"background-color: #acb5be;color: white; padding: 0 4px; cursor: pointer\">+</span>').removeClass('p-close').addClass('p-open');
            else
                $(this).html(_text_review + ' <span style=\"background-color: #acb5be;color: white; padding: 0 4px; cursor: pointer\">-</span>').addClass('p-close').removeClass('p-open');
        }
    });
});
";
$css = "
.k-course-details-main .course-overview {
    padding: 15px;
    background-color: white;
    margin-bottom: 1em;
}
.k-course-details-main .course-overview p {
    padding: 0;
    margin: 0 0 1em;
    background-color: transparent;
}
.k-course-details-main .course-overview ul li {
    margin-right: 0;
    position: static;
    font-size: 14px;
    display: list-item;
    list-style: inherit;
}
.k-course-details-main .course-overview ul {
    display: block;
    width: auto;
    padding: 0 0 0 3.2em;
    margin-bottom: 1em;
    list-style-type: initial;
}
.course-overview > p {
    border: none !important;
}
";

$this->registerJs($jsScript, View::POS_END);
?>
