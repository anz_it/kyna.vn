<?php
/**
 * Created by PhpStorm.
 * User: macintosh
 * Date: 8/17/17
 * Time: 11:25 AM
 */
/* @var $course \app\models\Course */
use common\helpers\CDNHelper;

$cdnUrl = CDNHelper::getMediaLink();

$isFreeCourse = $course->sellPrice == 0;
?>
<div class="scroll-box">
    <ul class="detail-part show notfinished">
        <li class="title-part">
            <i class="icon-arrow-collapse-open hidden-sm-down"></i>
            CHƯA HOÀN THÀNH (<?= count($notFinishedMissions) ?>)
            <i class="icon-arrow-down pull-right hidden-md-up"></i>
        </li>
        <?php foreach ($notFinishedMissions as $notFinishedMission) : ?>
        <li class="sub-part">
            <a href="javascript:void(0)">
                <img src="<?= $cdnUrl?>/img/kpoint/icon-quest-notfinished.png" alt="Chưa hoàn thành"> <?= $notFinishedMission->name ?> (+<?= $isFreeCourse && $notFinishedMission->k_point_for_free_course != null ? $notFinishedMission->k_point_for_free_course : $notFinishedMission->k_point ?> Kpoint) <?= $notFinishedMission->summaryText ?>
            </a>
        </li>
        <?php endforeach; ?>
    </ul>
    <ul class="detail-part show finished">
        <li class="title-part">
            <i class="icon-arrow-collapse-open hidden-sm-down"></i>
            HOÀN THÀNH (<?= count($finishedMissions) ?>)
            <i class="icon-arrow-down pull-right hidden-md-up"></i>
        </li>
        <?php foreach ($finishedMissions as $finishedMission) : ?>
            <li class="sub-part">
                <a href="javascript:void(0)">
                    <img src="<?= $cdnUrl?>/img/kpoint/icon-quest-finished.png" alt="Đã hoàn thành"> <?= $finishedMission->name ?> (+<?= $isFreeCourse && $finishedMission->k_point_for_free_course != null ? $finishedMission->k_point_for_free_course : $finishedMission->k_point ?> Kpoint) <?= $finishedMission->summaryText ?>
                </a>
            </li>
        <?php endforeach; ?>
    </ul>
</div>
