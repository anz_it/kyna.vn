<?php

namespace app\modules\career\controllers;

use kyna\career\models\Career;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * Default controller for the `career` module
 */
class CareerController extends \app\components\Controller
{
    public $layout =  '@app/modules/career/views/layouts/main';
    public $viewPath = '@app/modules/career/views/career';
    public $bodyId = 'page-about';

    public function init()
    {
        parent::init();
        $this->setViewPath($this->viewPath);
    }

    public function actionIndex($slug)
    {
        // get available career items
        $data = Career::findAll(['status' => Career::BOOL_YES]);
        // format render data
        $renderData = [];
        foreach ($data as $item) {
            $renderData[$item->category_id][] = $item;
        }
        $model = Career::findOne(['slug' => $slug]);
        if (!$model) {
            throw new NotFoundHttpException('link ko tồn tại');
        }
        return $this->render("index", [
            'model' => $model,
            'data' => $renderData
        ]);
    }
}
