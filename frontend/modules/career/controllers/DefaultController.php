<?php

namespace app\modules\career\controllers;

use kyna\career\models\Career;
use yii\web\Controller;

/**
 * Default controller for the `career` module
 */
class DefaultController extends \app\components\Controller
{
    public $layout =  '@app/modules/career/views/layouts/main';
    public $viewPath = '@app/modules/career/views/default';
    public $bodyId = 'page-about';

    public function init()
    {
        parent::init();
        $this->setViewPath($this->viewPath);
    }

    public function actionIndex()
    {
        // get available career items
        $data = Career::findAll(['status' => Career::BOOL_YES]);
        // format render data
        $renderData = [];
        foreach ($data as $item) {
            $renderData[$item->category_id][] = $item;
        }
        return $this->render("index", ['data' => $renderData]);
    }
}
