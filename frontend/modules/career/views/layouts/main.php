<!DOCTYPE HTML>
<?php

use frontend\widgets\FooterWidget;
use frontend\assets\CareerAsset;

CareerAsset::register($this);
$this->beginPage();
?>
<html  lang="<?= Yii::$app->language ?>">
<head>
 <?= $this->render('@app/views/layouts/common/html_head') ?>
 <!-- LINK FONT -->
 <link href='https://fonts.googleapis.com/css?family=Roboto:400,500,700,300' rel='stylesheet' type='text/css'>
 <!-- CSS CATE -->
 <link href="http://media.kyna.local/career/css/fonts.css" rel="stylesheet">
</head>

<body <?php if(!empty($this->context->bodyId)) { ?> id="<?= $this->context->bodyId; ?>" <?php } ?> <?php if(!empty($this->context->bodyClass)) { ?> class="<?= $this->context->bodyClass; ?>" <?php } ?> >
<?php $this->beginBody()?>

<?= $content; ?>

<?= FooterWidget::widget() ?>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
