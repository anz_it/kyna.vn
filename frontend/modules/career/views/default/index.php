<?php
use yii\web\View;
use \kyna\career\models\Career;
use \frontend\widgets\CareerHeaderWidget;
use \frontend\widgets\FooterWidget;
use yii\widgets\Breadcrumbs;

use common\helpers\GoogleSnippetHelper;

$this->title = 'Tuyển dụng - Kyna.vn';

// breadcrumbs css
$this->registerCss("
        @media (max-width: 640px) {
            .breadcrumb-container {
                display: none;
            }
        }
         .breadcrumb-container {
            margin: 0px;
            background-color: #fafafa;
            text-align: left;
            padding-top: 75px;
         }
         .breadcrumb {
            border-radius: 0px;
            padding: 8px 0px;
            margin: 0px 0px 0px 7px;
            background-color: inherit;
         }
        .breadcrumb > li + li::before {
            padding-right: .5rem;
            padding-left: .5rem;
            color: #a0a0a0;
            content: \"»\";
        }
         .breadcrumb > li {
            color: #666 !important;
            font-weight: bold;
         }
         .breadcrumb > li > a {
            color: inherit;
         }
         .breadcrumb > li.active {
            color: #666 !important;
            font-weight: normal;
         }
        #banner div p {
            top: 650px; 
        }
        #banner div span i {
            top: 750px;
        }
}
");
?>
    <div class="page-about-header">
        <?= CareerHeaderWidget::widget() ?>
        <!-- Start breadcrumbs-->
        <div class="breadcrumb-container">
            <div class="container">
                <?php
                $homeUrl = yii\helpers\Url::toRoute(['/'], true);
                echo Breadcrumbs::widget([
                    'options' => [
                        'class' => 'breadcrumb',
                        'itemscope' => '',
                        'itemtype' => 'http://schema.org/BreadcrumbList',
                    ],
                    'homeLink' => [
                        'label' => '<h5><i class="fa fa-home"></i> Trang chủ</h5>',
                        'url' => $homeUrl,
                        'encode' => false,
                        'template' => GoogleSnippetHelper::renderBreadcrumbTemplate($homeUrl, "<i class=\"fa fa-home\"></i> Trang chủ", 1)

                    ],
                    'links' => [
                        [
                            'label' => "Tuyển dụng",
                            'template' => GoogleSnippetHelper::renderBreadcrumbTemplate($homeUrl.'p/kyna/tuyen-dung', 'Tuyển dụng', 2, true)
                        ],
                    ],
                ])
                ?>
            </div>
        </div>
        <!-- End breadcrumbs -->
        <section id="banner">
            <div class="container">
                <p class="col-sm-8 col-xs-12">Tham gia ngay cùng chúng tôi</p>
                <span><i class="fa fa-chevron-down" aria-hidden="true"></i></span>
            </div>
            <!--end .container-->
        </section>
    </div>
    <!--end wrap-header-->
    <main class="career-home">
        <section id="k-apply-join">
            <div class="container">
                <h2>Join the team</h2>
                <ul class="first row">
                    <li class="col-md-8 col-xs-12 box first">
                        <img src="/career/img/kin_ca_3man.jpg" alt="Join the team" class="img-responsive">
                    </li>
                    <li class="col-md-4 col-xs-12 box last">
                        <p>Là một trong những đơn vị đi đầu trong lĩnh vực đào đạo trực tuyến tại Việt Nam từ năm 2011. Tháng 5/2013, Dream Viet Education bắt đầu xây dựng hệ thống website Kyna.vn và hợp tác với các giảng viên uy tín, có chuyên môn cao nhằm tạo ra các khóa học online chất lượng. Sau 2 năm hoạt động, Kyna.vn hiện có hơn 150,000 học viên đang theo học với tổng cộng hơn 2,500 video nội dung cùng các bài học đa dạng.</p>
                    </li>
                </ul>

                <ul class="last row">
                    <li class="col-md-4 col-xs-12 box">
                        <p>Mục tiêu của Kyna.vn giữ vững được ngôi vị tiên phong, đồng thời đa dạng hóa, nâng cấp những nội dung đào tạo. Một mặt đáp ứng được nhu cầu học tập online ngày càng lớn của học viên trong nước, mặt khác là nâng tầm ảnh hưởng, mở rộng mạng lưới phát triển sang các nước láng giềng châu Á. Để làm được điều đó, Kyna.vn tìm kiếm và trân trọng những thành viên: cùng có đam mê và thế mạnh về lĩnh vực đào tạo trực tuyến đang còn khá mới mẻ này. Chỉ cần bạn có niềm tin và nỗ lực muốn góp sức Kyna.vn chúng tôi luôn cần bạn.</p>
                    </li>
                    <li class="col-md-4 col-xs-12 box">
                        <img src="/career/img/kin_ca_cus_huong.jpg" alt="Nhân viên" class="img-responsive">
                    </li>
                    <li class="col-md-4 col-xs-12 box">
                        <img src="/career/img/kin_ca_dev_khoa.jpg" alt="Nhân viên" class="img-responsive">
                    </li>
                </ul>
            </div>
        </section>

        <section id="k-apply-separator">
            <div class="container">
                <h2>BẠN SẼ THẤY, LÀM VIỆC Ở KYNA.VN TUYỆT VỜI NHƯ THẾ NÀO</h2>
                <img src="/career/img/kin_ca_kyna_team.jpg" alt="Ban quản lý và leader" class="img-responsive leader">
                <ul class="first row">
                    <li class="col-md-5 col-xs-12 box">
                        <p>
                            - Nhân sự trẻ, năng động, giàu đam mê: Cùng là những người trẻ, chúng ta có khát khao. Và cũng bởi vì trẻ, chúng ta thừa niềm tin và cơ hội để chinh phục thử thách.<br/>
                            - Đội ngũ quản lý: Không ai khác chính là anh Nguyễn Thanh Minh và anh Nguyễn Tấn Hiếu. Cả hai anh cùng trẻ nhưng đều đạt được những thành công nhất định Năng nổ, nhiệt tình và trên hết là sự tôn trọng thấu hiểu nhân viên.<br/>
                            - Văn hóa: Tất cả mọi người cùng thống nhất xem Kyna.vn là ngôi nhà thứ 2, cùng nhau cống hiến và tận hưởng những niềm vui trong công việc. Nơi mà “niềm vui” được trân trọng và đánh giá là 1 trong 5 giá trị cốt lõi.
                            Cơ hội: Để giữ vững được ngôi vị tiên phong trong lĩnh vực đào tạo trực tuyến tại Việt Nam, mỗi cá nhân tại Kyna.vn được tạo điều kiện tốt nhất để trau dồi và phát huy những thế mạnh của mình.<br/>
                            - Tất nhiên, lương thưởng cạnh tranh, chế độ đãi ngộ tốt, tương xứng với những gì bạn cống hiến.
                        </p>
                    </li>
                    <li class="col-md-7 col-xs-12 box">
                        <img src="/career/img/kin_ca_2cus.jpg" alt="Nhân viên" class="img-responsive">
                    </li>
                </ul>
                <ul class="last row">
                    <li class="col-md-6 col-xs-12 video box">
                        <div class="videoWrapper">
                            <!-- Copy & Pasted from YouTube -->
                            <iframe width="560" height="315" src="https://www.youtube.com/embed/kfeCOs8gILo" frameborder="0" allowfullscreen></iframe>
                        </div>
                    </li>
                    <li class="col-md-6 col-xs-12 box">
                        <img src="/career/img/kin_ca_video_editor.jpg" alt="Nhân viên" class="img-responsive">
                    </li>
                </ul>
            </div><!--end .container-->
        </section>

        <section id="k-apply-main">
            <div class="container">
                <h2>THAM GIA NGAY CÙNG CHÚNG TÔI!</h2>
                <p>Được thử thách trong môi trường năng động. Được đào tạo chuyên sâu về lĩnh vực đào tạo trực tuyến. Được công nhận năng lực và mở rộng con đường thăng tiến. Bạn luôn được chào đón tại Kyna.vn!</p>
                <ul>
                    <li class="list first">
                        <div class="col-md-7 col-xs-12 title">
                            <h4>Vị trí đang tuyển</h4>
                        </div>
                        <div class="col-md-3 col-xs-12 number">
                            Số lượng
                        </div>
                        <div class="col-md-2 col-xs-12 date">
                            Hạn chót nộp
                        </div>
                    </li>
                <?php if (!empty($data)): ?>
                    <?php foreach ($data as $key => $list): ?>
                        <li class="list title">
                            <div class="col-md-12 col-xs-12 title">
                                <h5><?= Career::$category[$key] ?></h5>
                            </div>
                        </li>
                        <?php foreach ($list as $item): ?>
                        <li class="list">
                            <div class="col-md-7 col-xs-12 title">
                                <a href="<?= $item->getLink() ?>"><?= $item->short_title ?></a>
                            </div>
                            <div class="col-md-3 col-xs-12 number">
                                <span class="mb">Số lượng:</span> <?= $item->quantity ?>
                            </div>
                            <div class="col-md-2 col-xs-12 date">
                                <span class="mb">Hạn chót nộp:</span> <?= Yii::$app->formatter->asDatetime($item->end_date, Yii::$app->formatter->dateFormat) ?>
                            </div>
                        </li>
                        <?php endforeach; ?>
                    <?php endforeach; ?>
                <?php endif; ?>
                </ul>
            </div>
        </section>

        <section>
            <div class="k-subscriber" style="display:none">
                <div class="container">
                    <div class="col-lg-7 col-xs-12 k-subscriber-title">
                        <ul>
                            <li>ĐĂNG KÝ NHẬN MAIL</li>
                            <li>Đăng ký để nhận những bài viết thú vị và ưu đã đặc biệt từ Kyna.vn</li>
                        </ul>
                    </div>
                    <div class="col-lg-5 col-xs-12 k-subscriber-form">
                        <form action="https://kyna.vn/giang-day" id="form_subscribe" onsubmit="return submit_subscribe();" method="post" accept-charset="utf-8">
                            <div class="col-sm-8" id="inp_subscribe">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="icon-mail-outline"></i> </span>
                                    <input type="text" class="form-control" placeholder="Email" name="email" required="" id="email_footer_subscribe">
                                </div>
                            </div>
                            <div class="col-sm-4 btn-wrap">
                                <input type="submit" name="submit" value="ĐĂNG KÝ" class="btn btn-primary">
                            </div>
                        </form>
                    </div>
                </div>
                <!--end .container-->
            </div>
            <!--end k-subscriber-->
        </section>
    </main>
<?php
$script = "
    ;(function($, window, document, undefined){
        $(document).ready(function(){
            // ====================================================================
            $(\"#kin_wlc\").height(window.innerHeight);
            $(\"#kin_wlc .fa-angle-down\").click(function(){
                $('html,body').animate({scrollTop: window.innerHeight },'slow');
            });
            $(\".team_cat a\").click(function(e){
                e.preventDefault();
                var cat = $(this).attr('data-cat');
                if (cat == 'all'){
                    $(\".team_list .col-sm-3\").show();
                }else{
                    $(\".team_list .col-sm-3\").hide();
                    $(\".team_list .\"+cat).fadeIn(1000);
                }
            });
            // ====================================================================
            function submit_subscribe(){
                var chk = $(\"#form_subscribe\")[0].checkValidity();
                var email = $(\"#email_footer_subscribe\").val();
                if (!chk || email.length < 6){
                    alert('Email chưa đúng !');
                    return false;
                }
    
                var that = $(\"#inp_subscribe\");
                var data_send = $(\"#form_subscribe\").serialize() + \"&btn_submit=true\";
                var frm_html = that.html();
                that.html(\"<img src='/career/img/w8loading32x34.GIF' align='middle' />\");
                $.post('/users/email_subscribe/sendy?no_name=true&ln=kyna_footer', data_send,
                    function(data){
                        if (data.inserted){ that.html(\"<span>Đăng ký Thành công !</span>\"); }
                        else if (data.inserted == 0) {
                            that.html(\"<span>Bạn đã đăng ký email này rồi !</span>\");
                        }else{  that.html(frm_html); }
                    }, 'json'
                );
                return false;
            }
            var url = window.location.href;
            $(\"#kif_menu > .navbar-nav>li>a\").each(function(){
                var href = $(this).attr('href');
                if (url.indexOf(href)>0){
                    $(this).addClass('active');
                }
            });
            // ====================================================================
            var HeightAboutWrapHeader = $(\"#page-about .page-about-header\").height();
            $('#page-about #banner').css({\"height\": + window.innerHeight + \"px\"})
            $(\"#page-about #banner span i\").click(function () {
                $('html,body').animate({scrollTop: window.innerHeight - 50}, 'slow');
            });
        });
    })(window.jQuery || window.Zepto, window, document);";
$this->registerJs($script, View::POS_END, 'career-homepage');
