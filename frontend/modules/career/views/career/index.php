<?php
use yii\web\View;
use \kyna\career\models\Career;
use \frontend\widgets\CareerHeaderWidget;
use \frontend\widgets\FooterWidget;

$this->title = $model->title . ' - Kyna.vn';
?>

<div class="page-about-header">
    <?= CareerHeaderWidget::widget() ?>
</div>
<!--end wrap-header-->
<main class="detail-career">
    <section>
        <div class="container">
            <div class="col-md-4 col-xs-12 sidebar">
                <h3>Vị trí đang tuyển</h3>
                <ul class="hr_title_list">
                    <?php if (!empty($data)): ?>
                        <?php foreach ($data as $key => $list): ?>
                            <li class="title"><?= Career::$category[$key] ?></li>
                            <?php foreach ($list as $item): ?>
                                <li><a href="<?= $item->getLink() ?>"><?= $item->short_title ?></a></li>
                            <?php endforeach; ?>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </ul>
            </div><!--end .sidebar-->
            <div class="col-md-8 col-xs-12 content" style="padding-bottom: 20px;">
                <div class="clearfix row">
                    <?= $model->getDateDescription() ?>
                </div>
                <div class="hr_content">
                    <h3 class="text-left" style="line-height: 35px;"><?= $model->title ?></h3>
                    <div class="separator">
                        <?= $model->content ?>
                    </div>

                </div>
                <div class="clearfix"></div>

            </div><!--end .content-->
        </div><!--end .container-->
    </section>
</main>


