<?php

namespace frontend\modules\user\assets;

use common\assets\FrontendAsset;
use yii\web\View;

/**
 * This is class asset bunle for `course` layout
 */
class UserAsset extends FrontendAsset
{

    // include css files
    public $css = [
        'css/main.min.css',
    ];
    public $cssOptions = [
        'type' => 'text/css'
    ];
    // include js
    public $js = [
        ['src/js/courses.js', 'position' => View::POS_END],
        ['src/js/offpage.js', 'position' => View::POS_END],
        ['js/script-main.js', 'position' => View::POS_END],
        ['src/js/main.js?version=1521096761111', 'position' => View::POS_END],
        ['js/bootstrap.min.js', 'position' => View::POS_END],
        ['src/js/owl.carousel.min.js', 'position' => View::POS_END],
        // JS Menu Mobile
        ['js/jquery.sidr.min.js', 'position' => View::POS_END],
        // ajax
        ["js/ajax-caller.js", 'position' => View::POS_END],
    ];

    public $depends = [
        'yii\web\YiiAsset',
    ];

    public $jsOptions = ['position' => View::POS_HEAD];
}
