<?php
/**
 * Created by PhpStorm.
 * User: macintosh
 * Date: 7/12/17
 * Time: 2:53 PM
 */

namespace app\modules\user\components;

use Yii;
use yii\web\NotFoundHttpException;

class AuthAction extends \yii\authclient\AuthAction
{

    /**
     * Runs the action.
     */
    public function run()
    {
        if (!empty($_GET[$this->clientIdGetParamName])) {
            $clientId = $_GET[$this->clientIdGetParamName];
            /* @var $collection \yii\authclient\Collection */
            $collection = Yii::$app->get($this->clientCollection);
            if (!$collection->hasClient($clientId)) {
                throw new NotFoundHttpException("Unknown auth client '{$clientId}'");
            }
            $client = $collection->getClient($clientId);

            $returnUrl = Yii::$app->user->getReturnUrl(Yii::$app->request->referrer);
            Yii::$app->user->setReturnUrl($returnUrl);

            return $this->auth($client);
        }

        throw new NotFoundHttpException();
    }
}