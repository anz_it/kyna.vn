<?php

namespace app\modules\user\components;

class Facebook extends \Facebook\Facebook {
    public $app_id = '191634267692814';
    public $app_secret = '2807d29ac5742521188979e8c9ce02bc';
    public $default_graph_version = \Facebook\Facebook::DEFAULT_GRAPH_VERSION;
    public $persistent_data_handler = false;

    public function __construct()
    {
        parent::__construct([
            'app_id' => $this->app_id,
            'app_secret' => $this->app_secret,
            'default_graph_version' => $this->default_graph_version,
            'persistent_data_handler' => $this->persistent_data_handler,
        ]);
    }
}
