<?php

namespace app\modules\user\controllers;

use common\helpers\RequestCookieHelper;
use kyna\order\models\Order;
use kyna\order\models\traits\OrderTrait;
use Yii;
use yii\helpers\ArrayHelper;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use yii\web\Cookie;
use yii\web\Response;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;

use dektrium\user\models\LoginForm;

use kyna\user\models\User;
use kyna\user\models\UserCourse;
use kyna\settings\models\Setting;

use app\models\Category;
use app\modules\user\components\AuthAction;

/*
 * Class SecurityController override dektrium Controller.
 */
class SecurityController extends \dektrium\user\controllers\SecurityController
{
    public $rootCats = [];
    public $settings = [];
    public $bodyClass = '';

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    ['allow' => true, 'actions' => ['login', 'auth', 'blocked', 'forgot-password', 'force-login', 'login-by-cookie', 'test'], 'roles' => ['?']],
                    ['allow' => true, 'actions' => ['login', 'auth', 'logout', 'force-login', 'login-by-cookie' ,'test'], 'roles' => ['@']],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                    'login-by-cookie' => ['post'],
                    'test' => ['post'],
                ],
            ],
        ];
    }

    /** @inheritdoc */
    public function actions()
    {
        return [
            'auth' => [
                'class' => AuthAction::className(),
                // if user is not logged in, will try to log him in, otherwise
                // will try to connect social account to user.
                'successCallback' => \Yii::$app->user->isGuest
                    ? [$this, 'authenticate']
                    : [$this, 'connect'],
            ],
        ];
    }

    public function init()
    {

        $ret = parent::init();

        $this->rootCats = Category::getList(0, ['id', 'name', 'slug']);
        $this->settings = ArrayHelper::map(Setting::find()->all(), 'key', 'value');

        $this->on(self::EVENT_AFTER_LOGIN, [$this, 'afterLogin']);
        $this->on(self::EVENT_AFTER_LOGOUT, [$this, 'afterLogout']);

        return $ret;
    }

    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            //$this->goHome();
            $this->redirect(['/user/course/index']);
        }

        // remove session activation_code if any
        if (Yii::$app->session->has('activation_code')) {
            Yii::$app->session->remove('activation_code');
        }

        /** @var LoginForm $model */
        $model = Yii::createObject(LoginForm::className());
        $event = $this->getFormEvent($model);

        $this->trigger(self::EVENT_BEFORE_LOGIN, $event);
        if ($model->load(Yii::$app->request->post())) {
            if ($model->login()) {
                $user = Yii::$app->user->identity;

                if (!$model->rememberMe) {
                    $user->auth_key = Yii::$app->security->generateRandomString();
                    $user->save(false);
                }

                $this->trigger(self::EVENT_AFTER_LOGIN, $event);

                $defaultUrl = Yii::$app->request->post('currentUrl', ['/user/course/index']);
                $redirectUrl = Yii::$app->user->getReturnUrl($defaultUrl);
                if (Yii::$app->request->isAjax) {
                    Yii::$app->response->format = Response::FORMAT_JSON;
                    return [
                        'result' => true,
                        'redirectUrl' => $redirectUrl,
                    ];
                } else {
                    $this->redirect($redirectUrl);
                }
            }

            if (Yii::$app->request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return [
                    'result' => false,
                    'errors' => $model->errors
                ];
            }
        }

        if(Yii::$app->request->isAjax) {
            return $this->renderAjax('@app/modules/user/views/security/login');
        }

        return $this->render('@app/modules/user/views/security/login');
    }

    public function afterLogin()
    {
        $cart = Yii::$app->cart;
        $positions = $cart->getPositions();

        $removeCourses = [];
        foreach ($positions as $position) {
            $alreadyInCourse = UserCourse::find()->where([
                'user_id' => Yii::$app->user->id,
                'course_id' => $position->id
            ])->exists();

            if ($alreadyInCourse) {
                // user has already registerd this course => remove cart item
                $cart->remove($position);
                $removeCourses[] = $position->name;
            }
        }

        if (!empty($removeCourses)) {
            $message = "Đã bỏ các khóa học sau ra khỏi giỏ hàng của bạn bởi vì bạn đã tham gia các khóa học này: <b>" . implode(',', $removeCourses)  . "</b>.";
            Yii::$app->session->setFlash('warning', $message);
            if (empty($cart->getPositions())) {
                // cart is empty => redirect to cart index with message
                Yii::$app->user->setReturnUrl(['/cart/default/index']);
            }
        }
    }

    public function actionForceLogin($user_id, $hash)
    {
        if (!Yii::$app->user->isGuest) {
            Yii::$app->user->logout();
        }

        $user = User::findOne($user_id);
        if (is_null($user) || $user->auth_key !== $hash) {
            throw new NotFoundHttpException();
        }

        if (Yii::$app->user->login($user)) {
            // refresh _request cookie
//            RequestCookieHelper::refreshCookie();
            // end refresh _request cookie
            $this->goHome();
        }
    }

    /**
     * Logs the user out and then redirects to the homepage.
     *
     * @return Response
     */
    public function actionLogout()
    {
        $event = $this->getUserEvent(\Yii::$app->user->identity);

        $this->trigger(self::EVENT_BEFORE_LOGOUT, $event);

        \Yii::$app->getUser()->logout();

        $this->trigger(self::EVENT_AFTER_LOGOUT, $event);

        return $this->goHome();
    }

    public function afterLogout() {
        $cart = Yii::$app->cart;

        // remove all item after logout
        $cart->emptyCart();
    }

}
