<?php

namespace app\modules\user\controllers;

use kyna\gamification\models\UserPoint;
use Yii;

use dektrium\user\helpers\Password;

use kyna\course\models\CourseDocument;
use kyna\course\models\response\AnalyticResponseModel;
use kyna\course\models\search\CourseLearnerQnaSearch;
use kyna\user\models\UserCourse;
use kyna\user\models\search\UserCourseSearch;

use common\helpers\ResponseModel;
use common\recommendation\Recommendation;
use pahanini\refiner\DataProvider;
/**
 * CourseController
 */
class CourseController extends \app\modules\user\components\Controller
{

    public function beforeAction($action)
    {
        $ret = parent::beforeAction($action);

        $user = Yii::$app->user->identity;
        if ($user != null && Password::validate((string)$user->created_at, $user->password_hash)) {
            Yii::$app->session->setFlash('warning', 'Bạn cần phải đổi mật khẩu cho lần đăng nhập đầu tiên.');
            Yii::$app->session->setFlash('need-to-change-pass', 'Bạn cần phải đổi mật khẩu cho lần đăng nhập đầu tiên.');
            $this->redirect(['/user/profile/edit']);
        }

        return $ret;
    }

    public function actionIndex()
    {
        $searchModel = new UserCourseSearch();
        $searchModel->user_id = Yii::$app->user->id;
        $searchModel->is_activated = UserCourseSearch::BOOL_YES;

        $dataProvider = $searchModel->searchCourse(Yii::$app->request->queryParams);

        if (Yii::$app->request->isAjax) {
            return $this->renderPartial('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider
            ]);
        }

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionStarted()
    {
        $searchModel = new UserCourseSearch();
        $searchModel->user_id = Yii::$app->user->id;
        $searchModel->is_activated = UserCourseSearch::BOOL_YES;
        $searchModel->is_started = UserCourse::BOOL_YES;

        $dataProvider = $searchModel->searchCourse(Yii::$app->request->queryParams);

        if (Yii::$app->request->isAjax) {
            return $this->renderPartial('started', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider
            ]);
        }

        return $this->render('started', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider
        ]);
    }

    public function actionNotStarted()
    {
        $searchModel = new UserCourseSearch();
        $searchModel->user_id = Yii::$app->user->id;
        $searchModel->is_activated = UserCourseSearch::BOOL_YES;
        $searchModel->is_started = UserCourse::BOOL_NO;

        $dataProvider = $searchModel->searchCourse(Yii::$app->request->queryParams);

        if (Yii::$app->request->isAjax) {
            return $this->renderPartial('not-started', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider
            ]);
        }

        return $this->render('not-started', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider
        ]);
    }

    public function actionAnalytic()
    {
        if (Yii::$app->request->isAjax) {
            $userPoint = UserPoint::findOne(Yii::$app->user->id);

            $resp = new AnalyticResponseModel();

            $courseIds = UserCourse::find()->joinWith('course')->where(['user_id' => Yii::$app->user->id, 'is_activated' => UserCourseSearch::BOOL_YES])->select('course_id')->column();

            $resp->course_count = count($courseIds);

            $question = new CourseLearnerQnaSearch();

            $question->user_id = Yii::$app->user->id;

            $resp->question_count = $question->search(null)->totalCount;

            $resp->document_count = CourseDocument::find()->where(['course_id' => $courseIds])->count();

            $resp->k_point = $userPoint != null ? $userPoint->k_point : 0;

            return ResponseModel::responseJson($resp);
        }

        return ResponseModel::responseJson(null, 200, 'Http request is not allowed');
    }
}