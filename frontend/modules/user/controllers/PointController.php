<?php
/**
 * Created by PhpStorm.
 * User: macintosh
 * Date: 8/25/17
 * Time: 2:06 PM
 */

namespace app\modules\user\controllers;

use Yii;
use kyna\gamification\models\search\UserPointHistorySearch;

class PointController extends \app\modules\user\components\Controller
{

    public function actionIndex()
    {
        $searchModel = new UserPointHistorySearch();

        $searchModel->user_id = Yii::$app->user->id;

        if (Yii::$app->request->isAjax) {
            return $this->renderPartial('index', [
                'searchModel' => $searchModel,
            ]);
        }

        return $this->render('index', [
            'searchModel' => $searchModel,
        ]);
    }
}