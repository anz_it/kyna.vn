<?php

namespace app\modules\user\controllers;

use Yii;
use yii\web\Response;
use yii\web\UploadedFile;
use app\modules\user\models\forms\ProfileForm;
use common\helpers\CDNHelper;
use common\lib\CDNImage;

/**
 * Description of ProfileController
 */
class ProfileController extends \app\modules\user\components\Controller
{
    public function actionUploadAvatar()
    {
        $user = Yii::$app->user->identity;
        if ($avatarUrl = $this->_uploadImage($user, 'avatar')) {
            $user->avatar = $avatarUrl;
        }
        if ($user->save()) {
            $resizedAvatar = CDNHelper::image($user->avatar, [
                'resizeMode' => 'crop',
                'size' => CDNHelper::IMG_SIZE_AVATAR_LARGE,
                'returnMode' => 'url',
            ]);
            Yii::$app->session->set('lastUploadFiles', [$resizedAvatar]);
        }
    }

    public function actionLastUploaded()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        return Yii::$app->session->get('lastUploadFiles');
    }

    private function _uploadImage($model, $attribute)
    {
        $files = UploadedFile::getInstances($model, $attribute);
        if (!sizeof($files)) {
            $oldAttributes = $model->oldAttributes;

            return isset($oldAttributes[$attribute]) ? $oldAttributes[$attribute] : false;
        }

        $file = $files[0];

        $cdnImage = new CDNImage($model, $attribute);
        $result = $cdnImage->upload($file);

        return $result;
    }

    public function actionEdit()
    {
        $isMobile = Yii::$app->devicedetect->isMobile() || Yii::$app->devicedetect->isTablet();
        if ($isMobile) {
            $this->layout = '@app/modules/user/views/layouts/user_profile_mobile';
        }

        $profile = new ProfileForm();

        if ($profile->load(Yii::$app->request->post()) && $profile->validate()) {
            if ($profile->save()) {
                if ($returnUrl = Yii::$app->request->get('returnUrl')) {
                    return $this->redirect($returnUrl);
                }

                Yii::$app->session->setFlash('success', 'Cập nhật thành công!');
                return $this->renderAjax('edit', [
                    'profile' => $profile,
                    'isMobile' => $isMobile
                ]);
            }
        }

        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('edit', [
                'profile' => $profile,
                'isMobile' => $isMobile
            ]);
        }

        return $this->render('edit', [
            'profile' => $profile,
            'isMobile' => $isMobile
        ]);
    }

    public function actionProfile()
    {
        return $this->render('profile');
    }
}
