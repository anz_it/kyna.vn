<?php

namespace app\modules\user\controllers;

use Yii;
use kyna\order\models\search\OrderSearch;

class TransactionController extends \app\modules\user\components\Controller
{

    public function actionComplete()
    {
        $searchModel = new OrderSearch();

        $searchModel->user_id = Yii::$app->user->id;
        $statuses = [OrderSearch::ORDER_STATUS_COMPLETE];

        return $this->render('complete', [
            'searchModel' => $searchModel,
            'statuses' => $statuses
        ]);
    }

    public function actionInComplete()
    {
        $searchModel = new OrderSearch();

        $searchModel->user_id = Yii::$app->user->id;
        $statuses = [
            OrderSearch::ORDER_STATUS_PAYMENT_FAILED,
            OrderSearch::ORDER_STATUS_REQUESTING_PAYMENT,
            OrderSearch::ORDER_STATUS_NEW,
            OrderSearch::ORDER_STATUS_PENDING_CONTACT,
            OrderSearch::ORDER_STATUS_PENDING_PAYMENT,
            OrderSearch::ORDER_STATUS_DELIVERING
        ];

        return $this->render('in_complete', [
            'searchModel' => $searchModel,
            'statuses' => $statuses
        ]);
    }
}
