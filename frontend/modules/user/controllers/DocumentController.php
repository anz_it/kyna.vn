<?php

namespace app\modules\user\controllers;

use kyna\user\models\search\UserCourseSearch;
use Yii;

class DocumentController extends \app\modules\user\components\Controller
{
    
    public function actionIndex()
    {
        $searchModel = new UserCourseSearch();
        $searchModel->user_id = Yii::$app->user->id;
        $searchModel->is_activated = UserCourseSearch::BOOL_YES;

        if (Yii::$app->request->isAjax) {
            return $this->renderPartial('index', [
                'searchModel'      => $searchModel
            ]);
        }
        
        return $this->render('index', [
            'searchModel' => $searchModel
        ]);
    }
}
