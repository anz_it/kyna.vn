<?php

namespace app\modules\user\controllers;

use app\models\CourseSearch;
use kyna\course\models\Course;
use kyna\course\models\Teacher;
use kyna\course\models\TeacherNotify;
use kyna\user\models\UserMeta;
use Yii;
use yii\filters\AccessControl;

use dektrium\user\helpers\Password;

use kyna\course\models\CourseDocument;
use kyna\course\models\response\AnalyticResponseModel;
use kyna\course\models\search\CourseLearnerQnaSearch;
use kyna\user\models\UserCourse;
use kyna\user\models\search\UserCourseSearch;

use common\helpers\ResponseModel;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;

/**
 * TeacherController
 */
class TeacherController extends \app\components\Controller
{
    public $layout = '@app/modules/user/views/layouts/teacher';
    public $teacher;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index'],
                        'roles' => ['?', '@'],
                    ],
                ],
            ],
        ];
    }

    public function getViewPath()
    {
        return Yii::getAlias('@app/modules/user/views/teacher');
    }

    public function actionIndex($slug)
    {
        $get = Yii::$app->request->get();
        if (isset($get['affiliate_id'])) {
            $route = ['/user/teacher/index'];
            unset($get['affiliate_id']);
            if (sizeof($get)) {
                $route = array_merge($route, $get);
            }

            $url = Url::toRoute($route);
            return $this->redirect($url, 301);
        }

        $teacher = $this->getTeacher($slug);
        Yii::$app->view->registerMetaData($teacher);
        $this->view->params['teacher'] = $teacher;
        $searchModel = new CourseSearch();
        $dataProvider = $searchModel->searchCourseByTeacher(['teacher_id' => $teacher->id]);

        if (Yii::$app->request->isAjax) {
            return $this->renderPartial('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                'teacher' => $teacher
            ]);
        }

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'teacher' => $teacher
        ]);
    }

    public function actionAnalytic($slug)
    {
        if (Yii::$app->request->isAjax) {

            $teacher = $this->getTeacher($slug);
            $courseQuery = Course::find()->where(['teacher_id' => $teacher->id, 'status' => Course::BOOL_YES, 'is_deleted' => Course::BOOL_NO]);

            $resp = new AnalyticResponseModel();

            $resp->course_count  = $courseQuery->count();

            $question = new CourseLearnerQnaSearch();

            $question->teacherId = $teacher->id;
            $question->is_approved = CourseLearnerQnaSearch::BOOL_YES;

            $resp->question_count = $question->search(['course_status' => Course::STATUS_ACTIVE, 'only_question' => true])->totalCount;

            $courses = $courseQuery->all();
            $resp->hour_count = 0;
            if ($courses) {
                foreach ($courses as $course) {
                    $userCourses = UserCourse::find()
                        ->where([
                            "course_id" => $course->id,
                            "is_activated" => UserCourse::BOOL_YES
                        ])
                        ->count();
                    $resp->hour_count += $course->total_time / 3600 * $userCourses;
                }
            }
            $resp->hour_count = ($resp->hour_count < 1) ? 1 : $resp->hour_count;
            $resp->hour_count = round($resp->hour_count);

            return ResponseModel::responseJson($resp);
        }

        return ResponseModel::responseJson(null, 200);
    }

    public function actionReceiveNotify($slug)
    {
        $teacher = $this->getTeacher($slug);
        if(Yii::$app->request->isAjax) {
            $notify = TeacherNotify::findOne([
                'teacher_id' => $teacher->id,
                'user_id' => Yii::$app->user->id
            ]);
            if (empty($notify)) {
                $notify = new TeacherNotify();
                $notify->teacher_id = $teacher->id;
                $notify->user_id = Yii::$app->user->id;
            }
            if ($notify->save()) {
                $status = true;
                $message = 'Bạn đã đăng ký nhận tin mới của giảng viên.';
            } else {
                $status = false;
                $message = 'Xảy ra lỗi trong quá trình đăng ký. Vui lòng thử lại sau!';
            }
            return $this->renderAjax('receive-notify', [
                'status' => $status,
                'message' => $message
            ]);
        }
    }

    private function getTeacher($slug)
    {
        $userMetaTblName = UserMeta::tableName();
        $teacherTblName = Teacher::tableName();
        $teacher = Teacher::find()
            ->join('INNER JOIN', UserMeta::tableName(), "{$userMetaTblName}.user_id = {$teacherTblName}.id AND {$userMetaTblName}.key = 'slug' AND {$userMetaTblName}.value = '{$slug}'")
            ->one();
        if (empty($teacher)) {
            throw new NotFoundHttpException("Không tìm thấy Teacher với slug {$slug}");
        }
        return $teacher;
    }
}