<?php
/**
 *
 */
namespace app\modules\user\controllers;

use kyna\course\models\CourseDiscussionSearch;
use yii;

class DiscussController extends \app\modules\user\components\Controller
{
    public function actionIndex()
    {
        $modelSearch = new CourseDiscussionSearch();

        $modelSearch->user_id = Yii::$app->user->id;

        if (Yii::$app->request->isAjax) {
            return $this->renderPartial('index', [
                'searchModel' => $modelSearch
            ]);
        }
        return $this->render('index',[
            'searchModel' => $modelSearch
        ]);
    }

}