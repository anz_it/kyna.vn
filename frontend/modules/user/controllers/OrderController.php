<?php

namespace app\modules\user\controllers;

use dektrium\user\models\LoginForm;
use yii;
use yii\helpers\Url;

use app\modules\user\models\forms\OrderForm;
use app\modules\user\models\forms\OtpForm;
use kyna\order\models\traits\OrderTrait;


class OrderController extends \app\modules\user\components\Controller
{
    use OrderTrait;
    public $_form;

    public $_model;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['active-cod', 'captcha', 'get-otp-code', 'reset-pass', 'login'],
                        'roles' => ['?'],
                    ],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'captcha' => [
                'class' => 'app\components\TextCaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null
            ],
        ];
    }

    /**
     * @return string
     */
    public function actionActiveCod()
    {
        if (Yii::$app->user->isGuest) {
            $view = 'active_otp';
            $activeForm = new OtpForm();
            $activeForm->setScenario(OtpForm::SCENARIO_ACTIVE);
            $formName = 'OtpForm';
            Yii::$app->session->remove('otp_captcha');
        } else {
            $view = 'active_cod';
            $activeForm = new OrderForm();
            $activeForm->scenario = OrderForm::SCENARIO_ACTIVE_COD;
            $formName = 'OrderForm';
        }

        if (Yii::$app->request->isPost) {
            $data = Yii::$app->request->post($formName);
            if (!empty($data)) {
                if ($activeForm->load(Yii::$app->request->post()) && $activeForm->validate()) {
                    if ($activeForm->complete()) {
                        // login and redirect to khoa hoc cua toi
                        $user = $activeForm->order->user;
                        Yii::$app->getUser()->login($user);
                        return $this->redirect(['/user/course/index']);
                    } else {
                        $message = 'Mã thẻ kích hoạt không chính xác';
                        $activeForm->addError('activation_code', $message);
                    }

                    return $this->renderAjax($view, ['message' => $message, 'order_form' => $activeForm]);
                }
            }
        }

        if (Yii::$app->request->isAjax) {
            return $this->renderAjax($view, [
                'order_form' => $activeForm
            ]);
        }

        return $this->render($view, [
            'order_form' => $activeForm
        ]);
    }

    public function actionGetOtpCode()
    {
        $activeForm = new OtpForm();
        $view = 'active_otp';
        $formName = 'OtpForm';
        if (Yii::$app->request->isAjax) {
            $data = Yii::$app->request->post($formName);
            if (!empty($data)) {
                $otpMsg = false;
                if ($activeForm->load(Yii::$app->request->post()) && $activeForm->validateInfo()) {
                    // check phone limit
                    if ($activeForm->checkPhoneLimit()) {
                        /** @var LoginForm $model */
                        $model = Yii::createObject(LoginForm::className());
                        $model->login = $activeForm->order->user->email;
                        Yii::$app->session->set('activation_code', $activeForm->activation_code);
                        return $this->renderAjax('login', [
                            'activation_code' => $activeForm->activation_code,
                            'model' => $model
                        ]);
                    }
                    if ($activeForm->sendOtp()) {
                        $otpMsg = true;
                    } else {
                        $otpMsg = false;
                    }
                }
                return $this->renderAjax($view, [
                    'order_form' => $activeForm,
                    'otp_msg' => $otpMsg
                ]);
            }
        }
        return $this->renderAjax($view, [
            'order_form' => $activeForm
        ]);
    }

    public function actionLogin()
    {
        /** @var LoginForm $model */
        $model = Yii::createObject(LoginForm::className());
        if (Yii::$app->request->isAjax) {
            if ($model->load(Yii::$app->request->post())) {
                if ($model->login()) {
                    $user = Yii::$app->user->identity;
                    $redirectUrl = Url::toRoute(['/user/course/index']);
                    $this->redirect($redirectUrl);
                }
            }
        }
        return $this->renderAjax('login', [
            'model' => $model,
        ]);
    }
}