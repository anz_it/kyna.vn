<?php

namespace app\modules\user\controllers;

use kyna\user\models\UserCourse;
use Yii;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;

use dektrium\user\controllers\RegistrationController as BaseRegistrationController;

use kyna\user\models\User;
use kyna\user\models\Profile;
use kyna\settings\models\Setting;
use kyna\user\models\Token;

use app\models\Category;
use app\modules\user\models\forms\RegistrationForm;
use yii\web\Response;
use yii\widgets\ActiveForm;
class RegistrationController extends BaseRegistrationController
{

    public $rootCats = [];
    public $settings = [];
    public $bodyClass = '';

    public function init()
    {
        parent::init();
        $this->rootCats = Category::getList(0, ['id', 'name', 'slug', 'home_icon', 'menu_icon']);
        $this->settings = ArrayHelper::map(Setting::find()->all(), 'key', 'value');

        $this->on(self::EVENT_AFTER_REGISTER, [$this, 'afterRegister']);
        $this->on(self::EVENT_AFTER_CONNECT, [$this, 'afterRegister']);
    }

    public function actionConnect($code)
    {
        $account = $this->finder->findAccount()->byCode($code)->one();

        if ($account === null || $account->getIsConnected()) {
            throw new NotFoundHttpException();
        }

        /** @var User $user */
        $model = \Yii::createObject([
            'class'    => RegistrationForm::className(),
            'email'    => $account->email,
            'name'    => $account->decodedData['name'],
        ]);

        $this->performAjaxValidation($model);

        if ($model->load(\Yii::$app->request->post()) && $user = $model->register()) {
            $event = $this->getFormEvent($model);

            $this->trigger(self::EVENT_AFTER_REGISTER, $event);

            $account->connect($user);
            \Yii::$app->user->login($user, $this->module->rememberFor);
            return $this->goBack();
        }

        return $this->render('connect', [
            'model'   => $model,
            'account' => $account,
        ]);
    }

    public function actionRegister()
    {
        //var_dump(PhoneNumberHelper::getCarrier('0977654321'));die;
        if (!$this->module->enableRegistration) {
            throw new NotFoundHttpException();
        }
        $renderFunction = Yii::$app->request->isAjax ? 'renderAjax' : 'render';

        /** @var RegistrationForm $model */
        $model = Yii::createObject(RegistrationForm::className());

        $fbId = Yii::$app->request->get('id');
        if (!empty($fbId)) {
            $model->fbId = $fbId;
            $model->name = Yii::$app->request->get('name');
            $model->email = Yii::$app->request->get('email');
        }
        $event = $this->getFormEvent($model);

        if(!empty($model) && !empty($fbId)){

            $this->trigger(self::EVENT_BEFORE_REGISTER, $event);
        }
        if(Yii::$app->request->isPost){
            $data = Yii::$app->request->post();
            if ($model->load($data)) {
                $sendEmail = true;
                if(isset($data['not_send_email']))
                    $sendEmail = false;

                if($model->register($sendEmail)){
                    $this->trigger(self::EVENT_AFTER_REGISTER, $event);

                    $defaultUrl = Yii::$app->request->post('currentUrl', ['/user/course/index']);
                    $redirectUrl = Yii::$app->user->getReturnUrl($defaultUrl);
                    if (Yii::$app->request->isAjax) {
                        Yii::$app->response->format = Response::FORMAT_JSON;
                        return [
                            'result' => true,
                            'redirectUrl' => $redirectUrl,
                        ];
                    } else {
                        $this->redirect($redirectUrl);
                    }
                }else{
                    Yii::$app->response->format = Response::FORMAT_JSON;
                    return [
                        'result' => false,
                        'errors' => $model->errors
                    ];
                }
            }
        }

        return $this->$renderFunction('@app/modules/user/views/registration/register', [
            'model' => $model,
            'module' => $this->module,
        ]);
    }

    private function createToken($user)
    {
        $token = new Token();
        $token->user_id = $user->id;
        $token->code = Yii::$app->session->id;
        $token->type = Token::TYPE_LOGIN;
        $token->save(false);
    }

    public function afterRegister($event)
    {
        $form = $event->form;

        if ($user = $this->_matchingEmail($form->email)) {
            $profile = $user->profile;
            $profile->name = $form->name;
            $profile->phone_number = $form->phonenumber;

            if (!empty($form->fbId)) {
                $profile->facebook_id = $form->fbId;
            }

            $profile->save(false);
            if (Yii::$app->user->login($user)) {
//                $this->goHome();
            }
        }
        //Add default course
        if(!empty(Yii::$app->user->id)) {
            $userCourse = new UserCourse();
            $userCourse->user_id = Yii::$app->user->id;
            $userCourse->course_id = 10;
            $userCourse->is_activated = UserCourse::BOOL_YES;
            $userCourse->activation_date = time();
            $userCourse->save();
        }
//        $this->goHome();
    }

    private function _matchingFb($fbId)
    {
        return User::find()->leftJoin(Profile::tableName() . ' p', User::tableName() . '.id = p.user_id')
            ->where([
                'p.facebook_id' => $fbId,
            ])->one();
    }

    private function _matchingEmail($email)
    {
        return User::find()->where(['email' => $email])->one();
    }

}
