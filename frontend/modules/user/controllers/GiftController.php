<?php
/**
 * Created by PhpStorm.
 * User: macintosh
 * Date: 8/25/17
 * Time: 2:38 PM
 */

namespace app\modules\user\controllers;

use kyna\gamification\models\Gift;
use kyna\gamification\models\GiftContent;
use kyna\gamification\models\UserGift;
use kyna\gamification\models\UserPoint;
use kyna\gamification\models\search\UserGiftSearch;
use kyna\gamification\models\UserPointHistory;

use kyna\promotion\models\Promotion;
use kyna\promotion\models\PromotionSearch;
use Yii;
use yii\web\NotFoundHttpException;
use yii\web\Response;

use app\modules\user\models\GiftSearch;
use yii\filters\AccessControl;

class GiftController extends \app\modules\user\components\Controller
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['?'],
                        'actions' => ['exchange']
                    ],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $searchModel = new PromotionSearch();
        $searchModel->type = null;
        $searchModel->user_id = Yii::$app->user->id;

        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('index', [
                'searchModel' => $searchModel,
            ]);
        }

        return $this->render('index', [
            'searchModel' => $searchModel,
        ]);
    }

    public function actionExchange()
    {
        $this->layout = '@app/views/layouts/main';

        $searchModel = new GiftSearch();
        $searchModel->status = GiftSearch::STATUS_ACTIVE;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $userPoint = UserPoint::findOne(Yii::$app->user->id);

        return $this->render('exchange', [
            'dataProvider' => $dataProvider,
            'userPoint' => $userPoint
        ]);
    }

    public function actionConfirm($id)
    {
        $model = $this->findModel($id);

        if (Yii::$app->request->isPost) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            $userPoint = UserPoint::findOne(Yii::$app->user->id);
            if ($userPoint == null || $userPoint->k_point < $model->k_point) {
                return [
                    'result' => false,
                    'message' => 'Không đủ điểm để quy đổi'
                ];
            }

            /* @var $giftContent GiftContent */
            $giftContent = $model->giftContent;
            $promotion = $giftContent->generatePromotion();
            if (!empty($promotion)) {
                $userGift = new UserGift();
                $userGift->user_id = Yii::$app->user->id;
                $userGift->code = $promotion->code;
                $userGift->gift_id = $model->id;
                $userGift->save(false);

                $userPoint->k_point -= $model->k_point;
                $userPoint->k_point_usable -= $model->k_point;
                if ($userPoint->save()) {
                    $userPointHistory = new UserPointHistory();
                    $userPointHistory->user_id = $userPoint->user_id;
                    $userPointHistory->type = UserPointHistory::TYPE_EXCHANGE_GIFT;
                    $userPointHistory->k_point = -1 * $model->k_point;
                    $userPointHistory->description = "Đổi thành công quà: " . $model->title;
                    $userPointHistory->save();
                }

                return [
                    'result' => true,
                    'content' => $this->renderAjax('success', [
                        'gift' => $model,
                        'code' => $promotion,
                        'userGift' => $userGift
                    ])
                ];
            }
        }

        return $this->renderPartial('confirm', ['model' => $model]);
    }

    public function findModel($id)
    {
        $model = Gift::findOne(['id' => $id, 'status' => Gift::STATUS_ACTIVE, 'is_deleted' => Gift::BOOL_NO]);

        if (empty($model)) {
            throw new NotFoundHttpException();
        }

        return $model;
    }
}
