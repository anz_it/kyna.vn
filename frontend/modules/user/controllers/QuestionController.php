<?php

namespace app\modules\user\controllers;

use kyna\course\models\search\CourseLearnerQnaSearch;
use Yii;

class QuestionController extends \app\modules\user\components\Controller
{
    
    public function actionIndex()
    {
        $question = new CourseLearnerQnaSearch();

        $question->user_id = Yii::$app->user->id;

//        $question->question_id = null;

        if (Yii::$app->request->isAjax) {

            return $this->renderPartial('index', [
                'searchModel' => $question
            ]);
        }
        
        return $this->render('index', [
            'searchModel' => $question
        ]);
    }
}
