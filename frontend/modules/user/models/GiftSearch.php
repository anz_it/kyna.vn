<?php
/**
 * Created by PhpStorm.
 * User: macintosh
 * Date: 10/9/17
 * Time: 11:29 AM
 */

namespace app\modules\user\models;

use kyna\gamification\models\Gift;
use yii\data\ActiveDataProvider;

class GiftSearch extends \kyna\gamification\models\search\GiftSearch
{

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Gift::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'created_time' => SORT_DESC,
                ]
            ],
            'pagination' => [
                'pagesize' => '12'
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'status' => $this->status
        ]);

        return $dataProvider;
    }

}
