<?php

namespace app\modules\user\models;

use Yii;
use app\modules\user\components\Mailer;
use kyna\user\models\UserCourse;

class User extends \kyna\user\models\User
{
    
    protected function getMailer()
    {
        return Yii::$container->get(Mailer::className());
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        if ($insert) {
            // Nội dung khóa hướng dẫn này không còn phù hợp với v3, nên để ra, sau này có khóa mới thì cập nhật lại.
           // UserCourse::active($this->id, Yii::$app->params['beginLearningCourseId']);
        }
    }
}

