<?php

namespace app\modules\user\models\forms;

use Yii;
use yii\helpers\Html;

use dektrium\user\helpers\Password;

use kyna\user\models\User;

class ProfileForm extends \yii\base\Model
{
    
    public $name;
    public $email;
    public $gender;
    public $birthday;
    public $address;
    public $phone;
    public $location_id;
    public $is_receive_email_newsletter;
    public $is_receive_email_new_course_created;
    
    // password
    public $current_password;
    public $new_password;
    public $new_password_repeat;
    
    const EVENT_AFTER_SAVE = 'afterSave';

    public function init()
    {
        $ret = parent::init();
        
        if ($user = Yii::$app->user->identity) {
            $this->name = $user->profile->name;
            $this->email = $user->email;
            $this->phone = $user->profile->phone_number;
            $this->birthday = $user->birthday;
            $this->gender = $user->gender;
            $this->location_id = $user->location_id;
            $this->address = $user->address;
            $this->is_receive_email_newsletter = $user->is_receive_email_newsletter;
            $this->is_receive_email_new_course_created = $user->is_receive_email_new_course_created;

            if (Password::validate((string)$user->created_at, $user->password_hash)) {
                $this->current_password = $user->created_at;
            }
        }
        
        $this->on(self::EVENT_AFTER_SAVE, [$this, 'afterSave']);
        
        return $ret;
    }
    
    public function rules()
    {
        return [
            [['name', 'email', 'gender', 'phone'], 'required'],
            ['name', 'string', 'max' => 50],
            ['email', 'email'],
            ['email', 'checkUnique'],
            [
                ['new_password', 'new_password_repeat'],
                'required',
                'when' => function ($model) { 
                    return !empty($model->current_password); 
                },
                'whenClient' => "function (attribute, value) {
                    return $('input[name=\'ProfileForm[current_password]\']').val().length > 0;
                }"
            ],
            [
                ['current_password', 'new_password_repeat'],
                'required',
                'when' => function ($model) { 
                    return !empty($model->new_password); 
                },
                'whenClient' => "function (attribute, value) {
                    return $('input[name=\'ProfileForm[new_password]\']').val().length > 0;
                }"
            ],
            ['current_password', 'checkCorrectPassword'],
            ['new_password_repeat', 'compare', 'compareAttribute' => 'new_password', 'message' => 'Mật khẩu mới không giống nhau.'],
            [['birthday', 'address', 'location_id', 'is_receive_email_newsletter', 'is_receive_email_new_course_created'], 'safe']
        ];
    }

    public function beforeValidate()
    {
        $this->name = Html::encode($this->name, false);
        $this->address = Html::encode($this->address, false);
        return parent::beforeValidate();
    }
    
    /**
     * @desc check attribute is unique
     * @param type $attribute
     * @param type $params
     */
    public function checkUnique($attribute, $params)
    {
        if (User::find()->where([$attribute => $this->$attribute])->andWhere(['!=', 'id', Yii::$app->user->id])->exists()) {
            $this->addError($attribute,  $this->getAttributeLabel($attribute) . " đã được sử dụng, vui lòng nhập lại.");
        }
    }
    
    /**
     * @desc validate current user password before update new password
     * @param type $attribute
     * @param type $params
     */
    public function checkCorrectPassword($attribute, $params)
    {
        $user = Yii::$app->user->identity;
        
        if (!Yii::$app->security->validatePassword($this->$attribute, $user->password_hash)) {
            $this->addError($attribute, "Mật khẩu hiện tại không đúng.");
        }
    }

    public function attributeLabels()
    {
        return [
            'name' => 'Họ tên',
            'email' => 'Email',
            'gender' => 'Giới tính',
            'birthday' => 'Ngày sinh',
            'address' => 'Địa chỉ',
            'phone' => 'Số điện thoại',
            'is_receive_email_newsletter' => 'Nhận email từ ban đào tạo khóa học Kyna.vn',
            'is_receive_email_new_course_created' => 'Nhận email nhắc nhở có bài học mới',
            'current_password' => 'Mật khẩu',
            'new_password' => 'Mật khẩu mới',
            'new_password_repeat' => 'Nhập lại mật khẩu mới',
        ];
    }
    
    /**
     * @desc save user profile
     * @return boolean
     */
    public function save($validate = true)
    {
        if ($validate && !$this->validate()) {
            return false;
        }
        $user = Yii::$app->user->identity;
        $profile = $user->profile;
        
        // load user meta values
        $user->loadMeta($this->attributes);

        $profile->name = $this->name;
        $profile->phone_number = $this->phone;
        $user->gender = $this->gender;
        $user->is_receive_email_new_course_created = $this->is_receive_email_new_course_created;
        $user->is_receive_email_newsletter = $this->is_receive_email_newsletter;

        $result = $profile->save();
        
        if (!empty($this->new_password)) {
            $user->password_hash = Yii::$app->security->generatePasswordHash($this->new_password);
        }

        $result = $result && $user->save();
        
        // trigger afterSave event function
        $this->trigger(self::EVENT_AFTER_SAVE);
        
        return $result;
    }
    
    /**
     * @desc call after saving profile
     * @return boolean
     */
    public function afterSave()
    {
        $this->new_password = $this->current_password = $this->new_password_repeat = null;
        
        return true;
    }
    
}
