<?php

namespace app\modules\user\models\forms;

use app\modules\user\components\Mailer;
use dektrium\user\Finder;
use kyna\user\models\Token;

class RecoveryForm extends \dektrium\user\models\RecoveryForm
{

    /**
     * @param Mailer $mailer
     * @param Finder $finder
     * @param array  $config
     */
    public function __construct(Mailer $mailer, Finder $finder, $config = [])
    {
        $this->mailer = $mailer;
        $this->mailer->setRecoverySubject('[Kyna.vn] Yêu cầu phục hồi mật khẩu.');
        
        $this->finder = $finder;
        parent::__construct($mailer, $finder, $config);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            'emailExist' => ['email', 'checkExist'],
            'emailSpam' => ['email', 'checkSpam'],
        ]);
    }
    
    public function checkExist($attribute)
    {
        if (!$this->hasErrors($attribute)) {
            $user = $this->finder->findUserByEmail($this->email);
            if (is_null($user)) {
                $this->addError($attribute, 'Email không tồn tại trong hệ thống');
            }
        }
    }
    
    public function checkSpam($attribute)
    {
        if (!$this->hasErrors($attribute)) {
            $user = $this->finder->findUserByEmail($this->email);
            if (!is_null($user)) {
                $isExistToken = Token::find()->where(['user_id' => $user->id])->andWhere(['>=', 'created_at', strtotime('-5 minutes')])->exists();
                if ($isExistToken) {
                    $this->addError('email', 'Bạn vừa thực hiện request, vui lòng kiểm tra email trước khi gửi yêu cầu mới.');
                }
            }
        }
    }

}