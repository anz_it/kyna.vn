<?php

namespace app\modules\user\models\forms;

use Yii;
use app\modules\user\models\User;
use dektrium\user\models\RegistrationForm as BaseRegistrationForm;
use common\validators\PhoneNumberValidator;
use yii\helpers\Html;

class RegistrationForm extends BaseRegistrationForm
{

    public $name;
    public $fbId;
    public $phonenumber;

    public function rules()
    {
        $user = $this->module->modelMap['User'];

        return array_merge(parent::rules(), [
            'nameRequired' => ['name', 'required'],
            'nameLength' => ['name', 'string', 'min' => 3, 'max' => 50],
            'fbId' => ['fbId', 'safe'],
            'emailUnique'   => [
                'email',
                'unique',
                'targetClass' => $user,
                'message' => 'Email đã được sử dụng'
            ],
            'phonenumberRequired' => ['phonenumber', 'required'],
            'phonenumberLength' => ['phonenumber', 'string', 'min' => 9, 'max' => 10],
            'phonenumber' => ['phonenumber', 'integer'],
        ]);
    }

    public function beforeValidate()
    {
        $this->username = $this->email;
        $this->name = Html::encode($this->name, false);
        return parent::beforeValidate();
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'email'    => 'Email',
            'username' => 'Username',
            'password' => 'Mật khẩu',
            'phonenumber' => 'Điện thoại'
        ];
    }

    public function register($sendMail = true)
    {
        if (!$this->validate()) {
            return false;
        }

        /** @var User $user */
        $user = Yii::createObject(User::className());
        $user->setScenario('register');
        $this->loadAttributes($user);

        if (!$user->register($sendMail)) {
            return false;
        }

        Yii::$app->session->setFlash(
            'info',
            Yii::t(
                'user',
                'Your account has been created and a message with further instructions has been sent to your email'
            )
        );

        return $user;
    }

}
