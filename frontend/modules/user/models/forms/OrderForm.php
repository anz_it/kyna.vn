<?php

namespace app\modules\user\models\forms;

use Yii;
use kyna\order\models\Order;

class OrderForm extends \yii\base\Model
{

    protected $_order;

    const SCENARIO_ACTIVE_COD = 'active_cod';

    public $activation_code;

    public function scenarios()
    {
        return [
            self::SCENARIO_ACTIVE_COD => ['activation_code']
        ];
    }

    public function rules()
    {
        return [
            [['activation_code'], 'string', 'on' => [self::SCENARIO_ACTIVE_COD]],
            [['activation_code'], 'required', 'on' => [self::SCENARIO_ACTIVE_COD]],
            [['activation_code'], 'checkExist', 'on' => [self::SCENARIO_ACTIVE_COD]]
        ];
    }

    public function attributeLabels()
    {
        return [
            'activation_code' => 'Mã kích hoạt'
        ];
    }

    public function checkExist()
    {
        // check if activation code is activated
        $order = Order::find()
            ->where([
                'activation_code' => $this->activation_code,
                'user_id' => Yii::$app->user->id,
                'is_activated' => Order::BOOL_YES,
            ])->andWhere(['or', ['status' => Order::ORDER_STATUS_COMPLETE], ['status' => Order::ORDER_STATUS_DELIVERING]])
            ->one();
        if ($order) {
            $this->addError('activation_code', 'Mã kích hoạt đã được sử dụng.');
            return false;
        }

        // check if activation code can use
        $result = $this->getOrder();
        if ($result == null) {
            $this->addError('activation_code', 'Mã kích hoạt không chính xác.');
            return false;
        }

        return true;
    }

    public function complete()
    {
        $ret = false;
        if (empty($this->_order)) {
            $this->_order = $this->getOrder();
        }
        if ($this->_order != null) {
            // active courses
            if (Order::activate($this->_order->id) !== false) {
                $ret = true;
            }
        }
        return $ret;
    }

    public function getOrder()
    {
        if (empty($this->_order)) {
            $this->_order = Order::find()->where([
                'activation_code' => $this->activation_code,
                'user_id' => Yii::$app->user->id,
                'is_activated' => Order::BOOL_NO,

            ])->andWhere(['or', ['status' => Order::ORDER_STATUS_COMPLETE], ['status' => Order::ORDER_STATUS_DELIVERING]])
                ->one();
        }
        return $this->_order;
    }

}