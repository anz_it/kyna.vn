<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

use common\widgets\Alert;
use common\widgets\upload\Upload;
use common\widgets\locationfield\LocationField;

$this->title = 'Kyna.vn - Chỉnh sửa thông tin cá nhân';
if (empty($profile->gender)) {
    $profile->gender = 0;
}
?>

<div class="k-profile-edit-content">
    <section <?php
    if ($isMobile == false) {
        echo 'id="k-courses-header"';
    }
    ?> class="k-height-header">
        <header>
            <h2>Chỉnh sửa thông tin cá nhân</h2>
            <div class="user-avatar">
                <?=
                Upload::widget([
                    'model' => Yii::$app->user->identity,
                    'attribute' => 'avatar',
                    'autoUploadUrl' => '/user/profile/upload-avatar',
                    'lastUploadUrl' => '/user/profile/last-uploaded',
                    'display' => 'image',
                    //'name' => 'user-avatar', // match $this->uploadParamName
                    'viewPath' => '@app/modules/user/views/profile/_avatar',
                ])
                ?>
            </div>
        </header>
    </section>
    <section class="k-courses-header-list">
        <?php
        $form = ActiveForm::begin([
            'id' => 'profile-form',
            'action' => Url::toRoute(['/user/profile/edit', 'returnUrl' => Yii::$app->request->get('returnUrl')]),
        ])
        ?>
        <div class="wrap-top clearfix">
            <?php
            $need_change_pass = Yii::$app->session->getFlash('need-to-change-pass');
            if (empty($need_change_pass)) {
                $flash_message = Yii::$app->session->getFlash('warning');
                if ($flash_message == 'Bạn cần phải đổi mật khẩu cho lần đăng nhập đầu tiên.') {
                    $need_change_pass = $flash_message;
                }
            }
            ?>
            <?php
            if (empty($need_change_pass)):
                ?>

                <?= Alert::widget() ?>
            <?php
            endif;
            ?>
            <?= $form->errorSummary($profile, ['class' => 'error-summary alert alert-warning']) ?>

            <?=
            $form->field(
                $profile, 'name', [
                    'options' => [
                        'placeholder' => 'Họ Tên'
                    ],
                    'template' => '
                    <div class="input col-md-6 col-sm-12 col-xs-12">
                        <div class="col-md-4 col-xs-12 left">
                            <label class="control-label">{label}</label>
                        </div>
                        <div class="col-md-8 col-xs-12 right">
                            <span class="icon"><i class="icon-user" aria-hidden="true"></i></span>{input}{hint}
                        </div>
                    </div>
                '
                ]
            )->textInput();
            ?>

            <?=
            $form->field(
                $profile, 'email', [
                    'options' => [
                        'placeholder' => 'Email',
                    ],
                    'template' => '
                    <div class="input col-md-6 col-sm-12 col-xs-12">
                        <div class="col-md-4 col-xs-12 left">
                            <label class="control-label">{label}</label>
                        </div>
                        <div class="col-md-8 col-xs-12 right">
                            <span class="icon"><i class="icon-mail" aria-hidden="true"></i></span>{input}{hint}
                        </div>
                    </div>
                    '
                ]
            )->textInput([
                'readonly' => true,
                'disabled' => 'disabled'
            ]);
            ?>

            <?=
            $form->field(
                $profile, 'gender', [
                    'options' => ['class' => 'k-listing-characteristics-list'],
                    'template' => '
                            <div class="input radio col-md-6 col-sm-12 col-xs-12">
                                <div class="col-md-4 col-xs-12 left">{label}</div>
                                <div class="col-md-8 col-xs-12 right">
                                        {input}
                                        {hint}
                                </div>
                            </div>
                        ',
                    'enableClientValidation' => false
                ]
            )->radioList([
                '0' => 'Nữ',
                '1' => 'Nam'
            ], [
                'itemOptions' => [
                    'class' => 'gender-input',
                    'labelOptions' => ['class' => 'gender-label']
                ]
            ]);
            ?>

            <?=
            $form->field($profile, 'birthday', [
                'options' => [
                    'id' => 'profileform-birthday'
                ],
                'template' => '
                    <div class="input col-md-6 col-sm-12 col-xs-12">
                        <div class="col-md-4 col-xs-12 left">
                            <label class="control-label">{label}</label>
                        </div>
                        <div class="col-md-8 col-xs-12 right">
                            <span class="icon"><i class="icon-birthday" aria-hidden="true"></i></span>{input}{hint}
                        </div>
                    </div>
                '
            ])->widget(\kartik\date\DatePicker::classname(), [
                'layout' => '<span class="input-group-addon kv-date-calendar" title="Chọn ngày sinh"><i class="glyphicon glyphicon-calendar"></i></span>{input}  {picker}',
                'options' => ['placeholder' => 'Ngày-Tháng-Năm'],
                'type' => \kartik\date\DatePicker::TYPE_COMPONENT_APPEND,
                'removeButton' => [
                    'icon' => 'trash',
                ],
                'pluginOptions' => [
                    'todayHighlight' => true,
                    'format' => 'dd-mm-yyyy',
                    'todayHighlight' => true,
                    'autoclose' => true,
                    'endDate' => '-10y'
                ]
            ]) ?>

            <?=
            $form->field(
                $profile, 'address', [
                    'options' => ['id' => 'profileform-address'],
                    'template' => '
                    <div class="input col-md-6 col-sm-12 col-xs-12">
                        <div class="col-md-4 col-xs-12 left">
                            <label class="control-label">{label}</label>
                        </div>
                        <div class="col-md-8 col-xs-12 right">
                            <span class="icon"><i class="icon-location" aria-hidden="true"></i></span>{input}{hint}
                        </div>
                    </div>
                    '
                ]
            )->textInput()
            ?>

            <?=
            $form->field(
                $profile, 'phone', [
                    'options' => ['id' => 'profileform-phone'],
                    'template' => '
                    <div class="input col-md-6 col-sm-12 col-xs-12">
                        <div class="col-md-4 col-xs-12 left">
                            <label class="control-label">{label}</label>
                        </div>
                        <div class="col-md-8 col-xs-12 right">
                            <span class="icon"><i class="icon-call" aria-hidden="true"></i></span>{input}{hint}
                        </div>
                    </div>
                    '
                ]
            )->textInput()
            ?>

            <?=
            $form->field($profile, 'location_id')->widget(LocationField::className(), [
                'showFieldLabels' => true,
                'fieldGroupClass' => 'input col-sm-6 col-xs-12 field-profileform-address',
                'fieldLabelClass' => 'col-sm-4 col-xs-12 left pd0',
                'fieldInputClass' => 'col-sm-8 col-xs-12 right',
                'wrapper' => false
            ])->label(false)
            ?>

            <div class="clearfix"></div>
            <?=
            $form->field(
                $profile, 'is_receive_email_newsletter', [
                    'options' => ['class' => 'input col-xs-12'],
                    'template' => '
                        {input}{label}
                    '
                ]
            )->checkbox(['id' => 'checkbox1'])
            ?>

            <?=
            $form->field(
                $profile, 'is_receive_email_new_course_created', [
                    'options' => ['class' => 'input col-xs-12'],
                    'template' => '{input}{label}'
                ]
            )->checkbox(['id' => 'checkbox2'])
            ?>
            <div class="line"><span></span></div>

        </div>

        <div class="wrap-center clearfix">
            <?php
             if (!empty($need_change_pass)):
            ?>
                <?= Alert::widget() ?>
            <?php
            endif;
            ?>
            <?=
            $form->field(
                $profile, 'current_password', [
                    'options' => [
                        'id' => 'profileform-current_password',
                        'class' => 'clearfix',
                    ],
                    'template' => '
                    <div class="input col-md-6 col-sm-12 col-xs-12">
                        <div class="col-md-4 col-xs-12 left pd0">
                            <label class="control-label">{label}</label>
                        </div>
                        <div class="col-md-8 col-xs-12 right">
                            <i class="fa fa-eye-slash" aria-hidden="true"></i>
                            <span class="icon">
                                <i class="icon-lock" aria-hidden="true"></i>
                            </span>{input}{hint}
                        </div>
                    </div>
                    <div class="input col-md-6 col-sm-12 col-xs-12">
                        <a href="javascript:void(0);" id="click-change-pass">Đổi mật khẩu</a>
                    </div>
                    '
                ]
            )->passwordInput([
                'placeholder' => 'Mật Khẩu',
                'value' => $profile->current_password,
                'disabled' => (!empty($profile->current_password) || Yii::$app->session->hasFlash('need-to-change-pass')) ? 'disabled' : false
            ])
            ?>
            <?php if (!empty($profile->current_password) || Yii::$app->session->hasFlash('need-to-change-pass')): ?>
                <?= $form->field($profile, 'current_password')->hiddenInput()->label(false) ?>
            <?php endif; ?>

            <div class="wrap-change-pass">
                <?=
                $form->field(
                    $profile, 'new_password', [
                        'template' => '
                        <div class="input col-md-6 col-sm-12 col-xs-12">
                            <div class="col-md-4 col-xs-12 left pd0">
                                <label class="control-label">{label}</label>
                            </div>
                            <div class="col-md-8 col-xs-12 right">
                                <i class="fa fa-eye-slash" aria-hidden="true"></i>
                                <span class="icon">
                                    <i class="icon-lock" aria-hidden="true"></i>
                                </span>{input}{hint}
                            </div>
                        </div>
                        '
                    ]
                )->passwordInput(['placeholder' => 'Mật Khẩu Mới'])
                ?>

                <?=
                $form->field(
                    $profile, 'new_password_repeat', [
                        'template' => '
                        <div class="input col-md-6 col-sm-12 col-xs-12">
                            <div class="col-md-4 col-xs-12 left pd0">
                                <label class="control-label">{label}</label>
                            </div>
                            <div class="col-md-8 col-xs-12 right">
                                <i class="fa fa-eye-slash" aria-hidden="true"></i>
                                <span class="icon">
                                    <i class="icon-lock" aria-hidden="true"></i>
                                </span>{input}{hint}
                            </div>
                        </div>
                        '
                    ]
                )->passwordInput(['placeholder' => 'Nhập Lại Mật Khẩu'])
                ?>

            </div>

            <div class="clearfix"></div>
            <div class="wrap-footer clearfix">
                <div class="input col-xs-12 right">
                    <?= Html::submitButton('Lưu', ['class' => 'button']) ?>
                    <a href="/trang-ca-nhan/khoa-hoc" id="btn-reset-edit-profile" class="button-close">Hủy </a>
                </div>
            </div>
            <script type="text/javascript">
                $('#profileform-gender input[type=radio]:checked').parent(".gender-label").addClass("selected");
                $('.field-profileform-address select').css('text-indent', '20px');
                $('input[type=checkbox]:checked').parent("label").addClass("selected");
                $('input[type=checkbox]:checked').parent("label").find("input").attr("value", 1);
                $("#btn-reset-edit-profile").click(function () {
                    CoursesAction.ResetProfile();
                });
                $("#click-change-pass").click(function () {
                    $(".wrap-change-pass").addClass("open");
                    $(".k-courses-header-list #profile-form .wrap-center .input .right i.fa").css("display", "block");
                    $(this).hide();
                });
                <?php if (!empty($profile->current_password) || Yii::$app->session->hasFlash('need-to-change-pass')) :
                $scrollToPass = Yii::$app->session->removeFlash('need-to-change-pass');
                ?>
                $(document).ready(function () {
                    var headerHeight = $('#header').height();
                    <?php if (!empty($scrollToPass)): ?>
                    // scroll to password section
                    $('html, body').animate({

                        scrollTop: $("#profileform-current_password").offset().top - headerHeight - 70
                    }, 1000);
                    <?php endif; ?>
                });
                <?php else: ?>
                $("#profile-form input#profileform-current_password").prop('disabled', true);
                $("body").on('click', '#click-change-pass', function (e) {
                    e.preventDefault();
                    $("#profile-form input#profileform-current_password").prop('disabled', false);
                    if ($("#profile-form input[type='password']").is(':disabled'))
                        $("#profile-form input[type='password']").attr('disabled', false);
                    else
                        $("#profile-form input[type='password']").attr('disabled', true);
                });
                <?php endif; ?>

                $(".k-courses-header-list #profile-form .wrap-center .input .right i.fa").css("display", "none");
                $(".k-courses-header-list #profile-form .wrap-center .input .right .form-control").attr("type", "text");

                // Change type input
                $(".k-courses-header-list #profile-form .wrap-center .input .right i.fa").click(function () {
                    $(this).parent().find("input.form-control").toggleClass("isHidePassword");
                    if ($(this).parent().find("input.form-control").hasClass("isHidePassword")) {
                        $(this).parent().find("input.form-control").attr("type", "password");
                        $(this).removeClass("fa-eye-slash");
                        $(this).addClass("fa-eye");
                    } else {
                        $(this).parent().find("input.form-control").attr("type", "text");
                        $(this).removeClass("fa-eye");
                        $(this).addClass("fa-eye-slash");
                    }
                });

            </script>
        </div>
        <script>
            $(document).ajaxComplete(function (event, xhr, settings) {
                if ((settings.url.indexOf("/trang-ca-nhan/thong-tin") > -1) && (settings.type.toLocaleLowerCase().indexOf("post") > -1) && (xhr.status == 200)) {
                    $('.k-profile-edit-content .alert.alert-success').css('marginTop', '20px');
                    $("html, body").animate({scrollTop: 0}, "slow");
                    <?php
                        if(!empty($need_change_pass)):
                    ?>
                    window.location.href = "/trang-ca-nhan/khoa-hoc";

                    <?php
                        endif;
                    ?>
                }
            });
        </script>
</div>

<?php ActiveForm::end() ?>
</section>
</div>
<?php
$css = "
        .k-courses-header-list #profile-form .wrap-top .alert-warning ul li {
            width: 100%;
            float: none;
        }
        .field-profileform-birthday .input-group {
            display: block;
        }
        .datepicker-dropdown {
            box-shadow:  0 2px 8px rgba(0,0,0,0.3);
            padding: 5px;
        } 
        .kv-date-calendar {
            position: absolute;
            top: 0;
            left: 5px;
            height: 40px;
            display: block;
        }
        .k-courses-header-list #profile-form .wrap-top .input .right span.icon {
            z-index: 99;
        }
    ";

$this->registerCss($css);
?>
