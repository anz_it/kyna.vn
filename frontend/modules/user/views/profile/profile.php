<?php
use common\helpers\CDNHelper;
use yii\helpers\Url;
use yii\helpers\Html;

$user = Yii::$app->user->identity;
?>
<div class="tab-content clearfix col-xs-12" id="myaccount-main">
    <div class="avatar-acount">
        <?php $avUrl = CDNHelper::image($user->avatarImage, [
            'size' => CDNHelper::IMG_SIZE_AVATAR_SMALL,
            'resizeMode' => 'crop',
            'returnMode' => 'url'
        ]);
        if (!$avUrl) {
            $avUrl = '/img/default_avatar.png';
        }

        ?>
        <img src="<?=$avUrl?>" class="img-responsive" />
        <div class="name"><?= $user->profile->name; ?></div>
    </div>
    <div class="info-account">
        <ul class="clearfix">
            <li>
                <div class="img-courses-small img-count-courses-small"></div>
                <span class="result course-number">200</span>
                <span class="lab">Khóa học</span>
            </li>
<!--            <li>-->
<!--                <div class="img-courses-small img-percomplete-courses-small"></div>-->
<!--                <span class="lab">Tỉ lệ hoàn thành</span>-->
<!--                <span class="result">88%</span>-->
<!--            </li>-->
            <li>
                <div class="img-courses-small img-document-courses-small"></div>
                <span class="result document-number">10</span>
                <span class="lab">Tài liệu</span>
            </li>
            <li>
                <div class="img-courses-small img-question-courses-small"></div>
                <span class="result question-number">10</span>
                <span class="lab">Câu hỏi</span>
            </li>
<!--            <li>-->
<!--                <div class="img-courses-small img-kpoint-courses-small"></div>-->
<!--                <span class="result">10</span>-->
<!--                <span class="lab">Kpoint</span>-->
<!--            </li>-->
        </ul>
    </div>
    <div class="action-account">
        <ul class="clearfix">
            <a href="<?= Url::toRoute(['/user/profile/edit']); ?>">
                <li id="k-courses-header-btn-edit" data-target="#slide-out">
                    <i class="icon-edit"></i>
                    <span class="lab">Chỉnh sửa thông tin</span>
                </li>
            </a>
            <a id="k-courses-header-btn-active" href="<?= Url::toRoute(['/user/order/active-cod']) ?>" data-toggle="popup" data-target="#activeCOD" data-ajax data-push-state=false>
                <li>
                    <i class="icon-truck"></i>
                    <span class="lab">Kích hoạt mã COD</span>
                </li>
            </a>
<!--            <a href="messenger_courses.php">-->
<!--                <li>-->
<!--                    <i class="icon-mail"></i>-->
<!--                    <span class="lab">Tin nhắn</span>-->
<!--                </li>-->
<!--            </a>-->
            <a href="<?= Url::toRoute(['/user/transaction/in-complete']) ?>">
                <li>
                    <i class="icon-tags"></i>
                    <span class="lab">Giao dịch</span>
                </li>
            </a>
            <a href="#" data-toggle="modal" data-target="#popup-thong-bao-deltaxu">
                <li>
                    <i>
                         <svg width="100%" height="100%" viewBox="0 0 512 512" style="width: 20px;">
	
                             <path d="m501 384c-29 0-53-24-53-53l0-96c0-78-48-149-120-178c-8-33-37-57-72-57c-34 0-65 24-73 57c-71 29-119 100-119 178l0 96c0 29-24 53-53 53c-6 0-11 5-11 11c0 6 5 10 11 10l490 0c6 0 11-4 11-10c0-6-5-11-11-11z m-181 43l-128 0c-6 0-11 4-11 10c0 41 34 75 75 75c41 0 75-34 75-75c0-6-5-10-11-10z" style="fill: #757373;">
                             </path>
                         </svg>
                     </i>
                    <span class="lab">Thông báo về DeltaXu</span>
                </li>
            </a>
            <?= Html::beginForm(['/user/security/logout'], 'post') ?>
            <a href="javascript:" onclick="$(this).closest('form').submit()">
                <li>
                    <i class="icon-sign-out"></i>
                    <span class="lab">Đăng xuất</span>
                </li>
            </a>
            <?= Html::endForm(); ?>
        </ul>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $.ajax({
            url: '<?php echo Url::toRoute(["course/analytic"]) ?>',
            method: 'GET',
            type: 'json',
            success: function (resp) {
                var res = JSON.parse(resp);

                $('.course-number').empty();

                $('.course-number').append(res.object.course_count);

                $('.document-number').empty();

                $('.document-number').append(res.object.document_count);

                $('.question-number').empty();

                $('.question-number').append(res.object.question_count);
            },
            error: function (err) {
                console.log(err);
            }
        });
    });
</script>
