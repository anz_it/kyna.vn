<?php
use common\helpers\CDNHelper;

//  private static $_options = [
//     'size' => false,
//     'resizeMode' => false,
//     'fixedRatio' => false,
//     'returnMode' => 'img',
//
//     'title'      => false,
//     'alt'        => false,
//     'class'      => false,
// ];

$imgTag = $img ? CDNHelper::image($img, [
    'resizeMode' => 'crop',
    'size' => CDNHelper::IMG_SIZE_AVATAR_LARGE,
    'alt' => 'Hình đại diện',
    'data-img-large' => $img,
    'class' => 'file-upload-img',
]) : false;

?>
<label class="file-upload<?= !$imgTag ? ' avatar-empty' : '' ?>" for="<?= $id ?>" id="<?= $id ?>-wrapper"
    data-file-upload
    <?= $uploadUrl ? 'data-upload-url="'.$uploadUrl.'"' : ''?>
    <?= $lastUploadUrl ? 'data-last-upload-url="'.$lastUploadUrl.'"' : '' ?>>

    <?= $imgTag ? $imgTag : '<i class="icon-add"></i>' ?>
    <?= $input ?>
</label>

<script>
    $(".file-upload-input").change(function (){
        var fileExtension = ['jpg', 'png'];
        if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
            alert("Chỉ chấp nhận file hình có đuôi : "+fileExtension.join(', '));
            return false;
        }
    });
</script>
<style>

.user-avatar {
    margin: 20px auto;
    display: table;
}
.user-avatar .file-upload {
    display: table-cell;
    width: 122px;
    height: 122px;
    border: 1px solid transparent;
    border-radius: 100%;
    vertical-align: middle;
    position: relative;
    text-align: center;
    overflow: hidden;
}
.user-avatar .file-upload.avatar-empty {
    border-color: #ccc;
}
.user-avatar .file-upload:hover::after {
    /*font-family: "fontastic-icon" !important;*/
    content: "Đổi hình";
    position: absolute;
    top: 0;
    left: 0;
    width: 120px;
    height: 120px;
    line-height: 120px;
    color: #fff;
    border-radius: 50%;
    background-color: rgba(0,0,0,.5);
}
#k-courses-header header img {
    width: 120px;
    height: 120px;
    object-fit: fill;
/* border-radius: 50%; */
    margin: 0;
}

</style>
