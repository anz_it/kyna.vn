<div role="tabpanel" class="tab-pane clearfix tab-parent-content " id="messages"> 

    <ul class="nav listing" role="tablist">
        <li class="active">
            <div class="col-md-4 col-sm-5 col-xs-12 left">
                <div class="wrap-image">
                    <span class="teacher"><img src="/img/my-courses/author.jpg" alt=""/></span>
                    <span class="icon"><img src="/img/my-courses/icon-teacher.png" alt=""/></span>
                </div><!--end .wrap-image-->                               
                <h4 class="name">Nguyễn Văn A</h4>
                <p class="date">22/4/2016</p>
            </div><!--end .left-->
            <div class="col-md-8 col-sm-7 col-xs-12 right">
                <h4><span class="new"></span> <a href="#messages-list-1" aria-controls="messages-list-1" role="tab" data-toggle="tab" class="messages-link">Chào bạn mình muốn hỏi</a></h4>
                <p>Thầy cho em hỏi CPA chỉ phù hợp với website theo dạng bán hàng</p>                
            </div><!--end .right-->                                        
        </li>
        
        <li>
            <div class="col-md-4 col-sm-5 col-xs-12 left">
                <div class="wrap-image">
                    <span class="teacher"><img src="/img/my-courses/author.jpg" alt=""/></span>
                    <span class="icon"><img src="/img/my-courses/icon-teacher.png" alt=""/></span>
                </div><!--end .wrap-image-->                               
                <h4 class="name">Nguyễn Văn A</h4>
                <p class="date">22/4/2016</p>
            </div><!--end .left-->
            <div class="col-md-8 col-sm-7 col-xs-12 right">
                <h4><span class="new"></span> <a href="#messages-list-2" aria-controls="messages-list-2" role="tab" data-toggle="tab" class="messages-link">Chào bạn mình muốn hỏi</a></h4>
                <p>Thầy cho em hỏi CPA chỉ phù hợp với website theo dạng bán hàng</p>                
            </div><!--end .right-->                                       
        </li>
                        
    </ul>
  
    <div class="wrapper clearfix">  
        <div class="col-lg-4 col-md-5 col-xs-3 left pd0">            
            <ul class="nav last" role="tablist">
                <li class="active">                
                    <div class="wrap-image">
                        <span class="teacher"><img src="/img/my-courses/author.jpg" alt=""/></span>
                        <span class="icon"><img src="/img/my-courses/icon-teacher.png" alt=""/></span>
                    </div><!--end .wrap-image-->                               
                    <h4 class="name">Nguyễn Văn A</h4>
                    <p class="date">22/4/2016 <span class="new"></span></p>
                    <a href="#messages-list-1" aria-controls="messages-list-1" role="tab" data-toggle="tab" class="messages-link"></a>
                </li>
                
                <li>                
                    <div class="wrap-image">
                        <span class="teacher"><img src="/img/my-courses/author.jpg" alt=""/></span>
                        <span class="icon"><img src="/img/my-courses/icon-teacher.png" alt=""/></span>
                    </div><!--end .wrap-image-->                               
                    <h4 class="name">Nguyễn Văn A</h4>
                    <p class="date">22/4/2016 <span class="new"></span></p>
                    <a href="#messages-list-2" aria-controls="messages-list-2" role="tab" data-toggle="tab" class="messages-link"></a>
                </li>
                
                <li>                
                    <div class="wrap-image">
                        <span class="teacher"><img src="/img/my-courses/author.jpg" alt=""/></span>
                        <span class="icon"><img src="/img/my-courses/icon-teacher.png" alt=""/></span>
                    </div><!--end .wrap-image-->                               
                    <h4 class="name">Nguyễn Văn A</h4>
                    <p class="date">22/4/2016 <span class="new"></span></p>
                    <a href="#messages-list-1" aria-controls="messages-list-1" role="tab" data-toggle="tab" class="messages-link"></a>
                </li>
                
                <li>                
                    <div class="wrap-image">
                        <span class="teacher"><img src="/img/my-courses/author.jpg" alt=""/></span>
                        <span class="icon"><img src="/img/my-courses/icon-teacher.png" alt=""/></span>
                    </div><!--end .wrap-image-->                               
                    <h4 class="name">Nguyễn Văn A</h4>
                    <p class="date">22/4/2016 <span class="new"></span></p>
                    <a href="#messages-list-2" aria-controls="messages-list-2" role="tab" data-toggle="tab" class="messages-link"></a>
                </li> 
                
                <li>                
                    <div class="wrap-image">
                        <span class="teacher"><img src="/img/my-courses/author.jpg" alt=""/></span>
                        <span class="icon"><img src="/img/my-courses/icon-teacher.png" alt=""/></span>
                    </div><!--end .wrap-image-->                               
                    <h4 class="name">Nguyễn Văn A</h4>
                    <p class="date">22/4/2016 <span class="new"></span></p>
                    <a href="#messages-list-1" aria-controls="messages-list-1" role="tab" data-toggle="tab" class="messages-link"></a>
                </li>
                
                <li>                
                    <div class="wrap-image">
                        <span class="teacher"><img src="/img/my-courses/author.jpg" alt=""/></span>
                        <span class="icon"><img src="/img/my-courses/icon-teacher.png" alt=""/></span>
                    </div><!--end .wrap-image-->                               
                    <h4 class="name">Nguyễn Văn A</h4>
                    <p class="date">22/4/2016 <span class="new"></span></p>
                    <a href="#messages-list-2" aria-controls="messages-list-2" role="tab" data-toggle="tab" class="messages-link"></a>
                </li> 
                
                <li>                
                    <div class="wrap-image">
                        <span class="teacher"><img src="/img/my-courses/author.jpg" alt=""/></span>
                        <span class="icon"><img src="/img/my-courses/icon-teacher.png" alt=""/></span>
                    </div><!--end .wrap-image-->                               
                    <h4 class="name">Nguyễn Văn A</h4>
                    <p class="date">22/4/2016 <span class="new"></span></p>
                    <a href="#messages-list-1" aria-controls="messages-list-1" role="tab" data-toggle="tab" class="messages-link"></a>
                </li>
                
                <li>                
                    <div class="wrap-image">
                        <span class="teacher"><img src="/img/my-courses/author.jpg" alt=""/></span>
                        <span class="icon"><img src="/img/my-courses/icon-teacher.png" alt=""/></span>
                    </div><!--end .wrap-image-->                               
                    <h4 class="name">Nguyễn Văn A</h4>
                    <p class="date">22/4/2016 <span class="new"></span></p>
                    <a href="#messages-list-2" aria-controls="messages-list-2" role="tab" data-toggle="tab" class="messages-link"></a>
                </li>                                                
                
            </ul>
        </div><!--end .left-->
    
        <div class="col-lg-8 col-md-7 col-xs-9 right">
            <div class="tab-content clearfix">            
                <div role="tabpanel" class="tab-pane clearfix active" id="messages-list-1"> 
                    <div class="wrap-content">
                        <h3>1. Chào bạn mình muốn hỏi?.</h3>
                        <p>Thầy cho em hỏi CPA chỉ phù hợp với những website theo dạng bán hàng Thầy cho em hỏi CPA chỉ phù hợp với những website theo dạng bán hàng Thầy cho em hỏi CPA chỉ phù hợp với những website theo dạng bán hàng Thầy cho em hỏi CPA chỉ phù hợp với những website theo dạng bán hàng Thầy cho em hỏi CPA chỉ phù hợp với những website theo dạng bán hàng Thầy cho em hỏi CPA chỉ phù hợp với những website theo dạng bán hàng Thầy cho em hỏi CPA chỉ phù hợp với những website theo dạng bán hàng Thầy cho em hỏi CPA chỉ phù hợp với những website theo dạng bán hàng Thầy cho em hỏi CPA chỉ phù hợp với những website theo dạng bán hàng Thầy cho em hỏi CPA chỉ phù hợp với những website theo dạng bán hàng Thầy cho em hỏi CPA chỉ phù hợp với những website theo dạng bán hàng Thầy cho em hỏi CPA chỉ phù hợp với những website theo dạng bán hàng Thầy cho em hỏi CPA chỉ phù hợp với những website theo dạng bán hàng Thầy cho em hỏi CPA chỉ phù hợp với những website theo dạng bán hàng</p>
                    </div><!--end .wrap-content-->                    
                </div><!--#courses-learned -->
        
                <div role="tabpanel" class="tab-pane clearfix" id="messages-list-2"> 
                    <div class="wrap-content">
                        <h3>2. Chào bạn mình muốn hỏi?.</h3>
                        <p>Thầy cho em hỏi CPA chỉ phù hợp với những website theo dạng bán hàng Thầy cho em hỏi CPA chỉ phù hợp với những website theo dạng bán hàng  Thầy cho em hỏi CPA chỉ phù hợp với những website theo dạng bán hàng Thầy cho em hỏi CPA chỉ phù hợp với những website theo dạng bán hàng Thầy cho em hỏi CPA Thầy cho em hỏi CPA chỉ phù hợp với những website theo dạng bán hàng Thầy cho em hỏi CPA chỉ phù hợp với những website theo dạng bán hàng  Thầy cho em hỏi CPA chỉ phù hợp với những website theo dạng bán hàng Thầy cho em hỏi CPA chỉ phù hợp với những website theo dạng bán hàng Thầy cho em hỏi CPA Thầy cho em hỏi CPA chỉ phù hợp với những website theo dạng bán hàng Thầy cho em hỏi CPA chỉ phù hợp với những website theo dạng bán hàng  Thầy cho em hỏi CPA chỉ phù hợp với những website theo dạng bán hàng Thầy cho em hỏi CPA chỉ phù hợp với những website theo dạng bán hàng Thầy cho em hỏi CPA chỉ phù hợp với những website theo dạng bán hàng Thầy cho em hỏi CPA chỉ phù hợp với những website theo dạng bán hàng Thầy cho em hỏi CPA chỉ phù hợp với những website theo dạng bán hàng Thầy cho em hỏi CPA chỉ phù hợp với những website theo dạng bán hàng  Thầy cho em hỏi CPA chỉ phù hợp với những website theo dạng bán hàng Thầy cho em hỏi CPA chỉ phù hợp với những website theo dạng bán hàng Thầy cho em hỏi CPA chỉ phù hợp với những website theo dạng bán hàng Thầy cho em hỏi CPA chỉ phù hợp với những website theo dạng bán hàng Thầy cho em hỏi CPA chỉ phù hợp với những website theo dạng bán hàng Thầy cho em hỏi CPA chỉ phù hợp với những website theo dạng bán hàng Thầy cho em hỏi CPA chỉ phù hợp với những website theo dạng bán hàng</p>
                    </div><!--end .wrap-content-->    
                </div><!--#courses-learned -->
            </div> 
            <div class="wrapper-comment">
                <ul >
                    <li class="reply"><a href="#reply"><i class="fa fa-reply" aria-hidden="true"></i> Trả lời</a></li>
                    <li class="delete"><a href="#"><i class="fa fa-trash-o" aria-hidden="true"></i> Xóa tin nhắn</a></li>                
                </ul>
                <div class="box-comment">
                    <form>
                        <img src="/img/my-courses/icon-border-messages.png"/>
                        <textarea class="form-control" rows="3" placeholder="Nhập trả lời của bạn ở đây"></textarea>
                        <div class="wrap-button">                            
                              <button type="submit" class="btn button">Gửi trả lời</button>                            
                        </div>
                    </form>
                </div><!--end .box-comment-->
                              
            </div><!--end .wrapper-commet-->
        </div><!--end .right-->       
    </div><!--end .wrapper-->                     
</div><!--end #courses-messages-->
