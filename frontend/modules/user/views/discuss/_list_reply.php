<?php
use yii\widgets\ListView;
use yii\data\ArrayDataProvider;

?>
<?= ListView::widget([
    'dataProvider' => ArrayDataProvider(['allModels' => $model->comments]),
    'layout' => '<div class="content-sub">{items}</div>',
    'id' => 'listview-replies-' . $model->id,
    'itemView' => '_reply_item',
    'emptyText' => false
]) ?>