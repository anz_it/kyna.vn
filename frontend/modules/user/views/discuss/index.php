<?php

$this->title = 'Thảo luận';
?>

<div class="tab-content clearfix col-xs-12" id="discuss-courses-main">
    <div id="my-question">
        <?= \yii\widgets\ListView::widget([
            'dataProvider' => $searchModel->search(null),
            'itemView' => '_item',
            'layout' => "{items}\n<nav id='pager-container'>{pager}</nav>",
            'pager' => [
                'prevPageLabel' => '<span>&laquo;</span>',
            'nextPageLabel' => '<span>&raquo;</span>',
            'options' => [
                'class' => 'pagination',
            ],
        ]
        ]) ?>
    </div>
</div>
