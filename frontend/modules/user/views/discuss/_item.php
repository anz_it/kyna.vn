<?php
use yii\helpers\Url;
use common\helpers\CDNHelper;

$cdnUrl = CDNHelper::getMediaLink();
?>
<div class="box-question row">
    <div class="avt-member hidden-sm-down">
        <a href="#">
            <img class="icon-question" alt="question" src="<?= $cdnUrl ?>/img/icon/icon-question.png">
            <img class="avt" alt="<?= $model->user->profile->name ?>" src="<?= $model->user->avatarImage ?>" style="width: 100%;"
            data-holder-rendered="true">
        </a>
    </div>
    <div class="col-content">
        <div class="row-title">
              <span class="title-name">
                    <?= $model->user->profile->name ?>
              </span>
              <span>
                    <i class="icon-circle hidden-sm-down"></i>
              </span>
              <span class="title-date hidden-sm-down">
                    <?= $model->postedTime ?>
              </span>
            <span class="title-date hidden-md-up">(<?= $model->postedTime ?>)</span>
        </div>
        <div class="row-question">
            <?= $model->comment ?>
        </div>
        <div class="row-action hidden-sm-down">
            <!--<span>
              1 <i class="icon-like"></i>
            </span>
            <span>
              <i class="icon-circle"></i>
            </span>-->
            <!--<a href="#lesson-form-reply-<?= $model->id ?>">
            <span class="btn-reply-discuss courses-style">
              <i class="icon-reply"></i> Trả lời
            </span>
            </a>-->
            <span>
              <i class="icon-circle"></i>
            </span>
            <span class="courses-style">Khóa <a href="<?= Url::toRoute(['/course/learning/index', 'id' =>  $model->course_id]) ?>"><?= $model->course->name; ?></a></span>
            <!--<span class="courses-style">
              <i class="icon-follow"></i> Theo dõi
            </span>-->
        </div>
        <?php if ($model->comments):?>
        <?= $this->render('_list_reply', ['model' => $model]) ?>
        <?php endif;?>
    </div>
</div>
