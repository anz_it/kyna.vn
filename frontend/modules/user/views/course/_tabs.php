<?php

use yii\helpers\Url;
use kyna\user\models\UserCourse;

if (!isset($learned)) {
    $searchModel->is_started = UserCourse::BOOL_YES;
    $learned = $searchModel->searchCourse()->totalCount;
}
if (!isset($bought)) {
    $searchModel->is_started = null;
    $bought = $searchModel->searchCourse()->totalCount;
}
if (!isset($notStarted)) {
    $searchModel->is_started = UserCourse::BOOL_NO;
    $notStarted = $searchModel->searchCourse()->totalCount;
}

$userCourseParams = Yii::$app->request->get('UserCourseSearch');
$params = [
    'UserCourseSearch[categoryId]' => isset($userCourseParams['categoryId']) ? $userCourseParams['categoryId'] : null,
    'UserCourseSearch[courseName]' => isset($userCourseParams['courseName']) ? $userCourseParams['courseName'] : null
];
?>

<ul class="nav nav-tabs">
    <li <?= (Yii::$app->controller->action->id == 'index') ? 'class="active-tab"' : '' ?>>
        <a href="<?= Url::toRoute(['/user/course/index'] + $params) ?>">Tất cả (<?= $bought; ?>)</a>
    </li>
    <li <?= (Yii::$app->controller->action->id == 'not-started') ? 'class="active-tab"' : '' ?>>
        <a href="<?= Url::toRoute(['/user/course/not-started'] + $params) ?>">Chưa học (<?= $notStarted; ?>)</a>
    </li>
    <li <?= (Yii::$app->controller->action->id == 'started') ? 'class="active-tab"' : '' ?>>
        <a href="<?= Url::toRoute(['/user/course/started'] + $params) ?>">Đang học (<?= $learned; ?>)</a>
    </li>
    <!--        <li><a data-toggle="tab" href="#mycourses-care">Quan tâm (20)</a></li>-->
</ul>
