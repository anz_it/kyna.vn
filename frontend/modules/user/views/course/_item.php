<?php

use yii\helpers\Url;
use yii\bootstrap\Html;
use common\helpers\DocumentHelper;
use common\helpers\CDNHelper;

/* @var $model \kyna\user\models\UserCourse */

$course = $model->course;
if ($model->is_started) {
    $learnUrl = Url::toRoute([
        '/course/learning/index',
        'id' => !empty($course->id) ? $course->id : '',
        'section' => !empty($model->current_section) ? $model->current_section : '',
        'lesson' => !empty($model->current_lesson) ? $model->current_lesson : '',
    ]);
} else {
    $learnUrl = Url::toRoute([
        '/course/learning/index',
        'id' => !empty($course->id) ? $course->id : '',
    ]);
}

$teacherUrl = Url::toRoute(['/user/teacher/index', 'slug' => !empty($course) ? $course->teacher->slug : '']);
?>
<div class="k-box-card-wrap clearfix">
    <div class="img">
        <a href="<?= $learnUrl ?>" title="<?= $course->name ?>" data-pjax="0">
            <?= CDNHelper::image($course->image_url, [
                'alt' => $course->name,
                'class' => 'img-fluid',
                'size' => CDNHelper::IMG_SIZE_THUMBNAIL,
                'resizeMode' => 'cover',
            ]) ?>
        </a>
        <span class="time hidden-xs-down"><?= !empty($course->totalTimeText) ? $course->totalTimeText : '' ?></span>
        <div class="rating-mb hidden-sm-up"><b><?= $course->rating ?><i class="icon-star"></i></b></div>
    </div>
    <!--end .img-->
    <div class="content">
        <h4>
            <a href="<?= $learnUrl ?>" title="<?= $course->name ?>" data-pjax="0">
                <?= !empty($course->name) ? $course->name : '' ?>
            </a>
        </h4>
        <a href="<?= $teacherUrl ?>" data-pjax = "0" class="author"><?= !empty($course->teacher->profile->name) ? $course->teacher->profile->name : ''; ?></a>
        <span class=""><?= !empty($course->teacher->title) ? $course->teacher->title : '' ?></span>
    </div>
    <!--end .content -->
    <div class="wrap-progress clearfix">
        <div class="point-box clearfix">
            <span class="left">Điểm hiệu quả</span>
            <span class="right"><?= $model->process?>% <i class="fa fa-question-circle" aria-hidden="true"><div class="popup-guide">Điểm hiệu quả được tính theo thời gian xem video bài học và điểm các bài kiểm tra trong khóa học</div></i></span>
        </div>
        <div class="progress hidden-xs-down">
            <div class="progress-bar" style="width:<?=  $model->process . '%' ?>"></div>
        </div>
        <div class="details-bottom clearfix">
            <?php
            if ($model->process === 100) {
                $class = 'btn-replay';
                $text = 'Học lại';
            } elseif ($model->is_started) {
                $class = 'btn-play';
                $text = 'Tiếp tục học';
            } else {
                $class = 'btn-play';
                $text = 'Bắt đầu học';
            }
            ?>
            <div class="btn-start">
                <a href="<?= $learnUrl ?>" class="btn <?= $class ?>" data-pjax="0"><?= $text ?></a>
            </div>
            <?php if ($model->isGraduated) : ?>
                <div class="btn-medal">
                    <?php
                    $userId = Yii::$app->user->id;
                    $queryParams = [
                        'courseId' => $model->course_id,
                        'userId' => $userId
                    ];
                    $hash = DocumentHelper::hashParams($queryParams);
                    $url = Url::toRoute(['/course/certificate/index', 'courseId' => $model->course_id, 'userId' => $userId, 'hash' => $hash])
                    ?>
                    <?= Html::a('&nbsp;', $url, [
                        'class' => 'btn img-favorite-courses',
                        'target' => '_blank',
                        'data-pjax' => '0'
                    ]) ?>
                    <div class="popup-guide">
                        Khi tham gia học trên Kyna.vn, bạn sẽ nhận được chứng chỉ hoàn thành khi đạt 50% và chứng chỉ xuất sắc khi đạt 70%
                    </div>
                </div>
            <?php endif; ?>
            <div class="s-rating hidden-xs-down" style="float:right;">
                <!-- <strong style="color: #fb6a00;"><?= $model->process . '%' ?></strong> -->
                <?php if ($course->rating_users_count > 0) : ?>
                <ul>
                    <li>
                        <span>
                            <i class="icon-star<?= ($course->rating == 0.5 ? '-half' : '') ?> <?= ($course->rating > 0 ? 'color-rating' : '') ?>"></i>
                            <i class="icon-star<?= ($course->rating == 1.5 ? '-half' : '') ?> <?= ($course->rating > 1 ? 'color-rating' : '') ?>"></i>
                            <i class="icon-star<?= ($course->rating == 2.5 ? '-half' : '') ?> <?= ($course->rating > 2 ? 'color-rating' : '') ?>"></i>
                            <i class="icon-star<?= ($course->rating == 3.5 ? '-half' : '') ?> <?= ($course->rating > 3 ? 'color-rating' : '') ?>"></i>
                            <i class="icon-star<?= ($course->rating == 4.5 ? '-half' : '') ?> <?= ($course->rating > 4 ? 'color-rating' : '') ?>"></i>
                        </span>
                    </li>
                </ul>
                <?php endif; ?>
            </div>
        </div><!--end .details-bottom-->
    </div><!--end .wrap-progress-->
</div>
