<?php

use yii\widgets\ListView;
use yii\helpers\Url;
use yii\widgets\Pjax;

$this->title = 'Khóa học của tôi';
?>
<?php Pjax::begin([]) ?>
<div id="mycourses" class="clearfix">
    <?= $this->render('_tabs', ['learned' => $dataProvider->totalCount, 'searchModel' => $searchModel]) ?>
    <?= $this->render('_search', ['searchModel' => $searchModel, 'action' => Url::to(['/user/course/started'])]) ?>
</div>

<div id="mycourses-learned">
    <?=
    ListView::widget([
        'dataProvider' => $dataProvider,
        'itemView' => '_item',
        'layout' => "{items}\n<nav id='pager-container'>{pager}</nav>",
        'options' => [
            'tag' => 'ul',
            'class' => 'clearfix k-box-card-list',
            'data-pjax' => true,
            'role' => 'tabpanel'
        ],
        'itemOptions' => [
            'tag' => 'li',
            'class' => 'col-xl-3 col-lg-4 col-md-6 col-xs-12 k-box-card'
        ],
        'pager' => [
            'prevPageLabel' => '<span>&laquo;</span>',
            'nextPageLabel' => '<span>&raquo;</span>',
            'options' => [
                'class' => 'pagination',
            ],
        ]
    ])
    ?>
</div>
    <script type="text/javascript">
        $(document).ready(function(){
            CoursesAction.ResizeBoxProduct("#mycourses-learned");
            if (window.innerWidth < 768) {
                $("#mycourses-learned").removeClass("hidden-sm-down")
            }
        });
        /* Menu-tab Courses */
    </script>
<?php Pjax::end() ?>
