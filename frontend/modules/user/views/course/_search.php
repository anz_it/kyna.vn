<?php

use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\web\View;

use kyna\user\models\UserCourse;
use kyna\course\models\Category;
use kyna\course\models\Course;

$categories = Category::find()->joinWith('courses')->andWhere([
    Course::tableName() . '.id' => array_keys(UserCourse::find()->select('course_id')->andWhere(['user_id' => Yii::$app->user->id])->asArray()->indexBy('course_id')->all()),
    'parent_id' => 0
])->select([Category::tableName() . '.id', Category::tableName() . '.name'])->all();
?>
<style>
    #mycourses .search-filter {
        display: inline-block;
        float: right;
        width: 460px;
        position: relative;
        top: 20px;
    }
    #mycourses form .input-group-btn button {
        border: none;
    }
    #mycourses form .input-group-btn button:focus {
        outline: none;
        background: none;
    }
    #mycourses form input {
        border: none;
        border-radius: 5px;
    }
    #mycourses form input:focus {
        outline: none !important;
        box-shadow: none !important;
    }

    #mycourses form .input-group-btn button:hover {
        background: none;
        color: #387836;
    }
    #mycourses ul.nav {
        display: inline-block;
    }
    #mycourses .search-filter button {
        background: none;
        border: 1px solid #ddd;
    }

    #mycourses .search-filter form select {
        width: 200px;
        height: 37px;
        padding:5px;
        margin: 0;
        -webkit-border-radius:4px;
        -moz-border-radius:4px;
        border-radius:4px;
        border: 1px solid #ddd;
        color:#888;
        outline:none;
        display: inline-block;
        -webkit-appearance:none;
        -moz-appearance:none;
        appearance:none;
        cursor:pointer;
    }

    /* Targetting Webkit browsers only. FF will show the dropdown arrow with so much padding. */
    @media screen and (-webkit-min-device-pixel-ratio:0) {
        select {padding-right:18px}
    }

    #mycourses .search-filter form label {
        position: relative;
    }
    #mycourses .search-filter form label:after {
        content:'<>';
        font:11px "Consolas", monospace;
        color:#aaa;
        -webkit-transform:rotate(90deg);
        -moz-transform:rotate(90deg);
        -ms-transform:rotate(90deg);
        transform:rotate(90deg);
        right:8px; top:11px;
        padding:0 0 2px;
        border-bottom:1px solid #ddd;
        position:absolute;
        pointer-events:none;
    }
    #mycourses .search-filter form label:before {
        content:'';
        right:6px; top:0px;
        width:20px; height:20px;
        position:absolute;
        pointer-events:none;
        display:block;
    }
    #mycourses .search-filter form .wrap-input-filter {
        width: calc(100% - 210px);
        float: right;
        border: 1px solid #ddd;
        border-radius: 3px;
    }

    @media (max-width: 1199px) {
        .body-courses-section ul#menu-courses .deltaxu {
            display: none;
        }
    }
    @media (max-width: 767px) {
        #mycourses {
            display: inline-block;
            margin-left: -15px;
            margin-right: -15px;
        }
        #mycourses .search-filter {
            width: 100%;
            top: 0px;
            padding: 0 15px;
        }
        #mycourses .open > .dropdown-menu {
            display: block;
            width: 100%;
        }
        #mycourses .search-filter button {
            width: 100%;
        }
        #mycourses .search-filter .btn-group {
            width: 100%;
        }
        #mycourses form {
            width: 100%;
            margin-top: 15px;
        }
        #mycourses ul.nav {
            display: table;
            font-size: 14px;
        }
        #mycourses ul.nav li{
            padding: 10px 5px;
            height: auto;
        }
    }
    @media (max-width: 543px) {
        #mycourses .search-filter form label,
        #mycourses .search-filter form label select {
            width: 100%;
        }
        #mycourses .search-filter form .wrap-input-filter {
            width: 100%;
            max-width: none;
        }
    }
</style>
<div class="search-filter">
    <?php $form = ActiveForm::begin([
        'id' => 'user-course-search-form',
        'method' => 'get',
        'action' => $action,
        'options' => [
            'data-pjax' => ''
        ]
    ]) ?>
        <label>
            <?= $form->field($searchModel, 'categoryId')->dropDownList(ArrayHelper::map($categories, 'id', 'name'), ['prompt' => 'Chọn danh mục', 'id' => 'select-cat'])->label(false)->error(false) ?>
        </label>

        <div class="input-group wrap-input-filter">
            <?= $form->field($searchModel, 'courseName')->textInput(['placeholder' => 'Tìm khóa học bạn đang quan tâm'])->label(false)->error(false) ?>
            <span class="input-group-btn">
                <button class="btn btn-secondary search-button" type="submit"><i class="icon-search icon hidden-480"></i></button>
            </span>
        </div>
    <?php ActiveForm::end() ?>
</div>
<?php
$script = "
    (function($) {
        $('body').on('change', '#select-cat', function () {
            $('#user-course-search-form').submit();
        });
    })(jQuery);
";

$this->registerJs($script, View::POS_END, 'user-course-filter');
?>
