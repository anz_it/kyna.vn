<?php

use yii\widgets\ListView;
use yii\helpers\Url;
use yii\widgets\Pjax;

use kyna\user\models\UserCourse;

$this->title = 'Khóa học của tôi';


// Set breadcrumbs css
$this->registerCss("
    @media (max-width: 767px) {
        .breadcrumb-container {
            display: none;
        }
    }
    .breadcrumb-container {
        margin-top: 67px;
        background-color: #fafafa;
    }
	@media (min-width: 768px) and (max-width: 991px){
		.breadcrumb-container{
			margin-top: 0;
		}
	}
    .breadcrumb {

        border-radius: 0px;
        padding: 8px 0px;
        margin: 0px;
        background-color: inherit;
    }
    .breadcrumb > li + li::before {
        padding-right: .5rem;
        padding-left: .5rem;
        color: #a0a0a0;
        content: \"»\";
    }
    .breadcrumb > li {
        color: #666 !important;
        font-weight: bold;
    }
    .breadcrumb > li > a {
        color: inherit;
    }
    .breadcrumb > li.active {
        color: #666 !important;
        font-weight: normal;
    }
    #k-listing {
        margin-top: 0px;
        padding-top: 30px;
    }

    #hot-courses h3 {
        display: none;
    }

    @media (min-width: 768px) {
        #hot-courses h3 {
            display: block;
            color: #50ad4e;
            font-size: 18px;
            padding-left: 15px;
        }

        #best-seller-courses h3 {
            display: block;
            color: #50ad4e;
            font-size: 18px;
            padding-left: 15px;
        }
    }

    .hot-category .name {
        position: absolute;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        text-align: center;
        padding-top: 22%;
        font-size: large;
        font-weight: bolder;
        color: #FFFFFF;
    }

    .hot-category:hover .overlay {
        width:100%;
        height:100%;
        position:absolute;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        background-color:#000;
        opacity:0.2;
        border-radius:1px;
    }

");

?>
<?php Pjax::begin([]) ?>
<div id="mycourses" class="clearfix">
    <?= $this->render('_tabs', ['bought' => $dataProvider->totalCount, 'searchModel' => $searchModel]) ?>
    <?= $this->render('_search', ['searchModel' => $searchModel, 'action' => Url::to(['/user/course/index'])]) ?>
</div>
<div id="mycourses-buy">
    <?=
    ListView::widget([
        'dataProvider' => $dataProvider,
        'itemView' => '_item',
        'layout' => "{items}\n<nav id='pager-container'>{pager}</nav>",
        'options' => [
            'tag' => 'ul',
            'class' => 'clearfix k-box-card-list',
            'role' => 'tabpanel',
            'data-pjax' => true
        ],
        'itemOptions' => [
            'tag' => 'li',
            'class' => 'col-xl-3 col-lg-4 col-md-6 col-xs-12 k-box-card'
        ],
        'pager' => [
            'prevPageLabel' => '<span>&laquo;</span>',
            'nextPageLabel' => '<span>&raquo;</span>',
            'options' => [
                'class' => 'pagination',
            ],
        ]
    ])
    ?>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        CoursesAction.ResizeBoxProduct("#mycourses-buy");
        if (window.innerWidth < 768) {
            $("#mycourses-buy").removeClass("hidden-sm-down")
        }
        $('footer').css('z-index','10');
    });
    /* Menu-tab Courses */
</script>
<?php Pjax::end() ?>
