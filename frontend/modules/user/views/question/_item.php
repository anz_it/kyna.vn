<?php
use yii\helpers\Url;
use common\helpers\CDNHelper;

$cdnUrl = CDNHelper::getMediaLink();

$formatter = Yii::$app->formatter;
?>
<div class="box-question row">
    <div class="avt-member hidden-sm-down">
        <img class="icon-question" alt="question" src="<?= $cdnUrl ?>/img/my-courses/icon-question.png">
        <img class="avt" alt="avatar-member" src="<?= $model->user->avatarImage; ?>">
    </div>
    <div class="col-content">
        <div class="row-title">
            <span class="title-name"><?= $model->user->profile->name; ?></span>
            <span><i class="icon-circle hidden-sm-down"></i></span>
            <span class="title-date hidden-sm-down"><?= $formatter->asDatetime($model->posted_time); ?></span>
            <div class="title-date hidden-md-up">(<?= $formatter->asDatetime($model->posted_time); ?>)</div>
        </div>
        <div class="courses-style hidden-md-up">Khóa <a href="<?= Url::toRoute(['/course/learning/index', 'id' => $model->course_id]) ?>"><?= $model->course->name; ?></a></div>
        <div class="row-question">
            <?= $model->content; ?>
        </div>
        <div class="row-action hidden-sm-down">
            <!--                <span>1 <i class="icon-like"></i></span>-->
            <span><i class="icon-circle hidden-sm-down"></i></span>
            <span class="courses-style">Khóa <a href="<?= Url::toRoute(['/course/learning/index', 'id' => $model->course_id]) ?>"><?= $model->course->name; ?></a></span>
        </div>
        <?php $answers = $model->answers; ?>
        <?php foreach ($answers as $answer) { ?>
            <div class="row-reply">
                <div class="calloutUpfront hidden-sm-down">
                    <div class="calloutUpback">
                    </div>
                </div>
                <div class="txt-reply">
                    <div class="inner">
                        <?= $answer->content; ?>
                    </div>
                </div>
                <div class="see-more">Xem thêm</div>
            </div>
        <?php } ?>
    </div>
</div>
