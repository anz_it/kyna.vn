<?php
use yii\widgets\ListView;

$this->title = 'Hỏi đáp';
?>
<div role="tabpanel" class="tab-pane clearfix tab-parent-content active" id="question-courses-main">

    <?php
    $question_count = $searchModel->search(null)->totalCount;
    ?>
    <div id="mycourses">
        <ul class="nav nav-tabs sub-menu">
            <li><a data-toggle="tab" href="#my-questions">Câu hỏi của bạn (<?= $question_count; ?>)</a></li>
<!--            <li><a data-toggle="tab" href="#followed-questions">Đang quan tâm (20)</a></li>-->
        </ul>
    </div>

    <div id="my-questions">
<!--        <div class="clearfix">-->
<!--            <form action="search_result.php">-->
<!--                <div class="search-input-area col-md-5 col-sm-12">-->
<!--                    <input class="input-search" type="text">-->
<!--                    <button class="btn-search" type="button"><i class="icon-search"></i> </button>-->
<!--                </div>-->
<!--            </form>-->
<!--            <div class="wrap-sort col-md-7 col-sm-12">-->
<!--                Sắp xếp theo <button class="btn-lasted">Mới nhất</button>-->
<!--                <button class="btn-highlight">Mới nhất</button>-->
<!--            </div>-->
<!--        </div>-->
            <?= ListView::widget([
                'dataProvider' => $searchModel->search(null),
                'itemView' => '_item',
                'layout' => "{items}\n<div class='pagination'>{pager}</div>",
//                    'options' => [
//                        'tag' => 'ul',
//                        'class' => 'tab-pane active clearfix learning-wrap',
//                        'id' => 'courses-bought',
//                        'role' => 'tabpanel'
//                    ],
//                    'itemOptions' => [
//                        'tag' => 'li',
//                        'class' => 'col-lg-4 col-md-6 col-xs-12 box-product'
//                    ],
                'pager' => [
                    'prevPageLabel' => '<i class="fa fa-angle-left color-green"></i>',
                    'nextPageLabel' => '<i class="fa fa-angle-right color-green"></i>',
                    'options' => [
                        'id' => 'pager-container',
                    ]
                ]
            ]) ?>

</div><!--end #courses-support-->
    <script type="application/javascript">
        CoursesAction.CheckAnswerHeight();
    </script>

