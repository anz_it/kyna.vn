<?php
/**
 * Created by IntelliJ IDEA.
 * User: trangnguyen
 * Date: 3/27/2018
 * Time: 11:11 AM
 */
use yii\helpers\Url;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ActiveForm;
use yii\captcha\Captcha;
use common\helpers\CDNHelper;

$cdnUrl = CDNHelper::getMediaLink();
$this->title = 'Kích hoạt khóa học COD';
?>

<?php
$form = ActiveForm::begin([
    'id' => 'active-otp-form',
    'enableAjaxValidation' => false,
    'enableClientValidation'=> false,
    'action' => Url::toRoute(['/user/order/active-cod']),
    'options' => [
        'class' => 'otp-form'
    ]
]);
?>
    <div class="modal-body">
        <h3><?= $this->title ?></h3>

        <?= $form->errorSummary($order_form, ['class' => 'error-report otp-error-summary', 'encode' => '<i class="icon-error"></i>']) ?>

        <?php if (Yii::$app->session->hasFlash('otp_msg')): ?>
            <div class="otp-summary">
                <ul>
                    <li><?= Yii::$app->session->getFlash('otp_msg') ?></li>
                </ul>
            </div>
        <?php endif; ?>

        <h4>Bước 1: Nhập mã thẻ kích hoạt COD <i class="fa fa-question-circle" id="cod-popover" title="Mã thẻ kích hoạt là gì?" data-trigger="hover focus" data-placement="left" data-toggle="popover" data-content="Mã thẻ kích hoạt là 1 dãy 6 ký tự, bao gồm chữ và số. Ví dụ: NBW6RQ"></i></h4>
        <?= $form->field(
            $order_form,
            'activation_code',
            [
                'options' => ['class' => 'input'],
                'inputOptions'   => ['class' => 'form-control', 'id' => 'codeCOD'],
                'template' => '<div class="input-cod"><span class="icon"><i class="icon-card"></i></span>{input}</div>{hint}',
            ]
        )->textInput();
        ?>

        <h4 class="otp-header">Bước 2: Nhập mã xác thực OTP <i class="fa fa-question-circle" id="otp-popover" title="Mã xác thực OTP là gì?" data-trigger="hover focus" data-placement="left" data-toggle="popover" data-content="Mã xác thực OTP là 1 dãy 6 số. Ví dụ: 352648"></i></h4>
        <?php if (Yii::$app->session->has('otp_captcha')): ?>
            <label class="otp-info">Để tiếp tục lấy mã OTP, vui lòng nhập các ký tự trong hình</label>
            <?= $form->field($order_form, 'verify_code', ['options' => ['class' => 'clearfix'],])->widget(Captcha::className(), [
                'captchaAction' => '/user/order/captcha',
                'template' => '{input} <div class="otp-captcha">{image}</div>',
                'options' => ['placeholder' => 'Nhập Captcha', 'class' => 'form-control'],
            ])->label(false) ?>
            <?= Html::button('Gửi mã về số điện thoại', ['class' => 'btn btn-get-otp captcha', 'data-loading-text' => "<i class='fa fa-spinner fa-spin '></i> Đang gửi"]); ?>
        <?php else: ?>
            <?= Html::button('Lấy mã xác thực OTP', ['class' => 'btn btn-get-otp', 'data-loading-text' => "<i class='fa fa-spinner fa-spin '></i> Đang gửi"]); ?>
        <?php endif; ?>
        <?= $form->field(
            $order_form,
            'otp_code',
            [
                'options' => ['class' => 'input'],
                'inputOptions'   => ['class' => 'form-control', 'id' => 'codeOTP'],
                'template' => '<div class="input-cod"><span class="icon"><i class="icon-card"></i></span>{input}</div>{hint}',
            ]
        )->textInput();
        ?>

        <?= Html::button('Kích hoạt khóa học', ['class' => 'btn btn-active-otp', 'id' => 'btn_submit_otp', 'data-loading-text' => "<i class='fa fa-spinner fa-spin '></i> Đang xử lý"]); ?>

        <?php
        $redirectUrl = Yii::$app->user->isGuest ? Url::toRoute(['/']) : Url::toRoute(['/user/course/index']);
        $script = "
        (function($) {
            $(document).ready(function(){
                $('#cod-popover').popover();
                $('#otp-popover').popover();
                if (window.location.pathname && window.location.pathname.indexOf('kich-hoat') > -1) {
                    var content = $('#active-otp-form');
                    $('#activeCOD .modal-content').append(content);
                    $('#activeCOD').modal();
                    $('#activeCOD').on('hidden.bs.modal', function() {
                        window.location.href = '{$redirectUrl}';
                    })
                }
                $('#btn_submit_otp').click(function(e){
                    var that = $(this);
                    that.button('loading');
                    var form = $('#active-otp-form');
                    var url = form.attr('action');
                    $.post(url, form.serialize(), function (res) {
                        that.button('reset');
                        form.parent().html(res);
                    }).error(function(){
                        that.button('reset');
                    });
                });
                $('.btn-get-otp').click(function(){
                    var that = $(this);
                    that.button('loading');
                    var form = $('#active-otp-form');                   
                    $.post('/user/order/get-otp-code', form.serialize(), function (res) {
                        that.button('reset');
                        form.parent().html(res);
                    }).error(function(){
                        that.button('reset');
                    });
                });               
            });
        })(jQuery);
        ";
        $this->registerJs($script);
        ?>

    </div>
<?php ActiveForm::end(); ?>

<?php
$css = "
        .modal.modal-activeCOD .modal-dialog {
            margin: 80px auto 10px !important;
        }
        .otp-form button {
            width: 100%;
        }
        .otp-form .modal-body > h4 {
            font-size: 14px;
            margin-top: 10px;
            margin-bottom: 10px;
        }
        .otp-form .modal-body > h3 {           
            margin-bottom: 20px;
            text-align: center;
        }
        .otp-form .otp-summary {
            position: relative;
            padding: 8px 0 8px 40px;
            color: rgb(80, 172, 77);
            border: 1px solid rgb(80, 172, 77);
            margin-top: 20px;
            margin-bottom: 10px;
        }
        .otp-form .otp-summary ul {
            margin-bottom: 0;
        }
        .otp-form .otp-summary:before {
            content: \"\";
            position: absolute;
            width: 24px;
            height: 24px;
            top: calc(50% - 12.5px);
            left: 8px;
            background-image: url({$cdnUrl}/img/otp/icon-success-otp.png);
        }
        .otp-form .otp-error-summary {
            position: relative;
            padding: 8px 0 8px 40px !important;
            color: rgb(190, 82, 83) !important;
            border-color: rgb(190, 82, 83) !important;
        }
        .otp-form .otp-error-summary ul {
            text-align: left !important;
        }
        .otp-form .otp-error-summary p {
            display: none;
        }
        .otp-form .error-report.otp-error-summary:before {
            content: \"\";
            position: absolute;
            width: 24px;
            height: 24px;
            top: calc(50% - 12.5px);
            left: 8px;
            background-image: url({$cdnUrl}/img/otp/icon-error-otp.png);
        }       
        .otp-form .fa-question-circle {
            font-size: 18px;
            color: #777777;
            position: relative;
            top: 1px;
            left: 5px;
            cursor: pointer;
        }      
        .otp-form .btn-get-otp {
            width: 100%;
            background-color: rgba(0,0,0,0);
            border: 2px solid #ff8e43;
            color: #ff8e43;
        }
        .otp-form .btn-get-otp.captcha {
            border: 2px solid #666;
            color: #353535;
            background-color: rgb(222, 222, 222);
            margin-top: 15px;
        }
        .otp-form .field-otpform-verify_code {
            margin-top: 0;
        }
        .otp-form .otp-info {
            color: rgb(190, 82, 83);
        }
        .otp-form .field-otpform-verify_code input {
            width: calc(100% - 105px);
            float: left;
            border-radius: 3px;
        }
        .otp-form .field-otpform-verify_code img {
            width: 90px;           
            height: 30.5px;
        }
        .otp-form .field-codeOTP {
            margin-top: 15px;
        }
        .otp-form .otp-header {
            margin-top: 15px;
        }
        .otp-form .btn-active-otp {
            width: 100%;
            padding: .375rem 1rem;
            margin-top: 20px;
            background: #50ad4e;
            color: white;
            border-bottom: 2px solid #489b46;          
            display: inline-block;
            border-radius: 3px;
            transition: background-color 100ms ease-in-out, color 100ms ease-in-out, border-color 100ms ease-in-out;
        }
        .otp-form .error-report {
            margin-top: 20px !important;
        }
        .otp-form .help-block {
            display: none;
        }
        .otp-form .otp-captcha {
            float: right;
            background-image: url({$cdnUrl}/img/otp/bg-captcha.png);
            border: 1px solid #aaa;
            margin-top: 1px;
        }
    ";
$this->registerCss($css);
?>