<?php
/**
 * @author: Hong Ta
 * @desc: views of active cod form.
 */
use yii\helpers\Url;
use yii\helpers\Html;
use yii\web\View;
use app\modules\user\models\forms\ProfileForm;
use common\helpers\CDNHelper;

//use common\widgets\locationfield\LocationField;
use yii\widgets\ActiveForm;
$this->title = 'Kích hoạt COD';
$cdnUrl = CDNHelper::getMediaLink();
?>

<?php if(empty($message)) { ?>

<?php
$form = ActiveForm::begin([
    'id' => 'active-cod-form',
    'action' => Url::toRoute(['/user/order/active-cod']),
]);
?>
<div class="model-header">
    <span class="icon-cross" data-dismiss="modal"></span>
</div>
<div class="modal-body">
    <center><h4>Nhập mã thẻ kích hoạt</h4></center>

    <?= $form->errorSummary($order_form, ['class' => 'error-report', 'encode' => '<i class="icon-error"></i>']) ?>

    <?= $form->field(
        $order_form,
        'activation_code',
        [
            'options' => ['class' => 'input'],
            'inputOptions'   => ['placeHolder' => 'Mã thẻ kích hoạt', 'class' => 'form-control', 'id' => 'codeCOD'],
            'template' => '<div class="input-cod"><span class="icon"><i class="icon-card"></i></span>{input}</div>{hint}',
        ]
    )->textInput()->hint('<div class="question-cod"><a role="button" id="cod-popover" title="Mã thẻ kích hoạt là gì?" data-trigger="hover focus" data-container=".question-cod" data-placement="top" data-toggle="popover" data-content="Mã thẻ kích hoạt là 1 dãy 6 ký tự, bao gồm chữ và số. Ví dụ: NBW6RQ" style="color: #a0a0a0;">' . '  Mã thẻ kích hoạt là gì?' . '</a></div>')
    ?>

    <center><?= Html::submitButton('Kích hoạt', ['class' => 'btn btn-active-cod']); ?></center>

    <?php
    $redirectUrl = Yii::$app->user->isGuest ? Url::toRoute(['/']) : Url::toRoute(['/user/course/index']);
    $script = "
    (function($) {
        $(document).ready(function(){
            $('#cod-popover').popover();
            if (window.location.pathname && window.location.pathname.indexOf('kich-hoat') > -1) {
                var content = $('#active-cod-form');
                $('#activeCOD .modal-content').append(content);
                $('#activeCOD').modal();
                $('#activeCOD').on('hidden.bs.modal', function() {
                    window.location.href = '{$redirectUrl}';
                })
            }
        });
    })(jQuery);
    ";
      $this->registerJs($script);
    ?>

</div>
<?php ActiveForm::end(); ?>
<?php } else { ?>
<div class="modal-body">
    <div class="alert alert-success">
        <?= $message; ?>
    </div>
</div>
    <div class="modal-footer" style="border-top: none; padding-top: 0">
        <div class="input col-xs-12">
            <!--        <div class="col-sm-2 col-xs-12 left pd0"></div>-->
            <div class="col-sm-12 col-xs-12 right">
                <div style="margin: 0 auto; width: 60%">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>

            </div><!--end .right-->
        </div><!--end .input-->
    </div>

<?php } ?>

<?php
    $css = "
        .modal.modal-activeCOD .modal-dialog {
            margin: 80px auto 10px !important;
        }
        #active-cod-form .error-summary {
            position: relative;
            padding: 8px 0 8px 40px !important;
            color: rgb(190, 82, 83) !important;
            border-color: rgb(190, 82, 83) !important;
        }
        #active-cod-form .error-summary ul {
            text-align: left !important;
        }
        #active-cod-form .error-summary p {
            display: none;
        }
        #active-cod-form .error-report:before {
            content: \"\";
            position: absolute;
            width: 24px;
            height: 24px;
            top: calc(50% - 12.5px);
            left: 8px;
            background-image: url({$cdnUrl}/img/otp/icon-error-otp.png);
        }    
    ";
    $this->registerCss($css);
?>
