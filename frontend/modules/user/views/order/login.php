<?php
/**
 * Created by IntelliJ IDEA.
 * User: trangnguyen
 * Date: 3/27/2018
 * Time: 11:11 AM
 */
use yii\helpers\Url;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ActiveForm;
use yii\captcha\Captcha;
use common\helpers\CDNHelper;

$cdnUrl = CDNHelper::getMediaLink();
$this->title = 'Kích hoạt khóa học COD';
?>

<?php
$form = ActiveForm::begin([
    'id' => 'active-otp-form',
    'enableAjaxValidation' => false,
    'enableClientValidation'=> false,
    'action' => Url::toRoute(['/user/order/login']),
    'options' => [
        'class' => 'otp-form'
    ]
]);
$msg = 'Tài khoản học của bạn đã sử dụng số điện thoại khác với 2 số đã có trong lịch sử để mua khóa học mới. Để đảm bảo an toàn, vui lòng đăng nhập mật khẩu đang sử dụng';
if ($model->hasErrors('password')) {
    $msg = 'Mật khẩu không chính xác';
    if (empty($model->password)) {
        $msg = 'Mật khẩu không được để trống';
    }
}
?>
    <div class="modal-body">
        <h3><?= $this->title ?></h3>

        <div class="error-report error-summary otp-error-summary <?= $model->hasErrors('password') ? 'form-error-summary' : '' ?>">
            <ul>
                <li><?= $msg ?></li>
            </ul>
        </div>

        <?= $form->field($model, 'login')->hiddenInput()->label(false) ?>
        <h4>Nhập mật khẩu</h4>
        <?= $form->field(
            $model,
            'password',
            [
                'options' => ['class' => 'input'],
                'inputOptions'   => ['class' => 'form-control'],
                'template' => '<div class="input-cod">{input}</div>{hint}',
            ]
        )->passwordInput();
        ?>

        <?= Html::button('Đăng nhập', ['class' => 'btn btn-active-otp', 'id' => 'btn_submit_otp', 'data-loading-text' => "<i class='fa fa-spinner fa-spin '></i> Đang xử lý"]); ?>

        <h4>Hoặc sử dụng Facebook đã kết nối</h4>
        <a class="facebook auth-link button-facebook"
            href="<?= Url::to(['/user/security/auth', 'authclient' => 'facebook']) ?>"
            title="Facebook" data-popup-width="860" data-popup-height="480"><i
            class="icon-facebook"></i>
            Đăng nhập bằng Facebook
        </a>
        <?php
        $redirectUrl = Yii::$app->user->isGuest ? Url::toRoute(['/']) : Url::toRoute(['/user/course/index']);
        $script = "
        (function($) {
            $(document).ready(function(){                           
                $('#btn_submit_otp').click(function(e){
                    var that = $(this);
                    that.button('loading');
                    var form = $('#active-otp-form');
                    var url = form.attr('action');
                    $.post(url, form.serialize(), function (res) {
                        that.button('reset');
                        form.parent().html(res);
                    }).error(function(){
                        that.button('reset');
                    });
                });                             
            });
        })(jQuery);
        ";
        $this->registerJs($script);
        ?>

    </div>
<?php ActiveForm::end(); ?>

<?php
$css = "
        .modal.modal-activeCOD .modal-dialog {
            margin: 80px auto 10px !important;
        }
        .otp-form button {
            width: 100%;
        }
        .otp-form .modal-body > h4 {
            font-size: 14px;
            margin-top: 15px;
            margin-bottom: 10px;
        }
        .otp-form .modal-body > h3 {           
            margin-bottom: 20px;
            text-align: center;
        }
        .otp-form .otp-summary {
            position: relative;
            padding: 8px 0 8px 40px;
            color: rgb(80, 172, 77);
            border: 1px solid rgb(80, 172, 77);
            margin-top: 20px;
            margin-bottom: 15px;
        }
        .otp-form .otp-summary ul {
            margin-bottom: 0;
            text-align: left !important;
        }
        .otp-form .otp-error-summary {
            position: relative;
            padding: 8px !important;
            color: rgb(190, 82, 83) !important;
            border-color: rgb(190, 82, 83) !important;
        }
        .otp-form .otp-error-summary ul {
            text-align: left !important;
        }
        .otp-form .otp-error-summary p {
            display: none;
        }      
        .otp-form .fa-question-circle {
            font-size: 18px;
            color: #777777;
            position: relative;
            top: 1px;
            left: 5px;
            cursor: pointer;
        }            
        .otp-form .otp-info {
            color: rgb(190, 82, 83);
        }             
        .otp-form .otp-header {
            margin-top: 15px;
        }
        .otp-form .btn-active-otp {
            width: 100%;
            padding: .375rem 1rem;
            margin-top: 12px;
            background: #50ad4e;
            color: white;
            border-bottom: 2px solid #489b46;          
            display: inline-block;
            border-radius: 3px;
            transition: background-color 100ms ease-in-out, color 100ms ease-in-out, border-color 100ms ease-in-out;
        }
        .otp-form .error-report {
            margin-top: 20px !important;
        }
        .otp-form .help-block {
            display: none;
        }       
        .otp-form .button-facebook {
            background: #3b5998;
            color: white;
            border-bottom: 2px solid #344e86;
            border-top: none;
            border-right: none;
            border-left: none;
            padding: 10px 35px;
            display: inline-block;
            border-radius: 3px;
            transition: background-color 100ms ease-in-out, color 100ms ease-in-out, border-color 100ms ease-in-out;
            height: 35px;
            width: 100%;
            max-width: 320px;
            line-height: 35px;
            padding: 0 !important;
            text-align: center;
        }
        .modal.modal-activeCOD .modal-dialog .modal-content .modal-body .input-cod .form-control {
            padding: 0.375rem 0.75rem;
        }
        .otp-form .error-report.form-error-summary {
            padding: 8px 0 8px 40px !important;
        }
        .otp-form .error-report.form-error-summary:before {
            content: \"\";
            position: absolute;
            width: 24px;
            height: 24px;
            top: calc(50% - 12.5px);
            left: 8px;
            background-image: url({$cdnUrl}/img/otp/icon-error-otp.png);
        }    
    ";
$this->registerCss($css);
?>