<?php

use yii\helpers\Url;
use yii\web\View;
use yii\bootstrap\ActiveForm;
?>
<ul class="menu-login-register nav sub-menu hidden-md-up">
    <li><a href="<?= Url::toRoute(['/user/security/login'])?>">Đăng nhập</a></li>
    <li class="active-tab"><a href="#"  data-toggle="tab" >Đăng ký</a></li>
    <div class="clearfix"></div>
</ul>
<div id="wrap-register-form-mb" class="k-popup-account">
    <div class="modal-dialog" role="document">
        <div class="modal-content connect-fb">
            <div class="left">
                <div class="inner">
                    <ul class="k-popup-account-top">
                        <li>
                            <img src="http://graph.facebook.com/<?= $account->client_id ?>/picture?type=large&amp;width=80&amp;height=80" alt="<?= $model->name ?>" class="fb-avatar">
                            <h5>Xin chào, <br /><?= $model->name ?></h5>
                        </li>
                        <li>Để hoàn tất kết nối với Kyna.vn,<br> bạn cần điền các thông tin sau</li>
                    </ul>
                </div>
            </div>
            <!-- end .left -->
            <div class="right">
                <div class="modal-header">
                    <button type="button" class="k-popup-account-close close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">Kết nối Facebook với Kyna</h4>
                </div>
                <div class="modal-body clearfix">
                    <?php
                    $form = ActiveForm::begin([
                        'id' => 'facebook-register-form',
                        'enableAjaxValidation' => true,
                        'action' => ['/user/registration/connect', 'code' => Yii::$app->request->get('code')]
                    ])
                    ?>
                    <?= $form->errorSummary($model, ['class' => 'error-summary alert alert-warning']) ?>

                    <?=
                    $form->field($model, 'email', [
                        'template' => '{beginWrapper}<span class="icon icon-mail"></span>{input}' . PHP_EOL . '{error}{endWrapper}',
                        'inputOptions' => [
                            'placeholder' => 'Email của bạn'
                        ]
                    ])->input('email')->label(false)->error(false);
                    ?>

                    <?=
                    $form->field($model, 'password', [
                        'template' => '{beginWrapper}<span class="icon icon-lock"></span>{input}' . PHP_EOL . '{error}{endWrapper}',
                        'inputOptions' => [
                            'placeholder' => 'Mật khẩu'
                        ]
                    ])->passwordInput()->label(false)->error(false);
                    ?>

                    <?=
                    $form->field($model, 'name', [
                        'template' => '{beginWrapper}<span class="icon icon-user"></span>{input}' . PHP_EOL . '{error}{endWrapper}',
                        'inputOptions' => [
                            'placeholder' => 'Họ tên'
                        ]
                    ])->textInput()->label(false)->error(false);
                    ?>

                    <?=
                    $form->field($model, 'phonenumber', [
                        'template' => '{beginWrapper}<span class="icon icon-call"></span>{input}' . PHP_EOL . '{error}{endWrapper}',
                        'inputOptions' => [
                            'placeholder' => 'Số điện thoại'
                        ]
                    ])->textInput()->label(false)->error(false);
                    ?>

                    <div class="button-submit">
                        <button type="submit" class="btn btn-default background-green hover-bg-green">Kết nối</button>
                    </div><!--end .button-popup-->

                    <?php ActiveForm::end() ?>

                    <ul class="k-popup-account-bottom hidden-md-down">
                        <li>Nếu đã có tài khoản</li>
                        <li><a href="<?= Url::toRoute(['/user/security/login']) ?>" data-target="#k-popup-account-login" data-toggle="modal" data-ajax data-push-state="false">Đăng nhập</a></li>
                    </ul>
                </div>
                <!--end .modal-body-->
            </div>
        </div>
    </div>
</div>
<?php
$css = "
       .popup-register {
            background-color: transparent;
       }

       .k-popup-account-top .fb-avatar {
           width: 80px;
           height: 80px;
           -webkit-border-radius: 50%;
           -moz-border-radius: 50%;
           -ms-border-radius: 50%;
           -o-border-radius: 50%;
           border-radius: 50%;
           margin-bottom: .75em;
       }
       .k-popup-account-top h5 {
           margin-bottom: 1em;
       }
       #wrap-register-form-mb {
           margin-top: 50px;
       }
    ";

$script = "
    ;(function($, window, document, undefined){
        $(document).ready(function(){
            if (window.location.pathname && window.location.pathname.indexOf('ket-noi') > -1) {
              var content = $('#wrap-register-form-mb');
              $('#k-popup-account-register').append(content);
              $('#k-popup-account-register').modal();

              $('#k-popup-account-register').on('hidden.bs.modal', function() {
                  window.location.href = '/';
              });
            }

            $('#k-popup-account-register').on('hidden.bs.modal', function() {
                $('#facebook-register-form .error-summary').hide();
                $('#user-login').val('');
                $('#user-password').val('');
            });
        });
    })(window.jQuery || window.Zepto, window, document);";

$this->registerCss($css);
$this->registerJs($script, View::POS_END, 'login-submit');
?>
