<?php

use yii\helpers\Url;
use yii\web\View;
use yii\bootstrap\ActiveForm;

?>
<ul class="menu-login-register nav sub-menu hidden-md-up">
    <li><a href="<?= Url::toRoute(['/user/security/login']) ?>">Đăng nhập</a></li>
    <li class="active-tab"><a href="#" data-toggle="tab">Đăng ký</a></li>
    <div class="clearfix"></div>
</ul>
<div id="wrap-register-form-mb" class="k-popup-account">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="left">
                <div class="inner">
                    <h4>Bạn đã có <br>tài khoản Kyna?</h4>
                    <p>Kyna.vn, hệ sinh thái giáo dục trực tuyến hàng đầu Việt Nam</p>
                    <a href="/dang-nhap" data-toggle="modal" data-target="#k-popup-account-login" data-ajax=""
                       data-push-state="false">Đăng nhập</a>
                </div>
            </div>
            <!-- end .left -->
            <div class="right">
                <div class="modal-header">
                    <button type="button" class="k-popup-account-close close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">Đăng ký</h4>
                </div>
                <div class="modal-body clearfix">
                    <ul class="k-popup-account-top">
                        <?php if (!$model->fbId) : ?>
                            <li>
                                <a class="facebook auth-link button-facebook"
                                   href="<?= Url::to(['/user/security/auth', 'authclient' => 'facebook']) ?>"
                                   title="Facebook" data-popup-width="860" data-popup-height="480"><i
                                            class="icon-facebook"></i> Đăng nhập bằng Facebook</a>
                            </li>
                            <li>- Hoặc đăng ký tài khoản Kyna -</li>
                        <?php else : ?>
                            <li>
                                <img src="http://graph.facebook.com/<?= $model->fbId ?>/picture?type=large&amp;width=80&amp;height=80"
                                     alt="<?= $model->name ?>" class="fb-avatar">
                                <h5><?= $model->name ?></h5>
                            </li>
                            <li>Để hoàn tất đăng ký trên Kyna.vn,<br> bạn cần điền các thông tin sau</li>
                        <?php endif; ?>
                    </ul>

                    <?php
                    $form = ActiveForm::begin([
                        'id' => 'facebook-register-form',
                        'action' => ['/user/registration/register']
                    ])
                    ?>
                    <?= $form->errorSummary($model, ['class' => 'error-summary alert alert-warning']) ?>

                    <?= $form->field($model, 'fbId')->hiddenInput(['id' => 'register-form-fbid'])->label(false) ?>

                    <?=
                    $form->field($model, 'email', [
                        'template' => '{beginWrapper}<span class="icon icon-mail"></span>{input}' . PHP_EOL . '{error}{endWrapper}',
                        'inputOptions' => [
                            'placeholder' => 'Email của bạn'
                        ]
                    ])->input('email')->label(false)->error(false);
                    ?>

                    <?=
                    $form->field($model, 'password', [
                        'template' => '{beginWrapper}<span class="icon icon-lock"></span>{input}' . PHP_EOL . '{error}{endWrapper}',
                        'inputOptions' => [
                            'placeholder' => 'Mật khẩu'
                        ]
                    ])->passwordInput()->label(false)->error(false);
                    ?>

                    <?=
                    $form->field($model, 'name', [
                        'template' => '{beginWrapper}<span class="icon icon-user"></span>{input}' . PHP_EOL . '{error}{endWrapper}',
                        'inputOptions' => [
                            'placeholder' => 'Họ tên'
                        ]
                    ])->textInput()->label(false)->error(false);
                    ?>

                    <?=
                    $form->field($model, 'phonenumber', [
                        'template' => '{beginWrapper}<span class="icon icon-call"></span>{input}' . PHP_EOL . '{error}{endWrapper}',
                        'inputOptions' => [
                            'placeholder' => 'Số điện thoại'
                        ]
                    ])->textInput()->label(false)->error(false);
                    ?>

                    <div class="button-submit">
                        <button type="submit" id="register-user"
                                class="btn btn-default background-green hover-bg-green">Đăng ký
                        </button>
                    </div><!--end .button-popup-->

                    <?php ActiveForm::end() ?>

                    <div class="fb-login-pc">
                        <p>- hoặc - </p>
                        <a class="facebook auth-link button-facebook"
                           href="<?= Url::to(['/user/security/auth', 'authclient' => 'facebook']) ?>" title="Facebook"
                           data-popup-width="860" data-popup-height="480"><i class="icon-facebook"></i> Đăng nhập bằng
                            Facebook</a>
                    </div>

                    <ul class="k-popup-account-bottom hidden-md-down">
                        <li>Nếu đã có tài khoản</li>
                        <li><a href="<?= Url::toRoute(['/user/security/login']) ?>" data-target="#k-popup-account-login"
                               data-toggle="modal" data-ajax data-push-state="false">Đăng nhập</a></li>
                    </ul>
                </div>
                <!--end .modal-body-->
            </div>
        </div>
    </div>
</div>
<?php
$css = "
       .popup-register {
            background-color: transparent;
       }

       .k-popup-account-top .fb-avatar {
           width: 80px;
           height: 80px;
           -webkit-border-radius: 50%;
           -moz-border-radius: 50%;
           -ms-border-radius: 50%;
           -o-border-radius: 50%;
           border-radius: 50%;
           margin-bottom: .75em;
       }
       .k-popup-account-top h5 {
           margin-bottom: 1em;
       }
       #wrap-register-form-mb {
           margin-top: 50px;
       }
    ";

$script = "
    ;(function($, window, document, undefined){
        $(document).ready(function(){
            $('#register-user').click( function(event) {
            
                event.preventDefault();
                var data = $('#facebook-register-form').serializeArray();
                $('#register-user').attr('disabled', 'disabled');
                data.push({name: 'currentUrl', value: window.location.href})
    
                $.post('/user/registration/register', data, function (response) {
                    if (response.result) {
                        // login success
                        if (response.redirectUrl) {
                            window.location.replace(response.redirectUrl);
                        } else {
                            window.location.reload();
                        }
                    } else {
                        $('#register-user').removeAttr('disabled');
                        // login failed
                        var liHtml = '';
                        $.each(response.errors, function (key, value){
                            liHtml += '<li>' + value[0] + '</li>';
                        });
    
                        $('#facebook-register-form .error-summary ul').html(liHtml);
                        $('#facebook-register-form .error-summary').show();
                    }
                    
                });
            });
        
            if (window.location.pathname && window.location.pathname.indexOf('dang-ky') > -1) {
              var content = $('#wrap-register-form-mb');
              $('#k-popup-account-register').append(content);
              $('#k-popup-account-register').modal();
              $('a[data-target=\"#k-popup-account-login\"]').removeAttr('data-toggle').removeAttr('data-ajax');

              $('#k-popup-account-register').on('hidden.bs.modal', function() {
                  window.location.href = '/';
              });
            }

            $('#k-popup-account-register').on('hidden.bs.modal', function() {
                $('#facebook-register-form .error-summary').hide();
                $('#user-login').val('');
                $('#user-password').val('');
            });

            if ($('#register-form-fbid').val().length > 0) {
                var fbReg = $('#k-popup-account-register');
                fbReg.find('.k-popup-fb-login').css('display', 'none');
                var tmp = fbReg.find('.k-popup-account-top');
                tmp.find('h5').prepend('Xin chào, <br />');
                var left = fbReg.find('.left').find('.inner');
                left.empty();
                left.append(tmp);
                fbReg.find('.right').find('h4').html('Đăng ký <br />bằng facebook');
            }
        });
    })(window.jQuery || window.Zepto, window, document);";

$this->registerCss($css);
$this->registerJs($script, View::POS_END, 'login-submit');
?>
