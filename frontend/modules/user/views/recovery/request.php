<?php

use yii\web\View;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use common\widgets\Alert;
use common\helpers\CDNHelper;

$cdnUrl = CDNHelper::getMediaLink();

$this->title = 'Quên mật khẩu';
$this->params['breadcrumbs'][] = $this->title;
?>
<?= ($flag === TRUE)?'<div id="k-recover-password">':''?>
  <div class="modal-dialog" role="document">
    <div class="modal-content k-popup-account-mb-content">
        <div class="left">
          <div class="inner">
            <h4>Bạn chưa có <br>tài khoản Kyna?</h4>
            <p>Kyna.vn, hệ sinh thái giáo dục trực tuyến hàng đầu Việt Nam</p>
            <a href="/dang-ky" data-toggle="modal" data-target="#k-popup-account-register" data-ajax="" data-push-state="false">Đăng ký</a>
          </div>
        </div>
        <!-- end .left -->
        <div class="right">
          <div class="modal-header">
            <button type="button" class="k-popup-account-close close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
            <h4 class="modal-title">Lấy lại mật khẩu</h4>
          </div>
          <div class="modal-body clearfix">
            <p>
              Để lấy lại mật khẩu, bạn nhập email đăng nhập vào ô dưới đây. Sau đó Kyna.vn sẽ gửi email hướng dẫn bạn khôi phục mật khẩu
            </p>
            <?php
            $form = ActiveForm::begin([
              'id' => 'password-recovery-form',
              'enableAjaxValidation' => false,
              'enableClientValidation' => false,
              'action' => Url::toRoute(['/user/recovery/request']),
              'options' => [
                'data-ajax' => ($flag === TRUE)?false:true,
                'data-target' => ($flag === TRUE)?'#k-recover-password':'#k-popup-account-reset'
              ]
            ]);
            ?>
            <?= Alert::widget() ?>

            <?= $form->errorSummary($model, ['class' => 'error-summary alert alert-warning']) ?>

            <?=
            $form->field($model, 'email', [
              'template' => '{beginWrapper}<span class="icon icon-mail"></span>{input}' . PHP_EOL . '{error}{endWrapper}',
              'options' => ['class' => 'form-group'],
              'inputOptions' => [
                'placeholder' => 'Email của bạn'
              ]
              ])->textInput(['autofocus' => true])->error(false)
              ?>
              <div class="button-submit">
                <?= Html::submitButton(Yii::t('user', 'Continue'), ['id' => 'btn-submit-forgot']) ?><br>
              </div>
              <?php ActiveForm::end(); ?>

              <a href="/dang-nhap" data-toggle="modal" data-target="#k-popup-account-login" class="back-login-pc">
                <img src="<?= $cdnUrl ?>/img/icon/icon-long-arrow-left.png" alt="">
                Quay lại đăng nhập
              </a>

              <a href="<?= Url::toRoute(['/user/security/login']) ?>" data-target="#k-popup-account-login" data-toggle="modal" data-ajax data-push-state="false" class="back-login">Quay lại đăng nhập</a>

              <ul class="k-popup-account-bottom">
                <li>Nếu bạn chưa có tài khoản</li>
                <li><a href="<?= Url::toRoute(['/user/registration/register']) ?>" data-target="#k-popup-account-register" data-toggle="modal" data-ajax data-push-state="false">Đăng ký</a></li>
              </ul>
            </div><!--end modal-body-->
        </div>
        <!-- end right -->
      </div>
      <!-- end .k-popup-account-mb-content -->
  </div>
<?= ($flag === TRUE)?'</div>':''?>
<?php $script = "
;(function($, window, document, undefined){
    // $('a[data-target=\"#k-popup-account-register\"]').click(function(){
    //     window.location.href = '/dang-ky';
    // })
    $(document).ready(function(){
        if (window.location.pathname && window.location.pathname.indexOf('quen-mat-khau') > -1) {
            var content = $('#k-recover-password');
            $('#k-popup-account-reset').append(content);
            $('#k-popup-account-reset').modal();
            $('a[data-target=\"#k-popup-account-register\"], a[data-target=\"#k-popup-account-login\"]').removeAttr('data-toggle').removeAttr('data-ajax');
            $('#k-popup-account-login').on('hidden.bs.modal', function() {
                window.location.href = '/';
            })
        }
    });
})(window.jQuery || window.Zepto, window, document);";
?>
<?php
$this->registerJs($script, View::POS_END, 'login-submit');
?>
