<?php

use yii\helpers\Url;
use yii\web\View;
use kyna\promotion\models\UserVoucherFree;
$cId = $this->context->id;

?>
<div id="menu-courses-mob">
<!--    <div class="button-group-tool hidden-md-up">-->
<!--        <a class="btn btn-cod-active" href="--><?//= Url::toRoute(['/user/order/active-cod']) ?><!--" data-toggle="popup" data-target="#activeCOD" data-ajax data-push-state=false>Kích hoạt COD</a>-->
<!--        <a class="btn btn-edit-info-user" href="--><?//= Url::toRoute(['/user/profile/edit']); ?><!--" title="Chỉnh sửa thông tin">Sửa thông tin tài khoản</a>-->
<!--    </div>-->
    <div id="scroll-tab-menu-course">
        <ul class="nav nav-tabs container hidden-md-up" id="mobile-course-menu">
            <?php if($cId != 'transaction') { ?>
            <li id="courses-tab" <?php if ($cId == 'course') {
                echo 'class="active-tab"';
            } ?>>
                <a href="<?= Url::toRoute(['/user/course/index', 'is_started' => \kyna\user\models\UserCourse::BOOL_NO]) ?>#courses-list" data-ajax data-target="#mycourses-main">
                    Bài học
                </a>
            </li>
            <li id="document-tab" <?php if ($cId == 'document') {
                echo 'class="active-tab"';
            } ?>>
                <a href="<?= Url::toRoute(['/user/document/index']) ?>#document" data-ajax data-target="#mycourses-main">
                    Tài liệu
                </a>
            </li>
            <li id="question-tab" <?php if ($cId == 'question') {
                echo 'class="active-tab"';
            } ?>>
                <a href="<?= Url::toRoute(['/user/question/index']) ?>#question-answer" data-ajax data-target="#mycourses-main">
                    Câu hỏi
                </a>
            </li>
            <?php }else{ ?>
            <li id="trade-tab" <?php if ($cId == 'transaction') {
                echo 'class="active-tab"';
            } ?>>
                <a href="<?= Url::toRoute(['/user/transaction/index']) ?>#transaction" data-ajax data-target="#mycourses-main">
                    Giao dịch
                </a>
            </li>
            <?php } ?>
            <li id="summary-tab" <?php if ($cId == 'profile') {
                echo 'class="active-tab"';
            } ?>>
                <a href="<?= Url::toRoute(['/user/profile/profile']) ?>">
                  Tài khoản
                </a>
            </li>
            <li id="history-kpoint" <?php if ($cId == 'point') {
                echo 'class="active-tab"';
            } ?>>
                <a href="<?= Url::toRoute(['/user/point/index']) ?>">
                  Điểm Kpoint
                </a>
            </li>
            <li id="history-voucher" <?php if ($cId == 'gift') {
                echo 'class="active-tab"';
            } ?>>
                <a href="<?= Url::toRoute(['/user/gift/index']) ?>">
                  Voucher
                </a>
            </li>
<!--            --><?php //if (UserVoucherFree::isDateRunCampaignVoucherFree() == true): ?>
<!--                <li id="btn-voucher-free">-->
<!--                    <a href="#" data-toggle="modal" data-target="#modalvoucher">Lấy mã của bạn</a>-->
<!--                </li>-->
<!--            --><?php //endif;?>
        </ul>
    </div>
</div>
<script type="text/javascript">
    $("body")
        .on("ajax.updated", "#mobile-course-menu li", function(e){
            $("#mobile-course-menu li").removeClass("active-tab");
            $(e.currentTarget).addClass("active-tab");
            setHeightDiv();
        });
</script>


<style>
    #btn-voucher-free a{
        background-image: -moz-linear-gradient( 90deg, rgb(255,216,0) 0%, rgb(246,255,2) 100%);
        background-image: -webkit-linear-gradient( 90deg, rgb(255,216,0) 0%, rgb(246,255,2) 100%);
        background-image: -ms-linear-gradient( 90deg, rgb(255,216,0) 0%, rgb(246,255,2) 100%);
        box-shadow: 0px 4px 5px 0px rgba(142, 62, 24, 0.14);
        border-radius: 30px;
        color: #f91808;
        font-weight: 700;
        padding: 12px 40px 8px;
        position: relative;
        bottom: 5px;
    }
    #btn-voucher-free:hover{
        background-color: transparent !important;
        border: none !important;
        opacity: 0.8;
    }
    @media screen and (max-width: 425px){
        #scroll-tab-menu-course{

        }
        #btn-voucher-free{
            position: absolute;
            top: -60px;
            left: 38%;
            transform: translateX(-50%);

        }
    }
    @media screen and (max-width: 375px){

        #btn-voucher-free{
            left: 34%;
            transform: translateX(-50%);

        }
    }
    @media screen and (max-width: 325px){

        #btn-voucher-free{
            left: 29%;
            transform: translateX(-50%);

        }
    }
</style>
