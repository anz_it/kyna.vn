<?php

use frontend\modules\user\assets\UserAsset;
use yii\helpers\Html;
use frontend\widgets\HeaderWidget;
use frontend\widgets\FooterWidget;
use frontend\widgets\GaWidget;

\frontend\assets\AppAsset::register($this);

$this->beginPage();

$flag = false;
if(Yii::$app->devicedetect->isMobile() || Yii::$app->devicedetect->isTablet()){
    $flag = true;
}
$cId = $this->context->id;
?>
<!DOCTYPE HTML>
<html lang="<?= Yii::$app->language ?>">
<head>
    <?= $this->render('@app/views/layouts/common/html_head') ?>
</head>
<body class="page-teacher">
    <?php $this->beginBody()?>
       <?php echo Yii::$app->settings->bodyScript ?>

        <?= HeaderWidget::widget(['rootCats' => $this->context->rootCats]); ?>

        <main>
            <?= $this->render("@app/modules/user/views/layouts/_teacher_info", ['flag' => $flag]); ?>
            <div class="container">
                <?= $content; ?>
            </div>
        </main>

        <?= FooterWidget::widget(); ?>
    <?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
