<?php

use frontend\modules\user\assets\UserAsset;
use yii\helpers\Html;
use frontend\widgets\HeaderWidget;
use frontend\widgets\FooterWidget;
use frontend\widgets\GaWidget;
use frontend\widgets\PopupWidget;

\frontend\assets\AppAsset::register($this);

$this->beginPage();

$flag = false;
if(Yii::$app->devicedetect->isMobile() || Yii::$app->devicedetect->isTablet()){
    $flag = true;
}
$cdnUrl = \common\helpers\CDNHelper::getMediaLink();
?>
<!DOCTYPE HTML>
<html lang="<?= Yii::$app->language ?>">
<head>
    <?= $this->render('@app/views/layouts/common/html_head') ?>
</head>
<body>
    <?php $this->beginBody()?>
    <?php echo Yii::$app->settings->bodyScript ?>

        <?= HeaderWidget::widget(['rootCats' => $this->context->rootCats]); ?>

        <main>
            <section class="body-courses-section">
                <?= $this->render('@app/modules/user/views/layouts/_nav_mobile', ['flag' => $flag]); ?>
                <div class="container">
                    <!-- Tab panes -->
                    <div class="tab-content clearfix col-xs-12" id="mycourses-main">
                        <?= $content; ?>
                    </div>
                </div>
            </section>
        </main>

    <div class="modal fade" id="popup-lesson">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="popup_body">
                        <a href="https://kyna.vn/p/nhom-khoa-hoc/reading-eggs-ung-dung-hoc-tieng-anh-hang-dau-cho-tre/405782" target="_blank" title=""><img src="<?= $cdnUrl ?>/img/lesson/20180124-Banner-readingeggs.png" alt=""></a>
                        <img class="closePopup" data-dismiss="modal" src="<?= $cdnUrl ?>/img/lesson/btn-close.png" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>

    <style>
        .fancybox-skin{
            padding: 0 !important;
        }
        .fancybox-close{
            background-image: url('<?=$cdnUrl?>/src/img/home/close-button.png') !important;
            background-size: 100% 100%;
            background-repeat: no-repeat;
            width: 28px;
            height: 28px;
        }
    </style>
    <?= PopupWidget::widget([
        'position' => PopupWidget::POSITION_MY_COURSE,
        'course_id' => Yii::$app->request->get('id')
    ]) ?>
        <?= FooterWidget::widget(); ?>
    <?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
