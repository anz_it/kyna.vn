<?php

use yii\helpers\Url;
use yii\web\View;
use kyna\promotion\models\UserVoucherFree;
$cId = $this->context->id;
?>

<ul class="nav nav-tabs container hidden-sm-down" id="menu-courses">
    <li id="courses-tab" <?php
if ($cId == 'course') {
    echo 'class="active-tab"';
}
?>>
        <a href="<?= Url::toRoute(['/user/course/index']) ?>" data-target="#mycourses-main">
            Khóa học
        </a>
    </li>
    <li id="document-tab" <?php
        if ($cId == 'document') {
            echo 'class="active-tab"';
        }
        ?>>
        <a href="<?= Url::toRoute(['/user/document/index']) ?>" data-target="#mycourses-main" >
            Tài liệu
        </a>
    </li>
    <li id="question-tab" <?php
        if ($cId == 'question') {
            echo 'class="active-tab"';
        }
        ?>>
        <a href="<?= Url::toRoute(['/user/question/index']) ?>" data-target="#mycourses-main">
            Hỏi giảng viên
        </a>
    </li>
    <li id="discuss-tab" <?php
    if ($cId == 'discuss') {
        echo 'class="active-tab"';
    }
        ?>>
        <a href="<?= Url::toRoute(['/user/discuss/index']) ?>" data-target="#mycourses-main">
            Thảo luận
        </a>
    </li>
    <!--        <li id="messenger-tab">-->
    <!--            <a href="messenger_courses.html">-->
    <!--                Tin nhắn-->
    <!--            </a>-->
    <!--        </li>-->
    <li id="point-tab" <?php
    if ($cId == 'point') {
        echo 'class="active-tab"';
    }
    ?>>
        <a href="<?= Url::toRoute(['/user/point/index']) ?>" data-target="#mycourses-main">
            Điểm Kpoint
        </a>
    </li>
    <li id="code-tab" <?php
    if ($cId == 'gift') {
        echo 'class="active-tab"';
    }
    ?>>
        <a href="<?= Url::toRoute(['/user/gift/index']) ?>" data-target="#mycourses-main">
            Voucher
        </a>
    </li>
    <li id="trade-tab" <?php
    if ($cId == 'transaction') {
        echo 'class="active-tab"';
    }
        ?>>
        <a href="<?= Url::toRoute(['/user/transaction/in-complete']) ?>" data-target="#mycourses-main">
            Giao dịch
        </a>
    </li>
<!--    <li class="deltaxu">-->
<!--        <a href="#" data-toggle="modal" data-target="#popup-thong-bao-deltaxu"  ><img src="--><?php //echo Yii::$app->getUrlManager()->getBaseUrl() ?><!--/src/img/course/chuong.png"  />Thông báo về DeltaXu</a>-->
<!--    </li>-->

<!--    --><?php //if (UserVoucherFree::isDateRunCampaignVoucherFree() == true): ?>
<!--        <li id="btn-voucher-free">-->
<!--            <a href="#" data-toggle="modal" data-target="#modalvoucher">Lấy mã của bạn</a>-->
<!--        </li>-->
<!--    --><?php //endif;?>

</ul>

<!-- Fixed navbar -->
<script type="text/javascript">
    $("body")
            .on("ajax.updated", "#menu-courses li", function (e) {
                $("#menu-courses li").removeClass("active-tab");
                $(e.currentTarget).addClass("active-tab");
                setHeightDiv();
                CoursesAction.Init();
                CoursesAction.ResizeBoxProduct("#mycourses-buy");
            });
</script>


<style>
    #btn-voucher-free a{
        background-image: -moz-linear-gradient( 90deg, rgb(255,216,0) 0%, rgb(246,255,2) 100%);
        background-image: -webkit-linear-gradient( 90deg, rgb(255,216,0) 0%, rgb(246,255,2) 100%);
        background-image: -ms-linear-gradient( 90deg, rgb(255,216,0) 0%, rgb(246,255,2) 100%);
        box-shadow: 0px 4px 5px 0px rgba(142, 62, 24, 0.14);
        border-radius: 30px;
        color: #f91808;
        font-weight: 700;
        padding: 12px 40px 8px;
        position: relative;
        bottom: 5px;
    }
    #btn-voucher-free:hover{
        background-color: transparent !important;
        border: none !important;
        opacity: 0.8;
    }

</style>