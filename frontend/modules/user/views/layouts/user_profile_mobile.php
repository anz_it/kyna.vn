<?php

use frontend\modules\user\assets\UserAsset;
use yii\helpers\Html;
use frontend\widgets\HeaderWidget;
use frontend\widgets\FooterWidget;
use frontend\widgets\GaWidget;

\frontend\assets\AppAsset::register($this);

$this->beginPage();

$flag = false;
if(Yii::$app->devicedetect->isMobile() || Yii::$app->devicedetect->isTablet()){
    $flag = true;
}

?>
<!DOCTYPE HTML>
<html lang="<?= Yii::$app->language ?>">
<head>
    <?= $this->render('@app/views/layouts/common/html_head') ?>
</head>
<body>
    <?php $this->beginBody()?>
    <?php echo Yii::$app->settings->bodyScript ?>

        <?= HeaderWidget::widget(['rootCats' => $this->context->rootCats]); ?>

        <main>
            <section id="k-courses-header" class="k-height-header">
                <div class="container">
                    <!-- Tab panes -->
                    <?= $content; ?>
                </div>
            </section>
        </main>

        <?= FooterWidget::widget(); ?>
    <?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
