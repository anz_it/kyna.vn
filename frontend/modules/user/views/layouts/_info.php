<?php
use yii\helpers\Url;
use common\helpers\CDNHelper;

$user = Yii::$app->user->identity;
?>
<section id="k-courses-header" class="k-height-header hidden-sm-down">


    <div class="container">

        <?php if (Yii::$app->session->hasFlash('error')): ?>
            <?= Yii::$app->session->getFlash('error') ?>
        <?php endif; ?>

        <header>
            <?= CDNHelper::image($user->avatarImage, [
                'alt' => $user->profile->name,
                'class' => 'img-fluid',
                'size' => CDNHelper::IMG_SIZE_AVATAR_LARGE,
                'resizeMode' => 'crop',
            ]) ?>
            <h2>
                <span class="name"><?= $user->profile->name; ?></span>&nbsp;<span class="email">/ <?= $user->email; ?></span>
            </h2>
            <ul>
                <li data-activates="slide-out">
                    <a href="<?= Url::toRoute(['/user/profile/edit']); ?>" title="Chỉnh sửa thông tin">
                        Chỉnh sửa
                    </a>
                </li>

                <li id="k-courses-header-btn-active">
                    <a href="<?= Url::toRoute(['/user/order/active-cod']) ?>" data-toggle="popup" data-target="#activeCOD" data-ajax data-push-state=false>
                        Kích hoạt mã COD
                    </a>
                </li>
            </ul>
        </header>
        <section class="k-courses-header-list">
            <div class="course-summary">
                <ul>
                    <li>
                        <p>Số khóa học</p>
                        <div class="img-courses img-count-courses"></div>
                        <span class="course-number">0</span>
                    </li>
                    <li>
                        <p>Tài liệu</p>
                        <div class="img-courses img-document-courses"></div>
                        <span class="document-number">0</span>
                    </li>
                    <li>
                        <p>Câu hỏi</p>
                        <div class="img-courses img-question-courses"></div>
                        <span class="question-number">0</span>
                    </li>
                    <li>
                        <p>Kpoint</p>
                        <div class="img-courses img-kpoint-courses"></div>
                        <span class="kpoint-number">0</span>
                    </li>
                </ul>
            </div>
        </section>
    </div><!--end .container-->
</section>

<script type="text/javascript">
    $(document).ready(function () {
        $.ajax({
            url: '<?php echo Url::toRoute(["course/analytic"]) ?>',
            method: 'GET',
            type: 'json',
            success: function (resp) {
                var res = JSON.parse(resp);

                $('.course-number').empty();

                $('.course-number').append(res.object.course_count);

                $('.question-number').empty();

                $('.question-number').append(res.object.question_count);

                $('.document-number').empty();

                $('.document-number').append(res.object.document_count);

                $('.kpoint-number').empty();

                $('.kpoint-number').append(res.object.k_point);
            },
            error: function (err) {
                console.log(err);
            }
        });
    });
</script>
