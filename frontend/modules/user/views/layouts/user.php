<?php

use frontend\modules\user\assets\UserAsset;
use yii\helpers\Html;
use frontend\widgets\HeaderWidget;
use frontend\widgets\FooterWidget;
use frontend\widgets\GaWidget;
use frontend\widgets\PopupWidget;
\frontend\assets\AppAsset::register($this);

$this->beginPage();

$flag = false;
if(Yii::$app->devicedetect->isMobile() || Yii::$app->devicedetect->isTablet()){
    $flag = true;
}
$cId = $this->context->id;

$cdnUrl = \common\helpers\CDNHelper::getMediaLink();
?>
<!DOCTYPE HTML>
<html lang="<?= Yii::$app->language ?>">
<head>
    <?= $this->render('@app/views/layouts/common/html_head') ?>
</head>
<body>
    <?php $this->beginBody()?>
       <?php echo Yii::$app->settings->bodyScript ?>

        <?= HeaderWidget::widget(['rootCats' => $this->context->rootCats]); ?>

        <main>
            <?php if(!Yii::$app->user->isGuest && $cId != 'profile') {?>
                <?= $this->render("@app/modules/user/views/layouts/_info", ['flag' => $flag]); ?>
            <?php } ?>
            <section class="body-courses-section">

                <!-- Em huân thêm-->
                <?= $this->render('@app/modules/user/views/layouts/_nav_mobile', ['flag' => $flag]); ?>
                <!-- Kết thúc thêm-->
                <div class="container">

                    <?= $this->render('@app/modules/user/views/layouts/_nav', ['flag' => $flag]); ?>

                    <div class="tab-content clearfix col-xs-12" id="mycourses-main">
                    <!-- Tab panes -->
                        <?= $content; ?>
                    </div>
                </div>
            </section>
        </main>
        <style>
            .fancybox-skin{
                padding: 0 !important;
            }
            .fancybox-close{
                background-image: url('<?=$cdnUrl?>/src/img/home/close-button.png') !important;
                background-size: 100% 100%;
                background-repeat: no-repeat;
                width: 28px;
                height: 28px;
            }
        </style>
        <?= PopupWidget::widget([
            'position' => PopupWidget::POSITION_MY_COURSE,
            'course_id' => Yii::$app->request->get('id')
        ]) ?>

        <?= FooterWidget::widget(); ?>
    <?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
