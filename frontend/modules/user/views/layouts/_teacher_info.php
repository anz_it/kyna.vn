<?php
use yii\helpers\Url;
use yii\web\View;
use common\helpers\CDNHelper;

$teacher = $this->params['teacher'];
?>
<section id="k-courses-header" class="k-height-header">
    <div class="container">
        <header>
            <?= CDNHelper::image($teacher->avatarImage, [
                'alt' => $teacher->profile->name . ', ' . $teacher->title,
                'title' => $teacher->profile->name,
                'class' => 'img-fluid',
                'size' => CDNHelper::IMG_SIZE_AVATAR_LARGE,
                'resizeMode' => 'crop',
            ]) ?>
            <h1 class="name"><?= $teacher->profile->name; ?></h1>
            <h2 class="email"><?= $teacher->title; ?></h2>
            <ul>
                <li>
                    <?php if (Yii::$app->user->isGuest): ?>
                        <a href="<?= Url::toRoute(['/user/security/login']) ?>" data-toggle="modal" data-target="#k-popup-account-login" data-ajax data-push-state=false>
                            Nhận tin mới của giảng viên
                        </a>
                    <?php else: ?>
                        <a href="<?= Url::toRoute(['/user/teacher/receive-notify', 'slug' => $teacher->slug]) ?>"
                           class="<?= $teacher->isUserRegister ? 'disabled' : '' ?>"
                           data-toggle="popup" data-target="#teacher_notify" data-ajax data-push-state=false data-disable-parent=true data-disable-parent-text="Bạn đã đăng kí nhận tin">
                            <?= $teacher->isUserRegister ? 'Bạn đã đăng kí nhận tin' : 'Nhận tin mới của giảng viên' ?>
                        </a>
                    <?php endif; ?>
                </li>
            </ul>
        </header>
        <section class="k-courses-header-list">
            <div class="course-summary">
                <ul>
                    <li>
                        <p>Số khóa học</p>
                        <div class="img-count-courses"></div>
                        <span class="course-number">0</span>
                    </li>
                    <li>
                        <p>Giờ giảng</p>
                        <div class="img-hour-courses"></div>
                        <span class="hour-number">0</span>
                    </li>
                    <li>
                        <p>Câu hỏi</p>
                        <div class="img-question-courses"></div>
                        <span class="question-number">0</span>
                    </li>
                </ul>
            </div>
        </section>
    </div><!--end .container-->
</section>

<?php
$analyticUrl = Url::toRoute(['teacher/analytic', 'slug' => $teacher->slug]);
$script = "
    $(document).ready(function () {
        $.ajax({
            url: '{$analyticUrl}',
            method: 'GET',
            type: 'json',
            success: function (resp) {
                var res = JSON.parse(resp);
                $('.course-number').text(res.object.course_count);
                $('.question-number').text(res.object.question_count);
                $('.hour-number').text(res.object.hour_count);
            }          
        });
    });
";
$this->registerJs($script, View::POS_READY, 'teacher-analytic');
?>
