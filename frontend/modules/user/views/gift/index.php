<?php

use yii\widgets\Pjax;
use yii\widgets\ListView;
use common\assets\ClipboardAsset;

/**
 * @var $searchModel \kyna\gamification\models\search\UserGiftSearch
 */
$this->title = 'Quà tặng của tôi';

ClipboardAsset::register($this);
?>


<div class="tab-content clearfix col-xs-12" id="promotion-history-main">
    <div class="promotion-tutorial"><a href="https://kyna.vn/p/kyna/cau-hoi-thuong-gap/huong-dan-su-dung-voucher-coupon"><i>Hướng dẫn dùng Voucher</i> <i class="fa fa-question-circle-o" style="font-size: 18px;position: relative; top: 1px;" aria-hidden="true"></i></a></div>
    <div class="promotion-history-list">
        <div class="list-promotion-title">
            QUÀ TẶNG CỦA TÔI
        </div>
        <?php Pjax::begin() ?>
            <?=
            ListView::widget([
                'dataProvider' => $searchModel->search(Yii::$app->request->queryParams),
                'layout' => "<div class='promotion-items'>{items}</div>\n<div class='pull-right'>{pager}</div>",
                'itemView' => '_item',
                'pager' => [
                    'prevPageLabel' => '<span>&laquo;</span>',
                    'nextPageLabel' => '<span>&raquo;</span>',
                    'options' => [
                        'class' => 'pagination',
                    ],
                ]
            ])
            ?>
        <?php Pjax::end()?>
    </div>

</div>
