<?php
/**
 * Created by PhpStorm.
 * User: macintosh
 * Date: 8/30/17
 * Time: 2:14 PM
 * @var $this \yii\web\View
 * @var $userPoint \kyna\gamification\models\UserPoint
 */

use yii\web\View;
use yii\widgets\Pjax;
use yii\widgets\ListView;
use common\helpers\CDNHelper;
use common\assets\ClipboardAsset;

$cdnUrl = CDNHelper::getMediaLink();
ClipboardAsset::register($this);
?>
<main>
    <section id="exchange-header" class="k-height-header">
        <div class="container">
            <img src="<?= $cdnUrl ?>/img/kpoint/kpoint-store-title.png" alt="">
            <div class="btn-tutorial">
                <a href="https://kyna.vn/p/kyna/cau-hoi-thuong-gap/kpoint-va-cach-quy-doi-kpoint-tren-kyna" class="btn">XEM HƯỚNG DẪN</a>
            </div>
        </div>
    </section>
    <?php Pjax::begin(['id' => 'gift-pjax']) ?>
    <section id="exchange-body" class="body-courses-section">
        <div class="container">
            <div class="info-kpoint">
                <span>Tổng Kpoint: <b><?= $userPoint != null ? $userPoint->k_point : 0 ?></b> kpoint</span><span class="per"> / </span>
                <span>Kpoint khả dụng: <b><?= $userPoint != null ? $userPoint->k_point_usable : 0 ?></b> kpoint</span>
            </div>
            <?=
            ListView::widget([
                'dataProvider' => $dataProvider,
                'layout' => "<div class='promotion-items'>{items}</div>\n<div class='pull-right'>{pager}</div>",
                'itemView' => '_gift_item',
                'viewParams' => ['userPoint' => $userPoint],
                'options' => [
                    'class' => 'row list-view-gift',
                ],
                'itemOptions' => [
                    'class' => 'col-lg-4 col-md-6 col-xs-12'
                ],
                'pager' => [
                    'prevPageLabel' => '<span>&laquo;</span>',
                    'nextPageLabel' => '<span>&raquo;</span>',
                    'options' => [
                        'class' => 'pagination',
                    ],
                ]
            ])
            ?>
        </div>
    </section>
    <script type="text/javascript">
        var obj = $('#exchange-body .promotion-items .item');
        var max_tit = 0;
        var n_col = 3;
        if (window.matchMedia('(max-width: 991px)').matches) {
            n_col = 2;
        }
        if (window.matchMedia('(max-width: 767px)').matches) {
            n_col = 1;
        }
        if (n_col > 1) {
            var flag = 0;
            $(obj).each(function() {
                var _max_tit = $(this).find('.gift-title h3').height();

                if (_max_tit > max_tit)
                    max_tit = _max_tit;

                flag++;
                if (flag % n_col == 0 || flag == $(obj).length) {
                    var loop = n_col;
                    if (flag == $(obj).length) {
                        loop = ($(obj).length % n_col);
                        loop = loop == 0 ? n_col : loop;
                    }
                    for (var i = 1; i <= loop; i++) {
                        $(obj[flag - i]).find('.gift-title').height(max_tit);
                    }
                    max_tit = 0;
                }
            })
        } else {
            $(obj).find('.gift-title').height('auto');
        }
    </script>
    <?php Pjax::end()?>
</main>
<?php $script = "
        $(document).ready(function(){
            $('body').on('click', '#btn-submit', function (e) {
                e.preventDefault();

                $(this).prop('disabled', true);

                $('#exchange-form').submit();
                return false;
            });

            $('body').on('click', '.gift-button a[href=\"#\"]', function (e) {
                e.preventDefault();
            });

            $('body').on('submit', '#exchange-form', function (e) {
                e.preventDefault();

                var data = $(this).serializeArray();
                var action = $(this).attr('action');

                $.post(action, data, function (res) {
                    if (res.result) {
                        $('#modal').html(res.content);
                        $.pjax.reload({container: '#gift-pjax', async: false});
                    }
                });
            });
        });
    ";
$this->registerJs($script, View::POS_END);
?>
