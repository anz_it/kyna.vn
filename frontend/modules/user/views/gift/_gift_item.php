<?php

use yii\helpers\Url;
use common\helpers\CDNHelper;
/* @var $model \kyna\gamification\models\Gift */
/* @var $userPoint \kyna\gamification\models\UserPoint */
?>
<div class="item">
    <?php if (!empty($model->image_url)) : ?>
        <div class="gift-image">
            <?= CDNHelper::image($model->image_url, [
                'alt' => $model->title,
                'title' => $model->title,
                'size' => CDNHelper::IMG_SIZE_ORIGINAL,
            ]) ?>
        </div>
    <?php endif; ?>
    <div class="gift-title">
        <h3><?= $model->title ?></h3>
    </div>
    <div class="gift-button">
        <?php if ($userPoint != null && $userPoint->k_point_usable >= $model->k_point) : ?>
            <a href="<?= Url::to(['/user/gift/confirm', 'id' => $model->id])?>" class="btn" data-toggle="modal" data-ajax="" data-push-state="false" data-target="#modal">
                Đổi <?= number_format($model->k_point, 0, ',', '.')?> KPOINT
            </a>
        <?php else: ?>
            <?php if (!Yii::$app->user->isGuest) : ?>
                <a href="#" class="btn btn-notEnough">
                    Đổi <?= number_format($model->k_point, 0, ',', '.')?> KPOINT
                </a>
                <div class="tooltip-button">
                    <span>Bạn chỉ cần thêm <?= $model->k_point - ($userPoint != null ? $userPoint->k_point_usable : 0) ?>KPOINT để đổi quà này</span>
                </div>
            <?php else: ?>
                <a href="<?= Url::toRoute(['/user/security/login']) ?>" class="btn btn-notEnough" data-toggle="modal" data-target="#k-popup-account-login" data-ajax="" data-push-state="false">
                    Đổi <?= number_format($model->k_point, 0, ',', '.')?> KPOINT
                </a>
            <?php endif; ?>
        <?php endif; ?>
    </div>
</div>
