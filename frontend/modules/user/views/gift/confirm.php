<?php
/**
 * Created by PhpStorm.
 * User: macintosh
 * Date: 8/31/17
 * Time: 9:13 AM
 */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\helpers\CDNHelper;
use kyna\gamification\models\GiftContent;

$cdnUrl = CDNHelper::getMediaLink();
?>

<div class="modal-dialog" id="confirm-exchange" role="document">
    <div class="modal-content">
        <div class="header-modal">
            <img src="<?= $cdnUrl ?>/img/kpoint/btn-close.png" alt="" data-dismiss="modal" aria-label="Close">
            <div class="name"><?= $model->title ?></div>
            <?php if (!empty($model->giftContent->min_amount)) : ?>
                <div class="desc">
                    (Áp dụng cho <?= $model->giftContent->type == GiftContent::TYPE_COUPON ? 'khóa học' : 'đơn hàng'?> có giá trị » <?= Yii::$app->formatter->asCurrency($model->giftContent->min_amount) ?>)
                </div>
            <?php endif; ?>
        </div>
        <?php
        $form = ActiveForm::begin([
            'id' => 'exchange-form',
            'action' => Url::toRoute(['/user/gift/confirm', 'id' => $model->id]),
            'method' => 'POST',
            'enableClientValidation' => false
        ]);
        ?>
        <div class="button-submit">
            <?= Html::submitButton('XÁC NHẬN ĐỔI', ['id' => 'btn-submit', 'class' => 'btn btn-exchange-confirm']); ?>
        </div>
        <i><a href="/p/kyna/cau-hoi-thuong-gap#cach-thanh-toan-khi-mua-tren-khoa-hoc-1">Xem hướng dẫn cách sử dụng <i class="fa fa-question-circle-o" aria-hidden="true"></i></a></i>
        <?php ActiveForm::end() ?>
        <br>
    </div>
</div>
