<?php
/**
 * Created by PhpStorm.
 * User: macintosh
 * Date: 9/1/17
 * Time: 9:50 AM
 */

use yii\web\View;
use yii\helpers\Html;
use kyna\gamification\models\UserGift;
use kyna\gamification\models\GiftContent;
use kyna\promotion\models\Promotion;

/**
 * @var $model Promotion
 */
$gift_can_use = $model->checkGiftCanUse();
?>
<div class="box row">
    <div class="col-md-9 col-xl-10">
        <div class="promotion-detail">
            <span><b><?= $model->gift != null ? $model->gift->title : '' ?></b>
                <?php if (!empty($model->min_amount)) : ?>
                    (Áp dụng cho <?= $model->type == Promotion::KIND_COURSE_APPLY ? 'khóa học' : 'đơn hàng'?> có giá trị » <?= Yii::$app->formatter->asCurrency($model->min_amount) ?>)
                <?php endif; ?>
            </span>
            <div class="row">
                <div class="col-md-7 col-lg-4">
                    <span>Mã quà tặng: <?= $model->code ?></span>
                </div>
                <div class="col-md-5 col-lg-4">
                    <span>Tình trạng: <?= ( !empty($gift_can_use)) ? '<span class="green">Chưa sử dụng</span>' : '<span class="gray">Đã sử dụng</span>' ?></span>
                </div>
                <div class="col-md-12 col-lg-4 <?= $model->end_date ?>">
                    <?php if (!empty($model->end_date)) : ?>
                        <span>Hạn dùng: <?=  date("H\hi\, d.m.Y", $model->end_date) ?></span>
                    <?php endif; ?>
                </div>
            </div>
            <?php if (!$model->getCanUseInFrontend()): ?>
                <span>(<?= Promotion::PROMOTION_NOT_FRONTEND_DESC ?>)</span>
            <?php endif; ?>
        </div>
    </div>
    <div class="promotion-button col-md-3 col-xl-2">
        <?php
        switch ($model->discount_type) {
            case Promotion::TYPE_PERCENTAGE;
                $discountText = 'Giảm ngay ' . $model->value . '%';
                break;

            case Promotion::TYPE_FEE;
                $discountText = 'Giảm ngay ' . Yii::$app->formatter->asCurrency($model->value);
                break;

            case Promotion::TYPE_PARITY;
                $discountText = 'Đồng giá ' . Yii::$app->formatter->asCurrency($model->value);
                break;

            default;
        }
            $title = $discountText . ($model->type == Promotion::KIND_COURSE_APPLY ? ' cho khóa học' : ' cho đơn hàng') . (!empty($model->min_amount) ? (' trên ' . Yii::$app->formatter->asCurrency($model->min_amount)) : '');
        ?>
        <?php if (!$model->getCanUseInFrontend()): ?>
            <button type="button" disabled="disabled" class="btn btn-used">» Sao chép mã</button>
        <?php elseif ($gift_can_use): ?>
            <?= Html::button('» Sao chép mã', [
                'class' => 'btn btn-clipboard',
                'data-clipboard-text' => $model->code,
                'data-clipboard-title' => $title
            ])?>
        <?php else: ?>
            <button type="button" class="btn btn-used">» Đã sử dụng</button>
        <?php endif; ?>
    </div>
</div>
<?php
$userId = Yii::$app->user->id;
$script = "
        var clipboard = new Clipboard('.btn-clipboard');

        $('.btn-clipboard').tooltip({
            trigger: 'click',
            placement: 'bottom'
        }).on('click', function(){
            localStorage.setItem('kyna-code-{$userId}', $(this).data('clipboard-text'));
            localStorage.setItem('kyna-title-code-{$userId}', $(this).data('clipboard-title'));
            $('.btn-clipboard').html('» Sao chép mã').removeClass('btn-used');
            $(this).addClass('btn-used').html('» Đã sao chép');
        });

        function setTooltip(btn, message) {
            $(btn).tooltip('hide')
            .attr('data-original-title', message)
            .tooltip('show');
        }

        function hideTooltip(btn) {
            setTimeout(function() {
            $(btn).tooltip('hide');
            }, 1000);
        }

        clipboard.on('success', function(e) {
            setTooltip(e.trigger, 'Copied!');
            hideTooltip(e.trigger);
        });
        $(document).ready(function(){
            var code = localStorage.getItem('kyna-code');
            if (code != '' || code != undefine)
                $('[data-clipboard-text=' + code + ']').addClass('btn-used').html('» Đã sao chép');
        })
    ";
$this->registerJs($script, View::POS_END);
?>
