<?php
/**
 * Created by PhpStorm.
 * User: macintosh
 * Date: 8/31/17
 * Time: 3:18 PM
 *
 * @var $gift \kyna\gamification\models\Gift
 */

use yii\helpers\Url;
use yii\web\View;
use yii\helpers\Html;
use common\helpers\CDNHelper;

use kyna\gamification\models\GiftContent;
use kyna\promotion\models\Promotion;

$cdnUrl = CDNHelper::getMediaLink();
?>
<div class="modal-dialog" role="document" id="success-exchange">
    <div class="modal-content">
        <div class="header-modal">
            <img src="<?= $cdnUrl ?>/img/kpoint/btn-close.png" alt="" data-dismiss="modal" aria-label="Close">
            <div class="desc">Chúc mừng bạn đã đổi thành công</div>
            <div class="name"><?= $gift->title ?></div>
        </div>
        <div class="body-modal clearfix">
            Bạn có thể sao chép mã và nhập ngay trên trang thanh toán. Ngoài ra Kyna sẽ gửi thông tin cho bạn qua email cũng như lưu lại tại trang <a href="<?= Url::toRoute(['/user/gift/index']) ?>">Quản lý Voucher</a>. Hãy bắt đầu sử dụng và trải nghiệm học tập trên Kyna.vn.
        </div>
        <?php
            switch ($userGift->promotion->discount_type) {
                case Promotion::TYPE_PERCENTAGE;
                    $discountText = 'Giảm ngay ' . $userGift->promotion->value . '%';
                    break;

                case Promotion::TYPE_FEE;
                    $discountText = 'Giảm ngay ' . Yii::$app->formatter->asCurrency($userGift->promotion->value);
                    break;

                case Promotion::TYPE_PARITY;
                    $discountText = 'Đồng giá ' . Yii::$app->formatter->asCurrency($userGift->promotion->value);
                    break;

                default;
            }
            $title = $discountText . ($gift->giftContent->type == GiftContent::TYPE_COUPON ? ' cho khóa học' : ' cho đơn hàng') . (!empty($gift->giftContent->min_amount) ? (' trên ' . Yii::$app->formatter->asCurrency($gift->giftContent->min_amount)) : '');
        ?>
        <?= Html::button('SAO CHÉP MÃ', [
            'class' => 'btn btn-success-confirm btn-clipboard',
            'data-clipboard-text' => $code->code,
            'data-clipboard-title' => $title,
        ])?>
    </div>
</div>
<?php
$userId = Yii::$app->user->id;
$script = "
        var clipboard = new Clipboard('.btn-clipboard', {
            container: document.getElementById('modal')
        });

        $('.btn-clipboard').tooltip({
            trigger: 'click',
            placement: 'bottom'
        }).on('click', function(){
            localStorage.setItem('kyna-code', $(this).data('clipboard-text'));
            localStorage.setItem('kyna-title-code-{$userId}', $(this).data('clipboard-title'));
            $(this).addClass('btn-used').html('ĐÃ SAO CHÉP');
        });

        function setTooltip(btn, message) {
            $('.btn-clipboard').tooltip('hide')
            .attr('data-original-title', message)
            .tooltip('show');
        }

        function hideTooltip(btn) {
            setTimeout(function() {
            $('.btn-clipboard').tooltip('hide');
            }, 1000);
        }

        clipboard.on('success', function(e) {
            setTooltip(e.trigger, 'Copied!');
            hideTooltip(e.trigger);
        });
    ";
$this->registerJs($script, View::POS_END);
?>
