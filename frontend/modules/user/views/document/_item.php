<?php
use common\helpers\FontAwesomeHelper;
use yii\helpers\Url;
?>
<?php if(!empty($model->course->documents)) { ?>
<ul class="col-xs-12">
    <li class="root clearfix">
        <i class="icon-list"></i> <span class="file-name"><?= $model->course->name; ?></span>
    </li>

    <?php foreach($model->course->documents as $document) { ?>
        <?php
        $queryParams = [
            'id' => $document->id,
            'action' => 'view'
        ];
        $hashView = $document->getHash($queryParams);
        $queryParams['action'] = 'download';
        $hashDownload = $document->getHash($queryParams);
        ?>
        <li class="col-xs-12 clearfix">
            <div class="fa fa-file-pdf-o fa-1x fa-pull-left hidden-sm-down"></div>
            <span class="group-name">
                <span class="file-name">
<!--                    <p>-->
<!--
                        <span class="line"></span>
                        ?= FontAwesomeHelper::file($document->mime_type, ['1x', 'pull-left']); ?>
-->
                        <?= $document->title; ?>
<!--                    </p>-->
                </span>
                <span class="folder-name"><?= $model->course->name; ?></span>
            </span>
            <span class="pull-right">
                <a class="btn btn-see-online" data-pjax="0" href="<?= Url::toRoute(["/course/learning/document", 'id' => $document->id, 'action' => 'view', 'hash' => $hashView]) ?>" target="_blank">
                    <i class="icon-book"></i>&nbsp;&nbsp;Xem Online
                </a>
                <a class="btn btn-download" data-pjax="0" href="<?= Url::toRoute(["/course/learning/document", 'id' => $document->id, 'action' => 'download', 'hash' => $hashDownload]) ?>">
                    <i class="icon-file-pdf hidden-md-up"></i><i class="icon-download-file  hidden-sm-down" aria-hidden="true"></i> <span class="hidden-sm-down">&nbsp;&nbsp;Download</span>
                </a>
            </span>
            <div class="line1p hidden-md-up"></div>
        </li>
    <?php } ?>
</ul><!--end .wrap-box-->
    <div class="padding10p">
        <div class="line1p"></div>
    </div>
<?php } ?>

