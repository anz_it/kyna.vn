<?php

use yii\widgets\ListView;

$this->title = 'Tài liệu';
?>
<div class="tab-content clearfix col-xs-12" id="document-courses-main">
    <div class="box-tree col-xs-12">
        <?= ListView::widget([
            'dataProvider' => $searchModel->searchDocuments(null),
            'itemView' => '_item',
            'layout' => "{items}\n<nav id='pagination'>{pager}</nav>",
            'pager' => [
                'options' => [
                    'id' => 'pager-container'
                ]
            ]
        ]) ?>
    </div>
</div>