<?php

use yii\widgets\ListView;

$this->title = 'Lịch sử giao dịch';
?>


<div class="tab-content clearfix col-xs-12" id="checkout-courses-main" style="padding-bottom: 30px;">

    <?= $this->render('_tabs') ?>

    <div id="unpaid">

        <?=
        ListView::widget([
            'dataProvider' => $searchModel->search($statuses),
            'itemView' => '_item',
            'layout' => "{items}\n<nav id='pager-container'>{pager}</nav>",
            'pager' => [
                'prevPageLabel' => '<span>&laquo;</span>',
                'nextPageLabel' => '<span>&raquo;</span>',
                'options' => [
                    'class' => 'pagination',
                ],
            ]
        ])
        ?>
    </div>


</div><!--end #transaction-->

