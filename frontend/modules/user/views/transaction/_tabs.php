<?php

use yii\helpers\Url;
use kyna\order\models\Order;

$needPaymentStatus = [
    Order::ORDER_STATUS_PAYMENT_FAILED,
    Order::ORDER_STATUS_REQUESTING_PAYMENT,
    Order::ORDER_STATUS_NEW,
    Order::ORDER_STATUS_PENDING_CONTACT,
    Order::ORDER_STATUS_PENDING_PAYMENT,
    Order::ORDER_STATUS_DELIVERING
];

$completeStatus = [Order::ORDER_STATUS_COMPLETE];

$totalNeedPayment = Order::find()->where([
    'status' => $needPaymentStatus,
    'user_id' => Yii::$app->user->id,
])->count();

$totalComplete = Order::find()->where([
    'status' => $completeStatus,
    'user_id' => Yii::$app->user->id,
])->count();
?>

<div id="mycourses">
    <ul class="nav nav-tabs">
        <li <?= $this->context->action->id == 'in-complete' ? 'class="active-tab"' : '' ?>>
            <a href="<?= Url::toRoute(['/user/transaction/in-complete']) ?>" data-target="#mycourses-main" >
                Chưa thanh toán (<?= $totalNeedPayment; ?>)
            </a>
        </li>
        <li <?= $this->context->action->id == 'complete' ? 'class="active-tab"' : '' ?>>
            <a href="<?= Url::toRoute(['/user/transaction/complete']) ?>" data-target="#mycourses-main" >
                Đã thanh toán (<?= $totalComplete; ?>)
            </a>
        </li>
    </ul>
</div>
