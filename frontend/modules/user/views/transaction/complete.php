<?php

use yii\widgets\ListView;
use kyna\order\models\Order;

$this->title = 'Lịch sử giao dịch';
?>


<div class="tab-content clearfix col-xs-12" id="checkout-courses-main" style="padding-bottom: 30px;">

    <?= $this->render('_tabs') ?>

    <div id="paid">
        <?=
        ListView::widget([
            'dataProvider' => $searchModel->search($statuses),
            'itemView' => '_item',
            'layout' => "{items}\n<div class='pull-right'>{pager}</div>",
            'pager' => [
                'prevPageLabel' => '<span>&laquo;</span>',
                'nextPageLabel' => '<span>&raquo;</span>',
                'options' => [
                    'class' => 'pagination',
                ],
            ]
        ])
        ?>
    </div><!--end #transaction-1-->

</div><!--end #transaction-->

