<?php

use common\helpers\CDNHelper;
use kyna\order\models\Order;
use yii\helpers\Url;

$formatter = Yii::$app->formatter;
?>
<div class="row-fluid row-paid clearfix">
    <div class="col-paid col-md-4 col-xs-12">
        <div class="infomation">
            <div><div class="img-checkout img-id-checkout"></div>Mã đơn hàng: #<?= $model->id; ?></div>
            <div><div class="img-checkout img-date-checkout"></div>Ngày đặt: <?= $formatter->asDatetime($model->created_time); ?></div>
            <?php if(!empty($model->paymentMethodName)){ ?> <div><div class="img-checkout img-pay-checkout"></div><?= $model->paymentMethodName; ?></div> <?php } ?>
            <div><div class="img-checkout img-money-checkout"></div>Tổng cộng: <?= $formatter->asCurrency($model->total); ?> (<?= $model->statusLabel; ?>)</div>
        </div>
        <?php if (in_array($model->status, [
            Order::ORDER_STATUS_PAYMENT_FAILED,
            Order::ORDER_STATUS_REQUESTING_PAYMENT,
            Order::ORDER_STATUS_NEW])) : ?>
            <a class="btn btn-paid" href="<?= Url::to(['/cart/checkout/index', 'orderId' => $model->id]) ?>">Thanh toán</a>
        <?php endif; ?>
    </div>
    <div class="col-detail col-md-8 hidden-sm-down">
        <?php $items = $model->details ?>
        <?php foreach($items as $item) { ?>
            <?php if ($item->courseInfo): ?>
                <div class="row-paid-courses clearfix">
                    <div class="col-image">
                        <?= CDNHelper::image($item->courseInfo->image_url, [
                            'alt' => $item->courseInfo->name,
                            'size' => CDNHelper::IMG_SIZE_THUMBNAIL_SMALL,
                            'resizeMode' => 'cover',
                        ]) ?>
                    </div>
                    <div class="col-info-courses">
                        <div class="title"><?= !empty($item->courseInfo->name) ? $item->courseInfo->name : null; ?></div>
                        <div class="author"><?= !empty($item->course->teacher) ? $item->course->teacher->profile->name : null; ?> / <?= !empty($item->course->teacher) ? $item->course->teacher->title : null; ?></div>
                        <div class="price"><?= $formatter->asCurrency($item->unit_price) ?></div>
                    </div>
                </div>
            <?php endif; ?>
        <?php } ?>
    </div>
</div>
