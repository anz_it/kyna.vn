<?php

use common\helpers\CDNHelper;
use frontend\modules\course\widgets\HotSale;
use frontend\modules\course\widgets\DongGiaFlashSaleTet;
$formatter = \Yii::$app->formatter;
$isMobile = Yii::$app->devicedetect->isMobile() || Yii::$app->devicedetect->isTablet();
?>
<div class="k-box-card-wrap clearfix">
    <div class="img">
        <?= CDNHelper::image($model->image_url, [
            'alt' => $model->name,
            'class' => 'img-fluid',
            'size' => CDNHelper::IMG_SIZE_THUMBNAIL,
            'resizeMode' => 'cover',
        ]) ?>
        <div class="label-wrap">
            <?php if(HotSale::isDateRunCampaignHotSale()):?>
                <?= HotSale::widget(['course_id' => $model->id])?>
            <?php else:?>
                <?php if ($model->is_new): ?>
                    <span class="lb-new">NEW</span>
                <?php endif; ?>

                <?php if ($model->is_hot): ?>
                    <span class="lb-hot">HOT</span>
                <?php endif; ?>
            <?php endif;?>
            <?= DongGiaFlashSaleTet::widget(['course_id' => $model->id])?>
        </div>
        <!-- end .label-wrap -->
    </div>
    <!--end .img-->


    <div class="content">
        <div class="box-style">
            <span class="st-combo">COMBO</span>
            <span class="time pc"><?= count($model->comboItems) ?> khóa học</span>
        </div>
        <h4><?= $model->name ?></h4>
        <span class="author pc">Khóa học combo</span>
        <span class="pc">Bao gồm <?= count($model->comboItems) ?> khóa học</span>
    </div>
    <!--end .content -->


    <div class="content-mb">
    </div>

    <div class="view-price">
        <ul>
            <?php if (!empty($model->discountAmount)) : ?>
                <li class="price"><strong><?= $formatter->asCurrency($model->sellPrice) ?></strong>
                </li>
                <li class="sale">
                    <span><?= $formatter->asCurrency($model->oldPrice) ?>đ</span>
                    <div class="label-discount">
                        <?php if ($model->discountPercent > 0): ?>
                            (-<?= $model->discountPercent ?>%)
                        <?php endif; ?>
                    </div>
                </li>
            <?php elseif (!empty($model->sellPrice)) : ?>
                <li class="price"><span><?= $formatter->asCurrency($model->oldPrice) ?></span></li>
            <?php else : ?>
                <li class="price"><span>Miễn phí</span></li>
            <?php endif; ?>
        </ul>
    </div>
    <!--end .view-price-->

    <div class="view-price-mb">
        <div class="count-items">
            <div class="number"><?= count($model->comboItems) ?></div>
            <div class="text">khóa học</div>
        </div>
        <div class="price">
            <div class="label-price">
                <?php if (!empty($model->discountAmount)) : ?>
                    <div class="first"><?= number_format($model->sellPrice, "0", "", ".") ?>đ</div>
                    <div class="last"><s><?= number_format($model->oldPrice, "0", "", ".") ?>đ</s></div>
                <?php elseif (!empty($model->oldPrice)) : ?>
                    <div class="first"><?= number_format($model->oldPrice, "0", "", ".") ?>đ</div>
                <?php else : ?>
                    <div class="first">Miễn phí</div>
                <?php endif; ?>
            </div>
        </div>
    </div>
    <!--end .view-price mb-->
</div>
<a href="<?= $model->url ?>" class="card-popup" data-pjax="0"></a>
<!--end .wrap-->
