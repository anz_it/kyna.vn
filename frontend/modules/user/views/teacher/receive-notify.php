<?php
$this->title = 'Đăng ký nhận tin mới của giảng viên';

$classStatus = $status ? 'alert-success' : 'alert-danger';
?>

<div class="modal-body">
    <div class="alert <?= $classStatus ?>">
        <?= $message; ?>
    </div>
</div>
<div class="modal-footer" style="border-top: none; padding-top: 0">
    <div class="input col-xs-12">
        <!--        <div class="col-sm-2 col-xs-12 left pd0"></div>-->
        <div class="col-sm-12 col-xs-12 right">
            <center>
                <button type="button" class="btn btn-receive-notify" data-dismiss="modal">Đóng</button>
            </center>

        </div><!--end .right-->
    </div><!--end .input-->
</div>

<?php
    $css = "
        .modal.modal-activeCOD .modal-dialog {
            margin: 80px auto 10px !important;
        }
    ";
    $this->registerCss($css);
?>
