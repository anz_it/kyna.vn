<?php

use common\helpers\CDNHelper;
use kyna\course\models\Course;
use frontend\modules\course\widgets\HotSale;
use frontend\modules\course\widgets\DongGiaFlashSaleTet;
$flag = false;
if (Yii::$app->devicedetect->isMobile() || Yii::$app->devicedetect->isTablet()) {
    $flag = true;
}

$totalTimePartial = Course::getTotalTimeInfo($model->total_time);
?>
<div class="k-box-card-wrap clearfix"><div class="img">
        <?= CDNHelper::image($model->image_url, [
            'alt' => $model->name,
            'class' => 'img-fluid',
            'size' => CDNHelper::IMG_SIZE_THUMBNAIL,
            'resizeMode' => 'cover',
        ]) ?>

        <div class="label-wrap">
            <?php if(HotSale::isDateRunCampaignHotSale()):?>
                <?= HotSale::widget(['course_id' => $model->id])?>
            <?php else:?>
                <?php if ($model->is_new): ?>
                    <span class="lb-new">NEW</span>
                <?php endif; ?>

                <?php if ($model->is_hot): ?>
                    <span class="lb-hot">HOT</span>
                <?php endif; ?>
            <?php endif;?>
            <?= DongGiaFlashSaleTet::widget(['course_id' => $model->id])?>
        </div>

        <!--start course rating-->
        <?= $this->render('_rating', ['model' => $model]) ?>
        <!--end rating-->
    </div>
    <!--end .img-->

    <!--<div class="content">
        <!-start course rating->
        <?/*= $this->render('_rating', ['model' => $model]) */?>
        <!--end rating->
        <h3><?/*= $model->name */?></h3>
        <span class="author"><?/*= $model->teacher->profile->name */?></span>
        <span class=""><?/*= $model->teacher->title */?></span>
    </div>-->


    <div class="content">
        <div class="box-style">
            <?php if ($model->type == Course::TYPE_VIDEO): ?>
                <span class="st-video"><i class="fa fa-youtube-play" aria-hidden="true"></i> Khóa học video</span>
            <?php elseif ($model->type == Course::TYPE_SOFTWARE): ?>
                <span class="st-app"><i class="fa fa-mobile" aria-hidden="true"></i> Phần mềm</span>
            <?php endif; ?>
            <span class="time pc"><i class="fa fa-clock-o" aria-hidden="true"></i> <?= $model->totalTimeText ?></span>
        </div>
        <h4><?= $model->name ?></h4>
        <span class="author"><?= $model->teacher->profile->name ?></span>
        <span class="major"><?= $model->teacher->title ?></span>
    </div>

    <!--end .content -->

    <div class="content-mb">
        <b><?= $model->teacher->profile->name ?>,</b> <?= $model->teacher->title ?>
    </div>

    <!--end .content mb -->
    <!--<div class="view-price">
        <ul>
            <?php /*if (!empty($model->discountAmount)) : */?>
                <li class="sale"><span><?/*= number_format($model->oldPrice, "0", "", ".") */?>đ</span></li>
                <li class="price"><strong><?/*= number_format($model->sellPrice, "0", "", ".") */?>đ</strong>
                </li>
            <?php /*elseif (!empty($model->oldPrice)) : */?>
                <li class="price"><strong><?/*= number_format($model->oldPrice, "0", "", ".") */?>đ</strong></li>
            <?php /*else : */?>
                <li class="price"><strong>Miễn phí</strong></li>
            <?php /*endif; */?>
        </ul>
    </div>-->


    <div class="view-price">
        <ul>
            <?php if (!empty($model->discountAmount)) : ?>
                <li class="price"><strong><?= number_format($model->sellPrice, "0", "", ".") ?>đ</strong>
                </li>
                <li class="sale">
                    <span><?= number_format($model->oldPrice, "0", "", ".") ?>đ</span>
                    <div class="label-discount">
                        <?php if ($model->discountPercent > 0): ?>
                            (-<?= $model->discountPercent ?>%)
                        <?php endif; ?>
                    </div>
                </li>
            <?php elseif (!empty($model->oldPrice)) : ?>
                <li class="price"><strong><?= number_format($model->oldPrice, "0", "", ".") ?>đ</strong></li>
            <?php else : ?>
                <li class="price"><strong>Miễn phí</strong></li>
            <?php endif; ?>
        </ul>
    </div>
    <!--end .view-price-->

    <div class="view-price-mb">
        <div class="student">
            <div class="number"><?= $model->total_users_count ?></div>
            <div class="text">học viên</div>
        </div>
        <div class="time">
            <div class="number"><?= $totalTimePartial['number'] ?></div>
            <div class="text"><?= $totalTimePartial['unit'] ?></div>
        </div>
        <div class="price">
            <div class="label-price">
                <?php if (!empty($model->discountAmount)) : ?>
                    <div class="first"><?= number_format($model->sellPrice, "0", "", ".") ?>đ</div>
                    <div class="last"><s><?= number_format($model->oldPrice, "0", "", ".") ?>đ</s></div>
                <?php elseif (!empty($model->oldPrice)) : ?>
                    <div class="first"><?= number_format($model->oldPrice, "0", "", ".") ?>đ</div>
                <?php else : ?>
                    <div class="first">Miễn phí</div>
                <?php endif; ?>
            </div>
        </div>
    </div>
    <!--end .view-price mb-->
</div>
<a href="<?= $model->url ?>" data-pjax="0" class="card-popup"></a>
<!--end .wrap-->
