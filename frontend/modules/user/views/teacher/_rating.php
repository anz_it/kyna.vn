<?php
/**
 * Created by PhpStorm.
 * User: macintosh
 * Date: 5/31/17
 * Time: 3:00 PM
 */
/* @var $model \common\elastic\Course */

use common\helpers\CDNHelper;

$cdnUrl = CDNHelper::getMediaLink();
?>

<div class="rating-box clearfix">
    <div class="dot" position="1"><i class="fa fa-circle" aria-hidden="true"></i></div>
    <div class="rating-text"><b><?= $model->rating ?> <i class="icon icon-star"></i></b> <span>(<?= $model->rating_users_count ?><detail> đánh giá</detail>)</span></div>
    <div class="dot" position="2"><i class="fa fa-circle" aria-hidden="true"></i></div>
    <div class="number-student"><img src="<?= $cdnUrl ?>/img/icon-user-circle.png" alt=""> <span><?= $model->total_users_count ?> học viên<detail> đăng ký học</detail></span></div>
</div>
