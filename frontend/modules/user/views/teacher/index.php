<?php

use yii\widgets\ListView;
use yii\helpers\Url;
use yii\widgets\Pjax;
use yii\web\View;
use kyna\course\models\Course;

?>

<section class="teacher-description clearfix">
    <div id="k-course-details-author" class="k-course-details-author col-md-9 col-sm-9 col-xs-12" path="scrolling">
        <h2 class="title-content">Thông tin giảng viên</h2>
        <ul>
            <li class="k-course-details-author-teacher">
                <?php if (!empty($teacher->introduction)): ?>
                <div class="content-teacher">
                    <div class="content-teacher-box">
                        <?= $teacher->introduction; ?>
                    </div>
                </div>
                <div class="k-open-description" id="k-open-description">
                    <div class="btn-more">» Xem đầy đủ «</div>
                </div><!--end .wrap-content-teacher-->
                <?php endif; ?>
            </li>
        </ul>
    </div>
    <div class="k-course-details-author col-sm-3 col-md-3 col-xs-12">
        <h4 class="title-content">Chuyên gia về</h4>
        <ul class="teacher-tag">
            <?php foreach ($teacher->tagsObjects as $tag): ?>
                <li><a href="<?= Url::toRoute(['/course/default/index', 'tag' => $tag->slug]) ?>" title="<?= $tag->title ?>"><?= $tag->tag ?></a></li>
            <?php endforeach; ?>
        </ul>
    </div>
</section>

<section class="body-courses-section clearfix">
    <div class="tab-content clearfix col-xs-12" id="mycourses-main">
        <h4 class="title-content">Các khóa học giảng dạy</h4>
        <!-- Tab panes -->
        <?php Pjax::begin([]) ?>
        <div class="course-list">
            <?=
            ListView::widget([
                'dataProvider' => $dataProvider,
                'layout' => "{items}\n<nav id='pager-container'>{pager}</nav>",
                'itemView' => function ($model) {
                    $model = (object)$model;
                    return $this->render($model->type == Course::TYPE_COMBO ? '_item_combo' : '_item', ['model' => $model]);
                },
                'options' => [
                    'tag' => 'ul',
                    'class' => 'clearfix k-box-card-list',
                    'data-pjax' => true
                ],
                'itemOptions' => [
                    'tag' => 'li',
                    'class' => 'col-xl-3 col-lg-4 col-md-6 col-xs-12 k-box-card'
                ],
                'pager' => [
                    'prevPageLabel' => '<span>&laquo;</span>',
                    'nextPageLabel' => '<span>&raquo;</span>',
                    'options' => [
                        'class' => 'pagination',
                    ]
                ]
            ])
            ?>
        </div>
        <?php
        $script = "            
            $(document).ready(function () {
                if (typeof setHeightDiv === 'function') { 
                    setHeightDiv();
                }               
            });
        ";
        $this->registerJs($script, View::POS_READY, 'resize-course-item');
        ?>
        <?php Pjax::end() ?>
    </div>
</section>
<?php
$script = "
    $(document).ready(function () {
        $('#k-open-description').click(function() {
            var _root = $(this).parent();
            $(_root).toggleClass('open');
            if ($(_root).hasClass('open')) {
                $(_root).find('.content-teacher').animate({
                    height: $(_root).find('.content-teacher-box').height()
                }, 400);
                $(this).find('.btn-more').html('» Thu gọn «');
            } else {
                $(_root).find('.content-teacher').animate({
                    height: 130
                }, 400);
                $(this).find('.btn-more').html('» Xem đầy đủ «');
            }
        });
    });
";
$this->registerJs($script, View::POS_READY, 'teacher-page');
?>
