<?php

use yii\widgets\ListView;
use yii\helpers\Url;
use yii\widgets\Pjax;
use common\helpers\CDNHelper;

$cdnUrl = CDNHelper::getMediaLink();

$this->title = 'Lịch sử Kpoint';
?>
<div id="menu-kpoint" class="clearfix">
    <ul class="nav nav-tabs">
        <li class="active-tab">
            <a href="/trang-ca-nhan/diem">Thông tin Kpoint</a>
        </li>
    </ul>
</div>
<?php Pjax::begin(); ?>
<div class="tab-content clearfix col-xs-12 col-lg-12" id="point-history-main">
    <div class="point-history-banner clearfix">
        <div class="box-total clearfix">
            <img src="<?= $cdnUrl ?>/img/kpoint/icon-kpoint.png" alt="">
            <div class="text">
                <span>Tổng Kpoint</span><br><span><b><?= number_format($searchModel->userPoint != null ? $searchModel->userPoint->k_point : 0, 0, ',', '.')?></b> Kpoint</span>
            </div>
        </div>
        <div class="box-total clearfix">
            <img src="<?= $cdnUrl ?>/img/kpoint/icon-kpoint.png" alt="">
            <div class="text">
                <span>Kpoint khả dụng</span><br><span><b><?= number_format($searchModel->userPoint != null ? $searchModel->userPoint->k_point_usable : 0, 0, ',', '.')?></b> Kpoint</span>
            </div>
        </div>
        <div class="box-button">
            <a href="<?= Url::to(['/user/gift/exchange'])?>" class="btn btn-change-kpoint">ĐỔI QUÀ TẶNG</a>
        </div>
    </div>
    <div class="point-history-list">
        <div class="list-point-title">
            LỊCH SỬ KPOINT
        </div>
        <?=
        ListView::widget([
            'dataProvider' => $searchModel->search(Yii::$app->request->queryParams),
            'layout' => "<div class='list-point-detail'>{items}</div>\n<div class='pull-right'>{pager}</div>",
            'itemView' => '_item',
            'pager' => [
                'prevPageLabel' => '<span>&laquo;</span>',
                'nextPageLabel' => '<span>&raquo;</span>',
                'options' => [
                    'class' => 'pagination',
                ],
            ]
        ])
        ?>
    </div>

</div>
<?php
$js = <<< SCRIPT
/* To initialize BS3 tooltips set this below */
$(function () {
    $("[data-toggle='popover']").popover({ trigger: "hover", html: true });
});
SCRIPT;

$this->registerJs($js);
?>
<?php Pjax::end(); ?>
