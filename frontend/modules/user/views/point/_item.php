<?php
/**
 * Created by PhpStorm.
 * User: macintosh
 * Date: 9/1/17
 * Time: 9:50 AM
 */

use kyna\gamification\models\UserPointHistory;
use yii\helpers\Html;
use common\helpers\CDNHelper;

$cdnUrl = CDNHelper::getMediaLink();

/**
 * @var $model \kyna\gamification\models\UserPointHistory
 */
?>
<!-- <?php
if ($index != 0) {
    echo "<hr>";
}
?> -->
<div class="item-point-detail clearfix">
    <?php
    $imgUrl = '';
    $signNumber = false;
    switch ($model->type) {
        // + point from mission
        case UserPointHistory::TYPE_MISSION:
            $imgUrl = '/img/kpoint/icon-kpoint-add.png';
            $signNumber = true;
            break;

        // - point when exchange gift
        case UserPointHistory::TYPE_EXCHANGE_GIFT:
            $imgUrl = '/img/kpoint/icon-kpoint-change.png';
            break;

        // +/- point manually by admin
        case UserPointHistory::TYPE_MANUAL:
            if ($model->k_point > 0){
                $imgUrl = '/img/kpoint/icon-kpoint-add.png';
                $signNumber = true;
            }
            else {
                $imgUrl = '/img/kpoint/icon-kpoint-err.png';
            }
            break;

        default:
            break;
    }
    ?>
    <div class="img-item">
        <img src="<?= $cdnUrl.$imgUrl ?>" alt="">
    </div>
    <div class="info-item">
        <div>
            <span><?= Yii::$app->formatter->asDatetime($model->created_time) ?></span>
            <br>
            <span> <?= $model->description ?>
                <?php
                if(!empty($model->referenceText)) {
                    echo '<i class="fa fa-question-circle" aria-hidden="true"><div class="popup-guide">' . $model->referenceText .'</div></i>';
                }
                ?>

            </span>
        </div>
    </div>
    <div class="kpoint-item">
        <div>
            <?= $signNumber ? '<span class="positiveNum">+' : '<span>' ?><?= $model->k_point ?></span>
            <br>
            <span>KPOINT</span>
        </div>
    </div>
</div>
