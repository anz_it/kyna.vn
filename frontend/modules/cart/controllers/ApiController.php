<?php

namespace app\modules\cart\controllers;

use Yii;

use kyna\payment\models\PaymentMethod;
use app\modules\cart\components\Controller;
use kyna\order\models\Order;

/* 
 * Class ApiController to handle all ipn requests.
 */
class ApiController extends Controller
{
    
    /**
     * @desc action for RedirectUrl
     * @param redirect
     */
    public function actionResponse($method)
    {
        $paymentMethod = PaymentMethod::loadModel($method);
        $get = Yii::$app->request->get();
        
        $result = $paymentMethod->ipn($get);
        if (empty($result['error'])) {
            Yii::$app->cart->emptyCart($result['orderId']);
            
            // redirect to result page
            $this->redirect(['/cart/checkout/success', 'orderId' => $result['orderId']]);
        } else {
            if (!empty($get['vpc_OrderInfo'])) {
                Order::makeFailedPayment($get['vpc_OrderInfo']);
            }
            //Mommo payment
            if($method == "momo"){
                if(!empty($result['orderId'])) {
                    Order::makeFailedPayment($result['orderId']);
                }
            }

            //Payoo payment
            if($method == "payoo"){
                if(!$result['result']) {
                    Order::makeFailedPayment($result['orderId']);
                }
            }

            $this->redirect(['/cart/checkout/failure', 'orderId' => $result['orderId']]);
        }
    }
    
    /**
     * @desc Api Ipn action for third party call
     * @param string $method
     */
    public function actionIpn($method)
    {
        $paymentMethod = PaymentMethod::loadModel($method);
        $get = Yii::$app->request->get();
        
        $paymentMethod->ipn($get);
        
        echo $paymentMethod->printIpnResponse();
    }
    
}
