<?php

namespace app\modules\cart\controllers;

use app\modules\cart\models\form\PromoCodeForm;
use app\modules\cart\models\Product;
use common\helpers\ResponseError;
use common\helpers\ResponseModel;
use kyna\course\models\Course;
use kyna\order\models\Order;
use kyna\order\models\traits\OrderTrait;
use kyna\promotion\models\Promotion;
use Yii;
use yii\base\ActionEvent;

class PromoController extends \app\modules\cart\components\Controller
{

    public function actionApply()
    {
        $model = new PromoCodeForm();

        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()) {
                return $model->applyCode();

            } else {
                Yii::$app->cart->setDiscountAmount(Yii::$app->cart->getTotalDiscountAmount());
                Yii::$app->cart->promotionCode = null;
                Yii::$app->cart->resetCartPromotion();
                Yii::$app->cart->saveCart();
                if(!empty(Yii::$app->cart->getOrderId())) {
                    Yii::$app->cart->updateOrderFromCart(Yii::$app->cart->getOrderId());
                }
                $mgs = "";
                foreach ($model->getErrors() as $err){
                    $mgs .= $err[0];
                }
                Yii::$app->session->setFlash('error', $mgs);
                return ResponseModel::responseJson($model->getErrors(), ResponseModel::VALIDATION_ERROR_CODE);
            }
        }
    }

/**
 * @param $model PromoCodeForm
 * @return string
 */
private
function _applyHistoryOrder($model)
{
    $order = $model->order;
    $saveCode = false;
    if (!$model->isCoupon) {
        // voucher
        foreach ($order->details as $detail) {
            $discountAmount = $detail->course->discountAmount;
            if (empty($discountAmount)) {
                $detail->discount_amount = 0;
                $detail->save(false);
                $saveCode = true;
            }
        }
        if ($saveCode == true) {
            $order->total_discount = $model->getDiscountPrice();
            $order->promotion_code = $model->code;
            $order->total = $order->sub_total - $order->total_discount;
        }
    } else {
        // coupon
        $courses = $model->getCoursesCanUseCode();
        foreach ($order->details as $detail) {
            if (in_array($detail->course_id, $courses)) {
                if (!$model->isPercentage && !$model->isParity) {
                    $detail->discount_amount = $model->getDiscountPrice();
                } elseif (!$model->isParity) {
                    $detail->discount_amount = $model->getPercentagePrice($model->percentage, $detail->unit_price);
                } else {
                    $detail->discount_amount = $model->getParityPrice($detail->unit_price);
                }
                if ($detail->discount_amount > $detail->unit_price) {
                    $detail->discount_amount = $detail->unit_price;
                }
                $detail->save(false);
                $saveCode = true;
            }
        }

        if ($saveCode == true) {
            Order::applyPromotion($model->order_id, $model->code);
        }
    }
    $order->save(false);
    return ResponseModel::responseJson($model);
}
}