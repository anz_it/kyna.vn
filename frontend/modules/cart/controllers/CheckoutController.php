<?php

namespace app\modules\cart\controllers;

use common\campaign\CampaignTet;
use kyna\promotion\models\UserVoucherFree;
use Yii;
use yii\web\Cookie;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\web\HttpException;
use yii\base\ActionEvent;
use yii\widgets\ActiveForm;
use yii\filters\AccessControl;
use yii\validators\EmailValidator;
use yii\validators\RegularExpressionValidator;

use common\helpers\ArrayHelper;
use common\helpers\PhoneNumberHelper;
use common\helpers\DocumentHelper;

use kyna\user\models\User;
use kyna\order\models\Order;
use kyna\course\models\Course;
use kyna\user\models\Address;
use kyna\promotion\models\Promotion;
use kyna\settings\models\Setting;
use kyna\user\models\UserTelesale;
use kyna\payment\models\PaymentMethod;
use kyna\payment\models\TopupTransaction;
use kyna\commission\models\AffiliateUser;

use app\modules\cart\models\form\PaymentForm;
use app\modules\cart\components\Controller;
use app\modules\cart\components\ShoppingCart;
use app\modules\cart\models\form\TopUpForm;
use app\modules\cart\models\Product;
use app\modules\cart\models\form\PromoCodeForm;
use kyna\promo\models\GroupDiscount;

/*
 * Class CheckoutController for shopping cart payments
 */

class CheckoutController extends Controller
{

    public $layout = 'checkout';
    public $mainDivId = 'checkout-home';
    public $mainDivClass = 'checkout';
    public $order = null;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['?', '@'],
                    ],
                    [
                        'allow' => false,
                        'roles' => ['?'],
                        'actions' => ['success', 'failure']
                    ],
                ],
            ],
        ];
    }

    public function init()
    {
        $ret = parent::init();
        return $ret;
    }

    public function beforeAction($action)
    {
        $ret = parent::beforeAction($action);
        if ($action->id == 'index') {
            $courseIdString = Yii::$app->request->get('id', null);
            $code = Yii::$app->request->get('code');
            $affiliateId = Yii::$app->request->get('aff_id');
            $paymentMethod = Yii::$app->request->get('payment_method');

            if (!empty($courseIdString) || !empty($code) || !empty($affiliateId) || $paymentMethod) {
                if (!$this->validatePaymentLink($courseIdString, $code, $affiliateId, $paymentMethod)) {
                    throw new NotFoundHttpException();
                }
            }

            if (!empty($courseIdString)) {
                $courseIds = explode(',', $courseIdString);
                // override cart items
                Yii::$app->cart->emptyCart();
                foreach ($courseIds as $courseId) {
                    $product = Product::findOne($courseId);
                    if ($product == null) {
                        continue;
                    }
                    Yii::$app->cart->put($product, 1, $product->discountAmount);
                }
            }


            if ($code != null) {
                $result = $this->applyCode($code);
                if ($result) {
                    Yii::$app->session->setFlash('success', 'Áp dụng mã giảm giá thành công');
                } else {
                    Yii::$app->session->setFlash('warning', 'Áp dụng mã giảm giá không thành công');
                }
            }

            if (!empty($affiliateId)) {
                $this->applyAffiliate($affiliateId);
            }
        }

        return $ret;
    }

    private function printCart()
    {
        $cartPositions = Yii::$app->cart->positions;
        echo "<pre>";
        foreach ($cartPositions as $position) {
            $tmp = 'quantity :' . $position->quantity .
                '-orignal Price:' . $position->oldPrice .
                '- discount:' . $position->discountAmount .
                ' -voucherDiscunt:' . $position->voucherDiscountAmount .
                ' - promotion code : ' . $position->promotionCode;
            var_dump($tmp);

        }
        die();


    }


    /**
     * @desc Checkout main action
     * @return type
     */
    /*  public function actionIndex()
      {
          $id = Yii::$app->request->get('orderId');
          // check if order is process by telesale
          $oldOrder = Order::findOne(Yii::$app->cart->getOrderId());
          if ($oldOrder && $oldOrder->is_done_telesale_process) {
              Yii::$app->cart->emptyCart();
              return $this->redirect(['/cart/default/index']);
          }
          $cart = Yii::$app->cart;
          $order = null;
          $userAddress = $this->loadUserAddress();


          if (!empty($id)) {
              $paymentForm = new PaymentForm();
              $orderId = $id;
              $order = $cart->getOrder($id);
              if (empty($order) || $order->user_id != Yii::$app->user->id) {
                  throw new NotFoundHttpException("Không tìm thấy đơn hàng");
              }


          }

          $paymentForm = new PaymentForm();
          //GroupDiscount::applyGroupDiscountCartItems($cart->getPositions());
          $paymentForm->totalAmount = $cart->getTotalPriceAfterDiscountAll();

          //empty cart
          if (empty($cart->getPositions())) {
              return $this->redirect(['/cart/default/index']);
          }


          if (empty($orderId)) {
              $orderId = $cart->getOrderId();
          }
          if (!empty($orderId)) {
              $order = $cart->getOrder($orderId);

              $details = $order->details;


              Yii::$app->cart->removeAll();

              Yii::$app->cart->addFromOrder($order);

              // $this->printCart();

              //  $this->printCart();
              $paymentForm->paidAmount = $order->paidAmount;


          }


          $paymentForm->order_id = $orderId;

          $paymentMethods = $this->loadPaymentMethod($paymentForm->totalAmount);
          if ($paymentForm->load(Yii::$app->request->post())) {
              if ($paymentForm->method !== 'free') {
                  $this->checkFreeCourse($paymentForm->totalAmount, $paymentForm);
              }

              Yii::$app->response->format = Response::FORMAT_JSON;

              if ($paymentForm->validate()) {

                  $order = Order::placeOrder($orderId, $paymentForm->method, $paymentForm->getOrderDetails(), $paymentForm->getShippingAddress(), false, $paymentForm->user_id, $paymentForm->promotion_code);

                  if ($orderId == null && $order) {
                      Yii::$app->cart->orderId = $order->id;
                      $orderId = $order->id;
                  }

                  $response = $paymentForm->doPayment($order);

                  if (!$response['result']) {
                      Order::makeFailedPayment($orderId);

                      return [
                          'result' => true,
                          'redirectUrl' => Url::to(['failure'])
                      ];
                  }
                  if (!empty($response['redirectUrl'])) {
                      // redirect payment
                      return [
                          'result' => true,
                          'redirectUrl' => $response['redirectUrl']
                      ];
                  }

                  if ($response['result'] && $response['continue_pay'] === false) {
                      // payment success
                      return [
                          'result' => true,
                          'redirectUrl' => Url::to(['success', 'orderId' => $orderId])
                      ];
                  } else {
                      $paymentForm->paidAmount = $order->paidAmount;
                  }

                  Yii::$app->session->setFlash('payment-method', $paymentForm->method);
                  return [
                      'result' => true,
                  ];
              } else {
                  $errors = ActiveForm::validate($paymentForm);
                  return [
                      'result' => empty($errors) ? true : false,
                      'errors' => $errors
                  ];
              }
          }

          if (!empty(Yii::$app->params['discount_combo'])) {
              // remove promotion if has not combo
              if (!empty(Yii::$app->cart->getPromotionCode()) && (Yii::$app->cart->getPromotionCode() == Yii::$app->params['discount_combo'])) {
                  Yii::$app->cart->removePromotion($orderId);
              }
              if (empty(Yii::$app->cart->getPromotionCode()) && Yii::$app->cart->hasCombo()) {
                  Yii::$app->cart->applyCampaignPromotion();
              }
          }

          return $this->render('index', [
              'paymentMethods' => $paymentMethods,
              'userAddress' => $userAddress,
              'paymentForm' => $paymentForm,
              'order' => $order,
          ]);
      }
      */


    public function actionIndex()
    {
        $id = Yii::$app->request->get('orderId');


        $cart = Yii::$app->cart;
        $userAddress = $this->loadUserAddress();
        $order = null;

        $isUpdateOrder = true;
        if (!empty($id)) {
            $orderId = $id;
        }

        if (empty($orderId)) {
            $orderId = $cart->getOrderId();
        }


        $paymentForm = new PaymentForm();
        if (!empty($orderId)) {
          //  $paymentForm->updateOrderFromCart();
            $order = $cart->getOrder($orderId);
            Yii::$app->cart->removeAll();
        Yii::$app->cart->addFromOrder($order);
            $paymentForm->order_id = $orderId;
            $paymentMethods = $this->loadPaymentMethod(Yii::$app->cart->getTotalPriceAfterDiscountAll());

        } else {

            if (empty($cart->getPositions())) {

                return $this->redirect(['/cart/default/index']);
            }

        }

        $paymentForm->totalAmount = Yii::$app->cart->getTotalPriceAfterDiscountAll();
        $paymentMethods = $this->loadPaymentMethod(Yii::$app->cart->getTotalPriceAfterDiscountAll());

        if ($paymentForm->load(Yii::$app->request->post())) {
            if ($paymentForm->method !== 'free') {
                $this->checkFreeCourse($paymentForm->totalAmount, $paymentForm);
            }

            Yii::$app->response->format = Response::FORMAT_JSON;

            if ($paymentForm->validate()) {

                $order = Order::placeOrder($orderId, $paymentForm->method, $paymentForm->getOrderDetails(), $paymentForm->getShippingAddress(), false, $paymentForm->user_id, $paymentForm->promotion_code);

                if ($orderId == null && $order) {
                    Yii::$app->cart->orderId = $order->id;
                    $orderId = $order->id;
                }

                $response = $paymentForm->doPayment($order);

                if (!$response['result']) {
                    Order::makeFailedPayment($orderId);

                    return [
                        'result' => true,
                        'redirectUrl' => Url::to(['failure'])
                    ];
                }
                if (!empty($response['redirectUrl'])) {
                    // redirect payment
                    return [
                        'result' => true,
                        'redirectUrl' => $response['redirectUrl']
                    ];
                }

                if ($response['result'] && $response['continue_pay'] === false) {
                    // payment success

                    return [
                        'result' => true,
                        'redirectUrl' => Url::to(['success', 'orderId' => $orderId])
                    ];
                } else {
                    $paymentForm->paidAmount = $order->paidAmount;
                }

                Yii::$app->session->setFlash('payment-method', $paymentForm->method);
                return [
                    'result' => true,
                ];
            } else {
                $errors = ActiveForm::validate($paymentForm);
                return [
                    'result' => empty($errors) ? true : false,
                    'errors' => $errors
                ];
            }
        }

        if (!empty(Yii::$app->params['discount_combo'])) {
            // remove promotion if has not combo
            if (!empty(Yii::$app->cart->getPromotionCode()) && (Yii::$app->cart->getPromotionCode() == Yii::$app->params['discount_combo'])) {
                Yii::$app->cart->removePromotion($orderId);
            }
            if (empty(Yii::$app->cart->getPromotionCode()) && Yii::$app->cart->hasCombo()) {
                Yii::$app->cart->applyCampaignPromotion();
            }
        }
        $promotionCode = Yii::$app->cart->getPromotionCode();
        if(CampaignTet::InTimesCampaign() && empty($promotionCode)){
            $cart = \Yii::$app->cart;
            $cartItems = array_reverse($cart->getPositions());
            $totalPrice = 0;
            foreach ($cartItems as $item)
            {
                /* @var $item Product */
                if(!in_array($item->getId(),CampaignTet::COURSE_NOT_APPLY_VOUCHER)){
                    $totalPrice += $item->getPrice();
                }
            }
            $voucher = CampaignTet::voucherLixTet($totalPrice);
            Yii::$app->cart->setPromotionCode($voucher);
            Yii::$app->cart->applyPromotionCode();
        }

        return $this->render('index', [
            'paymentMethods' => $paymentMethods,
            'userAddress' => $userAddress,
            'paymentForm' => $paymentForm,
            'order' => $order,
        ]);
    }

    private function applyCampaignPromotion()
    {
        if (empty(Yii::$app->params['discount_combo'])) {
            return false;
        }
        $model = new PromoCodeForm();
        $model->code = Yii::$app->params['discount_combo'];
        if ($model->validate()) {
            $cartPositions = Yii::$app->cart->positions;
            $saveCode = false;

            if (!$model->isCoupon) {
                // voucher
                foreach ($cartPositions as $position) {
                    $isCombo = $position->getCombo();
                    $discountAmount = $position->discountAmount;
                    if (empty($isCombo) && empty($discountAmount)) {
                        Yii::$app->cart->update($position, 1, 0, $model->code);
                        $saveCode = true;
                    }
                }
                if ($saveCode == true) {
                    Yii::$app->cart->setDiscountAmount($model->getDiscountPrice());
                }
            } else {
                // coupon
                $courses = $model->getCoursesCanUseCode();
                $hackCombo = false;
                if (isset($this->settings['code_can_use_for_combo'])) {
                    $codes = explode(',', $this->settings['code_can_use_for_combo']);
                    $codes = array_map('trim', $codes);
                    if (!empty(Yii::$app->params['discount_combo'])) {
                        $codes[] = Yii::$app->params['discount_combo'];
                    }
                    if (!empty($codes) && in_array($model->code, $codes)) {
                        $hackCombo = true;
                    }
                }

                foreach ($courses as $course) {
                    $cartItem = $cartPositions[$course];
                    $oldPrice = $cartItem->price;

                    if ($hackCombo && $cartItem->type == Course::TYPE_COMBO) {
                        $oldPrice = $cartItem->cost;
                    }

                    if (!$model->isPercentage && !$model->isParity) {
                        $newDiscount = $model->getDiscountPrice();
                    } elseif (!$model->isParity) {
                        $newDiscount = $model->getPercentagePrice($model->percentage, $oldPrice);
                    } else {
                        $newDiscount = $model->getParityPrice($oldPrice);
                    }
                    if ($hackCombo && $cartItem->type == Course::TYPE_COMBO) {
                        $newDiscount += $cartItem->discountAmount;
                    }
                    Yii::$app->cart->update($cartItem, 1, $newDiscount, $model->code);
                }

                if (!empty($courses)) {
                    $saveCode = true;
                }
            }

            if ($saveCode) {
                Yii::$app->cart->promotionCode = $model->code;
            }
            return true;
        } else {
            return false;
        }
    }

    /**
     * @param $orderId
     * @return string
     * @throws HttpException
     */
    public function actionSuccess($orderId)
    {
        //Case cod payment reg with old user email
        if (Yii::$app->user->isGuest) {
            $this->order = Order::findOne([
                'id' => $orderId,
                'payment_method' => 'cod',
                'status' => Order::ORDER_STATUS_PENDING_CONTACT

            ]);
        } else {
            $this->order = Order::findOne([
                'id' => $orderId,
                'user_id' => Yii::$app->user->id
            ]);
        }
        if ($this->order == null) {
            throw new NotFoundHttpException();
        }

        $settings = ArrayHelper::map(Setting::find()->all(), 'key', 'value');

        if ($this->order->status == Order::ORDER_STATUS_IN_COMPLETE) {
            $this->mainDivId = "checkout-failed";
        } else {
            $codeVoucher = $this->order->getPromotionCode();
            if(!empty($codeVoucher)){
                list($code,$userId) = UserVoucherFree::isUserVoucherFree($codeVoucher);
                if(!empty($code) && !empty($userId)){
                    UserVoucherFree::applyVoucherFree($userId,$code,UserVoucherFree::PREFIX,Yii::$app->user->id);
                }
            }
            $this->mainDivId = "checkout-succ-online";
            $this->mainDivClass = "checkout success";
        }

        return $this->render('success', [
            'order' => $this->order,
            'settings' => $settings
        ]);
    }

    public function actionFailure()
    {
        $this->mainDivId = "checkout-failed";
        $this->mainDivClass = "checkout failed";
        $orderId = Yii::$app->cart->getOrderId();
        $this->order = Order::findOne($orderId);
        if (empty($this->order) || $this->order->user_id != Yii::$app->user->id) {
            throw new NotFoundHttpException('Không tìm thấy khóa học');
        }
        return $this->render('failure', [
            'order' => $this->order
        ]);
    }

    private function loadUserAddress()
    {
        if (!Yii::$app->user->isGuest) {
            $userAddress = Yii::$app->user->identity->userAddress;
        }

        if (empty($userAddress)) {
            $userAddress = new Address();
            $userAddress->user_id = Yii::$app->user->id;
        }
        $userAddress->scenario = 'pay_cod';

        return $userAddress;
    }


    /**
     * @param $event
     */
    public function afterPlaceOrder($event)
    {
        $actionMeta = $event->actionMeta;
        $orderMeta = $actionMeta['order'];
        $order = Order::findOne($orderMeta['id']);

        // send register success except: online payment(atm/cc) and epay
        if ($order->paymentMethod->payment_type != PaymentMethod::PAYMENT_TYPE_REDIRECT && !in_array($order->payment_method, ['epay', 'ipay'])) {
            // create KynaLingo coupon for campaign t2
            if ($order->getHasGroupDiscountCourse()) {
                $code = Promotion::createLingoPromotion($order->user->id);
            }
            $mailer = Yii::$app->mailer;
            $mailer->htmlLayout = '@common/mail/layouts/main';

            $mailer->compose('order/register_success', [
                'order' => $order,
                'settings' => $this->settings,
                'code' => !empty($code) ? $code : null
            ])->setTo($order->user->email)
                ->setSubject("Đăng ký đơn hàng #{$order->id} thành công.")
                ->send();
        }
    }

    public function actionTopup()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $formModel = new TopUpForm();

        $this->mainDivId = "checkout-succ-online";
        $this->mainDivClass = "checkout success";

        if ($formModel->load(Yii::$app->request->post()) && $formModel->validate()) {
            $provider = PhoneNumberHelper::detectProviderCode($formModel->phone_number);
            TopupTransaction::updateAll([
                'phone_number' => $formModel->phone_number,
                'provider' => $provider
            ], ['order_id' => $formModel->order_id]);

            return ['result' => 1];
        } else {
            return ['result' => 0, 'errors' => $formModel->errors];
        }
    }

    protected function loadPaymentMethod($amount)
    {
        if ($amount <= 0) {
            return [];
        }
        $setting = Setting::find()->where(['key' => 'cod_min_amount'])->one();

        if (!empty($setting) && ($amount < $setting->value)) {
            return PaymentMethod::getAvailable(PaymentMethod::GET_AVAILABLE_NOT_COD);
        }

        return PaymentMethod::getAvailable(PaymentMethod::GET_AVAILABLE_ALL);
    }

    protected function checkFreeCourse($orderTotal, $paymentForm)
    {
        if ($orderTotal <= 0) {
            $paymentForm->method = 'free';
            return true;
        }
        return false;
    }

    protected function checkPaymentMethodRedirect($payment_method)
    {
        $payment_method = PaymentMethod::find()->where(['class' => $payment_method])->one();
        if (!empty($payment_method) && $payment_method->payment_type == PaymentMethod::PAYMENT_TYPE_REDIRECT) {
            return true;
        }
        return false;
    }

    private function applyCode($code)
    {
        $model = new PromoCodeForm();
        $model->code = $code;
        if ($model->validate()) {
            $cartPositions = Yii::$app->cart->positions;
            $saveCode = false;
            if (!$model->isCoupon) {
                // voucher
                foreach ($cartPositions as $position) {
                    $isCombo = $position->getCombo();
                    $discountAmount = $position->discountAmount;
                    if (empty($isCombo) && empty($discountAmount)) {
                        Yii::$app->cart->update($position, 1, 0, $model->code);
                        $saveCode = true;
                    }
                }
                if ($saveCode == true) {
                    Yii::$app->cart->setDiscountAmount($model->getDiscountPrice());
                }
            } else {
                // coupon
                $courses = $model->getCoursesCanUseCode();
                $hackCombo = false;

                if (isset($this->settings['code_can_use_for_combo'])) {
                    $codes = explode(',', $this->settings['code_can_use_for_combo']);
                    $codes = array_map('trim', $codes);
                    if (!empty($codes) && in_array($model->code, $codes)) {
                        $hackCombo = true;
                    }
                }

                foreach ($courses as $course) {
                    $cartItem = $cartPositions[$course];
                    $oldPrice = $cartItem->price;
                    if ($hackCombo && $cartItem->type == Course::TYPE_COMBO) {
                        $oldPrice = $cartItem->cost;
                    }

                    if (!$model->isPercentage && !$model->isParity) {
                        $newDiscount = $model->getDiscountPrice();
                    } elseif (!$model->isParity) {
                        $newDiscount = $model->getPercentagePrice($model->percentage, $oldPrice);
                    } else {
                        $newDiscount = $model->getParityPrice($oldPrice);
                    }
                    if ($hackCombo && $cartItem->type == Course::TYPE_COMBO) {
                        $newDiscount += $cartItem->discountAmount;
                    }
                    Yii::$app->cart->update($cartItem, 1, $newDiscount, $model->code);
                }

                if (!empty($courses)) {
                    $saveCode = true;
                }
            }

            if ($saveCode) {
                Yii::$app->cart->promotionCode = $model->code;
                return true;
            }
        }
        return false;
    }

    public function renderScriptUserCare()
    {
        // don't show popup to get user info at checkout controller
        return false;
    }

    public function actionValidateEmail()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $email = Yii::$app->request->post('email');
        $paymentMethod = Yii::$app->request->post('paymentMethod');

        if (empty($email)) {
            return [
                'result' => false,
                'message' => 'Email không được để trống',
            ];
        }

        $validator = new EmailValidator();
        if (!$validator->validate($email)) {
            return [
                'result' => false,
                'message' => 'Vui lòng nhập đúng định dạng email',
            ];
        }

        if (Yii::$app->user->isGuest) {
            if ($paymentMethod != 'cod' && User::find()->where(['email' => $email])->exists()) {
                return [
                    'result' => false,
                    'message' => 'Email <font color="black"><b>' . $email . '</b></font> đã có tài khoản. Nếu đó là tài khoản của bạn, vui lòng đăng nhập</i>',
                    'errorCode' => 2,
                ];
            } else {
                return [
                    'result' => true,
                    'message' => '* Email <span>' . $email . '</span> sẽ là tài khoản đăng nhập để học<br>Thông tin mật khẩu đăng nhập sẽ được gửi qua email khi thanh toán mua khoá học thành công'
                ];
            }
        }

        return [
            'result' => true,
            'message' => '',
        ];
    }

    public function actionAddUserCare()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $phone = Yii::$app->request->post('phone');

        $validator = new RegularExpressionValidator([
            'pattern' => '/^((0[0-9]{9,10})|(\+[0-9]{11,12})|([1-9]{1}[0-9]{10,13}))$/'
        ]);
        if ($validator->validate($phone)) {
            // add to user care
            $positions = Yii::$app->cart->getPositions();
            $courseIds = [];

            foreach ($positions as $position) {
                $courseIds[] = $position->id;
            }

            Yii::$app->response->cookies->add(new Cookie([
                'name' => 'cart-user-info',
                'value' => [
                    'phone_number' => $phone
                ]
            ]));

            UserTelesale::add($phone, count($courseIds) == 1 ? $courseIds[0] : $courseIds);

            return [
                'result' => true,
            ];
        } else {
            return [
                'result' => false,
            ];
        }
    }

    private function validatePaymentLink($courseIdString, $code, $affiliateId, $paymentMethod)
    {
        $hash = Yii::$app->request->get('hash');
        if (empty($hash)) {
            return false;
        }

        $queryParams = [];
        if (!empty($courseIdString)) {
            $queryParams['id'] = $courseIdString;
        }
        if (!empty($code)) {
            $queryParams['code'] = $code;
        }
        if (!empty($courseIdString)) {
            $queryParams['aff_id'] = $affiliateId;
        }
        if (!empty($paymentMethod)) {
            $queryParams['payment_method'] = $paymentMethod;
        }

        $serverHash = DocumentHelper::hashParams($queryParams);
        if ($serverHash != $hash) {
            return false;
        } else {
            return true;
        }
    }

}
