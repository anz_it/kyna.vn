<?php

namespace app\modules\cart\widgets;
use yii\base\Widget;
use yii\base\InvalidConfigException;
use yii\helpers\Html;

class ButtonAddToCart extends Widget {
    public $addToCartUrl;
    public $productIdValue;

    public $productIdParam = 'pid';
    public $quantity = 1;

    public $label = 'Đăng ký';
    public $options = [];
    public $viewPath = 'add-to-cart-button';
    public $enableAjax = true;
    public $method = 'post';

    public function init() {
        if (!isset($addToCartUrl)) {
            throw new InvalidConfigException('ButtonAddToCart::$addToCartUrl must be specified');
        }
        if (!isset($productIdValue)) {
            throw new InvalidConfigException('ButtonAddToCart::$productIdValue must be specified');
        }
        parent::init();
        if (!isset($options['class'])) {
            $options['class'] = 'btn btn-primary';
        }
        $options['type'] = 'submit';
    }

    public function run() {
        return $this->render($this->viewPath, [
            'addToCartUrl' => $this->addToCartUrl,
            'productIdValue' => $this->productIdValue,
            'label' => $this->label,
            'productIdParam' => $this->productIdParam,
            'quantity' => $this->quantity,
            'enableAjax' => $this->enableAjax,
            'method' => $this->method,
            'options' => $this->options,
        ]);
    }
}
