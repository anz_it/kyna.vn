+function($) {
    'use strict';

    var MiniCart = function(element, options) {
        this.$element = $(element),
        this.$total = this.$element.find('total')

        this.init();
    }

    MiniCart.prototype.update = function (resp) {
        if (resp != null || resp != undefined) {
            this.$element.html(resp)
        }
        else {
            var url = this.$element.data("remote")
            $.get(url, function (resp) {
                this.$element.html(resp)
                this.$element.trigger("k.cart.updated")
            })
        }
    }

    MiniCart.prototype.add = function (relatedTarget, data) {
        var that = this

        this.$element.trigger("k.cart.add")
        $.post(data.url, data, function (resp) {
            this.$element.trigger("k.cart.added")
            that.update(resp)
        });
    }

    MiniCart.VERSION = '1.0.0';

    MiniCart.DEFAULTS = {
    }

    function Plugin(option, _relatedTarget, params) {
        return this.each(function() {
            var $this = $(this)
            var data = $this.data('k.minicart')
            var options = $.extend({}, MiniCart.DEFAULTS, $this.data(), typeof option == 'object' && option)

            if (!data) $this.data('k.minicart', (data = new MiniCart(this, options)))
            else if (typeof option == 'string') data[option](_relatedTarget, params)
        })
    }

    var old = $.fn.minicart

    $.fn.minicart = Plugin
    $.fn.minicart.Constructor = MiniCart

    $.fn.minicart.noConflict = function() {
        $.fn.minicart = old
        return this
    }

    $(window).on("load", function() {
        var addToCart = function (url, elm, data) {
            var $target = $(".minicart")
            Plugin.call($target, 'add', elm, data);
        }
        $("body")
            .on("click", "[data-toggle='addToCart']", function (e) {
                e.preventDefault();
                var data = $(this).data();
                data.url = this.href;
                addToCart(this, data);
            })
            .on("submit", "form[data-toggle='addToCart']", function (e) {
                e.preventDefault();
                var data = $(this).serializeObject();
                data.url = this.action;
                addToCart(this, data);
            })
    });
}(jQuery);
