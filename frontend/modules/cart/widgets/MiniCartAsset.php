<?php

namespace app\modules\cart\widgets;
use yii\web\AssetBundle;

class MiniCartAsset extends AssetBundle {
    //public $sourcePath = __DIR__.'/assets';
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $js = [
        'js/minicart.js',
    ];
    public $depends = [
        'app\assets\AppAsset',
    ];
}
