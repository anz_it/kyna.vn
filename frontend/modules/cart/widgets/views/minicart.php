<?php
use yii\helpers\Url;
use yii\widgets\ListView;
use Yii;
?>

<a href="<?= Url::toRoute(['/cart/default/index']) ?>" class="nav-link hidden-sm-down" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
    <span class="icon icon-cart"></span>
    <span class="sr-only">Giỏ hàng</span>
    <span class="badge"><?= $itemCount ?></span>
</a>
<a href="<?= Url::toRoute(['/cart/default/index']) ?>" class="nav-link hidden-md-up" role="button" aria-haspopup="true" aria-expanded="false">
    <span class="icon icon-cart"></span>
    <span class="sr-only">Giỏ hàng</span>
    <span class="badge"><?= $itemCount ?></span>
</a>
<div class="dropdown-menu dropdown-menu-right">
    <div class="dropdown-content">
        <?= ListView::widget([
            'dataProvider' => $dataProvider,
            'emptyText' => 'Giỏ hàng trống',
            'layout' => '{items}',
            'options' => ['tag' => false],
            'itemOptions' => [
                'tag' => 'div',
                'class' => 'dropdown-item',
            ],
            'itemView' => function($model) {
                $cost =  Yii::$app->formatter->asCurrency($model->cost);
                return '<div>'.$model->name.'</div><div class="price"><ins>'.$cost.'</ins></div>';
            }
        ]) ?>
        <footer class="dropdown-item">
            <p class="subtotal">Tổng cộng: <strong><?= Yii::$app->formatter->asCurrency($cartTotal) ?></strong></p>
            <div class="row">
                <div class="col-sm-6">
                    <a class="btn btn-secondary btn-block" href="<?= Url::toRoute(['/cart']) ?>">Xem giỏ hàng</a>
                </div>
                <div class="col-sm-6">
                    <a class="btn btn-primary btn-block" href="<?= Url::toRoute(['/cart/checkout']) ?>">Thanh toán</a>
                </div>
            </div>
        </footer>
    </div>
</div>
