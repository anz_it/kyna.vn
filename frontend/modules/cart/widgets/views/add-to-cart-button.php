<?php
use yii\helpers\Html;
?>
<form action="<?= $addToCartUrl ?>" method="<?= $method ?>"<?= $enableAjax ? 'data-submit="addToCartUrl"' : '' ?>>
    <?= Html::hiddenInput('quantity', $quantity) ?>
    <?= Html::hiddenInput($productIdParam, $productIdValue) ?>
    <?= Html::button($label, $options) ?>
</form>
