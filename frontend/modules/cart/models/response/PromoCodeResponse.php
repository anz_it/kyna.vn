<?php
/**
 * @author: Hong Ta
 * @desc: Response json for fronend
 */

namespace app\modules\cart\models\response;

use yii;
use common\helpers\ResponseModel;

class PromoCodeResponse extends ResponseModel{

    public $code;

    public $value;

    public $kind;

    public $type;

}