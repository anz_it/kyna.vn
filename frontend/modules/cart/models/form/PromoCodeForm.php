<?php

namespace app\modules\cart\models\form;

use app\models\Course;
use common\campaign\CampaignTet;
use common\helpers\ResponseModel;
use kyna\order\models\Order;
use kyna\promotion\models\MinigameVoucher;
use kyna\promotion\models\UserVoucherFree;
use kyna\promotion\models\Promotion;
use kyna\settings\models\Setting;
use kyna\user\models\User;
use Yii;
use common\helpers\RequestModel;

/**
 * Promo Code Form model for Shopping Cart payment.
 * @property  $_promotion \kyna\promotion\models\Promotion
 */
class PromoCodeForm extends RequestModel
{

    public $code;
    public $_promotion;
    public $order_id;
    public $voucherCodeFree;

    public function rules()
    {
        return [
            [['code'], 'required'],
            [['code'], 'validateCode'],
            [['order_id'], 'safe']
        ];
    }

    public function attributeLabels()
    {
        return [
            'Code' => 'Mã giảm giá',
        ];
    }

    public function validateCode()
    {
        $voucher = Yii::$app->request->post();
        if($voucher && isset($voucher['PromoCodeForm']) && isset($voucher['PromoCodeForm']['code'])){
            $codeVoucher = $voucher['PromoCodeForm']['code'];
            $this->validateUserVoucherFree($codeVoucher);
        }else{
            $this->validateUserVoucherFree($this->code);
        }

        if(isset(Yii::$app->params['campaign_minigame']) && !empty(Yii::$app->params['campaign_minigame'])){
            if(MinigameVoucher::checkPrefix($this->code)){
                $courses_id = Yii::$app->params['campaign_minigame'];
                $courseInCart = Yii::$app->cart->getPositions();
                if(MinigameVoucher::checkProductCartMiniGame($courseInCart,$courses_id ) == false){
                    $this->addError('code', 'Mã voucher này chỉ áp dụng cho một khóa học thuộc chính sách.');
                }
            }
        }

        $this->_promotion = Promotion::find()->andWhere([
            'code' => $this->code, 'status' => Promotion::STATUS_ACTIVE
        ])->one();

        if ($this->_promotion == null) {
            $this->addError('code', 'Mã giảm giá không chính xác.');
            return;
        }
        //Code su dung cho mobile app
        $app_prefix = substr( $this->code, 0, 4 );
        if(strtolower($app_prefix) == "app_"){
            $this->addError('code', "Mã này chỉ sử dụng trên ứng dụng di động");
            return;
        }
        if (!empty($this->_promotion->start_date) && strtotime("now") < $this->_promotion->start_date) {
            $this->addError('code', 'Mã giảm giá chưa tới thời hạn sử dụng.');
            return;
        }
        $userId = null;
        if (!Yii::$app->user->isGuest) {
            $userId = Yii::$app->user->id;
        }
        //Check gift promotion
        if (!empty($this->_promotion->user_id)) {
            if (empty($userId)) {
                $this->addError('code', 'Bạn phải đăng nhập để sử dụng mã này.');
                return;
            }
            if ($this->_promotion->user_id != $userId) {
                $this->addError('code', 'Mã giảm giá không hợp lệ.');
                return;
            }
        }
        if (!$this->_promotion->getCanUseInFrontend()) {
            $this->addError('code', 'Mã giảm giá không hợp lệ.');
            return;
        }

        /* $existOrder = Order::find()
             ->where(['user_id' => Yii::$app->user->id, 'promotion_code' => $this->code])
             ->andWhere(['NOT IN', 'status', [
                 Order::ORDER_STATUS_NEW,
                 Order::ORDER_STATUS_CANCELLED
             ]])
             ->exists();*/

        //kiem tra so lan su dung 1 users
        $user_used_times = Order::find()
            ->where(['user_id' => Yii::$app->user->id, 'promotion_code' => $this->code])
            ->andWhere(['NOT IN', 'status', [
                Order::ORDER_STATUS_NEW,
                Order::ORDER_STATUS_CANCELLED,
            ]])
            ->count();

        //Thanh toan lai khong tinh vao so lan su dung
        $is_old_order = Order::find()
            ->andWhere(['id' => $this->order_id, 'user_id' => Yii::$app->user->id, 'promotion_code' => $this->code])
            ->andWhere(['status' => Order::ORDER_STATUS_PAYMENT_FAILED])->exists();
        if(!empty($is_old_order)) {
            $user_used_times -= 1;
        }
        if ($user_used_times >= $this->_promotion->user_number_usage) {
            $this->addError('code', 'Mã giảm giá đã hết lần sử dụng cho bạn.');
            return;
        }

        if(empty($this->order_id) || Order::findOne($this->order_id)->promotion_code != $this->code)
        {
            //kiem tra tong so lan su dung
            if ($this->_promotion->number_usage <= $this->_promotion->current_number_usage) {
                $this->addError('code', 'Mã giảm giá đã hết số lần sử dụng.');
                return;
            }

            //kiem tra thoi han su dung
            if (!((empty($this->_promotion->end_date) || strtotime("now") <= $this->_promotion->end_date) &&
                (empty($this->_promotion->start_date) || strtotime("now") >= $this->_promotion->start_date))) {
                $this->addError('code', 'Mã giảm giá đã hết hạn sử dụng.');
                return;
            }

        }




        //kiểm tra giá trị tối thiểu của đơn hàng được áp dụng
        if ($this->_promotion->value && ($this->_promotion->type != Promotion::KIND_COURSE_APPLY)) { //vourher áp dụng cho đơn hang
            if ($this->checkCondition($this->_promotion, $this->order_id) === false) {
                $this->addError('code', 'Đơn hàng không đủ điều kiện sử dụng mã giảm giá này.');
                return;
            }
        }
        //kiểm tra đơn hàng từng khóa học
        if (!$this->checkIsApplyOrder()) {
            $this->addError('code', 'Khóa học không thỏa điều kiện áp dụng.');
            return;
        }

    }

    public function validateUserVoucherFree($codeVoucher){
        list($code,$userId) = UserVoucherFree::isUserVoucherFree($codeVoucher);
        if(!empty($code) && !is_null($userId)){
            $this->voucherCodeFree = $codeVoucher;
            $this->code = $code;
            $user = User::findOne(['id' => $userId]);

            if (!$user) {
                $this->addError('code', 'Mã giảm giá không chính xác.');
                return;
            }

            if (!Yii::$app->user->isGuest) {
                if(!UserVoucherFree::checkUserUseFirstMyVoucher($userId,Yii::$app->user->id,$code)){
                    $this->addError('code', 'Bạn không được xài mã voucher của chính mình lần đầu tiên.');
                    return;
                }

                if(!UserVoucherFree::checkApplyOrder($userId,Yii::$app->user->id)){
                    $this->addError('code', 'Đã hết số lượt sử dụng VOUCHER FREE của bạn trong chương trình. Giới thiệu thêm 03 người khác sử dụng mã của bạn để được thêm 1 lượt sử dụng VOUCHER FREE chương trình này.');
                    return;
                }
            }

        }else if(isset($code) && !empty($code)){
            if($codeVoucher === UserVoucherFree::PREFIX){
                $this->addError('code', 'Mã giảm giá không chính xác.');
                return;
            }
        }
    }

    public function getDiscountPrice()
    {
        if ($this->_promotion) {
            if ($this->_promotion->value && $this->_promotion->discount_type == Promotion::TYPE_FEE) {
                return $this->_promotion->value;
            }
        }
        return 0;
    }

    public function getPercentagePrice($percentage, $course_price)
    {
        $result = (int)$course_price * (int)$percentage / 100;
        return $result;
    }

    /**
     * Parity Price for Course calculation, apply into discount
     */
    public function getParityPrice($course_price = null)
    {
        $result = 0;
        if ($this->_promotion->value && $this->_promotion->discount_type == Promotion::TYPE_PARITY && $this->_promotion->value < $course_price) {
            $result = $course_price - $this->_promotion->value;
        }
        return $result;
    }

    /**
     * @return bool
     */
    public function getIsCoupon()
    {
        if (!empty($this->_promotion)) {
            if ($this->_promotion->type == Promotion::KIND_COURSE_APPLY) {
                return true;
            }
        }
    }

    /**
     * @return bool
     */
    public function getIsPercentage()
    {
        if (!empty($this->_promotion)) {
            if ($this->_promotion->discount_type == Promotion::TYPE_PERCENTAGE) {
                return true;
            }
        }
    }

    public function getPercentage()
    {
        if (!empty($this->_promotion)) {
            if ($this->_promotion->discount_type == Promotion::TYPE_PERCENTAGE) {
                return $this->_promotion->value;
            }
        }
    }

    public function getIsParity()
    {
        if (!empty($this->_promotion)) {
            if ($this->_promotion->discount_type == Promotion::TYPE_PARITY) {
                return true;
            }
        }
    }

    public function getIsFee()
    {
        if (!empty($this->_promotion)) {
            if ($this->_promotion->discount_type == Promotion::TYPE_FEE) {
                return true;
            }
        }
    }

    /**
     * @return mixed
     */
    protected function getPromotion()
    {
        $this->_promotion = Promotion::find()->andWhere([
            'code' => $this->code,
            'is_used' => Promotion::IS_NOT_USED
        ])->andWhere(
            'promotion.end_date IS NULL OR promotion.end_date = 0 OR promotion.end_date > :current_date', [':current_date' => time()
        ])->one();

        if ($this->_promotion === NULL) {
            $promotion = Promotion::find()->andWhere([
                'code' => $this->code,
                'is_used' => Promotion::IS_USED
            ])->andWhere(
                'promotion.end_date > :current_date', [':current_date' => time()
            ])->one();

            if ($promotion && $promotion->current_number_usage < $promotion->number_usage) {
                $this->_promotion = $promotion;
            }
        }

        return $this->_promotion;
    }

    /**
     * @param $promotion
     * @return bool
     */
    public function checkCondition($promotion, $order_id = null)
    {
        $cartCostWithDiscount = Yii::$app->cart->getTotalPriceAfterDiscountOriginal();
        if (!empty($promotion->min_amount) && $promotion->min_amount > $cartCostWithDiscount) {
            return false;
        }
        return true;
    }

    /**
     * Get all course ids can use coupon code
     * @return array
     */
    public function getCoursesCanUseCode()
    {
        $noApplyIds = [];

        if(!empty($this->code)){
            $promotion = Promotion::findOne(['code'=>$this->code]);
            if($promotion->apply_special_course){
                //các khóa học set trong common ngoại trừ
                $exceptCourses = Setting::find()->andWhere(['key' => 'no_apply_voucher_courses', 'is_deleted' => 0])->one();
                if (!empty($exceptCourses)) {
                    $noApplyIds = explode(",", $exceptCourses->value);
                }
            }
        }
        if(CampaignTet::InTimesCampaign()){
            $noApplyIds = array_merge($noApplyIds,CampaignTet::COURSE_NOT_APPLY_VOUCHER);
        }
        $courseIds = [];
        $result = [];

        $order = null; //$this->order;

        $cartItems = Yii::$app->cart->getPositions();
        foreach ($cartItems as $item) {
            //Free course
            if ($item->sellPrice <= 0)
                continue;

            //Skip courses in setting
            if (!empty($noApplyIds)) {
                if (in_array($item->id, $noApplyIds))
                    continue;
            }
            $isCombo = $item->type == Course::TYPE_COMBO ? 1 : 0;
            //All course is unselected
            if (empty($this->_promotion->apply_all)) {
                //Select apply combo
                if (!empty($isCombo)) {
                    if (!empty($this->_promotion->apply_all_combo)) {
                        if (!empty($isCombo) && $item->sellPrice > 0) {
                            $courseIds[] = $item->id;

                        }
                    }


                } else {
                    //Single course selected
                    if (!empty($this->_promotion->apply_all_single_course) && empty($this->_promotion->apply_all_single_course_double)) {
                        if (empty($item->discountAmount)) {
                            $courseIds[] = $item->id;
                        }
                    }
                    //Single course selected and check double discount
                    if (!empty($this->_promotion->apply_all_single_course) && !empty($this->_promotion->apply_all_single_course_double)) {
                        $courseIds[] = $item->id;
                    }
                }

                // User select courses
                if (!empty($this->_promotion->courses)) {
                    foreach ($this->_promotion->courses as $course) {
                        if ($course->id == $item->id) {
                            if (!in_array($course->id, $courseIds))
                                $courseIds[] = $item->id;
                        }
                    }
                }
            } ////All course is selected
            else {
                $courseIds[] = $item->id;
            }
        }

        if(!empty($this->voucherCodeFree)){
            list($code,$userId) = UserVoucherFree::isUserVoucherFree($this->voucherCodeFree);
            if(!empty($code) && !empty($userId)){
                $courseIds = UserVoucherFree::getListMaxPriceProducts(Yii::$app->cart->getPositions(),$courseIds);
            }
        }

        return $courseIds;
    }

    public function checkIsApplyCourse($course)
    {
        //Check free course
        if ($course->sellPrice <= 0)
            return false;

        if ($this->_promotion->type == Promotion::KIND_COURSE_APPLY || $this->_promotion->type == Promotion::KIND_ORDER_APPLY) {
            $noApplyIds = [];
            $exceptCourses = Setting::find()->andWhere(['key' => 'no_apply_voucher_courses', 'is_deleted' => 0])->one();
            if (!empty($exceptCourses)) {
                $noApplyIds = explode(",", $exceptCourses->value);
            }
            if (in_array($course->id, $noApplyIds))
                return false;

            if (!empty($this->_promotion->apply_all)) {
                return true;
            } else {
                $isCombo = $course->type == Course::TYPE_COMBO ? 1 : 0;
                if ($isCombo) {
                    if (empty($this->_promotion->apply_all_combo)) {
                        return false;
                    }
                }
                if (!empty($isCombo) && !empty($this->_promotion->apply_all_combo)) {
                    //Select apply combo
                    if ($course->sellPrice > 0) {
                        return true;

                    }

                } else {
                    //Single course selected
                    if (!empty($this->_promotion->apply_all_single_course)) {
                        if (empty($course->discountAmount)) {
                            return true;
                        }
                    }
                    //Single course selected and check double discount
                    if (!empty($this->_promotion->apply_all_single_course) && !empty($this->_promotion->apply_all_single_course_double)) {
                        if (!empty($course->discountAmount)) {
                            return true;
                        }
                    }

                    // User select courses
                    if (!empty($this->_promotion->courses)) {

                        foreach ($this->_promotion->courses as $c) {
                            if ($c->id == $course->id) {
                                return true;
                            }
                        }
                    }
                }
            }
        }
        return false;
    }


    public function checkIsApplyOrder()
    {
        $cartItems = Yii::$app->cart->getPositions();

        //danh sách các khóa học thỏa điều kiện
        $courseIds = $this->getCoursesCanUseCode();
        if(!empty($this->voucherCodeFree)){
            list($code,$userId) = UserVoucherFree::isUserVoucherFree($this->voucherCodeFree);
            if(!empty($code) && !empty($userId)){
                $listCourseCanApply = UserVoucherFree::getListMaxPriceProducts(Yii::$app->cart->getPositions(),$courseIds);
            }
        }
        if (!empty($courseIds)) {
            if ($this->_promotion->type == Promotion::KIND_ORDER_APPLY) {

                if ($this->_promotion->apply_condition === Promotion::ONE_COURSE_CONDITION) {
                    return true;
                }

                if ($this->_promotion->apply_condition === Promotion::ALL_COURSE_CONDITION) {
                    if (count($courseIds) == count($cartItems))
                        return true;
                    return false;
                }
            }
            return true;
        }
        return false;

    }

    public function getDiscountType()
    {
        return $this->_promotion->discount_type;
    }

    public function getOrder()
    {
        return Order::findOne($this->order_id);
    }

    private function printCart()
    {
        $cartPositions = Yii::$app->cart->positions;
        echo "<pre>";
        foreach ($cartPositions as $position) {
            $tmp = 'quantity :' . $position->quantity . '- price : ' . $position->oldPrice . '- discount:' . $position->discountAmount . '- vourcherDiscount:' . $position->voucherDiscountAmount . ' - promotion code : ' . $position->promotionCode;
            var_dump($tmp);

        }
        die();


    }

    public function applyCode($show_message = true)
    {
        if ($this->validate()) {
            $cartPositions = Yii::$app->cart->positions;

            //tính tổng giá trị đơn hàng được apply vourcher
            $total_order_can_apply = 0;
            $listCourseCanApply = $this->getCoursesCanUseCode();
            foreach ($cartPositions as $key => $value) {
                if (in_array($key, $listCourseCanApply)) {
                    $total_order_can_apply = $total_order_can_apply + $value->oldPrice - $value->originDiscount;
                }
            }
            // tổng giá trị vourcher được giảm
            $total_vourcher_value = 0;

            // case %
            if ($this->getDiscountType() == Promotion::TYPE_PERCENTAGE) {
                $total_vourcher_value = $this->getPercentagePrice($this->percentage, $total_order_can_apply);
            } else {
                $total_vourcher_value = $this->getDiscountPrice();
            }
            // case fix
            $voucherValueDiscount = 0;
            if($this->_promotion->type == Promotion::KIND_COURSE_APPLY) {
                foreach ($cartPositions as $key  => $position) {
                    if(in_array($key,$listCourseCanApply))
                    {
                        $price_after_original_discount = $position->oldPrice - $position->originDiscount;
                        if($this->getDiscountType() == Promotion::TYPE_FEE) // giá cố định
                        {
                            $voucherValueDiscount =  $price_after_original_discount >  $total_vourcher_value   ? $total_vourcher_value : $price_after_original_discount;
                        } else { //giảm phần trăm
                            $voucherValueDiscount = $this->getPercentagePrice($this->getPercentage(), $price_after_original_discount);
                        }
                        Yii::$app->cart->update($position, $position->getQuantity(), $position->discountAmount, $this->code, $voucherValueDiscount);
                    } else {
                        Yii::$app->cart->update($position, $position->getQuantity(), $position->discountAmount, $this->code, 0);
                    }
                }
            } else {
                foreach ($cartPositions as $key  => $position) {
                    $price_after_original_discount = $position->oldPrice - $position->originDiscount;
                    if($total_vourcher_value >= $total_order_can_apply)
                    {
                        $voucherValueDiscount = $position->oldPrice  - $position->discountAmount;
                    } else {
                        $voucherValueDiscount =$total_vourcher_value * ($price_after_original_discount / $total_order_can_apply);
                    }
                    if(in_array($key,$listCourseCanApply))
                    {
                        Yii::$app->cart->update($position, $position->getQuantity(), $position->discountAmount, $this->code, $voucherValueDiscount);
                    } else {
                        Yii::$app->cart->update($position, $position->getQuantity(), $position->discountAmount, $this->code, 0);
                    }
                }
            }
            Yii::$app->cart->setDiscountAmount(Yii::$app->cart->getTotalDiscountAmount());

            $cartPositions = Yii::$app->cart->positions;
            $saveCode = true;
            if ($saveCode) {
                if(!empty($this->voucherCodeFree)){
                    list($code,$userId) = UserVoucherFree::isUserVoucherFree($this->voucherCodeFree);
                    if(!empty($code) && !empty($userId)){
                        Yii::$app->cart->promotionCode = $code.$userId;
                    }
                }else{
                    Yii::$app->cart->promotionCode = $this->code;
                }
                $order = Yii::$app->cart->getOrder();
                if (!empty($order)) {
                    if(!empty($this->voucherCodeFree)){
                        list($code,$userId) = UserVoucherFree::isUserVoucherFree($this->voucherCodeFree);
                        if(!empty($code) && !empty($userId)){
                            $order->setPromotion($code.$userId);
                        }
                    }else{
                        $order->setPromotion($this->code);
                    }
                }
                if ($show_message) {
                    Yii::$app->session->setFlash('success', 'Áp dụng mã giảm giá thành công.');
                }
            }
            Yii::$app->cart->saveCart();
            return ResponseModel::responseJson($this);
        } else {
            Yii::$app->cart->setDiscountAmount(Yii::$app->cart->getTotalDiscountAmount());
            Yii::$app->cart->promotionCode = null;
            Yii::$app->cart->resetCartPromotion();
            Yii::$app->cart->saveCart();
            return ResponseModel::responseJson($this->getErrors(), ResponseModel::VALIDATION_ERROR_CODE);
        }
    }



    private function _applyHistoryOrder()
    {
        $order = $this->order;
        $saveCode = false;
        if (!$this->isCoupon) {
            // voucher
            foreach ($order->details as $detail) {
                $discountAmount = $detail->course->discountAmount;
                if (empty($discountAmount)) {
                    $detail->discount_amount = 0;
                    $detail->save(false);
                    $saveCode = true;
                }
            }
            if ($saveCode == true) {
                $pdiscount = 0;
                if ($this->getDiscountType() == Promotion::TYPE_FEE)
                    $pdiscount = $this->getDiscountPrice();
                if ($this->getDiscountType() == Promotion::TYPE_PERCENTAGE) {
                    $pdiscount = $this->getPercentagePrice($this->percentage, $order->sub_total);
                }
                $order->total_discount = $pdiscount;
                $order->promotion_code = $this->code;
                $order->total = $order->sub_total - $order->total_discount;
            }
        } else {
            // coupon
            $courses = $this->getCoursesCanUseCode();

            foreach ($order->details as $detail) {
                if (in_array($detail->course_id, $courses)) {

                    $newDiscount = 0;
                    if ($this->isPercentage) {
                        $newDiscount = $this->getPercentagePrice($this->percentage, $detail->unit_price);
                    }

                    if ($this->isFee) {
                        $newDiscount = $this->getDiscountPrice();
                    }
                    $detail->discount_amount = $newDiscount;

                    if ($detail->discount_amount > $detail->unit_price) {
                        $detail->discount_amount = $detail->unit_price;
                    }
                    $detail->save(false);
                    $saveCode = true;
                }
            }

            if ($saveCode == true) {
                Order::applyPromotion($this->order_id, $this->code);
            }
        }
        $order->save(false);
        return ResponseModel::responseJson($this);
    }
}
