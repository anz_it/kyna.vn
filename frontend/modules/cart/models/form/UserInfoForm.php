<?php

namespace app\modules\cart\models\form;

use Yii;

class UserInfoForm extends \yii\base\Model
{

    public $full_name;
    public $phone_number;


    public function rules()
    {
        return [
            [['phone_number'], 'required'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'full_name' => 'Họ và tên',
            'phone_number' => 'Số điện thoại',
        ];
    }

}
