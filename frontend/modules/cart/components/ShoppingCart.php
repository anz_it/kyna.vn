<?php

namespace app\modules\cart\components;

use app\modules\cart\models\Product;
use kyna\course_combo\models\CourseCombo;
use kyna\course_combo\models\CourseComboItem;
use kyna\promo\models\GroupDiscount;
use kyna\promotion\models\Promotion;
use Yii;

use yii\web\NotFoundHttpException;
use yz\shoppingcart\CartPositionInterface;
use yz\shoppingcart\CostCalculationEvent;
use yz\shoppingcart\ShoppingCart as ShoppingCartBase;
use yz\shoppingcart\CartActionEvent;

use kyna\order\models\Order;
use app\modules\cart\models\form\PromoCodeForm;
use kyna\course\models\Course;
use common\helpers\ArrayHelper;
use kyna\settings\models\Setting;

/* 
 * Shopping cart component class.
 */

class ShoppingCart extends ShoppingCartBase
{

    public $storeInCache = true;
    private $_email;
    private $_orderId;
    private $_discountAmount;
    private $_shippingAmount;
    private $_promotionCode;
    private $_userInfo = [];

    /**
     * @desc Override init function to load cart from Cache
     */
    public function init()
    {
        Yii::$app->session->open();

        if (Yii::$app->user->isGuest) {
            $this->cartId = Yii::$app->session->id;
        } else {
            $userId = Yii::$app->user->id;

            $this->cartId = "User_{$userId}";
        }

        $this->load();
        $this->on(self::EVENT_CART_CHANGE, [$this, "updateCampaign11"]);
    }

    public function isRequiredGift()
    {
        /* @var $product Product */
        $count = 0;
        foreach ($this->_positions as $index => $product) {
            if ($product->isCampaign11) {
                $count++;
            }
        }
        if ($count % 2 == 1) {
            return 1;
        }
        return 0;
    }

    public function hasGroupDiscountCourse()
    {
        $cartItems = $this->getPositions();

        $groupDiscount = GroupDiscount::applyGroupDiscountCartItems($cartItems);
        $currentGroupDiscount = $groupDiscount['currentGroupDiscount'];
        return !empty($currentGroupDiscount);
    }

    public function hasCombo()
    {
        /* @var $product Product */
        foreach ($this->_positions as $index => $product) {
            if ($product->type == Product::TYPE_COMBO) {
                return true;
            }
        }
        return false;
    }

    public function getListProductId($exeptIds = [])
    {
        /* @var $product Product */
        $result = [];
        foreach ($this->_positions as $index => $product) {
            if ($product->isCampaign11 && !in_array($product->id, $exeptIds)) {
                $result[] = $product->id;
            }
        }
        return $result;
    }

    public function updateCampaign11()
    {
        if (empty($this->_positions)) {
            return false;
        }
        /* @var $product Product */
        $checkCampaign = 0;
        $prevId = 0;
        foreach ($this->_positions as $index => $product) {
            if ($product->isCampaign11) {
                $product = $product->removeGift();
                $checkCampaign++;
                if ($checkCampaign == 2) {
                    $checkCampaign = 0;
                    // update price current product
                    $product = $product->setGift(true);
                    // update previous product price
                    $productPrev = $this->_positions[$prevId];
                    $productPrev = $productPrev->setGift();
                    $this->_positions[$productPrev->getId()] = $productPrev;
                } else {
                    $prevId = $index;
                }
                $this->_positions[$product->getId()] = $product;
            }

        }
        $this->saveCart();
    }

    public function updateDiscountGroup()
    {
        if (empty($this->_positions)) {
            return false;
        }
        foreach ($this->_positions as $index => $product) {

        }
    }

    /**
     * Override Session with Cache
     */
    public function load($oldCart = null)
    {
        if ($this->storeInCache) {
            // load cart stored in cache
            if ($this->cartId != Yii::$app->session->id) {
                $sessionCart = Yii::$app->cache->get(Yii::$app->session->id);
                if (!empty($sessionCart)) {
                    $this->setSerialized($sessionCart['positions']);
                    $this->_orderId = $sessionCart['order_id'];
                    if (!empty($sessionCart['email'])) {
                        $this->_email = $sessionCart['email'];
                    }
                    if (!empty($sessionCart['discountAmount'])) {
                        $this->_discountAmount = $sessionCart['discountAmount'];
                    }
                    if (!empty($sessionCart['shippingAmount'])) {
                        $this->_discountAmount = $sessionCart['shippingAmount'];
                    }
                    if (!empty($sessionCart['promotionCode'])) {
                        $this->_promotionCode = $sessionCart['promotionCode'];
                    }
                    if (!empty($sessionCart['userInfo'])) {
                        $this->_userInfo = $sessionCart['userInfo'];
                    }

                    $this->saveCart();
                    Yii::$app->cache->set(Yii::$app->session->id, FALSE);
                    return;
                }
            }

            $data = Yii::$app->cache->get($this->cartId);
        } else {
            // get cart stored in session
            $data = Yii::$app->session->get($this->cartId);
        }

        // load cart
        if ($data !== false && !is_null($data)) {
            $this->setSerialized($data['positions']);
            $this->_orderId = $data['order_id'];
            if (!empty($data['email'])) {
                $this->_email = $data['email'];
            }
            if (!empty($data['discountAmount'])) {
                $this->_discountAmount = $data['discountAmount'];
            }
            if (!empty($data['shippingAmount'])) {
                $this->_discountAmount = $data['shippingAmount'];
            }
            if (!empty($data['promotionCode'])) {
                $this->_promotionCode = $data['promotionCode'];
            }
            if (!empty($data['userInfo'])) {
                $this->_userInfo = $data['userInfo'];
            }
        }
    }

    /**
     * Override save cart from Session to Cache
     */
    public function saveCart()
    {

        if (empty($this->positions)) {
            $this->_orderId = null;
            $this->_promotionCode = null;
        }

        $key = $this->cartId;
        $value = [
            'positions' => $this->getSerialized(),
            'order_id' => $this->_orderId,
            'email' => $this->_email,
            'promotionCode' => $this->_promotionCode,
            'discountAmount' => $this->_discountAmount,
            'shippingAmount' => $this->_shippingAmount,
            'userInfo' => $this->_userInfo,
        ];


        if ($this->storeInCache) {
            Yii::$app->cache->set($key, $value, Yii::$app->params['expiredCartTime']);
        } else {
            Yii::$app->session->set($key, $value);
        }
        if(!empty($this->_orderId))
        {
            $this->updateOrderFromCart($this->_orderId);
        }

    }


    public function putNotApply($position, $quantity = 1, $discount = 0, $promotionCode = null, $voucherAmount = 0)
    {

        if (isset($this->_positions[$position->getId()])) {
            return $this->update($position, $quantity, $discount, $promotionCode, $voucherAmount);
        }
        $position->setQuantity($quantity);
        $position->setDiscountAmount($discount);
        $position->setVoucherDiscountAmount($voucherAmount);
        // $position->setPromotionCode($promotionCode);
        $this->_positions[$position->getId()] = $position;

        $this->trigger(self::EVENT_POSITION_PUT, new CartActionEvent([
            'action' => CartActionEvent::ACTION_POSITION_PUT,
            'position' => $this->_positions[$position->getId()],
        ]));
        $this->trigger(self::EVENT_CART_CHANGE, new CartActionEvent([
            'action' => CartActionEvent::ACTION_POSITION_PUT,
            'position' => $this->_positions[$position->getId()],
        ]));


        $this->saveCart();
    }


    /**
     * @desc Override put function to just keep maximum quantity is only 1
     * @param CartPositionInterface $position
     * @param int $quantity
     */
    public function put($position, $quantity = 1, $discount = 0, $promotionCode = null, $voucherAmount = 0)
    {

        if (isset($this->_positions[$position->getId()])) {
            return $this->update($position, $quantity, $discount, $promotionCode, $voucherAmount);
        }
        $position->setQuantity($quantity);
        $position->setDiscountAmount($discount);
        $position->setVoucherDiscountAmount($voucherAmount);
        // $position->setPromotionCode($promotionCode);
        $this->_positions[$position->getId()] = $position;

        $this->trigger(self::EVENT_POSITION_PUT, new CartActionEvent([
            'action' => CartActionEvent::ACTION_POSITION_PUT,
            'position' => $this->_positions[$position->getId()],
        ]));
        $this->trigger(self::EVENT_CART_CHANGE, new CartActionEvent([
            'action' => CartActionEvent::ACTION_POSITION_PUT,
            'position' => $this->_positions[$position->getId()],
        ]));

        // var_dump($this->_promotionCode); die();
        $this->resetCartPromotion();
        if (!empty($this->_promotionCode)) {
            $this->applyPromotionCode();
        }
        $this->saveCart();
    }




     public function addFromOrder($order)
     {
         foreach ($order->details as $detail){

             //Combo course
             if(!empty($detail->course_combo_id)){
                 $p = Product::findOne(['id' => $detail->course_combo_id]);
                 if (isset($this->_positions[$detail->course_combo_id])) {
                     continue;
                 }
                 $voucher_combo_discount = 0;
                 foreach ($order->details as $course_detail) {
                     if ($course_detail->course_combo_id == $p->id) {
                         $course_combo = Product::findOne(['id' => $course_detail->course_id]);
                         $priceInCombo = CourseComboItem::findOne(['course_id'=>$course_detail->course_id, 'course_combo_id' => $course_detail->course_combo_id])->price;
                         $voucher_combo_discount +=  $course_detail->discountAmount -  ($course_combo->oldPrice - $priceInCombo);
                     }
                 }

             }
             else {
                 $p = Product::findOne(['id' => $detail->course_id]);
             }

             $original_discount = $p->getOriginDiscount();
             $voucherDiscount = $detail->discountAmount - $original_discount;

             // var_dump( $p->discountAmount . ' - ' .  $p->getOriginDiscount());
             if($p->type == Product::TYPE_COMBO)
             {
                 Yii::$app->cart->putNotApply($p, 1, $p->getOriginDiscount() , $order->promotion_code,$voucher_combo_discount);


             } else {
               //  var_dump($original_discount . ' - ' . $voucherDiscount);
                 Yii::$app->cart->putNotApply($p, 1, $p->getOriginDiscount() , $order->promotion_code, $voucherDiscount);
             }

         }

         $this->setPromotionCode($order->promotion_code) ;
         $this->setOrderId($order->id);


         $this->saveCart();
     }

    private function printCart()
    {
        $cartPositions = Yii::$app->cart->positions;
        echo "<pre>";
        foreach ($cartPositions as $position) {
            $tmp = 'quantity :' . $position->quantity .
                '- price : ' . $position->oldPrice .
                '- discount:' . $position->discountAmount .
                '- vourcherDiscount:' . $position->voucherDiscountAmount .
                ' - promotion code : ' . $position->promotionCode;
            var_dump($tmp);

        }
        die();


    }


    public function resetCartPromotion()
    {
        foreach ($this->_positions as $position) {
            $this->update($position, $position->getQuantity(), $position->discountAmount, null, 0);
        }

    }

    /**
     * @param CartPositionInterface $position
     * @param int $quantity
     */
    public function update($position, $quantity = 1, $discount = 0, $promotionCode = null, $vourcherDiscountAmount = 0)
    {
        if ($quantity <= 0) {
            $this->remove($position);
            return;
        }

        if (!isset($this->_positions[$position->getId()])) {
            return $this->put($position, $quantity, $discount, $promotionCode);
        }

        $this->_positions[$position->getId()]->setQuantity($quantity);
        $this->_positions[$position->getId()]->setDiscountAmount($discount);
        $this->_positions[$position->getId()]->setPromotionCode($promotionCode);
        $this->_positions[$position->getId()]->setVoucherDiscountAmount($vourcherDiscountAmount);

        $this->trigger(self::EVENT_POSITION_UPDATE, new CartActionEvent([
            'action' => CartActionEvent::ACTION_UPDATE,
            'position' => $this->_positions[$position->getId()],
        ]));
        $this->trigger(self::EVENT_CART_CHANGE, new CartActionEvent([
            'action' => CartActionEvent::ACTION_UPDATE,
            'position' => $this->_positions[$position->getId()],
        ]));

        $this->saveCart();
    }

    public function updatePosition($oldPosition, $newPosition)
    {
        if (!isset($this->_positions[$oldPosition->getId()])) {
            return $this->put($newPosition, 1);
        }

        $newPositions = [];
        foreach ($this->_positions as $position) {
            if ($position->getId() == $oldPosition->getId()) {
                $newPositions[$newPosition->getId()] = $newPosition;
            } else {
                $newPositions[$position->getId()] = $position;
            }
        }
        $this->_positions = $newPositions;
        $this->_positions[$newPosition->getId()]->setQuantity(1);
        $this->_positions[$newPosition->getId()]->setDiscountAmount(0);
        $this->_positions[$newPosition->getId()]->setPromotionCode(null);

        $this->trigger(self::EVENT_POSITION_UPDATE, new CartActionEvent([
            'action' => CartActionEvent::ACTION_UPDATE,
            'position' => $this->_positions[$newPosition->getId()],
        ]));
        $this->trigger(self::EVENT_CART_CHANGE, new CartActionEvent([
            'action' => CartActionEvent::ACTION_UPDATE,
            'position' => $this->_positions[$newPosition->getId()],
        ]));

        $this->saveCart();
    }

    public function removeAll()
    {
        $this->_positions = [];
        $this->_promotionCode = null;
        $this->trigger(self::EVENT_CART_CHANGE, new CartActionEvent([
            'action' => CartActionEvent::ACTION_REMOVE_ALL,
        ]));


        $this->saveCart();
    }

    public function setPositions($positions)
    {
        $this->_positions = array_filter($positions, function (CartPositionInterface $position) {
            return $position->quantity > 0;
        });
        $this->trigger(self::EVENT_CART_CHANGE, new CartActionEvent([
            'action' => CartActionEvent::ACTION_SET_POSITIONS,
        ]));

        $this->saveCart();
    }

    public function setOrderId($orderId)
    {
        $this->_orderId = $orderId;

        $this->saveCart();
    }

    public function getOrderId()
    {
        return $this->_orderId;
    }


    public function getOrderDetails()
    {

        $orderDetails = [];

        foreach ($this->getPositions() as $item) {
            if ($item->type == Product::TYPE_COMBO) {
                $total_combo_price =  $item->getOriginalPrice() - $item->discountAmount ; //tông gia combo da giam chua ap dung

                foreach ($item->comboItems as $comboItem) {
                    $pItem = Product::findOne($comboItem->course_id);
                    $pItem->setCombo($item);
                    $discountAmount = $pItem->price - $comboItem->price;
                    $pItem->setVoucherDiscountAmount( $item->voucherDiscountAmount * ( $comboItem->price /$total_combo_price) );
                    $pItem->setDiscountAmount($discountAmount + $pItem->getVoucherDiscountAmount());
                    $pItem->setPromotionCode($item->getPromotionCode());
                    $orderDetails[] = $pItem;
                }

            } else {
                $new_item = clone $item;
                $new_item->discountAmount = $item->discountAmount + $item->voucherDiscountAmount;
                $orderDetails[] = $new_item;
          }
        }
        return $orderDetails;
    }


    public function updateOrderFromCart($orderId)
    {
        if (!Yii::$app->user->isGuest ) {

            $orderDetails = $this->getOrderDetails();
            if (empty($orderId)) {
                $orderMeta = ['point_of_sale' => 'shoppingcart'];
                $order = Order::create($orderDetails, Yii::$app->user->id, $orderMeta,Order::SCENARIO_CREATE_FRONTEND, $this->getPromotionCode());

            } else {
                // var_dump(Yii::$app->cart->getTotalPriceAfterDiscountAll()); die();
                // $this->printOderDetail($orderDetails);
                $order = Order::updateDetails($orderId, $orderDetails, $this->getTotalPriceAfterDiscountAll(), $this->getPromotionCode());

            }
            if ($order) {
                $orderId = $order->id;
                $this->_orderId = $orderId;
            }
        }
    }

    public function getOrder($id = null, $status = [
        Order::ORDER_STATUS_PAYMENT_FAILED,
        Order::ORDER_STATUS_REQUESTING_PAYMENT,
        Order::ORDER_STATUS_NEW,
        Order::ORDER_STATUS_PENDING_PAYMENT
    ])
    {
        $orderId = $this->_orderId;
        if ($id != null) {
            $orderId = $id;
        }
        $order = Order::find()->where(['id' => $orderId]);
        if (!empty($status)) {
            $order->andWhere(['status' => $status]);
        }
        $order = $order->one();
        if ($order == null) {
            return new Order();
            //throw new NotFoundHttpException();
        }

        return $order;
    }

    public function setPromotionCode($promotionCode)
    {
        $this->_promotionCode = $promotionCode;

        $this->saveCart();
    }

    public function getPromotionCode()
    {
        return $this->_promotionCode;
    }

    public function setShippingAmount($shippingAmount)
    {
        $this->_shippingAmount = $shippingAmount;

        $this->saveCart();
    }

    public function getShippingAmount()
    {
        return $this->_shippingAmount;
    }

    public function setEmail($email)
    {
        $this->_email = $email;

        $this->saveCart();
    }

    public function getEmail()
    {
        return $this->_email;
    }

    public function emptyCart($orderId = null)
    {
        if ($orderId == null || $this->_orderId == $orderId) {
            $this->_positions = [];
            $this->_orderId = null;
            $this->_email = null;
            $this->_discountAmount = null;
            $this->_promotionCode = null;

            $this->saveCart();
        }
    }

    public function setDiscountAmount($discountAmount)
    {
        $this->_discountAmount = $discountAmount;

        $this->saveCart();
    }

    public function getDiscountAmount()
    {
        return $this->_discountAmount;
    }

    //include original discount and voucher disount
    public function getTotalDiscountAmount()
    {
        $result = 0;
        foreach ($this->_positions as $position) {
            $result += $position->discountAmount + $position->voucherDiscountAmount;
        }
        return $result;
    }

    public function getTotalPriceAfterDiscountAll()
    {
        $result = 0;
        foreach ($this->_positions as $position) {
            $result += $position->oldPrice - ($position->discountAmount + $position->voucherDiscountAmount);
        }

        return $result;
    }

    public function getTotalPriceAfterDiscountOriginal()
    {
        $result = 0;
        foreach ($this->_positions as $position) {
            $result += $position->oldPrice - $position->discountAmount;
        }

        return $result;
    }


    public function getCost($withDiscount = false, $orderID = null)
    {
        $cost = 0;
        if ($orderID) {
            $order = $this->getOrder($orderID);
            $cost = $order->sub_total;
            if ($withDiscount) {
                $cost = max(0, $cost - $order->total_discount);
            }
        } else {
            /* @var Product $position */
            foreach ($this->_positions as $position) {
                $cost += $position->getCost($withDiscount);
            }

            $costEvent = new CostCalculationEvent([
                'baseCost' => $cost,
                'discountValue' => $this->getDiscountAmount()
            ]);
            $this->trigger(self::EVENT_COST_CALCULATION, $costEvent);
            if ($withDiscount)
                $cost = max(0, $cost - $costEvent->discountValue);
        }
        return $cost;
    }

    /**
     * Override remove cart methods.
     *
     */
    public function remove($position)
    {

        if (!empty($this->_positions)) {
            $this->removeById($position->getId());
        }


        $this->resetCartPromotion();
        if (!empty($this->_promotionCode)) {
            $this->applyPromotionCode();
        }
        $this->saveCart();
    }

    /**
     * Removes position from the cart by ID
     * @param string $id
     */
    public function removeById($id)
    {
        if (isset($this->_positions[$id])) {
            $this->trigger(self::EVENT_BEFORE_POSITION_REMOVE, new CartActionEvent([
                'action' => CartActionEvent::ACTION_BEFORE_REMOVE,
                'position' => $this->_positions[$id],
            ]));
            $this->trigger(self::EVENT_CART_CHANGE, new CartActionEvent([
                'action' => CartActionEvent::ACTION_BEFORE_REMOVE,
                'position' => $this->_positions[$id],
            ]));
            $position = $this->_positions[$id];
            unset($this->_positions[$id]);

            $items = $this->_positions;

            if (count($items) == 0) {
                $this->_promotionCode = null;
                $this->_discountAmount = null;
            } else {
                // re-validate promotion code
                //$this->updatePromotion();
                $this->applyPromotionCode();
                $arr_code = null;
                foreach ($items as $item) {
                    if (!empty($item->promotionCode)) {
                        $arr_code[] = $item->promotionCode;
                    }
                }
                if (empty($arr_code)) {
                    $this->_promotionCode = null;
                    $this->_discountAmount = null;
                }
            }

            $this->trigger(self::EVENT_CART_CHANGE, new CartActionEvent([
                'action' => CartActionEvent::ACTION_BEFORE_REMOVE,
                'position' => $position,
            ]));


            $this->saveCart();
        }

    }

    public function getUserInfo()
    {
        return $this->_userInfo;
    }

    public function setUserInfo($info)
    {
        $this->_userInfo = $info;
        $this->saveCart();
    }

    public function getIsFreeOrder()
    {
        $items = $this->_positions;

        if (count($items) > 0 && $this->getCost(true) === 0) {
            return true;
        }

        return false;
    }

    public function checkOrderHasPromotion($orderId = null)
    {
        if (Yii::$app->user->isGuest || empty($orderId)) {
            return false;
        }
        $userId = Yii::$app->user->id;
        $order = Order::find()->andWhere(['id' => $orderId, 'user_id' => $userId, 'promotion_code' => $this->_promotionCode])->one();
        return !empty($order) ? true : false;
    }

    public function removePromotion($orderID = null)
    {
        if (empty($orderID)) {
            // remove from cart
            foreach ($this->_positions as $position) {
                $this->update($position, 1, 0);
            }
            $this->_promotionCode = null;
            $this->_discountAmount = null;
            $this->saveCart();
        } else {
            // remove from order
            if ($order = Order::findOne($orderID)) {
                Order::removePromotion($order->id);
            }
            foreach ($this->_positions as $position) {
                $this->update($position, 1, 0);
            }
            $this->_promotionCode = null;
            $this->_discountAmount = null;
            $this->saveCart();
        }
    }

    public function updatePromotion()
    {
        $cartCost = $this->getCost(false);
        $promotion = Promotion::find()->andWhere([
            'code' => $this->_promotionCode
        ])->one();
        if ($promotion && $cartCost >= $promotion->value) {
            // cart total must be larger than min promotion code amount
            if (!empty($promotion->min_amount) && $promotion->min_amount > $cartCost) {
                $this->removePromotion();
            }
        }
    }

    public function applyPromotionCode()
    {
        if (!empty($this->_promotionCode)) {
            $promotionForm = new PromoCodeForm();
            $promotionForm->code = $this->_promotionCode;
            $promotionForm->order_id = $this->_orderId;
            if ($this->checkOrderHasPromotion($this->_orderId)) {
                $this->removePromotion($this->_orderId);
                Promotion::removeFromOrder($promotionForm->code);
            }
            $promotionForm->applyCode(false);
        }
    }

    public function applyCampaignPromotion()
    {
        if (empty(Yii::$app->params['discount_combo'])) {
            return false;
        }
        $settings = ArrayHelper::map(Setting::find()->all(), 'key', 'value');
        $model = new PromoCodeForm();
        $model->code = Yii::$app->params['discount_combo'];
        if ($model->validate()) {
            $cartPositions = Yii::$app->cart->positions;
            $saveCode = false;

            if (!$model->isCoupon) {
                // voucher
                foreach ($cartPositions as $position) {
                    $isCombo = $position->getCombo();
                    $discountAmount = $position->discountAmount;
                    if (empty($isCombo) && empty($discountAmount)) {
                        Yii::$app->cart->update($position, 1, 0, $model->code);
                        $saveCode = true;
                    }
                }
                if ($saveCode == true) {
                    Yii::$app->cart->setDiscountAmount($model->getDiscountPrice());
                }
            } else {
                // coupon
                $courses = $model->getCoursesCanUseCode();
                $hackCombo = false;
                if (isset($settings['code_can_use_for_combo'])) {
                    $codes = explode(',', $settings['code_can_use_for_combo']);
                    $codes = array_map('trim', $codes);
                    if (!empty(Yii::$app->params['discount_combo'])) {
                        $codes[] = Yii::$app->params['discount_combo'];
                    }
                    if (!empty($codes) && in_array($model->code, $codes)) {
                        $hackCombo = true;
                    }
                }

                foreach ($courses as $course) {
                    $cartItem = $cartPositions[$course];
                    if ($hackCombo && $cartItem->type == Course::TYPE_COMBO) {
                        $oldPrice = $cartItem->cost;
                        if (!$model->isPercentage && !$model->isParity) {
                            $newDiscount = $model->getDiscountPrice();
                        } elseif (!$model->isParity) {
                            $newDiscount = $model->getPercentagePrice($model->percentage, $oldPrice);
                        } else {
                            $newDiscount = $model->getParityPrice($oldPrice);
                        }
                        if ($hackCombo && $cartItem->type == Course::TYPE_COMBO) {
                            $newDiscount += $cartItem->discountAmount;
                        }
                        Yii::$app->cart->update($cartItem, 1, $newDiscount, $model->code);
                    }
                }

                if (!empty($courses)) {
                    $saveCode = true;
                }
            }

            if ($saveCode) {
                Yii::$app->cart->promotionCode = $model->code;
            }
            return true;
        } else {
            return false;
        }
    }
}
