<?php

namespace app\modules\cart\components;

use common\helpers\PhoneNumberHelper;

use kyna\payment\events\TransactionEvent;
use kyna\payment\models\TopupTransaction;
use kyna\payment\models\PaymentMethod;
use kyna\order\models\Order;

use app\modules\cart\models\form\TopUpForm;

/* 
 * Base Controller class for Cart module
 */
class Controller extends \app\components\Controller
{
    
    public function init()
    {
        $ret = parent::init();
        
        TransactionEvent::on(PaymentMethod::className(), PaymentMethod::EVENT_PAYMENT_SUCCESS, [$this, 'completeOrder']);

        return $ret;
    }
    
    public function completeOrder($transactionEvent)
    {
        $transData = $transactionEvent->transData;
        $orderId = $transData['orderId'];

        $isOrderCompleted = false;
        if(!empty($orderId)){
            $isOrderCompleted = Order::find()->andWhere(['id' => $orderId, 'status' => Order::ORDER_STATUS_COMPLETE])->exists();
        }
        if(!$isOrderCompleted) {
            Order::complete($transData['orderId'], $transData['transactionCode']);

            \Yii::$app->session->setFlash('need-tracking', true);
        }
    }

}
