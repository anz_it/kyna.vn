<?php

use yii\helpers\Url;

?>
<script>
    // Cap bought-stauts, action-button, thanh toan nhanh
    $(".bought-status > ul").html('<li class="text-success"><b><i class="fa fa-check-circle"></i> Đã thêm khóa học này vào giỏ hàng</b></li>');
//    $("a.add-to-cart").addClass("reg").html('Đã thêm vào giỏ');
    $(".action-button").html('<a class="btn-lg btn-block thanh-toan" href="<?= Url::toRoute(['/cart/default/index']) ?>"><b>XEM GIỎ HÀNG VÀ THANH TOÁN</b></a>');
    $(".buy-now-popup").hide();
    $(".alert-add-to-cart").html('<a href="/gio-hang"><b><i class="icon-long-arrow-right"></i> Xem giỏ hàng và thanh toán</b></a>');
    $(".scrollBarDetail").html('<a class="add-to-cart notCombo reg" href="<?= Url::toRoute(['/cart/default/index']) ?>">Xem giỏ hàng và thanh toán</a>');
</script>
