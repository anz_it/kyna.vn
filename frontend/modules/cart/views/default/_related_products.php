<section>
    <div class="k-course-card-slide">
        <div class="container">
            <div class="col-xl-12 col-md-11 col-xs-12">
               <h4 class="k-course-card-slide-title">Có thể xem thêm các khóa học dưới đây</h4>
                <ul class="clearfix k-box-card-list list-unstyled">
                    <?php for ($i = 0; $i < 5; $i++) : ?>
                    <li class="k-box-card">
                        <div class="k-box-card-wrap">
                           <div class="img">
                              <img src="https://lh6.googleusercontent.com/--h7AosQIlJ0/VNBzaECB6_I/AAAAAAAAK74/1MF_UH2ZCpI/s800/Kh%25C3%25B3a%252030%2520ng%25C3%25A0y.jpg" alt="" class="img-fluid">
                              <div class="teacher mb">
                                <ul>
                                  <li>
                                    <img src="img/banner.jpg" alt="" class="img-teacher">
                                  </li>
                                  <li>
                                    TS. Dương Ngọc Dũng test
                                  </li>
                                </ul>
                              </div>
                              <span class="time">10giờ 0 phút test</span>
                              <span class="background-detail">
                                <span class="wrap-position">
                                <a href="" data-toggle="modal" data-target="#modal">Xem chi tiết</a>
                                </span>
                              </span>        
                           </div>
                           <!--end .img-->
                           <div class="content" style="height: 154px;">
                              <h4>30 ngày để tự tin và...</h4>
                              <span class="author pc">Nguyễn Thanh Minh</span>
                              <span class="pc">Đồng sáng lập DeltaViet Education Corporation</span>
                              <ul>
                                 <li>
                                    <span class="icon-star">
                                    <i class="icon icon-star-two active"></i> 
                                    <i class="icon icon-star-two active"></i> 
                                    <i class="icon icon-star-two active"></i>
                                    <i class="icon icon-star-half active"></i>
                                    <i class="icon icon-star-two"></i>                                                              
                                    </span>
                                 </li>
                                 <!--
                                    <li>(1200 đánh giá)</li>
                                    -->
                              </ul>
                           </div>
                           <!--end .content -->
                           <div class="view-price">
                              <ul>
                                 <li class="price"><span class="bold">VND 399,000.00</span></li>
                              </ul>
                           </div>
                           <!--end .view-price-->
                           <a href="/khoa-hoc/30-ngay-de-tu-tin-va-giao-tiep-hieu-qua-p1" class="link-wrap" data-toggle="modal" data-target="#modal"></a>               
                        </div>    
                    <a href="" class="card-popup" data-toggle="modal" data-target="#modal"></a>
                    <!--end .wrap-->                
                    </li>
                    <?php endfor; ?>                                                        
                </ul> 
            </div>
        </div><!--end .container-->
    </div><!--end .k-course-card-slide-->
</section>