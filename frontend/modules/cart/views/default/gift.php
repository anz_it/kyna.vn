<?php
/**
 * Created by IntelliJ IDEA.
 * User: admin
 * Date: 12/23/2017
 * Time: 10:30 AM
 */
use yii\web\View;
use common\helpers\CDNHelper;
use yii\helpers\Url;

$cdnUrl = CDNHelper::getMediaLink();
?>

<div class="modal-header">
    <h5 class="modal-title">Khóa học bạn mua nằm trong chương trình <a href="/tag/mua1tang1"><b>Mua 1 Tặng 1</b></a>. Hãy chọn khóa bạn
        muốn được tặng nhé!</h5>
</div>
<div class="modal-body campaign11">
    <select class="selecte-getone-buyone js-states form-control">
        <?php foreach ($courseList as $course):
            $teacherName = $course->teacher->profile ? $course->teacher->profile->name : $course->teacher->email;
            ?>
            <option slug="<?= $course->slug ?>" teacher="<?= $teacherName ?>" value="<?= $course->id ?>"><?= $course->name ?></option>
        <?php endforeach; ?>
    </select>
    <a class="see-detail" href="<?= Url::to(['/course/default/view', 'slug' => $courseList{0}->slug]) ?>"><i>» Xem chi tiết khóa học</i></a>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-deny" data-dismiss="modal">Tôi không muốn chọn khóa tặng</button>
    <button type="button" class="btn-add-to-cart btn btn-success add-to-cart" data-pid="<?= $courseList{0}->id ?>" data-dismiss="modal">Hoàn tất</button>
</div>


<?php
$js = "
    $('document').ready(function(){
        $('.selecte-getone-buyone').select2({
            templateResult: formatState,
            width: '100%'
        });
        $('.selecte-getone-buyone').on('change', function (e) {
            var obj = $(this).select2('data');
            $('.campaign11 .see-detail').attr('href', '/' + obj[0].element.attributes.slug.value);
            $('.btn-add-to-cart').data('pid', e.currentTarget.value);
        });
    });
    function formatState (state) {
        if (!state.id) {
            return state.text;
        }
        var _state = $(
            '<b>' + state.text + '</b><br>- <span>' + state.element.getAttribute('teacher') + '</span>'
        );
        return _state;
    };
";
$this->registerJs($js, View::POS_END, 'js-customselect');
?>
