<?php

use app\modules\cart\models\Product;
use common\helpers\CDNHelper;

$formatter = \Yii::$app->formatter;
$cdnUrl = CDNHelper::getMediaLink();


$cost = $item->getCost(false);

$costWithDiscount = $item->getCost();
?>

<div class="k-shopping-list-items-title" data-id="<?= $item->id ?>" data-price="<?= $costWithDiscount ?>" data-brand="<?= (!empty($item->teacher))? $item->teacher->profile->name : ''?>">
    <div class="items-img">
        <a href="<?= $item->url ?>" title="<?= $item->name ?>">
            <?php if (!empty($item->image_url)) : ?>
                <?= CDNHelper::image($item->image_url, [
                    'alt' => $item->name,
                    'class' => 'img-fluid',
                    'size' => CDNHelper::IMG_SIZE_THUMBNAIL_SMALL,
                    'resizeMode' => 'cover',
                ]) ?>
            <?php else: ?>
                <img src="<?= $cdnUrl ?>/src/img/cart/default.jpg" alt="<?= $item->name ?>" class="img-fluid">
            <?php endif; ?>
        </a>
    </div>

    <div class="items-text">
        <h4>
            <a href="<?= $item->url ?>" title="<?= $item->name ?>"><b><?= $item->name ?></b><?= $item->purchaseTypeTextFrontend === NULL ? "" : "<span> - Thời hạn " . $item->purchaseTypeTextFrontend . "</span>"?></a>
        </h4>
        <?php if (!empty($item->teacher_id)) : ?>
        <!-- <p><?= "{$item->teacher->profile->name} / {$item->teacher->title}" ?></p> -->
        <?php endif ?>
        <?php if ($item->type == Product::TYPE_COMBO) : ?>
            <ul class="comboItem">
                <?php foreach ($item->comboItems as $comboItem) : ?>
                <li>
                    <div class="media">
                        <div class="media-left">
                            &#9679;
                        </div>
                        <div class="media-body">
                            <a href="<?= $comboItem->course->url ?>"><?= $comboItem->course->name ?></a>
                        </div>
                     </div>
                </li>
                <?php endforeach; ?>
            </ul>
        <?php elseif (false) : ?>
        <span class="cart-item-exist alert alert-warning">Khóa học này đã tồn tại trong combo: <strong><?= $combo ?></strong>.</span>
        <?php endif; ?>
        <div class="k-shopping-list-items-group-price -mob">
            <?php if (empty($cost)) : ?>
                <span class="orange">Miễn phí</span>
            <?php elseif ($cost == $costWithDiscount) : ?>
                <span class="orange"><?= $formatter->asCurrency($cost) ?></span>
            <?php else: ?>
                <span class="orange"><?= $formatter->asCurrency($costWithDiscount) ?></span> <span><s>(<?= $formatter->asCurrency($cost) ?>)</s></span>
            <?php endif; ?>
        </div>
        <a href="javascript:" data-id="<?= $item->getId() ?>" class="items-remove cart-item-remove"><img src="<?= $cdnUrl ?>/img/icon-delete.png" alt=""> <i>Xóa khóa học</i></a>

    </div>
    <!--end .text-->
</div>
<!--end .title-->

<div class="k-shopping-list-items-group-price">
    <div class="k-shopping-list-items-price-old">
        <span><s><?= $formatter->asCurrency($cost) ?></s></span>
    </div>
    <div class="k-shopping-list-items-sale">
        <span><?= $formatter->asCurrency($costWithDiscount - $cost) ?></span>
    </div>
    <div class="k-shopping-list-items-price-new">
        <?php if (empty($cost)) : ?>
            <span>Miễn phí</span>
        <?php elseif ($cost == $costWithDiscount) : ?>
            <span><?= $formatter->asCurrency($cost) ?></span>
        <?php else: ?>
            <span><?= $formatter->asCurrency($costWithDiscount) ?></span>
        <?php endif; ?>
    </div>
</div>
