<?php

use yii\helpers\Url;
use yii\web\View;
use yii\widgets\ActiveForm;
use common\widgets\Alert;
use common\helpers\CDNHelper;
use kyna\tag\models\Tag;
use kyna\promo\models\GroupDiscount;
use frontend\modules\course\widgets\CampaignTetBanner;
use common\campaign\CampaignTet;
$cdnUrl = CDNHelper::getMediaLink();

$formatter = \Yii::$app->formatter;

$this->title = "Giỏ hàng của bạn";
$cartItems = array_reverse($cart->getPositions());
$totalCost = $cart->getCost();
$totalDiscount = $cart->getTotalDiscountAmount();
$totalCount = $cart->getCount();
$totalPaymentAmount = $cart->getTotalPriceAfterDiscountAll();

$settings = Yii::$app->controller->settings;

// TODO: get hot key words by search
$hotKeywords = !empty($settings['hot_keyword']) ? explode(',', $settings['hot_keyword']) : [
    'Giao tiếp',
    'Monkey junior',
    'Bán hàng',
    'Giáo dục sớm',
    'Tiếng anh',
    'Marketing',
    'Bất động sản',
    'Tiếng hoa',
    'Tiếng Nhật'
];

$desktopTags = Tag::topTags(Tag::TYPE_DESKTOP, 5);
$highLightCourses =  \common\elastic\Course::getFeatureCourse(4);

// Set breadcrumbs css
$this->registerCss("
    
    #hot-courses h3 {
        margin-bottom: 12px !important;
        font-size:22px;
    }
    
    .banner{
        margin-left: 120px;
    }
    @media (max-width: 1200px) {         
        .banner{
            display: none;
        }
    }  
    @media (max-width: 767px) {
            
        #k-shopping .k-shopping-list h3 {
            margin-bottom: 10px;
        }
        
        .breadcrumb-container {
            display: none;
        }
         h3.mobile{
            display: block !important;
            color: #50ad4e;
            font-weight: bold;
        }
        
        #hot-courses {
            margin-top: 0 !important;
            border-top: 0 !important;
            padding-top: 0;
        }
        
        .slick-slide {
            padding: 0 !important;
        }
        .k-box-card-list  .k-box-card-wrap {
            padding: 5px;
        }
        .k-box-card{
            padding: 0 !important;
        }
        
        .slick-dots li button:before {
            font-family: 'slick';
            font-size: 10px;
        }
        
        #hot-courses h3 {
            display: block !important;
        }

    }
    .breadcrumb-container {
        background-color: #fafafa;
    }
	@media (min-width: 768px) and (max-width: 991px){
		.breadcrumb-container{
			margin-top: 0;
		}
		
		#k-shopping .k-shopping-list h3 {
            margin-bottom: 20px;
        }
	}
    .breadcrumb {

        border-radius: 0px;
        padding: 8px 0px;
        margin: 0px;
        background-color: inherit;
    }
    .breadcrumb > li + li::before {
        padding-right: .5rem;
        padding-left: .5rem;
        color: #a0a0a0;
        content: \"»\";
    }
    .breadcrumb > li {
        color: #666 !important;
        font-weight: bold;
    }
    .breadcrumb > li > a {
        color: inherit;
    }
    .breadcrumb > li.active {
        color: #666 !important;
        font-weight: normal;
    }
    #k-listing {
        margin-top: 0px;
        padding-top: 30px;
    }

    #hot-courses h3 {
        display: none;
    }

    @media (min-width: 768px) {
         h2.mobile{
            display: none !important;
        }
        
        .st-combo.mobile{
            display: none !important;
        }
        
        #hot-courses h3 {
            display: block;
            color: #50ad4e;
            font-size: 18px;
            padding-left: 15px;
            padding-right: 14px;
            padding-bottom: 10px;
            padding-top: 10px;
        }

        #best-seller-courses h3 {
            display: block;
            color: #50ad4e;
            font-size: 18px;
            padding-left: 15px;
        }

    }
    
    .hot-category .name {
        position: absolute;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        text-align: center;
        padding-top: 22%;
        font-size: large;
        font-weight: bolder;
        color: #FFFFFF;
    }

    .hot-category:hover .overlay {
        width:100%;
        height:100%;
        position:absolute;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        background-color:#000;
        opacity:0.2;
        border-radius:1px;
    }
    #hot-courses{
        margin-top: 40px;
        border-top: 1px solid #a0a0a0;
        padding-top: 30px;
    }
    #hot-courses h3{
        font-size: 20px;
    }
    
");

?>



<main>
    <div id="k-shopping" class="container k-height-header">
        <?= CampaignTetBanner::widget()?>
        <div class="k-shopping-list">
            <?= Alert::widget() ?>

            <?php if ($totalCount) : ?>
                <section>
                    <div class="k-header-shopping-list clearfix">
                        <div class="k-header-shopping-list-left">
                            <img src="<?= $cdnUrl ?>/img/cart/icon-cart-checkout.png" alt="">
                            <div class="k-header-shopping-list-left-info">
                                <div class="k-header-shopping-list-left-info-tit">Thông tin giỏ hàng</div>
                                <div class="k-header-shopping-list-left-info-des">
                                    <span><?= $totalCount ?></span> <span class="pc">khóa học,</span><span class="mob"><b>khóa học</b> đã chọn</span> <span><?= $formatter->asCurrency($totalPaymentAmount) ?></span>
                                </div>
                            </div>
                        </div>
                        <div class="k-header-shopping-list-right">
                            <a href="<?= Url::toRoute(['/cart/checkout/index']) ?>" class="btn-payment"><?= ($cart->isFreeOrder) ? 'Xác nhận đăng ký' : 'TIẾP TỤC THANH TOÁN' ?></a>
                        </div>
                    </div>
                </section>
                <section>
                    <?php
                    $form = ActiveForm::begin([
                                'action' => Url::toRoute(['/cart/default/remove']),
                                'id' => 'cart-form',
                                'method' => 'post'
                            ])
                    ?>
                    <input type="hidden" name="pids[]"/>
                    <ol class="k-shopping-list-items list-unstyled">
                        <?php foreach ($cartItems as $item) : ?>
                            <li class="items">
                                <?= $this->render('_cart_item', ['item' => $item]) ?>
                            </li>
                        <?php endforeach; ?>
                        <?php $isMobile = Yii::$app->devicedetect->isMobile()
                            || Yii::$app->devicedetect->isTablet();
                        $promotionCode = Yii::$app->cart->getPromotionCode();

                        $totalPrice = 0;
                        if(CampaignTet::InTimesCampaign()){
                            foreach ($cartItems as $item)
                            {
//                                echo $item->getId();
                                /* @var $item app\modules\cart\models\Product */
                                if(!in_array($item->getId(),CampaignTet::COURSE_NOT_APPLY_VOUCHER)){
                                    $totalPrice += $item->getPrice();
                                }
                            }
                        }

                        ?>
                        <?php if(CampaignTet::InTimesCampaign() && !$isMobile && CampaignTet::isVoucherLixTet($promotionCode)):?>
                            <li class="items">
                                <div class="k-shopping-list-items-title">
                                    <div class="items-img">
                                        <strong>Giảm giá Lì Xì Hot(-<?= CampaignTet::percentLiXiTet($totalPrice)?>%)</strong>
                                    </div>

                                    <div class="items-text">
                                        <h4>
                                            <a href="/nuoi-day-con-tu-ky" title="Nuôi dạy con tự kỷ"><b></b></a>
                                        </h4>
                                        <!-- <p>Đào Hải Ninh / Giám đốc trung tâm Giáo dục Tuệ Quang</p> -->
                                        <div class="k-shopping-list-items-group-price -mob">
                                            <span class="orange">VND 199,000.00</span> <span><s>(VND 400,000.00)</s></span>
                                        </div>

                                    </div>
                                    <!--end .text-->
                                </div>
                                <!--end .title-->

                                <div class="k-shopping-list-items-group-price">
                                    <div class="k-shopping-list-items-price-old">
                                        <span><s></s></span>
                                    </div>
                                    <div class="k-shopping-list-items-sale">
                                        <span><?= $formatter->asCurrency( -$totalPrice* CampaignTet::percentLiXiTet($totalPrice)/100) ?></span>
                                    </div>
                                    <div class="k-shopping-list-items-price-new">
                                        <span></span>
                                    </div>
                                </div>
                            </li>
                        <?php endif;?>
                    </ol>
                    <?php ActiveForm::end() ?>
                </section>
                <section>
                    <?php if(Yii::$app->params['campaign_birthday']): ?>
                    <div class="k-shopping-checkout-total-price clearfix">
                        <div class="k-shopping-checkout-total-price-text" style="width: 50%">
                            <span>TỔNG GIÁ CAMPAIGN SINH NHẬT</span>
                            <label for="">Tổng giá campaign sinh nhật</label>
                        </div>
                        <div class="k-shopping-list-items-group-price" style="width: 50%">
                            <div class="k-shopping-checkout-total-price-new">
                                <span><?= $formatter->asCurrency(GroupDiscount::getTotalPriceBirthday($cartItems)) ?></span>
                            </div>
                        </div>
                    </div>
                    <?php endif;?>
                    <div class="k-shopping-checkout-total-price clearfix">
                        <div class="k-shopping-checkout-total-price-text">
                            <span>TỔNG THÀNH TIỀN</span>
                            <label for="">Học phí gốc</label>
                            <label for="">Giảm giá</label>
                            <?php if(CampaignTet::InTimesCampaign() && CampaignTet::isVoucherLixTet($promotionCode)): ?>
                                <label for="">Lì Xì(-<?= CampaignTet::percentLiXiTet($totalPrice)?>%)</label>
                            <?php endif;?>
                            <label for="">Tổng cộng</label>
                        </div>
                        <div class="k-shopping-list-items-group-price">
                            <div class="k-shopping-checkout-total-price-old">
                                <span><s><?= $formatter->asCurrency($totalCost) ?></s></span>
                            </div>
                            <?php if(CampaignTet::InTimesCampaign()&& CampaignTet::isVoucherLixTet($promotionCode)): ?>
                                <?php $lixi = $totalPrice* CampaignTet::percentLiXiTet($totalPrice)/100;?>
                                <div class="k-shopping-checkout-total-price-sale">
                                    <span><?= $formatter->asCurrency(-$totalDiscount ) ?></span>
                                </div>
                            <?php else:?>
                                <div class="k-shopping-checkout-total-price-sale">
                                    <span><?= $formatter->asCurrency(-$totalDiscount) ?></span>
                                </div>
                            <?php endif;?>

                            <?php if(CampaignTet::InTimesCampaign()): ?>
                                <?php if($isMobile && CampaignTet::isVoucherLixTet($promotionCode)):?>
                                    <div class="k-shopping-checkout-total-price-sale">
                                        <span><?= $formatter->asCurrency( $totalPrice* CampaignTet::percentLiXiTet($totalPrice)/100) ?></span>
                                    </div>
                                <?php endif;?>
                            <?php endif;?>
                            <div class="k-shopping-checkout-total-price-new">
                                <span><?= $formatter->asCurrency($totalPaymentAmount) ?></span>
                            </div>
                        </div>
                    </div>
                    <div class="k-shopping-checkout-choose-another">
                        <a href="<?= Url::toRoute(['/course/default/index']); ?>"><img src="<?= $cdnUrl ?>/img/icon-arrow-left.png" alt=""> Chọn thêm khóa học khác</a>
                    </div>

                </section>
                <section>
                    <div class="k-shopping-checkout-note clearfix">
                        <ul>
                            <li><img src="<?= $cdnUrl ?>/img/cart/icon-cart-note-1.png" alt="">Hoàn tiền trong 30 ngày</li>
                            <li><img src="<?= $cdnUrl ?>/img/cart/icon-cart-note-2.png" alt="">Các phương thức thanh toán linh hoạt</li>
                            <li><img src="<?= $cdnUrl ?>/img/cart/icon-cart-note-3.png" alt="">Nội dung học liên tục, xuyên suốt</li>
                        </ul>
                    </div>
                </section>

                <!-- start hot courses-->
                <?php if (!empty($hotCourses) && !empty($hotCoursesMobile)) {?>
                    <?php
                        if(Yii::$app->devicedetect->isMobile()){
                            $hotCourses = $hotCoursesMobile;
                        }else if(Yii::$app->devicedetect->isTablet()){
                            $hotCourses = $hotCourses;
                        }
                    ?>
                    <section id="hot-courses">
                        <h3 style="color:#50ad4e;">
                            <strong>Thường được mua cùng</strong>
                        </h3>
                        <div class="box">
                            <?= $this->render('_list_hot', [
                                'dataProvider' => $hotCourses,
                            ]);
                            ?>
                        </div>

                    </section>
                <?php }; ?>
                <!-- end hot courses -->

            <?php else: ?>
                <div class="cart-empty clearfix">
                    <p class="title">Bạn chưa chọn được khóa học nào!</p>
                    <img src="<?= $cdnUrl ?>/img/cart/icon-checkout-empty-1.png" class="img-fluid"/>
                </div><!--end .cart-empty-->
                <div id="k-learn-what-today">
                    <h2 class="title">Bạn có thể tìm kiếm khóa học theo các chủ đề sau</h2>
                    <div class="row">
                        <?php foreach($desktopTags as $tag) { ?>
                            <div class="item">
                                <a href="<?= Url::toRoute(['/course/default/index', 'tag' => $tag->slug]); ?>">
                                    <?= CDNHelper::image($tag->image_url, [
                                        'alt' => $tag->tag,
                                        'class' => '',
                                        'size' => CDNHelper::IMG_SIZE_ORIGINAL,
                                        'resizeMode' => 'cover',
                                    ]) ?>
                                    <div class="text">
                                        <span><?= $tag->tag ?></span>
                                    </div>
                                </a>
                            </div>
                        <?php } ?>
                    </div>
                </div>
                <div class="k-learn-related">
                    <div class="title">Khóa học nổi bật</div>
                    <ul class="clearfix k-box-card-list">
                        <?php foreach ($highLightCourses as $course) { ?>
                            <li class="col-xl-3 col-lg-4 col-md-6 col-xs-12 k-box-card">
                                <?= $this->render('@app/modules/course/views/default/_box_product', ['model' => $course]); ?>
                            </li>
                        <?php } ?>
                    </ul>
                </div>
            <?php endif; ?>
        </div>

    </div><!--end #k-cart-->

</main>
<?php if (!empty($cartItems)): ?>
<div class="group-btn-inpayment-mb">
    <div class="container">
        <a href="/danh-sach-khoa-hoc" class="back-to-list-courses hidden-sm-down"><img src="<?= $cdnUrl ?>/img/icon-arrow-left.png" alt=""> Chọn thêm khóa học khác</a>
        <?php if(Yii::$app->params['campaign_birthday']):?>
            <a href="<?= Url::toRoute(['/course/default/index', 'tag' => 'giamsinhnhatkyna']) ?>" class="banner"><img src="<?= $cdnUrl ?>/img/sinh-nhat-kyna/giohangchitiet.png?v=2" alt=""></a>
        <?php endif;?>
        <a href="<?= Url::toRoute(['/cart/checkout/index']) ?>" class="btn-payment-mb"><?= ($cart->isFreeOrder) ? 'XÁC NHẬN ĐĂNG KÝ' : 'TIẾP TỤC THANH TOÁN' ?></a>
    </div>

</div>
<?php endif; ?>
<?php
$script = "
    $('document').ready(function() {
        $('body').on('click', '.cart-item-remove', function () {
            var pid = $(this).data('id');
            $('input[name=\'pids[]\']').val(pid);
            $('#cart-form').submit();
        });
    });
";
$this->registerJs($script, View::POS_END, 'cart-remove-js');
$this->registerJsFile($cdnUrl . '/src/js/add-to-cart.js', ['position' => View::POS_END]);
?>
