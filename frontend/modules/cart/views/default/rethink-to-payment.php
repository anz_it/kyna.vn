<?php

use yii\helpers\Url;
use common\helpers\CDNHelper;

$cdnUrl = CDNHelper::getMediaLink();
?>
<div class="modal-body">
    <img src="<?= $cdnUrl ?>/src/img/popup/popup.png" alt="" class="img-fluid">
    <div class="content">
        <div class="text">
            <p><span><?= Yii::$app->cart->count ?></span> khóa học bạn chọn vẫn còn nằm trong giỏ hàng. <br/>Điều gì khiến bạn không muốn học những khóa học ấy.</p>
            <ul>
                <li><a href="#" data-dismiss="modal">Tiếp tục xem trang</a></li>
                <li><a href="<?= Url::to(['/cart/checkout/index']) ?>">Thanh toán giỏ hàng</a></li>
            </ul>
        </div><!--end .text-->
    </div><!--end .content-->
</div>