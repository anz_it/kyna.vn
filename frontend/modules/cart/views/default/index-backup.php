<?php

use yii\helpers\Url;
use yii\web\View;
use yii\widgets\ActiveForm;
use common\widgets\Alert;
use common\helpers\CDNHelper;

$cdnUrl = CDNHelper::getMediaLink();

$formatter = \Yii::$app->formatter;

$this->title = "Giỏ hàng của bạn";
$cartItems = array_reverse($cart->getPositions());
$totalCost = $cart->getCost();
$totalCount = $cart->getCount();

$settings = Yii::$app->controller->settings;

// TODO: get hot key words by search
$hotKeywords = !empty($settings['hot_keyword']) ? explode(',', $settings['hot_keyword']) : [
    'Giao tiếp',
    'Monkey junior',
    'Bán hàng',
    'Giáo dục sớm',
    'Tiếng anh',
    'Marketing',
    'Bất động sản',
    'Tiếng hoa',
    'Tiếng Nhật'
];
?>

<main>
    <div id="k-shopping" class="container k-height-header">
        <div class="col-lg-8 col-xs-12 k-shopping-list">
            <?= Alert::widget() ?>

            <?php if ($totalCount) : ?>
                <h3><span><?= $totalCount ?> khóa học</span> đã chọn</h3>
                <header class="clearfix k-shopping-list-header">
                    <div class="col-md-8 col-sm-8 col-xs-8 title">Khóa học</div>
                    <div class="col-md-2 col-sm-2 col-xs-2 number">Số lượng</div>
                    <div class="col-md-2 col-sm-2 col-xs-2 price">Giá</div>
                </header>
                <section>
                    <?php
                    $form = ActiveForm::begin([
                                'action' => Url::toRoute(['/cart/default/remove']),
                                'id' => 'cart-form',
                                'method' => 'post'
                            ])
                    ?>
                    <input type="hidden" name="pids[]"/>
                    <ol class="k-shopping-list-items list-unstyled">
                        <?php foreach ($cartItems as $item) : ?>
                            <li class="items">
                                <?= $this->render('_cart_item', ['item' => $item]) ?>
                            </li>
                        <?php endforeach; ?>
                    </ol>
                    <?php ActiveForm::end() ?>
                </section>
                <section>
                    <div class="k-shopping-checkout">
                        <ul class="k-shopping-checkout-total-price list-unstyled">
                            <li>Học phí gốc</li>
                            <li class="price"><?= $formatter->asCurrency($totalCost) ?></li>
                            <li>Tổng cộng</li>
                            <li class="price-total"><?= $formatter->asCurrency($cart->getCost(true)) ?></li>
                        </ul>

                        <!--
                        <form class="form-inline">
                            <div class="form-group">
                              <input type="text" value="" name="" id="" placeholder="Nhập mã khuyến mãi" class="text form-control">
                            </div>
                            <button type="submit" class="btn btn-primary">Áp dụng</button>
                        </form>
                        -->

                        <ul class="k-shopping-checkout-button list-unstyled">
                            <li>
                                <a href="<?= Url::toRoute(['/course/default/index']) ?>"><i class="icon icon-caret-left"></i> Chọn thêm khóa học khác</a>
                            </li>
                            <li>
                                <a href="<?= Url::toRoute(['/cart/checkout/index']) ?>" class="btn-payment"><?= ($cart->isFreeOrder) ? 'Xác nhận đăng ký' : 'Tiếp tục chọn cách thanh toán' ?> <i class="icon icon-caret-right"></i></a>
                            </li>
                        </ul>
                    </div><!--end k-shopping-checkout-->
                </section>
            <?php else: ?>
                <div class="cart-empty clearfix">
                    <div class="col-sm-5 col-xs-12 img">
                        <img src="<?= $cdnUrl ?>/img/cart/icon-checkout-empty.png" class="img-fluid"/>
                    </div><!--end .img-->
                    <div class="col-sm-7 col-xs-12 text">
                        <h4>Bạn chưa chọn khóa học nào<span>Hãy bắt đầu lựa chọn khóa học bạn muốn</span></h4>
                        <span class="button"><a href="<?= Url::toRoute(['/course/default/index']) ?>" class="background-green hover-bg-green">Xem tất cả khóa học</a></span>
                        <p>Bạn cũng có thể tìm kiếm những chủ đề nổi bất trên Kyna</p>
                        <ul>
                            <?php foreach ($hotKeywords as $keyword) : ?>
                                <li><a href="<?= Url::toRoute(['/course/default/index', 'q' => $keyword]) ?>" title="<?= $keyword ?>"><?= $keyword ?></a></li>
                            <?php endforeach; ?>
                        </ul>
                    </div><!--end .text-->
                </div><!--end .cart-empty-->
            <?php endif; ?>
        </div><!--end .k-shopping-list-->
        <div class="col-lg-4 col-xs-12 k-shopping-sidebar">
            <?= $this->render("_why_learn_kyna") ?>
        </div><!--end .k-shopping-sidebar-->

    </div><!--end #k-cart-->

</main>

<?php
$script = "
    $('document').ready(function() {
        $('body').on('click', '.cart-item-remove', function () {
            var pid = $(this).data('id');
            $('input[name=\'pids[]\']').val(pid);
            $('#cart-form').submit();
        });
    });
";
$this->registerJs($script, View::POS_END, 'cart-remove-js');
?>
