<?php

use yii\helpers\Url;
use yii\web\View;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use app\modules\cart\models\form\UserInfoForm;
use common\helpers\CDNHelper;

$cdnUrl = CDNHelper::getMediaLink();

$model = new UserInfoForm();
?>

<!-- Modal -->
<div class="modal-body user-info-form">
    <img src="<?= $cdnUrl ?>/src/img/popup/popup.png" alt="" class="img-fluid">
    <div class="content">
        <div class="text">
            <p><span><?= Yii::$app->cart->count ?></span> khóa học bạn chọn vẫn còn nằm trong giỏ hàng. Điều gì khiến bạn không muốn học những khóa học ấy.</p>
            <h4>Lưu số điện thoại của bạn tại đây để được hỗ trợ tư vấn và nhận SMS khuyến mãi cho những khóa mà bạn đã chọn</h4>
            <?php $form = ActiveForm::begin([
                'id' => 'user-info-form',
                'options' => ['class' => 'navbar-form navbar-left'],
                'action' => Url::toRoute(['/cart/default/add-user-info'])
            ]) ?>
                <?= $form->errorSummary($model, ['header' => '', 'class' => 'error-summary alert alert-warning']) ?>

                <?= $form->field($model, 'phone_number', [
                    'options' => ['class' => 'form-group col-sm-7 col-xs-12'],
                    'template' => '{input}{hint}'
                ])->textInput(['placeholder' => 'Số điện thoại của tôi là ...'])->label(false) ?>

                <div class="col-sm-5 col-xs-12">
                    <?= Html::submitButton('Nhận tin khuyến mãi', ['class' => 'btn btn-default send']) ?>
                </div>
            <?php ActiveForm::end() ?>
        </div><!--end .text-->
    </div><!--end .content-->
</div>

<?php
$script = '
    $(document).ready(function(){
        $("body").on("submit", "#user-info-form", function (event) {
            event.preventDefault();

            var data = $(this).serializeArray();

            $.post("/cart/default/add-user-info", data, function (response) {
                if (response.result) {
                    $(\'.user-info-form .content\').html(\'<div class="alert alert-success"><span>Cảm ơn bạn đã để lại thông tin, chúng tôi sẽ liên hệ sớm nhất có thể để tư vấn các khóa học cho bạn.</span></div>\');
                } else {
                    // login failed
                    var liHtml = "";
                    $.each(response.errors, function (key, value){
                        liHtml += "<li>" + value[0] + "</li>";
                    });

                    $("#user-info-form .error-summary ul").html(liHtml);
                    $("#user-info-form .error-summary").show();
                }

            });
        });
    });
';

$this->registerJs($script, View::POS_END, 'add-user-info');
?>
