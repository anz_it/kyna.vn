<?php
/**
 * Created by PhpStorm.
 * User: nguyenphanngu
 * Date: 9/8/17
 * Time: 10:57 AM
 */

/**
 * @var $this \yii\web\View
 */

use yii\helpers\StringHelper;
use common\helpers\CDNHelper;
use kyna\course\models\Course;
use yii\widgets\ListView;
use yii\web\View;

$formatter = \Yii::$app->formatter;
?>
<?= ListView::widget([
    'dataProvider' => $dataProvider,
    'layout' => "{items}\n",
    'itemView' => function ($model) {
        $model = (object)$model;
        return $this->render($model->type == Course::TYPE_COMBO ? '_box_combo' : '_box_product', ['model' => $model]);
    },
    'options' => [
        'tag' => 'ul',
        'class' => 'clearfix k-box-card-list hot-courses-container'
    ],
    'itemOptions' => [
        'tag' => 'li',
        'class' => 'col-xl-3 col-lg-4 col-md-6 col-xs-12 k-box-card'
    ],
//    'pager' => [
//        'prevPageLabel' => '<span>&laquo;</span>',
//        'nextPageLabel' => '<span>&raquo;</span>',
//        'options' => [
//            'class' => 'pagination',
//        ]
//    ]
])
?>
<?php
    $hotBoxJx = "      
       var slickSettings_1025px = {
            infinite: true,
            // dots: true,
            slidesToShow: 2,
            slidesToScroll: 2,
             autoplay: true,
             autoplaySpeed: 5000
        }
        
        var slickSettings_1200px = {
            infinite: true,
            // dots: true,
            slidesToShow: 2,
            slidesToScroll: 1,
             autoplay: true,
             autoplaySpeed: 5000
        }
        
        var slickSettings_769px = {
            infinite: true,
            // dots: true,
            slidesToShow: 2,
            slidesToScroll: 2,
             autoplay: true,
             autoplaySpeed: 5000
        }
        var slickSettings_426px = {
            infinite: true,
             dots: true,
             slidesToShow: 1,
             slidesToScroll: 1,
             autoplay: true,
             autoplaySpeed: 5000
        }
        
        var slickSettings = {
            infinite: true,
            dots: true,
            slidesToShow: 4,
            slidesToScroll: 1,
            autoplay: false,
            autoplaySpeed: 5000,
            adaptiveHeight: true,
            prevArrow: $('.previous'),
            nextArrow: $('.next'),
            responsive: [
                {
                    breakpoint: 1200,
                    settings: slickSettings_1200px
                },
                {
                    breakpoint: 1025,
                    settings: slickSettings_1025px
                },
                {
                    breakpoint: 769,
                    settings: slickSettings_769px
                },
                {
                    breakpoint: 426,
                    settings: slickSettings_426px
                }
                
            ]
        };
        
        $(document).ready(function() {
            $('.hot-courses-container').slick(slickSettings);
        });
    ";

    $hotBoxCss = "
        .slider-slick{
            float: right;
        }

        .slider-slick a {
            text-decoration: none;
            display: inline-block;
            padding: 1px 11px;
        }

        .slider-slick a:hover {
            background-color: #ddd;
            color: black;
        }

        .slider-slick .previous {
            background-color: #4CAF50;
            color: white;
            font-size: 22px;
        }

        .slider-slick .next {
            background-color: #4CAF50;
            color: white;
            font-size: 22px;
        }

        slider-slick. .round {
            border-radius: 50%;
        }
        

        .slick-dots {
            position: absolute;
            bottom: 10px;
        }
    ";

    $this->registerJs($hotBoxJx, View::POS_END);
    $this->registerCss($hotBoxCss, ['position' => View::POS_END]);
?>
