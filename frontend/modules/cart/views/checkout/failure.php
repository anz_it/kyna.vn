<?php

use yii\helpers\Url;
use common\helpers\CDNHelper;

$this->title = "Thanh toán thất bại";

$settings = $this->context->settings;
$hotline = !empty($settings['hotline']) ? $settings['hotline'] : '1900.6364.09';
$supportMail = !empty($settings['email_footer']) ? $settings['email_footer'] : 'hotro@kyna.vn';

$cdnUrl = CDNHelper::getMediaLink();

echo $this->render("./failure/_failure_header", ['order' => $order]);
echo $this->render("./common/_order_header_summary", ['order' => $order]);
?>
<div class="checkout-confirm-content">

    <p class="checkout-confirm-failed-text">Cám ơn bạn đã đăng ký khóa học trên <span href="<?= Url::base() ?>"
                                                                                      class="bold color-green">Kyna.vn</span>.
        Tuy nhiên, thanh toán của bạn qua <?= $order->paymentMethodName ?> không thành công.</p>
    <div class="checkout-confirm-part-1 box clearfix">
        <div class="col-lg-3 col-sm-4 col-xs-12 img pd0">
            <img src="<?= $cdnUrl ?>/img/icon-checkout-confirm-1.png" alt="Kyna.vn" class="img-fluid"/>
        </div><!--end .col-lg-3 col-sm-4 col-xs-12 img -->
        <div class="col-lg-9 col-sm-8 col-xs-12 text">
            <h4>Bước 1</h4>
            <p>Bạn sẽ nhận email thông báo đơn hàng và hướng dẫn bạn thanh toán lại cho đơn hàng thông qua email
                <span class="bold"><?= $order->user->email ?></span>.</p>
        </div><!--end .col-lg-9 col-sm-8 col-xs-12 text-->
    </div><!--end .checkout-confirm-part-1-->

    <div class="checkout-confirm-part-2 box clearfix">
        <div class="col-lg-3 col-sm-4 col-xs-12 img pd0">
            <img src="<?= $cdnUrl ?>/img/icon-checkout-confirm-failed-2.png" alt="Kyna.vn" class="img-fluid"/>
        </div><!--end .col-lg-3 col-sm-4 col-xs-12 img -->
        <div class="col-lg-9 col-sm-8 col-xs-12 text">
            <h4>Bước 2</h4>
            <p>Trong email sẽ bao gồm link giúp bạn thanh toán lại cho đơn hàng không thành công. Bạn có thể click
                vào link và thử thanh toán lại (Lưu ý, bạn cần đăng nhập khi thanh toán).</p>
        </div><!--end .col-lg-9 col-sm-8 col-xs-12 text-->
    </div><!--end .checkout-confirm-part-2-->

    <p class="checkout-confirm-failed-text">Với bất kì thắc mắc nào, bạn có thể liên hệ qua hotline <span><a
                href="tel:<?= $hotline ?>" class="color-green bold"><?= $hotline ?></a></span> hoặc email đến <span><a
                href="mailto:<?= $supportMail ?>" class="color-green bold"><?= $supportMail ?></a></span>.</p>

</div><!--end .checkout-confirm-content -->

<div class="checkout-confirm-ul-bottom">
    <ul>
        <li><a href="<?= Url::toRoute(['/course/default/index']) ?>"><i class="fa fa-caret-left"></i> Chọn thêm khóa
                học khác</a></li>
        <!--<li><a href="<?= Url::toRoute(['/cart/default/index']) ?>">Quay lại giỏ hàng</a></li>-->
        <li><a href="<?= Url::toRoute(['/cart/checkout/index']) ?>">Thanh toán lại</a></li>
    </ul>
</div><!--end .checkout-confirm-ul-bottom-->
