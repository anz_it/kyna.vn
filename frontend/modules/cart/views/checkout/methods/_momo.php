<?php
if($visible):
?>

<div class="checkout-list" id="checkout-momo">
    <input id="radio-momo" type="radio" name="PaymentForm[method]" value="momo" <?php if ($model->method == 'momo') {echo 'checked';} ?>>
    <label for="radio-momo">
        <span><span></span></span><methodName style="background-image: url('/img/checkout/momo.png')"><span ><?= $methodName ?></span></methodName>
    </label>
    <div class="checkout-wrap-list">
        <div class="checkout-sub-list" id="checkout-show-momo">
            <p>Sau khi điền thông tin mua hàng và bấm hoàn tất đơn hàng, hệ thống sẽ hiển thị mã QR kèm hướng dẫn. Bạn cần tải và cài
                ứng dụng Momo trên điện thoại và sử dụng để quét mã QR trên để thanh toán. Momo-Phương thức thanh toán nhanh, tiện lợi, an toàn
                được cấp phép và quản lý bởi NH Nhà Nước Việt Nam.
            </p>
            <p style="color: green; font-weight: bold">Khóa học sẽ được kích hoạt ngay sau khi bạn thanh toán thành công</p>

        </div>
        <!--end .checkout-sub-list-->
    </div><!--end .checkout-wrap-list-->
</div><!--end .checkout-list-->
<?php endif;?>
