<?php

$cdnUrl = \common\helpers\CDNHelper::getMediaLink();

if($visible):
?>

<div class="checkout-list" id="checkout-payoo">
    <?php echo $model->method;?>
    <input id="radio-payoo" type="radio" name="PaymentForm[method]" value="payoo" <?php if ($model->method == 'payoo') {echo 'checked';} ?>>
    <label for="radio-payoo">
        <span><span></span></span><methodName style="background-image: url('<?= $cdnUrl?>/img/payoo/payoo.png')"><span ><?= $methodName ?></span></methodName>
    </label>
    <div class="checkout-wrap-list">
        <div class="checkout-sub-list" id="checkout-show-payoo">
            <span>Sau khi điền thông tin mua hàng và hoàn tất đơn hàng, hệ thống sẽ hiển thị mã Code. Bạn có thể thanh toán
                bằng mã code tại các cửa hàng tiện lợi sau:
            </span>
            <ul class="horizontal-list">
                <li><img src="<?= $cdnUrl ?>/img/payoo/bsmart.jpg"></li>
                <li><img src="<?= $cdnUrl ?>/img/payoo/circlek.jpg"></li>
                <li><img src="<?= $cdnUrl ?>/img/payoo/ministop.jpg"></li>
                <li><img src="<?= $cdnUrl ?>/img/payoo/cocomart.jpg"></li>
                <li><img src="<?= $cdnUrl ?>/img/payoo/familymart.jpg"></li>
                <li><img src="<?= $cdnUrl ?>/img/payoo/mediamart.jpg"></li>
                <li><img src="<?= $cdnUrl ?>/img/payoo/ecomart.jpg"></li>
                <li><img src="<?= $cdnUrl ?>/img/payoo/citimart.jpg"></li>
                <li><img src="<?= $cdnUrl ?>/img/payoo/vinmart.jpg"></li>
                <li><img src="<?= $cdnUrl ?>/img/payoo/vinpro.jpg"></li>
                <li><img src="<?= $cdnUrl ?>/img/payoo/vinmartplus.jpg"></li>
                <li><img src="<?= $cdnUrl ?>/img/payoo/fptshop.jpg"></li>
                <li><img src="<?= $cdnUrl ?>/img/payoo/dienmaycholon.jpg"></li>
                <li><img src="<?= $cdnUrl ?>/img/payoo/phucanh.jpg"></li>
                <li><img src="<?= $cdnUrl ?>/img/payoo/hnammobile.jpg"></li>
                <li><img src="<?= $cdnUrl ?>/img/payoo/longhung.jpg"></li>
                <li><img src="<?= $cdnUrl ?>/img/payoo/bkc.jpg"></li>
            </ul>
            <span>Danh sách cửa hàng gần nhà bạn</span>
            <a href="https://payoo.vn/map/public/?verify=true#"
               onclick="event.preventDefault();event.stopPropagation();$.fancybox.open({&quot;href&quot;:&quot;https://www.payoo.vn/map/index.php&quot;,&quot;wrapCSS&quot;:&quot;payoo&quot;,&quot;helpers&quot;:{&quot;overlay&quot;:{&quot;closeClick&quot;:true}},&quot;tpl&quot;:{&quot;closeBtn&quot;:&quot;<a title=\&quot;Đóng\&quot; class=\&quot;fancybox-item fancybox-close\&quot; href=\&quot;javascript:;\&quot;></a>&quot;},&quot;type&quot;:&quot;iframe&quot;,title: this.title});" class="map" title="Tìm cửa hàng theo bản đồ">(Xem danh sách)</a>
        </div>
        <!--end .checkout-sub-list-->
    </div><!--end .checkout-wrap-list-->
</div><!--end .checkout-list-->
<?php endif;?>
