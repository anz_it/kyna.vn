<?php

use yii\helpers\Html;

?>
<?php if ($visible) { ?>
    <div class="checkout-list" id="checkout-card">
        <input id="radio-mobile-card" type="radio" name="PaymentForm[method]"
               value="epay" <?php if ($model->method == 'epay') {
            echo 'checked';
        } ?>>
        <label for="radio-mobile-card"><span><span></span></span><methodName><span>Thẻ cào điện thoại</span></methodName></label>
        <div class="checkout-wrap-list">
            <div class="checkout-sub-list" id="checkout-show-epay">
                Thẻ cào điện thoại các nhà mạng: Vinaphone, Mobiphone, Viettel.<br>
                <b>Kyna.vn</b> sẽ hoàn lại số tiền dư sau khi thanh toán thành công vào số điện thoại mà bạn cung cấp.
                <!-- Old -->
                <!-- <div class="alert alert-warning">
                    <p><b>Kyna.vn</b> sẽ hoàn lại số tiền dư sau khi bạn thanh toán thành công vào số điện thoại mà bạn cung cấp.</p>
                    <?php if(!empty($order) && !empty($response)) { ?>
                        <?php
                            $remain = $order->realPayment - $response['amount'];
                        ?>
                        <p>Bạn đã thanh toán <b><?= \Yii::$app->formatter->asCurrency($response['amount']) ?></b> cho đơn hàng <b>#<?= $order->id;  ?></b>.
                            Bạn cần phải thanh toán thêm <b><?= \Yii::$app->formatter->asCurrency($remain); ?></b>. Vui lòng tiếp tục thanh toán.
                        </p>
                    <?php } ?>
                </div> -->
                <!-- <div class="input-group checkout-radio-card">
                    <div class="col-sm-4 col-xs-12 left pd0">
                        <?= Html::activeLabel($model, 'serviceCod') ?>
                    </div>
                    <div class="col-sm-8 col-xs-12 right">
                        <ul>
                            <li>
                                <input id="radio-mobile-card-type-viettel" type="radio" name="PaymentForm[serviceCode]"
                                       value="VTT">
                                <label for="radio-mobile-card-type-viettel"><span><span></span></span>Viettel</label>
                            </li>
                            <li>
                                <input id="radio-mobile-card-type-mobi" type="radio" name="PaymentForm[serviceCode]"
                                       value="VMS">
                                <label for="radio-mobile-card-type-mobi"><span><span></span></span>Mobiphone</label>
                            </li>
                            <li>
                                <input id="radio-mobile-card-type-vina" type="radio" name="PaymentForm[serviceCode]"
                                       value="VNP">
                                <label for="radio-mobile-card-type-vina"><span><span></span></span>Vinaphone</label>
                            </li>
                        </ul>
                    </div>
                </div> --><!-- /input-group -->
                <!-- <div class="input-group">
                    <div class="col-sm-4 col-xs-12 left pd0">
                        <?= Html::activeLabel($model, 'pinCode') ?>
                    </div>
                    <div class="col-sm-8 col-xs-12 right">
                        <?= $form->field($model, 'pinCode')->textInput(['placeholder' => 'Nhập chữ số dưới lớp bạc'])->label(false) ?>
                    </div>
                </div>--><!-- /input-group -->
                <!--<div class="input-group">
                    <div class="col-sm-4 col-xs-12 left pd0">
                        <?= Html::activeLabel($model, 'serial') ?>
                    </div>
                    <div class="col-sm-8 col-xs-12 right">
                        <?= $form->field($model, 'serial')->textInput(['placeholder' => 'Nhập số seri in trên thẻ cào'])->label(false) ?>
                    </div>
                </div>--><!-- /input-group -->
                <!-- Old -->
            </div><!--end .checkout-sub-list-->
        </div><!--end .checkout-wrap-list-->
    </div><!--end .checkout-list-->
<?php } ?>
