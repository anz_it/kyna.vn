<?php

use yii\helpers\Html;

?>
<?php if ($visible) { ?>
    <div class="checkout-list" id="checkout-card">
        <input id="radio-mobile-card" type="radio" name="PaymentForm[method]"
               value="ipay" <?php if ($model->method == 'ipay') {
            echo 'checked';
        } ?>>
        <label for="radio-mobile-card"><span><span></span></span><methodName><span>Thẻ cào điện thoại</span></methodName></label>
        <div class="checkout-wrap-list">
            <div class="checkout-sub-list" id="checkout-show-ipay">
                Thẻ cào điện thoại các nhà mạng: Vinaphone, Mobiphone, Viettel.<br>
                <b>Kyna.vn</b> sẽ hoàn lại số tiền dư bằng cách gửi tặng Voucher tương ứng số tiền dư.
                Nếu số tiền dư < 50.000đ thì bạn sẽ nhận được voucher 50.000đ, từ 50.000đ-100.000đ tương ứng voucher 100.000đ, trên 100.000đ tương ứng nhiều voucher 100.000đ (số lượng được làm tròn lên).
                Bạn có thể xem voucher và sử dụng bằng cách vào menu <?= Html::a("Voucher", \yii\helpers\Url::to(['/user/gift/index']))?> trong trang khóa học của tôi.
            </div><!--end .checkout-sub-list-->
        </div><!--end .checkout-wrap-list-->
    </div><!--end .checkout-list-->
<?php } ?>
