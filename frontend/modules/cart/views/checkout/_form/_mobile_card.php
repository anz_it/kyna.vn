<?php
/**
 * Created by PhpStorm.
 * User: macintosh
 * Date: 6/26/17
 * Time: 2:35 PM
 */
/* @var $paymentForm \app\modules\cart\models\form\PaymentForm */
/* @var $order \kyna\order\models\Order */

use yii\helpers\Html;
use kyna\payment\models\PaymentTransaction;
use common\helpers\CDNHelper;

$cdnUrl = CDNHelper::getMediaLink();

$paymentTransactions = [];
if ($order != null) {
    $paymentTransactions = $order->getPaymentTransactions()
        ->where(['payment_method' => ['epay', 'ipay']])
        ->andWhere(['status' => PaymentTransaction::STATUS_SUCCESS])
        ->andWhere(['is not', 'amount', null])->all();
}
?>

<div class="detail-method-pay">
    <div class="clearfix" data-render="checkout-show-mobile-card">
        <br>
        <h3>NẠP THẺ CÀO</h3>
        <div class="wrap-card-mobile">
            <div class="box">
                <div class="tit">Bạn đã nạp thành công: <?= count($paymentTransactions) ?> thẻ <?php if (count($paymentTransactions) > 0) : ?>(<a href="#" class="see-history"><i>Xem lịch sử nạp</i></a>)
                    <div class="history-card">
                        <ul>
                            <?php foreach ($paymentTransactions as $payment) : ?>
                                <li><img src="<?= $cdnUrl ?>/img/checkout/accept.png" alt=""> Nạp thành công thẻ: <b><?= $payment->pinCode ?></b> (<?= $payment->serviceName ?>, <?= Yii::$app->formatter->asCurrency($payment->amount)?>)</li>
                            <?php endforeach; ?>
                        </ul>
                    </div>
                    <?php endif; ?>
                </div>
                <?php
                $remainingAmount = max($paymentForm->totalAmount - $paymentForm->paidAmount, 0);
                ?>
                <div class="row">
                    <div class="left col-xs-7 col-md-8">Số tiền cần thanh toán:</div>
                    <div class="right col-xs-5 col-md-4"><?= Yii::$app->formatter->asCurrency($paymentForm->totalAmount) ?></div>
                    <div class="left col-xs-7 col-md-8">Số tiền đã nạp:</div>
                    <div class="right col-xs-5 col-md-4"><?= Yii::$app->formatter->asCurrency($paymentForm->paidAmount > 0 ? $paymentForm->paidAmount : 0) ?></div>
                    <div class="left col-xs-7 col-md-8">Số tiền cần nạp thêm:</div>
                    <div class="right col-xs-5 col-md-4"><?= Yii::$app->formatter->asCurrency($remainingAmount) ?></div>
                </div>
            </div>
        </div>
        <?php if ($remainingAmount > 0) : ?>
            <br>
            <div class="input-group checkout-radio-card field-paymentform-servicecode">
                <ul>
                    <li>
                        <input id="radio-mobile-card-type-viettel" type="radio" name="PaymentForm[serviceCode]"
                               value="VTT">
                        <label for="radio-mobile-card-type-viettel"><span><span></span></span><name>Viettel</name></label>
                    </li>
                    <li>
                        <input id="radio-mobile-card-type-mobi" type="radio" name="PaymentForm[serviceCode]"
                               value="VMS">
                        <label for="radio-mobile-card-type-mobi"><span><span></span></span><name>Mobiphone</name></label>
                    </li>
                    <li>
                        <input id="radio-mobile-card-type-vina" type="radio" name="PaymentForm[serviceCode]"
                               value="VNP">
                        <label for="radio-mobile-card-type-vina"><span><span></span></span><name>Vinaphone</name></label>
                    </li>
                </ul>
                <div class="help-block input-error" style="padding-left: 15px;"></div>
            </div>
            <div class="clearfix">
                <div class="item">
                    <?= $form->field($paymentForm, 'pinCode')->textInput() ?>
                </div>
                <div class="item">
                    <?= $form->field($paymentForm, 'serial')->textInput() ?>
                </div>
                <div class="item button">
                    <?= Html::button('Nạp thẻ cào', ['class' => 'btn btn-submit-card-mobile'])?>
                </div>
            </div>
        <?php endif; ?>
        <div class="note col-xs-12">
            <br><i><b>Lưu ý:</b></i><br>
            Kyna.vn sẽ hoàn lại số tiền dư sau khi bạn thanh toán thành công bằng mã voucher giảm giá.
        </div>
    </div>
</div>
<script type="text/javascript">
    $('.see-history').on('click', function(e){
        e.preventDefault();
        $('.history-card').toggleClass('open');
        if($('.history-card').hasClass('open')){
            $('.history-card').height($('.history-card ul').height());
        }
        else {
            $('.history-card').height(0);
        }
    })
</script>
