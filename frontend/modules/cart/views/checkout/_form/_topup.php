<?php

use yii\web\View;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;

$data = Yii::$app->request->get('data');
?>
<div class="checkout-list-check" id="topup-info">
    <div class="wrap">
        <p class="alert alert-warning">
            Bạn còn dư <?= Yii::$app->formatter->asCurrency($refunds) ?> khi thanh toán đơn hàng này: Vui lòng nhập số điện thoại để <b>Kyna.vn</b> trả lại tiền dư qua số điện thoại
        mà bạn cung cấp.
        </p>
        <?php $form = ActiveForm::begin([
            'id' => "submit-topup-form",
            'action' => Url::toRoute(['/cart/checkout/topup']),
            'method' => 'POST',
            'options' => ['class' => 'clearfix'],
        ]); ?>
        <?= $form->field($topup, 'phone_number')->textInput(); ?>
        <?= $form->field($topup, 'order_id')->hiddenInput(['value' => $order->id])->label(false); ?>
        <?= $form->field($topup, 'transaction_id')->hiddenInput([
            'value' => !empty($data['transactionId']) ? $data['transactionId'] : null
        ])->label(false); ?>
        
        <button class="checkout-button" type="submit">Hoàn tất</button>
        <?php $form->end(); ?>
    </div>
</div>
<?php $script = "
    ;(function($, window, document, undefined){
        $(document).ready(function(){
            $('body').on('submit', '#submit-topup-form', function (event) {
                event.preventDefault();

                var data = $(this).serializeArray();

                $.post('/cart/checkout/topup', data, function (response) {
                    if (response.result == 1) {
                        $('#topup-info').html('<p class=\'alert alert-success\'>Số điện thoại của bạn đã được cập nhật.</p>');
                    }
                });
            });
        });
    })(window.jQuery, window, document);"
?>
<?php
$this->registerJs($script, View::POS_END, 'topup-submit');
?>