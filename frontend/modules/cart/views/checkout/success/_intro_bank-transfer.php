<?php
use common\helpers\CDNHelper;

$cdnUrl = CDNHelper::getMediaLink();
?>

<div class="checkout-confirm-content">
    <p class="checkout-confirm-succ-text">Cám ơn bạn đã đăng ký khóa học trên <span class="color-green bold">Kyna.vn</span>.</p>

    <div class="checkout-confirm-part-1 box clearfix">
        <div class="col-lg-3 col-sm-4 col-xs-12 img pd0">                             
            <img src="<?= $cdnUrl ?>/img/icon-checkout-confirm-1.png" alt="Kyna.vn" class="img-fluid"/>
        </div><!--end .col-lg-3 col-sm-4 col-xs-12 img -->
        <div class="col-lg-9 col-sm-8 col-xs-12 text">
            <h4>Bước 1</h4>
            <p>Bạn sẽ nhận email xác nhận đơn hàng và hướng dẫn bạn học trên <span class="color-green bold">Kyna.vn</span> thông qua email <span class="bold"><?= $order->user->email ?></span>.</p>
        </div><!--end .col-lg-9 col-sm-8 col-xs-12 text-->                                                                                
    </div><!--end .checkout-confirm-part-1--> 

    <div class="checkout-confirm-part-2 chuyenkhoan box clearfix">
        <div class="col-lg-3 col-sm-4 col-xs-12 img pd0">
            <img src="<?= $cdnUrl ?>/img/icont-confirm-succ-chuyenkhoa-2.png" alt="Kyna.vn" class="img-fluid"/>
        </div><!--end .col-lg-3 col-sm-4 col-xs-12 img -->
        <div class="col-lg-9 col-sm-8 col-xs-12 text">
            <h4>Bước 2</h4>
            <p>Bạn chuyển khoản cho <span class="color-green bold">Kyna.vn </span>theo các thông tin sau:</p>
            <ul>
                <li><span class="bold">&#8226; Số tài khoản:</span> <?= (!empty($settings['bank_account_number']) ? $settings['bank_account_number'] : '0531 0024 67984') ?>.</li>
                <li><span class="bold">&#8226; Chủ tài khoản:</span> <?= (!empty($settings['bank_account_name']) ? $settings['bank_account_name'] : 'Công ty cổ phần DREAM VIET EDUCATION') ?>.</li>
                <li><span class="bold">&#8226; Ngân hàng:</span> <?= (!empty($settings['bank_name']) ? $settings['bank_name'] : 'Ngân hàng Vietcombank, Chi nhánh Bình Thạnh, TP.HCM') ?>.</li>
            </ul>
            <h6 class="bold">Ghi chú khi chuyển khoản:</h6>
            <ul>
                <li>&#8226; Tại mục "Ghi chú" khi chuyển khoản, bạn ghi rõ: Số điện thoại - Họ và tên - Email đăng ký học - Khóa học đăng ký.</li>
                <li>&#8226; Ví dụ: 0909090909 - Nguyen Thi Huong Lan - nguyenthihuonglan@gmail.com Kỹ năng quản lý cảm xúc.</li>                                    
            </ul>
            <h6 class="bold">Hoặc chuyển qua Paypal</h6>
            <ul>
                <li>&#8226; Địa chỉ email <?= (!empty($settings['paypal_email']) ? $settings['paypal_email'] : 'minh@deltaviet.com') ?>.</li>
                <li>&#8226; Tại mục "Message" khi chuyển tiền, bạn ghi rõ: Số điện thoại - Họ và tên - Email đăng ký học - Khóa học đăng ký.</li>
                <?php
                $paypalExchange = !empty($settings['paypal_exchange_rate']) ? $settings['paypal_exchange_rate'] : 22300;
                ?>
                <li>&#8226; Tỉ giá áp dụng 1 USD = <?= Yii::$app->formatter->asCurrency($paypalExchange) ?> (tỉ giá trên PayPal).</li>
            </ul>
        </div><!--end .col-lg-9 col-sm-8 col-xs-12 text-->                                                                                
    </div><!--end .checkout-confirm-part-2--> 

    <div class="checkout-confirm-part-3 box clearfix">
        <div class="col-lg-3 col-sm-4 col-xs-12 img pd0">
            <img src="<?= $cdnUrl ?>/img/icont-confirm-succ-chuyenkhoa-3.png" alt="Kyna.vn" class="img-responsive"/>
        </div><!--end .col-lg-3 col-sm-4 col-xs-12 img -->
        <div class="col-lg-9 col-sm-8 col-xs-12 text">
            <h4>Bước 3</h4>
            <p>Nhân viên chăm sóc của <span class="bold color-green">Kyna.vn</span> sẽ gửi email thông báo kích hoạt cho bạn sau khi nhận được khoản chuyển qua ngân hàng.</p>
        </div><!--end .col-lg-9 col-sm-8 col-xs-12 text-->                                                                                
    </div><!--end .checkout-confirm-part-3--> 

    <div class="checkout-confirm-part-4 box clearfix">
        <div class="col-lg-3 col-sm-4 col-xs-12 img pd0">
            <img src="<?= $cdnUrl ?>/img/icon-checkout-confirm-4.png" alt="Kyna.vn" class="img-responsive"/>
        </div><!--end .col-lg-3 col-sm-4 col-xs-12 img -->
        <div class="col-lg-9 col-sm-8 col-xs-12 text">
            <h4>Bước 4</h4>
            <p>Bạn đăng nhập <span class="color-green bold">Kyna.vn</span> với tài khoản đã đăng ký. Để bắt đầu học, bạn vào mục <span class="bold">Khóa học của tôi</span> và click chọn nút <span class="bold">Bắt đầu học</span> ở khóa học muốn tham gia.</p>
        </div><!--end .col-lg-9 col-sm-8 col-xs-12 text-->                                                                                
    </div><!--end .checkout-confirm-part-4-->                                                                        
    <?php
    $supportMail = (!empty($settings['email_footer']) ? $settings['email_footer'] : 'hotro@kyna.vn');
    $supportPhone = (!empty($settings['hot_line']) ? $settings['hot_line'] : '1900.6364.09');
    ?>
    <p class="checkout-confirm-succ-text">Với bất kì thắc mắc nào, bạn có thể liên hệ qua hotline 
        <span class="color-green bold"><a href="tel:<?= $supportPhone ?>" class="color-green bold"><?= $supportPhone ?></a></span> 
        hoặc email đến 
        <span class="color-green bold"><a href="mailto:<?= $supportMail ?>" class="color-green bold"><?= $supportMail ?></a>.</span>
    </p>                                                                    
</div><!--end .checkout-confirm-content-->