<?php
use common\helpers\CDNHelper;
use kyna\payment\models\PaymentTransaction;

$cdnUrl = CDNHelper::getMediaLink();
$paymentTransaction = $order->getPaymentTransactionPayoo();
$dateExpire = date('d/m/Y', strtotime("+2 day", time()));
$dateExpireHour = date('H', strtotime("+2 day", time()));
$dateExpireMi = date('i', strtotime("+2 day", time()));
?>
<div class="checkout-confirm-content">
    <p class="checkout-confirm-failed-text">Cám ơn bạn đã đăng ký khóa học trên <span class="bold color-green">Kyna.vn</span>
        </br>Mã thanh toán Payoo của bạn là <strong  style="font-size: 25px;color: #50ad4e;">
            <?= $paymentTransaction->transaction_code;?> </strong>
        </br>
        Vui Lòng thanh toán trước <strong style="font-size: 25px;color: #FB6A00;"><?= $dateExpireHour?> : <?= $dateExpireMi?> phút Ngày <?= $dateExpire?> </strong>
    </p>
    <div class="checkout-confirm-part-1 box clearfix">
        <div class="col-lg-3 col-sm-4 col-xs-12 img pd0">                             
                <img src="<?= $cdnUrl ?>/img/icon-checkout-confirm-1.png" alt="Kyna.vn" class="img-responsive"/>
        </div><!--end .col-lg-3 col-sm-4 col-xs-12 img -->
        <div class="col-lg-9 col-sm-8 col-xs-12 text">
            <h4>Bước 1</h4>
            <p>Bạn sẽ nhận email xác nhận đơn hàng và hướng dẫn bạn kích hoạt  thông qua email <span class="bold"><?= $order->user->email ?></span>.
                Trong email bao gồm mã thanh toán Payoo có nội dung  <strong><?= $paymentTransaction->transaction_code;?> </strong>
            </p>
        </div><!--end .col-lg-9 col-sm-8 col-xs-12 text-->                                                                                
    </div><!--end .checkout-confirm-part-1--> 

    <div class="checkout-confirm-part-2 box clearfix">
        <div class="col-lg-3 col-sm-4 col-xs-12 img pd0">
            <img src="<?= $cdnUrl ?>/img/icon-checkout-confirm-succ-2.png" alt="Kyna.vn" class="img-responsive"/>
        </div><!--end .col-lg-3 col-sm-4 col-xs-12 img -->
        <div class="col-lg-9 col-sm-8 col-xs-12 text">
            <h4>Bước 2</h4>
            <p>
                - Học viên  mang mã thanh toán đến các cửa hàng tiện lợi. </br>
                - Yêu cầu giao dịch viên hỗ trợ thanh học phí cho Kyna. </br>
                - Học viên thanh toán bằng tiền mặt cho Giao dịch viên. </br>
                - Học viên nhận biên nhận thu tiền từ Giao dịch viên, kiểm tra thông tin và giữ biên nhận. </br>
                - Xem cửa hàng gần nhất <a href="https://payoo.vn/map/public/?verify=true#"
                   onclick="event.preventDefault();event.stopPropagation();$.fancybox.open({&quot;href&quot;:&quot;https://www.payoo.vn/map/index.php&quot;,&quot;wrapCSS&quot;:&quot;payoo&quot;,&quot;helpers&quot;:{&quot;overlay&quot;:{&quot;closeClick&quot;:true}},&quot;tpl&quot;:{&quot;closeBtn&quot;:&quot;<a title=\&quot;Đóng\&quot; class=\&quot;fancybox-item fancybox-close\&quot; href=\&quot;javascript:;\&quot;></a>&quot;},&quot;type&quot;:&quot;iframe&quot;,title: this.title});" class="map" title="Tìm cửa hàng theo bản đồ">(Xem danh sách)</a>
            </p>
        </div><!--end .col-lg-9 col-sm-8 col-xs-12 text-->

        <div class="col-xs-12">
            <div style="padding: 0;border: 0;background: none;" class="checkout-sub-list payoo" id="checkout-show-payoo">
                <ul class="horizontal-list" >
                    <li><img style="width: 74px" src="<?= $cdnUrl ?>/img/payoo/bsmart.jpg"></li>
                    <li><img style="width: 74px" src="<?= $cdnUrl ?>/img/payoo/circlek.jpg"></li>
                    <li><img style="width: 74px" src="<?= $cdnUrl ?>/img/payoo/ministop.jpg"></li>
                    <li><img style="width: 74px" src="<?= $cdnUrl ?>/img/payoo/cocomart.jpg"></li>
                    <li><img style="width: 74px" src="<?= $cdnUrl ?>/img/payoo/familymart.jpg"></li>
                    <li><img style="width: 74px" src="<?= $cdnUrl ?>/img/payoo/mediamart.jpg"></li>
                    <li><img style="width: 74px" src="<?= $cdnUrl ?>/img/payoo/ecomart.jpg"></li>
                    <li><img style="width: 74px" src="<?= $cdnUrl ?>/img/payoo/citimart.jpg"></li>
                    <li><img style="width: 74px" src="<?= $cdnUrl ?>/img/payoo/vinmart.jpg"></li>
                    <li><img style="width: 74px" src="<?= $cdnUrl ?>/img/payoo/vinpro.jpg"></li>
                    <li><img style="width: 74px" src="<?= $cdnUrl ?>/img/payoo/vinmartplus.jpg"></li>
                    <li><img style="width: 74px" src="<?= $cdnUrl ?>/img/payoo/fptshop.jpg"></li>
                    <li><img style="width: 74px" src="<?= $cdnUrl ?>/img/payoo/dienmaycholon.jpg"></li>
                    <li><img style="width: 74px" src="<?= $cdnUrl ?>/img/payoo/phucanh.jpg"></li>
                    <li><img style="width: 74px" src="<?= $cdnUrl ?>/img/payoo/hnammobile.jpg"></li>
                    <li><img style="width: 74px" src="<?= $cdnUrl ?>/img/payoo/longhung.jpg"></li>
                    <li><img style="width: 74px" src="<?= $cdnUrl ?>/img/payoo/bkc.jpg"></li>
                </ul>
            </div>
        </div>
    </div><!--end .checkout-confirm-part-2-->

    <div class="checkout-confirm-part-3 box clearfix">
        <div class="col-lg-3 col-sm-4 col-xs-12 img pd0">
            <img src="<?= $cdnUrl ?>/img/icon-checkout-confirm-3.png" alt="Kyna.vn" class="img-fluid"/>
        </div><!--end .col-lg-3 col-sm-4 col-xs-12 img -->
        <div class="col-lg-9 col-sm-8 col-xs-12 text">
            <h4>Bước 3</h4>
            <p>Sau khi nhân viên kích hoạt thành công cho bạn, bạn truy cập <span class="color-green bold">Kyna.vn</span>
                và đăng nhập với tài khoản đã đăng ký.Để bắt đầu học, bạn vào mục <strong>Khóa học của tôi</strong> và click chọn
                nút <strong>Bắt đầu học </strong> ở khóa học muốn tham gia.</p>
        </div><!--end .col-lg-9 col-sm-8 col-xs-12 text-->
    </div><!--end .checkout-confirm-part-3-->
    
    <?php
    $supportMail = (!empty($settings['email_footer']) ? $settings['email_footer'] : 'hotro@kyna.vn');
    $supportPhone = (!empty($settings['hot_line']) ? $settings['hot_line'] : '1900.6364.09');
    ?>
    <p class="checkout-confirm-failed-text">Với bất kì thắc mắc nào, bạn có thể liên hệ qua hotline 
        <span>
            <a href="tel:<?= $supportPhone ?>" class="color-green bold"><?= $supportPhone ?></a>
        </span> hoặc email đến 
        <span><a href="mailto:<?= $supportMail ?>" class="color-green bold"><?= $supportMail ?></a></span>.
    </p> 

</div><!--end .checkout-confirm-content -->
