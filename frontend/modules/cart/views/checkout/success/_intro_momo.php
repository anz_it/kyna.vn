<?php
use common\helpers\CDNHelper;

$cdnUrl = CDNHelper::getMediaLink();
?>
<div class="checkout-confirm-content">
    <p class="checkout-confirm-failed-text">Cám ơn bạn đã đăng ký khóa học trên <span class="bold color-green">Kyna.vn</span></p>                                                            
    <div class="checkout-confirm-part-1 box clearfix">
        <div class="col-lg-3 col-sm-4 col-xs-12 img pd0">                             
            <img src="<?= $cdnUrl ?>/img/icon-checkout-confirm-1.png" alt="Kyna.vn" class="img-responsive"/>
        </div><!--end .col-lg-3 col-sm-4 col-xs-12 img -->
        <div class="col-lg-9 col-sm-8 col-xs-12 text">
            <h4>Bước 1</h4>
            <p>Bạn sẽ nhận email xác nhận đơn hàng và hướng dẫn bạn học trên <span class="bold color-green">Kyna.vn </span>thông qua email <span class="bold"><?= $order->user->email ?></span>.</p>
        </div><!--end .col-lg-9 col-sm-8 col-xs-12 text-->                                                                                
    </div><!--end .checkout-confirm-part-1--> 

    <div class="checkout-confirm-part-2 box clearfix">
        <div class="col-lg-3 col-sm-4 col-xs-12 img pd0">
            <img src="<?= $cdnUrl ?>/img/icon-checkout-confirm-succ-2.png" alt="Kyna.vn" class="img-responsive"/>
        </div><!--end .col-lg-3 col-sm-4 col-xs-12 img -->
        <div class="col-lg-9 col-sm-8 col-xs-12 text">
            <h4>Bước 2</h4>
            <p>Để bắt đầu học, bạn vào mục <span class="bold">Khóa học của tôi </span>và click chọn nút <span class="bold">Bắt đầu học </span>ở khóa học muốn tham gia.</p>
        </div><!--end .col-lg-9 col-sm-8 col-xs-12 text-->                                                                                
    </div><!--end .checkout-confirm-part-2--> 
    
    <?php
    $supportMail = (!empty($settings['email_footer']) ? $settings['email_footer'] : 'hotro@kyna.vn');
    $supportPhone = (!empty($settings['hot_line']) ? $settings['hot_line'] : '1900.6364.09');
    ?>
    <p class="checkout-confirm-failed-text">Với bất kì thắc mắc nào, bạn có thể liên hệ qua hotline 
        <span>
            <a href="tel:<?= $supportPhone ?>" class="color-green bold"><?= $supportPhone ?></a>
        </span> hoặc email đến 
        <span><a href="mailto:<?= $supportMail ?>" class="color-green bold"><?= $supportMail ?></a></span>.
    </p> 

</div><!--end .checkout-confirm-content -->
