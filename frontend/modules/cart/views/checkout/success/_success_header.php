<?php
use common\helpers\CDNHelper;

$cdnUrl = CDNHelper::getMediaLink();
?>
<div class="checkout-succ-title clearfix">
    <div class="media">
        <div class="media-left">
            <img src="<?= $cdnUrl ?>/img/icon-checkout-succ.jpg" alt="Kyna.vn" class="img-responsive">
        </div>
        <div class="media-body">
            <h2>Đăng ký khóa học thành công</h2>
            <span>Số đơn hàng <strong><?php echo (!empty($order->order_number) ? $order->order_number : $order->id) ?></strong></span>
        </div>
    </div>
</div>