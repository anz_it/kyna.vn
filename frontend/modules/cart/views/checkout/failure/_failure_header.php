<?php
use common\helpers\CDNHelper;
$cdnUrl = CDNHelper::getMediaLink();
?>
<div class="checkout-failed-title clearfix">
    <div class="media">
        <div class="media-left">
            <img src="<?= $cdnUrl ?>/img/icon-checkout-failed.jpg" alt="Kyna.vn" class="img-fluid"/>
        </div>
        <div class="media-body">
            <h2>Đăng ký khóa học thất bại</h2>
            <span>Số đơn hàng <?php echo (!empty($order->order_number) ? $order->order_number : $order->id) ?></span>
        </div>
    </div>
</div><!--end .checkout-failed-title-->
