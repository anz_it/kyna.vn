<!-- PC -->
<style>
    .checkout .checkout-list-part{
        width: 100%;
    }
    .checkout .checkout-list-part li{
        width: calc(100% / 3);
    }
    .checkout .checkout-list-part li .checkout-span{
        width: 100%;
        display: inline-block;
        text-align: center;
    }
</style>
<ul class="checkout-list-part pc">
    <li>
        <!-- <span class="checkout-triangle-left"></span> -->
        <span class="checkout-span">Xem giỏ hàng</span>
        <span class="checkout-triangle-right"></span>
    </li>

    <li>
        <span class="checkout-triangle-left"></span>
        <span class="checkout-span">Chọn cách thanh toán và điền thông tin</span>
        <span class="checkout-triangle-right"></span>
    </li>

    <li>
        <span class="checkout-triangle-left"></span>
        <span class="checkout-span">Hoàn tất đơn hàng</span>
    </li>
</ul>
<!-- END PC -->

<!-- MOBILE -->
<ul class="checkout-list-part mb">
    <!-- <li>
        <span class="checkout-triangle-left"></span>
        <span class="checkout-icon-cart"></span>
        <span class="checkout-triangle-right"></span>
    </li>

    <li>
        <span class="checkout-triangle-left"></span>
        <span class="checkout-icon-order"></span>
        <span class="checkout-triangle-right"></span>
    </li>

    <li>
        <span class="checkout-triangle-left"></span>
        <span class="checkout-icon-noti"></span>
        <span class="checkout-triangle-right-last"></span>
    </li> -->
    <li>
        <!-- <span class="checkout-triangle-left"></span> -->
        <span class="checkout-span">Giỏ hàng</span>
        <span class="checkout-triangle-right"></span>
    </li>

    <li>
        <span class="checkout-triangle-left"></span>
        <span class="checkout-span">Thanh toán</span>
        <span class="checkout-triangle-right"></span>
    </li>

    <li>
        <span class="checkout-triangle-left"></span>
        <span class="checkout-span">Hoàn tất</span>
    </li>
</ul>
<!-- END MOBILE -->
