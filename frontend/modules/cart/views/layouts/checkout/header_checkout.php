<?php
use common\helpers\CDNHelper;

$cdnUrl = CDNHelper::getMediaLink();
?>

<div class="checkout-header clearfix">
    <div class="left-first col-sm-4 col-xs-6 pd0">
        <h2 class="logo"><a href="/"><img src="<?= $cdnUrl ?>/img/logo/Kynavnraftlogo.svg" alt="Kyna.vn" class="img-responsive" /></a></h2>
    </div><!--end .left-first-->
    <div class="right-first col-sm-8 col-xs-6 pd0">
        <ul>
            <li>
            <span class="bold text-transform">Hotline</span>
            </li>
            <li>
            <span class="color-green bold">1900 6364 09</span>
            </li>
        </ul>
    </div><!--end .left-first-->
</div><!--end .checkout-header-->
