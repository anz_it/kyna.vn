<?php

use yii\helpers\Url;
use common\widgets\Alert;
use common\helpers\CDNHelper;
use kyna\promotion\models\Promotion;
use kyna\course\models\Course;
use kyna\promo\models\GroupDiscount;
use common\campaign\CampaignTet;
$cdnUrl = CDNHelper::getMediaLink();

$formatter = Yii::$app->formatter;
$order = $this->context->order;
$orderDetails = $order->details;

$groupComboDetails = [];
foreach ($orderDetails as $item) {
    if (!empty($item->course_combo_id)) {
        $groupComboDetails[$item->course_combo_id][] = $item;
    } else {
        $groupComboDetails[] = $item;
    }
}

$checkCampaign = 0;
$courses = array();
foreach ($orderDetails as &$detail) {
    $course = $detail->course;
    if(empty($detail->course_combo_id)) {
        $courses[] = $course;
    }
    if ($course->isCampaign11) {
        $checkCampaign ++;
        if ($checkCampaign == 2) {
            $checkCampaign = 0;
            $detail->isGift = true;
        }
    }
}

// get combo discount for campaign, only for coupon
$totalDiscount = $order->totalDiscount;

$discountValue = $totalDiscount -  $order->group_discount_amount;
//$group_discount_amount = $order->group_discount_amount;

$promotionCode = $order->promotion_code;
$campaignDiscount = 0;


$groupDiscount = GroupDiscount::applyGroupDiscountCartItems($courses);
$currentGroupDiscount = $groupDiscount['currentGroupDiscount'];
$discountGroupValue = $groupDiscount['totalGroupDiscount'];
$totalPrice = 0;
?>

<div class="wrap-content-right">
    <div class="fixed-flag"></div>
    <div class="wrap">
        <h4>
            <img src="<?= $cdnUrl ?>/img/icon-mini-cart.png" alt="">
            <b><span class="color-green"><?= count($orderDetails) ?> khóa học</span></b>
        </h4>
        <ul class="list">
            <?php foreach ($groupComboDetails as $key => $item) : ?>
                <?php
                 //hien thi gia goc combo course
                if (is_array($item)) {
                    $course_combo = Course::findOne(['id' => $key]);
                    $firstItem = $item[0];
                    $meta = json_decode($firstItem->course_meta);
                    $name = !empty($meta->combo_name) ? $meta->combo_name : $meta->name;
                    $price = $course_combo->oldPrice - $course_combo->getDiscountAmount();
                    $slug = $course_combo->slug;
                    $courseId = $key;
                    $type = \kyna\course\models\Course::TYPE_COMBO;

                    /*if (!empty(Yii::$app->params['discount_combo']) && $promotionCode == Yii::$app->params['discount_combo']) {
                        $promotion = Promotion::findOne(['code' => $promotionCode]);
                        $discountPrice = $promotion->getDiscountPrice();
                        $totalDiscount -= $discountPrice;
                        $campaignDiscount += $discountPrice;
                    }*/
                } else {
                    $meta = json_decode($item->course_meta);
                    $name = $meta->name;
                    $subPrice = $item->unit_price;
                    $discount = $item->discount_amount;
                    $price = $item->unit_price - $item->course->discountAmount;
                    $slug = $meta->slug;
                    $courseId = $item->course_id;
                    $type = \kyna\course\models\Course::TYPE_VIDEO;
                }
                $courseDetailUrl = Url::toRoute(['/course/default/view', 'id' => $courseId, 'slug' => $slug]);
                $tcourse = Course::findOne(['id' => $courseId]);
                if(($tcourse->type !=\kyna\course\models\Course::TYPE_COMBO) && (!in_array($tcourse->id,CampaignTet::COURSE_NOT_APPLY_VOUCHER)))
                    $totalPrice += $price;
                ?>

                <li class="clearfix" data-id="<?= $courseId ?>" data-price="<?= $price ?>" data-course-type="<?= $type ?>">
                    <div class="col-sm-9 col-xs-8 title pd0 text">
                        <h6><?= $name ?><a class="product-hidden-link" href="<?= $courseDetailUrl ?>" style="display: none"><?= $name ?></a></h6>
                        <span class="product-hidden-price price" style="display: none;"><?php if (!empty($price)) {echo $formatter->asCurrency($price);} else {echo 'Miễn phí';}?></span>
                    </div><!--end .col-xs-2 col-xs-4 name-->
                    <div class="col-sm-3 col-xs-4 price pd0">
                        <b><span class="">
                            <?php
                            if (!is_array($item)) {
                                echo $item->getCostText();
                            } elseif (!empty($price)) {
                                echo $formatter->asCurrency($price);
                            } else {
                                echo 'Miễn phí';
                            }
                            ?>
                        </span></b>
                    </div><!--end .col-md-10 col-xs-8 price-->
                </li>
            <?php endforeach; ?>
        </ul>
        <div class="checkout-list-price">
            <ul>
                <li>
                    <span>Học phí gốc</span>
                    <span class="price total-cost" data-price="<?= $order->subTotal ?>"><b><?= $formatter->asCurrency($order->subTotal) ?></b></span>
                </li>
                <li>
                    <span>Giảm giá </span>
                    <?php $giamLixi = $totalPrice* CampaignTet::percentLiXiTet($totalPrice)/100?>
                    <span class="price"><b> <?= $discountValue > 0 ? '-' : '' ?> <?= $formatter->asCurrency($discountValue-$giamLixi) ?></b></span>
                </li>
                <?php if(Yii::$app->params['campaign_birthday']):?>
                    <li>
                        <span>Tổng giá campaign sinh nhật</span>
                        <span class="price total-cost" ><b><?= $formatter->asCurrency(GroupDiscount::getTotalPriceBirthday($courses)) ?></b></span>
                    </li>

                <?php endif;?>
                <?php
                if(!empty($currentGroupDiscount)):
                    ?>
                    <li>
                        <span>Mua <?= $currentGroupDiscount->course_quantity ?> giảm <?= $currentGroupDiscount->percent_discount ?> %</span>
                        <span class="price"><b> <?= $discountGroupValue > 0 ? '-' : '' ?> <?= $formatter->asCurrency($discountGroupValue) ?></b></span>
                    </li>
                <?php
                endif;
                ?>
                <?php if (!empty(Yii::$app->params['discount_combo']) && $campaignDiscount > 0): ?>
                <li>
                    <span>Mã <?= Yii::$app->params['discount_combo'] ?></span>
                    <span class="price"><b> <?= $campaignDiscount > 0 ? '-' : '' ?> <?= $formatter->asCurrency($campaignDiscount) ?></b></span>
                </li>
                <?php endif; ?>
                <?php if(CampaignTet::InTimesCampaign()):?>
                    <?php $promotionCode = CampaignTet::voucherLixTetRender($totalPrice);?>
                    <?php if(CampaignTet::isVoucherLixTet($promotionCode)):?>
                        <li>
                            <span>Giảm giá Lì Xì Hot(-<?= CampaignTet::percentLiXiTetOther($totalPrice)?>%)</span>
                            <span class="price"><?= $formatter->asCurrency( $totalPrice* \common\campaign\CampaignTet::percentLiXiTetOther($totalPrice)/100) ?></span>
                        </li>
                    <?php endif;?>
                <?php endif;?>
                <li>
                    <span class="color-orange"><b>THÀNH TIỀN</b></span>
                    <span class="price total-price color-orange"><b><?= $formatter->asCurrency($order->realPayment) ?></b></span>
                </li>
            </ul>
            <div class="note-cod"><b>Lưu ý:</b> <i>Chưa bao gồm phí vận chuyển</i></div>
        </div><!--end .checkout-list-price-->
        <?php
        Alert::widget();
        ?>
    </div><!--end .wrap-->
    <div class="re-money">
        <img src="<?= $cdnUrl ?>/img/checkout/hoan-tien.png" alt="">
        <p>Hoàn học phí trong vòng 30 ngày nếu không hài lòng về khóa học</p>
        <?php if ($order->getHasGroupDiscountCourse()): ?>
        <a href="/trang-ca-nhan/voucher-coupon" target="_blank">
            <img style="width:100%" src="<?= $cdnUrl ?>/img/promotion/order_complete.png" alt="">
        </a>
        <?php endif; ?>
        <?php if (GroupDiscount::checkApplyBirthday($order->details) && $order->status == \kyna\order\models\Order::ORDER_STATUS_COMPLETE): ?>
           <img style="width:100%" src="<?= $cdnUrl ?>/img/sinh-nhat-kyna/birthday_voucher.png" alt="">
        <?php endif; ?>
    </div>
</div><!--end .col-md-5 col-xs-12 wrap-content-right pd0-mb-->
