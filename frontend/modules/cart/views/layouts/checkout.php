<?php

use yii\web\View;
use yii\helpers\Url;
use frontend\assets\AppAsset;
use common\widgets\Alert;
use common\helpers\CDNHelper;
use common\assets\BootstrapNotifyAsset;

$cdnUrl = CDNHelper::getMediaLink();

AppAsset::register($this);
BootstrapNotifyAsset::register($this);

$this->beginPage();
$app = Yii::$app;
?>
<!DOCTYPE HTML>
<html lang="<?= $app->language ?>">
<head>
    <?= $this->render('@app/views/layouts/common/html_head') ?>
    <link rel="stylesheet" href="<?= $cdnUrl ?>/css/checkout.css?v=1516332743">
    <!-- Google Tag Manager -->
    <script type="text/javascript" src= "<?= Yii::$app->params['media_link']?>/src/js/gtm.js?v=1508382297"></script>
    <script type="text/javascript" src="<?= Yii::$app->params['media_link']?>/src/js/fb-pixel.js?v=1521778876"></script>
    <script type="text/javascript" src="<?= Yii::$app->params['media_link']?>/src/js/headScript.js?v=1521778878"></script>
    <!-- End Google Tag Manager -->
</head>

<body>
    <?= $this->render('@app/modules/cart/views/layouts/checkout/tracking_script')?>
    <div class="loading-checkout">

    </div>
    <?php $this->beginBody() ?>

    <div class="<?= $this->context->mainDivClass ?>" id="<?= $this->context->mainDivId ?>">
        <div class="wrap-content">
            <div class="container">
                <ul class="clearfix wrap-main">
                    <li class="col-xs-12 wrap-content-left col">
                        <?= $this->render('./checkout/header_checkout') ?>
                        <?= $this->render('./checkout/checkout_list_part') ?>



                        <div class="checkout-wrap-content">
                            <?= $content ?>
                        </div>

                        <?php
                        if (in_array($this->context->action->id, ['success', 'failure'])) {
                            echo $this->render('./checkout/order_details');
                        } else {
                            echo $this->render('./checkout/cart');
                        }
                        ?>
                    </li>

                    <li class="col-lg-7 col-xs-12 wrap-checkout-button-mb col">
                        <div class="wrap-checkout-button">
                            <a class="checkout-button back-to-cart" href="<?= Url::toRoute(['/cart/default/index']) ?>"><i class="fa fa-long-arrow-left" aria-hidden="true"></i> Quay lại</a>
                            <button class="checkout-button-mb">Hoàn tất</button>
                        </div>
                    </li>
                </ul>
            </div>
        </div><!--end .wrap-content-->
    </div>
    <!-- POPUP REGISTER -->
    <div class="modal fade k-popup-account" id="k-popup-account-register" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">

    </div>
    <!-- END POPUP REGISTER -->

    <div class="modal fade k-popup-account" id="k-popup-account-login" tabindex="-1" role="dialog">
    </div>

    <div class="modal fade k-popup-account" id="k-popup-account-reset" tabindex="-1" role="dialog">
    </div>

    <?php $this->endBody() ?>

    <?php $this->registerJsFile($cdnUrl.'/js/script-checkout.js?v=15018342322', ['position' => View::POS_END]) ?>
</body>
</html>
<?php $this->endPage() ?>
