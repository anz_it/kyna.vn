<?php

namespace app\modules\api\components;

use yii\rest\ActiveController as BaseActiveController;
use yii\filters\auth\HttpBasicAuth;
use kyna\api\models\ApiCredential;
use yii\web\UnauthorizedHttpException;

class Controller extends BaseActiveController
{

    public $serializer = [
        'class' => 'yii\rest\Serializer',
        'collectionEnvelope' => 'items',
    ];

    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['authenticator'] = [
            'class' => HttpBasicAuth::className(),
            'auth' => function ($username, $password) {
                if (!empty($username) && !empty($password)) {
                    $identity = ApiCredential::findByUsername($username);
                    if ($identity != null && $identity->validatePassword($password)) {
                        return $identity;
                    } else {
                        throw new UnauthorizedHttpException('Your request was made with invalid credentials');
                    }
                } elseif (!empty($username)) {
                    $identity = ApiCredential::findIdentityByAccessToken($username);
                }

                return $identity;
            }
        ];

        return $behaviors;
    }
}