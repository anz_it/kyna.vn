<?php

namespace app\modules\api\models\forms;

use Yii;
use yii\base\InvalidValueException;

use app\models\Course;
use kyna\order\models\Order;
use kyna\order\models\OrderQueue;
use kyna\user\models\User;
use kyna\user\models\Address;
use kyna\user\models\UserCourse;
use kyna\commission\models\AffiliateUser;

class CreateOrderForm extends \yii\base\Model
{

    public $user_email;
    public $direct_discount_amount;
    public $group_discount;
    public $payment_method;
    public $course_list;
    public $shipping_contact_name;
    public $shipping_phone_number;
    public $shipping_street_address;
    public $shipping_location_id;
    public $operator_id;
    public $group_discount_amount;

    public function scenarios()
    {
        return [
            'default' => [
                'user_email',
                'direct_discount_amount',
                'group_discount_amount',
                'group_discount',
                'course_list',
                'payment_method',
                'shipping_contact_name',
                'shipping_phone_number',
                'shipping_street_address',
                'shipping_location_id',
                'operator_id',
            ],
        ];
    }

    public function rules()
    {
        return [
            ['user_email', 'email'],
            ['operator_id', 'integer'],
            [['direct_discount_amount', 'group_discount_amount'], 'number'],
            [['direct_discount_amount', 'group_discount_amount'], 'default', 'value' => 0],
            ['group_discount', 'boolean'],
            ['shipping_method', 'string', 'max' => 20],

            [['shipping_contact_name', 'shipping_phone_number', 'shipping_street_address', 'shipping_location_id'], 'required', 'when' => function ($model) {
                return $model->payment_method == 'ghn';
            }, 'whenClient' => 'function (attribute, value) {
                return $(\'[name*="[payment_method]"]\').val() == "cod";
            }', 'message' => 'Thông tin này bắt buộc nếu đơn hàng là COD'],

            [['user_email', 'course_list'], 'required'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'user_email' => 'Email học viên',
            'direct_discount_amount' => 'Giảm giá trực tiếp',
            'group_discount' => 'Giảm giá theo nhóm',
            'group_discount_amount' => 'Mức giảm giá theo nhóm',
            'payment_method' => 'Hình thức thanh toán',
            'order_details' => 'Danh sách khóa học',
            'shipping_vendor_id' => 'Dịch vụ giao nhận',
            'course_list' => 'Danh sách đăng ký',
            'subtotal' => 'Thành tiền',
            'shipping_contact_name' => 'Tên người nhận',
            'shipping_phone_number' => 'Số điện thoại',
            'shipping_street_address' => 'Địa chỉ',
            'shipping_location_id' => 'Địa phương',
            'shipping_vendor_id' => 'Đơn vị giao vận',
        ];
    }

    public function create()
    {
        /* Validate user before create */
        if (!$this->user_email) {
            return false;
        }

        $user = $this->findUser($this->user_email);

        if (!$user) {
            throw new InvalidValueException("Can't create user with this email");
        } else {
            // check duplicate user courses
            $courseIds = explode(',', $this->course_list);
            $removedCourses = [];

            foreach ($courseIds as $key => $courseId) {
                $course = Course::findOne($courseId);
                if (empty($course)) {
                    unset($courseIds[$key]);
                    continue;
                }
                $alreadyCourses = [];
                $removed = false;

                if ($course->type == Course::TYPE_COMBO) {
                    foreach ($course->comboItems as $item) {
                        if (UserCourse::find()->where(['user_id' => $user->id, 'course_id' => $item->course_id])->exists()) {
                            $removed = true;
                            $alreadyCourses[] = $item->course->name;
                        }
                    }
                } else {
                    if (UserCourse::find()->where(['user_id' => $user->id, 'course_id' => $course->id])->exists()) {
                        $removed = true;
                    }
                }

                if ($removed) {
                    unset($courseIds[$key]);
                    $removedCourses[] = "<b>{$course->name}</b>" . (!empty($alreadyCourses) ? "(đã học: <b>" . implode(', ', $alreadyCourses) . "</b>)" : '');
                }
            }

            if (!empty($removedCourses)) {
                $this->course_list = implode(',', $courseIds);

                Yii::$app->session->setFlash('warning', "Đã loại bỏ các khóa học sau: " . implode(', ', $removedCourses) . ", học viên <b>{$user->profile->name}</b> đã tham gia các khóa học này.");
                return false;
            }
        }

        /* Start creating order */
        $order = Order::createEmpty(Order::SCENARIO_CREATE_BACKEND);
        $order->on(Order::EVENT_ORDER_CREATED, [$this, 'pushToQueue']);

        $order->direct_discount_amount = $this->direct_discount_amount;
        $order->group_discount_amount = $this->group_discount_amount;
        $order->payment_method = $this->payment_method;
        $order->user_id = $user->id;
        $order->reference_id = $this->operator_id;
        $order->operator_id = $this->operator_id;

        if ($order->isCod and $address = $this->updateUserAddress($user->id)) {
            $order->shippingAddress = $address;
        }

        $affiliateUser = AffiliateUser::find()->where(['user_id' => Yii::$app->user->id, 'status' => AffiliateUser::STATUS_ACTIVE])->one();
        if (!empty($affiliateUser)) {
            // set affiliate
            $order->affiliate_id = Yii::$app->user->id;
        }

        // save order details
        $order->addDetails($this->prepareOrderItems());
        if ($order->save()) {
            return $order;
        }

        return false;
    }

    protected function prepareOrderItems()
    {
        $courseIds = explode(',', $this->course_list);
        $courses = Course::find()->where(['id' => $courseIds])->all();

        $addItems = [];
        foreach ($courses as $key => $course) {
            if ($course->type == Course::TYPE_COMBO) {
                unset($courses[$key]);

                $comboItems = $course->comboItems;
                foreach($comboItems as $item) {
                    if (!array_key_exists($item->course_id, $courses)) {
                        $product = Product::findOne($item->course_id);
                        $product->setCombo($course);
                        $product->setDiscountAmount($product->price - $item->price);

                        $addItems[$product->id] = $product;
                    }
                }
            } else {
                $addItems[$course->id] = $course;
            }
        }

        return $addItems;
    }

    protected function updateUserAddress($userId)
    {
        if (!$address = Address::find()->where(['user_id' => $userId])->one()) {
            $address = new Address();
            $address->user_id = $userId;
        }

        $address->attributes = [
            'email' => $this->user_email,
            'contact_name' => $this->shipping_contact_name,
            'phone_number' => $this->shipping_phone_number,
            'street_address' => $this->shipping_street_address,
            'location_id' => $this->shipping_location_id,
        ];

        if (!$address->save()) {
            var_dump($address->errors);die;
            $this->errors = $address->errors;
            return false;
        }

        return $address;
    }

    protected function findUser($email)
    {
        $user = User::find()->where(['email' => $email])->one();

        if (!$user) {
            $user = new User([
                'scenario' => 'create',
            ]);
            $user->load(['User' => [
                'email' => $email,
                'username' => $email,
            ]]);
            $user->create();
        }

        return $user;
    }

    public function pushToQueue($actionEvent)
    {
        $order = $actionEvent->sender;
        OrderQueue::pushToQueue($order);
    }
}
