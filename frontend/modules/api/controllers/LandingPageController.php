<?php

namespace app\modules\api\controllers;

use Yii;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\helpers\Json;
use yii\helpers\ArrayHelper;

use common\lib\CI_Encrypt;

use kyna\user\models\UserTelesale;
use kyna\user\models\Address;
use kyna\user\models\User;
use kyna\user\models\UserCourse;
use kyna\course_combo\models\CourseCombo;
use kyna\order\models\Order;
use kyna\commission\models\AffiliateUser;
use kyna\base\models\Location;

use app\modules\api\models\forms\CreateOrderForm;
use app\modules\cart\models\Product;

class LandingPageController extends \yii\web\Controller
{
    
    public function init()
    {
        $ret = parent::init();
        
        $this->enableCsrfValidation = false;
        
        return $ret;
    }
    
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'register' => ['post'],
                ],
            ],
        ];
    }
    
    public function actionRegister()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        
        $model = new UserTelesale();
        $model->setScenario('landing-page');
            
        $model->email = !empty($post['email']) ? $post['email'] : null;
        $model->full_name = !empty($post['name']) ? $post['name'] : null;
        $model->phone_number = !empty($post['phone']) ? $post['phone'] : null;
//        $model->type = !empty($post['user_type']) ? $post['user_type'] : null;
//        $model->amount = !empty($post['cod_money']) ? $post['cod_money'] : null;
        $model->course_id = !empty($post['course_id']) ? $post['course_id'] : null;
        $model->combo_id = !empty($post['combo_id']) ? $post['combo_id'] : null;
//        $model->bonus = !empty($post['bonus']) ? $post['bonus'] : null;
        $model->street_address = !empty($post['street_address']) ? $post['street_address'] : null;
        
        // detect exist affiliate
        $cookies = Yii::$app->request->cookies;
        if ($affiliateId = $cookies->getValue('affiliate_id')) {
            $model->affiliate_id = $affiliateId;
        }
        //$model->form_name = $post['form_name'];
//        $model->tel_id = $v2AssignUser->id;

        if ($model->save()) {
            $ret = [
                'result' => true,
                'errors' => [],
                'message' => '',
            ];
        } else {
            $ret = [
                'result' => false,
                'errors' => $model->errors,
                'message' => 'Có lỗi xảy ra trong quá trình xử lý dữ liệu.',
            ];
        }

        echo Json::encode($ret);
    }
    
    public function actionRegisterCod()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        
        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();
            $model = new CreateOrderForm();
            $model->payment_method = 'ghn';
            
            $model->user_email = !empty($post['txtemailr']) ? $post['txtemailr'] : null;
            $model->shipping_phone_number = !empty($post['phone']) ? $post['phone'] : null;
            $model->shipping_contact_name = !empty($post['txtnamer']) ? $post['txtnamer'] : null;
            $model->shipping_street_address = !empty($post['street_address']) ? $post['street_address'] : null;
            $model->shipping_location_id = !empty($post['location_id']) ? $post['location_id'] : null;
            
            $model->course_list = !empty($post['group_id']) ? $post['group_id'] : null;
            
            if ($order = $model->create()) {
                // active courses for order
                $courseIds = ArrayHelper::map($order->details, 'id', 'course_id');
                UserCourse::register($order->user_id, $courseIds);
                
                $ret = [
                    'result' => true,
                    'errors' => [],
                    'message' => '',
                ];
            } else {
                $ret = [
                    'result' => false,
                    'errors' => $model->errors,
                    'message' => 'Có lỗi xảy ra trong quá trình xử lý dữ liệu.',
                ];
            }
            
            echo Json::encode($ret);
        }
    }
    
    public function actionV2Register()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        
        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();
            
            $model = new UserTelesale();
            $model->setScenario('landing-page');
            
            if (!empty($post['email'])) {
                $model->email = $post['email'];
            }
            if (!empty($post['fullname'])) {
                $model->full_name = $post['fullname'];
            }
            if (!empty($post['phone_number'])) {
                $model->phone_number = $post['phone_number'];
            }
            if (!empty($post['user_type'])) {
                $model->type = $post['user_type'];
            }
            if (!empty($post['cod_money'])) {
                $model->amount = $post['cod_money'];
            }
            if (!empty($post['course_id'])) {
                $model->course_id = $post['course_id'];
            }
            if (!empty($post['combo_id'])) {
                $model->course_id = $post['combo_id'];
            }
            if (!empty($post['bonus'])) {
                $model->bonus = $post['bonus'];
            }
            if (!empty($post['affiliate_id'])) {
                $affUser = User::find()->where(['v2_id' => $post['affiliate_id']])->one();
                if (!is_null($affUser)) {
                    $model->affiliate_id = $affUser->id;
                }
            }
            if (!empty($post['street_address'])) {
                $model->street_address = $post['street_address'];
            }
            if (!empty($post['form_name'])) {
                $model->form_name = $post['form_name'];
            }
            if (!empty($post['district_id'])) {
                $v2District = Yii::$app->dbv2->createCommand("SELECT * FROM district WHERE id = {$post['district_id']}")->queryOne();
                if (!empty($v2District['name'])) {
                    $location = Location::getByDistrictText($v2District['name']);
                    if (!is_null($location)) {
                        $model->location_id = $location->id;
                    }
                }
            }
            if (!empty($post['assign_id'])) {
                $v2AssignUser = User::find()->andWhere(['v2_id' => $post['assign_id']])->one();
                if (!is_null($v2AssignUser)) {
                    $model->tel_id = $v2AssignUser->id;
                }
            }
            
            if ($model->save()) {
                $ret = [
                    'result' => true,
                    'errors' => [],
                    'message' => '',
                ];
            } else {
                $ret = [
                    'result' => false,
                    'errors' => $model->errors,
                    'message' => 'Có lỗi xảy ra trong quá trình xử lý dữ liệu.',
                ];
            }
            
            echo Json::encode($ret);
        }
    }
    
    public function actionRegisterCodV2()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        
        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();
            $orderDetails = [];
            $subTotal = $totalDiscount = 0;
            
            $combo = CourseCombo::find()->where(['type' => CourseCombo::TYPE_COMBO, 'v2_id' => $post['group_promotion_id']])->one();
            if (!is_null($combo)) {
                foreach ($combo->comboItems as $comboItem) {
                    $pItem = Product::findOne($comboItem->course_id);
                    
                    $discount = $pItem->price - $comboItem->price;
                    
                    $pItem->setDiscountAmount($discount);
                    $pItem->setCombo($combo);
                    
                    $subTotal += $pItem->price;
                    $totalDiscount += $discount;
                    
                    $orderDetails[] = $pItem;
                }
            }
            
            $order = Order::createEmpty(Order::SCENARIO_CREATE_FRONTEND);
            $order->status = Order::ORDER_STATUS_PENDING_CONTACT;
            $order->payment_method = 'ghn';
            
            $order->sub_total = $subTotal;
            $order->total_discount = $totalDiscount;
            $order->shipping_fee = $post['shipping'];
            $order->total = $post['cost'] + $post['shipping'];
            $order->activation_code = $post['activate_code'];
            
            $registeredDate = date_create_from_format("Y-m-d H:i:s", trim($post['created_date']));
            if ($registeredDate !== false) {
                $order->order_date = $registeredDate->getTimestamp();
            }
            
            if (!empty($post['affiliate_id'])) {
                $affUser = User::find()->andWhere(['v2_id' => $post['affiliate_id']])->one();
                if (is_null($affUser)) {
                    // migrate user
                    $affUser = $this->migrateUser($post['affiliate_id']);
                }
                
                if ($affUser !== false) {
                    $affiliateUser = AffiliateUser::find()->where(['user_id' => $affUser->id, 'status' => AffiliateUser::STATUS_ACTIVE])->one();
                    if (is_null($affiliateUser)) {
                        $affiliateUser = new AffiliateUser();
                        $affiliateUser->user_id = $affUser->id;

                        $v2AffiliateUser = Yii::$app->dbv2->createCommand("SELECT * FROM affiliate WHERE user_id = {$post['affiliate_id']}")->queryOne();
                        if (!empty($v2District['cate_id'])) {
                            $affiliateUser->affiliate_category_id = $v2District['cate_id'];
                        }

                        $affiliateUser->save();
                    }
                    // set affiliate
                    $order->affiliate_id = $affUser->id; 
                }
            }
            
            if (!empty($post['user_id'])) {
                $user = User::find()->andWhere(['v2_id' => $post['user_id']])->one();
                if (!is_null($user)) {
                    $order->user_id = $user->id;
                } else {
                    // migrate user
                    $user = $this->migrateUser($post['user_id']);
                    $order->user_id = $user !== false ? $user->id : null;
                }
            }
            
            $order->shippingAddress = $this->updateUserAddress($user->id, $post['email'], $post['fullname'], $post['phone_number'], $post['street'], $post['district'], $post['notes']);
            
            $order->addDetails($orderDetails);
            
            if ($order->save()) {
                // active courses for order
                $courseIds = ArrayHelper::map($order->details, 'id', 'course_id');
                UserCourse::register($order->user_id, $courseIds);
                
                $ret = [
                    'result' => true,
                    'errors' => [],
                    'message' => '',
                ];
            } else {
                $ret = [
                    'result' => false,
                    'errors' => $order->errors,
                    'message' => 'Có lỗi xảy ra trong quá trình xử lý dữ liệu.',
                ];
            }
            
            echo Json::encode($ret);
        }
    }
    
    private function updateUserAddress($userId, $email, $name, $phone, $street, $v2District = null, $note = null)
    {
        if (!$address = Address::find()->where(['user_id' => $userId])->one()) {
            $address = new Address();
            $address->user_id = $userId;
        }
        
        $address->attributes = [
            'email' => $email,
            'contact_name' => $name,
            'phone_number' => $phone,
            'street_address' => $street,
        ];
        
        if (!is_null($v2District)) {
            $location = Location::getByDistrictText($v2District);
            if (!is_null($location)) {
                $address->location_id = $location->id;
            }
        }

        if (!$address->save()) {
            return false;
        }

        return $address;
    }
    
    private function migrateUser($v2Id)
    {
        $dataV2 = Yii::$app->dbv2->createCommand("SELECT * FROM users WHERE id = {$v2Id}")->queryOne();
        
        if ($dataV2 === false) {
            return FALSE;
        }
        
        $user = new User();
        
        $user->v2_id = $v2Id;
        $user->username = $dataV2['username'];
        $user->email = $dataV2['email'];
        
        $encypt = new CI_Encrypt();
        
        $password = $encypt->decode($dataV2['password']);
        
        $user->password_hash = Yii::$app->security->generatePasswordHash($password);
        $createdDate = date_create_from_format("Y-m-d H:i:s", trim($dataV2['registered_date']));
        if ($createdDate !== false) {
            $user->created_at = $createdDate->getTimestamp();
        }

        if (!$user->save()) {
            if ($user->hasErrors('username') && !$user->hasErrors('email') && $dataV2['username'] != $dataV2['email']) {
                $user->username = $dataV2['email'];

                if ($user->save()) {
                    Yii::$app->dbv2->createCommand("UPDATE users SET `is_sync` = 1 WHERE id = {$v2Id}")->excute();
                } else {
                    echo Json::encode($user->errors);Yii::$app->end();
                }
            }
        }
        sleep(3);
        // save user profile
        $profile = $user->profile;
        $profile->name = $dataV2['fullname'];
        $profile->public_email = $dataV2['email'];
        $profile->phone_number = $dataV2['phone_number'];
        $profile->facebook_id = $dataV2['facebook_account'];

        $profile->save(false); 
        $this->saveUserMeta($user, $dataV2);
        
        return $user;
    }
    
    private function saveUserMeta($user, $dataV2)
    {
        $user->gender = intval($dataV2['gender']);
        
        $user->address = $dataV2['street_address'];
        $user->birthday = $dataV2['birthday'];
        if (!empty($dataV2['district'])) {
            $location = Location::getByDistrictText($dataV2['district']);
            if (!is_null($location)) {
                $user->location_id = $location->id;
            }
        }
        $user->is_receive_email_newsletter = $dataV2['news_email'];
        $user->is_receive_email_new_course_created = $dataV2['study_email'];
        $user->is_finished_tutorial_beginner = $dataV2['finished_tutorial_beginner'];
        if (!empty($dataV2['manager_id'])) {
            $manaUser = User::find()->andWhere(['v2_id' => $dataV2['manager_id']])->one();
            if (!is_null($manaUser)) {
                $user->manager_id = $manaUser->id;
            }
        }
        if (!empty($dataV2['large_avatar'])) {
            $user->avatar = $dataV2['large_avatar'];
        } else {
            $user->avatar = $dataV2['avatar'];
        }

        return $user->save();
    }
    
}
