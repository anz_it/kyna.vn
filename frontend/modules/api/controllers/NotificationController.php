<?php
/**
 * Created by PhpStorm.
 * User: ngunp
 * Date: 6/28/2017
 * Time: 4:10 PM
 */

namespace app\modules\api\controllers;

use yii\helpers\Json;
use app\modules\api\components\Controller;
use common\helpers\NotificationHelper;

class NotificationController extends Controller
{
    public $modelClass = '';

    public function behaviors()
    {
        $behaviors =  parent::behaviors(); // TODO: Change the autogenerated stub
        unset($behaviors['authenticator']);
        return $behaviors;
    }



    public function verbs()
    {
        return [
            'update-token' => ['POST']

        ];
    }

    public function actionUpdateToken ()
    {
        $params = \Yii::$app->request->getBodyParams();
        $user = \Yii::$app->user;

        $userId = null;
        if (isset($user->identity->id))
            $userId = $user->identity->id;

        $result = NotificationHelper::updateToken($params['token'], $userId);
        if ($result) {
            $ret = [
                'success' => true,
                'message' => "Cập nhật token thành công",
//                'data' => $result,
            ];
        } else {
            $ret = [
                'success' => false,
                'message' => "Cập nhật token thất bại",
//                'data' => $result,
            ];
        }
        return json_encode($ret);
    }

    // Will move to back end
//    public function actionCreate_notification()
//    {
//        $params = \Yii::$app->request->getBodyParams();
//
//        $data = NotificationHelper::createNotification($params);
//
//        return $data;
//
//    }
//
//    public function actionGet_notification()
//    {
//        $id = \Yii::$app->request->get('id');
//        $data = NotificationHelper::getNotification($id);
//
//        return $data;
//    }
//
//    public function actionUpdate_notification()
//    {
//        $params = \Yii::$app->request->getBodyParams();
//        $data = NotificationHelper::updateNotification($params);
//        return Json::encode($data);
//    }
//
//    public function actionDelete_notification()
//    {
//        $params = \Yii::$app->request->getBodyParams();
//        $data = NotificationHelper::deleteNotification($params['id']);
//        return Json::encode($data);
//    }
//
//    public function actionSend_message()
//    {
//        $data = NotificationHelper::sendMessage([]);
//        return $data;
//    }
//
//    public function actionAdd_token_to_topic()
//    {
//        $params = \Yii::$app->request->getBodyParams();
//        if (empty($params['token']))
//            return false;
//        $token = $params['token'];
//        $topic = (!empty($params['topic']))? $params['topic'] : null;
//        $data = NotificationHelper::addTokenToTopic($token, $topic);
//        return $data;
//    }
//
//    public function actionTest()
//    {
//        echo "hello";
//    }
}