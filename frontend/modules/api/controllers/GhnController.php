<?php

namespace app\modules\api\controllers;

use Yii;
use app\modules\api\components\Controller;
use kyna\order\models\Order;
use kyna\payment\lib\ghn\Ghn;
use kyna\payment\models\PaymentTransaction;

class GhnController extends Controller
{
    public $modelClass = '';

    public function actions()
    {
        return [];
    }

    public function verbs()
    {
        return [
            'ipn' => ['POST']
        ];
    }

    public function actionIpn()
    {
        $params = \Yii::$app->getRequest()->getBodyParams();
        $result = [
            'success' => false,
            'message' => '',
            'params' => $params
        ];
        $ghn = new Ghn();

        // validate params from GHN IPN
        if (!$ghn->validateIpnMessage($params)) {
            $result['message'] = "Invalid parameters";
            Yii::info($result, 'ipn');
            return $result;
        }

        // update status shipping and order
        $resultUpdate = $ghn->updateShippingStatus($params);
        $result = array_merge($result, $resultUpdate);

        // log result
        \Yii::info($result, 'ipn');
        return $result;
    }
}
