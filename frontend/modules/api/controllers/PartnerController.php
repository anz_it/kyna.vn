<?php

namespace app\modules\api\controllers;

use app\modules\api\components\Controller;
use kyna\partner\models\Partner;
use kyna\partner\models\Transaction;

class PartnerController extends Controller
{
    public $modelClass = '';

    public function actions()
    {
        return [];
    }

    public function verbs()
    {
        return [
            'ipn' => ['POST']
        ];
    }

    public function actionIpn($slug)
    {
        $partner = Partner::findOne(['class' => $slug]);
        $params = \Yii::$app->getRequest()->getBodyParams();
        $result = [
            'status' => 0,
            'message' => '',
            'params' => $params
        ];
        if ($partner && !empty($params['code']) && !empty($params['used'])) {
            $transaction = Transaction::find()
                ->where(['code' => $params['code'], 'partner_id' => $partner->id, 'status' => Transaction::STATUS_COMPLETE])
                ->one();
            if (!empty($transaction)) {
                if (intval($params['used']) > $transaction->num_used) {
                    $transaction->num_used = intval($params['used']);
                    $transaction->save(false);
                    $result['status'] = 1;
                    $result['message'] = "Completed";
                } else {
                    $result['message'] = 'Invalid Partner parameters';
                }
            } else {
                $result['message'] = "Transaction with code {$params['code']} does not exist";
            }
        } else {
            $result['message'] = 'Invalid Partner parameters';
        }

        \Yii::info($result, 'partner');
        return $result;
    }
}
