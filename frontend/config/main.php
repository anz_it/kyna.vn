<?php

$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'), require(__DIR__ . '/../../common/config/params-local.php'), require(__DIR__ . '/params.php'), require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'bootstrap' => [
        'log',
    ],
    'controllerNamespace' => 'frontend\controllers',
    'modules' => [
        'page' => [
            'class' => 'app\modules\page\PageModule',
        ],
        'api' => [
            'class' => 'app\modules\api\ApiModule',
        ],
        'course' => [
            'class' => 'app\modules\course\CourseModule',
        ],
        'cart' => [
            'class' => 'app\modules\cart\CartModule',
        ],
        'user' => [
            'class' => 'dektrium\user\Module',
            'enableUnconfirmedLogin' => true,
            'enableConfirmation' => false,
            'enableRegistration' => true,
            'modelMap' => [
                'User' => 'kyna\user\models\User',
                'Profile' => 'kyna\user\models\Profile',
                'LoginForm' => 'dektrium\user\models\LoginForm',
                'RegistrationForm' => 'app\modules\user\models\forms\RegistrationForm',
            ],
            'controllerMap' => [
                'security' => 'app\modules\user\controllers\SecurityController',
                'recovery' => 'app\modules\user\controllers\RecoveryController',
                'profile' => 'app\modules\user\controllers\ProfileController',
                'course' => 'app\modules\user\controllers\CourseController',
                'message' => 'app\modules\user\controllers\MessageController',
                'question' => 'app\modules\user\controllers\QuestionController',
                'transaction' => 'app\modules\user\controllers\TransactionController',
                'document' => 'app\modules\user\controllers\DocumentController',
                'discuss' => 'app\modules\user\controllers\DiscussController',
                'order'     => 'app\modules\user\controllers\OrderController',
                'registration'  => 'app\modules\user\controllers\RegistrationController',
                'auth'  => 'app\modules\user\controllers\AuthController',
                'point'  => 'app\modules\user\controllers\PointController',
                'gift'  => 'app\modules\user\controllers\GiftController',
				'teacher' => 'app\modules\user\controllers\TeacherController',
            ],
            'urlRules' => [],
        ],
        'treemanager' =>  [
            'class' => '\kartik\tree\Module',
        ],
        'email' => [
            'class' => 'app\modules\email\EmailModule',
        ],
        'career' => [
            'class' => 'app\modules\career\CareerModule',
        ],
        'faq' => [
            'class' => 'app\modules\faq\FaqModule',
        ],

    ],
    'components' => [
        'user' => [
            'class' => 'app\components\WebUser',
            'identityClass' => 'kyna\user\models\User',
            'loginUrl' => ['/user/security/login'],
            'enableAutoLogin' => false,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning', 'info'],
                    'categories' => ['application'],
                    'logFile' => '@app/runtime/logs/app.log',
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'categories' => ['ipn'],
                    'levels' => ['error', 'warning', 'info'],
                    'logFile' => '@app/runtime/logs/ipn.log',
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'categories' => ['partner'],
                    'levels' => ['error', 'warning', 'info'],
                    'logFile' => '@app/runtime/logs/partner.log',
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'categories' => ['payment'],
                    'levels' => ['error'],
                    'logFile' => '@app/runtime/logs/payment.log',
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'categories' => ['monkey'],
                    'levels' => ['error', 'warning', 'info'],
                    'logFile' => '@app/runtime/logs/monkey.log',
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'categories' => ['debug'],
                    'levels' => ['error', 'warning', 'info', 'trace'],
                    'logFile' => '@app/runtime/logs/debug.log',
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'rules' => require __DIR__.'/route.php',
        ],
        'view' => [
            'class' => 'app\components\View',
            'theme' => [
                'pathMap' => [
                    '@dektrium/user/views' => '@app/modules/user/views'
                ],
            ],
        ],
        'cart' => [
            'class' => 'app\modules\cart\components\ShoppingCart',
            'cartId' => 'kyna-cart',
        ],
        'facebook' => [
            'class' => 'app\modules\user\components\Facebook',
        ],
        'assetManager' => [
            //'baseUrl' => '@cdn/assets',
            'bundles' => [
                'yii\web\JqueryAsset' => [
                    'sourcePath' => '@bower',
                    'js' => [
                        'jquery/dist/jquery.min.js',
                    ]
                ],
                'yii\bootstrap\BootstrapAsset' => [
                    'css' => [],
                ],
            ],
        ],
        'settings' => [
            'class' => 'app\components\Settings',
        ],
        'category' => [
            'class' => 'app\modules\course\components\Category',
            'urlWithId' => false,
        ],
        'request' => [
            'parsers' => [
                'application/json' => 'yii\web\JsonParser',
            ]
        ]
    ],
    'params' => $params,
];
