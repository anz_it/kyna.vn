<?php
return [
    /**
     * Expiration Time for Cache Cart
     */
    'expiredCartTime' => 86400, // 1 day
    'transactionPrefix' => 'kyna.vn_',
    // site info
    'hotline' => '1900 6364 09',
    'facebook' => [
        'appId' => '191634267692814',
    ],
    'beginLearningCourseId' => '10',
    'mobileTrackUrl' => 'http://mobi.kyna.vn',
    'pageSize' => 30
];
