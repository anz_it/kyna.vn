<?php

return [
    // home affiliate
    '/<affiliate_id:\d+>' => '/site/index',
    //course listing without catId
    'danh-sach-khoa-hoc/<parent:[a-z0-9\-]+>/<slug:[a-z0-9\-]+>/<affiliate_id:\d+>' => '/course/default/index',
    'danh-sach-khoa-hoc/<slug:[a-z0-9\-]+>/<affiliate_id:\d+>' => '/course/default/index',
    'danh-sach-khoa-hoc/<parent:[a-z0-9\-]+>/<slug:[a-z0-9\-]+>' => '/course/default/index',
    // course listing without category but has affiliate
    'danh-sach-khoa-hoc/<affiliate_id:\d+>' => '/course/default/index',
    'danh-sach-khoa-hoc/<slug:[a-z0-9\-]+>' => '/course/default/index',
    //'tim-kiem/<q:.*>'                                               => '/course/default/index',
    'tag/<tag:[a-z0-9\-]+>/<affiliate_id:\d+>' => '/course/default/index',
    'tag/<tag:[a-z0-9\-]+>' => '/course/default/index',

    //course listing with catId
    'danh-sach-khoa-hoc/<parent:[a-z0-9\-]+>/<slug:[a-z0-9\-]+>-c<catId:\d+>/<affiliate_id:\d+>' => '/course/default/index',
    'danh-sach-khoa-hoc/<parent:[a-z0-9\-]+>/<slug:[a-z0-9\-]+>-c<catId:\d+>' => '/course/default/index',
    'danh-sach-khoa-hoc/<slug:[a-z0-9\-]+>-c<catId:\d+>/<affiliate_id:\d+>' => '/course/default/index',
    'danh-sach-khoa-hoc/<slug:[a-z0-9\-]+>-c<catId:\d+>' => '/course/default/index',

    // course listing without category
    [
        'pattern' => 'khuyen-mai/nhom-khoa-hoc/<affiliate_id:\d+>',
        'route' => '/course/default/index',
        'defaults' => ['course_type' => 2],
    ],
    [
        'pattern' => 'khuyen-mai/nhom-khoa-hoc',
        'route' => '/course/default/index',
        'defaults' => ['course_type' => 2],
    ],
    'danh-sach-khoa-hoc' => '/course/default/index',

    // learning page
    //15&section=10203&lesson=225
    'lop-hoc/<id:\d+>/<section:\d+>/<lesson:\d+>' => '/course/learning/index',
    'lop-hoc/<id:\d+>/<section:\d+|{nodeId}>' => '/course/learning/index',
    'lop-hoc/<id:\d+>' => '/course/learning/index',
    'chung-nhan/<courseId:\d+>/<userId:\d+>' => '/course/certificate/index',

    // shopping cart
    'gio-hang' => '/cart/default/index',
    'thanh-toan/<orderId:\d+>' => '/cart/checkout/index',
    'thanh-toan' => '/cart/checkout/index',
    'thanh-toan/thanh-cong/<orderId:\d+>' => '/cart/checkout/success',
    'thanh-toan/khong-thanh-cong' => '/cart/checkout/failure',
    'cart/api/ipn/<method:\w+>' => '/cart/api/ipn',
    'thanh-toan-ngay/<products>' => '/cart/default/add-product-to-cart',

    // user page
    'trang-ca-nhan/khoa-hoc' => '/user/course/index',
    'trang-ca-nhan/khoa-hoc/dang-hoc' => '/user/course/started',
    'trang-ca-nhan/khoa-hoc/chua-hoc' => '/user/course/not-started',
    'trang-ca-nhan/tai-lieu' => '/user/document/index',
    'trang-ca-nhan/cau-hoi-giang-vien' => '/user/question/index',
    'trang-ca-nhan/thao-luan' => '/user/discuss/index',
    'trang-ca-nhan/giao-dich/chua-hoan-thanh' => '/user/transaction/in-complete',
    'trang-ca-nhan/giao-dich/hoan-thanh' => '/user/transaction/complete',
    'trang-ca-nhan/thong-tin' => '/user/profile/edit',
    'trang-ca-nhan/diem' => '/user/point/index',
    'trang-ca-nhan/doi-qua' => '/user/gift/exchange',
    'trang-ca-nhan/voucher-coupon' => '/user/gift/index',
    'giang-vien/<slug:[a-zA-Z0-9\-]+>' => '/user/teacher/index',
    'giang-vien/<slug:[a-zA-Z0-9\-]+>/<affiliate_id:\d+>' => '/user/teacher/index',

    'kich-hoat' => '/user/order/active-cod',
    'dang-ky' => '/user/registration/register',
    'ket-noi' => '/user/registration/connect',
    'dang-nhap' => '/user/security/login',
    'quen-mat-khau' => '/user/recovery/request',
    'user/recover/<id:[0-9]+>/<code:.*>' => '/user/recovery/reset',

    // landing page
    'p/kyna/tuyen-dung' => '/career/default/index',
    'p/kyna/tuyen-dung/<slug:[a-zA-Z0-9\-]+>' => '/career/career/index',

    // faq page
    'p/kyna/cau-hoi-thuong-gap' => '/faq/default/index',
    'p/kyna/cau-hoi-thuong-gap-mapp' => '/faq/mobile/index',
    'p/kyna/cau-hoi-thuong-gap/<slug:[a-zA-Z0-9\-]+>' => '/faq/default/view',


    'p/<dir:[a-z0-9\-\/]+>/<slug:[a-zA-Z0-9\-]+>/<affiliate_id:\d+>' => '/page/default/index',
    'p/<slug:[a-z0-9\-\/]+>/<affiliate_id:\d+>' => '/page/default/index',
    'p/<dir:[a-z0-9\-\/]+>/<slug:[a-zA-Z0-9\-]+>' => '/page/default/index',
    'p/<slug:[a-zA-Z0-9\-\/]+>' => '/page/default/index',
    'users/adpia/index' => '/page/default/adpia',
    'dang-ky-thanh-cong' => '/page/success/index',
    // 've-kyna/<slug:kyna/[a-z0-9-/]+>' => '/page/default/index',
    // 'tuyen-dung/<slug:tuyen-dung/[a-z0-9-/]+>' => '/page/default/index',
    // 'test/<slug:test-page[a-z0-9-/]+>' => '/page/default/index',

    // single course
    '<slug:[a-zA-Z0-9\-]+>-p<id:\d+>/<affiliate_id:\d+>' => 'course/default/view',
    '<slug:[a-zA-Z0-9\-]+>-p<id:\d+>' => 'course/default/view',
    '<slug:[a-zA-Z0-9\-]+>/<affiliate_id:\d+>' => 'course/default/view',
    '<slug:[a-zA-Z0-9\-]+>' => 'course/default/view',
    [
        'pluralize' => false,
        'class' => 'yii\rest\UrlRule',
        'controller' => [
            'app/modules/email/default' => 'email/default',
        ],
        'extraPatterns' => [
            'post get-email' => 'get-email',
            'post delete-email' => 'delete-email',
            'post check-email' => 'check-email',
        ],
        'tokens' => [],
    ],

    'api/partner/<slug:[a-zA-Z0-9\_]+>' => 'api/partner/ipn',

];
