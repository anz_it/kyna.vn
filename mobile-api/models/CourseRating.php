<?php
/**
 * Created by PhpStorm.
 * User: truongn
 * Date: 11/3/17
 * Time: 5:09 PM
 */

namespace mobile\models;


use yii\data\ActiveDataProvider;
use  Yii;

class CourseRating extends \kyna\course\models\CourseRating
{

    public  $time;
    public  $user_name;
    public function fields()
    {
        $fields = ['score','review_content','time','user_name'];
        return $fields;
    }
    public function init(){

        $this->on(self::EVENT_AFTER_FIND, [$this, 'fillProperty']);
        $this->on(self::EVENT_AFTER_INSERT, [$this, 'fillProperty']);
    }

    public function rules()
    {
        return
        [
            [['score_of_content', 'score_of_video', 'score_of_teacher','review_content'], 'required'],
            [['review_content'], 'string', 'min' => 30],
            [[ 'score_of_content', 'score_of_video', 'score_of_teacher'], 'number', 'min' => 0, 'max' => 5],

        ];

    }


    public function fillProperty()
    {
        $this->time = Yii::$app->formatter->asDatetime($this->created_time);
        $this->user_name = !empty($this->cheat_name) ? $this->cheat_name : (!empty($this->user->profile->name) ? $this->user->profile->name : $this->user->email);
    }



}