<?php
/**
 * Created by PhpStorm.
 * User: truongnguyen
 * Date: 11/16/17
 * Time: 5:19 PM
 */

namespace mobile\models;


class QuizQuestion extends \kyna\course\models\QuizQuestion
{
    private $_processor;
    public function fields()
    {
        $fields = ['id', 'type', 'image_url', 'sound_url', 'video_embed', 'content', 'number_of_answers'];
        return $fields;
    }

    public function getProcessor()
    {
        if ($this->_processor == null) {
            switch ($this->type) {
                case self::TYPE_MULTIPLE_CHOICE:
                    $this->_processor = new MultipleChoiceQuestionProcessor();
                    break;

                case self::TYPE_ONE_CHOICE:
                    $this->_processor = new SingleChoiceQuestionProcessor();
                    break;

                case self::TYPE_WRITING:
                    $this->_processor = new EssayQuestionProcessor();
                    break;

                case self::TYPE_FILL_INTO_DOTS:
                    $this->_processor = new FillInQuestionProcessor();
                    break;
                case self::TYPE_CONNECT_ANSWER:
                    $this->_processor = new ConnectQuestionProcessor();

            }

            $this->_processor->setQuestion($this);
        }

        return $this->_processor;
    }

    public function getNumber_of_answers()
    {
        if ($this->type == QuizQuestion::TYPE_FILL_INTO_DOTS) {
            return count(QuizQuestion::findAll(['parent_id' => $this->id]));
        } else {
            return null;
        }
    }

}