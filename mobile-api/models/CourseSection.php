<?php
/**
 * Created by PhpStorm.
 * User: truongn
 * Date: 11/3/17
 * Time: 5:09 PM
 */

namespace mobile\models;


use yii\data\ActiveDataProvider;
use  Yii;

class CourseSection extends \kyna\course\models\CourseSection
{

    public $finishedLessons = [];
    public function fields()
    {
        $fields = ['name', 'id'];
        return $fields;
    }


}