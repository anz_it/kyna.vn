<?php
/**
 * Created by PhpStorm.
 * User: khanhphan
 * Date: 9/26/17
 * Time: 5:02 PM
 */

namespace mobile\models;

use yii\helpers\Url;
use yii\data\ActiveDataProvider;
use Yii;
class Category extends \kyna\course\models\Category
{

    public $icon_url ;
    public $type_of_course;
    public $value;

    public function init()
    {
        parent::init();
        $this->on(self::EVENT_AFTER_FIND, [$this, 'fillProperty']);
    }
    public function search($params)
    {
        $query = self::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $query->andFilterWhere([
            'parent_id' => isset($params['parent_id']) ? $params['parent_id'] : null,
            'status' => isset($params['status']) ? $params['status'] : 1,
            'is_deleted' => 0,
        ]);

        return $dataProvider;
    }

    public function fields()
    {
        $fields = ['id', 'name','icon_url','type_of_course', 'value'];
        return $fields;
    }

    public function fillProperty()
    {
        if($this->slug == "tat-ca-khoa-hoc" || $this->slug == "/") {
            $this->type_of_course = "special_link";
            $this->value = "all-course";
        }
        if($this->slug == "khoa-hoc-combo") {
            $this->type_of_course = "special_link";
            $this->value = "combo-course";
        }
        if($this->slug != "khoa-hoc-combo" && ($this->slug != "tat-ca-khoa-hoc" &&  $this->slug != "/")) {
            $this->type_of_course = "normal_link";
            $this->value = "single-course";
        }

        if(!empty($this->home_icon))
            $this->icon_url = $this->home_icon;
        $this->icon_url = $this->menu_icon;
        if(empty($this->icon_url)){
            if(!empty($this->parent)){
                $pcategory = $this->parent;
                if(!empty($pcategory->home_icon))
                    $this->icon_url = $pcategory->home_icon;
                $this->icon_url = $pcategory->menu_icon;
            }
        }
    }

    public static function getList($parent_id = 0, $select = ['*'])
    {
        if (!($list = \Yii::$app->cache->get('children_category_of_'.$parent_id)) OR !isset(Yii::$app->cache)) {
            $list = self::find()->select($select)
                ->andWhere(['parent_id' => $parent_id])
                ->andFilterWhere(['status' => self::STATUS_ACTIVE])
                ->orderBy('order')->all();

            if (isset(\Yii::$app->cache)) {
                \Yii::$app->cache->set('featured_course_ids', $list, 600);
            }
        }

        return $list;
    }
    public function getUrl($withId = false, $scheme = false)
    {
        if (Yii::$app->category) {
            $params[] = Yii::$app->category->defaultRoute;
            $withId = Yii::$app->category->urlWithId;
        }
        else {
            $url = '/';
            if ($this->_categoryUrlSegments['module']) {
                $url .= $this->_categoryUrlSegments['module'].'/';
            }

            $url .= ($this->_categoryUrlSegments['controller']) ? $this->_categoryUrlSegments['controller'].'/' : 'default/';
            $url .= ($this->_categoryUrlSegments['action']) ? $this->_categoryUrlSegments['action'].'/' : 'index';

            $params[] = $url;
        }

        if ($this->slug) {
            $params['slug'] = $this->slug;
        }
        else {
            // force id if slug not found
            $withId = true;
            $params['slug'] = 'danh-muc';
        }

        if ($withId) {
            $params['catId'] = $this->id;
        }

        if ($this->parent_id > 0 and $parent = $this->parent->slug) {
            $params['parent'] = $parent;
        }

        return Url::toRoute($params, $scheme);
        //return \yii\helpers\Url::toRoute(['/course/default/index', 'catId' => $this->id, 'slug' => $this->slug]);
    }

}