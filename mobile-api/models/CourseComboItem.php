<?php
/**
 * Created by PhpStorm.
 * User: truongnguyen
 * Date: 11/16/17
 * Time: 10:37 AM
 */

namespace mobile\models;


class CourseComboItem extends  \kyna\course_combo\models\CourseComboItem
{
    public function fields()
    {
        $fields = ['id','course_combo_id','course_id','course'];
        return $fields;
    }

    public function getCourse()
    {
        return $this->hasOne(CourseView::className(), ['id' => 'course_id']);
    }
}