<?php
/**
 * Created by PhpStorm.
 * User: truongnguyen
 * Date: 11/16/17
 * Time: 5:35 PM
 */

namespace mobile\models;


class QuizAnswer extends \kyna\course\models\QuizAnswer
{
    public function fields()
    {
        $fields = ['id','content','explanation','image_url','is_correct'];
        return $fields;
    }
}