<?php
/**
 * Created by PhpStorm.
 * User: truongnguyen
 * Date: 11/16/17
 * Time: 1:25 PM
 */

namespace mobile\models;


use kyna\course\models\CourseLessionNote;
use yii\base\Model;

class CourseLessonNote extends CourseLessionNote
{
    public function fields()
    {
        $fields  = ['id','course_lession_id','note','title','playbacktime'];
        return $fields;
    }

    public function rules()
    {
        return [
            [['note','current_run_time'], 'required'],
            [['course_lession_id'], 'required', 'message' => 'Vui lòng chọn bài học trước khi nhập ghi chú'],
        ];
    }
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'course_lession_id' => 'Bài học',
            'user_id' => 'User ID',
            'note' => 'Ghi chú',
            'current_run_time' => 'Thời gian hiện tại',
        ];
    }

    public function getTitle()
    {
       $courseLesson =  CourseLesson::findOne($this->course_lession_id);
       $courseSection = CourseSection::findOne($courseLesson->section_id);
       return '#'. $courseLesson->name .' - '.$courseSection->name  ;
    }



}