<?php
/**
 * Created by PhpStorm.
 * User: truongnguyen
 * Date: 11/16/17
 * Time: 10:05 AM
 */

namespace mobile\models;


use common\helpers\ArrayHelper;
use kyna\base\models\MetaField;
use kyna\order\models\OrderDetails;
use kyna\promotion\models\UserVoucherFree;
use kyna\tag\models\CourseTag;

class CourseCombo extends CourseDetail // \kyna\course_combo\models\CourseCombo
{
    public $description;
    public $type_key;

    public function enableMeta()
    {
        return MetaField::MODEL_COURSE_COMBO;
    }
    public function getObjectKey($type)
    {
        return 'course_id';
    }
    public function init()
    {
        $this->on(self::EVENT_AFTER_FIND, [$this, 'fillProperty']);
    }
    public function fields()
    {
        $fields = ['id','name','image_url','type','type_key','oldPrice','sellPrice','discount_amount','percent_discount','teacherCount','total_users_count','description','benefit','object'
        ,'courses', 'promotion_image_url'];
        return $fields;
    }

    public function getCourses()
    {
        return $this->hasMany(CourseComboItem::className(), ['course_combo_id' => 'id']);
    }
    public function getTeacherCount()
    {
        $teachers = [];
        foreach ($this->comboItems as $item) {
            if (!key_exists($item->course->teacher_id, $teachers)) {
                $teachers[$item->course->teacher_id] = $item->course->teacher_id;
            }
        }

        return count($teachers);
    }
    public function getDiscount_rate()
    {
        if($this->sellPrice == 0)
        {
            return 0;
        } else {
            return \Yii::$app->formatter->asPercent( 1 - ($this->sellPrice / $this->oldPrice) );
        }

    }
    public function fillProperty()
    {
        $this->discount_amount = $this->getDiscountAmount();
        $this->percent_discount = parent::getDiscountPercent();
        $this->type_key = $this->getMobileTypeKey();
        if(!empty(\Yii::$app->params['enable_app_promotion'])) {
            if(!in_array($this->id, \Yii::$app->params['not_apply_promo_course_ids'])) {
                $this->promotion_image_url = '/img/app_promotion/banner_detail.png?v=2.0';
            }else{
                $this->promotion_image_url = '/img/app_promotion/banner_detail_not_discount.png?v=2.0';
            }
        }
        else if(UserVoucherFree::isDateRunCampaignVoucherFree()){
            $tagVoucherFree = Tag::find()->andWhere(['slug' => 'voucher-free', 'status' => Tag::STATUS_ACTIVE])->one();
            if(!empty($tagVoucherFree)){
                $courseTags = CourseTag::find()->select(['course_id'])->andWhere(['tag_id' => $tagVoucherFree->id])->asArray()->all();
                $courseTagIds = ArrayHelper::getColumn($courseTags, 'course_id');
                if(in_array($this->id, $courseTagIds)){
                    $this->promotion_image_url = '/img/app_promotion/free_kyna.png?v=1.0';
                }
            }
        }
    }

    private function getMobileTypeKey()
    {
        if(method_exists($this, 'getTypeKey')){
            return  parent::getTypeKey();
        }

        $typeKeys = ['1'=>'video', '2'=>'combo','3'=>'app'];
        $type  = $this->type;
        if(!empty($typeKeys[$type]))
            return $typeKeys[$type];
        return null;
    }

    public static function find()
    {
        $query = parent::find();

        return $query->andWhere(['type' => self::TYPE_COMBO]);
    }
    public function getOrderDetails()
    {
        return OrderDetails::find()->alias('t')->where(['course_combo_id' => $this->id])->groupBy(['course_combo_id', 'order_id']);
    }

    public function getTotal_users_count()
    {
        $query = new \yii\db\Query();
        $count = $query->select('user_id')->from('orders o')
            ->leftJoin('order_details t', 'o.id = t.order_id')
            ->andWhere(['!=', 'o.user_id', 0])
            ->andWhere(['!=', 'o.status', Order::ORDER_STATUS_CANCELLED])
            ->andWhere(['t.course_combo_id' => $this->id])
            ->groupBy([ 'o.user_id'])->count();
        return $count;
    }
}