<?php
/**
 * Created by PhpStorm.
 * User: truongn
 * Date: 11/20/17
 * Time: 11:07 AM
 */

namespace mobile\models;


use common\helpers\StringHelper;
use dektrium\user\helpers\Password;
use Psr\Log\InvalidArgumentException;
use yii\base\InvalidParamException;
use yii\base\Model;

class ChangePassword extends Model
{
    public $current_password;
    public $new_password;
    public $new_password_repeat;
    const EVENT_AFTER_SAVE = 'afterSave';

    public function rules()
    {
        return [
            [['current_password','new_password_repeat','new_password'],'required'],
            ['current_password', 'checkCorrectPassword'],
            ['new_password_repeat', 'compare', 'compareAttribute' => 'new_password', 'message' => 'Mật khẩu mới không giống nhau.'],
        ];
    }

    public function attributeLabels()
    {
        return
        [
            'current_password' => 'Mật khẩu hiện tại',
            'new_password' => 'Mât khẩu mới',
            'new_password_repeat' => 'Xác nhận mật khẩu mới'
        ];
    }

    public function checkCorrectPassword($attribute, $params)
    {
        $user = \Yii::$app->user->identity;
        if (!$this->validatePassword($this->$attribute, $user->password_hash)) {
            $this->addError($attribute, "Mật khẩu hiện tại không đúng.");
        }
    }


    private function validatePassword($password, $hash)
    {
        if (!preg_match('/^\$2[axy]\$(\d\d)\$[\.\/0-9A-Za-z]{22}/', $hash, $matches)
            || $matches[1] < 4
            || $matches[1] > 30
        ) {

            throw new InvalidArgumentException('Hash is invalid.');
        }

        $test = crypt($password, $hash);
        $n = strlen($test);


        if ($n !== 60) {
            return false;
        }

        return $this->compareString($test, $hash);

    }

    private function compareString($expected, $actual)
    {
        $expected .= "\0";
        $actual .= "\0";
        $expectedLength = StringHelper::byteLength($expected);
        $actualLength = StringHelper::byteLength($actual);
        $diff = $expectedLength - $actualLength;
        for ($i = 0; $i < $actualLength; $i++) {
            $diff |= (ord($actual[$i]) ^ ord($expected[$i % $expectedLength]));
        }
        return $diff === 0;
    }


    public function save($validate = true)
    {
        if ($validate && !$this->validate()) {
            return false;
        }
        $user = \Yii::$app->user->identity;

        if (!empty($this->new_password)) {
            $user->password_hash = \Yii::$app->security->generatePasswordHash($this->new_password);
        }

        $result =  $user->save();

        // trigger afterSave event function
        $this->trigger(self::EVENT_AFTER_SAVE);

        return $result;
    }

    /**
     * @desc call after saving profile
     * @return boolean
     */
    public function afterSave()
    {
        $this->new_password = $this->current_password = $this->new_password_repeat = null;

        return true;
    }
}