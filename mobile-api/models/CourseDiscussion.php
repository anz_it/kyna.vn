<?php
/**
 * Created by PhpStorm.
 * User: truongnguyen
 * Date: 11/16/17
 * Time: 2:10 PM
 */

namespace mobile\models;


class CourseDiscussion extends \kyna\course\models\CourseDiscussion
{
    public function fields()
    {
       $fields = ['comment','user_name','user_image','post_time','parent_id','id','user_id','childrens'];
       return $fields;
    }

    public function getPost_time()
    {
        return parent::getPostedTime();
    }
    public function getUser_name()
    {
        return \kyna\user\models\User::findOne($this->user_id)->profile->name;

    }
    public function getUser_image()
    {
        return \kyna\user\models\User::findOne($this->user_id)->avatarImage;
    }

    public function getChildrens()
    {
        return CourseDiscussion::find()
            ->andWhere(['parent_id' => $this->id])
            ->orderBy(['created_time' => SORT_ASC])->limit(5)->all();
    }

}