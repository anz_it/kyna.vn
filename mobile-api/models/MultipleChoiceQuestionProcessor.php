<?php
/**
 * Created by PhpStorm.
 * User: khanhphan
 * Date: 11/2/16
 * Time: 4:08 PM
 */

namespace mobile\models;


class MultipleChoiceQuestionProcessor extends QuestionProcessor
{
    
    public function calculateScore($answer)
    {
        $answerIds = explode(', ', $answer);
        
        if (empty($answerIds)) {
            return 0;
        }
        
        $correctAnswerQuery = $this->getQuestion()->getCorrectAnswers();
        $correctAnswerIds = array_keys($correctAnswerQuery->select('id')->indexBy('id')->asArray()->all());
        
        $diff = array_merge(array_diff($answerIds, $correctAnswerIds), array_diff($correctAnswerIds, $answerIds));
        if (count($diff) > 0) {
            // wrong, then score = 0
            return 0;
        }
        
        // right => calculate score and return
        return $this->getQuestion()->calculateScore();
    }
    


}