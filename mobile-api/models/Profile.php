<?php
/**
 * Created by PhpStorm.
 * User: truongn
 * Date: 11/20/17
 * Time: 10:12 AM
 */

namespace mobile\models;


use dektrium\user\helpers\Password;
use yii\base\Model;
use yii\helpers\Html;

class Profile extends \yii\base\Model
{

    public $name;
    public $email;
    public $gender;
    public $birthday;
    public $address;
    public $phone;
    public $location_id;
    public $location_name;
    public $city_id;
    public $city_name;
    public $is_receive_email_newsletter;
    public $is_receive_email_new_course_created;
    public $avatar;


    const EVENT_AFTER_SAVE = 'afterSave';

    public function init()
    {
        $ret = parent::init();

        if ($user = \Yii::$app->user->identity) {
            $this->name = $user->profile->name;
            $this->email = $user->email;
            $this->phone = $user->profile->phone_number;
            $this->birthday = $user->birthday;
            $this->gender = $user->gender;
            $this->location_id = $user->location_id;
            $this->address = $user->address;
            $this->is_receive_email_newsletter = $user->is_receive_email_newsletter;
            $this->is_receive_email_new_course_created = $user->is_receive_email_new_course_created;
            $this->avatar = $user->avatar;

        }
        return $ret;
    }

    public function rules()
    {
        return [
            [['name', 'email', 'gender', 'phone'], 'required'],
            ['name', 'string', 'max' => 50],
            ['email', 'email'],
            ['email', 'checkUnique'],
            [['birthday', 'address', 'location_id', 'is_receive_email_newsletter', 'is_receive_email_new_course_created'], 'safe']
        ];
    }

    public function beforeValidate()
    {
        $this->name = Html::encode($this->name, false);
        $this->address = Html::encode($this->address, false);
        return parent::beforeValidate();
    }

    /**
     * @desc check attribute is unique
     * @param type $attribute
     * @param type $params
     */
    public function checkUnique($attribute, $params)
    {
        if (User::find()->where([$attribute => $this->$attribute])->andWhere(['!=', 'id', \Yii::$app->user->id])->exists()) {
            $this->addError($attribute, $this->getAttributeLabel($attribute) . " đã được sử dụng, vui lòng nhập lại.");
        }
    }

    /**
     * @desc validate current user password before update new password
     * @param type $attribute
     * @param type $params
     */


    public function attributeLabels()
    {
        return [
            'name' => 'Họ tên',
            'email' => 'Email',
            'gender' => 'Giới tính',
            'birthday' => 'Ngày sinh',
            'address' => 'Địa chỉ',
            'phone' => 'Số điện thoại',
            'is_receive_email_newsletter' => 'Nhận email từ ban đào tạo khóa học Kyna.vn',
            'is_receive_email_new_course_created' => 'Nhận email nhắc nhở có bài học mới',

        ];
    }

    /**
     * @desc save user profile
     * @return boolean
     */
    public function save($validate = true)
    {
        if ($validate && !$this->validate()) {
            return false;
        }
        $user = \Yii::$app->user->identity;
        $profile = $user->profile;

        // load user meta values
        $user->loadMeta($this->attributes);

        $profile->name = $this->name;
        $profile->phone_number = $this->phone;
        $user->gender = $this->gender;
        $user->is_receive_email_new_course_created = $this->is_receive_email_new_course_created;
        $user->is_receive_email_newsletter = $this->is_receive_email_newsletter;

        $result = $profile->save();


        $result = $result && $user->save();

        // trigger afterSave event function
        $this->trigger(self::EVENT_AFTER_SAVE);

        return $result;
    }


}
