<?php
/**
 * Created by PhpStorm.
 * User: truongn
 * Date: 11/29/17
 * Time: 5:02 PM
 */

namespace mobile\models;


class QuizSession extends \kyna\course\models\QuizSession
{
    private $_answers = [];
    public $remaining_time;
    public $remaining_time_text;
    public $wait_for_mark = false;
    public $show_prev_result = false;
    public $show_redo = true;
    public $message = '';
    public $redo_text = 'Làm lại';


    public function init()
    {
        $this->on(self::EVENT_AFTER_FIND, [$this, 'fillProperty']);
        $this->on(self::EVENT_AFTER_UPDATE, [$this, 'fillProperty']);
        $this->on(self::EVENT_AFTER_INSERT, [$this, 'fillProperty']);
    }

    public function fields()
    {
        return [
            'id',
            'duration',
            'is_passed',
            'total_score',
            'total_quiz_score',
            'duration',
            'start_time',
            'remaining_time',
            'remaining_time_text',
            'wait_for_mark',
            'message',
            'show_prev_result',
            'show_redo',
            'redo_text',
            'status'
        ];
    }

    public function setAnswers($value)
    {
        $sessionAnswerIds = array_keys($value);
        $sessionAnswers = QuizSessionAnswer::find()->where(['id' => $sessionAnswerIds])->indexBy('id')->all();

        foreach ($sessionAnswers as $sessionAnswer) {
            $answer = isset($value[$sessionAnswer->id]) ? $value[$sessionAnswer->id] : 0;
            $sessionAnswer->attributes = [
                'answer' => is_array($answer) ? implode(', ', $answer) : $answer,
            ];
        }

        $this->_answers = $sessionAnswers + $this->_answers;
    }

    public function calculateScore()
    {
        foreach ($this->_answers as $answer) {
            /**
             * return 1 for now
             * TODO: Need to calculate with coefficient
             */
            $score = $answer->calculateScore();
        }

    }

    public function fillProperty()
    {
        $this->remaining_time = $this->getRemainingTime();
        $this->remaining_time_text = gmdate('H:i:s', $this->remaining_time);
        $submit_status = [
            QuizSession::STATUS_SUBMITTED,
            QuizSession::STATUS_MARKED,
            QuizSession::STATUS_WAITING_FOR_MARK
        ];
        if (in_array($this->status, $submit_status)) {
            if ($this->status !== QuizSession::STATUS_WAITING_FOR_MARK) {
                $this->show_prev_result = true;
            } else {
                $this->show_prev_result = false;
                $this->wait_for_mark = true;
                $this->message = 'Bạn đã hoàn thành bài tập và đã nộp bài, vui lòng chờ giảng viên chấm điểm';
            }
            $quiz = $this->quiz;
            $cannotRedo = $quiz->max_of_redo > 0 && intval($this->countOfRedo) >= intval($quiz->max_of_redo);
            $this->show_redo = !$cannotRedo;
            $this->redo_text .= !empty($quiz->max_of_redo) ? " ({$this->countOfRedo}/{$quiz->max_of_redo})" : '';

        }

    }

    public static function initSession($quizId, $userId)
    {
        $session = self::getIsDone($userId, $quizId);

        if (!empty($session)) {
            return $session;
        }

        $quizSession = self::find()->andWhere([
            'quiz_id' => $quizId,
            'user_id' => $userId,
            'status' => [self::STATUS_DOING, self::STATUS_DEFAULT, self::STATUS_WAITING_FOR_MARK]
        ])->one();

        if ($quizSession == null) {
            $quiz = Quiz::findOne($quizId);

            $quizSession = new QuizSession();
            $quizSession->attributes = [
                'quiz_id' => $quizId,
                'user_id' => $userId,
                'status' => self::STATUS_DEFAULT,
                'start_time' => null,
                'duration' => (int)$quiz->duration,
            ];
            if (!$quizSession->save()) {
                return false;
            }

            $quizSession->initAnswers($quiz);
        } else {
            if ($quizSession->status == self::STATUS_DOING && $quizSession->isExpired) {
                $quizSession->finish();
                $quizSession->save();
            }
        }

        return $quizSession;
    }

    public function initAnswers($quiz)
    {
        if ($quiz->is_random_question) {
            $orderBy = new \yii\db\Expression('rand()');
        } else {
            $orderBy = ['t.order' => SORT_ASC];
        }

        $columns = ['quiz_session_id', 'quiz_detail_id', 'quiz_question_id', 'parent_id'];
        $details = \kyna\course\models\QuizDetail::find()->andWhere(['t.quiz_id'=> $quiz->id,'t.is_deleted' => 0, 't.status' => 1])->alias('t')->select(['t.id', 't.quiz_question_id', 't.parent_id'])->joinWith('quizQuestion')->andWhere([QuizQuestion::tableName() . '.status' => QuizQuestion::STATUS_ACTIVE])->orderBy($orderBy)->indexBy('id')->asArray()->all();
        $answers = [];
        foreach ($details as $id => $detail) {
            $answers[] = [$this->id, $id, $detail['quiz_question_id'], $detail['parent_id']];
        }

        QuizSessionAnswer::insertMany($columns, $answers);
    }

}