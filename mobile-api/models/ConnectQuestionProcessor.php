<?php
namespace mobile\models;



use kyna\course\models\QuizAnswer;

class ConnectQuestionProcessor extends QuestionProcessor
{

    /**
     * @param string $answer the value can be integer, string
     * @return mixed
     */
    public function calculateScore($answer)
    {
        if ($this->question->parent_id == null || $this->question->parent_id == 0) {
            return 0;
        }
        // this answers is id;
        $answerObject = QuizAnswer::findOne($answer);

        if ($answerObject != null && $answerObject->question_id == $this->question->id)
            return 1;

        return 0;
    }
}