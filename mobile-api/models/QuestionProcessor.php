<?php

namespace mobile\models;
use yii\base\Object;

/**
 * Class QuestionProcessor
 * @package app\modules\course\models
 *
 * @property QuizQuestion $question
 */
class QuestionProcessor extends Object
{

    private $_question;



    public function setQuestion($question)
    {
        $this->_question = $question;
    }

    public function getQuestion()
    {
        return $this->_question;
    }


}
