<?php
/**
 * Created by PhpStorm.
 * User: truongn
 * Date: 11/20/17
 * Time: 11:55 AM
 */

namespace mobile\models;


use app\models\Token;
use app\modules\user\components\Mailer;
use dektrium\user\Finder;
use dektrium\user\models\RecoveryForm;
use yii\base\Model;


class ForgotPassword extends RecoveryForm
{
    public $email;
    public $mailer;

    public function __construct(\mobile\components\Mailer $mailer, Finder $finder, $config = [])
    {
        $this->mailer = $mailer;
        $this->mailer->setRecoverySubject('[Kyna.vn] Yêu cầu phục hồi mật khẩu.');

        $this->finder = $finder;
        parent::__construct($mailer, $finder, $config);
    }

    /*public function fields()
    {
        $fields = ['email'];
        return $fields;
    } */

    public function rules()
    {
        return [
          ['email','required'],
          ['email','checkExist'],
          ['email','checkSpam']
        ];
    }

    public function checkExist($attribute)
    {
        if (!$this->hasErrors($attribute)) {
            $user =  User::findOne(['email'=>$this->email]);
            if (is_null($user)) {
                $this->addError($attribute, 'Email không tồn tại trong hệ thống');
            }
        }
    }

    public function checkSpam($attribute)
    {
        if (!$this->hasErrors($attribute)) {
            $user =  User::findOne(['email'=>$this->email]);
            if (!is_null($user)) {
                $isExistToken = \dektrium\user\models\Token::find()->where(['user_id' => $user->id])->andWhere(['>=', 'created_at', strtotime('-5 minutes')])->exists();
                if ($isExistToken) {
                    $this->addError('email', 'Bạn vừa thực hiện request, vui lòng kiểm tra email trước khi gửi yêu cầu mới.');
                }
            }
        }
    }

    public function sendRecoveryMessage()
    {
        if (!$this->validate()) {
            return false;
        }

        $user = User::findOne(['email'=>$this->email]);

        if ($user instanceof User) {
            /** @var Token $token */
            $token = \Yii::createObject([
                'class' => \dektrium\user\models\Token::className(),
                'user_id' => $user->id,
                'type' => \dektrium\user\models\Token::TYPE_RECOVERY,
            ]);

            if (!$token->save(false)) {
                return false;
            }

            if (!$this->mailer->sendRecoveryMessage($user, $token)) {
                return false;
            }

        /*    return $this->mailer->sendMessage(
                $user->email,
                $this->mailer->getRecoverySubject(),
                '@common/mail/user/recover_password',
                [
                    'fullname' => !empty($user->profile->name) ? $user->profile->name : $user->email,
                    'link' => $token->url,
                ]
            ); */
        }

        return true;
    }



}