<?php
/**
 * Created by PhpStorm.
 * User: truongnguyen
 * Date: 11/16/17
 * Time: 5:23 PM
 */

namespace mobile\models;



class QuizDetail extends \kyna\course\models\QuizDetail
{
    public function fields()
    {
        $fields = ['id','question','quiz_question_id','parent_id','answers'];
        return $fields;
    }

    public function getQuestion()
    {
        return QuizQuestion::findOne($this->quiz_question_id);
    }

    public function getAnswers()
    {
        return QuizAnswer::find()->where(['question_id'=> $this->quiz_question_id])->all();
    }
}