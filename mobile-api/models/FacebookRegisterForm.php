<?php
/**
 * Created by PhpStorm.
 * User: truongn
 * Date: 11/27/17
 * Time: 2:53 PM
 */

namespace mobile\models;


use yii\base\Model;

class FacebookRegisterForm extends Model
{
    public $access_code;
    public $name;
    public $id ;
    public $email;
    public function rules()
    {
        return [
            ['access_code','required']
        ];
    }

    public function getFacebookInfo()
    {
        $fb = new \Facebook\Facebook(['app_id' => \Yii::$app->params['facebook_client_id'], 'app_secret' => \Yii::$app->params['facebook_client_secret']]);
        $fb->setDefaultAccessToken($this->access_code);
        $request = $fb->request('GET', '/me?fields=id,name,email');
        try {
            $response = $fb->getClient()->sendRequest($request);
        } catch (Facebook\Exceptions\FacebookResponseException $e) {
            $this->addError('',$e->getMessage());
            return false;
        } catch (Facebook\Exceptions\FacebookSDKException $e) {
            $this->addError('',$e->getMessage());
            return false;
        }
        $graphNode = $response->getGraphNode();
        $this->id = $graphNode['id'];
        $this->name = $graphNode['name'];
        if(!empty($graphNode['email']))
            $this->email = $graphNode['email'];
        else
            $this->email = $graphNode['id']."@facebook.com";
        return true;

    }

}