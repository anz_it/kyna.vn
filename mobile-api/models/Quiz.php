<?php
/**
 * Created by PhpStorm.
 * User: macintosh
 * Date: 9/29/17
 * Time: 3:07 PM
 */

namespace mobile\models;


class Quiz extends \kyna\course\models\Quiz
{
    public $is_random_question;
   // public $duration;
    public $is_back_or_next;
    public $is_show_result_when_done_a_question;
    public $style_show;
    public function fields()
    {
        return [
            'id',
            'name',
            'image_url',
            'is_back_or_next',
            'is_show_result_when_done_a_question',
            'style_show',
            'details',



        ];
    }

    public function init()
    {
        $this->on(self::EVENT_AFTER_FIND, [$this, 'fillProperty']);
    }

    public function getDetails()
    {
        return $this->hasMany(QuizDetail::className(), ['quiz_id' => 'id'])->where(['parent_id' => null]);
    }

    /*public function getDetails()
    {
        $tmp =  $this->hasMany(QuizDetail::className(), ['quiz_id' => 'id'])->where(['parent_id' => null])->all();
        if($this->__get('is_random_question') == '1')
        {
            shuffle($tmp);
        }
        return $tmp;
    }*/

    public function fillProperty()
    {
        $this->is_random_question =  $this->__get('is_random_question');
        $this->duration = $this->__get('duration');
        $this->is_back_or_next = $this->__get('is_back_or_next');
        $this->is_show_result_when_done_a_question = $this->__get('is_show_result_when_done_a_question');
        $this->style_show = $this->__get('style_show') == 'Từng câu một' ? 1 : 2;

    }

    public function getSession($userId)
    {
        return QuizSession::initSession($this->id, $userId);
    }

}