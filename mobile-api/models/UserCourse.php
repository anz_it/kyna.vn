<?php
/**
 * Created by PhpStorm.
 * User: truongn
 * Date: 11/10/17
 * Time: 6:03 PM
 */

namespace  mobile\models;

class UserCourse extends \kyna\user\models\UserCourse
{

    public function fields()
    {
        $fields = ['is_started' ,'is_graduated','course','process','course_id'];
        return $fields;
    }

    public function getCourse()
    {
        return $this->hasOne(Course::className(), ['id' => 'course_id']) ->andWhere(['is_deleted' => Course::BOOL_NO]);
    }

}