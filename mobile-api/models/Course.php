<?php
/**
 * Created by PhpStorm.
 * User: truongn
 * Date: 11/3/17
 * Time: 5:09 PM
 */

namespace mobile\models;


use yii\data\ActiveDataProvider;

class Course extends CourseDetail
{
    public function fields()
    {
        $fields = ['id', 'name',  'image_url', 'oldPrice','sellPrice' , 'percent_discount', 'total_time_text', 'discount_amount'
            , "total_users_count", 'rating', 'teacher_name','is_bought','teacher_title','type','type_key','type_text'];
        return $fields;
    }
    public function fillProperty()
    {
        parent::fillProperty();
        $this->percent_discount = parent::getDiscountPercent();
        $this->discount_amount = $this->getDiscountAmount();

    }

}