<?php
/**
 * Created by PhpStorm.
 * User: truongnguyen
 * Date: 11/16/17
 * Time: 3:02 PM
 */

namespace mobile\models;


class CourseLearnerQna extends \kyna\course\models\CourseLearnerQna
{
    public $tag_name;
    public $avatar_image;
    public $name;
    public function init()
    {
        $this->on(self::EVENT_AFTER_FIND, [$this, 'fillProperty']);
    }
    public function fields()
    {
        $fields = ['id', 'avatar_image', 'content', 'is_on_behalf_of_teacher', 'name', 'tag_name', 'time','question_id', 'answers'];
        return $fields;
    }

    public function getUser_name()
    {
        return User::findOne($this->user_id)->profile->name;
    }

    public function getTime()
    {
        return parent::getPostedTime();
    }
    public function fillProperty()
    {
        $isTeacher = $this->is_on_behalf_of_teacher || $this->user_id == $this->course->teacher_id;
        $isUser = !$isTeacher && ($this->question == null || $this->user_id == $this->question->user_id);
        $tagStr = $isTeacher ? 'GV' : (!$isUser ? 'QTV' : '') ;
        $this->tag_name = $tagStr;
        $user_name = $isTeacher ? $this->course->teacher->profile->name : ($isUser ? $this->user->profile->name : 'Trợ lý học tập Kyna');
        $this->name = $user_name;
        $this->avatar_image = $this->user->avatarImage;
    }


}