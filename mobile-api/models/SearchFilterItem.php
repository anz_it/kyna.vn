<?php
/**
 * Created by PhpStorm.
 * User: truongn
 * Date: 11/21/17
 * Time: 9:45 AM
 */

namespace mobile\models;


use yii\base\Model;

class SearchFilterItem extends Model
{
    public $key;
    public $doc_count;
    public $items;

    public function fields()
    {
       $fileds = ['key','doc_count','description','items'];
       return $fileds;
    }

    public function getDescription()
    {
        switch ($this->key){
            case 'time':
                return 'Tìm theo thời lượng';
            case 'level':
                return 'Tìm theo trình độ yêu cầu';
            case 'discount':
                return 'Tìm theo chương trình khuyến mãi';
            case 'hot':
                return 'Tìm theo đặc điểm khóa học';
            case 'type':
                return 'Tìm theo loại khóa học';
            default:
                return 'N/A';
        }
    }

}