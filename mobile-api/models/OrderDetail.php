<?php
/**
 * Created by PhpStorm.
 * User: truongnguyen
 * Date: 11/15/17
 * Time: 1:12 PM
 */

namespace mobile\models;


use kyna\order\models\OrderDetails;

class OrderDetail extends OrderDetails
{
    public function fields()
    {
        $fields = ['course_id','course_combo_id','total_price','course'];
        return $fields;
    }

    public function getTotal_price()
    {
        return parent::getItemTotal();
    }

    public function getCourse()
    {
        return Course::findOne($this->course_id);
    }

}