<?php

namespace mobile\models;

class CourseView extends CourseDetail
{
    public function fields()
    {
        $fields = ['id', 'name',  'oldPrice','sellPrice' , 'percent_discount', 'discount_amount',
            'teacher_name','teacher_title', 'type' ,'type_key'];
        return $fields;
    }
    public function fillProperty()
    {
        //parent::fillProperty();
        $this->percent_discount = parent::getDiscountPercent();
        $this->discount_amount = $this->getDiscountAmount();
        $this->type_text = $this->getTypeText();
        $this->type_key = $this->getMobileTypeKey();
        if($this->type != Course::TYPE_COMBO)
        {
            $this->teacher_introduction=  $this->teacher->introduction;
            $this->teacher_name =   $this->teacher->profile->name ;
            $this->teacher_title =  $this->teacher->title ;

        }

    }
    private function getMobileTypeKey()
    {
        $typeKeys = ['1'=>'video', '2'=>'combo','3'=>'app'];
        $type  = $this->type;
        if(!empty($typeKeys[$type]))
            return $typeKeys[$type];
        return null;
    }

}