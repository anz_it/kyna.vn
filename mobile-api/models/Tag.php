<?php
/**
 * Created by PhpStorm.
 * User: truongn
 * Date: 12/7/17
 * Time: 1:03 PM
 */

namespace mobile\models;


class Tag extends \kyna\tag\models\Tag
{
    public function fields()
    {
        return [
            'id',
            'image_url',
            'name',
            'tag',
            'slug'

        ];
    }

    public function getName()
    {
        return $this->tag;
    }

}