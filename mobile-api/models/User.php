<?php

/**
 * Created by PhpStorm.
 * User: khanhphan
 * Date: 9/26/17
 * Time: 11:52 AM
 */
namespace mobile\models;

use mobile\helpers\SecurityHelper;

class User extends \kyna\user\models\User
{
    public static function findIdentityByAccessToken($token, $type = null)
    {

        $userId = SecurityHelper::restoreUserIdFromToken($token);
        $model = static::findOne($userId);
        if ($model == null)
            return null;
        if ($model->access_token == $token)
            return $model;

        return null;
    }
}