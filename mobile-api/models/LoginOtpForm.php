<?php
/**
 * Created by PhpStorm.
 * User: Tan
 * Date: 8/30/2018
 * Time: 2:50 PM
 */

namespace mobile\models;


use dektrium\user\helpers\Password;
use mobile\helpers\SecurityHelper;
use yii\base\Model;
use Yii;

class LoginOtpForm extends Model
{
    protected $_order;
    public $activation_code;
    public $email;
    public $password;

    public function rules()
    {
        return [
            [['activation_code'], 'string'],
            [['email', 'password', 'activation_code'], 'required'],
            [['activation_code'], 'checkExist']
        ];
    }

    public function active()
    {

        $loginForm = new LoginForm();
        $loginForm->email = $this->email;
        $loginForm->password = $this->password;
        if ($loginForm->validate()) {
            $loggedIn = $loginForm->login();
            if (!$this->validate()) {
                return $this;
            }
            if ($loggedIn) {
                $user = $this->_order->user;
                $activated = $this->activeCod();
                $avatar = Yii::$app->user->identity->avatarImage;
                $isChangePassword = Password::validate((string)$user->created_at, $user->password_hash);
                return [
                    'cod_activated' => $activated,
                    'login_user' => [
                        'change_password' => $isChangePassword,
                        'access_token' => Yii::$app->user->identity->access_token,
                        'email' => Yii::$app->user->identity->email,
                        'name' => Yii::$app->user->identity->profile->name,
                        'signature' => SecurityHelper::createSignature(Yii::$app->user->identity->access_token),
                        'avatar_url' => $avatar
                    ]
                ];
            }
        } else {
            return $loginForm;
        }
    }


    public function checkExist()
    {
        // check if activation code can use
        $result = $this->getOrder();
        if ($result == null) {
            $this->addError('activation_code', 'Mã kích hoạt không hợp lệ.');
        }
    }

    private function activeCod()
    {
        $ret = false;
        if (empty($this->_order)) {
            $this->_order = $this->getOrder();
        }
        if ($this->_order != null) {
            // active courses
            if (Order::activate($this->_order->id) !== false) {
                $ret = true;
            }
        }
        return $ret;
    }


    public function getOrder()
    {

        if (empty($this->_order)) {
            $this->_order = Order::find()->where([
                'activation_code' => $this->activation_code,
                'user_id' => Yii::$app->user->id,
                'is_activated' => Order::BOOL_NO,

            ])->andWhere(['or', ['status' => Order::ORDER_STATUS_COMPLETE], ['status' => Order::ORDER_STATUS_DELIVERING]])
                ->one();
        }
        return $this->_order;
    }
}