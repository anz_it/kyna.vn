<?php
/**
 * Created by PhpStorm.
 * User: truongn
 * Date: 11/27/17
 * Time: 5:22 PM
 */

namespace mobile\models;


use yii\base\Model;

class SyncPercentForm extends Model
{
    public $duration;
    public $time;
    public $lesson_id;
    public $user_course_id;

    public function rules()
    {
        return [
            [['time','lesson_id','duration','user_course_id'],'required'],

        ];
    }
}