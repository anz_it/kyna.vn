<?php
/**
 * Created by PhpStorm.
 * User: truongn
 * Date: 11/8/17
 * Time: 4:15 PM
 */


namespace mobile\models;


use yii\data\ActiveDataProvider;

class Teacher extends \kyna\course\models\Teacher
{


    public $name;
    public function init()
    {
        $this->on(self::EVENT_AFTER_FIND, [$this, 'fillProperty']);
    }

    public  function  fields()
    {
        $fields = ['introduction','name','avatar','courses','title'];
        return $fields;
    }

    public function getCourses()
    {
        return $this->hasMany(Course::className(), ['teacher_id' => 'id'])->where(['status' => \kyna\course\models\Course::STATUS_ACTIVE]);
    }


    public function fillProperty()
    {
        $this->name = $this->profile->name;
        if(empty($this->avatar)){
            $this->avatar = '/src/img/default.png';
        }
    }

}