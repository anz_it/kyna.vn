<?php
/**
 * Created by PhpStorm.
 * User: Tan
 * Date: 9/11/2018
 * Time: 11:35 AM
 */

namespace mobile\models;

use yii\data\ActiveDataProvider;

class Notification extends \kyna\notification\models\Notification
{
    const SCENARIO_GET_DETAIL = 'SCENARIO_GET_DETAIL';
    public function fields()
    {
        $fields = parent::fields();
        if($this->scenario == self::SCENARIO_GET_DETAIL){
            $fields = [
                'screen_type' => function($model){
                    return $model->screen_type_mobile;
                },
                'notification_type' => function($model){
                    return 'Thông báo';
                },
                'title' => function($model){
                    return $model->title;
                },
                'content' =>function($model){
                    return $model->body;
                },
                'data' => function($model){
                    return $model->data;
                }
            ];
        }
        return $fields;
    }

    public static function getMobileNotifications($page= 1, $limit = 20){
        $topic = YII_ENV == YII_ENV_PROD ? self::TOPIC_LIVE : self::TOPIC_STAGING;
        $query = self::find()
            ->andWhere(['is_type' => self::IS_MOBILE])
            ->andWhere(['topic' => $topic])
            ->andWhere(['is_deleted' => 0])
            ->orderBy(['created_at' => SORT_DESC]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        foreach ($dataProvider->getModels() as $model){
            $model->setScenario(self::SCENARIO_GET_DETAIL);
        }

        $dataProvider->pagination->pageSize = $limit;

        return $dataProvider->getModels();

    }
}