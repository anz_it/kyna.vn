<?php
/**
 * Created by PhpStorm.
 * User: truongn
 * Date: 11/21/17
 * Time: 1:44 PM
 */

namespace mobile\models;


use yii\base\Model;

class FacetItem extends Model
{
    public $key;
    public $value;
    public $count;

    public function fields()
    {
        $fields = ['count','label','value'];
        return $fields;
    }

    public function getLabel()
    {
        switch ($this->key)
        {
            case 'discount':
                return CourseElastic::getFacetDiscount($this->value);
            case 'level':
                return CourseElastic::getFacetLevel($this->value);
            case 'hot':
                return CourseElastic::getFacetHot($this->value);
            case 'time':
                return CourseElastic::getFacetTime($this->value);
            case 'type' :
                return CourseElastic::getFacetType($this->value);
        }
    }
}