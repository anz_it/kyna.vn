<?php
/**
 * Created by PhpStorm.
 * User: truongn
 * Date: 11/3/17
 * Time: 5:09 PM
 */

namespace mobile\models;

use common\campaign\CampaignTet;
use common\helpers\ArrayHelper;
use kyna\course\models\Course;
use kyna\promotion\models\UserVoucherFree;
use kyna\tag\models\CourseTag;
use yii\data\ActiveDataProvider;

class CourseDetail extends Course
{
    public $discount_amount;
    public $total_users;
    public $total_time_text;
    public $lesson_count;
    public $level_text;
    public $teacher_name;
    public $teacher_title;
    public $certificate;
    public $what_you_learn;
    public $teacher_introduction;
    public $is_bought;
    public $support_devices_text ;
    public $purchase_type_text;
    public $teacher_avatar;
    public $type_text;
    public $type_key;
    public $promotion_image_url;

    public function init()
    {
        $this->on(self::EVENT_AFTER_FIND, [$this, 'fillProperty']);
    }

    public function fields()
    {
        $fields = ['id', 'name', 'type','video_url', 'image_url','oldPrice', 'sellPrice','discount_amount', 'discount_percent', 'total_time_text','type_text','type_key'
            , "total_users_count", "teacher_name", 'lesson_count', 'level_text', 'teacher_title', 'certificate', 'rating_users_count',
            'rating', 'sections', 'overview','ratings','teacher_introduction','is_bought','support_devices_text','purchase_type_text','teacher_id','teacher_avatar','screenshots', 'promotion_image_url'];
        return $fields;
    }
    private function getMobileTypeKey()
    {
        if(method_exists($this, 'getTypeKey')){
            return  parent::getTypeKey();
        }

        $typeKeys = ['1'=>'video', '2'=>'combo','3'=>'app'];
        $type  = $this->type;
        if(!empty($typeKeys[$type]))
            return $typeKeys[$type];
        return null;
    }



    public function getDiscount_percent()
    {
        return parent::getDiscountPercent();

    }

    public function fillProperty()
    {
        $this->discount_amount = $this->getDiscountAmount();
        $this->lesson_count = $this->getLessonCount();
        $this->level_text = $this->getLevelText();
        $this->total_time_text = $this->getTotalTimeText(false);
        $this->certificate = "Cấp chứng nhận hoàn thành";
        $this->what_you_learn = "What you learn from this lesson";
        $this->support_devices_text = "Xem được trên máy tính, điện thoại, tablet";
        $this->purchase_type_text = $this->getPurchaseTypeText();
        $teacher_avatar = '/src/img/default.png';
        if(!empty($this->teacher)){
            if(!empty($this->teacher->avatar)){
                $teacher_avatar = $this->teacher->avatar;
            }
        }
        $this->teacher_avatar = $teacher_avatar;
        $this->type_text = $this->getTypeText();
        $this->type_key = $this->getMobileTypeKey();
        if($this->type != Course::TYPE_COMBO)
        {
            $this->teacher_introduction=  $this->teacher->introduction;
            $this->teacher_name =   $this->teacher->profile->name ;
            $this->teacher_title =  $this->teacher->title ;

        }

        if(\Yii::$app->user->isGuest)
        {
            $this->is_bought= false;
        } else {
            $this->is_bought = UserCourse::find()
                ->where(['user_id' => \Yii::$app->user->getId()])
                ->andWhere(['course_id' => $this->id])
                ->exists();
        }
        $is_review  = \Yii::$app->params['is_review'];
        if($is_review){
            $this->teacher_introduction = str_replace('Kyna.vn','KYNA', $this->teacher_introduction);
            $this->teacher_introduction = str_replace('kyna.vn','KYNA', $this->teacher_introduction);
            $this->teacher_introduction = str_replace('KYNA.VN','KYNA', $this->teacher_introduction);


            $this->overview = str_replace('Kyna.vn','KYNA', $this->overview);
            $this->overview = str_replace('kyna.vn','KYNA', $this->overview);
            $this->overview = str_replace('KYNA.VN','KYNA', $this->overview);

            $this->overview = str_replace('http://','#', $this->overview);
            $this->overview = str_replace('https://','#', $this->overview);

            $this->teacher_introduction = str_replace('http://','#', $this->teacher_introduction);
            $this->teacher_introduction = str_replace('https://','#', $this->teacher_introduction);

        }

        if(CampaignTet::InTimesCampaign()) {
            if($this->type != self::TYPE_COMBO) {
                $this->promotion_image_url = '/img/campaign-tet/banner-app-detail.png';
            }
        }
        else if(UserVoucherFree::isDateRunCampaignVoucherFree()){
            $tagVoucherFree = Tag::find()->andWhere(['slug' => 'voucher-free', 'status' => Tag::STATUS_ACTIVE])->one();
            if(!empty($tagVoucherFree)){
                $courseTags = CourseTag::find()->select(['course_id'])->andWhere(['tag_id' => $tagVoucherFree->id])->asArray()->all();
                $courseTagIds = ArrayHelper::getColumn($courseTags, 'course_id');
                if(in_array($this->id, $courseTagIds)){
                    $this->promotion_image_url = '/img/app_promotion/free_kyna.png?v=1.0';
                }
            }

        }

    }

    public function getRatings()
    {
        $tmp = $this->hasMany(CourseRating::className(), ['course_id' => 'id']);
        return $tmp->where(['status' => CourseRating::STATUS_ACTIVE]);
    }
    public function getLessonCount()
    {
        return parent::getSections()->andWhere([
            '>=',
            'lvl',
            1
        ])->count();
    }
    public function getSections()
    {
        return $this->hasMany(CourseSectionLearning::className(), ['course_id' => 'id'])->andOnCondition([
            'course_id' => $this->id,
            'lvl' => 0,
            'visible' => self::BOOL_YES,
            'active' => self::BOOL_YES
        ])->orderBy('root, lft');

    }

    public function getLearnerQuestions()
    {
        return $this->hasMany(CourseLearnerQna::className(), ['course_id' => 'id'])
            ->onCondition(['or', ['question_id' => 0], ['question_id' => NULL]]);
    }


}