<?php
/**
 * Created by PhpStorm.
 * User: khanhphan
 * Date: 9/28/17
 * Time: 3:04 PM
 */

namespace mobile\models;


use kyna\user\models\Profile;
use mobile\helpers\SecurityHelper;
use Yii;

use dektrium\user\models\RegistrationForm as BaseRegistrationForm;
use common\validators\PhoneNumberValidator;
use yii\helpers\Html;

class RegistrationForm extends BaseRegistrationForm
{

    public $name;
    public $phone;

    public function rules()
    {
        $user = $this->module->modelMap['User'];

        return array_merge(parent::rules(), [
            'nameRequired' => ['name', 'required'],
            'nameLength' => ['name', 'string', 'min' => 3, 'max' => 50],

            'emailUnique'   => [
                'email',
                'unique',
                'targetClass' => $user,
                'message' => 'Email đã được sử dụng'
            ],
            ['phone', 'required'],
            ['phone', PhoneNumberValidator::className()],
            'usernameRequired' => ['username', 'safe'],
        ]);
    }

    public function beforeValidate()
    {
        $this->username = "app_".Yii::$app->security->generateRandomString(10);
        $this->name = Html::encode($this->name, false);
        return parent::beforeValidate();
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'email'    => 'Email',
            'password' => 'Mật khẩu',
            'phone' => 'Điện thoại',
            'name' => 'Họ tên'
        ];
    }

    public function register()
    {
        if (!$this->validate()) {
            return ['success' => false,  'user' => $this];
        }

        /** @var User $user */
        $user = Yii::createObject(User::className());
        $user->setScenario('register');
        $this->loadAttributes($user);


        if (!$user->register()) {
            return ['success' => false, 'user' => $user];
        } else {
            $profile = $user->profile;
            if ($profile == null) {
                $profile = new Profile();
                $profile->user_id = $user->id;

            }
            $profile->name = $this->name;
            $profile->phone_number = $this->phone;
            $profile->save(false);

            //Add default course
            if(!empty($user->id)) {
                $userCourse = new UserCourse();
                $userCourse->user_id = $user->id;
                $userCourse->course_id = 10;
                $userCourse->is_activated = UserCourse::BOOL_YES;
                $userCourse->activation_date = time();
                $userCourse->save();
            }
        }
        $user->access_token = SecurityHelper::generateAccessToken($user->id);
        $user->save(false);

        return ['success' => true, 'user' => $user];
    }

    public function formName()
    {
        return '';
    }
}
