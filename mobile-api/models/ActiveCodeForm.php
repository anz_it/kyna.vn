<?php
/**
 * Created by PhpStorm.
 * User: truongn
 * Date: 11/22/17
 * Time: 2:40 PM
 */

namespace mobile\models;


use app\modules\user\models\forms\OrderForm;
use yii\base\Model;

class ActiveCodeForm extends Model
{
    public $activation_code;
    private  $_order;

    public function rules()
    {
        return [
            [['activation_code'], 'string'],
            [['activation_code'], 'required'],
            [['activation_code'], 'checkExist']
        ];
    }

    public function checkExist()
    {
        // check if activation code can use
        $result = $this->getOrder();
        if ($result == null) {
            $this->addError('activation_code', 'Mã kích hoạt không hợp lệ.');
        }
    }

    public function complete()
    {
        $ret = false;
        if (empty($this->_order)) {
            $this->_order = $this->getOrder();
        }
        if ($this->_order != null) {
            // active courses
            if (Order::activate($this->_order->id) !== false) {
                $ret = true;
            }
        }
        return $ret;
    }

    public function getOrder()
    {
        if (empty($this->_order)) {
            $this->_order = \kyna\order\models\Order::find()->where(['activation_code' => $this->activation_code, 'user_id' => \Yii::$app->user->id])->one();
        }
        return $this->_order;
    }

}