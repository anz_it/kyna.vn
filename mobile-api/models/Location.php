<?php
/**
 * Created by PhpStorm.
 * User: truongn
 * Date: 11/20/17
 * Time: 10:35 AM
 */

namespace mobile\models;


class Location extends \kyna\base\models\Location
{
    public function fields()
    {
        $fields = ['id','name','parent_id','position'];
        return $fields;
    }
}