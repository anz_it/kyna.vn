<?php

namespace mobile\models;

class FillInQuestionProcessor extends QuestionProcessor
{
    
    public function calculateScore($answer)
    {
        if($this->question->parent_id == null || $this->question->parent_id == 0)
            return 0;
        $correctAnswers =  $this->question->answers;
        foreach ($correctAnswers as $correctAnswer) {
            if (trim($correctAnswer->content) == trim($answer))
                return 1;
        }
        return 0;
    }


}