<?php
/**
 * Created by PhpStorm.
 * User: truongnguyen
 * Date: 11/15/17
 * Time: 11:43 AM
 */

namespace mobile\models;

use kyna\order\models\search\OrderSearch;

class Order extends \kyna\order\models\Order
{
    public function fields()
    {
        $fields = ['id',  'order_date_text',  'total_text', 'status', 'statusLabel', 'details'];
        return $fields;
    }



    public function getDetails()
    {
        return OrderDetail::find()
            ->where(['order_id' => $this->id])->all();
    }

    public function getOrder_date_text()
    {
        return \Yii::$app->formatter->asDatetime($this->created_time);
    }

    public function getTotal_text()
    {
        return \Yii::$app->formatter->asCurrency($this->total);
    }
}