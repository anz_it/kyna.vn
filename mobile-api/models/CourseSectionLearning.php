<?php
/**
 * Created by PhpStorm.
 * User: truongnguyen
 * Date: 11/16/17
 * Time: 3:31 PM
 */

namespace mobile\models;


class CourseSectionLearning extends CourseSection
{


    public $children_sections = [];

    public function init()
    {
        $this->on(self::EVENT_AFTER_FIND, [$this, 'fillProperty']);
    }

    public function fields()
    {
        $fields = ['name', 'id', 'children_sections'];
        return $fields;
    }

    public function fillProperty()
    {

        $children_sections = [];
        if (\Yii::$app->user->isGuest) {
            $children_sections = CourseSection::find()
                ->where(['root' => $this->root, 'active' => 1, 'lvl' => 1, 'visible' => 1])->orderBy('root, lft')->all();
        } else {

            $course = Course::find()->andWhere(['id'=>$this->course_id])->one();
            if(!empty($course)) {
                $userId = \Yii::$app->user->id;
                $this->finishedLessons = $course->getFinishedLessons($userId);
            }

            $children_sections = CourseSectionDetail::find()
                ->where(['root' => $this->root, 'active' => 1, 'lvl' => 1, 'visible' => 1])->orderBy('root, lft')->all();
            foreach ($children_sections as $children_section){
                    $children_section->finishedLessons = $this->finishedLessons;
            }
        }
        $this->children_sections = $children_sections;

    }



}