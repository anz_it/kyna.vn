<?php
namespace mobile\models;

use kyna\order\models\OrderShipping;
use Yii;
use kyna\otp\lib\Sms;
use kyna\otp\models\Otp;
use kyna\order\models\Order;

class OtpForm extends \yii\base\Model
{

    const OTP_TIME_INTERVAL = 30;
    const OTP_TIME_LIMIT = 300;
    const OTP_TIME_PER_DAY = 3;
    const SCENARIO_ACTIVE = 'active';
    const SCENARIO_GET_OTP = 'get_otp';

    protected $_order;
    public $activation_code;
    public $verify_code;
    public $otp_code;


    public function rules()
    {
        return [
            [['activation_code', 'otp_code'], 'string'],
            [['activation_code'], 'checkExist'],
            [['activation_code'], 'required', 'message' => 'Vui lòng nhập mã thẻ kích hoạt'],
            [['otp_code'], 'required', 'message' => 'Vui lòng nhập mã OTP', 'on' => [self::SCENARIO_ACTIVE]],
            [['otp_code'], 'validateOtp', 'on' => [self::SCENARIO_ACTIVE]],
            [['verify_code'], 'safe'],

        ];
    }

    public function attributeLabels()
    {
        return [
            'activation_code' => 'Mã kích hoạt'
        ];
    }

    public function checkExist()
    {
        // check if activation code can use
        $result = $this->getOrder();
        if ($result == null) {
            $this->addError('activation_code', 'Mã thẻ kích hoạt không chính xác');
        }
    }

    public function complete()
    {
        $ret = false;
        if (empty($this->_order)) {
            $this->_order = $this->getOrder();
        }
        if ($this->_order != null) {
            // active courses
            if (Order::activate($this->_order->id) !== false) {
                $ret = true;
            }
        }
        return $ret;
    }

    public function getOrder()
    {
        if (empty($this->_order)) {
            $this->_order = Order::find()->where([
                'activation_code' => $this->activation_code,
                'is_activated' => Order::STATUS_DEACTIVE,
                'status' => [
                    Order::ORDER_STATUS_COMPLETE,
                    Order::ORDER_STATUS_DELIVERING,
                ]
            ])->one();
        }
        return $this->_order;
    }

    public function validateOtp()
    {
        $otpTimeLimit = self::OTP_TIME_LIMIT;
        $otp = Otp::find()->where([
                'activation_code' => $this->activation_code,
                'otp_code' => $this->otp_code,
                'is_used' => Otp::BOOL_NO,
                'status' => Otp::STATUS_ACTIVE
            ])
            ->andWhere("created_time >= (UNIX_TIMESTAMP() - {$otpTimeLimit})")
            ->exists();
        if (!$otp) {
            $this->addError('otp_code', 'Mã OTP không chính xác');
        }
    }

    public function validateOtpTime()
    {
        $otpInterval = self::OTP_TIME_INTERVAL;
        $checkRuleInterval = Otp::find()->where(['activation_code' => $this->activation_code])
            ->andWhere("created_time > (UNIX_TIMESTAMP() - {$otpInterval})")
            ->orderBy('id DESC')
            ->exists();
        if ($checkRuleInterval) {
            return false;
        }
        return true;
    }

    public function validateOtpTimePerDay()
    {
        $timePerDay = Otp::find()->where(['activation_code' => $this->activation_code])
            ->andWhere("created_time > (UNIX_TIMESTAMP() - 86400)")
            ->orderBy('id DESC')
            ->count();
        if ($timePerDay >= self::OTP_TIME_PER_DAY) {
            return false;
        }
        return true;
    }

    public function sendOtp()
    {

        // check if activation code can use
        if (empty($this->activation_code)) {
            $this->addError('activation_code', 'Vui lòng nhập mã thẻ kích hoạt');
            return false;
        }
        $order = $this->getOrder();
        if (empty($order)) {
            $this->addError('activation_code', 'Mã thẻ kích hoạt không chính xác');
        } else {
            // validate exist user shipping
            $shippingAddress = $order->shippingAddress;
            if (empty($shippingAddress)) {
                $this->addError('otp_code', 'Vui lòng kiểm tra số điện thoại người nhận');
                return false;
            }
            // show captcha or not
            $timePerDay = Otp::find()->where(['activation_code' => $this->activation_code])
                ->andWhere("created_time > (UNIX_TIMESTAMP() - 86400)")
                ->orderBy('id DESC')
                ->count();

            if ($timePerDay > 0) {
                if (!$this->validate()) {
                    return false;
                }
            } else {
                Yii::$app->session->remove('otp_captcha');
            }
            // check otp rule
            if (!$this->validateOtpTimePerDay()) {
                $this->addError('otp_code', 'Đã quá số lần quy định, vui lòng thử lại sau 24h');
                return false;
            }
            if (!$this->validateOtpTime()) {
                $this->addError('otp_code', 'Vui lòng lấy mã xác thực sau 30s');
                return false;
            }
            // disable old otp code
            Otp::updateAll(['status' => Otp::STATUS_DEACTIVE], ['activation_code' => $this->activation_code]);
            // create new otp code
            $otp = new Otp();
            $otp->activation_code = $this->activation_code;
            $otp->phone = $shippingAddress->phone_number;
            $otp->status = Otp::STATUS_DEACTIVE;

            if ($otp->save()) {
                // send via sms
                $smsContent = "{$otp->otp_code} la ma xac thuc (OTP) kich hoat khoa hoc tren Kyna.vn, thoi han 5 phut. Vui long khong cung cap cho bat ky ai.";
                $smsLib = new Sms();
                if ($smsLib->send($otp->phone, $smsContent, $otp->id) || true) {
                    $otp->status = Otp::STATUS_ACTIVE;
                    $otp->save(false);
                    Yii::$app->session->setFlash('otp_msg', "Mã OTP đã được gửi qua số điện thoại {$otp->phone}");
                    return true;
                }
            }
            $this->addError('otp_code', 'Không thể gửi mã OTP');
        }
        return false;
    }

    public function validateInfo()
    {
        // check if activation code can use
        if (empty($this->activation_code)) {
            $this->addError('activation_code', 'Vui lòng nhập mã thẻ kích hoạt');
            return false;
        }
        $order = $this->getOrder();
        if (empty($order)) {
            $this->addError('activation_code', 'Mã thẻ kích hoạt không chính xác');
            return false;
        }
        return true;
    }

    public function checkPhoneLimit()
    {
        $order = $this->getOrder();
        $shippingTblName = OrderShipping::tableName();
        $orderQuantity = Order::find()
            ->alias('o')
            ->select(['phone' => "REPLACE(TRIM({$shippingTblName}.phone_number), ' ', '')"])
            ->innerJoin($shippingTblName, "{$shippingTblName}.order_id = o.id")
            ->where([
                'o.status' => [Order::ORDER_STATUS_COMPLETE, Order::ORDER_STATUS_DELIVERING],
                'o.user_id' => $order->user_id
            ])->groupBy('phone')->count();
        if ($orderQuantity > 2) {
            return true;
        }
        return false;
    }
}