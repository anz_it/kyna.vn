<?php
/**
 * Created by PhpStorm.
 * User: khanhphan
 * Date: 11/2/16
 * Time: 4:09 PM
 */

namespace mobile\models;

use kyna\course\models\QuizAnswer;

class SingleChoiceQuestionProcessor extends QuestionProcessor
{
    
    public function calculateScore($answer)
    {

        $isCorrect = QuizAnswer::find()->andWhere([
            'id' => $answer,
            'is_correct' => QuizAnswer::BOOL_YES
        ])->exists();

        if ($isCorrect) {
            return $this->getQuestion()->calculateScore();
        }
        
        return 0;
    }
}