<?php

namespace mobile\models;

use Yii;

/**
 * This is the model class for table "quiz_session_answers".
 *
 * @property integer $id
 * @property integer $quiz_session_id
 * @property integer $quiz_detail_id
 * @property string $answer
 * @property integer $is_correct
 * @property integer $score
 * @property QuizQuestion $question
 */
class QuizSessionAnswer extends \kyna\course\models\QuizSessionAnswer
{


    public function getQuestion()
    {
        if (empty($this->quiz_question_id)) {
            $quizDetail = QuizDetail::findOne($this->quiz_detail_id);
            if ($quizDetail) {

                $dt = self::findOne($this->id);
                $dt->quiz_question_id = $quizDetail->quiz_question_id;
                $dt->save(false);
            }
        }
        return $this->hasOne(QuizQuestion::className(), ['id' => 'quiz_question_id']);
    }
}
