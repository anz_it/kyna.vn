<?php
/**
 * Created by PhpStorm.
 * User: truongnguyen
 * Date: 11/16/17
 * Time: 5:33 PM
 */

namespace mobile\models;


class QuizQuestionDetail extends QuizQuestion
{
    public function fields()
    {
        $fields = ['type','image_url','sound_url','video_embed','content','answers'];
        return $fields;
    }

    public function getAnswers()
    {
        return $this->hasMany(QuizAnswer::className(), ['question_id' => 'id']);
    }

}