<?php
/**
 * Created by PhpStorm.
 * User: macintosh
 * Date: 9/29/17
 * Time: 3:04 PM
 */

namespace mobile\models;


use kyna\learning\models\UserCourseLesson;
use yii\data\ActiveDataProvider;

class CourseLesson extends \kyna\course\models\CourseLesson
{

    public $is_finished = false;
    /**
     * Return fields in api response
     * @return array
     */
    public function fields()
    {
        return [
            'id',
            'name',
            'type',
            'quiz_id',
            'is_passed',
            'is_finished',


        ];
    }

    public function getIs_passed()
    {
        $user = \Yii::$app->user;
        $course_id = $this->getSection()->one()->course_id;
        $user_course = UserCourse::findOne(['user_id'=>$user->id, 'course_id'=>$course_id]);
        if(!empty($user_course)) {
            $user_course_id = $user_course->id;
        }
        else
            return false;

        if(!empty($user_course_id)) {
            $user_course_lesson = UserCourseLesson::findOne(['user_course_id' => $user_course_id, 'course_lesson_id' => $this->id]);
            if(!empty($user_course_lesson)) {
                $is_passed = ['is_passed'];
            }
            else
                return false;
        }
        return $is_passed == null ? false : $is_passed;



    }

}