<?php
/**
 * Created by PhpStorm.
 * User: macintosh
 * Date: 9/27/17
 * Time: 1:59 PM
 */

namespace mobile\models;

use yii\data\ActiveDataProvider;
use kyna\course\models\Course;

class Banner extends \kyna\settings\models\Banner
{

    public $course_type;
    public $course_title;
    public $app_action_key;

    public function init()
    {
        $this->on(self::EVENT_AFTER_FIND, [$this, 'fillProperty']);
    }

    public function search($params)
    {
        $query = self::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $query->andFilterWhere([
            'category_id' => isset($params['category_id']) ? $params['category_id'] : null,
            'type' => isset($params['type']) ? $params['type'] : [
                Banner::TYPE_APP_HOME_SLIDER
            ],
            'status' => Banner::STATUS_ACTIVE
        ]);

        $date = date('Y-m-d');
        $query->andWhere("from_date is null or (from_date is not null and from_date <= '{$date}')");
        $query->andWhere("to_date is null or (to_date is not null and to_date >= '{$date}')");

        return $dataProvider;
    }

    public function fields()
    {
        return [
            'id',
            'title',
            'image_url',
            'app_action',
            'app_action_key',
            'app_action_data',
            'course_type',
            'course_title'
        ];
    }

    public function fillProperty()
    {
        //Vao chi tiet khoa hoc
        if ($this->app_action == 1) {
            if (!empty($this->app_action_data)) {
                $course_id = (int)$this->app_action_data;
                $course = Course::findOne($course_id);
                if (!empty($course)) {
                    $this->course_title = $course->name;
                    $this->course_type = $course->type;
                }
            }
        }
        $actionKeys = ['1' => 'detail', '2' => 'search', '3' => 'web_view', '4' => 'tag'];
        $this->app_action_key = $actionKeys[$this->app_action];

    }
}