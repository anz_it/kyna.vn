<?php
/**
 * Created by PhpStorm.
 * User: Tan
 * Date: 8/30/2018
 * Time: 2:31 PM
 */

namespace mobile\models;


use common\campaign\CampaignTet;
use yii\base\Model;
use Yii;

class CartForm extends Model
{
    public $course_ids;

    public function rules()
    {
        return [
            ['course_ids', 'required']
        ];
    }

    public function calCartPrice()
    {
        if (!$this->validate()) {
            return $this;
        }
        $ret = [];
        $subTotal = 0;
        $totalDiscount = 0;
        $promotionDiscount = 0;

        if (CampaignTet::InTimesCampaign()) {
            $percentLiXiTet = self::percentLiXiTet($this->course_ids);
            if ($percentLiXiTet > 0) {
                $promotionName = "Giảm giá Lì Xì Hot(-" . $percentLiXiTet . "%)";
                $promotionDiscount = self::getTotalPriceInCampaignTet($this->course_ids) * $percentLiXiTet / 100;
                $ret['promotion_name'] = $promotionName;
                $ret['promotion_discount'] = $promotionDiscount;
            }
        }

        foreach ($this->course_ids as $course_id) {
            $course = Course::find()->andWhere(['id' => $course_id])->one();
            if (!empty($course)) {
                $subTotal += $course->oldPrice;
                $totalDiscount += $course->discountAmount;
            }
        }

        $total = $subTotal - $totalDiscount - $promotionDiscount;


        $ret['sub_total'] = $subTotal;
        $ret['total_discount'] = $totalDiscount;
        $ret['total'] = $total > 0 ? $total : 0;
        $ret['cart_promotion_image_url'] = null; 

        return $ret;
    }

    private static function getTotalPriceInCampaignTet($courseIds)
    {
        $totalPrice = 0;
        foreach ($courseIds as $courseId) {
            $course = Course::find()->andWhere(['id' => $courseId])->one();
            if (!empty($course) && !in_array($courseId,\common\campaign\CampaignTet::COURSE_NOT_APPLY_VOUCHER)) {
                if ($course->type != Course::TYPE_COMBO) {
                    $totalPrice += $course->oldPrice - $course->discountAmount;
                }
            }
        }

        return $totalPrice;
    }

    private static function percentLiXiTet($courseIds)
    {

        $priceOrder = self::getTotalPriceInCampaignTet($courseIds);
        $percent = 0;
        if($priceOrder == 0){
            return 0;
        }
        if ($priceOrder >= 2000000) {
            return $percent = 20;
        }

        if ($priceOrder >= 1000000) {
            return $percent = 15;
        }

        if ($priceOrder >= 500000) {
            return $percent = 10;
        }

        if ($priceOrder >= 300000) {
            return $percent = 5;
        }

        return $percent;
    }
}