<?php
/**
 * Created by PhpStorm.
 * User: truongn
 * Date: 1/11/17
 * Time: 11:00AM
 */

namespace mobile\models;


use common\helpers\CDNHelper;
use kyna\course\models\Teacher;
use yii\data\ActiveDataProvider;
use mobile\models\User;

class CourseElastic extends \common\elastic\Course
{
    public $is_bought;
    private static $select_properties = ['type'=>'', 'id' => '', 'name' => '', 'teacher_name' => '','old_price'=>'', 'sell_price' => '', 'discount_percent' => '',
        'is_hot'=>'','is_new'=>'','is_feature'=>'',
        'discount_amount' => '', 'total_users_count' => '', 'image_url' => '', 'rating' => '','combo_item_count'=>'','teacher_count'=>'',
        'type'=>'','total_time_text'=>''];


    public function getTeacher_Count()
    {
        $teachers = [];
        foreach ($this->items as $item) {
            if (!key_exists($item->course->teacher_id, $teachers)) {
                $teachers[$item->course->teacher_id] = $item->course->teacher_id;
            }
        }

        return count($teachers);
    }

    public static function getFeatureCourse($offset = 0, $limit = 8)
    {
        $course = parent::getFeatureCourse($limit);
        $result = [];
        foreach ($course as $element) {
            $tmp = array_intersect_key($element, self::    $select_properties);
           /* $tmp['image_url'] = CDNHelper::image($tmp['image_url'],[
                'size' => CDNHelper::IMG_SIZE_THUMBNAIL,
                'resizeMode' => 'cover',
                'returnMode' => CDNHelper::IMG_RETURN_MODE_URL
            ]);*/
            $tmp['type'] = (int)$tmp['type'];
            array_push($result, $tmp);
        }
        $result = self::addUserCourseField($result);
        $result = self::addTypeKey($result);
        return $result;

    }

    public static function getListCourse($search = [], $offset = 0, $limit = 10)
    {

        $course = parent::getListCourse($search, $offset, $limit);
        $result = [];
        foreach ($course['data'] as $element) {
            $tmp = array_intersect_key($element, self::    $select_properties);
            /*$tmp['image_url'] = CDNHelper::image($tmp['image_url'],[
                'size' => CDNHelper::IMG_SIZE_THUMBNAIL,
                'resizeMode' => 'cover',
                'returnMode' => CDNHelper::IMG_RETURN_MODE_URL
            ]);*/
            array_push($result, $tmp);
        }
        $result = self::addUserCourseField($result);
        $result = self::addTypeKey($result);
        $filter = [];
        foreach ( $course['agg_search_data'] as $item) {
            $filter_item = new SearchFilterItem();
            $filter_item->key = $item['key'];
            $filter_item->doc_count = $item['doc_count'];
            $filter_item->items = [];
            foreach ($item['facet_value']['buckets'] as $facet_item) {
                $facet = new FacetItem();
                $facet->key= $filter_item->key;
                $facet->value = $facet_item['key'];
                $facet->count = $facet_item['doc_count'];
                array_push($filter_item->items,$facet);
            }
            array_push($filter,$filter_item);
        }
        
        if(isset($search['catId'])) {
            $sub_categories = Category::find()
                ->where(['status' => Category::STATUS_ACTIVE])
                ->andWhere(['is_deleted' => false])
                ->orderBy('order')
                ->andWhere(['parent_id' => $search['catId']])->all();
            return ['total' => $course['total'], 'courses' => $result, 'sub_categories' => $sub_categories,'filter'=>$filter];
        } else {
            $sub_categories = Category::find()
                ->where(['status' => Category::STATUS_ACTIVE])
                ->andWhere(['is_deleted' => false])
                ->andWhere(['parent_id' => 0])
                ->orderBy('order')->all();
            return ['total' => $course['total'], 'courses' => $result, 'sub_categories' => $sub_categories, 'filter'=>$filter];
        }

    }

    private static function addUserCourseField($search_result)
    {
        if(!\Yii::$app->user->isGuest)
        {
            $user_course = UserCourse::find()
                ->where(['user_id' => \Yii::$app->user->getId()])->select('course_id')->asArray()->all();

            for($i = 0 ; $i < count($search_result) ; $i++)
            {
                $course_item = $search_result[$i];
                if(self::checkCourseInUserCourse($course_item['id'],$user_course))
                {
                    $search_result[$i]['is_bought'] = true;
                } else {
                    $search_result[$i]['is_bought'] = false;
                }
                $search_result [$i]['is_free'] = $course_item['sell_price'] <= 0;
            }


        }

        return $search_result;
    }

    private static function checkCourseInUserCourse($course_id, $user_courses)
    {
        foreach ($user_courses as $usr_course){
            if($usr_course['course_id'] == $course_id) return true;
        }
        return false;

    }
    private static function getMobileTypeKey($type)
    {
        $typeKeys = ['1'=>'video', '2'=>'combo','3'=>'app'];
        if(!empty($typeKeys[$type]))
            return $typeKeys[$type];
        return null;
    }
    private static function addTypeKey($search_result)
    {
        for($i = 0 ; $i < count($search_result) ; $i++){
            $search_result [$i]['type_key'] = self::getMobileTypeKey($search_result[$i]['type']);
        }
        return $search_result;
    }



}