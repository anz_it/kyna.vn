<?php

namespace mobile\models;
use Yii;
/**
 * This is the model class for table "push_notification".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $token
 * @property string $os_type
 * @property integer $screen_type
 * @property string $data
 * @property string $device_model
 * @property integer $created_time
 * @property integer $updated_time

 */
class PushNotification extends \kyna\base\ActiveRecord
{
    const TYPE_COURSE_DETAIL = 1;
    const TYPE_CATEGORY = 2;
    const TYPE_TAG = 3;
    const TYPE_SEARCH = 4;
    const TYPE_WEB_VIEW = 5;


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'push_notification';
    }



    public static function getScreenTypes()
    {
        return
            [
                 TYPE_COURSE_DETAIL => 'Course Detail',
                 TYPE_CATEGORY => 'Category',
                 TYPE_TAG => 'Tag',
                 TYPE_SEARCH => 'Search',
                 TYPE_WEB_VIEW => 'Web view',
             ];
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'created_time', 'updated_time'], 'integer'],
            [['token', 'os_type','device_model'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'token' => 'Token',
            'os_type' => 'Os Type',
            'device_model' => 'Device Type',
            'created_time' => 'Ngày tạo',
            'updated_time' => 'Cập nhật cuối',
        ];
    }
}