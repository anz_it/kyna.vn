<?php

namespace mobile\models;


class CourseSubSectionDetail extends CourseSection
{

    public function fields()
    {
        $fields = ['name', 'id', 'lessons'];
        return $fields;
    }

    public function getLessons()
    {
        return CourseLesson::find()
            ->where(['section_id' => $this->id, 'status' => CourseLesson::BOOL_YES, 'is_deleted' => CourseLesson::BOOL_NO])->orderBy(['order' => SORT_ASC])->all();
    }


}