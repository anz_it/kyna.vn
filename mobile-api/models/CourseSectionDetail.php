<?php
/**
 * Created by PhpStorm.
 * User: truongn
 * Date: 11/24/17
 * Time: 12:16 PM
 */

namespace mobile\models;


class CourseSectionDetail extends CourseSection
{

    public $sub_children_sections = [];
    public function init()
    {
        $this->on(self::EVENT_AFTER_FIND, [$this, 'fillProperty']);
    }

    public function fields()
    {
        $fields = ['name', 'id', 'lessons', 'sub_children_sections'];
        return $fields;
    }

    public function fillProperty()
    {
        if (\Yii::$app->user->isGuest) {
            $children_sections = CourseSection::find()
                ->where(['root' => $this->root, 'active' => 1, 'lvl' => 2, 'visible' => 1])->orderBy('root, lft')->all();
        } else {

            $children_sections = CourseSubSectionDetail::find()
                ->where(['root' => $this->root, 'active' => 1, 'lvl' => 2, 'visible' => 1])->orderBy('root, lft')->all();
        }
        $this->sub_children_sections = $children_sections;
    }

    public function getLessons()
    {

        $lessons = CourseLesson::find()
            ->where(['section_id' => $this->id, 'status' => CourseLesson::BOOL_YES, 'is_deleted' => CourseLesson::BOOL_NO])->orderBy(['order' => SORT_ASC])->all();

        foreach ($lessons as $lesson){
            if (!empty($this->finishedLessons) && in_array($lesson->id, $this->finishedLessons)) {
                $lesson->is_finished = true;
            } else {
                $lesson->is_finished = false;
            }
        }
        return $lessons;

    }


}