<?php
/**
 * Created by PhpStorm.
 * User: khanhphan
 * Date: 9/27/17
 * Time: 1:12 AM
 */

namespace mobile\components;

trait LoginRequireTrait
{
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => HttpBearerAuth::className(),
        ];
        return $behaviors;
    }

}