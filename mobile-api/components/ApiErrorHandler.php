<?php


namespace mobile\components;


use common\components\GoogleStackDriverLog;
use yii\base\UserException;
use yii\helpers\VarDumper;
use yii\web\ErrorHandler;
use Yii;

class ApiErrorHandler  extends ErrorHandler
{
    public function handleException($exception)
    {
        if ($exception instanceof ExitException) {
            return;
        }

        $this->exception = $exception;

        // disable error capturing to avoid recursive errors while handling exceptions
        $this->unregister();

        // set preventive HTTP status code to 500 in case error handling somehow fails and headers are sent
        // HTTP exceptions will override this value in renderException()
        if (PHP_SAPI !== 'cli') {
            http_response_code(500);
        }

        try {
            $statusCode = $exception->statusCode;
            $exceptCodes = [404, 401];
            $message = $exception->getMessage();
            $writeLog = true;
            if($statusCode == 400 && $message == "Bad signature"){
                $writeLog = false;
            }
            if(in_array($statusCode, $exceptCodes)){
                $writeLog = false;
            }
           if($writeLog) {
               if (!YII_DEBUG) {
                   $postData = Yii::$app->request->getBodyParams();
                   $postData = json_encode($postData);
                   $log = "Api Post Data: ". $postData."\n".$exception->getTraceAsString();
                   Yii::$app->logger->log($exception->getMessage(), GoogleStackDriverLog::LEVEL_CRITICAL, $log);
               }
               $this->logException($exception);
           }
            if ($this->discardExistingOutput) {
                $this->clearOutput();
            }
            $this->renderException($exception);

            if (!YII_ENV_TEST) {
                \Yii::getLogger()->flush(true);
                if (defined('HHVM_VERSION')) {
                    flush();
                }
                exit(1);
            }
        } catch (\Exception $e) {
            // an other exception could be thrown while displaying the exception
            $msg = "An Error occurred while handling another error:\n";
            $msg .= (string) $e;
            $msg .= "\nPrevious exception:\n";
            $msg .= (string) $exception;
            if (YII_DEBUG) {
                if (PHP_SAPI === 'cli') {
                    echo $msg . "\n";
                } else {
                    echo '<pre>' . htmlspecialchars($msg, ENT_QUOTES, Yii::$app->charset) . '</pre>';
                }
            } else {
                echo 'An internal server error occurred.';
            }
            $msg .= "\n\$_SERVER = " . VarDumper::export($_SERVER);
            error_log($msg);
            if (defined('HHVM_VERSION')) {
                flush();
            }
            exit(1);
        }

        $this->exception = null;
    }


}