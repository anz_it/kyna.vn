<?php
/**
 * Created by PhpStorm.
 * User: truongn
 * Date: 11/20/17
 * Time: 2:30 PM
 */

namespace mobile\components;


use dektrium\user\models\Token;
use dektrium\user\models\User;
use Yii;

class Mailer extends \dektrium\user\Mailer
{

    /**
     * @desc override dektrium Mailer sendWelcomeMessage function
     * @param User $user
     * @param Token $token
     * @param type $showPassword
     * @return type
     */
    public function sendWelcomeMessage(User $user, Token $token = null, $showPassword = false)
    {
        return $this->sendMessage(
            $user->email,
            'Chúc mừng bạn đã đăng ký tài khoản thành công tại Kyna.vn',
            '@common/mail/user/welcome',
            [
                'fullName' => !empty($user->profile->name) ? $user->profile->name : $user->email,
                'email' => $user->email,
                'settings' => Yii::$app->settings
            ]
        );
    }

    public function sendRecoveryMessage(User $user, Token $token)
    {
        //Replace link url hostname for kyna.vn
        $link = $token->url;
        $host_name = Yii::$app->request->hostInfo;
        $link = str_replace($host_name, "https://kyna.vn", $link);
        return $this->sendMessage(
            $user->email,
            $this->getRecoverySubject(),
            '@common/mail/user/recover_password',
            [
                'fullName' => !empty($user->profile->name) ? $user->profile->name : $user->email,
                'link' => $link,
                'settings' => Yii::$app->settings
            ]
        );
    }

    protected function sendMessage($to, $subject, $view, $params = [])
    {
        $mailer = Yii::$app->mailer;
        $mailer->htmlLayout = false;

        if ($this->sender === null) {
            $this->sender = isset(Yii::$app->params['adminEmail']) ?
                Yii::$app->params['adminEmail']
                : 'no-reply@example.com';
        }

        return $mailer->compose($view, $params)
            ->setTo($to)
            ->setFrom($this->sender)
            ->setSubject($subject)
            ->send();
    }
}