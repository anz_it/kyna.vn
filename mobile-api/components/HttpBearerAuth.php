<?php
/**
 * Created by PhpStorm.
 * User: khanhphan
 * Date: 12/19/17
 * Time: 11:49 AM
 */

namespace mobile\components;


class HttpBearerAuth extends \yii\filters\auth\HttpBearerAuth
{

    public function isOptional($action)
    {
        if ($this->optional == '*' || in_array('*', $this->optional))
            return true;
        return parent::isOptional($action);
    }
}