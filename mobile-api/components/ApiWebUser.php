<?php
/**
 * Created by PhpStorm.
 * User: khanhphan
 * Date: 9/27/17
 * Time: 2:12 AM
 */

namespace mobile\components;


use mobile\helpers\SecurityHelper;
use yii\web\BadRequestHttpException;
use yii\web\User;
use Yii;
class ApiWebUser extends User
{
    public function init()
    {
        $ret = parent::init();

        $this->on(self::EVENT_AFTER_LOGIN, [$this, 'onAfterLogin']);
        $this->on(self::EVENT_AFTER_LOGOUT, [$this, 'onAfterLogout']);

        return $ret;
    }
    public function loginByAccessToken($token, $type = null)
    {
        $signature = \Yii::$app->request->getHeaders()->get('X-Signature-Token');
        if (!SecurityHelper::checkSignature($signature, $token))
            throw new BadRequestHttpException("Bad signature");
        return parent::loginByAccessToken($token, $type);
    }
    public function onAfterLogin($event)
    {
        $user = $event->identity;

        $oldLoginToken = $user->loginToken;
        if (!is_null($oldLoginToken)) {
            Yii::$app->session->destroySession($oldLoginToken->code);
        }
        //web login with session
        if(!empty(Yii::$app->session->id)) {
            $user->createLoginToken();
            if (!empty($user->loginToken) && $user->loginToken->code != Yii::$app->session->id) {
                $user->loginToken->code = Yii::$app->session->id;
                $user->loginToken->save();
            }
        }
    }
    public function onAfterLogout($event)
    {
        $user = $event->identity;
        $oldLoginToken = $user->loginToken;
        if (!is_null($oldLoginToken)) {
            $oldLoginToken->delete();
        }

    }
}