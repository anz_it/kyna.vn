

## Category



* Url: 

  ```v1/categories``` => lấy danh sách categories cha

  ```v1/categories/16``` => lấy thông tin chi tiết của category 16

   ```v1/categories/16/children``` => lấy các cat con trực tiếp của category 16

* Method: ```GET```
* Params: 

   ```per-page```: Số lượng categories trên mỗi trang

   ```page``` : Lấy trang nào

* Response body
```JSON
{
    "success": true,
    "data": [
        {
            "id": 16,
            "parent_id": 0,
            "name": "Kỹ năng cho công việc",
            "slug": "ky-nang-cho-cong-viec",
            "level": 0,
            "description": "Khóa học Nỹ Năng Cho Công Việc...",
            "order": 3,
            "type": 0,
            "status": 1,
            "is_deleted": false,
            "created_time": 1466482586,
            "updated_time": 1486436001,
            "v2_id": 16,
            "title": null,
            "redirect_url": null,
            "seo_description": null,
            "has_child": 4
        }
    ]
}
```

* Response headers
   ```
   X-Pagination-Current-Page:1
   X-Pagination-Page-Count:2
   X-Pagination-Per-Page:20
   X-Pagination-Total-Count:37
   ```


* Ghi chú: Các thông tin phân trang nằm trong response header như mô tả ở trên.



