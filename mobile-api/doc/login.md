

## Login



* Url: ```v1/login```
* Method: ```POST```
* Data:
```JSON
{
  "email": "email@example.com",
  "password": "somepass"
}
```

* Response success
```JSON
{
    "success": true,
    "data": {
        "success": true,
        "access_token": "NvE-y1DhCND-fa-63vVBrrLFKKNyvuzz",
        "email": "thanhkhanhcntt@gmail.com",
        "name": "Phan Thanh Khánh",
        "signature": "064bd4a4c918f49de627371fa6136c58af631193da7e31ad0047b1d8fdcbce4e"
    }
}
```

* Response fail
```JSON
{
    "success": true,
    "data": {
        "success": false,
        "errors": {
            "password": [
                "Invalid login or password"
            ]
        }
    }
}
```

* Ghi chú: Sau khi user login, mỗi request gửi lên server sẽ đính kèm trong header các field như sau:

```
Authorization: <token>
X-Signature-Token:  <signature>
```


