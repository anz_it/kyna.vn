<?php

use yii\web\View;
use common\helpers\CDNHelper;

$cdnUrl = CDNHelper::getMediaLink();
$this->registerCssFile($cdnUrl . '/css/feedback.css');
?>

<div id="k-wrap-feedback">
	<div id="k-feedback">
		<div class="box-k-feedback">
			<h2>Góp ý cho Kyna.vn<span class="icon-out-feedback">-</span></h2>
			<form action="" data-type="multiple" id="landing-page-id" name="landing-page-id" method="post" accept-charset="utf-8">
				<div class="form-group input input-phone">
					<input type="number" class="form-control" name="phone" id="feedback-phone" placeholder="Số điện thoại*" required="">
					<span>Số điện thoại*</span>
				</div>
				<div class="form-group input input-email">
					<input type="email" class=" form-control" name="email" id="feedback-email" placeholder="Email*" required="">
					<span>Email*</span>
				</div>
				<h3>Bạn có sẵn sàng giới thiệu Kyna.vn với người thân và bạn bè?</h3>
				<ul>
					<li>
						<input type="radio" id="feedback-ss" name="feedback-radio" value="Sẵn sàng" data-value="Sẵn sàng" checked>
						<label for="feedback-ss">Sẵn sàng</label>
						<div class="check"></div>
					</li>

					<li>
						<input type="radio" id="feedback-ct" name="feedback-radio" value="Có thể" data-value="Có thể">
						<label for="feedback-ct">Có thể</label>
						<div class="check">
							<div class="inside"></div>
						</div>
					</li>

					<li>
						<input type="radio" id="feedback-k" name="feedback-radio" value="Không" data-value="Không">
						<label for="feedback-k">Không</label>
						<div class="check">
							<div class="inside"></div>
						</div>
					</li>
				</ul>

				<h3>Bạn có góp ý gì về phiên bản mới của Kyna.vn?</h3>
				<textarea cols="50" id="feedback-textarea" placeholder="Bạn có thể góp ý về cả giao diện, nội dung các lĩnh vực bạn muốn có thêm trên Kyna.vn"></textarea>
				<p class="error"></p>
				<button type="submit" class="btn-box-register">GỬI GÓP Ý</button>
			</form>
		</div>
	</div>
	<!--end #k-feedback-->

	<div id="k-feedback-button">
		<img src="<?= $cdnUrl ?>/img/pen.png" alt="">
		<div>GÓP Ý</div>
	</div>
	<div id="k-feedback-result">
		<h2>Góp ý cho Kyna.vn<span class="icon-out-feedback">-</span></h2>
		<div class="box-result">
				<div class="text">Cảm ơn bạn đã đóng góp ý kiến cho Kyna. Chúc bạn một ngày học tập và làm việc nhiều năng lượng.</div>
				<button type="submit" class="btn-box-register btn-result">ĐÓNG</button>
		</div>
	</div>
</div>
<!--end #k-wrap-feedback -->
<?php $this->registerJsFile($cdnUrl . '/js/feedback.js', ['position' => View::POS_END]) ?>