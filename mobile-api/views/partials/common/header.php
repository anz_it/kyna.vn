<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ActiveForm;
use app\widgets\CategoryNav;
use app\modules\cart\widgets\MiniCart;

?>

<header>
    <!-- -->
    <nav class="navbar navbar-light">
        <div class="container">
            <button class="navbar-toggler hidden-md-up pull-xs-left" type="button"  data-offpage="#nav-categories" aria-controls="exCollapsingNavbar" aria-expanded="false" aria-label="Toggle navigation">
                <i class="icon icon-bars"></i>
            </button>
            <a href="/p/campaign/dong-gia/399564" class="navbar-brand logo">
                <img src="<?= Url::toRoute("/"); ?>/img/logo_1_tang_1.svg" alt="Kyna.vn">
            </a>

            <ul class="nav navbar-nav">
                <li class="nav-item nav-item-categories dropdown">
                    <a class="nav-link hidden-sm-down" href="#" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                        <i class="icon icon-bars"></i>
                        <span class="hidden-lg-down">Danh mục khóa học</span>
                    </a>
                    <div class="dropdown-menu offpage-menu" id="nav-categories">
                        <div class="dropdown-content offpage-content">
                            <header class="hidden-md-up offpage-header">
                                <button class="close offpage-close back" data-offpage="#nav-categories" data-icon="5"></button>
                                <h4><img src="<?= Url::toRoute("/"); ?>/img/logo_1_tang_1.svg" alt="Kyna.vn" height="50"></h4>
                            </header>
                            <?= CategoryNav::widget() ?>
                        </div>
                    </div>
                </li>
            </ul>

            <ul class="nav navbar-nav pull-xs-right">
                <li class="nav-item nav-item-hotline hidden-md-down">
                    <span class="text"><i class="icon-hot-line"></i> Hotline</span>
                    <a class="number" href="tel:<?= Yii::$app->params['hotline'] ?>"><?= Yii::$app->params['hotline'] ?></a>
                </li>

                <li class="nav-item nav-item-mini-cart minicart dropdown">
                    <?= MiniCart::widget() ?>
                </li>

                <?= Yii::$app->user->isGuest ? $this->render('_nav-login') : $this->render('_nav-profile') ?>
            </ul>
            <form action="<?= Url::toRoute(['/course/default/index']) ?>" id="search-form" class="col-md-4 col-lg-3 col-xl-4 col-xs-12" method="get">
                <div class="input-group">
                    <input name="q" type="text" class="form-control" placeholder="Tìm khóa học bạn cần" autocomplete="off">
                    <span class="input-group-btn">
                        <button class="btn btn-secondary search-button hidden-sm-down" type="submit">
                            <i class="icon-search icon"></i>
                        </button>
                        <button class="btn btn-primary search-button hidden-md-up" type="submit">
                            Tìm
                        </button>
                    </span>
                </div>
            </form>

        </div><!--end .container-->
    </nav>
</header>

<?php
/*$script = "
;(function($){
    $('body').on('submit', '#profile-form', function(e) {
         e.preventDefault();

         var url = $(this).attr('action');
         var form = $(e.target);

         $.post(url, form.serialize(), function (res) {
             form.parent().html(res);
         });
    });
    $('body').on('submit', '#active-cod-form', function(e){
        e.preventDefault();

        var url = $(this).attr('action');
        var form = $(e.target);

        $.post(url, form.serialize(), function (res) {
            form.parent().html(res);
        });
    });
})(jQuery);
" ?>

<?php $this->registerJs($script, View::POS_END, 'profile-submit')
*/
?>
