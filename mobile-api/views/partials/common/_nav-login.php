<?php
use yii\helpers\Url;
?>
<li class="nav-item dropdown nav-item-login">
    <a href="<?= Url::toRoute(['/user/security/login']) ?>" class="nav-link nav-button dropdown-toggle hidden-sm-down" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
        Tài khoản
    </a>
    <a href="<?= Url::toRoute(['/user/security/login']) ?>" class="nav-link hidden-md-up" role="button" aria-haspopup="true" aria-expanded="false">
        <span class="icon icon-profile"></span>
    </a>

    <div class="dropdown-menu dropdown-menu-right">
        <h4 class="title">Bắt đầu học trên Kyna bằng các cách sau</h4>
        <?= Yii::$app->facebook->loginButton(null, [
            'class' => 'btn btn-facebook btn-block'
        ]) ?>
        <!-- <a href="#" class="btn btn-google btn-block">
            <i class="fa fa-google" aria-hidden="true"></i> Đăng nhập bằng facebook
        </a> -->
        <a href="<?= Url::toRoute('/user/security/login') ?>" class="btn btn-primary btn-block" data-toggle="modal" data-target="#modal" data-modal-class="modal-narrow" data-remote="false">
            Đăng nhập Kyna
        </a>
        <a href="<?= Url::toRoute(['/user/registration/register'])?>" class="btn btn-secondary btn-block" data-toggle="modal" data-target="#modal" data-modal-class="modal-narrow" data-remote="false">
            Đăng ký Kyna
        </a>
    </div>

</li>
