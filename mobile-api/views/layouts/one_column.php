<?php
    use yii\widgets\Breadcrumbs;
    use common\helpers\GoogleSnippetHelper;
?>
<?php $this->beginContent('@app/views/layouts/main.php'); ?>

<!-- Breadcrumbs start -->
<div class="breadcrumb-container">
    <div class="container">
        <?php
        $homeUrl = yii\helpers\Url::toRoute(['/'],true);
        echo Breadcrumbs::widget([
            'options' => [
                'class' => 'breadcrumb',
                'itemscope' => '',
                'itemtype' => 'http://schema.org/BreadcrumbList',
            ],
            'homeLink' => [
                'label' => '<i class="fa fa-home"></i> Trang chủ',
                'url' => $homeUrl,
                'encode' => false,
                'template' => GoogleSnippetHelper::renderBreadcrumbTemplate($homeUrl, '<i class=\'fa fa-home\'></i> Trang chủ', 1),
            ],
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ])
        ?>
    </div>
</div>
<!-- Breadcrumbs end -->

<section>
    <?= $content; ?>
</section>
<?php $this->endContent(); ?>
