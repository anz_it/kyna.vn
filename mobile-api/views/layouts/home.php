<!DOCTYPE HTML>
<?php

use yii\web\View;

use common\helpers\CDNHelper;
use common\assets\BootstrapNotifyAsset;

use frontend\assets\HomeAsset;
use frontend\widgets\HeaderWidget;
use frontend\widgets\FooterWidget;
use frontend\widgets\PopupWidget;

HomeAsset::register($this);
BootstrapNotifyAsset::register($this);
$cdnUrl = CDNHelper::getMediaLink();
$this->beginPage();
?>
<html  lang="<?= Yii::$app->language ?>">
<head>
    <?= $this->render('common/html_head') ?>
    <script type="text/javascript">
        var mediaBaseUrl = '<?= Yii::$app->params['media_link']?>';
    </script>
</head>

<body>
    <?php $this->beginBody()?>

        <?php echo Yii::$app->settings->bodyScript?>

        <?= HeaderWidget::widget(['rootCats' => $this->context->rootCats]); ?>

        <?= $content; ?>

        <?= FooterWidget::widget(); ?>

        <?php $this->registerJsFile($cdnUrl . '/src/js/add-to-cart.js?v=1515119844', ['position' => View::POS_END]) ?>
        <?php $this->registerJsFile($cdnUrl . '/src/js/course-pop-up.js', ['position' => View::POS_END]) ?>

        <?= PopupWidget::widget([
            'position' => PopupWidget::POSITION_ALL
        ]) ?>
    <?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
