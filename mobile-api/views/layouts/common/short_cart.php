<?php

use yii\helpers\Url;

$cart = Yii::$app->cart;
$formatter = Yii::$app->formatter;
$cartItems = $cart->getPositions();
?>

<ul class="dropdown-menu wrap clearfix">
    <li class="clearfix wrap-form-cart k-add-to-cart-register" id="k-header-form-cart">
        <?php if (!empty($cart->getCount())){ ?>
            <ul class="list">
                <?php foreach($cartItems as $item) { ?>
                <li>
                    <div class="col-xs-12 text">
                        <a href="<?= $item->url; ?>"><?= $item->name; ?>
                            <span class="price">
                                <?= $item->getCostText() ?>
                            </span>
                        </a>

                    </div><!--end .col-xs-8 text-->
                </li>
                <?php } ?>
            </ul>
            <?php if (Yii::$app->cart->isRequiredGift()): ?>
                <div class="byone_giveone">
                    <p>Bạn được tặng 1 khóa hoc trong chương trình <a href="<?= Url::toRoute(['/course/default/index', 'tag' => 'mua1tang1']); ?>" title=""><br />Mua 1 Tặng 1.</a> Đừng quên chọn nhé!</p>
                </div>
            <?php endif; ?>
            <div class="wrap-total">
              <a href="<?= Url::toRoute(['/cart/default/index'])?>" class="btn-view">&#187; <span>Xem giỏ hàng</span></a>
              <h6>Tổng cộng: <strong><?= $formatter->asCurrency($cart->getCost(true)) ?></strong></h6>
            </div><!--end .wrap-total-->
            <div class="button-wrap">
                <?php if(Yii::$app->cart->isRequiredGift()): ?>
                <a href="#popup_checkout_page" data-toggle="modal" class="btn-payment" data-backdrop="static" data-keyboard="false">Thanh toán</a>
                <?php else: ?>
                <a href="<?= Url::toRoute(['/cart/checkout/index'])?>" class="btn-payment">Thanh toán</a>
                <?php endif; ?>
            </div>
            <!-- <ul class="button">
                <li></li>
                <li></li>
            </ul> -->
            <?php } else { ?>
            <p class="empty-cart">Giỏ hàng trống!</p>
        <?php } ?>
    </li>
</ul>
