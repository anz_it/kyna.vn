<?php
use yii\bootstrap\Html;
?>

<meta charset="<?= Yii::$app->charset ?>">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<?= Html::csrfMetaTags() ?> 


<title><?= Html::encode($this->title) ?></title>

<?php $this->head() ?>

<link rel="icon" href="/favo_ico.png">
