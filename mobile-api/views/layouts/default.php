<!DOCTYPE HTML>
<?php

use yii\helpers\Html;
use yii\web\View;
use frontend\widgets\HeaderWidget;
use frontend\widgets\FooterWidget;
use frontend\widgets\GaWidget;

use frontend\assets\HomeAsset;

HomeAsset::register($this);
$this->beginPage();
?>
<html  lang="<?= Yii::$app->language ?>">
<head>
    <?= $this->render('common/html_head') ?>
</head>

<body>
    <?= $this->render("@viewPartials/common/header") ?>
    <main>
        <?php $this->beginBody()?>

            <?= $content; ?>

        <?php $this->endBody() ?>
    </main>
    <?= $this->render("@viewPartials/common/footer") ?>
</body>
</html>
<?php $this->endPage() ?>
