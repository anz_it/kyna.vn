<!DOCTYPE HTML>
<?php

use mobile\widgets\HeaderWidget;
use mobile\widgets\FooterWidget;
use mobile\assets\HomeAsset;
use common\assets\BootstrapNotifyAsset;

HomeAsset::register($this);
BootstrapNotifyAsset::register($this);
$this->beginPage();
?>
<html  lang="<?= Yii::$app->language ?>">
<head>
    <?= $this->render('common/html_head') ?>
    <link rel="stylesheet" href="/css/font-awesome.min.css">
</head>

<body <?php if(!empty($this->context->bodyClass)) { ?> class="<?= $this->context->bodyClass; ?>" <?php } ?> >
    <?php $this->beginBody()?>
    <?php echo Yii::$app->settings->bodyScript ?>
        <?= HeaderWidget::widget(['rootCats' => !empty($this->context->rootCats) ? $this->context->rootCats : null]); ?>

        <?= $content; ?>

        <?= FooterWidget::widget(); ?>
    <?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
