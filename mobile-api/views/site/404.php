<?php

use yii\helpers\Url;
use kyna\tag\models\Tag;
use common\helpers\CDNHelper;

$cdnUrl = CDNHelper::getMediaLink();

$this->title = "Kyna.vn | Không tìm thấy trang";

$settings = Yii::$app->controller->settings;
$hotKeywords = Tag::topTags(Tag::TYPE_DESKTOP, 10);
?>
<main>
 
    <section>
        <div id="k-page-404" class="k-height-header container">
            <div class="k-page-404-wrap">
                <div class="col-md-6 col-xs-12 box img">
                    <h2><img src="<?= $cdnUrl ?>/src/img/404/404.png" class="img-fluid" alt="404"></h2>
                </div>
                <!--end .img-->
                <div class="col-md-6 col-xs-12 box text">
                    <h3>Đừng lo lắng</h3>
                    <p class="top">Trong cuộc sống chúng ta vẫn thường hay đi vào những ngã rẽ không đúng!<br>Đây không phải là lớp học Online bạn đang tìm!</p>
                    <p class="bottom">Trở lại đúng hướng và xem các gợi ý tìm kiếm khóa học khác của chúng tôi ở bên dưới</p>
                    <ul>
                        <?php foreach ($hotKeywords as $tag) : ?>
                            <li><a href="<?= Url::toRoute(['/course/default/index', 'tag' => $tag->slug]) ?>" title="<?= $tag->title ?>"><?= $tag->tag ?></a></li>
                        <?php endforeach; ?>
                    </ul>
                </div>
                <!--end .text-->
            </div>
        </div><!--end #page-404-->
    </section>
    
</main>
