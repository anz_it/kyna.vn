{use class='yii\helpers\Url'}
{set title="Kyna.vn - Học online cùng chuyên gia"}
<section>
    <span class="get-height-header"></span>
    <div id="register-success-account">
        <div class="container">
            <ul>
                <li><span><i class="fa fa-check" aria-hidden="true"></i></span> Chúc mừng bạn đã đăng ký thành công trên Kyna.vn! Bạn có thể bắt đầu chọn khóa học phù hợp và tham gia học.</li>
                <li><a href="#">Xem tất cả khóa học</a></li>
            </ul>
        </div><!--end .container-->
    </div><!--end #register-success-account-->
    <div id="wrap-banner-top">
        <div class="banner-main clearfix">
            <div class="container">
                <div class="wrap-content-banner-top col-md-10 col-md-offset-1 pd0">
                    <h2 class="title">Tìm khóa học bạn đang quan tâm</h2>
                    {ActiveForm assign='searchFormIndex' options=[class=>'clearfix form-search'] id='search-form-index' action="{Url::toRoute(['/course/default/index'])}" method='get'}
                        <input type="text" name='q' class="form-control" class="search-form" placeholder="Nhập từ khóa để tìm khóa học bạn cần">
                        <span class="input-group-btn icon">
                            <button class="btn btn-default" type="submit"><i class="fa fa-search"></i></button>
                        </span>
                    {/ActiveForm}
                    {$hotKeywords = ['marketing', 'làm giàu', 'lập trình IOS', 'dạy con', 'nuôi con']}
                    {foreach from=$hotKeywords item=$keyword}
                    <div class="box">
                        <a href="{Url::toRoute(['/course/default/index', 'q' => $keyword])}">{ucfirst($keyword)}</a>
                    </div><!--end .box-->
                    {/foreach}
                </div><!--end .wrap-content-banner-top -->
            </div><!--end .container-->
        </div><!--end .banner-main-->
    </div><!--end #wrap-banner-top-->
</section>

<section>
    <div id="wrap-banner-center">
        <div class="container">
            <div class="col-sm-4 col-xs-12 box">
                <a href="#"><img src="/img/banner-center-1.png" alt="kyna.vn" class="img-responsive" /></a>
            </div><!--end .col-sm-4 col-xs-12 box-->
            <div class="col-sm-4 col-xs-12 box">
                <a href="#"><img src="/img/banner-center-2.png" alt="kyna.vn" class="img-responsive" /></a>
            </div><!--end .col-sm-4 col-xs-12 box-->
            <div class="col-sm-4 col-xs-12 box">
                <a href="#"><img src="/img/banner-center-3.png" alt="kyna.vn" class="img-responsive" /></a>
            </div><!--end .col-sm-4 col-xs-12 box-->
        </div><!--end .container-->
    </div><!--end #wrap-banner-center-->
</section>

<section>
    <div id="wrap-content">
        <div class="container">
            <h2 class="title-main color-den-n">Khóa học nổi bật cho bạn</h2>
            <ul class="clearfix wrap-box-product">
            {foreach from=$highLightCourses item=$course}
                <li class="col-md-3 col-sm-6 col-xs-12 box-product">
                    {$this->render('@app/views/layouts/common/box-product', ['model' => $course])}
                </li><!--end .col-md-3 col-sm-6 col-xs-12 box-product-->
            {/foreach}
            </ul>

        </div><!--end .container-->
        <span class="button-more"><a href="{Url::toRoute(['/course/default/index'])}">Xem tất cả khóa học</a></span>
    </div><!--end #wrap-content-->
</section>

<section>
    <div id="wrap-banner-bottom">
        <div class="banner-main clearfix">
            <div class="container">
                <div class="wrap-content-banner-bottom col-md-10 col-md-offset-1 col-md-12">
                    <h2 class="title-main">Danh mục khóa học đang cung cấp</h2>
                    <div class="wrap clearfix">
                        {foreach from=$this->context->rootCats key=key item=rootCat}
                            {if ($key < 11)}
                            <div class="box">
                                <a href="{$rootCat.url}" class="title" title='{$rootCat.name}'>
                                    <span class="text">{$rootCat.name}</span>
                                    <span class="icon"><i class="fa fa-archive"></i></span>
                                </a>
                            </div><!--end .box-->
                            {/if}
                        {/foreach}
                        <div class="box">
                            <span>
                                <a href="{Url::toRoute(['/course/default/index'])}" class="title button">
                                    Xem tất cả
                                </a>
                            </span>
                        </div><!--end .box-->
                    </div><!--end .wrap-->
                </div><!--end .wrap-content-banner-bottom-->
            </div><!--end .container-->
        </div><!--end .banner-main-->
    </div><!--end #wrap-banner-bottom-->
</section>

<section>
    <div id="wrap-content-bottom">
        <div class="col-md-6 col-xs-12 background-white left-before"></div>
        <div class="container pd0-mb">
            <div class="col-md-6 col-xs-12 left">
                <h2 class="title-main color-den-n">Tại sao nên chọn Kyna?</h2>
                <ul>
                    <li>
                        <span class="icon">
                            <img src="/img/icon-content-bottom.png" alt="Tại sao nên chọn Kyna" class="img-responsive"/>
                        </span>
                        <span class="text">
                            <span class="color-green bold">Thanh toán linh động: </span>
                            tiền mặt, chuyển khoản ngân hàng, online qua ngân hàng Vietcombank, Vietinbank… , thẻ VISA/Mastercard
                        </span>
                    </li>
                    <li>
                        <span class="icon">
                            <img src="/img/icon-content-bottom.png" alt="Tại sao nên chọn Kyna" class="img-responsive"/>
                        </span>
                        <span class="text">
                            <span class="color-green bold">Thanh toán linh động: </span>
                            tiền mặt, chuyển khoản ngân hàng, online qua ngân hàng Vietcombank, Vietinbank… , thẻ VISA/Mastercard
                        </span>
                    </li>
                    <li>
                        <span class="icon">
                            <img src="/img/icon-content-bottom.png" alt="Tại sao nên chọn Kyna" class="img-responsive"/>
                        </span>
                        <span class="text">
                            <span class="color-green bold">Thanh toán linh động: </span>
                            tiền mặt, chuyển khoản ngân hàng, online qua ngân hàng Vietcombank, Vietinbank… , thẻ VISA/Mastercard
                        </span>
                    </li>
                    <li>
                        <span class="icon">
                            <img src="/img/icon-content-bottom.png" alt="Tại sao nên chọn Kyna" class="img-responsive"/>
                        </span>
                        <span class="text">
                            <span class="color-green bold">Thanh toán linh động: </span>
                            tiền mặt, chuyển khoản ngân hàng, online qua ngân hàng Vietcombank, Vietinbank… , thẻ VISA/Mastercard
                        </span>
                    </li>
                </ul>
            </div><!--end .col-md-6 col-xs-12 left-->
            <div class="col-md-6 col-xs-12 right pd0-mb">
                <h2 class="title-main title-slider-content-bottom">Học viên nói về chúng tôi</h2>
                <div class="wrap-slider-content-bottom">
                    <div class="wrap">
                        <div class="detail clearfix">
                            <div class="col-md-4 col-xs-12 pd0">
                                <img src="/img/home/lechinhdoan.png" alt="Lê Chính Đoan" class="img-responsive"/>
                            </div><!--end .col-md-4 col-xs-12 left-->
                            <div class="col-md-8 col-xs-12 right">
                                <span class="title bold">Chị Lê Chính Đoan (Tp.HCM)</span>
                                <span class="lesson">Học viên khóa học Cho con ăn đúng cách</span>
                            </div><!--end .col-md-8 col-xs-12 right-->
                        </div><!--end .detail-->
                        <p class="content">Tôi đã có kinh nghiệm trong việc nuôi dạy hai con của tôi (bé nhỏ nay đã 2 tuổi) tuy nhiên những gì mà khóa học mang lại vẫn rất hữu ích cho bản thân tôi. Theo tôi, cái khó nhất đó là quan điểm của cha mẹ trong việc nuôi dạy con. Ở phụ huynh, thường hay có sự so sánh con mình với con hàng xóm mà quên mất các yếu tố khác (như cô Liên đã nói) và thường lo lắng quá mức trong từng bữa ăn của con. Do đó, cha mẹ tự tạo áp lực cho mình và trẻ là người nhận lãnh hậu quả (bữa ăn trở thành giờ cực hình). Cảm ơn Cô đã rất nhẹ nhàng, chân tình nhưng rất tâm lý và sâu sắc qua từng lời giảng. Kính chúc Cô nhiều sức khỏe, giữ mãi phong độ và tâm huyết để giúp ích cho cộng đồng và mang lại niềm vui cho trẻ thơ. Chúc cho các phụ huynh học được cách nuôi dạy con thích hợp với các thiên thần của mình. Cảm ơn chương trình rất nhiều.</p>
                    </div><!-- .wrap-->

                    <div class="wrap">
                        <div class="detail clearfix">
                            <div class="col-md-4 col-xs-12 pd0">
                                <img src="/img/home/nguyenxuanxanh.png" alt="Nguyễn Xuân Xanh" class="img-responsive"/>
                            </div><!--end .col-md-4 col-xs-12 left-->
                            <div class="col-md-8 col-xs-12 right">
                                <span class="title bold">Nguyễn Xuân Xanh, Đồng Nai</span>
                            </div><!--end .col-md-8 col-xs-12 right-->
                        </div><!--end .detail-->
                        <p class="content">Tôi rất tâm đắc bài học về sự “nhượng bộ - cam kết” khi bị khách hàng từ chối để bán được mức giá mà mình mong muốn. Các tình huống của anh Seb Trần đưa ra rất sát với thực tế công việc tôi đang gặp phải. Rất hữu ích, cảm ơn!</p>
                    </div><!--end .wrap-->
                </div><!--end .wrap-slider-content-bottom-->

            </div><!--end .col-md-6 col-xs-12 right-->
        </div><!--end .container-->
    </div><!--end #wrap-content-bottom-->
</section>

<section>
    <div id="wrap-banner-footer">
        <div class="banner-main clearfix">
            <div class="container">
                <span class="text">Tham gia giảng dạy tại Kyna</span>
                <a href="#" class="button">Tìm hiểu thêm</a>
                <img src="img/home/home-giangvien.png" alt="Tham gia giảng dạy tại Kyna" class="img-responsive"/>
            </div>
        </div><!--end .banner-main-->
    </div><!--end #wrap-banner-footer-->
</section>
