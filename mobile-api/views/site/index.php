<?php
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\bootstrap\Html;
use kyna\tag\models\Tag;
use common\helpers\CDNHelper;

$this->title = "Kyna.vn - Học online cùng chuyên gia";
$cdnUrl = CDNHelper::getMediaLink();

?>

<main>
    <section>
        <div id="k-banner" class="clearfix k-height-header pc">
            <div class="container">
                <div class="wrap-content col-md-10 col-md-offset-1 pd0">
                    <h1 class="title">Tìm khóa học bạn đang quan tâm</h1>
                    <?php $form = ActiveForm::begin([
                        'id' => 'search-form-index',
                        'action' => Url::toRoute(['/course/default/index']),
                        'method' => 'get',
                        'options' => [
                            'class' => 'clearfix form-search'
                        ]
                    ]); ?>
                    <input type="text" name="q" class="form-control" placeholder="Nhập từ khóa để tìm khóa học bạn cần">
                    <button class="btn btn-default" type="submit">
                        <i class="icon-search icon"></i>
                    </button>
                    <?php ActiveForm::end(); ?>
                    <?php
                        $hotKeywords = Tag::topTags(Tag::TYPE_DESKTOP, 10);
                    ?>
                    <ul>
                        <?php foreach ($hotKeywords as $tag) { ?>
                            <li class="box">
                                <a href="<?= Url::toRoute(['/course/default/index', 'tag' => $tag->slug]); ?>" title="<?= $tag->title ?>">
                                    <?= $tag->tag; ?>
                                </a>
                            </li>
                        <?php } ?>
                    </ul>
                </div>
                <!--end .wrap-content-banner-top -->
            </div><!--end .container-->
        </div><!--end #k-banner-->

        <?= \frontend\widgets\SliderWidget::widget() ?>
    </section>

    <section>
        <div class="container mb" id="k-interest">
            <h2 class="title">Chủ đề bạn quan tâm</h2>
            <ul class="wrap-list">
                <?php $mobileTags = Tag::topTags(Tag::TYPE_MOBILE) ?>
                <?php foreach($mobileTags as $tag) { ?>
                    <li class="col-xs-6 box">
                        <a href="<?= Url::toRoute(['/course/default/index', 'tag' => $tag->slug]); ?>">
                            <?= CDNHelper::image($tag->image_url, [
                                'alt' => $tag->tag,
                                'class' => 'img-fluid',
                                'size' => CDNHelper::IMG_SIZE_ORIGINAL,
                                'resizeMode' => 'cover',
                            ]) ?>
                            <span><?= $tag->tag ?></span>
                        </a>
                    </li>
                <?php } ?>
<!--                <li class="col-xs-6 box">-->
<!--                    <a href="#">-->
<!--                        <img src="../src/img/mobile/home/truyen-thong-tiep-thi.png" alt="Truyền thông, tiếp thị" class="img-fluid">-->
<!--                        <span>Truyền thông, tiếp thị</span>-->
<!--                    </a>-->
<!--                </li>-->
<!--                <li class="col-xs-6 box">-->
<!--                    <a href="#">-->
<!--                        <img src="../src/img/mobile/home/kinh-doanh.png" alt="Kinh doanh" class="img-fluid">-->
<!--                        <span>Kinh doanh</span>-->
<!--                    </a>-->
<!--                </li>-->
<!--                <li class="col-xs-6 box">-->
<!--                    <a href="#">-->
<!--                        <img src="../src/img/mobile/home/ky-nang-cho-nguoi-di-lam.png" alt="Kỹ năng cho người đi làm" class="img-fluid"><span>Kỹ năng cho người đi làm</span></a>-->
<!--                </li>-->
<!--                <li class="col-xs-6 box">-->
<!--                    <a href="#"><img src="../src/img/mobile/home/nuoi-day-con.png" alt="Nuôi dạy con" class="img-fluid"><span>Nuôi dạy con</span></a>-->
<!--                </li>-->
<!--                <li class="col-xs-6 box">-->
<!--                    <a href="#"><img src="../src/img/mobile/home/nghe-thuat-va-doi-song.png"-->
<!--                                     alt="Nghệ thuật và đời sống" class="img-fluid"><span>Nghệ thuật và đời sống</span></a>-->
<!--                </li>-->
            </ul>
            <div class="btn-more">
                <a href="/danh-sach-khoa-hoc" class="btn-block">Xem tất cả <i class="icon icon-arrow-right"></i></a>
            </div><!--end .btn-more-->
        </div><!--end #k-interest-->

        <!-- <div class="clearfix k-teaching mb first">
            <div class="col-xs-5 img">
                <img src="../src/img/mobile/teaching-first.png" alt=""/>
            </div>
            <div class="col-xs-7 text">
                <p>Bạn muốn giảng dạy tại Kyna?</p>
                <a href="/p/kyna/hop-tac-giang-day-online" class="btn">Tìm hiểu</a>
            </div>
        </div> -->
    </section>

    <section>
        <div id="k-highlights" class="container">
            <h2 class="title">Khóa học nổi bật cho bạn</h2>
            <ul class="clearfix k-box-card-list">
                <?php foreach ($highLightCourses as $course) { ?>
                    <li class="col-xl-3 col-lg-4 col-md-6 col-xs-12 k-box-card">
                        <?= $this->render('@app/modules/course/views/default/_box_product', ['model' => $course]); ?>
                    </li>
                <?php } ?>
            </ul>
        <span class="button-more">
            <a href="/danh-sach-khoa-hoc" class="btn btn-primary-kyna">Xem tất cả khóa học</a>
        </span>
        </div><!--end #k-highlights-->
    </section>

    <section>
        <div id="k-supply-list" class="pc">
            <div class="container k-supply-list-wrap">
                <div class="k-supply-list-inner col-lg-10 col-lg-offset-1 col-sm-12">
                    <h2 class="title">Danh mục khóa học đang cung cấp</h2>
                    <div class="k-supply-list-box clearfix">
                        <ul>
                            <?php foreach ($this->context->rootCats as $key => $rootCat) { ?>
                                <?php if ($key < 11) { ?>
                                    <li>
                                        <a href="<?= $rootCat->url ?>" title="<?= $rootCat->name; ?>">
                                            <span class="text"><?= $rootCat->name; ?></span>
                                            <span class="icon <?= $rootCat->slug; ?>"></span>
                                        </a>
                                    </li>
                                <?php } ?>
                            <?php } ?>
                            <li>
                  <span>
                    <a href="<?= Url::toRoute(['/course/default/index']); ?>" class="title button">
                        Xem tất cả
                    </a>
                  </span>
                            </li>
                        </ul>
                    </div>
                    <!--end .k-supply-list-box-->
                </div>
                <!--end .wrap-content-banner-bottom-->
            </div>
            <!--end .k-supply-list-wrap-->
        </div><!--end #k-supply-list-->
    </section>

    <section>
        <div id="k-about-us" class="clearfix pc">
            <div class="col-lg-6 col-md-12 col-xs-12 k-about-us-background"></div>
            <div class="container">
                <div class="col-lg-6 col-md-12 col-xs-12 k-about-us-why">
                    <h2 class="title">Tại sao nên chọn Kyna?</h2>
                    <ul>
                        <li>
                    <span class="icon">
                    <img src="<?= $cdnUrl ?>/src/img/home/hoc-cung-chuyen-gia.svg" alt="Học cùng chuyên gia" class="img-fluid">
                    </span>
                    <span class="text">
                    <span>Học cùng chuyên gia: </span>
                    Tương tác với chuyên gia, nhận sự trợ giúp từ chuyên gia.
                    </span>
                        </li>
                        <li>
                    <span class="icon">
                    <img src="<?= $cdnUrl ?>/src/img/home/hoc-mai-mai.svg" alt="Thanh toán một lần" class="img-fluid">
                    </span>
                    <span class="text">
                    <span>Thanh toán một lần: </span>
                    Phương thức thanh toán linh hoạt, thanh toán một lần sở hữu bài học mãi mãi.
                    </span>
                        </li>
                        <li>
                    <span class="icon">
                    <img src="<?= $cdnUrl ?>/src/img/home/hoc-moi-noi.svg" alt="Mọi lúc mọi nơi" class="img-fluid">
                    </span>
                    <span class="text">
                    <span>Mọi lúc mọi nơi: </span>
                    Học mọi lúc mọi nơi qua điện thoại, máy tính, ipad có kết nối Internet.
                    </span>
                        </li>
                        <li>
                    <span class="icon">
                    <img src="<?= $cdnUrl ?>/src/img/home/hoan-tien.svg" alt="Cam kết hoàn tiền" class="img-fluid">
                    </span>
                    <span class="text">
                    <span>Cam kết hoàn tiền: </span>
                    Học viên được hoàn tiền học phí nếu thấy khóa học không hiệu quả.
                    </span>
                        </li>
                    </ul>
                </div>
                <!--end .col-md-6 col-xs-12 left-->
                <div class="col-lg-6 col-md-12 col-xs-12 k-about-us-comment">
                    <h2 class="title">Học viên nói về chúng tôi</h2>
                    <div class="k-about-us-slide">
                        <ul>
                            <li>
                                <div class="k-about-us-customer clearfix">
                                    <div class="col-md-4 col-xs-12 img">
                                        <img src="<?= $cdnUrl ?>/src/img/home/hinhme1.png" alt="Chị Lê Hương - mẹ bé Quỳnh Anh (Tp.HCM)" class="img-fluid"/>
                                    </div><!--end .col-md-4 col-xs-12 left-->
                                    <div class="col-md-8 col-xs-12 text">
                                        <span>Chị Lê Hương - mẹ bé Quỳnh Anh (Tp.HCM)</span>
                                        <span>Cùng con tham gia khóa <b>Học tính toán nhanh cùng bàn tính Soroban</b></span>
                                    </div><!--end .col-md-8 col-xs-12 right-->
                                </div><!--end .detail-->
                                <p>Vì muốn Quỳnh Anh có sự chuẩn bị thật tốt trước khi vào lớp một nên mình cho bé làm quen với toán sớm. Nhưng khi nhắc đến toán thì bé cứ lơ đi, không có hứng thú. Tình cờ mình biết đến khóa học toán online của Kyna, rất bất ngờ là bé rất vui vẻ và học rất chăm chỉ. Nhờ khóa học mà Quỳnh Anh nhà mình giờ đây không còn sợ toán nữa, cách học thuận tiện của Kyna cũng giúp mình được bên con nhiều hơn và theo dõi việc học của con dễ dàng.</p>
                            </li>

                            <li>
                                <div class="k-about-us-customer clearfix">
                                    <div class="col-md-4 col-xs-12 img">
                                        <img src="<?= $cdnUrl ?>/src/img/home/hinhme2.png" alt="Anh Phạm Nguyên Vũ (Tp.HCM)" class="img-fluid"/>
                                    </div><!--end .col-md-4 col-xs-12 left-->
                                    <div class="col-md-8 col-xs-12 text">
                                        <span>Anh Phạm Nguyên Vũ (Tp.HCM)</span>
                                        <span>Học viên khóa <b>Kỹ năng thuyết phục khách hàng và xử lý từ chối</b></span>
                                    </div><!--end .col-md-8 col-xs-12 right-->
                                </div><!--end .detail-->
                                <p>Làm kinh doanh, điều quan trọng nhất đối với tôi đó chính là khách hàng. Vì có họ thì mình mới tồn tại được. Mấy năm trước do còn non kinh nghiệm nên tôi luôn gặp vấn đề với những “thượng đế” của mình. Nhưng bây giờ thì đã khác, từ khi tham gia khóa học của thầy Lê Kim Tú và ứng dụng những gì thầy dạy vào thực tế, tôi trở nên tự tin và chủ động hơn, biết cách nắm bắt tâm lý để từ đó thuyết phục khách hàng. Khi họ từ chối cũng không sao, tôi cũng đã biết cách tạo cơ hội tốt để hợp tác với họ vào một dịp khác trong tương lai.</p>
                            </li>

                            <li>
                                <div class="k-about-us-customer clearfix">
                                    <div class="col-md-4 col-xs-12 img">
                                        <img src="<?= $cdnUrl ?>/src/img/home/hinhme3.png" alt="Bạn Nguyễn Hoàng Thảo Uyên (Huế)" class="img-fluid"/>
                                    </div><!--end .col-md-4 col-xs-12 left-->
                                    <div class="col-md-8 col-xs-12 text">
                                        <span>Bạn Nguyễn Hoàng Thảo Uyên (Huế)</span>
                                        <span>Học viên khóa <b>Tự học tiếng Nhật cho người mới bắt đầu</b></span>
                                    </div><!--end .col-md-8 col-xs-12 right-->
                                </div><!--end .detail-->
                                <p>Mình mê Nhật Bản từ những bộ phim hoạt hình và luôn mong một ngày nào đó sẽ biết tiếng Nhật. Vậy mà, đi học rồi ra trường đi làm bận rộn, mãi vẫn không thực hiện được. Hồi đầu năm vừa rồi, ngồi lên plan cho năm mới thì quyết tâm là sẽ học tiếng Nhật thôi. Thế là tham gia khóa học tại Kyna. Vì học online nên không ảnh hưởng lắm đến công việc, chương trình học được biên soạn rất khoa học, bài bản, sinh động nữa,… nên mình học và tiếp thu rất nhanh. Đến giờ qua năm mới rồi, mình đã có một vốn tiếng Nhật kha khá và đặc biệt là hoàn thành được mục tiêu. Hehe! ^^</p>
                            </li>
                        </ul>

                    </div><!--end .wrap-slider-content-bottom-->
                </div>
                <!--end .col-md-6 col-xs-12 right-->
            </div>
            <!--end .container-->
        </div>
        <!--end #wrap-content-bottom-->
    </section>

    <section>
        <div id="k-teaching-kyna" class="pc">
            <div class="container k-teaching-kyna-wrap">
                <h2>Tham gia giảng dạy tại Kyna</h2>
                <a href="/p/kyna/giang-day" class="button btn">Tìm hiểu thêm</a>
                <img src="<?= $cdnUrl ?>/src/img/home/home-giangvien.png" alt="Tham gia giảng dạy tại Kyna" class="img-fluid">
            </div>
        </div>

        <div class="clearfix k-teaching mb">
            <div class="col-xs-5 img">
                <img src="<?= $cdnUrl ?>/src/img/mobile/teaching.png" alt=""/>
            </div><!--end .img -->
            <div class="col-xs-7 text">
                <a href="/p/kyna/gioi-thieu" class="btn">Giới thiệu về Kyna</a>
            </div><!--end .text -->
        </div><!--end .k-teaching-->
    </section>

    <!-- <div id="feedback-v3">
        <a href="/p/kyna/gop-y-phien-ban-moi?utm_source=website&utm_medium=mainsite&utm_campaign=surveylaunch" target="_blank">
            <span>Góp ý</span>
            <img src="../src/img/icon-feedback.png" alt="Góp ý">
        </a>
    </div> -->


    <script type="text/javascript">
      jQuery(document).ready(function($) {
        $(document).on('shown.bs.modal', "#modal", function () {
          setTimeout(function () {
            FB.XFBML.parse();
          }, 2000);
        });
      });
      $zopim(function() {
        $zopim.livechat.window.onShow(function () {
          $('#feedback').css('bottom', '450px');
        });
        $zopim.livechat.window.onHide(function () {
          $('#feedback').css('bottom', '40px');
        })
      });
    </script>
</main>
