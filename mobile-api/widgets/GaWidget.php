<?php

namespace frontend\widgets;

use Yii;
use yii\web\View;

class GaWidget extends \yii\base\Widget
{
    
    public $id = '';
    public $loadEventScript = false;
    public $position = View::POS_BEGIN;
    
    public function run()
    {
        if (empty($this->id)) {
            return;
        }
        return $this->render('ga', [
            'id' => $this->id,
            'position' => $this->position,
            'loadEventScript' => $this->loadEventScript
        ]);
    }
}
