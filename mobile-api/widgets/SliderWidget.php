<?php
/**
 * Created by PhpStorm.
 * User: macintosh
 * Date: 5/16/17
 * Time: 2:09 PM
 */

namespace frontend\widgets;

use yii\base\Widget;

use kyna\settings\models\BannerGroup;

class SliderWidget extends Widget
{

    public function run()
    {
        $date = date('Y-m-d');

        $groups = BannerGroup::find()->where([
            'type' => BannerGroup::TYPE_HOME_SLIDER
        ])
            ->andWhere("from_date is null or (from_date is not null and from_date <= '{$date}')")
            ->andWhere("to_date is null or (to_date is not null and to_date >= '{$date}')")
            ->andWhere(['status' => BannerGroup::STATUS_ACTIVE])
            ->orderBy('id DESC')
            ->all();

        if (empty($groups)) {
            return false;
        }
        return $this->render('slider', ['groups'=> $groups]);
    }
}