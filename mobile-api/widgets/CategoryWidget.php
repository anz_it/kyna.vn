<?php

namespace app\widgets;

use app\models\Category;

/*
 * This is Category widget to get categories and display on Frontend
 */
class CategoryWidget extends \yii\base\Widget
{
    public function run()
    {
        $catModel = \Yii::$app->category->current;
        $catId = ($catModel) ? $catModel->id : 0;
        $rootCats = Category::getList($catId, ['id', 'name', 'slug', 'parent_id']);

        if (!sizeof($rootCats) and $catModel->parent_id > 0) {
            $rootCats = Category::getList($catModel->parent_id, ['id', 'name', 'slug', 'parent_id']);
            $catModel = $catModel->parent;
        }

        return $this->render('category', [
            'rootCats' => $rootCats,
            'catId' => $catId,
            'catModel' => $catModel,
        ]);
    }
}
