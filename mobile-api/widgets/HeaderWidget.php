<?php

namespace mobile\widgets;

use yii;
use common\widgets\base\BaseWidget;

class HeaderWidget extends BaseWidget
{

    public $rootCats;

    public function run()
    {
        return $this->getHeader();
    }

    public function getHeader()
    {
        $catId = false;
        if ($cate = Yii::$app->category->current) {
            $catId = $cate->id;
        }
        return $this->render('header', [
                    'settings' => $this->settings,
                    'rootCats' => $this->rootCats,
                    'catId' => $catId
        ]);
    }

}
