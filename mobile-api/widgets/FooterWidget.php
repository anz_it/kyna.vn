<?php

namespace mobile\widgets;

use yii;
use common\widgets\base\BaseWidget;

class FooterWidget extends BaseWidget
{

    public function run()
    {
        return $this->getFooter();
    }

    public function getFooter()
    {
        return $this->render('footer', [
            'settings' => $this->settings,
        ]);
    }

}
