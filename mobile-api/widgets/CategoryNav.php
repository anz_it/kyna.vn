<?php

namespace app\widgets;

use Yii;
use yii\base\Widget;
use app\models\Category;

class CategoryNav extends Widget {
    private $_categories;
    public $view = 'category-nav';

    public $refresh = false;

    public function init() {
        parent::init();

        if ($this->refresh) {
            Yii::$app->cache->delete('categories');
        }

        if (!($categories = Yii::$app->cache->get('categories'))) {
            $categories = Category::find()->all();
            Yii::$app->cache->set('categories', $categories);
        }
        $this->_categories = $categories;
    }

    public function run() {
        return $this->render($this->view, [
            'categories' => $this->_categories,
        ]);
    }
}
