<?php
/**
 * Created by PhpStorm.
 * User: macintosh
 * Date: 8/8/17
 * Time: 4:31 PM
 */

use yii\web\View;
?>

<a id="popup" href="" data-href-pc="<?= $cdnUrl . $popup->image_url ?>" data-href-mob="<?= $cdnUrl . (!empty($popup->mobile_image_url) ? $popup->mobile_image_url : $popup->image_url) ?>" data-fancybox-group="gallery" title="<?= $popup->title ?>">

</a>

<?php
$script = "
;(function ($, window, document, undefined) {
    $(document).ready(function () {
        if (window.matchMedia('(max-width: 767px)').matches)
            $('#popup').attr('href',$('#popup').data('href-mob'));
        else
            $('#popup').attr('href',$('#popup').data('href-pc'));
        $('#popup').fancybox({
            afterShow: function () {
                $('img.fancybox-image').wrap($('<a />', {
                    // set anchor attributes
                    href: '{$popup->link}',
                    title: '{$popup->title}',
                    target: '_blank'
                }));
            }
        }).trigger('click');
    });
})(window.jQuery, window, document);";

$this->registerJs($script, View::POS_END, 'register-popup-js');
?>
