<?php
/* @var $this yii\web\View */

use yii\helpers\Url;
use yii\web\View;
use yii\widgets\ActiveForm;

use mobile\models\Course;
use frontend\widgets\BannerWidget;
use kyna\settings\models\Banner;
use common\helpers\CDNHelper;

$cdnUrl = CDNHelper::getMediaLink();

?>

<?= BannerWidget::widget([
    'type' => Banner::TYPE_TOP,
    'id' => 'fixed-topbar'
]) ?>

<header>
    <nav class="navbar navbar-light k-header-wrap">
        <div class="container">
            <div class="navbar-brand logo col-lg-2 col-md-2">
                <?php if (empty($settings) || empty($settings['logo_url'])) { ?>
                    <a href="/p/campaign/dong-gia/399564"><img src="<?= $cdnUrl ?>/img/logo_1_tang_1.svg" alt="Kyna.vn" class="img-fluid"></a>
                <?php } else { ?>
                    <img src="<?= $settings['logo_url']; ?>" alt="Kyna.vn" class="img-fluid"/>
                <?php } ?>
            </div>

            <ul class="nav navbar-nav k-header-menu col-xl-2 col-md-1">
                <li class="nav-item dropdown">
                    <div class="nav-wrap">
                        <a class="nav-link" href="#" data-toggle="dropdown" role="button" aria-haspopup="true"
                           aria-expanded="false"><i class="icon icon-bars"></i>
                            <span>Danh mục khóa học</span></a>
                        <span class="hidden-xs-down"><a href="#" class="nav-mb" data-offpage="#nav-mobile"><i
                                    class="icon icon-bars"></i></a></span>
                        <ul class="dropdown-menu wrap-menu-list clearfix ">
                            <!--Button SeeMore-->
                            <li class="see-more"><a href="<?= Url::toRoute(['/course/default/index']) ?>">Tất cả khoá học</a>
                            </li>

                            <li class="khuyen-mai-nhom-khoa-hoc">
                                <a href="<?= Url::toRoute(['/course/default/index', 'course_type' => Course::TYPE_COMBO]) ?>">
                                    Khóa học Combo
                                </a>
                            </li>
                            <?php foreach ($rootCats as $rootCat) { ?>
                                <li class="<?= $rootCat->slug; ?>">
                                    <a href="<?= $rootCat->url ?>"><?= $rootCat->name; ?></a>
                                </li>
                            <?php } ?>
                        </ul>
                    </div>
                </li>
            </ul>

            <?php if (!Yii::$app->user->isGuest) { ?>
                <?php echo $this->render('@app/views/layouts/common/header_right_login') ?>
            <?php } else { ?>
                <?php echo $this->render('@app/views/layouts/common/header_right_guest') ?>
            <?php } ?>

            <?php if(Yii::$app->request->url != Url::toRoute(['/user/security/login']) && Yii::$app->request->url != Url::toRoute(['/user/registration/register'])) { ?>
            <div class="form-inline k-header-search col-md-4 col-md-pull-4 col-xs-12">
                <div class="row-menu-bar-mobile hidden-sm-up">
                    <div class="k-menu-list-course col-xs-8">
                        <a class="nav-link" href="#" data-offpage="#nav-mobile">
                            <i class="icon icon-bars"></i>
                            <span>Danh mục khóa học</span>
                        </a>
                    </div>
                    <div class="k-button-search-course col-xs-4">
                        <a id="k-button-search-course" class="nav-link" href="#">
                            <i class="icon-search icon"></i>
                            <span>Tìm</span>
                        </a>
                    </div>
                </div>
                <?php
                $get = $_GET;
                unset($get['q']);
                unset($get['sort']);
                unset($get['page']);
                $form = ActiveForm::begin([
                    'id' => 'search-form',
                    'action' => Url::toRoute(['/course/default/index']),
                    'method' => 'get'
                ]); ?>

                <div class="input-group">
                    <button class="icon-search hidden-sm-up"></button>
                    <input id="live-search-bar" name="q" type="text" class="form-control live-search-bar" placeholder="Tìm khóa học bạn quan tâm"
                           autocomplete="off">
                        <span class="input-group-btn">
                            <button class="btn btn-secondary search-button" type="submit"><i
                                  class="icon-search icon hidden-480"></i>
                            <span class="mob">Tìm</span></button>
                        </span>
                    <span class="k-close-search-popup hidden-sm-up" id="k-close-search-popup">
                        <svg width="12" height="12" viewBox="0 0 24 24"><path d="m367 256l142-141c2-2 3-5 3-8c0-3-1-5-3-7l-96-97c-2-2-4-3-7-3c0 0 0 0 0 0c-3 0-6 1-8 3l-142 142l-141-142c-2-2-5-3-8-3c0 0 0 0 0 0c-3 0-5 1-7 3l-96 96c-5 4-5 11-1 15l142 142l-142 141c-4 4-4 11 0 15l96 97c2 2 4 3 7 3l0 0c3 0 6-1 8-3l142-142l141 142c2 2 5 3 8 3c3 0 5-1 7-3l97-96c4-4 4-11 0-15z" transform="scale(0.046875 0.046875)"></path></svg>
                    </span>
                </div>
                <?php ActiveForm::end(); ?>

                <!--               Live earch result-->
                <div id="live-search-result" class="live-search-result" ></div>
                <?php $this->registerJsFile($cdnUrl . '/src/js/jquery-ui.js', ['position' => View::POS_END]) ?>
                <?php $this->registerJsFile($cdnUrl . '/src/js/autocomplete.js', ['position' => View::POS_END]) ?>
                <!--                End live search result-->
            </div>
            <?php } ?>

        </div><!--end .container-->
    </nav>

    <!-- Menu mobile -->
    <div id="nav-mobile" class="offpage-menu offpage-left">
        <header>
            <div class="k-header-offpage-menu">
                <a href="/p/campaign/dong-gia/399564">
                    <img src="<?= $cdnUrl ?>/img/logo_1_tang_1.svg" alt="Kyna.vn" class="img-responsive">
                </a>
                <a href="#" class="right offpage-close" data-offpage="#nav-mobile">
                    <i class="icon icon-arrow-right-bold" aria-hidden="true"></i>
                </a>
            </div><!--header -->
        </header>
        <div class="content">
            <div class="panel-group" id="accordion">
                <?php if (empty($catId)) : ?>
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <div class="panel-title danh-sach-khoa-hoc">
                                <a href="<?= Url::toRoute(['/course/default/index']) ?>">Tất cả khóa học</a>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <div class="panel-title khuyen-mai-nhom-khoa-hoc">
                              <a href="<?= Url::toRoute(['/course/default/index', 'course_type' => Course::TYPE_COMBO]) ?>">
                                  Khóa học Combo
                              </a>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>
                <?php foreach($rootCats as $rootCat) { ?>
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <div class="panel-title <?= $rootCat->slug; ?>">
                                <a href="<?= $rootCat->url; ?>"><?= $rootCat->name; ?></a>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
            <!-- ./panel-group -->
        </div><!--end .content-->
    </div><!--end #nav-mobile-->
    <!-- End Menu mobile-->
</header>

<?php $script = "
;(function($){
    if($('#topbar').length > 0){
      var imgHeight = $('#topbar').find('img:visible').height();
      var LISTING_MARGIN_TOP = 67;
      $('.k-header-wrap').css('top', imgHeight + 'px');
      $('#k-listing').css('marginTop', LISTING_MARGIN_TOP + imgHeight + 'px');
    }
    $('body').on('submit', '#profile-form', function(e) {
         e.preventDefault();

         var url = $(this).attr('action');
         var form = $(e.target);
         console.log(form);
         console.log(form.parent());

         $.post(url, form.serialize(), function (res) {
             form.parents('.k-profile-edit-content').html(res);
         });
    });
    $('body').on('submit', '#active-cod-form', function(e){
        e.preventDefault();

        var url = $(this).attr('action');
        var form = $(e.target);

        $.post(url, form.serialize(), function (res) {
            form.parent().html(res);
        });
    });
})(jQuery);
" ?>

<?php $this->registerJs($script, View::POS_END, 'profile-submit') ?>
