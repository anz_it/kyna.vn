<?php
use yii\helpers\Url;
?>
<?php foreach ($categories as $category) : ?>
    <a href="<?= $category->url ?>" class="dropdown-item category-icon <?= $category->slug; ?>">
        <?= $category->name; ?>
    </a>
<?php endforeach; ?>
<!--Button SeeMore-->
<a href="<?= Url::toRoute(['/course/default/index']) ?>" class="dropdown-item category-icon see-more">Xem thêm</a>
