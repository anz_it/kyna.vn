<?php
/**
 * Created by PhpStorm.
 * User: khanhphan
 * Date: 4/28/17
 * Time: 12:41 AM
 */
use common\helpers\Html;
use common\elastic\Course;

$keys = [
    'discount' => 'Tìm theo chương trình khuyến mãi',
    'hot' => 'Tìm theo đặc điểm khóa học',
    'time' => 'Tìm theo thời lượng',
    'level' => 'Tìm theo trình độ yêu cầu',
];

$arrKeys = array_keys($keys);
usort($facets, function ($a, $b) use ($arrKeys) {
    return array_search($a['key'], $arrKeys) > array_search($b['key'], $arrKeys);
});

?>
<section>
    <?php
    $url = parse_url(Yii::$app->request->url);
    echo Html::beginForm($url['path'], 'get', [
        'id' => 'facets-form'
    ]);
    if (isset($_GET['q'])) {
        echo Html::hiddenInput('q', $_GET['q']);
    }
    foreach ($facets as $facet) {
        sort($facet['facet_value']['buckets']);
        $fun = "getFacet" . ucfirst($facet['key']);
        ?>
        <div class="k-listing-characteristics price-facet">
            <h3><?= $keys[$facet['key']] ?></h3>
            <ul class="k-listing-characteristics-list">
                <?php foreach ($facet['facet_value']['buckets'] as $bucket) {
                    $value = Course::$fun($bucket['key']);
                    ?>
                    <li class="checkbox">

                        <input id="facet-<?= $facet['key'] . '-' . $bucket['key'] ?>"
                               type="checkbox"
                               name="facets[<?= $facet['key'] ?>][<?= $bucket['key'] ?>]"
                            <?= isset($_GET['facets'][$facet['key']][$bucket['key']]) ? 'checked="checked"' : '' ?>
                               value="<?= $bucket['key'] ?>">
                        <label for="facet-<?= $facet['key'] . '-' . $bucket['key'] ?>">
                            <span><span></span></span><?= $value ?>
                        </label>
                        <span class="tag"><?= $bucket['doc_count'] ?></span>
                    </li>
                <?php } ?>

            </ul>
        </div>
    <?php }
    echo Html::endForm();
    ?>
</section>

<script>
    $('#facets-form :checkbox').click(function () {
        $('#facets-form').submit();
    });

</script>
