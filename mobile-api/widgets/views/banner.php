<?php
/**
 * Created by PhpStorm.
 * User: macintosh
 * Date: 5/11/17
 * Time: 4:53 PM
 */

use kyna\settings\models\Banner;
use common\helpers\CDNHelper;

$cdnUrl = CDNHelper::getMediaLink();

$numberOfBanner = count($banners);
?>
<div class="<?= $this->context->containerClass ?>" id="<?= $this->context->id ?>">
    <?php foreach ($banners as $banner) : ?>
        <?php if ($numberOfBanner > 1): ?>
            <div class="item">
        <?php endif; ?>
        <?php if (!empty($banner->link)) : ?>
            <a href="<?= $banner->link ?>" title="<?= $banner->title ?>" target="_blank">
        <?php endif; ?>
            <?php if (!empty($banner->image_url)) : ?>
                <?= CDNHelper::image($banner->image_url, [
                    'alt' => $banner->title,
                    'title' => $banner->title,
                    'class' => 'image-topbar banner-pc img-fluid',
                    'size' => CDNHelper::IMG_SIZE_ORIGINAL,
                ]) ?>
            <?php endif; ?>
            <?php if (!empty($banner->mobile_image_url)) : ?>
                <?= CDNHelper::image($banner->mobile_image_url, [
                    'alt' => $banner->title,
                    'title' => $banner->title,
                    'class' => 'image-topbar banner-mb img-fluid',
                    'size' => CDNHelper::IMG_SIZE_ORIGINAL,
                ]) ?>
            <?php endif; ?>
        <?php if (!empty($banner->link)) : ?>
            </a>
        <?php endif; ?>
        <?php if ($banner->type == Banner::TYPE_TOP): ?>
            <img src="<?= $cdnUrl ?>/img/close.png" class="close-topbar">
        <?php endif; ?>
        <?php if (!empty($banner->description) || (!empty($banner->title) && $banner->type != Banner::TYPE_CHECKOUT_PAGE)) : ?>
            <div class="info">
                <?php if (!empty($banner->title)): ?>
                <h5>
                    <a href="<?= $banner->link ?>" title="<?= $banner->title ?>" target="_blank">
                        <?= $banner->title ?>
                    </a>
                </h5>
                <?php endif; ?>
                <?= $banner->description ?>
            </div>
        <?php endif; ?>
        <?php if ($numberOfBanner > 1): ?>
            </div>
        <?php endif; ?>
    <?php endforeach; ?>
</div>
