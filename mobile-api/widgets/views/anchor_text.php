<?php

?>
<div id="anchor-text" class="col-xs-12">
    <?= $anchorText->value ?>
    <style>
        #anchor-text{
            border-bottom: 1px solid #ccc;
            padding: 10px 0 25px;
            margin: 0 15px 10px;
        }
        #anchor-text i{
            position: relative;
            font-size: 6px;
            padding: 0 6px;
            top: -3px;
        }
        #anchor-text a{
            color: initial;
        }
        #anchor-text a:hover{
            text-decoration: underline !important;
        }
    </style>
</div>
