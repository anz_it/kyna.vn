<?php
/**
 * Created by PhpStorm.
 * User: khanhphan
 * Date: 5/1/17
 * Time: 1:25 PM
 */

use yii\helpers\Url;
?>
<section>
    <div class="k-listing-hot-topics">
        <h3>Chủ đề đang hot</h3>
        <ul class="k-listing-hot-topics-list">
            <?php foreach ($tags as $tag) : ?>
                <li><a href="<?= Url::toRoute(['/course/default/index', 'tag' => $tag->slug]) ?>" title="<?= $tag->title ?>"><?= $tag->tag ?></a></li>
            <?php endforeach; ?>
        </ul>
    </div>
</section>

