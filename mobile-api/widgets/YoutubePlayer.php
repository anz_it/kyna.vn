<?php

namespace app\widgets;

use common\helpers\CDNHelper;
use Yii;
use yii\base\Widget;

class YoutubePlayer extends Widget
{

    public $thumbnailSize = CDNHelper::IMG_SIZE_THUMBNAIL_YOUTUBE;
    public $model;

    public function init()
    {
        parent::init();
    }

    public function run()
    {
        return $this->render('youtube-player', [
            'thumbnailSize' => $this->thumbnailSize,
            'model' => $this->model
        ]);
    }
}
