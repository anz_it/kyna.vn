<?php

namespace mobile\modules\cart;

/**
 * cart module definition class
 */
class CartModule extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'mobile\modules\cart\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
