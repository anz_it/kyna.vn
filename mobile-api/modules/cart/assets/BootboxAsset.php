<?php

namespace mobile\modules\cart\assets;

use yii\web\AssetBundle;

class BootboxAsset extends AssetBundle
{
    public $sourcePath = '@bower/';

    public $css = [];

    public $js = [
        'bootbox.js/bootbox.js'
    ];
    public $depends = [];

}