<?php

namespace mobile\modules\cart\components;

use app\models\Category as CategoryModel;
use Yii;
use yii\base\Component;

class Category extends Component
{
    public $categoryUrlSegments = [
        'module' => 'course',
        'controller' => 'default',
        'action' => 'index',
    ];

    public $courseUrlSegments = [
        'module' => 'course',
        'controller' => 'default',
        'action' => 'view',
    ];

    public $urlWithId = false;

    private $_category;

    public function init()
    {
        parent::init();
        $this->getCurrent();
    }

    public function getDefaultRoute()
    {
        $url = '/';
        if ($this->categoryUrlSegments['module']) {
            $url .= $this->categoryUrlSegments['module'] . '/';
        }

        $url .= ($this->categoryUrlSegments['controller']) ? $this->categoryUrlSegments['controller'] . '/' : 'default/';
        $url .= ($this->categoryUrlSegments['action']) ? $this->categoryUrlSegments['action'] . '/' : 'index';

        return $url;
    }

    private function _isCategory()
    {
        $segments = $this->categoryUrlSegments;
        $currentSegments = [
            'controller' => Yii::$app->controller->id,
            'module' => Yii::$app->controller->module->id,
            'action' => Yii::$app->controller->action->id,
        ];

        return ($segments == $currentSegments);
    }

    public function setCurrent($category)
    {
        if ($category instanceof CategoryModel) {
            $this->_category = $category;
        }
    }

    public function getCurrent()
    {
        if ($this->_category instanceof CategoryModel) {
            return $this->_category;
        }

        if ($this->_isCategory()) {
            if ($catId = Yii::$app->request->get('catId')) {
                $this->_category = CategoryModel::findOne($catId);
            } elseif ($catSlug = Yii::$app->request->get('slug')) {
                $parentSlug = Yii::$app->request->get('parent');
                $catTblName = CategoryModel::tableName();

                $catQuery = CategoryModel::find()
                    ->alias($catTblName)
                    ->andWhere([
//                        $catTblName . '.status' => CategoryModel::STATUS_ACTIVE,
                        $catTblName . '.slug' => $catSlug
                ]);
                if (!empty($parentSlug)) {
                    $catQuery->leftJoin($catTblName . ' p', "p.id = $catTblName.parent_id");
                    $catQuery->andWhere(['p.slug' => $parentSlug]);
                }
                $this->_category = $catQuery->one();
            }
        }

        return $this->_category;
    }
}
