<?php

namespace mobile\modules\cart\components;

use common\helpers\PhoneNumberHelper;

use kyna\payment\events\TransactionEvent;
use kyna\payment\models\TopupTransaction;
use kyna\payment\models\PaymentMethod;
use kyna\order\models\Order;

use mobile\modules\cart\models\form\TopUpForm;

/* 
 * Base Controller class for Cart module
 */
class Controller extends \mobile\modules\cart\controllers\Controller
{
    
    public function init()
    {
        $ret = parent::init();
        
        TransactionEvent::on(PaymentMethod::className(), PaymentMethod::EVENT_PAYMENT_SUCCESS, [$this, 'completeOrder']);

        return $ret;
    }
    
    public function completeOrder($transactionEvent)
    {
        $transData = $transactionEvent->transData;

        $order = Order::complete($transData['orderId'], $transData['transactionCode']);

        if ($order === false) {
            return;
        }

        // check topup
        if ($order->payment_method === 'epay' && $refund = $order->refund) {
            $cards = TopUpForm::getCards($refund);
            
            foreach ($cards as $card => $quantity) {
                $topupModel = new TopupTransaction();
                $topupModel->order_id = $order->id;
                $topupModel->transaction_id = $order->getPaymentTransactions()->one()->id;
                $topupModel->amount = $card;
                $topupModel->quantity = $quantity;
                $topupModel->is_topup = TopupTransaction::BOOL_NO;
                $topupModel->status = TopupTransaction::STATUS_CREATED;
                if (!empty($order->user->profile->phone_number)) {
                    $topupModel->phone_number = $order->user->profile->phone_number;
                    $topupModel->provider = PhoneNumberHelper::detectProviderCode($order->user->profile->phone_number);
                }
                
                $topupModel->save();
            }
        }
    }
    
}
