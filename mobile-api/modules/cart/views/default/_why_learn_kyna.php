<aside>
    <h4>Tại sao học trên Kyna</h4>
    <ul class="k-shopping-sidebar-list">
        <li>
            <span>Học bất cứ khi nào, ở đâu</span>
        </li>
        <li>                    
            <span>Nội dung học liên tục, xuyên suốt</span>
        </li>
        <li>                   
            <span>Được đảm bảo về chất lượng</span>
        </li>
        <li>                   
            <span>Giao khóa học tận nhà</span>
        </li>
        <li>                   
            <span>Phương thức thanh toán linh hoạt</span>
        </li>
    </ul>
</aside>