<?php

use yii\helpers\Url;
use yii\web\View;
use yii\widgets\ActiveForm;
use common\widgets\Alert;
use common\helpers\CDNHelper;

$cdnUrl = CDNHelper::getMediaLink();

$formatter = \Yii::$app->formatter;

$this->title = "Giỏ hàng của bạn";
$cartItems = array_reverse($cart->getPositions());
$totalCost = $cart->getCost();
$totalCount = $cart->getCount();

$settings = Yii::$app->controller->settings;

// TODO: get hot key words by search
$hotKeywords = !empty($settings['hot_keyword']) ? explode(',', $settings['hot_keyword']) : [
    'Giao tiếp',
    'Monkey junior',
    'Bán hàng',
    'Giáo dục sớm',
    'Tiếng anh',
    'Marketing',
    'Bất động sản',
    'Tiếng hoa',
    'Tiếng Nhật'
];
?>

<main>
    <div id="k-shopping" class="container k-height-header">
        <div class="k-shopping-list">
            <?= Alert::widget() ?>

            <?php if ($totalCount) : ?>
                <h3><span><?= $totalCount ?> khóa học</span> đã chọn</h3>
                <header class="clearfix k-shopping-list-header">
                    <div class="col-md-8 col-sm-8 col-xs-8 title">Khóa học</div>
                    <div class="col-md-2 col-sm-2 col-xs-2 number">Số lượng</div>
                    <div class="col-md-2 col-sm-2 col-xs-2 price">Giá</div>
                </header>
                <section>
                    <?php
                    $form = ActiveForm::begin([
                                'action' => Url::toRoute(['/cart/default/remove']),
                                'id' => 'cart-form',
                                'method' => 'post'
                            ])
                    ?>
                    <input type="hidden" name="pids[]"/>
                    <ol class="k-shopping-list-items list-unstyled">
                        <?php foreach ($cartItems as $item) : ?>
                            <li class="items">
                                <?= $this->render('_cart_item', ['item' => $item]) ?>
                            </li>
                        <?php endforeach; ?>
                    </ol>
                    <?php ActiveForm::end() ?>
                </section>
                <section>
                    <div class="k-shopping-checkout">
                        <ul class="k-shopping-checkout-total-price list-unstyled">
                            <li>Học phí gốc</li>
                            <li class="price"><?= $formatter->asCurrency($totalCost) ?></li>
                            <li>Tổng cộng</li>
                            <li class="price-total"><?= $formatter->asCurrency($cart->getCost(true)) ?></li>
                        </ul>

                        <!--
                        <form class="form-inline">
                            <div class="form-group">
                              <input type="text" value="" name="" id="" placeholder="Nhập mã khuyến mãi" class="text form-control">
                            </div>
                            <button type="submit" class="btn btn-primary">Áp dụng</button>
                        </form>
                        -->

                        <ul class="k-shopping-checkout-button list-unstyled">
                            <li>
                                <a href="<?= Url::toRoute(['/course/default/index']) ?>"><i class="icon icon-caret-left"></i> Chọn thêm khóa học khác</a>
                            </li>
                            <li>
                                <a href="<?= Url::toRoute(['/cart/checkout/index']) ?>" class="btn-payment"><?= ($cart->isFreeOrder) ? 'Xác nhận đăng ký' : 'Tiếp tục chọn cách thanh toán' ?> <i class="icon icon-caret-right"></i></a>
                            </li>
                        </ul>
                    </div><!--end k-shopping-checkout-->
                </section>
            <?php else: ?>
                <div class="cart-empty clearfix">
                    <img src="<?= $cdnUrl ?>/img/cart/icon-checkout-empty-1.png" class="img-fluid"/>
                </div><!--end .cart-empty-->
                <div id="k-learn-what-today">
                    <h2 class="title">Bạn có thể tìm kiếm khóa học theo các chủ đề sau</h2>
                    <div class="row">
                        <div class="item">
                            <a href="#">
                                <img src="https://media.kyna.vn/uploads/courses/784/img/image_url-1501392449.cover-263x147.jpg" alt="">
                                <div class="text">
                                    <span>Làm sao uốn dẻo</span>
                                </div>
                            </a>
                        </div>
                        <div class="item">
                            <a href="#">
                                <img src="https://media.kyna.vn/uploads/courses/784/img/image_url-1501392449.cover-263x147.jpg" alt="">
                                <div class="text">
                                    <span>Làm sao uốn dẻo</span>
                                </div>
                            </a>
                        </div>
                        <div class="item">
                            <a href="#">
                                <img src="https://media.kyna.vn/uploads/courses/784/img/image_url-1501392449.cover-263x147.jpg" alt="">
                                <div class="text">
                                    <span>Làm sao uốn dẻo</span>
                                </div>
                            </a>
                        </div>
                        <div class="item">
                            <a href="#">
                                <img src="https://media.kyna.vn/uploads/courses/784/img/image_url-1501392449.cover-263x147.jpg" alt="">
                                <div class="text">
                                    <span>Làm sao uốn dẻo</span>
                                </div>
                            </a>
                        </div>
                        <div class="item">
                            <a href="#">
                                <img src="https://media.kyna.vn/uploads/courses/784/img/image_url-1501392449.cover-263x147.jpg" alt="">
                                <div class="text">
                                    <span>Làm sao uốn dẻo</span>
                                </div>
                            </a>
                        </div>
                        <div class="item">
                            <a href="#">
                                <img src="https://media.kyna.vn/uploads/courses/784/img/image_url-1501392449.cover-263x147.jpg" alt="">
                                <div class="text">
                                    <span>Làm sao uốn dẻo</span>
                                </div>
                            </a>
                        </div>
                        <div class="item">
                            <a href="#">
                                <img src="https://media.kyna.vn/uploads/courses/784/img/image_url-1501392449.cover-263x147.jpg" alt="">
                                <div class="text">
                                    <span>Làm sao uốn dẻo</span>
                                </div>
                            </a>
                        </div>
                        <div class="item">
                            <a href="#">
                                <img src="https://media.kyna.vn/uploads/courses/784/img/image_url-1501392449.cover-263x147.jpg" alt="">
                                <div class="text">
                                    <span>Làm sao uốn dẻo</span>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
        </div>

    </div><!--end #k-cart-->

</main>

<?php
$script = "
    $('document').ready(function() {
        $('body').on('click', '.cart-item-remove', function () {
            var pid = $(this).data('id');
            $('input[name=\'pids[]\']').val(pid);
            $('#cart-form').submit();
        });
    });
";
$this->registerJs($script, View::POS_END, 'cart-remove-js');
?>
