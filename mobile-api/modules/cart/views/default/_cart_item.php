<?php

use app\modules\cart\models\Product;
use common\helpers\CDNHelper;


$formatter = \Yii::$app->formatter;
$cdnUrl = CDNHelper::getMediaLink();
?>

<div class="col-md-8 col-sm-8 col-xs-12 k-shopping-list-items-title" data-id="$item->id" data-brand="<?= (!empty($item->teacher))? $item->teacher->profile->name : ''?>">
    <div class="col-sm-4 col-xs-12 row items-img">
        <a href="<?= $item->url ?>" title="<?= $item->name ?>">
            <?php if (!empty($item->image_url)) : ?>
                <?= CDNHelper::image($item->image_url, [
                    'alt' => $item->name,
                    'class' => 'img-fluid',
                    'size' => CDNHelper::IMG_SIZE_THUMBNAIL_SMALL,
                    'resizeMode' => 'cover',
                ]) ?>
            <?php else: ?>
                <img src="<?= $cdnUrl ?>/src/img/cart/default.jpg" alt="<?= $item->name ?>" class="img-fluid">
            <!--end .img -->
            <?php endif; ?>
        </a>
    </div>

    <div class="col-sm-8 col-xs-12 items-text">
        <h4>
            <a href="<?= $item->url ?>" title="<?= $item->name ?>"><?= $item->name ?></a>
        </h4>
        <?php if (!empty($item->teacher_id)) : ?>
        <p><?= "{$item->teacher->profile->name} / {$item->teacher->title}" ?></p>
        <?php endif ?>
        <?php if ($item->type == Product::TYPE_COMBO) : ?>
            <ul class="comboItem">
                <?php foreach ($item->comboItems as $comboItem) : ?>
                <li>
                    <div class="media">
                        <div class="media-left">
                            &#9679;
                        </div>
                        <div class="media-body">
                            <a href="<?= $comboItem->course->url ?>"><?= $comboItem->course->name ?></a>
                        </div>
                     </div>
                </li>
                <?php endforeach; ?>
            </ul>
        <?php elseif (false) : ?>
        <span class="cart-item-exist alert alert-warning">Khóa học này đã tồn tại trong combo: <strong><?= $combo ?></strong>.</span>
        <?php endif; ?>
        <a href="javascript:" data-id="<?= $item->getId() ?>" class="items-remove cart-item-remove">
        <i class="icon icon-trash-o"></i> Xóa khóa học</a>

    </div>
    <!--end .text-->
</div>
<!--end .title-->
<div class="col-md-2 col-sm-2 col-xs-12 k-shopping-list-items-number">
    <span>1</span>
    <input type="hidden" size="4" readonly="true" name="" id="" value="<?= $item->quantity ?>" class="item-quantity">
</div>
<!--end .number-->
<div class="col-md-2 col-sm-2 col-xs-6 k-shopping-list-items-price">
    <?php
    $cost = $item->getCost(false);

    $costWithDiscount = $item->getCost();
    ?>
    <?php if (empty($cost)) : ?>
        <span class="sale">Miễn phí</span>
    <?php elseif ($cost == $costWithDiscount) : ?>
        <span class="sale"><?= $formatter->asCurrency($cost) ?></span>
    <?php else: ?>
        <span class="sale-out"><del><?= $formatter->asCurrency($cost) ?></del></span>
        <span class="sale"><?= $formatter->asCurrency($costWithDiscount) ?></span>
    <?php endif; ?>
</div>
<!--end .price-->
