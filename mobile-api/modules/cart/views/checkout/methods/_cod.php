<?php

/* @var $model \app\modules\cart\models\form\PaymentForm */

?>

<div class="checkout-list" id="checkout-cod">
    <input id="radio-cod" type="radio" name="PaymentForm[method]" value="cod" <?php if ($model->method == 'cod') {echo 'checked';} ?>>
    <label for="radio-cod">
        <span><span></span></span><methodName><span><?= $methodName ?></span></methodName>
    </label>
    <div class="checkout-wrap-list">
        <div class="checkout-sub-list" id="checkout-show-cod">
            Bạn cần điền đủ các thông tin mua hàng bên dưới. Sau đó, Kyna.vn sẽ giao phong bì có chứa mã kích hoạt và thu tiền tận nơi.
            <br><br>
            <ul style="padding-left: 20px">
              <li style="display: list-item !important; list-style-type: disc;"><b>MIỄN PHÍ</b> đối với các đơn hàng có giá trị <b>từ 300.000 đồng trở lên</b> (áp dụng trên toàn quốc)</li>
              <li style="display: list-item !important; list-style-type: disc;">Đối với những đơn hàng <b>dưới 300.000</b>, Kyna sẽ áp dụng biểu giá của dịch vụ giao nhận như sau:<br>
                  - Phí giao nhận tại các quận ngoại thành Tp. Hồ Chí Minh, ngoại thành Hà Nội: 10.000VNĐ<br>
                  - Phí giao nhận tại khu vực nội thành các tỉnh khác: 20.000VNĐ<br>
                  - Phí giao nhận tại khu vực ngoại thành tỉnh khác: 30.000VNĐ
              </li>
            </ul>
        </div>
        <!--end .checkout-sub-list-->
    </div><!--end .checkout-wrap-list-->
</div><!--end .checkout-list-->
