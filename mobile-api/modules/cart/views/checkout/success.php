<?php
/**
 * @var $this View
 */

use yii\helpers\Url;
use yii\web\View;
use kyna\order\models\Order;

$this->title = "Đăng ký khóa học thành công";

if ($order->status == Order::ORDER_STATUS_IN_COMPLETE && in_array($order->payment_method, ['epay', 'ipay'])) {
    echo $this->render("./failure/_failure_header", ['order' => $order]);
    echo $this->render("./common/_order_header_summary", ['order' => $order]);
} else {
    echo $this->render("./success/_success_header", ['order' => $order]);

    echo $this->render("./common/_order_header_summary", ['order' => $order]);

    if (!empty($order->payment_method)) {
        echo $this->render("./success/_intro_{$order->payment_method}", ['order' => $order, 'settings' => $settings]);
    }
}

$isTrackingFb = Yii::$app->session->getFlash('need-tracking', false);
?>

<?php if (Yii::$app->session->hasFlash('need-tracking')) {
    echo $this->render('@app/modules/cart/views/layouts/checkout/tracking_script');
}?>

<?php if (in_array($order->payment_method, ['epay', 'ipay']) && $order->status == Order::ORDER_STATUS_IN_COMPLETE) { ?>
    <p class="checkout-confirm-failed-text">Cám ơn bạn đã đăng ký khóa học trên <span href="#" class="bold color-green">Kyna.vn</span>.
        Tuy nhiên, thanh toán của bạn qua thẻ điện thoại không thành công. Hoặc thẻ cào không đủ thanh toán cho đơn hàng
        này.</p>
    <p class="checkout-confirm-failed-text">Với bất kì thắc mắc nào, bạn có thể liên hệ qua hotline <span><a
                    href="tel:1900.6364.09" class="color-green bold">1900.6364.09</a></span> hoặc email đến <span><a
                    href="mailto:hotro@kyna.vn" class="color-green bold">hotro@kyna.vn</a></span>.</p>
<?php } else { ?>
    <div class="checkout-confirm-ul-bottom">
        <ul>
           <!-- <li><a href="<?/*= Url::toRoute(['/back-to-app/home']) */?>"><i class="fa fa-caret-left"></i> Chọn thêm khóa
                    học khác</a></li>-->

            <li>
                <a href="<?= Url::toRoute(['/back-to-app/mycourse']) ?>" style="width: 100%;
                    background: #fb6a00;
                    border: none;
                    padding: 15px 30px;
                    position: fixed;
                    bottom: 0;
                    left: 0;
                    margin-bottom: 0;
                    border-radius: 0;
                    font-size: 18px">
                    XEM KHÓA HỌC CỦA TÔI
                </a>
            </li>
        </ul>
    </div><!--end .checkout-confirm-ul-bottom-->
<?php } ?>

<?php
if ($messages = Yii::$app->session->getFlash('mission', false)) {
    $title = '<span class="label label-info" style="padding: 5px;">+' . $messages['point'] . ' KPOINT</span><br>';
    $message = 'Wohoo ~ Bạn vừa hoàn thành nhiệm vụ "' . $messages['missionName'] . '" <a href="#" class="btn-mission-tab" style="font-style: italic">» Nhiệm vụ</a>';

    $script = "
        $(document).ready(function () {
            $('body').on('click', 'a.btn-mission-tab', function () {
                $('#li-mission-tab a').trigger('click');
            });
            $.notify({
                title: '{$title}',
                message: '{$message}',
                allow_dismiss: true
            }, {
                type: 'notify-kpoint',
                animate: {
                    enter: 'animated bounceInDown',
                    exit: 'animated bounceOutUp'
                },
                placement: {
                    from: 'bottom',
                    align: 'right'
                }
            });
        });
    ";

    $this->registerJs($script, View::POS_END);
}
?>
