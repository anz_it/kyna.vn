<?php
/**
 * Created by PhpStorm.
 * User: macintosh
 * Date: 7/26/17
 * Time: 9:48 AM
 */
?>
<div class="info-checkout">
    <h3>THÔNG TIN MUA HÀNG</h3>
    <div class="clearfix">
        <div class="col-xs-12 col-sm-6">
            <?= $form->field($paymentForm, 'email', [
                'template' => "<fieldset" . (!Yii::$app->user->isGuest ? ' class="readonly"' : '') . "><legend>{label} <span>*</span></legend>\n{input}\n</fieldset>{error}",
                'enableClientValidation' => false
            ])
                ->textInput(['readonly' => Yii::$app->user->isGuest ? false : true])
                ->error([
                    'encode' => false
                ]) ?>
            <div class="note-email"></div>
        </div>
    </div>
    <div class="clearfix">
        <div class="col-xs-12 col-sm-6">
            <?= $form->field($paymentForm, 'contact_name')->textInput() ?>
        </div>
        <div class="col-xs-12 col-sm-6">
            <?= $form->field($paymentForm, 'phone_number')->textInput() ?>
        </div>
        <input type="hidden" name="uid" value="<?= Yii::$app->request->get('uid') ?>">
        <input type="hidden" name="sess_id" value="<?= Yii::$app->request->get('sess_id') ?>">
    </div>
    <div class="clearfix">
        <div class="col-xs-12 col-sm-6" visible-input="address" style="display: none">
            <?= $form->field($paymentForm, 'street_address')->textarea(['rows' => 2]) ?>
        </div>

        <div class="col-xs-12 col-sm-6" visible-input="note" style="display: none">
            <?= $form->field($paymentForm, 'note', ['template' => "<fieldset><legend>{label}</legend>\n{input}\n</fieldset>{error}"])->textarea(['id' => 'note', 'rows' => 2]) ?>
        </div>
    </div>
</div>
