<?php

use yii\web\View;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use common\helpers\CDNHelper;
use kyna\promotion\models\UserVoucherFree;

$cdnUrl = CDNHelper::getMediaLink();

/* @var $paymentForm \app\modules\cart\models\form\PaymentForm */
/* @var $this View */

$this->title = "Thanh toán";
$formatter = Yii::$app->formatter;
$cart = Yii::$app->cart;

$id = Yii::$app->request->get('orderId');
if ($order !== null) {
    $promotionCode = $order->promotion_code;
    $totalWithoutShipping = $order->total - $order->shippingAmount;
} else {
    $totalWithoutShipping = $cart->getCost(true);
}
if (empty($promotionCode)) {
    $promotionCode = !empty($cart->promotionCode) ? $cart->promotionCode : null;
}
$paymentForm->promotion_code = $promotionCode;

$mobileId = Yii::$app->request->get('mobile-id');
?>

<?php $form = ActiveForm::begin([
    'id' => "checkout-form",
    'action' => Url::toRoute([
        $mobileId != null ? '/cart/mobile-checkout/index' : '/cart/mobile-checkout/index',
        'orderId' => Yii::$app->request->get('orderId'),
        'mobile-id' => $mobileId,
        'token' => Yii::$app->request->get('token'),
        'signature' => Yii::$app->request->get('signature'),
    ]),
    'method' => 'POST',
    'options' => ['class' => 'clearfix'],
    'fieldConfig' => [
        'template' => "<fieldset><legend>{label} <span>*</span></legend>\n{input}\n</fieldset>{error}",
        'errorOptions' => [
            'encode' => false,
            'class' => 'help-block input-error'
        ],
    ]
]); ?>

    <?= Html::activeHiddenInput($paymentForm, 'promotion_code') ?>

    <?php if ($paymentForm->totalAmount > 0) : ?>
        <div class="checkout-list-check field-paymentform-method">
            <h3 style="margin-bottom: 0px; margin-top: 25px">CHỌN PHƯƠNG THỨC THANH TOÁN</h3>
            <div class="payment-method-error input-error" style="padding-left: 15px"></div>
            <div class="wrap clearfix">
                <?php
                $visible = false;
                foreach ($paymentMethods as $method) {
                    $methodView = "methods/_{$method->class}";
                    if ($method->class == 'epay' && (int)$paymentForm->totalAmount <= $method::LIMIT_PAYMENT) {
                        $visible = true;
                    }

                    if (is_file(dirname(__FILE__) . '/' . $methodView . '.php')) {
                        echo $this->render($methodView, [
                            'model' => $paymentForm,
                            'form' => $form,
                            'methodName' => $method->name,
                            'visible' => $visible,
                            'disable' => $paymentForm->totalAmount > 0 ? false : true,
                            'order' => $order,
                            'response' => !empty($response) ? $response : null,
                            'totalWithoutShipping' => $totalWithoutShipping
                        ]);
                    } elseif (!empty($method->content)) {
                        echo $method->content;
                    }
                }
                ?>
            </div><!--end .wrap-->
        </div><!--end .checkout-list-check-->

        <?= $this->render('_user_form', ['form' => $form, 'paymentForm' => $paymentForm]) ?>

        <?= $this->render('_form/_mobile_card', [
            'response' => isset($response) ? $response : null,
            'paymentForm' => $paymentForm,
            'order' => $order,
            'form' => $form
        ]); ?>
    <?php else: ?>
        <div class="checkout-free-content">
            <img src="<?= $cdnUrl ?>/img/checkout/icon-free-checkout.png" alt="Bạn được miễn phí <?= ($cart->getCount() > 1 ? 'các ' : '') ?>khóa học này" class="img-responsive">
            <h2>Bạn được miễn phí <?= ($cart->getCount() > 1? 'các ' : '') ?>khóa học này</h2>
            <p>Ngay sau khi hoàn tất, bạn có thể bắt đầu tham gia học trên Kyna.vn</p>
        </div><!--end .checkout-free-content-->

        <?= $this->render('_user_form', ['form' => $form, 'paymentForm' => $paymentForm]) ?>
        
        <?php
        $this->registerCss("
            .checkout-free-content {
                text-align: center;
            }
            .checkout-free-content img {
                margin: 30px auto;
            }
            .checkout-free-content h2 {
                color: #5db25b;
                font-size: 24px;
                font-weight: bold;
            }
        ");
        ?>
    <?php endif; ?>

    <?php if (Yii::$app->user->isGuest) : ?>
    <!-- Login section -->
    <div class="login-for-guest">
        <div class="box-login-for-guest">
            <div class="title">Nếu bạn đã từng mua khoá học và có tài khoản ở KYNA.VN, bạn có thể đăng nhập để không phải nhập lại thông tin cá nhân</div>
            <a class="btn btn-login-account-kyna" id="buttonLoginMobile">Đăng nhập Kyna</a>
            <span> </span>
        </div>
    </div>
    <?php endif; ?>

    <div class="wrap-checkout-button-pc">
        <div class="container">
            <div class="clearfix">
                <div class="item">
                    <span>Phương thức thanh toán</span><br>
                    <span>
                        <b for-id="method">
                            <?php
                            if (!empty($paymentForm->method)) {
                                $selectedMethods = array_filter($paymentMethods, function ($value) use ($paymentForm) {
                                    return $value->class == $paymentForm->method;
                                });
                                $selectedMethod = array_shift($selectedMethods);
                                if (!empty($selectedMethod)) {
                                    echo $selectedMethod->name;
                                }
                            }
                            ?>
                        </b>
                    </span>
                </div>
                <div class="item">
                    <span>Tổng số tiền thanh toán</span><br>
                    <span><b><?= Yii::$app->formatter->asCurrency($paymentForm->totalAmount) ?></b></span>
                </div>
                <div class="item">
                    <span>Email tài khoản</span><br>
                    <span><b for-id="email"><?= $paymentForm->email ?></b></span>
                </div>
                <div class="button">
                    <div class="tool-tip">Chọn phương thức thanh toán và điền các thông tin để "Hoàn tất đơn hàng"</div>
                    <button class="checkout-button"><b>HOÀN TẤT ĐƠN HÀNG</b></button>
                </div>
            </div>
        </div>
    </div>

<?php ActiveForm::end() ?>


<?php
if (!empty($paymentForm->method)) {
    // init selected payment method css
    $script = "
    $(document).ready(function () {
        $('input[name=\'PaymentForm[method]\']').each(function() {
            if ($(this).val() == '{$paymentForm->method}') {
                $(this).trigger('click');
            }
        });
    });
    ";
    $this->registerJs($script, View::POS_END);
}
if (Yii::$app->user->isGuest) {
    // init selected payment method css
    $script = "
    $('body').on('blur', '.field-paymentform-email', function () {
        var paymentMethod = $(\"input[name=\'PaymentForm[method]\']:radio:checked\").val();
        var email = $(this).find('input').val();
        $('.note-email').html('');
        var emailElement = $(this).parents('.form-group');

        $.ajax({
            url: '/mobile-checkout/validate-email',
            method: 'post',
            data: {
                email: email,
                paymentMethod: paymentMethod
            },
            success: function (res) {
              
                if (res.result) {
                                   

                    $('[for-id=\"email\"]').html(email);
                    $('.field-paymentform-email').removeClass('has-error').addClass('has-success');
                    $('.field-paymentform-email .help-block').html('');
                    $('.note-email').removeClass('input-error');
                    $('.note-email').show().html(res.message);
                    $('.field-paymentform-contact_name').parent().parent().show();
                } else {
                   
                    $('.field-paymentform-email').removeClass('has-success').addClass('has-error');
                    $('.field-paymentform-email .input-error').html(res.message);
                    if (res.errorCode !== undefined && res.errorCode == 2) {
                        $('.field-paymentform-contact_name').parent().parent().hide();
                    } else {
                        $('.field-paymentform-contact_name').parent().parent().show();
                    }
                }
            }
        });

        $('body').on('blur', '#paymentform-phone_number', function (e) {
            var phone = $(this).val();

            $.ajax({
                url: '/mobile-checkout/add-user-care',
                method: 'post',
                data: {
                    phone: phone,
                },
                success: function (res) {
                    if (res.result) {
                        // success
                    } else {
                        // error
                    }
                }
            });
        });
    });
    ";
    $this->registerJs($script, View::POS_END, 'validate-email');
}
?>

<?php $user_id = Yii::$app->user->getId(); if(!empty($user_id)): ?>
    <?php $userVoucherFree = UserVoucherFree::getUserVoucherFree(Yii::$app->user->getId()); ?>
    <?php if (UserVoucherFree::isDateRunCampaignVoucherFree() == true): ?>
        <!-- Modal Voucher-->
        <div class="modal fade" id="modalvoucher" tabindex="-1" role="dialog" aria-labelledby="modalVoucher"
             aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>

                    <div class="modal-body">
                        <img class="title" src="<?= $cdnUrl ?>/img/voucher-free/voucher-free-title.png">
                        <img class="title-sp" src="<?= $cdnUrl ?>/img/voucher-free/voucher-free-title-sp.png">
                        <div class="copy-voucher">
                            <p>Đây là mã của bạn</p>
                            <input id="showvoucher" value="<?php echo $userVoucherFree->getCode(); ?>" type="text">
                            <a href="#" onclick="Copyvoucher()">Sao chép</a>
                        </div>

                        <ul>
                            <li>Cứ giới thiệu thành công 03 người khác sử dụng mã này, bạn sẽ được tặng thêm 01 lượt sử
                                dụng mã VOUCHER FREE trong chương trình. Bạn có thể xem lại mã này trong trang Khóa học
                                của tôi.
                            </li>
                            <li>Việc bạn sử dụng mã VOUCHER FREE của mình hay của người khác thì cũng tính là 01 lượt sử
                                dụng mã VOUCHER FREE trong chương tình.
                            </li>
                            <li>Trong lượt sử dụng đầu tiên thì bạn không thể sử dụng mã VOUCHER FREE của chính mình mà
                                bạn phải sử dụng mã VOUCHER FREE của người khác.
                            </li>
                            <li>Đối với lượt được tặng thêm, bạn có quyền sử dụng mã của mình hoặc của người khác. Ghi
                                chú: không tính lượt giới thiệu đối với lượt tự sử dụng mã của mình.
                            </li>
                        </ul>
                        <a class="fb-share" href="https://www.facebook.com/sharer/sharer.php?u=https://kyna.vn/p/campaign/voucher-free">Chia sẻ Facebook</a>
                        <div class="lp-voucher">
                            <a class="lp-link" href="https://kyna.vn/p/campaign/voucher-free">>> Xem giới thiệu chương
                                trình <<</a>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <p>Bạn đã giới thiệu được: <br><span><?php echo $userVoucherFree->getTotalIntroduce(); ?></span>
                            lượt dùng mã</p>
                        <p>Được tặng thêm: <span>0<?php echo $userVoucherFree->getTotalReceived(); ?></span> lượt
                            <br>(đã sử dụng <span>0<?php echo $userVoucherFree->getTotalConsume(); ?></span> lượt)</p>
                    </div>
                </div>
            </div>
        </div>
    <?php endif; ?>
<?php endif; ?>
<script>
    function Copyvoucher() {
        var copyText = document.getElementById("showvoucher");
        copyText.select();
        document.execCommand("copy");

    }
</script>
<style>
    #modalvoucher .modal-body {
        background-image: url("<?= $cdnUrl ?>/img/voucher-free/bg-popup-voucher-free.png");
        background-position: top;
        background-size: cover;
        background-repeat: no-repeat;
        text-align: center;
        padding: 20px 40px;
        color: #ffffff;

    }

    #modalvoucher .modal-body .title {
        margin-bottom: 15px;

    }

    #modalvoucher .modal-body .title-sp {
        display: none;

    }

    #modalvoucher .modal-body .copy-voucher p {

        padding-bottom: 10px;
        font-weight: 700;
        font-size: 15px;
        margin-bottom: 0;

    }

    #modalvoucher .modal-body .copy-voucher input {
        width: 182px;
        height: 35px;
        padding: 5px 10px;
        font-weight: 700;
        color: #000000;
        font-size: 15px;
        border: none;
        outline: none;
        border-radius: 5px 0 0 5px;
    }

    #modalvoucher .modal-body .copy-voucher a {
        background-color: #ffe800;
        color: #000000;
        font-size: 12px;
        font-weight: 700;
        padding: 12px 12px 9px;
        position: relative;
        left: -4px;
        border-radius: 0 5px 5px 0;
    }

    #modalvoucher .modal-body ul {
        text-align: left;
        padding: 15px 0 0 0;
        font-size: 14px;
        line-height: 1.4;
        margin-bottom: 20px;

    }

    #modalvoucher .modal-body ul li {
        padding-bottom: 8px;
        position: relative;
        text-align: justify;
        display: block;
        vertical-align: initial;

    }

    #modalvoucher .modal-body ul li:before {
        content: '\f0da';
        font-family: FontAwesome;
        position: absolute;
        left: -10px;

    }

    #modalvoucher .modal-body .fb-share {
        border: 1px solid #ffffff;
        padding: 10px 20px;
        font-size: 15px;
        font-weight: 700;
        border-radius: 30px;
        color: #ffffff;
        margin-bottom: 15px;
    }

    #modalvoucher .modal-body .fb-share:before {
        content: '\f09a';
        font-family: FontAwesome;
        margin-right: 8px;
    }

    #modalvoucher .modal-body .lp-voucher {
        margin: 20px 0 0 0;
    }

    #modalvoucher .modal-body .lp-voucher .lp-link {
        border-bottom: 1px solid #ffffff;
        font-weight: 700;
        font-size: 15px;
        color: #ffffff;
    }

    #modalvoucher button {
        position: absolute;
        right: -20px;
        top: -20px;
        padding: 0px 10px;
        border-radius: 50%;
        background: white;
        z-index: 1;
        opacity: 1;
    }

    #modalvoucher button span {
        font-size: 40px;
        font-weight: 400;
        color: grey;
    }

    #modalvoucher .modal-footer {
        padding: 18px;
        text-align: center;
        background-color: #dd4f1b;
        border-top: none;
        color: #ffffff;

    }

    #modalvoucher .modal-footer p {
        font-size: 15px;
        line-height: 1.4;
        margin-bottom: 0;
    }

    #modalvoucher .modal-footer p br {
        display: none;
    }

    #modalvoucher .modal-footer p span {
        font-weight: 700;
    }

    @media screen and (max-width: 425px) {
        #modalvoucher .modal-body .title-sp {
            display: block;
            margin: 0 auto;
            margin-bottom: 15px;

        }

        #modalvoucher .modal-body .title {
            display: none;

        }

        #modalvoucher .modal-body {
            padding: 20px;
        }

        #modalvoucher .modal-body .copy-voucher input {
            width: 154px;
        }

        #modalvoucher .modal-footer p br {
            display: block;
        }
    }
</style>