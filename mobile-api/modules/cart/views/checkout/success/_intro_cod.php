<?php
use common\helpers\CDNHelper;

$cdnUrl = CDNHelper::getMediaLink();
?>
<div class="checkout-confirm-content">
    <p class="checkout-confirm-succ-text">Cám ơn bạn đã đăng ký khóa học trên <span class="color-green bold">Kyna.vn</span>.</p>

    <div class="checkout-confirm-part-1 box clearfix">
        <div class="col-lg-3 col-sm-4 col-xs-12 img pd0">
            <img src="<?= $cdnUrl ?>/img/icon-checkout-confirm-1.png" alt="Kyna.vn" class="img-fluid"/>
        </div><!--end .col-lg-3 col-sm-4 col-xs-12 img -->
        <div class="col-lg-9 col-sm-8 col-xs-12 text">
            <h4>Bước 1</h4>
            <p>Bạn sẽ nhận email xác nhận đơn hàng và hướng dẫn bạn học trên <span class="color-green bold">Kyna.vn</span> thông qua email <span class="bold"><?= $order->user->email ?></span>.</p>
        </div><!--end .col-lg-9 col-sm-8 col-xs-12 text-->
    </div><!--end .checkout-confirm-part-1-->

    <div class="checkout-confirm-part-2 box clearfix">
        <div class="col-lg-3 col-sm-4 col-xs-12 img pd0">
            <img src="<?= $cdnUrl ?>/img/icon-checkout-confirm-2.png" alt="Kyna.vn" class="img-fluid"/>
        </div><!--end .col-lg-3 col-sm-4 col-xs-12 img -->
        <div class="col-lg-9 col-sm-8 col-xs-12 text">
            <h4>Bước 2</h4>
            <p>Nhân viên chăm sóc của  <span class="color-green bold">Kyna.vn</span> sẽ gọi đến số <strong><?php echo (!empty($order->user->userAddress) ? $order->user->userAddress->phone_number : '') ?></strong> để xác nhận đơn hàng và địa chỉ giao hàng.</p>
        </div><!--end .col-lg-9 col-sm-8 col-xs-12 text-->
    </div><!--end .checkout-confirm-part-2-->

    <div class="checkout-confirm-part-3 box clearfix">
        <div class="col-lg-3 col-sm-4 col-xs-12 img pd0">
            <img src="<?= $cdnUrl ?>/img/icon-checkout-confirm-3.png" alt="Kyna.vn" class="img-fluid"/>
        </div><!--end .col-lg-3 col-sm-4 col-xs-12 img -->
        <div class="col-lg-9 col-sm-8 col-xs-12 text">
            <h4>Bước 3</h4>
            <p>Nhân viên vận chuyển giao mã kích hoạt để địa chỉ giao hàng bạn đã đăng ký.</p>
        </div><!--end .col-lg-9 col-sm-8 col-xs-12 text-->
    </div><!--end .checkout-confirm-part-3-->

    <div class="checkout-confirm-part-4 box clearfix">
        <div class="col-lg-3 col-sm-4 col-xs-12 img pd0">
            <img src="<?= $cdnUrl ?>/img/icon-checkout-confirm-4.png" alt="Kyna.vn" class="img-fluid"/>
        </div><!--end .col-lg-3 col-sm-4 col-xs-12 img -->
        <div class="col-lg-9 col-sm-8 col-xs-12 text">
            <h4>Bước 4</h4>
            <p>Bạn đăng nhập <span class="color-green bold">Kyna.vn</span> với tài khoản đã đăng ký. Sau đó vào mục <strong>Kích hoạt mã</strong> để nhập mã đã được: giao đến trước đó.</p>
        </div><!--end .col-lg-9 col-sm-8 col-xs-12 text-->
    </div><!--end .checkout-confirm-part-4-->

    <div class="checkout-confirm-part-5 box clearfix">
        <div class="col-lg-3 col-sm-4 col-xs-12 img pd0">
            <img src="<?= $cdnUrl ?>/img/icon-checkout-confirm-5.png" alt="Kyna.vn" class="img-fluid"/>
        </div><!--end .col-lg-3 col-sm-4 col-xs-12 img -->
        <div class="col-lg-9 col-sm-8 col-xs-12 text">
            <h4>Bước 5</h4>
            <p>Để bắt đầu học, bạn vào mục <strong>Khóa học của tôi</strong> và click chọn nút <strong>Bắt đầu học</strong> ở khóa học muốn tham gia.</p>
        </div><!--end .col-lg-9 col-sm-8 col-xs-12 text-->
    </div><!--end .checkout-confirm-part-5-->
    <?php
    $supportMail = (!empty($settings['email_footer']) ? $settings['email_footer'] : 'hotro@kyna.vn');
    $supportPhone = (!empty($settings['hot_line']) ? $settings['hot_line'] : '1900.6364.09');
    ?>
    <p class="checkout-confirm-succ-text">
        Với bất kì thắc mắc nào, bạn có thể liên hệ qua hotline 
        <span class="color-green bold"><a href="tel:<?= $supportPhone ?>" class="color-green bold"><?= $supportPhone ?></span> 
        hoặc email đến 
        <span class="color-green bold"><a href="mailto:<?= $supportMail ?>" class="color-green bold"><?= $supportMail ?></a>.</span>
    </p>
</div><!--end .checkout-confirm-content-->