<?php

$address = $order->user->userAddress;
?>

<div class="checkout-banner-confirm">
    <ul>
        <li>
            <p><span class="bold">Thông tin người mua</span></p>
            <p><?= $order->user->profile->name ?></p>
            <p><?= $order->user->email ?></p>
            <?php if (!empty($address)) : ?>
                <p><?= $address->phone_number ?></p>
            <?php endif ?>

        </li>
        <li>
            <p><span class="bold">Phương thức thanh toán</span></p>
            <p><?= $order->paymentMethodName ?></p>                                
            <p><span class="bold">Số tiền thanh toán</span></p>
            <p><?= \Yii::$app->formatter->asCurrency($order->realPayment) ?></p>
            <?php if ($order->promotion_code) { ?>
                <p><span class="bold">Mã khuyến mãi</span></p>
                <p><?= $order->promotion_code ?></p>
            <?php } ?>

        </li>
    </ul>
</div><!--end .checkout-banner-confirm-->