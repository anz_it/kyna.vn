<?php

use yii\web\View;
use yii\helpers\Url;
use mobile\assets\AppAsset;
use common\widgets\Alert;
use common\helpers\CDNHelper;

$cdnUrl = CDNHelper::getMediaLink();

AppAsset::register($this);

$this->beginPage();
$app = Yii::$app;
?>
<!DOCTYPE HTML>
<html lang="<?= $app->language ?>">
<head>
    <?= $this->render('@mobile/views/layouts/common/html_head') ?>
    <link rel="stylesheet" href="<?= $cdnUrl ?>/css/checkout.css?v=1502187741">
</head>

<body>
    <div class="loading-checkout">

    </div>
    <?php $this->beginBody() ?>

    <?= $this->render('checkout/tracking_script')?>

    <div class="<?= $this->context->mainDivClass ?>" id="<?= $this->context->mainDivId ?>">
        <div class="wrap-content">
            <div class="container">
                <ul class="clearfix wrap-main">
                    <li class="col-xs-12 wrap-content-left col" style="padding-bottom: 60px!important;">

                        <?= \common\widgets\NotifyAlert::widget() ?>

                        <div class="checkout-wrap-content">
                            <?= $content ?>
                        </div>

                        <?php
                        if (in_array($this->context->action->id, ['success', 'failure'])) {
                            echo $this->render('./checkout/order_details');
                        } else {
                            echo $this->render('./checkout/cart');
                        }
                        ?>
                    </li>

                    <li class="col-lg-7 col-xs-12 wrap-checkout-button-mb col" style="padding: 0;height: auto">
                        <div class="wrap-checkout-button" style="display: flex; justify-content: center">
                            <!--<a class="checkout-button back-to-cart" href="<?/*= Url::toRoute(['/cart/default/index']) */?>"><i class="fa fa-long-arrow-left" aria-hidden="true"></i> Quay lại</a>-->
                            <button class="checkout-button-mb" style="border-radius: 0; margin-bottom: 0; width: 100%; height: 54px">HOÀN TẤT ĐƠN HÀNG</button>
                        </div>
                    </li>
                </ul>
            </div>
        </div><!--end .wrap-content-->
    </div>
    <!-- POPUP REGISTER -->
    <div class="modal fade k-popup-account" id="k-popup-account-register" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">

    </div>
    <!-- END POPUP REGISTER -->

    <div class="modal fade k-popup-account" id="k-popup-account-login" tabindex="-1" role="dialog">
    </div>

    <div class="modal fade k-popup-account" id="k-popup-account-reset" tabindex="-1" role="dialog">
    </div>

    <?php $this->endBody() ?>

    <?php $this->registerJsFile($cdnUrl.'/js/script-checkout.js?v=15018342321', ['position' => View::POS_END]) ?>
</body>
</html>
<?php $this->endPage() ?>
