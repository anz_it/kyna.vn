<?php
/**
 * Created by PhpStorm.
 * User: macintosh
 * Date: 7/19/17
 * Time: 10:24 AM
 */

use common\helpers\CDNHelper;

$cdnUrl = CDNHelper::getMediaLink();

?>

<!-- Google Tag Manager -->
<script type="text/javascript" src="<?= $cdnUrl ?>/src/js/gtm.js?v=1508125121"></script>
<script type="text/javascript" src="<?= $cdnUrl ?>/src/js/fb-pixel.js?v=1508381550"></script>
<script type="text/javascript">
    $( window ).load(function() {
        window.dataLayer = window.dataLayer || [];
        var gtmUA = new GTMUA();
        (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-TVZVN3');
        <!-- Facebook Pixel Code -->
        setTimeout(function(){
            var script = document.createElement("script");
            script.setAttribute("type", "text/javascript");
            script.innerHTML = "var fbPixel = new FBPIXEL()";
            document.body.appendChild(script);
        }, 1000);
        <!-- End Facebook Pixel Code -->
    });
</script>
<!-- End Google Tag Manager -->

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TVZVN3"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- Piwik -->
<script type="text/javascript">
    var _paq = _paq || [];
    // tracker methods like "setCustomDimension" should be called before "trackPageView"
    _paq.push(["setDocumentTitle", document.domain + "/" + document.title]);
    _paq.push(["setCookieDomain", "*.kyna.vn"]);
    _paq.push(["setDomains", ["*.kyna.vn"]]);
    _paq.push(["disableCookies"]);
    //NATIVE_CODE//
    _paq.push(['trackPageView']);
    _paq.push(['enableLinkTracking']);
    _paq.push(['enableHeartBeatTimer']);
    (function() {
        var u="//track.kyna.vn/";
        _paq.push(['setTrackerUrl', u+'piwik.php']);
        _paq.push(['setSiteId', '1']);
        var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
        g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'piwik.js'; s.parentNode.insertBefore(g,s);
    })();
</script>
<noscript><p><img src="//track.kyna.vn/piwik.php?idsite=1&rec=1" style="border:0;" alt="" /></p></noscript>
<!-- End Piwik Code -->
