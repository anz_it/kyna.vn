<?php

use yii\helpers\Url;
use common\widgets\Alert;
use common\helpers\CDNHelper;
use \mobile\models\Course;
$cdnUrl = CDNHelper::getMediaLink();

$formatter = Yii::$app->formatter;
$order = $this->context->order;
$orderDetails = $order->details;

$groupComboDetails = [];
foreach ($orderDetails as $item) {
    if (!empty($item->course_combo_id)) {
        $groupComboDetails[$item->course_combo_id][] = $item;
    } else {
        $groupComboDetails[] = $item;
    }
}
?>

<div class="wrap-content-right">
    <div class="fixed-flag"></div>
    <div class="wrap">
        <h4>
            <img src="<?= $cdnUrl ?>/img/icon-mini-cart.png" alt="">
            <b><span class="color-green"><?= count($orderDetails) ?> khóa học</span></b>
        </h4>
        <ul class="list">
            <?php foreach ($groupComboDetails as $key => $item) : ?>
                <?php
                if (is_array($item)) {
                    $course_combo = Course::findOne(['id' => $key]);
                    $firstItem = $item[0];
                    $meta = json_decode($firstItem->course_meta);
                    $name = !empty($meta->combo_name) ? $meta->combo_name : $meta->name;
                    $price = $course_combo->oldPrice - $course_combo->getDiscountAmount();
                    $slug = $course_combo->slug;
                    $courseId = $key;
                    $type = \kyna\course\models\Course::TYPE_COMBO;

                    /*if (!empty(Yii::$app->params['discount_combo']) && $promotionCode == Yii::$app->params['discount_combo']) {
                        $promotion = Promotion::findOne(['code' => $promotionCode]);
                        $discountPrice = $promotion->getDiscountPrice();
                        $totalDiscount -= $discountPrice;
                        $campaignDiscount += $discountPrice;
                    }*/
                } else {
                    $meta = json_decode($item->course_meta);
                    $name = $meta->name;
                    $subPrice = $item->unit_price;
                    $discount = $item->discount_amount;
                    $price = $item->unit_price - $item->course->discountAmount;
                    $slug = $meta->slug;
                    $courseId = $item->course_id;
                    $type = \kyna\course\models\Course::TYPE_VIDEO;
                }
                $courseDetailUrl = Url::toRoute(['/course/default/view', 'id' => $courseId, 'slug' => $slug]);
                ?>

                <li class="clearfix" data-id="<?= $courseId ?>" data-course-type="<?= $type ?>">
                    <div class="col-sm-9 col-xs-8 title pd0 text">
                        <h6><?= $name ?><a class="product-hidden-link" href="<?= $courseDetailUrl ?>" style="display: none"><?= $name ?></a></h6>
                        <span class="product-hidden-price price" style="display: none;"><?php if (!empty($price)) {echo $formatter->asCurrency($price);} else {echo 'Miễn phí';}?></span>
                    </div><!--end .col-xs-2 col-xs-4 name-->
                    <div class="col-sm-3 col-xs-4 price pd0">
                        <b><span class="">
                            <?php
                            if (!empty($price)) {
                                echo $formatter->asCurrency($price);
                            } else {
                                echo 'Miễn phí';
                            }
                            ?>
                        </span></b>
                    </div><!--end .col-md-10 col-xs-8 price-->
                </li>
            <?php endforeach; ?>
        </ul>
        <div class="checkout-list-price">
            <ul>
                <li>
                    <span>Học phí gốc</span>
                    <span class="price total-cost" data-price="<?= $order->subTotal ?>"><b><?= $formatter->asCurrency($order->subTotal) ?></b></span>
                </li>
                <li>
                    <span>Giảm giá</span>
                    <span class="price"><b> <?= $order->totalDiscount > 0 ? '-' : '' ?> <?= $formatter->asCurrency($order->totalDiscount) ?></b></span>
                </li>
                <li>
                    <span class="color-orange"><b>THÀNH TIỀN</b></span>
                    <span class="price total-price color-orange"><b><?= $formatter->asCurrency($order->realPayment) ?></b></span>
                </li>
            </ul>
            <div class="note-cod"><b>Lưu ý:</b> <i>Chưa bao gồm phí vận chuyển</i></div>
        </div><!--end .checkout-list-price-->
        <?php
        Alert::widget();
        ?>
    </div><!--end .wrap-->
    <div class="re-money">
        <img src="<?= $cdnUrl ?>/img/checkout/hoan-tien.png" alt="">
        <p>Hoàn học phí trong vòng 30 ngày nếu không hài lòng về khóa học</p>
    </div>
</div><!--end .col-md-5 col-xs-12 wrap-content-right pd0-mb-->
