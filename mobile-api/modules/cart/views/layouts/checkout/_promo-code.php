<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\web\View;
use common\helpers\CDNHelper;

$cdnUrl = CDNHelper::getMediaLink();

$promoCode = new mobile\modules\cart\models\form\PromoCodeForm();
$promoCode->code = $promotionCode;
?>

<div class="checkout-apdung clearfix">

    <p>Mã khuyến mãi (nhập mã và click Áp dụng)</p>
    <?php $form = ActiveForm::begin([
        'id' => 'promo-code-form',
        //'enableAjaxValidation' => true,
        'enableClientValidation' => false,
        'action' => Url::to('/cart/promo/apply'),
        'options' => [
            'class' => 'clearfix'
        ]
    ]) ?>
    <?php if (!empty($order_id)): ?>
        <?= $form->field($promoCode, 'order_id')->hiddenInput(['value' => $order_id])->label(false) ?>
    <?php endif; ?>

    <?= $form->field($promoCode, 'code', [
        'options' => ['class' => 'form-group input-code'],
        'enableAjaxValidation' => false
    ])->textInput([
        'placeholder' => 'Nhập mã khuyến mãi',
        'class' => 'text form-control',
        'required' => true,
        // 'readonly' => !empty($promotionCode) ? true : false,
    ])->label(false) ?>

    <div class="button-submit">
        <?= Html::submitButton('Áp dụng', [
            'class' => 'btn-send-apdung' . (!empty($promotionCode) ? '' : ''),
            'onclick' => !empty($promotionCode) ? ' return true;' : 'return true;'
        ]) ?>
    </div><!--end .button-popup-->
    <div class="clearfix"></div>

    <div class="error-label">

    </div>
    <?php if(\kyna\promotion\models\UserVoucherFree::isDateRunCampaignVoucherFree() == true): ?>
        <div class="voucher-free-link">
            <ul>
                <?php $userId = Yii::$app->user->isGuest ? "" : Yii::$app->user->id ;?>
                <?php if($userId) :?>
                    <li><a href="#" data-toggle="modal" data-target="#modalvoucher">Xem mã của tôi</a></li>
                <?php endif;?>
                <li><a href="https://kyna.vn/p/campaign/voucher-free">Xem thông tin chương trình</a></li>
            </ul>
        </div>
    <?php endif;?>
    <?php ActiveForm::end() ?>
    <div class="hint-kyna-code">
        <img src="<?= $cdnUrl ?>/img/checkout/icon-code-voucher-coupon.png" alt=""> <span>Nhập mã <b class="green">ABCDSD</b>. Giảm ngay abcccc</span>
    </div>
</div><!--end checkout-apdung-->

<script type="text/javascript">


</script>
<?php
$userId = Yii::$app->user->isGuest ? "" : Yii::$app->user->id ;
$script = "
    ;(function($, window, document, undefined){
        $(document).ready(function () {
            $('body').on('beforeSubmit', 'form#promo-code-form', function (e) {
                e.preventDefault();
                var form = $(this);
                // return false if form still have some validation errors
                if (form.find('.has-error').length) {
                    return false;
                }
                // submit form
                $.ajax({
                    url: form.attr('action'),
                    type: 'post',
                    data: form.serialize(),
                    success: function (response) {
                        var obj = JSON.parse(response);
                        $('.error-label').empty();
                        var code = $('#promocodeform-code').val();

                        if (parseInt(obj.error_code) == 0) {
                            window.location.reload();
                        } else {
                            window.location.reload();
                           // $('.error-label').append(\"<p class='error-mess'>* \" + obj.object.code + \"</p>\");
                          
                        }
                        return false;
                    }
                });
                return false;
            });
            //hint-kyna-code
            var code = localStorage.getItem('kyna-code-{$userId}');
            var title = localStorage.getItem('kyna-title-code-{$userId}');
            if (code !== '' && code !== undefined && code !== null)
                $('.hint-kyna-code').show().html('<img src=\"".$cdnUrl."/img/checkout/icon-code-voucher-coupon.png\" alt=\"\"> Nhập mã <b class=\"green\">' + code + '</b>. ' + title);
            else
                $('.hint-kyna-code').hide();
        });
    })(window.jQuery || window.Zepto, window, document);
";
$css = "
        .label-danger {
            white-space: normal;
        }
    ";
$this->registerCss($css);
$this->registerJs($script, View::POS_END, 'promotion_submit');
?>
