<?php

use yii\helpers\Url;
use common\widgets\Alert;
use common\helpers\CDNHelper;
use mobile\widgets\BannerWidget;
use kyna\settings\models\Banner;
use kyna\promotion\models\Promotion;
use kyna\course\models\Course;
use kyna\promo\models\GroupDiscount;
use common\campaign\CampaignTet;
$cdnUrl = CDNHelper::getMediaLink();

$formatter = \Yii::$app->formatter;
$cart = \Yii::$app->cart;
$order = null;


$historyOrderID = null;

$groupDiscount = GroupDiscount::applyGroupDiscountCartItems($cart->getPositions());
$currentGroupDiscount = $groupDiscount['currentGroupDiscount'];
$discountGroupValue = $groupDiscount['totalGroupDiscount'];

$promotionCode = !empty($cart->promotionCode) ? $cart->promotionCode : null;
$subTotal = $cart->getCost();
$total = $cart->getTotalPriceAfterDiscountAll();
$discount = $cart->getTotalDiscountAmount(); //tổng khuyển mãi bao gồm vourcher

$details = $cart->getPositions();


// get combo discount for campaign, only for coupon
$campaignDiscount = 0;
if (!empty(Yii::$app->params['discount_combo']) && $promotionCode == Yii::$app->params['discount_combo']) {
    $promotion = Promotion::findOne(['code' => $promotionCode]);
    $discountPrice = $promotion->getDiscountPrice();
    if ($discountPrice > 0) {
        foreach ($details as $detail) {
            if (!empty($historyOrderID)) {
                $course = $detail->course;
            } else {
                $course = $detail;
            }
            $isCombo = $course->type == Course::TYPE_COMBO;
            if ($isCombo) {
                $discount -= $discountPrice;
                $campaignDiscount += $discountPrice;
            }
        }
    }
}
?>

<div class="wrap-content-right">
    <div class="fixed-flag"></div>
    <div class="wrap">
        <h4>
            <img src="<?= $cdnUrl ?>/img/icon-mini-cart.png" alt="">
            <b><span class="color-green"><?= count($details) ?> khóa học</span></b>
            <a href="<?= Url::to(['/cart/default/index']) ?>">Thay đổi</a>
        </h4>
        <ul class="list">
            <?php $posistions = $details; ?>
            <?php foreach ($posistions as $id => $position) : ?>
                <?php
                    $course = $position;
                    $cost= $position->getCost();
                    $courseDetailUrl = Url::toRoute(['/course/default/view', 'id' => $course->id, 'slug' => $course->slug]);
                ?>
                <li class="clearfix" data-id="<?= $course->id ?>" data-course-type="<?= $course->type?>">
                    <div class="col-sm-9 col-xs-8 title pd0 text">
                        <h6><?= $course->name ?><a class="product-hidden-link" href="<?= $courseDetailUrl ?>" style="display: none"><?= $course->name ?></a></h6>
                        <span class="product-hidden-price price" style="display: none;"><?= $position->getCostText() ?></span>
                    </div><!--end .col-xs-2 col-xs-4 name-->
                    <div class="col-sm-3 col-xs-4 price pd0">
                        <span><b>
                            <?= $position->getCostText() ?>
                        </b></span>
                    </div><!--end .col-md-10 col-xs-8 price-->
                </li>
            <?php endforeach; ?>
        </ul>
        <?= $this->render('_promo-code', ['promotionCode' => $promotionCode, 'order_id' => $historyOrderID]); ?>
        <div class="checkout-list-price">
            <ul>
                <li>
                    <span>Học phí gốc</span>
                    <span class="price total-cost" data-price="<?= $subTotal ?>"><b><?= $formatter->asCurrency($subTotal) ?></b></span>
                </li>

                <?php if(Yii::$app->params['campaign_birthday']) :
                    $totalPriceForBithday = GroupDiscount::getTotalPriceBirthday($posistions);?>
                    <li>
                        <span>Tổng giá campaign sinh nhật</span>
                        <span class="price"><b> <?= $formatter->asCurrency(GroupDiscount::getTotalPriceBirthday($posistions))?></b></span>

                    </li>
                <?php endif;?>
                <?php
                if(!empty($promotionCode)):?>
                <?php
                    $app_prefix = substr( $promotionCode, 0, 4 );
                    if(strtolower($app_prefix) == "app_"):
                    ?>
                        <li>
                            <span>Giảm giá khi mua trên App</span>
                            <span class="price"><b> - <?= $formatter->asCurrency(100000) ?></b></span>
                        </li>
                    <?php endif;?>
                <?php endif;?>
                <?php
                $totalPrice = 0;
                if(CampaignTet::InTimesCampaign()){
                    $cartItems = array_reverse($details);
                    foreach ($cartItems as $item)
                    {
                        /* @var $item app\modules\cart\models\Product */
                        if(!in_array($item->getId(),CampaignTet::COURSE_NOT_APPLY_VOUCHER)){
                            $totalPrice += $item->getPrice();
                        }
                    }
                }
                ?>


                <?php if(!CampaignTet::InTimesCampaign()):?>
                    <li>
                        <span>Tổng giảm giá</span>
                        <span class="price"><b> <?= $discount > 0 ? '-' : '' ?> <?= $formatter->asCurrency($discount) ?></b></span>
                    </li>
                <?php else: ?>

                    <li>
                        <?php $giamLixi = $totalPrice* CampaignTet::percentLiXiTet($totalPrice)/100?>
                        <span>Tổng giảm giá</span>
                        <span class="price"><b> <?= $discount > 0 ? '-' : '' ?> <?= $formatter->asCurrency($discount-$giamLixi) ?></b></span>
                    </li>
                <?php endif;?>
                <?php
                if(!empty($currentGroupDiscount)):
                    ?>
                    <li>
                        <span>Mua <?= $currentGroupDiscount->course_quantity ?> giảm <?= $currentGroupDiscount->percent_discount ?> %</span>
                        <span class="price"><b> <?= $discountGroupValue > 0 ? '-' : '' ?> <?= $formatter->asCurrency($discountGroupValue) ?></b></span>
                    </li>
                <?php
                endif;
                ?>

                <?php if (!empty(Yii::$app->params['discount_combo']) && $campaignDiscount > 0): ?>
                    <li>
                        <span>Mã <?= Yii::$app->params['discount_combo'] ?></span>
                        <span class="price"><b> <?= $campaignDiscount > 0 ? '-' : '' ?> <?= $formatter->asCurrency($campaignDiscount) ?></b></span>
                    </li>
                <?php endif; ?>
                <?php $promotionCode = Yii::$app->cart->getPromotionCode();
                ?>
                <?php if(CampaignTet::InTimesCampaign() && CampaignTet::isVoucherLixTet($promotionCode)):?>
                    <li>
                        <span>Giảm giá Lì Xì Hot(-<?= CampaignTet::percentLiXiTet($totalPrice)?>%)</span>
                        <?php $giamLixi = $totalPrice* CampaignTet::percentLiXiTet($totalPrice)/100?>
                        <span class="price"><?= $formatter->asCurrency($giamLixi) ?></span>
                    </li>
                <?php endif;?>
                <li>
                    <span class="color-orange"><b>THÀNH TIỀN</b></span>
                    <span class="price total-price color-orange"><b><?= $formatter->asCurrency($total) ?></b></span>
                </li>
            </ul>
            <div class="note-cod"><b>Lưu ý:</b> <i>Chưa bao gồm phí vận chuyển</i></div>
        </div><!--end .checkout-list-price-->
        <?php
        Alert::widget();
        ?>
    </div><!--end .wrap-->

    <div class="re-money">
        <?= BannerWidget::widget([
            'type' => Banner::TYPE_CHECKOUT_PAGE,
            'id' => 'promotion',
            'containerClass' => 'promotion'
        ]) ?>
        <img src="<?= $cdnUrl ?>/img/checkout/hoan-tien.png" alt="">
        <p>Hoàn học phí trong vòng 30 ngày nếu không hài lòng về khóa học</p>
    </div>
</div><!--end .col-md-5 col-xs-12 wrap-content-right pd0-mb-->
