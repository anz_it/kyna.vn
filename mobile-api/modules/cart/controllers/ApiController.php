<?php

namespace mobile\modules\cart\controllers;


use mobile\helpers\SecurityHelper;
use Yii;

use kyna\payment\models\PaymentMethod;
use mobile\modules\cart\components\Controller;
use kyna\order\models\Order;

/* 
 * Class ApiController to handle all ipn requests.
 */
class ApiController extends Controller
{
    
    /**
     * @desc action for RedirectUrl
     * @param redirect
     */
    public function actionResponse($method)
    {
        $paymentMethod = PaymentMethod::loadModel($method);
        $get = Yii::$app->request->get();
        
        $result = $paymentMethod->ipn($get);

        if(!empty(Yii::$app->user->id))
            $user_id = Yii::$app->user->id;
        $userInfo = Yii::$app->cart->getUserInfo();
        if(!empty($userInfo['user_id']) && empty($user_id))
            $user_id = $userInfo['user_id'];
         if(empty($user_id)){
             $order = Order::find()->andWhere(['id'=>$result['orderId']])->one();
             if(!empty($order))
             {
                 $user_id = $order->user_id;
             }
         }
         if(!empty($user_id)) {
             $user = \mobile\models\User::find()->andWhere(['id' => $user_id])->one();
         }
        if (!empty($user)) {
            Yii::$app->user->login($user);
            if(empty($user->access_token)) {
                $user->access_token = SecurityHelper::generateAccessToken($user->id);
                $user->save(false);
                $token = $user->access_token;
            }
            else{
                $token = $user->access_token;
            }

            $signature = SecurityHelper::createSignature($token);
        }
        if (empty($result['error'])) {
            Yii::$app->cart->emptyCart($result['orderId']);
            
            // redirect to result page

            $this->redirect(['/cart/mobile-checkout/success', 'orderId' => $result['orderId'],'token' => $token, 'signature' => $signature]);
        } else {
            if (!empty($get['vpc_OrderInfo'])) {
                Order::makeFailedPayment($get['vpc_OrderInfo']);
            }
            //Mommo payment
            if($method == "momo"){
                if(!empty($result['orderId'])) {
                    Order::makeFailedPayment($result['orderId']);
                }
            }
            $this->redirect(['/cart/mobile-checkout/failure', 'orderId' => $result['orderId'], 'token' => $token, 'signature' => $signature]);
        }
    }
    
    /**
     * @desc Api Ipn action for third party call
     * @param string $method
     */
    public function actionIpn($method)
    {
        $paymentMethod = PaymentMethod::loadModel($method);
        $get = Yii::$app->request->get();
        
        $paymentMethod->ipn($get);
        
        echo $paymentMethod->printIpnResponse();
    }
    
}
