<?php

namespace mobile\modules\cart\controllers;

use Yii;
use yii\filters\VerbFilter;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\web\Cookie;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;

use yz\shoppingcart\CartActionEvent;

use kyna\user\models\UserCourse;
use kyna\user\models\UserTelesale;
use kyna\user\models\TimeSlot;
use kyna\course\models\Course;

use common\helpers\RoleHelper;

use mobile\modules\cart\models\form\UserInfoForm;
use mobile\modules\cart\components\ShoppingCart;
use mobile\modules\cart\models\Product;
use common\campaign\CampaignTet;
/**
 * Default controller for the `cart` module
 */
class DefaultController extends Controller
{
    
    public $layout = '@mobile/views/layouts/one_column';
    
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'add' => ['post'],
                    'remove' => ['post'],
                    'check-payment' => ['post'],
                ],
            ],
        ];
    }
    
    public function init()
    {
        $ret = parent::init();
        
        CartActionEvent::on(ShoppingCart::className(), ShoppingCart::EVENT_POSITION_PUT, [$this, 'cartAddedItem']);
        CartActionEvent::on(ShoppingCart::className(), ShoppingCart::EVENT_BEFORE_POSITION_REMOVE, [$this, 'cartRemovedItem']);
        
        return $ret;
    }
    
    /**
     * @desc Cart index
     * @return view
     */
    public function actionIndex()
    {
        $cart = Yii::$app->cart;
        $promotionCode = Yii::$app->cart->getPromotionCode();
        if(CampaignTet::InTimesCampaign() && empty($promotionCode)){
            $cartItems = array_reverse($cart->getPositions());
            $totalPrice = 0;
            foreach ($cartItems as $item)
            {
                /* @var $item Product */
                if(!in_array($item->getId(),CampaignTet::COURSE_NOT_APPLY_VOUCHER)){
                    $totalPrice += $item->getPrice();
                }
            }
            $voucher = CampaignTet::voucherLixTet($totalPrice);
            Yii::$app->cart->setPromotionCode($voucher);
            Yii::$app->cart->applyPromotionCode();
        }
        
        return $this->render('index', ['cart' => $cart]);
    }
    
    /**
     * @desc action add to cart
     * @param integer $id
     * @return view
     * @throws NotFoundHttpException
     */
    public function actionAdd()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $pId = Yii::$app->request->post('pid');
        $product = $this->loadProductModel($pId);

        if (!Yii::$app->cart->hasPosition($product->id)) {
            if (!$this->checkAlreadyInCourse($product->id)) {
                Yii::$app->cart->put($product, 1, $product->discountAmount);
                $alertContent = $this->renderPartial('_result', ['newItem' => true, 'productName' => $product->name], true);
            } else {
                $alertContent = $this->renderPartial('_course_exists', ['newItem' => false, 'productName' => $product->name], true);
            }
        } else {
            $alertContent = $this->renderPartial('_result', ['newItem' => false, 'productName' => $product->name], true);
        }
        
        return [
            'result' => true,
            'totalCount' => Yii::$app->cart->getCount(),
            'content' => $this->renderPartial('@mobile/views/layouts/common/short_cart', ['justAdded' => true], true),
            'alertContent' => $alertContent,
        ];
    }

    public function actionBuyNow()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $pId = Yii::$app->request->post('pid');
        $product = $this->loadProductModel($pId);

        Yii::$app->cart->emptyCart();
        Yii::$app->cart->put($product, 1, $product->discountAmount);

        return [
            'result' => true,
            'redirectUrl' => Url::to(['/cart/checkout/index'])
        ];
    }
    
    protected function checkAlreadyInCourse($pId)
    {
        return UserCourse::find()->where([
                'user_id' => Yii::$app->user->id,
                'course_id' => $pId
            ])->exists();
    }

    protected function loadProductModel($pId)
    {
        $product = Product::findOne($pId);
        if (empty($product)) {
            throw new NotFoundHttpException();
        }
        
        return $product;
    }

    /**
     * @desc remove item from cart
     * @return redirect
     * @throws BadRequestHttpException
     * @throws NotFoundHttpException
     */
    public function actionRemove()
    {
        $pIds = Yii::$app->request->post('pids');
        
        if (empty($pIds)) {
            throw new BadRequestHttpException();
        }
        
        foreach ($pIds as $pId) {
            $product = $this->loadProductModel($pId);
            
            Yii::$app->cart->remove($product);
        }
        
        return $this->redirect(['index']);
    }
    
    public function actionCheckPayment()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        
        $paymentUrl = Url::toRoute(['/cart/checkout/index']);
        
        if (Yii::$app->user->isGuest) {
            // user not logged in
            Yii::$app->user->setReturnUrl($paymentUrl);

            return [
                'result' => false,
            ];
        }
        
        return [
            'result' => true,
            'paymentUrl' => $paymentUrl
        ];
    }
    
    /**
     * @desc trigger when add combo then check if has already any combo items in cart => delete combo item
     * @param type $cartActionEvent
     */
    public function cartAddedItem($cartActionEvent)
    {
        $product = $cartActionEvent->position;
        
        if ($product->type == Product::TYPE_COMBO) {
            $comboItemCourseIds = ArrayHelper::map($product->comboItems, 'course_id', 'course_id');
            $cartPositions = Yii::$app->cart->getPositions();
            $cartItemIds = array_keys($cartPositions);

            $existItemsInComboIds = array_intersect($comboItemCourseIds,  $cartItemIds);
            $removedPositions = [];
            foreach ($existItemsInComboIds as $existsItemId) {
                $removePosition = $cartPositions[$existsItemId];
                if (Yii::$app->cart->remove($removePosition)) {
                    $removedPositions[] = $removePosition->name;
                }
            }
            
            if (!empty($removedPositions)) {
                Yii::$app->session->setFlash('warning', "Khóa học <b>" . implode(',', $removedPositions) . "</b> đã bao gồm trong Combo <b>" . $product->name . "</b> nên đã được bỏ ra khỏi giỏ hàng.");
            }
        }
    }
    
    public function cartRemovedItem($cartActionEvent)
    {
        $product = $cartActionEvent->position;
        if (!empty($product->combo)) {
            $positions = Yii::$app->cart->getPositions();
            $removedComboItems = [];
            
            foreach ($positions as $position) {
                if ($position->id == $product->id) {
                    continue;
                }
                if (!empty($position->combo) && $position->combo->id == $product->combo->id) {
                    $discount = !empty($position->price_discount) ? ($position->getPrice(false) - $position->price_discount) : 0;
                    Yii::$app->cart->update($position, 1, $discount);
                    $removedComboItems[] = $position->name;
                }
            }
            
            if (!empty($removedComboItems)) {
                Yii::$app->session->setFlash('warning', "Các khóa học vừa không được hưởng giá khuyến mãi của Combo <b>{$product->combo->name}</b>: " . implode(', ', $removedComboItems));
            }
        }
    }
    
    public function actionAddUserInfo()
    {
        $model = new UserInfoForm();

        if ($model->load(Yii::$app->request->post())) {
            if (Yii::$app->request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;
            }

            if ($model->validate()) {
                // save user info to cookie
                Yii::$app->response->cookies->add(new Cookie([
                    'name' => 'cart-user-info',
                    'value' => [
                        'phone_number' => $model->phone_number
                    ]
                ]));

                $positions = Yii::$app->cart->getPositions();
                $courseIds = [];

                foreach ($positions as $position) {
                    $courseIds[] = $position->id;
                }

                $isSuccess = $this->addUserCare($model->phone_number, count($courseIds) == 1 ? $courseIds[0] : $courseIds);
                if ($isSuccess) {
                    return [
                        'result' => true,
                    ];
                } else {
                    return [
                        'result' => false,
                        'message' => 'Xảy ra lỗi trong quá trình lưu trữ!'
                    ];
                }
            } else {
                return [
                    'result' => false,
                    'message' => 'Xảy ra lỗi trong quá trình kiểm tra dữ liệu!',
                    'errors' => $model->errors
                ];
            }
        }

        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('user-info-form');
        }

        return $this->render('user-info-form');
    }

    /**
     * add cookie for ignoring show popup to get user info
     */
    public function actionDisablePopup()
    {
        $key = 'get-user-cart-info-' . date('Y-m-d');
        // add cookie to check has already shown popup get user info
        Yii::$app->response->cookies->add(new Cookie([
            'name' => $key,
            'value' => 1,
            'expire' => strtotime('tomorrow')
        ]));
    }

    public function actionAddUserCare()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $email = $userId = null;

        if (Yii::$app->user->isGuest) {
            $cookies = Yii::$app->request->cookies;

            $userInfo = $cookies->getValue('cart-user-info');
            if ($userInfo == null) {
                return;
            }

            $phoneNumber = $userInfo['phone_number'];
        } else {
            $user = Yii::$app->user->identity;
            $profile = $user->profile;

            $phoneNumber = $profile->phone_number;
            $userId = $user->id;
        }

        $positions = Yii::$app->cart->getPositions();
        $courseIds = [];

        foreach ($positions as $position) {
            $courseIds[] = $position->id;
        }

        $isSuccess = $this->addUserCare($phoneNumber, count($courseIds) == 1 ? $courseIds[0] : $courseIds, $userId);
        if ($isSuccess) {
            return [
                'result' => true,
            ];
        } else {
            return [
                'result' => false,
                'message' => 'Xảy ra lỗi trong quá trình lưu trữ!'
            ];
        }
    }

    public function actionRethinkToPayment()
    {
        return $this->renderAjax('rethink-to-payment');
    }

    public function addUserCare($phone, $courseId = null, $userId = null)
    {
        $courseIds = [];
        if (is_array($courseId)) {
            $courseIds = $courseId;
        } else {
            $courseIds[] = $courseId;
        }

        $userTelesale = UserTelesale::find()->where([
            'type' => UserTelesale::TYPE_AUTO_CART,
            'phone_number' => $phone,
        ])
            ->andWhere(['>=', 'created_time', strtotime('today')])
            ->one();

        $needSave = false;
        if (is_null($userTelesale)) {
            $userTelesale = new UserTelesale();

            $userTelesale->phone_number = $phone;
            $userTelesale->type = UserTelesale::TYPE_AUTO_CART;
            $userTelesale->list_course_ids = implode(', ', $courseIds);
            $userTelesale->user_id = $userId;
            $userTelesale->tel_id = TimeSlot::getOperatorsInShift(RoleHelper::ROLE_TELESALE, UserTelesale::TYPE_AUTO_CART);

            // detect exist affiliate
            $cookies = Yii::$app->request->cookies;
            if ($affiliateId = $cookies->getValue('affiliate_id')) {
                $userTelesale->affiliate_id = $affiliateId;
            }

            $needSave = true;
        } else {
            $oldCourses = explode(', ', $userTelesale->list_course_ids);
            $userTelesale->list_course_ids = implode(', ', array_unique(array_merge($oldCourses, $courseIds)));
            $userTelesale->created_time = time();

            $diff = array_merge(array_diff($oldCourses, $courseIds), array_diff($courseIds, $oldCourses));
            if (count($diff) > 0) {
                $needSave = true;
            }
        }



        if ($needSave) {
            $formNames = [];

            if (!empty($userTelesale->list_course_ids)) {
                $totalCourseIds = explode(', ', $userTelesale->list_course_ids);
                foreach ($totalCourseIds as $courseId) {
                    $course = Course::findOne($courseId);
                    if ($course != null) {
                        $formNames[] = $course->name;
                    }
                }

                $userTelesale->form_name = implode(', ', $formNames);
            }

            if (!$userTelesale->save(false)) {
                return false;
            }
        }

        return true;
    }

}
