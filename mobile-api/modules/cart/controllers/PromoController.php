<?php

namespace mobile\modules\cart\controllers;

use mobile\modules\cart\models\form\PromoCodeForm;
use common\helpers\ResponseModel;
use kyna\order\models\Order;
use Yii;

class PromoController extends \mobile\modules\cart\components\Controller
{

    public function actionApply()
    {
        $model = new PromoCodeForm();

        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()) {
                return $model->applyCode();

            } else {
                Yii::$app->cart->setDiscountAmount(Yii::$app->cart->getTotalDiscountAmount());
                Yii::$app->cart->promotionCode = null;
                Yii::$app->cart->resetCartPromotion();
                Yii::$app->cart->saveCart();
                if(!empty(Yii::$app->cart->getOrderId())) {
                    Yii::$app->cart->updateOrderFromCart(Yii::$app->cart->getOrderId());
                }
                $mgs = "";
                foreach ($model->getErrors() as $err){
                    $mgs .= $err[0];
                }
                Yii::$app->session->setFlash('error', $mgs);
                return ResponseModel::responseJson($model->getErrors(), ResponseModel::VALIDATION_ERROR_CODE);
            }
        }
    }

}