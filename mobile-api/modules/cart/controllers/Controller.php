<?php

namespace mobile\modules\cart\controllers;

use common\helpers\RequestCookieHelper;
use Yii;
use yii\web\Cookie;
use yii\helpers\ArrayHelper;
use yii\web\Response;

use kyna\settings\models\Setting;
use kyna\commission\models\AffiliateUser;
use kyna\user\models\User;

use mobile\models\Category;

use common\helpers\CDNHelper;

/*
 * This is override core Controller class for Frontend usage
 */

class Controller extends \yii\web\Controller
{

    public $layout = '@mobile/modules/cart/views/layouts/two_columns';
    public $rootCats = [];
    public $settings = [];
    public $bodyClass = '';
    public $cdnUrl = '';

    public function init()
    {
        $ret = parent::init();

        $this->cdnUrl = CDNHelper::getMediaLink();

        $this->rootCats = Category::getList(0, ['id', 'name', 'slug']);
        $this->settings = ArrayHelper::map(Setting::find()->all(), 'key', 'value');

        if (!empty($this->settings['js-head'])) {
            $js = $this->settings['js-head'];
            $this->view->registerJs($js, View::POS_HEAD);
        }
        if (!empty($this->settings['js-foot'])) {
            $js = $this->settings['js-foot'];
            $this->view->registerJs($js, View::POS_FOOT);
        }

        if (!empty(Yii::$app->user->identity->loginToken)) {
            $code = Yii::$app->user->identity->loginToken->code;

            Yii::$app->session->open();

            if ($code != Yii::$app->session->id){
                Yii::$app->user->logout();
            }
            Yii::$app->session->close();
        }

//        if (!Yii::$app->user->isGuest) {
//            var_dump(Yii::$app->user->identity->loginToken->code);
//            var_dump(Yii::$app->session->id);
//        }

//        if (!Yii::$app->user->isGuest) {
//            $requestCookie = Yii::$app->request->cookies->get('_request');
//            $currentCacheKey = Yii::$app->user->id. '_request_current';
//            $lastCacheKey = Yii::$app->user->id. '_request_last';
//            $cachedCurrentRequestCookie = Yii::$app->cache->get($currentCacheKey);
//            $cachedLastCacheRequestCookie = Yii::$app->cache->get($lastCacheKey);
//
//            if (empty($requestCookie) || empty($cachedCurrentRequestCookie) || empty($cachedCurrentRequestCookie['cookie'])) {
//                    $logMessage = [
//                        'user_id' => Yii::$app->user->id,
//                        'request_cookie' => $requestCookie,
//                        'cached_current_cookie' => $cachedCurrentRequestCookie,
//                        'cached_last_cookie' => $cachedLastCacheRequestCookie,
//                    ];
//                    Yii::info($logMessage, 'logout');
//                Yii::$app->user->logout();
//            } else if ($cachedCurrentRequestCookie['cookie'] === $requestCookie->value) {
//                if ($cachedCurrentRequestCookie['value'] >= RequestCookieHelper::MAX_REQUEST_COUNT) {
//                    RequestCookieHelper::refreshCookie();
//                } else {
//                    $cachedCurrentRequestCookie['value'] += 1;
//                    Yii::$app->cache->set(
//                        $currentCacheKey,
//                        $cachedCurrentRequestCookie,
//                        RequestCookieHelper::CURRENT_REQUEST_COOKIE_DURATION
//                    );
//                }
//            } else {
//                if (empty($cachedLastCacheRequestCookie) || $cachedLastCacheRequestCookie['cookie'] !== $requestCookie->value) {
////                    Yii::info(['test' => 'ahihi'], 'logout');
//                    $logMessage = [
//                        'user_id' => Yii::$app->user->id,
//                        'request_cookie' => $requestCookie,
//                        'cached_current_cookie' => $cachedCurrentRequestCookie,
//                        'cached_last_cookie' => $cachedLastCacheRequestCookie,
//                    ];
//                    Yii::info($logMessage, 'logout');
//
//                    Yii::$app->user->logout();
//                }
//            }
//        }

        $this->_registerMetaTag();
        $this->renderScriptUserCare();
        return $ret;
    }

    public function beforeAction($action)
    {

        if (isset(Yii::$app->params['maintenance']) && Yii::$app->params['maintenance'] === true) {
            return $this->redirect('/bao-tri');
        }
        $ret = parent::beforeAction($action);

        if (!Yii::$app->request->get('debug') && $debugModule = Yii::$app->getModule('debug')) {
            $debugModule->instance->allowedIPs = [];
        }

        $this->detectAffiliate();
        return $ret;
    }

    public function renderScriptUserCare()
    {
        $hasUserInfo = false;

        if (Yii::$app->user->isGuest) {
            $cookies = Yii::$app->request->cookies;

            $userInfo = $cookies->getValue('cart-user-info');
            if ($userInfo == null) {
                $key = 'get-user-cart-info-' . date('Y-m-d');

                $isToday = $cookies->getValue($key);
                if ($isToday == null) {
                    // if user have not seen popup today then register pop
                    $this->view->registerJsFile($this->cdnUrl . '/src/js/popup-get-user-info.js');
                }
            } else {
                $hasUserInfo = true;
            }
        } else {
            $hasUserInfo = true;
        }

        if ($hasUserInfo) {
            // if user have not seen popup today then register pop
            $this->view->registerJsFile($this->cdnUrl . '/src/js/cart-to-user-care.js');
        }
    }

    protected function detectAffiliate()
    {
        if (Yii::$app->session->hasFlash('affiliate_id')) {
            $affiliate_id = Yii::$app->session->getFlash('affiliate_id');

            $script = "
                (function ($, window, document, undefined) {
                    // trigger event when user mouse out from kyna
                    $(document).ready(function () {
                        var data = {
                            affiliate_id: " . $affiliate_id . "
                        };
                        
                        var csrfPram = $('meta[name=\'csrf-param\']').attr('content');
                        var csrfToken = $('meta[name=\'csrf-token\']').attr('content');
                        
                        data[csrfPram] = csrfToken;
                        $.ajax({
                            url: '/site/detect-affiliate',
                            type: 'POST',
                            data: data
                        });
                    });
                })(window.jQuery, window, document);
            ";

            $this->view->registerJs($script, View::POS_END, 'detect-affiliate-js');
        }
    }

    public function actionDetectAffiliate()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $affiliateId = Yii::$app->request->post('affiliate_id', 0);

        return $this->applyAffiliate($affiliateId);
    }

    public function applyAffiliate ($affiliateId)
    {
        $user = User::findOne($affiliateId);
        if (is_null($user)) {
            return [
                'result' => false,
                'message' => 'Không tìm thấy user',
            ];
        }

        $cookies = Yii::$app->request->cookies;
        $currentId = $cookies->getValue('affiliate_id');

        // check if not exist or update affiliate_id
        if (empty($currentId) || $currentId != $affiliateId) {
            /* @var AffiliateUser $currentAffUser */
            /* @var AffiliateUser $affiliateUser */
            $currentAffUser = AffiliateUser::find()->where(['user_id' => $currentId])->one();
            $affiliateUser = AffiliateUser::find()->where([
                'user_id' => $affiliateId,
                'status' => AffiliateUser::STATUS_ACTIVE
            ])->one();
            if (empty($affiliateUser)) {
                return [
                    'result' => false,
                    'message' => 'Không tìm thấy affiliate',
                ];
            }
            if (!empty($currentAffUser)) {
                if ($affiliateUser->affiliate_category_id == Yii::$app->params['idOfOtherDisplayAffCategory']) {
                    if (isset($this->settings['priority_aff_ids'])) {
                        $priorityAffIds = explode(',', $this->settings['priority_aff_ids']);
                        if (!empty($priorityAffIds) && in_array($currentId, $priorityAffIds)) {
                            return [
                                'result' => false,
                                'message' => 'Không thể ghi đè affiliate ưu tiên',
                            ];
                        }
                    }
                    if ($currentAffUser->affiliateCategory->affiliate_group_id == Yii::$app->params['id_aff_group_third_party']) {
                        return [
                            'result' => false,
                            'message' => 'Không thể ghi đè affiliate của bên thứ 3',
                        ];
                    }
                }

                // validate rule override affiliate category
                if (in_array($affiliateUser->affiliate_category_id,
                    $currentAffUser->affiliateCategory->category_can_not_override)) {
                    return [
                        'result' => false,
                        'message' => 'Không thể ghi đè lên loại affiliate này',
                    ];
                }

                // aff has prefix 'Kyna'(in setting kyna_aff_cat_ids) cannot overwrite special affs in setting id_aff_can_not_overwrite_by_kyna
                $kynaAffIds = explode(',', $this->settings['kyna_aff_cat_ids']);
                $specialAffIds = explode(',', $this->settings['id_aff_can_not_overwrite_by_kyna']);
                if (in_array($currentId, $specialAffIds) && in_array($affiliateUser->affiliate_category_id,
                        $kynaAffIds)) {
                    return [
                        'result' => false,
                        'message' => 'Không thể ghi đè lên affiliate này',
                    ];
                }
            }

            $cookies = Yii::$app->response->cookies;

            // add affiliate cookie to the response
            $cookies->add(new Cookie([
                'name' => 'affiliate_id',
                'value' => $affiliateId,
                'expire' => $affiliateUser->cookieExpireTimestamp
            ]));

            return [
                'result' => true,
                'message' => 'Đã cập nhật affiliate',
            ];
        }

        return [
            'result' => false,
            'message' => 'Affiliate không hợp lệ hoặc trùng thông tin',
        ];
    }

    /**
     * Register Meta Tags
     */
    private function _registerMetaTag() {
        $this->view->registerMetaTag([
            'name' => 'google-site-verification',
            'content' => 'JQvj84cgEFSsi0_LRfTN39yKNQ5ZogkvSUw9bcQR_t0'
        ]);
    }
}
