<?php

namespace mobile\modules\cart\controllers;

use common\campaign\CampaignTet;
use common\helpers\DateTimeHelper;
use kyna\promotion\models\UserVoucherFree;
use kyna\user\models\Address;
use Yii;
use yii\filters\AccessControl;
use yii\web\Cookie;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\web\HttpException;
use yii\base\ActionEvent;
use yii\widgets\ActiveForm;
use yii\validators\EmailValidator;
use yii\validators\RegularExpressionValidator;

use common\helpers\ArrayHelper;
use common\helpers\PhoneNumberHelper;
use common\helpers\DocumentHelper;

use kyna\user\models\User;
use kyna\order\models\Order;
use kyna\course\models\Course;
use kyna\promotion\models\Promotion;
use kyna\settings\models\Setting;
use kyna\user\models\UserTelesale;
use kyna\payment\models\PaymentMethod;
use kyna\payment\models\TopupTransaction;
use kyna\commission\models\AffiliateUser;

use mobile\modules\cart\models\form\PaymentForm;
use mobile\modules\cart\components\Controller;
use mobile\modules\cart\components\ShoppingCart;
use mobile\modules\cart\models\form\TopUpForm;
use mobile\modules\cart\models\Product;
use mobile\modules\cart\models\form\PromoCodeForm;

use mobile\helpers\SecurityHelper;


/*
 * Class CheckoutController for shopping cart payments
 */
class MobileCheckoutController extends Controller
{

    public $layout = 'checkout';
    public $mainDivId = 'checkout-home';
    public $mainDivClass = 'checkout';
    public $order = null;
    public $isNewRequest = false;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['?', '@'],
                    ],
                    [
                        'allow' => false,
                        'roles' => ['?'],
                        'actions' => ['success', 'failure']
                    ],
                ],
            ],
        ];
    }

    public function init()
    {
        $ret = parent::init();
        ActionEvent::on(Order::className(), Order::EVENT_ORDER_PLACED, [$this, 'afterPlaceOrder']);
        $this->setViewPath('@mobile/modules/cart/views/checkout');
        return $ret;
    }

    public function beforeAction($action)
    {
        $ret = parent::beforeAction($action);

        if ($action->id == 'index') {

            $orderId = Yii::$app->request->get('orderId');
            $courseIdString = Yii::$app->request->get('id', null);
            $code = Yii::$app->request->get('code');
            $affiliateId = Yii::$app->request->get('aff_id');
            $paymentMethod = Yii::$app->request->get('payment_method');
            $user_id = Yii::$app->request->get('uid');
            $sess_id = Yii::$app->request->get('sess_id');
            $old_sess_id = Yii::$app->session->get('sess_id');

            if(empty($old_sess_id) || $sess_id != $old_sess_id){
                $this->isNewRequest = true;
                Yii::$app->session->set('sess_id', $sess_id);

            }
            if (!empty($courseIdString) || !empty($code) || !empty($affiliateId) || $paymentMethod) {
                if (!$this->validatePaymentLink($courseIdString, $code, $affiliateId, $paymentMethod, $sess_id, $user_id)) {
                    throw new NotFoundHttpException();
                }
            }
            if(!empty($orderId) && !empty($user_id)){
                if(!$this->validateOrderPaymentLink($orderId, $user_id, $sess_id)){
                    throw new NotFoundHttpException();
                }

            }

            if (!empty($courseIdString) && $this->isNewRequest) {
                $courseIds = explode(',', $courseIdString);
                // override cart items
                Yii::$app->cart->emptyCart();
                $totalPrice = 0;
                foreach ($courseIds as $courseId) {
                    $product = Product::findOne($courseId);
                    if ($product == null) {
                        continue;
                    }
                    Yii::$app->cart->put($product, 1, $product->discountAmount);
                    $totalPrice += $product->discountAmount;

                }
                /*
                if(DateTimeHelper::checkAppPromotionTime()) {
                    Yii::$app->cart->setPromotionCode("APP_KYNA");
                    Yii::$app->cart->applyPromotionCode();
                }*/
                $promotionCode = Yii::$app->cart->getPromotionCode();
                if(CampaignTet::InTimesCampaign() && empty($promotionCode)){
                    $cart = Yii::$app->cart;
                    $cartItems = array_reverse($cart->getPositions());
                    $totalPrice = 0;
                    foreach ($cartItems as $item)
                    {
                        /* @var $item Product */
                        if(!in_array($item->getId(),CampaignTet::COURSE_NOT_APPLY_VOUCHER)){
                            $totalPrice += $item->getPrice();
                        }
                    }

                    $voucher = CampaignTet::voucherLixTet($totalPrice);
                    Yii::$app->cart->setPromotionCode($voucher);
                    Yii::$app->cart->applyPromotionCode();
                }
            }


            if (!empty($affiliateId)) {
                $this->applyAffiliate($affiliateId);
            }
        }

        return $ret;
    }

    /**
     * @desc Checkout main action
     * @return type
     */
    public function actionIndex()
    {
        $id = Yii::$app->request->get('orderId');

        $cart = Yii::$app->cart;
        $userAddress = $this->loadUserAddress();
        $order = null;

        if (!empty($id)) {
            $orderId = $id;
        }

        if (empty($orderId)) {
            $orderId = $cart->getOrderId();
        }


        $paymentForm = new PaymentForm();
        if (!empty($orderId)) {
            //  $paymentForm->updateOrderFromCart();
            $order = $cart->getOrder($orderId);
            Yii::$app->cart->removeAll();
            Yii::$app->cart->addFromOrder($order);
           /* if($this->isNewRequest && DateTimeHelper::checkAppPromotionTime()) {
                $oldPromotionCode =  Yii::$app->cart->getPromotionCode();
                if (empty($oldPromotionCode)) {
                    Yii::$app->cart->setPromotionCode("APP_KYNA");
                    Yii::$app->cart->applyPromotionCode();
                }
            }*/
            if($this->isNewRequest && CampaignTet::InTimesCampaign()){
                $oldPromotionCode =  Yii::$app->cart->getPromotionCode();
                if (empty($oldPromotionCode)) {
                    $cartItems = array_reverse($cart->getPositions());
                    $totalPrice = 0;
                    foreach ($cartItems as $item)
                    {
                        /* @var $item Product */
                        if(!in_array($item->getId(),CampaignTet::COURSE_NOT_APPLY_VOUCHER)){
                            $totalPrice += $item->getPrice();
                        }
                    }

                    $voucher = CampaignTet::voucherLixTet($totalPrice);
                    Yii::$app->cart->setPromotionCode($voucher);
                    Yii::$app->cart->applyPromotionCode();
                }
            }
            $paymentForm->order_id = $orderId;
            $paymentMethods = $this->loadPaymentMethod(Yii::$app->cart->getTotalPriceAfterDiscountAll());

        } else {
            $positions = $cart->getPositions();
            if (empty($positions)) {
                return $this->redirect(['/cart/default/index']);
            }

        }

        $paymentForm->totalAmount = Yii::$app->cart->getTotalPriceAfterDiscountAll();
        $paymentMethods = $this->loadPaymentMethod(Yii::$app->cart->getTotalPriceAfterDiscountAll());

        if ($paymentForm->load(Yii::$app->request->post())) {

            $postData = Yii::$app->request->getBodyParams();

            //Get user Info
            $user_id = (int) $postData['uid'];
            if(empty($user_id))
            {
                if(!empty($paymentForm->user_id) && !Yii::$app->user->isGuest) {
                    $user_id = $paymentForm->user_id;
                }

            }
            if(!empty($user_id)) {
                $user = \mobile\models\User::find()->andWhere(['id' => $user_id])->one();
            }
            if (!empty($user)) {
                if(Yii::$app->user->isGuest) {
                    Yii::$app->user->login($user);
                }
                if(empty($user->access_token)) {
                    $user->access_token = SecurityHelper::generateAccessToken($user->id);
                    $user->save(false);
                    $token = $user->access_token;
                }
                else{
                    $token = $user->access_token;
                }
                $signature = SecurityHelper::createSignature($token);
            }
            //End get user Info

            if ($paymentForm->method !== 'free') {
                $this->checkFreeCourse($paymentForm->totalAmount, $paymentForm);
            }

            Yii::$app->response->format = Response::FORMAT_JSON;

            if ($paymentForm->validate()) {

                $order = Order::placeOrder($orderId, $paymentForm->method, $paymentForm->getOrderDetails(), $paymentForm->getShippingAddress(), false, $paymentForm->user_id, $paymentForm->promotion_code);
                if ($orderId == null && $order) {
                    Yii::$app->cart->orderId = $order->id;
                    $orderId = $order->id;
                }

                $response = $paymentForm->doPayment($order);


                if (!$response['result']) {
                    Order::makeFailedPayment($orderId);
                    if(!empty($token) && !empty($signature)) {
                        return [
                            'result' => true,
                            'redirectUrl' => Url::to(['failure', 'token' => $token, 'signature' => $signature])
                        ];
                    }
                    return
                        [
                            'result' => true,
                            'redirectUrl' => Url::to(['failure'])
                        ];
                }
                if (!empty($response['redirectUrl'])) {
                    // redirect payment
                    return [
                        'result' => true,
                        'redirectUrl' => $response['redirectUrl']
                    ];
                }

                if ($response['result'] && $response['continue_pay'] === false) {
                    // payment success
                    if(!empty($token) && !empty($signature)) {
                        return [
                            'result' => true,
                            'redirectUrl' => Url::to(['success', 'orderId' => $orderId, 'token' => $token, 'signature' => $signature])
                        ];
                    }
                    return [
                        'result' => true,
                        'redirectUrl' => Url::to(['success', 'orderId' => $orderId])
                    ];
                } else {
                    $paymentForm->paidAmount = $order->paidAmount;
                }

                Yii::$app->session->setFlash('payment-method', $paymentForm->method);
                return [
                    'result' => true,
                ];
            } else {
                $errors = ActiveForm::validate($paymentForm);
                return [
                    'result' => empty($errors) ? true : false,
                    'errors' => $errors
                ];
            }
        }

        if (!empty(Yii::$app->params['discount_combo'])) {
            // remove promotion if has not combo
            if (!empty(Yii::$app->cart->getPromotionCode()) && (Yii::$app->cart->getPromotionCode() == Yii::$app->params['discount_combo'])) {
                Yii::$app->cart->removePromotion($orderId);
            }
            if (empty(Yii::$app->cart->getPromotionCode()) && Yii::$app->cart->hasCombo()) {
                Yii::$app->cart->applyCampaignPromotion();
            }
        }
        $promotionCode = Yii::$app->cart->getPromotionCode();
        if(CampaignTet::InTimesCampaign()  && empty($promotionCode)){
            $cartItems = array_reverse($cart->getPositions());
            $totalPrice = 0;
            foreach ($cartItems as $item)
            {
                /* @var $item Product */
                if(!in_array($item->getId(),CampaignTet::COURSE_NOT_APPLY_VOUCHER)){
                    $totalPrice += $item->getPrice();
                }
            }

            $voucher = CampaignTet::voucherLixTet($totalPrice);
            Yii::$app->cart->setPromotionCode($voucher);
            Yii::$app->cart->applyPromotionCode();
        }

        return $this->render('index', [
            'paymentMethods' => $paymentMethods,
            'userAddress' => $userAddress,
            'paymentForm' => $paymentForm,
            'order' => $order,
        ]);
    }


    /**
     * @param $orderId
     * @return string
     * @throws HttpException
     */
    public function actionSuccess($orderId)
    {
        $signature = Yii::$app->request->get('signature');
        $token = Yii::$app->request->get('token');
        $user_id = Yii::$app->user->id;
        if(empty($user_id)) {
            if (SecurityHelper::checkSignature($signature, $token)) {
                $user_id = SecurityHelper::restoreUserIdFromToken($token);
            }
        }
        if(!empty($user_id)) {
            $this->order = Order::findOne([
                'id' => $orderId,
                'user_id' => $user_id
            ]);
        }
        else{
            $this->order = Order::findOne([
                'id' => $orderId,
            ]);
        }
        if ($this->order == null) {
            throw new NotFoundHttpException();
        }

        $settings = ArrayHelper::map(Setting::find()->all(), 'key', 'value');

        if ($this->order->status == Order::ORDER_STATUS_IN_COMPLETE) {
            $this->mainDivId = "checkout-failed";
        } else {
            $codeVoucher = $this->order->getPromotionCode();
            if(!empty($codeVoucher)){
                list($code,$userId) = UserVoucherFree::isUserVoucherFree($codeVoucher);
                if(!empty($code) && !empty($userId)){
                    UserVoucherFree::applyVoucherFree($userId,$code,UserVoucherFree::PREFIX,Yii::$app->user->id);
                }
            }

            $this->mainDivId = "checkout-succ-online";
            $this->mainDivClass = "checkout success";
        }

        return $this->render('success', [
            'order' => $this->order,
            'settings' => $settings
        ]);
    }

    public function actionFailure()
    {
        $this->mainDivId = "checkout-failed";
        $this->mainDivClass = "checkout failed";
        $orderId = Yii::$app->cart->getOrderId();
        if(empty($orderId))
            $orderId = Yii::$app->request->get('orderId');
        $this->order = Order::findOne($orderId);

        return $this->render('failure', [
            'order' => $this->order
        ]);
    }

    private function loadUserAddress()
    {
        if (!Yii::$app->user->isGuest) {
            $userAddress = Yii::$app->user->identity->userAddress;
        }

        if (empty($userAddress)) {
            $userAddress = new Address();
            $userAddress->user_id = Yii::$app->user->id;
        }
        $userAddress->scenario = 'pay_cod';

        return $userAddress;
    }


    public function afterPlaceOrder($event)
    {
        $actionMeta = $event->actionMeta;
        $orderMeta = $actionMeta['order'];
        $order = Order::findOne($orderMeta['id']);

        // send register success except: online payment(atm/cc) and epay
        if ($order->paymentMethod->payment_type != PaymentMethod::PAYMENT_TYPE_REDIRECT && !in_array($order->payment_method, ['epay', 'ipay'])) {
            $mailer = Yii::$app->mailer;
            $mailer->htmlLayout = '@common/mail/layouts/main';

            $mailer->compose('order/register_success', [
                'order' => $order,
                'settings' => $this->settings,
            ])->setTo($order->user->email)
                ->setSubject("Đăng ký đơn hàng #{$order->id} thành công.")
                ->send();
        }
    }

    public function actionTopup()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $formModel = new TopUpForm();

        $this->mainDivId = "checkout-succ-online";
        $this->mainDivClass = "checkout success";

        if ($formModel->load(Yii::$app->request->post()) && $formModel->validate()) {
            $provider = PhoneNumberHelper::detectProviderCode($formModel->phone_number);
            TopupTransaction::updateAll([
                'phone_number' => $formModel->phone_number,
                'provider' => $provider
            ], ['order_id' => $formModel->order_id]);

            return ['result' => 1];
        } else {
            return ['result' => 0, 'errors' => $formModel->errors];
        }
    }

    protected function loadPaymentMethod($amount)
    {
        if ($amount <= 0) {
            return [];
        }
        $setting = Setting::find()->where(['key' => 'cod_min_amount'])->one();

        if (!empty($setting) && ($amount < $setting->value)) {
            return PaymentMethod::getAvailable(PaymentMethod::GET_AVAILABLE_NOT_COD);
        }

        return PaymentMethod::getAvailable(PaymentMethod::GET_AVAILABLE_ALL);
    }

    protected function checkFreeCourse($orderTotal, $paymentForm)
    {
        if ($orderTotal <= 0) {
            $paymentForm->method = 'free';
            return true;
        }
        return false;
    }

    protected function checkPaymentMethodRedirect($payment_method)
    {
        $payment_method = PaymentMethod::find()->where(['class' => $payment_method])->one();
        if (!empty($payment_method) && $payment_method->payment_type == PaymentMethod::PAYMENT_TYPE_REDIRECT) {
            return true;
        }
        return false;
    }

    public function renderScriptUserCare()
    {
        // don't show popup to get user info at checkout controller
        return false;
    }

    public function actionValidateEmail()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $email = Yii::$app->request->post('email');
        $paymentMethod = Yii::$app->request->post('paymentMethod');

        if (empty($email)) {
            return [
                'result' => false,
                'message' => 'Email không được để trống',
            ];
        }

        $validator = new EmailValidator();
        if (!$validator->validate($email)) {
            return [
                'result' => false,
                'message' => 'Vui lòng nhập đúng định dạng email',
            ];
        }

        if (Yii::$app->user->isGuest) {
            if ($paymentMethod != 'cod' && User::find()->where(['email' => $email])->exists()) {
                return [
                    'result' => false,
                    'message' => 'Email <font color="black"><b>' . $email . '</b></font> đã có tài khoản. Nếu đó là tài khoản của bạn, vui lòng đăng nhập</i>',
                    'errorCode' => 2,
                ];
            } else {
                return [
                    'result' => true,
                    'message' => '* Email <span>' . $email . '</span> sẽ là tài khoản đăng nhập để học<br>Thông tin mật khẩu đăng nhập sẽ được gửi qua email khi thanh toán mua khoá học thành công'
                ];
            }
        }

        return [
            'result' => true,
            'message' => '',
        ];
    }

    public function actionAddUserCare()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $phone = Yii::$app->request->post('phone');

        $validator = new RegularExpressionValidator([
            'pattern' => '/^((0[0-9]{9,10})|(\+[0-9]{11,12})|([1-9]{1}[0-9]{10,13}))$/'
        ]);
        if ($validator->validate($phone)) {
            // add to user care
            $positions = Yii::$app->cart->getPositions();
            $courseIds = [];

            foreach ($positions as $position) {
                $courseIds[] = $position->id;
            }

            Yii::$app->response->cookies->add(new Cookie([
                'name' => 'cart-user-info',
                'value' => [
                    'phone_number' => $phone
                ]
            ]));

            UserTelesale::add($phone, count($courseIds) == 1 ? $courseIds[0] : $courseIds);

            return [
                'result' => true,
            ];
        } else {
            return [
                'result' => false,
            ];
        }
    }



    private function validatePaymentLink($courseIdString, $code, $affiliateId, $paymentMethod, $sess_id, $user_id)
    {

        $hash = Yii::$app->request->get('hash');
        if (empty($hash)) {
            return false;
        }

        $queryParams = [];
        if (!empty($courseIdString)) {
            $queryParams['id'] = $courseIdString;
        }
        if (!empty($code)) {
            $queryParams['code'] = $code;
        }
        if (!empty($courseIdString) && !empty($affiliateId)) {
            $queryParams['aff_id'] = $affiliateId;
        }
        if (!empty($paymentMethod)) {
            $queryParams['payment_method'] = $paymentMethod;
        }
        if (!empty($sess_id)) {
            $queryParams['sess_id'] = $sess_id;
        }
        if (!empty($user_id)) {
            $queryParams['uid'] = $user_id;
        }
        $serverHash = DocumentHelper::hashParams($queryParams);
        if ($serverHash != $hash) {

            return false;
        } else {
            if (!empty($user_id)) {
                $user = \mobile\models\User::find()->andWhere(['id' => $user_id])->one();
                if (!empty($user)) {
                    Yii::$app->user->login($user);
                }
                else{
                    Yii::$app->user->logout();
                }

            }
            else{
                Yii::$app->user->logout();
            }
            return true;
        }
    }


    private function validateOrderPaymentLink($orderId, $user_id, $sess_id)
    {
        $hash = Yii::$app->request->get('hash');
        if (empty($hash)) {
            return false;
        }

        $queryParams = [];
        if (!empty($orderId)) {
            $queryParams['order_id'] = $orderId;
        }
        if (!empty($sess_id)) {
            $queryParams['sess_id'] = $sess_id;
        }
        if (!empty($user_id)) {
            $queryParams['uid'] = $user_id;
        }

        $serverHash = DocumentHelper::hashParams($queryParams);
        if ($serverHash != $hash) {

            return false;
        } else {
            if (!empty($user_id)) {

                $user = \mobile\models\User::find()->andWhere(['id' => $user_id])->one();
                if (!empty($user)) {
                    Yii::$app->user->login($user);
                }

            }
            return true;
        }
        return false;
    }

}
