<?php

namespace mobile\modules\cart\models;

use yii\base\Component;
use \yz\shoppingcart\CartPositionTrait;
use yz\shoppingcart\CartPositionInterface;
use yz\shoppingcart\CostCalculationEvent;

/*
 * This is Product class for shopping cart.
 */
class Product extends \kyna\course\models\Course implements \yz\shoppingcart\CartPositionInterface
{
    use CartPositionTrait;

    protected $_comboDiscountAmount = 0;
    protected $_discount = 0;
    protected $_combo = 0;
    protected $_promotionCode = null;
    protected $_promotionSchedule = null;
    public $isGift;
    protected $_voucherDiscountAmount = 0;


    public function getVoucherDiscountAmount()
    {
        return $this->_voucherDiscountAmount;
    }

    public function getTotalDiscountAmount()
    {
        return $this->_voucherDiscountAmount + parent::getDiscountAmount();
    }

    public function getOriginalPrice()
    {
        return $this->oldPrice;
    }



    public function setVoucherDiscountAmount($v)
    {
        $this->_voucherDiscountAmount = $v;
    }

    public function getOriginalProduct()
    {
        return self::findOne($this->id);
    }

    public function setGift($isGift = false)
    {
        $originalProduct = $this->getOriginalProduct();
        $this->isGift = $isGift;
        $this->setDiscountAmount($originalProduct->getDiscountAmount() + $originalProduct->getPrice() / 2);
        return $this;
    }

    public function removeGift()
    {
        $originalProduct = $this->getOriginalProduct();
        $this->isGift = false;
        $this->setDiscountAmount($originalProduct->getDiscountAmount());
        return $this;
    }

    public function getPrice($withDiscount = true)
    {
        return $withDiscount ? ($this->oldPrice - $this->getDiscountAmount()) : $this->oldPrice;
    }

    public function setPromotionCode($promotionCode)
    {
        $this->_promotionCode = $promotionCode;
    }

    public function getPromotionCode()
    {
        return !empty($this->_promotionCode) ? $this->_promotionCode : null;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setDiscountAmount($discount)
    {
        $this->_discount = $discount;
    }

    //giảm trước
    public function getDiscountAmount()
    {
        $discount = parent::getDiscountAmount();
        return !empty($this->_discount) ? $this->_discount : $discount;
    }


    public function getOriginDiscount(){
        return parent::getDiscountAmount();
    }

    public function setCombo($combo)
    {
        $this->_combo = $combo;
    }

    public function getCombo()
    {
        return $this->_combo;
    }

    public function getCost($withDiscount = true)
    {
        $cost = $this->getQuantity() * $this->getPrice(false);
        $costEvent = new CostCalculationEvent([
            'baseCost' => $cost,
            'discountValue' => $this->discountAmount + $this->getDiscountGroupAmount()
        ]);
        if ($this instanceof Component)
            $this->trigger(CartPositionInterface::EVENT_COST_CALCULATION, $costEvent);
        if ($withDiscount)
            $cost = max(0, $cost - $costEvent->discountValue);
        return $cost;
    }

    public function getOriginalCost()
    {
        $cost = $this->getQuantity() * $this->getPrice(false);
        return $cost;
    }

    public function getCostText()
    {
        $costText = '';
        $formatter = \Yii::$app->formatter;
        $cost = $this->getCost();
        if ($this->getIsCampaign11()) {
            if ($this->isGift) {
                $costText = 'Khóa tặng';
            } else {
                $originalProduct = $this->getOriginalProduct();
                $costText = $formatter->asCurrency($originalProduct->getPrice());
            }
        } elseif (!empty($cost)) {
            $costText = $formatter->asCurrency($cost);
        } else {
            $costText = 'Miễn phí';
        }
        return $costText;
    }
    public function getComboDiscountAmount()
    {
        return $this->_comboDiscountAmount;
    }
    public function setComboDiscountAmount($combo_discount)
    {
        $this->_comboDiscountAmount = $combo_discount;
    }
}