<?php

namespace mobile\modules\cart\models\form;

use kyna\payment\models\PaymentMethod;
use kyna\base\models\Location;
use kyna\order\models\Order;
use kyna\promo\models\Promotion;
use kyna\user\models\Address;
use kyna\user\models\User;
use kyna\user\models\UserTelesale;
use kyna\course\models\Course;

use mobile\modules\cart\models\Product;

use Yii;
use yii\base\InvalidParamException;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\BadRequestHttpException;

use mobile\helpers\SecurityHelper;

/*
 * Payment Form model for Shopping Cart payment.
 */

class PaymentForm extends \yii\base\Model
{

    public $method;
    // scenario cod
    public $contact_name;
    public $email;
    public $phone_number;
    public $street_address;
    public $location_id;
    public $note;
    // scenario mobile card
    public $serviceCode;
    public $pinCode;
    public $serial;
    //
    public $promotion_code;
    public $order_id;
    public $user_id;
    //
    public $paidAmount = 0;
    public $totalAmount = 0;
    /**
     * @var
     */

    public $city_id;

    public function init()
    {
        $ret = parent::init();

        if (Yii::$app->session->hasFlash('payment-method')) {
            $this->method = Yii::$app->session->getFlash('payment-method');
        }

        if (Yii::$app->session->hasFlash('user_telesale_id')) {
            if (Yii::$app->user->isGuest) {
                $userTelesaleID = Yii::$app->session->getFlash('user_telesale_id');
                $userTelesale = UserTelesale::findOne($userTelesaleID);
                if ($userTelesale) {
                    $this->email = $userTelesale->email;
                    $this->contact_name = $userTelesale->full_name;
                    $this->phone_number = $userTelesale->phone_number;
                    $this->street_address = $userTelesale->street_address;
                }
            }
            if (empty($this->method)) {
                $this->method = 'cod';
            }
        }

        $user = Yii::$app->cart->getUserMobilePayment();
        if(!empty($user))
        {
            $this->loadByUser($user);
        }

        if (!Yii::$app->user->isGuest && $user == null) {
            $user = Yii::$app->user->identity;
            $this->loadByUser($user);
        }

        $orderId = Yii::$app->cart->orderId;
        if ($id = Yii::$app->request->get('orderId')) {
            $orderId = $id;
        }
        if (!Yii::$app->user->isGuest) {
            $orderDetails = $this->getOrderDetails();

            if (empty($orderId)) {
                $orderMeta = ['point_of_sale' => 'shoppingcart'];
                $order = Order::create($orderDetails, Yii::$app->user->id, $orderMeta);
            } else {
                $order = Order::updateDetails($orderId, $orderDetails, Yii::$app->cart->getCost(true));
            }
            if ($order) {
                $orderId = $order->id;
                Yii::$app->cart->orderId = $orderId;
            }
        }

        return $ret;
    }

    private function loadByUser($user)
    {
        $this->user_id = $user->id;
        $this->email = $user->email;

        $lastCodOrder = Order::find()
            ->where([
                'user_id' => $this->user_id,
                'payment_method' => 'cod',
            ])->orderBy('id desc')->one();
        if ($lastCodOrder != null) {
            $orderShipping = $lastCodOrder->orderShipping;
            if ($orderShipping != null) {
                $this->street_address = $orderShipping->street_address;
            }
        }

        $this->contact_name = $user->profile->name;
        $this->phone_number = $user->profile->phone_number;

        if (empty($this->street_address)) {
            $this->street_address = $user->address;
        }
    }

    public function getOrderDetails()
    {
        $orderDetails = [];

        foreach (Yii::$app->cart->getPositions() as $item) {
            if ($item->type == Product::TYPE_COMBO) {
                $combo = Course::findOne($item->id);
                $totalComboDiscount = $item->discountAmount;
                $codeDisCount = $totalComboDiscount - $combo->discountAmount;
                $totalComoboPrice = $item->oldPrice;

                foreach ($item->comboItems as $comboItem) {
                    $pItem = Product::findOne($comboItem->course_id);
                    $discountAmount = $pItem->price - $comboItem->price;

                    if (isset(Yii::$app->controller->settings['code_can_use_for_combo'])) {
                        $codes = explode(',', Yii::$app->controller->settings['code_can_use_for_combo']);
                        if (!empty($codes) && in_array($this->promotion_code, $codes)) {
                            $percent = $pItem->getPrice(false) * 100 / $totalComoboPrice;
                            $itemCodeDiscount = $codeDisCount * $percent / 100;
                            $discountAmount += $itemCodeDiscount;
                        }
                    }

                    $pItem->setDiscountAmount($discountAmount);
                    $pItem->setCombo($item);

                    $orderDetails[] = $pItem;
                }
            } else {
                $orderDetails[] = $item;
            }
        }

        return $orderDetails;
    }

    public function rules()
    {
        return [
            [['method'], 'required', 'message' => 'Bạn chưa chọn hình thức thanh toán'],
            [['email', 'contact_name', 'phone_number'], 'required'],
            ['email', 'email'],
            ['email', 'validateEmail', 'when' => function ($model) {
                return Yii::$app->user->isGuest && $model->method != 'cod';
            }, 'whenClient' => "function (attribute, value) {
                var method = $('input[name=\'PaymentForm[method]\']:radio:checked').val();
                return method !== 'cod';
            }"],
            [['contact_name'], 'string', 'max' => 30],
            [['phone_number'], 'match', 'pattern' => '/^((0[0-9]{9,10})|(\+[0-9]{11,12})|([1-9]{1}[0-9]{10,13}))$/', 'message' => 'Vui lòng nhập đúng số điện thoại'],
            [['street_address'], 'string', 'max' => 255],
            [['email'], 'string', 'max' => 255],
            // rules for COD
            [
                ['street_address'], 'required', 'when' => function ($model) {
                return $model->method == 'cod';
            }, 'whenClient' => "function (attribute, value) {
                    var method = $('input[name=\'PaymentForm[method]\']:radio:checked').val();
                    return method === 'cod';
                }"
            ],
            // rules for mobile card
            [
                ['serviceCode'], 'required', 'when' => function ($model) {
                return $model->method == 'epay' && $model->paidAmount < $model->totalAmount;
            }, 'whenClient' => "function (attribute, value) {
                    var method = $('input[name=\'PaymentForm[method]\']:radio:checked').val();
                    return method === 'epay';
                }", 'message' => 'Bạn chưa chọn loại thẻ'
            ],
            [
                ['pinCode', 'serial'], 'required', 'when' => function ($model) {
                return $model->method == 'epay' && $model->paidAmount < $model->totalAmount;
            }, 'whenClient' => "function (attribute, value) {
                    var method = $('input[name=\'PaymentForm[method]\']:radio:checked').val();
                    return method === 'epay';
                }"
            ],
            [['note', 'promotion_code'], 'string'],
            [['promotion_code'], 'validatePromotion'],
            [['order_id'], 'safe'],
        ];
    }

    public function validateEmail($attribute)
    {
        if (Yii::$app->user->isGuest) {
            if (!$this->hasErrors('email')) {
                $orderId = Yii::$app->cart->orderId;
                if (empty($orderId) && User::find()->where(['email' => $this->email])->exists()) {
                    $this->addError('email', 'Email <font color="black"><b>' . $this->email . '</b></font> đã có tài khoản. Nếu đó là tài khoản của bạn, vui lòng đăng nhập</i>');
                }
            }
        }
    }

    public function attributeLabels()
    {
        return [
            'contact_name' => 'Họ tên',
            'email' => 'Email đăng ký học',
            'phone_number' => 'Số điện thoại',
            'street_address' => 'Địa chỉ',
            'note' => 'Ghi chú',
            'method' => 'Phương thức thanh toán',
            'serviceCode' => 'Chọn loại thẻ',
            'pinCode' => 'Mã thẻ cào',
            'serial' => 'Số seri thẻ',
        ];
    }

    public function beforeValidate()
    {
        // html encode all attributes before validate
        foreach ($this->attributes as $key => $value) {
            $this->{$key} = Html::encode($value);
        }
        return parent::beforeValidate(); // TODO: Change the autogenerated stub
    }

    public function afterValidate()
    {
        $ret = parent::afterValidate();

        if (!$this->hasErrors()) {
            $user = User::findOne(['email' => $this->email]);
            if ($user == null) {
                // create user
                $user = $this->createUser();
                // save to cart
                $userInfo = Yii::$app->cart->getUserInfo();
                $userInfo['user_id'] = $user->id;

                $this->user_id = $user->id;
                // make user login
                if (Yii::$app->request->get('mobile-id') == null) {
                    Yii::$app->user->login($user);
                }
            } else {
                $this->user_id = $user->id;
            }

            $userInfo['email'] = $user->email;
            $userInfo['name'] = $this->contact_name;
            $userInfo['phone_number'] = $this->phone_number;
            $userInfo['address'] = $this->street_address;

            Yii::$app->cart->setUserInfo($userInfo);
        }

        return $ret;
    }

    public function createUser()
    {
        $user = new User([
            'scenario' => 'create',
        ]);
        $time = time();
        $user->load(['User' => [
            'email' => $this->email,
            'username' => $this->email,
            'created_at' => $time,
            'password' => (string)$time,
            'access_token' => SecurityHelper::generateAccessToken($user->id)
        ]]);
        if ($user->create(false)) {
            // trigger job send welcome email
            $user->sendWelcomeEmailConsole();
            // register profile
            $profile = $user->profile;
            $profile->name = $this->contact_name;
            $profile->phone_number = $this->phone_number;

            $profile->save();
        }

        return $user;
    }

    /**
     * @desc call pay funtion from PaymentMethod class
     * @param type $order
     * @return boolean
     */
    public function doPayment($order)
    {
        if (!empty($this->location_id)) {
            $location = Location::findOne($this->location_id);
            $this->city_id = $location->parent_id;
        }

        $paymentMethodModel = PaymentMethod::loadModel($this->method, $this->attributes);
        if (empty($paymentMethodModel)) {
            throw new InvalidParamException('System could not found the payment method: ' . $this->method);
        }
        if ($paymentMethodModel->isPayable()) {
            $paymentMethod = strtolower($order->payment_method);
            if ($paymentMethod != 'cod') {
                $isRedirect = false;
                if ($paymentMethodModel->payment_type === PaymentMethod::PAYMENT_TYPE_REDIRECT) {
                    $paymentMethodModel->returnUrl = Url::toRoute(['/cart/api/response', 'method' => $this->method], true);
                    $isRedirect = true;
                }
                $response = $paymentMethodModel->pay($order);

                if ($isRedirect) {
                    return [
                        'result' => true,
                        'isRedirect' => true,
                        'redirectUrl' => $response,
                    ];
                }
            } else {
                // update user address when method is ghn
                $this->updateUserAddress();
                $response = true;
            }

        } else {
            $response = true;
        }
        //Get response code, handle for E-pay gateway.
        if ($response) {
            //Check error from E-pay charging result
            if ($response['error']) {
                $result = [
                    'result' => false,
                    'errors' => $paymentMethodModel->errors,
                    'object' => $response,
                    'error_code' => -1,
                ];
            } elseif (empty($response['error']) && $paymentMethodModel->continue_pay === true) {
                $this->pinCode = $this->serial = null;
                $result = [
                    'result' => true,
                    'continue_pay' => $paymentMethodModel->continue_pay,
                    'data' => $response
                ];
            } else {
                // success, clear cart
                Yii::$app->cart->emptyCart($this->order_id);
                $result = [
                    'result' => true,
                    'continue_pay' => $paymentMethodModel->continue_pay,
                    'data' => $response
                ];
            }

        } else {
            $result = [
                'result' => false,
                'errors' => $paymentMethodModel->errors
            ];
        }

        return $result;
    }

    public function getShippingAddress()
    {
        if (!empty($this->contact_name)) {
            return [
                'contact_name' => $this->contact_name,
                'phone_number' => $this->phone_number,
                'street_address' => $this->street_address,
                'email' => $this->email,
                'location_id' => $this->location_id,
                'note' => $this->note
            ];
        }

        return null;
    }

    protected function updateUserAddress()
    {
        $userId = Yii::$app->user->id;
        if (!$address = Address::find()->where(['user_id' => $userId])->one()) {
            $address = new Address();
            $address->user_id = $userId;
        }

        $address->attributes = [
            'email' => $this->email,
            'contact_name' => $this->contact_name,
            'phone_number' => $this->phone_number,
            'street_address' => $this->street_address,
            'location_id' => $this->location_id,
        ];

        if (!$address->save()) {
            return false;
        }

        return true;
    }

    public function validatePromotion($attribute)
    {
        // validate Promotion Code
        $promotion = Promotion::find()->andWhere([
            'code' => $this->promotion_code
        ])->one();
        // check code available: not null and to be in valid period
        if ($promotion == null || $promotion->getIsExistOrder($this->order_id) || !$promotion->canUse) {
            // remove promotion code from cart/order
            Yii::$app->cart->removePromotion($this->order_id);
            $this->addError($attribute, "Mã giảm giá {$this->promotion_code} không chính xác hoặc đã được sử dụng.");
            $this->promotion_code = null;
        }
    }
}
