<?php
/**
 * Created by PhpStorm.
 * User: Tan
 * Date: 7/19/2018
 * Time: 2:20 PM
 */

namespace mobile\modules\cart\models\form;

use kyna\payment\models\PaymentMethod;
use kyna\base\models\Location;
use kyna\order\models\Order;
use kyna\promotion\models\Promotion;
use kyna\user\models\Address;
use kyna\user\models\User;
use kyna\user\models\UserTelesale;
use kyna\course\models\Course;

use mobile\modules\cart\models\Product;
use kyna\promo\models\GroupDiscount;
use Yii;
use yii\base\InvalidParamException;
use yii\helpers\Html;
use yii\helpers\Url;


class PaymentFormV3 extends \yii\base\Model
{

    public $method;
    // scenario cod
    public $contact_name;
    public $email;
    public $phone_number;
    public $street_address;
    public $location_id;
    public $note;
    // scenario mobile card
    public $serviceCode;
    public $pinCode;
    public $serial;
    //
    public $promotion_code;
    public $order_id;
    public $user_id;
    //
    public $paidAmount = 0;
    public $totalAmount = 0;
    /**
     * @var
     */

    public $city_id;

    public function init()
    {
        $ret = parent::init();

        if (Yii::$app->session->hasFlash('payment-method')) {
            $this->method = Yii::$app->session->getFlash('payment-method');
        }

        if (Yii::$app->session->hasFlash('user_telesale_id')) {
            if (Yii::$app->user->isGuest) {
                $userTelesaleID = Yii::$app->session->getFlash('user_telesale_id');
                $userTelesale = UserTelesale::findOne($userTelesaleID);
                if ($userTelesale) {
                    $this->email = $userTelesale->email;
                    $this->contact_name = $userTelesale->full_name;
                    $this->phone_number = $userTelesale->phone_number;
                    $this->street_address = $userTelesale->street_address;
                }
            }
            if (empty($this->method)) {
                $this->method = 'cod';
            }
        }

        if (!Yii::$app->user->isGuest) {
            $user = Yii::$app->user->identity;
            $this->user_id = Yii::$app->user->id;
            $this->email = $user->email;

            $lastCodOrder = Order::find()
                ->where([
                    'user_id' => $this->user_id,
                    'payment_method' => 'cod',
                ])->orderBy('id desc')->one();
            if ($lastCodOrder != null) {
                $orderShipping = $lastCodOrder->orderShipping;
                if ($orderShipping != null) {
                    $this->street_address = $orderShipping->street_address;
                }
            }

            $this->contact_name = $user->profile->name;
            $this->phone_number = $user->profile->phone_number;

            if (empty($this->street_address)) {
                $this->street_address = $user->address;
            }
        }

        $orderId = Yii::$app->cart->orderId;

        if ($id = Yii::$app->request->get('orderId')) {
            $orderId = $id;
        }
        // $this->updateOrderFromCart();
        return $ret;
    }

    public function updateOrderFromCart()
    {
        if (!Yii::$app->user->isGuest ) {

            $orderDetails = $this->getOrderDetails();
            if (empty($orderId)) {
                $orderMeta = ['point_of_sale' => 'shoppingcart'];
                $order = Order::create($orderDetails, Yii::$app->user->id, $orderMeta,Order::SCENARIO_CREATE_FRONTEND, Yii::$app->cart->getPromotionCode());

            } else {

                $order = Order::updateDetails($orderId, $orderDetails, Yii::$app->cart->getTotalPriceAfterDiscountAll(), Yii::$app->cart->getPromotionCode());

            }
            if ($order) {
                $orderId = $order->id;
                Yii::$app->cart->orderId = $orderId;
            }
        }
    }

    private function printCart()
    {
        $cartPositions = Yii::$app->cart->positions;
        echo "<pre>";
        foreach ($cartPositions as $position) {
            $tmp =  'quantity :' . $position->quantity . '- price : '  . $position->price . '- discount:' . $position->discountAmount  .= '- voucher discount:' . $position->voucherDiscountAmount .  ' - promotion code : '. $position->promotionCode ;
            var_dump($tmp );

        }
        die();


    }



    public function getOrderDetails()
    {

        $orderDetails = [];

        foreach (Yii::$app->cart->getPositions() as $item) {
            if ($item->type == Product::TYPE_COMBO) {
                $total_combo_price =  $item->getOriginalPrice() - $item->discountAmount ; //tông gia combo da giam chua ap dung

                foreach ($item->comboItems as $comboItem) {
                    $pItem = Product::findOne($comboItem->course_id);

                    $pItem->setCombo($item);
                    $discountAmount = $pItem->price - $comboItem->price;
                    $pItem->setVoucherDiscountAmount( $item->voucherDiscountAmount * ( $comboItem->price /$total_combo_price) );

                    $pItem->setDiscountAmount($discountAmount + $pItem->getVoucherDiscountAmount());

                    $pItem->setPromotionCode($item->getPromotionCode());
                    $orderDetails[] = $pItem;

                }

            } else {
                $new_item = clone $item;
                $new_item->discountAmount = $item->discountAmount + $item->voucherDiscountAmount;
                $orderDetails[] = $new_item;

            }


        }



        //  GroupDiscount::applyGroupDiscountCartItems($orderDetails);

        //   $this->printOderDetail($orderDetails);
        return $orderDetails;
    }

    private function printOderDetail($orderDetails)
    {
        echo '<pre/>';
        foreach ($orderDetails as $position )
        {
            $tmp =  'quantity :' . $position->quantity . '- price : '  . $position->price . '- discount:' . $position->discountAmount .   '-voucher discount:' . $position->voucherDiscountAmount .   ' - promotion code : '. $position->promotionCode ;
            var_dump($tmp );
        }
        die();
    }

    public function rules()
    {
        return [
            [['method'], 'required', 'message' => 'Bạn chưa chọn hình thức thanh toán'],
            [['email', 'contact_name', 'phone_number'], 'required'],
            ['email', 'email'],
            ['email', 'validateEmail', 'when' => function ($model) {
                return Yii::$app->user->isGuest && $model->method != 'cod';
            }, 'whenClient' => "function (attribute, value) {
                var method = $('input[name=\'PaymentForm[method]\']:radio:checked').val();
                return method !== 'cod';
            }"],
            [['contact_name'], 'string', 'max' => 30],
            [['phone_number'], 'match', 'pattern' => '/^((0[0-9]{9,10})|(\+[0-9]{11,12})|([1-9]{1}[0-9]{10,13}))$/', 'message' => 'Vui lòng nhập đúng số điện thoại'],
            [['street_address'], 'string', 'max' => 255],
            [['email'], 'string', 'max' => 255],
            // rules for COD
            [
                ['street_address'], 'required', 'when' => function ($model) {
                return $model->method == 'cod';
            }, 'whenClient' => "function (attribute, value) {
                    var method = $('input[name=\'PaymentForm[method]\']:radio:checked').val();
                    return method === 'cod';
                }"
            ],
            // rules for mobile card
            [
                ['serviceCode'], 'required', 'when' => function ($model) {
                return ($model->method == 'epay' || $model->method == 'ipay') && $model->paidAmount < $model->totalAmount;
            }, 'whenClient' => "function (attribute, value) {
                    var method = $('input[name=\'PaymentForm[method]\']:radio:checked').val();
                    return (method === 'epay' || method === 'ipay');
                }", 'message' => 'Bạn chưa chọn loại thẻ'
            ],
            [
                ['pinCode', 'serial'], 'required', 'when' => function ($model) {
                return ($model->method == 'epay' || $model->method == 'ipay') && $model->paidAmount < $model->totalAmount;
            }, 'whenClient' => "function (attribute, value) {
                    var method = $('input[name=\'PaymentForm[method]\']:radio:checked').val();
                    return (method === 'epay' || method === 'ipay');
                }"
            ],
            [['note', 'promotion_code'], 'string'],
            //[['promotion_code'], 'validatePromotion'],
            [['order_id'], 'safe'],
        ];
    }

    public function validateEmail($attribute)
    {
        if (Yii::$app->user->isGuest) {
            if (!$this->hasErrors('email')) {
                $orderId = Yii::$app->cart->orderId;
                if (empty($orderId) && User::find()->where(['email' => $this->email])->exists()) {
                    $this->addError('email', 'Email <font color="black"><b>' . $this->email . '</b></font> đã có tài khoản. Nếu đó là tài khoản của bạn, vui lòng đăng nhập</i>');
                }
            }
        }
    }

    public function attributeLabels()
    {
        return [
            'contact_name' => 'Họ tên',
            'email' => 'Email đăng ký học',
            'phone_number' => 'Số điện thoại',
            'street_address' => 'Địa chỉ',
            'note' => 'Ghi chú',
            'method' => 'Phương thức thanh toán',
            'serviceCode' => 'Chọn loại thẻ',
            'pinCode' => 'Mã thẻ cào',
            'serial' => 'Số seri thẻ',
        ];
    }

    public function beforeValidate()
    {
        // html encode all attributes before validate
        foreach ($this->attributes as $key => $value) {
            $this->{$key} = Html::encode($value);
        }
        return parent::beforeValidate(); // TODO: Change the autogenerated stub
    }

    public function afterValidate()
    {
        $ret = parent::afterValidate();

        if (!$this->hasErrors()) {
            $user = User::findOne(['email' => $this->email]);
            $isNewUser = true;
            if ($user == null) {
                $isNewUser = true;
                // create user
                $user = $this->createUser();
                // save to cart
                $userInfo = Yii::$app->cart->getUserInfo();
                $userInfo['user_id'] = $user->id;

                $this->user_id = $user->id;
            } else {
                $isNewUser = false;
                $this->user_id = $user->id;
            }
            if($isNewUser) {
                // make user login
                Yii::$app->user->login($user);
            }
            $userInfo['email'] = $user->email;
            $userInfo['name'] = $this->contact_name;
            $userInfo['phone_number'] = $this->phone_number;
            $userInfo['address'] = $this->street_address;

            Yii::$app->cart->setUserInfo($userInfo);
        }

        return $ret;
    }

    public function createUser()
    {
        $user = new User([
            'scenario' => 'create',
        ]);
        $time = time();
        $user->load(['User' => [
            'email' => $this->email,
            'username' => $this->email,
            'created_at' => $time,
            'password' => (string)$time
        ]]);
        if ($user->create(false)) {
            // trigger job send welcome email
            $user->sendWelcomeEmailConsole();
            // register profile
            $profile = $user->profile;
            $profile->name = $this->contact_name;
            $profile->phone_number = $this->phone_number;
            $profile->save();
        }

        return $user;
    }

    /**
     * @desc call pay funtion from PaymentMethod class
     * @param type $order
     * @return boolean
     */
    public function doPayment($order)
    {
        if (!empty($this->location_id)) {
            $location = Location::findOne($this->location_id);
            $this->city_id = $location->parent_id;
        }

        $paymentMethodModel = PaymentMethod::loadModel($this->method, $this->attributes);
        if (empty($paymentMethodModel)) {
            throw new InvalidParamException('System could not found the payment method: ' . $this->method);
        }
        if ($paymentMethodModel->isPayable()) {
            $paymentMethod = strtolower($order->payment_method);
            if ($paymentMethod != 'cod') {
                $isRedirect = false;
                if ($paymentMethodModel->payment_type === PaymentMethod::PAYMENT_TYPE_REDIRECT) {
                    $paymentMethodModel->returnUrl = Url::toRoute(['/cart/api/response', 'method' => $this->method], true);
                    $isRedirect = true;
                }
                $response = $paymentMethodModel->pay($order);

                if ($isRedirect) {
                    if(!$response){
                        return [
                            'result' => false,
                            'isRedirect' => true,

                        ];
                    }
                    return [
                        'result' => true,
                        'isRedirect' => true,
                        'redirectUrl' => $response,
                    ];
                }
            } else {
                // update user address when method is ghn
                $this->updateUserAddress();
                $response = true;
            }

        } else {
            $response = true;
        }
        //Get response code, handle for E-pay gateway.
        if ($response) {
            //Check error from E-pay charging result
            if ($response['error']) {
                $result = [
                    'result' => false,
                    'errors' => $paymentMethodModel->errors,
                    'object' => $response,
                    'error_code' => -1,
                ];
            } elseif (empty($response['error']) && $paymentMethodModel->continue_pay === true) {
                $this->pinCode = $this->serial = null;
                $result = [
                    'result' => true,
                    'continue_pay' => $paymentMethodModel->continue_pay,
                    'data' => $response
                ];
            } else {
                // success, clear cart
                Yii::$app->cart->emptyCart($this->order_id);
                $result = [
                    'result' => true,
                    'continue_pay' => $paymentMethodModel->continue_pay,
                    'data' => $response
                ];
            }

        } else {
            $result = [
                'result' => false,
                'errors' => $paymentMethodModel->errors
            ];
        }

        return $result;
    }

    public function getShippingAddress()
    {
        if (!empty($this->contact_name)) {
            return [
                'contact_name' => $this->contact_name,
                'phone_number' => $this->phone_number,
                'street_address' => $this->street_address,
                'email' => $this->email,
                'location_id' => $this->location_id,
                'note' => $this->note
            ];
        }

        return null;
    }

    protected function updateUserAddress()
    {
        $userId = Yii::$app->user->id;
        if (!$address = Address::find()->where(['user_id' => $userId])->one()) {
            $address = new Address();
            $address->user_id = $userId;
        }

        $address->attributes = [
            'email' => $this->email,
            'contact_name' => $this->contact_name,
            'phone_number' => $this->phone_number,
            'street_address' => $this->street_address,
            'location_id' => $this->location_id,
        ];

        if (!$address->save()) {
            return false;
        }

        return true;
    }

    public function validatePromotion($attribute)
    {
        // validate Promotion Code
        $promotion = Promotion::find()->andWhere([
            'code' => $this->promotion_code
        ])->one();
        // check code available: not null and to be in valid period
        if ($promotion == null || $promotion->getIsExistOrder($this->order_id) || !$promotion->canUse) {
            // remove promotion code from cart/order
            Yii::$app->cart->removePromotion($this->order_id);
            $this->addError($attribute, "Mã giảm giá {$this->promotion_code} không chính xác hoặc đã được sử dụng.");
            $this->promotion_code = null;
        }
    }
}
