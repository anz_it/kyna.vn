<?php
/**
 * Created by PhpStorm.
 * User: khanhphan
 * Date: 9/27/17
 * Time: 1:12 AM
 */

namespace mobile\modules\v1\controllers;


use kyna\order\models\search\OrderSearch;
use mobile\components\ActiveController;
use mobile\components\LoginRequireTrait;
use mobile\components\RestController;
use mobile\helpers\SecurityHelper;
use mobile\models\ActiveCodeForm;
use mobile\models\CourseElastic;
use mobile\models\LoginForm;
use mobile\models\Order;
use mobile\models\RegistrationForm;
use mobile\models\User;
use mobile\models\UserCourse;
use Yii;
use yii\rest\Controller;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use mobile\models\PushNotification;
class AuthController extends GuestController
{
    use LoginRequireTrait;

    public function actionUserCourses()
    {
        $user_courses = UserCourse::find()
            ->where(['user_id' => Yii::$app->user->getId(), 'is_activated' => UserCourse::STATUS_ACTIVE ])->orderBy(['created_time'=>SORT_DESC])->all();
        /*$results = array();
        foreach ($user_courses as $user_course){
            if(!empty($user_course->course)){
               $results[] = $user_course;
            }
        }*/
        return $user_courses;

    }


    public function actionOrder($is_completed = 1, $page = 1, $limit = 10)
    {
        $formatter = Yii::$app->formatter;

            $searchModel = new OrderSearch();

            $searchModel->user_id = Yii::$app->user->id;
            if($is_completed != 1) {
                $statuses = [
                    OrderSearch::ORDER_STATUS_PAYMENT_FAILED,
                    OrderSearch::ORDER_STATUS_REQUESTING_PAYMENT,
                    OrderSearch::ORDER_STATUS_NEW,
                    OrderSearch::ORDER_STATUS_PENDING_CONTACT,
                    OrderSearch::ORDER_STATUS_PENDING_PAYMENT,
                    OrderSearch::ORDER_STATUS_DELIVERING
                ];
            }
            else
            {
                $statuses = [OrderSearch::ORDER_STATUS_COMPLETE];
            }
            $searchModel->load(array('page'=>$page));

            $searchResult =  $searchModel->search($statuses);
            $searchResult->pagination->pageSize = $limit;
            $listOrder = $searchResult->getModels();

            $orders = array();
            foreach ($listOrder as $item )
            {
                $order = new \stdClass();
                $order->id = $item->id;
                $order->created_time = $formatter->asDatetime($item->created_time);
                $order->total = (int)$item->total;
                $order->paymentMethodName = $item->paymentMethodName;
                $order->statusLabel = $item->statusLabel;
                $order->payment_enabled = 0;
                if (in_array($item->status, [
                    Order::ORDER_STATUS_PAYMENT_FAILED,
                    Order::ORDER_STATUS_REQUESTING_PAYMENT,
                    Order::ORDER_STATUS_NEW]))
                {
                    $order->payment_enabled = 1;
                }
                $order_details = array();
                foreach ($item->details as $order_detail)
                {
                    if ($order_detail->courseInfo)
                    {
                        $course_detail = new \stdClass();
                        $course_detail->image_url = $order_detail->courseInfo->image_url;
                        $course_detail->title = $order_detail->courseInfo->name;
                        $course_detail->author_name = !empty($order_detail->course->teacher) ? $order_detail->course->teacher->profile->name : '';
                        $course_detail->author_title = !empty($order_detail->course->teacher) ? $order_detail->course->teacher->title : '';
                        $course_detail->price = $order_detail->unit_price;
                        $order_details[] = $course_detail;
                    }

                }
                $order->order_details = $order_details;
                $orders[] = $order;
            }
            return array( 'total_count'=>$searchResult->getTotalCount(),'list_order'=>$orders);
    }

    public function actionActiveCode()
    {
        $model = new ActiveCodeForm();
        if ($model->load(Yii::$app->getRequest()->getBodyParams(), '') && $model->validate() && $model->complete()) {
            return "Kích hoạt thành công";

        } else {
            throw new BadRequestHttpException("Mã kích hoạt không hợp lệ.");
        }
    }
    public function actionPushNotify()
    {
        $params = Yii::$app->request->bodyParams;

        if(!empty($params['os_type']))
          $os_type = $params['os_type'];
        else
         $os_type = '';

        if(!empty($params['device_model']))
          $device_model = $params['device_model'];
        else
            $device_model = '';

        $user_id = Yii::$app->user->getId();
        $push_notify = PushNotification::find()->andWhere(['user_id'=>$user_id])->one();
        if(empty($push_notify)) {
            $push_notify = new PushNotification();
            $push_notify->user_id = $user_id;
            $push_notify->os_type = $os_type;
            $push_notify->device_model = $device_model;
            $push_notify->token = Yii::$app->user->identity->access_token;
            $push_notify->save();
        }
        else
        {
            if(!empty($os_type))
              $push_notify->os_type = $os_type;
            if(!empty($device_model))
                $push_notify->device_model = $device_model;
            $push_notify->token = Yii::$app->user->identity->access_token;
            $push_notify->save();

        }
        return $push_notify;
    }
    public function actionGetUserPushNotify()
    {
        $user_id = Yii::$app->user->getId();
        $push_notify = PushNotification::find()->andWhere(['user_id'=>$user_id])->one();
        if(!empty($push_notify))
            return $push_notify;
        else
            throw new NotFoundHttpException("Không tìm thấy push notification.");
    }
}