<?php
/**
 * Created by PhpStorm.
 * User: Tan
 * Date: 1/25/2018
 * Time: 4:11 PM
 */

namespace  mobile\modules\v1\controllers;

use mobile\components\RestController;
use mobile\components\LoginRequireTrait;
use mobile\models\Quiz;
use mobile\models\QuizQuestion;
use mobile\models\QuizSession;
use mobile\models\QuizSessionAnswer;
use yii\caching\TagDependency;
use common\helpers\CacheHelper;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
class QuizController extends RestController
{
    use LoginRequireTrait;
    public function actionStartQuiz($id)
    {
        $quiz = Quiz::findOne($id);
        if(empty($quiz)){
            throw new NotFoundHttpException("Không tìm thấy bài quiz.");
        }

        $userId = \Yii::$app->user->getId();
        $status = \Yii::$app->request->get('status', false);
        $reload_page = false;
        //Do Again
        if ($status == QuizSession::STATUS_REDO) {
            $redoSession = QuizSession::find()->where([
                'user_id' => $userId,
                'quiz_id' => $id,
                'status' => [QuizSession::STATUS_SUBMITTED, QuizSession::STATUS_WAITING_FOR_MARK, QuizSession::STATUS_MARKED]
            ])->one();
            if (!empty($redoSession)) {
                $redoSession->status = $status;
                $redoSession->is_passed = QuizSession::BOOL_NO;
                $redoSession->start_time = null;
                $redoSession->save();
                unset($status);
            }
            $reload_page = true;
        }
        //End Do Again
        $session = $this->_loadSessionModel($quiz, $userId);

        if (!$reload_page && $session->start_time == null) {
            $session->start_time = time();
            $session->save(false);
        }
       /* $session->save(false);
        if ($session->start_time == null) {
        $session->start_time = time();
        $session->save(false);
        }*/
        $answerModelQuery = $session->getAnswerModels();
        $answerModelQuery->andWhere(['or', 'parent_id is null', 'parent_id = 0']);

        $list_answer = $answerModelQuery->all();
        $answers = array();
        foreach ($list_answer as $answer)
        {
            $item = (object)$answer->attributes;
            $question = $answer->question;
            $item->question_type = $question->type;
            if($question->type == QuizQuestion::TYPE_FILL_INTO_DOTS)
            {
                $children = QuizSessionAnswer::find()->where(['quiz_session_id' => $answer->quiz_session_id, 'parent_id' => $answer->quiz_detail_id])->all();
                $item->children =  $children;
            }
            $answers[] = $item;
        }
        $callable = function () use ($answers) {
            return $answers;
        };
        $cacheTags = new TagDependency(['tags' => QuizSessionAnswer::tableName() . '-' . $id . '-' . $userId]);
        $answerModels = CacheHelper::getQueryCacheByTag($callable, $cacheTags);

        return $answerModels;

    }
    public function actionFinishQuiz($id)
    {
        $quiz = Quiz::findOne($id);
        if(empty($quiz))
        {
            throw new NotFoundHttpException("Không tìm thấy bài quiz.");
        }
        $userId = \Yii::$app->user->getId();
        $session = $this->_loadSessionModel($quiz, $userId);
        $postData = \Yii::$app->getRequest()->getBodyParams();
        $answers = array('QuizSession'=>array('answers'=>$postData));
        if ($session->load($answers)) {
            $session->calculateScore();
            $session->finish();
            $session->save(false);
        }
         elseif ($session->start_time == null) {
            $session->start_time = time();
            $session->save(false);
        }
        //var_dump($session->time_remaining); exit;
        return $session;
    }

    /**
     * @param $quiz Quiz
     * @param $userId
     * @return QuizSession
     */
    private function _loadSessionModel($quiz, $userId)
    {
        $session = $quiz->getSession($userId);

        $now = time();
        if ($session->status == QuizSession::STATUS_DEFAULT) {
            if ($session->start_time == $now) {
                $session->time_remaining = $session->duration;
                $session->status = QuizSession::STATUS_DOING;
            } elseif ($session->time_remaining != null && $session->time_remaining <= 0) {
                $session->time_remaining = 0;
                $session->status = QuizSession::STATUS_SUBMITTED;
            } else {
                $session->time_remaining = $session->duration - ($now - $session->start_time);
                $session->status = QuizSession::STATUS_DOING;
            }
            $session->last_interactive = $now;
            $session->save(false);
        }

        return $session;
    }
    public function actionResult($quiz_session_id)
    {
        $session = QuizSession::findOne($quiz_session_id);
        if ($session == null) {
            throw new NotFoundHttpException;
        }

        $answerModelQuery = $session->getAnswerModels();
        $answerModelQuery->andWhere(['or', 'parent_id is null', 'parent_id = 0']);
        // one by one question

        $answerModels = $answerModelQuery->all();
        $result = array();
        foreach ($answerModels as $answerModel)
        {
            if($answerModel->question->type == QuizQuestion::TYPE_FILL_INTO_DOTS) {
                $user_answer = QuizSessionAnswer::find()->where(['quiz_session_id' => $answerModel->quiz_session_id, 'parent_id' => $answerModel->quiz_detail_id])->all();
                $answers = array();
                foreach ($user_answer as $user_ans)
                {
                    $answers[] = $user_ans->question->answers;
                }
                $result[] = array('question'=> $answerModel->question, 'user_answer'=> $user_answer ,'answers'=> $answers);
            }
            else if($answerModel->question->type == QuizQuestion::TYPE_WRITING)
            {
                $answers = $answerModel->question->answer_explain;
                $result[] = array('question'=> $answerModel->question, 'user_answer'=> $answerModel ,'answers'=> $answers);
            }
            else {
                $answers = $answerModel->question->answers;
                $result[] = array('question'=> $answerModel->question, 'user_answer'=> $answerModel ,'answers'=> $answers);
            }

        }

        return $result;
    }
}