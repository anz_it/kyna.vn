<?php
/**
 * Created by PhpStorm.
 * User: truongn
 * Date: 11/20/17
 * Time: 10:10 AM
 */

namespace mobile\modules\v1\controllers;


use mobile\components\LoginRequireTrait;
use mobile\components\RestController;
use mobile\models\ChangePassword;
use mobile\models\ChangePasswordCod;
use mobile\models\Profile;
use mobile\models\ProfileForm;
use kyna\gamification\models\UserPoint;
use kyna\gamification\models\search\UserPointHistorySearch;
use kyna\course\models\Course;
use kyna\base\models\Location;
use yii\web\NotFoundHttpException;
use kyna\gamification\models\Gift;
use kyna\gamification\models\GiftContent;
use kyna\gamification\models\UserGift;
use kyna\gamification\models\UserPointHistory;
use kyna\promotion\models\PromotionSearch;
use mobile\models\GiftSearch;
use kyna\promotion\models\Promotion;
use Yii;

use yii\web\UploadedFile;
use common\helpers\CDNHelper;
use common\lib\CDNImage;

class ProfileController extends RestController
{
    use LoginRequireTrait;

    public function actionGet()
    {
        $model = new Profile();
        if(!empty($model->location_id))
        {
            $location = $this->getLocation($model->location_id);
            if(!empty($location))
            {
                $model->location_name = $location->name;
                if(!empty($location->parent_id))
                {
                    $city = $this->getLocation($location->parent_id);
                    if(!empty($city))
                    {
                        $model->city_name = $city->name;
                        $model->city_id = $city->id;
                    }
                }
            }
        }
        if(empty($model->avatar)){
            $model->avatar = '/src/img/default.png';
        }
        return $model;
    }

    public function actionUpdate()
    {
        $profile = new Profile();
        if ($profile->load(\Yii::$app->request->post(), '') && $profile->validate()) {
            $profile->save();
            return $profile;
        } else {
            return $profile;
        }
    }

    public function actionChangePassword()
    {
        $model = new ChangePassword();
        if ($model->load(\Yii::$app->request->post(), '') && $model->validate()) {
            $model->save();
            return "success";
        } else {
            return $model;
        }
    }

    public function actionChangePasswordCod()
    {
        $model = new ChangePasswordCod();
        if ($model->load(\Yii::$app->request->post(), '') && $model->validate()) {
            $model->save();
            return "success";
        } else {
            return $model;
        }
    }

    public function actionGetKpoint()
    {

        $userPoint = UserPoint::findOne(\Yii::$app->user->id);
        return $userPoint != null ? $userPoint->k_point : 0;
    }
    public function actionHistoryKpoint($page = 1, $limit = 10)
    {
        $userPoint = UserPoint::findOne(\Yii::$app->user->id);
        $totalPoint = $userPoint != null ? $userPoint->k_point : 0;
        $searchModel = new UserPointHistorySearch();
        $searchModel->user_id = \Yii::$app->user->id;
        $result = $searchModel->search(array('page'=>$page));
        $result->pagination->pageSize = $limit;
        $listHistory = array();
        $searchResult = $result->getModels();
        if(!empty($searchResult))
        {

            foreach ($searchResult as $res)
            {
                $history = (object)$res->attributes;
                $history->created_time = \Yii::$app->formatter->asDatetime($res->created_time);
                $history->usable_date = \Yii::$app->formatter->asDatetime($res->usable_date);
                $signNumber = false;
                switch ($res->type) {
                    // + point from mission
                    case UserPointHistory::TYPE_MISSION:
                        $signNumber = true;
                        break;
                    // - point when exchange gift
                    case UserPointHistory::TYPE_EXCHANGE_GIFT:
                        break;

                    // +/- point manually by admin
                    case UserPointHistory::TYPE_MANUAL:
                        if ($res->k_point > 0){
                            $signNumber = true;
                        }
                        break;
                    default:
                        break;
                }
                $history->sign_number = $signNumber;
                $history->referenceText = $res->referenceText;
                $listHistory[] = $history;
            }
        }

        return array('total_kpoint'=>$totalPoint, 'total_count'=>$result->getTotalCount(),'list_history'=>$listHistory);
    }
    public function actionMissionSumary($course_id)
    {
        $course = Course::findOne($course_id);
        list($finishedMissions, $notFinishedMissions) = $course->getSummaryMissionByUser(\Yii::$app->user->id);
        return array('finishMissions'=>$finishedMissions, 'notFinishedMissions'=>$notFinishedMissions);
    }
    public function actionListGiftExchange($page = 1, $limit = 10)
    {
        $userPoint = UserPoint::findOne(\Yii::$app->user->id);
        $searchModel = new GiftSearch();
        $searchModel->status = GiftSearch::STATUS_ACTIVE;
        $result  = $searchModel->search(array('page'=>$page));
        $result->pagination->pageSize = $limit;
        $gifts = $result->getModels();
        $listGift = array();
        if(!empty($gifts))
        {
            foreach ($gifts as $gift)
            {
                $giftItem = (object)$gift->attributes;

                $giftItem->created_time = \Yii::$app->formatter->asDatetime($giftItem->created_time);
                $giftItem->updated_time = \Yii::$app->formatter->asDatetime($giftItem->updated_time);
                if(!empty($userPoint) && $userPoint->k_point_usable >= $giftItem->k_point)
                    $giftItem->kpoint_need_more = 0;
                else
                $giftItem->kpoint_need_more = $giftItem->k_point - ($userPoint != null ? $userPoint->k_point_usable : 0);

                $giftItem->apply_text = "";
                if(!empty($gift->giftContent))
                    if(!empty($gift->giftContent->min_amount))
                     $giftItem->apply_text = 'Áp dụng cho '. ($gift->giftContent->discount_type == Promotion::KIND_COURSE_APPLY ? 'khóa học' : 'đơn hàng').' có giá trị » '. \Yii::$app->formatter->asCurrency($gift->giftContent->min_amount) ;

                $listGift[] = $giftItem;
            }
        }
        $kpoint_total = $userPoint != null ? $userPoint->k_point : 0;
        $kpoint_usable =  $userPoint != null ? $userPoint->k_point_usable : 0 ;
        return array('total_kpoint'=> $kpoint_total, 'kpoint_usable' =>$kpoint_usable ,'total_count'=>$result->getTotalCount(), 'list_gift'=>$listGift);
    }
    public function actionGiftConfirm($id)
    {
        $model = Gift::findOne($id);

        if (empty($model)) {
            throw new NotFoundHttpException();
        }

            $userPoint = UserPoint::findOne(\Yii::$app->user->id);
            if ($userPoint == null || $userPoint->k_point < $model->k_point) {
                return [
                    'result' => false,
                    'message' => 'Không đủ điểm để quy đổi'
                ];
            }

            /* @var $giftContent GiftContent */
            $giftContent = $model->giftContent;
            $promotion = $giftContent->generatePromotion();
            if (!empty($promotion)) {
                $userGift = new UserGift();
                $userGift->user_id = Yii::$app->user->id;
                $userGift->code = $promotion->code;
                $userGift->gift_id = $model->id;
                $userGift->save(false);

                $userPoint->k_point -= $model->k_point;
                $userPoint->k_point_usable -= $model->k_point;
                if ($userPoint->save()) {
                    $userPointHistory = new UserPointHistory();
                    $userPointHistory->user_id = $userPoint->user_id;
                    $userPointHistory->type = UserPointHistory::TYPE_EXCHANGE_GIFT;
                    $userPointHistory->k_point = -1 * $model->k_point;
                    $userPointHistory->description = "Đổi thành công quà: " . $model->title;
                    $userPointHistory->save();
                }
                return [
                        'result' => true,
                        'gift' => $model,
                        'code' => $promotion->code,

                ];

            }
            return [
                'result' => false,
                'message' => 'Có lỗi trong quá trình đổi'
            ];

    }
    public function actionGetGifts($page = 1, $limit = 10)
    {
        $searchModel = new PromotionSearch();
        $searchModel->type = null;
        $searchModel->user_id = \Yii::$app->user->id;
        $searchResult = $searchModel->search(array('page'=>$page));
        $searchResult->pagination->pageSize = $limit;
        $listResult = $searchResult->getModels();
        $gifts = array();
        foreach ($listResult as $item)
        {
            $gift = (object)$item->attributes;
            $gift->title = $item->gift != null ? $item->gift->title : '';
            $gift->apply_text = "";
            if (!empty($gift->min_amount))
                  $gift->apply_text ='Áp dụng cho '. ($item->type == Promotion::KIND_COURSE_APPLY ? 'khóa học' : 'đơn hàng').' có giá trị » '. \Yii::$app->formatter->asCurrency($item->min_amount) ;

            $gift->used_status = $item->checkGiftCanUse() ? 0 : 1;
            $gift->expiration_date = date("H\hi\, d.m.Y", $gift->end_date);
            $gifts[] = $gift;
        }
        return array('total_count'=>$searchResult->getTotalCount(),'list_gift'=> $gifts);

    }
    public function actionUploadAvatar()
    {
        $user = \Yii::$app->user->identity;
        if ($avatarUrl = $this->_uploadImage($user, 'avatar')) {
            $user->avatar = $avatarUrl;
        }
       if(!empty($avatarUrl)) {
           if ($user->save()) {
               $resizedAvatar = CDNHelper::image($user->avatar, [
                   'resizeMode' => 'crop',
                   'size' => CDNHelper::IMG_SIZE_AVATAR_LARGE,
                   'returnMode' => 'url',
               ]);
               \Yii::$app->session->set('lastUploadFiles', [$resizedAvatar]);
           }
           if (!empty($resizedAvatar))
               return array('uploaded' => 1, 'image_url' => $resizedAvatar);
       }
        return array('uploaded'=>0);
    }

    private function _uploadImage($model, $attribute)
    {
        $files = UploadedFile::getInstancesByName($attribute);
        if (!sizeof($files)) {
            $oldAttributes = $model->oldAttributes;

            return isset($oldAttributes[$attribute]) ? $oldAttributes[$attribute] : false;
        }

        $file = $files[0];
        $cdnImage = new CDNImage($model, $attribute);
        $result = $cdnImage->upload($file);

        return $result;
    }
    private function getLocation($location_id)
    {
        if(empty($location_id))
            throw new NotFoundHttpException("Không tìm thấy địa chỉ.");

        $location= Location::find()->where(['id'=>$location_id,'status'=>1])->one();
        if(!empty($location))
            return $location;
        else
            throw new NotFoundHttpException("Không tìm thấy địa chỉ.");
    }
}