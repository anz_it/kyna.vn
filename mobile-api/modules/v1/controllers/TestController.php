<?php
/**
 * Created by PhpStorm.
 * User: khanhphan
 * Date: 12/19/17
 * Time: 10:45 AM
 */

namespace mobile\modules\v1\controllers;


use mobile\components\RestController;
use mobile\components\HttpBearerAuth;
use Yii;

class TestController extends  RestController
{

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => HttpBearerAuth::className(),
            'optional' => '*'
        ];
        return $behaviors;
    }

    public function actionTest1()
    {

        if (Yii::$app->user->isGuest) {
            return ['message' => 'Toi la guest'];
        }
        return ['message' => 'User ID của tôi là '.Yii::$app->user->id];
    }

    public function actionTest2()
    {
        if (Yii::$app->user->isGuest) {
            return ['message' => 'Toi la guest'];
        }
        return ['message' => 'User ID của tôi là '.Yii::$app->user->id];
    }
}