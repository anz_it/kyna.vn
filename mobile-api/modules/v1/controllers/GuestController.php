<?php

namespace mobile\modules\v1\controllers;

use common\helpers\DocumentHelper;
use dektrium\user\clients\Facebook;
use dektrium\user\helpers\Password;
use dektrium\user\models\RecoveryForm;
use Facebook\FacebookClient;
use kyna\settings\models\Setting;
use kyna\user\models\Profile;
use mobile\components\LoginRequireTrait;
use mobile\components\RestController;
use mobile\helpers\SecurityHelper;
use mobile\models\Banner;
use mobile\models\CartForm;
use mobile\models\Category;
use mobile\models\CategoryDetail;
use mobile\models\Course;
use mobile\models\CourseCombo;
use mobile\models\CourseDetail;
use mobile\models\CourseElastic;
use mobile\models\FacebookRegisterForm;
use mobile\models\ForgotPassword;
use mobile\models\Location;
use mobile\models\LoginForm;
use mobile\models\LoginOtpForm;
use mobile\models\Notification;
use mobile\models\OtpForm;
use mobile\models\RegistrationForm;
use mobile\models\ShoppingForm;
use mobile\models\Tag;
use mobile\models\Teacher;
use mobile\models\User;
use mobile\models\UserCourse;
use Psr\Log\LoggerTrait;
use yii\filters\AccessControl;
use yii\filters\auth\HttpBearerAuth;
use yii\helpers\StringHelper;
use yii\helpers\Url;
use yii\rest\Controller;
use Yii;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;
use mobile\models\PushNotification;
class GuestController extends RestController
{
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => \mobile\components\HttpBearerAuth::className(),
            'optional' => '*'
        ];

     /*   $behaviors['cached'] = [
            'class' => 'yii\filters\PageCache',
           'only' => ['course-detail','home-info','free-course','list-course-by-category','search-course','free-course','combo-course','video-course','get-teacher','categories'],
            'duration' => 300,
            'variations' => [
                Yii::$app->request->get()
            ],

        ]; */
        return $behaviors;
    }

    public function actionHomeInfo()
    {
        $categories = Category::find()
            ->where(['status' => Category::STATUS_ACTIVE])
            ->andWhere(['is_deleted' => false])
            ->andWhere(['parent_id' => 0])
            ->orderBy('order')
            ->all();

        $banners = Banner::find(!['mobile_image_url' => null])
            ->where(['type' => Banner::TYPE_APP_HOME_SLIDER])
            ->andWhere(['status' => Banner::STATUS_ACTIVE])->all();


        $courses = CourseElastic::getFeatureCourse();
        /*  $what_you_learn = [['name' => 'Day con hoc toan voi Monkey Junior', 'id' => 1, 'image_url' => '/uploads/courses/751/img/image_url.jpg'],
              ['name' => 'Day con hoc toan voi Monkey Junior', 'id' => 1, 'image_url' => '/uploads/courses/751/img/image_url.jpg'],
              ['name' => 'Day con hoc toan voi Monkey Junior', 'id' => 1, 'image_url' => '/uploads/courses/751/img/image_url.jpg'],
              ['name' => 'Day con hoc toan voi Monkey Junior', 'id' => 1, 'image_url' => '/uploads/courses/751/img/image_url.jpg'],
              ['name' => 'Day con hoc toan voi Monkey Junior', 'id' => 1, 'image_url' => '/uploads/courses/751/img/image_url.jpg']]; */
        //   $courses = Course::find(['status' => Course::STATUS_ACTIVE, 'is_hot' => true,'is_deleted' =>false ])->limit(40)->all();

        $what_you_learn = Tag::find()->where(['is_mobile' => Tag::BOOL_YES, 'status' => Tag::STATUS_ACTIVE, 'is_deleted' => Tag::BOOL_NO])->all();
        $result = ['categories' => $categories, 'banners' => $banners, 'courses' => $courses, 'what_you_learn' => $what_you_learn];

        return $result;
    }


    public function actionListCourseByCategory()
    {
        $get = Yii::$app->request->get();
        if (!isset($get['limit'])) {
            $get['limit'] = Yii::$app->params['limit_record'];
        }
        if (!isset($get['offset'])) {
            $get['offset'] = 0;
        }
        $courses = CourseElastic::getListCourse($get, $get['offset'], $get['limit']);
        if (!isset($courses) || count($courses) == 0) {
            throw new NotFoundHttpException('Not found');
        }
        $sub_categories = Category::find()
            ->where(['status' => Category::STATUS_ACTIVE])
            ->andWhere(['parent_id' => $get['catId']])
            ->limit(Yii::$app->params['limit_record'])->all();
        $result = ['courses' => $courses, 'categories' => $sub_categories];
        return $result;
    }

    public function actionCourseDetail($id , $type = Course::TYPE_VIDEO)
    {
        if($type == Course::TYPE_COMBO)
        {
            $course = CourseCombo::findOne($id);

        } else {
            $course = CourseDetail::findOne($id);
        }
        if(empty($course))
        {
            throw new NotFoundHttpException("Không tìm thấy khóa học.");
        }
        return $course;


    }


    public function actionFreeCourse()
    {
        $get = Yii::$app->request->get();
        if (!isset($get['limit'])) {
            $get['limit'] = Yii::$app->params['limit_record'];
        }
        if (!isset($get['offset'])) {
            $get['offset'] = 0;
        }
        $get['facets']['discount'][0] = 0;
        $result = CourseElastic::getListCourse($get, $get['offset'], $get['limit']);
        return $result;

    }

    public function actionComboCourse()
    {
        $get = Yii::$app->request->get();
        if (!isset($get['limit'])) {
            $get['limit'] = Yii::$app->params['limit_record'];
        }

        $get['course_type'] = 2;
        $result = CourseElastic::getListCourse($get, $get['offset'], $get['limit']);
        return $result;

    }

    public function actionVideoCourse()
    {
        $get = Yii::$app->request->get();
        if (!isset($get['limit'])) {
            $get['limit'] = Yii::$app->params['limit_record'];
        }
        if (!isset($get['offset'])) {
            $get['offset'] = 0;
        }
        $get['course_type'] = 3;
        $result = CourseElastic::getListCourse($get, $get['offset'], $get['limit']);
        return $result;

    }

    public function actionSingleCourse()
    {
        $get = Yii::$app->request->get();
        if (!isset($get['limit'])) {
            $get['limit'] = Yii::$app->params['limit_record'];
        }
        if (!isset($get['offset'])) {
            $get['offset'] = 0;
        }
        $get['course_type'] = 1;
        $result = CourseElastic::getListCourse($get, $get['offset'], $get['limit']);
        return $result;

    }

    public function actionSearchCourse()
    {
        $get = Yii::$app->request->get();
        if (!isset($get['limit'])) {
            $get['limit'] = Yii::$app->params['limit_record'];
        }
        if (!isset($get['offset'])) {
            $get['offset'] = 0;
        }
        $result = CourseElastic::getListCourse($get, $get['offset'], $get['limit']);
        return $result;
    }

    public function actionGetTeacher()
    {
        $get = Yii::$app->request->get();
        if (!isset($get['id'])) {
            return new BadRequestHttpException("required id field");
        }
        $teacher = Teacher::findOne($get['id']);
        if(empty($teacher))
            throw new NotFoundHttpException("Không tìm thấy giảng viên.");
        return $teacher;

    }

    public function actionLogin()
    {
        $model = new LoginForm();
        $post_data = Yii::$app->getRequest()->getBodyParams();
        $force_login = false;
        if(!empty( $post_data['force_login'])){
            $force_login = true;
        }

        if($force_login){
            $setting_email = Setting::find()->andWhere(['key' => 'email_force_login', 'is_deleted' => 0])->one();
            if(!empty($setting_email)) {
                $email = $post_data['email'];
                if ($email == trim($setting_email->value)) {
                    $user = User::findOne(['email' => $email]);
                    if (!empty($user)) {
                        $result = Yii::$app->user->login($user);
                        if ($result) {
                            $user->access_token = SecurityHelper::generateAccessToken($user->id);
                            $user->save(false);
                            $avatar = Yii::$app->user->identity->avatarImage;
                            return [
                                'access_token' => Yii::$app->user->identity->access_token,
                                'email' => Yii::$app->user->identity->email,
                                'name' => Yii::$app->user->identity->profile->name,
                                'signature' => SecurityHelper::createSignature(Yii::$app->user->identity->access_token),
                                'avatar_url' => $avatar
                            ];
                        }

                    }
                }
            }
        }

        if ($model->load(Yii::$app->getRequest()->getBodyParams(), '') && $model->login()) {
            $avatar = Yii::$app->user->identity->avatarImage;
            return [
                'access_token' => Yii::$app->user->identity->access_token,
                'email' => Yii::$app->user->identity->email,
                'name' => Yii::$app->user->identity->profile->name,
                'signature' => SecurityHelper::createSignature(Yii::$app->user->identity->access_token),
                'avatar_url' => $avatar
            ];
        } else {
            $model->validate();
            return $model;
        }
    }


    public function actionLoginByFacebook()
    {
        $postData = Yii::$app->getRequest()->getBodyParams();
        if (!isset($postData['access_code'])) {
            throw new BadRequestHttpException("Access code is required");
        }

        $model = new FacebookRegisterForm();
        if ($model->load(Yii::$app->getRequest()->getBodyParams(), '') && $model->validate() && $model->getFacebookInfo()) {
            $user = User::findOne(['email' => $model->email]);
            $profile_updated = true;
            if($model->email === $model->id . "@facebook.com")
            {
                $profile_updated = false;
            }
            if (isset($user)) {
                $user->access_token = SecurityHelper::generateAccessToken($user->id);
                if (!isset($user->gender) || empty($user->profile->phone_number) || strpos($user->email, 'facebook.com') !== false) {
                    $profile_updated = false;
                }
                $user->save(false);
            } else { //create new user
                $user = new User();
                $user->setScenario('register');
                $user->email = $model->email;
                $user->username = $model->email;

                $user->password = uniqid('', true);
                if (!$user->register()) {
                    return $user;
                } else {
                    $profile = $user->profile;
                    if ($profile == null) {
                        $profile = new Profile();
                        $profile->user_id = $user->id;
                    }
                    $profile->facebook_id = $model->id;
                    $profile->name = $model->name;
                    $profile->save(false);
                }
                $user->access_token = SecurityHelper::generateAccessToken($user->id);
                $user->save(false);
                $profile_updated = false;
            }
            return [
                'access_token' => $user->access_token,
                'email' => $user->email,
                'name' => $user->profile->name,
                'profile_updated' => $profile_updated,
                'signature' => SecurityHelper::createSignature($user->access_token),
                'avatar_url' => $user->avatarImage
            ];
        } else {

            return new BadRequestHttpException("Login error");
        }
    }

    public function actionRegister()
    {
        /** @var RegistrationForm $model */
        $model = Yii::createObject(RegistrationForm::className());

        if ($model->load(Yii::$app->request->bodyParams)) {

            $data = $model->register();
            // var_dump($data); die();
            if ($data['success']) {
                $user = $data['user'];
                return [
                    'access_token' => $user->access_token,
                    'email' => $user->email,
                    'name' => $user->profile->name,
                    'signature' => SecurityHelper::createSignature($user->access_token)
                ];
            } else {
                return $data['user'];
            }
        } else {
            $model->validate();
            return $model;
        }

    }


    public function actionForgotPassword()
    {
        $model = Yii::createObject([
            'class' => ForgotPassword::className(),
            'scenario' => ForgotPassword::SCENARIO_REQUEST,
        ]);
        if ($model->load(Yii::$app->getRequest()->getBodyParams(), '') && $model->validate()) {
            $model->sendRecoveryMessage();
            return ['message'=>'Nội dung hướng dẫn khởi tạo lại mật khẩu đã được gửi qua email.'];
        } else {
            return $model;
        }
    }

    public function actionLiveSearch()
    {
        $get = Yii::$app->request->get();
        if (empty($get['searchText'])) {
            $searchText = "";
            $searchResult = \common\elastic\Course::getCompletionTerm("");

        } else {
            // Alow unicode char, number only
            $searchText = $get['searchText'];
            $searchText = preg_replace('/[^A-Za-z0-9\-\' ' . \common\helpers\StringHelper::getUnicodeCharaters() . ']/', '', $searchText);  // escape apostraphe
            $searchResult = \common\elastic\Course::getCompletionTerm($searchText);
        }


        $result = [];
        foreach ($searchResult['data'] as $item) {
            array_push($result, ['id' => $item['id'], 'name' => $item['name'],'type'=>$item['type']]);
        }

        return $result;
    }

    public function actionAppInfo()
    {
        /*  $locations = Location::find()->where(['status'=>Location::STATUS_ACTIVE])->orderBy('position')->all();
          return ['locations' => $locations]; */
        $media_url = Yii::$app->params['media_link'];
        $video_url = Yii::$app->params['videoServer'];
        $topic_name = Yii::$app->params['topic_name'];
        $is_review = $this->is_review;
        $intro_link = 'https://kyna.vn/p/kyna/gioi-thieu-mapp';
        $faq_link = 'https://kyna.vn/p/kyna/cau-hoi-thuong-gap-mapp';
        if($is_review){
            $intro_link = 'https://kyna.vn/p/kyna/gioi-thieu-mapp-test';
            $faq_link = 'https://kyna.vn/p/kyna/cau-hoi-thuong-gap-mapp-test';
        }

        return [
            'media_url' => $media_url,
            'video_url' => $video_url,
            'document_url' => 'https://media.kyna.vn',
            'topic_name'=>$topic_name,
            'isReview' => $is_review,
            'intro_link' => $intro_link,
            'faq_link' => $faq_link,

        ];
    }

    public function actionCategories()
    {
        $categories = CategoryDetail::find()
            ->where(['status' => Category::STATUS_ACTIVE])
            ->andWhere(['is_deleted' => false])
            ->andWhere(['parent_id' => 0])
            ->orderBy('order')
            ->all();

        return $categories;
    }
    public function actionGetCity($cityId = null)
    {
        if(empty($cityId))
        {
            $locations = Location::find()->where(['parent_id'=>0,'status'=>1])->all();
            return $locations;
        }
        else
        {
            $location= Location::find()->where(['parent_id'=>$cityId,'status'=>1])->all();
            if(!empty($location))
                return $location;
            else
                throw new NotFoundHttpException("Không tìm thấy thành phố.");
        }


    }
    public function actionGetLocation($location_id)
    {
        if(empty($location_id))
            throw new NotFoundHttpException("Không tìm thấy địa chỉ.");

        $location= Location::find()->where(['id'=>$location_id,'status'=>1])->one();
        if(!empty($location))
            return $location;
        else
            throw new NotFoundHttpException("Không tìm thấy địa chỉ.");
    }
    public function actionGetNotifications()
    {
        return Notification::getMobileNotifications();
    }
    public function actionFetchPaymentLink()
    {
        $postData = Yii::$app->getRequest()->getBodyParams();

        $ids = !empty($postData['course_ids']) ? $postData['course_ids'] : null;
        if(empty($ids)){
            throw new NotFoundHttpException('Không tìm thấy id khóa học');
        }
        $token = !empty($postData['token']) ? $postData['token'] : '';
        $signature = !empty($postData['signature']) ? $postData['signature'] : '';
        $courseIdString = implode(',', $ids);
        $queryParams = [];
        $queryParams['id'] = $courseIdString;
        $sess_id = uniqid();
        $queryParams['sess_id'] = $sess_id;
        $userId = null;
        if(!empty($token) && !empty($signature)) {
            if (SecurityHelper::checkSignature($signature, $token)) {
                $user = \mobile\models\User::findIdentityByAccessToken($token);
                if (!empty($user)) {
                    Yii::$app->user->login($user);
                    $userId = Yii::$app->user->id;
                }
            }

        }
        if(!empty($userId))
        {
            $queryParams['uid'] = $userId;
        }
        if (!empty($this->code)) {
            $queryParams['code'] = $this->code;
        }


        $hash = DocumentHelper::hashParams($queryParams);

        $url = Url::base(true) . Url::to(['/cart/mobile-checkout/index',
                'id' => $courseIdString,
                'sess_id' => $sess_id,
                'uid' => $userId,
                'hash' => $hash
            ]);

        return array('url'=>$url);
    }


    public function actionFetchOrderPaymentLink()
    {
        $postData = Yii::$app->getRequest()->getBodyParams();

        $order_id = !empty($postData['order_id']) ? $postData['order_id'] : null;
        if(empty($order_id)){
            throw new NotFoundHttpException('Không tìm thấy mã order');
        }
        $token = !empty($postData['token']) ? $postData['token'] : '';
        $signature = !empty($postData['signature']) ? $postData['signature'] : '';
        $queryParams = [];
        $queryParams['order_id'] = $order_id;
        $sess_id = uniqid();
        $queryParams['sess_id'] = $sess_id;
        $userId = null;
        if(!empty($token) && !empty($signature)) {
            if (SecurityHelper::checkSignature($signature, $token)) {
                $user = \mobile\models\User::findIdentityByAccessToken($token);
                if (!empty($user)) {
                    Yii::$app->user->login($user);
                    $userId = Yii::$app->user->id;
                }
            }

        }
        if(!empty($userId))
        {
            $queryParams['uid'] = $userId;
        }
        $hash = DocumentHelper::hashParams($queryParams);

        $url = Url::base(true) . Url::to(['/cart/mobile-checkout/index',
                'orderId' => $order_id,
                'sess_id' => $sess_id,
                'uid' => $userId,
                'hash' => $hash
            ]);

        return array('url'=> $url);
    }

    public function actionLoginActiveCod(){
        $postData = Yii::$app->getRequest()->getBodyParams();

        $loginOtpForm = new LoginOtpForm();
        $loginOtpForm->load($postData, '');
        return $loginOtpForm->active();

    }


    public function actionActiveCodOtp()
    {
        $postData = Yii::$app->getRequest()->getBodyParams();
        $otpForm = new OtpForm();
        $otpForm->setScenario(OtpForm::SCENARIO_ACTIVE);
        $loaded = $otpForm->load($postData, '');
        if ($otpForm->validate()) {
            if ($loaded && $otpForm->complete()) {
                // login and redirect to khoa hoc cua toi
                $user = $otpForm->order->user;
                if (!empty($user)) {
                    $result = Yii::$app->user->login($user);
                    if ($result) {
                        $user->access_token = SecurityHelper::generateAccessToken($user->id);
                        $user->save(false);
                        $avatar = Yii::$app->user->identity->avatarImage;
                        $isChangePassword = Password::validate((string)$user->created_at, $user->password_hash);
                        return [
                            'cod_activated' => true,
                            'login_user' => [
                                'change_password' => $isChangePassword,
                                'access_token' => Yii::$app->user->identity->access_token,
                                'email' => Yii::$app->user->identity->email,
                                'name' => Yii::$app->user->identity->profile->name,
                                'signature' => SecurityHelper::createSignature(Yii::$app->user->identity->access_token),
                                'avatar_url' => $avatar
                            ]
                        ];
                    }

                }

            } else {
                $message = 'Mã thẻ kích hoạt không chính xác';
                $otpForm->addError('activation_code', $message);
            }
        }
        return $otpForm;

    }

    public function actionSendOtpCode()
    {
        $postData = Yii::$app->getRequest()->getBodyParams();
        $otpForm = new OtpForm();
        $loaded = $otpForm->load($postData, '');
        $must_login = false;
        $otpMsg = false;
        if ($loaded && $otpForm->validateInfo()) {
            // check phone limit
            if ($otpForm->checkPhoneLimit()) {
                $must_login = true;
                return ['must_login' => $must_login, 'otp_sent' => $otpMsg];
            }
            if ($otpForm->sendOtp()) {
                $otpMsg = true;
            } else {
                $otpMsg = false;
            }
        }
        if ($otpForm->hasErrors()) {
            return $otpForm;
        }

        return ['must_login' => $must_login, 'otp_sent' => $otpMsg];


    }
    public function actionCalCartPrice(){
        $postData = Yii::$app->getRequest()->getBodyParams();
        $cartForm = new CartForm();
        $cartForm->load($postData, '');
        return $cartForm->calCartPrice();
    }

    public function actionGetDynamicScreen($deep_link){

        $typeCourseDetail = 1;
        $typeCategory = 2;
        $typeTagDetail = 3;
        $typeSearch = 4;
        $typeWebview = 5;
        if(!in_array($deep_link, [1,2,3,4,5])){
            $deep_link = 1;
        }

        $detailCourse = [
            'screen_type' => $typeCourseDetail,
            'title' => 'Xây dựng quan hệ bằng tiếng Anh',
            'data' => '944'
        ];

        $category = [
            'screen_type' => $typeCategory,
            'title' => 'Vi tính văn phòng',
            'data' => '22'
        ];
        $tag = [
            'screen_type' => $typeTagDetail,
            'title' => 'Yoga',
            'data' => 'yoga'
        ];

        $search = [
            'screen_type' => $typeSearch,
            'title' => 'tieng anh',
            'data' => 'tieng anh'
        ];

        $webView = [
            'screen_type' => $typeWebview,
            'title' => 'Thành Thạo Word 2010 trong 5 giờ',
            'data' => 'https://staging.kyna.vn/thanh-thao-word-2010-trong-5-gio'
        ];

        $listData = [$detailCourse, $category, $tag, $search, $webView];
        return $listData[$deep_link-1];
    }

}