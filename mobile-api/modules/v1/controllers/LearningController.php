<?php
/**
 * Created by PhpStorm.
 * User: khanhphan
 * Date: 9/27/17
 * Time: 2:30 AM
 */

namespace mobile\modules\v1\controllers;


use Elasticsearch\Common\Exceptions\Forbidden403Exception;
use kyna\course\models\Course;
use kyna\course\models\CourseLessionNote;
use kyna\learning\models\UserCourseLesson;
use kyna\user\models\UserCourse;
use mobile\components\ActiveController;
use mobile\components\LoginRequireTrait;
use mobile\components\RestController;
use mobile\models\CourseDetail;
use mobile\models\CourseDiscussion;
use mobile\models\CourseDocument;
use mobile\models\CourseLearnerQna;
use mobile\models\CourseLesson;
use mobile\models\CourseLessonDetail;
use mobile\models\CourseLessonNote;
use mobile\models\CourseRating;
use mobile\models\CourseSection;
use mobile\models\CourseSectionLearning;
use mobile\models\Quiz;
use mobile\models\QuizDetail;
use mobile\models\QuizQuestion;
use mobile\models\QuizSession;
use mobile\models\SyncPercentForm;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use kyna\course\models\Teacher;
use kyna\course\models\TeacherNotify;
use mobile\models\OrderForm;
use Yii;
use yii\data\ActiveDataProvider;
class LearningController extends RestController
{

    use LoginRequireTrait;


    public function actionView($course_id)
    {
        $userCourse = UserCourse::findOne(['course_id' => $course_id, 'user_id' => \Yii::$app->user->id]);

        $course = CourseDetail::findOne($course_id);
        if ($course == null || $userCourse == null) {
            throw new NotFoundHttpException("Không tìm thấy khóa học.");
        }

        return $course;
    }

    public function actionSyncPercent($finish = 0)
    {
        $model = new SyncPercentForm();
        if ($model->load(\Yii::$app->getRequest()->getBodyParams(), '') && $model->validate()) {

            $userCourse = UserCourse::find()->where(['id' => $model->user_course_id] )->one();
            if ($userCourse == null || $userCourse->user_id != Yii::$app->user->id)
                throw new NotFoundHttpException();


            /** @var  $userCourseLesson UserCourseLesson */
            $userCourseLesson = UserCourseLesson::find()->where(['user_course_id' => $userCourse->id, 'course_lesson_id' => $model->lesson_id])->one();
            $new_mission = 0;
            if(empty($finish)) {
                if($model->duration > 0) {
                    $percent = round($model->time / $model->duration * 100, 2);
                    if ($userCourseLesson != null && $userCourseLesson->process < $percent) {
                        $userCourseLesson->scenario = UserCourseLesson::SCENARIO_PROCESS;

                        $userCourseLesson->user_course_id = $userCourse->id;
                        $userCourseLesson->course_lesson_id = $model->lesson_id;
                        $userCourseLesson->process = $percent;
                        $userCourseLesson->save(false);

                        $userCourse->calculateProgress();

                    }
                }
            }
            else
            {
                if (UserCourseLesson::pass($userCourse->id, $model->lesson_id)) {
                     $userCourse->calculateProgress();
                }
            }
            if ($messages = Yii::$app->session->getFlash('mission', false)) {
                $kpoint_plus = $messages['point'];

                list($finishedMissions, $notFinishedMissions) = $userCourse->course->getSummaryMissionByUser($userCourse->user_id);
                $finishedMisssionsCount = count($finishedMissions);
                $totalMissionCount = count($notFinishedMissions) + $finishedMisssionsCount;

                $sumary = "{$finishedMisssionsCount}/{$totalMissionCount}";
                $new_mission = 1;
                return [
                    'new_mission' => $new_mission,
                    'kpoint_plus' => $kpoint_plus,
                    'sumary'=>$sumary,
                    'mission_name'=>$messages['missionName'],

                ];
            }

            return [
                'new_mission' => $new_mission
            ];

        }


    }

    public function actionNotes($course_lesson_id = null)
    {
        if (\Yii::$app->getRequest()->isPost) {
            $model = new CourseLessonNote();
            if ($model->load(\Yii::$app->getRequest()->getBodyParams(), '') && $model->validate()) {
                $this->checkAccessCourse($model->lession->course->id);
                $model->user_id = \Yii::$app->user->getId();
                $model->save();
                return $model;
            } else {
                return $model;
            }

        } else {

            $course_lesson = CourseLesson::findOne($course_lesson_id);
            if(empty($course_lesson) || $this->checkAccessCourse($course_lesson->course_id))
            {
                throw new NotFoundHttpException("Không tìm thấy bài học.");
            }
           /* $model = CourseLessonNote::find()
                ->select('course_lesson_notes.*')
                ->leftJoin('course_lessons', '`course_lesson_notes`.`course_lession_id` = `course_lessons`.`id`')
                ->leftJoin('courses', '`courses`.`id` = `course_lessons`.`course_id`')
                ->where(['courses.id' => $course_id])->all(); */

            $model = CourseLessonNote::find()->where(['course_lession_id'=>$course_lesson_id, 'user_id' => \Yii::$app->user->getId()])->all();
            return $model;
        }


    }
    public function actionGetNoteByCourse($course_id)
    {
        $course = CourseDetail::findOne($course_id);
        if ($course == null || $this->checkAccessCourse($course_id)) {
            throw new NotFoundHttpException("Không tìm thấy khóa học.");
        }
        $model = CourseLessonNote::find()
            ->select('course_lesson_notes.*')
            ->leftJoin('course_lessons', '`course_lesson_notes`.`course_lession_id` = `course_lessons`.`id`')
            ->leftJoin('courses', '`courses`.`id` = `course_lessons`.`course_id`')
            ->where(['courses.id' => $course_id, 'course_lesson_notes.user_id' => \Yii::$app->user->getId()])->all();
        return $model;
    }

    public function actionDiscussion($course_id, $page= 1, $limit = 5)
    {
        $userCourse = UserCourse::findOne(['course_id' => $course_id, 'user_id' => \Yii::$app->user->id]);
        $course = CourseDetail::findOne($course_id);
        if ($course == null || $userCourse == null) {
            throw new NotFoundHttpException("Không tìm thấy khóa học.");
        }
        // return CourseLessonNote::search($course_id);

        $csIds = Yii::$app->db->createCommand("select user_id from auth_assignment where item_name = 'CustomerService'")->queryColumn();

        $query = CourseDiscussion::find()
            ->where(['course_id' => $course_id])
            ->andWhere(['parent_id' => 0, 'status' => CourseDiscussion::STATUS_ACTIVE])
            ->orderBy(['created_time' => SORT_DESC]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

       $dataProvider->pagination->pageSize = $limit;


        return array('total_count'=>$dataProvider->getTotalCount(),'qtv_ids'=>$csIds,'list_discussion'=>$dataProvider->getModels());
    }
    public function actionAddDiscussion()
    {
        if (\Yii::$app->getRequest()->isPost) {
            $postData = \Yii::$app->getRequest()->getBodyParams();

            $course_id = $postData['course_id'];
            $userCourse = UserCourse::findOne(['course_id' => $course_id, 'user_id' => \Yii::$app->user->id]);
            $course = CourseDetail::findOne($course_id);
            if ($course == null || $userCourse == null) {
                throw new NotFoundHttpException("Không tìm thấy khóa học.");
            }

            $discuss = new CourseDiscussion();
            $discuss->user_id = \Yii::$app->user->id;
            if($discuss->load(\Yii::$app->getRequest()->getBodyParams(), '') && $discuss->validate())
            {
                $discuss->save();
                return $discuss;
            }
            return $discuss;

        }
    }

    public function actionComment($commentId, $limit = null, $offset = null)
    {

        $model = CourseDiscussion::find()
            ->andWhere(['parent_id' => $commentId])
            ->orderBy(['created_time' => SORT_ASC]);
        if ($limit == null) $model = $model->all();
        else {
            $model = $model->offset($offset)->limit($limit)->all();
        }
        return $model;
    }


    public function actionQna($course_id, $page= 1, $limit = 10)
    {
        $userCourse = UserCourse::findOne(['course_id' => $course_id, 'user_id' => \Yii::$app->user->id]);
        $course = CourseDetail::findOne($course_id);
        if ($course == null || $userCourse == null) {
            throw new NotFoundHttpException("Không tìm thấy khóa học.");
        }
        // return CourseLessonNote::search($course_id);
        $query  = $course->getLearnerQuestions();
        $query->andWhere(['is_approved' => $course::BOOL_YES]);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'attributes' => ['id'],
                'defaultOrder' => [
                    'id' => SORT_DESC
                ]
            ]
        ]);

        $dataProvider->pagination->pageSize = $limit;
        return array('total_count'=>$dataProvider->getTotalCount(),'list_qna'=>$dataProvider->getModels());
    }
    public function actionAddQna()
    {
        if (\Yii::$app->getRequest()->isPost) {
            $postData = \Yii::$app->getRequest()->getBodyParams();
            $course_id = $postData['course_id'];
            $userCourse = UserCourse::findOne(['course_id' => $course_id, 'user_id' => \Yii::$app->user->id]);
            $course = CourseDetail::findOne($course_id);
            if ($course == null || $userCourse == null) {
                throw new NotFoundHttpException("Không tìm thấy khóa học.");
            }
            $courseLearnerQna = new CourseLearnerQna();
            if($courseLearnerQna->load(\Yii::$app->getRequest()->getBodyParams(), '') && $courseLearnerQna->validate())
            {
                $courseLearnerQna->save();
                return $courseLearnerQna;
            }


            return $courseLearnerQna;
        }
    }

    public function actionDocument($course_id)
    {
        $userCourse = UserCourse::findOne(['course_id' => $course_id, 'user_id' => \Yii::$app->user->id]);
        $course = CourseDetail::findOne($course_id);
        if ($course == null || $userCourse == null) {
            throw new NotFoundHttpException("Không tìm thấy khóa học.");
        }
        // return CourseLessonNote::search($course_id);
        $model = CourseDocument::find()
            ->where(['course_id' => $course_id])->orderBy(['id' => SORT_DESC])->all();
        return $model;
    }

    public function actionContent($course_id)
    {
        $userId = \Yii::$app->user->id;
        $userCourse = UserCourse::findOne(['course_id' => $course_id, 'user_id' => $userId]);

        $course = CourseDetail::findOne($course_id);
        if (empty($course) || empty($userCourse)) {
            throw new NotFoundHttpException("Không tìm thấy khóa học.");
        }
        $current_section_id = $userCourse->current_section;
        $current_lesson_id = $userCourse->current_lesson;

        if(empty($current_section_id))
        {
            $current_section_id = $course->getDefaultSection()->id;
        }

        if(!empty($current_section_id))
        {
            $parent_section =  CourseSection::findOne($current_section_id);
        }



        if(empty($current_lesson_id)) {
            $current_lesson_id = CourseLesson::find()->andWhere(['section_id' => $current_section_id,
                'status' => CourseLesson::BOOL_YES  ])->min('id');
        }

        $last_lesson_detail  = CourseLessonDetail::findOne($current_lesson_id);


        if ($userCourse->is_started == UserCourse::BOOL_NO) {
            $userCourse->is_started = UserCourse::BOOL_YES;
            $userCourse->started_date = time();
        }

        $userCourse->save();

        $models = CourseSectionLearning::find()
            ->where(['course_id' => $course_id, 'active' => 1, 'lvl' => 0, 'visible' => 1])->orderBy('root, lft')->all();

        $is_embeed = false;
        $embeed_link = '';
        if (!empty($course) && !empty($course->custom_content)  ) {
            $is_embeed = true;
            $embeed_link = $course->custom_content;
        }

        return ['is_embeed' => $is_embeed, 'embeed_link'=> $embeed_link, 'content'=>$models, 'current_section_id' => $current_section_id ,  'parent_section_id' => !empty($parent_section) ?  $parent_section->root : null ,  'last_lesson_id' => $current_lesson_id, 'last_lesson' => $last_lesson_detail];
    }

    public function actionQuiz($quiz_id)
    {

        $model = Quiz::findOne([
            'id' => $quiz_id,
            'status' => Quiz::STATUS_ACTIVE,
            'is_deleted' => false
        ]);
        $userId = \Yii::$app->user->getId();

       $session = $model->getSession($userId);
        if (!isset($model)) {
            throw new NotFoundHttpException('Không tìm thấy bài quiz');
        }

        $user_course = (UserCourse::findOne(['course_id' => $model->course_id, 'user_id' => \Yii::$app->user->id]));
        if (!isset($user_course)) {
            throw new NotFoundHttpException('Không tìm thấy bài quiz');
        }

        return ['quiz'=>$model, 'session'=>$session];
    }

    public function actionStartLesson($id)
    {
        $model = CourseLessonDetail::findOne($id);
        if ($model == null ||  !$model->canUserAccess()) {
            throw new NotFoundHttpException('không tìm thấy bài học');
        }
        $userId = Yii::$app->user->getId();
        $canLearn = $model->getCanLearnByDate($userId);
        $isLearnQuick = $model->getIsQuick($userId);
        if($canLearn || $isLearnQuick){

        }else{
            $tmp = $model->getDateCanLearn(\Yii::$app->user->getId());
            $model->is_quick_learn = true;
            $model->message = 'Bạn chưa thể xem được bài học này, hãy đợi đến ngày '.$tmp .' bạn nhé!!.';
            return $model;
        }
        $userCourseLesson = UserCourseLesson::find()->where([
            'user_course_id' => $model->getUser_course_id(),
            'course_lesson_id' => $model->id,
        ])->one();
        $userCourse = UserCourse::getLearning($model->course_id, $userId);


        if(!empty($model->section_id) && !empty($userCourse)) {
            // check to save current section
            if ($userCourse->current_section != $model->section_id) {
                $userCourse->current_section = $model->section_id;
                $userCourse->save(false);
            }
            // check to save current lesson
            if ($userCourse->current_lesson != $id) {
                $userCourse->current_lesson = $id;
                $userCourse->save(false);
            }
        }

        if ($userCourseLesson == null) {
            $userCourseLesson = new UserCourseLesson();
            $userCourseLesson->user_course_id = $model->user_course_id;
            $userCourseLesson->course_lesson_id = $model->id;
            $userCourseLesson->start_time = time();
            $result = $userCourseLesson->save();
        }
            return $model;

    }

    public function actionEndLesson()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $userCourseId = Yii::$app->request->post('user_course_id', 0);
        $lessonId = Yii::$app->request->post('lesson_id', 0);

        $result = false;

        $isNotPassedLesson = UserCourseLesson::find()->where([
            'user_course_id' => $userCourseId,
            'course_lesson_id' => $lessonId,
            'is_passed' => UserCourseLesson::BOOL_YES
        ])->andWhere(['IS NOT', 'process', null])->exists();

        if (!$isNotPassedLesson) {
            $result = UserCourseLesson::pass($userCourseId, $lessonId);
            $userCourse = UserCourse::findOne($userCourseId);
            if ($userCourse) {
                $userCourse->calculateProgress();
            }
        }

        return [
            'result' => $result
        ];
    }


    public function actionReview($course_id)
    {

        $this->checkAccessCourse($course_id);
        $model = new CourseRating();
        $model->user_id = \Yii::$app->user->getId();
        $model->course_id = $course_id;

        if ($model->load(\Yii::$app->request->post(), '') && $model->validate() && $model->save()) {

            return $model;
        } else {
            return $model;
        }


    }

    private function checkAccessCourse($course_id)
    {
        $userCourse = UserCourse::findOne(['course_id' => $course_id, 'user_id' => \Yii::$app->user->id]);
        $course = CourseDetail::findOne($course_id);
        if ($course == null || $userCourse == null) {
            throw new NotFoundHttpException("Không tìm thấy khóa học.");
        }

    }

    public function actionFollowTeacher()
    {
        $teacherId = Yii::$app->request->post('teacherId', 0);
        $teacher = Teacher::findOne(['id'=>$teacherId]);
        if(empty($teacher))
            throw new NotFoundHttpException("Không tìm thấy giảng viên.");
        $userId = \Yii::$app->user->id;
        $notify = TeacherNotify::findOne([
            'teacher_id' => $teacherId,
            'user_id' => $userId
        ]);

        if (empty($notify)) {
            $notify = new TeacherNotify();
            $notify->teacher_id = $teacherId;
            $notify->user_id = $userId;
            if ($notify->save()) {
               return $notify;
            } else {
               return $notify;
            }
        }
        return $notify;
    }
    public function actionUnFollowTeacher()
    {
        $teacherId = Yii::$app->request->post('teacherId', 0);
        $userId = \Yii::$app->user->id;
        $notify = TeacherNotify::findOne([
            'teacher_id' => $teacherId,
            'user_id' => $userId
        ]);

        if (!empty($notify)) {
            $notify->delete();
            return $notify;
        }
        else
            throw new NotFoundHttpException("Không tìm thấy giảng viên theo dõi.");
    }
    public function actionCheckFollowTeacher()
    {
        $teacherId = Yii::$app->request->post('teacherId', 0);
        $teacher = Teacher::findOne(['id'=>$teacherId]);
        if(empty($teacher))
            throw new NotFoundHttpException("Không tìm thấy giảng viên.");
        $userId = \Yii::$app->user->id;
        $notify = TeacherNotify::findOne([
            'teacher_id' => $teacherId,
            'user_id' => $userId
        ]);
        if(!empty($notify))
        {
            return array('followed'=>1);
        }

         return array('followed'=>0);

    }
    public function actionActiveCod()
    {
        $code = Yii::$app->request->post('code', 0);
        if(empty($code))
            throw new NotFoundHttpException("Bạn chưa nhập mã COD.");
        $activeForm = new  OrderForm();
        $activeForm->scenario = OrderForm::SCENARIO_ACTIVE_COD;
        $activeForm->activation_code = $code;
        if($activeForm->validate())
        {
            if($activeForm->complete()) {
                return ['status'=>'success', 'message'=>'Bạn đã kích hoạt thành công.'];
            }
            else {
                return ['status' => 'fail', 'message' => 'Không thể kích hoạt.'];
            }
        }
        else
        throw new NotFoundHttpException("Mã COD không đúng.");
    }
    public function actionQuickLearn($course_id)
    {
        if(empty($course_id)) {
            throw new NotFoundHttpException("Không tìm thấy khóa học.");
        }

        $userCourse = \mobile\models\UserCourse::findOne(['course_id' => $course_id, 'user_id' => \Yii::$app->user->id]);
        $course = CourseDetail::findOne($course_id);
        if ($course == null || $userCourse == null) {
            throw new NotFoundHttpException("Không tìm thấy khóa học.");
        }
        $userCourse->is_quick = UserCourse::BOOL_YES;
        if ($userCourse->validate()) {
            $userCourse->save();
            return $userCourse;
        }
        return $userCourse;
    }
    public  function actionCheckQuickLearn($lesson_id)
    {

        $lesson = CourseLesson::findOne(['id' => $lesson_id, 'status' => CourseLesson::STATUS_ACTIVE]);
        if(empty($lesson))
            throw new NotFoundHttpException("Không tìm thấy bài học.");
        $this->checkAccessCourse($lesson->course_id);
        $userId = \Yii::$app->user->id;
        $canLearn = $lesson->getCanLearnByDate($userId);
        $isLearnQuick = $lesson->getIsQuick($userId);
        if ($canLearn || $isLearnQuick)
        {
            return array('is_learn_quick'=>1);
        }
        else
        {
            return array('is_learn_quick'=>0);
        }
    }

}