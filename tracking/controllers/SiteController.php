<?php

/**
 * Created by PhpStorm.
 * User: khanhphan
 * Date: 1/10/17
 * Time: 10:08 AM
 */

namespace tracking\controllers;

use kyna\page\models\Page;

class SiteController extends \yii\web\Controller
{

    public function actionIndex ($ref)
    {

        $this->layout = false;

        $page = Page::find()->where(['slug' => $ref, 'status' => Page::STATUS_ACTIVE])->one();

        if ($page == null)
            return $this->redirect("https://kyna.vn");

        return $this->render('index', ['page' => $page]);

    }
    public function actionTest()
    {
        echo "<script>alert('test');</script>";
    }

}