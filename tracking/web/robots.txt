User-agent: *

Disallow: /user/
Disallow: /users/
Disallow: /checkout/
Disallow: /cart/
Disallow: /course/learning

Disallow: /lop-hoc
Disallow: /gio-hang
Disallow: /thanh-toan
Disallow: /trang-ca-nhan
Disallow: /kick-hoat
Disallow: /dang-nhap
Disallow: /dang-ky
Disallow: /quen-mat-khau

Sitemap: https://kyna.vn/sitemap.xml
