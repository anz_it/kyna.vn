#!/usr/bin/env php
<?php
/**
 * Created by PhpStorm.
 * User: khanhphan
 * Date: 5/17/17
 * Time: 9:47 AM
 */
$name = date('Ymd_His')."_kyna.sql";
echo "Running backup ... \n";
$backup = shell_exec("mysqldump -ukyna -pDKyN@co111 -h10.5.247.13 kyna > /data/backups/$name");
echo "Running backup done, with result: \n$backup\n";
echo "Running tar gz...";
$tarGz = shell_exec("tar -cvzf /data/backups/{$name}.tar.gz /data/backups/{$name}");
echo "Tar gz done with result: \n$tarGz\n";
echo "Running copy to amz";

$upload = shell_exec("rclone copy /data/backups/{$name}.tar.gz Amazon:/database_backup/");
if (empty($upload)) {
    echo "Removing files...\n";
    unlink("/data/backups/{$name}");
    unlink("/data/backups/{$name}.tar.gz");
    echo "Done";
} else {
    echo "Upload error...".$upload;

}

?>