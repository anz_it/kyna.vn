<?php

$config = [
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'fM2hT237NcmHV8A4yNvPBa8J8s0iMLa8',
        ],
        'user' => [
            'class' => 'app\components\WebUser',
            'identityClass' => 'kyna\user\models\User',
            'loginUrl' => ['/user/security/login'],
            'identityCookie' => [
                'name' => '_identity',
                'path' => '/',
                'domain' => ".kyna.local",
            ],
            'enableAutoLogin' => true,
        ],
        'authClientCollection' => [
            'class'   => \yii\authclient\Collection::className(),
            'clients' => [
                'facebook' => [
                    'class'        => 'dektrium\user\clients\Facebook',
                    'clientId'     => '1371557246260154',
                    'clientSecret' => 'b1ab08aa87ef7f6c14fea5062d62b27d',
                ],
            ],
        ],
    ],
];

if (!YII_ENV_TEST) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
    ];
    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];
}

return $config;
