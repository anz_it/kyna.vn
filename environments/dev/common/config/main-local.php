<?php
return [
    'components' => [
        'session' => array(
            //'savePath' => '/some/writeable/path',
            'cookieParams' => array(
                'path' => '/',
                'domain' => '.dev.kyna.vn',
            ),
        ),
        'request' => [
            'enableCookieValidation' => true,
            'enableCsrfValidation' => true,
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'fM2hT237NcmHV8A4yNvPBa8J8s0iMLa8',
            'csrfCookie' => [
                'name' => '_csrf',
                'path' => '/',
                'domain' => ".dev.kyna.vn",
            ],
        ],
        'user' => [
            'identityCookie' => [
                'name' => '_identity',
                'path' => '/',
                'domain' => ".dev.kyna.vn",
            ],
        ],
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=localhost;dbname=kyna_dev_20181205',
            'username' => 'kyna_dev',
            'password' => 'Kyna123@X',
            'charset' => 'utf8mb4',
        ],
        'dbcontent' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=127.0.0.1;dbname=kyna_content',
            'username' => 'root',
            'password' => '123456',
            'charset' => 'utf8',
        ],
        'dbv2' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=103.56.156.104;dbname=djo',
            'username' => 'kyna',
            'password' => 'DKyN@co111',
            'charset' => 'utf8',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@common/mail',
            'useFileTransport' => false,
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'email-smtp.us-west-2.amazonaws.com',
                'username' => 'AKIAJYG4AN2U3FRXR52A',
                'password' => 'AiktBpqWF+MLeAuR94/5J89TFZx55j5RgXBnS3NQLAeD',
                'encryption' => 'tls',
                'port' => '2587',
            ],
            'messageConfig' => [
                'charset' => 'UTF-8',
                'from' => ['hotro@kyna.vn' => 'Hỗ trợ Kyna.vn']
            ]
        ],

        'elasticsearch' => [
            'class' => 'common\components\ElasticSearch',
            'hosts' => [
                ['host' => '127.0.0.1', 'port' => 9200],
                // configure more hosts if you have a cluster
            ],
        ],
        'elasticlog' => [
            'class' => 'common\components\ElasticLog',
            'hosts' => [
                ['host' => 'elastic01', 'port' => 9200],
                // configure more hosts if you have a cluster
            ]
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'common\components\ElasticLog',
                    'categories' => ['curl', 'logout'],
                    'levels' => ['error', 'warning', 'info'],
                ],

            ]
        ],

    ],
];
