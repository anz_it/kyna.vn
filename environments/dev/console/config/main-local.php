<?php
return [
    'bootstrap' => ['gii'],
    'modules' => [
        'gii' => 'yii\gii\Module',
    ],
    'components' => [
        // db backup before switch product's environment
        'db_backup' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=localhost;dbname=kyna_20170521_bk',
            'username' => 'root',
            'password' => 'DKyN@co111',
            'charset' => 'utf8',
        ],
        // db new environment but switch to old environment
        'db_new' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=localhost;dbname=kyna_20170522_new',
            'username' => 'root',
            'password' => 'DKyN@co111',
            'charset' => 'utf8',
        ],
        'urlManager' => [
            'baseUrl' => 'http://dev.kyna.vn',
        ],
    ]
];
