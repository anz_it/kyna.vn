<?php
$config = [
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'KCKvyjEX32F3vvt3HGxqCm0z16EhhDF9',
        ],
        'user' => [
            'class' => 'app\components\WebUser',
            'identityClass' => 'kyna\user\models\User',
            'loginUrl' => ['/user/security/login'],
            'identityCookie' => [
                'name' => '_identity',
                'path' => '/',
                'domain' => ".kyna.vn",
            ],
            'enableAutoLogin' => true,
        ],
        'authClientCollection' => [
            'class'   => \yii\authclient\Collection::className(),
            'clients' => [
                'facebook' => [
                    'class'        => 'dektrium\user\clients\Facebook',
                    'clientId'     => '191634267692814',
                    'clientSecret' => '2807d29ac5742521188979e8c9ce02bc',
                ],
            ],
        ],
    ],
];

if (!YII_ENV_TEST) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
    ];
    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];
}

return $config;
