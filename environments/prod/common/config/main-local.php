<?php


$rediscache = [
    'class' => 'yii\redis\Cache',
    'serializer' => ['igbinary_serialize', 'igbinary_unserialize'],
    'redis' => [
        'hostname' => '10.140.0.4',
        'port' => 6379,
        'database' => 1,
    ],
];

return [
    'components' => [
        'session' => array(
            //'savePath' => '/some/writeable/path',
            'cookieParams' => array(
                'path' => '/',
                'domain' => '.kyna.vn',
            ),
        ),
        'request' => [
            'enableCookieValidation' => true,
            'enableCsrfValidation' => true,
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'fM2hT237NcmHV8A4yNvPBa8J8s0iMLa8',
            'csrfCookie' => [
                'name' => '_csrf',
                'path' => '/',
                'domain' => ".kyna.vn",
            ],
        ],
        'user' => [
            'identityCookie' => [
                'name' => '_identity',
                'path' => '/',
                'domain' => ".kyna.vn",
            ],
        ],
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=127.0.0.1;port=3306;dbname=kyna',
            'username' => 'kyna',
            'password' => 'DKyN@co111',
            'charset' => 'utf8mb4',
            'serverStatusCache' => false,
            'slaveConfig' => [
                'username' => 'kyna',
                'password' => 'DKyN@co111',
                'charset' => 'utf8mb4',
                'serverStatusCache' => false,
                'attributes' => [
                    // use a smaller connection timeout
                    PDO::ATTR_TIMEOUT => 10,
                ],
            ],
            'slaves' => [
                ['dsn' => 'mysql:host=127.0.0.1;port=3307;dbname=kyna'],

            ],

        ],
        'dbcontent' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=10.140.0.2;dbname=kyna_content',
            'username' => 'root',
            'password' => 'DKyN@co111',
            'charset' => 'utf8',
        ],
        'dbv2' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=127.0.0.1;dbname=djo',
            'username' => 'kyna',
            'password' => 'DKyN@co111',
            'charset' => 'utf8',
        ],
        'cache' => $rediscache,
        'authManager' => [
            'class' => 'dektrium\rbac\components\DbManager',
            'cache' => 'cache',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@common/mail',
            'useFileTransport' => false,
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'email-smtp.us-west-2.amazonaws.com',
                'username' => 'AKIAIKI4M4ADUQABO42Q',
                'password' => 'Akcifv7GQ5nz9W5Iq2jQPepnH/GdIq0GPvdoXiGPzAzR',
                'encryption' => 'tls',
                'port' => '587',
            ],
            'messageConfig' => [
                'charset' => 'UTF-8',
                'from' => ['hotro@kyna.vn' => 'Hỗ trợ Kyna.vn']
            ]
        ],
        'elasticsearch' => [
            'class' => 'common\components\ElasticSearch',
            'hosts' => [
                ['host' => '10.140.0.3', 'port' => 9200],
                ['host' => '10.140.0.4', 'port' => 9200],
                ['host' => '10.140.0.5', 'port' => 9200],
              //  ['host' => '10.140.0.6', 'port' => 9200],
                // configure more hosts if you have a cluster
            ],
        ],
        'elasticlog' => [
            'class' => 'common\components\ElasticLog',
            'hosts' => [
                ['host' => '10.140.0.3', 'port' => 9200],
                ['host' => '10.140.0.4', 'port' => 9200],
                ['host' => '10.140.0.5', 'port' => 9200],
              //  ['host' => '10.140.0.6', 'port' => 9200],
                // configure more hosts if you have a cluster
            ]
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'common\components\ElasticLog',
                    'categories' => ['curl', 'logout'],
                    'levels' => ['error', 'warning', 'info'],
                ],

            ]
        ],

    ],
];
