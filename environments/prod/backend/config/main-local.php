<?php
$config = [
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'KCKvyjEX32F3vvt3HGxqCm0z16EhhDF9',
        ],
        'session' => array(
            //'savePath' => '/some/writeable/path',
            'cookieParams' => array(
                'path' => '/',
                'domain' => '.dashboard.kyna.vn',
            ),
        ),
        'dbReport' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=10.5.247.9;dbname=kyna',
            'username' => 'kyna',
            'password' => 'DKyN@co111',
            'charset' => 'utf8',
        ],
    ],
];

return $config;
