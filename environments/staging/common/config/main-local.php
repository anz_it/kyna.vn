<?php



$rediscache = [
    'class' => 'yii\redis\Cache',
    'serializer' => ['igbinary_serialize', 'igbinary_unserialize'],
    'redis' => [
        'hostname' => '127.0.0.1',
        'port' => 6379,
        'database' => 9,
    ],
];

return [
    'components' => [
        'session' => array(
            //'savePath' => '/some/writeable/path',
            'cookieParams' => array(
                'path' => '/',
                'domain' => '.staging.kyna.vn',
            ),
        ),
        'request' => [
            'enableCookieValidation' => true,
            'enableCsrfValidation' => true,
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'fM2hT237NcmHV8A4yNvPBa8J8s0iMLa8',
            'csrfCookie' => [
                'name' => '_csrf',
                'path' => '/',
                'domain' => ".staging.kyna.vn",
            ],
        ],
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=127.0.0.1;dbname=kyna_staging_20181205',
            'username' => 'kyna_staging',
            'password' => 'Kyna123@X',
            'charset' => 'utf8mb4',

        ],
        'dbcontent' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=10.140.0.2;dbname=kyna_content',
            'username' => 'root',
            'password' => 'DKyN@co111',
            'charset' => 'utf8',
        ],
        'piwikDb' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=10.5.247.4;dbname=piwik',
            'username' => 'kyna',
            'password' => 'DKyN@co111',
            'charset' => 'utf8',

        ],
        'dbv2' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=127.0.0.1;dbname=djo',
            'username' => 'root',
            'password' => '1@3456',
            'charset' => 'utf8',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@common/mail',
            'useFileTransport' => false,
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'email-smtp.us-west-2.amazonaws.com',
                'username' => 'AKIAJYG4AN2U3FRXR52A',
                'password' => 'AiktBpqWF+MLeAuR94/5J89TFZx55j5RgXBnS3NQLAeD',
                'encryption' => 'tls',
                'port' => '2587',
            ],
            'messageConfig' => [
                'charset' => 'UTF-8',
                'from' => ['hotro@kyna.vn' => 'Hỗ trợ Kyna.vn']
            ]
        ],
        'cache' => $rediscache,
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
            'cache' => $rediscache,
        ],
        'elasticsearch' => [
            'class' => 'common\components\ElasticSearch',
            'hosts' => [
                ['host' => '127.0.0.1', 'port' => 9200],

                // configure more hosts if you have a cluster
            ],
        ],
        'elasticlog' => [
            'class' => 'common\components\ElasticLog',
            'hosts' => [
                ['host' => '127.0.0.1', 'port' => 9200],
                // configure more hosts if you have a cluster
            ]
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'common\components\ElasticLog',
                    'categories' => ['curl', 'logout'],
                    'levels' => ['error', 'warning', 'info'],
                ],

            ]
        ],
    ],
];
