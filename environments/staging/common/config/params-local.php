<?php
return [
    'adminEmail' => 'admin@kyna.vn',
    'supportEmail' => 'hotro@kyna.vn',
    'user.passwordResetTokenExpire' => 3600,
    'pageSize' => 12,
    'epaySandBox' => true,
    // base Url
    'baseUrl' => 'https://staging.kyna.vn',
    'flowplayer' => [
        'key' => '$631977711768310',
    ],
    'googleAnalytics' => 'UA-84135820-1',
    // order
    'payment_fees' => [
        'epay' => 20,
        'onepay_atm' => 4,
        'onepay_cc' => 4,
        'momo' => 4,
        'payoo' => 2,
        'cod' => [
            31 => 35000, //
            24 => 35000, // TpHCM
            -1 => 55000 // >= 1tr
        ],
        'bank-transfer' => 0,
        'in-store' => 0,
    ],
    'onepay' => [
        'atm' => [
            '_vpc_User' => 'op01',
            '_vpc_Password' => 'op123456',
            '_vpc_AccessCode' => 'D67342C2',
            '_vpc_SecureSecret' => 'A3EFDFABA8653DF2342E8DAC29B51AF0',
            '_vpc_Merchant' => 'ONEPAY',
            '_PayUrl' => 'https://mtf.onepay.vn/onecomm-pay/vpcpost.op',
            '_QueryUrl' => 'https://mtf.onepay.vn/onecomm-pay/Vpcdps.op'
        ],
        'cc' => [
            '_vpc_User' => 'op01',
            '_vpc_Password' => 'op123456',
            '_vpc_AccessCode' => '6BEB2546',
            '_vpc_SecureSecret' => '6D0870CDE5F24F34F3915FB0045120DB',
            '_vpc_Merchant' => 'TESTONEPAY',
            '_PayUrl' => 'https://mtf.onepay.vn/vpcpay/vpcpay.op',
            '_QueryUrl' => 'https://mtf.onepay.vn/vpcpay/Vpcdps.op'
        ],
    ],
    'momo' => [
        '_payUrl' => 'https://testing.momo.vn/gw_payment/transactionProcessor',
        '_partnerCode' => 'MOMO0HGO20180417',
        '_accessKey' => 'E8HZuQRy2RsjVtZp',
        '_serectKey' => 'fj00YKnJhmYqahaFWUgkg75saNTzMrbO',
        '_notifyurl' => 'https://momo.vn/notify',
    ],
    'payoo' => [
        '_payUrl' => 'https://newsandbox.payoo.com.vn/v2/paynow/order/create',
        '_businessAPIUrl' => 'https://bizsandbox.payoo.com.vn/BusinessRestAPI.svc',
        '_apiUsername' => 'iss_kyna_BizAPI',
        '_apiPassword' => 'VU35H/kqv2W3BeJT',
        '_signature' => 'b4hS8hFiv3vRfdeYIwYlHIIkwV9lTP4Wg+VRPPOWOmTZV8ei4QfI881y8NHFc9uw',
        '_username' => 'iss_kyna',
        '_shop_id' => 822,
        '_shop_title' => 'kyna',
        '_shop_domain' => 'http://localhost',
        '_serectKey' => 'b03f1ebcbf1d70680e1c9cbe5b86cd01',
    ],
    'cod' => [
        'ProshipAlias' => [
            'pickupId' => [
                '28017' => 'southern',
                '28018' => 'northern',
            ]
        ],
        'GhnAlias' => [
            'pickupId' => [
                '178039' => 'southern',
                '178043' => 'northern',
            ]
        ],
        'GhtkAlias' => [
            'pickupId' => [
                '147530' => 'southern',
                '147531' => 'northern',
            ]
        ],
    ],
    'ghn' => [
        '_apiUrl' => 'https://testapipds.ghn.vn:9999/external/b2c/',
        '_clientId' => '142534',
        '_apiKey' => 'Y4wjWtyaTnvcNpW5',
        '_apiSecretKey' => '101221E5644C3E439AB40601D0D3001F',
        '_password' => '1LmjCEkMEYqGT3aJw',
    ],
    'proship' => [
        '_merchantSiteCode' => '85087',
        '_secureCode' => '0dd3029fd6582b2403d714ee30635bb506f5588f',
        '_apiUrl' => 'http://dev.api.proship.vn/',
        '_pickUps' => [
            'TpHCM' => [
                'name' => "Tp Hồ Chí Minh",
                'id' => '28017',
                'cityId' => '29',         // Ho Chi Minh
                'districtId' => '645'      // Binh Thanh
            ],
            'Hanoi' => [
                'name' => "Hà Nội",
                'id' => '28018',
                'cityId' => '22',
                'districtId' => '229'
            ]
        ],
    ],
    'ghtk' => [
        '_apiUrl' => 'https://dev.giaohangtietkiem.vn',
        '_token' => '8fA4eBd5C2A975062231e8A854Ce7f9ba5e11747',
        '_hash' => 'b49f3c17bd9690796e0297b9e6c6e5fdfcb942f4da192d97a5d558239f9385ef', // hash('sha256', 'ilovekyna@ghtk_ipn');
        '_pickUps' => [
            'TpHcm' => [
                'name' => 'TP Hồ Chí Minh',
                'id' => '147530',
                'districId' => '1088',
                'districtName' => 'Quận Bình Thạnh',
                'address' => 'Lầu 6 Tòa nhà Thịnh Phát - 178/8 Đường D1 P.25',
                'tel' => '0916796012',
            ],
            'Hanoi' => [
                'name' => 'Hà Nội',
                'id' => '147531',
                'districId' => '7',
                'districtName' => 'Đống Đa',
                'address' => 'Lầu 6 Tòa nhà Viện Công Nghệ - 25 Vũ Ngọc Phan',
                'tel' => '0981674905',
            ],
        ]
    ],
    'fcm' => [
        '_secretKey' => 'AIzaSyDjCbi747Vq3DVms8atGNXj2IoNe0CKRS0',
        '_defaultTopic' => 'kyna_all_staging',
        '_server_key_mobile' => 'AAAAzgxj82s:APA91bHluLh4WPoOzfj32fUFbNXK_PhiES1zEn4WZC0GQoNUfFL3mqprQ_6txFt-Jki-6V8hLhxpY3dBvMLTa9XtPlhCkWpjXLBY5N27Hog8GvbNugwWuiBgkN8heSw8zxP3jazy5dFL',
    ],
    'id_of_monkey_junior_guide_course' => 544,
    'id_aff_group_teacher_owner' => 2,
    'id_aff_group_third_party' => 4,
    'taamkru' => [
        '_apiUrl' => 'https://api.taamkru.com/testkyna/1/activation',
        '_apiSecretKey' => 'vyuaboiYUTE!89009_)!UNOPDAP{=1[PLK:J!nmGF&^!@(>@!(*#_nmew0',
        '_code_expiration' => '',
        '_product_expiration' => '',
        '_num_activations' => 3,
    ],
    'touch_n_math' => [
        '_apiUrl' => 'http://testapi.touchnmath.vn/api/kyna/activation',
        '_apiSecretKey' => '123456789asdfghjklASDFGHJKL',
        '_code_expiration' => '',
        '_product_expiration' => '',
        '_num_activations' => 3,
    ],
    'kids_up' => [
        '_apiUrl' => 'http://dev.api.kidsup.net',
        '_apiUserName' => 'kidsup@kyna.vn',
        '_apiPassWord' => 'X8yfhXoudyHTAoaRyLksZTmiPVNhAE89MCxStSsrDcH7zZ63VU',
    ],
    'monkey_junior' => [
        '_apiUrl' => 'http://test.daybehoc.com/api/service',
        '_apiSecretKey' => '312bd01a479f1bfbe07c8d0a084096d6',
        '_categoryObject' => [
            '204' => 'com.earlystart.us.full',
            '205' => 'com.earlystart.alllanguage',
            '523' => 'com.earlystart.vn.full',
            '773' => 'com.earlystart.zh.full',
            '774' => 'com.earlystart.uk.full',
            '775' => 'com.earlystart.vnn.full',
            '776' => 'com.earlystart.fr.full',
            '777' => 'com.earlystart.sp.full',
            '858' => 'com.earlystart.stories.6month',
            '859' => 'com.earlystart.stories.1year',
            '860' => 'com.earlystart.stories.lifetime'
        ],
        'online_payment' => ['epay', 'onepay_atm', 'onepay_cc', 'momo']
    ],
    'rabbitmq' => [
        'host' => '103.56.156.119',
        'port' => 5672,
        'username' => 'guest',
        'password' => 'guest',
        'queues' => [
            'learning-process' => 'learning-process'
        ]
    ],
    'domainFrontend' => 'beta.kyna.com.vn',
    // qna
    'default_receive_qna_emails' => 'Monday',
    'static_link' => 'https://staging.kyna.vn/media/',
    'media_link' => 'https://staging.kyna.vn/media',
    'lp_media_link' => 'https://media.kyna.vn',
    'template_server' => [
        '_baseUrl' => 'kyna_template',
    ],
    'cookie_request' => [
        'domain' => '.kyna.vn'
    ],
    'fb_offline_conversion' => [
        'accessToken' => 'EAACuSlRz3w4BACbSdU24ZC0aN0oiUW2TwtV8bM0dC3lFLqR5WayQQS30SaPiT8GqBOl2RIOYj4PWV5oAtWjtmtSQGLK0heJT6Go6z6bNYphA8uOxzm3FSGSprJZApZCWxgv9v3SgH6etJN0AembOzaMddD3DNy56POZBAzrPk2qRkQWCBfrX',
        'apiVersion' => 'v2.10',
        'systemUserId' => '100022765624753',
        'business_manager_id' => '1163841440297213',
        'data_set_id' => '137414683575437',
        'upload_tag' => 'test_upload_tag'
    ],
    'kyna_payment_url' => '10.140.0.4:8081',
	'kpoint_usable_time' => 72 * 3600, // 72 hours
    'is_campaign_1_1' => false,
    'discount_combo' => '',
    'campaign_discount_group' => false,
    'elasticsearch_name' => 'courses_product_staging',
    'sms' => [
        '_apiUrl' => 'https://cloudsms.vietguys.biz:4438/api/index.php',
        '_apiUser' => 'kyna.vn',
        '_apiSecretKey' => 'xuucr',
        '_brandName' => 'KYNA.VN'
    ],
    'campaign_birthday' => new DateTime('2018-06-29 13:30', new DateTimeZone('Asia/Ho_Chi_Minh')) > new DateTime('now', new DateTimeZone('Asia/Ho_Chi_Minh')) ? true : false,

    'start_promotion_time' => '2018-12-04 00:00',
    'end_promotion_time' => '2018-12-07 23:59',
    'not_apply_promo_course_ids' => [1105, 1185, 817],

    'intervals_time' => [['2018-10-18 00:00:00', '2018-10-22 23:59:59']],
    'campaign_list_course_ids' => [1,3,4,5,6,12,14,15,17,19,20,27,28,29,30,31,44,49,51,53,55,61,62,63,64,65,66,67,69,71,76,78,79,81,82,85,86,90,91,92,94,100,105,107,108,115,116,117,118,119,120,121,122,124,127,128,129,130,132,133,137,140,141,142,143,144,145,146,147,148,149,150,151,152,153,154,155,156,159,161,162,163,164,168,169,170,171,172,173,174,175,177,178,180,181,185,186,187,188,190,191,193,194,195,196,197,198,201,202,203,206,207,208,209,210,211,212,213,214,215,216,219,220,290,309,313,315,339,351,352,353,354,355,356,357,358,359,368,369,370,371,372,373,374,375,376,378,379,380,381,382,383,384,414,415,416,417,418,434,435,442,443,444,445,446,447,448,465,467,481,482,484,485,486,487,488,489,490,492,493,497,503,504,519,520,521,524,525,532,534,535,543,544,545,546,549,550,551,554,557,564,567,569,571,572,573,579,581,582,588,591,598,599,604,605,606,609,610,611,612,613,614,620,621,623,624,625,630,631,632,633,634,635,636,640,641,642,643,644,646,647,648,650,651,652,653,655,659,660,661,664,670,671,677,678,679,680,681,685,694,695,696,698,699,700,701,702,710,712,713,714,715,719,720,721,722,723,725,726,727,728,729,730,731,732,733,734,735,736,737,738,739,748,749,750,751,752,755,756,760,761,762,764,765,766,767,768,769,780,784,787,790,791,793,795,796,797,799,800,801,802,804,805,809,810,811,812,813,814,815,816,818,822,824,825,826,829,830,836,837,838,839,841,843,847,848,849,850,851,852,853,854,855,856,857,861,862,863,865,866,867,868,870,871,872,873,874,875,876,877,878,879,880,881,883,887,888,890,897,906,910,912,918,920,921,922,923,927,931,933,938,939,940,946,947,948,949,950,997,998,999,1000,1001,1002,1003,1004,1007,1008,1009,1011,1090,1107,1112,1121,1122,1123,1125,1126,1127,1128,1132,1133,1134,1135,1136,1137,1138,1139,1140,1141,1142,1143,1144,1145,1146,1147,1148,1149,1150,1151,1152,1153,1154,1156,1157,1158,1159,1160,1161,1164,1167,1168,1170,1172,1173,1174,1175,1176,1177,1178,1179,1180,1181,1182,1183,1184,1187,1191,1192,1193,1194,1195,1196,1197,1201,1203,1204,1205,1206,1207,1208,1209,1210,1211,1212,1213,1214,1215,1216,1217,1218,1219,1220,1221,1222,1223,1224,1225,1226,1228,1229,1230,1231,1236,1237,1238,1239,1240,1241,1242,1245],
    'flashsale_interval_increase' => 5,
    'flashsale_increase_number' => 5,
    'intervals_time_voucher_free' => ['2018-10-22 00:00:00', '2018-10-23 23:59:59'],
    'campaign_voucher_free'=>[484,486,490,493,313,315,121,122,127,132,137,146,156,162,163,168,169,170,174,186,190,193,194,195,197,202,206,212,213,214,309,351,352,356,357,359,369,370,371,373,374,375,376,378,379,383,384,416,417,418,435,446,447,448,520,521,534,549,567,573,599,604,606,620,621,641,653,659,700,730,732,734,735,737,750,767,793,829,878,4,12,14,15,17,19,180,181,487,488,489,866,906,1127,580,528,200,199,176,158,134,77],

    'intervals_time_dong_gia' => ['2018-11-08 00:01:00', '2018-11-11 23:59:59'],
    'campaign_dong_gia'=>[1,3,4,5,6,12,14,15,17,19,27,28,29,30,44,49,51,53,55,61,62,63,64,65,66,67,69,76,90,91,92,94,100,105,116,117,118,119,120,121,122,127,128,129,130,132,133,137,140,141,142,143,144,145,146,147,148,149,150,151,152,153,154,155,156,159,161,162,163,164,168,169,170,171,172,173,174,175,177,178,180,181,185,186,187,188,190,191,193,194,195,196,197,198,201,202,203,206,207,208,209,210,211,212,213,214,215,216,219,220,290,309,313,315,339,351,352,353,354,355,356,357,358,359,368,369,370,371,372,373,374,375,376,378,379,380,381,382,383,384,414,415,416,417,418,434,435,442,443,444,445,446,447,448,465,467,481,482,484,485,486,487,488,489,490,492,493,497,503,504,519,520,521,524,525,532,534,543,544,545,546,549,550,551,554,557,564,567,569,571,572,573,579,581,582,588,591,598,599,604,605,606,620,621,623,624,625,630,632,633,634,635,636,640,641,642,643,644,646,647,648,650,651,652,653,655,659,660,661,664,670,671,677,678,679,680,681,685,694,695,696,699,700,701,702,710,712,713,714,715,719,720,721,722,723,725,726,727,728,729,730,731,732,733,734,735,736,737,738,739,748,749,750,751,752,755,756,760,761,762,764,765,766,767,768,769,780,784,787,790,791,793,795,796,797,799,800,801,802,804,805,809,810,811,812,813,814,815,816,818,822,824,825,826,829,830,836,837,838,839,841,843,847,848,849,850,851,852,853,854,855,856,857,861,862,863,865,866,867,868,870,871,872,873,874,875,876,877,878,879,880,881,883,887,888,890,897,906,910,912,918,920,921,922,923,927,931,933,938,939,940,946,947,948,949,950,997,998,999,1000,1001,1002,1003,1004,1007,1008,1009,1011,1090,1107,1112,1121,1122,1123,1125,1126,1127,1128,1132,1133,1134,1135,1136,1137,1138,1139,1140,1141,1142,1143,1144,1145,1146,1147,1148,1149,1150,1151,1152,1153,1154,1156,1157,1158,1159,1160,1161,1164,1167,1168,1170,1172,1174,1175,1176,1177,1178,1179,1180,1181,1182,1183,1184,1187,1191,1192,1193,1194,1195,1196,1197,1201,1203,1204,1205,1206,1207,1208,1209,1210,1211,1212,1213,1214,1215,1216,1217,1218,1219,1220,1221,1222,1223,1224,1225,1229,1230,1231,1236,1237,1238,1239,1240,1241,1242,1245],

    'intervals_time_birthday_soroban' => ['2019-01-16 09:59:59', '2019-02-15 16:00:00'],
    'campaign_minigame'=>[4,12,14,15,17,19,121,127,132,137,146,156,168,169,174,176,180,181,186,190,193,194,195,197,199,200,202,206,309,313,315,351,352,356,357,370,371,374,375,376,383,384,416,417,418,435,446,489,490,520,521,534,567,573,580,599,606,620,621,641,653,659,700,730,732,734,735,737,750,767,793,829,906,1127,143,187,211,215,368,369,373,379,382,447,448,467,485,604,636,671,685,719,731,733,738,749,764,769,790,791,800,801,813,815,818,854,865,872,876,878,923,931,933,938,939,949,62,63,545,549,721,768,661,119,196,290,355,359,378,465,519,579,582,642,644,677,694,712,720,752,761,766,784,795,797,804,805,811,812,816,822,830,837,841,843,847,848,849,851,852,857,866,867,868,871,873,874,875,883,897,918,1133,1150,29,853,208,503,635,643,751,870,660,5,28,76,92,116,118,141,148,151,162,170,188,191,203,212,219,339,353],
    'intervals_time_voucher_free_ref' => ['2019-01-18 00:01:00', '2019-01-19 23:59:59'],
    'disable_position'=> false,

    'prefix_voucher_free_ref'=>'FREEKYNAREF',
    'landing_page_id_voucher_free_ref'=>342,

    'intervals_time_campaign_hot_sale' => ['2018-12-24 00:01:00', '2019-01-08 23:59:59'],
    'campaign_hot_sale'=>[1,3,4,5,6,12,14,15,17,19,27,28,29,30,44,49,51,53,55,61,62,63,64,65,66,69,76,90,91,92,94,100,105,116,117,118,119,120,121,122,127,128,129,130,132,133,137,140,141,142,143,144,146,147,148,149,150,151,152,153,154,156,159,161,162,163,164,168,169,170,171,172,173,174,175,177,178,180,181,185,186,187,188,190,191,193,194,195,196,197,198,201,202,203,206,207,208,209,210,211,212,213,214,215,216,219,220,290,309,313,315,339,351,352,353,354,355,356,357,358,359,368,369,370,371,372,373,374,375,376,378,379,380,381,382,383,384,414,415,416,417,418,434,435,442,443,444,445,446,447,448,465,467,481,482,484,485,486,487,488,489,490,492,493,497,503,504,519,520,521,524,525,532,534,543,544,545,546,549,550,551,554,557,564,567,569,571,572,573,579,581,582,588,591,598,599,604,605,606,620,621,623,624,625,630,632,633,634,635,636,640,641,642,643,644,646,647,648,650,651,652,653,655,659,660,661,664,670,671,677,678,679,680,681,685,694,695,696,699,700,701,702,710,712,714,715,719,720,721,722,723,725,726,727,728,729,730,731,732,733,734,735,736,737,738,739,748,749,750,751,752,755,756,760,761,762,764,765,766,767,768,769,780,784,787,790,791,793,795,796,797,799,800,801,802,804,805,809,810,811,812,813,814,815,816,818,822,824,825,826,829,830,836,837,838,839,841,843,847,848,849,850,851,852,853,854,855,856,857,861,862,863,865,866,867,868,870,871,872,873,874,875,876,877,878,879,880,883,887,888,890,897,906,910,912,918,920,921,922,923,927,931,933,938,939,940,946,947,948,949,950,997,998,999,1000,1001,1002,1003,1004,1007,1008,1009,1011,1090,1107,1112,1121,1122,1123,1125,1126,1127,1128,1132,1133,1134,1135,1136,1137,1138,1139,1140,1141,1142,1143,1144,1145,1146,1147,1148,1149,1150,1151,1152,1153,1154,1156,1157,1158,1159,1160,1161,1164,1167,1168,1170,1172,1174,1175,1176,1177,1178,1179,1180,1181,1182,1183,1184,1187,1191,1192,1193,1194,1195,1196,1197,1201,1203,1204,1205,1206,1207,1208,1209,1210,1211,1212,1213,1214,1215,1216,1217,1218,1219,1220,1221,1222,1223,1224,1225,1229,1230,1231,1236,1237,1238,1239,1240,1241,1242,1245],

    'intervals_time_campaign_tet' => ['2019-01-01 00:01:00', '2019-01-30 23:59:59'],
    'intervals_time_campaign_tet_hours' => [['09:01:00', '11:59:59'], ['14:01:00', '16:59:59'],['20:01:00', '22:59:59']],
    'count_total_user_course'=>[926=>500]
];
