<?php

return [
    'limit_record'=>10,
    'product-index-alias' => 'courses_product',
    'media_url' => 'https://mobile-api-staging.kyna.vn/media',
    'video_url' => 'https://mobile-api-staging.kyna.vn',
    'videoServer' => 'https://media.kyna.vn:1443/vodhtml5livekyna',
    'facebook_client_id'     => '233393280519661',
    'facebook_client_secret' => 'e675bbbe0c343d45ddd503527af4a6e2',
    'topic_name' => ['kyna_mobile','kyna_mobile_dev'],
    'static_link' => 'https://mobile-api-staging.kyna.vn/media',
    'media_link' => 'https://mobile-api-staging.kyna.vn/media',
    'api_version' => '1.4',
    'is_review' => false,
];
