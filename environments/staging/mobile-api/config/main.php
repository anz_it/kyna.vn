<?php
/**
 * Created by PhpStorm.
 * User: khanhphan
 * Date: 9/26/17
 * Time: 10:57 AM
 */

$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'), require(__DIR__ . '/../../common/config/params-local.php'), require(__DIR__ . '/params.php'), require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'mobile-api',
    'language' => 'vi',
    'basePath' => dirname(__DIR__),
    'bootstrap' => [
        'log',
    ],
    'controllerNamespace' => 'mobile\controllers',

    'components' => [
        'user' => [
            'class' => 'mobile\components\ApiWebUser',
            'identityClass' => 'mobile\models\User',
            'identityCookie' => [
                'name' => '_identity',
                'path' => '/',
                'domain' => ".mobile-api-staging.kyna.vn",
            ],

        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'enableStrictParsing' => true,
            'showScriptName' => false,
            'rules' => [
                'v1/auth/<action>' => 'v1/auth/<action>',
                'v1/learning/<action>' => 'v1/learning/<action>',
                'v1/profile/<action>' => 'v1/profile/<action>',
                'v1/quiz/<action>' => 'v1/quiz/<action>',
                'v1/test/<action>' => 'v1/test/<action>',
                'v1/<action>' => 'v1/guest/<action>',
                'thanh-toan-mobile/<orderId:\d+>'              => '/cart/mobile-checkout/index',
                'thanh-toan-mobile'                            => '/cart/mobile-checkout/index',
                'thanh-toan-mobile/thanh-cong/<orderId:\d+>'   => '/cart/mobile-checkout/success',
                'thanh-toan-mobile/khong-thanh-cong'           => '/cart/mobile-checkout/failure',
                'mobile-checkout/validate-email'               =>'/cart/mobile-checkout/validate-email',

                'mobile-checkout/add-user-care'                =>'/cart/mobile-checkout/add-user-care',
                'gio-hang'                                     => '/cart/default/index',
                'cart/api/response'                            =>'/cart/api/response',
                'cart/mobile-checkout/success'                 =>'/cart/mobile-checkout/success',
                'cart/mobile-checkout/failure'                 =>'/cart/mobile-checkout/failure',
                'cart/promo/apply'                              =>'/cart/promo/apply'

            ],
        ],
        'request' => [
            'parsers' => [
                'application/json' => 'yii\web\JsonParser',
            ],
            'enableCsrfValidation' => true,
            'cookieValidationKey' => 'fM2hT237NcmHV8A4yNvPBa8J8s0iMLa8',
            'csrfCookie' => [
                'name' => '_csrf',
                'path' => '/',
                'domain' => ".mobile-api-staging.kyna.vn",
            ],
        ],
        'errorHandler' => [
            'class' => '\mobile\components\ApiErrorHandler',
        ],
        'session' => array(
            //'savePath' => '/some/writeable/path',
            'cookieParams' => array(
                'path' => '/',
                'domain' => '.mobile-api-staging.kyna.vn',
            ),
        ),


        'response' => [
            'class' => 'yii\web\Response',
            'on beforeSend' => function ($event) {
                $path = \Yii::$app->requestedRoute;
                $response = $event->sender;
                $except_paths = [
                    'cart/mobile-checkout/index',
                    'thanh-toan-mobile',
                    'cart/mobile-checkout/success',
                    'thanh-toan-mobile/thanh-cong',
                    'thanh-toan-mobile/khong-thanh-cong',
                    'cart/mobile-checkout/failure',
                    'mobile-checkout/validate-email',
                    'cart/mobile-checkout/validate-email',
                    'cart/mobile-checkout/add-user-care',
                    'cart/default/index',
                    'cart/api/response',
                    'cart/checkout/failure',
                    'cart/promo/apply'
                    ];
                    if ($response->data !== null && !in_array($path,$except_paths)) {
                        $response->data = [
                            'success' => $response->isSuccessful,
                            'data' => $response->data,
                        ];
                        //$response->statusCode = 200;
                    }

            },
        ],


        'cart' => [
            'class' => 'mobile\modules\cart\components\ShoppingCart',
            'cartId' => 'kyna-cart',
        ],
        'settings' => [
            'class' => 'mobile\components\Settings',
        ],
        'category' => [
            'class' => 'mobile\modules\cart\components\Category',
            'urlWithId' => false,
        ],
        'assetManager' => [
            //'baseUrl' => '@cdn/assets',
            'linkAssets' => false,
            'bundles' => [
                'yii\web\JqueryAsset' => [
                    'sourcePath' => '@bower',
                    'js' => [
                        'jquery/dist/jquery.min.js',
                    ]
                ],
                'yii\bootstrap\BootstrapAsset' => [
                    'css' => [],
                ],
            ],
        ],
    ],
    'modules' => [
        'v1' => [
            'class' => 'mobile\modules\v1\V1Module',
        ],
        'user' => [
            'class' => 'dektrium\user\Module',
            'enableConfirmation' => false,
        ],
        'treemanager' =>  [
            'class' => '\kartik\tree\Module',
        ],
        'cart'=>[
            'class' => 'mobile\modules\cart\CartModule',
        ]
    ],
    'params' => $params
];