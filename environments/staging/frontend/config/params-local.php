<?php
return [
    'beginLearningCourseId' => '10',
    'course-rating-page-size' => 10,
    'landing-page-redirects' => [
        // from => to
        'nhom-khoa-hoc/tang-khoa-ielts-mien-phi' => 'nhom-khoa-hoc/chinh-phuc-ielts-7-5',
        'nhom-khoa-hoc/luyen-thi-ielts-speaking-55' => 'nhom-khoa-hoc/chinh-phuc-ielts-7-5',
        'nhom-khoa-hoc/luyen-thi-ielts-speaking-55-v2' => 'nhom-khoa-hoc/chinh-phuc-ielts-7-5',
        'nhom-khoa-hoc/luyen-thi-ielts-speaking-55-v3' => 'nhom-khoa-hoc/chinh-phuc-ielts-7-5',
        'nhom-khoa-hoc/luyen-thi-ielts-speaking-55-v4' => 'nhom-khoa-hoc/chinh-phuc-ielts-7-5',
        'nhom-khoa-hoc/luyen-ielts-speaking-cung-chuyen-gia' => 'nhom-khoa-hoc/chinh-phuc-ielts-7-5',
    ],
    'category-combo-id' => 66,
    'category-all-id' => 65,
    'idOfOtherDisplayAffCategory' => 6,
    'no-index-urls' => [
        '/noi-voi-con-ve-tinh-duc'
    ],
    'lp_banned_emails' => ['dangthuong1955@gmail.com','cucthao1959@gmail.com','nhuthe1957@gmail.com','dangkhuong1933@gmail.com','thanhnhan1988@gmail.com']
];
